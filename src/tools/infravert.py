#!/usr/bin/env python
import re
from functools import partial
import ms.version
ms.version.addpkg('argparse', '1.2.1')
import argparse
ms.version.addpkg('lxml', '3.2.5')
from lxml import etree

INDENT = '    ' # indent with 4 spaces
DEBUG  = False

class Orientation:
	Horizontal = 0
	Vertical   = 1
	
'''----------------------------------------------------------------------------
| The intermediate representation used for widget in the XamRibbon.           |
----------------------------------------------------------------------------'''
class Widget:
	Unknown        = 0
	Comment        = 1
	RibbonTabItem  = 2
	RibbonGroup    = 3
	Panel          = 4
	ButtonGroup    = 5
	DropdownButton = 6
	Button         = 7
	ComboBox       = 8
	Label          = 9
	
	def __init__(self, type, id, properties, errors):
		self.id         = id
		self.type       = type
		self.properties = properties
		self.errors     = errors
		self.children   = []
		
	
'''----------------------------------------------------------------------------
| Goes through each element of the XAML and generates the correct code for    |
| them.                                                                       |
----------------------------------------------------------------------------'''
# Makes assumption of well formed XAML code
class XamRibbon:
	MINSIZEATTR = '{http://infragistics.com/Ribbon}RibbonGroup.MinimumSize'
	MAXSIZEATTR = '{http://infragistics.com/Ribbon}RibbonGroup.MaximumSize'

	class ConversionException(Exception):
		def __init__(self, msg):
			self.msg = msg
	
	class Param:
		def __init__(self, match, name, conversion):
			self.match      = match
			self.name       = name
			self.conversion = conversion
	
	def __init__(self, file_name, recover):
		self._file_name = file_name
		# Parse the XML
		parser = etree.XMLParser()
		try:
			tree       = etree.parse(file_name)
			self._root = tree.getroot()
		except etree.ParseError as e:
			print 'XML parse error:  '+e.msg
		self._recover = recover

	def convert(self, igRibbon, wpf):
		root = Widget(Widget.Unknown, self._file_name, {}, [])
		self._stack = [root]
		self._unknown_count = 0
		
		# Ignore Id
		self._ignore_id_params = [
			self.Param('Id', '', self._ignore),
		]
		# Button
		self._button_params = [
			self.Param('Id'            , ''           , self._ignore),
			self.Param('Caption'       , 'Text'       , self._string),
			self.Param('IsEnabled'     , 'Enabled'    , self._bool),
			self.Param('IsCheckable'   , 'IsCheckable', self._bool),
			self.Param('LargeImage'    , 'Image'      , self._image),
			self.Param('SmallImage'    , 'SmallImage' , self._image),
			self.Param('ToolTip'       , 'ToolTip'    , self._string),
			self.Param(self.MAXSIZEATTR, ''           , self._ignore),
			self.Param(self.MINSIZEATTR, ''           , self._ignore),
		]
		# ComboBox
		self._combo_box_params = [
			self.Param('Id'        , ''        , self._ignore),
			self.Param('IsEditable', 'Editable', self._bool),
		]
		# Label
		self._label_params = [
			self.Param('Id'  , ''    , self._ignore),
			self.Param('Text', 'Text', self._string),
		]
		
		# Map the handlers for each XamRibbon element
		igRibbon = '{'+igRibbon+'}'
		wpf      = '{'+wpf+'}'
		self._element_handlers = {
			# Ignore
			igRibbon+'XamRibbon'              : self._convert_children,
			igRibbon+'XamRibbon.Tabs'         : self._convert_children,
			wpf+'ResourceDictionary'          : self._convert_children,
			# Convert
			igRibbon+'RibbonTabItem'          : self._RibbonTabItem,
			igRibbon+'RibbonGroup'            : self._RibbonGroup,
			igRibbon+'ToolHorizontalWrapPanel': partial(self._Panel, True, Orientation.Horizontal),
			igRibbon+'ToolVerticalWrapPanel'  : partial(self._Panel, True, Orientation.Vertical),
			igRibbon+'ButtonGroup'            : partial(self._widget, Widget.ButtonGroup, self._ignore_id_params),
			igRibbon+'MenuTool'               : partial(self._sized_widget, Widget.DropdownButton, self._button_params),
			igRibbon+'ButtonTool'             : partial(self._Button, False),
			igRibbon+'ToggleButtonTool'       : partial(self._Button, True),
			igRibbon+'ToolMenuItem'           : partial(self._widget, Widget.Button, self._button_params),
			wpf+'ComboBox'                    : partial(self._widget, Widget.ComboBox, self._combo_box_params),
			wpf+'TextBlock'                   : partial(self._widget, Widget.Label, self._label_params),
			wpf+'StackPanel'                  : partial(self._Panel, False, Orientation.Vertical),
		}
		# Do the conversion
		self._convert_element(self._root)
		if converter._unknown_count > 0:
			print str(converter._unknown_count)+' elements found that could not be converted'
		return root

	def _convert_element(self, element):
		self._element_handlers.get(element.tag, self._Unknown)(element)

	def _convert_children(self, element):
		for child in element: self._convert_element(child)
	
	def _push_widget(self, widget):
		self._stack[-1].children.append(widget)
	
	# Generic widget conversions
	def _widget(self, type, params, element, properties={}, errors=[], id=None):
		new_properties, new_errors = self._convert_properties(params, element)
		new_properties.update(properties)
		new_errors.extend(errors)
		widget = Widget(type, id or element.get('Id'), new_properties, new_errors)
		
		if DEBUG: widget.line = element.sourceline

		self._push_widget(widget)
		
		if len(element) > 0:
			self._stack.append(widget)
			self._convert_children(element)
			self._stack.pop()
			
	def _sized_widget(self, type, params, element, properties={}, errors=[], id=None):
		try:
			if self.MAXSIZEATTR in element.keys() or self.MINSIZEATTR in element.keys():
				properties['Size'] = self._resolve_size(element.get(self.MAXSIZEATTR), element.get(self.MINSIZEATTR))
		except self.ConversionException as e:
				errors.append(e.msg)
		self._widget(type, params, element, properties, errors, id)
				
	def _convert_properties(self, conversions, element):
		params = {}
		errors = []
		
		for attr in element.items():
			try:
				converted = False
				for conversion in conversions:
					if attr[0] == conversion.match:
						params[conversion.name] = conversion.conversion(attr[1])
						converted = True
						break;
						
				if not converted:
					errors.append('Cannot convert attribute // '+attr[0]+'='+attr[1])
					
			except self.ConversionException as e:
				errors.append(e.msg)
			except UserWarning:
				pass
		
		return (params, errors)
		
	# Custom widget conversions
	def _RibbonTabItem(self, element):
		self._widget(Widget.RibbonTabItem, {}, element, id=element.get('Header'))

	def _RibbonGroup(self, element):
		self._widget(Widget.RibbonGroup  , {}, element, id=element.get('Caption'))

	def _Panel(self, wrap, orientation, element):
		properties = {'Wrap': 'true' if wrap else 'false'}
		if element.get('Orientation') == None:
			if orientation == Orientation.Horizontal: properties['Orientation'] = 'Horizontal'
			else:                                     properties['Orientation'] = 'Vertical'
		else:
			properties['Orientation'] = element.get('Orientation')
		self._sized_widget(Widget.Panel, self._ignore_id_params, element, properties)
		
	def _Button(self, checkable, element):
		properties = {}
		if checkable: properties['IsCheckable'] = 'true'
		self._sized_widget(Widget.Button, self._button_params, element, properties)
	
	def _Unknown(self, element):
		if type(element.tag) is str:
			self._unknown_count += 1
			if self._recover:
				self._widget(Widget.Unknown, {}, element, {'Tag': element.tag, 'Line': element.sourceline})
			else:
				snippet = etree.tostring(element).rstrip()
				snippet = re.sub(r' xmlns(:.*?)?=".*?"', '', snippet) # remove the namespace clutter
				if snippet.count('\n') > 0:
					snippet = '\n'+snippet+'\n'
				self._push_widget(Widget(Widget.Comment,
				                         None,
				                         {'Text': snippet},
				                         ['Do not know how to convert the below from near line '+str(element.sourceline)+':']))
		else: # Convert the comments
			self._push_widget(Widget(Widget.Comment, None, {'Text': element.text}, []))
	
	# Conversion helpers
	def _ignore(self, txt):
		raise UserWarning()
	
	def _string(self, txt):
		return '@"'+txt+'"'
	
	def _bool(self, txt):
		return txt.lower()
	
	def _image(self, txt):
		return 'new BitmapImage(new Uri(@"'+txt+'"))'

	def _size(self, txt):
		if    txt == 'ImageOnly'         : return 'ImageOnly'
		elif  txt == 'ImageAndTextNormal': return 'Small'
		elif  txt == 'ImageAndTextLarge' : return 'Large'
		else: raise self.ConversionException(txt+' not a supported size')

	def _resolve_size(self, min_size, max_size):
		# If both sizes set to same or only one is set use the size
		if min_size != None and max_size != None:
			if min_size == max_size: return self._size(min_size)
			else: raise ConversionException('Cannot convert sizes // min=%s max=%s' % (min_size, max_size))
		elif min_size != None: return self._size(min_size)
		elif max_size != None: return self._size(max_size)
		

'''----------------------------------------------------------------------------
| Uses knowledge of the MSDesktop interface to generate code to create the UI |
| elements in C#.                                                             |
----------------------------------------------------------------------------'''
class CSharpWriter():

	def __init__(self, initial_tab, initial_group):
		self._chrome_manager = '_chromeManager'
		self._initial_tab   = initial_tab
		self._initial_group = initial_group
		
	def write(self, file, tree):
		self._file        = file
		self._code        = CodeWriter(file)
		self._tab         = self._initial_tab
		self._group       = self._initial_group
		self._warn_count  = 0
		self._error_count = 0
		
		self._var_counts = {}
		self._stack      = [] 
		
		self._element_writers = {
			Widget.Unknown        : self._Unknown,
			Widget.Comment        : self._Comment,
			Widget.RibbonTabItem  : self._RibbonTabItem,
			Widget.RibbonGroup    : self._RibbonGroup,
			Widget.Panel          : self._Panel,
			Widget.ButtonGroup    : partial(self._widget, 'buttonGroup'   , 'InitialButtonGroupParameters'   , 'IButtonGroupViewContainer'),
			Widget.DropdownButton : partial(self._widget, 'dropdownButton', 'InitialDropdownButtonParameters', 'IDropdownButtonViewContainer'),
			Widget.Button         : self._Button,
			Widget.ComboBox       : partial(self._widget, 'comboBox'      , 'InitialComboBoxParameters'      , ''),
			Widget.Label          : partial(self._widget, 'label'         , 'InitialLabelParameters'         , ''),
		}
		
		self._preamble(tree.id)
		self._write_children(tree)
		self._postamble()
		
		msg = ''
		if self._error_count > 0:
			msg += str(self._error_count)+' errors'
			if self._warn_count > 0:
				msg += ' and '
		if self._warn_count > 0:
			msg += str(self._warn_count)+' warnings'
		if len(msg) > 0:
			print msg+' need addressing'
		
	def _write_children(self, widget):
		for item in widget.children:
			self._element_writers[item.type](item)
			
	def _widget_preamble(self, widget, name):
		# Check there is a tab and group for the widget to be placed into
		if   self._tab   == None:
			raise IOError("No tab to add widget to. Use --tab.")
		elif self._group == None:
			raise IOError("No group to add widget to. Use --group.")
			
		# Check if a variable declaration is needed
		if len(widget.children) == 0:
			line = ''
		else:
			num = self._var_counts.get(name) or 0
			self._var_counts[name] = num + 1
			line = 'var '+name+str(num)+' = '
		
		# Add to ribbon or container
		if len(self._stack) == 0:
			line += self._chrome_manager+'.Ribbon[@"'+self._tab+'"]'+'[@"'+self._group+'"].AddWidget('
		else:
			line += self._stack[-1]+'.AddWidget('
		
		# Add widget ID
		if widget.id != None: line += '@"'+widget.id+'",'
		
		self._code.line(line)
		self._code.indent()
		
	def _widget_postamble(self, widget, name, type=''):
		self._code.unindent()
		if len(type) > 0: type = ' as '+type
		self._code.line(')' + type + ';')
		self._code.line()
	
	def _object_with_params(self, type, widget):
		if   len(widget.properties) == 0 and len(widget.errors) == 0:
			self._code.line('new '+type+'()')
		elif len(widget.properties) == 1 and len(widget.errors) == 0:
			self._code.line('new '+type+' { '+widget.properties.keys()[0]+' = '+widget.properties.values()[0]+' }')
		else:
			self._code.line('new '+type)
			self._code.line('{')
			self._code.indent()
			# Calculate formatting
			align = 0
			for key in widget.properties.keys():
				align = max(align, len(key))
			align += 1
			# Put the params
			for param in widget.properties.items(): self._code.line(param[0].ljust(align)+'= '+param[1]+',');
			# Put the errors
			if DEBUG and len(widget.errors) > 0: self._code.line('// From near line '+str(widget.line))
			for err in widget.errors: self._error(err)
			# Close up scope
			self._code.unindent()
			self._code.line('}')
			
	def _widget(self, name, initial_params_type, cast_type, widget):
		self._widget_preamble(widget, name)
		self._object_with_params(initial_params_type, widget)
		self._widget_postamble(widget, name, cast_type)
		
		if len(widget.children) > 0:
			self._stack.append(name+str(self._var_counts.get(name)-1))
			self._write_children(widget)
			self._stack.pop()
	
	def _Unknown(self, unknown):
		num = self._var_counts.get('unknown') or 0
		self._var_counts['unknown'] = num + 1
		tag  = unknown.properties['Tag']
		line = unknown.properties['Line']
		if DEBUG:
			if len(unknown.children) == 0:
				self._error('-       Ignored tag       from near line '+str(line)+' - // ' + tag)
			else:
				self._error('< ('+str(num).rjust(3)+') Ignored tag open  from near line '+str(line)+' < // ' + tag)
				self._write_children(unknown)
				self._error('> ('+str(num).rjust(3)+') Ignored tag close from near line '+str(line)+' > // ' + tag)
		else:
			self._error('Ignored tag (from near line '+str(line)+') // ' + tag)
			self._write_children(unknown)
	
	def _Comment(self, comment):
		for err in comment.errors:
			self._error(err)
		self._code.line('/*'+comment.properties['Text']+'*/')
	
	def _RibbonTabItem(self, ribbonTabItem):
		self._tab = ribbonTabItem.id
		self._write_children(ribbonTabItem)
		self._tab = None
	
	def _RibbonGroup(self, ribbonTabGroup):
		self._group = ribbonTabGroup.id
		self._write_children(ribbonTabGroup)
		self._group = None
	
	def _Panel(self, panel):
		# Need to convert orientation it use the enum
		panel.properties['Orientation'] = 'InitialPanelParameters.Layout.' + panel.properties['Orientation']
		self._widget('panel', 'InitialPanelParameters', 'IPanelViewContainer', panel)
		
	def _Button(self, button):
		# Need to convert button size
		if 'Size' in button.properties:
			button.properties['Size'] = 'ButtonSize.' + button.properties['Size']
		self._widget('panel', 'InitialButtonParameters', '', button)
	
	def _preamble(self, file_name):
		self._code.line('// Generated by infravert')
		self._code.line('// Converted from '+file_name)
		if DEBUG:
			self._code.line('// DEBUG:')
			self._code.line('//  * Numbers match open and close of ignored tags')
			self._code.line('//  * < open, > close, - empty tag')
		self._file.write(
"""using System;
using System.Windows.Media.Imaging;

using Microsoft.Practices.Composite.Modularity;

using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.ViewContainers;

namespace %s
{
    class %s : IModule
    {
        public %s(IChromeManager chromeManager)
        {
            _chromeManager  = chromeManager;
        }

        public void Initialize()
        {
""" % (namespace, module, module)
		)
		self._code.indent(3)
		
	def _postamble(self):
		self._file.write(
"""        }
		
        private IChromeManager  _chromeManager;
	}
}
"""
		)
		
	def _warning(self, txt, comment=None):
		self._warn_count += 1
		self._preprocessor('warning', txt, comment)

	def _error(self, txt, comment=None):
		self._error_count += 1
		self._preprocessor('error', txt, comment)

	def _preprocessor(self, kind, txt, comment):
		indent = self._code.get_indent()
		self._code.set_indent(0)
		self._code.line('#'+kind+' '+txt)
		if comment != None:
			self._code.line('/*')
			self._code.line(comment)
			self._code.line('*/')
		self._code.set_indent(indent)
		
		
'''----------------------------------------------------------------------------
| Gives formating when writing code lines including support for indentation   |
| and preprocessor directives.                                                |
----------------------------------------------------------------------------'''
class CodeWriter:
	def __init__(self, file):
		self._file   = file
		self._indent = 0
	
	def indent    (self, delta=1): self._indent += delta
	def unindent  (self, delta=1): self._indent -= delta
	def get_indent(self)         : return self._indent
	def set_indent(self, indent) : self._indent = indent
	
	def line(self, txt=None):
		if txt != None:
			self._file.write((INDENT*self._indent)+txt)
		self._file.write('\n')


'''----------------------------------------------------------------------------
| Entry point.                                                                |
----------------------------------------------------------------------------'''
# Main
if __name__ == '__main__':
	# Options
	parser = argparse.ArgumentParser(description='Convert Infragistics XamRibbon XAML to an MSDotNet module')
	parser.add_argument('xaml'               , type=str, help='the XAML file to convert')
	parser.add_argument('-o', '--outfile'    , type=str, help='the output C# file')
	parser.add_argument('-n', '--namespace'  , type=str, help='the name of the namespace')
	parser.add_argument('-m', '--module'     , type=str, help='the name of the module')
	parser.add_argument('-t', '--tab'        , type=str, help='the initial tab name (e.g. when converting a tab group)')
	parser.add_argument('-g', '--group'      , type=str, help='the initial group name (e.g. when converting a within a tab group)')
	parser.add_argument(      '--igRibbon-ns', type=str, help='default http://infragistics.com/Ribbon')
	parser.add_argument(      '--wpf-ns'     , type=str, help='default http://schemas.microsoft.com/winfx/2006/xaml/presentation')
	parser.add_argument('-d', '--debug'  , action='store_const', const=True, help='output extra information to help analyse problems')
	parser.add_argument('-r', '--recover', action='store_const', const=True, help='recover from errors and recursively convert unkown tags')
	args = parser.parse_args()

	# Check options
	outfile   = args.outfile     or 'a.out.cs'
	module    = args.module      or 'A_Module'
	namespace = args.namespace   or 'A_Namespace'
	tab       = args.tab         or None
	group     = args.group       or None
	igRibbon  = args.igRibbon_ns or 'http://infragistics.com/Ribbon'
	wpf       = args.wpf_ns      or 'http://schemas.microsoft.com/winfx/2006/xaml/presentation'
	DEBUG     = args.debug       or False
	recover   = args.recover     or False
	
	# Convert
	converter = XamRibbon(args.xaml, recover)
	tree      = converter.convert(igRibbon, wpf)

	# Write as C#
	try:
		with open(outfile, 'w') as file:
			CSharpWriter(tab, group).write(file, tree)
	except IOError as e:
		print e
