﻿ 
 
using System.Windows;
using MorganStanley.MSDotNet.MSGui.Impl.DockManager;
using MorganStanley.MSDotNet.MSGui.Impl.DockManager.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.DockManager.Services;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;

namespace MSDesktop.Workspaces.DockManagerHookers
{
    [DockManagerHooker(true)]
    class HarmnniaDockManagerHooker:IDockManagerHooker
    {
        public void Hook()
        {
            DockManager.ContainerDocking += DockManager_ContainerDocking;
        }

        void DockManager_ContainerDocking(object sender, ContainerDockingEventArgs e)
        {
            var targetWindow = Window.GetWindow(e.TargetContainer);
            var sourceWindow = Window.GetWindow(e.SourceContainer);
            if (sourceWindow == targetWindow) return;
            if (WorkspaceManager.MixingMode &&
    !IsDockLayoutWorkspace(sourceWindow) && !IsDockLayoutWorkspace(targetWindow))
            {
                return;
            }

             if (e.TargetLocation == DockLocation.Center)
             {
                 if (e.SourceContainer.FindPane(p_ => p_.Content is TileView) != null)
                 {
                     return;
                 }
                 e.Cancel = true;
             }
             else
             {
                 e.Cancel = true;
             }
        }
        private static bool IsFreeDragablePane(ContentPane pane_)
        {
            WindowViewModel model = pane_.DataContext as WindowViewModel;
            return (model == null || string.IsNullOrEmpty(model.WorkspaceCategory) || pane_.Content is TileView);
        }

        private bool IsDockLayoutWorkspace(Window window_)
        {
            var container = window_.Content as PaneContainer;
            if (container == null) return false;
            return container.FindPane(IsFreeDragablePane) == null;
        }
    }
}
