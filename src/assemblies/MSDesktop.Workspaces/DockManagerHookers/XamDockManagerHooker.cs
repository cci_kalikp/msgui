﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Infragistics.Windows.DockManager;
using Infragistics.Windows.DockManager.Dragging;
using Infragistics.Windows.DockManager.Events; 
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions;

namespace MSDesktop.Workspaces.DockManagerHookers
{
    [DockManagerHooker(false)]
    class XamDockManagerHooker:IDockManagerHooker
    {
        public void Hook()
        {
            if (XamDockManagerExtensions.Manager != null)
            { 
                XamDockManagerExtensions.Manager.PaneDragOver += DockManager_PaneDragOver;
                XamDockManagerExtensions.Manager.PaneDragEnded += DockManager_PaneDragEnded;
                XamDockManagerExtensions.TearingOffPane += new EventHandler<CancelPaneEventArgs>(ContentPaneHelper_TearingOffPane);
                XamDockManagerExtensions.ToreOffPane += new EventHandler<PaneEventArgs>(ContentPaneHelper_ToreOffPane); 
            }
        }
         

        void ContentPaneHelper_ToreOffPane(object sender, PaneEventArgs e)
        {
            if (pendingTab != null)
            {
                var newWindow = FindParent<Window>(e.Pane);
                if (newWindow != Application.Current.MainWindow)
                {
                    pendingTab.Close();
                }
                pendingTab = null;
            }
        }

        void ContentPaneHelper_TearingOffPane(object sender, CancelPaneEventArgs e)
        {
            WindowViewModel model = e.Pane.DataContext as WindowViewModel;
            if (model != null && e.Pane.Content is TileView)
            {
                pendingTab = model.Host as ShellTabViewModel;
            }
        }

        void DockManager_PaneDragEnded(object sender, PaneDragEndedEventArgs e)
        {
            if (pendingTab != null)
            {
                var newWindow = FindParent<Window>(XamDockManagerExtensions.Manager.ActivePane);
                if (newWindow != Application.Current.MainWindow)
                {
                    pendingTab.Close();
                }
                pendingTab = null;
            }
 
        }


        private ShellTabViewModel pendingTab;
        private void DockManager_PaneDragOver(object sender, PaneDragOverEventArgs e)
        {
            var pane = e.Panes.First();

            if (e.DragAction is FloatPaneAction || e.DragAction is NewRootPaneAction)
            {
                if (IsFreeDragablePane(pane))
                {
                    e.IsValidDragAction = true;
                    if (WorkspaceManager.MixingMode) return;
                    WindowViewModel model = pane.DataContext as WindowViewModel;
                    if (e.DragAction is FloatPaneAction && model != null && pane.Content is TileView)
                    {
                        pendingTab = model.Host as ShellTabViewModel;
                    }
                    else if (e.DragAction is NewRootPaneAction && pane.Content is TileView && model != null)
                    {
                        //don't add tab, if user drag the window from tabbed mdi and then into it again
                        if (pendingTab == null)
                        {
                            pendingTab = ChromeManagerBase.Instance.Tabwell.AddTab(model.Title) as ShellTabViewModel;
                        }
                    }
                }
                else
                {
                    e.IsValidDragAction = false;
                }
            }
            else if (e.DragAction is AddToGroupAction)
            {
                var a = e.DragAction as AddToGroupAction;
                var targetWindow = FindParent<Window>(a.Group);
                if (WorkspaceManager.MixingMode && IsFreeDragablePane(pane) && !IsDockLayoutWorkspace(targetWindow))
                {
                    e.IsValidDragAction = true;
                    return;
                }

                if (a.Group is TabGroupPane)
                {
                    if (targetWindow == Application.Current.MainWindow)
                    {
                        e.IsValidDragAction = false;
                        return;
                    }
                    if (pane.Content is TileView)
                    {
                        e.IsValidDragAction = true;
                        return;
                    }
                }
                var originWindow = FindParent<Window>(e.Panes.First());
                e.IsValidDragAction = targetWindow == originWindow;

            }
            else if (e.DragAction is NewSplitPaneAction)
            {
                var a = e.DragAction as NewSplitPaneAction;
                var targetWindow = FindParent<Window>(a.Pane);
                if (WorkspaceManager.MixingMode && IsFreeDragablePane(pane) && !IsDockLayoutWorkspace(targetWindow))
                {
                    e.IsValidDragAction = true;
                    return;
                }

                var originWindow = FindParent<Window>(pane);
                e.IsValidDragAction = targetWindow == originWindow;
            }
            else if (e.DragAction is NewTabGroupAction)
            { 
                var a = e.DragAction as NewTabGroupAction;
                var targetWindow = FindParent<Window>(a.Pane);
                if (WorkspaceManager.MixingMode && IsFreeDragablePane(pane) && !IsDockLayoutWorkspace(targetWindow))
                {
                    e.IsValidDragAction = true;
                    return;
                }
                if (targetWindow == Application.Current.MainWindow)
                {
                    e.IsValidDragAction = false;
                }
                else
                {
                    if (pane.Content is TileView)
                    {
                        e.IsValidDragAction = true;
                    }
                    else
                    {
                        var originWindow = FindParent<Window>(pane);
                        e.IsValidDragAction = targetWindow == originWindow;
                    }
                }

            }
        }

        private bool IsDockLayoutWorkspace(Window window_)
        {
            CustomPaneToolWindow customPaneToolWindow = window_.Content as CustomPaneToolWindow;
            if (customPaneToolWindow == null) return false;
            var contentPane = customPaneToolWindow.ContentPane;
            if (contentPane == null) return false;
            return !IsFreeDragablePane(contentPane);
        }

        private static bool IsFreeDragablePane(ContentPane pane_)
        {
            WindowViewModel model = pane_.DataContext as WindowViewModel;
            return (model == null || string.IsNullOrEmpty(model.WorkspaceCategory) || pane_.Content is TileView);
        }
        private T FindParent<T>(FrameworkElement group)
            where T : FrameworkElement
        {
            while (group != null)
            {
                if (group is T)
                    return group as T;

                group = group.Parent as FrameworkElement;
            }

            return null;
        }
    }
}
