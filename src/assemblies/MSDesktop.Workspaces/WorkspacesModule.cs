﻿using Microsoft.Practices.Composite.Modularity;
using MSDesktop.Workspaces.Interfaces;
using MSDesktop.Workspaces.Options;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.Shell;

namespace MSDesktop.Workspaces
{
    public class WorkspacesModule : IEarlyInitializedFrameworkModule, IModule
    {
        private readonly WorkspaceManager workspaceManager;
        private readonly IApplicationOptions applicationOptions;
        private readonly ISubLayoutManager subLayoutManager;  

        public WorkspacesModule(IChromeRegistry chromeRegistry_, IChromeManager chromeManager_, ISubLayoutManager subLayoutManager_, IUnityContainer container_)
        {
            subLayoutManager = subLayoutManager_;
            Extensions.WorkspaceManager = this.workspaceManager = new WorkspaceManager(chromeRegistry_, chromeManager_, subLayoutManager_,
                container_.Resolve<IShell>(), container_.Resolve<PersistenceProfileService>(), container_.Resolve<InitialSettings>());
            applicationOptions = container_.Resolve<IApplicationOptions>();
            container_.RegisterInstance<IWorkspaceManager>(this.workspaceManager); 
        }

        public void Initialize()
        {
            var manager = new WorkspaceManagerOptionsPageControl(subLayoutManager);
            applicationOptions.AddOptionPage("MSDesktop", "Workspace Manager", manager);

            workspaceManager.Initialize();
        } 

    }

}
