﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq; 
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;
using System.Xml.Linq; 
using MSDesktop.Workspaces.Interfaces; 
using MSDesktop.Workspaces.Widgets;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop; 
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager; 
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.Factories;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions; 
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.Shell;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls; 
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.ViewModels; 

namespace MSDesktop.Workspaces   
{
    //todo: fix dock layout workspace exit
    //1) implement Closing event for ShellTabViewModel
    //2) implement Closing event for CustomPaneToolWindow that is triggered upon the active pane but invoked on the tool window (can be handled in PaneHeaderControl)
    //3) implement WindowViewContainer.RootChanged event, to be triggered when a workspace view is floated/docked. This event would be used by the workspace management to 
    //  3.1) register the saved/unsaved/opened/deleted workspaces
    //  3.2) hook/unhook Closing event of the root (ShellTabViewModel/IFloatingWindow), and check if the workspace can be closed in that event
    //4) implement WorkspaceFactoriesDropdownParameters to based on the Root instead of view container 
    internal class WorkspaceManager : IWorkspaceManager
    {
        internal static LayoutLoadStrategy WorkspaceCloseStrategy = LayoutLoadStrategy.Default;
        internal static LayoutLoadStrategyCallback WorkspaceCloseStrategyCallback = null;
        internal static LayoutEngine DefaultLayout = LayoutEngine.Dock;
        internal static bool SaveWorkspaceWhenSavingProfile = false;
        internal static string DefaultWorkspaceCategory;
        internal static InitialSettings ShellSettings;
        internal static IDictionary<string, string> DefaultWorkspaceTitles = new Dictionary<string, string>();
        internal static bool MixingMode = false;

        private const string WidgetPrefix = "MSDesktop.Workspaces.WorkspacesModule::";
        private const string EmptyWorkspaceFactoryId = "EmptyWorkspaceFactoryId";
        private readonly Dictionary<string, IDictionary<string, ImageSource>> workspaceCategories = new Dictionary<string, IDictionary<string, ImageSource>>(); 
        private readonly IChromeRegistry chromeRegistry;
        private readonly IChromeManager chromeManager;
        private readonly ISubLayoutManager subLayoutManager;
        private readonly IShell shell;
        private readonly PersistenceProfileService profileService;
        private bool floating = true;
        internal static SubLayoutLocation DefaultLocation = SubLayoutLocation.SavedLocation;
        internal SubLayoutLocation location = SubLayoutLocation.SavedLocation;
        private InitialSettings initialSettings;
       
        public WorkspaceManager(IChromeRegistry chromeRegistry_, IChromeManager chromeManager_, 
            ISubLayoutManager subLayoutManager_, IShell shell_, PersistenceProfileService profileService_, InitialSettings initialSettings_)
        {

            this.chromeRegistry = chromeRegistry_;
            this.chromeManager = chromeManager_;
            this.subLayoutManager = subLayoutManager_;
            this.shell = shell_;
            this.profileService = profileService_;
            initialSettings = initialSettings_;
            profileService.ProfileLoading += profileService_ProfileLoading;
            profileService_.ProfileSaved += monitor_ProfileSaved; 
            this.chromeRegistry.RegisterWindowFactory(EmptyWorkspaceFactoryId, (container_, state_) =>
                { 
                    if (string.IsNullOrEmpty(container_.Parameters.WorkspaceCategory))
                    {
                        container_.Parameters.WorkspaceCategory = DefaultWorkspaceCategory;
                    }
                    container_.Title = GetDefaultWorkspaceTitle(container_.Parameters.WorkspaceCategory);
                    container_.Parameters.InitialLocation = floating ? InitialLocation.Floating : InitialLocation.DockInNewTab;
                    IWidgetViewContainer dd = null;
                     dd = container_.Header.AddWidget(
                            WidgetPrefix + container_.Parameters.WorkspaceCategory + Guid.NewGuid(),
                            new InitialDropdownButtonParameters()
                                {
                                    Text = "Edit",
                                    Opening = (sender_, args_) =>
                                        {
                                            WorkspaceFactoriesDropdown.GenerateItems(dd, this, subLayoutManager_, container_.Parameters.WorkspaceCategory, LayoutEngine.Dock, container_); 
                                        }
                                });
                    dd.AddWidget(new InitialButtonParameters { Text = "dummy" });  
                    return true;
                });
 
             
        }
        public void Initialize()
        {
            WindowFactoryHolder holder;
            ((IChromeElementInitializerRegistry)chromeRegistry).TryGetWindowFactoryHolder(
                typeof(InitialTileParameters), out holder);
            holder.AddStep(InitializeTileWorkspace);
            if (DefaultLocation != SubLayoutLocation.SavedLocation)
            {
                location = DefaultLocation;
                floating = DefaultLocation == SubLayoutLocation.Floating;
            }
            else if (Bootstrapper.ShellModeUserConfigurable)
            {
                floating = ((ChromeManagerBase)chromeManager).Shell.WindowManager is FloatingOnlyDockViewModel;
                location = floating ? SubLayoutLocation.Floating : SubLayoutLocation.DockedInNewTab;
            }
            ((ChromeManagerBase) chromeManager).WindowManager.RegisterDockManagerHookers(this.GetType().Assembly);

            if (WorkspaceCloseStrategy == LayoutLoadStrategy.NeverAsk) 
                return;
            shell.OnClosing = OnClosing;
            if (WorkspaceCloseStrategy != LayoutLoadStrategy.NeverAskButPromptForExit)
            { 
                subLayoutManager.OpenedWorkspaces.CollectionChanged += workspaces_CollectionChanged;
            }

        }

        void workspaces_CollectionChanged(object sender_, NotifyCollectionChangedEventArgs e_)
        {
            if (e_.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (IWindowViewContainer window in e_.OldItems)
                {
                    window.Closing -= window_Closing;
                }
            }
            else if (e_.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (IWindowViewContainer window in e_.NewItems)
                {
                    window.Closing  += window_Closing;
                }  
            } 
        } 

        #region IWorkspaceManager members
        public void LoadWorkspace(string workspaceName_)
        {
            subLayoutManager.LoadSubLayout(workspaceName_, location);
        }
        public IWindowViewContainer CreateEmptyWorkspace(InitialWindowParameters parameters_)
        {
            var tileParameters = parameters_ as InitialTileParameters;
            if (tileParameters != null && !MixingMode)
            {
                tileParameters.IsFloating = floating;
            }

            WindowViewContainer rootView = (WindowViewContainer)chromeManager.CreateWindow(tileParameters != null ? 
                Guid.NewGuid().ToString():
                EmptyWorkspaceFactoryId, parameters_);
            rootView.Title = GetDefaultWorkspaceTitle(parameters_.WorkspaceCategory);
            subLayoutManager.CreateSubLayout(rootView);
            if (floating)
            {
                var root = DockHelper.GetRootOfFloatingPane(rootView);
                if (root != null)
                {
                    root.Title = root.SubLayoutTitle = rootView.Title;
                }  
            }
            
            return rootView;
        }

        public void RegisterWorkspace(IList<string> windowFactories_, string workspaceCategory_ = null, LayoutEngine layout_ = LayoutEngine.Default)
        {
            if (workspaceCategory_ == null) workspaceCategory_ = DefaultWorkspaceCategory;
             Dictionary<string, ImageSource> windowFactoriesWithImage = new Dictionary<string, ImageSource>();
            foreach (var windowFactory in windowFactories_)
            {
                windowFactoriesWithImage.Add(windowFactory, null);
            }
            RegisterWorkspace(windowFactoriesWithImage, workspaceCategory_, layout_);
        }

        public void RegisterWorkspace(IDictionary<string, ImageSource> windowFactories_, string workspaceCategory_ = null, LayoutEngine layout_ = LayoutEngine.Default)
        {
            if (workspaceCategory_ == null) workspaceCategory_ = DefaultWorkspaceCategory;
            if (this.workspaceCategories.ContainsKey(workspaceCategory_))
                return;

            this.workspaceCategories.Add(workspaceCategory_, windowFactories_);
            if (layout_ == LayoutEngine.Default)
            {
                layout_ = DefaultLayout;
            }
            this.RegisterWorkspaceDropdown(workspaceCategory_, layout_); 
        }

        public bool InitializeTileWorkspace(IWindowViewContainer container_, XDocument state_, Stack<InitializeWindowHandlerStep> chain_)
        {
            var initHandler = chain_.Pop();
            var initResult = initHandler(container_, state_, chain_);
            if (!initResult)
                return false;
            container_.Title = GetDefaultWorkspaceTitle(container_.Parameters.WorkspaceCategory);

            InitialTileParameters parameter = container_.Parameters as InitialTileParameters;
            if (parameter == null) return false;
            if (string.IsNullOrEmpty(parameter.WorkspaceCategory)) return true;
            IWidgetViewContainer dd = null;
            dd = container_.Header.AddWidget(WidgetPrefix + parameter.WorkspaceCategory + Guid.NewGuid(),
                new InitialDropdownButtonParameters()
                                {
                                    Text = "Edit",
                                    Opening = (sender_, args_) =>
                                        {
                                            WorkspaceFactoriesDropdown.GenerateItems(dd, this, subLayoutManager, parameter.WorkspaceCategory, LayoutEngine.Tile, container_, 
                                                parameter.GenerateAvailableItems && parameter.AppStoreButtonParameters == null); 
                                        }
                                });
            dd.AddWidget(new InitialButtonParameters {Text = "dummy"});
            subLayoutManager.CreateSubLayout(container_);
            return true;
        }

        public void RegisterWorkspaceWindowFactory(string id_, InitializeWindowHandler initHandler_, ImageSource image_ = null, string workspaceCategory_ = null, LayoutEngine layout_ = LayoutEngine.Default, DehydrateWindowHandler dehydrateHandler_ = null)
        {
            this.chromeRegistry.RegisterWindowFactory(id_).SetInitHandler(initHandler_).SetDehydrateHandler(dehydrateHandler_).AddToWorkspace(workspaceCategory_, image_, layout_);

        }
         
        public void PopulateWorkspaces(IWidgetViewContainer dropdown_)
        {
            dropdown_.Clear();
            foreach (var sublayout in subLayoutManager.GetSubLayouts())
            {
                var subLayoutCopied = sublayout;
                dropdown_.AddWidget(
                    new InitialButtonParameters
                    {
                        Text = sublayout.LayoutName,
                        Click = (sender_, args_) => subLayoutManager.LoadSubLayout(subLayoutCopied.LayoutName, location)
                    });
            }
            EventHandler<SubLayoutChangedEventArgs> handler = null;
            handler = (sender_, args_) =>
                {
                    subLayoutManager.SubLayoutChanged -= handler;
                    PopulateWorkspaces(dropdown_);
                };
            subLayoutManager.SubLayoutChanged += handler;
        }

        public IDictionary<string, ImageSource> GetChildWindowFactorys(string workspaceCategory_)
        {
            return workspaceCategories[workspaceCategory_];
        }
         
        internal bool SaveWorkspaceAs(IWindowViewContainer containerWindow_)
        {
            if (containerWindow_.IsEmptyWorkspace())
            {
                TaskDialog.ShowMessage("Cannot save an empty workspace", "Save workspace", TaskDialogCommonButtons.Close);
                return false;
            }
            TaskDialogResult dialogResult = TaskDialog.Show(new TaskDialogOptions()
            {
                UserInputEnabled = true,
                MainInstruction = @"Please provide workspace name",
                Title = @"Save workspace",
                CommonButtons = TaskDialogCommonButtons.OKCancel,
                MainIcon = VistaTaskDialogIcon.Information,
            });
            if (dialogResult.Result == TaskDialogSimpleResult.Cancel) return false;
            var workspaceName = dialogResult.UserInput;
            if (string.IsNullOrWhiteSpace(workspaceName))
            {
                TaskDialog.ShowMessage("Please enter a workspace name");
                return false;
            }
            workspaceName = workspaceName.Trim();
            string title = DockHelper.GetRootTitle(containerWindow_);
            if (string.Compare(workspaceName, title, StringComparison.InvariantCultureIgnoreCase) == 0) //save
            {
                subLayoutManager.SaveSubLayout(containerWindow_, location != SubLayoutLocation.SavedLocation, title, false);
                return true;
            }
            SubLayoutDefinition existingSubLayout = subLayoutManager.GetSubLayout(workspaceName);
            if (existingSubLayout != null)
            {
                if (TaskDialog.ShowMessage
                        (string.Format("Overwrite workspace '{0}'?", existingSubLayout.LayoutName), "Overwrite?",
                         TaskDialogCommonButtons.YesNo,
                         VistaTaskDialogIcon.Warning) == TaskDialogSimpleResult.No)
                {
                    return false;

                }
            }
            subLayoutManager.SaveSubLayout(containerWindow_, location != SubLayoutLocation.SavedLocation, workspaceName, true);
            return true;
        }


        #endregion

        #region Helper Methods

        private static string GetDefaultWorkspaceTitle(string workspaceCategory_)
        {
            string title;
            if (string.IsNullOrEmpty(workspaceCategory_) || !DefaultWorkspaceTitles.TryGetValue(workspaceCategory_, out title))
            {
                if (!DefaultWorkspaceTitles.TryGetValue(string.Empty, out title))
                {
                    title = "New Workspace";
                }
            }
            return title;
        }

        private bool GetSaveWorkspaceWhenSavingProfile()
        {
            if (SaveWorkspaceWhenSavingProfile)
            {
                return SaveWorkspaceWhenSavingProfile;
            }
            return subLayoutManager.ReadSubLayoutConfig(Extensions.SaveWorkspaceWhenSavingProfileOption) == "True";
        }

        private bool savingInternally = false;
        void monitor_ProfileSaved(object sender_, ProfileSavedEventArgs e_)
        { 
            if (savingInternally || !GetSaveWorkspaceWhenSavingProfile()) return;

            //override user preference if specified globally 
            bool resaveProfile = false; 
            foreach (var windowView in chromeManager.Windows)
            {
                if (string.IsNullOrEmpty(windowView.Parameters.WorkspaceCategory)) continue;
                
                if (subLayoutManager.IsSaved(windowView) || subLayoutManager.IsDeleted(windowView))
                {
                    subLayoutManager.SaveSubLayout(windowView, location != SubLayoutLocation.SavedLocation, DockHelper.GetRootTitle(windowView), false);
                }
                else
                {
                    if (SaveWorkspaceAs(windowView))
                    {
                        resaveProfile = true;
                    }
                }
            }

            if (resaveProfile)
            {
                savingInternally = true;
                try
                {
                    profileService.SaveCurrentProfile(); 
                }
                finally
                {
                    savingInternally = false;
                }
            }
        }


        void profileService_ProfileLoading(object sender_, ProfileLoadingEventArgs e_)
        {
            if (GetSaveWorkspaceWhenSavingProfile()) return;
            if (!CanClose()) e_.Cancel = true;
        }


        private bool OnClosing()
        {
            //already been handled when saving profile
            if (GetSaveWorkspaceWhenSavingProfile() && ShellSettings.LayoutLoadStrategy != LayoutLoadStrategy.NeverAsk) return true;
            return CanClose();
        }
         
        private bool CanClose()
        {
            return subLayoutManager.OpenedWorkspaces.All(windowView_ => !PresentUnsavedQuestion(windowView_, false) || TryClose(windowView_));
        }

        void window_Closing(object sender_, CancelEventArgs e_)
        { 
            if (e_.Cancel) return;
            var windowView = sender_ as IWindowViewContainer;
            if (windowView == null) return;
            if (!subLayoutManager.IsSubLayoutContainer(windowView))
            {
                windowView.Closing -= window_Closing;
                return;
            }
            if (!PresentUnsavedQuestion(windowView, false) ||
                TryClose(windowView))
            { 
                windowView.Closing -= window_Closing; 
                return;
            }

            e_.Cancel = true;
            
            //if don't close, do it for all other views
            var allViews = DockHelper.GetViewsInvolved(windowView);
            if (allViews.Count > 1)
            {
                foreach (var windowViewContainer in allViews)
                {
                    if (windowViewContainer != windowView)
                    {
                        EventHandler<CancelEventArgs> closing = null;
                        windowViewContainer.Closing += closing = (o_, args_) =>
                        {
                            args_.Cancel = true;
                            ((IWindowViewContainer)o_).Closing -= closing;
                        };
                    }
                } 
            }
             
        }


        private bool PresentUnsavedQuestion(IWindowViewContainer window_, bool exitApplication_)
        {
            switch (WorkspaceCloseStrategy)
            {
                case LayoutLoadStrategy.Default:
                    return subLayoutManager.IsChanged(window_, location != SubLayoutLocation.SavedLocation);
                case LayoutLoadStrategy.AlwaysAsk:
                    return true;
                case LayoutLoadStrategy.NeverAsk:
                    return false;
                case LayoutLoadStrategy.UseCallback:
                    return WorkspaceCloseStrategyCallback();
                case LayoutLoadStrategy.NeverAskButPromptForExit:
                    return exitApplication_ && subLayoutManager.IsChanged(window_, location != SubLayoutLocation.SavedLocation);
                default:
                    return true;
            }
        }

        private bool TryClose(IWindowViewContainer windowView_)
        {
            if (windowView_.IsEmptyWorkspace()) return true;  
            bool showExitDialog = subLayoutManager.ReadSubLayoutConfig(Extensions.ShowExitDialogOption).ToLower() == "true";
            if (showExitDialog)
            {
                IExitDialog dialog;

                if (initialSettings.EnableLegacyDialog)
                    dialog = new ExitDialog();
                else
                    dialog = new ExitTaskDialogLauncher();

                dialog.UsedForSubLayout = true;
                dialog.AppName = dialog.ProfileName = DockHelper.GetRootTitle(windowView_); 
                if (windowView_.Content != null)
                {
                    Window owner = Window.GetWindow((FrameworkElement)windowView_.Content);
                    if (owner != null && owner.IsLoaded)
                    {
                        dialog.Owner = owner;
                    }
                }
                bool? result = dialog.ShowDialog(); 
                if (result != true)
                {
                    return false;
                }
                if (dialog.DontAsk)
                {
                    subLayoutManager.WriteSubLayoutConfig(Extensions.SaveOnExitOption, dialog.SaveProfile.ToString());
                    subLayoutManager.WriteSubLayoutConfig(Extensions.ShowExitDialogOption, false.ToString());
                }
                if (dialog.SaveProfile)
                {
                    if (subLayoutManager.IsSaved(windowView_) || subLayoutManager.IsDeleted(windowView_))
                    {
                        subLayoutManager.SaveSubLayout(windowView_, location != SubLayoutLocation.SavedLocation, DockHelper.GetRootTitle(windowView_), false);
                    }
                    else
                    {
                        if (!SaveWorkspaceAs(windowView_))
                        {
                            return false;
                        }
                    }
                } 
            }
            else
            {
                bool saveOnExit = subLayoutManager.ReadSubLayoutConfig(Extensions.SaveOnExitOption).ToLower() == "true";
                if (saveOnExit)
                {
                    if (subLayoutManager.IsSaved(windowView_) || subLayoutManager.IsDeleted(windowView_))
                    {
                        subLayoutManager.SaveSubLayout(windowView_, location != SubLayoutLocation.SavedLocation, DockHelper.GetRootTitle(windowView_), false);
                    }
                    else
                    {
                        if (!SaveWorkspaceAs(windowView_))
                        {
                            return false;
                        }
                    }
                } 

            }

            return true;
        }

        private void RegisterWorkspaceItem(string workspaceCategory_, string id_, ImageSource image_, LayoutEngine layoutEngine_)
        {
            lock (workspaceCategories)
            {
                if (!workspaceCategories.ContainsKey(workspaceCategory_))
                {
                    RegisterWorkspace(new List<string>(), workspaceCategory_, layoutEngine_);
                }
                var windowFactories = workspaceCategories[workspaceCategory_];
                if (!windowFactories.ContainsKey(id_))
                {
                    windowFactories.Add(id_,image_);
                }
            } 
        }




 
        #endregion

        #region Workspace window creator dropdown button

        private void RegisterWorkspaceDropdown(string workspaceCategory_, LayoutEngine layout_)
        {
            if (Extensions.WorkspaceDropDownContainer != null)
            {
                IWidgetViewContainer dd = null;
                dd = Extensions.WorkspaceDropDownContainer.AddWidget(WidgetPrefix + workspaceCategory_ + Guid.NewGuid(),
                                    new InitialDropdownButtonParameters()
                                    {
                                        Text = workspaceCategory_,
                                        Opening = (sender_, args_) =>
                                        { 
                                            WorkspaceFactoriesDropdown.GenerateItems(dd, this, subLayoutManager, workspaceCategory_, layout_, null);
                                        }
                                    });

                dd.AddWidget(new InitialButtonParameters { Text = "dummy" });

            }
        }

        

        #endregion

        internal void ConvertWindowFactoryToWorkspaceUse(IWindowFactoryHolder windowFactoryHolder_, string workspaceCategory_, ImageSource image_, LayoutEngine engine_)
        {
            if (engine_ == LayoutEngine.Default) engine_ = DefaultLayout;
            if (string.IsNullOrEmpty(workspaceCategory_)) workspaceCategory_ = DefaultWorkspaceCategory;
            var wfh = windowFactoryHolder_ as WindowFactoryHolder;
            InitializeWindowHandlerStep step = (container_, state_, chain_) =>
                {
                    if (chain_.Count > 0)
                    {
                        var initHandler = chain_.Pop();
                        var initResult = initHandler(container_, state_, chain_);
                        if (!initResult)
                            return false;
                    }

                    container_.Parameters.WorkspaceCategory = workspaceCategory_;
                    if (engine_ == LayoutEngine.Dock && !(container_ is TileItemViewContainer))
                    { //Override IntialLocation

                        var existingWorkspaceWindow = container_.Parameters.InitialLocationTarget;
                        if (existingWorkspaceWindow == null)
                        {
                            existingWorkspaceWindow = chromeManager.Windows.FirstOrDefault((window_) =>
                                {
                                    var parameters = window_.Parameters;
                                    if (parameters == null)
                                        return false;

                                    return parameters.WorkspaceCategory == workspaceCategory_;
                                }); 
                        }
                     
                        
                        //Add workspace window creator button
                        IWidgetViewContainer dd = null;
                        dd = container_.Header.AddWidget(WidgetPrefix + workspaceCategory_ + Guid.NewGuid(),  
                                    new InitialDropdownButtonParameters()
                                    {
                                        Text = "Edit",
                                        Opening = (sender_, args_) =>
                                        {
                                            WorkspaceFactoriesDropdown.GenerateItems(dd, this, subLayoutManager, workspaceCategory_, LayoutEngine.Dock, existingWorkspaceWindow == null || existingWorkspaceWindow.Content == null ? container_ : existingWorkspaceWindow);
                                        }
                                    });
                         
                        dd.AddWidget(new InitialButtonParameters { Text = "dummy" });

                        if (existingWorkspaceWindow != null)
                        {
                            if (existingWorkspaceWindow.Content == null) //empty workspace
                            {
                                if (container_.Content != null)
                                {
                                    FrameworkElement element = container_.Content as FrameworkElement;
                                    RoutedEventHandler loaded = null;
                                    element.Loaded += loaded = (sender_, args_) =>
                                        {
                                            element.Dispatcher.BeginInvoke(new Action(existingWorkspaceWindow.Close), 
                                                                           DispatcherPriority.Background); 
                                            subLayoutManager.UpdateSubLayout(existingWorkspaceWindow, container_);
                                            element.Loaded -= loaded;

                                        };
                                }
                                else
                                {
                                    EventHandler<WindowEventArgs> createdHandler = null;
                                    container_.Created += createdHandler = (sender_, args_) =>
                                        {
                                            existingWorkspaceWindow.Close(); 
                                            subLayoutManager.UpdateSubLayout(existingWorkspaceWindow, container_);
                                            container_.Created -= createdHandler;
                                        };
                                }

                            }
                            container_.Parameters.InitialLocation = InitialLocation.DockRight;
                            container_.Parameters.InitialLocationTarget = existingWorkspaceWindow;
                        }
                        else
                        {
                            //new one
                            container_.Parameters.InitialLocation = floating ? InitialLocation.Floating : InitialLocation.DockInNewTab;
                            container_.Title = GetDefaultWorkspaceTitle(workspaceCategory_);
                            subLayoutManager.CreateSubLayout(container_);
                        } 
                    }
                    return true;

                };

            wfh.AddStep(step);
            RegisterWorkspaceItem(workspaceCategory_, wfh.Name, image_, engine_);
        }
    }
}
