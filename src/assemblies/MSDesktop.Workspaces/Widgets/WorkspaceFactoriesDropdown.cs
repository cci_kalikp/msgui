﻿ 
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.ViewContainers;
using MorganStanley.MSDotNet.MSGui.Impl;  

namespace MSDesktop.Workspaces.Widgets
{
    public static class WorkspaceFactoriesDropdown
    {
        internal static void GenerateItems(IWidgetViewContainer dropDownContainer_, WorkspaceManager manager_, ISubLayoutManager layoutManager_,
            string workspaceCategory_, LayoutEngine layout_, IWindowViewContainer containerWindow_, bool generateAvailableItems_=true)
        {
            dropDownContainer_.Clear();
            if (generateAvailableItems_)
            {
                foreach (var windowFactory in manager_.GetChildWindowFactorys(workspaceCategory_))
                {
                    var parameters = new InitialShowWindowButtonParameters
                        {
                            Text = windowFactory.Key,
                            WindowFactoryID = windowFactory.Key,
                            Image = windowFactory.Value,
                            WindowLayout = layout_
                        };
                    parameters.InitialParameters.WorkspaceCategory = workspaceCategory_;
                    parameters.InitialParameters.InitialLocationTarget = containerWindow_;
                    parameters.InitialParameters.InitialLocation = InitialLocation.DockRight;
                    dropDownContainer_.AddWidget(parameters);
                }
            }

            if (containerWindow_ != null)
            {
                if (generateAvailableItems_)
                {
                    dropDownContainer_.AddWidget(new InitialDropdownSeparatorParameters()); 
                }
                IButtonViewContainer[] deleteButton = {null};
                dropDownContainer_.AddWidget(new InitialButtonParameters 
                { 
                    Text = "Save",
                    //Enabled = !parameters_.ContainerWindow.IsEmptyWorkspace(),
                    Click = (sender_, args_) =>
                        {
                            //never saved before, which means this is a brand new workspace
                            if (!layoutManager_.IsSaved(containerWindow_) && !layoutManager_.IsDeleted(containerWindow_))
                            {
                                manager_.SaveWorkspaceAs(containerWindow_);
                            }
                            else
                            {
                                layoutManager_.SaveSubLayout(containerWindow_, manager_.location != SubLayoutLocation.SavedLocation, DockHelper.GetRootTitle(containerWindow_), false);
                            }
                            if (deleteButton[0] != null) deleteButton[0].Enabled = true;
                        }
                });
                dropDownContainer_.AddWidget(new InitialButtonParameters
                {
                    Text = "Save as",
                    Enabled = layoutManager_.IsSaved(containerWindow_),
                    Click = (sender_, args_) =>
                        {
                            manager_.SaveWorkspaceAs(containerWindow_);
                            if (deleteButton[0] != null) deleteButton[0].Enabled = true;
                        }
                });
                deleteButton[0] = dropDownContainer_.AddWidget(new InitialButtonParameters
                    {
                        Text = "Delete",
                        Enabled = layoutManager_.IsSaved(containerWindow_),
                        Click = (sender_, args_) =>
                            {
                                layoutManager_.RemoveSubLayout(DockHelper.GetRootTitle(containerWindow_));
                                if (deleteButton[0] != null) deleteButton[0].Enabled = false;
                            }
                    }) as IButtonViewContainer; 

            }
        }
         
      
    }
}
