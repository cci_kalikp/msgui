﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using MSDesktop.Workspaces.Interfaces;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;

namespace MSDesktop.Workspaces
{
    public static class Extensions
    {
        internal static WorkspaceManager WorkspaceManager;
        internal static IWidgetViewContainer WorkspaceDropDownContainer;
        public const string GlobalWorkspaceCategory = "Global";
        internal const string LoadWorkspacesID = "LoadWorkspacesID";
        public const string SaveOnExitOption = "SaveOnExit";
        public const string ShowExitDialogOption = "ShowExitDialog";
        public const string SaveWorkspaceWhenSavingProfileOption = "SaveWorkspaceWhenSavingProfile";
 
        public static void EnableWorkspaceSupport(this Framework f, LayoutEngine defaultLayout_ = LayoutEngine.Dock, string defaultWorkspaceCategory_ = GlobalWorkspaceCategory, string defaultWorkspaceTitle_ = "New Workspace")
        {
             var defaultWorkspaceTitles = new Dictionary<string, string>
                 {
                     {string.Empty, defaultWorkspaceTitle_ ?? "New Workspace"},
                     {defaultWorkspaceCategory_ ?? GlobalWorkspaceCategory, defaultWorkspaceTitle_ ?? "New Workspace"}
                 };
            EnableWorkspaceSupport(f, defaultWorkspaceTitles, defaultLayout_, defaultWorkspaceCategory_);

        }

        public static void EnableWorkspaceSupport(this Framework f, IDictionary<string, string> defaultWorkspaceTitles_, LayoutEngine defaultLayout_ = LayoutEngine.Dock, string defaultWorkspaceCategory_ = GlobalWorkspaceCategory)
        {
            //f.ForceLockerRegistrations(true);
            f.AddModule<WorkspacesModule>();
            if (defaultLayout_ != LayoutEngine.Tile)
            {
                f.EnableLoadedWorkspaceStickyTitle();
            }
            if (defaultWorkspaceTitles_ != null)
            { 
                WorkspaceManager.DefaultWorkspaceTitles = defaultWorkspaceTitles_;
            }
            WorkspaceManager.ShellSettings = f.InitialSettings;
            WorkspaceManager.DefaultWorkspaceCategory = defaultWorkspaceCategory_;
            WorkspaceManager.DefaultLayout = defaultLayout_; 
            f.EnableRemoveEmptyTabs(true, true); 
        }


        public static void SetDefaultWorkspaceLocation(this Framework f_, SubLayoutLocation location_)
        {
            WorkspaceManager.DefaultLocation = location_;
        }

        public static void SetWorkspaceCloseStrategy(this Framework framework_, LayoutLoadStrategy workspaceCloseStrategy_)
        {
            WorkspaceManager.WorkspaceCloseStrategy = workspaceCloseStrategy_;
        }

        public static void SetWorkspaceCloseStrategy(this Framework framework_,
                                 LayoutLoadStrategyCallback workspaceCloseStrategyCallback_)
        {
            WorkspaceManager.WorkspaceCloseStrategy = LayoutLoadStrategy.UseCallback;
            WorkspaceManager.WorkspaceCloseStrategyCallback = workspaceCloseStrategyCallback_;
        }

        public static void SaveWorkspaceWhenSavingProfile(this Framework framework_)
        {
            WorkspaceManager.SaveWorkspaceWhenSavingProfile = true;
        }

        public static void EnableWorkspaceMixingMode(this Framework framework_)
        {
            WorkspaceManager.MixingMode = true; 
            framework_.EnableRemoveEmptyTabs(false, false); 
        }
         
        public static IWindowFactoryHolder AddToWorkspace(this IWindowFactoryHolder f_, string workspaceCategory_ = null, ImageSource image_ = null, LayoutEngine layoutEngine_ = LayoutEngine.Default)
        {
            WorkspaceManager.ConvertWindowFactoryToWorkspaceUse(f_, workspaceCategory_, image_, layoutEngine_);
            return f_;
        }

        public static void AddWorkspaceDropdowns(this IChromeManager chromeManager_, IWidgetViewContainer parent_=null)
        {
            WorkspaceDropDownContainer = parent_ ?? chromeManager_.Ribbon["Workspaces"]["x"];
            
        }
         
        public static IWindowViewContainer CreateEmptyWorkspace(this IChromeManager chromeManager_, InitialWindowParameters parameters_)
        {
            return WorkspaceManager.CreateEmptyWorkspace(parameters_);
        }

        public static IWindowViewContainer CreateEmptyWorkspace(this IChromeManager chromeManager_, double? width = null,
                                                                double? height = null, string workspaceCategory_ = null)
        {
            InitialWindowParameters parameters = null;
            if (WorkspaceManager.DefaultLayout == LayoutEngine.Dock || 
                WorkspaceManager.DefaultLayout == LayoutEngine.Default)
            {
                parameters = new InitialWindowParameters();
            }
            else
            {
                parameters = new InitialTileParameters();
            }
            if (!string.IsNullOrEmpty(workspaceCategory_))
            {
                parameters.WorkspaceCategory = workspaceCategory_;
            }
            if (width != null && height != null)
            {
                parameters.Height = height.Value;
                parameters.Width = width.Value;
                parameters.SizingMethod = SizingMethod.Custom;
            }
            return WorkspaceManager.CreateEmptyWorkspace(parameters);
        }
        public static IWidgetViewContainer AddLoadWorkspacesButton(this IChromeManager chromeManager_, string text_, ImageSource image_, IWidgetViewContainer parentContainer_=null)
        {
            IWidgetViewContainer dropDown = null;
            dropDown = chromeManager_.AddWidget(LoadWorkspacesID, new InitialDropdownButtonParameters()
                 {
                     Text = text_,
                     Image = image_

                 }, parentContainer_);
            WorkspaceManager.PopulateWorkspaces(dropDown);
            return dropDown;
        }

        public static bool IsEmptyWorkspace(this IWindowViewContainer workspaceContainer_)
        {
            if (workspaceContainer_.Content == null) return true;
            var viewsContainer = workspaceContainer_ as IViewCollectionContainer;
            if (viewsContainer != null && viewsContainer.Children.Count == 0) return true;
            return false;
        }

       
    }
}
