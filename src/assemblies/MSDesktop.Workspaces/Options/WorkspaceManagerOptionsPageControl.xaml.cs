﻿ 
using System.Collections.Generic; 
using System.Windows.Controls; 
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Options;

namespace MSDesktop.Workspaces.Options
{
    /// <summary>
    /// Interaction logic for LayoutManagerOptionsPageControl.xaml
    /// </summary>
    public partial class WorkspaceManagerOptionsPageControl : UserControl, IValidatableOptionView, ISubItemAwareSeachablePage
    {
        public WorkspaceManagerOptionsPageControl(ISubLayoutManager subLayoutManager_)
        {
            InitializeComponent();
            Settings = new WorkspacesSettings(subLayoutManager_);
            Workspaces = new WorkspacesViewModel(subLayoutManager_);
            lstWorkspaces.DataContext = Workspaces;
            ctlSettings.DataContext = Settings;
        }

        internal WorkspacesSettings Settings { get; private set; }
        internal WorkspacesViewModel Workspaces { get; private set; }

        public bool OnValidate()
        {
            return lstWorkspaces.Validate();
        }

        public void OnInvalid()
        { 
            lstWorkspaces.HighlightInvalidWorkspace();
        }

        public void OnOK()
        {
            Settings.Save();
            Workspaces.Save();
        }

        public void OnCancel()
        {
            
        }

        public void OnDisplay()
        {
             Settings.Load();
            Workspaces.EnsureLoad();
        }

        public void OnHelp()
        {
             
        }

        public new System.Windows.UIElement Content
        {
            get { return this; }
        }

        public  IList<KeyValuePair<string, object>> GetMatchedItems(string textToSearch_)
        {
            var matchedItems = new List<KeyValuePair<string, object>>();
            Workspaces.AddMatchedWorkspaces(matchedItems, textToSearch_);
            Settings.AddMatchedOptions(matchedItems, textToSearch_);
            return matchedItems;
        }

        public void LocateItem(object item_)
        {
             if (lstWorkspaces.HighlightWorkspace(item_ as string)) return;
            ctlSettings.HighlightOption(item_ as string);

        }
    }
}
