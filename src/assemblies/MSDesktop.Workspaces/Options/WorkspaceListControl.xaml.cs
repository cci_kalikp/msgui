﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved


using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;

namespace MSDesktop.Workspaces.Options
{
    /// <summary>
    /// Interaction logic for RenameDeleteControl.xaml
    /// </summary>
    public partial class WorkspaceListControl : UserControl
    {
        public WorkspaceListControl()
        {
            InitializeComponent();
        }

        public bool HighlightWorkspace(string workspaceName_)
        {
            foreach (WorkspaceNameHolder workspace in listview.ItemsSource)
            {
                if (workspace.NewName == workspaceName_)
                {
                    HighlightWorkspace(workspace);
                }
            }
            return false;
        }

        private WorkspaceNameHolder invalidWorkspace;
        public bool Validate()
        {
            invalidWorkspace = null;
            var newNames = new List<string>();
            foreach (WorkspaceNameHolder workspaceNameHolder in listview.ItemsSource)
            {
                if (workspaceNameHolder.Delete)
                {
                    continue;
                }
                if (string.IsNullOrWhiteSpace(workspaceNameHolder.NewName))
                {
                    invalidWorkspace = workspaceNameHolder;
                    TaskDialog.ShowMessage("Empty layout names detected", "Invalid Settings",
                                           TaskDialogCommonButtons.Close, VistaTaskDialogIcon.Warning,
                                           Window.GetWindow(this));
                    return false;
                }
                string newName = workspaceNameHolder.NewName.ToLower();
                if (!newNames.Contains(newName))
                {
                    newNames.Add(newName);
                }
                else
                {
                    invalidWorkspace = workspaceNameHolder;
                    TaskDialog.ShowMessage("Duplicate workspace names detected.", "Invalid Settings",
                                           TaskDialogCommonButtons.Close, VistaTaskDialogIcon.Warning,
                                           Window.GetWindow(this));
                    return false;
                }
            }
            return true;
        }

        public void HighlightInvalidWorkspace()
        {
            if (invalidWorkspace != null)
            {
                HighlightWorkspace(invalidWorkspace);
                invalidWorkspace = null;
            }
        }

        private void HighlightWorkspace(WorkspaceNameHolder workspaceNameHolder_)
        {
            listview.SelectedItem = workspaceNameHolder_;
            listview.ScrollIntoView(workspaceNameHolder_);
            var item = listview.ItemContainerGenerator.ContainerFromItem(workspaceNameHolder_) as ListViewItem;
            if (item != null)
            {
                item.Highlight(); 
            }
        }
    }
}
