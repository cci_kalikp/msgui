﻿ 
using System.ComponentModel;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;

namespace MSDesktop.Workspaces.Options
{
    internal class WorkspaceNameHolder : ViewModelBase
    {
        public WorkspaceNameHolder(string name_)
        {
            Name = name_;
            NewName = name_;
            Delete = false;
        }


        public string Name { get; internal set; }

        private string newName;

        public string NewName
        {
            get { return this.newName; }
            set
            {
                this.newName = value;
                OnPropertyChanged("NewName");
                if (System.String.Compare(this.newName, this.Name, System.StringComparison.OrdinalIgnoreCase) != 0)
                {
                    NameToDisplay = string.Format(string.Format("{0} (Old Name: {1})", this.newName, this.Name));
                }
                else
                {
                    NameToDisplay = this.NewName;
                }
                OnPropertyChanged("NameToDisplay");
            }
        }

        public string NameToDisplay { get; private set; }
        private bool delete;

        public bool Delete
        {
            get { return delete; }
            set
            {
                delete = value;
                OnPropertyChanged("Delete");
            }
        }

        public override string ToString()
        {
            return NameToDisplay;
        }
    }
}
