﻿

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;

namespace MSDesktop.Workspaces.Options
{
    internal class WorkspacesSettings: ViewModelBase
    {
        private const string SaveOnExitDisplayName = "Save on exit";
        private const string ShowExitDialogDisplayName = "Show exit dialog";
        private const string SaveWorkspaceWhenSavingProfileDisplayName = "Save open workspaces when saving profile";
        private readonly ISubLayoutManager subLayoutManager;
        public WorkspacesSettings(ISubLayoutManager subLayoutManager_)
        {
            subLayoutManager = subLayoutManager_; 
        }


        private bool saveOnExit = true;
        [DisplayName(SaveOnExitDisplayName)] 
        public bool SaveOnExit
        {
            get
            {
                return saveOnExit;
            }
            set
            {
                saveOnExit = value;
                OnPropertyChanged("SaveOnExit");
            }
        }

        private bool showExitDialog = true;
        [DisplayName(ShowExitDialogDisplayName)] 
        public bool ShowExitDialog
        {
            get
            {
                return showExitDialog;
            }
            set
            {
                showExitDialog = value;
                OnPropertyChanged("ShowExitDialog");
            }
        }

        private bool saveWorkspaceWhenSavingProfile = false;
        [DisplayName(SaveWorkspaceWhenSavingProfileDisplayName)]
        public bool SaveWorkspaceWhenSavingProfile
        {
            get
            {
                return saveWorkspaceWhenSavingProfile;
            }
            set 
            { 
                saveWorkspaceWhenSavingProfile = value;
                OnPropertyChanged("SaveWorkspaceWhenSavingProfile");
            }
        }

        public bool SaveWorkspaceWhenSavingProfileCustomizable
        {
            get { return !WorkspaceManager.SaveWorkspaceWhenSavingProfile; }
        }

        public void Save()
        {
            subLayoutManager.WriteSubLayoutConfig(Extensions.SaveOnExitOption, saveOnExit.ToString());
            subLayoutManager.WriteSubLayoutConfig(Extensions.ShowExitDialogOption, showExitDialog.ToString());
            if (SaveWorkspaceWhenSavingProfileCustomizable)
            {
                subLayoutManager.WriteSubLayoutConfig(Extensions.SaveWorkspaceWhenSavingProfileOption, saveWorkspaceWhenSavingProfile.ToString());
            }
        }

        public void AddMatchedOptions(IList<KeyValuePair<string, object>> matchedItems_, string textToSearch_)
        {
            if (SaveOnExitDisplayName.ToLower().Contains(textToSearch_.ToLower().Trim()))
            {
                matchedItems_.Add(new KeyValuePair<string, object>(SaveOnExitDisplayName, Extensions.SaveOnExitOption));
            }
            if (ShowExitDialogDisplayName.ToLower().Contains(textToSearch_.ToLower().Trim()))
            {
                matchedItems_.Add(new KeyValuePair<string, object>(ShowExitDialogDisplayName, Extensions.ShowExitDialogOption));
            }
            if (SaveWorkspaceWhenSavingProfileCustomizable)
            {
                if (SaveWorkspaceWhenSavingProfileDisplayName.ToLower().Contains(textToSearch_.ToLower().Trim()))
                {
                    matchedItems_.Add(new KeyValuePair<string, object>(SaveWorkspaceWhenSavingProfileDisplayName, Extensions.SaveWorkspaceWhenSavingProfileOption));
                }
            } 
        }
         
        public void Load()
        {
            var config = subLayoutManager.ReadSubLayoutConfig(Extensions.SaveOnExitOption);
            if (!string.IsNullOrEmpty(config)) SaveOnExit = config.ToLower() == "true";
            config = subLayoutManager.ReadSubLayoutConfig(Extensions.ShowExitDialogOption);
            if (!string.IsNullOrEmpty(config)) ShowExitDialog = config.ToLower() == "true";
            if (SaveWorkspaceWhenSavingProfileCustomizable)
            {
                config = subLayoutManager.ReadSubLayoutConfig(Extensions.SaveWorkspaceWhenSavingProfileOption);
                if (!string.IsNullOrEmpty(config)) SaveWorkspaceWhenSavingProfile = config.ToLower() == "true";
   
            }
        }
    }
}
