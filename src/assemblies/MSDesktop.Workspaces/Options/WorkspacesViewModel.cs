﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MSDesktop.Workspaces.Options
{
    internal class WorkspacesViewModel
    {
        private readonly ISubLayoutManager subLayoutManager;
        public WorkspacesViewModel(ISubLayoutManager subLayoutManager_)
        {
            subLayoutManager = subLayoutManager_;
            subLayoutManager.SubLayoutChanged += new EventHandler<SubLayoutChangedEventArgs>(subLayoutManager_SubLayoutChanged);
            Workspaces = new ObservableCollection<WorkspaceNameHolder>();

        }

        void subLayoutManager_SubLayoutChanged(object sender, SubLayoutChangedEventArgs e)
        {
             if (!saving)
             {
                 loaded = false;
             }
        }

        public ObservableCollection<WorkspaceNameHolder> Workspaces { get; private set; }

        private bool saving = false;
        public void Save()
        {
            saving = true;
            try
            { 
                Dictionary<string, string> namingChanges = new Dictionary<string, string>();
                for (int i = Workspaces.Count - 1; i >= 0; i--)
                {
                    WorkspaceNameHolder workspaceNameHolder = Workspaces[i];
                    if (workspaceNameHolder.Delete)
                    {
                        subLayoutManager.RemoveSubLayout(workspaceNameHolder.Name);
                        Workspaces.RemoveAt(i);
                    }
                    else if (string.Compare(workspaceNameHolder.Name.Trim(), workspaceNameHolder.NewName.Trim(),
                  StringComparison.InvariantCultureIgnoreCase) != 0)
                    {
                        namingChanges.Add(workspaceNameHolder.Name, workspaceNameHolder.NewName.Trim());
                    }
                }
                
                if (namingChanges.Count > 0)
                {
                    subLayoutManager.RenameSubLayouts(namingChanges);
                }
            }
            finally
            {
                saving = false;
                loaded = false;
                EnsureLoad();
            }

           
        }

        public void AddMatchedWorkspaces(IList<KeyValuePair<string, object>> matchedItems_, string textToSearch_)
        {
            EnsureLoad();
            foreach (var workspaceNameHolder in this.Workspaces)
            {
                if (workspaceNameHolder.Name.ToLower().Contains(textToSearch_.ToLower().Trim()))
                {
                    matchedItems_.Add(new KeyValuePair<string, object>(workspaceNameHolder.Name, workspaceNameHolder.Name));
                }
            } 
        }

        private bool loaded = false;
        public void EnsureLoad()
        {
            if (!loaded)
            {
                Workspaces.Clear();
                foreach (var sublayout in subLayoutManager.GetSubLayouts())
                {
                    Workspaces.Add(new WorkspaceNameHolder(sublayout.LayoutName));
                } 
            }
            loaded = true;
        }
    }
}
