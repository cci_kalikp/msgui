﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;

namespace MSDesktop.Workspaces.Options
{
    /// <summary>
    /// Interaction logic for WorkspaceShutDownOptionsControl.xaml
    /// </summary>
    public partial class WorkspaceShutDownOptionsControl : UserControl
    {
        public WorkspaceShutDownOptionsControl()
        {
            InitializeComponent();
        }

        public bool HighlightOption(string optionName_)
        {
            if (optionName_ == Extensions.ShowExitDialogOption)
            {
                ShowExitDialogCheckBox.Highlight();
                return true;
            } 
            if (optionName_ == Extensions.SaveOnExitOption)
            {
                SaveOnExitCheckBox.Highlight();
                return true;
            }

            if (optionName_ == Extensions.SaveWorkspaceWhenSavingProfileOption && SaveWorkspaceWhenSavingProfileCheckBox.Visibility == Visibility.Visible)
            {
                SaveWorkspaceWhenSavingProfileCheckBox.Highlight();
            }

            return false;
        }

    }
}
