﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MSDesktop.Workspaces.Interfaces
{
    public interface IWorkspaceManager
    {
        void RegisterWorkspace(IList<string> windowFactories_,
                               string workspaceCategory_ = null, LayoutEngine layoutEngine_ = LayoutEngine.Default);

        void RegisterWorkspace(IDictionary<string, ImageSource> windowFactories_,
                       string workspaceCategory_ = null, LayoutEngine layout_ = LayoutEngine.Default);

        IWindowViewContainer CreateEmptyWorkspace(InitialWindowParameters parameters_);

        void RegisterWorkspaceWindowFactory(string id_, InitializeWindowHandler initHandler_, ImageSource image_ = null, string workspaceCategory_ = null, LayoutEngine layout_ = LayoutEngine.Default, DehydrateWindowHandler dehydrateHandler_ = null); 

        void PopulateWorkspaces(IWidgetViewContainer dropdown_);

        IDictionary<string, ImageSource> GetChildWindowFactorys(string workspaceCategory_);

        void LoadWorkspace(string workspaceName_);
    }
}
