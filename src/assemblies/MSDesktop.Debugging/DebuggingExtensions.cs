﻿using System.Windows.Controls;
using MSDesktop.Debugging.ViewModels.Dashboard;
using MSDesktop.Debugging.Views.Diagnostics;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core.SmartApi;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MSDesktop.Debugging
{
    public static class DebuggingExtensions
    {
        internal static IChromeManager ChromeManager;
        internal static IChromeRegistry ChromeRegistry; 
        internal static string logFilePath;

        public static void EnableDebugging(this Framework framework_)
        {
            ExtensionsUtils.RequireAssembly(ExternalAssembly.InfragisticsEditors,
              ExternalAssembly.InfragisticsDataPresenter,
              ExternalAssembly.InfragisticsEditors,
              ExternalAssembly.InfragisticsXamColorPicker);
            framework_.AddModule<DebugViewModule>();
            framework_.AddModule<LogViewModule>();
        }
         
        public static void SetInitialLogFilePath(this Framework framework_, string logFilePath_)
        {
            logFilePath = logFilePath_;
        }
         

        internal static IWidgetViewContainer GetDebuggingButtonContainer(this IChromeManager chromeManager_)
        {
            return chromeManager_.GetParentContainer("Help", "Support"); 
        }

        internal static DiagnosticsViewModel EnsureDiagnosticsView(IUnityContainer container_)
        {
            if (container_.IsRegistered<DiagnosticsViewModel>())
            {
                return container_.Resolve<DiagnosticsViewModel>();
            }
            if (!container_.IsRegistered<ItemsControl>("DashboardItemsControl"))
            {
                return null;
            } 
            var itemsControl = container_.Resolve<ItemsControl>("DashboardItemsControl");
            if (itemsControl != null)
            { 
                var diagnostics = new DiagnosticsView();
                var model = new DiagnosticsViewModel();
                container_.RegisterInstance(model);
                diagnostics.DataContext = model;
                 
                itemsControl.Items.Add(diagnostics);
                return model;
            }
            return null;
        }
    }

}
