﻿using System;
using System.Collections.Generic;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core;
using System.Windows.Media.Imaging;
using System.Xml.Linq; 
using MSDesktop.Debugging.ViewModels; 
using MSDesktop.Debugging.Extensions;
using System.Windows;
using System.Windows.Media;
using System.IO;

namespace MSDesktop.Debugging
{
    public class LogViewModule : IMSDesktopModule
    {       
        private readonly IChromeRegistry chromeRegistry;
        private readonly IChromeManager chromeManager;
        private readonly IPersistenceService persistenceService;
        private const string ButtonId = "LogViewerButton";
        private const string ViewId = "LogViewerWindow";

        private readonly IUnityContainer container;
        private ResourceDictionary rd = new ResourceDictionary()
            {
                Source = new Uri("/MSDesktop.Debugging;component/Views/Log/LogViewContainer.xaml", UriKind.Relative)
            };
        private readonly IApplication application;
        private bool dashboardRegistered = false;

        public LogViewModule(IChromeManager chromeManager_, IChromeRegistry chromeRegistry_,
            IPersistenceService persistenceService_, IUnityContainer container_, IApplication application_)
        {
            container = container_;
            DebuggingExtensions.ChromeManager = chromeManager = chromeManager_;
            DebuggingExtensions.ChromeRegistry = chromeRegistry = chromeRegistry_;
            persistenceService = persistenceService_;
            application = application_;
            application.ApplicationLoaded += application_ApplicationLoaded;
		}

        void application_ApplicationLoaded(object sender, EventArgs e)
        {
            RegisterToDashboard();
            ((MorganStanley.MSDotNet.MSGui.Impl.Application.Application)application).ThemeChanged +=
                (sender_, args_) => RegisterToDashboard();
        }

        private void RegisterToDashboard()
        {
            if (dashboardRegistered) return;
            var model = DebuggingExtensions.EnsureDiagnosticsView(container);
            if (model == null) return;
            string log = null;
            string startupDirectory = null;
            if (!string.IsNullOrWhiteSpace(DebuggingExtensions.logFilePath))
            {
                try
                {
                    var f = new FileInfo(DebuggingExtensions.logFilePath);
                    if (f.Exists)
                    {
                        log = f.FullName;
                        startupDirectory = f.DirectoryName;
                    }
                    else
                    {
                        var d = new DirectoryInfo(DebuggingExtensions.logFilePath);
                        if (d.Exists)
                        {
                            startupDirectory = d.FullName;
                        }
                    }
                }
                catch
                {

                }
            }
            else
            {
                log = Util.GetLogFileName(out startupDirectory);
            }
            var logmodel = new LogsViewModel(startupDirectory);
            logmodel.DisposeAutomatically = false;
            if (!string.IsNullOrEmpty(log))
            {
                logmodel.OpenLogCommand.Execute(log);
            }
            model.Models.Add(logmodel);
            dashboardRegistered = true;
        }

   

        public void Initialize()
        {
            persistenceService.AddGlobalPersistor(LogViewerSetting.PersistorId, LogViewerSetting.LoadState, LogViewerSetting.SaveState);
            chromeManager.AddWidget(ButtonId,  new InitialShowWindowButtonParameters()
            {
                Text = "Log Viewer",
                ToolTip = "View Log File",
                WindowFactoryID = ViewId,
                Image = new BitmapImage(new Uri("/MSDesktop.Debugging;component/Resources/LogViewer.png", UriKind.Relative)),
                InitialParameters = new InitialWindowParameters()
                {
                    Height = 600, 
                    Width = 950,
                    SizingMethod = SizingMethod.Custom,
                    Singleton = true
                }
            }, chromeManager.GetDebuggingButtonContainer());
            chromeRegistry.RegisterWindowFactory(ViewId, new InitializeWindowHandler(CreateView), new DehydrateWindowHandler(PersistWindowState)); 
        }

        private bool CreateView(IWindowViewContainer viewContainer_, XDocument state_)
        {
            FrameworkElement view = rd["LogViewContainer"] as FrameworkElement; 
            Dictionary<string, TabTagViewModel> logs = new Dictionary<string, TabTagViewModel>();
            string startupDirectory = null;
            if (state_ != null)
            {
                DateTime lastVisitedTime = new DateTime();
                XElement root = state_.Element("LogViewer");
                if (root != null)
                { 
                    foreach (XElement logElement in root.Elements("Log"))
                    {
                        XAttribute pathAttr = logElement.Attribute("FilePath");
                        if (pathAttr != null && !string.IsNullOrEmpty(pathAttr.Value) && !logs.ContainsKey(pathAttr.Value.Trim()))
                        {
                            try
                            {
                                FileInfo file = new FileInfo(pathAttr.Value.Trim());
                                if (!file.Exists) continue;
                                TabTagViewModel tag = new TabTagViewModel();
                                XAttribute tagTextAttr = logElement.Attribute("TagText");
                                if (tagTextAttr != null && !string.IsNullOrEmpty(tagTextAttr.Value))
                                {
                                    tag.TagText = tagTextAttr.Value.Trim();
                                }
                                XAttribute tagColorAttr = logElement.Attribute("TagColor");
                                if (tagColorAttr != null && !string.IsNullOrEmpty(tagColorAttr.Value))
                                {
                                    try
                                    {
                                        object color = new ColorConverter().ConvertFrom(tagColorAttr.Value);
                                        if (color is Color)
                                        {
                                            tag.TagColor = (Color)color;
                                        }
                                    }
                                    catch
                                    {
                                    }
                                }
                                logs.Add(pathAttr.Value.Trim(), tag);
                                if (file.LastWriteTime.CompareTo(lastVisitedTime) > 0)
                                {
                                    lastVisitedTime = file.LastWriteTime;
                                    startupDirectory = file.DirectoryName;
                                }
                            }
                            catch
                            {

                            }
                        }
                    }
                }
            }
            if (logs.Count == 0)
            {
                if (!string.IsNullOrWhiteSpace(DebuggingExtensions.logFilePath))
                {
                    try
                    {
                        var f = new FileInfo(DebuggingExtensions.logFilePath);
                        if (f.Exists)
                        {
                            logs.Add(f.FullName, new TabTagViewModel());
                            startupDirectory = f.DirectoryName;
                        }
                        else
                        {
                            var d = new DirectoryInfo(DebuggingExtensions.logFilePath);
                            if (d.Exists)
                            {
                                startupDirectory = d.FullName;
                            }
                        }
                    }
                    catch
                    {
                            
                    } 
                }
                else
                {
                    string log = Util.GetLogFileName(out startupDirectory);
                    if (log != null)
                    {
                        logs.Add(log, new TabTagViewModel());
                    }
                }

            }
            var context = new LogsViewModel(startupDirectory);
            view.DataContext = context; 
            foreach (var log in logs)
            {
                context.OpenLogCommand.Execute(log.Key);
                if (context.CurrentLog != null)
                {
                    context.CurrentLog.TagColor = log.Value.TagColor;
                    context.CurrentLog.TagText = log.Value.TagText;
                }
            }
            viewContainer_.Icon = MSDesktop.Debugging.Properties.Resources.LogViewer;
            viewContainer_.Content = view;
            viewContainer_.Title = "Log Viewer"; 
            viewContainer_.Closing += new EventHandler<System.ComponentModel.CancelEventArgs>(viewContainer__Closing);
            return true;
        }
         
        void viewContainer__Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            LogsViewModel context = ((FrameworkElement)((IWindowViewContainer)sender).Content).DataContext as LogsViewModel;
            if (context != null) context.Dispose();
        }

        private XDocument PersistWindowState(IWindowViewContainer viewContainer_)
        { 
            XDocument state = new XDocument();
            XElement root = new XElement("LogViewer"); 
            FrameworkElement container = viewContainer_.Content as FrameworkElement;
            LogsViewModel context = container.DataContext as LogsViewModel;
            if (context != null && context.Logs.Count > 0)
            {
                foreach (LogViewModel log in context.Logs)
                {
                    XElement logElement = new XElement("Log");
                    logElement.SetAttributeValue("FilePath", log.FilePath);
                    if (!string.IsNullOrEmpty(log.TagText))
                    {
                        logElement.SetAttributeValue("TagText", log.TagText);
                    }
                    if (log.TagColor != Colors.Transparent)
                    {
                        logElement.SetAttributeValue("TagColor", log.TagColor.ToString());
                    }
                    root.Add(logElement);
                }
            }
            state.Add(root);
            return state;
        }
    }
}
