﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Reactive.Linq;
using MSDesktop.Debugging.Extensions; 
using MSDesktop.Debugging.Models;

namespace MSDesktop.Debugging.ViewModels
{
    internal class RegexLogMonitor
    {
        private readonly Regex matchUntil;
        public IObservable<int> HaveNotSeenMatchStream { get; private set; }

        public RegexLogMonitor(LogViewModel log_)
        {
            matchUntil = new Regex(LogViewerSetting.Current.RegexMonitor.RegExToMonitor);
            uint interval = LogViewerSetting.Current.RegexMonitor.TimeIntervalInSec.Value;
            var logItems
            = Observable.FromEvent<LogItemsAddedEventArgs>(_ => log_.LogItemsAdded += _,
                                                          _ => log_.LogItemsAdded -= _)
              .SelectMany(e => e.ItemsAdded);

            var allItemsAfterOcurrenceOfAMatch = logItems.SkipWhile(item => !matchUntil.IsMatch(item.Details));

            var haveNotseenAlertForXSecAfterTheOccurence = allItemsAfterOcurrenceOfAMatch
              .BufferUntil(bufferedLogItem => matchUntil.IsMatch(bufferedLogItem.Last().Details)
                                              || IsTotalSecondsPassedMoreThanThreshold(bufferedLogItem, interval))
              .Where(bufferedLogItem => IsTotalSecondsPassedMoreThanThreshold(bufferedLogItem, interval));

            //???
            var lastLines =
              haveNotseenAlertForXSecAfterTheOccurence.Select(buffer => buffer.Last()).Select(item => (int)item.EntryNumber);

            HaveNotSeenMatchStream = LogViewerSetting.Current.RegexMonitor.RunOnce ? lastLines.Take(1) : lastLines;

        }

        private static bool IsTotalSecondsPassedMoreThanThreshold(IEnumerable<ILogEntry> bufferedLogItems_, double threshold_)
        {
           var a = bufferedLogItems_.Max(item => item.Date).Subtract(bufferedLogItems_.Min(item => item.Date)).TotalSeconds;
           return a > threshold_;
        }
    }
}