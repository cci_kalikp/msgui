﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace MSDesktop.Debugging.ViewModels
{
    public class TabTagViewModel
    {
        public string TagText { get; set; }

        public Color TagColor { get; set; }
    }
}
