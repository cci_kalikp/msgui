﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;

namespace MSDesktop.Debugging.ViewModels
{
    public class MonitorSettingViewModel : ViewModelBase, IDataErrorInfo
    {
        private bool isEnabled;
        public bool IsEnabled
        {
            get
            {
                return isEnabled;
            }
            set
            {
                if (isEnabled != value)
                {
                    isEnabled = value; 
                    OnPropertyChanged("IsEnabled");
                    TimeIntervalInSec = TimeIntervalInSec;
                    RegExToMonitor = RegExToMonitor;
                    CommandToRun = CommandToRun;
                }
            }
        }

        private uint? timeIntervalInSec;
        public uint? TimeIntervalInSec
        {
            get
            {
                return timeIntervalInSec;
            }
            set
            {
                if (IsEnabled && (value == null || value.Value == 0))
                {
                    errors["TimeIntervalInSec"] = Error = "Must be a positive value";
                }
                else
                {
                    errors.Remove("TimeIntervalInSec");
                    if (errors.Count == 0) Error = null;
                }
                timeIntervalInSec = value;
                OnPropertyChanged("TimeIntervalInSec");
            }
        }

        private string regexToMonitor;
        public string RegExToMonitor
        {
            get
            {
                return regexToMonitor;
            }
            set
            {
                if (IsEnabled && string.IsNullOrWhiteSpace(value))
                {
                    errors["RegExToMonitor"] = Error = "Regex is required";
                }
                else
                {
                    errors.Remove("RegExToMonitor");
                    if (errors.Count == 0) Error = null;
                }
                regexToMonitor = value;
                OnPropertyChanged("RegExToMonitor");
            }

        }

        private string commandToRun;
        public string CommandToRun
        {
            get
            {
                return commandToRun;
            }
            set
            {
                if (IsEnabled && string.IsNullOrWhiteSpace(value))
                {
                    errors["CommandToRun"] = Error = "Command is required";
                }
                else
                {
                    errors.Remove("CommandToRun");
                    if (errors.Count == 0) Error = null;
                }
                commandToRun = value;
                OnPropertyChanged("CommandToRun");
            }
        }
        public bool RunOnce { get; set; }
        public bool OnlyOnLiveLogEntries { get; set; }
        public string Arguments { get; set; }


        private Dictionary<string, string> errors = new Dictionary<string, string>();
        private string error;
        public string Error
        {

            get
            {
                return error;
            }
            set
            {
                error = value;
                NoError = string.IsNullOrEmpty(error);
            }
        }

        bool noError = false;
        public bool NoError
        {
            get
            {
                return noError;
            }
            set
            {
                noError = value;
                OnPropertyChanged("NoError");
            }
        }

        public string this[string columnName_]
        {
            get
            {
                string error;
                errors.TryGetValue(columnName_, out error);
                return error;
            }
        }

        public void CopyTo(MonitorSettingViewModel setting_)
        {
            setting_.isEnabled = this.isEnabled;
            if (setting_.isEnabled)
            {
                setting_.TimeIntervalInSec = this.TimeIntervalInSec;
                setting_.RegExToMonitor = this.RegExToMonitor;
                setting_.CommandToRun = this.CommandToRun;
                setting_.RunOnce = this.RunOnce;
                setting_.OnlyOnLiveLogEntries = this.OnlyOnLiveLogEntries;
                setting_.Arguments = this.Arguments;
            }
            else
            {
                setting_.timeIntervalInSec = null;
                setting_.RegExToMonitor = setting_.commandToRun = setting_.Arguments = null;
                setting_.RunOnce = setting_.OnlyOnLiveLogEntries = false;
            }
        } 
    }
}
