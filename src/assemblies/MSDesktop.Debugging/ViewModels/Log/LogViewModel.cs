﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;
using System.IO;
using System.Threading.Tasks; 
using System.Threading;
using System.Diagnostics;
using System.Windows.Input;
using Microsoft.Practices.Composite.Presentation.Commands;
using System.Windows.Media;  
using System.Collections; 
using System.Windows.Data;
using System.ComponentModel;
using System.Windows;
using System.Collections.ObjectModel; 
using MSDesktop.Debugging.Models; 
using System.Reactive.Linq;
using System.Reactive.Subjects;
using ViewModelBase = MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.ViewModelBase;

namespace MSDesktop.Debugging.ViewModels
{
    public class LogViewModel:ViewModelBase
    { 
        private readonly Subject<ILogEntry> entries = new Subject<ILogEntry>();
        private ILogReader logReader;
        private IDisposable subscription;
        private IDisposable monitor;
        private LogsViewModel parent;
        //we use List<T> for markedItemsList as the source list for MarkeItems CollectionView
        //instead of the ObservableCollection, so that marked items can be added to the collection view in background thread
        //however, we need to remember to Refresh MarkedItems every time we made some changes to the markedItemsList
        //to reflect the change to the grid it bound to
        private IList markedItemsList;
        private List<string> similars = new List<string>();
        private ILogEntry unvisitedNewItem; 
        internal event Action<LogItemsAddedEventArgs> LogItemsAdded; 
        private readonly ManualResetEvent pauseEvent = new ManualResetEvent(true);
        private readonly CancellationTokenSource tokenSource = new CancellationTokenSource();
        private bool showOnlyMarkedItems = false;
        private uint entryLimit = LogViewerSetting.Current.EntryLimit;

        internal LogViewModel(LogsViewModel parent_, string logFileName_)
        {
            parent = parent_; 
            this.WordWrap = parent.WordWrap;
            this.FilePath = logFileName_;
            this.LogName = this.DisplayLogName = Path.GetFileNameWithoutExtension(logFileName_);
            logReader = LogReaderFactory.GetReader(FilePath);
            if (logReader != null)
            {
                logReader.LogVersionResolved += (sender_, args_) => OnPropertyChanged("LogVersion");
            }

            if (LogViewerSetting.Current.RegexMonitor.IsEnabled && 
                !LogViewerSetting.Current.RegexMonitor.OnlyOnLiveLogEntries)
            {
                CreateMonitor();
            }

            ViewHeaderCommand = new DelegateCommand<object>(_ => ViewHeader(), _ => header != null);
            ToggleFreezeCommand = new DelegateCommand<object>(_ =>
            {
                if (Started) Stop();
                else Start();
                ToggleFreezeCommand.RaiseCanExecuteChanged();
            }); 
            MarkSelectedCommand = new DelegateCommand<object>(_ => MarkSelected(), _ => CanMark());
            UnmarkSelectedCommand = new DelegateCommand<object>(_ => UnmarkSelected(), _ => CanUnmark());
            UnmarkAllCommand = new DelegateCommand<object>(_ => UnmarkAll(), _ => markedItemsList != null && markedItemsList.Count > 0);
            //We could have been able to disable and enable the Marked Item Navigation commands according to the current view
            //However, since we need to iterate the Items collection view each time to decide that(checking markedItems is not enough, 
            //since the view can be filtered), it would hit the performance greatly, so we simply give message if the navigation failed
            GoToFirstMarkedCommand = new DelegateCommand<object>(_ => GoToFirstMarked());
            GoToLastMarkedCommand = new DelegateCommand<object>(_ => GoToLastMarked());
            GoToPreviousMarkedCommand = new DelegateCommand<object>(_ => GoToPreviousMarked());
            GoToNextMarkedCommand = new DelegateCommand<object>(_ => GoToNextMarked());
            MarkSimilarCommand = new DelegateCommand<object>(_ => MarkSimilar(), _ => SelectedItems != null && SelectedItems.Count > 0);
            EditTabTagCommand = new DelegateCommand<object>(_ => EditTabTag());
            GoToNewItemsCommand = new DelegateCommand<object>(_ => GoToNewItems());
            ToggleShowMarkedOnlyCommand = new DelegateCommand<object>(_ => ShowMarkedOnly()); 
            parent_.Logs.Add(this); 
        }
         
        public LogsViewModel Parent
        {
            get
            {
                return parent;
            }
        }

        private string header;
        public string Header
        {
            get
            {
                return header;
            }
            set
            {
                header = value;
                OnPropertyChanged("Header");
                ViewHeaderCommand.RaiseCanExecuteChanged();
            }
        }

        private ICollectionView visibleItems;
        public ICollectionView VisibleItems
        {
            get
            {
                if (visibleItems == null)
                {
                    Start();
                    LoadNewItems(); 
                }
                return visibleItems;
            }
            set
            {
                if (visibleItems != value)
                {
                    visibleItems = value;
                    OnPropertyChanged("VisibleItems");
                    UpdateCommandsStatus();
                }
            }
        }

        private ICollectionView items;
        public ICollectionView Items
        {
            get
            {
                return items;
            }
            set
            {
                if (items != value)
                {
                    bool isEmpty = items == null;
                    items = value;  
                    OnPropertyChanged("Items");
                    if (!showOnlyMarkedItems && (visibleItems != null || isEmpty))
                    {
                        if (visibleItems != null)
                        {
                            visibleItems.CurrentChanged -= new EventHandler(items_CurrentChanged);
                        }
                        VisibleItems = items;
                    }
                }
            }
        }

        private ICollectionView markedItems;
        public ICollectionView MarkedItems
        {
            get
            {
                if (markedItems == null)
                {
                    if (markedItemsList == null)
                    {
                        if (CurrentItem != null)
                        {
                            markedItemsList = Activator.CreateInstance(typeof(List<>).MakeGenericType(CurrentItem.GetType())) as IList;
                        }
                        else
                        {
                            markedItemsList = new List<ILogEntry>();
                        }
                    }
                    markedItems = CollectionViewSource.GetDefaultView(markedItemsList);
                    markedItems.Refresh(); 
                    
                }
                return markedItems;
            }
            set
            {
                if (markedItems != value && visibleItems != null)
                {
                    markedItems = value;
                    if (value != null)
                    {
                        markedItemsList = markedItems.SourceCollection as IList;
                    }
                    OnPropertyChanged("MarkedItems");
                    if (showOnlyMarkedItems)
                    {
                        if (visibleItems != null)
                        {
                            visibleItems.CurrentChanged -= new EventHandler(items_CurrentChanged);
                        }
                        if (!loading && markedItems != null && markedItems.SortDescriptions.Count == 0)
                        {
                            using (markedItems.DeferRefresh())
                            {
                                markedItems.SortDescriptions.Add(new SortDescription("EntryNumber", ListSortDirection.Descending));
                                markedItems.CurrentChanged += new EventHandler(items_CurrentChanged);
                            }
                        }
                        VisibleItems = markedItems;
                    }
                }
            }
        }

        private int itemsCount = 0;
        public int ItemsCount
        {
            get
            {
                return itemsCount;
            }
            set
            {
                itemsCount = value;
                OnPropertyChanged("ItemsCount");
            }
        }

       
        public IList SelectedItems { get; set; }

        public ILogEntry CurrentItem
        {
            get
            {
                return VisibleItems == null ? null : VisibleItems.CurrentItem as ILogEntry;
            }
        }

        private bool loading = false;
        private bool notLoaded = true;
        public bool NotLoaded
        {
            get
            {
                return notLoaded;
            }
            set
            {
                if (notLoaded != value)
                {
                    notLoaded = value;
                    OnPropertyChanged("NotLoaded");
                }
            }
        }


        public bool Loading
        {
            get
            {
                return loading;
            }
            set
            {
                if (loading != value)
                {
                    loading = value;
                    OnPropertyChanged("Loading");
                }
            }
        }
        private bool hasNewItems = false;
        public bool HasNewItems
        {
            get
            {
                return hasNewItems;
            }
            set
            {
                if (hasNewItems != value)
                {
                    hasNewItems = value;
                    OnPropertyChanged("HasNewItems");
                    UpdateDisplayLogName();
                }
            }
        }

        private bool started = false;
        public bool Started
        {
            get
            {
                return started;
            }
            set
            {
                if (started != value)
                {
                    started = value;
                    OnPropertyChanged("Started");
                }
            }
        }

        public LogType LogType
        {
            get
            {
                return logReader.LogType;
            }
        }

        public int LogVersion
        {
            get { return logReader.LogVersion; }
        }

        public string FilePath
        {
            get;
            private set;
        }

        public string LogName
        {
            get;
            private set;
        } 

        private string displayLogName;
        public string DisplayLogName
        {
            get
            {
                return displayLogName;
            }
            set
            {
                if (displayLogName != value)
                {
                    displayLogName = value;
                    OnPropertyChanged("DisplayLogName");
                }
            }
        }

        private string tagText;
        public string TagText
        {
            get
            {
                return tagText;
            }
            set
            {
                if (tagText != value)
                {
                    tagText = value;
                    OnPropertyChanged("TagText");
                    UpdateDisplayLogName();
                }
            }
        }

        internal void UpdateDisplayLogName()
        {
            string text = null;
            if (!string.IsNullOrWhiteSpace(tagText))
            {
                text = string.Format("{0} [{1}]", tagText, LogName);
            }
            else
            {
                text = LogName;
            }
            if (hasNewItems && this.parent.CurrentLog != this)
            {
                text = string.Format("{0} (has new entries)", text);
            }
            DisplayLogName = text;
        }

        private Color tagColor = Colors.Transparent;
        public Color TagColor
        {
            get
            {
                return tagColor;
            }
            set
            {
                if (tagColor != value)
                {
                    tagColor = value;
                    OnPropertyChanged("TagColor");
                }
            }
        }

        private bool wordWrap;
        public bool WordWrap
        {
            get
            {
                return wordWrap;
            }
            set
            {
                if (wordWrap != value)
                {
                    wordWrap = value;
                    OnPropertyChanged("WordWrap");
                }
            }
        }

        private bool internalMoving;
        public bool InternalMoving
        {
            get
            {
                return internalMoving;
            }
            set
            {
                if (internalMoving != value)
                {
                    internalMoving = value;
                    OnPropertyChanged("InternalMoving");
                }
            }
        }

        public DelegateCommand<object> ToggleFreezeCommand
        {
            get;
            private set;
        }

        public DelegateCommand<object> ViewHeaderCommand
        {
            get;
            private set;
        }
        public ICommand EditTabTagCommand
        {
            get;
            private set;
        }
         
        public DelegateCommand<object> MarkSelectedCommand
        {
            get;
            private set;
        }

        public DelegateCommand<object> UnmarkSelectedCommand
        {
            get;
            private set;
        }

        public DelegateCommand<object> UnmarkAllCommand
        {
            get;
            private set;
        }

        public DelegateCommand<object> GoToFirstMarkedCommand
        {
            get;
            private set;
        }

        public DelegateCommand<object> GoToPreviousMarkedCommand
        {
            get;
            private set;
        }

        public DelegateCommand<object> GoToNextMarkedCommand
        {
            get;
            private set;
        }

        public DelegateCommand<object> GoToLastMarkedCommand
        {
            get;
            private set;
        }

        public DelegateCommand<object> MarkSimilarCommand
        {
            get;
            private set;
        }

        public ICommand GoToNewItemsCommand
        {
            get;
            private set;
        }

        public ICommand ToggleShowMarkedOnlyCommand
        {
            get;
            private set;
        }

        private void Start()
        {
            Started = true;
            pauseEvent.Set();
        }

        private void Stop()
        {
            Started = false;
            pauseEvent.Reset();
        }

        private bool bindingMarkedItems = false;
        private void ShowMarkedOnly()
        {
            showOnlyMarkedItems = !showOnlyMarkedItems; 
            visibleItems.CurrentChanged -= new EventHandler(items_CurrentChanged);

            //changing the datasource and at the same time adding items to the datasource in another thread
            //since only the markedItems can be added in the background thread (it uses List<T> instead of ObservableCollection<T>)
            //we only track this only when setting the data source to MarkedItems
            //would trigger an exception in XamDataGrid
            if (showOnlyMarkedItems)
            {
                bindingMarkedItems = true;
            }
            try
            {
                VisibleItems = showOnlyMarkedItems ? MarkedItems : Items;
            }
            finally
            {
                bindingMarkedItems = false;
            }
            visibleItems.CurrentChanged += new EventHandler(items_CurrentChanged);
        }

        internal void CreateMonitor()
        {
            DisposeMonitor();
            monitor = (new RegexLogMonitor(this).HaveNotSeenMatchStream
                            .Subscribe(numberOfSecs => Warn(numberOfSecs, LogViewerSetting.Current.RegexMonitor.CommandToRun, LogViewerSetting.Current.RegexMonitor.Arguments)));
        }

        internal void DisposeMonitor()
        {
            if (monitor != null)
                monitor.Dispose();
            monitor = null;
        }

        private void Warn(int n_, string commandToRun_, string arguments_)
        {
            Console.WriteLine(@"Alert Triggered on line " + n_);
            if (!string.IsNullOrEmpty(commandToRun_))
            {
                try
                {
                    ShowDialogEventArgs arg = new ShowDialogEventArgs(commandToRun_, new string[] { commandToRun_, arguments_ }, "StartProcess");
                    parent.RaiseShowDialogRequest(arg);
                }
                catch
                {
                    Console.WriteLine(@"Error running command: " + commandToRun_);
                }
            }
        }
         
        List<FileSystemWatcher> isolatedLogFileWatchers = new List<FileSystemWatcher>();
        private void LoadNewItems()
        {
            if (loading) return;
            if (Items != null)
            {
                Items.CurrentChanged -= new EventHandler(items_CurrentChanged);
            }
            Items = null;
            if (markedItemsList != null)
            {
                markedItemsList.Clear();
            }
            if (markedItems != null)
            {
                markedItems.CurrentChanged -= new EventHandler(items_CurrentChanged);
            }
            markedItems = null;
            ItemsCount = 0;
            Loading = true;
            bool tailing = false; 
            Task.Factory.StartNew(new Action(
                 () =>
                 { 
                     subscription =
                    logReader.GetObservable(null, entryLimit, tokenSource)
          .Select(logResource =>
          {

              pauseEvent.WaitOne();

              if (logResource.Header != null)
              {
                  this.Header = logResource.Header;
                  return null;
              }
              if (logResource.Entry != null && 
                  logResource.Entry.Class == "IsolatedSubsystemSite" && 
                  logResource.Entry.Method == "SetLogFilePath" && 
                  !string.IsNullOrEmpty(logResource.Entry.Details))
              {
                  if (File.Exists(logResource.Entry.Details))
                  {
                      Application.Current.Dispatcher.BeginInvoke(new Action(() => parent.OpenLogCore(logResource.Entry.Details, false, false)));
                  }
                  else 
                  {
                      string directory = Path.GetDirectoryName(logResource.Entry.Details);
                      if (!string.IsNullOrEmpty(directory) && Directory.Exists(directory))
                      {
                          FileSystemWatcher watcher = new FileSystemWatcher(logResource.Entry.Details);
                          watcher.EnableRaisingEvents = true; 
                          watcher.Created += new FileSystemEventHandler(watcher_Created);
                          lock (isolatedLogFileWatchers)
                          { 
                              isolatedLogFileWatchers.Add(watcher);
                          }
                      }
                  }
                  return null;
                  //loading = true;
              }
              if (logResource.TailTimeCount > 0)
              {
                  tailing = true;
                  if (logResource.TailTimeCount > 1)
                  {
                      HasNewItems = true;
                  } 
                  return null;
              }

              if (logResource.LimitReached)
              {
                  tailing = true;
                  parent.RaiseShowDialogRequest(new ShowDialogEventArgs("Log Entry Truncated",
                      notLoaded ?
                      string.Format("Only the top {0} entries were loaded due to a log entry limit set in preferences.", entryLimit) :
                  "New entries added to log, not loaded due to a log entry limit set in preferences.", "Warning")); 
              }
              return logResource.Entry;
          })
          .Where(entry => entry != null)
          .Do(entry => entries.OnNext(entry))
          .Select(entry => entry)
          .Buffer(TimeSpan.FromMilliseconds(300), 5000)
          .Subscribe(
         //onNext
          logResources =>
          {
              if (logResources.Count > 0 || Header == null)
              {
                  Application.Current.Dispatcher.Invoke(new Action(() => this.AddItems(logResources)));
                  var handler = LogItemsAdded;
                  if (handler != null)
                  {
                      handler(new LogItemsAddedEventArgs(logResources));
                  }

                  NotLoaded = false;
              }
              else if (tailing) 
              {
                  //create the regex monitor upon log lines added later on
                  if (monitor == null &&
     LogViewerSetting.Current.RegexMonitor.IsEnabled &&
     LogViewerSetting.Current.RegexMonitor.OnlyOnLiveLogEntries)
                  {
                      CreateMonitor();
                  }

                  if (VisibleItems != null && VisibleItems.SortDescriptions.Count == 0)
                  {
                      Application.Current.Dispatcher.Invoke(new Action(() =>
                      {
                          using (VisibleItems.DeferRefresh())
                          {
                              VisibleItems.SortDescriptions.Add(new SortDescription("EntryNumber", ListSortDirection.Descending));
                              VisibleItems.CurrentChanged += new EventHandler(items_CurrentChanged);
                          }
                          VisibleItems.MoveCurrentToFirst();
                      })); 
                  } 
                  Loading = false;
                  tailing = false;
              } 
          },
          //onError
          exception => parent.RaiseShowDialogRequest(new ShowDialogEventArgs("Error While Parsing Log",
                              "Error occurred while parsing file" + Environment.NewLine + exception, "Error")));
                 }));
        }

        private void DisposeIsolatedLogFileWatcher(FileSystemWatcher watcher)
        {
            watcher.Created -= watcher_Created;
            watcher.Dispose();
            lock (watcher)
            {
                isolatedLogFileWatchers.Remove(watcher);
            }
        }
        void watcher_Created(object sender, FileSystemEventArgs e)
        {
            FileSystemWatcher watcher = (FileSystemWatcher) sender;
            DisposeIsolatedLogFileWatcher(watcher);
            if (File.Exists(e.FullPath))
            {
                Application.Current.Dispatcher.BeginInvoke(new Action(() => parent.OpenLogCommand.Execute(e.FullPath)));
            }
        }

        private void ViewHeader()
        {
            if (this.header == null) return;
            this.parent.RaiseShowDialogRequest(new ShowDialogEventArgs("Header", header, "HeaderInformation"));
        }

        private void EditTabTag()
        {
            TabTagViewModel model = new TabTagViewModel() { TagColor = this.TagColor, TagText = this.TagText };
            var arg = new ShowDialogEventArgs("Edit Tab Tag", model, "EditTabTag");
            this.parent.RaiseShowDialogRequest(arg);
            if (Convert.ToBoolean(arg.Result))
            {
                this.TagColor = model.TagColor;
                this.TagText = model.TagText;
            }
        }

        private bool CanMark()
        {
            if (markedItemsList == null || markedItemsList.Count == 0) return true;
            return (SelectedItems != null && SelectedItems.Count > 0 &&
                SelectedItems.Cast<ILogEntry>().Any(item => item != null && !item.Marked));
        }

        private bool CanUnmark()
        {
            if (markedItemsList == null || markedItemsList.Count == 0) return false;
            return (SelectedItems != null && SelectedItems.Count > 0 &&
                SelectedItems.Cast<ILogEntry>().Any(item => item != null && item.Marked));
        }

        private void MarkSelected()
        {
            if (SelectedItems != null && SelectedItems.Count > 0)
            {
                if (markedItemsList == null)
                {
                    markedItemsList = Activator.CreateInstance(typeof(List<>).MakeGenericType(SelectedItems[0].GetType())) as IList;
                }
                lock (markedItemsList)
                {
                    IList selectedItems = SelectedItems;
                    foreach (ILogEntry item in selectedItems)
                    {
                        if (item == null || item.Marked) continue;
                        item.Marked = true;
                        if (!markedItemsList.Contains(item))
                        {
                            markedItemsList.Add(item);
                        }
                    }
                    if (markedItems != null)
                    {
                        markedItems.Refresh();
                    }
                }
                UpdateCommandsStatus(); 
            }

        }

        private void UnmarkSelected()
        {
            if (SelectedItems != null && markedItemsList != null)
            {
                lock (markedItemsList)
                {
                    IList selectedItems = SelectedItems;
                    foreach (ILogEntry item in selectedItems)
                    {
                        if (item == null) continue;
                        item.Marked = false;
                        markedItemsList.Remove(item);
                    }
                    if (markedItems != null)
                    {
                        markedItems.Refresh();
                    }
                }
                UpdateCommandsStatus();
            }
        }

        private void UnmarkAll()
        {
            if (markedItemsList == null) return;
            lock (markedItemsList)
            {
                lock (similars)
                {
                    similars.Clear();
                }
                foreach (ILogEntry item in markedItemsList)
                {
                    if (item == null) continue;
                    item.Marked = false;
                }
                markedItemsList.Clear();
                if (markedItems != null)
                {
                    markedItems.Refresh();
                }
            }
            UpdateCommandsStatus();
        }


        private void MarkSimilar()
        {
            similars = new List<string>();
            if (SelectedItems != null)
            {
                IList selectedItems = SelectedItems;
                foreach (ILogEntry item in selectedItems)
                {
                    if (item == null) continue;
                    string similar = item.GetCharacteristic();
                    if (similar == null) continue;
                    if (!similars.Contains(similar))
                    {
                        similars.Add(similar);
                    }
                    item.Marked = true;
                    if (markedItemsList == null)
                    {
                        markedItemsList = Activator.CreateInstance(typeof(List<>).MakeGenericType(item.GetType())) as IList;
                    }
                    if (!markedItemsList.Contains(item))
                    {
                        markedItemsList.Add(item);
                    }
                }
            }
            if (similars.Count > 0)
            {
                IList list = Items.SourceCollection as IList;
                int count = list.Count;

                Task.Factory.StartNew(() =>
                    {
                        //lock (markedItemsList)
                        {
                            for(int i = 0; i < count; i++)
                            {
                                //wait until MarkedItems finished binding
                                while (bindingMarkedItems)
                                {
                                }
                                MarkIfSimilar(list[i] as ILogEntry);
                            }
                            if (markedItems != null)
                            {
                                Application.Current.Dispatcher.Invoke(new Action(() =>
                                markedItems.Refresh()));
                            }
                            UpdateCommandsStatus();
                        }
                    });
            }
        }

        private void MarkIfSimilar(ILogEntry item_)
        {
            if (item_.Marked) return;
            lock (similars)
            {
                foreach (var similar in similars)
                {
                    if (item_.IsSimilarTo(similar))
                    {
                        item_.Marked = true;
                        if (markedItemsList == null)
                        {
                            markedItemsList = Activator.CreateInstance(typeof(List<>).MakeGenericType(item_.GetType())) as IList;
                        }
                        if (!markedItemsList.Contains(item_))
                        {
                            markedItemsList.Add(item_);
                        }
                        break;
                    }
                }
            }
        }
         
        private void GoToNewItems()
        {
            if (unvisitedNewItem != null)
            {
                int current = VisibleItems.CurrentPosition;
                InternalMoving = true;
                VisibleItems.MoveCurrentTo(unvisitedNewItem);
                InternalMoving = false;
                if (CurrentItem == null)
                {
                    parent.RaiseShowDialogRequest(new ShowDialogEventArgs("Information", "New entries are filtered out.", "Information"));
                    //restore focus
                    if (current >= 0)
                    {
                        InternalMoving = true;
                        VisibleItems.MoveCurrentToPosition(current);
                        InternalMoving = false;
                    }
                    return;
                }
                unvisitedNewItem = null;
                HasNewItems = false;
            }
        }

        private void AddItems(IList<ILogEntry> newItems_)
        {

            if (Items == null) //no need to mark similar items, since this cannot start until the first page of the items are loaded
            {
                object items = null;
                //need to create th source collection in the type of the element to make the filter drop down of the 
                //grid it bound to to work 
                if (newItems_.Count > 0)
                {
                    items = Activator.CreateInstance(typeof(ObservableCollection<>).MakeGenericType(newItems_[0].GetType()));
                    IList list = items as IList;
                    foreach (var newItem in newItems_)
                    {
                        list.Add(newItem); 
                    }
                }
                else
                {
                    items = new ObservableCollection<ILogEntry>(newItems_);
                }
                Items = CollectionViewSource.GetDefaultView(items);
                
                ItemsCount = newItems_.Count;
                return;
            } 
            IList list2 = Items.SourceCollection as IList;
            foreach (var newItem in newItems_)
            {
                if (similars.Count > 0)
                {
                    MarkIfSimilar(newItem);
                }
                list2.Add(newItem);
                unvisitedNewItem = newItem;
            }
            if (showOnlyMarkedItems)
            {
                MarkedItems.Refresh();
            }
            ItemsCount += newItems_.Count;

        } 

        void items_CurrentChanged(object sender, EventArgs e)
        {
            UpdateCommandsStatus();
        }
         
        private void UpdateCommandsStatus()
        {
            MarkSelectedCommand.RaiseCanExecuteChanged();
            UnmarkSelectedCommand.RaiseCanExecuteChanged();
            UnmarkAllCommand.RaiseCanExecuteChanged();
            MarkSimilarCommand.RaiseCanExecuteChanged();
        }
         
        private void GoToFirstMarked()
        {
            if (CurrentItem != null && markedItemsList != null && markedItemsList.Count == 1 && markedItemsList[0] == CurrentItem) return;
            if (markedItemsList != null && markedItemsList.Count > 0)
            {
                int index = 0;
                foreach (ILogEntry item in VisibleItems)
                {
                    if (item.Marked)
                    {
                        InternalMoving = true;
                        VisibleItems.MoveCurrentToPosition(index);
                        InternalMoving = false;
                        return;
                    }
                    index++;
                }
            }

            FailedToMoveToMarkedItem();
        }

        private void GoToLastMarked()
        {
            if (CurrentItem != null && markedItemsList.Count == 1 && markedItemsList[0] == CurrentItem) return;
            int lastMarkedItemIndex = -1;
            if (markedItemsList != null && markedItemsList.Count > 0)
            {
                int index = 0;
                foreach (ILogEntry item in VisibleItems)
                {
                    if (item.Marked) lastMarkedItemIndex = index;
                    index++;

                }
            }
            if (lastMarkedItemIndex >= 0)
            {
                InternalMoving = true;
                VisibleItems.MoveCurrentToPosition(lastMarkedItemIndex);
                InternalMoving = false;
            }
            else
            {
                FailedToMoveToMarkedItem();
            }
        }

        private void GoToNextMarked()
        {
            if (CurrentItem != null && markedItemsList != null && markedItemsList.Count == 1 && markedItemsList[0] == CurrentItem) return;
            if (markedItemsList != null && markedItemsList.Count > 0)
            {
                bool passedCurrent = CurrentItem == null; //if no current item, move to the first marked item
                int index = 0;
                foreach (ILogEntry item in VisibleItems)
                {
                    if (passedCurrent && item.Marked)
                    {
                        InternalMoving = true;
                        VisibleItems.MoveCurrentToPosition(index);
                        InternalMoving = false;
                        return;
                    }
                    if (item == CurrentItem)
                    {
                        passedCurrent = true;
                    }
                    index++;
                }
            }
            if (CurrentItem == null || !CurrentItem.Marked)
            {
                FailedToMoveToMarkedItem();
            }
        }

        private void GoToPreviousMarked()
        {
            if (CurrentItem != null && markedItemsList != null && markedItemsList.Count == 1 && markedItemsList[0] == CurrentItem) return;

            int previousMarkedItemIndex = -1;
            if (markedItemsList != null && markedItemsList.Count > 0)
            {
                int index = 0;
                foreach (ILogEntry item in VisibleItems)
                {
                    if (item == CurrentItem) break; //if current item is null, this would always fail, so would move to last item
                    if (item.Marked) previousMarkedItemIndex = index;
                    index++;
                }
            }
            if (previousMarkedItemIndex != -1)
            {
                InternalMoving = true;
                VisibleItems.MoveCurrentToPosition(previousMarkedItemIndex);
                InternalMoving = false;
            }
            else if (CurrentItem == null || !CurrentItem.Marked)
            {
                FailedToMoveToMarkedItem();
            }
        }

        private void FailedToMoveToMarkedItem()
        {
            if (markedItemsList == null || markedItemsList.Count == 0)
            {
                parent.RaiseShowDialogRequest(new ShowDialogEventArgs("Marking", "No log entry is marked.", "Information"));
            }
            else
            {
                parent.RaiseShowDialogRequest(new ShowDialogEventArgs("Marking", "No visible log entry is marked.", "Information"));
            }
        }

        private bool disposed = false;
        protected override void OnDispose()
        {
            if (disposed) return;
            disposed = true;
            lock (isolatedLogFileWatchers)
            {
                for (int i = isolatedLogFileWatchers.Count - 1; i >= 0; i--)
                {
                    DisposeIsolatedLogFileWatcher(isolatedLogFileWatchers[i]);
                }
            }
            if (monitor != null) monitor.Dispose();
            if (subscription != null) subscription.Dispose();
            if (logReader != null)
            {
                tokenSource.Cancel();
                tokenSource.Dispose();
            }
            pauseEvent.Dispose();
            if (Items != null)
            {
                Items.CurrentChanged -= new EventHandler(items_CurrentChanged);
            }
            if (markedItems != null)
            {
                markedItems.CurrentChanged -= new EventHandler(items_CurrentChanged);
            }
           
        }

    }
}
