﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MSDesktop.Debugging.Models;

namespace MSDesktop.Debugging.ViewModels
{
    internal class LogItemsAddedEventArgs
    {
        public readonly IEnumerable<ILogEntry> ItemsAdded;

        public LogItemsAddedEventArgs(IEnumerable<ILogEntry> itemsAdded_)
        {
            this.ItemsAdded = itemsAdded_;
        }
    } 
}
