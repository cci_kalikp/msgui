﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Collections.ObjectModel;
using System.Windows.Input;
using Microsoft.Practices.Composite.Presentation.Commands;
using System.IO;
using System.Windows;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;

namespace MSDesktop.Debugging.ViewModels
{
    public class LogsViewModel : ViewModelBase, IShowDialogRequester
    {
        internal static string LogDirectory = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\Morgan Stanley\";
        internal const string LogFileExtension = ".log";
        private string directoryToMonitor;
        private FileSystemWatcher directoryWatcher;

        public LogsViewModel(string logDirectory_)
        {
            if (logDirectory_ != null)
            {
                LogDirectory = logDirectory_;
            }
            directoryToMonitor = LogDirectory;
            Logs = new ObservableCollection<LogViewModel>();
            OpenLogCommand = new DelegateCommand<string>(OpenLog);
            ToggleWordWrapCommand = new DelegateCommand<object>(_ => ToggleWordWrap());
            EditPreferencesCommand = new DelegateCommand<object>(_ => EditPreferences());
            WatchDirectoryCommand = new DelegateCommand<object>(_ => WatchDirectory());
            SetupMonitorCommand = new DelegateCommand<object>(_ => SetupMonitor());
            OpenLogsCommand = new DelegateCommand<object>(OpenLogs);
            this.DisposeAutomatically = true;
            this.DisableSort = LogViewerSetting.Current.DisableSort;
            if (Application.Current != null)
            {
                Application.Current.Exit += new ExitEventHandler(Current_Exit); 
            }
        }

        void Current_Exit(object sender, ExitEventArgs e)
        {
            this.Dispose(); 
        }
         

        public override string DisplayName
        {
            get { return "Logs"; }
            protected set
            {
                base.DisplayName = value;
            }
        }
        private ObservableCollection<LogViewModel> logs;
        public ObservableCollection<LogViewModel> Logs
        {
            get
            {
                return logs;
            }
            private set
            {
                logs = value;
            }
        }

        public ICommand OpenLogCommand
        {
            get;
            private set;
        }

        public ICommand WatchDirectoryCommand
        {
            get;
            private set;
        }

        public ICommand ToggleWordWrapCommand
        {
            get;
            private set;
        }

        public ICommand EditPreferencesCommand
        {
            get;
            private set;
        }

        public ICommand SetupMonitorCommand
        {
            get;
            private set;
        }

        public ICommand OpenLogsCommand
        {
            get;
            private set;
        }
 
        private LogViewModel currentLog;
        public LogViewModel CurrentLog
        {
            get
            {
                return currentLog;
            }
            set
            {
                currentLog = value;
                OnPropertyChanged("CurrentLog");
                if (currentLog != null)
                {
                    currentLog.UpdateDisplayLogName();
                }
            }
        }

        private bool wordWrap = false;
        public bool WordWrap
        {
            get
            {
                return wordWrap;
            }
            set
            {
                if (wordWrap != value)
                {
                    wordWrap = value;
                    OnPropertyChanged("WordWrap");
                }
            }
        }

        public bool DisposeAutomatically { get; set; }
        private void OpenLog(string parameter_)
        {
            string fileName = parameter_; 
            if (string.IsNullOrEmpty(fileName))
            {
                ShowDialogEventArgs arg = new ShowDialogEventArgs("Open Log", null, "OpenLogFile");
                RaiseShowDialogRequest(arg);
                fileName = arg.Result as string;
                if (fileName == null)
                {
                    string[] fileNames = arg.Result as string[];
                    if (fileNames != null)
                    {
                        OpenLogs(fileNames);
                        return;
                    }
                }
            }
            OpenLogCore(fileName, true, true);
        }

        internal void OpenLogCore(string fileName_, bool reportDuplicate_, bool activateLast_)
        {
            if (string.IsNullOrEmpty(fileName_)) return;
            foreach (LogViewModel log in Logs)
            {
                if (log.FilePath == fileName_)
                {
                    if (reportDuplicate_)
                    {
                        this.RaiseShowDialogRequest(new ShowDialogEventArgs("File already open", string.Format("File {0} is already open.", fileName_), "Warning"));
                        if (log != CurrentLog && activateLast_)
                        {
                            CurrentLog = log;
                        } 
                    }

                    return;
                }
            }

            try
            {
                LogViewModel logModel = new LogViewModel(this, fileName_);
                if (activateLast_)
                {
                    CurrentLog = logModel;
                }

            }
            catch (IOException ex)
            {
                TaskDialog.ShowMessage(ex.Message, "Error");
            }

        }
        
        private void ToggleWordWrap()
        {
            foreach (LogViewModel log in this.Logs)
            {
                log.WordWrap = !log.WordWrap;
            }
            this.WordWrap = !WordWrap;
        }

        private bool disableSort;
        public bool DisableSort
        {
            get { return disableSort; }
            set { disableSort = value; OnPropertyChanged("DisableSort"); }
        }
        private void EditPreferences()
        {
            var context = new PreferencesViewModel(LogViewerSetting.Current.EntryLimit, LogViewerSetting.Current.RememberCurrentSession, LogViewerSetting.Current.DisableSort);
            ShowDialogEventArgs arg = new ShowDialogEventArgs("Preferences", context, "Preferences");
            this.RaiseShowDialogRequest(arg);
            if (Convert.ToBoolean(arg.Result))
            {
                LogViewerSetting.Current.EntryLimit = context.MaxEntries;
                LogViewerSetting.Current.RememberCurrentSession = context.RememberCurrentSession; 
                this.DisableSort = LogViewerSetting.Current.DisableSort = context.DisableSort;
                //removed, due to the complexity when the log is still loading
                //foreach (LogViewModel log in this.Logs)
                //{
                //    log.EntryLimit = LogViewerSetting.Current.EntryLimit;
                //} 
            }
        }
 

        private void OpenLogs(object parameter_)
        {
            if (parameter_ == null) return;
            string[] files = parameter_ as string[];
            if (files == null || files.Length == 0) return;
            foreach (string file in files)
            {
                if (file.EndsWith(LogFileExtension))
                {
                    OpenLog(file);
                }
            }
        }

        private void WatchDirectory()
        {
            ShowDialogEventArgs arg = new ShowDialogEventArgs("Select Folder to Watch", directoryToMonitor, "SelectDirectory");
            RaiseShowDialogRequest(arg);
            string selectedDirectory = arg.Result as string;
            if (!string.IsNullOrWhiteSpace(selectedDirectory))
            {
                DirectoryInfo oldDirectory = new DirectoryInfo(directoryToMonitor);
                DirectoryInfo newDirectory = new DirectoryInfo(selectedDirectory);
                if (oldDirectory.FullName != newDirectory.FullName ||directoryWatcher == null)
                { 
                    directoryToMonitor = selectedDirectory;
                    DisposeDirectoryWatcher();
                    CreateDirectoryWatcher();
                }
            }
        }

        private void SetupMonitor()
        {
            MonitorSettingViewModel setting = new MonitorSettingViewModel();
            LogViewerSetting.Current.RegexMonitor.CopyTo(setting);
            ShowDialogEventArgs arg = new ShowDialogEventArgs("Log Monitor Setup", setting, "SetupMonitor");
            RaiseShowDialogRequest(arg);
            if (Convert.ToBoolean(arg.Result))
            {
                setting.CopyTo(LogViewerSetting.Current.RegexMonitor);
                foreach (LogViewModel log in this.Logs)
                {
                    if (!LogViewerSetting.Current.RegexMonitor.IsEnabled)
                    {
                        log.DisposeMonitor();
                    }
                    else
                    {
                        log.CreateMonitor();
                    }
                } 
            }
        }

        private void CreateDirectoryWatcher()
        {
            directoryWatcher = new FileSystemWatcher(directoryToMonitor)
            {
                IncludeSubdirectories = true,
                EnableRaisingEvents = true 
            };
            directoryWatcher.Created += directoryWatcher_Created;
        }

        private void DisposeDirectoryWatcher()
        {
            if (directoryWatcher != null)
            {
                directoryWatcher.EnableRaisingEvents = false;
                directoryWatcher.Created -= new FileSystemEventHandler(directoryWatcher_Created);
                directoryWatcher.Dispose();
                directoryWatcher = null;
            }
        }

        void directoryWatcher_Created(object sender, FileSystemEventArgs e)
        {
            if (e.ChangeType == WatcherChangeTypes.Created && e.FullPath.EndsWith(LogFileExtension))
            {
                Application.Current.Dispatcher.Invoke(new Action(() => OpenLogCore(e.FullPath, false, false))); 
            }
        }

        public event ShowDialogRequestHandler ShowDialogRequestHandler;

        internal void RaiseShowDialogRequest(ShowDialogEventArgs args_)
        {
            var handler = ShowDialogRequestHandler;
            if (handler != null)
            {
                handler(args_);
            }
        }

        protected override void OnDispose()
        {
            if (Application.Current != null)
            {
                Application.Current.Exit -= new ExitEventHandler(Current_Exit); 
            }
            DisposeDirectoryWatcher();
            foreach (LogViewModel log in this.logs)
            {
                log.Dispose();
            }
            currentLog = null; 
        }
    }
}
