﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using System.ComponentModel;

namespace MSDesktop.Debugging.ViewModels
{
    public class PreferencesViewModel : ViewModelBase, IDataErrorInfo
    { 
        public PreferencesViewModel(uint maxLines_, bool rememberCurrentSession_, bool disableSort_)
        {
            if (maxLines_ > 0)
            {
                EntryLimit = maxLines_;
                HasEntryLimit = true;
            }
            else
            {
                EntryLimit = 500000; 
            }
            rememberCurrentSession = rememberCurrentSession_;
            disableSort = disableSort_;
        }

        public bool StandaloneLogViewer
        {
            get { return DebuggingExtensions.ChromeManager == null; }
        }
        private bool hasEntryLimit = false;

        public bool HasEntryLimit
        {
            get { return hasEntryLimit; }
            set
            {
                if (hasEntryLimit != value)
                {
                    hasEntryLimit = value;
                    OnPropertyChanged("HasEntryLimit");
                }
            }
        }

        private uint entryLimit;
        public uint EntryLimit
        {
            get
            {
                return entryLimit;
            }
            set
            {
                if (value <= 0)
                {
                    error = errors["EntryLimit"] = "Must be a positive number";
                }
                else
                {
                    errors.Clear();
                    error = null;
                }
                entryLimit = value;
            }
        }

        private bool rememberCurrentSession;
        public bool RememberCurrentSession
        {
            get { return rememberCurrentSession; }
            set { rememberCurrentSession = value; OnPropertyChanged("RememberCurrentSession"); }
        }

        private bool disableSort;
        public bool DisableSort
        {
            get { return disableSort; }
            set { disableSort = value; OnPropertyChanged("DisableSort"); }
        }

        private Dictionary<string, string> errors = new Dictionary<string, string>();
        private string error;
        string IDataErrorInfo.Error
        {

            get
            {
                return error;
            }
        }

        string IDataErrorInfo.this[string columnName_]
        {
            get
            {
                string error;
                errors.TryGetValue(columnName_, out error);
                return error;
            }
        }
        public uint MaxEntries
        {
            get
            {
                return HasEntryLimit ? EntryLimit : 0;
            }
        }
    }
}
