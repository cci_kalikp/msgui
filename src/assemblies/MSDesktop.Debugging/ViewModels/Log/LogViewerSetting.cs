﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Xml.Linq; 

namespace MSDesktop.Debugging.ViewModels
{
    public class LogViewerSetting
    {
        public static LogViewerSetting Current { get; set; }
        public MonitorSettingViewModel RegexMonitor = new MonitorSettingViewModel();
        
        static LogViewerSetting()
        {
            Current = new LogViewerSetting();
        }

        public LogViewerSetting()
        {
            RememberCurrentSession = true;
        }

        public static void LoadState(XDocument state_)
        {
            if (state_ != null)
            {
                var entryLimitAttr = state_.Root.Attribute("EntryLimit");
                if (entryLimitAttr != null)
                {
                    uint entryLimit;
                    if (UInt32.TryParse(entryLimitAttr.Value, out entryLimit) && entryLimit > 0)
                    {
                        Current.EntryLimit = entryLimit;
                    }
                }
                var rememberCurrentSession = state_.Root.Attribute("RememberCurrentSession");
                if (rememberCurrentSession != null)
                {
                    Current.RememberCurrentSession = rememberCurrentSession.Value.ToLower() == "true";
                }
                var disableSort = state_.Root.Attribute("DisableSort");
                if (disableSort != null)
                {
                    Current.DisableSort = disableSort.Value.ToLower() == "true";
                }

                XElement monitorElement = state_.Root.Element("RegexMonitorSetting");
                if (monitorElement == null) return;
                var isEnabledAttr = monitorElement.Attribute("IsEnabled");
                Current.RegexMonitor.IsEnabled = isEnabledAttr != null && isEnabledAttr.Value.ToLower() == "true";
                if (Current.RegexMonitor.IsEnabled)
                {
                    var timeInternalInSecAttr = monitorElement.Attribute("TimeIntervalInSec");
                    if (timeInternalInSecAttr != null)
                    {
                        uint interval;
                        if (UInt32.TryParse(timeInternalInSecAttr.Value, out interval) && interval > 0)
                        {
                            Current.RegexMonitor.TimeIntervalInSec = interval;
                        }
                    }
                    if (Current.RegexMonitor.TimeIntervalInSec == null || Current.RegexMonitor.TimeIntervalInSec.Value == 0)
                    {
                        Current.RegexMonitor.IsEnabled = false;
                        return;
                    }
                    var regexAttr = monitorElement.Attribute("RegExToMonitor");
                    if (regexAttr != null)
                    {
                        Current.RegexMonitor.RegExToMonitor = regexAttr.Value.Trim();
                    }
                    var commandToRunAttr = monitorElement.Attribute("CommandToRun");
                    if (commandToRunAttr != null)
                    {
                        Current.RegexMonitor.CommandToRun = commandToRunAttr.Value.Trim();
                    }
                    var runOnceAttr = monitorElement.Attribute("RunOnce");
                    if (runOnceAttr != null)
                    {
                        bool runOnce;
                        if (Boolean.TryParse(runOnceAttr.Value, out runOnce))
                        {
                            Current.RegexMonitor.RunOnce = runOnce;
                        }
                    }
                    var onlyOnLiveLogEntriesAttr = monitorElement.Attribute("OnlyOnLiveLogEntries");
                    if (onlyOnLiveLogEntriesAttr != null)
                    {
                        bool onlyOnLiveLogEntries;
                        if (Boolean.TryParse(onlyOnLiveLogEntriesAttr.Value, out onlyOnLiveLogEntries))
                        {
                            Current.RegexMonitor.OnlyOnLiveLogEntries = onlyOnLiveLogEntries;
                        }
                    }
                    var argumentsAttr = monitorElement.Attribute("Arguments");
                    if (argumentsAttr != null)
                    {
                        Current.RegexMonitor.Arguments = argumentsAttr.Value.Trim();
                    }
                }
            } 
        }

        public static XDocument SaveState()
        {
            XElement rootElement = new XElement(PersistorId);
            if (Current.EntryLimit > 0)
            {
                rootElement.SetAttributeValue("EntryLimit", Current.EntryLimit);
            }
            rootElement.SetAttributeValue("RememberCurrentSession", Current.RememberCurrentSession);
            rootElement.SetAttributeValue("DisableSort", Current.DisableSort);
            XElement monitorElement = new XElement("RegexMonitorSetting");
            monitorElement.SetAttributeValue("IsEnabled", Current.RegexMonitor.IsEnabled.ToString());
            if (Current.RegexMonitor.IsEnabled)
            {
                if (Current.RegexMonitor.TimeIntervalInSec != null && Current.RegexMonitor.TimeIntervalInSec.Value > 0)
                {
                    monitorElement.SetAttributeValue("TimeIntervalInSec", Current.RegexMonitor.TimeIntervalInSec.ToString());
                    if (!string.IsNullOrWhiteSpace(Current.RegexMonitor.RegExToMonitor))
                    {
                        monitorElement.SetAttributeValue("RegExToMonitor", Current.RegexMonitor.RegExToMonitor.Trim());
                    }
                    if (!string.IsNullOrWhiteSpace(Current.RegexMonitor.CommandToRun))
                    {
                        monitorElement.SetAttributeValue("CommandToRun", Current.RegexMonitor.CommandToRun.Trim());
                    }
                    monitorElement.SetAttributeValue("RunOnce", Current.RegexMonitor.RunOnce.ToString());
                    monitorElement.SetAttributeValue("OnlyOnLiveLogEntries", Current.RegexMonitor.OnlyOnLiveLogEntries.ToString());
                    if (!string.IsNullOrWhiteSpace(Current.RegexMonitor.Arguments))
                    {
                        monitorElement.SetAttributeValue("Arguments", Current.RegexMonitor.Arguments.Trim());
                    }
                }
                else
                {
                    Current.RegexMonitor.IsEnabled = false;
                    monitorElement.SetAttributeValue("IsEnabled", Current.RegexMonitor.IsEnabled.ToString());
                }
            }
            rootElement.Add(monitorElement);
             
            return new XDocument(rootElement);
        }

        public static string PersistorId
        {
            get { return "LogViewerSetting"; }
        }
         

        public uint EntryLimit { get; set; }
        public bool RememberCurrentSession { get; set; }
        public bool DisableSort { get; set; }
 
    }
}
