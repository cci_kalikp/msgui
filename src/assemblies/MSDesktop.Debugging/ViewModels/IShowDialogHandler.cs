﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSDesktop.Debugging.ViewModels
{
    public interface IShowDialogHandler
    {
        void OnShowDialogRequest(ShowDialogEventArgs arg_);  
    }
}
