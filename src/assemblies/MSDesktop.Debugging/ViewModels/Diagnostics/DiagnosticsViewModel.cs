﻿using System; 
using System.Collections.ObjectModel; 
using System.ComponentModel;
using System.Linq; 
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;

namespace MSDesktop.Debugging.ViewModels.Dashboard
{
    class DiagnosticsViewModel:ViewModelBase, IDebugViewModel
    {
        public DiagnosticsViewModel()
        {
            this.Models = new ObservableCollection<INotifyPropertyChanged>(); 
        }

        public ObservableCollection<INotifyPropertyChanged> Models { get; private set; }

        private INotifyPropertyChanged currentModel;
        public INotifyPropertyChanged CurrentModel
        {
            get { return currentModel; }
            set
            {
                if (currentModel != value)
                {
                    currentModel = value;
                    OnPropertyChanged("CurrentModel");
                }
            }
        }
 
        public void ShowAssemblies()
        {
            var assemblyInfos = GetAssemblyInfos();
            if (assemblyInfos == null) return;
            CurrentModel = assemblyInfos;
        }

        public void ShowModules()
        {
            var moduleInfos = GetModuleInfos();
            if (moduleInfos == null) return;
            CurrentModel = moduleInfos;
        }

        public AssemblyInfosViewModel GetAssemblyInfos()
        {
            return this.Models.FirstOrDefault(m_ => m_ is AssemblyInfosViewModel) as AssemblyInfosViewModel;
        }

        public ModuleInfosViewModel GetModuleInfos()
        {
            return this.Models.FirstOrDefault(m_ => m_ is ModuleInfosViewModel) as ModuleInfosViewModel;

        }
    }
}
