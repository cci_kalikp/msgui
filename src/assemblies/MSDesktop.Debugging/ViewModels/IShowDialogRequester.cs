﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSDesktop.Debugging.ViewModels
{
    public interface IShowDialogRequester
    { 
        event ShowDialogRequestHandler ShowDialogRequestHandler; 
    }

    public delegate void ShowDialogRequestHandler(ShowDialogEventArgs args_);
    public class ShowDialogEventArgs:EventArgs
    {
        public ShowDialogEventArgs(string title_, object content_, string eventType_=null)
        {
            this.Title = title_;
            this.Content = content_;
            this.EventType = eventType_;
        }

        public string Title
        {
            get;
            private set;
        }

        public object Content
        {
            get;
            private set;
        }

        public string EventType
        {
            get;
            set;
        }

        public object Result
        {
            get;
            set;
        }
    }
}
