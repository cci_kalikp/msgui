﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Microsoft.Practices.Composite.Presentation.Commands;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;

namespace MSDesktop.Debugging.ViewModels
{
    internal class ModuleInfoViewModel : ViewModelBase
    {
        private readonly Collection<string> dependsOnModuleNames; 

        public ModuleInfoViewModel(ModuleInfosViewModel parent_, ModuleLoadInfo moduleLoadInfo_)
        {
            Parent = parent_;
            int index = moduleLoadInfo_.FullTypeName.IndexOf(',');
            if (index > 0)
            { 
                TypeName = moduleLoadInfo_.FullTypeName.Substring(0, index);
                AssemblyName = moduleLoadInfo_.FullTypeName.Substring(index + 1).Trim();
            }
            else
            {
                TypeName = moduleLoadInfo_.FullTypeName;
            }
            FullTypeName = moduleLoadInfo_.FullTypeName;
            Status = moduleLoadInfo_.Status;
            ModuleName = moduleLoadInfo_.ModuleName;
            dependsOnModuleNames = moduleLoadInfo_.DependsOnModuleNames;
            switch (Status)
            {
                case ModuleStatus.TypeResolutionFailed:
                    StatusText = "Failed To Resolve Type";
                    break;
                case ModuleStatus.NotLoaded:
                    StatusText = "Not Loaded";
                    break;
                case ModuleStatus.Exceptioned:
                    StatusText = "Exception Occurred";
                    break;
                case ModuleStatus.BlockedByEntitlements:
                    StatusText = "Entitlement Not Allowed";
                    break;
                case ModuleStatus.DependentModuleBlocked:
                    StatusText = "Modules Dependent on Blocked";
                    break;
                case ModuleStatus.BlockedByNoEntitlementsInfo:
                    StatusText = "Entitlement Not Found";
                    break;
                case ModuleStatus.ProcessExited:
                    StatusText = "Process Exited";
                    break; 
                default:
                    StatusText = Status.ToString();
                    break;

            }
            if (moduleLoadInfo_.LoadingTime >= 0)
                LoadingTime = new TimeSpan(moduleLoadInfo_.LoadingTime).TotalMilliseconds;
            ModuleType = moduleLoadInfo_.Type;
            IsEarlyModule = moduleLoadInfo_.IsEarlyModule;
            OutOfProcess = moduleLoadInfo_.OutOfProcess;
            if (moduleLoadInfo_.Error != null)
            {
                FailReason = moduleLoadInfo_.Error.GetDetailedMessage();
            }
            else if (moduleLoadInfo_.Status == ModuleStatus.BlockedByEntitlements ||
                     moduleLoadInfo_.Status == ModuleStatus.BlockedByNoEntitlementsInfo)
            {
                FailReason = moduleLoadInfo_.RejectReason;
            }
            else
            {
                FailReason = moduleLoadInfo_.TypeNotFoundReason;
            }
            Succeed = moduleLoadInfo_.Status == ModuleStatus.Loaded;
            GoToAssemblyCommand = new DelegateCommand<object>(o_ => GoToAssembly());
            ActivateCommand = new DelegateCommand<object>(o_ => Activate());
        }

        public int? Order { get; internal set; }
        public string TypeName { get; private set; }
        public string FullTypeName { get; private set; }
        public string AssemblyName { get; private set; }
        public ModuleStatus Status { get; private set; }
        public string StatusText { get; private set; }
        public double? LoadingTime { get; private set; }
        public ModuleType ModuleType { get; private set; }
        public bool IsEarlyModule { get; private set; }
        public string FailReason { get; private set; }
        public bool Succeed { get; private set; }
        public string ModuleName { get; private set; }
        public bool OutOfProcess { get; private set; }
        public ModuleInfosViewModel Parent { get; private set; } 
        public override string ToString()
        {
            return TypeName;
        }
        private IEnumerable<ModuleInfoViewModel> dependOnModules;
        public IEnumerable<ModuleInfoViewModel> DependOnModules
        {
            get
            {
                if (dependOnModules == null)
                {
                    if (dependsOnModuleNames != null && dependsOnModuleNames.Count > 0)
                    {
                        dependOnModules = Parent.Modules.Where(m_ => dependsOnModuleNames.Contains(m_.ModuleName)); 
                    }
                    else
                    {
                        dependOnModules = new List<ModuleInfoViewModel>();
                    }
                }
                return dependOnModules;
            }
        }

        private IEnumerable<ModuleInfoViewModel> dependedByModules; 
        public IEnumerable<ModuleInfoViewModel> DependedByModules
        {
            get
            {
                if (dependedByModules == null)
                {
                    dependedByModules = Parent.Modules.Where(m_ => m_.dependsOnModuleNames != null && m_.dependsOnModuleNames.Contains(ModuleName));   

                }
                return dependedByModules;
            }
        }

        private AssemblyInfoViewModel assembly;
        public AssemblyInfoViewModel Assembly
        {
            get
            {
                if (assembly == null && !string.IsNullOrEmpty(AssemblyName))
                {
                    assembly =
                        Parent.ContainerViewModel.GetAssemblyInfos().Assemblies.FirstOrDefault(
                            ass_ => ass_.FullName == AssemblyName);
                }
                return assembly;
            }
        }

        private bool? isAssemblyAssessible;
        public bool IsAssemblyAccessible
        {
           get
           {
               if (isAssemblyAssessible == null)
               {
                   isAssemblyAssessible = Assembly != null;
                   OnPropertyChanged("IsAssemblyAccessible");
               }
               return isAssemblyAssessible.Value;
           }  
             
        }
        public ICommand GoToAssemblyCommand { get; private set; }

        public ICommand ActivateCommand { get; private set; }

        private void GoToAssembly()
        {
            if (Assembly == null)
            {
                return;
            }
            Parent.ContainerViewModel.GetAssemblyInfos().CurrentAssembly = Assembly;
            this.Parent.ContainerViewModel.ShowAssemblies();

        }

        private void Activate()
        {
            this.Parent.CurrentModule = this;
            this.Parent.ContainerViewModel.ShowModules();
        }
    }
}
