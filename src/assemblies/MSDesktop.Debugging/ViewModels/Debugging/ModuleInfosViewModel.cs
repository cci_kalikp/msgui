﻿using System.Collections.Generic;
using System.Linq;
using MorganStanley.MSDotNet.MSGui.Core; 
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;

namespace MSDesktop.Debugging.ViewModels
{
    internal class ModuleInfosViewModel : ViewModelBase
    { 
        private readonly IModuleLoadInfos moduleLoadInfos; 
        public ModuleInfosViewModel(IDebugViewModel containerViewModel_, IModuleLoadInfos moduleLoadInfos_)
        {
            ContainerViewModel = containerViewModel_;
            moduleLoadInfos = moduleLoadInfos_;
            modules = new List<ModuleInfoViewModel>();
            int loadOrder = 0;
            foreach (ModuleLoadInfo info in moduleLoadInfos.Infos)
            {
                modules.Add(new ModuleInfoViewModel(this, info) { Order = loadOrder++ });
            }
            if (modules.Count > 1)
            {
                ModuleType firstModuleType = modules[0].ModuleType;
                ShowModuleType = modules.Any(m_ => m_.ModuleType != firstModuleType);
            }
        }

        public IDebugViewModel ContainerViewModel { get; private set; }
        private readonly List<ModuleInfoViewModel> modules;
        public List<ModuleInfoViewModel> Modules
        {
            get
            {
                return modules;
            }
        }

        private ModuleInfoViewModel currentModule;
        public ModuleInfoViewModel CurrentModule
        {
            get
            {
                return currentModule;
            }
            set
            {
                currentModule = value;
                OnPropertyChanged("CurrentModule");
            }
        }

        private bool showModuleType;
        public bool ShowModuleType
        {
            get { return showModuleType; }
            set
            {
                if (showModuleType != value)
                {
                    showModuleType = value;
                    OnPropertyChanged("ShowModuleType");
                }
            }
        }

        public void ActivateModule(ModuleInfoViewModel moduleInfoViewModel_)
        {
            CurrentModule = moduleInfoViewModel_;
        }

        public override string DisplayName
        {
            get { return "Modules"; }
            protected set
            {
                base.DisplayName = value;
            }
        }
    }
}
