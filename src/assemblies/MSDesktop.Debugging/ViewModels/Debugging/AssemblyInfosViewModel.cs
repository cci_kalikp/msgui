﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;

namespace MSDesktop.Debugging.ViewModels
{
    internal class AssemblyInfosViewModel:ViewModelBase
    { 
        public AssemblyInfosViewModel(IDebugViewModel containerViewModel_)
        {
            ContainerViewModel = containerViewModel_; 
        }

        public IDebugViewModel ContainerViewModel { get; private set; }

        private bool assemblyNotLoaded = true;
        public bool AssemblyNotLoaded
        {
            get
            {
                return assemblyNotLoaded;
            }
            set
            {
                assemblyNotLoaded = value;
                OnPropertyChanged("AssemblyNotLoaded");
            }
        }
        private List<AssemblyInfoViewModel> assemblies;
        public List<AssemblyInfoViewModel> Assemblies
        {
            get
            {
                if (assemblies == null)
                {
                    assemblies = new List<AssemblyInfoViewModel>();
                    foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
                    {
                        assemblies.Add(new AssemblyInfoViewModel(assembly, this));
                    }
                    AssemblyNotLoaded = false;
                }
                return assemblies;
            }
        }

        private AssemblyInfoViewModel currentAssembly;
        public AssemblyInfoViewModel CurrentAssembly
        {
            get
            {
                return currentAssembly;
            }
            set
            {
                currentAssembly = value;
                OnPropertyChanged("CurrentAssembly");
            }
        }

        public override string DisplayName
        {
            get { return "Assemblies"; }
            protected set
            {
                base.DisplayName = value;
            }
        }
    }
}
