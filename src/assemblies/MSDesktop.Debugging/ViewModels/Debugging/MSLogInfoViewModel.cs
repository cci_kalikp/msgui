﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSLog; 
using System.ComponentModel;
using System.Windows.Input;
using Microsoft.Practices.Composite.Presentation.Commands; 

namespace MSDesktop.Debugging.ViewModels
{
    internal class MSLogInfoViewModel : MSLogMessageViewModel, IDataErrorInfo, IShowDialogRequester
    {
        private bool thresholdSet = false;
        public MSLogInfoViewModel()
        { 
            InvokeMessageSenderCommand = new DelegateCommand<object>(new Action<object>(InvokeMessageSender));
            setThresholdCommand = new DelegateCommand<object>(new Action<object>(SetThreshold),
            new Func<object, bool>(_ =>
            { 
                return !thresholdSet && CurrentThresholdPolicy != null;
            }));
            this.Project = "msdesktop";
        }
 

        private MSLogThresholdPolicy currentThresholdPolicy;
        public MSLogThresholdPolicy CurrentThresholdPolicy
        {
            get
            {
                return currentThresholdPolicy;
            }
            set
            {
                if (value != currentThresholdPolicy)
                {
                    currentThresholdPolicy = value;
                    OnPropertyChanged("CurrentThresholdPolicy");
                }
            }
        }
         

        private Dictionary<string, string> errors = new Dictionary<string, string>();
        private string error;
        string IDataErrorInfo.Error
        {

            get
            {
                return error;
            }
        }

        string IDataErrorInfo.this[string columnName_]
        {
            get
            {
                string error;
                errors.TryGetValue(columnName_, out error);
                return error;
            }
        }

        private DelegateCommand<object> setThresholdCommand;
        public ICommand SetThresholdCommand
        {
            get
            {
                return setThresholdCommand;
            }
        }

        public ICommand InvokeMessageSenderCommand
        {
            get;
            private set;
        }
        protected override void OnPropertyChanged(string propertyName)
        {
            if (propertyName == "Meta")
            {
                if (string.IsNullOrEmpty(Meta))
                {
                    Meta = "*";
                }
                UpdatePriority();
            }
            else if (propertyName == "Project")
            {
                if (string.IsNullOrEmpty(Project))
                {
                    Project = "*";
                }
                UpdatePriority();
            }
            else if (propertyName == "Release")
            {
                if (string.IsNullOrEmpty(Release))
                {
                    Release = "*";
                }
                UpdatePriority();
            }
            else if (propertyName == "CurrentTarget")
            {
                UpdateThreshold();
            }
            else if (propertyName == "CurrentPriority")
            {
                thresholdSet = false;
                if (setThresholdCommand != null)
                {
                    setThresholdCommand.RaiseCanExecuteChanged();
                }
            }
            base.OnPropertyChanged(propertyName);
        }

        private void SetThreshold(object arg)
        {
            if (CurrentThresholdPolicy == null) return;
            CurrentThresholdPolicy.SetLayerThreshold(new MSLogLayer(Meta, Project, Release).ToString(), CurrentPriority);
            thresholdSet = true;
            setThresholdCommand.RaiseCanExecuteChanged();
        }

        private void InvokeMessageSender(object arg)
        {
            RaiseShowDialogRequest(new ShowDialogEventArgs("MSLogTester", new MSLogMessageViewModel()));
        }

        private void UpdateThreshold()
        {
            errors.Clear();
            error = null;
            try
            {
                CurrentThresholdPolicy = MSLog.GetThresholdPolicy(CurrentTarget);
            }
            catch (MSLogInvalidTargetException ex)
            {
                error = errors["CurrentTarget"] = ex.ToString();
            }
            if (CurrentThresholdPolicy == null)
            {
                error = errors["CurrentTarget"] = "No policy found for this target";
                CurrentPriority = MSLogPriority.None;
                thresholdSet = true;
                if (setThresholdCommand != null)
                {
                    setThresholdCommand.RaiseCanExecuteChanged();
                } 
                return;
            }
            UpdatePriority();
        }

        private void UpdatePriority()
        {
            if (CurrentThresholdPolicy == null)
            {
                CurrentPriority = MSLogPriority.None;
                return;
            }
            try
            {
                MSLogLayer layer = new MSLogLayer(Meta, Project, Release);
                CurrentPriority = CurrentThresholdPolicy.GetLayerThreshold(layer);
            }
            catch (Exception ex)
            {
                errors["Meta"] =
                errors["Project"] =
                errors["Release"] =
                error = ex.ToString();
            }
            thresholdSet = true;
            if (setThresholdCommand != null)
            {
                setThresholdCommand.RaiseCanExecuteChanged();
            }
        }


        public event ShowDialogRequestHandler ShowDialogRequestHandler; 

        internal void RaiseShowDialogRequest(ShowDialogEventArgs args_)
        {
            var handler = ShowDialogRequestHandler;
            if (handler != null)
            {
                handler(args_);
            }
        }
    }
}
