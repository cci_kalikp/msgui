﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSDesktop.Debugging.ViewModels
{
    interface IDebugViewModel
    {
        AssemblyInfosViewModel GetAssemblyInfos();

        ModuleInfosViewModel GetModuleInfos();

        void ShowAssemblies();

        void ShowModules();
    }
}
