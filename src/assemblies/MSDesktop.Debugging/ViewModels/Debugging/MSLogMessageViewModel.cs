﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.MSLog;
using System.Windows.Input;
using Microsoft.Practices.Composite.Presentation.Commands;
using System.Windows; 

namespace MSDesktop.Debugging.ViewModels
{
    internal class MSLogMessageViewModel:ViewModelBase
    {
        protected static readonly List<MSLogTarget> availableTargets;

        static MSLogMessageViewModel()
        {
            LogIterator iter = new LogIterator();
            MSLog.TraverseDestinations(MSLogTarget.Default, iter, true); 
            availableTargets = new List<MSLogTarget>(iter.Targets.Values);
            if (!iter.Targets.ContainsKey(MSLogTarget.Default.Path))
            {
                availableTargets.Add(MSLogTarget.Default);
            }
        }

        public MSLogMessageViewModel()
        {
            AvailableTargets = availableTargets;
            this.CurrentTarget = AvailableTargets[0];

            this.AvailablePriorities = new MSLogPriority[]{
                                                              MSLogPriority.All,
                                                              MSLogPriority.Debug,
                                                              MSLogPriority.Info,
                                                              MSLogPriority.Notice,
                                                              MSLogPriority.Warning,
                                                              MSLogPriority.Error,
                                                              MSLogPriority.Critical,
                                                              MSLogPriority.Alert,
                                                              MSLogPriority.Emergency,
                                                              MSLogPriority.None};
            this.CurrentPriority = AvailablePriorities[0];

            this.SendMessageCommand = new DelegateCommand<object>(new Action<object>(SendMessage)); 
            meta = "msdotnet";
            project = "*";
            release = "*";
        } 

         
        public List<MSLogTarget> AvailableTargets
        {
            get;
            private set;
        }

        public IList<MSLogPriority> AvailablePriorities
        {
            get;
            private set;
        }

        private string message;
        public string Message
        {
            get
            {
                return message;
            }
            set
            {
                if (message != value)
                {
                    message = value;
                    OnPropertyChanged("Message");
                }
            }
        }

        private string meta;
        public string Meta
        {
            get
            {
                return meta;
            }
            set
            {
                if (meta != value)
                {
                    meta = value;
                    OnPropertyChanged("Meta");
                }
            }
        }

        private string project;
        public string Project
        {
            get
            {
                return project;
            }
            set
            {
                if (project != value)
                {
                    project = value;
                    OnPropertyChanged("Project");
                }
            }
        }

        private string release;
        public string Release
        {
            get
            {
                return release;
            }
            set
            {
                if (release != value)
                {
                    release = value;
                    OnPropertyChanged("Release");
                }
            }
        }

        private MSLogTarget currentTarget;
        public MSLogTarget CurrentTarget
        {
            get
            {
                return currentTarget;
            }
            set
            {
                if (currentTarget != value)
                {
                    currentTarget = value;
                    OnPropertyChanged("CurrentTarget");
                }
            }
        }
         
        private MSLogPriority currentPriority;
        public MSLogPriority CurrentPriority
        {
            get
            {
                return currentPriority;
            }
            set
            {
                if (value != currentPriority)
                {
                    currentPriority = value; 
                    OnPropertyChanged("CurrentPriority");
                }
            }
        }

        public ICommand SendMessageCommand
        {
            get;
            private set;
        } 

        private void SendMessage(object arg)
        {
            MSLogMessage msg = new MSLogMessage();
            msg.Priority = this.currentPriority;
            msg.Target = this.currentTarget;
            msg.Layer = new MSLogLayer(Meta, Project, Release);
            msg.Location(this.GetType().Name, "SendMessage");
            msg.Message = message;
            msg.Send();
            //enabled for easier debug
            //MSLog.SendDirectly(msg);
        }
         
        private class LogIterator : MSLogDestination.IVisitor
        {
            public readonly Dictionary<string, MSLogTarget> Targets;

            public LogIterator()
            {
                Targets = new Dictionary<string, MSLogTarget>();
            }

            public void Visit(MSLogTarget target_, MSLogDestination dest_)
            { 
                string targetName = target_.Path;
                if (targetName.Length == 0) return;
                if (!Targets.ContainsKey(targetName))
                {
                    Targets.Add(targetName, target_);
                }
                while (targetName.Length > 1)
                {
                    targetName = targetName.Substring(0, targetName.LastIndexOf('/'));
                    if (targetName.Length == 0) break;
                    if (Targets.ContainsKey(targetName)) break;
                    MSLogTarget parentTarget = new MSLogTarget(targetName);
                    if (parentTarget.Valid)
                    {
                        Targets.Add(targetName, parentTarget);
                    }
                }
            }
        }

    }
}
