﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Microsoft.Practices.Composite.Presentation.Commands;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using System.Reflection; 
using System.IO;

namespace MSDesktop.Debugging.ViewModels
{
    internal class AssemblyInfoViewModel:ViewModelBase
    {
        private Assembly currentAssembly;
        private static readonly string microsoftDirectory;
        private static readonly string gacDirectory; 

        static AssemblyInfoViewModel()
        {
            string windowsRoot = Environment.GetEnvironmentVariable("WinDir");
            if (string.IsNullOrEmpty(windowsRoot))
            {
                windowsRoot = @"C:\Windows";
            }
            windowsRoot = windowsRoot.ToLower();
            microsoftDirectory = Path.Combine(windowsRoot, "microsoft.net");
            gacDirectory = Path.Combine(windowsRoot, @"assembly\gac");
        }

        public AssemblyInfoViewModel(Assembly assembly_, AssemblyInfosViewModel parent_)
        {
            Parent = parent_;
            this.currentAssembly = assembly_;
            this.Name = currentAssembly.GetName().Name;
            this.FullName = currentAssembly.FullName;
            if (!currentAssembly.IsDynamic)
            {
                try
                {
                    this.Location = currentAssembly.Location;
                }
                catch
                {
                }
            }
            CalculateProperties();

            if (string.IsNullOrEmpty(FullName))
            {
                Modules = new List<ModuleInfoViewModel>();
            }
            else
            {
                Modules = Parent.ContainerViewModel.GetModuleInfos().Modules.FindAll(m_ => m_.AssemblyName == this.FullName);
            }
            HasModules = Modules.Count > 0;
            ShowModulesCommand = new DelegateCommand<object>(o_ => ShowModules = true);
        }
         

        public AssemblyInfosViewModel Parent { get; private set; }
        private bool referenceByAssemblyNotLoaded = true;
        public bool ReferenceByAssembliesNotLoaded
        {
            get
            {
                return referenceByAssemblyNotLoaded;
            }
            set
            {
                referenceByAssemblyNotLoaded = value;
                OnPropertyChanged("ReferenceByAssembliesNotLoaded");
            }
        }

        private IEnumerable<AssemblyInfoViewModel> referencedByAssemblies;
        //use async binding to improve the performance
        public IEnumerable<AssemblyInfoViewModel> ReferencedByAssemblies
        {
            get
            {
                if (referencedByAssemblies == null && currentAssembly != null)
                {
                    referencedByAssemblies = from assembly in Parent.Assemblies
                                     where assembly.currentAssembly != currentAssembly &&
                                           assembly.DependOnAssemblies.Any(assemblyName_ => assemblyName_.Name == this.Name)
                                     select assembly;
                    ReferenceByAssembliesNotLoaded = false;

                }
                return referencedByAssemblies;
            }
        }

        private bool dependOnAssembliesNotLoaded = true;
        public bool DependOnAssembliesNotLoaded
        {
            get
            {
                return dependOnAssembliesNotLoaded;
            }
            set
            {
                dependOnAssembliesNotLoaded = value;
                OnPropertyChanged("DependOnAssembliesNotLoaded");
            }
        }

        private IList<AssemblyInfoViewModel> dependOnAssemblies;
        public IList<AssemblyInfoViewModel> DependOnAssemblies
        {
            get
            {
                if (dependOnAssemblies == null && currentAssembly != null)
                {
                    dependOnAssemblies = new List<AssemblyInfoViewModel>();
                    foreach (AssemblyName assemblyName in currentAssembly.GetReferencedAssemblies())
                    {
                        foreach (AssemblyInfoViewModel assemblyInfoViewModel in Parent.Assemblies)
                        {
                            if (assemblyInfoViewModel.FullName == assemblyName.FullName)
                            {
                                dependOnAssemblies.Add(assemblyInfoViewModel);
                                break; 
                            }
                        }
                    }
                    DependOnAssembliesNotLoaded = false;
                }
                return dependOnAssemblies;
            }
         }
         
        public List<ModuleInfoViewModel> Modules { get; private set; }

        public bool HasModules { get; private set; }
 
        public string Name { get; private set; }
        public string FullName { get; private set; } 
        public string Location { get; private set; }

        public string ImageRuntimeVersion { get; private set; }

        public AssemblyCategory Category { get; private set; }
        public enum AssemblyCategory
        {
            Core = 0,
            GAC = 1,
            Dynamic = 2,
            Production = 3,
            Unknown = 4
        }


        public ICommand ShowModulesCommand { get; private set; }

        private bool showModules;
        public bool ShowModules
        {
            get { return showModules; }
            set
            {
                //if (showModules != value)
                {
                    showModules = value;
                    OnPropertyChanged("ShowModules");
                }
            }
        }
        private void CalculateProperties()
        {
            Category = AssemblyCategory.Production;
            if (string.IsNullOrEmpty(Location))
            {
                // it's a dynamic module for which the Location property is not supported - e.g. Regex
                Category = AssemblyCategory.Dynamic;
                return;
            } 

            string location = this.Location.ToLower();
            string fullName = this.FullName.ToLower();
            if (location.StartsWith(microsoftDirectory) ||
                ((fullName.StartsWith("system") || fullName.StartsWith("accessibility")) &&
                  location.StartsWith(gacDirectory)))
            { 
                // it's a .NET core assembly
                Category = AssemblyCategory.Core;
            }
            else if (location.StartsWith(gacDirectory))
            {
                // it's loaded from the GAC but it's not a .NET core assembly
                Category = AssemblyCategory.GAC;
            } 
            else if (!location.StartsWith(@"\\san01b\DevAppsGML\dist\"))
            {
                // it's none of the above and it ain't running out of production!
                Category = AssemblyCategory.Unknown;
            }

            this.ImageRuntimeVersion = currentAssembly.ImageRuntimeVersion;
            
        }

    }
}
