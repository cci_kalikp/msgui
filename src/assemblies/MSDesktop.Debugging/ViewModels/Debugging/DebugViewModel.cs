﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core;
using MSDesktop.ModuleEntitlements;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;

namespace MSDesktop.Debugging.ViewModels
{
    internal class DebugViewModel:ViewModelBase, IDebugViewModel
    {
        public DebugViewModel(IModuleLoadInfos moduleLoadInfos_)
        {
            ModuleInfos = new ModuleInfosViewModel(this, moduleLoadInfos_);
            AssemblyInfos = new AssemblyInfosViewModel(this);
            MSLogInfo = new MSLogInfoViewModel();
        }

        public AssemblyInfosViewModel AssemblyInfos { get; private set; }

        public ModuleInfosViewModel ModuleInfos { get; private set; }

        public MSLogInfoViewModel MSLogInfo { get; private set; }

        private int selectedViewIndex;
        public int SelectedViewIndex
        {
            get { return selectedViewIndex; }
            set
            {
                if (selectedViewIndex != value)
                {
                    selectedViewIndex = value;
                    OnPropertyChanged("SelectedViewIndex");
                }
            }
        }


        public void ShowAssemblies()
        {
            this.SelectedViewIndex = 0;
        }

        public void ShowModules()
        { 
            this.SelectedViewIndex = 1;
        }

        public AssemblyInfosViewModel GetAssemblyInfos()
        {
            return AssemblyInfos;
        }

        public ModuleInfosViewModel GetModuleInfos()
        {
            return ModuleInfos;
        }
    }
}
