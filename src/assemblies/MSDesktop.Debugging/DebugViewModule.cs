﻿using System; 
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using System.Xml.Linq; 
using MSDesktop.Debugging.ViewModels;
using System.Windows.Media.Imaging;
using System.Windows; 
using MorganStanley.MSDotNet.MSGui.Core; 
using Application = System.Windows.Application;

namespace MSDesktop.Debugging
{
    public class DebugViewModule : IMSDesktopModule
    {
        private readonly IChromeRegistry chromeRegistry;
        private readonly IChromeManager chromeManager;
        private readonly IModuleLoadInfos moduleLoadInfos;
        private const string ButtonId = "DebugViewerButton";
        private const string ViewId = "DebugViewerWindow";
        private readonly IUnityContainer container;
        private readonly IApplication application;
        private bool dashboardRegistered = false;
        public DebugViewModule(IChromeManager chromeManager_, IChromeRegistry chromeRegistry_, IModuleLoadInfos moduleLoadInfos_, IUnityContainer container_, IApplication application_)
		{
            DebuggingExtensions.ChromeManager = chromeManager = chromeManager_; 
            DebuggingExtensions.ChromeRegistry = chromeRegistry = chromeRegistry_;
            moduleLoadInfos = moduleLoadInfos_;
            application = application_;
            application.ApplicationLoaded += application_ApplicationLoaded;
            container = container_;
		}

        void application_ApplicationLoaded(object sender, EventArgs e)
        {
            RegisterToDashboard();
            ((MorganStanley.MSDotNet.MSGui.Impl.Application.Application) application).ThemeChanged +=
                (sender_, args_) => RegisterToDashboard();
        }

        private void RegisterToDashboard()
        {
            if (dashboardRegistered) return;
            var model = DebuggingExtensions.EnsureDiagnosticsView(container);
            if (model == null) return;
            model.Models.Add(new AssemblyInfosViewModel(model));
            model.Models.Add(new ModuleInfosViewModel(model, moduleLoadInfos));
            dashboardRegistered = true;
        }
        public void Initialize()
        { 
            
            chromeManager.AddWidget(ButtonId, new InitialShowWindowButtonParameters()
            {
                Text = "Assembly Viewer",
                WindowFactoryID = ViewId,
                Image = new BitmapImage(new Uri("/MSDesktop.Debugging;component/Resources/AssemblyViewer.png", UriKind.Relative)),
                InitialParameters = new InitialWindowParameters()
                    {
                               Height = 800,
                               Width = 1000,
                               SizingMethod = SizingMethod.Custom,
                               Singleton = true 
                    }
            }, chromeManager.GetDebuggingButtonContainer());
            chromeRegistry.RegisterWindowFactory(ViewId, new InitializeWindowHandler(CreateView));
             
        }

        private bool CreateView(IWindowViewContainer viewContainer_, XDocument state_)
        {
            FrameworkElement view = Application.LoadComponent(new Uri("/MSDesktop.Debugging;component/Views/Debugging/DebugView.xaml", UriKind.Relative)) as FrameworkElement;
            view.DataContext = new DebugViewModel(moduleLoadInfos);
            viewContainer_.Icon = MSDesktop.Debugging.Properties.Resources.AssemblyViewer;
            viewContainer_.Content = view;
            viewContainer_.Title = "Assembly Viewer"; 
            return true;
        } 
         
    }
}
