﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using System.Reactive.Linq;
using System.Threading.Tasks;

namespace MSDesktop.Debugging.Models
{
    abstract class LogReaderBase:ILogReader
    {

        private string logFile;

        protected LogReaderBase() { }
        protected LogReaderBase(string logPath_)
        {
            this.logFile = logPath_;
        }
        public abstract LogType LogType
        {
            get;
        }

        public virtual int LogVersion
        {
            get { return 1; }
        }

        public event EventHandler<EventArgs> LogVersionResolved;
        protected virtual void OnLogVersionResolved()
        {
            var handler = LogVersionResolved;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }
        public ILogEntries Get(string logFile_, int entryLimit_ = 0)
        {
            using (var stream = new FileStream(logFile_, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                using (var reader = new StreamReader(stream, Encoding.UTF8))
                {
                    var header = ProcessHeader(reader, null, null);  
                    var entries = new List<ILogEntry>();
                    var limited = ProcessEntries(reader,
                                   null,
                                   null,
                                   entry_ =>
                                   {
                                       entries.Add(entry_);
                                       return entryLimit_ <= 0 || entry_.EntryNumber < entryLimit_;
                                   },
                                   null);

                    return new LogEntries(header, entries, limited);
                }
            }
        }

        public IObservable<LogEntryResource> GetObservable(string logFile_=null, uint entryLimit_ = 0, CancellationTokenSource tokenSource_=null)
        {
            if (logFile_ == null) logFile_ = logFile;
            if (string.IsNullOrEmpty(logFile))
            {
                throw new ArgumentNullException(logFile);
            }
            return Observable.Create<LogEntryResource>(observer =>
            {
                var fileChangedEvent = new AutoResetEvent(false);
                var tokenSource = tokenSource_ ?? new CancellationTokenSource();
                Task.Factory.StartNew(() =>
                {
                    try
                    {
                        using (var stream = new FileStream(logFile_, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                        {
                            using (var reader = new StreamReader(stream, Encoding.UTF8))
                            {
                                using (var watcher = new FileSystemWatcher(Path.GetDirectoryName(logFile_), (Path.GetFileName(logFile_))))
                                {
                                    var info = new FileInfo(logFile_);

                                    //Using this timer is a bit of hack.  
                                    //Windows7 only flushes the file cache to disk when someone reads from the file.
                                    //By refreshing the file info we're causing that to occur.  
                                    //Don't want to do this too frequently otherwise there may be a perf issue.
                                    //This is a win7 specific issue.  XP and win8 don't have it
                                    using (new Timer(state => info.Refresh(),
                                                     null,
                                                     TimeSpan.FromSeconds(15),
                                                     TimeSpan.FromSeconds(15)))
                                    {



                                        watcher.Changed += (s, e) => fileChangedEvent.Set();
                                        watcher.NotifyFilter = NotifyFilters.Size;
                                        watcher.EnableRaisingEvents = true;


                                        var header = ProcessHeader(reader, tokenSource.Token.WaitHandle, fileChangedEvent); 

                                        observer.OnNext(new LogEntryResource(header));

                                        try
                                        {
                                            int tailingTimeCount = 0;
                                            ProcessEntries(reader,
                                                           tokenSource.Token.WaitHandle,
                                                           fileChangedEvent,
                                                           entry =>
                                                           {
                                                               if (entryLimit_ > 0 && entry.EntryNumber >= entryLimit_)
                                                               {
                                                                   observer.OnNext(new LogEntryResource(entry, true));
                                                               }
                                                               else
                                                               {
                                                                   observer.OnNext(new LogEntryResource(entry, false));
                                                               }

                                                               //if entry limit reaches, end the reading process
                                                               return !tokenSource.IsCancellationRequested && 
                                                                   (entryLimit_ <= 0 || entry.EntryNumber < entryLimit_);
                                                           },
                                                           () => observer.OnNext(new LogEntryResource(++tailingTimeCount)));

                                            observer.OnCompleted();
                                        }
                                        catch (Exception ex)
                                        {
                                            observer.OnError(ex);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        observer.OnError(ex);
                    }
                }, tokenSource.Token, TaskCreationOptions.LongRunning, TaskScheduler.Default);

                return tokenSource.Cancel;
            });
        }

        protected bool ProcessEntries(StreamReader reader_, WaitHandle cancelledEvent_, WaitHandle fileChangedEvent_,
    Func<ILogEntry, bool> entryAdded_, Action atEof_)
        {
            uint entryNumber = 0;

            var logEntry = FetchLogEntry(reader_, cancelledEvent_, fileChangedEvent_, atEof_);
            while (logEntry != null)
            {
                var item = ParseEntry(logEntry);
                if (item == null)
                {
                    return false;
                } 
                item.EntryNumber = ++entryNumber;
                
                if (!entryAdded_(item))
                {
                    return false;
                }
                 
                logEntry = FetchLogEntry(reader_, cancelledEvent_, fileChangedEvent_, atEof_);
            }

            return true;
        }

        protected abstract string ProcessHeader(StreamReader reader_, WaitHandle cancelledEvent_, WaitHandle fileChangedEvent_);

        protected abstract string FetchLogEntry(StreamReader reader_, WaitHandle cancelledEvent_, WaitHandle fileChangedEvent_, Action atEoF_);

        protected abstract ILogEntry ParseEntry(string logEntry_); 

    }

    class LogEntries : ILogEntries
    {
        public LogEntries(string header_, IEnumerable<ILogEntry> entries_, bool entriesLimited_)
        {
            Header = header_;
            Entries = entries_;
            EntriesLimited = entriesLimited_;
        }

        public IEnumerable<ILogEntry> Entries { get; private set; }
        public bool EntriesLimited { get; private set; }
        public string Header { get; private set; }
    }
}
