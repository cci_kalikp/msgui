﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSDesktop.Debugging.Models
{
    class Log4NetLogEntry : LogEntryBase
    {
 
        #region Extra Properties
        public string ProcessId { get; set; } 
        public string ProcessName { get; set; }
        public Log4NetLogLevel Threshold { get; set; }
        public string SourceFile { get; set; } 
        public int SourceLineNumber { get; set; }
        #endregion 

        protected override DateTime ParseDate(string dateString)
        {
            return DateTime.ParseExact(dateString, "yyyy-MM-dd HH:mm:ss", null);

        }
    }

    public enum Log4NetLogLevel
    {
        Debug = 1,
        Info = 2,
        Warn = 3,
        Error = 4,
        Fatal = 5,
        ParseError = int.MaxValue,
    }
}
