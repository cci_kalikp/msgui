﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;

namespace MSDesktop.Debugging.Models
{
    class Log4NetLogReader: LogReaderBase
    {
        public override LogType LogType
        {
            get
            {
                return Models.LogType.Log4Net;
            }
        }

       public Log4NetLogReader() { }

       public Log4NetLogReader(string logFile_) : base(logFile_) { }

       public override int LogVersion
       {
           get
           {
               return version;
           }
       }
        private int version = 1;
        protected override string ProcessHeader(StreamReader reader_, WaitHandle cancelledEvent_, WaitHandle fileChangedEvent_)
        {
            var header = FetchEntry(reader_, cancelledEvent_, fileChangedEvent_, null, '#', null);
            if (header == null)
            {
                return null;
            }

            header = header.TrimStart('#').Replace("\n#", "\n").Replace("\n ", "\n").Trim();
            string versionString = GetLogVersion(header);
            if (!string.IsNullOrWhiteSpace(versionString))
            {
                double doubleVersion;
                if (!double.TryParse(versionString, out doubleVersion))
                {
                    throw new InvalidOperationException("Invalid log file version");
                }

                version = (int)doubleVersion;
                OnLogVersionResolved();
            }

            return header;
        }

        private static string GetLogVersion(string header_)
        {
            if (header_ == null)
            {
                return null;
            }

            var startIndex = header_.IndexOf("Log Version:");
            if (startIndex < 0)
            {
                return null;
            }

            startIndex += 12;

            var endIndex = header_.IndexOf("\n", startIndex);
            if (endIndex < 0)
            {
                return null;
            }

            return header_.Substring(startIndex, endIndex - startIndex).Trim();
        }

        protected override string FetchLogEntry(StreamReader reader_, WaitHandle cancelledEvent_, WaitHandle fileChangedEvent_, Action atEoF_)
        {
            return FetchEntry(reader_, cancelledEvent_, fileChangedEvent_, atEoF_, null, "ǁ");
        }
         
        protected override ILogEntry ParseEntry(string logEntry_)
        {
            var item = new Log4NetLogEntry();
            try
            {
                //We've got a log entry... so lets decode it!
                //The format is (for version 1):
                //<yyyy-MM-dd HH:mm:ss> <Log Level> ["<Managed Thread ID>"] ["<Logger/Class Name>"] ["<Member Name (optional)>"] ["<Source File (optional)>"] ["<Source Line Number (optional)>"] <message>ǁ
                //The format is (for version 2):
                //<yyyy-MM-dd HH:mm:ss> <Log Level> ["<Process Name>"] ["<Process ID>"] ["<Managed Thread ID>"] ["<Logger/Class Name>"] ["<Member Name (optional)>"] ["<Source File (optional)>"] ["<Source Line Number (optional)>"] <message>ǁ

                item.DateString = logEntry_.Substring(0, 19); 
                item.Time = item.DateString.Substring(11, 8);

                var index = logEntry_.IndexOf(' ', 20);
                item.Threshold = (Log4NetLogLevel)Enum.Parse(typeof(Log4NetLogLevel), logEntry_.Substring(20, index - 20), true);

                var position = SkipSpaces(logEntry_, index);

                item.ProcessName = version >= 2 ? GetBracketedEntry(logEntry_, ref position, false) : "";
                item.ProcessId = version >= 2 ? GetBracketedEntry(logEntry_, ref position, false) : ""; 
                item.Thread = GetBracketedEntry(logEntry_, ref position, false);
                item.Class = GetBracketedEntry(logEntry_, ref position, false);
                item.Method = GetBracketedEntry(logEntry_, ref position, true);
                item.SourceFile = GetBracketedEntry(logEntry_, ref position, true);


                var lineNumber = GetBracketedEntry(logEntry_, ref position, true);
                if (lineNumber != null)
                {
                    item.SourceLineNumber = int.Parse(lineNumber);
                } 
                item.Details = logEntry_.Substring(position).Trim('ǁ', ' ');
            }
            catch (Exception ex)
            {
                item.Threshold = Log4NetLogLevel.ParseError;
                if (!string.IsNullOrWhiteSpace(item.Details))
                {
                    item.Details =
                        string.Format(
                            "A Parse Failure occurred while processing this log entry:{0}{1}{0}{0}Original Log Entry Details:{0}{2}",
                            Environment.NewLine, ex, item.Details);
                }
                else
                {
                    item.Details = string.Format("A Parse Failure occurred while processing this log entry:{0}{1}", Environment.NewLine, ex);
                }
            }
            return item;
        }


        private static string FetchEntry(StreamReader reader, WaitHandle cancelledEvent, WaitHandle fileChangedEvent, Action atEoF, char? entryStartCharacter, string entryTerminationCharacter)
        {
            StringBuilder logEntrySb = null;
            string logEntry = string.Empty;
            while (true)
            {
                string line = null;
                if (entryStartCharacter != null)
                {
                    var characterInt = reader.Peek();
                    if (characterInt >= 0)
                    {
                        if ((char)characterInt != (char)entryStartCharacter)
                        {
                            return logEntrySb == null ? logEntry : logEntrySb.ToString();
                        }
                        line = reader.ReadLine();
                    }
                }
                else
                {
                    line = reader.ReadLine();
                }

                //If the line was null then we're at the end of the file.  
                //We need to wait to be notified that the file has changed
                while (line == null)
                {
                    if (cancelledEvent == null && fileChangedEvent == null)
                    {
                        return null;
                    }
                    if (atEoF != null)
                    {
                        atEoF();
                    }

                    if (cancelledEvent == null)
                    {
                        fileChangedEvent.WaitOne();
                    }
                    else if (WaitHandle.WaitAny(new[] { cancelledEvent, fileChangedEvent }) == 0)
                    {
                        //The cancellation token returned so exit
                        return null;
                    }
                    line = reader.ReadLine();
                }

                if (logEntry.Length > 0)
                {
                    if (logEntrySb == null)
                    {
                        logEntrySb = new StringBuilder(logEntry);
                    }
                    logEntrySb.AppendFormat("\n{0}", line);
                }
                else
                {
                    logEntry = line;
                }

                //We now have a line. Is this the completion of an entry or are there more lines that make it up?
                if (entryTerminationCharacter != null && line.EndsWith(entryTerminationCharacter))
                {
                    //End of the log entry
                    return logEntrySb == null ? logEntry: logEntrySb.ToString();
                }
            }
        }

        private static string GetBracketedEntry(string text_, ref int position_, bool isOptional_)
        {
            if (!isOptional_ || text_[position_] == '[' && text_[position_ + 1] == '"')
            {
                position_ += 2;

                var index = text_.IndexOf("\"]", position_);
                var entry = text_.Substring(position_, index - position_).Trim();

                position_ = SkipSpaces(text_, index + 2);

                return entry;
            }

            return null;
        }

        private static int SkipSpaces(string text_, int position_)
        {
            while (text_[position_] == ' ')
            {
                position_++;
            }

            return position_;
        }

    }
}
