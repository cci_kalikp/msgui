﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSDesktop.Debugging.Models
{
    public class LogEntryResource
    {
        private readonly bool _limitReached;
        private readonly ILogEntry _entry;
        private readonly string _header;
        private readonly int _tailTimeCount;

        public LogEntryResource(ILogEntry entry, bool limitReached)
        {
            _entry = entry;
            _limitReached = limitReached;
        }

        public LogEntryResource(string header)
        {
            _header = header;
        }

        public LogEntryResource(int tailingTimeCount_)
        {
            _tailTimeCount = tailingTimeCount_;
        }

        public bool LimitReached { get { return _limitReached; } }
        public ILogEntry Entry { get { return _entry; } }
        public string Header { get { return _header; } }
        public int TailTimeCount { get { return _tailTimeCount; } } 
    }
}
