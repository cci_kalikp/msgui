﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSDesktop.Debugging.Models
{
    public interface ILogEntries
    {
        string Header { get; }
        IEnumerable<ILogEntry> Entries { get; }
        bool EntriesLimited { get; }
    }
}
