﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.ComponentModel;
using MSDesktop.Debugging.Extensions;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;

namespace MSDesktop.Debugging.Models
{
    public abstract class LogEntryBase : ViewModelBase, ILogEntry
    {
        public uint EntryNumber { get; set; }

        private DateTime? date;
        public DateTime Date
        {
            get
            {
                if (date != null) return date.Value;
                date = ParseDate(DateString);
                return date.Value;
            }
        }

        protected abstract DateTime ParseDate(string dateString);

        public string DateString { get; set; }
        public string Time { get; set; }

        public string Thread { get; set; }

        public string Class { get; set; }

        public string Method { get; set; }

        public string Details { get; set; }

        private bool marked = false;
        public bool Marked
        {
            get
            {
                return marked;
            }
            set
            {
                if (marked != value)
                {
                    marked = value;
                    OnPropertyChanged("Marked");
                }
            }
        }

        public string GetCharacteristic()
        {
            string compareTo = this.Details;
            if (string.IsNullOrWhiteSpace(compareTo)) return null;
            int endIndex = compareTo.IndexOf('\n');
            if (endIndex > -1)
            {
                endIndex = Math.Min(Math.Min(30, compareTo.Length), endIndex);
            }
            else
            {
                endIndex = Math.Min(50, compareTo.Length);
            }
            compareTo = compareTo.Substring(0, endIndex);
            return compareTo;
        }

        public bool IsSimilarTo(string textToCompare_)
        {
            string characteristic = GetCharacteristic();
            if (characteristic == null) return false;
            double distance = Util.ComputeLevenshteinDistance(textToCompare_, characteristic);
            distance = 1.0F - distance / Math.Max(textToCompare_.Length, characteristic.Length);
            return (distance > .7);
        }
 
    }
}
