﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;

namespace MSDesktop.Debugging.Models
{
    public static class LogReaderFactory
    { 
        public static ILogReader GetReader(string logFile_)
        {
            if (string.IsNullOrEmpty(logFile_))
            {
                throw new ArgumentNullException("logFile_");
            }
            int timeout = 1000;
            while (true)
            {
                try
                {

                    using (var stream = new FileStream(logFile_, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {
                        using (var reader = new StreamReader(stream, Encoding.UTF8))
                        {
                            string firstLine = reader.ReadLine();
                            LogType logType = GetLogType(firstLine);
                            return GetReaderByType(logType, logFile_);
                        }
                    }
                } 
                catch (IOException)
                {
                    if (timeout <= 0) throw;
                }
                Thread.Sleep(200);
                timeout -= 200;
            }
             
        } 

        private static LogType GetLogType(string firstLine_)
        {
            if (firstLine_ == null) return LogType.Unknown;
            if (firstLine_.StartsWith("# MSLog log file")) return LogType.MSLog;
            if (firstLine_.StartsWith("# Log4Net Log File")) return LogType.Log4Net;
            return LogType.Unknown;
        }

        private static ILogReader GetReaderByType(LogType logType_, string logFilePath_)
        {
            switch (logType_)
            {
                case LogType.MSLog:
                    return new MSLogReader(logFilePath_);
                case LogType.Log4Net:
                    return new Log4NetLogReader(logFilePath_);
                case LogType.Unknown:
                default:
                    return new MSLogReader(logFilePath_);
            }
        }
    }
}
