﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace MSDesktop.Debugging.Models
{
    public interface ILogReader
    {
        ILogEntries Get(string logFile, int entryLimit_ = 0);
        IObservable<LogEntryResource> GetObservable(string logFile_ = null, uint entryLimit_ = 0, CancellationTokenSource tokenSource_ = null);
        LogType LogType { get; }
        int LogVersion { get; }
        event EventHandler<EventArgs> LogVersionResolved;

    }

    public enum LogType
    {
        MSLog = 0,
        Log4Net = 1,
        Unknown = Int32.MaxValue
    }
}
