﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace MSDesktop.Debugging.Models
{
    class MSLogEntry:LogEntryBase
    {
         
        #region Extra Properties
        public MSLogLevel Threshold { get; set; } 
        public string Client { get; set; }
        public string Project { get; set; }
        #endregion
        private const string DATE_FORMAT = "MMM dd HH:mm:ss";
        private const string DATE_FORMAT2 = "MMM dd HH:mm:ss:fff";
        private const string DATE_FORMAT3 = "MMM dd HH:mm:ss:ff";
        private const string DATE_FORMAT4 = "MMM dd HH:mm:ss:f";

        protected override DateTime ParseDate(string dateString)
        {
            return DateTime.ParseExact(dateString, new[] {DATE_FORMAT, DATE_FORMAT2, DATE_FORMAT3, DATE_FORMAT4}, null, DateTimeStyles.None);
            
        }
    }

    /// <summary>
    /// Redefining this here because the way it is done in MorganStanley.MSDotNet.MSLog.MSLogPriority
    /// is way too confusing.
    /// </summary>
    public enum MSLogLevel
    {
        Clear = -2,
        None = -1,
        All = 0,
        Debug = 1,
        Info = 2,
        Notice = 3,
        Warning = 4,
        Error = 5,
        Critical = 6,
        Alert = 7,
        Emergency = 8
    }
}
