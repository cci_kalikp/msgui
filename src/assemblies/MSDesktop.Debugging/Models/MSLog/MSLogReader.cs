﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using System.Text.RegularExpressions;

namespace MSDesktop.Debugging.Models
{
    class MSLogReader:LogReaderBase
    { 
        private const string LogEntryStartRegex =
 @"
        (?<date>\w\w\w\s\d\d\s(?<time>\d\d:\d\d:\d\d(:\d+)?))
      \s
        (?<computer>([\S])+)
      (\s[\S]+)?
      \s+
        \[
          (?>
            \d+
            (,
              (?<thread>              # http://blog.stevenlevithan.com/archives/balancing-groups
                ( 
                  [^\[\]]+
                  |
                  \[(?<Depth>)
                  |
                  \](?<-Depth>)
                )*
                (?(Depth)(?!))
              )
            )?
          )
        \]
      \s
        (?<number>\d+)
      \s
        (?<threshold>\w+)
      \s 
      ";
        private static readonly Regex EntryStartRegex = new Regex(LogEntryStartRegex, RegexOptions.Compiled | RegexOptions.IgnorePatternWhitespace);
        private static readonly Regex ProjectRegex = new Regex(@"[^/\s]+/[^/\s]+/[^/\s]+\s", RegexOptions.Compiled);
        private static readonly Regex DetailsRegex = new Regex(@"\([\w`\<\>]+\.[^\)]*\)\s*$", RegexOptions.Compiled);
        private static readonly Regex ThreadRegex = new Regex(@"Thread #(\d+)", RegexOptions.Compiled);

        public override LogType LogType
        {
            get
            {
                return Models.LogType.MSLog;
            }
        }

        public MSLogReader() { }

        public MSLogReader(string logFile_) : base(logFile_) { }

        protected override string ProcessHeader(StreamReader reader_, WaitHandle cancelledEvent_, WaitHandle fileChangedEvent_)
        {
            int firstChar = reader_.Peek();
            if (firstChar > 0 && (char)firstChar == '#')
            {
                return reader_.ReadLine().Substring(1);
            }
            return null;
        }
         
        protected override string FetchLogEntry(StreamReader reader_, WaitHandle cancelledEvent_, WaitHandle fileChangedEvent_, Action atEoF_)
        {
            StringBuilder logEntrySb = null;
            string logEntry = string.Empty;
            string line = null;
            while (true)
            { 
                //If the line was null then we're at the end of the file.  
                //We need to wait to be notified that the file has changed
                while ((line = ReadNextLine(reader_)) == null)
                {
                    if (fileChangedEvent_ == null && cancelledEvent_ == null)
                    {
                        return null;
                    }
                    if (atEoF_ != null)
                    {
                        atEoF_();
                    }

                    if (cancelledEvent_ == null)
                    {
                        fileChangedEvent_.WaitOne();
                    }
                    else if (WaitHandle.WaitAny(new[] {cancelledEvent_, fileChangedEvent_}) == 0)
                    {
                         //The cancellation token returned so exit
                         return null; 
                    }
                }

                if (logEntry.Length > 0)
                {
                    if (logEntrySb == null)
                    {
                        logEntrySb = new StringBuilder(logEntry);
                    }
                    logEntrySb.AppendFormat("\n{0}", line);
                }
                else
                {
                    logEntry = line;
                }

                if (IsEndOfLogEntry(reader_))
                {
                    return logEntrySb == null ? logEntry : logEntrySb.ToString();
                }
            } 
        }


        protected override ILogEntry ParseEntry(string logEntry_)
        {
            Match match = currentMatch ?? EntryStartRegex.Match(logEntry_);
            if (!match.Success) return null;
            MSLogEntry entry = new MSLogEntry();
            entry.DateString = match.Groups["date"].Value;
            entry.Time = match.Groups["time"].Value;
            entry.Client = match.Groups["computer"].Value;
            entry.Thread = match.Groups["thread"].Value;
            entry.Threshold = (MSLogLevel)Enum.Parse(typeof(MSLogLevel), match.Groups["threshold"].Value, true); 
            string detailText = logEntry_.Substring(match.Length);
            Match projectMatch = ProjectRegex.Match(detailText);
            if (projectMatch.Success)
            {
                entry.Project = projectMatch.Value;
                detailText = detailText.Substring(projectMatch.Length);
            }
            SeparateClassAndMethod(detailText, entry);
            return entry;
        }


        #region Helper Methods
        private string lastLineRead = null;
        private Match currentMatch = null;
        private Match nextMatch = null;
        private string PeekNextLine(StreamReader reader_)
        {
            if (reader_.EndOfStream)
            {
                return null;
            }
            else
            {
                lastLineRead = reader_.ReadLine();
                return lastLineRead;
            }
        }

        private string ReadNextLine(StreamReader reader_)
        {
            if (lastLineRead != null)
            {
                //this is the last line, use the line peeked last time
                //and set the lastLineRead to null, so when ReadNextLine is hit again,
                //if no new lines comes, it would return null
                if (reader_.EndOfStream)
                {
                    string copy = lastLineRead;
                    lastLineRead = null;
                    return copy;
                }
                return lastLineRead;
            }
            string nextLine = reader_.ReadLine();
            //skip empty lines
            while (nextLine == string.Empty && !reader_.EndOfStream)
            {
                nextLine = reader_.ReadLine();
            }
            return nextLine;
        } 

        private bool IsEndOfLogEntry(StreamReader reader_)
        {
            currentMatch = nextMatch;
            string nextLine = PeekNextLine(reader_);
            if (nextLine == null)
            {
                nextMatch = null;
                return true;
            }
            nextMatch = EntryStartRegex.Match(nextLine);
            if (nextMatch.Success) return true;
            nextMatch = null; 
            return false;
        }

        private static void SeparateClassAndMethod(string remainder_,  MSLogEntry entry_)
        {
            Match detailMatch = DetailsRegex.Match(remainder_); 
            if (!detailMatch.Success)
            {
                entry_.Details = remainder_;
                return;
            }
            if (!string.IsNullOrEmpty(entry_.Thread))
            {
                Match threadMatch = ThreadRegex.Match(remainder_);
                if (threadMatch.Success)
                {
                    entry_.Thread = threadMatch.Value;
                }
            }
            var capture = detailMatch.Captures[0]; 
            string s = capture.Value;
            int separator = s.LastIndexOf('.');
            string className = s.Substring(1, separator - 1);
            if (className != "noclass")
            {
                entry_.Class = className;
            }
            if (separator < s.Length - 1)
            {
                entry_.Method = s.Substring(separator + 1, s.Length - separator - 2);
            }
            entry_.Details = remainder_.Remove(capture.Index);
        }
        #endregion
    }
}
