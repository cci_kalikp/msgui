﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSDesktop.Debugging.Models
{
    public interface ILogEntry
    {
        uint EntryNumber { get; set; }
        DateTime Date { get; }
        string Time { get; }
        string Thread { get; } 
        string Class { get; }
        string Method { get; } 
        string Details { get; }
        string GetCharacteristic();
        bool IsSimilarTo(string textToCompare_);
        bool Marked { get; set; }
    }
}
