﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows; 

namespace MSDesktop.Debugging.Extensions
{
    public class InvokeMessageSenderHandler: ShowDialogHandlerBase
    { 
        protected override string ViewId
        {
            get { return "MSLogMessageSender"; }
        } 

        protected override Size DialogSize
        {
            get { return new Size(500, 190); }
        }

        protected override FrameworkElement CreateElement()
        {
            return Application.LoadComponent(new Uri("MSDesktop.Debugging;component/Views/Debugging/SendMSMessageView.xaml", UriKind.Relative)) as FrameworkElement;
        }

    }
}
