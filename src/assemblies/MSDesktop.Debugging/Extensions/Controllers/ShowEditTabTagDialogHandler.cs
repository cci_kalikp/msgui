﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows; 

namespace MSDesktop.Debugging.Extensions
{
    internal class ShowEditTabTagDialogHandler:ShowDialogHandlerBase
    {
        protected override string ViewId
        {
            get { return "EditTabTagView"; }
        }
         
        protected override Size DialogSize
        {
            get { return new Size(300, 130); }
        }

        protected override ResizeMode ResizeMode
        {
            get
            {
                return System.Windows.ResizeMode.NoResize;
            }
        }

        protected override FrameworkElement CreateElement()
        {
            return Application.LoadComponent(new Uri("MSDesktop.Debugging;component/Views/Log/EditTabTagView.xaml", UriKind.Relative)) as FrameworkElement;
        }
    }
}
