﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MSDesktop.Debugging.ViewModels;
using System.Diagnostics;

namespace MSDesktop.Debugging.Extensions
{
    internal class StartProcessHandler : IShowDialogHandler
    {
        public void OnShowDialogRequest(ShowDialogEventArgs arg_)
        {
            string fileName = null;
            string parameter = null;
            string[] arguments = arg_.Content as string[];
            if (arguments == null)
            {
                fileName = arg_.Content as string;
            }
            if (arguments.Length > 0)
            {
                fileName = arguments[0];
                if (arguments.Length > 1)
                {
                    parameter = arguments[1];
                }
            }
            if (!string.IsNullOrWhiteSpace(fileName))
            {
                try
                {
                    Process.Start(fileName, parameter); 
                }
                catch
                {
                    Console.WriteLine(@"Error running command: " + fileName);
                }
            }
        }
    }
}
