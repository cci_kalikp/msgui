﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using MSDesktop.Debugging.ViewModels;
using MSDesktop.Debugging.Views;

namespace MSDesktop.Debugging.Extensions
{
    public class LogViewModelToTemplateTypeHandler: ItemToTemplateTypeHandler
    {
        public Type GetTemplateTypeFor(object item)
        {
            LogViewModel log = item as LogViewModel;
            if (log == null) return null;
            switch (log.LogType)
            {
                case Models.LogType.Log4Net:
                    return typeof(Log4NetLogView);
                case Models.LogType.MSLog:
                    return typeof(MSLogView);
                default:
                    return null;
            } 
        }
    }
}
