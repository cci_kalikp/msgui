﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using MSDesktop.Debugging.ViewModels; 
using MorganStanley.MSDotNet.MSGui.Core; 

namespace MSDesktop.Debugging.Extensions
{
    public static class ShowDialogController
    { 
        public static IShowDialogRequester GetRequester(DependencyObject obj)
        {
            return (IShowDialogRequester)obj.GetValue(RequesterProperty);
        }

        public static void SetRequester(DependencyObject obj, IShowDialogRequester value)
        {

            obj.SetValue(RequesterProperty, value);
        }

        // Using a DependencyProperty as the backing store for Requester.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RequesterProperty =
            DependencyProperty.RegisterAttached("Requester", typeof(IShowDialogRequester), typeof(ShowDialogController), new FrameworkPropertyMetadata(OnRequesterChanged));

        private static readonly List<FrameworkElement> hookedControls = new List<FrameworkElement>(); 
        private static void OnRequesterChanged(DependencyObject d_, DependencyPropertyChangedEventArgs e_)
        {
            FrameworkElement control = d_ as FrameworkElement;
            if (control != null && !hookedControls.Contains(control))
            {
                control.HandleUnloaded(c_ =>
                    {
                        SetRequester(c_, null);
                        hookedControls.Remove(c_);
                    }); 
                hookedControls.Add(control);
            }
            IShowDialogHandler handler = GetHandler(d_);
            if (handler == null) return;
            
            var oldRequester = e_.OldValue as IShowDialogRequester;
            if (oldRequester != null)
            {
                UnregisterHandler(oldRequester, handler);
            }
            var newRequester = e_.NewValue as IShowDialogRequester;
            if (newRequester != null)
            {
                RegisterHandler(newRequester, handler);
            }
        }
         

        public static IShowDialogHandler GetHandler(DependencyObject obj)
        {
            return (IShowDialogHandler)obj.GetValue(HandlerProperty);
        }

        public static void SetHandler(DependencyObject obj, IShowDialogHandler value)
        {
            obj.SetValue(HandlerProperty, value);
        }

        // Using a DependencyProperty as the backing store for Handler.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HandlerProperty =
            DependencyProperty.RegisterAttached("Handler", typeof(IShowDialogHandler), typeof(ShowDialogController), new FrameworkPropertyMetadata(OnHandlerChanged));


        private static void OnHandlerChanged(DependencyObject d_, DependencyPropertyChangedEventArgs e_)
        {
            IShowDialogRequester requester = GetRequester(d_);
            if (requester == null) return;

            var oldHandler = e_.OldValue as IShowDialogHandler;
            if (oldHandler != null)
            {
                UnregisterHandler(requester, oldHandler);
            }
            var newHandler = e_.NewValue as IShowDialogHandler;
            if (newHandler != null)
            {
                RegisterHandler(requester, newHandler);
            }
        }
         
        private static void UnregisterHandler(IShowDialogRequester requester_, IShowDialogHandler handler_)
        {
            IShowDialogHandler handlerWrapper;
            if (handlerWrappers.TryGetValue(handler_, out handlerWrapper))
            {
                  requester_.ShowDialogRequestHandler -= handlerWrapper.OnShowDialogRequest;
                handlerWrappers.Remove(handler_);
            }
        }

        private static void RegisterHandler(IShowDialogRequester requester_, IShowDialogHandler handler_)
        {
            IShowDialogHandler handlerWrapper;
            if (!handlerWrappers.TryGetValue(handler_, out handlerWrapper))
            {
                handlerWrapper = new HandlerWrapper(handler_);
                handlerWrappers.Add(handler_, handlerWrapper);
            } 
            requester_.ShowDialogRequestHandler += handlerWrapper.OnShowDialogRequest;
        }

        private static Dictionary<IShowDialogHandler, IShowDialogHandler> handlerWrappers = new Dictionary<IShowDialogHandler, IShowDialogHandler>();

        private class HandlerWrapper:IShowDialogHandler
        {
            IShowDialogHandler handler;
            public HandlerWrapper(IShowDialogHandler handler_)
            {
                handler = handler_;
            }

            public void OnShowDialogRequest(ShowDialogEventArgs arg_)
            {
                Application.Current.Dispatcher.Invoke(new Action<ShowDialogEventArgs>(arg => handler.OnShowDialogRequest(arg)), arg_);
            }
        }
    }
     

}
