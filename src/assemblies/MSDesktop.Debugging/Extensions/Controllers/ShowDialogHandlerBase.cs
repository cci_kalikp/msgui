﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MSDesktop.Debugging.ViewModels;
using System.Windows;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MSDesktop.Debugging.Extensions
{
    public abstract class ShowDialogHandlerBase: IShowDialogHandler
    {
        private static List<Type> registeredHandlers = new List<Type>();
        
        protected abstract FrameworkElement CreateElement();

        protected abstract Size DialogSize
        {
            get;
        }

        protected abstract string ViewId
        {
            get;
        }

        protected virtual ResizeMode ResizeMode
        {
            get
            {
                return System.Windows.ResizeMode.CanResizeWithGrip;
            }
        }

        ShowDialogEventArgs currentArg;
        public void OnShowDialogRequest(ShowDialogEventArgs arg_)
        {
            currentArg = arg_;
            if (DebuggingExtensions.ChromeManager != null)
            {
                arg_.Result = ShowDialogUseDialogFactory();
            }
            else
            {
                arg_.Result = ShowDialogUseWindow();
            } 
        }

        private bool? ShowDialogUseDialogFactory()
        {
            if (!registeredHandlers.Contains(this.GetType()))
            {
                lock (registeredHandlers)
                {
                    //instead of using the recommended RegisterWindowFactory method, we still use the RegisterDialogFactory,
                    //since in this way, we could save our effort to achieve the effect of center parent windows location
#pragma warning disable 0618
                    DebuggingExtensions.ChromeRegistry.RegisterDialogFactory(ViewId, new MorganStanley.MSDotNet.MSGui.Core.ChromeManager.InitializeDialogHandler(CreateDialog));
#pragma warning restore 0618
                    registeredHandlers.Add(this.GetType());

                }
            }
#pragma warning disable 0618
            return DebuggingExtensions.ChromeManager.CreateDialog(ViewId).ShowDialog();
#pragma warning restore 0618
        }

#pragma warning disable 0618
        private bool CreateDialog(IDialogWindow dialog_)
#pragma warning restore 0618
        {
            FrameworkElement view = CreateElement();
            view.DataContext = currentArg.Content;
            dialog_.Title = currentArg.Title;
            dialog_.Content = view;
            dialog_.Topmost = true;
            dialog_.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            dialog_.SizeToContent = SizeToContent.Manual;
            dialog_.Width = DialogSize.Width;
            dialog_.Height = DialogSize.Height;
            dialog_.ResizeMode = ResizeMode;
            dialog_.Owner = Util.GetActiveWindow();
            return true;
        }

        private bool? ShowDialogUseWindow()
        {
            Window dialog = new Window();
            dialog.Padding = new Thickness(2);
            Window owner = Util.GetActiveWindow();
            if (owner != null) dialog.Icon = owner.Icon;
            FrameworkElement view = CreateElement();
            view.DataContext = currentArg.Content;
            dialog.Title = currentArg.Title;
            dialog.Content = view;
            dialog.Topmost = true;
            dialog.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            dialog.SizeToContent = SizeToContent.Manual;
            dialog.Width = DialogSize.Width;
            dialog.Height = DialogSize.Height;
            dialog.ResizeMode = ResizeMode;
            dialog.Owner = owner;
            return dialog.ShowDialog();
        }

    }
}
