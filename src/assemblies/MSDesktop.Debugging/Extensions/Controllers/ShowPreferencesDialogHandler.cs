﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace MSDesktop.Debugging.Extensions
{
    internal class ShowPreferencesDialogHandler:ShowDialogHandlerBase
    {
        protected override string ViewId
        {
            get { return "PreferencesView"; }
        }

        protected override Size DialogSize
        {
            get { return  DebuggingExtensions.ChromeManager == null ? new Size(325, 208) : new Size(320, 190); }
        }

        protected override ResizeMode ResizeMode
        {
            get
            {
                return System.Windows.ResizeMode.NoResize;
            }
        }

        protected override FrameworkElement CreateElement()
        {
            return Application.LoadComponent(new Uri("MSDesktop.Debugging;component/Views/Log/PreferencesView.xaml", UriKind.Relative)) as FrameworkElement;
        }
    }
}
