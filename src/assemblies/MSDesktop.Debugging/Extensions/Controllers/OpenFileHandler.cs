﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MSDesktop.Debugging.ViewModels;
using Microsoft.Win32;

namespace MSDesktop.Debugging.Extensions
{
    internal class OpenLogFileHandler:IShowDialogHandler
    {
        private OpenFileDialog openFileDialog;
        public OpenLogFileHandler(string initialDir_, string filter_)
        {
            openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = initialDir_;
            openFileDialog.Filter = filter_;
            openFileDialog.Multiselect = true;
        }

        public void OnShowDialogRequest(ShowDialogEventArgs arg_)
        { 
            if (openFileDialog.ShowDialog(Util.GetActiveWindow()).Value)
            {
                arg_.Result = openFileDialog.FileNames;
            }
        }
    }
}
