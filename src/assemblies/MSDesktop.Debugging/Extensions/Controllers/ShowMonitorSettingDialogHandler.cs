﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows; 
namespace MSDesktop.Debugging.Extensions
{
    internal class ShowMonitorSettingDialogHandler : ShowDialogHandlerBase
    {
        protected override string ViewId
        {
            get { return "MonitorSettingView"; }
        } 

        protected override Size DialogSize
        {
            get { return new Size(450, 182); }
        }

        protected override ResizeMode ResizeMode
        {
            get
            {
                return System.Windows.ResizeMode.NoResize;
            }
        }

        protected override FrameworkElement CreateElement()
        {
            return Application.LoadComponent(new Uri("MSDesktop.Debugging;component/Views/Log/MonitorSettingView.xaml", UriKind.Relative)) as FrameworkElement;
        }
    }
}
