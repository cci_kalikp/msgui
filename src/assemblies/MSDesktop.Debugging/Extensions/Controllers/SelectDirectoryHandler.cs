﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MSDesktop.Debugging.ViewModels;
using System.Windows.Interop; 

namespace MSDesktop.Debugging.Extensions
{
    internal class SelectDirectoryHandler:IShowDialogHandler
    {
        private System.Windows.Forms.FolderBrowserDialog openFileDialog;
        public SelectDirectoryHandler()
        {
            openFileDialog = new System.Windows.Forms.FolderBrowserDialog();
        }

        public void OnShowDialogRequest(ShowDialogEventArgs arg_)
        {
            string currentPath = arg_.Content as string;
            if (!string.IsNullOrWhiteSpace(currentPath))
            {
                openFileDialog.SelectedPath = currentPath;
            }
            openFileDialog.ShowNewFolderButton = false;
            openFileDialog.Description = arg_.Title; 
            if (openFileDialog.ShowDialog(new WindowWrapper(new WindowInteropHelper(Util.GetActiveWindow()).Handle))
                == System.Windows.Forms.DialogResult.OK)
            {
                arg_.Result = openFileDialog.SelectedPath;
            }
        }

        public class WindowWrapper : System.Windows.Forms.IWin32Window
        {
            public WindowWrapper(IntPtr handle)
            {
                _hwnd = handle;
            }

            public IntPtr Handle
            {
                get { return _hwnd; }
            }

            private IntPtr _hwnd;
        }
    }
}
