﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using MSDesktop.Debugging.ViewModels;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;

namespace MSDesktop.Debugging.Extensions
{
    public class LogViewShowDialogRouter:IShowDialogHandler
    {
        private static Dictionary<string, IShowDialogHandler> handlers = new Dictionary<string,IShowDialogHandler>(); 
         

        public void OnShowDialogRequest(ShowDialogEventArgs arg_)
        {
            if (string.IsNullOrEmpty(arg_.EventType)) return;
            EnsureHandlers();
            IShowDialogHandler handler;
            if (handlers.TryGetValue(arg_.EventType, out handler))
            {
                handler.OnShowDialogRequest(arg_);
            }
        }

        private static void EnsureHandlers()
        {
            if (handlers.Count == 0)
            {
                lock (handlers)
                {
                    if (handlers.Count == 0)
                    { 
                        handlers.Add("Error", new ShowMessageHandler(VistaTaskDialogIcon.Error));
                        handlers.Add("Warning", new ShowMessageHandler(VistaTaskDialogIcon.Warning));
                        handlers.Add("Information", new ShowMessageHandler(VistaTaskDialogIcon.Information));
                        handlers.Add("OpenLogFile", new OpenLogFileHandler(LogsViewModel.LogDirectory, string.Format("Log files (*{0})|*{0}|All files (*.*)|*.*", LogsViewModel.LogFileExtension)));
                        handlers.Add("EditTabTag", new ShowEditTabTagDialogHandler());
                        handlers.Add("Preferences", new ShowPreferencesDialogHandler());
                        handlers.Add("SelectDirectory", new SelectDirectoryHandler());
                        handlers.Add("SetupMonitor", new ShowMonitorSettingDialogHandler());
                        handlers.Add("StartProcess", new StartProcessHandler());
                        handlers.Add("HeaderInformation", new ShowHeaderInformationHandler());
                    }
                }
            }
        }
    }
}
