﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MSDesktop.Debugging.ViewModels; 
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;

namespace MSDesktop.Debugging.Extensions
{
    internal class ShowMessageHandler : IShowDialogHandler
    {
        private VistaTaskDialogIcon icon;
        public ShowMessageHandler(VistaTaskDialogIcon icon_)
        {
            icon = icon_;
        }

        public void OnShowDialogRequest(ShowDialogEventArgs arg_)
        {
            TaskDialog.Show(new TaskDialogOptions()
            {
                Title = arg_.Title,
                Content = arg_.Content as string,
                MainIcon = icon,
                CommonButtons = TaskDialogCommonButtons.Close,
                Owner = Util.GetActiveWindow()
            });
            
        }
    }
}
