﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Windows;

namespace MSDesktop.Debugging.Extensions
{
    class ShowHeaderInformationHandler : ShowDialogHandlerBase
    { 
        protected override System.Windows.FrameworkElement CreateElement()
        {
            return Application.LoadComponent(new Uri("MSDesktop.Debugging;component/Views/Log/HeaderInformationView.xaml", UriKind.Relative)) as FrameworkElement;

        }

        protected override System.Windows.Size DialogSize
        {
            get { return new System.Windows.Size(450, 330); }
        }

        protected override System.Windows.ResizeMode ResizeMode
        {
            get
            {
                return System.Windows.ResizeMode.CanResize;
            }
        }

        protected override string ViewId
        {
            get { return "HeaderInformationView"; }
        }
    }
}
