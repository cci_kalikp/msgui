﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;

namespace MSDesktop.Debugging.Extensions
{
    internal static class ObsevableExtensions
    {
        public static IObservable<List<T>> BufferSlidingUntil<T>(this IObservable<T> source_, Func<List<T>, bool> predicate_)
        {
            return Observable.Create<List<T>>
              (o =>
                 {
                     var allBuffers = new List<List<T>>();
                     return source_.Subscribe(n =>
                                                 {
                                                     allBuffers.ForEach(buffer => buffer.Add(n));
                                                     var currentBuffer = new List<T> { n };
                                                     allBuffers.Add(currentBuffer);
                                                     var buffersThatMeetPredicate = allBuffers.Where(predicate_).ToList();
                                                     allBuffers.RemoveAll(l => predicate_(l));
                                                     buffersThatMeetPredicate.ForEach(o.OnNext);
                                                 },
                                              o.OnError, o.OnCompleted);
                 });
        }

        public static IObservable<IEnumerable<T>> BufferUntil<T>(this IObservable<T> source_, Func<IEnumerable<T>, bool> predicate_)
        {
            return Observable.Create<IEnumerable<T>>
            (o =>
               {
                   var buffer = new List<T>();
                   return source_.Subscribe(n =>
                                              {
                                                  buffer.Add(n);
                                                  if (!predicate_(buffer)) return;
                                                  o.OnNext(buffer);
                                                  buffer = new List<T>();
                                              },
                                            o.OnError, o.OnCompleted);
               });
             
        }
    }
}