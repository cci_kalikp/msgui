﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Text.RegularExpressions;
using System.Xml;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using System.IO;
using System.Diagnostics; 

namespace MSDesktop.Debugging.Extensions
{
    public class ViewSelectedXmlCommand : ICommand
    {
        private static readonly string XML_VIEWER;
        private const string ElementRegEx = "<(\\w+)(\\s\\w+(:\\w+){0,1}=[\"\'].*[\"\'])*?>";
        private TaskDialogOptions errorDialog = new TaskDialogOptions()
        {
            MainIcon = VistaTaskDialogIcon.Error,
            Title = "Error",
            Content = "Invalid XML selection"
        };

        static ViewSelectedXmlCommand()
        {
            string programPath = Environment.GetEnvironmentVariable("ProgramFiles");
            string programPath2 = Environment.GetEnvironmentVariable("ProgramFiles(x86)");
            if (string.IsNullOrEmpty(programPath))
            {
                programPath = "C:\\Program Files";
            }
            if (string.IsNullOrEmpty(programPath2))
            {
                programPath2 = "C:\\Program Files (x86)";
            }
            XML_VIEWER = Path.Combine(programPath, "Google\\Chrome\\Application\\chrome.exe");
            if (File.Exists(XML_VIEWER)) return;
            XML_VIEWER = Path.Combine(programPath2, "Google\\Chrome\\Application\\chrome.exe");
            if (File.Exists(XML_VIEWER)) return;
            XML_VIEWER = Path.Combine(programPath, "Mozilla Firefox\\msfirefox.exe");
            if (File.Exists(XML_VIEWER)) return;
            XML_VIEWER = Path.Combine(programPath2, "Mozilla Firefox\\msfirefox.exe");
            if (File.Exists(XML_VIEWER)) return;
            XML_VIEWER = "iexplore.exe"; 
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged;
        public void RaiseCanExecuteChanged()
        {
            var handler = CanExecuteChanged;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }


        public void Execute(object parameter)
        {
            CustomFlowDocumentScrollViewer viewer = parameter as CustomFlowDocumentScrollViewer;
            if (viewer == null) return; 
            string text = viewer.Selection == null ? string.Empty: viewer.Selection.Text;
            if (text.Length == 0)
            {
                viewer.SelectAll();
                if (viewer.Selection != null) 
                {
                    string allText = viewer.Selection.Text;
                    Match firstElement = Regex.Match(allText, ElementRegEx);
                    if (firstElement.Success)
                    {
                        string elementName = firstElement.Groups[1].Value;
                        Match lastElement = Regex.Match(allText, string.Format("</{0}>", elementName),
                                                        RegexOptions.RightToLeft);
                        text = allText.Substring(firstElement.Index,
                                                 lastElement.Index + lastElement.Length - firstElement.Index);
                    }
                }
                viewer.UnselectAll();
            }
            if (text.Length == 0)
            {
                errorDialog.Owner = Util.GetActiveWindow();
                TaskDialog.Show(errorDialog);
                return;
            }

            try
            { 
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.LoadXml(text);
                string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "LogViewer.xml");
                XmlWriter writer = XmlWriter.Create(path);
                xmlDocument.WriteContentTo(writer);
                writer.Flush();
                writer.Close();
                var proc = new Process();
                proc.StartInfo.FileName = XML_VIEWER;
                proc.StartInfo.Arguments = string.Format("\"{0}\"", path);
                proc.Start();
            }
            catch
            {
                errorDialog.Owner = Util.GetActiveWindow();
                TaskDialog.Show(errorDialog); 
            }
        }
    }
}
