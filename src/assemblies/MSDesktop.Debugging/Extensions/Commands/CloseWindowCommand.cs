﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows; 

namespace MSDesktop.Debugging.Extensions
{
    public class CloseWindowCommand:ICommand
    {
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged;
        public void RaiseCanExecuteChanged()
        {
            var handler = CanExecuteChanged;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        public bool? DialogResult { get; set; } 
        
        public void Execute(object parameter)
        {
            DependencyObject element = parameter as DependencyObject;
            if (element != null)
            {
                Window w = Window.GetWindow(element);
                if (w != null)
                {
                    if (DialogResult != null)
                    {
                        w.DialogResult = DialogResult;
                    }
                    w.Close();
                }
            }
        } 
    }
}
