﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Diagnostics;

namespace MSDesktop.Debugging.Extensions
{
    public class OpenFileCommand:ICommand
    {
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged;
        public void RaiseCanExecuteChanged()
        {
            var handler = CanExecuteChanged;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        
        public void Execute(object parameter)
        {
            string path = parameter as string;
            if (!string.IsNullOrEmpty(path))
            {
                Process.Start(path);
            }
        } 
    }
}
