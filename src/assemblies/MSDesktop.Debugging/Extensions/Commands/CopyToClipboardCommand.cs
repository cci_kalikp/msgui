﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows;

namespace MSDesktop.Debugging.Extensions
{
    public class CopyToClipboardCommand:ICommand
    { 
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged;
        public void RaiseCanExecuteChanged()
        {
            var handler = CanExecuteChanged;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        public void Execute(object parameter)
        {
            if (parameter == null) return;
 
            //this will always fail, seems to be a bug in .net 4.0
            //Clipboard.SetText(parameter.ToString());

            Clipboard.SetDataObject(parameter);
        }
         
    }
}
