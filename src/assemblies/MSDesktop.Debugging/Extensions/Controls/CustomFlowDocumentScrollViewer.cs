﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;
using System.Globalization;

namespace MSDesktop.Debugging.Extensions
{
    public class CustomFlowDocumentScrollViewer:FlowDocumentScrollViewer
    {
        private const double adjustment = 40;

        public CustomFlowDocumentScrollViewer()
        {
             //The FlowDocumentScrollViewer style defined in StandardWindowsElementsLuna.xml is removed
            //since it's the duplication of the default style, and for some unknown reason, would enlarge the zoom slider greatly
            this.SetResourceReference(FrameworkElement.StyleProperty, typeof(FlowDocumentScrollViewer));  
        }

        public bool WordWrap
        {
            get { return (bool)GetValue(WordWrapProperty); }
            set { SetValue(WordWrapProperty, value); }
        }

        // Using a DependencyProperty as the backing store for WordWrap.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty WordWrapProperty =
            DependencyProperty.Register("WordWrap", typeof(bool), typeof(CustomFlowDocumentScrollViewer),
            new UIPropertyMetadata(false, WordWrapChanged));

        private static void WordWrapChanged(DependencyObject d_, DependencyPropertyChangedEventArgs e_)
        {
            if (e_.OldValue != e_.NewValue)
            {
                ((CustomFlowDocumentScrollViewer)d_).SetWordWrap();
            }
        } 

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e_)
        { 
            base.OnPropertyChanged(e_);
            if (e_.Property == DocumentProperty && e_.OldValue != e_.NewValue)
            {
                SetWordWrap();    
                
            }  
        }

        public void SelectAll()
        {
            if (this.Document != null)
            {
                this.Selection.Select(this.Document.ContentStart, this.Document.ContentEnd); 
            }
        }

        public void UnselectAll()
        {
            if (this.Document != null)
            {
                this.Selection.Select(this.Document.ContentStart, this.Document.ContentStart);
            }
        }
          
        private void SetWordWrap()
        {
            if (Document != null)
            {
                if (WordWrap)
                {
                    Document.ClearValue(FlowDocument.PageWidthProperty);
                }
                else
                {
                   Document.PageWidth = CalculatePageWidth(Document); 
                }
            }
        }

        private static double CalculatePageWidth(FlowDocument document_)
        {
            double width = 0;
            foreach (Paragraph p in document_.Blocks)
            {
                //note: this would only work if the document is converted through ObjectsToFlowDocumentConverter
                //which set the raw string as the tag of the paragraph
                //a more general solution might be walk through all the inlines of the paragraph to get the full text
                string text = p.Tag as string;
                if (text == null)
                {
                    continue;
                }
                 
                FormattedText ft = new FormattedText(text,
                        CultureInfo.CurrentCulture,
                        FlowDirection.LeftToRight,
                        new Typeface(p.FontFamily, p.FontStyle, p.FontWeight, p.FontStretch),
                        p.FontSize, Brushes.Black);

                if (ft.Width + adjustment > width)
                {
                    width = ft.Width + adjustment;
                }
            }
            return width;
        }
        
        
    }
}
 