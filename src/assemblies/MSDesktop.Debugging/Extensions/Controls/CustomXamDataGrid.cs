﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using Infragistics.Windows;
using Infragistics.Windows.DataPresenter;
using System.Windows;
using System.Windows.Data;
using System.Collections;
using Infragistics.Windows.Controls;
using Infragistics.Windows.DataPresenter.Events;
using Infragistics.Windows.Editors;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MSDesktop.Debugging.Extensions
{
    public class CustomXamDataGrid:XamDataGrid
    {
        public CustomXamDataGrid()
        {
            this.SetResourceReference(FrameworkElement.StyleProperty, typeof(XamDataGrid)); 
        } 
         

        public readonly static DependencyProperty SelectedObjectsProperty =
          DependencyProperty.Register("SelectedObjects", typeof(IList), typeof(CustomXamDataGrid),
            new FrameworkPropertyMetadata()
            { 
                DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged, 
                DefaultValue = new List<object>()
            });

        public IList SelectedObjects
        {
            get { return GetValue(SelectedObjectsProperty) as IList; }
            set { SetValue(SelectedObjectsProperty, value); }
        }
 

        public int VisibleRowCount
        {
            get { return (int)GetValue(VisibleRowCountProperty); }
            set { SetValue(VisibleRowCountProperty, value); }
        }

        // Using a DependencyProperty as the backing store for VisibleRowCount.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty VisibleRowCountProperty =
            DependencyProperty.Register("VisibleRowCount", typeof(int), typeof(CustomXamDataGrid), new UIPropertyMetadata(0));
         

        public bool IgnoreSelectedRecords
        {
            get { return (bool)GetValue(IgnoreSelectedRecordsProperty); }
            set { SetValue(IgnoreSelectedRecordsProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IgnoreSelectedRecords.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IgnoreSelectedRecordsProperty =
            DependencyProperty.Register("IgnoreSelectedRecords", typeof(bool), typeof(CustomXamDataGrid), new UIPropertyMetadata(false));
         

        public bool AllowSorting
        {
            get { return (bool)GetValue(AllowSortingProperty); }
            set { SetValue(AllowSortingProperty, value); }
        }

        // Using a DependencyProperty as the backing store for AllowSorting.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AllowSortingProperty =
            DependencyProperty.Register("AllowSorting", typeof(bool), typeof(CustomXamDataGrid), new UIPropertyMetadata(true));  

        public bool AllowFiltering
        {
            get { return (bool)GetValue(AllowFilteringProperty); }
            set { SetValue(AllowFilteringProperty, value); }
        }

        // Using a DependencyProperty as the backing store for AllowFiltering.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AllowFilteringProperty =
            DependencyProperty.Register("AllowFiltering", typeof(bool), typeof(CustomXamDataGrid), new UIPropertyMetadata(false, AllowFilteringChanged));
 

        private static void AllowFilteringChanged(DependencyObject d_, DependencyPropertyChangedEventArgs e_)
        {
            CustomXamDataGrid grid = d_ as CustomXamDataGrid;
            if (grid.FieldSettings != null)
            {
                //The FieldSettings as well as the Field class are not in the visual tree so they can't participate in a normal WPF binding.
                //So the binding of the FieldSettings.AllowRecordFiltering doesn't work if bind to the DataContext, we need to use this method
                grid.FieldSettings.AllowRecordFiltering = grid.AllowFiltering;
            } 
        } 
        protected override void OnRecordFilterChanging(RecordFilterChangingEventArgs args)
        {
            base.OnRecordFilterChanging(args);
            FilterCellValuePresenter fcvp = Utilities.GetDescendantFromType(this, typeof(FilterCellValuePresenter), true) as FilterCellValuePresenter;
            DataRecordPresenter filterRecord = Utilities.GetAncestorFromType(fcvp, typeof(DataRecordPresenter), true) as DataRecordPresenter;

            FilterCellValuePresenter currentFilter = CellValuePresenter.FromRecordAndField(filterRecord.DataRecord, args.NewRecordFilter.Field) as FilterCellValuePresenter;
            TextEditorBase editor = currentFilter.FindVisualChildren<TextEditorBase>().FirstOrDefault();
            if (editor != null)
            {
                //break this temporarily to void the EndEdit of the filter editor called as soon as the current item is changed
                if (this.IsSynchronizedWithCurrentItem)
                {
                    this.IsSynchronizedWithCurrentItem = false;
                    editor.EditModeEnded += editor_EditModeEnded;
                }
            } 
        }

        //rebuild the link to underlying collection view after user finishes entering the filter
        void editor_EditModeEnded(object sender, Infragistics.Windows.Editors.Events.EditModeEndedEventArgs e)
        {
            ValueEditor editor = (ValueEditor)sender;
            editor.EditModeEnded -= editor_EditModeEnded;
            this.IsSynchronizedWithCurrentItem = true; 
        }
         
        protected override void OnSorting(SortingEventArgs args)
        {
            if (!AllowSorting) args.Cancel = true; 
            base.OnSorting(args);
        } 
   
        protected override void OnRecordsInViewChanged(RecordsInViewChangedEventArgs args_)
        { 
            base.OnRecordsInViewChanged(args_);
          

            this.VisibleRowCount = RecordManager.GetFilteredInDataRecords().Count<Record>();
            if (VisibleRowCount > 0)
            {
                if (this.SelectedItems.Records.Count == 0 && this.ActiveRecord == null && RecordManager.Current.Count > 0)
                {
                    this.ActiveRecord = RecordManager.Current[0];
                }
                try
                {
                    if (!string.IsNullOrWhiteSpace(HighlightSourceField) &&
                        this.FieldLayouts.Count > 0 &&
                        this.FieldLayouts[0].RecordFilters.Count > 0)
                    {
                        var filter = this.FieldLayouts[0].RecordFilters[HighlightSourceField];
                        if (filter != null)
                        {
                            var conditions = filter.Conditions;
                            if (conditions.Count > 0)
                            {
                                ComparisonCondition condition = conditions[0] as ComparisonCondition;
                                if (condition != null && condition.Value != null)
                                {
                                    this.HighlightValue = condition.Value.ToString().Trim();
                                    return;
                                }
                            }
                        }
                    }
                    HighlightValue = null;
                }
                catch { }
            }
            else
            {
                this.SelectedObjects = new List<object>();
            }

        }
         
        public string HighlightSourceField
        {
            get { return (string)GetValue(HighlightSourceFieldProperty); }
            set { SetValue(HighlightSourceFieldProperty, value); }
        }

        // Using a DependencyProperty as the backing store for HighlightSourceField.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HighlightSourceFieldProperty =
            DependencyProperty.Register("HighlightSourceField", typeof(string), typeof(CustomXamDataGrid), new UIPropertyMetadata(null));
         

        public string HighlightValue
        {
            get { return (string)GetValue(HighlightValueProperty); }
            set { SetValue(HighlightValueProperty, value); }
        }

        // Using a DependencyProperty as the backing store for HighlightValue.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HighlightValueProperty =
            DependencyProperty.Register("HighlightValue", typeof(string), typeof(CustomXamDataGrid), new UIPropertyMetadata(null));

        protected override void OnRecordActivated(RecordActivatedEventArgs args_)
        {
            base.OnRecordActivated(args_); 
            UpdateSelectedObjects();
        }
         
        protected override void OnSelectedItemsChanged(SelectedItemsChangedEventArgs args_)
        {
            base.OnSelectedItemsChanged(args_);
            UpdateSelectedObjects();
        }

        private void UpdateSelectedObjects()
        {
            SortedList<int, object> selectedObjects = new SortedList<int, object>();
            if (!IgnoreSelectedRecords)
            {
                if (this.SelectedItems.Records.Count > 0)
                {
                    foreach (DataRecord record in this.SelectedItems.Records)
                    {
                        if (record.DataItem != null && !selectedObjects.ContainsKey(record.VisibleIndex))
                        {
                            selectedObjects.Add(record.VisibleIndex, record.DataItem);
                        }
                    }
                }
            }
            DataRecord activeRecord = this.ActiveRecord as DataRecord;
            if (activeRecord != null && !activeRecord.IsSelected && 
                activeRecord.DataItem != null && !selectedObjects.ContainsKey(activeRecord.VisibleIndex))
            {
                selectedObjects.Add(activeRecord.VisibleIndex, activeRecord.DataItem);
            }
            
            this.SelectedObjects = new List<object>(selectedObjects.Values);
        }



        public FieldSortDescription SortedField
        {
            get { return (FieldSortDescription)GetValue(SortedFieldProperty); }
            set { SetValue(SortedFieldProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SortedField.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SortedFieldProperty =
            DependencyProperty.Register("SortedField", typeof(FieldSortDescription), typeof(CustomXamDataGrid), new UIPropertyMetadata(null, SortedFieldChanged));

        private static void SortedFieldChanged(DependencyObject dependencyObject_, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs_)
        {
            FieldSortDescription sortedField = (FieldSortDescription) dependencyPropertyChangedEventArgs_.NewValue;
            if (sortedField == null) return;
            XamDataGrid grid = (XamDataGrid)dependencyObject_;
            bool alreadyThere = false;
            for (int i = grid.FieldLayouts[0].SortedFields.Count - 1; i >= 0; i--)
            {
                var field = grid.FieldLayouts[0].SortedFields[i];
                if (field.FieldName == sortedField.FieldName &&
                    field.Direction == sortedField.Direction)
                {
                    alreadyThere = true;
                }
                else
                {
                    grid.FieldLayouts[0].SortedFields.RemoveAt(i);
                }
            }
            if (!alreadyThere)
            {
                grid.FieldLayouts[0].SortedFields.Add(sortedField);
            }
                 
        }
    }
}
