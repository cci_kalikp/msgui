﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls; 
using System.Windows;

namespace MSDesktop.Debugging.Extensions
{
    public class CustomDataTemplateSelector: DataTemplateSelector
    { 
        Dictionary<Object, DataTemplate> templatesCache = new Dictionary<Object, DataTemplate>();

        public Boolean CacheTemplates { get; set; }

        public ItemToTemplateTypeHandler Handler { get; set; }

        public override DataTemplate SelectTemplate(object item, System.Windows.DependencyObject container)
        {
            if (item != null)
            {
                DataTemplate template;
                if (!this.templatesCache.TryGetValue(item, out template))
                {
                    if (Handler != null)
                    {
                        var templateType = this.Handler.GetTemplateTypeFor(item);
                        if (templateType != null)
                        {
                            template = new DataTemplate()
                            {
                                VisualTree = new FrameworkElementFactory()
                                {
                                    Type = templateType
                                }
                            };
                        } 
                    } 
                }

                if (template != null)
                {
                    return template;
                }
            }

            return base.SelectTemplate(item, container); 
        }
    }

    public interface ItemToTemplateTypeHandler
    {
        Type GetTemplateTypeFor(Object item);
    }

}
