﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSLog;
using System.IO;
using System.Windows;
using System.Windows.Media;

namespace MSDesktop.Debugging.Extensions
{
    public static class Util
    {
        public static string GetLogFileName(out string startupDirectory)
        {
            startupDirectory = null;
            DateTime lastVisitedTime = new DateTime();
            string fileName = null;
            foreach (var destination in MSLog.Destinations)
            { 
                var fileDestination = destination as MSLogFileDestination;
                if (fileDestination != null && !string.IsNullOrEmpty(fileDestination.FileName))
                {
                    try
                    {
                        FileInfo file = new FileInfo(fileDestination.FileName);
                        if (file.Exists && file.LastWriteTime.CompareTo(lastVisitedTime) > 0)
                        {
                            lastVisitedTime = file.LastWriteTime; 
                            fileName = file.FullName;
                            startupDirectory = file.DirectoryName;
                        }
                    }
                    catch
                    {
                    }
                }
            }
            return fileName;
        }

        public static Window GetActiveWindow()
        {
            return Application.Current.Windows.Cast<Window>().FirstOrDefault(x => x.IsActive) ??
Application.Current.Windows.Cast<Window>().FirstOrDefault(x => x.Visibility == Visibility.Visible);

        }


        /// <summary>
        /// Contains approximate string matching
        /// Levenshtein distance between two words is the minimum number of single-character edits 
        /// (insertion, deletion, substitution) required to change one word into the other.
        /// </summary>
        public static int ComputeLevenshteinDistance(string source_, string target_)
        {
            int n = source_.Length;
            int m = target_.Length;
            int[,] d = new int[n + 1, m + 1];

            // Step 1
            if (n == 0)
            {
                return m;
            }

            if (m == 0)
            {
                return n;
            }

            // Step 2
            for (int i = 0; i <= n; d[i, 0] = i++)
            {
            }

            for (int j = 0; j <= m; d[0, j] = j++)
            {
            }

            // Step 3
            for (int i = 1; i <= n; i++)
            {
                //Step 4
                for (int j = 1; j <= m; j++)
                {
                    // Step 5
                    int cost = (target_[j - 1] == source_[i - 1]) ? 0 : 1;

                    // Step 6
                    d[i, j] = Math.Min(
                      Math.Min(d[i - 1, j] + 1, d[i, j - 1] + 1),
                      d[i - 1, j - 1] + cost);
                }
            }
            // Step 7
            return d[n, m];
        }
         
         
    }
}
