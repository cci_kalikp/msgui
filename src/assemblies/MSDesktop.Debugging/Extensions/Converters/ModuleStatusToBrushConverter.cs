﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Media;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MSDesktop.Debugging.Extensions
{
    public class ModuleStatusToBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null || !(value is ModuleStatus))
            {
                return null;
            }
            ModuleStatus status = (ModuleStatus) value;
            switch (status)
            {
                case ModuleStatus.TypeResolutionFailed: 
                case ModuleStatus.Exceptioned:
                case ModuleStatus.ProcessExited:
                    return Brushes.Crimson;
                case ModuleStatus.BlockedByEntitlements:
                case ModuleStatus.DependentModuleBlocked:
                case ModuleStatus.BlockedByNoEntitlementsInfo:
                    return Brushes.Blue;
                default:
                    return Brushes.Black;

            }
        }

        public object ConvertBack(object value, Type targetType, object parameter,
                                  System.Globalization.CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
