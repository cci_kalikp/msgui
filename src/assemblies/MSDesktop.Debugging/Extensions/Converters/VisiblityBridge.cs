﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace MSDesktop.Debugging.Extensions
{
    public class VisiblityBridge:Freezable
    {
        protected override Freezable CreateInstanceCore()
        {
            return new VisiblityBridge();
        } 

        public Visibility Visibility
        {
            get { return (Visibility)GetValue(VisibilityProperty); }
            set { SetValue(VisibilityProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Visibility.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty VisibilityProperty =
            DependencyProperty.Register("Visibility", typeof(Visibility), typeof(VisiblityBridge), new UIPropertyMetadata(Visibility.Visible));

        
    }
}
