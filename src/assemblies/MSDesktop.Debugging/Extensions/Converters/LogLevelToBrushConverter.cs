﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data; 
using System.Windows.Media;
using Infragistics.Windows.DataPresenter;
using MSDesktop.Debugging.Models;

namespace MSDesktop.Debugging.Extensions
{
    public class LogLevelToBrushConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values == null || values.Length < 3 || !(values[1] is MSLogLevel || values[1] is Log4NetLogLevel))
            {
                return Binding.DoNothing;
            }
            
            //remove following lines if when the record is selected we still want to use the calculated color for Threshold field 
            if (values[0] != null && System.Convert.ToBoolean(values[0])) return Brushes.Transparent;

            string fieldName = values[2] as string;
            if (values[1] is MSLogLevel)
            {
                MSLogLevel level = (MSLogLevel)values[1];
                switch (level)
                {
                    case MSLogLevel.Info:
                        return fieldName == "Threshold" ? Brushes.Gainsboro : Brushes.Transparent;
                    case MSLogLevel.Debug:
                        return fieldName == "Threshold" ? Brushes.LimeGreen : Brushes.Transparent;
                    case MSLogLevel.Warning:
                        return fieldName == "Threshold" ? Brushes.Yellow : Brushes.Transparent;
                    case MSLogLevel.Error:
                        return fieldName == "Threshold" ? Brushes.Crimson : Brushes.Transparent;
                    case MSLogLevel.Notice:
                        return fieldName == "Threshold" ? Brushes.LightSteelBlue : Brushes.Transparent;
                    //for loglevel ge critical, highlight the whole line
                    case MSLogLevel.Emergency:
                    case MSLogLevel.Critical:
                        return Brushes.Firebrick;
                    case MSLogLevel.Alert:
                        return Brushes.Orange;
                    default:
                        return Binding.DoNothing;
                }
            }
            else
            {
                Log4NetLogLevel level = (Log4NetLogLevel)values[1];
                switch (level)
                {
                    case Log4NetLogLevel.Debug:
                        return fieldName == "Threshold" ? Brushes.LimeGreen : Brushes.Transparent;
                    case Log4NetLogLevel.Info:
                        return fieldName == "Threshold" ? Brushes.Gainsboro : Brushes.Transparent;
                    case Log4NetLogLevel.Warn:
                        return fieldName == "Threshold" ? Brushes.Yellow : Brushes.Transparent;
                    case Log4NetLogLevel.Error:
                        return fieldName == "Threshold" ? Brushes.Crimson : Brushes.Transparent;
                    case Log4NetLogLevel.Fatal:
                        return Brushes.Firebrick;
                    case Log4NetLogLevel.ParseError:
                    default:
                        return Binding.DoNothing;
                }
            }
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
