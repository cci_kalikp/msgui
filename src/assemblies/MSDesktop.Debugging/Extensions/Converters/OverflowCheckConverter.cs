﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Data;
using Infragistics.Windows.Editors;
using System.Windows.Media;
using System.Windows;
 
namespace MSDesktop.Debugging.Extensions
{
    public class OverflowCheckConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values[0] != null && values[0] != DependencyProperty.UnsetValue)
            { 
                XamTextEditor editor = values[1] as XamTextEditor;
                if (editor != null)
                {
                    FormattedText ft = new FormattedText(editor.Text, culture, editor.FlowDirection, new Typeface(editor.FontFamily, editor.FontStyle, editor.FontWeight, editor.FontStretch), editor.FontSize, editor.Foreground);
                    if (ft.Width + 4 > (double)values[0])
                    {
                        return true;
                    }
                }
                else
                {
                    TextBlock textBlock = values[1] as TextBlock;
                    if (textBlock != null)
                    {
                        FormattedText ft = new FormattedText(textBlock.Text, culture, textBlock.FlowDirection, new Typeface(textBlock.FontFamily, textBlock.FontStyle, textBlock.FontWeight, textBlock.FontStretch), textBlock.FontSize, textBlock.Foreground);
                        if (ft.Width + 4 > (double)values[0])
                        {
                            return true;
                        }
                    }
                }

            }
            return false;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            return new object[] { };
        }
    }
}
