﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Documents;
using System.Collections;

namespace MSDesktop.Debugging.Extensions
{
    public class ObjectsToFlowDocumentConverter:IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values == null || values.Length == 0 || values[0] == null) return Binding.DoNothing;
            IEnumerable items = values[0] as IEnumerable;
            string highlightText = null;
            if (values.Length > 1 && values[1] != null)
            {
                highlightText = values[1].ToString(); 
            }
            Brush highlightBrush = null;
            if (values.Length > 2)
            {
                highlightBrush = values[2] as Brush;
                if (highlightBrush == null)
                {
                    highlightBrush = new BrushConverter().ConvertFrom(values[2]) as Brush;
                }
            } 
            string propertyName = parameter != null ? parameter.ToString() : null;
            return GenerateDocument(items, highlightText, highlightBrush, propertyName);
        }
         

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        private static FlowDocument GenerateDocument(IEnumerable items_, string highlightText, Brush highlightBrush_, string propertyName_)
        {
            FlowDocument doc = new FlowDocument(); 
            doc.PagePadding = new Thickness(0);
            foreach (var item in items_)
            {
                if (item == null) continue;
                string text = null;
                if (string.IsNullOrWhiteSpace(propertyName_))
                {
                    text = item.ToString();
                }
                else
                {
                    try
                    {
                        text = item.GetType().GetProperty(propertyName_).GetValue(item, null).ToString();
                    }
                    catch
                    {
                        continue;
                    }
                }
                Paragraph p = new Paragraph();
                p.Tag = text;
                p.Inlines.AddRange(GenerateInlines(text, highlightText, highlightBrush_));
                doc.Blocks.Add(p);  
            } 
            return doc;
        }

        private static List<Inline> GenerateInlines(string text_, string highlightText_, Brush highlightBrush_)
        {
            List<Inline> lines = new List<Inline>();
             
            if (!string.IsNullOrWhiteSpace(highlightText_) && highlightBrush_ != null)
            {
                int found = text_.IndexOf(highlightText_, StringComparison.CurrentCultureIgnoreCase);
                while (found != -1)
                {
                    if (found > 0)
                    {
                        string leftText = text_.Substring(0, found);
                        lines.Add(new Run(leftText));
                    }
                    lines.Add(new Run(text_.Substring(found, highlightText_.Length)) { Foreground = highlightBrush_ });
                    int right = found + highlightText_.Length;
                    if (text_.Length == right)
                    {
                        text_ = string.Empty;
                        break;
                    }
                    text_ = text_.Substring(right);
                    found = text_.IndexOf(highlightText_);
                }
            }
            if (text_.Length > 0)
            {
                lines.Add(new Run(text_));
            }
            return lines;
        }
        
    }
}
