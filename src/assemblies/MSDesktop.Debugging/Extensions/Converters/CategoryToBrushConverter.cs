﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using MSDesktop.Debugging.ViewModels;
using System.Windows.Media;

namespace MSDesktop.Debugging.Extensions
{
    public class CategoryToBrushConverter:IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null || !(value is AssemblyInfoViewModel.AssemblyCategory))
            {
                return Binding.DoNothing;
            }
            switch ((AssemblyInfoViewModel.AssemblyCategory)value)
            {
                case AssemblyInfoViewModel.AssemblyCategory.Core:
                    return Brushes.DimGray;
                case AssemblyInfoViewModel.AssemblyCategory.GAC:
                    return Brushes.Blue;
                case AssemblyInfoViewModel.AssemblyCategory.Dynamic:
                    return Brushes.LightSlateGray;
                case AssemblyInfoViewModel.AssemblyCategory.Production:
                    return Binding.DoNothing;
                case AssemblyInfoViewModel.AssemblyCategory.Unknown:
                    return Brushes.Red;
                default:
                    return Binding.DoNothing;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
