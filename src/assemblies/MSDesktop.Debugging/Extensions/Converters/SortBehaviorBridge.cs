﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Infragistics.Windows.DataPresenter;

namespace MSDesktop.Debugging.Extensions
{
    public class SortBehaviorBridge : Freezable
    {
        protected override Freezable CreateInstanceCore()
        {
            return new SortBehaviorBridge();
        }

        public LabelClickAction LabelClickAction
        {
            get { return (LabelClickAction)GetValue(LabelClickActionProperty); }
            set { SetValue(LabelClickActionProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Visibility.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LabelClickActionProperty =
            DependencyProperty.Register("LabelClickAction", typeof(LabelClickAction), typeof(SortBehaviorBridge), new UIPropertyMetadata(LabelClickAction.Default));
         

        public FieldSortDescription SortedField
        {
            get { return (FieldSortDescription)GetValue(SortedFieldProperty); }
            set { SetValue(SortedFieldProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SortedField.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SortedFieldProperty =
            DependencyProperty.Register("SortedField", typeof(FieldSortDescription), typeof(SortBehaviorBridge), new UIPropertyMetadata(null));

        

        
            
    }
}
