﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Data;
using Infragistics.Windows.DataPresenter; 

namespace MSDesktop.Debugging.Extensions
{
    public class FieldSortDescriptionConverter: IValueConverter
    {
        public bool Ascending { get; set; }
        public string FieldName { get; set; }
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (!System.Convert.ToBoolean(value)) return null;
            return new FieldSortDescription(FieldName, Ascending ? ListSortDirection.Ascending : ListSortDirection.Descending, false);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
