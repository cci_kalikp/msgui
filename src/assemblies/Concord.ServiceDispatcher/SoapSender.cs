using MorganStanley.IED.Concord.ServiceDispatcher.Configuration;
using MorganStanley.MSDotNet.MSNet;
using MorganStanley.MSDotNet.MSSoap.Client;
using MorganStanley.MSDotNet.MSSoap.Common;

namespace MorganStanley.IED.Concord.ServiceDispatcher
{
  internal class SoapSender : ISoapSender
  {
    private IServiceSetting _serviceSetting;

    public SoapSender(IServiceSetting _serviceSetting_)
    {
      _serviceSetting = _serviceSetting_;
    }

    public SoapMessage GetResponse(IMSNetLoop loop_, SoapClientMessage message_)
    {
      ISoapOutputTransport transport = _serviceSetting.GetTransport(loop_);
      if (transport is IHttpClientCall)
      {
        return ((IHttpClientCall)transport).Send(message_, _serviceSetting.Timeout);
      }
      if (_serviceSetting.Connection.UseRequestResponse)
      {
        IRequestResponseCall client = _serviceSetting.GetRequestResponseClient(transport);
        ISoapResponse soapResponse = client.SyncSendRequest(new SoapEnvelopeRequest(message_.Envelope), (int)_serviceSetting.Timeout);
        return new SoapMessage(soapResponse.Envelope);
      }
      return transport.SyncSendRequest(message_, _serviceSetting.Timeout);
    }
  }
}