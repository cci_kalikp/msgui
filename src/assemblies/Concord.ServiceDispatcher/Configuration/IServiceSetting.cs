#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2009 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

  $Id: //eai/msdotnet/msgui/trunk/assemblies/Concord.ServiceDispatcher/Configuration/IServiceSetting.cs#3 $
  $DateTime: 2013/01/18 15:24:18 $
  $Change: 813214 $
  $Author: smulovic $
_____________________________________________________________________________*/
#endregion File Info Header

using System.Collections.Generic;

using MorganStanley.MSDotNet.MSNet;
using MorganStanley.MSDotNet.MSSoap.Client;

namespace MorganStanley.IED.Concord.ServiceDispatcher.Configuration
{
  /// <summary>
  /// Represents service specific settings
  /// </summary>
  /// <remarks>
  /// You may implement this interface to customize a service's configuration, 
  /// or inherit from <see cref="ServiceSetting"/>
  /// </remarks>
  public interface IServiceSetting
  {
    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="XmlServiceTypeInfo.Application"]/*'/>
    string Application { get; }

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="XmlServiceTypeInfo.Service"]/*'/>
    string Service { get; }

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="XmlServiceTypeInfo.ServiceID"]/*'/>
    string ServiceID { get; }

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="XmlServiceTypeInfo.Version"]/*'/>
    string Version { get; }

    /// <summary>
    /// Gets the <see cref="ConnectionInfo"/>
    /// </summary>
    ConnectionInfo Connection { get; }

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="XmlServiceTypeInfo.Timeout"]/*'/>
    long Timeout { get; }

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="XmlServiceTypeInfo.Actions"]/*'/>
    ICollection<ActionInfo> Actions { get; }

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="XmlServiceTypeInfo.ValidationEnabled"]/*'/>
    bool ValidationEnabled { get; }

    /// <summary>
    /// ISoapSender provides a generic concept for making a web servie with whatever transport, taking
    /// in a generic SoapMessage and return a SoapMessage
    /// </summary>
    ISoapSender GetServiceSender { get; }

    /// <summary>
    /// Gets the <see cref="ISoapOutputTransport"/> for the service
    /// </summary>
    /// <param name="loop_">the transport </param>
    /// <returns></returns>
    ISoapOutputTransport GetTransport(IMSNetLoop loop_);

    /// <summary>
    /// Returns an array of <see cref="XmlServiceExtension"/>s applicable for the specified <paramref name="actionInfo_"/>
    /// </summary>
    /// <param name="actionInfo_">The actionInfo for which to get extensions</param>
    /// <returns>An array of <see cref="XmlServiceExtension"/></returns>
    XmlServiceExtension[] GetExtensions(ActionInfo actionInfo_);

    /// <summary>
    /// get RequestResponseClient
    /// </summary>
    /// <returns></returns>
    IRequestResponseCall GetRequestResponseClient(ISoapOutputTransport transport_);
  }
}