﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using MorganStanley.IED.Concord.Logging;
using MorganStanley.MSDotNet.MSNet;
using MorganStanley.MSDotNet.MSSoap.Client;

namespace MorganStanley.IED.Concord.ServiceDispatcher.Configuration
{
  /// <summary>
  /// Implements <see cref="IServiceSetting"/> by acting as a wrapper around <see cref="XmlServiceTypeInfo"/>
  /// </summary>
  /// <remarks>
  /// You may wish to inherit from this class to customize service configuration at runtime. 
  /// For example, you can override <see cref="GetExtensions"/> to instantiate extensions 
  /// using a IoC container such as Unity or Spring.net
  /// </remarks>
  public class ServiceSetting : IServiceSetting
  {
    #region Declarations

    private readonly ICollection<ActionInfo> _actions;
    private readonly XmlServiceTypeInfo _serviceTypeInfo;

    #endregion

    #region Constructors

    ///<summary>
    /// Initializes a new instance of the ServiceSetting class.
    ///</summary>
    ///<param name="serviceTypeInfo_">The <see cref="XmlServiceTypeInfo"/> to wrap</param>
    public ServiceSetting(XmlServiceTypeInfo serviceTypeInfo_)
    {
      _serviceTypeInfo = serviceTypeInfo_;
      _actions = new ReadOnlyCollection<ActionInfo>(_serviceTypeInfo.Actions);

      if (_serviceTypeInfo.Application != null &&
          _serviceTypeInfo.Application.Trim().Length > 0)
      {
        Application = _serviceTypeInfo.Application;
      }
      else if (ServiceManager.Service.Application != null &&
               ServiceManager.Service.Application.Trim().Length > 0)
      {
        Application = ServiceManager.Service.Application;
      }
    }

    #endregion

    public XmlServiceTypeInfo ServiceTypeInfo
    {
      get { return _serviceTypeInfo; }
    }

    #region IServiceSetting Members

    /// <summary>
    /// Gets the <see cref="ISoapOutputTransport"/> for the service
    /// </summary>
    /// <param name="loop_">the transport </param>
    /// <returns></returns>
    public virtual ISoapOutputTransport GetTransport(IMSNetLoop loop_)
    {
      return TransportManager.GetTransport(loop_, _serviceTypeInfo.Connection);
    }

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="XmlServiceTypeInfo.Application"]/*'/>
    public string Application { get; protected set; }

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="XmlServiceTypeInfo.Service"]/*'/>
    public virtual string Service
    {
      get { return _serviceTypeInfo.Service; }
    }

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="XmlServiceTypeInfo.ServiceID"]/*'/>
    public virtual string ServiceID
    {
      get { return _serviceTypeInfo.ServiceID; }
    }

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="XmlServiceTypeInfo.Version"]/*'/>
    public virtual string Version
    {
      get { return _serviceTypeInfo.Version; }
    }

    /// <summary>
    /// Gets the <see cref="ConnectionInfo"/>
    /// </summary>
    public virtual ConnectionInfo Connection
    {
      get { return _serviceTypeInfo.Connection; }
    }

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="XmlServiceTypeInfo.Timeout"]/*'/>
    public virtual long Timeout
    {
      get { return _serviceTypeInfo.Timeout; }
    }

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="XmlServiceTypeInfo.ValidationEnabled"]/*'/>
    public virtual bool ValidationEnabled
    {
      get { return _serviceTypeInfo.ValidationEnabled; }
    }

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="XmlServiceTypeInfo.Actions"]/*'/>
    public virtual ICollection<ActionInfo> Actions
    {
      get { return _actions; }
    }

    /// <summary>
    /// Returns an array of <see cref="XmlServiceExtension"/>s applicable for the specified <paramref name="actionInfo_"/>
    /// </summary>
    /// <remarks>
    /// This method aggregates extensions defined in the service and the specified <paramref name="actionInfo_"/>. 
    /// If <see cref="ValidationEnabled"/> is <c>true</c> then the <see cref="ValidationExtension"/> is added as well.
    /// </remarks>
    /// <param name="actionInfo_">The actionInfo for which to get extensions</param>
    /// <returns>An array of <see cref="XmlServiceExtension"/></returns>
    public virtual XmlServiceExtension[] GetExtensions(ActionInfo actionInfo_)
    {
      var extensionTypes = new List<ServiceExtensionType>();

      // Loop through all service defined extensions and add or remove them from the arraylist
      for (int index = 0; index < _serviceTypeInfo.ServiceExtensions.Count; index++)
      {
        if (_serviceTypeInfo.ServiceExtensions[index].Action == "add")
        {
          extensionTypes.Add(_serviceTypeInfo.ServiceExtensions[index]);
        }
        else
        {
          extensionTypes.Remove(_serviceTypeInfo.ServiceExtensions[index]);
        }
      }

      // Loop through all action defined extensions and add or remove them from the arraylist
      for (int index = 0; index < actionInfo_.ServiceExtensions.Count; index++)
      {
        if (actionInfo_.ServiceExtensions[index].Action == "add")
        {
          extensionTypes.Add(actionInfo_.ServiceExtensions[index]);
        }
        else
        {
          extensionTypes.Remove(actionInfo_.ServiceExtensions[index]);
        }
      }
      // Loop through all extensions and instantiate them
      XmlServiceExtension[] extensions;
      int offset = 0;

      // Add the required ValidationExtension
      if (_serviceTypeInfo.ValidationEnabled &&
          actionInfo_.Validators != null)
      {
        extensions = new XmlServiceExtension[extensionTypes.Count + 1];
        extensions[0] = new ValidationExtension();
        offset = 1;
      }
      else
      {
        extensions = new XmlServiceExtension[extensionTypes.Count];
      }

      for (int index = 0; index < extensionTypes.Count; index++)
      {
        Type type = extensionTypes[index].Type;

        if (type != null)
        {
          try
          {
            object instance = Activator.CreateInstance(type);

            if (instance != null)
            {
              extensions[index + offset] = ((XmlServiceExtension) instance);
            }
          }
          catch (Exception e_)
          {
            #region Logging

            if (Log.IsLogLevelEnabled(LogLevel.Error))
            {
              LogLayer.Error.SetLocation("ServiceSetting", "GetExtensions").WriteFormat(
                Res.EXCEPTION_EXTENSION_CREATE_FAILED, type.FullName);
            }

            #endregion Logging

            // Caller Inform
            throw new XmlServiceDispatchException(
              String.Format(Res.EXCEPTION_EXTENSION_CREATE_FAILED, type.FullName), e_);
          }
        }
      }

      return extensions;
    }

    public virtual IRequestResponseCall GetRequestResponseClient(ISoapOutputTransport transport_)
    {
      return TransportManager.GetRequestResponseClient(transport_);
    }

    public virtual ISoapSender GetServiceSender
    {
      get { return new SoapSender(this); }
    }

    #endregion
  }
}