#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/Concord.ServiceDispatcher/Configuration/ServiceManager.cs#5 $
  $DateTime: 2014/02/07 09:03:56 $
    $Change: 865668 $
    $Author: caijin $
_____________________________________________________________________________*/
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Xml;
using System.IO;
using System.Text;

using MorganStanley.IED.Concord.Logging;
using MorganStanley.MSDotNet.MSSoap.SSL;

namespace MorganStanley.IED.Concord.ServiceDispatcher.Configuration
{
	#region Exceptions
	/// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="ServiceConfigException"]/*'/>
	[Serializable]
	public class ServiceConfigException : XmlServiceDispatchException
	{
		/// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="ServiceConfigException.ServiceConfigException"]/*'/>
		public ServiceConfigException(string message_, Exception innerException_)
			: base(message_, innerException_)
		{
			//Auto Log?
		}

		/// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="ServiceConfigException.ServiceConfigException1"]/*'/>
		public ServiceConfigException(string message_)
			: base(message_)
		{
			//Auto Log?
		}
	}
	#endregion Exceptions

	/// <summary>
	/// TODO: Documentation
	/// </summary>
	internal class ServiceManager : IEnumerable<KeyValuePair<string, IServiceSetting>>
	{
		#region Constants
		#endregion Constants

		#region Enums
		#endregion Enums

		#region Declarations
		private string _encoding;
		private string _application;

		private Dictionary<string, IServiceSetting> _serviceSettings = new Dictionary<string, IServiceSetting>();

		private object _lock = new object();
		#endregion Declarations

		#region Constructors
		static ServiceManager()
		{
			Service = new ServiceManager();
		}

		private ServiceManager()
		{
		}
		#endregion Constructors

		#region Public Properties
		public static ServiceManager Service;

		public string Application
		{
			get { return _application; }
			set { _application = value; }
		}

		public string Encoding
		{
			get { return _encoding; }
			set { _encoding = value; }
		}
		#endregion Public Properties

		#region Public Methods
		internal static void LoadServices(System.String fileName_)
		{
			#region Logging
			if (Log.IsLogLevelEnabled(LogLevel.Debug))
			{
				LogLayer.Debug.SetLocation("ServiceManager", "LoadServices").WriteFormat("Loading service configuration, File='{0}'", fileName_);
			}
			#endregion Logging

			if (!File.Exists(fileName_))
			{
				#region Logging
				if (Log.IsLogLevelEnabled(LogLevel.Error))
				{
					LogLayer.Error.SetLocation("ServiceManager", "LoadServices").WriteFormat(Res.EXCEPTION_SERVICECONFIG_NOT_FOUND, fileName_);
				}
				#endregion Logging

				throw new ServiceConfigException(String.Format(Res.EXCEPTION_SERVICECONFIG_NOT_FOUND, fileName_));
			}
			using (FileStream fs = new FileStream(fileName_, FileMode.Open, FileAccess.Read, FileShare.Read))
			{
				LoadServices(fs);
			}
		}

		internal static void LoadServices(XmlReader reader_)
		{
			#region Logging
			if (Log.IsLogLevelEnabled(LogLevel.Debug))
			{
				LogLayer.Debug.SetLocation("ServiceManager", "LoadServices").Write("Loading service configuration from an instance of XmlReader.");
			}
			#endregion Logging

			if (reader_ == null)
			{
				throw new ArgumentNullException("reader_");
			}

			ServiceDispatcherRegistry sdr = null;

			try
			{
				System.Xml.XmlDocument dom_ = new System.Xml.XmlDocument();
				dom_.Load(reader_);
				Deserialize(dom_, ref sdr);

			}
			catch (Exception e_)
			{
				#region Logging
				if (Log.IsLogLevelEnabled(LogLevel.Error))
				{
					LogLayer.Error.SetLocation("ServiceManager", "LoadServices").Write("Unable to parse the configuration file.", e_);
				}
				#endregion Logging

				throw new ServiceConfigException(Res.EXCEPTION_SERVICECONFIG_ERROR_DESERIALIZE, e_);
			}

			Service.SetupServices(sdr);
		}

		internal static void LoadServices(System.Uri uri_)
		{
			#region Logging
			if (Log.IsLogLevelEnabled(LogLevel.Debug))
			{
				LogLayer.Debug.SetLocation("ServiceManager", "LoadServices").WriteFormat("Loading service configuration, Uri='{0}'", uri_.ToString());
			}
			#endregion Logging

			XmlUrlResolver resolver = new XmlUrlResolver();
			Uri fulluri = null;

			try
			{
				fulluri = resolver.ResolveUri(uri_, string.Empty);
			}
			catch (UriFormatException ufe_)
			{
				#region Logging
				if (Log.IsLogLevelEnabled(LogLevel.Error))
				{
					LogLayer.Error.SetLocation("ServiceManager", "LoadServices").Write("Invalid URL", ufe_);
				}
				#endregion Logging

				throw new ServiceConfigException(Res.EXCEPTION_SERVICECONFIG_INVALID_URL_FORMAT, ufe_);
			}

			try
			{
				Stream s = (Stream)resolver.GetEntity(fulluri, null, typeof(Stream));
				LoadServices(s);
			}
			catch (XmlException xe_)
			{
				#region Logging
				if (Log.IsLogLevelEnabled(LogLevel.Error))
				{
					LogLayer.Error.SetLocation("ServiceManager", "LoadServices").Write("Unable to read the config file", xe_);
				}
				#endregion Logging

				throw new ServiceConfigException(Res.EXCEPTION_SERVICECONFIG_ERROR_READ, xe_);
			}
			catch (UriFormatException ufe_)
			{
				#region Logging
				if (Log.IsLogLevelEnabled(LogLevel.Error))
				{
					LogLayer.Error.SetLocation("ServiceManager", "LoadServices").Write("Invalid URL", ufe_);
				}
				#endregion Logging

				throw new ServiceConfigException(Res.EXCEPTION_SERVICECONFIG_INVALID_URL, ufe_);
			}
		}

		internal static void LoadServices(System.IO.Stream inStream_)
		{
			#region Logging
			if (Log.IsLogLevelEnabled(LogLevel.Debug))
			{
				LogLayer.Debug.SetLocation("ServiceManager", "LoadServices").Write("Loading service configuration from a stream");
			}
			#endregion Logging

			ServiceDispatcherRegistry sdr = null;

			if (inStream_.CanSeek)
			{
				inStream_.Position = 0;
			}

			try
			{

				XmlDocument dom_ = new System.Xml.XmlDocument();
				dom_.Load(inStream_);
				Deserialize(dom_, ref  sdr);
			}
			catch (Exception e_)
			{
				#region Logging
				if (Log.IsLogLevelEnabled(LogLevel.Error))
				{
					LogLayer.Error.SetLocation("ServiceManager", "LoadServices").Write("Unable to parse the configuration file", e_);
				}
				#endregion Logging

				throw new ServiceConfigException(Res.EXCEPTION_SERVICECONFIG_ERROR_DESERIALIZE, e_);
			}

			Service.SetupServices(sdr);
		}

		public IEnumerator<KeyValuePair<string, IServiceSetting>> GetEnumerator()
		{
			return new List<KeyValuePair<string, IServiceSetting>>(_serviceSettings).GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		//Note the search is now done on the AliasName
		public IServiceSetting this[string aliasName_]
		{
			get
			{
				#region Logging
				if (Log.IsLogLevelEnabled(LogLevel.Debug))
				{
					LogLayer.Debug.SetLocation("ServiceManager", "Item").WriteFormat("Retrieving IServiceSetting, AliasName='{0}'", aliasName_);
				}
				#endregion Logging
				lock (_lock)
				{
					IServiceSetting serviceSetting;

					_serviceSettings.TryGetValue(aliasName_, out serviceSetting);
					return serviceSetting;
				}
			}
			set
			{
				#region Logging
				if (Log.IsLogLevelEnabled(LogLevel.Debug))
				{
					LogLayer.Debug.SetLocation("ServiceManager", "Item").WriteFormat("Setting IServiceSetting, AliasName='{0}'", aliasName_);
				}
				#endregion Logging
				lock (_lock)
				{
					_serviceSettings[aliasName_] = value;
				}
			}
		}

		internal static void Deserialize(XmlDocument dom_, ref ServiceDispatcherRegistry sdr_)
		{
			if (TransportManager.CloseConnectionsOnConfigurationLoad)
			{
				TransportManager.Clear();
			}

			if (sdr_ == null)
			{
				sdr_ = new Concord.ServiceDispatcher.Configuration.ServiceDispatcherRegistry();
			}

			XmlNamespaceManager nsmgr = new XmlNamespaceManager(dom_.NameTable);
			nsmgr.AddNamespace("CONCORD_SD", ServiceDispatcherRegistry.RUNTIME_CONFIG_NAMESPACE);

			System.Xml.XmlNode main_ = dom_.SelectSingleNode("CONCORD_SD:serviceDispatcherRegistry", nsmgr);

			if (main_ != null)
			{
				#region Encoding Type
				XmlNode element = main_.SelectSingleNode("CONCORD_SD:encodingType", nsmgr);
				if (null != element) sdr_.EncodingType = element.InnerXml;
				#endregion Encoding Type

				#region Application
				element = main_.SelectSingleNode("CONCORD_SD:application", nsmgr);
				if (null != element) sdr_.Application = element.InnerXml;
				#endregion Application

				#region CloseConnectionsOnConfigurationLoad
				element = main_.SelectSingleNode("CONCORD_SD:closeConnectionsOnConfigurationLoad", nsmgr);
				if (null != element)
				{
					sdr_.CloseConnectionsOnConfigurationLoad = bool.Parse(element.InnerXml);
				}
				else
				{
					sdr_.CloseConnectionsOnConfigurationLoad = TransportManager.DEFAULT_CLOSE_CONNECTIONS_ONLOAD;
				}
				#endregion CloseConnectionsOnConfigurationLoad

				#region ConnectOnSend
				element = main_.SelectSingleNode("CONCORD_SD:connectOnSend", nsmgr);
				if (null != element)
				{
					sdr_.ConnectOnSend = bool.Parse(element.InnerXml);
				}
				else
				{
					sdr_.ConnectOnSend = TransportManager.DEFAULT_CONNECT_ON_SEND;
				}
				#endregion ConnectOnSend

				#region DefaultHeartbeatInterval
				element = main_.SelectSingleNode("CONCORD_SD:defaultHeartbeatInterval", nsmgr);
				if (null != element)
				{
					sdr_.DefaultHeartbeatInterval = int.Parse(element.InnerXml);
				}
				else
				{
					sdr_.DefaultHeartbeatInterval = SoapSSLTCPConnection.DEFAULT_HEARTBEAT_INTERVAL_SECONDS;
				}
				#endregion DefaultHeartbeatInterval

				#region DefaultInvokeTimeout
				element = main_.SelectSingleNode("CONCORD_SD:defaultInvokeTimeout", nsmgr);
				if (null != element)
				{
					sdr_.DefaultInvokeTimeout = long.Parse(element.InnerXml);
				}
				else
				{
					sdr_.DefaultInvokeTimeout = TransportManager.DEFAULT_INVOKE_TIMEOUT;
				}
				#endregion DefaultInvokeTimeout

				#region DefaultReconnectTimeout
				element = main_.SelectSingleNode("CONCORD_SD:defaultReconnectTimeout", nsmgr);
				if (null != element)
				{
					sdr_.DefaultReconnectTimeout = int.Parse(element.InnerXml);
				}
				else
				{
					sdr_.DefaultReconnectTimeout = TransportManager.DEFAULT_RECONNECT_TIMEOUT;
				}
				#endregion DefaultReconnectTimeout

				#region EnableHeartbeat
				element = main_.SelectSingleNode("CONCORD_SD:enableHeartbeat", nsmgr);
				if (null != element)
				{
					sdr_.EnableHeartbeat = bool.Parse(element.InnerXml);
				}
				else
				{
					sdr_.EnableHeartbeat = TransportManager.DEFAULT_USEHEARTBEAT;
				}
				#endregion EnableHeartbeat

				#region EstablishConnectionsOnLoad
				element = main_.SelectSingleNode("CONCORD_SD:establishConnectionsOnLoad", nsmgr);
				if (null != element)
				{
					sdr_.EstablishConnectionsOnLoad = bool.Parse(element.InnerXml);
				}
				else
				{
					sdr_.EstablishConnectionsOnLoad = TransportManager.DEFAULT_ESTABLISH_CONNECTIONS_ONLOAD;
				}
				#endregion EstablishConnectionsOnLoad

				#region HandleSyncConnectErrors
				element = main_.SelectSingleNode("CONCORD_SD:handleSyncConnectErrors", nsmgr);
				if (null != element)
				{
					sdr_.HandleSyncConnectErrors = bool.Parse(element.InnerXml);
				}
				else
				{
					sdr_.HandleSyncConnectErrors = TransportManager.DEFAULT_HANDLE_SYNC_CONNECT_ERRORS;
				}
				#endregion HandleSyncConnectErrors

				#region UseAsyncConnects
				element = main_.SelectSingleNode("CONCORD_SD:useAsyncConnects", nsmgr);
				if (null != element)
				{
					sdr_.UseAsyncConnects = bool.Parse(element.InnerXml);
				}
				else
				{
					sdr_.UseAsyncConnects = TransportManager.DEFAULT_USE_ASYNC_CONNECTS;
				}
				#endregion UseAsyncConnects

				//Setup TransportManager
				TransportManager.CloseConnectionsOnConfigurationLoad = sdr_.CloseConnectionsOnConfigurationLoad;
				TransportManager.ConnectOnSend = sdr_.ConnectOnSend;
				TransportManager.DefaultHeartbeatInterval = sdr_.DefaultHeartbeatInterval;
				TransportManager.DefaultInvokeTimeout = sdr_.DefaultInvokeTimeout;
				TransportManager.DefaultReconnectTimeout = sdr_.DefaultReconnectTimeout;
				TransportManager.EnableHeartbeat = sdr_.EnableHeartbeat;
				TransportManager.EstablishConnectionsOnLoad = sdr_.EstablishConnectionsOnLoad;
				TransportManager.HandleSyncConnectErrors = sdr_.HandleSyncConnectErrors;
				TransportManager.UseAsyncConnects = sdr_.UseAsyncConnects;

				#region Glocal Connections
				Hashtable connectionsByName = new Hashtable();
				System.Xml.XmlNodeList connectionsNodeList = main_.SelectNodes("CONCORD_SD:connections/CONCORD_SD:connection", nsmgr);
				if (connectionsNodeList != null)
				{
					ArrayList connections = new ArrayList();

					foreach (XmlNode connectionNode in connectionsNodeList)
					{
						ConnectionInfo connectionInfo = new ConnectionInfo();
						try
						{
							if (connectionNode.Attributes["name"] != null)
							{
								connectionInfo.Name = connectionNode.Attributes["name"].Value;
							}

							if (connectionNode.Attributes["useRequestResponse"] != null)
							{
								connectionInfo.UseRequestResponse = Convert.ToBoolean(connectionNode.Attributes["useRequestResponse"].Value);
							}

							connectionInfo.Transport = (Transport)Enum.Parse(typeof(Transport), connectionNode.Attributes["transport"].Value, true);

							element = connectionNode.SelectSingleNode("CONCORD_SD:url", nsmgr);
							if (null != element) connectionInfo.Url = element.InnerXml;

							element = connectionNode.SelectSingleNode("CONCORD_SD:hostPort", nsmgr);
							if (null != element) connectionInfo.HostPort = element.InnerXml;

							element = connectionNode.SelectSingleNode("CONCORD_SD:backupHostPort", nsmgr);
							if (null != element) connectionInfo.BackupHostPort = element.InnerXml;

							TransportManager.AddConnection(connectionInfo);
							connections.Add(connectionInfo);
							connectionsByName[connectionInfo.Name] = connectionInfo;
						}
						catch (Exception ex)
						{
							LogLayer.Error.SetLocation("ServiceManager", "Deserialize").WriteFormat("Unable to connect to transport with name: {0}, transport type: {1}, Url: {2}, Hostport: {3}, BackupHostPort: {4}, UseRequestResponse: {5}", ex, connectionInfo.Name, connectionInfo.Transport, connectionInfo.Url, connectionInfo.HostPort, connectionInfo.BackupHostPort, connectionInfo.UseRequestResponse);
						}
					}

					sdr_.Connections = (ConnectionInfo[])connections.ToArray(typeof(ConnectionInfo));
				}
				#endregion Global Connections

				#region Services
				var serviceTypeInfos = new List<XmlServiceTypeInfo>();
				System.Xml.XmlNodeList nodeList = main_.SelectNodes("CONCORD_SD:xmlServiceTypes/CONCORD_SD:xmlServiceTypeInfo", nsmgr);
				if (nodeList != null && nodeList.Count > 0)
				{
					foreach (XmlNode node in nodeList)
					{
						string name = null;
						try
						{
							var serviceTypeInfo = new XmlServiceTypeInfo();

							#region Name
							element = node.SelectSingleNode("CONCORD_SD:name", nsmgr);
							if (null != element)
							{
								name = element.InnerXml;
								serviceTypeInfo.Name = name;
							}
							#endregion Name

							#region Aliases

							System.Xml.XmlNodeList aliasNameList =
									node.SelectNodes("CONCORD_SD:aliasNames/CONCORD_SD:aliasName", nsmgr);
							if (null != aliasNameList)
							{
								serviceTypeInfo.AliasNames = new string[aliasNameList.Count];
								for (int i = 0; i < aliasNameList.Count; i++)
								{
									serviceTypeInfo.AliasNames[i] = aliasNameList.Item(i).InnerXml;
								}
							}

							#endregion Aliases

							#region Application

							element = node.SelectSingleNode("CONCORD_SD:application", nsmgr);
							if (null != element) serviceTypeInfo.Application = element.InnerXml;

							#endregion Application

							#region Timeout

							element = node.SelectSingleNode("CONCORD_SD:timeout", nsmgr);
							if (null != element) serviceTypeInfo.Timeout = System.Convert.ToInt32(element.InnerXml);

							#endregion Timeout

							#region Service Connection

							XmlNode connectionNode = node.SelectSingleNode("CONCORD_SD:connection", nsmgr);
							if (null != connectionNode)
							{
								if (connectionNode.Attributes["ref"] != null)
								{
									var connectionName = connectionNode.Attributes["ref"].Value;
									if (!connectionsByName.ContainsKey(connectionName))
									{
										throw new InvalidOperationException(
												string.Format("Unable to find connection name '{0}' for serviceType '{1}'",
																			connectionName, serviceTypeInfo.Name));
									}

									serviceTypeInfo.Connection =
											connectionsByName[connectionNode.Attributes["ref"].Value] as ConnectionInfo;
								}
								else
								{
									Configuration.ConnectionInfo connectionInfo = new Configuration.ConnectionInfo();

									if (connectionNode.Attributes["name"] != null)
									{
										connectionInfo.Name = connectionNode.Attributes["name"].Value;
									}

									if (connectionNode.Attributes["useRequestResponse"] != null)
									{
										connectionInfo.UseRequestResponse =
												Convert.ToBoolean(connectionNode.Attributes["useRequestResponse"].Value);
									}

									connectionInfo.Transport =
											(Transport)
											Enum.Parse(typeof(Transport), connectionNode.Attributes["transport"].Value,
																 true);

									element = connectionNode.SelectSingleNode("CONCORD_SD:url", nsmgr);
									if (null != element) connectionInfo.Url = element.InnerXml;

									element = connectionNode.SelectSingleNode("CONCORD_SD:hostPort", nsmgr);
									if (null != element) connectionInfo.HostPort = element.InnerXml;

									element = connectionNode.SelectSingleNode("CONCORD_SD:backupHostPort", nsmgr);
									if (null != element) connectionInfo.BackupHostPort = element.InnerXml;

									TransportManager.AddConnection(connectionInfo);

									serviceTypeInfo.Connection = connectionInfo;
								}
							}

							#endregion Service Connection

							#region Service Header

							element = node.SelectSingleNode("CONCORD_SD:service", nsmgr);
							if (null != element) serviceTypeInfo.Service = element.InnerXml;

							#endregion Service Header

							#region ServiceID Header

							element = node.SelectSingleNode("CONCORD_SD:serviceID", nsmgr);
							if (null != element) serviceTypeInfo.ServiceID = element.InnerXml;

							#endregion ServiceID Header

							#region Version Header

							element = node.SelectSingleNode("CONCORD_SD:version", nsmgr);
							if (null != element) serviceTypeInfo.Version = element.InnerXml;

							#endregion Version Header

							#region ValidationEnabled

							element = node.SelectSingleNode("CONCORD_SD:validationEnabled", nsmgr);
							if (null != element)
								serviceTypeInfo.ValidationEnabled = System.Convert.ToBoolean(element.InnerXml);

							#endregion ValidationEnabled

							#region ServiceExtensions

							System.Xml.XmlNode extensionList = node.SelectSingleNode("CONCORD_SD:serviceExtensions",
																																			 nsmgr);
							if (extensionList != null && extensionList.ChildNodes.Count > 0)
							{
								serviceTypeInfo.ServiceExtensions = new ServiceExtensionConfig();
								foreach (XmlNode extension in extensionList.ChildNodes)
								{
									if (!string.IsNullOrEmpty(extension.InnerXml))
									{
										var serviceExtensionType = Type.GetType(extension.InnerXml);
										if (serviceExtensionType == null)
										{
											throw new ArgumentNullException("serviceExtensionType", string.Format("The serviceExtensionType {0} with name {1} cannot be found", extension.InnerXml, extension.Name));
										}
										serviceTypeInfo.ServiceExtensions.Add(
												new ServiceExtensionType(serviceExtensionType,
																								 extension.Name.ToLower()));
									}
								}
							}

							#endregion ServiceExtensions

							#region Actions

							System.Xml.XmlNodeList actionList = node.SelectNodes("CONCORD_SD:actions/CONCORD_SD:actionInfo", nsmgr);
							var actions = new List<ActionInfo>();
							if (actionList != null && actionList.Count > 0)
							{
								foreach (XmlNode action in actionList)
								{
									var actionInfo = new ActionInfo();
									element = action.SelectSingleNode("CONCORD_SD:name", nsmgr);
									if (null != element) actionInfo.Name = element.InnerXml;

									extensionList = action.SelectSingleNode("CONCORD_SD:serviceExtensions", nsmgr);
									if (extensionList != null && extensionList.ChildNodes.Count > 0)
									{
										actionInfo.ServiceExtensions = new ServiceExtensionConfig();
										foreach (XmlNode extension in extensionList.ChildNodes)
										{
											if (!string.IsNullOrEmpty(extension.InnerXml))
											{
												var serviceExtensionType = Type.GetType(extension.InnerXml);

												if (serviceExtensionType != null)
												{
													actionInfo.ServiceExtensions.Add(new ServiceExtensionType(serviceExtensionType, extension.Name.ToLower()));
												}
												else
												{
													throw new ArgumentNullException("serviceExtensionType", string.Format("The serviceExtensionType {0} with name {1} cannot be found", extension.InnerXml, extension.Name));
												}
											}
										}
									}

									element = action.SelectSingleNode("CONCORD_SD:requestInfo", nsmgr);
									if (null != element)
									{
										actionInfo.RequestInfo = new ConversionInfo();
										//sdr_.XmlServiceTypes[infoCount].Actions[actionCount].RequestInfo.Xslt = new ConversionInfo.Xslt();
										XmlNode subelement = element.SelectSingleNode("CONCORD_SD:xslt", nsmgr);
										if (null != subelement && null != subelement.InnerXml)
										{
											if (null != subelement.Attributes)
											{
												if (null != subelement.Attributes["type"])
												{
													//Well I guess this is OK since XsltType enum is hardcoded
													string type = subelement.Attributes.GetNamedItem("type").Value;
													switch (type)
													{
														case "Uri":
															actionInfo.RequestInfo.Xslt.Type = XsltType.Uri;
															break;
														case "Inline":
															actionInfo.RequestInfo.Xslt.Type = XsltType.Inline;
															break;
														default:
															actionInfo.RequestInfo.Xslt.Type = XsltType.Uri;
															break;
													}

													actionInfo.RequestInfo.Xslt.Value = subelement.InnerXml;
												}
											}
										}
									}

									element = action.SelectSingleNode("CONCORD_SD:responseInfo", nsmgr);
									if (null != element)
									{
										actionInfo.ResponseInfo = new ConversionInfo();
										//sdr_.XmlServiceTypes[infoCount].Actions[actionCount].RequestInfo.Xslt = new ConversionInfo.Xslt();
										XmlNode subelement = element.SelectSingleNode("CONCORD_SD:xslt", nsmgr);
										if (null != subelement && null != subelement.InnerXml)
										{

											if (null != subelement.Attributes)
											{
												if (null != subelement.Attributes["type"])
												{
													//Well I guess this is OK since XsltType enum is hardcoded
													string type = subelement.Attributes.GetNamedItem("type").Value;
													switch (type)
													{
														case "Uri":
															actionInfo.ResponseInfo.Xslt.Type = XsltType.Uri;
															break;
														case "Inline":
															actionInfo.ResponseInfo.Xslt.Type = XsltType.Inline;
															break;
														default:
															actionInfo.ResponseInfo.Xslt.Type = XsltType.Uri;
															break;
													}


													actionInfo.ResponseInfo.Xslt.Value = subelement.InnerXml;
												}
											}
										}
									}

									XmlNodeList validatorList = action.SelectNodes("CONCORD_SD:validators/CONCORD_SD:validator", nsmgr);
									int validatorCount = 0;
									if (validatorList != null && validatorList.Count > 0)
									{
										actionInfo.Validators = new Validator[validatorList.Count];
										foreach (XmlNode validator in validatorList)
										{
											actionInfo.Validators[validatorCount] = new Validator();
											if (!string.IsNullOrEmpty(validator.InnerXml))
												actionInfo.Validators[validatorCount].Type = Type.GetType(validator.InnerXml);

											validatorCount++;
										}
									}
									actions.Add(actionInfo);
								}
							}
							serviceTypeInfo.Actions = actions.ToArray();

							#endregion Actions

							serviceTypeInfos.Add(serviceTypeInfo);
						}
						catch (Exception ex)
						{
							LogLayer.Error.SetLocation("ServiceManager", "Deserialize").WriteFormat("Unable to create serviceTypeInfo with name: {0}", ex, name);
						}
					}
				}
				sdr_.XmlServiceTypes = serviceTypeInfos.ToArray();
				#endregion Services
			}
		}
		#endregion Public Methods

		#region Protected Methods
		#endregion Protected Methods

		#region Private Helper Methods
		private void SetupServices(ServiceDispatcherRegistry sdr_)
		{
			lock (_lock)
			{
				#region Logging
				if (Log.IsLogLevelEnabled(LogLevel.Debug))
				{
					LogLayer.Debug.SetLocation("ServiceManager", "SetupServices").Write("Configuring services");
				}
				#endregion Logging

				Service._serviceSettings.Clear();
				XsltServiceExtension.ClearCache();
				Service._encoding = sdr_.EncodingType;
				Service._application = sdr_.Application;

				foreach (XmlServiceTypeInfo xs in sdr_.XmlServiceTypes)
				{
					if (xs.AliasNames.Length > 0)
					{
						#region Logging
						if (Log.IsLogLevelEnabled(LogLevel.Debug))
						{
							LogLayer.Debug.SetLocation("ServiceManager", "SetupServices").WriteFormat("Loading Service '{0}'", xs.Name);
						}
						#endregion Logging

						IServiceSetting serviceSetting = new ServiceSetting(xs);

						//TODO There might be multipl e entries with the same name. Display a nice message if that's the case.
						for (int i = 0; i < xs.AliasNames.Length; i++)
						{
							Service._serviceSettings[xs.AliasNames[i]] = serviceSetting;
						}
					}
					else
					{
						#region Logging
						if (Log.IsLogLevelEnabled(LogLevel.Error))
						{
							LogLayer.Error.SetLocation("ServiceManager", "SetupServices").Write("Invalid XmlServiceTypeInfo in configuration file.  All XmlServiceTypeInfo entries must have a valid Alias Name or Name.");
						}
						#endregion Logging

						throw new ServiceConfigException(Res.EXCEPTION_SERVICECONFIG_MISSING_SERVICETYPE);
					}
				}
			}
		}


		#endregion Private Helper Methods

	}
}
