#region File Info Header
  /*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

  $Id: //eai/msdotnet/msgui/trunk/assemblies/Concord.ServiceDispatcher/Configuration/ServiceRegistry.cs#3 $
  $DateTime: 2013/01/18 15:24:18 $
  $Change: 813214 $
  $Author: smulovic $
  _____________________________________________________________________________*/
#endregion

using System;
using System.Collections;
using System.Xml.Serialization;

namespace MorganStanley.IED.Concord.ServiceDispatcher.Configuration
{
  /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="ServiceDispatcherRegistry"]/*'/>
  public sealed class ServiceDispatcherRegistry 
  {
    public static readonly string RUNTIME_CONFIG_NAMESPACE = "http://xml.ms.com/ied/concord/servicedispatcher/configuration/0.4";

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="ServiceDispatcherRegistry.ServiceDispatcherRegistry"]/*'/>
    public ServiceDispatcherRegistry() 
    {
    }

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="ServiceDispatcherRegistry.Application"]/*'/>
    public string Application;

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="ServiceDispatcherRegistry.EncodingType"]/*'/>
    public string EncodingType;

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="ServiceDispatcherRegistry.CloseConnectionsOnConfigurationLoad"]/*'/>
    public bool CloseConnectionsOnConfigurationLoad;

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="ServiceDispatcherRegistry.ConnectOnSend"]/*'/>
    public bool ConnectOnSend;

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="ServiceDispatcherRegistry.DefaultHeartbeatInterval"]/*'/>
    public int DefaultHeartbeatInterval;

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="ServiceDispatcherRegistry.DefaultInvokeTimeout"]/*'/>
    public long DefaultInvokeTimeout;

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="ServiceDispatcherRegistry.DefaultReconnectTimeout"]/*'/>
    public int DefaultReconnectTimeout;

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="ServiceDispatcherRegistry.EnableHeartbeat"]/*'/>
    public bool EnableHeartbeat;

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="ServiceDispatcherRegistry.EstablishConnectionsOnLoad"]/*'/>
    public bool EstablishConnectionsOnLoad;

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="ServiceDispatcherRegistry.HandleSyncConnectErrors"]/*'/>
    public bool HandleSyncConnectErrors;

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="ServiceDispatcherRegistry.UseAsyncConnects"]/*'/>
    public bool UseAsyncConnects;

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="ServiceDispatcherRegistry.XmlServiceTypes"]/*'/>
    public XmlServiceTypeInfo[] XmlServiceTypes;

    public ConnectionInfo[] Connections;
  }

  /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="XmlServiceTypeInfo"]/*'/>
  public sealed class XmlServiceTypeInfo 
  {
    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="XmlServiceTypeInfo.XmlServiceTypeInfo"]/*'/>
    public XmlServiceTypeInfo() 
    {
      ServiceExtensions = new ServiceExtensionConfig();
      Connection = new ConnectionInfo();
      Timeout = TransportManager.DefaultInvokeTimeout;
    }

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="XmlServiceTypeInfo.AliasName"]/*'/>
    public string[] AliasNames;

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="XmlServiceTypeInfo.Name"]/*'/>
    public string Name;

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="XmlServiceTypeInfo.Application"]/*'/>
    public string Application;

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="XmlServiceTypeInfo.Service"]/*'/>
    public string Service;

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="XmlServiceTypeInfo.ServiceID"]/*'/>
    public string ServiceID;

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="XmlServiceTypeInfo.Version"]/*'/>
    public string Version;

    public ConnectionInfo Connection;

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="XmlServiceTypeInfo.Timeout"]/*'/>
    public long Timeout;

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="XmlServiceTypeInfo.ServiceExtensions"]/*'/>
    public ServiceExtensionConfig ServiceExtensions;

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="XmlServiceTypeInfo.ValidationEnabled"]/*'/>
    public bool ValidationEnabled;

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="XmlServiceTypeInfo.Actions"]/*'/>
    public ActionInfo[] Actions;
  }

  /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="ServiceExtensionConfig"]/*'/>
  public sealed class ServiceExtensionConfig : IXmlSerializable
  {
    private ArrayList _extensions = new ArrayList();

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="ServiceExtensionConfig.Count"]/*'/>
    public int Count 
    {
      get { return _extensions.Count; }
    }

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="ServiceExtensionConfig.RemoveAt"]/*'/>
    public void RemoveAt(int x)
    {
      _extensions.RemoveAt(x);

    }

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="ServiceExtensionConfig.Add"]/*'/>
    public void Add(ServiceExtensionType toAdd)
    {
      _extensions.Add(toAdd);

    }

  #region Implementation of IXmlSerializable
    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="ServiceExtensionConfig.WriteXml"]/*'/>
    public void WriteXml(System.Xml.XmlWriter writer)
    {
      throw new NotImplementedException(String.Format(Res.EXCEPTION_SERIALIZE_NOT_SUPPORTED, "ServiceExtensionConfig"));
    }

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="ServiceExtensionConfig.GetSchema"]/*'/>
    public System.Xml.Schema.XmlSchema GetSchema()
    {
      return null;
    }

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="ServiceExtensionConfig.ReadXml"]/*'/>
    public void ReadXml(System.Xml.XmlReader reader)
    {
      while (reader.Read()) 
      {
        if (reader.Name == "serviceExtensions") 
        {
          reader.ReadEndElement();
          break;
        }
        else if (reader.Name == "add" || reader.Name == "remove") 
        {
          string typeName = reader.ReadString();

          Type type = null;

          try 
          {
            type = Type.GetType(typeName);
          } 
          catch (Exception e_) 
          {
            throw new ServiceConfigException(String.Format(Res.EXCEPTION_UNABLE_TO_CREATE_INSTANCE, typeName), e_);
          }

          if (type == null) 
          {
            throw new ServiceConfigException(String.Format(Res.EXCEPTION_UNABLE_TO_CREATE_INSTANCE, typeName));
          }
          else if (!type.IsSubclassOf(typeof(XmlServiceExtension))) 
          {
            throw new ServiceConfigException(String.Format(Res.EXCEPTION_DOES_NOT_DEREIVE_FROM_SERVICEEXTENSION, typeName));
          }

          _extensions.Add(new ServiceExtensionType(type, reader.Name));
        } 
      }
    }
  #endregion

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="ServiceExtensionConfig.Item"]/*'/>
    public ServiceExtensionType this[int index_]
    {
      get { return (ServiceExtensionType) _extensions[index_]; }
    }
  }

  /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="ServiceExtensionType"]/*'/>
  public sealed class ServiceExtensionType 
  {
    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="ServiceExtensionType.ServiceExtensionType"]/*'/>
    public ServiceExtensionType(Type type_, string action_) 
    {
      Type = type_;
      Action = action_;
    }

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="ServiceExtensionType.ServiceExtensionType"]/*'/>
    public ServiceExtensionType() 
    {

    }

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="ServiceExtensionType.Type"]/*'/>
    public Type Type;

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="ServiceExtensionType.Action"]/*'/>
    public string Action;

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="ServiceExtensionType.Equals"]/*'/>
    public override bool Equals(object object_) 
    {
      return ((ServiceExtensionType)object_).Type == Type;
    }

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="ServiceExtensionType.GetHashCode"]/*'/>
    public override int GetHashCode() 
    {
      return Type.GetHashCode();
    }
  }

  /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="ConversionInfo"]/*'/>
  public sealed class ConversionInfo 
  {
    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="ConversionInfo.ConversionInfo"]/*'/>
    public ConversionInfo()
    {
      this.Xslt = new Xslt();
    }

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="ConversionInfo.Xslt"]/*'/>
    public Xslt Xslt;
  }

  /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="XsltType"]/*'/>
  public enum XsltType 
  {
    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="XsltType.Uri"]/*'/>
    Uri = 1,

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="XsltType.Inline"]/*'/>
    Inline = 2
  }

  /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="Xslt"]/*'/>
  public sealed class Xslt 
  {
    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="Xslt.Type"]/*'/>
    public XsltType Type;

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="Xslt.Value"]/*'/>
    public string Value;
  }

  /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="ActionInfo"]/*'/>
  public sealed class ActionInfo 
  {
    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="ActionInfo.ActionInfo"]/*'/>
    public ActionInfo() 
    {
      ServiceExtensions = new ServiceExtensionConfig();
    }

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="ActionInfo.Name"]/*'/>
    public string Name;

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="ActionInfo.RequestInfo"]/*'/>
    public ConversionInfo RequestInfo;

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="ActionInfo.ResponseInfo"]/*'/>
    public ConversionInfo ResponseInfo;

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="ActionInfo.ServiceExtensions"]/*'/>
    public ServiceExtensionConfig ServiceExtensions;

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="ActionInfo.Validators"]/*'/>
    public Validator[] Validators;
  }

  /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="Validator"]/*'/>
  public sealed class Validator : IXmlSerializable
  {
    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="Validator.Type"]/*'/>
    public Type Type;

  #region Implementation of IXmlSerializable
    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="Validator.WriteXml"]/*'/>
    public void WriteXml(System.Xml.XmlWriter writer_)
    {
      throw new NotImplementedException(String.Format(Res.EXCEPTION_SERIALIZE_NOT_SUPPORTED, "Validator"));   
    }

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="Validator.GetSchema"]/*'/>
    public System.Xml.Schema.XmlSchema GetSchema()
    {
      return null;
    }

    /// <include file='xmldocs/ServiceRegistry.cs.xml' path='doc/doc[@for="Validator.ReadXml"]/*'/>
    public void ReadXml(System.Xml.XmlReader reader_)
    {
      string typeName = reader_.ReadElementString();

      if (typeName != null && typeName.Length > 0) 
      {
        try 
        {
          Type = Type.GetType(typeName);
        } 
        catch (Exception e_) 
        {
          // Caller Inform
          throw new ServiceConfigException(String.Format(Res.EXCEPTION_UNABLE_TO_CREATE_INSTANCE, typeName), e_);
        }

        if (Type == null) 
        {
          throw new ServiceConfigException(String.Format(Res.EXCEPTION_UNABLE_TO_CREATE_INSTANCE, typeName));
        } 
        else if (Type.GetInterface("IValidator", true) == null)
        {
          throw new ServiceConfigException(String.Format(Res.EXCEPTION_DOES_NOT_IMPLEMENT_IVALIDATOR, typeName));
        }
      }
    }
  #endregion
  }

  public sealed class ConnectionInfo 
  {
    internal ConnectionInfo() 
    {
    }

    public ConnectionInfo(Transport transport_, string endpoint_, string backup_) 
    {
      Transport = transport_;

      if (transport_ == Transport.Http || transport_ == Transport.OpticksHttp) 
      {
        Url = endpoint_;
      } 
      else 
      {
        HostPort = endpoint_;
        BackupHostPort = backup_;
      }
    }

    public string Name;
    public Transport Transport;
    public string Url;
    public string HostPort;
    public string BackupHostPort;
    public bool UseRequestResponse = true;
  }

  public enum Transport 
  {
    Http = 0,
    Tcp = 1,
    OpticksHttp = 2,
    KrbTcp = 3,
    KrbSspiTcp = 4,
    KrbAutoTcp = 5,
    KrbWcfTcp = 6
  }
}