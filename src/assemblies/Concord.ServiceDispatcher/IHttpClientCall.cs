#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/Concord.ServiceDispatcher/IHttpClientCall.cs#3 $
  $DateTime: 2013/01/18 15:24:18 $
    $Change: 813214 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion

using MorganStanley.MSDotNet.MSSoap.Client;
using MorganStanley.MSDotNet.MSSoap.Common;

namespace MorganStanley.IED.Concord.ServiceDispatcher
{
  public interface IHttpClientCall
	{
    SoapMessage Send(SoapClientMessage request_, long timeoutMilis);
	}

  public interface IRequestResponseCall
  {
     ISoapResponse SyncSendRequest(ISoapRequest soapRequest_, int timeoutMilis);
  }
}
