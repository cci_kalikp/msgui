#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/Concord.ServiceDispatcher/IValidator.cs#3 $
  $DateTime: 2013/01/18 15:24:18 $
    $Change: 813214 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion

using System;

using MorganStanley.IED.Concord.ServiceDispatcher;

namespace MorganStanley.IED.Concord.ServiceDispatcher
{
  /// <include file='xmldocs/IValidator.cs.xml' path='doc/doc[@for="IValidator"]/*'/>
  public interface IValidator
	{
    /// <include file='xmldocs/IValidator.cs.xml' path='doc/doc[@for="IValidator.Validate"]/*'/>
    void Validate(string actionName_, object[] parameters_);
	}
}
