#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

    $Id: //eai/msdotnet/msgui/trunk/assemblies/Concord.ServiceDispatcher/Transports/OpticksHttp/OpticksHttpClient.cs#3 $
    $DateTime: 2013/01/18 15:24:18 $
    $Change: 813214 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion

using System;
using System.Diagnostics;
using System.Xml;
using System.Net;
using System.IO;
using System.Text;

using MorganStanley.IED.Concord.Logging;
using MorganStanley.IED.Concord.Runtime;

using MorganStanley.MSDotNet.MSNet;
using MorganStanley.MSDotNet.MSSoap.Common;

namespace MorganStanley.IED.Concord.ServiceDispatcher.Transports.OpticksHttp
{
  /// <summary>
  /// Summary description for OpticksHttpClient.
  /// </summary>
  public class OpticksHttpClient : Http.SOAPHttpClient
  {
    #region Constants
    private const string CLASS_NAME = "OpticksHttpClient";
    #endregion Constants

    #region Constructors
    internal OpticksHttpClient(IMSNetLoop loop_, string url_)
      : base(loop_, url_)
    {
    }
    #endregion Constructors

    #region Internal Methods
    public override SoapMessage Send(SoapClientMessage request_, long timeout_)
    {
      #region Logging
      if (Log.IsLogLevelEnabled(LogLevel.Debug))
      {
        LogLayer.Debug.SetLocation(CLASS_NAME, "Send").WriteFormat("{0} Envelope='{1}'", "Request", ((SoapEnvelopeDomImpl)request_.Envelope).XmlDocument.OuterXml);
      }
      #endregion Logging

      SoapMessage response = this.SendRequest(request_, timeout_);

      #region Logging
      if (Log.IsLogLevelEnabled(LogLevel.Debug))
      {
        LogLayer.Debug.SetLocation(CLASS_NAME, "Send").WriteFormat("{0} Envelope='{1}'", "Response", ((SoapEnvelopeDomImpl)response.Envelope).XmlDocument.OuterXml);
      }
      #endregion Logging

      if (response != null)
      {
        response = CreateProperSoapMessage(response);
      }

      if (response != null && response.Fault != null)
      {
        throw new SoapFaultException(response.Fault.FaultString, response.Fault);
      }

      return response;
    }
    #endregion Internal Methods

    #region Private Methods
    private SoapMessage CreateProperSoapMessage(SoapMessage response_)
    {
      #region Logging
      if (Log.IsLogLevelEnabled(LogLevel.Debug))
      {
        LogLayer.Debug.SetLocation(CLASS_NAME, "CreateProperSoapMessage").WriteFormat("Response Before = {1}", (response_.ToString()));
      }
      #endregion Logging

      SoapMessage response = null;

      try
      {
        string responseXML = response_.IOBuffer.ToString();
        int xmlStart = responseXML.IndexOf("<?xml");
        int xmlEnd = responseXML.Length - 1;

        SoapEnvelopeDomImpl envelope = new SoapEnvelopeDomImpl();
        SoapBodyDomImpl soapBody = (SoapBodyDomImpl)envelope.CreateBody();
        XmlDocumentFragment fragment = envelope.XmlDocument.CreateDocumentFragment();
        fragment.InnerXml = responseXML.Substring(xmlStart, xmlEnd - xmlStart); ;

        if (fragment.FirstChild.NodeType == XmlNodeType.XmlDeclaration)
        {
          fragment.RemoveChild(fragment.FirstChild);
        }

        soapBody.XmlElement.AppendChild(fragment);
        response = new SoapMessage((ISoapEnvelope)envelope);
      }
      catch (Exception ex_)
      {
        LogLayer.Error.SetLocation(CLASS_NAME, "CreateProperSoapMessage()").WriteFormat("Error adding SOAP Header to Response: {0}", ex_.ToString());
      }

      return response;
    }
    #endregion Private Methods
  }
}