#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/Concord.ServiceDispatcher/Transports/RequestResponseClient.cs#3 $
  $DateTime: 2013/01/18 15:24:18 $
    $Change: 813214 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion

using MorganStanley.MSDotNet.MSNet;

using MorganStanley.MSDotNet.MSSoap.Client;
using MorganStanley.MSDotNet.MSSoap.Common;
using MorganStanley.MSDotNet.MSSoap.Server;

namespace MorganStanley.IED.Concord.ServiceDispatcher.Transports
{
  /// <summary>
  /// Summary description for RequestResponseClient.
  /// </summary>
  public class RequestResponseClient : SoapRequestResponseClient, IRequestResponseCall
  {
    #region Constructors
    public RequestResponseClient(ISoapInputTransport inputTransport_,
      ISoapOutputTransport outputTransport_,
      IMSNetLoop loop_)
      : base(inputTransport_, outputTransport_, loop_)
    {
    }
    #endregion Constructors

    #region Base Class Overrides
    protected override string GenerateRequestID(ISoapEnvelope envelope_)
    {
      // XmlServiceDispatcher.CreateHeader() already defines a requestid that we want. Use
      // that requestid if it exists, otherwise, use base implementation to generate a
      // new unique RequestID.
      ISoapHeader header = envelope_.CreateHeader();
      if (header != null && header.RequestID == null)
      {
        return base.GenerateRequestID(envelope_);
      }
      else
      {
        return header.RequestID;
      }
    }
    #endregion Base Class Overrides
  }
}