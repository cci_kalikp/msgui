///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
//
// Unpublished copyright.  All rights reserved.  This material contains
// proprietary information that shall be used or copied only within Morgan
// Stanley, except with written permission of Morgan Stanley.
//
///////////////////////////////////////////////////////////////////////////////
// %Z% %W% %I% %E% %U%
using System;
using System.IO;
using System.Xml;

using MorganStanley.MSDotNet.MSNet;
using MorganStanley.MSDotNet.MSSoap;

namespace MorganStanley.MSDotNet.MSSoap.SSL
{
  /// <summary>
  /// Summary description for HeartBeatDelegate.
  /// </summary>
  public delegate void SoapHeartBeatDelegate(object sender_, SoapHeartBeatEventArgs args_);
}
