﻿using MorganStanley.MSDotNet.MSNet;
using MorganStanley.MSDotNet.MSSoap.Common;

namespace MorganStanley.IED.Concord.ServiceDispatcher.Transports.Tcp
{
    /// <summary>
    /// Kerborized connection using WCF
    /// </summary>
    public class SoapKrbWcfTcpConnection : SoapTCPConnection
    {
        private const string CLASS = "SoapKrbWcfTcpConnection";

        public SoapKrbWcfTcpConnection(IMSNetLoop netLoop_, MSNetInetAddress address_, MSNetID id_, bool enableInitialRead_)
            : base(netLoop_, address_, id_, enableInitialRead_)
        {
            Authenticator = CreateAuthenticator();
        }

        public static MSNetAuthLayer CreateAuthenticator()
        {
            MSNetKerberosAuthProperties authProperties = new MSNetKerberosAuthProperties();
            IMSNetAuthMechanism mech = new MSNetKerberosWCFAuthMechanism(authProperties);
            MSNetAuthLayer authLayer = new MSNetAuthLayer();
            authLayer.AddAuthMechanism(mech);
            return authLayer;
        }
    }
}