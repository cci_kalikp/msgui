///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
//
// Unpublished copyright.  All rights reserved.  This material contains
// proprietary information that shall be used or copied only within Morgan
// Stanley, except with written permission of Morgan Stanley.
//
///////////////////////////////////////////////////////////////////////////////
// %Z% %W% %I% %E% %U%
using System;
using MorganStanley.MSDotNet.MSNet;

namespace MorganStanley.MSDotNet.MSSoap.SSL
{
  /// <summary>
  /// Summary description for HeartBeatEventArgs.
  /// </summary>
  public class SoapHeartBeatEventArgs : MSNetEventArgs
  {
    public SoapHeartBeatEventArgs(MSNetID id_) : base(id_)
    {
    }
  }
}
