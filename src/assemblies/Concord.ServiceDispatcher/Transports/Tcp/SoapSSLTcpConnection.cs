#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2007 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/Concord.ServiceDispatcher/Transports/Tcp/SoapSSLTcpConnection.cs#4 $
  $DateTime: 2014/02/09 20:49:49 $
    $Change: 865807 $
    $Author: caijin $
_____________________________________________________________________________*/
#endregion File Info Header

using System;
using System.IO;
using System.Xml;
using MorganStanley.IED.Concord.ServiceDispatcher;
using MorganStanley.MSDotNet.MSNet;
using MorganStanley.MSDotNet.MSNet.SSL;
using MorganStanley.MSDotNet.MSSoap.Common;
using Log = MorganStanley.MSDotNet.MSLog;

namespace MorganStanley.MSDotNet.MSSoap.SSL
{
  /// <summary>
  /// Summary description for SoapSSLTCPConnection.
  /// </summary>
    public class SoapSSLTCPConnection : MSNetProtocolSSLTCPConnection, Server.ISoapInputTransport, Client.ISoapOutputTransport, IHeartBeatConnection
  {
    public static readonly long TIMEOUT_VALUE_NONE  = -1;
    public static readonly int DEFAULT_HEARTBEAT_INTERVAL_SECONDS = 30;
    public static readonly string HB_REQUEST_ACTION = "SimplePingRequest";
    public static readonly string HB_RESPONSE_ACTION = "SimplePingResponse";
    public static readonly string NS_HB_URI = "http://xml.msdw.com/ns/appmw/soap/1.0/ping";
    
    public event SoapHeartBeatDelegate OnHeartBeat;

    protected long m_timeoutDelay = TIMEOUT_VALUE_NONE;  
    protected object m_timeoutCallbackId = null;
    protected MSNetTimerDelegate m_timeoutListener = null;
    // Until the server side is implemented this will be internal
    internal SoapTransportContext m_transportContext = null;

    protected bool m_hbEnabled = false;
    protected int m_hbInterval = DEFAULT_HEARTBEAT_INTERVAL_SECONDS;
    protected object m_hbObject = new object();
    protected MSNetTimerDelegate m_hbTimerCb;
    protected MSNetVoidObjectDelegate m_syncedHBNotify;
  
    protected class RequestAsyncOperation : MSNetAsyncOperation
    {
      public SoapMessage m_response;
      public SoapMessage m_request;
      public MSNetStringMessage m_responseMsg;

      public bool m_readingEnabled;
      
      public RequestAsyncOperation(IMSNetLoop invoker_,
        AsyncCallback userCallback_,
        object state_)
        : base(invoker_, userCallback_, state_)
      {
      }
    }

    protected class RequestAsyncState
    {
      public SoapMessage m_request;
      public MSNetStringMessage m_response;
      public RequestAsyncOperation m_asyncop;
      public bool m_readingEnabled;
    }
    
    public SoapSSLTCPConnection(IMSNetLoop loop_,
      MSNetInetAddress address_,
      MSNetID id_,
      IMSNetProtocol protocol_,
      MSNetServerCertificateValidator serverAuthContext_,
      int cacheSize_,
      TimeSpan reconnectTimespan_
      ) : base(loop_, address_, id_, protocol_, serverAuthContext_, cacheSize_, reconnectTimespan_)
    {
      base.OnError += new MSNetErrorDelegate(this.ErrorCallback);
      base.OnUnSynchronisedReadEvent += new MSNetReadDelegate(this.ReadCallback);
      m_hbTimerCb = new MSNetTimerDelegate(HBTimerCallback);
      m_syncedHBNotify = new MSNetVoidObjectDelegate(this.SyncedHBNotify);
    }

    public bool HeartBeatEnabled
    {
      get
      {
        return m_hbEnabled;
      }
    }

    public void DisableHeartBeat()
    {
      lock(this)
      {
        if (m_hbEnabled)
        {
          MSNetLoopPoolImpl.Instance().CancelCallback(m_hbObject);
        }
      }
    }

    public void EnableHeartBeat()
    {
      lock(this)
      {
        if (!m_hbEnabled)
        {
          m_hbEnabled = true;
          if (this.IsEstablished) 
          {
            Common.SoapMessage hbMsg = new Common.SoapMessage(new Common.SoapIOBuffer(CreateHBMsg()));
            this.BeginSendMessage(hbMsg, null, null);
          }
          m_hbObject = MSNetLoopPoolImpl.Instance().CallbackRepeatedly(m_hbInterval * 1000, m_hbTimerCb);
        }
      }
    }

    protected virtual byte[] CreateHBMsg()
    {
      MemoryStream stream = new MemoryStream(512);
      XmlWriter writer = new XmlTextWriter(stream, System.Text.Encoding.ASCII);
      
      //soap env
      writer.WriteStartElement(Common.SoapConstants.NS_PRE_SOAP_ENV, Common.SoapConstants.ELEM_ENVELOPE, Common.SoapConstants.NS_URI_SOAP_ENV);
      
      //header
      writer.WriteStartElement(Common.SoapConstants.NS_PRE_SOAP_ENV, Common.SoapConstants.ELEM_HEADER, Common.SoapConstants.NS_URI_SOAP_ENV);
      //timestamp
      writer.WriteStartElement(Common.SoapConstants.NS_PRE_MSDW_HEADER, Common.SoapConstants.ELEM_HEADER_TIMESTAMP, Common.SoapConstants.NS_URI_MSDW_HEADER);
      writer.WriteString(Convert.ToInt64((DateTime.UtcNow - SoapConstants.TIMESTAMP_REF_TIME).TotalMilliseconds).ToString());
      //end timestamp
      writer.WriteEndElement();
      //end header
      writer.WriteEndElement();
      
      //body
      writer.WriteStartElement(Common.SoapConstants.NS_PRE_SOAP_ENV, Common.SoapConstants.ELEM_BODY, Common.SoapConstants.NS_URI_SOAP_ENV);
      //hb request
      writer.WriteStartElement(HB_REQUEST_ACTION, NS_HB_URI);
      //end hb request
      writer.WriteEndElement();
      //end body
      writer.WriteEndElement();
      //end soap env
      writer.WriteEndElement();
      writer.Flush();
      byte[] bytes = new byte[(int)stream.Length];
      Array.Copy(stream.GetBuffer(), 0, bytes, 0, bytes.Length);
      return bytes;
    }

    void HBTimerCallback(object timer_, MSNetTimerEventArgs args_)
    {
      if (this.IsEstablished) 
      {
        Common.SoapMessage hbMsg = new Common.SoapMessage(new Common.SoapIOBuffer(CreateHBMsg()));
        this.BeginSendMessage(hbMsg, null, null);
      }
    }

    public int HeartBeatIntervalSeconds
    {
      set
      {
        m_hbInterval = value;
      }
      get
      {
        return m_hbInterval;
      }
    }

    /// <summary>
    /// Check to see if the SOAPTransportContext has been configured
    /// to enabled idle/inactivity timeouts on this connection.
    /// </summary>
    internal void InitTimeoutDelay(SoapTransportContext context_)
    {
      long configTimeout = m_transportContext.TranportOptions.GetIdleTimeout();
      if (configTimeout != -1)
      {
        m_timeoutDelay = configTimeout;
        CheckInitTimeoutListener();
      }
      else m_timeoutDelay = TIMEOUT_VALUE_NONE;
    }

    protected override void Dispose(bool explicitDispose_)
    {
      base.Dispose(explicitDispose_);
      if (explicitDispose_)
      {
        CheckCancelTimeoutListener();
        Log.MSLog.Debug().Location("SoapSSLTCPConnection","Destroy")
          .Append("Destroyed SoapSSLTCPConnection with context: ")
          .Append(m_transportContext)
          .SendQuietly();
      }
    }
    
    
    #region ISoapAbstractTransport implementation    
    /// <summary>
    /// <see cref="MorganStanley.MSDotNet.MSSoap.Common.ISoapAbstractTransport"/>
    /// implementation
    /// </summary>
    /// <remarks>
    /// See
    /// <see cref="MorganStanley.MSDotNet.MSSoap.Common.ISoapAbstractTransport.Init"/>
    /// for more information on this method
    /// </remarks>
    void ISoapAbstractTransport.Init(SoapTransportContext context_)
    {
      m_transportContext = context_;
      m_transportContext.InputTransport = this;
      m_transportContext.OutputTransport = this;
      
      InitTimeoutDelay(context_);

      Log.MSLog.Debug().Location("SoapTCPConnection","Init").
        Append("Initialized SoapTCPConnection with context: " + m_transportContext).
        SendQuietly();
    }

    void ISoapAbstractTransport.Destroy()
    {
    }
    
    /// <summary>
    /// <see cref="MorganStanley.MSDotNet.MSSoap.Common.ISoapAbstractTransport"/>
    /// implementation
    /// </summary>
    /// <remarks>
    /// See
    /// <see cref="MorganStanley.MSDotNet.MSSoap.Common.ISoapAbstractTransport.Start"/>
    /// for more information on this method
    /// </remarks>
    void ISoapAbstractTransport.Start()
    {
      BeginConnect(null, null);

      Log.MSLog.Debug().Location("SoapTCPConnection","Start").
        Append("Started SoapTCPConnection with context: " + m_transportContext).
        SendQuietly();    
    }
  
    /// <summary>
    /// <see cref="MorganStanley.MSDotNet.MSSoap.Common.ISoapAbstractTransport"/>
    /// implementation
    /// </summary>
    /// <remarks>
    /// See
    /// <see cref="MorganStanley.MSDotNet.MSSoap.Common.ISoapAbstractTransport.Stop"/>
    /// for more information on this method
    /// </remarks>
    void ISoapAbstractTransport.Stop()
    {
      Close();

      Log.MSLog.Debug().Location("SoapTCPConnection","Stop").
        Append("Stopped SoapTCPConnection with context: " + m_transportContext).
        SendQuietly();    
    }

    /// <value>
    /// <see cref="MorganStanley.MSDotNet.MSSoap.Common.ISoapAbstractTransport"/>
    /// implementation
    /// </value>
    /// <remarks>
    /// See
    /// <see cref="MorganStanley.MSDotNet.MSSoap.Common.ISoapAbstractTransport.IsStarted"/>
    /// for more information on this property
    /// </remarks>
    bool ISoapAbstractTransport.IsStarted
    {
      get
      {
        return IsEstablished;
      }
    }
  
    /// <value>
    /// <see cref="MorganStanley.MSDotNet.MSSoap.Common.ISoapAbstractTransport"/>
    /// implementation
    /// </value>
    /// <remarks>
    /// See
    /// <see cref="MorganStanley.MSDotNet.MSSoap.Common.ISoapAbstractTransport.TransportContext"/>
    /// for more information on this property
    /// </remarks>
    SoapTransportContext ISoapAbstractTransport.TransportContext
    {
      get
      {
        return m_transportContext;
      }
    }
    #endregion    

    #region ISOAPInputTransport implementation
    protected Server.RequestCallback m_requestCallback;
    protected Server.MessageCallback m_messageCallback;
    protected Server.ExceptionCallback m_exceptionCallback;
    
    /// <value>
    /// <see cref="MorganStanley.MSDotNet.MSSoap.Server.ISoapInputTransport"/>
    /// implementation
    /// </value>
    /// <remarks>
    /// See
    /// <see cref="MorganStanley.MSDotNet.MSSoap.Server.ISoapInputTransport.RequestHandler"/>
    /// for more information on this property
    /// </remarks>
    public Server.RequestCallback RequestHandler
    {
      get
      {
        return m_requestCallback;
      }
      set
      {
        m_requestCallback = value;
      }
    }
    
    /// <value>
    /// <see cref="MorganStanley.MSDotNet.MSSoap.Server.ISoapInputTransport"/>
    /// implementation
    /// </value>
    /// <remarks>
    /// See
    /// <see cref="MorganStanley.MSDotNet.MSSoap.Server.ISoapInputTransport.MessageHandler"/>
    /// for more information on this property
    /// </remarks>
    public Server.MessageCallback MessageHandler
    {
      get
      {
        return m_messageCallback;
      }
      set
      {
        m_messageCallback = value;
      }
    }

    /// <value>
    /// <see cref="MorganStanley.MSDotNet.MSSoap.Server.ISoapInputTransport"/>
    /// implementation
    /// </value>
    /// <remarks>
    /// See
    /// <see cref="MorganStanley.MSDotNet.MSSoap.Server.ISoapInputTransport.ExceptionHandler"/>
    /// for more information on this property
    /// </remarks>
    public Server.ExceptionCallback ExceptionHandler
    {
      get
      {
        return m_exceptionCallback;
      }
      set
      {
        m_exceptionCallback = value;
      }
    }
    #endregion
    protected IAsyncResult DoAsyncSend(RequestAsyncOperation operation_)
    {
      try
      {
   
        operation_.m_readingEnabled = IsReadingEnabled;
        if (operation_.m_readingEnabled)
        {
          DisableReading();
        }

        byte[] requestBytes = operation_.m_request.GetXmlBytes();
        MSNetStringMessage requestMsg = new MSNetStringMessage(requestBytes, true);
        //Common.SoapMessageTracer.Trace("SoapSSLTCPConnection", "DoAsyncSend (sending request message)", operation_.m_request, m_transportContext);
        return this.BeginSend(requestMsg, new AsyncCallback(this.DoneSendforSendRequest), operation_);
      }
      finally
      {
        CheckInitTimeoutListener();      
      } 
    }


    /// <summary>
    /// <see cref="MorganStanley.MSDotNet.MSSoap.Client.ISoapOutputTransport"/>
    /// implementation
    /// </summary>
    /// <remarks>
    /// See
    /// <see cref="MorganStanley.MSDotNet.MSSoap.Client.ISoapOutputTransport.BeginSendRequest"/>
    /// for more information on this method
    /// </remarks>
    public IAsyncResult BeginSendRequest(SoapMessage request_, AsyncCallback callback_, object state_)
    {
      throw new NotSupportedException("Use BeginSendMessage");
    }
    
    /// <summary>
    /// <see cref="MorganStanley.MSDotNet.MSSoap.Client.ISoapOutputTransport"/>
    /// implementation
    /// </summary>
    /// <remarks>
    /// See
    /// <see cref="MorganStanley.MSDotNet.MSSoap.Client.ISoapOutputTransport.EndSendRequest"/>
    /// for more information on this method
    /// </remarks>
    public SoapMessage EndSendRequest(IAsyncResult res_)
    {
      throw new NotSupportedException("Use Begin/EndSendMessage");
    }    
    
    protected void DoneSendforSendRequest(IAsyncResult res_)
    {
      RequestAsyncOperation operation = (RequestAsyncOperation)res_.AsyncState;
      try
      {
        this.EndSend(res_);
        operation.m_responseMsg =  new MSNetStringMessage();
        this.BeginRead(operation.m_responseMsg, new AsyncCallback(this.DoneReadforSendRequest), operation);
      }
      catch(Exception e)
      {
        operation.Exception = new SoapException("SoapSSLTCPConnection.DoneSend(): " +
          "Caught Exception: " + e.ToString(), e);
        operation.OperationDoneNotifyUser();
      }
    }
    
    protected void DoneReadforSendRequest(IAsyncResult res_)
    {
      RequestAsyncOperation operation = (RequestAsyncOperation)res_.AsyncState;
      try
      {
        this.EndRead(res_);

        byte[] xmlBytes;
        int offset;
        int length;
        operation.m_responseMsg.GetBytes(out xmlBytes, out offset, out length);
        operation.m_response = new SoapMessage(new SoapIOBuffer(xmlBytes, offset, length));
        if (operation.m_readingEnabled)
        {
          EnableReading();
        }
      }
      catch(Exception e)
      {
        operation.Exception = new SoapException("SoapSSLTCPConnection.DoneSend(): " +
          "Caught Exception: " + e.ToString(), e);
      }
      finally
      {
        operation.OperationDoneNotifyUser();
      }
    }
    
    /// <summary>
    /// <see cref="MorganStanley.MSDotNet.MSSoap.Client.ISoapOutputTransport"/>
    /// implementation
    /// </summary>
    /// <remarks>
    /// See
    /// <see cref="MorganStanley.MSDotNet.MSSoap.Client.ISoapOutputTransport.SyncSendRequest"/>
    /// for more information on this method
    /// </remarks>
    public SoapMessage SyncSendRequest(SoapMessage request_, 
      long millisec_)
    {
      lock(this)
      {
        if (!IsEstablished)
        {
          if (MorganStanley.IED.Concord.ServiceDispatcher.TransportManager.ConnectOnSend) 
          {
            try
            {
              SyncConnect();
            }
            catch(MSNetTimeOutException)
            {
              throw new SoapException("SoapSSLTCPConnection.sendRequest(): " +
                "Connection is not established");
            }
          } 
          else 
          {
            throw new SoapException("Not connected.");
          }
        }
        bool readingEnabled = IsReadingEnabled;
        if (readingEnabled)
        {
          DisableReading();
        }
        try
        {
          byte[] requestBytes = request_.GetXmlBytes();
          MSNetStringMessage requestMsg = new MSNetStringMessage(requestBytes, true);
          try
          {
            //Common.SoapMessageTracer.Trace("SoapSSLTCPConnection", "SyncSendRequest (sending request message)", request_, m_transportContext);
            SyncSend(requestMsg, (int)millisec_);
          }
          catch (MSNetTimeOutException x_)
          {
            throw new Common.SoapTimeoutException("SoapSSLTCPConnection.sendRequest(): " +
              "Caught timeout exception: " + x_);
          }
          MSNetStringMessage responseMsg = new MSNetStringMessage();
          bool isResponseMsg = false;
          while(!isResponseMsg)
          {
            try
            {
              SyncRead(responseMsg, (int)millisec_);

              byte[] xmlBytes;
              int offset;
              int length;
              responseMsg.GetBytes(out xmlBytes, out offset, out length);
              if (CheckHBMsg(xmlBytes, offset, length))
              {
                NotifyHeartBeat();
              }
              else
              {
                isResponseMsg = true;
              }
            }
            catch (MSNetTimeOutException x_)
            {
              throw new Common.SoapTimeoutException("SoapSSLTCPConnection.sendRequest(): " +
                "Caught timeout exception: ", x_);
            }
          }

          byte[] xmlBytes2;
          int offset2;
          int length2;
          responseMsg.GetBytes(out xmlBytes2, out offset2, out length2);
          SoapMessage soapMessage = new SoapMessage(new SoapIOBuffer(xmlBytes2, offset2, length2));
          return soapMessage;
        }
        catch (MSNetException x_)
        {
          throw new SoapException("SoapSSLTCPConnection.sendRequest(): " +
            "Caught MSNetException: " + x_.ToString(), x_);
        }
        finally
        {
          CheckInitTimeoutListener();      
          if (readingEnabled)
          {
            EnableReading();
          }
        }
      }
    }

    /// <summary>
    /// <see cref="MorganStanley.MSDotNet.MSSoap.Client.ISoapOutputTransport"/>
    /// implementation
    /// </summary>
    /// <remarks>
    /// See
    /// <see cref="MorganStanley.MSDotNet.MSSoap.Client.ISoapOutputTransport.SyncSendMessage"/>
    /// for more information on this method
    /// </remarks>
    public void SyncSendMessage(SoapMessage message_, long millisec_)
    {
      lock(this)
      {
        if (!IsEstablished)
        {
          if (MorganStanley.IED.Concord.ServiceDispatcher.TransportManager.ConnectOnSend) 
          {
            try
            {
              SyncConnect();
            }
            catch(MSNetTimeOutException )
            {
              throw new SoapException("SoapSSLTCPConnection.sendRequest(): " +
                "Connection is not established");
            }
          } 
          else 
          {
            throw new SoapException("Not connected.");
          }
        }
        try
        {
          MSNetStringMessage msg = 
            new MSNetStringMessage(message_.GetXmlBytes());
          //Common.SoapMessageTracer.Trace("SoapSSLTCPConnection", "SyncSendMessage (sending request message)", message_, m_transportContext);
          try
          {
            SyncSend(msg, (int)millisec_);
          }
          catch (MSNetTimeOutException x_)
          {
            throw new Common.SoapTimeoutException("SoapSSLTCPConnection.sendMessage(): " +
              "Caught timeout exception: ", x_);
          }
        }
        catch (MSNetException x_)
        {
          throw new SoapException("SoapSSLTCPConnection.sendMessage(): " +
            "Caught MSNetException: " + x_.ToString(), x_);
        }
        finally
        {
          CheckInitTimeoutListener();
        }
      }
    }

    /// <summary>
    /// <see cref="MorganStanley.MSDotNet.MSSoap.Client.ISoapOutputTransport"/>
    /// implementation
    /// </summary>
    /// <remarks>
    /// See
    /// <see cref="MorganStanley.MSDotNet.MSSoap.Client.ISoapOutputTransport.BeginSendMessage"/>
    /// for more information on this method
    /// </remarks>
    public IAsyncResult BeginSendMessage(SoapMessage message_, AsyncCallback callback_,
      object state_)
    {
      lock(this)
      {
        if (!IsEstablished)
        {
          if (MorganStanley.IED.Concord.ServiceDispatcher.TransportManager.ConnectOnSend) 
          {  
            try
            {          
              this.SyncConnect();
            }
            catch(Exception e)
            {
              throw new SoapException("Failed to connect.", e);
            }
          }
          else 
          {
            throw new SoapException("Not connected.");
          }
        }
      }
      try
      {
        MSNetStringMessage msg = new MSNetStringMessage(message_.GetXmlBytes());
        //Common.SoapMessageTracer.Trace("SoapSSLTCPConnection", "BeginSendMessage (sending request message)", message_, m_transportContext);
        return BeginSend(msg, callback_, state_);
      }
      finally
      {
        CheckInitTimeoutListener();      
      }
    }

    /// <summary>
    /// <see cref="MorganStanley.MSDotNet.MSSoap.Client.ISoapOutputTransport"/>
    /// implementation
    /// </summary>
    /// <remarks>
    /// See
    /// <see cref="MorganStanley.MSDotNet.MSSoap.Client.ISoapOutputTransport.EndSendMessage"/>
    /// for more information on this method
    /// </remarks>
    public void EndSendMessage(IAsyncResult res_)
    {
      EndSend(res_);
    }
    
    /// <summary>
    /// <see cref="MorganStanley.MSDotNet.MSSoap.Client.ISoapOutputTransport"/>
    /// implementation
    /// </summary>
    /// <remarks>
    /// See
    /// <see cref="MorganStanley.MSDotNet.MSSoap.Client.ISoapOutputTransport.SyncSendResponse"/>
    /// for more information on this method
    /// </remarks>
    public void SyncSendResponse(SoapMessage response_, long timeout_,
      SoapMessage.AttributeMap requestAttributes_)
    {
      SyncSendMessage(response_, timeout_);
    }

    /// <summary>
    /// <see cref="MorganStanley.MSDotNet.MSSoap.Client.ISoapOutputTransport"/>
    /// implementation
    /// </summary>
    /// <remarks>
    /// See
    /// <see cref="MorganStanley.MSDotNet.MSSoap.Client.ISoapOutputTransport.BeginSendResponse"/>
    /// for more information on this method
    /// </remarks>
    public IAsyncResult BeginSendResponse(SoapMessage response_, 
      SoapMessage.AttributeMap requestAttributes_,
      AsyncCallback callback_,
      object state_)
    {
      return BeginSendMessage(response_,callback_,state_);
    }
    
    /// <summary>
    /// <see cref="MorganStanley.MSDotNet.MSSoap.Client.ISoapOutputTransport"/>
    /// implementation
    /// </summary>
    /// <remarks>
    /// See
    /// <see cref="MorganStanley.MSDotNet.MSSoap.Client.ISoapOutputTransport.EndSendResponse"/>
    /// for more information on this method
    /// </remarks>
    public void EndSendResponse(IAsyncResult res_)
    {
      EndSendMessage(res_);
    }

    /// <value>
    /// The timeout value for the connection to close itself
    /// </value>
    /// <remarks>
    /// If this connection should close itself after some idle time,
    /// set the timeout value to something other than the defined
    /// constant TIMEOUT_VALUE_NONE.
    /// </remarks>
    public long IdleTimeout
    {
      set
      {
        m_timeoutDelay = value;
      }
      get
      {
        return m_timeoutDelay;
      }
    }

    // If idle timeouts have been enabled, setup a callback on this
    // connection's MSNetLoop to invoke a TimeoutListener.  If a
    // TimeoutListener had already been set, it is cancelled.
    protected void CheckInitTimeoutListener()
    {
      if (m_timeoutDelay != TIMEOUT_VALUE_NONE)
      {
        if (m_timeoutListener == null)
        {
          m_timeoutListener = new MSNetTimerDelegate(this.TimeoutListener);
        }
        CheckCancelTimeoutListener();
        m_timeoutCallbackId = 
          NetLoop.CallbackAfterDelay(m_timeoutDelay, m_timeoutListener);
      }
    }
    
    protected void CheckCancelTimeoutListener()
    {
      if (m_timeoutCallbackId != null)
      {
        NetLoop.CancelCallback(m_timeoutCallbackId);
      }
    }
  

    protected void TimeoutListener(object caller_, MSNetTimerEventArgs args_)
    {
      if(this.IsEstablished)
      {
        Log.MSLog.Info().Location("SoapSSLTCPConnection.TimeoutListener","eventOccurred").
          Append("Closing connection due to idle timeout: " +
          this.ToString()).SendQuietly();
        this.Close();
      }
    }

    protected void NotifyHeartBeat()
    {
      m_loop.DispatchCallbackWithState(m_syncedHBNotify, new SoapHeartBeatEventArgs(this.m_id));
    }

    protected void SyncedHBNotify(object o_)
    {
      SoapHeartBeatDelegate onHeartBeat = OnHeartBeat;
      if (onHeartBeat != null)
      {
        onHeartBeat(this, (SoapHeartBeatEventArgs)o_);
      }
    }

    protected virtual bool CheckHBMsg(byte[] xmlBytes, int offset_, int length_)
    {
      MemoryStream stream = new MemoryStream(xmlBytes, offset_, length_);
      XmlReader reader = new XmlTextReader(stream);
      while(reader.Read())
      {
        if (reader.NodeType == XmlNodeType.Element && reader.LocalName == Common.SoapConstants.ELEM_BODY &&
          reader.NamespaceURI == Common.SoapConstants.NS_URI_SOAP_ENV)
        {
          while(reader.Read())
          {
            if (reader.NodeType == XmlNodeType.Element && reader.LocalName == HB_RESPONSE_ACTION &&
              reader.NamespaceURI == NS_HB_URI)
            {
              return true;
            }
          }
        }
      }
      reader.Close();
      return false;
    }

    protected void ReadCallback(object id_, MSNetReadEventArgs message_)
    {
      CheckInitTimeoutListener();     
      byte[] xmlBytes;
      int offset;
      int length;
      ((MSNetStringMessage)message_.Message).GetBytes(out xmlBytes, out offset, out length);
      if (CheckHBMsg(xmlBytes, offset, length))
      {
        NotifyHeartBeat();
      }
      else
      {
        if (m_messageCallback != null)
        {
          SoapMessage soapMessage = new SoapMessage(new SoapIOBuffer(xmlBytes, offset, length));
                
          //Common.SoapMessageTracer.Trace("SoapSSLTCPConnection.NetConnectionListener","ReadCallback (got response message)", soapMessage, m_transportContext);

          if (m_messageCallback != null)
          {
            m_loop.DispatchCallback(new MSNetVoidObjectDelegate(this.SyncInvokeMessageCallback), soapMessage);
          }
        
        }
        else
        {
          Log.MSLog.Warning().Location("SoapSSLTCPConnection.NetConnectionListener","ReadCallback").
            Append("Transport listener is null!").SendQuietly();
        }
      }
    }

    protected void SyncInvokeMessageCallback(object message_)
    {
      m_messageCallback(this, (SoapMessage)message_);
    }
    
    protected void ErrorCallback(object id_, MSNetErrorEventArgs error_)
    {
      if (m_exceptionCallback != null)
      {
        SoapException x = 
          new SoapException("MSNetConnection error callback: " + 
          error_.ToString());
        m_exceptionCallback(this, x);
      }
      else Log.MSLog.Warning().Location("SoapSSLTCPConnection.NetConnectionListener","ErrorCallback").
             Append("Transport listener is null!").SendQuietly();
    }

    public override string ToString()
    {
      return "SoapSSLTCPConnection: name = " + Name + 
        "; context = " + m_transportContext;
    }
  }
}
