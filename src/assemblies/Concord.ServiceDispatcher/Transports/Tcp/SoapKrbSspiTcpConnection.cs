﻿using MorganStanley.MSDotNet.MSNet;
using MorganStanley.MSDotNet.MSSoap.Common;

namespace MorganStanley.IED.Concord.ServiceDispatcher.Transports.Tcp
{
  /// <summary>
  /// Kerborized connection using SSPI, requires Microsoft WSE 3.0 to be installed (\\%DISTSRV%\apps\Microsoft\WebServiceExtensions\3.0\Go\)
  /// </summary>
  public class SoapKrbSspiTcpConnection : SoapTCPConnection
  {
    private const string CLASS = "SoapKrbSspiTcpConnection";

    public SoapKrbSspiTcpConnection(IMSNetLoop netLoop_, MSNetInetAddress address_, MSNetID id_, bool enableInitialRead_)
      : base(netLoop_, address_, id_, enableInitialRead_)
    {
      Authenticator = CreateAuthenticator();
    }

    public static MSNetAuthLayer CreateAuthenticator()
    {
      MSNetKerberosAuthProperties authProperties = new MSNetKerberosAuthProperties();
      IMSNetAuthMechanism mech = new MSNetKerberosSSPIAuthMechanism(authProperties);
      MSNetAuthLayer authLayer = new MSNetAuthLayer();
      authLayer.AddAuthMechanism(mech);
      return authLayer;
    }
  }
}

