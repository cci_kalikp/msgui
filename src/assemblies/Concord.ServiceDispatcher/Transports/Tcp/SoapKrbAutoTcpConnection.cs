﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using MorganStanley.MSDotNet.MSNet;
using MorganStanley.MSDotNet.MSSoap.Common;

namespace MorganStanley.IED.Concord.ServiceDispatcher.Transports.Tcp
{
  /// <summary>
  /// Kerberized connection that uses KrbTcp logic for 32bit, or KrbSsspi for 64bit
  /// </summary>
  class SoapKrbAutoTcpConnection : SoapTCPConnection
  {
    public SoapKrbAutoTcpConnection(IMSNetLoop netLoop_, MSNetInetAddress address_, MSNetID id_, bool enableInitialRead_)
      : base(netLoop_, address_, id_, enableInitialRead_)
    {
      Authenticator = Is64BitOperatingSystem ? SoapKrbSspiTcpConnection.CreateAuthenticator() : SoapKrbTcpConnection.CreateAuthenticator();
    }

    //from http://stackoverflow.com/questions/336633/how-to-detect-windows-64-bit-platform-with-net
    private static bool Is64BitProcess
    {
      get
      {
        return (IntPtr.Size == 8);
      }
    }


    private static bool Is64BitOperatingSystem
    {
      get
      {
        return Is64BitProcess || InternalCheckIsWow64();
      }
    }

    [DllImport("kernel32.dll", SetLastError = true, CallingConvention = CallingConvention.Winapi)]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static extern bool IsWow64Process(
        [In] IntPtr hProcess,
        [Out] out bool wow64Process
    );

    [MethodImpl(MethodImplOptions.NoInlining)]
    private static bool InternalCheckIsWow64()
    {
      if ((Environment.OSVersion.Version.Major == 5 && Environment.OSVersion.Version.Minor >= 1) ||
          Environment.OSVersion.Version.Major >= 6)
      {
        using (Process p = Process.GetCurrentProcess())
        {
          bool retVal;
          if (!IsWow64Process(p.Handle, out retVal))
          {
            return false;
          }
          return retVal;
        }
      }
      else
      {
        return false;
      }
    }
  }
}
