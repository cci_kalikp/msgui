#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/Concord.ServiceDispatcher/Transports/Http/SOAPHttpClient.cs#4 $
  $DateTime: 2013/08/13 10:58:51 $
    $Change: 841602 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion

using System;
using System.Diagnostics;
using System.Xml;
using System.Net;
using System.IO;
using System.Text;

using MorganStanley.IED.Concord.Logging;
using MorganStanley.IED.Concord.Runtime;

using MorganStanley.MSDotNet.MSNet;
using MorganStanley.MSDotNet.MSSoap.Common;

namespace MorganStanley.IED.Concord.ServiceDispatcher.Transports.Http
{
    #region HttpClientUnAuthorizedException Class
    internal class HttpClientUnAuthorizedException : ApplicationException
    {
        #region Constructors
        public HttpClientUnAuthorizedException() { }
        #endregion Constructors

        #region Base Class Overrides
        public override string Message
        {
            get { return Res.EXCEPTION_CLIENT_LINK_NOT_AUTHORIZED; }
        }
        #endregion Base Class Overrides
    }
    #endregion HttpClientUnAuthorizedException Class

    #region SoapHttpClientException Class
    /// <summary>
    /// Represents a SOAP exception.
    /// </summary>
    public class SoapHttpClientException : ApplicationException
    {
        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="we_"></param>
        public SoapHttpClientException(WebException we_)
            : base(String.Format("Soap Http Client Exception: \r\n" +
              "Status: {0}\r\n" +
              "Description: {1}\r\n", we_.Status, we_.Message), we_)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="we_"></param>
        /// <param name="r_"></param>
        public SoapHttpClientException(WebException we_, HttpWebResponse r_)
            : base(String.Format("Soap Http Client Exception: \r\n" +
              "Status: {0}\r\n" +
              "Description: {1}\r\n" +
              "Response Status: {2}\r\n" +
              "Response Description: {3}\r\n", we_.Status, we_.Message, r_.StatusCode.ToString(), r_.StatusDescription), we_)
        {
        }
        #endregion Constructors
    }
    #endregion SoapHttpClientException Class

    #region ModifyAddressEventArgs Class
    public class ModifyAddressEventArgs
    {
        #region Declarations
        private Uri _address;
        private SoapClientMessage _message;
        #endregion Declarations

        #region Constructors
        internal ModifyAddressEventArgs(SoapClientMessage message_, Uri address_)
        {
            _message = message_;
            _address = address_;
        }
        #endregion Constructors

        #region Public Properties
        public SoapClientMessage SoapMessage
        {
            get { return _message; }
        }

        public Uri Address
        {
            get { return _address; }
        }
        #endregion Public Properties
    }
    #endregion ModifyAddressEventArgs Class

    #region RequestCreatedEventArgs Class
    public class RequestCreatedEventArgs
    {
        #region Declarations
        private HttpWebRequest _request;
        private SoapMessage _message;
        #endregion Declarations

        #region Constructors
        internal RequestCreatedEventArgs(SoapClientMessage message_, HttpWebRequest request_)
        {
            _message = message_;
            _request = request_;
        }
        #endregion Constructors

        #region Public Properties
        public SoapMessage SoapMessage
        {
            get { return _message; }
        }

        public HttpWebRequest HttpWebRequest
        {
            get { return _request; }
        }
        #endregion Public Properties
    }
    #endregion RequestCreatedEventArgs Class

    #region SOAPHttpClient Class
    /// <summary>
    /// Inherited from MSNET SoapHttpClient, this will
    /// expose the HTTP headers for modification
    /// </summary>
    public class SOAPHttpClient : SoapHttpClient, IHttpClientCall
    {
        #region Declarations
        private static bool _expect100Continue = false;
        private static ModifyAddressHandler _modifyAddressCallback;

        public delegate Uri ModifyAddressHandler(object sender_, ModifyAddressEventArgs e_);
        public delegate void RequestCreatedHandler(object sender_, RequestCreatedEventArgs e_);

        public static event RequestCreatedHandler RequestCreated;
        #endregion Declarations

        #region Constants
        private const string CLASS_NAME = "SOAPHttpClient";
        #endregion Constants

        #region Constructors
        /// <summary>
        /// Constructor with a default time out
        /// </summary>
        /// <param name="loop_">MSNETLoop that SOAP calls will be running on</param>
        /// <param name="url_">the URL address string</param>
        public SOAPHttpClient(IMSNetLoop loop_, string url_)
            : base(loop_, url_)
        {
            m_proxy = GlobalProxySelection.GetEmptyWebProxy();

            if (ConcordRuntime.Current.Services.ProxyType == ConcordProxyType.Http
              && ConcordRuntime.Current.Services.ProxyHost != null)
            {
                m_proxy = new WebProxy(ConcordRuntime.Current.Services.ProxyHost, ConcordRuntime.Current.Services.ProxyPort);

                #region Logging
                if (Log.IsLogLevelEnabled(LogLevel.Debug))
                {
                    LogLayer.Debug
                      .SetLocation(CLASS_NAME, "SOAPHttpClient")
                      .WriteFormat("Create SOAPHttpClient, Proxy='{0}:{1}'", ConcordRuntime.Current.Services.ProxyHost, ConcordRuntime.Current.Services.ProxyPort.ToString());
                }
                #endregion Logging
            }
        }
        #endregion Constructors

        #region Public Properties
        public static bool Expect100Continue
        {
            get { return _expect100Continue; }
            set { _expect100Continue = value; }
        }

        public static ModifyAddressHandler ModifyAddressCallback
        {
            get { return _modifyAddressCallback; }
            set { _modifyAddressCallback = value; }
        }
        #endregion Public Properties

        #region Internal Methods
        /// <summary>
        /// Synchronously sends out a SOAP request and wait for the response
        /// using the time out specified by the caller
        /// </summary>
        /// <param name="request_">the SOAP request message</param>
        /// <param name="timeout_">user specified timeout</param>
        /// <returns>a SoapMessage containing the response</returns>
        public virtual SoapMessage Send(SoapClientMessage request_, long timeout_)
        {
            #region Logging
            if (Log.IsLogLevelEnabled(LogLevel.Debug))
            {
                LogLayer.Debug.SetLocation(CLASS_NAME, "Send").WriteFormat("{0} Envelope='{1}'", "Request", ((SoapEnvelopeDomImpl)request_.Envelope).XmlDocument.OuterXml);
            }
            #endregion Logging

            SoapMessage response = this.SendRequest(request_, timeout_);

            #region Logging
            if (Log.IsLogLevelEnabled(LogLevel.Debug))
            {
                LogLayer.Debug.SetLocation(CLASS_NAME, "Send").WriteFormat("{0} Envelope='{1}'", "Response", ((SoapEnvelopeDomImpl)response.Envelope).XmlDocument.OuterXml);
            }
            #endregion Logging

            if (response != null && response.Fault != null)
            {
                throw new SoapFaultException(response.Fault.FaultString, response.Fault);
            }

            return response;
        }
        #endregion Internal Methods

        #region Protected Methods
        protected virtual void OnRequestCreated(RequestCreatedEventArgs e_)
        {
            if (RequestCreated != null)
            {
                RequestCreated(this, e_);
            }
        }

        protected static void SetRequestHeaders(HttpWebRequest request_)
        {
            request_.Method = SoapConstants.HTTP_HEADER_POST;
            request_.Accept = SoapConstants.HTTP_XML_CONTENT_TYPE;
            request_.ContentType = SoapConstants.HTTP_XML_CONTENT_TYPE;

            WebHeaderCollection headers = request_.Headers;
            try
            {
                headers.Add(SoapConstants.HTTP_HEADER_PRAGMA, SoapConstants.HTTP_NO_CACHE);
                headers.Add(SoapConstants.HTTP_HEADER_CACHE_CONTROL, SoapConstants.HTTP_NO_CACHE);

                System.String soapActionURI = (request_.Address.AbsolutePath.Length == 0) ?
                  SoapConstants.HTTP_SOAP_ACTION_NO_INDICATION :
                  SoapConstants.HTTP_SOAP_ACTION_URI_INDICATION;
                headers.Add(SoapConstants.HTTP_HEADER_SOAP_ACTION, soapActionURI);
            }
            catch (ArgumentNullException e_)
            {
                throw new SoapException(
                  "SoapHttpClient.SetRequestHeaders(): " +
                  "Caught ArgumentNullException: " +
                  e_.ToString(),
                  SoapConstants.FAULT_CODE_CLIENT,
                  e_);
            }
            catch (ArgumentException e_)
            {
                throw new SoapException(
                  "SoapHttpClient.SetRequestHeaders(): " +
                  "Caught ArgumentException: " +
                  e_.ToString(),
                  SoapConstants.FAULT_CODE_CLIENT,
                  e_);
            }
        }

        protected void SyncWriteToNetworkStream(byte[] requestBytes_, Stream networkStream_)
        {
            using (BinaryWriter writer = new BinaryWriter(networkStream_))
            {
                writer.Write(requestBytes_, 0, requestBytes_.Length);
            }
        }

        protected SoapMessage SendRequest(SoapClientMessage msg_, long timeout_)
        {
            HttpWebResponse response = null;
            SoapMessage returnMsg = null;
            try
            {
                bool bResend;
                do
                {
                    bResend = false;
                    try
                    {
                        byte[] requestBytes = msg_.GetXmlBytes();

                        if (_modifyAddressCallback != null)
                        {
                            m_address = _modifyAddressCallback(this, new ModifyAddressEventArgs(msg_, m_address));
                        }

                        HttpWebRequest request = this.CreateRequest();
                        this.OnRequestCreated(new RequestCreatedEventArgs(msg_, request));

                        request.Timeout = Convert.ToInt32(timeout_);
                        request.ContentLength = requestBytes.Length;
                        //request.Headers.Add("Content-Encoding", "gzip");
                        //request.ContentType = "text/octet-stream";

                        try
                        {
                            using (Stream requestStream = request.GetRequestStream())
                            {
                                this.SyncWriteToNetworkStream(requestBytes, requestStream);
                            }

                            response = (HttpWebResponse)request.GetResponse();
                        }
                        catch (WebException e_)
                        {
                            string httpAddress = m_address != null ? (m_address.AbsoluteUri ?? "uri is null") : "no address";

                            #region Logging
                            if (Log.IsLogLevelEnabled(LogLevel.Error))
                            {
                                LogLayer.Error.SetLocation(CLASS_NAME, "SendRequest").Write(
                                  "WebException occurred address : " + httpAddress, e_);
                            }
                            #endregion Logging

                            if (e_.Response != null)
                            {
                                response = (HttpWebResponse)e_.Response;

                                switch (response.StatusCode)
                                {
                                    case HttpStatusCode.Unauthorized:
                                        throw new HttpClientUnAuthorizedException();

                                    case HttpStatusCode.InternalServerError:
                                        if (response.ContentLength != -1)
                                        {
                                            byte[] msg = ProcessResponse(response);
                                            if (msg != null)
                                            {
                                                returnMsg = new SoapMessage(new SoapIOBuffer(msg));
                                                if (returnMsg != null && returnMsg.Fault != null && returnMsg.Fault.FaultString != null)
                                                {
                                                    throw new SoapFaultException(returnMsg.Fault.FaultString, returnMsg.Fault);
                                                }
                                            }
                                        }
                                        throw new SoapHttpClientException(e_, response);

                                    default:
                                        throw new SoapHttpClientException(e_, response);
                                }
                            }
                            else
                            {
                                throw GetCorrespondingSoapException(e_);
                            }
                        }

                        if ((response.StatusCode == HttpStatusCode.OK) || (response.StatusCode == HttpStatusCode.InternalServerError))
                        {
                            byte[] msg = null;
                            if (response.ContentLength != -1)
                            {
                                msg = ProcessResponse(response);
                            }

                            // get return cookies
                            CookieCollection cc = response.Cookies;
                            if (cc != null)
                            {
                                // Get Auth Token
                                foreach (Cookie c in cc)
                                {
                                    if (c.Name.CompareTo("Auth") == 0)
                                    {
                                        ConcordRuntime.Current.UserInfo.AuthToken = ASCIIEncoding.ASCII.GetBytes(c.Value);
                                        ConcordRuntime.Current.UserInfo.BigDogContext.AuthToken = ConcordRuntime.Current.UserInfo.AuthToken;
                                        break;
                                    }
                                }
                            }

                            if (msg != null)
                            {
                                try
                                {
                                    returnMsg = new SoapMessage(new SoapIOBuffer(msg));
                                }
                                catch (SoapParseException ex_)
                                {
                                    if (Log.IsLogLevelEnabled(LogLevel.Error))
                                    {
                                        Log.Error.SetLocation(CLASS_NAME, "SendRequest").WriteFormat("Error parsing response", ex_, Encoding.ASCII.GetString(msg));
                                    }

                                    throw;
                                }
                            }
                        }
                        else
                        {
                            #region Logging
                            if (Log.IsLogLevelEnabled(LogLevel.Error))
                            {
                                LogLayer.Error.SetLocation(CLASS_NAME, "SendRequest").WriteFormat("Unexpected HTTP response, StatusCode={0}, StatusDescription={1}", response.StatusCode, response.StatusDescription);
                            }
                            #endregion Logging

                            throw new SoapException(String.Format(Res.EXCEPTION_UNEXPECTED_HTTP_CODE, response.StatusCode), SoapConstants.FAULT_CODE_SERVER);
                        }
                    }
                    catch (HttpClientUnAuthorizedException)
                    {
                        ConcordRuntime.Current.UserInfo.AuthToken = null;
                        ConcordRuntime.Current.UserInfo.Password = null;
                        bResend = true;

                        #region Logging
                        if (Log.IsLogLevelEnabled(LogLevel.Warning))
                        {
                            LogLayer.Warning.SetLocation("SOAPHttpClient", "SendRequest").Write("Received http unauthorized error, about to trigger the login and resend the request.");
                        }
                        #endregion Logging
                        /*

            ConcordRuntime.Current.UserInfo.AuthToken = null;
            ConcordRuntime.Current.UserInfo.Password = null;

            // Only retry if this is a secure connection
            if (ConcordRuntime.Current.Services.SecureExternal) 
            {
              bResend = true;
            } 
            else 
            {
              throw;
            }
            */
                    }
                } while (bResend);
            }
            catch (Exception ex_)
            {
                #region Logging
                if (Log.IsLogLevelEnabled(LogLevel.Error))
                {
                    LogLayer.Error.SetLocation(CLASS_NAME, "SendRequest").Write("Received exception and will throw up the chain", ex_);
                }
                #endregion Logging

                throw;
            }
            finally
            {
                if (null != response)
                {
                    response.Close();
                }
            }

            return returnMsg;
        }
        #endregion Protected Methods

        #region Private Helper Methods
        private void SetupBigDogHeaders(HttpWebRequest request_)
        {
            // If SecureExternal is false then we authenticate
            // and so we don't care about UserInfo and do nothing
            if (ConcordRuntime.Current.Services.SecureExternal)
            {
                if (ConcordRuntime.Current.UserInfo != null)
                {
                    if (ConcordRuntime.Current.UserInfo.AuthToken != null)
                    {
                        CookieCollection cc = new CookieCollection();
                        cc.Add(new Cookie("Auth", ASCIIEncoding.ASCII.GetString(ConcordRuntime.Current.UserInfo.AuthToken)));
                        if (request_.CookieContainer == null)
                        {
                            request_.CookieContainer = new CookieContainer();
                        }
                        if (cc != null)
                        {
                            foreach (Cookie c in cc)
                            {
                                c.Path = "/";
                                c.Domain = m_address.Host;
                                request_.CookieContainer.Add(c);
                            }
                        }
                    }
                    else
                    {
                        string password = ConcordRuntime.Current.UserInfo.Password;
                        if (password == null || password.Trim() == string.Empty)
                        {
                            if (!ConcordRuntime.Current.UserInfo.Authenticate())
                            {
                                throw new SoapHttpClientException(new WebException(Res.EXCEPTION_CLIENT_LINK_NOT_AUTHORIZED));
                            }
                        }

                        ICredentials credential = new NetworkCredential(ConcordRuntime.Current.UserInfo.Username,
                          ConcordRuntime.Current.UserInfo.Password);
                        request_.Credentials = credential;
                        if (request_.CookieContainer == null)
                        {
                            request_.CookieContainer = new CookieContainer();
                        }
                    }
                }
                else
                {
                    #region Logging
                    if (Log.IsLogLevelEnabled(LogLevel.Error))
                    {
                        LogLayer.Error
                          .SetLocation(CLASS_NAME, "SetupBigDogHeaders")
                          .Write("Critical Error: ConcordRuntime.Current.UserInfo is not initialized!");
                    }
                    #endregion Logging

                    throw new ApplicationException(Res.EXCEPTION_USERINFO_NOT_DEFINED);
                }
            }
        }

        private HttpWebRequest CreateRequest()
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(m_address);
                request.KeepAlive = m_keepAlive;
                request.Pipelined = m_pipelined;
                request.Proxy = m_proxy;

                SetRequestHeaders(request);

                try
                {
                    if (!Expect100Continue)
                    {
                        System.Reflection.FieldInfo field = request.ServicePoint.GetType().GetField("m_Understands100Continue", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.GetField);
                        field.SetValue(request.ServicePoint, false);
                    }
                }
                catch (Exception) { /* Ignore */ }

                this.SetupBigDogHeaders(request);

                return request;
            }
            catch (InvalidCastException)
            {
                #region Logging
                if (Log.IsLogLevelEnabled(LogLevel.Error))
                {
                    LogLayer.Error.SetLocation(CLASS_NAME, "CreateRequest").Write("Illegal scheme. The specified url is not an http url");
                }
                #endregion Logging

                throw new ArgumentException(Res.EXCEPTION_INVALID_URI);
            }
        }

        private byte[] ProcessResponse(HttpWebResponse response_)
        {
            Stream stream = null;
            try
            {
                stream = response_.GetResponseStream();
                int bytesToRead = (int)response_.ContentLength;

                if (bytesToRead <= 0 && response_.StatusCode == HttpStatusCode.InternalServerError)
                {
                    // This is not a SOAP fault
                    throw new SoapException(Res.EXCEPTION_SERVER_RETURNED_ERROR, SoapConstants.FAULT_CODE_SERVER);
                }

                byte[] retVal = new byte[bytesToRead];

                // read into buffer ...
                int offset = 0;
                int bytes = stream.Read(retVal, offset, bytesToRead);
                while (bytes > 0)
                {
                    offset += bytes;
                    bytesToRead -= bytes;
                    bytes = stream.Read(retVal, offset, bytesToRead);
                }
                return retVal;
            }
            finally
            {
                if (stream != null)
                {
                    // Documentation states you need to Close the stream
                    // OR the request. Though closing both does not cause
                    // an exception. We will Close the stream
                    stream.Close();
                }
            }
        }

        private SoapException GetCorrespondingSoapException(WebException e_)
        {
            if (e_.Status == WebExceptionStatus.RequestCanceled)
            {
                return new SoapOperationAbortedException(Res.EXCEPTION_SOAPOPERATION_ABORTED, e_);
            }

            if (e_.Status == WebExceptionStatus.Timeout)
            {
                return new SoapTimeoutException(Res.EXCEPTION_SOAPTIMEOUT, e_);
            }

            return new SoapException(String.Format(Res.EXCEPTION_SOAP, e_.Message, e_.Status));
        }
        #endregion Private Helper Methods
    }
    #endregion SOAPHttpClient Class
}
