﻿<?xml version="1.0" encoding="utf-8" ?> 

<!--
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/Concord.ServiceDispatcher/XmlDocs/MemoryCache.cs.xml#3 $
  $DateTime: 2013/01/18 15:24:18 $
    $Change: 813214 $
    $Author: smulovic $
_____________________________________________________________________________*/
//-->

<doc>
  <doc for="MemoryCache">
    <summary>
      Stores cached items within the memory of the current process.
    </summary>
  </doc>
  
  <doc for="MemoryCache.IEnumerable.GetEnumerator">
    <summary>
      Returns an <see cref="System.Collections.IEnumerator">IEnumerator</see> that can iterate through the cache.
    </summary>
    <returns>
      An <see cref="System.Collections.IDictionaryEnumerator">IDictionaryEnumerator</see> for the cache.
    </returns>
    <remarks>
      <P>Enumerators only allow reading the data in the collection. Enumerators cannot 
      be used to modify the underlying collection.</P>
      <P>Initially, the enumerator is positioned before the first element in the 
      collection. <A 
      href="System.Collections.IEnumerator.Reset">Reset</A> also 
      brings the enumerator back to this position. At this position, calling <A 
      href="System.Collections.IEnumerator.Current">Current</A> throws 
      an exception. Therefore, you must call <A 
      href="System.Collections.IEnumerator.MoveNext">MoveNext</A> to 
      advance the enumerator to the first element of the collection before reading the 
      value of <B>Current</B>.</P>
      <P><B>Current</B> returns the same object until either <B>MoveNext</B> or 
      <B>Reset</B> is called. <B>MoveNext</B> sets <B>Current</B> to the next 
      element.</P>
      <P>After the end of the collection is passed, the enumerator is positioned after 
      the last element in the collection, and calling <B>MoveNext</B> returns 
      <B>false</B>. If the last call to <B>MoveNext</B> returned <B>false</B>, calling 
      <B>Current</B> throws an exception. To set <B>Current</B> to the first element 
      of the collection again, you can call <B>Reset</B> followed by 
      <B>MoveNext</B>.</P>
      <P>An enumerator remains valid as long as the collection remains unchanged. If 
      changes are made to the collection, such as adding, modifying or deleting 
      elements, the enumerator is irrecoverably invalidated and the next call to 
      <B>MoveNext</B> or <B>Reset</B> throws an <A 
      href="System.InvalidOperationException">InvalidOperationException</A>. 
      If the collection is modified between <B>MoveNext</B> and <B>Current</B>, 
      <B>Current</B> will return the element that it is set to, even if the enumerator 
      is already invalidated.</P>
      <P>The enumerator does not have exclusive access to the collection; therefore, 
      enumerating through a collection is intrinsically not a thread-safe procedure. 
      Even when a collection is synchronized, other threads could still modify the 
      collection, which causes the enumerator to throw an exception. To guarantee 
      thread safety during enumeration, you can either lock the collection during the 
      entire enumeration or catch the exceptions resulting from changes made by other 
      threads.</P>    
    </remarks>
  </doc>
  
  <doc for="MemoryCache.GetEnumerator">
    <summary>
      Returns an <see cref="System.Collections.IDictionaryEnumerator">IDictionaryEnumerator</see> that caniterate throught the cache.
    </summary>
    <remarks>
      <P>Enumerators only allow reading the data in the collection. Enumerators cannot 
      be used to modify the underlying collection.</P>
      <P>Initially, the enumerator is positioned before the first element in the 
      collection. <A 
      href="System.Collections.IEnumerator.Reset">Reset</A> also 
      brings the enumerator back to this position. At this position, calling <A 
      href="System.Collections.IEnumerator.Current">Current</A> throws 
      an exception. Therefore, you must call <A 
      href="System.Collections.IEnumerator.MoveNext">MoveNext</A> to 
      advance the enumerator to the first element of the collection before reading the 
      value of <B>Current</B>.</P>
      <P><B>Current</B> returns the same object until either <B>MoveNext</B> or 
      <B>Reset</B> is called. <B>MoveNext</B> sets <B>Current</B> to the next 
      element.</P>
      <P>After the end of the collection is passed, the enumerator is positioned after 
      the last element in the collection, and calling <B>MoveNext</B> returns 
      <B>false</B>. If the last call to <B>MoveNext</B> returned <B>false</B>, calling 
      <B>Current</B> throws an exception. To set <B>Current</B> to the first element 
      of the collection again, you can call <B>Reset</B> followed by 
      <B>MoveNext</B>.</P>
      <P>An enumerator remains valid as long as the collection remains unchanged. If 
      changes are made to the collection, such as adding, modifying or deleting 
      elements, the enumerator is irrecoverably invalidated and the next call to 
      <B>MoveNext</B> or <B>Reset</B> throws an <A 
      href="System.InvalidOperationException">InvalidOperationException</A>. 
      If the collection is modified between <B>MoveNext</B> and <B>Current</B>, 
      <B>Current</B> will return the element that it is set to, even if the enumerator 
      is already invalidated.</P>
      <P>The enumerator does not have exclusive access to the collection; therefore, 
      enumerating through a collection is intrinsically not a thread-safe procedure. 
      Even when a collection is synchronized, other threads could still modify the 
      collection, which causes the enumerator to throw an exception. To guarantee 
      thread safety during enumeration, you can either lock the collection during the 
      entire enumeration or catch the exceptions resulting from changes made by other 
      threads.</P>    
    </remarks>    
  </doc>
  
  <doc for="MemoryCache.Remove">
    <summary>
      Removes the element with the specified key from the cache.
    </summary>
    <param name="key_">
      The key of the element to remove.
    </param>
    <remarks>
      If the cache does not contain an element with the specified key, the cache remains unchanged.  No exception is thrown.
    </remarks>
  </doc>
  
  <doc for="MemoryCache.Contains">
    <summary>
      Determines whether the cache contains a specific key.
    </summary>
    <param name="key_">
      The key to locate in the cache.
    </param>
    <remarks>
      This implementation is close to O(1) in most cases.
    </remarks>
  </doc>
  
  <doc for="MemoryCache.Clear">
    <summary>
      Removes all elements from the cache.
    </summary>
    <remarks>
      <see cref="Count">Count</see> is set to zero.
    </remarks>
  </doc>
  
  <doc for="MemoryCache.Add">
    <summary>
      Adds an element with the specified key and value into the cache.
    </summary>
    <param name="key_">The key of the element to add.</param>
    <param name="value_">The value of the element to add.</param>
  </doc>
  
  <doc for="MemoryCache.IsReadOnly">
    <summary>
      Gets a value indicating whether the cache is read-only.
    </summary>
    <value>
      <b>true</b> if the cache is read-only; otherwise, <b>false</b>.  The default is <b>false</b>.
    </value>
  </doc>
  
  <doc for="MemoryCache.Item">
    <summary>
      Gets or sets the value associated with the specified key.<p>In C#, this property is the indexer.</p>
    </summary>
    <param name="key_">The key whose value to get or set.</param>
    <value>
      The value associated with the specified key.  If the specified key is not found, attempting to get it returns
      a null reference, and attempting to set it creates a new element using the specified key.
    </value>
  </doc>
  
  <doc for="MemoryCache.Values">
    <summary>
      Gets an <see cref="System.Collections.ICollection">ICollection</see> containg the values in the cache.
    </summary>
    <value>
      An <see cref="System.Collections.ICollection">ICollection</see> containg the values in the cache.
    </value>
  </doc>
  
  <doc for="MemoryCache.Keys">
    <summary>
      Gets an <see cref="System.Collections.ICollection">ICollection</see> containg the keys in the cache.
    </summary>
    <value>
      An <see cref="System.Collections.ICollection">ICollection</see> containg the keys in the cache.
    </value>
  </doc>  
  
  <doc for="MemoryCache.IsFixedSize">
    <summary>
      Gets a value indicating whether the cache has a fixed size.
    </summary>
    <value>
      <b>true</b> if the cache has a fixed size; otherwise, <b>false</b>.  The default is <b>false</b>.
    </value>
  </doc>
  
  <doc for="MemoryCache.CopyTo">
    <summary>
      Copies the cache elements to a one-dimensional <see cref="System.Array">Array</see> instance at the specified index.
    </summary>
    <param name="array_">
      The one-dimensional <see cref="System.Array">Array</see> that is the destination of the objects copied from the cache.  The
      <b>Array</b> must have zero-based indexing.
    </param>
    <param name="index_">
      The zero-based index in <i>array</i> at which copying begins.
    </param>
  </doc>
  
  <doc for="MemoryCache.IsSynchronized">
    <summary>
      Gets a value indicating whether access to the cache is synchronized (thread-safe).
    </summary>
    <value>
      <b>true</b> if access to the cache is synchronized (thread-safe); otherwise, <b>false</b>.  The default is <b>true</b>.
    </value>
  </doc>
  
  <doc for="MemoryCache.Count">
    <summary>
      Gets the number of key-and-value pairs contained in the cache.
    </summary>
    <value>
      The number of key-and-value pairs contained in the cache.
    </value>
  </doc>
  
  <doc for="MemoryCache.SyncRoot">
    <summary>
      Gets an object that can be used to synchronize access to the cache.
    </summary>
    <value>
      An object that can be used to synchronize access to the cache.
    </value>
  </doc>
  
  <doc for="MemoryCache.Dispose">
    <summary>
      Releases the resources used by the <see cref="MemoryCache">MemoryCache</see>.
    </summary>
  </doc>
</doc>
