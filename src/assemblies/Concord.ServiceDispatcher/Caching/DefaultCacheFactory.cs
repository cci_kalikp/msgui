#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/Concord.ServiceDispatcher/Caching/DefaultCacheFactory.cs#3 $
  $DateTime: 2013/01/18 15:24:18 $
    $Change: 813214 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion

using System;
using System.Collections.Specialized;
using System.Reflection;

namespace MorganStanley.IED.Concord.ServiceDispatcher.Caching
{
  /// <include file='xmldocs/DefaultCacheFactory.cs.xml' path='doc/doc[@for="CacheType"]/*'/>
  public enum CacheType 
  {
    /// <include file='xmldocs/DefaultCacheFactory.cs.xml' path='doc/doc[@for="CacheType.File"]/*'/>
    File,

    /// <include file='xmldocs/DefaultCacheFactory.cs.xml' path='doc/doc[@for="CacheType.Memory"]/*'/>
    Memory
  }

  /// <include file='xmldocs/DefaultCacheFactory.cs.xml' path='doc/doc[@for="CacheConfigurationException"]/*'/>
  public class CacheConfigurationException : ApplicationException 
  {
    /// <include file='xmldocs/DefaultCacheFactory.cs.xml' path='doc/doc[@for="CacheConfigurationException.CacheConfigurationException"]/*'/>
    public CacheConfigurationException(string message_) : base(message_) 
    {
    }
  }

  /// <include file='xmldocs/DefaultCacheFactory.cs.xml' path='doc/doc[@for="DefaultCacheFactory"]/*'/>
	public class DefaultCacheFactory : ICacheFactory
	{
    #region Declarations
    internal const string CACHE_TYPE_ATTRIBUTE = "cacheType";
    #endregion Declarations

    #region Constructors
		internal DefaultCacheFactory()
		{
		}
    #endregion Constructors

    #region Protected Methods
    /// <include file='xmldocs/DefaultCacheFactory.cs.xml' path='doc/doc[@for="DefaultCacheFactory.GetCacheInstance"]/*'/>
    protected virtual ICache GetCacheInstance(NameValueCollection attributes_) 
    {
      string type = attributes_[CACHE_TYPE_ATTRIBUTE];
      if (type == null) 
      {
        throw new CacheConfigurationException(Res.GetString(Res.CONFIG_MISSING_CACHETYPE_ATTRIBUTE));
      }

      CacheType cacheType;

      try 
      {
        cacheType = (CacheType) Enum.Parse(typeof(CacheType), type, true);
      } 
      catch (ArgumentException) 
      {
        throw new CacheConfigurationException(String.Format(Res.GetString(Res.CONFIG_INVALID_CACHETYPE), type));
      }

      switch(cacheType) 
      {
        case CacheType.File:
          return new FileCache();

        case CacheType.Memory:
          return new MemoryCache();
      }

      throw new CacheConfigurationException(String.Format(Res.GetString(Res.CONFIG_INVALID_CACHETYPE), type));
    }
    #endregion Protected Methods

    #region Implementation of ICacheFactory
    /// <include file='xmldocs/DefaultCacheFactory.cs.xml' path='doc/doc[@for="DefaultCacheFactory.GetCache"]/*'/>
    public ICache GetCache(NameValueCollection attributes_)
    {
      ICache cache = this.GetCacheInstance(attributes_);

      if (cache != null) 
      {
        foreach (string name in attributes_.AllKeys) 
        {
          MemberInfo[] members = cache.GetType().GetMember(name, MemberTypes.Field | MemberTypes.Property, BindingFlags.Public);

          if (members != null && members.Length > 0) 
          {
            cache.GetType().InvokeMember(name, BindingFlags.Public | BindingFlags.SetField | BindingFlags.SetProperty, null, cache, new object[] {attributes_[name]});
          }
        }
      }

      return cache;
    }
    #endregion
	}
}
