#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/Concord.ServiceDispatcher/Caching/CacheEntry.cs#3 $
  $DateTime: 2013/01/18 15:24:18 $
    $Change: 813214 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion

using System;

namespace MorganStanley.IED.Concord.ServiceDispatcher.Caching
{
  // TODO: This is for future use in the FileCache implementation.
	internal class CacheEntry
	{
    #region Declarations
    private string _key;
    private object _value;
    #endregion Declarations

    #region Constructors
		public CacheEntry(string key_, object value_)
		{
      this._key = key_;
      this._value = value_;
		}
    #endregion Constructors

    #region Public Properties
    public string Key 
    {
      get { return _key; }
    }

    public object Value 
    {
      get { return _value; }
    }
    #endregion Public Properties

    #region Internal Methods
    #endregion Internal Methods
	}
}
