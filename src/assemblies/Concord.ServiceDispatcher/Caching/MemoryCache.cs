#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/Concord.ServiceDispatcher/Caching/MemoryCache.cs#3 $
  $DateTime: 2013/01/18 15:24:18 $
    $Change: 813214 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion

using System;
using System.Collections;

namespace MorganStanley.IED.Concord.ServiceDispatcher.Caching
{
  /// <include file='xmldocs/MemoryCache.cs.xml' path='doc/doc[@for="MemoryCache"]/*'/>
	public class MemoryCache : ICache
	{
    #region Declarations
    private Hashtable _cache;
    #endregion Declarations

    #region Constructors
		internal MemoryCache()
    {
      _cache = Hashtable.Synchronized(new Hashtable());
		}
    #endregion Constructors

    #region Implementation of IEnumerable
    /// <include file='xmldocs/MemoryCache.cs.xml' path='doc/doc[@for="MemoryCache.IEnumerable.GetEnumerator"]/*'/>
    System.Collections.IEnumerator IEnumerable.GetEnumerator()
    {
      return ((IEnumerable) _cache).GetEnumerator();
    }
    #endregion Implementation of IEnumerable

    #region Implementation of IDictionary
    /// <include file='xmldocs/MemoryCache.cs.xml' path='doc/doc[@for="MemoryCache.GetEnumerator"]/*'/>
    public System.Collections.IDictionaryEnumerator GetEnumerator()
    {
      return ((IDictionary) _cache).GetEnumerator();
    }

    /// <include file='xmldocs/MemoryCache.cs.xml' path='doc/doc[@for="MemoryCache.Remove"]/*'/>
    public void Remove(object key_)
    {
      _cache.Remove(key_);
    }

    /// <include file='xmldocs/MemoryCache.cs.xml' path='doc/doc[@for="MemoryCache.Contains"]/*'/>
    public bool Contains(object key_)
    {
      return _cache.Contains(key_);
    }

    /// <include file='xmldocs/MemoryCache.cs.xml' path='doc/doc[@for="MemoryCache.Clear"]/*'/>
    public void Clear()
    {
      _cache.Clear();
    }

    /// <include file='xmldocs/MemoryCache.cs.xml' path='doc/doc[@for="MemoryCache.Add"]/*'/>
    public void Add(object key_, object value_)
    {
      _cache.Add(key_, value_);
    }

    /// <include file='xmldocs/MemoryCache.cs.xml' path='doc/doc[@for="MemoryCache.IsReadOnly"]/*'/>
    public bool IsReadOnly
    {
      get { return _cache.IsReadOnly; }
    }

    /// <include file='xmldocs/MemoryCache.cs.xml' path='doc/doc[@for="MemoryCache.Item"]/*'/>
    public object this[object key_]
    {
      get { return _cache[key_]; }
      set { _cache[key_] = value; }
    }

    /// <include file='xmldocs/MemoryCache.cs.xml' path='doc/doc[@for="MemoryCache.Values"]/*'/>
    public System.Collections.ICollection Values
    {
      get { return _cache.Values; }
    }

    /// <include file='xmldocs/MemoryCache.cs.xml' path='doc/doc[@for="MemoryCache.Keys"]/*'/>
    public System.Collections.ICollection Keys
    {
      get { return _cache.Keys; }
    }

    /// <include file='xmldocs/MemoryCache.cs.xml' path='doc/doc[@for="MemoryCache.IsFixedSize"]/*'/>
    public bool IsFixedSize
    {
      get { return _cache.IsFixedSize; }
    }
    #endregion

    #region Implementation of ICollection
    /// <include file='xmldocs/MemoryCache.cs.xml' path='doc/doc[@for="MemoryCache.CopyTo"]/*'/>
    public void CopyTo(System.Array array_, int index_)
    {
      _cache.CopyTo(array_, index_);
    }

    /// <include file='xmldocs/MemoryCache.cs.xml' path='doc/doc[@for="MemoryCache.IsSynchronized"]/*'/>
    public bool IsSynchronized
    {
      get { return _cache.IsSynchronized; }
    }

    /// <include file='xmldocs/MemoryCache.cs.xml' path='doc/doc[@for="MemoryCache.Count"]/*'/>
    public int Count
    {
      get { return _cache.Count; }
    }

    /// <include file='xmldocs/MemoryCache.cs.xml' path='doc/doc[@for="MemoryCache.SyncRoot"]/*'/>
    public object SyncRoot
    {
      get { return _cache.SyncRoot; }
    }
    #endregion

    #region Implementation of IDisposable
    /// <include file='xmldocs/MemoryCache.cs.xml' path='doc/doc[@for="MemoryCache.Dispose"]/*'/>
    public void Dispose()
    {
    }
    #endregion
  }
}
