#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/Concord.ServiceDispatcher/Caching/CacheManager.cs#3 $
  $DateTime: 2013/01/18 15:24:18 $
    $Change: 813214 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion

using System;
using System.Collections;
using System.Collections.Specialized;
using System.Diagnostics;

namespace MorganStanley.IED.Concord.ServiceDispatcher.Caching
{
  /// <include file='xmldocs/CacheManager.cs.xml' path='doc/doc[@for="CacheManager"]/*'/>
  public class CacheManager
  {
    #region Declarations
    private Hashtable _cacheCollection;
    private static readonly CacheManager _instance = new CacheManager();
    #endregion Declarations

    #region Constructors
    private CacheManager()
    {
      _cacheCollection = new Hashtable();
    }
    #endregion Constructors

    #region Public Properties
    /// <include file='xmldocs/CacheManager.cs.xml' path='doc/doc[@for="CacheManager.Current"]/*'/>
    public static CacheManager Current
    {
      get { return _instance; }
    }
    #endregion Public Properties

    #region Public Methods
    /// <include file='xmldocs/CacheManager.cs.xml' path='doc/doc[@for="CacheManager.GetCache"]/*'/>
    public ICache GetCache(string name_) 
    {
      //Debug.Assert(_cacheCollection != null, "CacheManager is null");

      ICache cache = (ICache) _cacheCollection[name_];

      if (cache == null) 
      {
        // TODO: Integration with configuration service to obtain
        // cache settings.  For now, we will hardcode the use of
        // the built-in factory.
        NameValueCollection attributes = new NameValueCollection();
        attributes.Add(DefaultCacheFactory.CACHE_TYPE_ATTRIBUTE, CacheType.Memory.ToString());

        ICacheFactory cacheFactory = new DefaultCacheFactory();
        cache = cacheFactory.GetCache(attributes);

        if (cache != null) 
        {
          _cacheCollection[name_] = cache;
        }
      } 

      return cache;
    }
    #endregion Public Methods
	}
}
