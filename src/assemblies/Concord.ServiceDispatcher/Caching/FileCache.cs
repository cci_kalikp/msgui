#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/Concord.ServiceDispatcher/Caching/FileCache.cs#3 $
  $DateTime: 2013/01/18 15:24:18 $
    $Change: 813214 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion

using System;
using System.Collections;

namespace MorganStanley.IED.Concord.ServiceDispatcher.Caching
{
	/// <include file='xmldocs/FileCache.cs.xml' path='doc/doc[@for="FileCache"]/*'/>
	public class FileCache : ICache
	{
    #region Declarations
    #endregion Declarations

    #region Constructors
		internal FileCache()
		{
		}
    #endregion Constructors

    #region Implementation of IEnumerable
    /// <include file='xmldocs/FileCache.cs.xml' path='doc/doc[@for="FileCache.IEnumerable.GetEnumerator"]/*'/>
    System.Collections.IEnumerator IEnumerable.GetEnumerator()
    {
      return null;
    }
    #endregion Implementation of IEnumerable

    #region Implementation of IDictionary
    /// <include file='xmldocs/FileCache.cs.xml' path='doc/doc[@for="FileCache.GetEnumerator"]/*'/>
    public System.Collections.IDictionaryEnumerator GetEnumerator()
    {
      return null;
    }

    /// <include file='xmldocs/FileCache.cs.xml' path='doc/doc[@for="FileCache.Remove"]/*'/>
    public void Remove(object key_)
    {
    
    }

    /// <include file='xmldocs/FileCache.cs.xml' path='doc/doc[@for="FileCache.Contains"]/*'/>
    public bool Contains(object key_)
    {
      return true;
    }

    /// <include file='xmldocs/FileCache.cs.xml' path='doc/doc[@for="FileCache.Clear"]/*'/>
    public void Clear()
    {
    
    }

    /// <include file='xmldocs/FileCache.cs.xml' path='doc/doc[@for="FileCache.Add"]/*'/>
    public void Add(object key_, object value_)
    {
    
    }

    /// <include file='xmldocs/FileCache.cs.xml' path='doc/doc[@for="FileCache.IsReadOnly"]/*'/>
    public bool IsReadOnly
    {
      get
      {
        return true;
      }
    }

    /// <include file='xmldocs/FileCache.cs.xml' path='doc/doc[@for="FileCache.Item"]/*'/>
    public object this[object key_]
    {
      get
      {
        return null;
      }
      set
      {
      }
    }

    /// <include file='xmldocs/FileCache.cs.xml' path='doc/doc[@for="FileCache.Values"]/*'/>
    public System.Collections.ICollection Values
    {
      get
      {
        return null;
      }
    }

    /// <include file='xmldocs/FileCache.cs.xml' path='doc/doc[@for="FileCache.Keys"]/*'/>
    public System.Collections.ICollection Keys
    {
      get
      {
        return null;
      }
    }

    /// <include file='xmldocs/FileCache.cs.xml' path='doc/doc[@for="FileCache.IsFixedSize"]/*'/>
    public bool IsFixedSize
    {
      get
      {
        return true;
      }
    }
    #endregion

    #region Implementation of ICollection
    /// <include file='xmldocs/FileCache.cs.xml' path='doc/doc[@for="FileCache.CopyTo"]/*'/>
    public void CopyTo(System.Array array_, int index_)
    {
    
    }

    /// <include file='xmldocs/FileCache.cs.xml' path='doc/doc[@for="FileCache.IsSynchronized"]/*'/>
    public bool IsSynchronized
    {
      get
      {
        return true;
      }
    }

    /// <include file='xmldocs/FileCache.cs.xml' path='doc/doc[@for="FileCache.Count"]/*'/>
    public int Count
    {
      get
      {
        return 0;
      }
    }

    /// <include file='xmldocs/FileCache.cs.xml' path='doc/doc[@for="FileCache.SyncRoot"]/*'/>
    public object SyncRoot
    {
      get
      {
        return null;
      }
    }
    #endregion

    #region Implementation of IDisposable
    /// <include file='xmldocs/FileCache.cs.xml' path='doc/doc[@for="FileCache.Dispose"]/*'/>
    public void Dispose()
    {
    
    }
    #endregion
	}
}
