#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/Concord.ServiceDispatcher/Caching/ICache.cs#3 $
  $DateTime: 2013/01/18 15:24:18 $
    $Change: 813214 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion

using System;
using System.Collections;
using System.Diagnostics;

namespace MorganStanley.IED.Concord.ServiceDispatcher.Caching
{
  /// <include file='xmldocs/ICache.cs.xml' path='doc/doc[@for="ICache"]/*'/>
  public interface ICache : IDictionary, IDisposable
  {
  }
}