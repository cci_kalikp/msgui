#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/Concord.ServiceDispatcher/Caching/Res.cs#3 $
  $DateTime: 2013/01/18 15:24:18 $
    $Change: 813214 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion

using System;
using System.Globalization;
using System.Resources;

namespace MorganStanley.IED.Concord.ServiceDispatcher.Caching
{
	/// <summary>
	/// Internal class for retrieving culture specific resources
	/// </summary>
	sealed class Res
	{
    #region Declarations
    private ResourceManager _resources = null;
    private static Res _loader = null;
    private static object _loaderLock = new object();
    #endregion Declarations

    #region Constructors
		private Res()
		{
      this._resources = new ResourceManager("MorganStanley.IED.Concord.ServiceDispatcher.Caching.Res", this.GetType().Module.Assembly);
		}
    #endregion Constructors

    #region Resource Constants
    // Constant values point to names within the resource files

    public const string CONFIG_MISSING_CACHETYPE_ATTRIBUTE = "CONFIG_MISSING_CACHETYPE_ATTRIBUTE";
    public const string CONFIG_INVALID_CACHETYPE = "CONFIG_INVALID_CACHETYPE";

    /*
    public const string UNABLE_TO_DESERIALIZE_RESULT = "UNABLE_TO_DESERIALIZE_RESULT";
    public const string INVALID_ACTION = "INVALID_ACTION";
    public const string NO_RETURN_VALUE = "NO_RETURN_VALUE";
    public const string INVALID_STAGE = "INVALID_STAGE";
    public const string UNABLE_TO_LOAD_STYLESHEET = "UNABLE_TO_LOAD_STYLESHEET";
    public const string UNABLE_TO_CREATE_INSTANCE = "UNABLE_TO_CREATE_INSTANCE";
    public const string ERROR_DURING_VALIDATION = "ERROR_DURING_VALIDATION";
    public const string DESERIALIZE_NOT_SUPPORTED = "DESERIALIZE_NOT_SUPPORTED";
    public const string SERIALIZE_NOT_SUPPORTED = "SERIALIZE_NOT_SUPPORTED";
    public const string SERVICE_NOT_FOUND = "SERVICE_NOT_FOUND";
    public const string DESERIALIZE_FAILED = "DESERIALIZE_FAILED";
    public const string EXTENSION_INVOKE_FAILED = "EXTENSION_INVOKE_FAILED";
    public const string EXTENSION_CREATE_FAILED = "EXTENSION_CREATE_FAILED";
    public const string XMLSERVICEDISPATCH_INVOKE_FAILED = "XMLSERVICEDISPATCH_INVOKE_FAILED";
    public const string SERVICECONFIG_NOT_FOUND = "SERVICECONFIG_NOT_FOUND";
    public const string SERVICECONFIG_ERROR_DESERIALIZE = "SERVICECONFIG_ERROR_DESERIALIZE";
    public const string SERVICECONFIG_INVALID_URL = "SERVICECONFIG_INVALID_URL";
    public const string SERVICECONFIG_INVALID_URL_FORMAT = "SERVICECONFIG_INVALID_URL_FORMAT";
    public const string SERVICECONFIG_ERROR_READ = "SERVICECONFIG_ERROR_READ";
    public const string SERVICECONFIG_MISSING_SERVICETYPE = "SERVICECONFIG_MISSING_SERVICETYPE";
    public const string DOES_NOT_IMPLEMENT_IVALIDATOR = "DOES_NOT_IMPLEMENT_IVALIDATOR";
    public const string DOES_NOT_DEREIVE_FROM_SERVICEEXTENSION = "DOES_NOT_DEREIVE_FROM_SERVICEEXTENSION";
    */
    #endregion Resource Constants

    #region Public Methods
    /// <summary>
    /// Gets the value of the specified <see cref="System.String">String</see> resource
    /// for the current culture.
    /// </summary>
    /// <param name="name_">The name of the resource to get.</param>
    /// <returns>
    /// The value of the resource localized for the caller's current culture settings.
    /// If a match is not possible, a null reference.
    /// </returns>
    public static string GetString(string name_) 
    {
      return GetString(null, name_);
    }

    /// <summary>
    /// Gets the value of the specified <see cref="System.String">String</see> resource
    /// for the specified culture.
    /// </summary>
    /// <param name="culture_">
    /// The <see cref="System.Globalization.CultureInfo">CultureInfo</see> object that
    /// represents the culture for which the resource is localized. Note that if the resource
    /// is not localized for this culture, the lookup will fall back using the culture's Parent
    /// property, stopping after looking in the neutral culture.
    /// </param>
    /// <param name="name_">The name of the resource to get.</param>
    /// <returns>
    /// The value of the resource localized for the caller's current culture settings.
    /// If a match is not possible, a null reference.
    /// </returns>
    public static string GetString(CultureInfo culture_, string name_) 
    {
      Res res = Res.GetLoader();

      if (res == null) 
      {
        return null;
      }

      return res._resources.GetString(name_, culture_);
    }
    #endregion Public Methods

    #region Private Methods
    private static Res GetLoader() 
    {
      lock(_loaderLock) 
      {
        if (Res._loader == null) 
        {
          Res._loader = new Res();
        }
      }

      return Res._loader;
    }
    #endregion Private Methods
	}
}
