#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/Concord.ServiceDispatcher/SoapClientMessage.cs#3 $
  $DateTime: 2013/01/18 15:24:18 $
    $Change: 813214 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion

namespace MorganStanley.IED.Concord.ServiceDispatcher
{
  using System;
  using System.Web;
  using System.Xml;
  using Configuration;
  using MSDotNet.MSSoap.Common;

  /// <include file='xmldocs/SoapClientMessage.cs.xml' path='doc/doc[@for="SoapClientMessage"]/*'/>
  sealed public class SoapClientMessage : SoapMessage
  {
    #region Declarations
    private ActionInfo            _actionInfo;
    private object[]              _parameterValues;
    private Type                  _returnType;
    private object                _returnValue;
    private XmlServiceExtension[] _extensions;
    private SoapMessageStage      _stage;
    private string                _requestID;
    #endregion Declarations

    #region Constructors
    public SoapClientMessage(ActionInfo actionInfo_, SoapEnvelopeDomImpl envelope_, object[] parameterValues_, Type returnType_, XmlServiceExtension[] extensions_) : base(envelope_)
    {
      _actionInfo = actionInfo_;
      _parameterValues = parameterValues_;
      _stage = SoapMessageStage.BeforeSerialize;
      _extensions = extensions_;
      _returnType = returnType_;

      if (UseEncodedGUIDForRequestID)
      {
        _requestID = HttpServerUtility.UrlTokenEncode(Guid.NewGuid().ToByteArray());
      }
      else
      {
        _requestID = Guid.NewGuid().ToString();
      }
    }
    #endregion Constructors

    #region Public Properties
    /// <summary>
    /// Set this to send the 23 byte request id that MSTK prefers
    /// </summary>
    public static bool UseEncodedGUIDForRequestID;

    /// <include file='xmldocs/SoapClientMessage.cs.xml' path='doc/doc[@for="SoapClientMessage.ActionInfo"]/*'/>
    public ActionInfo ActionInfo
    {
      get { return _actionInfo; }
    }

    /// <include file='xmldocs/SoapClientMessage.cs.xml' path='doc/doc[@for="SoapClientMessage.Parameters"]/*'/>
    public object[] Parameters 
    {
      get { return _parameterValues; }
      set { _parameterValues = value; }
    }

    /// <include file='xmldocs/SoapClientMessage.cs.xml' path='doc/doc[@for="SoapClientMessage.Stage"]/*'/>
    public SoapMessageStage Stage
    {
      get { return _stage; }
    }

    /// <include file='xmldocs/SoapClientMessage.cs.xml' path='doc/doc[@for="SoapClientMessage.RequestID"]/*'/>
    public string RequestID
    {
      get { return _requestID;}
    }
    #endregion Public Properties

    #region Public Methods
    /// <include file='xmldocs/SoapClientMessage.cs.xml' path='doc/doc[@for="SoapClientMessage.GetInParameterValue"]/*'/>
    public object GetInParameterValue(int index_) 
    {
      if (index_ < 0 || index_ >= _parameterValues.Length) 
      {
        throw new IndexOutOfRangeException("index_");
      }

      return _parameterValues[index_];
    }

    /// <include file='xmldocs/SoapClientMessage.cs.xml' path='doc/doc[@for="SoapClientMessage.GetReturnType"]/*'/>
    public Type GetReturnType() 
    {
      if (_returnType == null) 
      {
        throw new InvalidOperationException(Res.EXCEPTION_NO_RETURN_VALUE);
      }
      
      return _returnType;
    }

    /// <include file='xmldocs/SoapClientMessage.cs.xml' path='doc/doc[@for="SoapClientMessage.GetReturnValue"]/*'/>
    public object GetReturnValue() 
    {
      this.EnsureStage(SoapMessageStage.AfterDeserialize);

      if (_returnType == null) 
      {
        throw new InvalidOperationException(Res.EXCEPTION_NO_RETURN_VALUE);
      }
      
      return _returnValue;
    }
    #endregion Public Methods

    #region Internal Properties
    internal XmlServiceExtension[] Extensions 
    {
      get { return _extensions; }
    }

    internal object ReturnValue
    {
      set { _returnValue = value; }
    }

    internal Type ReturnType 
    {
      set { _returnType = value; }
    }
    #endregion Internal Properties

    #region Internal Methods
    internal void SetBody(string body_) 
    {
      //Debug.Assert(body_ != null);

      // Create a document fragment containing the action and add it to the body
      // of the envelope.
      SoapEnvelopeDomImpl envelope = (SoapEnvelopeDomImpl) Envelope;
      SoapBodyDomImpl body = (SoapBodyDomImpl) envelope.Body;

      XmlDocumentFragment fragment = envelope.XmlDocument.CreateDocumentFragment();
      fragment.InnerXml = body_;      

      if (fragment.FirstChild.NodeType == XmlNodeType.XmlDeclaration) 
      {
        fragment.RemoveChild(fragment.FirstChild);
      }
      
      body.XmlElement.AppendChild(fragment);
    }

    internal void SetStage(SoapMessageStage stage_) 
    {
      _stage = stage_;
    }
    #endregion Internal Methods

    #region Private Methods
    private void EnsureStage(SoapMessageStage stage_) 
    {
      if ((_stage & stage_) == 0) 
      {
        throw new InvalidOperationException(Res.EXCEPTION_INVALID_STAGE);
      }
    }
    #endregion Private Methods
  }

  /// <include file='xmldocs/SoapClientMessage.cs.xml' path='doc/doc[@for="SoapMessageStage"]/*'/>
  public enum SoapMessageStage 
  {
    /// <include file='xmldocs/SoapClientMessage.cs.xml' path='doc/doc[@for="SoapMessageStage.BeforeSerialize"]/*'/>
    BeforeSerialize,

    /// <include file='xmldocs/SoapClientMessage.cs.xml' path='doc/doc[@for="SoapMessageStage.AfterSerialize"]/*'/>
    AfterSerialize,

    /// <include file='xmldocs/SoapClientMessage.cs.xml' path='doc/doc[@for="SoapMessageStage.BeforeDeserialize"]/*'/>
    BeforeDeserialize,    

    /// <include file='xmldocs/SoapClientMessage.cs.xml' path='doc/doc[@for="SoapMessageStage.AfterDeserialize"]/*'/>
    AfterDeserialize
  }
}
