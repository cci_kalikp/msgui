#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/Concord.ServiceDispatcher/ValidationExtension.cs#3 $
  $DateTime: 2013/01/18 15:24:18 $
    $Change: 813214 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion

using System;

using MorganStanley.IED.Concord.ServiceDispatcher;

namespace MorganStanley.IED.Concord.ServiceDispatcher
{
  /// <include file='xmldocs/ValidationExtension.cs.xml' path='doc/doc[@for="ValidationException"]/*'/>
  public class ValidationException : XmlServiceDispatchException
  {
    #region Constructors
    /// <include file='xmldocs/ValidationExtension.cs.xml' path='doc/doc[@for="ValidationException.ValidationException"]/*'/>
    public ValidationException(string message_) : base(message_) 
    {
    }

    /// <include file='xmldocs/ValidationExtension.cs.xml' path='doc/doc[@for="ValidationException.ValidationException1"]/*'/>
    public ValidationException(string message_, System.Exception innerException_) : base (message_, innerException_)
    {
    }
    #endregion Constructors
  }

  /// <include file='xmldocs/ValidationExtension.cs.xml' path='doc/doc[@for="ValidationExtension"]/*'/>
	internal class ValidationExtension : XmlServiceExtension
	{
    #region Public Methods
    /// <include file='xmldocs/ValidationExtension.cs.xml' path='doc/doc[@for="ValidationExtension.BeforeSerialize"]/*'/>
    public override void BeforeSerialize(SoapClientMessage message_) 
    {
      if (message_.ActionInfo.Validators == null) 
      {
        return;
      }

      for (int index = 0; index < message_.ActionInfo.Validators.Length; index++) 
      {
        Type type = message_.ActionInfo.Validators[index].Type;
        object instance = null;

        try 
        {
          instance = Activator.CreateInstance(type);
        } 
        catch (Exception e_) 
        {
          throw new ValidationException(String.Format(Res.EXCEPTION_UNABLE_TO_CREATE_INSTANCE, type.FullName), e_);
        }

        try 
        {
          // Cast instance to IValidator and call Validate() method.  Validators are expected
          // to throw exceptions when a validation failure occurs.
          ((IValidator) instance).Validate(message_.ActionInfo.Name, message_.Parameters);
        } 
        catch (Exception e_) 
        {
          throw new ValidationException(Res.EXCEPTION_ERROR_DURING_VALIDATION, e_);
        }
      }
    }
    #endregion Public Methods
	}
}
