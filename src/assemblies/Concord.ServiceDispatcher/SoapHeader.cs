#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/Concord.ServiceDispatcher/SoapHeader.cs#3 $
  $DateTime: 2013/01/18 15:24:18 $
    $Change: 813214 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion

using System;
using System.Xml;

namespace MorganStanley.IED.Concord.ServiceDispatcher
{
  /// <include file='xmldocs/SoapHeader.cs.xml' path='doc/doc[@for="SoapHeader"]/*'/>
	public class SoapHeader
	{
    #region Declarations
    private string _name;
    private string _prefix;
    private string _namespaceUri;
    private XmlDocument _document;
    private XmlElement _element;
    #endregion Declarations

    #region Constructors
    /// <include file='xmldocs/SoapHeader.cs.xml' path='doc/doc[@for="SoapHeader.SoapHeader"]/*'/>
		public SoapHeader()
		{
      _document = new XmlDocument();
      _document.LoadXml("<headerEntry></headerEntry>");
		}

    /// <include file='xmldocs/SoapHeader.cs.xml' path='doc/doc[@for="SoapHeader.SoapHeader1"]/*'/>
    public SoapHeader(string name_) : this()
    {
      _name = name_;
      _element = _document.CreateElement(name_);
      _document.DocumentElement.AppendChild(_element);
    }

    /// <include file='xmldocs/SoapHeader.cs.xml' path='doc/doc[@for="SoapHeader.SoapHeader2"]/*'/>
    public SoapHeader(string prefix_, string localName_, string namespaceURI_) : this(localName_)
    {
      _prefix = prefix_;
      _namespaceUri = namespaceURI_;

      _element = _document.CreateElement(prefix_, _name, namespaceURI_);
      _document.DocumentElement.AppendChild(_element);
    }
    #endregion Constructors

    #region Public Properties
    /// <include file='xmldocs/SoapHeader.cs.xml' path='doc/doc[@for="SoapHeader.Prefix"]/*'/>
    public string Prefix 
    {
      get { return _prefix; }
    }

    /// <include file='xmldocs/SoapHeader.cs.xml' path='doc/doc[@for="SoapHeader.Name"]/*'/>
    public string Name 
    {
      get { return _name; }
    }

    /// <include file='xmldocs/SoapHeader.cs.xml' path='doc/doc[@for="SoapHeader.NamespaceURI"]/*'/>
    public string NamespaceURI
    {
      get { return _namespaceUri; }
    }

    /// <include file='xmldocs/SoapHeader.cs.xml' path='doc/doc[@for="SoapHeader.Value"]/*'/>
    public string Value 
    {
      get { return _element.InnerText; }
      set { _element.InnerText = value; }
    }

    /// <include file='xmldocs/SoapHeader.cs.xml' path='doc/doc[@for="SoapHeader.XmlElement"]/*'/>
    public XmlElement XmlElement 
    {
      get { return _element; }
    }
    #endregion Public Properties
	}
}
