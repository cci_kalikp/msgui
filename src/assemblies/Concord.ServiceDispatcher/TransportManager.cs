#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/Concord.ServiceDispatcher/TransportManager.cs#7 $
  $DateTime: 2014/04/03 10:05:52 $
    $Change: 874926 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion

using System;
using System.Collections;
using System.Threading;
using System.Xml;
using MorganStanley.IED.Concord.ServiceDispatcher.Configuration;
using MorganStanley.IED.Concord.ServiceDispatcher.Transports;
using MorganStanley.IED.Concord.ServiceDispatcher.Transports.Http;

using MorganStanley.IED.Concord.Logging;
using MorganStanley.IED.Concord.Runtime;
using MorganStanley.IED.Concord.ServiceDispatcher.Transports.Tcp;
//using MorganStanley.MSDotNet.MSAuth;
using MorganStanley.MSDotNet.MSNet;
using MorganStanley.MSDotNet.MSSoap.Client;
using MorganStanley.MSDotNet.MSSoap.Common;
using MorganStanley.MSDotNet.MSSoap.SSL;

namespace MorganStanley.IED.Concord.ServiceDispatcher
{
  #region TransportManager Class
  /// <summary>
  /// Summary description for EndpointManager.
  /// </summary>
  public class TransportManager
  {
    #region Constants
    private const string CLASS_NAME = "TransportManager";

    public static readonly int DEFAULT_RECONNECT_TIMEOUT = 100;
    public static readonly bool DEFAULT_USEHEARTBEAT = true;
    public static readonly long DEFAULT_INVOKE_TIMEOUT = 45000;
    public static readonly bool DEFAULT_USE_ASYNC_CONNECTS = false;
    public static readonly bool DEFAULT_HANDLE_SYNC_CONNECT_ERRORS = true;
    public static readonly bool DEFAULT_ESTABLISH_CONNECTIONS_ONLOAD = true;
    public static readonly bool DEFAULT_CLOSE_CONNECTIONS_ONLOAD = true;
    public static readonly bool DEFAULT_CONNECT_ON_SEND = false;
    #endregion Constants

    #region Declarations
    private static object _lock = new object();
    private static Hashtable _connectionInfo = new Hashtable();
    private static Hashtable _requestResponseClients = new Hashtable();
    private static Hashtable _transports = new Hashtable();
    private static int _defaultHeartbeatInterval = SoapSSLTCPConnection.DEFAULT_HEARTBEAT_INTERVAL_SECONDS;
    private static bool _enableHeartbeat = DEFAULT_USEHEARTBEAT;
    private static int _defaultReconnectTimeout = DEFAULT_RECONNECT_TIMEOUT;
    private static long _defaultInvokeTimeout = DEFAULT_INVOKE_TIMEOUT;
    private static bool _closeConnectionsOnLoad = DEFAULT_CLOSE_CONNECTIONS_ONLOAD;
    private static bool _establishConnectionsOnLoad = DEFAULT_ESTABLISH_CONNECTIONS_ONLOAD;
    private static bool _asyncConnects = DEFAULT_USE_ASYNC_CONNECTS;
    private static bool _handleSyncConnectErrors = DEFAULT_HANDLE_SYNC_CONNECT_ERRORS;
    private static bool _connectOnSend = DEFAULT_CONNECT_ON_SEND;
    #endregion Declarations

    #region Constructors
    private TransportManager()
    {
    }
    #endregion Constructors

    #region Public Properties

    public static Hashtable ConnectionInfo
    {
      get { return _connectionInfo; }
    }

    public static bool CloseConnectionsOnConfigurationLoad
    {
      get { return _closeConnectionsOnLoad; }
      set { _closeConnectionsOnLoad = value; }
    }

    public static bool ConnectOnSend
    {
      get { return _connectOnSend; }
      set { _connectOnSend = value; }
    }

    public static int DefaultHeartbeatInterval
    {
      get { return _defaultHeartbeatInterval; }
      set { _defaultHeartbeatInterval = value; }
    }

    public static long DefaultInvokeTimeout
    {
      get { return _defaultInvokeTimeout; }
      set { _defaultInvokeTimeout = value; }
    }

    public static int DefaultReconnectTimeout
    {
      get { return _defaultReconnectTimeout; }
      set { _defaultReconnectTimeout = value; }
    }

    public static bool EnableHeartbeat
    {
      get { return _enableHeartbeat; }
      set { _enableHeartbeat = value; }
    }

    public static bool EstablishConnectionsOnLoad
    {
      get { return _establishConnectionsOnLoad; }
      set { _establishConnectionsOnLoad = value; }
    }

    public static bool HandleSyncConnectErrors
    {
      get { return _handleSyncConnectErrors; }
      set { _handleSyncConnectErrors = value; }
    }

    public static bool UseAsyncConnects
    {
      get { return _asyncConnects; }
      set { _asyncConnects = value; }
    }
    #endregion Public Properties

    #region Internal Methods
    public static void AddConnection(ConnectionInfo connectionInfo_)
    {
      lock (_lock)
      {
        if (!_transports.ContainsKey(connectionInfo_.Name))
        {
          _connectionInfo[connectionInfo_.Name] = connectionInfo_;

          if (connectionInfo_.Transport == Transport.Tcp ||
            connectionInfo_.Transport == Transport.KrbTcp ||
            connectionInfo_.Transport == Transport.KrbSspiTcp ||
            connectionInfo_.Transport == Transport.KrbAutoTcp ||
            connectionInfo_.Transport == Transport.KrbWcfTcp)
          {
            ISoapOutputTransport transport = CreateTcpConnection(connectionInfo_);
          }
        }
        else
        {
          if (Log.IsLogLevelEnabled(LogLevel.Warning))
          {
            LogLayer.Warning.SetLocation(CLASS_NAME, "AddConnection").WriteFormat("Connection {0} already defined",
              connectionInfo_.Name);
          }
        }
      }
    }

    internal static RequestResponseClient GetRequestResponseClient(ISoapOutputTransport transport_)
    {
      lock (_lock)
      {
        return _requestResponseClients[transport_] as RequestResponseClient;
      }
    }

    public static void Clear()
    {
      lock (_lock)
      {
        DisconnectAll();
        _transports.Clear();
      }
    }
    #endregion Internal Methods

    #region Event Handlers
    private static void Heartbeat(object sender_, SoapHeartBeatEventArgs e_)
    {
      if (Log.IsLogLevelEnabled(LogLevel.Debug))
      {
        LogLayer.Debug.SetLocation(CLASS_NAME, "Heartbeat").Write(e_.ID.ToString());
      }
    }

    private static void TCPConnect(object sender_, MSNetConnectEventArgs e_)
    {
      if (Log.IsLogLevelEnabled(LogLevel.Info))
      {
        LogLayer.Info.SetLocation(CLASS_NAME, "TCPConnect").Write(e_.ID.ToString());
      }
    }

    private static void TCPDisconnect(object sender_, MSNetDisconnectEventArgs e_)
    {
      if (Log.IsLogLevelEnabled(LogLevel.Error))
      {
        LogLayer.Error.SetLocation(CLASS_NAME, "TCPDisconnect").Write("Disconnect recieved for: " + e_.ID.ToString());
      }
    }

    private static void ConnectCallback(IAsyncResult result_)
    {
      MSNetTCPConnection connection = result_.AsyncState as MSNetTCPConnection;



      if (connection != null)
      {
        try
        {
          connection.EndConnect(result_);

          if (connection is IHeartBeatConnection && connection.IsConnected && _enableHeartbeat)
          {
            ((IHeartBeatConnection)connection).EnableHeartBeat();
          }
        }
        catch (Exception ex_)
        {
          LogLayer.Error.SetLocation(CLASS_NAME, "ConnectCallback").Write("Error establishing SoapTcp connection", ex_);
        }
      }
    }
    #endregion Event Handlers

    #region Public Methods
    public static void ConnectAll()
    {
      lock (_lock)
      {
        foreach (object transport in _transports.Values)
        {
          MSNetTCPConnection connection = transport as MSNetTCPConnection;

          try
          {
            if (connection != null && !connection.IsConnected)
            {
              if (UseAsyncConnects)
              {
                connection.BeginConnect(new AsyncCallback(ConnectCallback), connection);
              }
              else
              {
                connection.SyncConnect();

                if (transport is IHeartBeatConnection && connection.IsConnected && _enableHeartbeat)
                {
                  ((IHeartBeatConnection)transport).EnableHeartBeat();
                }
              }
            }
          }
          catch (Exception ex_)
          {
            if (Log.IsLogLevelEnabled(LogLevel.Error))
            {
              LogLayer.Error.SetLocation(CLASS_NAME, "Connect").WriteFormat("Connection to '{0}' failed.", ex_, connection.Address.ToString());
            }

            if (!HandleSyncConnectErrors)
            {
              throw;
            }
          }
        }
      }
    }


    public static void DisconnectAll()
    {
      lock (_lock)
      {
        foreach (object transport in _transports.Values)
        {
          MSNetTCPConnection tcpConnection = transport as MSNetTCPConnection;
          if (tcpConnection != null && tcpConnection.IsConnected)
          {
            tcpConnection.Close();
          }
        }
      }
    }

    public static Hashtable Connections
    {
      get
      {
        lock (_lock)
        {
          return _transports.Clone() as Hashtable;
        }
      }
    }

    public static ISoapOutputTransport GetTransport(IMSNetLoop loop_, string name_)
    {
      ConnectionInfo connectionInfo = null;

      lock (_lock)
      {
        connectionInfo = _connectionInfo[name_] as ConnectionInfo;
      }

      if (connectionInfo != null)
      {
        return GetTransport(loop_, connectionInfo);
      }
      else
      {
        return null;
      }
    }

    public static ISoapOutputTransport GetTransport(IMSNetLoop loop_, ConnectionInfo connectionInfo_)
    {
      if (connectionInfo_.Transport == Transport.Http)
      {
        return new SOAPHttpClient(loop_, connectionInfo_.Url);
      }
      else if (connectionInfo_.Transport == Transport.OpticksHttp)
      {
        return new Transports.OpticksHttp.OpticksHttpClient(loop_, connectionInfo_.Url);
      }
      else
      {
        lock (_lock)
        {
          if (_transports.Contains(connectionInfo_.Name))
          {
            return (ISoapOutputTransport)_transports[connectionInfo_.Name];
          }
        }
      }
      throw new ArgumentException(Res.EXCEPTION_TRANSPORT_DOES_NOT_EXIST);
    }

    public static bool IsConnected(ConnectionInfo connectionInfo_)
    {
      lock (_lock)
      {
        if (_transports.Contains(connectionInfo_.Name))
        {
          MSNetAbstractLayersConnectionAdapter connection = _transports[connectionInfo_.Name] as MSNetAbstractLayersConnectionAdapter;
          if (connection != null)
          {
            return connection.IsConnected;
          }
        }
      }
      return false;
    }

    #endregion Public Methods

    #region Private Methods
    private static void Connect(MSNetAbstractLayersConnectionAdapter connection_, ConnectionInfo connectionInfo_)
    {
      try
      {
        if (UseAsyncConnects)
        {
          connection_.BeginConnect(new AsyncCallback(ConnectCallback), connection_);
        }
        else
        {
          connection_.SyncConnect();

          if (connection_ is IHeartBeatConnection && connection_.IsConnected && _enableHeartbeat)
          {
            ((IHeartBeatConnection)connection_).EnableHeartBeat();
          }
        }
      }
      catch (Exception ex_)
      {
        if (Log.IsLogLevelEnabled(LogLevel.Error))
        {
          LogLayer.Error.SetLocation(CLASS_NAME, "Connect").WriteFormat("Connection to '{0}' failed.", ex_, connectionInfo_.HostPort);
        }

        if (!HandleSyncConnectErrors)
        {
          throw;
        }
      }
    }

    private static MSNetInetAddress GetMSNetAddress(string hostPort_)
    {
      if (string.IsNullOrEmpty(hostPort_))
      {
        if (Log.IsLogLevelEnabled(LogLevel.Warning))
        {
          LogLayer.Warning.SetLocation(CLASS_NAME, "GetMSNetAddress").WriteFormat("Host port is null / empty");
        }
        return null;
      }

      MSNetInetAddress msNetAddress = null;
      try
      {
        msNetAddress = new MSNetInetAddress(hostPort_);
      }
      catch (Exception ex)
      {
        if (Log.IsLogLevelEnabled(LogLevel.Warning))
        {
          LogLayer.Warning.SetLocation(CLASS_NAME, "CreateTcpConnection").WriteFormat(
            "Resolution of host failed, HostPort='{0}'", ex, hostPort_);
        }
      }

      return msNetAddress;
    }


    private static ISoapOutputTransport CreateTcpConnection(ConnectionInfo connectionInfo_)
    {
      MSNetInetAddress address = XmlServiceDispatcher.ConnectionFactory != null ? XmlServiceDispatcher.ConnectionFactory.GetMSNetAddress(connectionInfo_.HostPort) : GetMSNetAddress(connectionInfo_.HostPort);

      MSNetInetAddress backupAddress = null;

      if (!String.IsNullOrEmpty(connectionInfo_.BackupHostPort))
      {
        backupAddress = GetMSNetAddress(connectionInfo_.BackupHostPort);
      }

      if (address == null && backupAddress == null)
      {
        if (Log.IsLogLevelEnabled(LogLevel.Emergency))
        {
          LogLayer.Emergency.SetLocation(CLASS_NAME, "CreateTcpConnection").WriteFormat(
            "Resolution of primary and backup hosts failed, Connection='{0}', HostPort='{1}', BackupHostPort='{2}'",
            connectionInfo_.Name,
            connectionInfo_.HostPort,
            connectionInfo_.BackupHostPort);
        }
      }
      else
      {
        if (backupAddress == null && !String.IsNullOrEmpty(connectionInfo_.BackupHostPort))
        {
          if (Log.IsLogLevelEnabled(LogLevel.Warning))
          {
            LogLayer.Warning.SetLocation(CLASS_NAME, "CreateTcpConnection").WriteFormat(
              "Resolution of backup host failed, while resolution of primary host succeeded.  Ignoring backup: Connection='{0}', HostPort='{1}', BackupHostPort='{2}'",
              connectionInfo_.Name,
              connectionInfo_.HostPort,
              connectionInfo_.BackupHostPort);
          }
        }

        if (address == null)
        {
          if (Log.IsLogLevelEnabled(LogLevel.Critical))
          {
            LogLayer.Critical.SetLocation(CLASS_NAME, "CreateTcpConnection").WriteFormat(
              "Resolution of primary host failed while backup host succeeded. Switching to backup: Connection='{0}', HostPort='{1}', BackupHostPort='{2}'",
              connectionInfo_.Name,
              connectionInfo_.HostPort,
              connectionInfo_.BackupHostPort);
          }

          // assigning backup to primary
          address = backupAddress;
        }
      }

      ThreadedLoop threadedLoop = new ThreadedLoop("SoapTcp: " + connectionInfo_.HostPort);
      threadedLoop.Start();

      ISoapOutputTransport transport = null;

      if (Concord.Runtime.ConcordRuntime.Current.Services.SecureExternal)
      {
        MSNetAbstractLayersConnectionAdapter connection = null;
        if (XmlServiceDispatcher.ConnectionFactory != null)
        {
          connection = XmlServiceDispatcher.ConnectionFactory.CreateTcpConnection(threadedLoop.Loop,
            address,
            new MSNetID(threadedLoop.Thread.Name),
            new MSNetStringProtocol(),
            ConcordRuntime.Current.Services.CertificateContext,
            0, TimeSpan.FromMilliseconds(_defaultReconnectTimeout));
          if (connection != null)
          {
            connection.AuthenticationContext = ConcordRuntime.Current.UserInfo.BigDogContext;
          }
        }
        else
        {
          SoapSSLTCPConnection soapSsltcpConnection = new SoapSSLTCPConnection(threadedLoop.Loop,
            address,
            new MSNetID(threadedLoop.Thread.Name),
            new MSNetStringProtocol(),
            ConcordRuntime.Current.Services.CertificateContext,
            0, TimeSpan.FromMilliseconds(_defaultReconnectTimeout));

          connection = soapSsltcpConnection;
          if (soapSsltcpConnection != null)
          {
            soapSsltcpConnection.AuthenticationContext = ConcordRuntime.Current.UserInfo.BigDogContext;
            soapSsltcpConnection.Proxy = ConcordRuntime.Current.Services.GetProxyInstance();
          }
        }

        if (connection != null && connection is IHeartBeatConnection)
        {
          IHeartBeatConnection heartBeatConnection = connection as IHeartBeatConnection;
          heartBeatConnection.HeartBeatIntervalSeconds = _defaultHeartbeatInterval;
          heartBeatConnection.OnHeartBeat += new SoapHeartBeatDelegate(Heartbeat);
        }

        if (EstablishConnectionsOnLoad)
        {
          Connect(connection, connectionInfo_);
        }

        transport = connection as ISoapOutputTransport;
      }
      else
      {
        SoapTCPConnection connection;

        if (connectionInfo_.Transport == Transport.KrbTcp)
        {
          // need to have done:
          //  WinAurora.Module.Load("kerberos");
          connection = new SoapKrbTcpConnection(threadedLoop.Loop, address, new MSNetID(threadedLoop.Thread.Name), true);
        }
        else if (connectionInfo_.Transport == Transport.KrbSspiTcp)
        {
          // need to have installed:
          //  \\%DISTSRV%\apps\Microsoft\WebServiceExtensions\3.0\Go\
          connection = new SoapKrbSspiTcpConnection(threadedLoop.Loop, address, new MSNetID(threadedLoop.Thread.Name), true);
        }
        else if (connectionInfo_.Transport == Transport.KrbWcfTcp)
        {
          //uses WCF for Kerberos, works on both 32bit and 64bit without WSE or Module Load.
          connection = new SoapKrbWcfTcpConnection(threadedLoop.Loop, address, new MSNetID(threadedLoop.Thread.Name), true);
        }
        else if (connectionInfo_.Transport == Transport.KrbAutoTcp)
        {
          //uses KrbTcp for 32bit, or KrbSsPiTcp for 64bit. Need to have done/installed as above 
          connection = new SoapKrbAutoTcpConnection(threadedLoop.Loop, address, new MSNetID(threadedLoop.Thread.Name), true);
        }
        else
        {
          connection = new SoapTCPConnection(threadedLoop.Loop, address, new MSNetID(threadedLoop.Thread.Name), true);
        }

        if (backupAddress != null)
        {
          connection.BackupAddress = backupAddress;
          connection.OnDisconnect += new MSNetDisconnectDelegate(TCPDisconnect);
          connection.OnConnect += new MSNetConnectDelegate(TCPConnect);
        }

        if (EstablishConnectionsOnLoad)
        {
          Connect(connection, connectionInfo_);
        }

        transport = connection;
      }

      if (transport != null)
      {
        lock (_lock)
        {
          _transports.Add(connectionInfo_.Name, transport);

          if (connectionInfo_.UseRequestResponse)
          {
            _requestResponseClients[transport] = new Transports.RequestResponseClient((MorganStanley.MSDotNet.MSSoap.Server.ISoapInputTransport)transport, transport, threadedLoop.Loop);
          }
        }
      }

      return transport;
    }
    #endregion Private Methods

    #region Inner Classes

    #region ThreadedLoop Class
    private class ThreadedLoop
    {
      #region Declarations
      private ManualResetEvent _threadCreateEvent;
      private Thread _thread;
      private IMSNetLoop _loop;
      #endregion Declarations

      #region Constructors
      public ThreadedLoop(string threadName_)
      {
        _threadCreateEvent = new ManualResetEvent(false);
        _thread = new Thread(new ThreadStart(RealLoop));
        _thread.Name = threadName_;
      }
      #endregion Constructors

      #region Public Methods
      public void Start()
      {
        if (null != _loop)
        {
          throw new InvalidOperationException(Res.EXCEPTION_CANNOT_START_LOOP_TWICE);
        }

        this.CreateThread();
      }
      #endregion Public Methods

      #region Public Properties
      public IMSNetLoop Loop
      {
        get
        {
          if (null == _loop)
          {
            throw new InvalidOperationException(Res.EXCEPTION_LOOP_NOT_STARTED);
          }

          return _loop;
        }
      }

      public Thread Thread
      {
        get
        {
          if (null == _loop)
          {
            throw new InvalidOperationException(Res.EXCEPTION_LOOP_NOT_STARTED);
          }

          return _thread;
        }
      }
      #endregion Public Properties

      #region Private Methods
      private void CreateThread()
      {
        _threadCreateEvent = new ManualResetEvent(false);
        _thread.IsBackground = true;
        _thread.Start();
        _threadCreateEvent.WaitOne();
      }

      private void RealLoop()
      {
        _loop = new MSNetLoopDefaultImpl();

        while (true)
        {
          try
          {
            _threadCreateEvent.Set();
            _loop.Loop();
          }
          catch (Exception)
          {
            // TODO
          }
        }
      }
      #endregion Private Methods
    }
    #endregion ThreadedLoop Class

    #endregion Inner Classes
  }
  #endregion TransportManager Class


}
