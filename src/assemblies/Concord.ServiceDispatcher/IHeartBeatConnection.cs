﻿using MorganStanley.MSDotNet.MSSoap.SSL;

namespace MorganStanley.IED.Concord.ServiceDispatcher
{
    public interface IHeartBeatConnection
    {
        bool HeartBeatEnabled { get; }
        void DisableHeartBeat();
        void EnableHeartBeat();
        int HeartBeatIntervalSeconds { get; set; }
        event SoapHeartBeatDelegate OnHeartBeat;
    }
}
