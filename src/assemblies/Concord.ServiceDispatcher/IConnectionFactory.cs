﻿using System;
using MorganStanley.IED.Concord.ServiceDispatcher.Configuration;
using MorganStanley.MSDotNet.MSNet;
using MorganStanley.MSDotNet.MSNet.SSL;
using MorganStanley.MSDotNet.MSSoap.Client;
using MorganStanley.MSDotNet.MSSoap.SSL;

namespace MorganStanley.IED.Concord.ServiceDispatcher
{
  public interface IConnectionFactory
  {
    MSNetAbstractLayersConnectionAdapter CreateTcpConnection(IMSNetLoop loop_, MSNetInetAddress address_,
      MSNetID id_,
      IMSNetProtocol protocol_,
      MSNetServerCertificateValidator serverAuthContext_,
      int cacheSize_,
      TimeSpan reconnectTimespan_);

    MSNetInetAddress GetMSNetAddress(string hostPort_);
  }
}
