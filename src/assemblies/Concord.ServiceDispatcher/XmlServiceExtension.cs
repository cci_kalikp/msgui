#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/Concord.ServiceDispatcher/XmlServiceExtension.cs#3 $
  $DateTime: 2013/01/18 15:24:18 $
    $Change: 813214 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion

using System;

namespace MorganStanley.IED.Concord.ServiceDispatcher
{
	/// <include file='xmldocs/XmlServiceExtension.cs.xml' path='doc/doc[@for="XmlServiceExtension"]/*'/>
	public abstract class XmlServiceExtension
	{
    /// <include file='xmldocs/XmlServiceExtension.cs.xml' path='doc/doc[@for="XmlServiceExtension.BeforeSerialize"]/*'/>
    public virtual void BeforeSerialize(SoapClientMessage message_) 
    {
    }

    /// <include file='xmldocs/XmlServiceExtension.cs.xml' path='doc/doc[@for="XmlServiceExtension.AfterSerialize"]/*'/>
    public virtual void AfterSerialize(SoapClientMessage message_)
    {
    }

    /// <include file='xmldocs/XmlServiceExtension.cs.xml' path='doc/doc[@for="XmlServiceExtension.BeforeDeserialize"]/*'/>
    public virtual void BeforeDeserialize(SoapClientMessage message_)
    {
    }

    /// <include file='xmldocs/XmlServiceExtension.cs.xml' path='doc/doc[@for="XmlServiceExtension.AfterDeserialize"]/*'/>
    public virtual void AfterDeserialize(SoapClientMessage message_)
    {
    }
	}
}
