#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/Concord.ServiceDispatcher/SoapHeaderCollection.cs#3 $
  $DateTime: 2013/01/18 15:24:18 $
    $Change: 813214 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion

using System;
using System.Collections;

namespace MorganStanley.IED.Concord.ServiceDispatcher
{
  /// <include file='xmldocs/SoapHeaderCollection.cs.xml' path='doc/doc[@for="SoapHeaderCollection"]/*'/>
	public class SoapHeaderCollection : CollectionBase
	{
    #region Constructors
    internal SoapHeaderCollection() 
    {
    }
    #endregion Constructors

    #region Public Methods
    /// <include file='xmldocs/SoapHeaderCollection.cs.xml' path='doc/doc[@for="SoapHeaderCollection.Item"]/*'/>
    public SoapHeader this[int index_] 
    {
      get	{return ((SoapHeader)(this.List[index_]));}
    }
		
    /// <include file='xmldocs/SoapHeaderCollection.cs.xml' path='doc/doc[@for="SoapHeaderCollection.Add"]/*'/>
    public int Add(SoapHeader header_) 
    {
      return this.List.Add(header_);
    }
		
    /// <include file='xmldocs/SoapHeaderCollection.cs.xml' path='doc/doc[@for="SoapHeaderCollection.Remove"]/*'/>
    public void Remove(SoapHeader header_) 
    {
      List.Remove(header_);
    }
		
    /// <include file='xmldocs/SoapHeaderCollection.cs.xml' path='doc/doc[@for="SoapHeaderCollection.GetEnumerator"]/*'/>
    public new SoapHeaderCollectionEnumerator GetEnumerator()	
    {
      return new SoapHeaderCollectionEnumerator(this);
    }
    #endregion Public Methods
		
    #region Public Inner Classes
    /// <include file='xmldocs/SoapHeaderCollection.cs.xml' path='doc/doc[@for="SoapHeaderCollectionEnumerator"]/*'/>
    public class SoapHeaderCollectionEnumerator : IEnumerator	
    {
      private	IEnumerator _enumerator;
      private	IEnumerable _temp;

      internal SoapHeaderCollectionEnumerator(SoapHeaderCollection mappings_)
      {
        _temp =	((IEnumerable)(mappings_));
        _enumerator = _temp.GetEnumerator();
      }
			
      /// <include file='xmldocs/SoapHeaderCollection.cs.xml' path='doc/doc[@for="SoapHeaderCollectionEnumerator.Current"]/*'/>
      public SoapHeader Current
      {
        get {return ((SoapHeader)(_enumerator.Current));}
      }
			
      object IEnumerator.Current
      {
        get {return _enumerator.Current;}
      }
			
      /// <include file='xmldocs/SoapHeaderCollection.cs.xml' path='doc/doc[@for="SoapHeaderCollectionEnumerator.MoveNext"]/*'/>
      public bool MoveNext()
      {
        return _enumerator.MoveNext();
      }
			
      bool IEnumerator.MoveNext()
      {
        return _enumerator.MoveNext();
      }
			
      /// <include file='xmldocs/SoapHeaderCollection.cs.xml' path='doc/doc[@for="SoapHeaderCollectionEnumerator.Reset"]/*'/>
      public void Reset()
      {
        _enumerator.Reset();
      }
			
      void IEnumerator.Reset() 
      {
        _enumerator.Reset();
      }
    }
    #endregion Public Inner Classes
  }
}
