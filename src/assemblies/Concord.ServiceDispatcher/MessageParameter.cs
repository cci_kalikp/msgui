#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/Concord.ServiceDispatcher/MessageParameter.cs#3 $
  $DateTime: 2013/01/18 15:24:18 $
    $Change: 813214 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion

using System;
using System.Collections;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace MorganStanley.IED.Concord.ServiceDispatcher
{
  /// <include file='xmldocs/MessageParameter.cs.xml' path='doc/doc[@for="MessageParameter"]/*'/>
	abstract public class MessageParameter
	{
    #region Declarations
    private SerializableHashtable _extendedProperties;
    #endregion Declarations

    #region Public Properties
    /// <include file='xmldocs/MessageParameter.cs.xml' path='doc/doc[@for="MessageParameter.ExtendedProperties"]/*'/>
    public SerializableHashtable ExtendedProperties 
    {
      get { return _extendedProperties; }
      set { _extendedProperties = value; }
    }
    #endregion Public Properties
	}

  /// <include file='xmldocs/MessageParameter.cs.xml' path='doc/doc[@for="SerializableHashtable"]/*'/>
  public class SerializableHashtable : Hashtable, IXmlSerializable
  {
    #region Constructors
    /// <include file='xmldocs/MessageParameter.cs.xml' path='doc/doc[@for="SerializableHashtable.SerialiableHashtable"]/*'/>
    public SerializableHashtable()
    {
    }
    #endregion Constructors

    #region Implementation of IXmlSerializable
    /// <include file='xmldocs/MessageParameter.cs.xml' path='doc/doc[@for="SerializableHashtable.WriteXml"]/*'/>
    public void WriteXml(System.Xml.XmlWriter writer_)
    {
      if (this.Count == 0) 
      {
        return;
      }

      foreach (object hashKey in this.Keys) 
      {
        writer_.WriteStartElement("Item");
        writer_.WriteAttributeString("name", hashKey.ToString());
        XmlSerializer serializer = new XmlSerializer(this[hashKey].GetType());
        serializer.Serialize(writer_, this[hashKey]);
        writer_.WriteEndElement();
      }
    }

    /// <include file='xmldocs/MessageParameter.cs.xml' path='doc/doc[@for="SerializableHashtable.GetSchema"]/*'/>
    public System.Xml.Schema.XmlSchema GetSchema()
    {
      return null;
    }

    /// <include file='xmldocs/MessageParameter.cs.xml' path='doc/doc[@for="SerializableHashtable.ReadXml"]/*'/>
    public void ReadXml(System.Xml.XmlReader reader_)
    {
      throw new NotImplementedException(String.Format(Res.EXCEPTION_DESERIALIZE_NOT_SUPPORTED, "SerializableHashtable"));
    }
    #endregion
  }
}
