#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/Concord.ServiceDispatcher/XsltServiceExtension.cs#3 $
  $DateTime: 2013/01/18 15:24:18 $
    $Change: 813214 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion

using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;

using MorganStanley.IED.Concord.Logging;
using MorganStanley.IED.Concord.ServiceDispatcher.Configuration;

using MorganStanley.MSDotNet.MSSoap.Common;

namespace MorganStanley.IED.Concord.ServiceDispatcher
{
  /// <include file='xmldocs/XsltServiceExtension.cs.xml' path='doc/doc[@for="TransformationException"]/*'/>
  public class TransformationException : XmlServiceDispatchException
  {
    #region Constructors
    /// <include file='xmldocs/XsltServiceExtension.cs.xml' path='doc/doc[@for="TransformationException.TransformationException"]/*'/>
    public TransformationException(string message_) : base(message_) 
    {
    }

    /// <include file='xmldocs/XsltServiceExtension.cs.xml' path='doc/doc[@for="TransformationException.TransformationException1"]/*'/>
    public TransformationException(string message_, Exception innerException_) : base(message_, innerException_)
    {
    }
    #endregion Constructors
  }

  /// <include file='xmldocs/XsltServiceExtension.cs.xml' path='doc/doc[@for="XsltServiceExtension"]/*'/>
  public class XsltServiceExtension : XmlServiceExtension
  {
    #region Declarations
    private static Hashtable _transforms;

    private const string REQUEST_ACTION = "Request";
    private const string RESPONSE_ACTION = "Response";
    #endregion Declarations

    #region Constructors
    static XsltServiceExtension()
    {
      _transforms = new Hashtable();
    }
    #endregion Constructors

    #region Public Methods
    /// <include file='xmldocs/XsltServiceExtension.cs.xml' path='doc/doc[@for="XsltServiceExtension.BeforeDeserialize"]/*'/>
    public override void BeforeDeserialize(SoapClientMessage message_) 
    {
      //Debug.Assert(message_ != null);
      this.Transform(message_);
    }

    /// <include file='xmldocs/XsltServiceExtension.cs.xml' path='doc/doc[@for="XsltServiceExtension.AfterSerialize"]/*'/>
    public override void AfterSerialize(SoapClientMessage message_) 
    {
      //Debug.Assert(message_ != null);
      this.Transform(message_);
    }
    #endregion Public Methods

    #region Internal Methods
    internal static void ClearCache() 
    {
      if (_transforms != null) 
      {
        _transforms.Clear();
      }
    }
    #endregion Internal Methods

    #region Protected Methods
    /// <include file='xmldocs/XsltServiceExtension.cs.xml' path='doc/doc[@for="XsltServiceExtension.Transform"]/*'/>
    protected virtual void Transform(SoapClientMessage message_) 
    {
      Xslt transformInfo = null;
      string fullActionName = null;
      bool fromCache = false;

      if (message_ == null) 
      {
        throw new ArgumentNullException("mesage_");
      }

      // Get the transform information from the ActionInfo.  If none is present, just exit.
      if (message_.Stage == SoapMessageStage.AfterSerialize) 
      {
        if (message_.ActionInfo == null || message_.ActionInfo.RequestInfo == null ||
          message_.ActionInfo.RequestInfo.Xslt == null) 
        {
          return;
        }

        transformInfo = message_.ActionInfo.RequestInfo.Xslt;
        fullActionName = String.Format("{0}.{1}", message_.ActionInfo.Name, REQUEST_ACTION);
      } 
      else if (message_.Stage == SoapMessageStage.BeforeDeserialize) 
      {
        if (message_.ActionInfo == null || message_.ActionInfo.ResponseInfo == null ||
          message_.ActionInfo.ResponseInfo.Xslt == null) 
        {
          return;
        }

        transformInfo = message_.ActionInfo.ResponseInfo.Xslt;
        fullActionName = String.Format("{0}.{1}", message_.ActionInfo.Name, RESPONSE_ACTION);
      }

      XslTransform transform = null;

      // Check the transform cache for an instance of the XslTransform.  If it is present
      // use it, otherwise create a new instance and add it to the cache.
      lock (_transforms.SyncRoot) 
      {
        if (_transforms[fullActionName] == null) 
        {
          transform = new XslTransform();

          try 
          {
            if (transformInfo.Type == XsltType.Uri) 
            {
              transform.Load(transformInfo.Value);
            } 
            else 
            {
              transform.Load(new XmlTextReader(new StringReader(transformInfo.Value)));
            }
          } 
          catch (Exception e_) 
          {
            // Caller Inform
            throw new TransformationException(String.Format(Res.EXCEPTION_UNABLE_TO_LOAD_STYLESHEET, fullActionName), e_);
          }

          _transforms.Add(fullActionName, transform);
          fromCache = false;
        } 
        else 
        {
          transform = (XslTransform) _transforms[fullActionName];
          fromCache = true;
        }
      }

      // Perform the transformation of the soap body.  This might throw an XsltException.  If it does, 
      // we'll let it be caught higher up the stack - caller beware.
      if (transform != null) 
      {
        SoapEnvelopeDomImpl envelope = (SoapEnvelopeDomImpl) message_.Envelope;

        #region Logging
        if (Log.IsLogLevelEnabled(LogLevel.Debug))
        {
          LogLayer.Debug.SetLocation("XsltServiceExtension", message_.Stage.ToString()).WriteFormat("{0} Action='{1}', FromCache='{2}', TransformInfo.Type='{3}', Stylesheet='{4}', Envelope='{5}'", "BeforeTransform", fullActionName, fromCache, transformInfo.Type.ToString(), transformInfo.Value, envelope.XmlDocument.OuterXml);
        }
        #endregion Logging

        // Transformations are plagued with namespace problems if we just pass the reference to the Action XmlElement
        // to the transform method.  To get around this, an interim XPathDocument is used.  Once the namespace
        // conflicts are fixed, this can be changed.
        StringWriter writer = new StringWriter();
        XPathDocument input = new XPathDocument(new StringReader(((SoapActionDomImpl)envelope.Body.Action).XmlElement.OuterXml));
        transform.Transform(input, null, writer);
        ((SoapBodyDomImpl) message_.Envelope.Body).XmlElement.InnerXml = writer.ToString();

        #region Logging
        if (Log.IsLogLevelEnabled(LogLevel.Debug)) 
        {
          LogLayer.Debug.SetLocation("XsltServiceExtension", message_.Stage.ToString()).WriteFormat("{0} Action='{1}', Envelope='{2}'", "AfterTransform", fullActionName, envelope.XmlDocument.OuterXml);
        }
        #endregion Logging
      }
    }
    #endregion Protected Methods
  }
}
