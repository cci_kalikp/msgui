#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/Concord.ServiceDispatcher/Applets/ServicesOptionPage.cs#6 $
  $DateTime: 2014/01/28 04:31:25 $
    $Change: 864173 $
    $Author: caijin $
_____________________________________________________________________________*/
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

using MorganStanley.IED.Concord.Application;
using MorganStanley.IED.Concord.ServiceDispatcher.Configuration;

namespace MorganStanley.IED.Concord.ServiceDispatcher.Applets
{
    public class ServicesOptionPage :   /* System.Windows.Forms.UserControl // */ OptionPageBase
  //public class ServicesOptionPage :   System.Windows.Forms.UserControl //  OptionPage
  {
    private System.Windows.Forms.ImageList _images;
    private System.Windows.Forms.ComboBox _serviceList;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Panel _messagePanel;
    private System.Windows.Forms.PictureBox pictureBox1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Panel _informationPanel;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.Label _versionLabel;
    private System.Windows.Forms.Label _URLLabel;
    private System.Windows.Forms.Label _serviceIDLabel;
    private System.Windows.Forms.Label _timeoutLabel;
    private System.Windows.Forms.Label _serviceLabel;
    private System.Windows.Forms.CheckBox _validationEnabledCheckBox;
    private System.Windows.Forms.ListBox _actionList;
    private System.Windows.Forms.Label label8;
    private PictureBox _connectionPictureBox;
    private IContainer components;

    #region Constructors
    public ServicesOptionPage()
    {
      InitializeComponent();
    }
    #endregion Constructors

    #region Implementation of IOptionPage
    public override void OnDisplay()
    {
      try
      {
        _serviceList.Items.Clear();


        foreach (KeyValuePair<string, IServiceSetting> pair in ServiceManager.Service)
        {
          string aliasName = pair.Key;
          _serviceList.Items.Add(aliasName);
        }

        // If there are no services, show the message panel indicating such
        _messagePanel.Location = new Point(0, 0);
          _messagePanel.Dock = DockStyle.Fill;
        _messagePanel.Visible = (_serviceList.Items.Count == 0);
        _informationPanel.Visible = false;
      }
      catch(Exception e_)
      {
        System.Windows.Forms.MessageBox.Show(e_.ToString());
      }
    }
    #endregion Implementation of IOptionPage

    #region Protected Methods
    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    protected override void Dispose( bool disposing )
    {
      if( disposing )
      {
        if(components != null)
        {
          components.Dispose();
        }
      }
      base.Dispose( disposing );
    }
    #endregion Protected Methods

		#region Component Designer generated code
    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ServicesOptionPage));
      this._images = new System.Windows.Forms.ImageList(this.components);
      this._serviceList = new System.Windows.Forms.ComboBox();
      this.label1 = new System.Windows.Forms.Label();
      this._messagePanel = new System.Windows.Forms.Panel();
      this.label2 = new System.Windows.Forms.Label();
      this.pictureBox1 = new System.Windows.Forms.PictureBox();
      this._informationPanel = new System.Windows.Forms.Panel();
      this._connectionPictureBox = new System.Windows.Forms.PictureBox();
      this.label8 = new System.Windows.Forms.Label();
      this._actionList = new System.Windows.Forms.ListBox();
      this._timeoutLabel = new System.Windows.Forms.Label();
      this._serviceIDLabel = new System.Windows.Forms.Label();
      this._URLLabel = new System.Windows.Forms.Label();
      this._versionLabel = new System.Windows.Forms.Label();
      this._serviceLabel = new System.Windows.Forms.Label();
      this._validationEnabledCheckBox = new System.Windows.Forms.CheckBox();
      this.label7 = new System.Windows.Forms.Label();
      this.label6 = new System.Windows.Forms.Label();
      this.label5 = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this._messagePanel.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
      this._informationPanel.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this._connectionPictureBox)).BeginInit();
      this.SuspendLayout();
      // 
      // _images
      // 
      this._images.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("_images.ImageStream")));
      this._images.TransparentColor = System.Drawing.Color.Magenta;
      this._images.Images.SetKeyName(0, "connected");
      this._images.Images.SetKeyName(1, "disconnected");
      this._images.Images.SetKeyName(2, "notapplicable");
      // 
      // _serviceList
      // 
      this._serviceList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this._serviceList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this._serviceList.Location = new System.Drawing.Point(8, 28);
      this._serviceList.Name = "_serviceList";
      this._serviceList.Size = new System.Drawing.Size(729, 21);
      this._serviceList.TabIndex = 0;
      this._serviceList.SelectedIndexChanged += new System.EventHandler(this._serviceList_SelectedIndexChanged);
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(8, 8);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(51, 13);
      this.label1.TabIndex = 1;
      this.label1.Text = "Services:";
      // 
      // _messagePanel
      // 
      this._messagePanel.Controls.Add(this.pictureBox1);
      this._messagePanel.Controls.Add(this.label2);
      this._messagePanel.Location = new System.Drawing.Point(84, 304);
      this._messagePanel.Name = "_messagePanel";
      this._messagePanel.Size = new System.Drawing.Size(386, 266);
      this._messagePanel.TabIndex = 2;
      this._messagePanel.Visible = false; 
      // 
      // label2
      //  
      this.label2.Dock = DockStyle.Fill;
      this.label2.BringToFront();
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(188, 16);
      this.label2.TextAlign = ContentAlignment.MiddleCenter;
      this.label2.TabIndex = 1; 
      this.label2.Text = "No services have been loaded .";
      // 
      // pictureBox1
      // 
      this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image"))); 
      this.pictureBox1.Dock = DockStyle.Left; 
      this.pictureBox1.Name = "pictureBox1";
      this.pictureBox1.Size = new System.Drawing.Size(74, 32);
      this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
      this.pictureBox1.TabIndex = 0;
      this.pictureBox1.TabStop = false;
      // 
      // _informationPanel
      // 
      this._informationPanel.Controls.Add(this._connectionPictureBox);
      this._informationPanel.Controls.Add(this.label8);
      this._informationPanel.Controls.Add(this._actionList);
      this._informationPanel.Controls.Add(this._timeoutLabel);
      this._informationPanel.Controls.Add(this._serviceIDLabel);
      this._informationPanel.Controls.Add(this._URLLabel);
      this._informationPanel.Controls.Add(this._versionLabel);
      this._informationPanel.Controls.Add(this._serviceLabel);
      this._informationPanel.Controls.Add(this._validationEnabledCheckBox);
      this._informationPanel.Controls.Add(this.label7);
      this._informationPanel.Controls.Add(this.label6);
      this._informationPanel.Controls.Add(this.label5);
      this._informationPanel.Controls.Add(this.label4);
      this._informationPanel.Controls.Add(this.label3);
      this._informationPanel.Location = new System.Drawing.Point(48, 58);
      this._informationPanel.Name = "_informationPanel";
      this._informationPanel.Size = new System.Drawing.Size(386, 216);
      this._informationPanel.TabIndex = 3;
      this._informationPanel.Visible = false;
      // 
      // _connectionPictureBox
      // 
      this._connectionPictureBox.Location = new System.Drawing.Point(364, 54);
      this._connectionPictureBox.Name = "_connectionPictureBox";
      this._connectionPictureBox.Size = new System.Drawing.Size(16, 16);
      this._connectionPictureBox.TabIndex = 13;
      this._connectionPictureBox.TabStop = false;
      // 
      // label8
      // 
      this.label8.AutoSize = true;
      this.label8.Location = new System.Drawing.Point(4, 100);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(45, 13);
      this.label8.TabIndex = 12;
      this.label8.Text = "Actions:";
      // 
      // _actionList
      // 
      this._actionList.Location = new System.Drawing.Point(52, 98);
      this._actionList.Name = "_actionList";
      this._actionList.Size = new System.Drawing.Size(328, 108);
      this._actionList.TabIndex = 11;
      // 
      // _timeoutLabel
      // 
      this._timeoutLabel.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
      this._timeoutLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._timeoutLabel.Location = new System.Drawing.Point(270, 32);
      this._timeoutLabel.Name = "_timeoutLabel";
      this._timeoutLabel.Size = new System.Drawing.Size(110, 16);
      this._timeoutLabel.TabIndex = 10;
      // 
      // _serviceIDLabel
      // 
      this._serviceIDLabel.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
      this._serviceIDLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._serviceIDLabel.Location = new System.Drawing.Point(270, 10);
      this._serviceIDLabel.Name = "_serviceIDLabel";
      this._serviceIDLabel.Size = new System.Drawing.Size(110, 16);
      this._serviceIDLabel.TabIndex = 9;
      // 
      // _URLLabel
      // 
      this._URLLabel.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
      this._URLLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._URLLabel.Location = new System.Drawing.Point(52, 54);
      this._URLLabel.Name = "_URLLabel";
      this._URLLabel.Size = new System.Drawing.Size(306, 16);
      this._URLLabel.TabIndex = 8;
      // 
      // _versionLabel
      // 
      this._versionLabel.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
      this._versionLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._versionLabel.Location = new System.Drawing.Point(52, 32);
      this._versionLabel.Name = "_versionLabel";
      this._versionLabel.Size = new System.Drawing.Size(144, 16);
      this._versionLabel.TabIndex = 7;
      // 
      // _serviceLabel
      // 
      this._serviceLabel.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
      this._serviceLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._serviceLabel.Location = new System.Drawing.Point(52, 10);
      this._serviceLabel.Name = "_serviceLabel";
      this._serviceLabel.Size = new System.Drawing.Size(144, 16);
      this._serviceLabel.TabIndex = 6;
      // 
      // _validationEnabledCheckBox
      // 
      this._validationEnabledCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
      this._validationEnabledCheckBox.Location = new System.Drawing.Point(4, 78);
      this._validationEnabledCheckBox.Name = "_validationEnabledCheckBox";
      this._validationEnabledCheckBox.Size = new System.Drawing.Size(124, 19);
      this._validationEnabledCheckBox.TabIndex = 5;
      this._validationEnabledCheckBox.Text = "Validation Enabled:";
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(208, 34);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(48, 13);
      this.label7.TabIndex = 4;
      this.label7.Text = "Timeout:";
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(4, 56);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(32, 13);
      this.label6.TabIndex = 3;
      this.label6.Text = "URL:";
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(4, 34);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(45, 13);
      this.label5.TabIndex = 2;
      this.label5.Text = "Version:";
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(208, 12);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(60, 13);
      this.label4.TabIndex = 1;
      this.label4.Text = "Service ID:";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(4, 12);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(46, 13);
      this.label3.TabIndex = 0;
      this.label3.Text = "Service:";
      // 
      // ServicesOptionPage
      // 
      this.Controls.Add(this._messagePanel);
      this.Controls.Add(this.label1);
      this.Controls.Add(this._serviceList);
      this.Controls.Add(this._informationPanel);
      this.Name = "ServicesOptionPage";
      this.Size = new System.Drawing.Size(740, 448);
      this._messagePanel.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
      this._informationPanel.ResumeLayout(false);
      this._informationPanel.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this._connectionPictureBox)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }
		#endregion

    private void _serviceList_SelectedIndexChanged(object sender, System.EventArgs e)
    {
      IServiceSetting service = ServiceManager.Service[_serviceList.Text];

      if (service != null) 
      {
        _serviceLabel.Text = service.Service;
        _serviceIDLabel.Text = service.ServiceID;
        _versionLabel.Text = service.Version;

        if (service.Connection.Transport == Transport.Http || service.Connection.Transport == Transport.OpticksHttp)
        {
          _URLLabel.Text =  service.Connection.Url;
          _connectionPictureBox.Image = _images.Images["notapplicable"];
        }
        else if (service.Connection.Transport == Transport.Tcp)
        {
          _URLLabel.Text = service.Connection.HostPort;

          _connectionPictureBox.Image = TransportManager.IsConnected(service.Connection) ? _images.Images["connected"] : _images.Images["disconnected"];
        }

        _timeoutLabel.Text = service.Timeout.ToString();
        _validationEnabledCheckBox.Checked = service.ValidationEnabled;

        _actionList.Items.Clear();

        foreach (ActionInfo actionInfo in service.Actions)
        {
          _actionList.Items.Add(actionInfo.Name);
        }
        
        _informationPanel.Location = new Point(0, _serviceList.Location.Y + _serviceList.Height + 2);
        _informationPanel.Visible = true;
      }
      else
      {
        _informationPanel.Visible = false;
      }
    } 
  }
}
