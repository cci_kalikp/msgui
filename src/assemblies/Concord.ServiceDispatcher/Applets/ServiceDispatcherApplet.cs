#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/Concord.ServiceDispatcher/Applets/ServiceDispatcherApplet.cs#6 $
  $DateTime: 2014/02/10 14:14:12 $
    $Change: 865990 $
    $Author: anbuy $
_____________________________________________________________________________*/
#endregion

using System;
using System.Collections.Generic;
using System.Net;
using System.Timers;
using System.Xml;

using MorganStanley.IED.Concord.Application;
using MorganStanley.IED.Concord.Configuration;
using MorganStanley.IED.Concord.Logging;
using MorganStanley.IED.Concord.ServiceDispatcher.Configuration;
using MorganStanley.MSDotNet.MSNet;

namespace MorganStanley.IED.Concord.ServiceDispatcher.Applets
{
  [ConfigurationSection("ServiceDisp")]
	public class ServiceDispatcherApplet : ConcordAppletBase
  {
    #region Constants
    internal const string ClassName = "ServiceDispatcherApplet";
    #endregion Constants

    #region Constructors
    public ServiceDispatcherApplet(IMSNetLoop loop_) : base (loop_)
		{
    }
    #endregion Constructors



    private class TimerSetting
    {
        internal readonly int Hour;
        internal readonly int Min;
        internal readonly int Sec;

        private readonly MSNetTCPEndPoint _endpoint;
        
        private readonly Timer _timer;

        private readonly Action<MSNetTCPEndPoint> _onTimeAction;

        private bool Before(DateTime time)
        {
            if (Hour < time.Hour)
                return true;
            if (Hour > time.Hour)
                return false;
            if (Min < time.Minute)
                return true;
            if (Min > time.Minute)
                return false;
            return Sec <= time.Second;
        }

        internal bool Before(TimerSetting ts)
        {
            if (Hour < ts.Hour)
                return true;
            if (Hour > ts.Hour)
                return false;
            if (Min < ts.Min)
                return true;
            if (Min > ts.Min)
                return false;

            return Sec <= ts.Sec;
        }

        private void ReSetTimer()
        {
            var now = DateTime.UtcNow;
            var secs = (Hour - now.Hour)*3600 + (Min - now.Minute)*60 + (Sec - now.Second);

            if (secs < 0)
            {
                secs += 24*3600;
            }
            _timer.Interval = secs*1000;
            _timer.Start();
        }

        public void Initialize()
        {
            if (Before(DateTime.UtcNow))
            {
                _onTimeAction(_endpoint);
            }

            _timer.Elapsed += (s, e) =>
            {
                _onTimeAction(_endpoint);
                ReSetTimer();
            };
            

            ReSetTimer();
        }

        public TimerSetting(string time, MSNetTCPEndPoint endpoint, Action<MSNetTCPEndPoint> onTimeAction)
        {
            var ts = time.Split(':');
            Hour = int.Parse(ts[0]);
            Min = int.Parse(ts[1]);
            Sec = int.Parse(ts[2]);
            
            _onTimeAction = onTimeAction;
            _endpoint = endpoint;
            _timer = new Timer() { AutoReset = false };
        }

       
    }

      private List<TimerSetting> _timerSettings = new List<TimerSetting>();

     

    private void LoadMSNetLoggingLevelConfig(XmlElement logNode)
    {
        
        foreach(var node  in logNode.GetElementsByTagName("suppress"))
        {
            try
            {
                var element = (XmlElement)node;

                var fromElem = element.GetElementsByTagName("from")[0];
                var toElem = element.GetElementsByTagName("to")[0];

                var fromTime = fromElem.InnerText;
                var toTime = toElem.InnerText;

                var endpointStr = element.GetElementsByTagName("endpoint")[0].InnerText;

                MSNetTCPEndPoint endpoint;
                long ip;
                int port;
                if (long.TryParse(endpointStr.Split(':')[0], out ip) && int.TryParse(endpointStr.Split(':')[1], out port)) // will need either ip address of domain:port address.
                {
                    endpoint = new MSNetTCPEndPoint(ip, port);
                }
                else
                {
                    endpoint = new MSNetTCPEndPoint(new IPAddress(0), 0, new MSNetInetAddress(endpointStr));
                }


                var fromSetting = new TimerSetting(fromTime, endpoint,
                                                   (ep) =>
                                                       {
                                                           LogLayer.Info.SetLocation("ServiceDispatcherApplet",
                                                                                     "Timer Event").WriteFormat(
                                                                                         "Turn off logging for endpoint " +
                                                                                         endpointStr);
                                                           MSNetTCPChannel.AddNoErrorEndPoint(ep);
                                                       });
                var toSetting = new TimerSetting(toTime, endpoint,
                                                 (ep) =>
                                                     {
                                                         LogLayer.Info.SetLocation("ServiceDispatcherApplet",
                                                                                   "Timer Event").WriteFormat(
                                                                                       "Turn on logging for endpoint " +
                                                                                       endpointStr);
                                                         MSNetTCPChannel.RemoveNoErrorEndPoint(ep);
                                                     });

                if (toSetting.Before(fromSetting))
                {
                    LogLayer.Info.SetLocation("ServiceDispatcherApplet", "LoadMSNetLoggingLevelConfig").WriteFormat(
                        "Turn off logging for endpoint " + endpointStr);
                    MSNetTCPChannel.AddNoErrorEndPoint(endpoint); //init status from last day
                    toSetting.Initialize();
                    fromSetting.Initialize();
                }
                else
                {
                    LogLayer.Info.SetLocation("ServiceDispatcherApplet", "LoadMSNetLoggingLevelConfig").WriteFormat(
                        "Turn on logging for endpoint " + endpointStr);
                    MSNetTCPChannel.RemoveNoErrorEndPoint(endpoint);
                    fromSetting.Initialize();
                    toSetting.Initialize();
                }


                _timerSettings.Add(fromSetting);
                _timerSettings.Add(toSetting);
            }
            catch (Exception e)
            {
                LogLayer.Error.SetLocation("ServiceDispatcherApplet", "LoadMSNetLoggingLevelConfig").WriteFormat(
                    "Cannot setup connection suppress time range " + ((XmlElement) node).InnerXml);
            }

        }

    }

    #region Implementation of IConcordApplet
    public override void OnConnect(MorganStanley.IED.Concord.Application.IConcordApplication application_)
    {
      try
      {
        foreach (ConcordAppletBase applet in application_.ConcordApplets) 
        {
          if (applet.Title == this.Title) return;
        } 

        //The applet will load services from concord.config
        XmlNode serviceNode = null;
        XmlElement logNode = null;

        XmlNamespaceManager xns = new XmlNamespaceManager(new NameTable());
        xns.AddNamespace("services", ServiceDispatcherRegistry.RUNTIME_CONFIG_NAMESPACE);

        try
        {
          if (application_.Configuration != null)
          {
            // Don't bother trying if object is remoted since XmlNamespaceManager is not
            // serializable or MarshalByRef
            if (!System.Runtime.Remoting.RemotingServices.IsTransparentProxy(application_))
            {
              serviceNode = application_.Configuration.GetNode(@"Config/services:serviceDispatcherRegistry", xns);
            }
          }
        }
        catch (Exception ex_)
        {
          if (Log.IsLogLevelEnabled(LogLevel.Warning))
          {
            LogLayer.Warning.SetLocation(ServiceDispatcherApplet.ClassName, "OnConnect").Write("Error reading main application configuration", ex_);
          }
        }

        if (serviceNode == null)
        {
          //Try for a services.config file
          Configurator serviceConfig = (Configurator)ConfigurationManager.GetConfig("services");
          if (serviceConfig != null)
          {
            serviceNode  = serviceConfig.GetNode(@"services:serviceDispatcherRegistry", xns);
            logNode = serviceConfig.GetNode(@"services:suppressLoggings", xns) as XmlElement;
          }
        }


        if (logNode != null)
        {
            LoadMSNetLoggingLevelConfig(logNode);
        }
       

        if (serviceNode != null)
        {
          XmlNodeReader xnr = new XmlNodeReader(serviceNode);
          ServiceManager.LoadServices(xnr);
        }
        else
        {
          LogLayer.Alert.SetLocation("ServiceDispatcherApplet", "OnConnect").WriteFormat("Couldn't find any repository of services to initialize. Please check the configuration");
        }

        ServicesOptionPage serviceOptionPage = application_.WindowManager.CreateThreadSafeOptionPage(typeof(ServicesOptionPage)) as ServicesOptionPage;
        application_.AddOptionPage(this, "Settings", serviceOptionPage);

            
      }
      catch(Exception e_)
      {
        LogLayer.Error.SetLocation("ServiceDispatcherApplet", "OnConnect").WriteFormat(e_.ToString());
      }

    }

    public override void OnDisconnect()
    {
    
    }

    public override string Title
    {
      get { return "Service Dispatcher"; }
    }

    public override string Description
    {
      get { return "Concord Service Dispatcher"; }
    }

    public override Version Version
    {
      get { return new Version(LogLayer.RELEASE); }
    }
    #endregion Implementation of IConcordApplet
	}
}
