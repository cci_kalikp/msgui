#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2008 Morgan Stanley, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/Concord.ServiceDispatcher/XmlServiceDispatcher.cs#5 $
  $DateTime: 2014/02/09 20:49:49 $
    $Change: 865807 $
    $Author: caijin $
_____________________________________________________________________________*/
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Xml;
using System.Xml.Serialization;

using MorganStanley.IED.Concord.Logging;
using MorganStanley.IED.Concord.Logging.RequestTracing;
using MorganStanley.IED.Concord.Runtime;
using MorganStanley.IED.Concord.ServiceDispatcher.Configuration;

using MorganStanley.MSDotNet.MSNet;
using MorganStanley.MSDotNet.MSSoap.Client;
using MorganStanley.MSDotNet.MSSoap.Common;

namespace MorganStanley.IED.Concord.ServiceDispatcher
{
    /// <include file='xmldocs/XmlServiceDispatcher.cs.xml' path='doc/doc[@for="XmlServiceDispatchException"]/*'/>
    public class XmlServiceDispatchException : ApplicationException
    {
        #region Constructors
        /// <include file='xmldocs/XmlServiceDispatcher.cs.xml' path='doc/doc[@for="XmlServiceDispatchException.XmlServiceDispatchException"]/*'/>
        public XmlServiceDispatchException(string message_)
            : base(message_)
        {
        }

        /// <include file='xmldocs/XmlServiceDispatcher.cs.xml' path='doc/doc[@for="XmlServiceDispatchException.XmlServiceDispatchException1"]/*'/>
        public XmlServiceDispatchException(string message_, System.Exception innerException_)
            : base(message_, innerException_)
        {
        }
        #endregion Constructors
    }

    /// <include file='xmldocs/XmlServiceDispatcher.cs.xml' path='doc/doc[@for="XmlServiceDispatcher"]/*'/>
    public abstract class XmlServiceDispatcher
    {
        #region Constants
        private const string NAMESPACE_MSDW_AUTH_PREFIX = "MSDW-AUTH";

        private const string MSDW_SOAP_HEADER_APPLICATION_TAG = "Application";
        private const string MSDW_SOAP_HEADER_SERVICE_TAG = "Service";
        private const string MSDW_SOAP_HEADER_VERSION_TAG = "Version";
        private const string MSDW_SOAP_HEADER_USER_ID_TAG = "UserID";
        private const string MSDW_SOAP_HEADER_WIN32_USER_TAG = "Win32User";
        private const string MSDW_SOAP_HEADER_MACHINE_NAME_TAG = "MachineName";

        private const string MSDW_SOAP_HEADER_RETURN_TYPE_TAG = "ReturnType";
        private const string MSDW_SOAP_HEADER_RETURN_TYPE_VALUE = "SOAP";
        private const string MSDW_SOAP_AUTH_HEADER_AUTHENTICATION_TAG = "Authentication";
        private const string MSDW_SOAP_AUTH_HEADER_USER_TAG = "User";
        private const string MSDW_SOAP_AUTH_HEADER_TOKEN_TAG = "Token";
        #endregion Constants

        #region Declarations
        private readonly IMSNetLoop _loop;              // the running loop
        private readonly IMSNetLoop _invoker;           // the invoking loop
        private string _serviceType;       // the type name of the current service
        private IServiceSetting _serviceSetting;    // current service configuration info
        private SoapHeaderCollection _headers;           // SOAP Headers
        private ISoapOutputTransport _transport;        // the client used to send request and get response
        private GenericMethodDelegate _genericInvoker;    // the delegate used to call all client's invoke method
        private string _application;       // Application SOAP Header
        private string _service = "";      // Service SOAP Header
        private string _serviceID = "";    // ServiceID SOAP Header
        private string _userID;            // UserID SOAP Header
        private string _version = "";      // Version SOAP Header
        private static int _timeoutCount;
        private static bool _useNamespaceInHeader = true;

        private delegate object GenericMethodDelegate(object caller_, string methodName_, object[] params_);

        /// <include file='xmldocs/XmlServiceDispatcher.cs.xml' path='doc/doc[@for="XmlServiceDispatcher.TimeOut"]/*'/>
        public static event EventHandler TimeOut;
        #endregion Declarations

        #region Constructors

        /// <include file='xmldocs/XmlServiceDispatcher.cs.xml' path='doc/doc[@for="XmlServiceDispatcher.XmlServiceDispatcher"]/*'/>
        protected XmlServiceDispatcher(IMSNetLoop runningLoop_, IMSNetLoop invokingLoop_)
            : this(string.Empty, runningLoop_, invokingLoop_)
        {
        }

        protected XmlServiceDispatcher(string aliasName_, IMSNetLoop runningLoop_, IMSNetLoop invokingLoop_)
        {
            #region Logging
            if (Log.IsLogLevelEnabled(LogLevel.Debug))
            {
                LogLayer.Debug.SetLocation("XmlServiceDispatcher", "XmlServiceDispatcher").WriteFormat("XmlServiceDispatcher created, Loop='{0}'", invokingLoop_.GetType().ToString());
            }
            #endregion Logging
            _invoker = invokingLoop_;
            _loop = runningLoop_;
            if (string.IsNullOrEmpty(aliasName_))
            {
                aliasName_ = GetType().ToString();
            }
            this.Initialize(ServiceManager.Service[aliasName_]);
        }

        /// <include file='xmldocs/XmlServiceDispatcher.cs.xml' path='doc/doc[@for="XmlServiceDispatcher.XmlServiceDispatcher1"]/*'/>
        protected XmlServiceDispatcher(IMSNetLoop invokingLoop_)
            : this(MSNetLoopPoolImpl.Instance(), invokingLoop_)
        {
        }

        protected XmlServiceDispatcher(string aliasName_, IMSNetLoop invokingLoop_)
            : this(aliasName_, MSNetLoopPoolImpl.Instance(), invokingLoop_)
        {
        }

        /// <include file='xmldocs/XmlServiceDispatcher.cs.xml' path='doc/doc[@for="XmlServiceDispatcher.XmlServiceDispatcher2"]/*'/>
        protected XmlServiceDispatcher()
            : this(string.Empty)
        {
        }

        protected XmlServiceDispatcher(string aliasName_)
            : this(aliasName_, MSNetLoopPoolImpl.Instance(), MSNetLoopPoolImpl.Instance())
        {
        }

        protected XmlServiceDispatcher(IServiceSetting serviceSetting_, IMSNetLoop runningLoop_, IMSNetLoop invokingLoop_)
        {
            #region Logging
            if (Log.IsLogLevelEnabled(LogLevel.Debug))
            {
                LogLayer.Debug.SetLocation("XmlServiceDispatcher", "XmlServiceDispatcher").WriteFormat("XmlServiceDispatcher created, Loop='{0}'", invokingLoop_.GetType().ToString());
            }
            #endregion Logging
            _invoker = invokingLoop_;
            _loop = runningLoop_;
            this.Initialize(serviceSetting_);
        }

        protected XmlServiceDispatcher(IServiceSetting serviceSetting_, IMSNetLoop invokingLoop_)
            : this(serviceSetting_, MSNetLoopPoolImpl.Instance(), invokingLoop_)
        {
        }

        protected XmlServiceDispatcher(IServiceSetting serviceSetting_)
            : this(serviceSetting_, MSNetLoopPoolImpl.Instance(), MSNetLoopPoolImpl.Instance())
        {
        }


        #endregion Constructors

        #region Public Properties
        /// <include file='xmldocs/XmlServiceDispatcher.cs.xml' path='doc/doc[@for="XmlServiceDispatcher.SoapHeaders"]/*'/>
        public virtual SoapHeaderCollection SoapHeaders
        {
            get { return _headers; }
        }

        /// <include file='xmldocs/XmlServiceDispatcher.cs.xml' path='doc/doc[@for="XmlServiceDispatcher.Application"]/*'/>
        public virtual string Application
        {
            get { return _application; }
            set { _application = value; }
        }

        /// <include file='xmldocs/XmlServiceDispatcher.cs.xml' path='doc/doc[@for="XmlServiceDispatcher.ServiceID"]/*'/>
        public virtual string ServiceID
        {
            get { return _serviceID; }
            set { _serviceID = value; }
        }

        /// <include file='xmldocs/XmlServiceDispatcher.cs.xml' path='doc/doc[@for="XmlServiceDispatcher.Service"]/*'/>
        public virtual string Service
        {
            get { return _service; }
            set { _service = value; }
        }

        /// <include file='xmldocs/XmlServiceDispatcher.cs.xml' path='doc/doc[@for="XmlServiceDispatcher.UserID"]/*'/>
        public virtual string UserID
        {
            get { return _userID; }
            set { _userID = value; }
        }

        /// <include file='xmldocs/XmlServiceDispatcher.cs.xml' path='doc/doc[@for="XmlServiceDispatcher.Version"]/*'/>
        public virtual string Version
        {
            get { return _version; }
            set { _version = value; }
        }

        /// <include file='xmldocs/XmlServiceDispatcher.cs.xml' path='doc/doc[@for="XmlServiceDispatcher.TotalTimeouts"]/*'/>
        public static int TotalTimeouts
        {
            get { return _timeoutCount; }
        }

        /// <include file='xmldocs/XmlServiceDispatcher.cs.xml' path='doc/doc[@for="XmlServiceDispatcher.UseNamespaceInHeader"]/*'/>
        public static bool UseNamespaceInHeader
        {
            get { return _useNamespaceInHeader; }
            set { _useNamespaceInHeader = value; }
        }

        public virtual ISoapOutputTransport Transport
        {
            get { return _transport; }
            set { _transport = value; }
        }
        #endregion Public Properties

        #region Public Methods
        /// <include file='xmldocs/XmlServiceDispatcher.cs.xml' path='doc/doc[@for="XmlServiceDispatcher.LoadServices"]/*'/>
        /// 
        public static void LoadServices(System.IO.Stream inStream_)
        {
            using (RequestTrace trace = new RequestTrace("XmlServiceDispatcher.LoadServices"))
            {
                ServiceManager.LoadServices(inStream_);
            }
        }

        /// <include file='xmldocs/XmlServiceDispatcher.cs.xml' path='doc/doc[@for="XmlServiceDispatcher.LoadServices1"]/*'/>
        public static void LoadServices(System.Uri uri_)
        {
            using (RequestTrace trace = new RequestTrace("XmlServiceDispatcher.LoadServices"))
            {
                ServiceManager.LoadServices(uri_);
            }
        }

        /// <include file='xmldocs/XmlServiceDispatcher.cs.xml' path='doc/doc[@for="XmlServiceDispatcher.LoadServices2"]/*'/>
        public static void LoadServices(System.Xml.XmlReader xmlReader_)
        {
            using (RequestTrace trace = new RequestTrace("XmlServiceDispatcher.LoadServices"))
            {
                ServiceManager.LoadServices(xmlReader_);
            }
        }

        /// <include file='xmldocs/XmlServiceDispatcher.cs.xml' path='doc/doc[@for="XmlServiceDispatcher.LoadServices3"]/*'/>
        public static void LoadServices(System.String fileName_)
        {
            using (RequestTrace trace = new RequestTrace("XmlServiceDispatcher.LoadServices"))
            {
                ServiceManager.LoadServices(fileName_);
            }
        }

        public static IConnectionFactory ConnectionFactory { get; set; }

        /// <include file='xmldocs/XmlServiceDispatcher.cs.xml' path='doc/doc[@for="XmlServiceDispatcher.ServiceSettings"]/*'/>
        public static IEnumerable<KeyValuePair<string, IServiceSetting>> ServiceSettings
        {
            get
            {
                return ServiceManager.Service;
            }
        }

        /// <include file='xmldocs/XmlServiceDispatcher.cs.xml' path='doc/doc[@for="XmlServiceDispatcher.AssignServiceSetting"]/*'/>
        public static void AssignServiceSetting(string aliasName_, IServiceSetting serviceSetting_)
        {
            ServiceManager.Service[aliasName_] = serviceSetting_;
        }

        /// <include file='xmldocs/XmlServiceDispatcher.cs.xml' path='doc/doc[@for="XmlServiceDispatcher.GetServiceSetting"]/*'/>
        public static IServiceSetting GetServiceSetting(string aliasName_)
        {
            return ServiceManager.Service[aliasName_];
        }


        #endregion Public Methods

        #region Protected Methods
        /// <include file='xmldocs/XmlServiceDispatcher.cs.xml' path='doc/doc[@for="XmlServiceDispatcher.CreateHeader"]/*'/>
        protected virtual void CreateSoapHeader(SoapClientMessage message_)
        {
            SoapHeaderDomImpl headers = (SoapHeaderDomImpl)message_.Envelope.CreateHeader();
            headers.XmlElement.SetAttribute("xmlns:" + SoapConstants.NS_PRE_MSDW_HEADER, SoapConstants.NS_URI_MSDW_HEADER);

            headers.ServiceID = ServiceID;
            headers.RequestID = message_.RequestID;
            this.CreateHeaderEntry(headers, MSDW_SOAP_HEADER_SERVICE_TAG, Service);
            this.CreateHeaderEntry(headers, MSDW_SOAP_HEADER_VERSION_TAG, Version);
            this.CreateHeaderEntry(headers, MSDW_SOAP_HEADER_APPLICATION_TAG, Application);
            if (UserID != null) this.CreateHeaderEntry(headers, MSDW_SOAP_HEADER_USER_ID_TAG, UserID);
            this.CreateHeaderEntry(headers, MSDW_SOAP_HEADER_MACHINE_NAME_TAG, Environment.MachineName);
            this.CreateHeaderEntry(headers, MSDW_SOAP_HEADER_WIN32_USER_TAG, Environment.UserName);
            this.CreateHeaderEntry(headers, MSDW_SOAP_HEADER_RETURN_TYPE_TAG, MSDW_SOAP_HEADER_RETURN_TYPE_VALUE);

            foreach (SoapHeader header in _headers)
            {
                SoapHeaderDomImpl.EntryImpl entry = headers.CreateEntry(header.Prefix, header.Name, header.NamespaceURI);
                entry.Value = header.Value;
                headers.AddEntry(entry);
            }

            headers.SetTimestamp();
        }

        protected T Invoke<T>(string action_, params object[] args_)
        {
            return (T)Invoke(action_, args_, typeof(T));
        }

        /// <include file='xmldocs/XmlServiceDispatcher.cs.xml' path='doc/doc[@for="XmlServiceDispatcher.Invoke"]/*'/>
        protected object Invoke(string action_, object[] args_, Type returnType_)
        {
            using (RequestTrace trace = new RequestTrace("XmlServiceDispatcher.Invoke"))
            {
                ActionInfo action = null;
                foreach (ActionInfo ai in this._serviceSetting.Actions)
                {
                    if (action_ == ai.Name)
                    {
                        action = ai;
                    }
                }

                SoapClientMessage message = this.GenerateSoapMessage(action_, args_, returnType_);

                // Send the SOAP request over the wire
                SoapMessage response = null;
                try
                {
                    ISoapSender sender = _serviceSetting.GetServiceSender;
                    if (sender == null)
                    {
                        throw new Exception("SoapSender is not set.");
                    }
                    response = sender.GetResponse(_loop, message);
                }
                catch (SoapTimeoutException ex_)
                {
                    Interlocked.Increment(ref _timeoutCount);

                    if (TimeOut != null)
                    {
                        TimeOut(message, EventArgs.Empty);
                    }

                    #region Logging
                    if (Log.IsLogLevelEnabled(LogLevel.Error))
                    {
                        LogLayer.Error.SetLocation("XmlServiceDispatcher", "Invoke").WriteFormat("A timeout has occurred while invoking the service, Action='{0}', RequestID='{1}'", ex_, action_, message.RequestID);
                    }
                    #endregion Logging

                    // Caller Confuse
                    throw new XmlServiceDispatchException(Res.EXCEPTION_XMLSERVICEDISPATCH_INVOKE_FAILED, ex_);
                }
                catch (SoapFaultException ex_)
                {
                    #region Logging
                    if (Log.IsLogLevelEnabled(LogLevel.Error))
                    {
                        LogLayer.Error.SetLocation("XmlServiceDispatcher", "Invoke").WriteFormat("A SOAP fault has occurred, Action='{0}', RequestID='{1}', FaultCode='{2}', FaultActor='{3}', FaultString='{4}', FaultDetail='{5}', Message='{6}'", ex_, action_, message.RequestID, ex_.Fault.FaultCode, ex_.Fault.FaultActor, ex_.Fault.FaultString, ex_.Fault.DetailString, ex_.Message);
                    }
                    #endregion Logging

                    throw;
                }
                catch (Exception e_)
                {
                    #region Logging
                    if (Log.IsLogLevelEnabled(LogLevel.Error))
                    {
                        LogLayer.Error.SetLocation("XmlServiceDispatcher", "Invoke").WriteFormat("An invocation error has occured, Action='{0}', RequestID='{1}'", e_, action_, message.RequestID);
                    }
                    #endregion Logging

                    // Caller Inform
                    throw new XmlServiceDispatchException(Res.EXCEPTION_XMLSERVICEDISPATCH_INVOKE_FAILED, e_);
                }

                // Replace message with response
                message = new SoapClientMessage(action, (SoapEnvelopeDomImpl)response.Envelope, args_, returnType_, message.Extensions);
                this.BeforeDeserialize(message);
                object retVal = this.Deserialize(message);

                // Pass the return value settings to the SoapClientMessage for the extensions to use
                message.ReturnValue = retVal;

                this.AfterDeserialize(message);

                return retVal;
            }
        }

        public virtual SoapClientMessage GenerateSoapMessage(
          string action_,
          object[] args_,
          Type returnType_
          )
        {
            ActionInfo action = null;
            foreach (ActionInfo ai in this._serviceSetting.Actions)
            {
                if (action_ == ai.Name)
                {
                    action = ai;
                }
            }

            if (action == null)
            {
                #region Logging

                if (Log.IsLogLevelEnabled(LogLevel.Error))
                {
                    LogLayer.Error.SetLocation("XmlServiceDispatcher", "Invoke").WriteFormat("Action '{0}', is invalid.", action_);
                }

                #endregion Logging

                throw new ArgumentException(String.Format(Res.EXCEPTION_INVALID_ACTION, action_), "action_");
            }

            SoapClientMessage message = this.CreateMessage(action, args_, returnType_);

            #region Logging

            if (Log.IsLogLevelEnabled(LogLevel.Info))
            {
                LogLayer.Info.SetLocation("XmlServiceDispatcher", "Invoke")
                        .WriteFormat("Invoke Action='{0}', RequestID={1}", action_, message.RequestID);
            }

            #endregion Logging

            this.CreateSoapHeader(message);
            this.BeforeSerialize(message);
            this.Serialize(message);
            this.AfterSerialize(message);

            return message;
        }

        /// <include file='xmldocs/XmlServiceDispatcher.cs.xml' path='doc/doc[@for="XmlServiceDispatcher.BeginInvoke"]/*'/>
        protected IAsyncResult BeginInvoke(object caller_, string methodName_, object[] params_, AsyncCallback callback_, object state_)
        {
            _genericInvoker = new GenericMethodDelegate(this.GenericMethodRunner);
            XmlServiceAsyncResult result = new XmlServiceAsyncResult(_genericInvoker, _invoker, callback_, state_);
            _genericInvoker.BeginInvoke(caller_, methodName_, params_, new AsyncCallback(this.DoneInvoke), result);
            return result;
        }

        /// <include file='xmldocs/XmlServiceDispatcher.cs.xml' path='doc/doc[@for="XmlServiceDispatcher.EndInvoke"]/*'/>
        protected object EndInvoke(IAsyncResult ar_)
        {
            XmlServiceAsyncResult result = (XmlServiceAsyncResult)ar_;
            result.EndOperation();
            return result.Result;
        }

        /// <include file='xmldocs/XmlServiceDispatcher.cs.xml' path='doc/doc[@for="XmlServiceDispatcher.Serialize"]/*'/>
        // TODO: Use xml maps instead of multiple serializers?
        protected virtual void Serialize(SoapClientMessage message_)
        {
            try
            {
                XmlSerializerNamespaces namespaces = new XmlSerializerNamespaces(new XmlQualifiedName[] { new XmlQualifiedName("", "") });

                using (StringWriter writer = new StringWriter())
                {
                    XmlTextWriter xmlWriter = new XmlTextWriter(writer);
                    xmlWriter.Formatting = Formatting.None;

                    Hashtable serializers = new Hashtable();

                    for (int index = 0; index < message_.Parameters.Length; index++)
                    {
                        IXmlReaderWriter xrw = message_.Parameters[index] as IXmlReaderWriter;
                        if (xrw != null)
                        {
                            xrw.WriteXml(xmlWriter);
                        }
                        else
                        {
                            Type type = message_.Parameters[index].GetType();
                            if (!serializers.Contains(type))
                            {
                                serializers.Add(type, new XmlSerializer(type));
                            }
                            ((XmlSerializer)serializers[message_.Parameters[index].GetType()]).Serialize(xmlWriter, message_.Parameters[index], namespaces);
                        }
                    }

                    xmlWriter.Flush();
                    writer.Flush();

                    message_.SetBody(writer.ToString());
                }
            }
            catch (Exception ex)
            {
                #region Logging
                if (Log.IsLogLevelEnabled(LogLevel.Error))
                {
                    LogLayer.Error.SetLocation("XmlServiceDispatcher", "Serialize").WriteFormat("Serialization failed for {0},{1}'", ex, this.Service, this.ServiceID);
                }
                #endregion Logging
                throw;
            }
        }

        /// <include file='xmldocs/XmlServiceDispatcher.cs.xml' path='doc/doc[@for="XmlServiceDispatcher.Deserialize"]/*'/>
        protected virtual object Deserialize(SoapClientMessage message_)
        {
            object returnValue = null;

            XmlElement response = ((SoapActionDomImpl)message_.Body.Action).XmlElement;

            try
            {

                XmlNodeReader reader = new XmlNodeReader(response);

                returnValue = System.Activator.CreateInstance(message_.GetReturnType());
                IXmlReaderWriter xrw = returnValue as IXmlReaderWriter;

                if (xrw != null)
                {
                    xrw.ReadXml(reader);
                }
                else
                {
                    XmlSerializer deserializer = new XmlSerializer(message_.GetReturnType());
                    returnValue = deserializer.Deserialize(reader);
                }
            }
            catch (Exception ex)
            {
                #region Logging
                if (Log.IsLogLevelEnabled(LogLevel.Error))
                {
                    LogLayer.Error.SetLocation("XmlServiceDispatcher", "Deserialize").WriteFormat("Deserialization failed for {0},{1}'", ex, this.Service, this.ServiceID);
                }
                #endregion Logging

                // Caller Inform
                throw new XmlServiceDispatchException(Res.EXCEPTION_DESERIALIZE_FAILED, ex);
            }

            return returnValue;
        }
        #endregion Protected Methods

        #region Private Classes
        /// <summary>
        /// This is used internally to handle the async operation and callback
        /// </summary>
        private class XmlServiceAsyncResult : MSNetAsyncOperation
        {
            internal GenericMethodDelegate InternalDelegate;
            internal object Result;

            public XmlServiceAsyncResult(GenericMethodDelegate delegate_, IMSNetLoop invoker_, AsyncCallback userCallback_, object state_)
                : base(invoker_, userCallback_, state_)
            {
                InternalDelegate = delegate_;
            }
        }
        #endregion Private Classes

        #region Private Helper Methods
        /// <summary>
        /// Iterates through all the extensions invoking the method corresponding to
        /// SoapClientMessage.Stage
        /// </summary>
        /// <param name="message_">Instance of SoapClientMessage for the current call.</param>
        private void RunExtensions(SoapClientMessage message_)
        {
            if (message_.Extensions == null) return;

            int index = 0;

            try
            {
                switch (message_.Stage)
                {
                    case SoapMessageStage.BeforeSerialize:
                        for (index = 0; index < message_.Extensions.Length; index++)
                        {
                            message_.Extensions[index].BeforeSerialize(message_);
                        }
                        break;

                    case SoapMessageStage.AfterSerialize:
                        for (index = 0; index < message_.Extensions.Length; index++)
                        {
                            message_.Extensions[index].AfterSerialize(message_);
                        }
                        break;

                    case SoapMessageStage.BeforeDeserialize:
                        for (index = 0; index < message_.Extensions.Length; index++)
                        {
                            message_.Extensions[index].BeforeDeserialize(message_);
                        }
                        break;

                    case SoapMessageStage.AfterDeserialize:
                        for (index = 0; index < message_.Extensions.Length; index++)
                        {
                            message_.Extensions[index].AfterDeserialize(message_);
                        }
                        break;
                }
            }
            catch (System.Reflection.TargetInvocationException e_)
            {
                #region Logging
                if (Log.IsLogLevelEnabled(LogLevel.Error))
                {
                    LogLayer.Error.SetLocation("XmlServiceDispatcher", "RunExtensions").WriteFormat(Res.EXCEPTION_EXTENSION_INVOKE_FAILED, message_.Extensions[index].GetType().ToString(), message_.Stage.ToString());
                }
                #endregion Logging

                // Caller Inform
                throw new XmlServiceDispatchException(String.Format(Res.EXCEPTION_EXTENSION_INVOKE_FAILED, message_.Extensions[index].GetType().ToString(), message_.Stage.ToString()), e_.InnerException);
            }
        }

        /// <summary>
        /// Use internally to process the IAsyncResult, so this will hide the
        /// calling into the correct thread from the service writers
        /// </summary>
        /// <param name="res_">the Asyncresult from the BeginInvoke</param>
        private void DoneInvoke(IAsyncResult res_)
        {
            XmlServiceAsyncResult asyncResult = (XmlServiceAsyncResult)res_.AsyncState;
            GenericMethodDelegate internalDelegate = asyncResult.InternalDelegate;

            try
            {
                asyncResult.Result = internalDelegate.EndInvoke(res_);
            }
            catch (TargetInvocationException tie_)
            {
                asyncResult.Exception = tie_.InnerException ?? tie_;
            }
            catch (Exception e_)
            {
                asyncResult.Exception = e_;
            }
            finally
            {
                asyncResult.OperationDoneNotifyUser();
            }
        }

        private void Initialize(IServiceSetting serviceSetting_)
        {
            _headers = new SoapHeaderCollection();
            _serviceType = GetType().ToString();

            _serviceSetting = serviceSetting_;
            _genericInvoker = GenericMethodRunner;

            if (_serviceSetting == null)
            {
                #region Logging
                if (Log.IsLogLevelEnabled(LogLevel.Error))
                {
                    LogLayer.Error.SetLocation("XmlServiceDispatcher", "Initialize").WriteFormat(Res.EXCEPTION_SERVICE_NOT_FOUND, _serviceType);
                }
                #endregion Logging

                throw new ServiceConfigException(String.Format(Res.EXCEPTION_SERVICE_NOT_FOUND, _serviceType));
            }

            if (ConcordRuntime.Current.UserInfo != null)
            {
                _userID = ConcordRuntime.Current.UserInfo.Username;
            }

            _application = _serviceSetting.Application;

            _service = _serviceSetting.Service;
            _serviceID = _serviceSetting.ServiceID;
            _version = _serviceSetting.Version;
            _transport = _serviceSetting.GetTransport(_loop);
        }

        /// <summary>
        /// this will use reflection to call API level methods and this is called internally with
        /// the GenericMethodDelegate asynchronously
        /// </summary>
        /// <param name="caller_"></param>
        /// <param name="methodName_"></param>
        /// <param name="params_"></param>
        /// <returns></returns>
        private object GenericMethodRunner(object caller_, string methodName_, object[] params_)
        {
            return caller_.GetType().InvokeMember(methodName_,
              BindingFlags.InvokeMethod | BindingFlags.Public | BindingFlags.Instance,
              null, caller_, params_);
        }

        /// <summary>
        /// Invokes <see cref="XmlServiceExtension.BeforeSerialize">BeforeSerialize</see> on all extensions.
        /// </summary>
        /// <param name="message_">The SOAP message to process.</param>
        private void BeforeSerialize(SoapClientMessage message_)
        {
            // Run Extensions with stage = BeforeSerialize      
            message_.SetStage(SoapMessageStage.BeforeSerialize);
            this.RunExtensions(message_);
        }

        /// <summary>
        /// Invokes <see cref="XmlServiceExtension.AfterSerialize">AfterSerialize</see> on all extensions.
        /// </summary>
        /// <param name="message_">The SOAP message to process.</param>
        private void AfterSerialize(SoapClientMessage message_)
        {
            // Run Extensions with stage = BeforeSerialize      
            message_.SetStage(SoapMessageStage.AfterSerialize);
            this.RunExtensions(message_);
        }

        /// <summary>
        /// Invokes <see cref="XmlServiceExtension.BeforeDeserialize">BeforeDeserialize</see> on all extensions.
        /// </summary>
        /// <param name="message_">The SOAP message to process.</param>
        private void BeforeDeserialize(SoapClientMessage message_)
        {
            // Run Extensions with stage = BeforeDeserialize
            message_.SetStage(SoapMessageStage.BeforeDeserialize);
            this.RunExtensions(message_);
        }

        /// <summary>
        /// Invokes <see cref="XmlServiceExtension.AfterDeserialize">AfterDeserialize</see> on all extensions.
        /// </summary>
        /// <param name="message_">The SOAP message to process.</param>
        private void AfterDeserialize(SoapClientMessage message_)
        {
            // Run Extensions with stage = AfterDeserialize
            message_.SetStage(SoapMessageStage.AfterDeserialize);
            this.RunExtensions(message_);
        }

        /// <summary>
        /// Creates and initializes an instance of SoapClientMessage.
        /// </summary>
        /// <param name="action_">The name of the XML Web service method.</param>
        /// <param name="args_">An array of objects containing the parameters to pass to the XML Web service.
        /// The order of the values in the array correspond to the order of the parameters in the calling
        /// method of the derived class.</param>
        /// <param name="returnType_">The <see cref="System.Type">Type</see> of the return value.</param>
        /// <returns>A SoapClientMessage with an initialized envelope and empty body.</returns>
        private SoapClientMessage CreateMessage(ActionInfo action_, object[] args_, Type returnType_)
        {
            // Create and initialize the SOAP envelope and header
            SoapEnvelopeDomImpl envelope = new SoapEnvelopeDomImpl();
            SoapBodyDomImpl soapBody = (SoapBodyDomImpl)envelope.CreateBody();

            return new SoapClientMessage(action_, envelope, args_, returnType_, _serviceSetting.GetExtensions(action_));
        }

        /// <summary>
        /// Add an individual SOAP header to the header section of the envelope.
        /// </summary>
        private SoapHeaderDomImpl.EntryImpl CreateHeaderEntry(SoapHeaderDomImpl header_, string entry_, string value_)
        {
            SoapHeaderDomImpl.EntryImpl entry;

            if (_useNamespaceInHeader)
            {
                entry = header_.CreateEntry(SoapConstants.NS_PRE_MSDW_HEADER, entry_, SoapConstants.NS_URI_MSDW_HEADER);
            }
            else
            {
                entry = header_.CreateEntry(entry_);
            }
            entry.Value = value_;
            header_.AddEntry(entry);
            return entry;
        }
        #endregion Private Helper Methods
    }
}