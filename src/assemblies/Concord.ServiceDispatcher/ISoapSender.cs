#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/Concord.ServiceDispatcher/ISoapSender.cs#3 $
  $DateTime: 2013/01/18 15:24:18 $
    $Change: 813214 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion

using MorganStanley.MSDotNet.MSNet;
using MorganStanley.MSDotNet.MSSoap.Common;

namespace MorganStanley.IED.Concord.ServiceDispatcher
{
  /// <summary>
  /// this interface is used to abstract the details of which transports XmlServiceDispacher 
  /// will be using to make web service call
  /// </summary>
  public interface ISoapSender
  {
    /// <summary>
    /// Provide a abstract concept of making an service and call and get response back
    /// </summary>
    /// <param name="loop_"></param>
    /// <param name="message_"></param>
    /// <returns></returns>
    SoapMessage GetResponse(IMSNetLoop loop_, SoapClientMessage message_);
  }
}