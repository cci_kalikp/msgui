﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Forms.Integration;
using System.Windows.Media;
using System.Xml.Linq;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Shell;

namespace MSDesktop.CreditOne
{
    public abstract class SideWindowModule : IModule
    {
        private const string SideWindowFactoryName = "_MSDesktopInternal_SideWindowFactory";
        private static readonly FontFamily HostFontFamily = new FontFamily("Microsoft Sans Serif");

        private readonly IChromeManager _chromeManager;
        private ShellWindow _mainWindow;
        private readonly IChromeRegistry _chromeRegistry;

        protected SideWindowModule(IChromeManager chromeManager, IChromeRegistry chromeRegistry)
        {
            _chromeRegistry = chromeRegistry;
            _chromeManager = chromeManager;
        }

        protected abstract FrameworkElement Control { get; }

        public virtual void Initialize()
        {
            System.Windows.Forms.Application.EnableVisualStyles();
            var chromeManagerBase = _chromeManager as ChromeManagerBase;
            if (chromeManagerBase != null)
            {
                chromeManagerBase.AfterCreateChrome += (sender, args) => OnChromeInitialized();
            }
            else
            {
                throw new InvalidOperationException("This module is supported in host process only.");
            }

            // Register the window factory
            _mainWindow = Application.Current.Windows.Cast<object>().FirstOrDefault(
                x => x as ShellWindow != null) as ShellWindow;

            if (_mainWindow == null)
            {
                _chromeRegistry.RegisterWindowFactory(SideWindowFactoryName, InitHandler);
            }
        }

        private bool InitHandler(IWindowViewContainer emptyViewContainer, XDocument state)
        {
            emptyViewContainer.Content = Control;
            return true;
        }

        private void OnChromeInitialized()
        {
            if (_mainWindow != null)
            {
                string id = Guid.NewGuid().ToString();
                _chromeRegistry.RegisterWidgetFactory(id, (container_, state_) =>
                    {
                        container_.Content = Control;
                        return true;
                    });
                _chromeManager[ChromeArea.NavigationBar].AddWidget(id, new InitialWidgetParameters());
            }
            else
            {
                _chromeManager.CreateWindow(SideWindowFactoryName, new InitialWindowParameters
                    {
                        InitialLocation = InitialLocation.Floating,
                    });

            }
        }

        protected static FrameworkElement GetFrameworkElementForControl(System.Windows.Forms.Control control)
        {
            var wfh = new WindowsFormsHost
                {
                    Margin = new Thickness(0),
                    FontSize = 11.33,
                    FontFamily = HostFontFamily,
                    Height = control.Height,
                    Visibility = Visibility.Visible,
                    Width = control.Width
                };

            wfh.Child = control;
            return wfh;
        }
    }
}
