﻿using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Impl;

namespace MSDesktop.CreditOne
{
    public static class FrameworkExtensions
    {
        public static void AddSingletonModule<T>(this Framework framework)
            where T : IModule
        {
            framework.Container.RegisterType<T>(new ContainerControlledLifetimeManager());
            framework.AddModule<T>();
        }
    }
}
