﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Text;
using System.Web;
using System.Windows;
using System.Windows.Navigation;
using System.Windows.Threading;
using System.Linq; 
using MorganStanley.MSDotNet.My;
using MSDesktop.WebSupport.Properties;
using mshtml;
using Newtonsoft.Json; 
using WebBrowser = System.Windows.Controls.WebBrowser;

namespace MSDesktop.WebSupport.DefaultWebControl
{
    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    [ComVisible(true)]
    public class DefaultWebControl : IWebControl
    {
        private static readonly IMSLogger Logger = MSLoggerFactory.CreateLogger<DefaultWebControl>();

        public IJavascriptCallsHandler JavascriptCallHandler { get; set; } 
        public DefaultWebControl()
        { 
            m_webBrowser = new WebBrowser();
            m_webBrowser.Loaded += WebBrowserLoaded;
            m_webBrowser.LoadCompleted += WebBrowserLoadCompleted;
            m_webBrowser.ObjectForScripting = this;
        }

        private readonly HTMLModule module;
        public DefaultWebControl(HTMLModule module_):this()
        { 
            this.module = module_;
        }
        #region IWebControl implementation

        private string decodedUrl = null;
        public void LoadFile(Uri uri_)
        { 
            if (uri_.OriginalString.StartsWith(URLModule.URLBasedModuleFakeAddress))
            {
                decodedUrl = HttpUtility.UrlDecode(uri_.Query.Substring(1));
                HTMLModuleInfo info = module != null && WebSupportExtensions.Registry.ContainsKey(module)? WebSupportExtensions.Registry[module] :
                    WebSupportExtensions.Registry.Values.FirstOrDefault(m_ => m_.Url.AbsoluteUri == decodedUrl);
                if (info == null)
                {
                    return;
                }

                URLModule urlModule = module as URLModule;
                string proxyHtml = urlModule == null ? File.ReadAllText("URLProxy.html") : urlModule.GetURLProxyHtml();
                proxyHtml = proxyHtml.Replace("$DESTINATION_URL", String.Format("\"{0}\"", decodedUrl))
                                     .Replace("$LOCATION", String.Format("\"{0}\"", info.Location))
                                     .Replace("$TITLE", String.Format("\"{0}\"", info.Title))
                                     .Replace("$IMAGEPATH", String.Format("\"{0}\"", (info.Image ?? string.Empty).Replace(@"\", @"\\")));
                Logger.DebugWithFormat("URLProxy generated for {0} with {1} and {2}", decodedUrl, info.Location,
                                       info.Title);
                m_webBrowser.NavigateToString(proxyHtml);
                return;
            }

            if (uri_.OriginalString.StartsWith(URLModule.URLBasedActivatedModuleFakeAddress))
            {
                decodedUrl = HttpUtility.UrlDecode(uri_.Query.Substring(1));
                HTMLModuleInfo info = module != null && WebSupportExtensions.Registry.ContainsKey(module) ? WebSupportExtensions.Registry[module] :
                    WebSupportExtensions.Registry.Values.FirstOrDefault(m_ => m_.Url.AbsoluteUri == decodedUrl);
                if (info == null)
                {
                    return;
                }
                ActivatedURLModule urlModule = module as ActivatedURLModule;
                string proxyHtml = urlModule == null ? File.ReadAllText("URLActivatedProxy.html") : urlModule.GetURLProxyHtml();
                proxyHtml = proxyHtml.Replace("$DESTINATION_URL", String.Format("\"{0}\"", decodedUrl))
                                     .Replace("$LOCATION", String.Format("\"{0}\"", info.Location))
                                     .Replace("$TITLE", String.Format("\"{0}\"", info.Title))
                                     .Replace("$IMAGEPATH", String.Format("\"{0}\"", (info.Image ?? string.Empty).Replace(@"\", @"\\")));
                Logger.DebugWithFormat("URLActivatedProxy generated for {0} with {1} and {2}", decodedUrl, info.Location,
                                       info.Title);
                m_webBrowser.NavigateToString(proxyHtml);
                return;
            }
            if (uri_.IsFile && !uri_.IsUnc)
            {
                if (!File.Exists(uri_.LocalPath))
                {
                    throw new Exception("Cannot access Web Page:" + uri_.LocalPath);
                }
                m_webBrowser.NavigateToStream(new FileStream(uri_.LocalPath, FileMode.Open ));
            }
            else
            {
                m_webBrowser.Navigate(uri_);
            }
        }

        public void CallJavascriptFunction(string functionName_, bool sync, params object[] args_)
        {
            try
            {
                m_webBrowser.InvokeScript(functionName_, args_);
            }
            catch (COMException)
            {
                Logger.Error(string.Format("There is no such JS function: {0}", functionName_));
            }
        }

        public object CallJavascriptFunctionWithResult(string functionName_, params object[] args_)
        {
            try
            {
                return m_webBrowser.InvokeScript(functionName_, args_);
            }
            catch (COMException)
            {
                Logger.Error(string.Format("There is no such JS function: {0}", functionName_));
                return null;
            }
        }

        public Dispatcher Dispatcher
        {
            get { return m_webBrowser.Dispatcher; }
        }

        public object Control
        {
            get { return m_webBrowser; }
        }

        #endregion

        public void CallFromJavascript(string callback_, string module_, string func_, string argsInJSON_)
        {
            if (module_ == null) throw new ArgumentNullException("module_");
            if (func_ == null) throw new ArgumentNullException("func_");
            if (argsInJSON_ == null) throw new ArgumentNullException("argsInJSON_");
            
            var args = JsonConvert.DeserializeObject<object[]>(argsInJSON_);

            typeof (IJavascriptCallsHandler).GetMethod(func_).Invoke(JavascriptCallHandler,
                BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy,
                null, args, null);
        }

        #region Private parts

        private readonly WebBrowser m_webBrowser;

        private void WebBrowserLoaded(object sender_, RoutedEventArgs e_)
        {
            EventHandler<EventArgs> copy = Loaded;
            if (copy != null)
            {
                copy(this, e_);
            }
        }

        private void WebBrowserLoadCompleted(object sender_, NavigationEventArgs e_)
        {
            //load the content in the iframe for url module would also trigger the LoadCompleted event, we need to skip this
            if (e_.Uri != null && !string.IsNullOrEmpty(decodedUrl) && e_.Uri.ToString() == decodedUrl)
            {
                return;
            }
            var scriptsToLoad = new[]
                                    {
                                        "Console",
                                        "Utils",
                                        "IModule",
                                        "IChromeRegistry",
                                        "IChromeManager",
                                        "IRoutingCommunicator",
                                        "ICommunicator",
                                        "IEventRegistrator"
                                    };

            var htmlDocument = (m_webBrowser.Document as HTMLDocumentClass);
            var head = htmlDocument.getElementsByTagName("head").item() as HTMLHeadElement; 
            foreach (var scriptName in scriptsToLoad)
            {
                ResourceManager resourceMgr = Resources.ResourceManager;
                string contents = resourceMgr.GetString(scriptName);

                var id = scriptName + "ScriptInjection";
                var scriptEl = htmlDocument.createElement("script");
                scriptEl.setAttribute("type", "text/javascript", 0);
                scriptEl.setAttribute("id", id, 0);
                (scriptEl as IHTMLScriptElement).text = contents;
                head.appendChild(scriptEl as IHTMLDOMNode);
            }  
            
            EventHandler<EventArgs> copy = LoadCompleted;
            if (copy != null)
            {
                copy(this, e_);
            }
        }

        #endregion

        #region IWebControl Members

        public event EventHandler<EventArgs> LoadCompleted;

        public event EventHandler<EventArgs> Loaded;

        #endregion

        public void Dispose()
        {
            m_webBrowser.Dispatcher.Invoke(new Action(m_webBrowser.Dispose));
        }
    }
}