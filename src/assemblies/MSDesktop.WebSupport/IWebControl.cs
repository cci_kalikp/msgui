﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Threading;
using Newtonsoft.Json.Linq;

namespace MSDesktop.WebSupport
{
    public interface IWebControl : IDisposable
    {
        /// <summary>
        /// Occurs when a page has completely finished loading.
        /// </summary>
        event EventHandler<EventArgs> LoadCompleted;
        
        void LoadFile(Uri htmlFileName_);

        void CallJavascriptFunction(string functionName_, bool sync, params object[] args_);

        object CallJavascriptFunctionWithResult(string functionName_, params object[] args_);

        Dispatcher Dispatcher { get; }

        object Control { get; }

        IJavascriptCallsHandler JavascriptCallHandler { get; set; }
    }

    public interface IJavascriptCallsHandler
    {
        void RegisterWindowFactory(string id, string loadState, string saveFunc, string title);

        void PlaceWidget(string id, string location, JContainer initialParamsJContainer, string typeName);

        void OnModuleInitialized();

        void RegisterWidgetFactory(string id);
        
        #region Legacy M2M
        void Subscribe(string messageTypeName, string onMessageCallback);
        void Publish(string publisherId, object message);
        bool GetPublisher(string messageTypeName, string publisherId);
        #endregion

        void GetModulePublisher(string messageTypeName, string publisherId);
        void GetModuleSubscriber(string messageTypeName, string subscriberId);
        void ModulePublisherPublish(string id, string type, JContainer message);
        void RegisterV2VPublisher(string messageType, string viewId, string publisherId);
        void PublishV2V(JContainer message, string publisherId, string messageType);
        void RegisterV2VSubscriber(string messageType, string viewId, string subscriberId);
        void GetRoutedPublisher(string type, string publisherId);
        void RoutedPublisherPublish(string publisherId, string type, JContainer content);
        void GetRoutedSubscriber(string typeName, string subscriberId);
    }
}
