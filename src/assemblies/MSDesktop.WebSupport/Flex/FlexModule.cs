﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows;
using Flash.External;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.FlexModule;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence.DelayedLoading;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;
using Newtonsoft.Json;

namespace MSDesktop.WebSupport.Flex
{
    public abstract class FlexModule : DelayedProfileLoadCoordinatingModule
    {
        protected abstract Uri Url { get; }

        private readonly IChromeManager _chromeManager;
        private readonly IChromeRegistry _chromeRegistry;
        private readonly IApplication _application;
        private readonly IEventRegistrator _eventRegistrator;
        private readonly Dictionary<FlexModuleFlashPlayerView, FlashPlayerInstanceInfo> _instanceInfos;
        private readonly Dictionary<string, object> _publishers = new Dictionary<string, object>();
        private readonly ICommunicator _communicator;
        private readonly IModuleGroup _moduleGroup;

        protected FlexModule(
            IUnityContainer container,
            IChromeManager chromeManager,
            IChromeRegistry chromeRegistry,
            IApplication application,
            IEventRegistrator eventRegistrator)
            : base(container)

        {
            _chromeManager = chromeManager;
            _chromeRegistry = chromeRegistry;
            _application = application;
            _eventRegistrator = eventRegistrator;
            _communicator = container.Resolve<ICommunicator>();
            _moduleGroup = container.Resolve<IModuleGroup>();
            _instanceInfos = new Dictionary<FlexModuleFlashPlayerView, FlashPlayerInstanceInfo>();
        }

        public override void Initialize()
        {
            base.Initialize();
            _application.ApplicationLoaded += OnApplicationLoaded;
        }

        private void OnApplicationLoaded(object sender, EventArgs args)
        {
             var flashPlayer = new FlexModuleFlashPlayerView();
            var instanceInfo = new FlashPlayerInstanceInfo();
            _instanceInfos.Add(flashPlayer, instanceInfo);
            flashPlayer.Loaded += FlashPlayerLoaded;
            flashPlayer.Source = Url.ToString();
            AddContentType(flashPlayer.GetType());
            EventHandler swfReadyHandler = null;
            var initializerWindow = new Window
                {
                    Width = flashPlayer.Width + 30,
                    Height = flashPlayer.Height + 50,
                    Top = -5000,
                    Left = -5000,
                    Content = flashPlayer,
                    ShowInTaskbar = false
                };
            swfReadyHandler = (o, eventArgs) =>
                {
                    instanceInfo.FlashProxy.Call("MSGUI_initialize");
                    instanceInfo.SwfReadyBecameTrue -= swfReadyHandler;
                    var flashProxy = _instanceInfos[flashPlayer].FlashProxy;
                    flashProxy.ExternalInterfaceCall -= ExternalInterfaceCall;
                    flashProxy.Dispose();
                    _instanceInfos.Remove(flashPlayer);
                    flashPlayer.Dispose();
                    initializerWindow.Close();
                    OnModuleInitialized();
                };

            instanceInfo.SwfReadyBecameTrue += swfReadyHandler;
            initializerWindow.Show();
        }


        private void FlashPlayerLoaded(object sender, RoutedEventArgs e)
        {
            var flashPlayer = sender as FlexModuleFlashPlayerView;

            // Create the proxy and register this app to receive notification when the proxy receives
            // a call from ActionScript
            var flashProxy = new ExternalInterfaceProxy(flashPlayer.m_flashPlayer);
            flashProxy.ExternalInterfaceCall += ExternalInterfaceCall;

            _instanceInfos[flashPlayer].AppReady = true;
            _instanceInfos[flashPlayer].FlashProxy = flashProxy;

            flashPlayer.Loaded -= FlashPlayerLoaded;
        }

        private void FlexViewContainerClosed(object sender, WindowEventArgs e)
        {
            var flashPlayer = e.ViewContainer.Content as FlexModuleFlashPlayerView;
            e.ViewContainer.Closed -= FlexViewContainerClosed;
            e.ViewContainer.ClosedQuietly -= FlexViewContainerClosed;
            if (flashPlayer == null) return;
            var flashProxy = _instanceInfos[flashPlayer].FlashProxy;
            flashProxy.ExternalInterfaceCall -= ExternalInterfaceCall;
            flashProxy.Dispose();
            _instanceInfos.Remove(flashPlayer);
            flashPlayer.Dispose();
        }

        protected override sealed void CloseInitializer()
        {
        }

        private object ExternalInterfaceCall(object sender, ExternalInterfaceCallEventArgs e)
        {
            string functionName = e.FunctionCall.FunctionName;
            var player = _instanceInfos.Keys.FirstOrDefault(p => ReferenceEquals(_instanceInfos[p].FlashProxy, sender));
            switch (functionName)
            {
                case "MSGUI_isReady":
                    return IsReady(player);
                case "MSGUI_setSWFIsReady":
                    SetSwfIsReady(player);
                    return null;
                case "MSGUI_useDirectCalls":
                    return UseDirectCalls();
                case "MSGUI_placeWidget":
                    PlaceWidget(e.FunctionCall.Arguments);
                    return null;
                case "MSGUI_registerWindowFactory":
                    return RegisterWindowFactory((string) e.FunctionCall.Arguments[0],
                        (string)e.FunctionCall.Arguments[1], 
                        e.FunctionCall.Arguments.Length > 2 ? (string)e.FunctionCall.Arguments[2] : (string)e.FunctionCall.Arguments[1]);
                case "MSGUI_registerV2VSubscriber":
                    RegisterV2VSubscriber((string) e.FunctionCall.Arguments[0], (string) e.FunctionCall.Arguments[1]);
                    return null;
                case "MSGUI_registerV2VPublisher":
                    RegisterV2VPublisher((string) e.FunctionCall.Arguments[0], (string) e.FunctionCall.Arguments[1]);
                    return null;
                case "MSGUI_publishV2V":
                    PublishV2V((string) e.FunctionCall.Arguments[0], (string) e.FunctionCall.Arguments[1],
                               (string) e.FunctionCall.Arguments[2]);
                    return null;
                case "MSGUI_registerSubscriber":
                    RegisterSubscriber((string) e.FunctionCall.Arguments[0]);
                    return null;
                case "MSGUI_registerPublisher":
                    RegisterPublisher((string) e.FunctionCall.Arguments[0]);
                    return null;
                case "MSGUI_publish":
                    Publish((string) e.FunctionCall.Arguments[0], (string) e.FunctionCall.Arguments[1]);
                    return null;
                default:
                    return null; //CallFromFlex(functionName, e.FunctionCall.Arguments);
            }
        }


        #region Methods called by Flash Player

        /// <summary>
        /// Called to check if the page has initialized and JavaScript is available
        /// </summary>
        /// <returns></returns>
        private bool IsReady(FlexModuleFlashPlayerView player)
        {
            FlashPlayerInstanceInfo info;
            return player != null && _instanceInfos.TryGetValue(player, out info) && info.AppReady;
        }

        /// <summary>
        /// Called to notify the page that the SWF has set it's callbacks
        /// </summary>
        private void SetSwfIsReady(FlexModuleFlashPlayerView player)
        {
            // record that the SWF has registered it's functions (i.e. that C#
            // can safely call the ActionScript functions)
            _instanceInfos[player].SwfReady = true;
        }

        /// <summary>
        /// Tells Flex that it's hosted in an ActiveX container
        /// </summary>
        /// <returns><code>true</code></returns>
        private static object UseDirectCalls()
        {
            return true;
        }

        private void PlaceWidget(object[] arguments)
        {
            var parametersType = Type.GetType((string) arguments[2]);
            var parameters = DecodeMessage(parametersType, (string) arguments[3]);
            _chromeManager.PlaceWidget(Guid.NewGuid().ToString(),
                                       (string) arguments[0],
                                       (string) arguments[1],
                                       (InitialWidgetParameters) parameters);
        }

        private bool RegisterWindowFactory(string factoryId, string callback, string title)
        {
            _chromeRegistry
                .RegisterWindowFactory(factoryId,
                                       (container, state) =>
                                           {
                                               var flashPlayer = new FlexModuleFlashPlayerView();
                                               var instanceInfo = new FlashPlayerInstanceInfo();
                                               _instanceInfos.Add(flashPlayer, instanceInfo);

                                               EventHandler swfReadyHandler = null;
                                               swfReadyHandler = (o, eventArgs) =>
                                                   {
                                                       instanceInfo.FlashProxy.Call("MSGUI_initializeCommunication");
                                                       instanceInfo.FlashProxy.Call("MSGUI_createWindow", callback,
                                                                                    container.ID);
                                                       instanceInfo.SwfReadyBecameTrue -= swfReadyHandler;
                                                       ViewLoadedCompletely();
                                                   };

                                               instanceInfo.SwfReadyBecameTrue += swfReadyHandler;
                                               flashPlayer.Loaded += FlashPlayerLoaded;
                                               flashPlayer.Source = Url.ToString();

                                               container.Title = title ?? callback;
                                               container.Content = flashPlayer;
                                               container.Closed += FlexViewContainerClosed;
                                               container.ClosedQuietly += FlexViewContainerClosed;

                                               return true;
                                           });
            return true;
        }


        #region V2V Communication

        private void RegisterV2VSubscriber(string viewId, string type)
        {
            var vc = _chromeManager.Windows.First(w => w.ID == viewId);
            var messageType = Type.GetType(type);

            var handler = typeof (FlexModule)
                .GetMethod("GetViewSubscriptionHandler", BindingFlags.Instance | BindingFlags.NonPublic)
                .MakeGenericMethod(messageType)
                .Invoke(this, new object[] {viewId, vc});

            SubscriberHelper.GetSubscriber(_eventRegistrator, messageType, vc, handler);
        }

        private void RegisterV2VPublisher(string viewId, string type)
        {
            var vc = _chromeManager.Windows.First(w => w.ID == viewId);
            var messageType = Type.GetType(type);
            typeof (IEventRegistrator).GetMethod("RegisterPublisher")
                                      .MakeGenericMethod(messageType)
                                      .Invoke(_eventRegistrator, new object[] {vc});
        }

        private Action<T> GetViewSubscriptionHandler<T>(string viewId, IWindowViewContainer viewContainer)
        {
            return message =>
                {
                    var control = (FlexModuleFlashPlayerView) viewContainer.Content;
                    var proxy = _instanceInfos[control].FlashProxy;
                    var serializedMessage = JsonConvert.SerializeObject(message);
                    control.Dispatcher.Invoke(
                        new Action(
                            () =>
                            proxy.Call("MSGUI_handleV2VSubscribedMessage", viewId, serializedMessage)));
                };
        }

        private void PublishV2V(string viewId, string encodedMessage, string messageType)
        {
            var vc = _chromeManager.Windows.First(w => w.ID == viewId);
            var type = Type.GetType(messageType);
            var publisher = typeof (IEventRegistrator)
                .GetMethod("GetRegisteredPublisher")
                .MakeGenericMethod(type)
                .Invoke(_eventRegistrator, new object[] {vc});

            object typedMessage = DecodeMessage(type, encodedMessage);

            typeof (IPublisher<>).MakeGenericType(type).GetMethod("Publish").Invoke(
                publisher,
                new[] {typedMessage, CommunicationTargetFilter.All, new string[] {}});
        }

        #endregion

        #region M2M Communication

        private void RegisterPublisher(string messageTypeName)
        {
            var messageType = Type.GetType(messageTypeName);
            _publishers[messageTypeName] = MessagingReflectionUtilities.GetPublisher(_communicator, messageType);
        }

        private void RegisterSubscriber(string messageTypeName)
        {
            var messageType = Type.GetType(messageTypeName);
            var handlerFactory = typeof (FlexModule)
                .GetMethod("GetSubscriptionHandler", BindingFlags.Instance | BindingFlags.NonPublic)
                .MakeGenericMethod(messageType);
            var handler = handlerFactory.Invoke(this, BindingFlags.Instance | BindingFlags.NonPublic,
                                                null, new object[] {messageTypeName}, null);

            MessagingReflectionUtilities.GetSubscriber(_communicator, messageType, handler);
            // _moduleGroup.SubscribeM2M(messageTypeName);

        }

        // DON'T DELETE: Used via reflection
        private Action<T> GetSubscriptionHandler<T>(string messageTypeName)
        {
            return message =>
                {
                    foreach (var flashPlayerInstanceInfo in _instanceInfos)
                    {
                        var proxy = flashPlayerInstanceInfo.Value.FlashProxy;
                        var serializedMessage = JsonConvert.SerializeObject(message);
                        flashPlayerInstanceInfo.Key.Dispatcher.Invoke(
                            new Action(
                                () =>
                                proxy.Call("MSGUI_handleSubscribedMessage", messageTypeName, serializedMessage)));
                    }

                };
        }

        private void Publish(string encodedMessage, string messageType)
        {
            object publisher;
            if (_publishers.TryGetValue(messageType, out publisher))
            {
                object message = DecodeMessage(Type.GetType(messageType), encodedMessage);
                MessagingReflectionUtilities.Publish(publisher, message);
                // _moduleGroup.PublishM2M(message);
            }
        }

        #endregion

        private static object DecodeMessage(Type messageType, string encodedMessage)
        {
            return typeof (JsonConvert)
                .GetMethods()
                .First(m => m.Name == "DeserializeObject" && m.GetParameters().Count() == 1 && m.IsGenericMethod)
                .MakeGenericMethod(new[] {messageType})
                .Invoke(null, new object[] {encodedMessage});

        }

        #endregion

        private class FlashPlayerInstanceInfo
        {
            private bool _swfReady;
            public bool AppReady { get; set; }

            public bool SwfReady
            {
                get { return _swfReady; }
                set
                {
                    _swfReady = value;
                    OnSwfReadyBecameTrue();
                }
            }

            public ExternalInterfaceProxy FlashProxy { get; set; }

            public event EventHandler SwfReadyBecameTrue;

            private void OnSwfReadyBecameTrue()
            {
                var handler = SwfReadyBecameTrue;
                if (handler != null) handler(this, EventArgs.Empty);
            }
        }
    }
}
