﻿var MSDesktopUtils = {};

MSDesktopUtils.isIE = false;

if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)) {
    var ieversion = new Number(RegExp.$1);
    MSDesktopUtils.isIE = true;
}
MSDesktopUtils.GetFunctionName = function (func) {
    var functionBody = func.toString();
    var functionName = functionBody.match(/function (\w*)/)[1];
    //alert(functionName);
    if (functionName == null || functionName.length == 0) {
        functionName = functionBody;
    };
    return functionName;
};

MSDesktopUtils.GetObjectForScripting = function () {
    if (MSDesktopUtils.isIE) {
        return window.external;
    } else {
        return NativeCallDispatcher;
    }
}

MSDesktopUtils.CallNative = function (module, func, args, callback) {
    if (MSDesktopUtils.isIE) {
        window.external.CallFromJavascript(callback, module, func, JSON.stringify(args));
    }
    else {
//        var argsJS = [];
//        for (var i = 0; i < args.length; i++) {
//            argsJS.push(JSON.stringify(args[i]));
//        }
        NativeCallDispatcher.exec(callback, module, func, JSON.stringify(args));
    }
}

MSDesktopUtils.OnCommandReturn = function (status, callback, result) {
    if (status == 1) {
        var callbackFn = window[callback];
        callbackFn(result);
    }
    else if (status > 1) {
        alert("Error occured when invoking native function; error code: " + status);
    }
}

function MSDesktopUtilsOnCommandReturn(status, callback, result) {
    MSDesktopUtils.OnCommandReturn(status, callback, result);
}