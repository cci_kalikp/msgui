//IChromeManager.js

var IChromeManager = {};

String.prototype.quote = function (s) { return "'" + s + "'"; }

IChromeManager.PlaceWidget = function () {
    if (arguments.length != 4) {
        alert("IChromeManager.PlaceWidget: invalid number of arguments");
        return;
    }
    if (typeof (arguments[0]) == "string" && typeof (arguments[1]) == "string" && typeof (arguments[2]) == "object" && typeof (arguments[3]) == "string") {
        var widgetID = arguments[0];
        var where = arguments[1];
        var initialParams = arguments[2];
        var initialParamsType = arguments[3];
        MSDesktopUtils.CallNative(IModule.ModuleID, "PlaceWidget", [widgetID, where, initialParams, initialParamsType]);
        //MSDesktopUtils.GetObjectForScripting().PlaceWidget(widgetID, where, initialParams, initialParamsType);
    }
    else {
        alert("IChromeManager.PlaceWidget: invalid types of arguments");
        return;
    }

};
