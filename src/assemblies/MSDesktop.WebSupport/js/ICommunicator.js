﻿var ICommunicator = {};

ICommunicator.subscribers = {};

// legacy M2M

ICommunicator.Subscribe = function () {
    if (arguments.length != 2) {
        return;
    }

    if (typeof (arguments[0]) == "string" && typeof (arguments[1]) == "function") {
        var messageType = arguments[0];
        var onMessage = MSDesktopUtils.GetFunctionName(arguments[1]);
        MSDesktopUtils.CallNative(IModule.ModuleID, "Subscribe", [messageType, onMessage]);
    }
};

ICommunicator.GetPublisher = function() {
    if (arguments.length < 1 || arguments.length > 3) {
        return false;
    }
    var messageType = arguments[0];

    if (arguments.length == 1) {
        var publisherId = guid();
        MSDesktopUtils.CallNative(IModule.ModuleID, "GetModulePublisher", [messageType, publisherId]);
        return new ModulePublisher(publisherId, messageType);
    }

    if (typeof(arguments[0]) == "string" && typeof(arguments[1]) == "string") {
        var publisherID = arguments[1];
        if (arguments.length == 3) {
            var publisherReady = MSDesktopUtils.GetFunctionName(arguments[2]);
            MSDesktopUtils.CallNative(IModule.ModuleID, "GetPublisher", [messageType, publisherID], publisherReady);
        } else {
            MSDesktopUtils.CallNative(IModule.ModuleID, "GetPublisher", [messageType, publisherID]);
        }
    }
};

ICommunicator.Publish = function() {
    if (arguments.length != 2) {
        return;
    }

    if (typeof(arguments[0]) == "string") {
        var publisherID = arguments[0];
        var message = arguments[1];
        MSDesktopUtils.CallNative(IModule.ModuleID, "Publish", [publisherID, message]);
    }
};

// end of legacy M2M

ICommunicator.GetSubscriber = function (type) {
    var subscriberId = guid();
    MSDesktopUtils.CallNative(IModule.ModuleID, "GetModuleSubscriber", [type, subscriberId]);
    var subscriber = new ModuleSubscriber(subscriberId, type);
    this.subscribers[subscriberId] = subscriber;
    return subscriber;
};

ICommunicator.ReceiveMessage = function (id, message) {
    if (id in this.subscribers) {
        this.subscribers[id].ReceiveMessage(JSON.parse(message));
    }
};

function ModulePublisher(id, type) {
    this.id = id;
    this.type = type;
};

ModulePublisher.prototype.Publish = function (message) {
    MSDesktopUtils.CallNative(IModule.ModuleID, "ModulePublisherPublish", [this.id, this.type, message]);
};

function ModuleSubscriber(id, type) {
    this.id = id;
    this.type = type;
    this.subscribeCallback = null;
};

ModuleSubscriber.prototype.Subscribe = function (callback) {
    this.subscribeCallback = callback;
};

ModuleSubscriber.prototype.ReceiveMessage = function (message) {
    if (this.subscribeCallback != null) {
        this.subscribeCallback(message);
    }
};

function receiveCommunicatorMessage(id, message) {
    ICommunicator.ReceiveMessage(id, message);
};