var IPersistenceService = {};

/**
 * we can add store/restore functions for saving/restoring the states 
 * @author hur
 * @params: persistorID, restoreStateHandler, saveStateHandler
 */
IPersistenceService.AddPersistor = function (persistorID, restoreStateHandler, saveStateHandler) {
    if (persistorID == null || persistorID == "") {
        return;
    }

    var restoreHandlerName = "";
    var saveHandlerName = "";

    if (typeof restoreStateHandler == "function") {
        restoreHandlerName = MSDesktopUtils.GetFunctionName(restoreStateHandler);
    }
    if (typeof saveStateHandler == "function") {
        saveHandlerName = MSDesktopUtils.GetFunctionName(saveStateHandler);
    }

    MSDesktopUtils.CallNative(IModule.ModuleID, "AddHTMLPersistor", [persistorID, restoreHandlerName, saveHandlerName, IModule.ModuleID]);

};