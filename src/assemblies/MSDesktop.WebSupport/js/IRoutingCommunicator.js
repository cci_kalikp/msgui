﻿var IRoutingCommunicator = {};

IRoutingCommunicator.subscribers = {};

IRoutingCommunicator.GetPublisher = function (type) {
    var publisherId = guid();
    MSDesktopUtils.CallNative(IModule.ModuleID, "GetRoutedPublisher", [type, publisherId]);
    return new RoutedPublisher(publisherId, type);
};

IRoutingCommunicator.GetSubscriber = function (type) {
    var subscriberId = guid();
    MSDesktopUtils.CallNative(IModule.ModuleID, "GetRoutedSubscriber", [type, subscriberId]);
    var subscriber = new RoutedSubscriber(subscriberId, type);
    this.subscribers[subscriberId] = subscriber;
    return subscriber;
};

IRoutingCommunicator.ReceiveMessage = function (id, message) {
    if (id in this.subscribers) {
        this.subscribers[id].ReceiveMessage(JSON.parse(message));
    };
};

function RoutedPublisher(id, type) {
    this.id = id;
    this.type = type;
};

RoutedPublisher.prototype.Publish = function (message) {
    MSDesktopUtils.CallNative(IModule.ModuleID, "RoutedPublisherPublish", [this.id, this.type, message]);
};

function RoutedSubscriber(id, type) {
    this.id = id;
    this.type = type;
    this.subscribeCallback = null;
};

RoutedSubscriber.prototype.Subscribe = function (callback) {
    this.subscribeCallback = callback;
};

RoutedSubscriber.prototype.ReceiveMessage = function (message) {
    if (this.subscribeCallback != null) {
        this.subscribeCallback(message);
    }
};

function receiveRoutingCommunicatorMessage(id, message) {
    IRoutingCommunicator.ReceiveMessage(id, message);
};

function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
               .toString(16)
               .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
         s4() + '-' + s4() + s4() + s4();
};
