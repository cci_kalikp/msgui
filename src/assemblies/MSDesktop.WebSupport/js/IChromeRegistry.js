//IChromeRegistry.js
var IChromeRegistry = {};
/**
 * @author hur
 * @description: register window factories
 */

String.prototype.quote = function (s) { return "'" + s + "'"; }

IChromeRegistry.RegisterWindowFactory = function () {
    if (arguments.length < 2) {
        return;
    }
    var viewID = arguments[0];
    var initFunc;
    var saveFunc;
    if (typeof (arguments[1]) == "function") {
        initFunc = MSDesktopUtils.GetFunctionName(arguments[1]);
    }
    if (arguments[2] != null && typeof (arguments[2]) == "function") {
        saveFunc = MSDesktopUtils.GetFunctionName(arguments[2]);
    }

    var windowTitle = viewID;
    if (arguments.length > 3) {
        windowTitle = arguments[3];
    }
    if (arguments[2] == null) {
        MSDesktopUtils.CallNative(IModule.ModuleID, "RegisterWindowFactory", [viewID, initFunc, null, windowTitle]);
    }
    else {
        MSDesktopUtils.CallNative(IModule.ModuleID, "RegisterWindowFactory", [viewID, initFunc, saveFunc, windowTitle]);
    }

};

IChromeRegistry.RegisterWidgetFactory = function (id) {
    MSDesktopUtils.CallNative(IModule.ModuleID, "RegisterWidgetFactory", [id]);
};

IChromeRegistry.ViewID = null;

function LoadViewStateRouter(loadStateHandler, viewID, state) {
    //alert('lvsr'+loadStateHandler+viewID+state+window[loadStateHandler]);
    IChromeRegistry.ViewID = viewID;
    if (state == null) {
        window[loadStateHandler]();
    } else {
        window[loadStateHandler](state);
    }
}