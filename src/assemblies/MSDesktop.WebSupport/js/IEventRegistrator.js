﻿var IEventRegistrator = {};

IEventRegistrator.Subscribers = {};

IEventRegistrator.RegisterSubscriber = function () {
    if (arguments.length == 1) {
        if (typeof (arguments[0]) != "string")
            return;
        var messageType = arguments[0];
        var subscriberID = Math.random().toString(36).substring(7);
        MSDesktopUtils.CallNative(IModule.ModuleID, "RegisterV2VSubscriber", [messageType, IChromeRegistry.ViewID, subscriberID]);
        var subscriber = {
            Subscribe: function (handler) {
                IEventRegistrator.Subscribers[subscriberID] = subscriber;
                subscriber.MessageReceived = handler;
            },
            MessageTypeName: messageType
        };
        return subscriber;
    }
}

IEventRegistrator.RegisterPublisher = function () {
    if (arguments.length == 1) {
        if (typeof (arguments[0]) != "string")
            return;
        var messageType = arguments[0];
        var publisherID = Math.random().toString(36).substring(7);
        MSDesktopUtils.CallNative(IModule.ModuleID, "RegisterV2VPublisher", [messageType, IChromeRegistry.ViewID, publisherID]);
        var publisher = {
            Publish: function (message) {
                MSDesktopUtils.CallNative(IModule.ModuleID, "PublishV2V", [message, publisherID, messageType]);
            },
            MessageTypeName: messageType
        };
        return publisher;
    }
}

IEventRegistrator.RouteSubscribtionNotification = function (subscriberID, message) {
    var subscriber = IEventRegistrator.Subscribers[subscriberID];
    subscriber.MessageReceived(message);
}

function IEventRegistratorRoute(id, message) {
    IEventRegistrator.RouteSubscribtionNotification(id, message);
}