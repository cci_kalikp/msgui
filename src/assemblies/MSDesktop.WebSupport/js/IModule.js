//Initialize.js
var IModule = {};

IModule.ModuleID = "";

//common initialization logic here, called by C# HTMLModule.Initialize method
IModule.InitializeModule = function () {

    /**
    * TODO: some neccesary initialization work
    */
    //save module's url as module ID
    if (arguments[0] != null) {
        IModule.ModuleID = arguments[0];
    }

    //call user-defined initialize funciton
    window.setTimeout(function() {
        Initialize();

        //raise initialization completed event in C# side
        MSDesktopUtils.CallNative(IModule.ModuleID, "OnModuleInitialized", []);
        //MSDesktopUtils.GetObjectForScripting().OnModuleInitialized();
    }, 3000);
};

function IModuleInitializeModule(url) {
    IModule.InitializeModule(url);
};

function IModuleSetModuleID(moduleId) {
    IModule.ModuleID = moduleId;
}
