﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Timers;
using System.Web;
using System.Windows.Controls;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager; 

namespace MSDesktop.WebSupport
{
    public class URLModule : HTMLModule
    {
        public static string URLBasedModuleFakeAddress = "http://urlhosting.ms.com/urlhost.html";
        public static string URLBasedActivatedModuleFakeAddress = "http://urlhosting.ms.com/urlactivatedhost.html";

        public URLModule(IUnityContainer unityContainer_)
            : base(unityContainer_)
        {
        }

        protected override Uri Url
        {
            get
            {
                var encodedUri = HttpUtility.UrlEncode(base.Url.AbsoluteUri);
                return new Uri(URLBasedModuleFakeAddress + "?" + encodedUri);
            }
        }

        internal protected virtual string GetURLProxyHtml()
        {
            return File.ReadAllText("URLProxy.html");
        }
    }

    public class ActivatedURLModule : HTMLModule
    { 

        public ActivatedURLModule(IUnityContainer unityContainer_)
            : base(unityContainer_)
        {
            this.ViewCreated += new EventHandler<WebControlEventArgs>(ActivatedURLModule_ViewCreated);
            this.ViewClosed += new EventHandler<WebControlEventArgs>(ActivatedURLModule_ViewClosed); 
        }


        void ActivatedURLModule_ViewCreated(object sender, WebControlEventArgs e)
        {
            if (e.ViewContainer.IsVisible)
            {
                LoadIfNeeded(e.ViewContainer);
            }
            e.ViewContainer.IsVisibleChanged += ViewContainer_IsVisibleChanged; 
        }

        void ActivatedURLModule_ViewClosed(object sender, WebControlEventArgs e)
        {
            e.ViewContainer.IsVisibleChanged -= ViewContainer_IsVisibleChanged;
        }

        void ViewContainer_IsVisibleChanged(object sender, MorganStanley.MSDotNet.MSGui.Core.VisibilityEventArgs e)
        {
           if (e.Visible)
           {
               IWindowViewContainer view = sender as IWindowViewContainer;
               if (view != null)
               {
                   LoadIfNeeded(view);
               } 
           } 
        } 
 
        private void LoadIfNeeded(IWindowViewContainer viewContainer_)
        {
            Timer timer = new Timer(500) { AutoReset = false };
            timer.Elapsed += (sender_, args_) =>
            {
                if (viewContainer_.IsVisible)
                {
                    IWebControl webControl = this.FindViewControl(viewContainer_.ID);
                    WebBrowser control = webControl.Control as WebBrowser;
                    if (control != null)
                    {
                        control.Dispatcher.Invoke((Action)(() =>
                        {
                            if (control.Source != null && control.Source.ToString() == HttpUtility.UrlDecode(this.Url.Query.Substring(1)))
                            {
                                return;
                            }
                            control.InvokeScript("doIt", new object[] { });
                        }));
                    }
                }
            };
            timer.Enabled = true; 
            
        }
        protected override Uri Url
        {
            get
            {
                var encodedUri = HttpUtility.UrlEncode(base.Url.AbsoluteUri);
                return new Uri(URLModule.URLBasedActivatedModuleFakeAddress + "?" + encodedUri);
            }
        }
        internal protected virtual string GetURLProxyHtml()
        {
            return File.ReadAllText("URLProxy.html");
        }
    }
}

