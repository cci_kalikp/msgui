﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Security.Policy;
using System.Windows.Controls;
using System.Xml.Linq;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.Persistence.DelayedLoading;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence.DelayedLoading;

namespace MSDesktop.WebSupport
{
    public static class WebSupportExtensions
    {
        private static readonly Dictionary<string, Uri> urlMap = new Dictionary<string, Uri>();
        private static bool webSupportEnabled = false;

        public static readonly IDictionary<object, HTMLModuleInfo> Registry = new Dictionary<object, HTMLModuleInfo>();

        public static void EnableWebSupport(this IFramework framework)
        {
            if (webSupportEnabled) return;
            DelayedProfileLoadingExtensionPoints.BeforeModuleInitialized += OnBeforeModuleInitialized;
            DelayedLoadingExtensions.EnableDelayedLoading();
            webSupportEnabled = true;
        }

        public static void AddHtmlModule<TModule>(this IFramework @this, Uri url) where TModule : HTMLModule
        {
            @this.AddHtmlModule<TModule>(url, url.ToString());
        }

        public static void AddHtmlModule<TModule>(this IFramework @this, Uri url, string name) where TModule : HTMLModule
        {
            @this.AddModule(typeof(TModule), name);
            urlMap.Add(name, url);
        }

        public static void EnableWebSupport(this IFramework framework, int moduleLoadTimeout)
        {
            framework.EnableWebSupport();
            DelayedLoadingExtensions.Timeout = moduleLoadTimeout;
        }

        private static void OnBeforeModuleInitialized(object sender, BeforeModuleInitializedEventArgs beforeModuleInitializedEventArgs)
        {
            var module = beforeModuleInitializedEventArgs.Module;
            var canHandle = module is HTMLModule || module is Flex.FlexModule;
            if (canHandle)
            {
                HandleModuleInit(beforeModuleInitializedEventArgs);
            }
        }

        private static void HandleModuleInit(BeforeModuleInitializedEventArgs e)
        {
            var module = e.Module;
            var moduleName = e.ModuleName;
            var extraInformation = e.ExtraInformation;
            var hasExtra = extraInformation != null &&
                           extraInformation.Count > 0;
            if (hasExtra)
            {
                CreateModuleInfo(extraInformation, module);
            }

            UpdateModuleInfoWithUrl(moduleName, module);

            if (Registry.ContainsKey(module))
            {
                Registry[module].Name = moduleName;
            }
        }

        private static void UpdateModuleInfoWithUrl(string moduleName, IMSDesktopModule module)
        {
            if (urlMap.ContainsKey(moduleName))
            {
                var url = urlMap[moduleName];
                if (Registry.ContainsKey(module))
                {
                    Registry[module].Url = url;
                }
                else
                {
                    Registry.Add(module, new HTMLModuleInfo
                    {
                        Url = url
                    });
                }
            }
        }

        private static void CreateModuleInfo(StringDictionary extraInfo, IMSDesktopModule module)
        {
            var htmlInfo = new HTMLModuleInfo();
            foreach (string infoName in extraInfo.Keys)
            {
                if (infoName.ToLower() == "url")
                {
                    string url = PathUtilities.ReplaceEnvironmentVariables(extraInfo[infoName]);
                    if (string.IsNullOrEmpty(url))
                    {
                        return;
                    }
                    htmlInfo.Url = new Uri(url);
                }
                else if (infoName.ToLower() == "location")
                {
                    htmlInfo.Location = extraInfo[infoName];
                }
                else if (infoName.ToLower() == "title")
                {
                    htmlInfo.Title = extraInfo[infoName];
                }
                else if (infoName.ToLower() == "image")
                {
                    htmlInfo.Image = PathUtilities.ReplaceEnvironmentVariables(extraInfo[infoName]);
                }
                else
                {
                    htmlInfo.ExtraInfo[infoName] = extraInfo[infoName];
                }
            }
            if (htmlInfo.Url == null) 
                return;

            Registry[module] = htmlInfo;
        }
    }

    public class HTMLModuleInfo
    {
        public HTMLModuleInfo()
        {
            ExtraInfo = new StringDictionary();
        }
        public Uri Url { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        public string Title { get; set; }
        public string Image { get; set; }
        public StringDictionary ExtraInfo { get; private  set; }
    }
}
