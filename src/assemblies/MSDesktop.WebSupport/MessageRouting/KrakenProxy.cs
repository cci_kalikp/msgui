﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using MSDesktop.MessageRouting;
using MSDesktop.MessageRouting.Routing;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MSDesktop.WebSupport.MessageRouting
{
    class KrakenProxy : IKrakenProxy
    {
        private readonly IRoutingCommunicator communicator;
        private readonly HTMLModule module;
        private readonly Dictionary<string, object> publishers;

        public KrakenProxy(IRoutingCommunicator communicator, HTMLModule module)
        {
            this.communicator = communicator;
            this.module = module;
            this.publishers = new Dictionary<string, object>();
        }

        public void GetRoutedPublisher(string typeName, string publisherId)
        {
            var type = Type.GetType(typeName);
            if (type == null) throw new ArgumentException(string.Format("Povided type {0} does not exist", typeName));
            var publisher = typeof(IRoutingCommunicator).GetMethod("GetPublisher")
                                                         .MakeGenericMethod(type)
                                                         .Invoke(communicator, new object[] { });
            publishers[publisherId] = publisher;
        }

        public void Publish(string publisherId, string typeName, JContainer content)
        {
            var publisher = publishers[publisherId];
            var type = Type.GetType(typeName);
            var message = content.ToObject(type);
            var info = (DeliveryInfo)typeof(IRoutedPublisher<>).MakeGenericType(type)
                                                  .GetMethod("Publish")
                                                  .Invoke(publisher, new[] { message });
        }

        public void GetRoutedSubscriber(string typeName, string subscriberId)
        {
            var type = Type.GetType(typeName);
            if (type == null) throw new ArgumentException(string.Format("Povided type {0} does not exist", typeName));
            var subscriber = typeof (IRoutingCommunicator).GetMethod("GetSubscriber")
                                                          .MakeGenericMethod(type)
                                                          .Invoke(communicator, new object[] {});
            var observable = typeof (IRoutedSubscriber<>).MakeGenericType(type)
                                                         .GetMethod("GetObservable")
                                                         .Invoke(subscriber, new object[] {});
            GetType()
                .GetMethod("DoSubscribe", BindingFlags.Instance | BindingFlags.NonPublic)
                .MakeGenericMethod(type)
                .Invoke(this, new object[] {observable, subscriberId, typeName});
        }

        private void DoSubscribe<T>(IObservable<T> observable, string subscriberId, string typeName)
        {
            observable.Subscribe(obj =>
                {
                    var content = JsonConvert.SerializeObject(obj);
                    foreach (var control in module.WebControls)
                    {
                        control.CallJavascriptFunction("receiveRoutingCommunicatorMessage", false, subscriberId, content);
                    }
                });
        }
    }
}
