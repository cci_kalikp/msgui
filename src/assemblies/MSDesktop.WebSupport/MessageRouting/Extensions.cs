﻿using MSDesktop.MessageRouting;
using Microsoft.Practices.Unity;

namespace MSDesktop.WebSupport.MessageRouting
{
    public static class Extensions
    {
        public static void EnableMessageRouting(this HTMLModule module)
        {
            var proxy = new KrakenProxy(module.Container.Resolve<IRoutingCommunicator>(), module);
            module.InjectKrakenProxy(proxy);
        }
    }
}
