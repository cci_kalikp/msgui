﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json.Linq;

namespace MSDesktop.WebSupport.MessageRouting
{
    interface IKrakenProxy
    {
        void GetRoutedPublisher(string type, string publisherId);
        void Publish(string publisherId, string type, JContainer content);
        void GetRoutedSubscriber(string typeName, string subscriberId);
    }
}
