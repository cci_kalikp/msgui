﻿using System;
using System.Windows;
using System.Windows.Threading;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.WebSupport;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity;

namespace MSDesktop.WebSupport
{

    public abstract class DelayedProfileLoadCoordinatingModule : IMSDesktopModule, IDelayedLoadModule
    {
        private readonly DispatcherTimer _timeoutTimer;
        private readonly IChromeManager _chromeManager;
        private readonly IModuleGroup _moduleGroup;

        protected DelayedProfileLoadCoordinatingModule(IUnityContainer container)
        {
            _chromeManager = container.Resolve<IChromeManager>();
            _moduleGroup = container.Resolve<IModuleGroup>();

            _timeoutTimer = new DispatcherTimer(TimeSpan.FromSeconds(WebSupportExtensions.Timeout),
                                               DispatcherPriority.ApplicationIdle,
                                               InitializationTimedOut, Application.Current.Dispatcher);

            _moduleGroup.RegisterDelayedLoadModule(this);
        }

        protected abstract void CloseInitializer();

        protected IChromeManager ChromeManager 
        {
            get { return _chromeManager; }
        }

        public virtual void Initialize()
        {
            _timeoutTimer.Start();
        }

        protected void AddContentType(Type contentType)
        {
            _moduleGroup.ContentTypes.Add(contentType);
        }

        protected void ViewLoadedCompletely()
        {
            if (_moduleGroup.ViewLoadedCompletely(WebSupportExtensions.Connection))
            {
                WebSupportExtensions.Connection = null;
            }
        }

        public void OnModuleInitialized()
        {
            if (_timeoutTimer != null)
            {
                _timeoutTimer.Stop();
            }

            CloseInitializer();
            _moduleGroup.DelayedLoadModuleInitialized(this);
        }

        private void InitializationTimedOut(object sender, EventArgs eventArgs)
        {
            if (_timeoutTimer != null)
            {
                _timeoutTimer.Stop();
            }

            CloseInitializer();
            _moduleGroup.DelayedLoadModuleInitializationFailed(this);
        }
    }
}
