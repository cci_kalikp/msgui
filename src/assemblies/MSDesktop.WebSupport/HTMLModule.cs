﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Xml.Linq;
using MSDesktop.WebSupport.MessageRouting;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Utility;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence.DelayedLoading;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MSDesktop.WebSupport
{
    public abstract class HTMLModule : DelayedProfileLoadCoordinatingModule, IJavascriptCallsHandler
    {
        private const string JsInitializeFunction = "IModuleInitializeModule";
        private const string JsSetModuleIDFunction = "IModuleSetModuleID";
        protected IWebControl _htmlInitializerControl;
        private Window _htmlInitializerWindow;

        private readonly IUnityContainer _unityContainer;
        private readonly IDictionary<string, Pair<object, Type>> _publishers;
        private readonly IDictionary<string, IWebControl> _viewToControl;
        private readonly IChromeRegistry _chromeRegistry;
        private readonly ICommunicator _communicator;
        private readonly IApplication _application;

        private readonly IEventRegistrator _eventRegistrator;
        private readonly IModuleGroup _moduleGroup;

        private readonly IDictionary<string, object> _v2vPublishers = new Dictionary<string, object>();

        private IKrakenProxy krakenProxy;
        private readonly IDictionary<string, object> moduleSubscribers = new Dictionary<string, object>();
        private readonly IDictionary<string, object> modulePublishers = new Dictionary<string, object>();
        protected bool RetainInitializerControl = false;

        #region Interface

        /// <summary>
        /// Should return a unique name within all HTML modules
        /// </summary>
        protected virtual string Name
        {
            get
            {
                var result = "";
                if (WebSupportExtensions.Registry.ContainsKey(this))
                {
                    result = WebSupportExtensions.Registry[this].Name;
                }
                return result;
            }
        }

        /// <summary>
        /// Should return name of a HTML file inside HTMLModule directory (without extension)
        /// </summary>
        protected virtual Uri Url
        {
            get { return WebSupportExtensions.Registry[this].Url; }
        }

        /// <summary>
        /// Should return an IWebControl implementation
        /// </summary>
        /// <returns></returns>
        protected virtual IWebControl CreateWebControlComponent()
        {
            return new DefaultWebControl.DefaultWebControl(this);
        }

        #endregion

        protected HTMLModule(IUnityContainer unityContainer) : base(unityContainer)
        {
            _unityContainer = unityContainer;

            _chromeRegistry = _unityContainer.Resolve<IChromeRegistry>();
            _unityContainer.Resolve<IPersistenceService>();

            _communicator = _unityContainer.Resolve<ICommunicator>();
            _unityContainer.Resolve<IApplication>();
            _publishers = new Dictionary<string, Pair<object, Type>>();
            _application = _unityContainer.Resolve<IApplication>();
            _moduleGroup = _unityContainer.Resolve<IModuleGroup>();
            _viewToControl = new Dictionary<string, IWebControl>();

            _eventRegistrator = _unityContainer.Resolve<IEventRegistrator>();
        }

        internal IUnityContainer Container { get { return _unityContainer; } }

        internal IEnumerable<IWebControl> WebControls { get { return _viewToControl.Values; } }

        #region Initialization

        // The workflow of initialization is as follows:
        // * create a window with a web control and load given URL to that control (Initialize)
        // * move the window away from the screen (LayoutLoaded)
        // * call the Initialize function on the JS side (LoadCompletedHandler)
        // * when it returns, close the window (OnModuleInitialized)
        public override void Initialize()
        {
            base.Initialize();
            try
            {

                _htmlInitializerControl = CreateWebControlComponent();
                AddContentType(_htmlInitializerControl.Control.GetType());
            }
            catch (FileNotFoundException fileNotFoundException)
            {
                if (fileNotFoundException.FileName.StartsWith("CefSharp"))
                {
                    // InstanceCount--;
                    var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    var setting = config.AppSettings.Settings["CPPRuntimeMissingMail"];
                    if (setting != null)
                    {
                        using (var client = new SmtpClient("mta-hub.ms.com"))
                        using (
                            var message =
                                new MailMessage(Environment.UserName + "@ms.com",
                                                setting.Value, "Missing CPP Runtime",
                                                string.Format(
                                                    "CPP Runtime is missing on machine {0}, which belongs to {1} in sysloc {2}",
                                                    Environment.MachineName,
                                                    Environment.UserName,
                                                    Environment.GetEnvironmentVariable(
                                                        "SYS_LOC"))))
                        {
                            client.Send(message);
                        }

                        return;

                    }

                    throw;
                }

                throw;
            }
            _htmlInitializerControl.JavascriptCallHandler = this;
            _htmlInitializerControl.LoadCompleted += LoadCompletedHandler;
            _htmlInitializerControl.LoadFile(Url);

            _application.ApplicationLoaded +=
                (sender, args) =>
                    {
                        _htmlInitializerWindow = new Window
                            {
                                Top = -5000,
                                Left = -5000,
                                Height = 100,
                                Width = 100,
                                Content = _htmlInitializerControl.Control,
                                ShowInTaskbar = false
                            };
                        _htmlInitializerWindow.Show();
                    };
        }

        private void LoadCompletedHandler(object sender, EventArgs e)
        {
            var webControl = sender as IWebControl;
            if (webControl != null)
            {
                webControl.LoadCompleted -= LoadCompletedHandler;
                webControl.CallJavascriptFunction(JsInitializeFunction, true, Name);
            }

        }
        
        protected override sealed void CloseInitializer()
        {
            if (!RetainInitializerControl)
                _htmlInitializerControl = null;

            if (_htmlInitializerWindow != null)
                _htmlInitializerWindow.Close();
        }

        #endregion

        #region M2M communication

        public void GetModulePublisher(string messageTypeName, string publisherId)
        {
            var type = Type.GetType(messageTypeName);
            if (type == null) throw new ArgumentException(string.Format("Provided type {0} does not exist", messageTypeName));
            var publisher = typeof(ICommunicator).GetMethod("GetPublisher")
                                                         .MakeGenericMethod(type)
                                                         .Invoke(_communicator, new object[] { });
            modulePublishers[publisherId] = publisher;
        }

        public void GetModuleSubscriber(string typeName, string subscriberId)
        {
            var type = Type.GetType(typeName);
            if (type == null) throw new ArgumentException(string.Format("Povided type {0} does not exist", typeName));
            var subscriber = typeof(ICommunicator).GetMethod("GetSubscriber")
                                                          .MakeGenericMethod(type)
                                                          .Invoke(_communicator, new object[] { });
            var observable = typeof(IModuleSubscriber<>).MakeGenericType(type)
                                                         .GetMethod("GetObservable")
                                                         .Invoke(subscriber, new object[] { });
            typeof(HTMLModule)
                .GetMethod("DoSubscribe", BindingFlags.Instance | BindingFlags.NonPublic)
                .MakeGenericMethod(type)
                .Invoke(this, new object[] { observable, subscriberId, typeName });

            // _moduleGroup.SubscribeM2M(typeName);
        }

        public void ModulePublisherPublish(string id, string typeName, JContainer content)
        {
            var publisher = modulePublishers[id];
            var type = Type.GetType(typeName);
            var message = content.ToObject(type);
            typeof(IModulePublisher<>).MakeGenericType(type)
                                                  .GetMethod("Publish", new[] { type })
                                                  .Invoke(publisher, new[] { message });
            // _moduleGroup.PublishM2M(message);
        }

        // DON'T DELETE: Used via reflection.
        private void DoSubscribe<T>(IObservable<T> observable, string subscriberId, string typeName)
        {
            observable.Subscribe(obj =>
                {
                    var content = JsonConvert.SerializeObject(obj);
                    foreach (var control in WebControls)
                    {
                        control.Dispatcher.Invoke(new Action(() =>
                            {
                                control.CallJavascriptFunction("receiveCommunicatorMessage", false, subscriberId,
                                                               content);
                            }));
                    }
                });
        }

        #region legacy M2M

        public void Subscribe(string messageTypeName, string onMessageCallback)
        {
            var messageType = Type.GetType(messageTypeName);
            var handlerFactory = GetType()
                .GetMethod("GetMessageHandler")
                .MakeGenericMethod(messageType);
            var handler = handlerFactory.Invoke(this, BindingFlags.Instance | BindingFlags.NonPublic,
                                                null, new object[] {onMessageCallback}, null);

            MessagingReflectionUtilities.GetSubscriber(_communicator, messageType, handler);
            // _moduleGroup.SubscribeM2M(messageTypeName);
        }

        public Action<T> GetMessageHandler<T>(string handler)
        {
            return
                m =>
                    {
                        foreach (var webControl in _viewToControl.Values)
                        {
                            var control = webControl;
                            webControl.Dispatcher.Invoke(
                                new Action(
                                    () => control.CallJavascriptFunction(
                                        handler,
                                        false,
                                        new object[] {JsonConvert.SerializeObject(m)})
                                    ));
                        }
                    };
        }

        public bool GetPublisher(string messageTypeName, string publisherId)
        {
            var messageType = Type.GetType(messageTypeName);
            var publisher = MessagingReflectionUtilities.GetPublisher(_communicator, messageType);
            _publishers.Add(publisherId, Pair.Make(publisher, messageType));
            return true;
        }

        public void Publish(string publisherId, object message)
        {
            var pair = _publishers[publisherId];
            var type = pair.Second;
            var publisher = pair.First;

            var pageObject = GetObjectFromJContainerAndType(message, type);
            MessagingReflectionUtilities.Publish(publisher, pageObject);
            // _moduleGroup.PublishM2M(pageObject);
        }

        #endregion

        #endregion

        #region Chrome management

        public void RegisterWindowFactory(string id, string loadState, string saveFunc, string title)
        {
            lock (_chromeRegistry)
            {
                _chromeRegistry
                    .RegisterWindowFactory(
                        id,
                        (container, state) =>
                            {
                                var viewControl = CreateWebControlComponent();
                                viewControl.LoadCompleted +=
                                    (sender, args) =>
                                        {
                                            _viewToControl[container.ID] = viewControl; 
                                            var copy = ViewCreated;
                                            if (copy != null)
                                            {
                                                copy(this, new WebControlEventArgs(container, viewControl));
                                            }

                                            if (saveFunc == null || state == null)
                                            {
                                                viewControl.CallJavascriptFunction("LoadViewStateRouter", true, loadState,
                                                                                   container.ID, null);
                                            }
                                            else
                                            {
                                                var htmlState =
                                                    state.Descendants("HTMLState").First().
                                                           Value;
                                                viewControl.CallJavascriptFunction("LoadViewStateRouter", true, loadState,
                                                                                   container.ID, htmlState);
                                            }
                                            ViewLoadedCompletely();
                                        };
                                viewControl.JavascriptCallHandler = this;
                                viewControl.LoadCompleted += ViewControlOnLoadCompleted;
                                viewControl.LoadFile(Url);
                                container.Title = title ?? id;
                                container.Content = viewControl.Control;
                                container.Closed += HtmlViewClosed;
                                container.ClosedQuietly += HtmlViewClosed;
                                return true;
                            },
                        viewContainer =>
                            {
                                var stateDoc = new XDocument();
                                if (!_viewToControl.ContainsKey(viewContainer.ID)) return stateDoc;
                                var rootEl = new XElement("HTMLView");
                                if (saveFunc == null) return stateDoc;
                                var control = _viewToControl[viewContainer.ID];
                                var htmlState = control.CallJavascriptFunctionWithResult(saveFunc);
                                var htmlStateEl = new XElement("HTMLState", htmlState);
                                rootEl.Add(htmlStateEl);
                                stateDoc.Add(rootEl);
                                return stateDoc;
                            });
            }
        }

        private void ViewControlOnLoadCompleted(object sender, EventArgs eventArgs)
        {
            var webControl = sender as IWebControl;
            if (webControl != null)
            {
                webControl.LoadCompleted -= LoadCompletedHandler;
                webControl.CallJavascriptFunction(JsSetModuleIDFunction, true, Name);
            }
        }

        private void HtmlViewClosed(object sender, WindowEventArgs windowEventArgs)
        {
            windowEventArgs.ViewContainer.Closed -= HtmlViewClosed;
            windowEventArgs.ViewContainer.ClosedQuietly -= HtmlViewClosed;
            var webControl = _viewToControl[windowEventArgs.ViewContainer.ID];
            webControl.Dispose();
            _viewToControl.Remove(windowEventArgs.ViewContainer.ID);
            var copy = ViewClosed;
            if (copy != null)
            {
                copy(this, new WebControlEventArgs(windowEventArgs.ViewContainer, webControl));
            }
        }

        public void PlaceWidget(string id, string location, JContainer initialParamsJContainer, string typeName)
        {
            var type = Type.GetType(typeName);
            var initialParams =
                GetObjectFromJContainerAndType(initialParamsJContainer, type) as InitialWidgetParameters;
            lock (ChromeManager)
            {
                ChromeManager.PlaceWidget(id, location, initialParams);
            }
        }

        public void RegisterWidgetFactory(string id)
        {
            lock (_chromeRegistry)
            {
                _chromeRegistry.RegisterWidgetFactory(id);
            }
        }

        protected IWebControl FindViewControl(string viewId)
        {
            IWebControl control;
            _viewToControl.TryGetValue(viewId, out control);
            return control;
        }

        #endregion

        #region V2V communication

        public void RegisterV2VPublisher(string messageType, string viewId, string publisherId)
        {
            var type = Type.GetType(messageType);
            var viewcontainer = ChromeManager.GetViews().First(w => w.Content == _viewToControl[viewId].Control);
            var publisher =
                typeof (IEventRegistrator).GetMethod("RegisterPublisher")
                                          .MakeGenericMethod(type)
                                          .Invoke(_eventRegistrator, new object[] {viewcontainer});
            _v2vPublishers[publisherId] = publisher;
        }

        public void PublishV2V(JContainer message, string publisherId, string messageType)
        {
            var type = Type.GetType(messageType);
            var typedMessage = GetObjectFromJContainerAndType(message, type);
            typeof (IPublisher<>).MakeGenericType(type).GetMethod("Publish").Invoke(
                _v2vPublishers[publisherId],
                new[] {typedMessage, CommunicationTargetFilter.All, new string[] {}});
        }

        public void RegisterV2VSubscriber(string messageType, string viewId, string subscriberId)
        {
            var type = Type.GetType(messageType);
            var viewcontainer = ChromeManager.GetViews().First(w => w.Content == _viewToControl[viewId].Control);
            
            var handler = GetType()
                .GetMethod("GetV2VMessageHandler")
                .MakeGenericMethod(type)
                .Invoke(this, new object[] {viewId, subscriberId});


            SubscriberHelper.GetSubscriber(_eventRegistrator, type, viewcontainer, handler);  
        }

        public Action<T> GetV2VMessageHandler<T>(string viewId, string subscriberId)
        {
            return message =>
                {
                    var control = _viewToControl[viewId];
                    control.Dispatcher.Invoke(
                        new Action(
                            () =>
                            control.CallJavascriptFunction("IEventRegistratorRoute", false, subscriberId,
                                                           JsonConvert.SerializeObject(message))));
                };
        }

        #endregion

        #region Kraken communication

        internal void InjectKrakenProxy(IKrakenProxy proxy)
        {
            krakenProxy = proxy;
        }

        public void GetRoutedPublisher(string type, string publisherId)
        {
            if (krakenProxy == null) throw new InvalidOperationException("MessageRouting extension is not enabled in this HTMLModule");
            krakenProxy.GetRoutedPublisher(type, publisherId);
        }

        public void RoutedPublisherPublish(string publisherId, string type, JContainer content)
        {
            if (krakenProxy == null) throw new InvalidOperationException("MessageRouting extension is not enabled in this HTMLModule");
            krakenProxy.Publish(publisherId, type, content);
        }

        public void GetRoutedSubscriber(string typeName, string subscriberId)
        {
            if (krakenProxy == null) throw new InvalidOperationException("MessageRouting extension is not enabled in this HTMLModule");
            krakenProxy.GetRoutedSubscriber(typeName, subscriberId);
        }

        #endregion

        #region View Events

        public event EventHandler<WebControlEventArgs> ViewCreated;
        public event EventHandler<WebControlEventArgs> ViewClosed;
 
        #endregion

        private static object GetObjectFromJContainerAndType(object jcontainer, Type type)
        {
            var jMessage = jcontainer as JContainer;

            return typeof (JToken)
                .GetMethod("ToObject", new Type[] {})
                .MakeGenericMethod(new[] {type})
                .Invoke(jMessage, new object[] {});
        }
    }

    public class WebControlEventArgs:WindowEventArgs
    {
        public WebControlEventArgs(IWindowViewContainer container_, IWebControl control_):base(container_)
        { 
            this.Control = control_;
        }
         
        public IWebControl Control { get; private set; }
    }

}
