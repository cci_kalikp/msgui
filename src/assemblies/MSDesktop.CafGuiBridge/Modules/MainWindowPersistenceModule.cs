﻿using System;
using System.Windows;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.CafGUI.Toolkit.Persistence;
using MorganStanley.MSDotNet.CafGUI.Toolkit.Persistence.Wpf;

namespace MSDesktop.CafGuiBridge.Modules
{
    public sealed class MainWindowPersistenceModule : IModule
    {
        private IPersistenceService _cafGuiPersistenceService;
        private readonly MorganStanley.MSDotNet.MSGui.Core.IPersistenceService _msdesktopPersistenceService;

        public MainWindowPersistenceModule(MorganStanley.MSDotNet.MSGui.Core.IPersistenceService msdesktopPersistenceService)
        {
            _msdesktopPersistenceService = msdesktopPersistenceService;
        }

        public void Initialize()
        {
            var cafGuiPersistence = Application.Current.Resources["ApplicationPersistence"] as WpfApplicationPersistence;
            if (cafGuiPersistence != null)
            {
                _cafGuiPersistenceService = cafGuiPersistence.ProfilesCollection.PersistenceService;
                _cafGuiPersistenceService.ProfileStateSaving += HandleStateSaving;
            }
        }

        private void HandleStateSaving(object sender, ProfileEventArgs e)
        {
            var profileName = e.ProfileName;
            if (profileName.Equals("default", StringComparison.CurrentCultureIgnoreCase))
            {
                profileName = "Default";
            }

            _msdesktopPersistenceService.SaveProfileAs(profileName);
        }
    }
}
