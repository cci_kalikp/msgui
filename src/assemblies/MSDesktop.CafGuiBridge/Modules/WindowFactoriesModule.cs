﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MSDesktop.CafGuiBridge.WindowFactories;

namespace MSDesktop.CafGuiBridge.Modules
{
    public sealed class WindowFactoriesModule : IModule
    {
        internal const string CafGuiWindowFactoryName = "CafGuiToolkitWindowFactory";

        private readonly IChromeRegistry _chromeRegistry;
        private readonly ICafGuiFactoryRegistry _factoryRegistry;
        private readonly IDictionary<string, string> _cafGuiViewIds;
        private readonly IDictionary<string, IWindowViewContainer> _placeholders;

        public WindowFactoriesModule(IChromeRegistry chromeRegistry, ICafGuiFactoryRegistry factoryRegistry)
        {
            _chromeRegistry = chromeRegistry;
            _factoryRegistry = factoryRegistry;
            _placeholders = new Dictionary<string, IWindowViewContainer>(StringComparer.OrdinalIgnoreCase);
            _cafGuiViewIds = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
        }

        public void Initialize()
        {
            // Register the window factories
            _chromeRegistry.RegisterWindowFactory(
                CafGuiWindowFactoryName,
                CreateWindowPlaceholder,
                DehydrateWindow);
        }

        internal XDocument DehydrateWindow(IWindowViewContainer container)
        {
            var parameters = container.Parameters as InitialCafGuiWindowParameters;
            var cafGuiViewId = parameters != null
                ? parameters.CafGuiViewId
                : _cafGuiViewIds[container.ID];

            // Check to see if we have CafGUI window factory, and can dehydrate it

            var viewContents = ((CafGuiFactoryRegistry) _factoryRegistry).DehydrateWindow(cafGuiViewId);

            XDocument document;
            if (viewContents != null)
            {
                document = new XDocument(
                    new XElement("CafGuiViewState",
                                 new XElement("CafGuiViewId", cafGuiViewId),
                                 viewContents));
            }
            else
            {
                document = new XDocument(
                    new XElement("CafGuiViewState",
                                 new XElement("CafGuiViewId", cafGuiViewId)));
            }

            return document;
        }

        internal bool CreateWindowPlaceholder(IWindowViewContainer container, XDocument state)
        {
            var parameters = container.Parameters as InitialCafGuiWindowParameters;
            if (parameters != null)
            {
                // Actual creation.
                container.Content = parameters.CafGuiUiElement;
                _placeholders.Add(parameters.CafGuiViewId, container);
            }
            else
            {
                // Placeholder from persistence. Set a simple content so that the view can be placed properly 
                // Place the container into placeholders collection. We expect it to be in the first node.
                var firstNode = state.Root as XContainer;

                if (firstNode != null)
                {
                    var idNode = firstNode.Descendants("CafGuiViewId").First();
                    var cafGuiViewId = idNode.Value;
                    var viewState = idNode.NodesAfterSelf().FirstOrDefault() as XElement;
                    container.Content = ((CafGuiFactoryRegistry)_factoryRegistry).CreateWindow(cafGuiViewId, viewState);
                    _cafGuiViewIds[container.ID] = cafGuiViewId;
                }
            }

            return true;
        }
    }
}
