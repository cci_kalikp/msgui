﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Xml;
using System.Xml.Linq;
using MorganStanley.IED.Concord.Configuration;
using MorganStanley.MSDotNet.CafGUI.Toolkit.Persistence;
using MorganStanley.MSDotNet.CafGUI.Toolkit.Persistence.Wpf;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;

namespace MSDesktop.CafGuiBridge.Persistence
{
    public class CafGuiFilePersistanceStorage : ConcordPersistenceStorage, IConfigurationInterceptingReader
    {
        // CafGUI configuration keys for the settings we are interested in
        private const string WindowBoundsKey = "MorganStanley.MSDotNet.MSGui.Impl.Shell.ShellWindow:mainShell:WindowBounds";
        private const string LayoutKey = "Infragistics.Windows.DockManager.XamDockManager:dockManager:Layout";
        private const string WindowManagerType = "MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl.ConcordWindowManager, MSDotNet.MSGui.Impl";

        private readonly WpfApplicationPersistence _persistence;
        private readonly CustomPersistableStateOwner _persistableStateOwner;

        public CafGuiFilePersistanceStorage()
        {
            _persistence = Application.Current.Resources["ApplicationPersistence"] as WpfApplicationPersistence;
            var mainWindow = Application.Current.MainWindow;
            var stateSet = WindowPersistence.GetStateSet(mainWindow);

            if (_persistence != null)
            {
                var persistenceConfiguration = _persistence.PersistenceConfiguration;

                if (string.IsNullOrEmpty(stateSet))
                {
                    stateSet = ApplicationPersistence.DefaultPersistableStateOwnerSetName;
                }

                PersistableStateOwnerSet persistableStateOwnerSet;
                if (persistenceConfiguration.PersistableStateOwnerSets.TryGetValue(stateSet,
                                                                                   out persistableStateOwnerSet))
                {
                    _persistableStateOwner = new CustomPersistableStateOwner();
                    persistableStateOwnerSet.PersistableStateOwners.Add(_persistableStateOwner);
                }
            }
        }

        public ConfigurationSection InterceptRead(ConfigurationSection original)
        {
            // Here, read the CafGUI profile. We are interested only in window layout. If the original's InnerXml is empty,
            // we must fall back onto CafGUI's persistence.
            //           PersistableWindowStateOwner windowStateOwner = new PersistableWindowStateOwner(window, 0, true);

            var configSection = original;
            var isOriginalAbsent = 
                original.ReadWriteNode == null 
                || string.IsNullOrWhiteSpace(original.ReadWriteNode.OuterXml) 
                || string.IsNullOrWhiteSpace(original.ReadWriteNode.InnerXml);

            if (_persistableStateOwner != null && isOriginalAbsent)
            {
                var persistenceService = _persistableStateOwner.PersistenceService;

                // Form the state XML.
                var layoutElement = new XElement("Layout");
                var stateElement = new XElement("State");

                var stateItem = persistenceService.GetStateItem(WindowBoundsKey);
                if (stateItem != null)
                {
                    var windowBounds = stateItem.ToString();
                    if (!string.IsNullOrEmpty(windowBounds))
                    {
                        stateElement.Add(WindowBoundsToMainWindowItem(windowBounds));
                    }
                }

                var xamManagerConfig = persistenceService.GetStateItem(LayoutKey);
                if (xamManagerConfig != null)
                {
                    var xamMgrConfigXml = xamManagerConfig.ToString();
                    if (!string.IsNullOrEmpty(xamMgrConfigXml))
                    {
                        stateElement.Add(XamManagerConfigToItem(xamMgrConfigXml));
                    }
                }

                layoutElement.SetAttributeValue("name", "Default");
                layoutElement.Add(stateElement);
                var layoutsElement = new XElement("Layouts", layoutElement);

                // Form the WindowManager element
                var windowManagerElement = new XElement("WindowManager",
                                                        layoutsElement,
                                                        new XElement("DefaultLayout", "Default"));
                windowManagerElement.SetAttributeValue("type", WindowManagerType);

                var document = new XDocument(
                    new XElement("MSDesktopLayouts", windowManagerElement));

                XmlDocument readWriteDocument;

                using (var documentReader = document.CreateReader())
                {
                    readWriteDocument = new XmlDocument();
                    readWriteDocument.Load(documentReader);
                }

                configSection = new ConfigurationSection(original.Name, original.Version, new XmlNode[] {},
                                                         readWriteDocument.DocumentElement);
            }

            return configSection;
        }

        private static void RemapContentPanes(XContainer element)
        {
            var panes = new Dictionary<string, Guid>();

            foreach (var paneElement in element
                .DescendantNodes()
                .OfType<XElement>()
                .Where(IsNodeContentPane))
            {
                var nameAttribute = paneElement.Attribute("name");
                if (nameAttribute != null)
                {
                    var cafGuiViewId = nameAttribute.Value;
                    if (!string.IsNullOrEmpty(cafGuiViewId))
                    {
                        Guid guid;
                        if (!panes.TryGetValue(cafGuiViewId, out guid))
                        {
                            guid = Guid.NewGuid();
                            panes.Add(cafGuiViewId, guid);
                        }

                        nameAttribute.Value = string.Format("{0}{1}", "contentPaneview", guid.ToString("N"));
                    }
                }
            }

            // Write out Panes section
            var panesElement = new XElement("Panes");
            element.Add(panesElement);

            foreach (var kvp in panes)
            {
                var iviewElement = new XElement("IView",
                                                new XElement("State",
                                                             new XElement("CafGuiViewState",
                                                                          new XElement("CafGuiViewId", kvp.Key))));
                iviewElement.SetAttributeValue("Id", string.Format("{0}{1}", "view", kvp.Value.ToString("N")));
                iviewElement.SetAttributeValue("ViewContainerType", "WithCreator");
                iviewElement.SetAttributeValue("CreatorID", "CafGuiToolkitWindowFactory");

                panesElement.Add(iviewElement);
            }
        }

        private static bool IsNodeContentPane(XElement node)
        {
            return node != null && node.Name.LocalName.Equals("contentPane", StringComparison.Ordinal);
        }

        private static XElement XamManagerConfigToItem(string xamManagerConfig)
        {
            var xamLayoutElement = new XElement("XamDockManagerLayout");
            var tabbedDockElement = new XElement("TabbedDock", xamLayoutElement);
            var element = new XElement("Item", tabbedDockElement);
            element.SetAttributeValue("Id", "MSGui.Internal.ViewManager");

            // We need to load xamManagerConfig into xmlDocument
            XDocument stateDoc;
            using (var reader = new StringReader(xamManagerConfig))
            {
                stateDoc = XDocument.Load(reader);
            }

            xamLayoutElement.Add(stateDoc.Root);
            RemapContentPanes(tabbedDockElement);

            return element;
        }

        private static XElement WindowBoundsToMainWindowItem(string windowBounds)
        {
            var stringParts = windowBounds.Split(';');
            var windowCoords = stringParts[1].Split(',');

            var mainWindowElement = new XElement("MainWindow");
            var element = new XElement("Item", mainWindowElement);
            element.SetAttributeValue("Id", "MSGui.Internal.Shell");

            mainWindowElement.SetAttributeValue("WindowState", stringParts[0]);
            mainWindowElement.SetAttributeValue("Left", windowCoords[0]);
            mainWindowElement.SetAttributeValue("Top", windowCoords[1]);
            mainWindowElement.SetAttributeValue("Width", windowCoords[2]);
            mainWindowElement.SetAttributeValue("Height", windowCoords[3]);

            return element;
        }

        private class CustomPersistableStateOwner : IPersistableStateOwner
        {
            private IPersistenceService _persistenceService;
            private bool _isActive = true;

            public void RestoreState(PersistentStorageType storageType)
            {
            }

            public void StoreState(PersistentStorageType storageType)
            {
            }

            public int Priority { get; set; }

            public bool IsActive
            {
                get
                {
                    return _isActive;
                }
                set
                {
                    _isActive = value;
                }
            }

            public IPersistenceService PersistenceService
            {
                get
                {
                    return _persistenceService;
                }
                set
                {
                    _persistenceService = value;
                }
            }
        }
    }
}
