﻿using System.Collections.Generic;
using System.Xml.Linq;

namespace MSDesktop.CafGuiBridge.WindowFactories
{
    internal sealed class CafGuiFactoryRegistry : ICafGuiFactoryRegistry
    {
        private readonly IList<ICafGuiWindowFactory> _factories;

        public CafGuiFactoryRegistry()
        {
            _factories = new List<ICafGuiWindowFactory>();
        }

        public object CreateWindow(string cafGuiWindowId, XElement state)
        {
            foreach (var factory in _factories)
            {
                object element;
                if (factory.TryCreateWindow(cafGuiWindowId, state, out element))
                {
                    return element;
                }
            }

            return null;
        }

        public XElement DehydrateWindow(string cafGuiFactoryId)
        {
            XElement state = null;
            foreach (var factory in _factories)
            {
                if (factory.TrySaveState(cafGuiFactoryId, out state))
                {
                    return state;
                }
            }

            return state;
        }

        public void RegisterCafGuiWindowFactory(ICafGuiWindowFactory factory)
        {
            _factories.Add(factory);
        }
    }
}
