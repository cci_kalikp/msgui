﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSDesktop.CafGuiBridge.WindowFactories
{
    public interface ICafGuiFactoryRegistry
    {
        void RegisterCafGuiWindowFactory(ICafGuiWindowFactory factory);
    }
}
