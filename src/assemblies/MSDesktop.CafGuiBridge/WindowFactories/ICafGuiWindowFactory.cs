﻿using System.Xml.Linq;

namespace MSDesktop.CafGuiBridge.WindowFactories
{
    public interface ICafGuiWindowFactory
    {
        bool TryCreateWindow(string cafGuiWindowId, XElement state, out object element);
        bool TrySaveState(string factoryId, out XElement state);
    }
}
