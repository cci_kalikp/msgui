﻿using MSDesktop.CafGuiBridge.Modules;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MSDesktop.CafGuiBridge.WindowFactories;

namespace MSDesktop.CafGuiBridge
{
    public static class CafGuiBridgeExtensions
    {
        public static void EnableCafGuiExtentions(this IFramework framework)
        {
            framework.AddPreInitializer<CafGuiBridgePreInitializer>();
            framework.AddModule<WindowFactoriesModule>();
            framework.AddModule<MainWindowPersistenceModule>();
        }
    }

    internal sealed class CafGuiBridgePreInitializer : IPreInitializer
    {
        public void PreInitialize(IUnityContainer container)
        {
            container.RegisterType<ICafGuiFactoryRegistry, CafGuiFactoryRegistry>(
                new ContainerControlledLifetimeManager());
        }
    }
}
