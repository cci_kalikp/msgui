#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.Configuration/FileConfigurationStoreEx.cs#1 $
  $DateTime: 2013/04/11 16:41:48 $
    $Change: 823942 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion

using System;
using System.Collections;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Xml;
using MorganStanley.MSDotNet.MSLog;


using MorganStanley.MSDotNet.MSXml;

namespace MorganStanley.Desktop.Configuration
{
  public class FileConfigurationStoreEx : IConfigurationStorageWriter, IConfigurationEnumeratesConfigs
  {
    #region Declarations
    private NameValueCollection _settings;
    private ArrayList _additionalFolders;
    private readonly string FOLDER_QUERY = "RETURN_FOLDER_LIST";
    private string _systemBaseDir;
    private string _userBaseDir;
    private string _omitConfigExtension;
    private MSXmlProcessorFactoryCollection _xmlProcessorCollection = new MSXmlProcessorFactoryCollection();
    private MSXmlHashtableParameterResolver _envVarParamResolver= new MSXmlHashtableParameterResolver();
    private static readonly string[] ETSCFG_PROFILE_ATTRIBUTES = { Configurator.CONFIG_SETTING_APP_KEY, 
                                                                   Configurator.CONFIG_SETTING_REGION_KEY,
                                                                   Configurator.CONFIG_SETTING_ENV_KEY };
    public static Hashtable NamespacesForXInclude = new Hashtable();
    #endregion Declarations

    #region Constructors
    /// <include file='xmldocs/FileConfigurationStoreEx.cs.xml' path='doc/doc[@for="FileConfigurationStoreEx.FileConfigurationStoreEx"]/*'/>
    public FileConfigurationStoreEx()
    {
      _additionalFolders = new ArrayList();

      ConcordXIncludeProcessorFactory includeProcessorFactory = new ConcordXIncludeProcessorFactory();
      _xmlProcessorCollection.Add(includeProcessorFactory);
      _envVarParamResolver.Hashtable = new Hashtable(System.Environment.GetEnvironmentVariables());
    }
    #endregion Constructors

    #region Implementation of IConfigurationStorageWriter
    /// <include file='xmldocs/FileConfigurationStoreEx.cs.xml' path='doc/doc[@for="FileConfigurationStoreEx.WriteConfiguration"]/*'/>
    public virtual void WriteConfiguration(string name_, string version_, System.Xml.XmlNode node_)
    {
      #region Logging
      if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
      {
        LogLayer.Debug.Location("FileConfigurationStore", "WriteConfig").Append(string.Format(Res.GetString(Res.LOG_WRITE_CONFIG), name_, version_, node_.OuterXml)).Send();
      }
      #endregion Logging

      string file = GetUserFilePath(_settings, name_, version_);
      
      using (FileStream fs = new FileStream(file, FileMode.Create, FileAccess.Write, FileShare.Read))
      {
        try 
        {
          node_.OwnerDocument.Save(fs);
        } 
        catch (Exception ex_) 
        {
          #region Logging
          if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Error)) 
          {
              LogLayer.Error.Location("FileConfigurationStore", "WriteConfig").Append(string.Format(Res.GetString(Res.LOG_ERROR_SAVING_CONFIG), ex_, name_, version_, node_.OuterXml)).Send();
          }
          #endregion Logging

          throw new ConfiguratorException("Error writing configuration", ex_);
        }
      }
    }
    #endregion Implementation of IConfigurationStorageWriter

    #region Implementation of IConfigurationStorageReader
    /// <include file='xmldocs/FileConfigurationStoreEx.cs.xml' path='doc/doc[@for="FileConfigurationStoreEx.Initialize"]/*'/>
    public virtual void Initialize(string settings_) 
    {
      #region Logging
      if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
      {
          LogLayer.Debug.Location("FileConfigurationStore", "Initialize").Append(string.Format(Res.GetString(Res.LOG_FILESTORE_INITIALIZE), settings_)).Send();
      }
      #endregion Logging

      _settings = this.ParseProfileString(settings_);
			
      if (_userBaseDir == null || _userBaseDir.Length == 0)
      {
        _userBaseDir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
      }

      if (_systemBaseDir == null || _systemBaseDir.Length == 0)
      {
        _systemBaseDir = AppDomain.CurrentDomain.BaseDirectory;
      }

      _userBaseDir = _userBaseDir.TrimEnd('\\');
      _systemBaseDir = _systemBaseDir.TrimEnd('\\');
    }

    public virtual void Initialize(string settings_, XmlNode config_) 
    {
      _settings = this.ParseProfileString(settings_);

      //read the node pertaining to this provider from app.config
      if (config_ != null)
      {
        XmlNode node = config_.SelectSingleNode("SystemBaseDir",null);
        if (node != null)
        {
          string sData = ParseDirectoryString(node.InnerXml);
          if (sData != null && sData.Length > 0)
          {
            _systemBaseDir = sData;
          }
        }

        node = config_.SelectSingleNode("UserBaseDir",null);
        if (node != null)
        {
          string sData = ParseDirectoryString(node.InnerXml);
          if (sData != null && sData.Length > 0)
          {
            _userBaseDir = sData; 
          }
        }

        node = config_.SelectSingleNode("OmitConfigExtension",null);
        if (node != null)
        {
          _omitConfigExtension = node.InnerXml;
        }

        node = config_.SelectSingleNode("AdditionalFolders",null);
        if (node != null)
        {
          ParseFolderString(node.InnerXml);
        }

        node = config_.SelectSingleNode("NameSpacesForXInclude", null);
        if (node != null)
        {
          SetNamespacesForXInclude(node);
        }

        node = config_.SelectSingleNode("etscfg", null);
        if (node != null)
        {
          GenerateEtscfgSettings(node);
        }

        node = config_.SelectSingleNode("XIncludeFolders", null);
        if (node != null)
        {
          SetXIncludeFolders(node);
        }
      }

      if (_userBaseDir == null || _userBaseDir.Length == 0)
      {
        _userBaseDir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
      }

      if (_systemBaseDir == null || _systemBaseDir.Length == 0)
      {
        _systemBaseDir = AppDomain.CurrentDomain.BaseDirectory;
      }

      _userBaseDir = _userBaseDir.TrimEnd('\\');
      _systemBaseDir = _systemBaseDir.TrimEnd('\\');
    }

    /// <include file='xmldocs/FileConfigurationStoreEx.cs.xml' path='doc/doc[@for="FileConfigurationStoreEx.ReadConfiguration"]/*'/>
    public virtual ConfigurationSection ReadConfiguration(string name_, string version_)
    {
      #region Logging
      if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
      {
          LogLayer.Debug.Location("FileConfigurationStore", "ReadConfiguration").Append(string.Format(Res.GetString(Res.LOG_STORE_READCONFIG), name_, version_)).Send();
      }
      #endregion Logging

      XmlNode userFile = GetUserFile(_settings, name_, version_);
      XmlNode[] systemFiles = GetFiles(_settings, name_, version_);

      return new ConfigurationSection(name_, version_, systemFiles, userFile);
    }

    /// <include file='xmldocs/FileConfigurationStoreEx.cs.xml' path='doc/doc[@for="FileConfigurationStoreEx.ReadConfigurations"]/*'/>
    public virtual ConfigurationSection[] ReadConfigurations(ConfigurationIdentityCollection configs_)
    {
      ConfigurationSection[] configSections = new ConfigurationSection[configs_.Count];

      int index = 0;
      foreach (ConfigurationIdentity config in configs_) 
      {
        configSections[index++] = this.ReadConfiguration(config.Name, config.Version);
      }

      return configSections;
    }
    #endregion Implementation of IConfigurationStorageReader

    #region Implementation of IConfigurationEnumeratesConfigs

    /// <include file='xmldocs/IConfigurationEnumeratesConfigs.cs.xml' path='doc/doc[@for="IConfigurationEnumeratesConfigs.EnumerateAvailableConfigs"]/*'/>
    public virtual ArrayList EnumerateAvailableConfigs(string pattern_)
    {
      //special case just return additonal folder list
      if (pattern_ == FOLDER_QUERY) 
      {
        return this._additionalFolders;
      }
      
      //discover which files matching pattern exist in our template folders plus additional folders
      ArrayList retVal = new ArrayList();
      
      string reg = _settings[Configurator.CONFIG_SETTING_REGION_KEY];
      string env = _settings[Configurator.CONFIG_SETTING_ENV_KEY];
      string role = _settings[Configurator.CONFIG_SETTING_ROLE_KEY];

      string dir = GetTemplateDirectory(null , null, null);
      if (dir != null)
      {
        string [] layouts = System.IO.Directory.GetFiles(dir, pattern_);
        if (layouts != null  && layouts.Length > 0)
        {
          for (int i = 0 ; i < layouts.Length ; i++)
          {
            int backslash = layouts[i].LastIndexOf(@"\");
            if (backslash > -1)
            {
              layouts[i] = layouts[i].Substring(backslash +1);
  
            }
            if(layouts[i].EndsWith(".config"))
              layouts[i] = layouts[i].Substring(0,layouts[i].Length - 7);
            retVal.Add(layouts[i]);
          }
        }
      }

      dir = GetTemplateDirectory(reg , null, null);
      if (dir != null)
      {
        string [] layouts = System.IO.Directory.GetFiles(dir, pattern_);
        if (layouts != null  && layouts.Length > 0)
        {
          for (int i = 0 ; i < layouts.Length ; i++)
          {
            
            int backslash = layouts[i].LastIndexOf(@"\");
            if (backslash > -1)
            {
              layouts[i] = layouts[i].Substring(backslash +1);
            }

            if(layouts[i].EndsWith(".config")) 
            {
              layouts[i] = layouts[i].Substring(0,layouts[i].Length - 7);
            }

            if (!retVal.Contains(layouts[i])) 
            {
              retVal.Add(layouts[i]);
            }
          }
        }
      }

      dir = GetTemplateDirectory(reg , env, role);
      if (dir != null)
      {
        string [] layouts = System.IO.Directory.GetFiles(dir, pattern_);
        if (layouts != null  && layouts.Length > 0)
        {
          for (int i = 0 ; i < layouts.Length ; i++)
          {
            int backslash = layouts[i].LastIndexOf(@"\");
            if (backslash > -1)
            {
              layouts[i] = layouts[i].Substring(backslash +1);
  
            }
            if(layouts[i].EndsWith(".config")) 
            {
              layouts[i] = layouts[i].Substring(0,layouts[i].Length - 7);
            }

            if (!retVal.Contains(layouts[i])) 
            {
              retVal.Add(layouts[i]);
            }
          }
        }
      }

      foreach (string folder in _additionalFolders)
      {
        if (folder != null)
        {
          string [] layouts = System.IO.Directory.GetFiles(folder, pattern_);
          if (layouts != null  && layouts.Length > 0)
          {
            for (int i = 0 ; i < layouts.Length ; i++)
            {
              int backslash = layouts[i].LastIndexOf(@"\");
              if (backslash > -1)
              {
                layouts[i] = layouts[i].Substring(backslash +1);
              }

              if(layouts[i].EndsWith(".config")) 
              {
                layouts[i] = layouts[i].Substring(0,layouts[i].Length - 7);
              }

              if (!retVal.Contains(layouts[i])) 
              {
                retVal.Add(layouts[i]);
              }
            }
          }
        }        
      }

      return retVal;
    }
    #endregion Implementation of IConfigurationEnumeratesConfigs

    #region Protected Methods
    protected virtual XmlElement GetDocumentFromFile(string file_)
    {
      XmlDocument doc = new XmlDocument();

      try
      {
        StreamReader sr = File.OpenText(file_);

        System.Xml.XmlTextReader textReader = new XmlTextReader(sr);
        MorganStanley.MSDotNet.MSXml.MSXmlProcessingReader reader = new MSXmlProcessingReader(textReader, _xmlProcessorCollection, _envVarParamResolver);

        doc.Load(reader);
        sr.Close();

        return doc.DocumentElement;
      }
      catch(Exception e_)
      {
        if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Warning))
        {
            LogLayer.Warning.Location("FileConfigurationStore", "GetDocumentFromFile")
                    .Append("Error reading file: " + file_)
                    .AppendException(e_)
                    .Send();
        }
      }

      // null implies that there was an error while getting the document.
      return null;
    }

    protected virtual XmlNode GetUserFile(NameValueCollection settings_, string name_, string version_)
    {
      string file = this.GetUserFilePath(settings_, name_, version_);

      #region Logging
      string logMessage = null;
      if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
      {
        logMessage = string.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file);
      }
      #endregion Logging

      if (File.Exists(file)) 
      {
        #region Logging
        if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
        {
          LogLayer.Debug.Location("FileConfigurationStore", "GetUserFile").Append(string.Format("{0}, Exists='True'", logMessage)).Send();
        }
        #endregion Logging

        return GetDocumentFromFile(file) ?? CreateEmptyConfig(name_);
      } 
      else
      {
        #region Logging
        if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
        {
            LogLayer.Debug.Location("FileConfigurationStore", "GetUserFile").Append(string.Format("{0}, Exists='False', Configuration was created", logMessage)).Send();
        }
        #endregion Logging

        return CreateEmptyConfig(name_);
      }
    }

    protected virtual string GetUserFilePath(NameValueCollection settings_, string name_, string version_)
    {
      string app = (settings_[Configurator.CONFIG_SETTING_LOCAL_APP_KEY] != null) ? settings_[Configurator.CONFIG_SETTING_LOCAL_APP_KEY] : settings_[Configurator.CONFIG_SETTING_APP_KEY];
      string user = settings_[Configurator.CONFIG_SETTING_USER_KEY];
      string reg = (settings_[Configurator.CONFIG_SETTING_LOCAL_REGION_KEY] != null) ? settings_[Configurator.CONFIG_SETTING_LOCAL_REGION_KEY] : settings_[Configurator.CONFIG_SETTING_REGION_KEY];
      string env = (settings_[Configurator.CONFIG_SETTING_LOCAL_ENV_KEY] != null) ? settings_[Configurator.CONFIG_SETTING_LOCAL_ENV_KEY] : settings_[Configurator.CONFIG_SETTING_ENV_KEY];
      string role = settings_[Configurator.CONFIG_SETTING_ROLE_KEY];
      
      string dir = GetUserDirectory(user, app, reg, env, role);

      if (name_ != null && _omitConfigExtension != null && _omitConfigExtension.Length > 0 && name_.EndsWith(_omitConfigExtension))
      {
        if (version_ != null) 
        {
          return string.Format(@"{0}\{1}.{2}", dir, name_, version_);
        } 
        else 
        {
          return string.Format(@"{0}\{1}", dir, name_);
        }
      }
      else
      {
        if (version_ != null) 
        {
          return string.Format(@"{0}\{1}.{2}.config", dir, name_, version_);
        } 
        else 
        {
          return string.Format(@"{0}\{1}.config", dir, name_);
        }
      }
    }

    protected virtual XmlNode[] GetFiles(NameValueCollection settings_, string name_, string version_)
    {
      ArrayList _configs = new ArrayList();

      string app = settings_[Configurator.CONFIG_SETTING_APP_KEY];
      string reg = settings_[Configurator.CONFIG_SETTING_REGION_KEY];
      string env = settings_[Configurator.CONFIG_SETTING_ENV_KEY];
      string role = settings_[Configurator.CONFIG_SETTING_ROLE_KEY];
      string dir = null, file = null;
      
      if (version_ != null) 
      {
        dir = GetTemplateDirectory(null, null, null);
        if (name_ != null && _omitConfigExtension != null && _omitConfigExtension.Length > 0 && name_.EndsWith(_omitConfigExtension))
        {
          file = string.Format(@"{0}\{1}.{2}", dir, name_, version_);
        }
        else
        {
          file = string.Format(@"{0}\{1}.{2}.config", dir, name_, version_);
        }

        if (File.Exists(file)) 
        {
          #region Logging
          if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
          {
            LogLayer.Debug.Location("FileConfigurationStore", "GetFiles").Append(string.Format("{0}, Exists='True'", string.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file))).Send();
          }
          #endregion Logging

          _configs.Add(GetDocumentFromFile(file));
        } 
        else 
        {
          #region Logging
          if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
          {
            LogLayer.Debug.Location("FileConfigurationStore", "GetFiles").Append(string.Format("{0}, Exists='False'", string.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file))).Send();
          }
          #endregion Logging
        }

        dir = GetTemplateDirectory(reg, null, null);

        if (name_ != null && _omitConfigExtension != null && _omitConfigExtension.Length > 0 && name_.EndsWith(_omitConfigExtension))
        {
          file = string.Format(@"{0}\{1}.{2}", dir, name_, version_);
        }
        else
        {
          file = string.Format(@"{0}\{1}.{2}.config", dir, name_, version_);
        }

        if (File.Exists(file)) 
        {
          #region Logging
          if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
          {
            LogLayer.Debug.Location("FileConfigurationStore", "GetFiles").Append(string.Format("{0}, Exists='True'", string.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file))).Send();
          }
          #endregion Logging

          _configs.Add(GetDocumentFromFile(file));
        }
        else 
        {
          #region Logging
          if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
          {
            LogLayer.Debug.Location("FileConfigurationStore", "GetFiles").Append(string.Format("{0}, Exists='False'", string.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file))).Send();
          }
          #endregion Logging
        }

        dir = GetTemplateDirectory(reg, env, null);
        if (name_ != null && _omitConfigExtension != null && _omitConfigExtension.Length > 0 && name_.EndsWith(_omitConfigExtension))
        {
          file = string.Format(@"{0}\{1}.{2}", dir, name_, version_);
        }
        else
        {
          file = string.Format(@"{0}\{1}.{2}.config", dir, name_, version_);
        }

        if (File.Exists(file)) 
        {
          #region Logging
          if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
          {
            LogLayer.Debug.Location("FileConfigurationStore", "GetFiles").Append(string.Format("{0}, Exists='True'", string.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file))).Send();
          }
          #endregion Logging

          _configs.Add(GetDocumentFromFile(file));
        }
        else 
        {
          #region Logging
          if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
          {
            LogLayer.Debug.Location("FileConfigurationStore", "GetFiles").Append(string.Format("{0}, Exists='False'", string.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file))).Send();
          }
          #endregion Logging
        }
				
        if (role != null)
        {
          dir = GetTemplateDirectory(reg, env, role);
          if (name_ != null && _omitConfigExtension != null && _omitConfigExtension.Length > 0 && name_.EndsWith(_omitConfigExtension))
          {
            file = string.Format(@"{0}\{1}.{2}", dir, name_, version_);
          }
          else
          {
            file = string.Format(@"{0}\{1}.{2}.config", dir, name_, version_);
          }

          if (File.Exists(file)) 
          {
            #region Logging
            if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
            {
              LogLayer.Debug.Location("FileConfigurationStore", "GetFiles").Append(string.Format("{0}, Exists='True'", string.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file))).Send();
            }
            #endregion Logging

            _configs.Add(GetDocumentFromFile(file));
          }
          else 
          {
            #region Logging
            if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
            {
              LogLayer.Debug.Location("FileConfigurationStore", "GetFiles").Append(string.Format("{0}, Exists='False'", string.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file))).Send();
            }
            #endregion Logging
          }				
        }

        //load additional folders
        foreach (string folderDir in _additionalFolders)
        {
          if (name_ != null && _omitConfigExtension != null && _omitConfigExtension.Length > 0 && name_.EndsWith(_omitConfigExtension))
          {
            file = string.Format(@"{0}\{1}.{2}", folderDir, name_, version_);
          }
          else
          {
            file = string.Format(@"{0}\{1}.{2}.config", folderDir, name_, version_);
          }

          if (File.Exists(file)) 
          {
            #region Logging
            if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
            {
              LogLayer.Debug.Location("FileConfigurationStoreEx", "GetFiles").Append(string.Format("{0}, Exists='True'", string.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file))).Send();
            }
            #endregion Logging

            _configs.Add(GetDocumentFromFile(file));
          }
          else 
          {
            #region Logging
            if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
            {
              LogLayer.Debug.Location("FileConfigurationStoreEx", "GetFiles").Append(string.Format("{0}, Exists='False'", string.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file))).Send();
            }
            #endregion Logging
          }
        }
      } 
      else 
      {
        dir = GetTemplateDirectory(null, null, null);
        if (name_ != null && _omitConfigExtension != null && _omitConfigExtension.Length > 0 && name_.EndsWith(_omitConfigExtension))
        {
          file = string.Format(@"{0}\{1}", dir, name_, version_);
        }
        else
        {
          file = string.Format(@"{0}\{1}.config", dir, name_);
        }

        if (File.Exists(file)) 
        {
          #region Logging
          if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
          {
            LogLayer.Debug.Location("FileConfigurationStore", "GetFiles").Append(string.Format("{0}, Exists='True'", string.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file))).Send();
          }
          #endregion Logging

          _configs.Add(GetDocumentFromFile(file));
        }
        else 
        {
          #region Logging
          if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
          {
            LogLayer.Debug.Location("FileConfigurationStore", "GetFiles").Append(string.Format("{0}, Exists='False'", string.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file))).Send();
          }
          #endregion Logging
        }

        dir = GetTemplateDirectory(reg, null, null);
        if (name_ != null && _omitConfigExtension != null && _omitConfigExtension.Length > 0 && name_.EndsWith(_omitConfigExtension))
        {
          file = string.Format(@"{0}\{1}", dir, name_, version_);
        }
        else
        {
          file = string.Format(@"{0}\{1}.config", dir, name_);
        }

        if (File.Exists(file)) 
        {
          #region Logging
          if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
          {
            LogLayer.Debug.Location("FileConfigurationStore", "GetFiles").Append(string.Format("{0}, Exists='True'", string.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file))).Send();
          }
          #endregion Logging

          _configs.Add(GetDocumentFromFile(file));
        }
        else 
        {
          #region Logging
          if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
          {
            LogLayer.Debug.Location("FileConfigurationStore", "GetFiles").Append(string.Format("{0}, Exists='False'", string.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file))).Send();
          }
          #endregion Logging
        }

        dir = GetTemplateDirectory(reg, env, null);
        if (name_ != null && _omitConfigExtension != null && _omitConfigExtension.Length > 0 && name_.EndsWith(_omitConfigExtension))
        {
          file = string.Format(@"{0}\{1}", dir, name_, version_);
        }
        else
        {
          file = string.Format(@"{0}\{1}.config", dir, name_);
        }

        if (File.Exists(file)) 
        {
          #region Logging
          if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
          {
            LogLayer.Debug.Location("FileConfigurationStore", "GetFiles").Append(string.Format("{0}, Exists='True'", string.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file))).Send();
          }
          #endregion Logging

          _configs.Add(GetDocumentFromFile(file));
        }
        else 
        {
          #region Logging
          if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
          {
            LogLayer.Debug.Location("FileConfigurationStore", "GetFiles").Append(string.Format("{0}, Exists='False'", string.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file))).Send();
          }
          #endregion Logging
        }

        if (role != null)
        {
          dir = GetTemplateDirectory(reg, env, role);
          if (name_ != null && _omitConfigExtension != null && _omitConfigExtension.Length > 0 && name_.EndsWith(_omitConfigExtension))
          {
            file = string.Format(@"{0}\{1}", dir, name_, version_);
          }
          else
          {
            file = string.Format(@"{0}\{1}.config", dir, name_);
          }

          if (File.Exists(file)) 
          {
            #region Logging
            if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
            {
              LogLayer.Debug.Location("FileConfigurationStore", "GetFiles").Append(string.Format("{0}, Exists='True'", string.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file))).Send();
            }
            #endregion Logging

            _configs.Add(GetDocumentFromFile(file));
          }
          else 
          {
            #region Logging
            if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
            {
              LogLayer.Debug.Location("FileConfigurationStore", "GetFiles").Append(string.Format("{0}, Exists='False'", string.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file))).Send();
            }
            #endregion Logging
          }
        }

        //load additional folders
        foreach (string folderDir in _additionalFolders)
        {
          if (name_ != null && _omitConfigExtension != null && _omitConfigExtension.Length > 0 && name_.EndsWith(_omitConfigExtension))
          {
            file = string.Format(@"{0}\{1}", folderDir, name_, version_);
          }
          else
          {
            file = string.Format(@"{0}\{1}.config", folderDir, name_);
          }				
          
          if (File.Exists(file)) 
          {
            #region Logging
            if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
            {
              LogLayer.Debug.Location("FileConfigurationStoreEx", "GetFiles").Append(string.Format("{0}, Exists='True'", string.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file))).Send();
            }
            #endregion Logging

            _configs.Add(GetDocumentFromFile(file));
          }
          else 
          {
            #region Logging
            if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
            {
                LogLayer.Debug.Location("FileConfigurationStoreEx", "GetFiles").Append(string.Format("{0}, Exists='False'", string.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file))).Send();
            }
            #endregion Logging
          }
        }
      }

      if (_configs.Count > 0) 
      {
        return (XmlNode[]) _configs.ToArray(typeof(XmlNode));
      } 
      else 
      {
        #region Logging
        if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
        {
            LogLayer.Debug.Location("FileConfigurationStore", "GetFiles").Append(Res.GetString(Res.LOG_FILESTORE_NOFILES_FOUND)).Send();
        }
        #endregion Logging

        return null;
      }
    }

    protected virtual string GetUserDirectory(string username_, string app_, string reg_, string env_, string role_)
    {
      string dir = String.Empty;
			
      if (role_ != null)
      {
        dir = string.Format(@"{0}\Morgan Stanley\{1}\Configuration\{2}\{3}\{4}", _userBaseDir, app_, reg_, env_, role_);
      }
      else
      {
        dir = string.Format(@"{0}\Morgan Stanley\{1}\Configuration\{2}\{3}", _userBaseDir, app_, reg_, env_);			
      }

      if (!System.IO.Directory.Exists(dir))
      {
        System.IO.Directory.CreateDirectory(dir);
      }

      return dir;
    }

    protected virtual string GetTemplateDirectory(string reg_, string env_, string role_)
    {
      string dir = null;

      if (reg_ == null && env_ == null && role_ == null) 
      {
        dir = string.Format(@"{0}\Configuration", _systemBaseDir);
      }
      else if (reg_ != null && env_ == null && role_ == null) 
      {
        dir = string.Format(@"{0}\Configuration\{1}", _systemBaseDir, reg_);
      } 
      else if (reg_ != null && env_ != null && role_ == null)
      {
        dir = string.Format(@"{0}\Configuration\{1}\{2}", _systemBaseDir, reg_, env_);
      }
      else
      {
        dir = string.Format(@"{0}\Configuration\{1}\{2}\{3}", _systemBaseDir, reg_, env_, role_);
      }
			if (!Directory.Exists(dir))
				dir = null;
      return dir;
    }

    protected virtual NameValueCollection ParseProfileString(string profileString_) 
    {
      NameValueCollection values = new NameValueCollection();

      string[] settings = profileString_.Split(';');

      for (int index = 0; index < settings.Length; index++) 
      {
        string[] namevalue = settings[index].Split('=');
        if (namevalue.Length == 2) 
        {
          values.Add(namevalue[0].Trim(), namevalue[1].Trim());
        } 
        else 
        {
          #region Logging
          if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Error)) 
          {
            LogLayer.Error.Location("FileConfigurationStore", "ParseProfileString").Append(string.Format(Res.GetString(Res.EXCEPTION_INVALID_PROFILESTRING), profileString_)).Send();
          }
          #endregion Logging

          throw new ConfiguratorException(string.Format(Res.GetString(Res.EXCEPTION_INVALID_PROFILESTRING), profileString_));
        }
      }

      if (values.Count == 0) 
      {
        #region Logging
        if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Error)) 
        {
            LogLayer.Error.Location("FileConfigurationStore", "ParseProfileString").Append(string.Format(Res.GetString(Res.EXCEPTION_INVALID_PROFILESTRING), profileString_)).Send();
        }
        #endregion Logging

        throw new ConfiguratorException(Res.GetString(Res.EXCEPTION_INVALID_PROFILESTRING));
      }
      
      return values;      
    }

    protected virtual void ParseFolderString(string folderString_) 
    {
      _additionalFolders = new ArrayList();

      if (folderString_ != null && folderString_.Length > 0)
      {
        string[] settings = folderString_.Split(';');

        for (int index = 0; index < settings.Length; index++) 
        {
          _additionalFolders.Add(settings[index]);
        }
      }
    }

    protected virtual string ParseDirectoryString(string data_)
    {
      //code allows for exactly 1 environment variable to be specified in the string
      //of form %ETSHOME%
      if (data_.IndexOf('%') > -1)
      {
        int nFirst = data_.IndexOf('%');
        int nLast = data_.LastIndexOf('%');

        if (nLast > nFirst)
        {
          string sLeft = "";
          if (nFirst > 0)
          {
            sLeft = data_.Substring(0,nFirst);
          }

          string sRight = "";
          if (nLast != data_.Length -1)
          {
            sRight = data_.Substring(nLast + 1);
          }

          string sMid = data_.Substring(nFirst +1, nLast - nFirst -1);
          //now swap it for environment variable
          string sEnv = "";
          try 
          {
            sEnv = System.Environment.GetEnvironmentVariable(sMid);
          }
          catch (Exception e_)
          {
            //user probably didn't have permission to read this variable
            #region Logging
            if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Error)) 
            {
              LogLayer.Error.Location("FileConfigurationStoreEx", "ParseDirectoryString").Append(string.Format("Error reading environment variable: {0}", e_, data_)).Send();
            }
            #endregion Logging 
            return null;
          }
          if (sEnv != null && sEnv.Length > 0)
          {
            return (sLeft + sEnv + sRight);
          }
        }

        #region Logging
        if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Error)) 
        {
            LogLayer.Error.Location("FileConfigurationStoreEx", "ParseDirectoryString").Append(string.Format("Directory string was of invalid format: {0}", data_)).Send();
        }
        #endregion Logging

        return null;
      }
      else
      {
        return data_;
      }
    }

    protected virtual void SetNamespacesForXInclude(XmlNode node_)
    {
      XmlNodeList nodes = node_.SelectNodes("NameSpace");
      if (nodes != null)
      {
        foreach(XmlElement element in nodes)
        {
          NamespacesForXInclude.Add(element.GetAttribute("prefix"), element.InnerXml);
        }
      }
    }

    protected virtual void SetXIncludeFolders(XmlNode node_)
    {
      try
      {
        XmlNodeList nodes = node_.SelectNodes("Folder");
        if (nodes != null)
        {
          foreach(XmlElement element in nodes)
          {
            string name = element.GetAttribute("name");
            string path = element.GetAttribute("path");

            if (!_envVarParamResolver.Hashtable.Contains(name))
            {
              if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Info))
              {
                LogLayer.Info.Location("FileConfigurationStore", "SetXIncludeFolders").Append("Adding folder for XInclude: " + name + ", path: " + path);
              }

              _envVarParamResolver.Hashtable.Add(name, path);
            }
          }
        }
      }
      catch(Exception ex_)
      {
        #region Logging
        if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Error)) 
        {
          LogLayer.Error.Location("FileConfigurationStore", "SetXIncludeFolders").Append(ex_.ToString());
        }
        #endregion Logging
      }
    }

    // read etscfg settinsg from app.exe.config
    // this allows tighter integration with etscfg
    // example config
    //
    //				<etscfg>
    //         <project name="euuimm" defaultLink="prod" >
    //          <profile app="Derivatives Desktop" region="ln" env="LNTestOMW1" link="qa" />
    //         <project name="euuilm" defaultLink="prod" />
    //        </etscfg>
    protected virtual void GenerateEtscfgSettings(XmlNode node_)
    {
      try
      {
        XmlNodeList nodes = node_.SelectNodes("project");
        if (nodes != null)
        {
          foreach(XmlElement element in nodes)
          {
            string projectName = element.GetAttribute("name");
            string defaultLink = element.GetAttribute("defaultLink");
            
            string etscfgGlobalString = String.Empty;            
            
            if (element.HasAttribute("isGlobal"))
            {
              if (element.GetAttribute("isGlobal").ToLower() == "true")
              {
                etscfgGlobalString = _settings[Configurator.CONFIG_SETTING_REGION_KEY];
              }
            }                       
            
            string uiArea = String.Empty;

            // Get the profile's settings which we'll use to search for a matching              
            // etscfg releaselink override
            NameValueCollection searchCriteria = new NameValueCollection(ETSCFG_PROFILE_ATTRIBUTES.Length);
            if (_settings[Configurator.CONFIG_SETTING_ETSCFG_KEY] != null)
            {
              searchCriteria.Add(Configurator.CONFIG_SETTING_ETSCFG_KEY, _settings[Configurator.CONFIG_SETTING_ETSCFG_KEY]);
            }
            else
            {
              foreach (string attribute in ETSCFG_PROFILE_ATTRIBUTES)
              {
                if (_settings[attribute] != null)
                { 
                  searchCriteria.Add(attribute, _settings[attribute]);
                }
              } 
            }

            // try to match an override
            XmlNodeList profilesList = element.SelectNodes("profile");
            if (profilesList != null)
            {
              foreach(XmlElement profile in profilesList)
              {
                  if (!(from string attributeName in searchCriteria.Keys
                        let profileValue = profile.GetAttribute(attributeName)
                        where profileValue == null || profileValue != searchCriteria[attributeName]
                        select attributeName).Any())
                  {
                      defaultLink = profile.GetAttribute("link");

                      if (profile.HasAttribute("uiArea"))
                      {
                          uiArea = @"\" + profile.GetAttribute("uiArea");
                      }
                      break;
                  }
              }
            }
              
            if (uiArea.Equals(String.Empty))
            {
              uiArea = @"\ui_" + defaultLink;
              if (element.HasAttribute("uiArea"))
              {
                uiArea = @"\" + element.GetAttribute("uiArea");
              }
            }

            // build project path in the form of
            // \\san01b\DevAppsGML\dist\etscfg\PROJ\<project>\<link>.current\common\<link>\ui_<link>
            // this is all hardcoded for now, maybe make this configurable in the future
            // 
            // to deal with global projects, prefix projectName with region if isGlobal=true in config
            //
            string projectPath = @"\\san01b\DevAppsGML\dist\etscfg\PROJ\" + etscfgGlobalString + projectName + @"\" + defaultLink
              + @".current\common\" + defaultLink + uiArea;

            // add to list of environment variables
            string key = "etscfg" + projectName;
            if (!_envVarParamResolver.Hashtable.Contains(key))
            {
              if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Info))
              {
                LogLayer.Info.Location("FileConfigurationStore", "GenerateEtscfgSettings").Append("Adding etscfg key: " + key + ", path: " + projectPath);
              }

              _envVarParamResolver.Hashtable.Add(key, projectPath);
            }
          }
        }
      }
      catch(Exception ex_)
      {
        #region Logging
        if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Error)) 
        {
          LogLayer.Error.Location("FileConfigurationStore", "GenerateEtscfgSettings").Append(ex_.ToString());
        }
        #endregion Logging
      }
    }
    #endregion Protected Methods

    #region Private Methods

    private XmlNode CreateEmptyConfig(string name_)
    {
      XmlDocument doc = new XmlDocument();
      doc.LoadXml(string.Format("<{0} />", name_));
      return doc.DocumentElement;
    }

    #endregion

  }
}
