#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.Configuration/IConfigurationEnumeratesConfigs.cs#1 $
  $DateTime: 2013/04/11 16:41:48 $
    $Change: 823942 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion

using System;
using System.Collections;
using System.Collections.Specialized;

namespace MorganStanley.Desktop.Configuration
{
  /// <include file='xmldocs/IConfigurationStoreExtended.cs.xml' path='doc/doc[@for="IConfigurationStoreExtended"]/*'/>
  public interface IConfigurationEnumeratesConfigs
  {
    #region Properties
    #endregion Properties

    #region Methods
    /// <include file='xmldocs/IConfigurationStoreExtended.cs.xml' path='doc/doc[@for="IConfigurationStoreExtended.EnumerateAvailableConfigs"]/*'/>
    ArrayList EnumerateAvailableConfigs(string pattern_);

    #endregion Methods
  }
}