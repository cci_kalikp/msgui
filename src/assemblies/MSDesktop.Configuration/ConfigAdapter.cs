﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Xml;
using MorganStanley.Desktop.Configuration;
using MorganStanley.MSDotNet.MSXml;
using MorganStanley.MSDotNet.My;

namespace MSDesktop.Configuration
{
    public class ModuleConfigAdapter : IConfigurationInterceptingReader, IConfigurationStorageWriter, IConfigurationEnumeratesConfigs
    {
        private static readonly IMSLogger Logger = MSLoggerFactory.CreateLogger<ModuleConfigAdapter>();
        private IConfigurationStorageReader _decoratedReader;
        private IConfigurationStorageWriter _decoratedWriter;
        private IConfigurationEnumeratesConfigs _decoratedEnumeratesConfigs;
        private readonly IDictionary<string, string> _assemblies = new ConcurrentDictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);

        public ConfigurationSection InterceptRead(ConfigurationSection original_)
        {
            string assemblyValue;
            if (_assemblies.TryGetValue(original_.Name, out assemblyValue))
            {
                try
                {
                    var document = new XmlDocument();
                    using (var sr = File.OpenText(assemblyValue))
                    using (var textReader = new XmlTextReader(sr))
                    using (var reader = new MSXmlProcessingReader(textReader, new MSXmlProcessorFactoryCollection()))
                    {

                        document.Load(reader);
                        sr.Close();
                    }

                    XmlNode[] nodes = new XmlNode[original_.ReadOnlyNodes.Length + 1];
                    original_.ReadOnlyNodes.CopyTo(nodes, 0);
                    nodes[nodes.Length - 1] = document.DocumentElement;
                    var section = new ConfigurationSection(original_.Name, original_.Version,
                                                           nodes,
                                                           original_.ReadWriteNode);
                    return section;
                }
                catch (Exception)
                {
                    // We do not modify section here
                }
            }

            return original_;
        }

        public void WriteConfiguration(string name_, string version_, XmlNode node_)
        {
            _decoratedWriter.WriteConfiguration(name_, version_, node_);
        }

        public void Initialize(string settings_)
        {
            throw new ConfiguratorException(string.Format("ConfigAdapter must have a valid configuration node specifying the type of the decorated storage provider."));
        }

        public void Initialize(string settings_, XmlNode config_)
        {
            IList<string> extensionlist = new List<string>();
            extensionlist.Add("concord");
            XmlNode decoratedConfig = config_.SelectSingleNode("./DecoratedStorageProvider");
            if ((decoratedConfig == null) || (decoratedConfig.Attributes == null) || (decoratedConfig.Attributes["type"] == null))
                throw new ConfiguratorException(string.Format("ConfigAdapter must have a valid configuration node specifying the type of the decorated storage provider."));

            string type = decoratedConfig.Attributes["type"].Value;

            if (string.IsNullOrEmpty(type))
                throw new ConfiguratorException(string.Format("ConfigAdapter must have a valid configuration node specifying the type of the decorated storage provider."));

            Type decoratedType = Type.GetType(type);

            if (decoratedType == null)
                throw new ConfiguratorException(string.Format("ConfigAdapter must have a valid configuration node specifying the type of the decorated storage provider. Type does not exist."));

            Logger.DebugWithFormat("Create instance of decorated storage provider '{0}'", decoratedType);

            object decorated = Activator.CreateInstance(decoratedType);

            _decoratedReader = decorated as IConfigurationStorageReader;
            _decoratedWriter = decorated as IConfigurationStorageWriter;
            _decoratedEnumeratesConfigs = decorated as IConfigurationEnumeratesConfigs;

            if (_decoratedReader != null)
            {
                _decoratedReader.Initialize(settings_, decoratedConfig);
            }

            var nodes = config_.SelectNodes("./Extensions/Extension");
            if (nodes != null)
                foreach (XmlNode node in nodes)
                {
                    var innerText = node.InnerText;
                    if (!string.IsNullOrEmpty(innerText) && !extensionlist.Contains(innerText))
                    {
                        extensionlist.Add(innerText);
                    }
                }

            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                CollectConfigsFromAssembly(assembly, extensionlist);

            }
            AppDomain.CurrentDomain.AssemblyLoad +=
                (sender, args) => CollectConfigsFromAssembly(args.LoadedAssembly, extensionlist);
        }

        private void CollectConfigsFromAssembly(Assembly assembly, IEnumerable<string> extensionlist)
        {
            if (assembly.IsDynamic) 
                return;

            foreach (var extension in extensionlist)
            {
                var assemblyPath = assembly.Location + "." + extension + ".config";
                if (File.Exists(assemblyPath))
                {
                    _assemblies.Add(assembly.GetName().Name, assemblyPath);
                }
            }
        }

        public ConfigurationSection ReadConfiguration(string name_, string version_)
        {
            var section = _decoratedReader.ReadConfiguration(name_, version_);
            if (section.ReadWriteNode != null)
            {
                var selectSingleNode = section.ReadWriteNode.SelectSingleNode(".");

                if ((section.ReadOnlyNodes == null) && (section.ReadWriteNode != null) &&
                    (selectSingleNode != null) && (selectSingleNode.OuterXml == string.Format("<{0} />", name_)))
                {
                    string assemblyValue;
                    if (_assemblies.TryGetValue(name_, out assemblyValue))
                    {
                        try
                        {
                            XmlDocument document = new XmlDocument();
                            document.Load(assemblyValue);
                            section = new ConfigurationSection(name_, version_, new XmlNode[] {document.DocumentElement},
                                                               section.ReadWriteNode);
                        }
                        catch (Exception)
                        {
                            // We do not modify section here
                        }
                    }
                }
            }
            return section;
        }

        public ConfigurationSection[] ReadConfigurations(ConfigurationIdentityCollection configEntries_)
        {
            var result = new ConfigurationSection[configEntries_.Count];
            for (int index = 0; index < configEntries_.Count; index++)
            {
                var identity = configEntries_[index];
                result[index] = ReadConfiguration(identity.Name, identity.Version);
            }
            return result;
        }

        public ArrayList EnumerateAvailableConfigs(string pattern_)
        {
            Regex regex = new Regex(pattern_);
            var list = _decoratedEnumeratesConfigs.EnumerateAvailableConfigs(pattern_);
            foreach (var assembly in _assemblies.Keys)
            {
                if (regex.IsMatch(assembly))
                    list.Add(assembly);
            }
            return list;
        }
    }
}
