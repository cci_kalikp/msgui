#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.Configuration/LogLayer.cs#1 $
  $DateTime: 2013/04/11 16:41:48 $
    $Change: 823942 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion

using System;
using System.Reflection;
using MorganStanley.MSDotNet.MSLog;


namespace MorganStanley.Desktop.Configuration
{
    internal class LogLayer
    {
        #region Constants
        public static readonly string META = "MSDesktop";
        public static readonly string PROJECT = "Configuration";
        public static readonly string RELEASE = Assembly.GetExecutingAssembly().GetName().Version.ToString();
        #endregion Constants

        #region Public Methods
        public static MSLogMessage Emergency
        {
            get { return MSLog.Emergency().SetLayer(META, PROJECT, RELEASE); }
        }

        public static MSLogMessage Alert
        {
            get { return MSLog.Alert().SetLayer(META, PROJECT, RELEASE); }
        }

        public static MSLogMessage Critical
        {
            get { return MSLog.Critical().SetLayer(META, PROJECT, RELEASE); }
        }

        public static MSLogMessage Error
        {
            get { return MSLog.Error().SetLayer(META, PROJECT, RELEASE); }
        }

        public static MSLogMessage Warning
        {
            get { return MSLog.Warning().SetLayer(META, PROJECT, RELEASE); }
        }

        public static MSLogMessage Notice
        {
            get { return MSLog.Notice().SetLayer(META, PROJECT, RELEASE); }
        }

        public static MSLogMessage Info
        {
            get { return MSLog.Info().SetLayer(META, PROJECT, RELEASE); }
        }

        public static MSLogMessage Debug
        {
            get { return MSLog.Debug().SetLayer(META, PROJECT, RELEASE); }
        }
        #endregion Public Methods
    }
}
