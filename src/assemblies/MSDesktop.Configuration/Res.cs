#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.Configuration/Res.cs#1 $
  $DateTime: 2013/04/11 16:41:48 $
    $Change: 823942 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion

using System;
using System.Globalization;
using System.Resources;

namespace MorganStanley.Desktop.Configuration
{
  /// <summary>
  /// Internal class for retrieving culture specific resources
  /// </summary>
  sealed class Res
  {
    #region Declarations
    private ResourceManager _resources = null;
    private static Res _loader = null;
    private static object _loaderLock = new object();
    #endregion Declarations

    #region Constructors
    private Res()
    {
        this._resources = new ResourceManager("MSDesktop.Configuration.Res", this.GetType().Module.Assembly);
    }
    #endregion Constructors

    #region Resource Constants
    // Constant values point to names within the resource files
    public const string EXCEPTION_INVALID_PROFILESTRING = "EXCEPTION_INVALID_PROFILESTRING";
    public const string EXCEPTION_NEED_CUSTOMPROFILE_TO_SAVE = "EXCEPTION_NEED_CUSTOMPROFILE_TO_SAVE";
    public const string EXCEPTION_NEED_CUSTOMPROFILE_TO_EDIT = "EXCEPTION_NEED_CUSTOMPROFILE_TO_EDIT";
    public const string EXCEPTION_UNABLE_TO_LOAD_CONFIG = "EXCEPTION_UNABLE_TO_LOAD_CONFIG";
    public const string EXCEPTION_NON_MATCHING_NODETYPE = "EXCEPTION_NON_MATCHING_NODETYPE";
    public const string EXCEPTION_MUST_INITIALIZE = "EXCEPTION_MUST_INITIALIZE";
    public const string EXCEPTION_NODE_DOES_NOT_EXIST = "EXCEPTION_NODE_DOES_NOT_EXIST";
    public const string EXCEPTION_CONFIG_IDENTITY_ALREADY_EXISTS = "EXCEPTION_CONFIG_IDENTITY_ALREADY_EXISTS";
    public const string EXCEPTION_MUST_IMPLEMENT_INTERFACE = "EXCEPTION_MUST_IMPLEMENT_INTERFACE";
    public const string EXCEPTION_UNABLE_TO_CREATE_INSTANCE = "EXCEPTION_UNABLE_TO_CREATE_INSTANCE";
    public const string EXCEPTION_CONFIG_IS_READONLY = "EXCEPTION_CONFIG_IS_READONLY";
    public const string EXCEPTION_ERROR_INITIALIZING_STORAGE_READER = "EXCEPTION_ERROR_INITIALIZING_STORAGE_READER";
    public const string EXCEPTION_CONFIG_DOES_NOT_EXIST = "EXCEPTION_CONFIG_DOES_NOT_EXIST";
    public const string EXCEPTION_WRITE_CONFIG = "EXCEPTION_WRITE_CONFIG";
    public const string EXCEPTION_DEFAULT_PROVIDER = "EXCEPTION_DEFAULT_PROVIDER";
    public const string EXCEPTION_ERROR_INITIALIZING_INTERCEPTORS = "EXCEPTION_ERROR_INITIALIZING_INTERCEPTORS";
    public const string EXCEPTION_ERROR_INTERCEPTING_READ = "EXCEPTION_ERROR_INTERCEPTING_READ";
    public const string EXCEPTION_ERROR_INTERCEPTING_WRITE = "EXCEPTION_ERROR_INTERCEPTING_WRITE";

    public const string LOG_CONFIGMGR_INITIALIZE = "LOG_CONFIGMGR_INITIALIZE";
    public const string LOG_CONFIGMGR_RESET= "LOG_CONFIGMGR_RESET";
    public const string LOG_CLEAR_CONFIG_CACHE= "LOG_CLEAR_CONFIG_CACHE";
    public const string LOG_PROVIDER_REQUEST = "LOG_PROVIDER_REQUEST";
    public const string LOG_PROVIDER_CREATEINSTANCE = "LOG_PROVIDER_CREATEINSTANCE";
    public const string LOG_PROVIDER_INITIALIZE = "LOG_PROVIDER_INITIALIZE";
    public const string LOG_PROVIDER_RETRIEVE_CONFIGS_BATCH = "LOG_PROVIDER_RETRIEVE_CONFIGS_BATCH";
    public const string LOG_PROVIDER_RETRIEVE_CONFIG = "LOG_PROVIDER_RETRIEVE_CONFIG";
    public const string LOG_CONFIG_HANDLER_TYPE = "LOG_CONFIG_HANDLER_TYPE";
    public const string LOG_ADD_CONFIG_TO_CACHE = "LOG_ADD_CONFIG_TO_CACHE";
    public const string LOG_GETCONFIG = "LOG_GETCONFIG";
    public const string LOG_GETCONFIG_CACHE_MISS = "LOG_GETCONFIG_CACHE_MISS";
    public const string LOG_GETCONFIG_CACHE_HIT = "LOG_GETCONFIG_CACHE_HIT";
    public const string LOG_CONFIG_NOT_FOUND = "LOG_CONFIG_NOT_FOUND";
    public const string LOG_WRITE_CONFIG = "LOG_WRITE_CONFIG";
    public const string LOG_CONFIG_DOES_NOT_EXIST = "LOG_CONFIG_DOES_NOT_EXIST";
    public const string LOG_CREATE_INSTANCE = "LOG_CREATE_INSTANCE";
    public const string LOG_DELETE_DOCUMENT_ELEMENT = "LOG_DELETE_DOCUMENT_ELEMENT";
    public const string LOG_CONFIGURATOR_GETVALUE = "LOG_CONFIGURATOR_GETVALUE";
    public const string LOG_CONFIGURATOR_GETNODE = "LOG_CONFIGURATOR_GETNODE";
    public const string LOG_CONFIGURATOR_SETVALUE = "LOG_CONFIGURATOR_SETVALUE";
    public const string LOG_CONFIGURATOR_DELETENODE = "LOG_CONFIGURATOR_DELETENODE";
    public const string LOG_CONFIGURATOR_SETNODE = "LOG_CONFIGURATOR_SETNODE";
    public const string LOG_CONFIGURATOR_ADDNODE = "LOG_CONFIGURATOR_ADDNODE";
    public const string LOG_CONFIGURATOR_SAVE = "LOG_CONFIGURATOR_SAVE";
    public const string LOG_FILESTORE_INITIALIZE = "LOG_FILESTORE_INITIALIZE";
    public const string LOG_STORE_READCONFIG = "LOG_STORE_READCONFIG";
    public const string LOG_FILESTORE_GETFILE = "LOG_FILESTORE_GETFILE";
    public const string LOG_FILESTORE_NOFILES_FOUND = "LOG_FILESTORE_NOFILES_FOUND";
    public const string LOG_STORE_WRITECONFIG = "LOG_STORE_WRITECONFIG";
    public const string LOG_ERROR_SAVING_CONFIG = "LOG_ERROR_SAVING_CONFIG";
    public const string LOG_ENUMERATE_CONFIGS = "LOG_ENUMERATE_CONFIGS";
    public const string LOG_CONFIG_SPECIFIC_PROVIDER = "LOG_CONFIG_SPECIFIC_PROVIDER";
    public const string LOG_INTERCEPTINGSTORE_INTERCEPTOR_CREATEINSTANCE = "LOG_INTERCEPTINGSTORE_INTERCEPTOR_CREATEINSTANCE";
    public const string LOG_INTERCEPTINGSTORE_DECORATED_CREATEINSTANCE = "LOG_INTERCEPTINGSTORE_DECORATED_CREATEINSTANCE";
    public const string LOG_INTERCEPTINGSTORE_INTERCEPTOR_READ = "LOG_INTERCEPTINGSTORE_INTERCEPTOR_READ";
    public const string LOG_INTERCEPTINGSTORE_INTERCEPTOR_WRITE = "LOG_INTERCEPTINGSTORE_INTERCEPTOR_WRITE";
    #endregion Resource Constants

    #region Public Methods
    /// <summary>
    /// Gets the value of the specified <see cref="System.String">String</see> resource
    /// for the current culture.
    /// </summary>
    /// <param name="name_">The name of the resource to get.</param>
    /// <returns>
    /// The value of the resource localized for the caller's current culture settings.
    /// If a match is not possible, a null reference.
    /// </returns>
    public static string GetString(string name_) 
    {
      return GetString(null, name_);
    }

    /// <summary>
    /// Gets the value of the specified <see cref="System.String">String</see> resource
    /// for the specified culture.
    /// </summary>
    /// <param name="culture_">
    /// The <see cref="System.Globalization.CultureInfo">CultureInfo</see> object that
    /// represents the culture for which the resource is localized. Note that if the resource
    /// is not localized for this culture, the lookup will fall back using the culture's Parent
    /// property, stopping after looking in the neutral culture.
    /// </param>
    /// <param name="name_">The name of the resource to get.</param>
    /// <returns>
    /// The value of the resource localized for the caller's current culture settings.
    /// If a match is not possible, a null reference.
    /// </returns>
    public static string GetString(CultureInfo culture_, string name_) 
    {
      Res res = Res.GetLoader();

      if (res == null) 
      {
        return null;
      }

      return res._resources.GetString(name_, culture_);
    }
    #endregion Public Methods

    #region Private Methods
    private static Res GetLoader() 
    {
      lock(_loaderLock) 
      {
        if (Res._loader == null) 
        {
          Res._loader = new Res();
        }
      }

      return Res._loader;
    }
    #endregion Private Methods
  }
}
