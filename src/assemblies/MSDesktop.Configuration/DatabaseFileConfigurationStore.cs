#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.Configuration/DatabaseFileConfigurationStore.cs#1 $
  $DateTime: 2013/04/11 16:41:48 $
    $Change: 823942 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion

using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Xml;

using MorganStanley.MSDotNet.MSNet;
using MorganStanley.MSDotNet.MSSoap;
using MorganStanley.MSDotNet.MSSoap.Common;
using MorganStanley.MSDotNet.MSSoap.Client;
using MorganStanley.MSDotNet.MSSoap.Server;
using MorganStanley.MSDotNet.MSXml;
using MorganStanley.MSDotNet.MSLog;
using MorganStanley.IED.Concord.Configuration;

using MorganStanley.IED.Concord.Runtime;

namespace MorganStanley.Desktop.Configuration
{
  /// <include file='xmldocs/DatabaseFileConfigurationStore.cs.xml' path='doc/doc[@for="DatabaseFileConfigurationStore"]/*'/>
  public class DatabaseFileConfigurationStore : IConfigurationStorageWriter
  {
    #region Declarations

    private static FileIdentityCollection _fileManifest;
    private NameValueCollection _settings;
    private string _systemBaseDir;
    private string _userBaseDir;
    private MSXmlProcessorFactoryCollection _xmlProcessorCollection = new MSXmlProcessorFactoryCollection();
    private string _username, _machineName; 
    private string _app, _region, _envKey, _envValue, _role, _roamingEnv;
    private string _version; 
    private IMSNetLoop _loop;

    private static bool _online = false; 
    private string _host, _port, _hostPort;
    private RoamingFileManager _roamingFileManager;
    private static int _getUserFileNamesRetryCount = 0;

    #endregion Declarations

    #region Constants
    private const string CLASS_NAME = "DatabaseFileConfigurationStore";
    protected const string DEFAULT_CONFIG_EXTENSION = ".config";
    private const int GET_USER_FILE_NAMES_RETRY_COUNT = 3;
    private const string NEW_USER = "newuser";
    private const string USER_EXISTS = "userexists";
    private const string NAMESPACE_HEADER = "xmlns:msdwHdr='http://xml.msdw.com/ns/appmw/soap/1.0/header'";
    private static string _defaultVersion = "1.0.1";
    private static Hashtable _versionMap = null;
    private static Hashtable _trueFileMap = null;
    #endregion Constants

    #region Constructors
    /// <include file='xmldocs/DatabaseFileConfigurationStore.cs.xml' path='doc/doc[@for="DatabaseFileConfigurationStore.DatabaseFileConfigurationStore"]/*'/>
    static DatabaseFileConfigurationStore()
    {
      _versionMap = new Hashtable();
      _trueFileMap = new Hashtable();
    }

    public DatabaseFileConfigurationStore()
    {
      _loop = MSNetLoopPoolImpl.Instance();
    }

    public DatabaseFileConfigurationStore(IMSNetLoop loop_)
    {
      _loop = loop_;
    }
    #endregion Constructors

    #region Implementation of IConfigurationStorageWriter
    /// <include file='xmldocs/DatabaseFileConfigurationStore.cs.xml' path='doc/doc[@for="DatabaseFileConfigurationStore.WriteConfiguration"]/*'/>
    public void WriteConfiguration(string name_, string version_, System.Xml.XmlNode node_)
    {
      #region Logging
      if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
      {
        LogLayer.Debug.Location("DatabaseFileConfigurationStore", "WriteConfig").Append(string.Format(Res.GetString(Res.LOG_WRITE_CONFIG), name_, version_, node_.OuterXml);
      }
      #endregion Logging

      if (_online)
      {
        string userId = Environment.UserName;
        string updated = DateTime.Now.ToString();
        string checksum = ComputeChecksum(node_.OuterXml);
        string fileName = name_ + DEFAULT_CONFIG_EXTENSION;
        string content = node_.OuterXml;
        string path = GetUserFilePath(_settings, name_, version_);
        path = path.Replace(@"\", @"\\");

        _version = (version_ != null) ? version_ : _defaultVersion;

        string fileId = null;
        if (_versionMap[path + fileName + _version] == null) 
        {
          fileId = Guid.NewGuid().ToString();
          this.RegisterFileForSave(path, fileName, _version, fileId);
        }

        RoamingFile file = new RoamingFile(fileName, fileId, userId, null, _app, _version, _envKey, _envValue, updated, null, userId, checksum, path, null, content);

        bool status = _roamingFileManager.SaveFile(_username, file);

        if (!status)
        {
          #region Logging
          if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Error)) 
          {
            LogLayer.Error.Location("DatabaseFileConfigurationStore", "WriteConfig").Append(string.Format(Res.GetString(Res.LOG_ERROR_SAVING_CONFIG), name_, version_, node_.OuterXml);
          }
          #endregion Logging
        }
        else 
        { // update fileManifest
          FileIdentity fI = _fileManifest.Get(fileName);
          _fileManifest.Remove(fI);
          _fileManifest.Add(FileIdentity.UpdateChecksum(fI, checksum));
        }
      }

      // whether online or not, still write to local disk
      string filePath = GetUserFilePath(_settings, name_, version_);
      
      using (FileStream fs = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.Read))
      {
        try 
        {
          node_.OwnerDocument.Save(fs);
        } 
        catch (Exception ex_) 
        {
          #region Logging
          if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Error)) 
          {
            LogLayer.Error.Location("DatabaseFileConfigurationStore", "WriteConfig").Append(string.Format(Res.GetString(Res.LOG_ERROR_SAVING_CONFIG), ex_, name_, version_, node_.OuterXml);
          }
          #endregion Logging

          throw new ConfiguratorException("Error writing configuration", ex_);
        }
      }
    }
    #endregion Implementation of IConfigurationStorageWriter

    #region Implementation of IConfigurationStorageReader
    /// <include file='xmldocs/DatabaseFileConfigurationStore.cs.xml' path='doc/doc[@for="DatabaseFileConfigurationStore.Initialize"]/*'/>
    
    public void Initialize(string settings_) 
    {
      #region Logging
      if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
      {
        LogLayer.Debug.Location("DatabaseFileConfigurationStore", "Initialize").Append(string.Format(Res.GetString(Res.LOG_FILESTORE_INITIALIZE), settings_);
      }
      #endregion Logging

      _settings = this.ParseProfileString(settings_);

      if (_userBaseDir == null || _userBaseDir.Length == 0)
      {
        _userBaseDir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
      }

      if (_systemBaseDir == null || _systemBaseDir.Length == 0)
      {
        _systemBaseDir = AppDomain.CurrentDomain.BaseDirectory;
      }

      _userBaseDir = _userBaseDir.TrimEnd('\\');
      _systemBaseDir = _systemBaseDir.TrimEnd('\\');
    }

    public void Initialize(string settings_, XmlNode config_) 
    {
      _settings = this.ParseProfileString(settings_);

      _app = _settings["app"];
      _region = _settings["region"];
      _envKey = _settings["env"];
      _role = _settings["role"];
      _roamingEnv = _settings["roamingenv"];
      _envValue = settings_; // full string

      // read the node pertaining to this provider from app.config
      if (config_ != null)
      {
        XmlNode node = config_.SelectSingleNode("SystemBaseDir",null);
        if (node != null)
        {
          string sData = ParseDirectoryString(node.InnerXml);
          if (sData != null && sData.Length > 0)
          {
            _systemBaseDir = sData;
          }
        }
        node = config_.SelectSingleNode("UserBaseDir",null);
        if (node != null)
        {
          string sData = ParseDirectoryString(node.InnerXml);
          if (sData != null && sData.Length > 0)
          {
            _userBaseDir = sData; 
          }
        }
      }
      if (config_ != null)
      {
        string env = string.Format("connection[@env='{0}']", _roamingEnv);
        XmlNode outerNode = config_.SelectSingleNode(env);
        XmlNode node = null;
        if (outerNode != null)
        {
          node = outerNode.SelectSingleNode("host");
          string sData = node.InnerXml;
          if (sData != null && sData.Length > 0)
          {
            _host = sData;
          }
        }
        node = outerNode.SelectSingleNode("port",null);
        if (node != null)
        {
          string sData = node.InnerXml;
          if (sData != null && sData.Length > 0)
          {
            _port = sData;
          }
        }
      }

      if (_host != null && _port != null)
      {
        _hostPort = string.Format("http://{0}:{1}/", _host, _port);
      }

      if (_userBaseDir == null || _userBaseDir.Length == 0)
      {
        _userBaseDir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
      }

      if (_systemBaseDir == null || _systemBaseDir.Length == 0)
      {
        _systemBaseDir = AppDomain.CurrentDomain.BaseDirectory;
      }

      _userBaseDir = _userBaseDir.TrimEnd('\\');
      _systemBaseDir = _systemBaseDir.TrimEnd('\\');
  
      _username = Environment.UserName;
      _machineName = Environment.MachineName;

      // create new RoamingFileManager with hostPort data
      _roamingFileManager = new RoamingFileManager(_hostPort, _app);

      // Retrieve all the files from the roaming profile.  Save these files for ReadConfiguration(s)
      if (null == _fileManifest) 
      {
        try 
        {
          // Retry up to GET_USER_FILE_NAMES_RETRY_COUNT times before continuing if error occurs
          while(_getUserFileNamesRetryCount < GET_USER_FILE_NAMES_RETRY_COUNT && (_fileManifest == null || _fileManifest.Count == 0))
          {
            _getUserFileNamesRetryCount++;
            _fileManifest = _roamingFileManager.GetUserFileNames(ConcordRuntime.Current.UserInfo.Username);
            _online = true;
          }
          _getUserFileNamesRetryCount = 0;
        }
        catch (System.Net.WebException ex_)
        {
          if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Error))
          {
            LogLayer.Error.Location(CLASS_NAME, "Initialize").Append("Unable to connect to FMS", ex_);
            _online = false;
          }
        }
        catch (MSNetHttpException ex_)
        {
          if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Error))
          {
            LogLayer.Error.Location(CLASS_NAME, "Initialize").Append("Unable to connect to FMS", ex_);
            _online = false;
          }
        }
        catch (MorganStanley.MSDotNet.MSSoap.Common.SoapTimeoutException ex_)
        {
          if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Error))
          {
            LogLayer.Error.Location(CLASS_NAME, "Initialize").Append("Unable to connect to FMS", ex_);
            _online = false;
          }
        }
        catch (Exception ex_) 
        {
          if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Error))
          {
            LogLayer.Error.Location(CLASS_NAME, "Initialize").Append("Error reading roaming profile", ex_);
            _online = false;
          }
        }

        if (_fileManifest == null || _fileManifest.Count == 0) 
        {
          string userStatus = null;
          try 
          {        
            string userApplicationId = Guid.NewGuid().ToString();
            string machineName = Environment.MachineName.ToLower();
            // try to insert user
            userStatus = _roamingFileManager.InsertUser(userApplicationId, _app, _username, machineName, _envKey, _envValue, _app);

            string userId = Environment.UserName;
            string updated = DateTime.Now.ToString();
            string path = _userBaseDir;
            path = path.Replace(@"\", @"\\");

            string version = _defaultVersion;

            if (userStatus.Equals(NEW_USER) || userStatus.Equals(USER_EXISTS))
            {
              if (userStatus.Equals(USER_EXISTS))
              {
                #region Logging
                if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Error)) 
                {
                  LogLayer.Error.Location("DatabaseFileConfigurationStore", "Initialise").Append("User existed in database without any files.");
                }
                #endregion Logging
              }

              string fileDir = GetUserFilePath();
              string[] configFiles = Directory.GetFiles(fileDir);
              foreach (string file_ in configFiles)
              {
                string localId = Guid.NewGuid().ToString();
                string fileId = Guid.NewGuid().ToString();
                string fileName = Path.GetFileName(file_);
                string fileContent = null;

                FileStream file = new FileStream(file_, FileMode.OpenOrCreate, FileAccess.Read);
                StreamReader sr = new StreamReader(file);
                fileContent = sr.ReadToEnd();
                sr.Close();
                file.Close();

                string checksum = ComputeChecksum(fileContent);

                RoamingFile userFile = new RoamingFile(fileName, fileId, userId, null, _app, version, _envKey, _envValue, updated, null, userId, checksum, path, localId, fileContent);

                bool status = false;
                try 
                {
                  status = _roamingFileManager.SaveFile(_username, userFile);
                }
                catch (System.Net.WebException)
                {

                }

                if (!status)
                {
                  #region Logging
                  if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Error)) 
                  {
                    LogLayer.Error.Location("DatabaseFileConfigurationStore", "Initialise").Append("Error retrieving files from FMS, not online");
                    _online = false;
                    break;
                  }
                  #endregion Logging
                }
                else 
                { 
                  this.RegisterFileForSave(path, fileName, _version, fileId);
                  // TODO: Update _trueFileMap as well _trueFileMap[COMBO] = fileContent;
                  _fileManifest.Add(new FileIdentity(fileName, path, version, checksum, fileId));
                }
              }
            }
          }
          catch (Exception ex_)
          {
            if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Error))
            {
              LogLayer.Error.Location(CLASS_NAME, "Initialize").Append("Error reading roaming profile", ex_);
            }
          }

          if (userStatus == null || userStatus == String.Empty)
          {
            _online = false; 
          }

        }

        if (_fileManifest != null && _fileManifest.Count >= 0)
        {
          foreach (FileIdentity file in _fileManifest)
          {
            string fileName = file.Name;
            string version = file.Version;
            version = (version != null) ? version : _defaultVersion;
            string path = file.Path + @"\\" + fileName;
            string fileId = file.FileId;

            this.RegisterFileForSave(path, fileName, version, fileId);
          } // foreach
        } // if
      } // if
    } // method


    /// <include file='xmldocs/DatabaseFileConfigurationStore.cs.xml' path='doc/doc[@for="DatabaseFileConfigurationStore.ReadConfiguration"]/*'/>
    public ConfigurationSection ReadConfiguration(string name_, string version_)
    {
      #region Logging
      if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
      {
        LogLayer.Debug.Location("DatabaseFileConfigurationStore", "ReadConfiguration").Append(string.Format(Res.GetString(Res.LOG_STORE_READCONFIG), name_, version_);
      }
      #endregion Logging

      XmlNode userFile = GetUserFile(_settings, name_, version_);
      XmlNode[] systemFiles = GetFiles(_settings, name_, version_);

      return new ConfigurationSection(name_, version_, systemFiles, userFile);
    }

    /// <include file='xmldocs/DatabaseFileConfigurationStore.cs.xml' path='doc/doc[@for="DatabaseFileConfigurationStore.ReadConfigurations"]/*'/>
    public ConfigurationSection[] ReadConfigurations(ConfigurationIdentityCollection configs_)
    {
      ConfigurationSection[] configSections = new ConfigurationSection[configs_.Count];

      int index = 0;
      foreach (ConfigurationIdentity config in configs_) 
      {
        configSections[index++] = this.ReadConfiguration(config.Name, config.Version);
      }

      return configSections;
    }
    #endregion Implementation of IConfigurationStorageReader

    #region Protected Methods

    #region RegisterFileForSave
    protected void RegisterFileForSave(string path_, string file_, string version_, string id_) 
    {
      _versionMap[path_ + file_ + version_] = id_;
    }
    #endregion RegisterFileForSave

    #endregion Protected Methods

    #region Private Methods

    #region InsertFiles
    public bool InsertFiles()
    {
      string fileDir = GetUserFilePath();
      string[] configFiles = Directory.GetFiles(fileDir);
      foreach (string file_ in configFiles)
      {
        // pInsertFile (@localId, @filePath, @fileId, @userId, @applicationId, @fileName, @file, @version, @checksum )
        string localId = Guid.NewGuid().ToString();
        string filePath = fileDir; // can inline
        string fileId = Guid.NewGuid().ToString();
        string fileName = file_;
        string fileContent = null;
        string version = "1.0";
        string checksum = null;
        
        FileStream file = new FileStream(file_, FileMode.OpenOrCreate, FileAccess.Read);
        StreamReader sr = new StreamReader(file);
        fileContent = sr.ReadToEnd();
        sr.Close();
        file.Close();

        checksum = ComputeChecksum(fileContent);

        string query = string.Format("pInsertFile '{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}'", localId, fileDir, fileId, _username, _app, Path.GetFileName(file_), fileContent, version, checksum);
      }
      return true;
    }
    #endregion InsertFiles


    #region GetDocumentFromFile
    private XmlElement GetDocumentFromFile(string file_)
    {
      XmlDocument doc = new XmlDocument();

      try
      {
        StreamReader sr = File.OpenText(file_);

        System.Xml.XmlTextReader textReader = new XmlTextReader(sr);
        MorganStanley.MSDotNet.MSXml.MSXmlProcessingReader reader = new MSXmlProcessingReader(textReader, _xmlProcessorCollection);

        doc.Load(reader);
        sr.Close();

        return doc.DocumentElement;
      }
      catch(Exception e_)
      {
        if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
        {
          LogLayer.Debug.Location("DatabaseFileConfigurationStore", "GetDocumentFromFile").Append("Error reading file: " + file_, e_);
        }
      }
      return null;
    }
    #endregion GetDocumentFromFile

    #region GetChecksumFromFile
    private string GetChecksumFromFile(string file_)
    {
      XmlDocument doc = new XmlDocument();

      try
      {
        StreamReader sr = File.OpenText(file_);

        System.Xml.XmlTextReader textReader = new XmlTextReader(sr);
        MorganStanley.MSDotNet.MSXml.MSXmlProcessingReader reader = new MSXmlProcessingReader(textReader, _xmlProcessorCollection);

        doc.Load(reader);
        sr.Close();

        return ComputeChecksum(doc.DocumentElement.OuterXml);
      }
      catch(Exception e_)
      {
        if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
        {
          LogLayer.Debug.Location("DatabaseFileConfigurationStore", "GetChecksumFromFile").Append("Error reading file checksum: " + file_, e_);
        }
      }
      return null;
    }
    #endregion GetChecksumFromFile

    #region GetUserFile
    private XmlNode GetUserFile(NameValueCollection settings_, string name_, string version_)
    {
      string file = this.GetUserFilePath(settings_, name_, version_);

      #region Logging
      string logMessage = null;
      if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
      {
        //logMessage = string.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file);
      }
      #endregion Logging

      if (File.Exists(file)) 
      {
        #region Logging
        if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
        {
          LogLayer.Debug.Location("DatabaseFileConfigurationStore", "GetUserFile").Append(string.Format("{0}, Exists='True'", logMessage);
        }
        #endregion Logging

        _version = (version_ != null) ? version_ : _defaultVersion;
        name_ += DEFAULT_CONFIG_EXTENSION;

        // first of all try local trueFileMap copy
        if (_trueFileMap != null && _trueFileMap.Count > 0)
        {
          XmlNode fileMapContent = _trueFileMap[name_ + _userBaseDir + _version] as XmlDocument;
          if (fileMapContent != null)
          {
            return fileMapContent;
          }
        }

        string roamingFileChecksum = string.Empty;
        string localFileChecksum = GetChecksumFromFile(file);

        if (_fileManifest != null)
        {
          if(_fileManifest.Contains(name_))
          {
            FileIdentity fileIdentity = _fileManifest.Get(name_);
            roamingFileChecksum = fileIdentity.Checksum;
          }
        }

        if (localFileChecksum.Equals(roamingFileChecksum) || !_online)
        {
          // get local version of file (faster than database retrieval)
          XmlNode content = GetDocumentFromFile(file);
          _trueFileMap.Add(name_ + _userBaseDir + _version, content);
          return content;
        }
        else
        {
          if (version_ == null || version_ == string.Empty)
          {
            version_ = _defaultVersion;
          }
          RoamingFile rpFile = _roamingFileManager.GetFile(Environment.UserName, name_, _userBaseDir, version_); 

          XmlNode dbContent = null;
          XmlDocument xdoc = new XmlDocument();
          string fileContent = RoamingFileManager.DecodeAndInflateFile(rpFile.FileContent);
          xdoc.LoadXml(fileContent);
          dbContent = xdoc.DocumentElement;
          _trueFileMap.Add(name_ + _userBaseDir + _version, dbContent);
          return dbContent;
        }
      } 
      else 
      {
        #region Logging
        if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
        {
          LogLayer.Debug.Location("DatabaseFileConfigurationStore", "GetUserFile").Append(string.Format("{0}, Exists='False', Configuration was created", logMessage);
        }
        #endregion Logging

        XmlDocument doc = new XmlDocument();
        doc.LoadXml(string.Format("<{0} />", name_));
        return doc.DocumentElement;
      }
    }
    #endregion GetUserFile

    #region GetUserFilePath

    private string GetUserFilePath()
    {
      string dir = String.Empty;
    
      if (_role != null)
      {
        dir = string.Format(@"{0}\Morgan Stanley\{1}\Configuration\{2}\{3}", _userBaseDir, _app, _region, _envKey);      
      }
      else
      {
        dir = string.Format(@"{0}\Morgan Stanley\{1}\Configuration\{2}\{3}\{4}", _userBaseDir, _app, _region, _envKey, _role);
      }

      if (!System.IO.Directory.Exists(dir))
      {
        System.IO.Directory.CreateDirectory(dir);
      }

      return dir;
    }

    private string GetUserFilePath(NameValueCollection settings_, string name_, string version_)
    {
      string app = settings_[Configurator.CONFIG_SETTING_APP_KEY];
      string user = settings_[Configurator.CONFIG_SETTING_USER_KEY];
      string reg = settings_[Configurator.CONFIG_SETTING_REGION_KEY];
      string env = settings_[Configurator.CONFIG_SETTING_ENV_KEY];
      
      string dir = GetUserDirectory(user, app, reg, env);

      if (version_ != null) 
      {
        return string.Format(@"{0}\{1}.{2}.config", dir, name_, version_);
      } 
      else 
      {
        return string.Format(@"{0}\{1}.config", dir, name_);
      }
    }
    #endregion GetUserFilePath

    #region ComputeChecksum
    private string ComputeChecksum(string string_) 
    {
      HashAlgorithm checksumGenerator = new System.Security.Cryptography.SHA1CryptoServiceProvider();
      
      byte[] hashBytes = checksumGenerator.ComputeHash(Encoding.UTF8.GetBytes(string_));
      StringBuilder sb = new StringBuilder();
      for (int i = 0; i < hashBytes.Length; i++) 
      {
        sb.Append(string.Format("{0:X2}", hashBytes[i]));
      }

      return sb.ToString();
    }
    #endregion ComputeChecksum

    #region GetFiles
    
    private XmlNode[] GetFiles(NameValueCollection settings_, string name_, string version_)
    {
      ArrayList _configs = new ArrayList();

      string app = settings_[Configurator.CONFIG_SETTING_APP_KEY];
      string reg = settings_[Configurator.CONFIG_SETTING_REGION_KEY];
      string env = settings_[Configurator.CONFIG_SETTING_ENV_KEY];
      string dir = null, file = null;
      
      if (version_ != null) 
      {
        dir = GetTemplateDirectory(null, null);
        file = string.Format(@"{0}\{1}.{2}.config", dir, name_, version_);
        if (File.Exists(file)) 
        {
          #region Logging
          if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
          {
            LogLayer.Debug.Location("DatabaseFileConfigurationStore", "GetFiles").Append(string.Format("{0}, Exists='True'", string.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file));
          }
          #endregion Logging

          _configs.Add(GetDocumentFromFile(file));

        } 
        else 
        {
          #region Logging
          if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
          {
            LogLayer.Debug.Location("DatabaseFileConfigurationStore", "GetFiles").Append(string.Format("{0}, Exists='False'", string.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file));
          }
          #endregion Logging
        }

        dir = GetTemplateDirectory(reg, null);
        file = string.Format(@"{0}\{1}.{2}.config", dir, name_, version_);
        if (File.Exists(file)) 
        {
          #region Logging
          if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
          {
            LogLayer.Debug.Location("DatabaseFileConfigurationStore", "GetFiles").Append(string.Format("{0}, Exists='True'", string.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file));
          }
          #endregion Logging

          _configs.Add(GetDocumentFromFile(file));

        }
        else 
        {
          #region Logging
          if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
          {
            LogLayer.Debug.Location("DatabaseFileConfigurationStore", "GetFiles").Append(string.Format("{0}, Exists='False'", string.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file));
          }
          #endregion Logging
        }

        dir = GetTemplateDirectory(reg, env);
        file = string.Format(@"{0}\{1}.{2}.config", dir, name_, version_);
        if (File.Exists(file)) 
        {
          #region Logging
          if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
          {
            LogLayer.Debug.Location("DatabaseFileConfigurationStore", "GetFiles").Append(string.Format("{0}, Exists='True'", string.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file));
          }
          #endregion Logging

          _configs.Add(GetDocumentFromFile(file));

        }
        else 
        {
          #region Logging
          if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
          {
            LogLayer.Debug.Location("DatabaseFileConfigurationStore", "GetFiles").Append(string.Format("{0}, Exists='False'", string.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file));
          }
          #endregion Logging
        }
      } 
      else 
      {
        dir = GetTemplateDirectory(null, null);
        file = string.Format(@"{0}\{1}.config", dir, name_);
        if (File.Exists(file)) 
        {
          #region Logging
          if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
          {
            LogLayer.Debug.Location("DatabaseFileConfigurationStore", "GetFiles").Append(string.Format("{0}, Exists='True'", string.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file));
          }
          #endregion Logging

          _configs.Add(GetDocumentFromFile(file));

        }
        else 
        {
          #region Logging
          if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
          {
            LogLayer.Debug.Location("DatabaseFileConfigurationStore", "GetFiles").Append(string.Format("{0}, Exists='False'", string.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file));
          }
          #endregion Logging
        }

        dir = GetTemplateDirectory(reg, null);
        file = string.Format(@"{0}\{1}.config", dir, name_);
        if (File.Exists(file)) 
        {
          #region Logging
          if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
          {
            LogLayer.Debug.Location("DatabaseFileConfigurationStore", "GetFiles").Append(string.Format("{0}, Exists='True'", string.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file));
          }
          #endregion Logging

          _configs.Add(GetDocumentFromFile(file));

        }
        else 
        {
          #region Logging
          if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
          {
            LogLayer.Debug.Location("DatabaseFileConfigurationStore", "GetFiles").Append(string.Format("{0}, Exists='False'", string.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file));
          }
          #endregion Logging
        }

        dir = GetTemplateDirectory(reg, env);
        file = string.Format(@"{0}\{1}.config", dir, name_);
        if (File.Exists(file)) 
        {
          #region Logging
          if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
          {
            LogLayer.Debug.Location("DatabaseFileConfigurationStore", "GetFiles").Append(string.Format("{0}, Exists='True'", string.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file));
          }
          #endregion Logging

          _configs.Add(GetDocumentFromFile(file));

        }
        else 
        {
          #region Logging
          if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
          {
            LogLayer.Debug.Location("DatabaseFileConfigurationStore", "GetFiles").Append(string.Format("{0}, Exists='False'", string.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file));
          }
          #endregion Logging
        }
      }

      if (_configs.Count > 0) 
      {
        return (XmlNode[]) _configs.ToArray(typeof(XmlNode));
      } 
      else 
      {
        #region Logging
        if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
        {
          LogLayer.Debug.Location("DatabaseFileConfigurationStore", "GetFiles").Append(Res.GetString(Res.LOG_FILESTORE_NOFILES_FOUND)).Send();
        }
        #endregion Logging

        return null;
      }
    }
    #endregion GetFiles

    #region GetUserDirectory
    private string GetUserDirectory(string username_, string app_, string reg_, string env_)
    {
      string dir = string.Format(@"{0}\Morgan Stanley\{1}\Configuration\{2}\{3}", _userBaseDir, app_, reg_, env_);

      if (!System.IO.Directory.Exists(dir))
      {
        System.IO.Directory.CreateDirectory(dir);
      }
      return dir;
    }
    #endregion GetUserDirectory

    #region GetTemplateDirectory
    private string GetTemplateDirectory(string reg_, string env_)
    {
      string dir = null;

      if (reg_ == null && env_ == null) 
      {
        dir = string.Format(@"{0}\Configuration", _systemBaseDir);
      }
      else if (reg_ != null && env_ == null) 
      {
        dir = string.Format(@"{0}\Configuration\{1}", _systemBaseDir, reg_);
      } 
      else 
      {
        dir = string.Format(@"{0}\Configuration\{1}\{2}", _systemBaseDir, reg_, env_);
      }

      return dir;
    }
    #endregion GetTemplateDirectory

    #region ParseProfileString
    private NameValueCollection ParseProfileString(string profileString_) 
    {
      NameValueCollection values = new NameValueCollection();

      string[] settings = profileString_.Split(';');

      for (int index = 0; index < settings.Length; index++) 
      {
        string[] namevalue = settings[index].Split('=');
        if (namevalue.Length == 2) 
        {
          values.Add(namevalue[0].Trim(), namevalue[1].Trim());
        } 
        else 
        {
          #region Logging
          if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Error)) 
          {
            LogLayer.Error.Location("DatabaseFileConfigurationStore", "ParseProfileString").Append(string.Format(Res.GetString(Res.EXCEPTION_INVALID_PROFILESTRING), profileString_);
          }
          #endregion Logging

          //throw new ConfiguratorException(string.Format(Res.GetString(Res.EXCEPTION_INVALID_PROFILESTRING), profileString_));
        }
      }

      if (values.Count == 0) 
      {
        #region Logging
        if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Error)) 
        {
          LogLayer.Error.Location("DatabaseFileConfigurationStore", "ParseProfileString").Append(string.Format(Res.GetString(Res.EXCEPTION_INVALID_PROFILESTRING), profileString_);
        }
        #endregion Logging

        //throw new ConfiguratorException(Res.GetString(Res.EXCEPTION_INVALID_PROFILESTRING));
      }
      
      return values;      
    }
    #endregion ParseProfileString

    #region ParseProfileString
    private string ParseDirectoryString(string data_)
    {
      //code allows for exactly 1 environment variable to be specified in the string
      //of form %ETSHOME%
      if (data_.IndexOf('%') > -1)
      {
        int nFirst = data_.IndexOf('%');
        int nLast = data_.LastIndexOf('%');
        if (nLast > nFirst)
        {
          string sLeft = "";
          if (nFirst > 0)
          {
            sLeft = data_.Substring(0,nFirst);
          }
          string sRight = "";
          if (nLast != data_.Length -1)
          {
            sRight = data_.Substring(nLast + 1);
          }

          string sMid = data_.Substring(nFirst +1, nLast - nFirst -1);
          //now swap it for environment variable
          string sEnv = "";
          try 
          {
            sEnv = System.Environment.GetEnvironmentVariable(sMid);
          }
          catch (Exception e_)
          {
            //user probably didn't have permission to read this variable
            #region Logging
            if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Error)) 
            {
              LogLayer.Error.Location("DatabaseFileConfigurationStore", "ParseDirectoryString").Append(string.Format("Error reading environment variable: {0}", data_ + e_);
            }
            #endregion Logging 
            return null;
          }
          if (sEnv != null && sEnv.Length > 0)
          {
            return (sLeft + sEnv + sRight);

          }

        }
        #region Logging
        if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Error)) 
        {
          LogLayer.Error.Location("DatabaseFileConfigurationStore", "ParseDirectoryString").Append(string.Format("Directory string was of invalid format: {0}", data_);
        }
        #endregion Logging
        return null;
      }
      else
      {
        return data_;
      }
    }
    #endregion ParseProfileString

    #endregion Private Methods
  }
}
