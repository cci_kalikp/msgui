#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.Configuration/ConfigurationIdentityCollection.cs#1 $
  $DateTime: 2013/04/11 16:41:48 $
    $Change: 823942 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion

using System;
using System.Collections;
using System.Linq;

namespace MorganStanley.Desktop.Configuration
{
    /// <include file='xmldocs/ConfigurationIdentityCollection.cs.xml' path='doc/doc[@for="ConfigurationIdentityCollection"]/*'/>
    public class ConfigurationIdentityCollection : CollectionBase
    {
        #region Constructors
        /// <include file='xmldocs/ConfigurationIdentityCollection.cs.xml' path='doc/doc[@for="ConfigurationIdentityCollection.ConfigurationIdentityCollection"]/*'/>
        public ConfigurationIdentityCollection()
        {
        }
        #endregion Constructors

        #region Public Methods
        /// <include file='xmldocs/ConfigurationIdentityCollection.cs.xml' path='doc/doc[@for="ConfigurationIdentityCollection.Item1"]/*'/>
        public ConfigurationIdentity this[string name_]
        {
            get { return this[name_, null]; }
        }

        /// <include file='xmldocs/ConfigurationIdentityCollection.cs.xml' path='doc/doc[@for="ConfigurationIdentityCollection.Item2"]/*'/>
        public ConfigurationIdentity this[string name_, string version_]
        {
            get
            {
                return
                    this.List.Cast<ConfigurationIdentity>()
                        .FirstOrDefault(entry => entry.Name == name_ && entry.Version == version_);
            }
        }

        /// <include file='xmldocs/ConfigurationIdentityCollection.cs.xml' path='doc/doc[@for="ConfigurationIdentityCollection.Item"]/*'/>
        public ConfigurationIdentity this[int index_]
        {
            get { return ((ConfigurationIdentity)(this.List[index_])); }
        }

        /// <include file='xmldocs/ConfigurationIdentityCollection.cs.xml' path='doc/doc[@for="ConfigurationIdentityCollection.Add"]/*'/>
        public int Add(ConfigurationIdentity entry_)
        {
            if (
                this.List.Cast<ConfigurationIdentity>()
                    .Any(entry => entry_.Name == entry.Name && entry_.Version == entry.Version))
            {
                throw new Exception(Res.GetString(Res.EXCEPTION_CONFIG_IDENTITY_ALREADY_EXISTS));
            }

            return this.List.Add(entry_);
        }

        /// <include file='xmldocs/ConfigurationIdentityCollection.cs.xml' path='doc/doc[@for="ConfigurationIdentityCollection.Remove"]/*'/>
        public void Remove(ConfigurationIdentity entry_)
        {
            List.Remove(entry_);
        }

        /// <include file='xmldocs/ConfigurationIdentityCollection.cs.xml' path='doc/doc[@for="ConfigurationIdentityCollection.GetEnumerator"]/*'/>
        public new ConfigurationIdentityCollectionEnumerator GetEnumerator()
        {
            return new ConfigurationIdentityCollectionEnumerator(this);
        }
        #endregion Public Methods

        #region Public Inner Classes
        /// <include file='xmldocs/ConfigurationIdentityCollection.cs.xml' path='doc/doc[@for="ConfigurationIdentityCollectionEnumerator"]/*'/>
        public class ConfigurationIdentityCollectionEnumerator : IEnumerator
        {
            #region Declarations
            private IEnumerator _enumerator;
            private IEnumerable _temp;
            #endregion Declarations

            /// <include file='xmldocs/ConfigurationIdentityCollection.cs.xml' path='doc/doc[@for="ConfigurationIdentityCollectionEnumerator.ConfigurationIdentityCollectionEnumerator"]/*'/>
            public ConfigurationIdentityCollectionEnumerator(ConfigurationIdentityCollection mappings_)
            {
                _temp = ((IEnumerable)(mappings_));
                _enumerator = _temp.GetEnumerator();
            }

            /// <include file='xmldocs/ConfigurationIdentityCollection.cs.xml' path='doc/doc[@for="ConfigurationIdentityCollectionEnumerator.Current"]/*'/>
            public ConfigurationIdentity Current
            {
                get { return ((ConfigurationIdentity)(_enumerator.Current)); }
            }

            object IEnumerator.Current
            {
                get { return _enumerator.Current; }
            }

            /// <include file='xmldocs/ConfigurationIdentityCollection.cs.xml' path='doc/doc[@for="ConfigurationIdentityCollectionEnumerator.MoveNext"]/*'/>
            public bool MoveNext()
            {
                return _enumerator.MoveNext();
            }

            bool IEnumerator.MoveNext()
            {
                return _enumerator.MoveNext();
            }

            /// <include file='xmldocs/ConfigurationIdentityCollection.cs.xml' path='doc/doc[@for="ConfigurationIdentityCollectionEnumerator.Reset"]/*'/>
            public void Reset()
            {
                _enumerator.Reset();
            }

            void IEnumerator.Reset()
            {
                _enumerator.Reset();
            }
        }
        #endregion Public Inner Classes
    }
}

