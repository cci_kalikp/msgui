#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2007 Morgan Stanley, All Rights Reserved
  Unpublished copyright.  This material contains proprietary
  information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.Configuration/IConfigurationInterceptingReader.cs#1 $
  $DateTime: 2013/04/11 16:41:48 $
    $Change: 823942 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion File Info Header

using System;

namespace MorganStanley.Desktop.Configuration
{
  /// <summary>
  /// Defines the contract that read-only configuration interceptors must implement.
  /// </summary>
  /// <remarks>
  /// <seealso cref="InterceptingConfigurationStore"/>
  /// </remarks>
  /// <example>
  /// See <see cref="IConfigurationInterceptingWriter"/> for sample implementation.
  /// </example> 
  public interface IConfigurationInterceptingReader
  {
    /// <summary>
    /// Invoked after a <see cref="ConfigurationSection"/> is requested and retrieved to allow manipulation before it is returned.
    /// </summary>
    /// <param name="original_">The original <see cref="ConfigurationSection"/></param>
    /// <returns>A modified <see cref="ConfigurationSection"/> or the original <see cref="ConfigurationSection"/></returns>
    ConfigurationSection InterceptRead(ConfigurationSection original_);
	}
}