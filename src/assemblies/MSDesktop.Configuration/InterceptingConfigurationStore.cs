#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2007 Morgan Stanley, All Rights Reserved
  Unpublished copyright.  This material contains proprietary
  information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.Configuration/InterceptingConfigurationStore.cs#1 $
  $DateTime: 2013/04/11 16:41:48 $
    $Change: 823942 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion File Info Header

using System;
using System.Collections;
using System.Xml;
using MorganStanley.MSDotNet.MSLog;

namespace MorganStanley.Desktop.Configuration
{
  /// <summary>
  /// Decorates another storage provider and 
  /// intercepts any request for specific config and passes it to one or more associated configuration interceptors.
  /// </summary>
  /// 
  /// <remarks>
  /// Each configuration interceptor must implement <see cref="IConfigurationInterceptingReader"/> 
  /// and can optionally implement <see cref="IConfigurationInterceptingWriter"/>. 
  /// 
  /// <note type="caution">
  /// After a request for a specific config is intercepted the <see cref="ConfigurationManager"/> 
  /// will add the results to its cache as usual. Consequent requests for the same config will be 
  /// returned from the cache and <b>not</b> passed to an interceptor. To force interception again 
  /// you can use <see cref="ConfigurationManager.ClearConfigCache">ConfigurationManager.ClearConfigCache</see>
  /// </note> 
  /// 
  /// <para>
  /// In the case of multiple intercepting readers for one name:
  ///   Using the same order specified in the config, each reader is passed the results of the previous interceptor (or the existing config for the first interceptor)
  /// </para>
  /// <para>
  /// In the case of multiple intercepting writers for one name:
  ///   Using the reverse order specified in the config, each writer is passed a <see cref="DecoratedWriteConfiguration"/> delegate pointing to the next interceptor (or a delegate pointing to the _decoratedWriter.WriteConfiguration for the last interceptor)
  /// </para>
  /// 
  /// 
  /// Interceptors:
  /// <list type="bullet">
  ///   <item><description>A</description></item>
  ///   <item><description>B</description></item>
  ///   <item><description>C</description></item> 
  /// </list>
  /// 
  /// When reading: 
  /// <list type="number">
  /// <item><description>The existing config is passed into A</description></item>
  /// <item><description>The results from A are passed into B</description></item>
  /// <item><description>The results from B are passed into C</description></item>
  /// <item><description>The results from C are returned.</description></item>
  /// </list>
  /// 
  /// When writing:
  /// <list type="number">
  /// <item><description>C is called with a DecoratedWriteConfiguration delegate pointing to B</description></item>
  /// <item><description>If C invokes the delegate, B is called with a DecoratedWriteConfiguration delegate pointing to A</description></item>
  /// <item><description>If B invokes the delegate, A is called with a DecoratedWriteConfiguration delegate pointing to _decoratedWriter.WriteConfiguration</description></item>
  /// </list>
  /// 
  /// <seealso cref="IConfigurationInterceptingReader"/>
  /// <seealso cref="IConfigurationInterceptingWriter"/>
  /// </remarks>
  ///  
  /// <example>
  /// The following config decorates a <see cref="FileConfigurationStoreEx "/> provider. It registers a custom 
  /// configuration interceptor to intercept requests for <c>RTD</c>.
  /// 
  /// <code escaped="true">
  ///   <concordConfiguration defaultProvider="MorganStanley.IED.Concord.Configuration.InterceptingConfigurationStore, Concord.Configuration">
  ///     <storageProviders>
  ///       <storageProvider type="MorganStanley.IED.Concord.Configuration.InterceptingConfigurationStore, Concord.Configuration">
  ///         <DecoratedStorageProvider type="MorganStanley.IED.Concord.Configuration.FileConfigurationStoreEx, Concord.Configuration">
  ///           <SystemBaseDir>%ETSHOME%</SystemBaseDir>
  ///           <UserBaseDir>\\msad\root\NA\NY\LIB\IED\SHARED\TraderDesktop\newusercfg\%USERNAME%</UserBaseDir>
  ///           <AdditionalFolders>\\msad\root\NA\NY\LIB\IED\SHARED\TraderDesktop\expimp\prodny</AdditionalFolders>
  ///           <OmitConfigExtension>.xml</OmitConfigExtension>
  ///         </DecoratedStorageProvider>
  ///         <ConfigurationInterceptors>
  ///           <ConfigurationInterceptor name="RTD" type="MorganStanley.IED.TradingOMW.NY.Data.FilterConnectionBalancer, IED.TradingOMWNY" />
  ///         </ConfigurationInterceptors>
  ///       </storageProvider>
  ///     </storageProviders>
  ///   </concordConfiguration>
  /// </code>
  /// </example>
  public class InterceptingConfigurationStore : IConfigurationStorageWriter, IConfigurationEnumeratesConfigs
  {
    #region Constants
    private const string CLASS_NAME = "InterceptingConfigurationStore";
    private const string CONFIGURATION_INTERCEPTORS_XPATH = "./ConfigurationInterceptors/ConfigurationInterceptor";
    private const string DECORATED_PROVIDER_XPATH = "./DecoratedStorageProvider";
    #endregion Constants

    #region Declarations
    private IConfigurationStorageReader _decoratedReader;
    private IConfigurationStorageWriter _decoratedWriter;
    private IConfigurationEnumeratesConfigs _decoratedEnumeratesConfigs;
    private Hashtable _interceptors = new Hashtable(CaseInsensitiveHashCodeProvider.DefaultInvariant,CaseInsensitiveComparer.DefaultInvariant); 
    #endregion Declarations

    /// <summary>
    /// This method is invalid for this storage provider as it requires a config node to specify the decorated storage provider
    /// </summary>
    public void Initialize(string settings_)
    {
      throw new ConfiguratorException(string.Format("{0} must have a valid configuration node specifying the type of the decorated storage provider.",CLASS_NAME));    
    }

    public void Initialize(string settings_, XmlNode config_)
    {
      InitializeDecoratedStorageProvider(settings_, config_);

      try
      {
        XmlNodeList interceptorNodes = config_.SelectNodes(CONFIGURATION_INTERCEPTORS_XPATH);

        if (interceptorNodes != null && interceptorNodes.Count > 0)
        {
          foreach (XmlNode interceptorNode in interceptorNodes)
          {
          
            string name = interceptorNode.Attributes["name"].Value;
            string type = interceptorNode.Attributes["type"].Value;

            Type interceptorType = Type.GetType(type);

            if (interceptorType.GetInterface(typeof(IConfigurationInterceptingReader).Name) == null) 
            {
              #region Logging
              if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Error)) 
              {
                LogLayer.Error.Location(CLASS_NAME, "Initialize").Append(string.Format(Res.GetString(Res.EXCEPTION_MUST_IMPLEMENT_INTERFACE), interceptorType, typeof(IConfigurationInterceptingReader).Name)).Send();
              }
              #endregion Logging

              throw new ConfiguratorException(string.Format(Res.GetString(Res.EXCEPTION_MUST_IMPLEMENT_INTERFACE), interceptorType, typeof(IConfigurationInterceptingReader).Name));
            } 

            #region Logging
            if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
            {
                LogLayer.Debug.Location(CLASS_NAME, "Initialize").Append(string.Format(Res.GetString(Res.LOG_INTERCEPTINGSTORE_INTERCEPTOR_CREATEINSTANCE), interceptorType, name)).Send();
            }
            #endregion Logging

            object interceptor = Activator.CreateInstance(interceptorType);

            ArrayList interceptorList = _interceptors[name] as ArrayList;
            if(interceptorList==null)
            {
              _interceptors[name] = interceptorList = new ArrayList();
            }
            interceptorList.Add(interceptor);
          }
        }
      }
      catch (Exception ex_)
      {
        if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Error))
        {
            LogLayer.Error.Location(CLASS_NAME, "Initialize")
                    .Append(Res.GetString(Res.EXCEPTION_UNABLE_TO_CREATE_INSTANCE))
                    .AppendException(ex_)
                    .Send();
        }
        throw new ConfiguratorException(Res.GetString(Res.EXCEPTION_UNABLE_TO_CREATE_INSTANCE), ex_);
      }
    }

    public ConfigurationSection ReadConfiguration(string name_, string version_)
    {
      if (_decoratedReader != null)
      {
        ConfigurationSection section = _decoratedReader.ReadConfiguration(name_, version_);
        return InterceptRead(section);
      }
      return null;
    }

    public ConfigurationSection[] ReadConfigurations(ConfigurationIdentityCollection configs_)
    {
      if (_decoratedReader != null)
      {
        ConfigurationSection[] sections = _decoratedReader.ReadConfigurations(configs_);
        ConfigurationSection[] intercepted = new ConfigurationSection[sections.Length];

        for (int i = 0; i < sections.Length; i++)
        {
          intercepted[i] = InterceptRead(sections[i]);
        }

        return intercepted;
      }
      return null;
    }

    public ArrayList EnumerateAvailableConfigs(string pattern_)
    {
      if (_decoratedEnumeratesConfigs != null)
      {
        return _decoratedEnumeratesConfigs.EnumerateAvailableConfigs(pattern_);
      }
      return new ArrayList(0);
    }

    public void WriteConfiguration(string name_, string version_, System.Xml.XmlNode node_)
    {
      if (_decoratedWriter != null && _interceptors.ContainsKey(name_))
      {
        try
        {
          ArrayList interceptorList = (ArrayList)_interceptors[name_];
         
          ChainedInterceptingWriter interceptorChain = BuildInterceptingWriterChain(interceptorList);
         
          interceptorChain.WriteConfiguration(name_, version_, node_);
        }
        catch (Exception ex_)
        {
          if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Error)) 
          {
            LogLayer.Error.Location(CLASS_NAME, "InterceptRead").Append(string.Format(Res.GetString(Res.EXCEPTION_ERROR_INTERCEPTING_WRITE), ex_, name_)).Send();
          }
          throw;
        }
      }
      else 
      {
        _decoratedWriter.WriteConfiguration(name_, version_, node_);
      }
    }

    private ChainedInterceptingWriter BuildInterceptingWriterChain(ArrayList interceptors_)
    {
      ChainedInterceptingWriter chain = new ChainedInterceptingWriter(new DecoratedWriteConfiguration(_decoratedWriter.WriteConfiguration));
      if(interceptors_!=null)
      {
        for (int i = 0; i < interceptors_.Count; i++)
        {
          IConfigurationInterceptingWriter nextInterceptor = interceptors_[i] as IConfigurationInterceptingWriter;

          if(nextInterceptor!=null)
          {
            chain = new ChainedInterceptingWriter(nextInterceptor, new DecoratedWriteConfiguration(chain.WriteConfiguration));
          }
        }
      }
      return chain;
    }

    private void InitializeDecoratedStorageProvider(string settings_, XmlNode config_)
    {
      XmlNode decoratedConfig = config_.SelectSingleNode(DECORATED_PROVIDER_XPATH);

      string type = decoratedConfig.Attributes["type"].Value;
      Type decoratedType = Type.GetType(type);

      #region Logging
      if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
      {
          LogLayer.Debug.Location(CLASS_NAME, "InitializeDecoratedStorageProvider").Append(string.Format(Res.GetString(Res.LOG_INTERCEPTINGSTORE_DECORATED_CREATEINSTANCE), decoratedType)).Send();
      }
      #endregion

      object decorated = Activator.CreateInstance(decoratedType);

      _decoratedReader = decorated as IConfigurationStorageReader;
      _decoratedWriter = decorated as IConfigurationStorageWriter;
      _decoratedEnumeratesConfigs = decorated as IConfigurationEnumeratesConfigs;

      if (_decoratedReader != null)
      {
        _decoratedReader.Initialize(settings_, decoratedConfig);
      }
    }

    private ConfigurationSection InterceptRead(ConfigurationSection existing_)
    {
      if (existing_ != null && _interceptors.ContainsKey(existing_.Name))
      {
        try
        {
          ArrayList interceptorList = (ArrayList)_interceptors[existing_.Name];
          
          ConfigurationSection intercepted = existing_;

          foreach (IConfigurationInterceptingReader interceptingReader in interceptorList)
          {
            #region Logging
            if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug))
            {
                LogLayer.Debug.Location(CLASS_NAME, "InterceptRead")
                        .Append(string.Format(Res.GetString(Res.LOG_INTERCEPTINGSTORE_INTERCEPTOR_READ), existing_.Name,
                                              interceptingReader.GetType().Name))
                        .Send();
            }
            #endregion
            intercepted = interceptingReader.InterceptRead(intercepted);
          }
          
         return intercepted;
        }
        catch (Exception ex_)
        {
          if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Error)) 
          {
              LogLayer.Error.Location(CLASS_NAME, "InterceptRead").Append(string.Format(Res.GetString(Res.EXCEPTION_ERROR_INTERCEPTING_READ), ex_, existing_.Name)).Send();
          }
          throw;
        }
      }
      return existing_;
    }

    /// <summary>
    /// Represents the <see cref="IConfigurationStorageWriter.WriteConfiguration">IConfigurationStorageWriter.WriteConfiguration</see> method on the decorated configuration provider that writes configuration. 
    /// </summary>
    public delegate void DecoratedWriteConfiguration(string name_, string version_, System.Xml.XmlNode node_);
  

    private class ChainedInterceptingWriter
    {
      private IConfigurationInterceptingWriter _interceptor;
      private DecoratedWriteConfiguration _nextWriter;


      public ChainedInterceptingWriter(DecoratedWriteConfiguration nextWriter_)
      {
        _nextWriter = nextWriter_;
      }

      public ChainedInterceptingWriter(IConfigurationInterceptingWriter interceptor_, DecoratedWriteConfiguration nextWriter_)
      {
        _interceptor = interceptor_;
        _nextWriter = nextWriter_;
      }

      public void WriteConfiguration(string name_, string version_, System.Xml.XmlNode node_)
      {
        if(_interceptor!=null)
        {
          #region Logging
          if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
          {
              LogLayer.Debug.Location(CLASS_NAME, "ChainedInterceptingWriter.WriteConfiguration").Append(string.Format(Res.GetString(Res.LOG_INTERCEPTINGSTORE_INTERCEPTOR_WRITE), name_, _interceptor.GetType().Name)).Send();
          }
          #endregion
          _interceptor.InterceptWrite(name_, version_, node_, _nextWriter);
        }
        else
        {
          _nextWriter(name_, version_, node_);
        }
      }
    }

  }

}