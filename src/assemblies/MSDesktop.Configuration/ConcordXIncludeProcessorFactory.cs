using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Xml;

using MorganStanley.MSDotNet.MSXml;

namespace MorganStanley.Desktop.Configuration
{
  /// <summary>
  /// This is our own implementation of XIncludeProcessorFactory
  /// 
  /// Main reason for copy is that we need to be able to get namespace manager into MSXmlXPointerReader
  /// which this implementation takes care of
  /// 
  /// </summary>
  public class ConcordXIncludeProcessorFactory : IMSXmlProcessorFactory
  {

    // return a list of xi:included URIs
    // need a way of keeping track whats getting included
    // to avoid nasty includes from \\ms\dev which would blow up in prod etc.
    public static string[] IncludedURIs
    {
      get
      {
        return (string[])_includedURIs.ToArray(typeof(string));
      }
    }

    private static ArrayList _includedURIs = new ArrayList();

    readonly string W3C_INCLUDE_URI = "http://www.w3.org/2001/XInclude";
    
    XmlReader IMSXmlProcessorFactory.GetProcessor(XmlReader xmlReader_)
    {
      Trace.WriteLine("XInclude Processing.");
      if(xmlReader_.MoveToAttribute("href"))
      {
        Uri includeURI;
        xmlReader_.ReadAttributeValue();
        string href = xmlReader_.Value;
        string xpointer = null;

        if (xmlReader_.MoveToAttribute("forcerelativehref"))
        {
          xmlReader_.ReadAttributeValue();
          Uri baseURI = new Uri(@Directory.GetCurrentDirectory() + @"\" + @href);
          includeURI = baseURI;
        }
        else
        {
          if (xmlReader_.BaseURI == String.Empty)
          {
            Trace.WriteLine("BaseURI is string.Empty.");
            includeURI = new Uri(href, true);
          }
          else
          {
            Trace.WriteLine("BaseURI is " + xmlReader_.BaseURI.ToString());
            Uri currentBaseURI = new Uri(xmlReader_.BaseURI);
            includeURI = new Uri(currentBaseURI, href, true);
          }
        }
        if (xmlReader_.MoveToAttribute("xpointer"))
        {
          xmlReader_.ReadAttributeValue();
          xpointer = xmlReader_.Value;
        }

        Trace.WriteLine("XInclude of "+ includeURI.ToString());

        // add uri to list of included uris
        _includedURIs.Add(includeURI.ToString());

        //XmlReader includedReader = new XmlTextReader(includeURI.AbsoluteUri);
        Stream stream =  (Stream)MSXmlResolverRegistry.Instance.Resolver.GetEntity(includeURI,null,typeof (Stream));
        XmlReader includedReader = new MSXmlControllableBaseURIReader(new XmlTextReader(stream),includeURI.AbsoluteUri);

        if (xpointer != null)
        {
          Trace.WriteLine("XPath expression for xinclude: " + xpointer);

          // adding namespaces if any
          XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlReader_.NameTable);
          if (FileConfigurationStoreEx.NamespacesForXInclude.Count > 0)
          {
            foreach(DictionaryEntry de in FileConfigurationStoreEx.NamespacesForXInclude)
            {
              nsmgr.AddNamespace((string)de.Key, (string)de.Value);
            }
          }

          includedReader = new MSXmlXPointerReader(includedReader, nsmgr, xpointer);
        }

        xmlReader_.Skip();
        return includedReader;
      }
      else
      {
        throw new MSXmlMissingAttributeException("include element missing required Href parameter.");
      }
    }
    
    XmlQualifiedName IMSXmlProcessorFactory.RecognizedName
    {
      get
      {  
        return new XmlQualifiedName("include",W3C_INCLUDE_URI);
      } 
    }    
  }
}
