#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.Configuration/LocalConfigurationStore.cs#1 $
  $DateTime: 2013/04/11 16:41:48 $
    $Change: 823942 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion

using System;
using System.Configuration;
using System.IO;
using System.Xml;
using MorganStanley.MSDotNet.MSLog;
using MorganStanley.MSDotNet.MSXml.XPath;


namespace MorganStanley.Desktop.Configuration
{
  /// <include file='xmldocs/LocalConfigurationStore.cs.xml' path='doc/doc[@for="LocalConfigurationStore"]/*'/>
	public class LocalConfigurationStore : IConfigurationStorageWriter, IConfigurationStorageReader
	{
    #region Declarations
    private static object _lock = new object();
    #endregion Declarations

    #region Constructors
    /// <include file='xmldocs/LocalConfigurationStore.cs.xml' path='doc/doc[@for="LocalConfigurationStore.LocalConfigurationStore"]/*'/>
		public LocalConfigurationStore()
		{
		}
    #endregion Constructors

    #region Implementation of IConfigurationStorageReader
    /// <include file='xmldocs/LocalConfigurationStore.cs.xml' path='doc/doc[@for="LocalConfigurationStore.Initialize"]/*'/>
    public void Initialize(string settings_) 
    {
    }

    public void Initialize(string settings_, XmlNode config_) 
    {
      
  
    }

    /// <include file='xmldocs/LocalConfigurationStore.cs.xml' path='doc/doc[@for="LocalConfigurationStore.ReadConfiguration"]/*'/>
    public ConfigurationSection ReadConfiguration(string name_, string version_) 
    {
      #region Logging
      string logMessage = null;
      if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
      {
        logMessage = string.Format(Res.GetString(Res.LOG_STORE_READCONFIG), name_, version_);
      }
      #endregion Logging

      XmlDocument localDoc = null;
      XmlNode node = null;

      string localFile = this.GetLocalConfigFilename();

      lock(_lock) 
      {
        if (File.Exists(localFile)) 
        {
          #region Logging
          if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug))
          {
              LogLayer.Debug.Location("LocalConfigurationStore", "ReadConfiguration")
                      .Append(string.Format("{0}, Exists='True'", logMessage))
                      .Send();
          }
          #endregion Logging

          localDoc = new XmlDocument();
          localDoc.Load(localFile);
        } 
        else 
        {
          #region Logging
          if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
          {
              LogLayer.Debug.Location("LocalConfigurationStore", "ReadConfiguration").Append(string.Format("{0}, Exists='False', Configuration was created", logMessage)).Send();
          }
          #endregion Logging

          localDoc = new XmlDocument();
          localDoc.LoadXml(string.Format("<Configuration><{0}/></Configuration>", name_));
        }
      }
    
      node = localDoc.SelectSingleNode("Configuration/" + name_);
      if (node != null) node = node.Clone();

      XmlNode readonlyNode = XmlConfigurationHandler.GetConfigNode(name_);
      XmlNode[] readonlyNodes = null;
      if (readonlyNode != null) readonlyNodes = new XmlNode[] {readonlyNode};

      ConfigurationSection config = new ConfigurationSection(name_, version_, readonlyNodes, node);
      return config;
    }

    /// <include file='xmldocs/LocalConfigurationStore.cs.xml' path='doc/doc[@for="LocalConfigurationStore.ReadConfigurations"]/*'/>
    public ConfigurationSection[] ReadConfigurations(ConfigurationIdentityCollection configs_) 
    {
      ConfigurationSection[] configSections = new ConfigurationSection[configs_.Count];

      int index = 0;
      foreach (ConfigurationIdentity config in configs_) 
      {
        configSections[index++] = this.ReadConfiguration(config.Name, config.Version);
      }

      return configSections;
    }
    #endregion Implementation of IConfigurationStorageReader

    #region Implementation of IConfigurationStorageWriter
    /// <include file='xmldocs/LocalConfigurationStore.cs.xml' path='doc/doc[@for="LocalConfigurationStore.WriteConfiguration"]/*'/>
    public void WriteConfiguration(string name_, string version_, XmlNode node_)
    {
      #region Logging
      if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
      {
        LogLayer.Debug.Location("LocalConfigurationStore", "WriteConfig").Append(string.Format(Res.GetString(Res.LOG_WRITE_CONFIG), name_, version_, node_.OuterXml)).Send();
      }
      #endregion Logging

      lock (_lock) 
      {
        try 
        {
          XmlDocument localDoc = new XmlDocument();
          string localFile = this.GetLocalConfigFilename();

          if (File.Exists(localFile)) 
          {
            localDoc = new XmlDocument();
            localDoc.Load(localFile);
          } 
          else 
          {
            localDoc = new XmlDocument();
            localDoc.LoadXml(string.Format("<Configuration><{0}/></Configuration>", name_));
          }

          MSXmlDocXPathWriter writer = new MSXmlDocXPathWriter(localDoc);

          XmlNode node = localDoc.ImportNode(node_, true);
          writer.SetNode("Configuration/" + name_, null, node);

          localDoc.Save(localFile);
        }
        catch (Exception ex_) 
        {
          #region Logging
          if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Error)) 
          {
              LogLayer.Error.Location("LocalConfigurationStore", "WriteConfig").Append(string.Format(Res.GetString(Res.LOG_ERROR_SAVING_CONFIG), ex_, name_, version_, node_.OuterXml)).Send();
          }
          #endregion Logging

          throw new ConfiguratorException("Error writing configuration", ex_);
        }
      }
    }
    #endregion Implementation of IConfigurationStorageWriter

    #region Private Methods
    private string GetLocalConfigFilename()
    {
      string configFile = AppDomain.CurrentDomain.SetupInformation.ConfigurationFile.ToLower();

      string woExtension = configFile.Substring(0, configFile.LastIndexOf(".config"));
      string localConfigFile = string.Format(@"{0}.local.config", woExtension);

      return localConfigFile;
    }
    #endregion Private Methods
	}

  /// <include file='xmldocs/LocalConfigurationStore.cs.xml' path='doc/doc[@for="XmlConfigurationHandler"]/*'/>
  public sealed class XmlConfigurationHandler : IConfigurationSectionHandler 
  {
    #region Implementation of IConfigurationSectionHandler
    /// <include file='xmldocs/LocalConfigurationStore.cs.xml' path='doc/doc[@for="XmlConfigurationHandler.Create"]/*'/>
    public object Create(object parent_, object configContext_, XmlNode section_) 
    {
      return section_;
    }

    #endregion Implementation of IConfigurationSectionHandler
    
    /// <include file='xmldocs/LocalConfigurationStore.cs.xml' path='doc/doc[@for="XmlConfigurationHandler.GetConfigNode"]/*'/>
    public static XmlNode GetConfigNode(string sectionName)
    {
      // see http://social.msdn.microsoft.com/Forums/en-US/clr/thread/1e14f665-10a3-426b-a75d-4e66354c5522
      var exeConfig = System.Configuration.ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
      var section = exeConfig.GetSection(sectionName);

      if (section == null)
        return null;

      var doc = new XmlDocument();
      doc.LoadXml(section.SectionInformation.GetRawXml());
      return doc.DocumentElement;
      
    }
  }
}
