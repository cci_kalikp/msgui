#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.Configuration/ConfigurationIdentity.cs#1 $
  $DateTime: 2013/04/11 16:41:48 $
    $Change: 823942 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion

using System;

namespace MorganStanley.Desktop.Configuration
{
    /// <include file='xmldocs/ConfigurationIdentity.cs.xml' path='doc/doc[@for="ConfigurationIdentity"]/*'/>
    public class ConfigurationIdentity
    {
        #region Constructors
        /// <include file='xmldocs/ConfigurationIdentity.cs.xml' path='doc/doc[@for="ConfigurationIdentity.ConfigurationIdentity"]/*'/>
        public ConfigurationIdentity(string name_)
        {
            Name = name_;
        }

        /// <include file='xmldocs/ConfigurationIdentity.cs.xml' path='doc/doc[@for="ConfigurationIdentity.ConfigurationIdentity1"]/*'/>
        public ConfigurationIdentity(string name_, string version_)
            : this(name_)
        {
            Version = version_;
        }

        /// <include file='xmldocs/ConfigurationIdentity.cs.xml' path='doc/doc[@for="ConfigurationIdentity.ConfigurationIdentity2"]/*'/>
        public ConfigurationIdentity(string name_, string version_, Type handlerType_)
            : this(name_, version_)
        {
            HandlerType = handlerType_;
        }

        /// <include file='xmldocs/ConfigurationIdentity.cs.xml' path='doc/doc[@for="ConfigurationIdentity.ConfigurationIdentity3"]/*'/>
        public ConfigurationIdentity(string name_, string version_, Type handlerType_, Type providerType_)
            : this(name_, version_, handlerType_)
        {
            ProviderType = providerType_;
        }
        #endregion Constructors

        #region Public Properties

        /// <include file='xmldocs/ConfigurationIdentity.cs.xml' path='doc/doc[@for="ConfigurationIdentity.HandlerType"]/*'/>
        public Type HandlerType { get; set; }

        /// <include file='xmldocs/ConfigurationIdentity.cs.xml' path='doc/doc[@for="ConfigurationIdentity.ProviderType"]/*'/>
        public Type ProviderType { get; set; }

        /// <include file='xmldocs/ConfigurationIdentity.cs.xml' path='doc/doc[@for="ConfigurationIdentity.Name"]/*'/>
        public string Name { get; set; }

        /// <include file='xmldocs/ConfigurationIdentity.cs.xml' path='doc/doc[@for="ConfigurationIdentity.Version"]/*'/>
        public string Version { get; set; }

        #endregion Public Properties
    }
}
