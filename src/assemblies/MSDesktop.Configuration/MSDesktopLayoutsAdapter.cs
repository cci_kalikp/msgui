﻿using System.Reflection;
using System.Xml;
using System.Xml.Linq;
using MorganStanley.Desktop.Configuration;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MorganStanley.MSDotNet.MSGui.Impl.Persistence
{
	public class MSDesktopLayoutsAdapter : IConfigurationInterceptingWriter
	{
		internal static IPersistenceStorage fps;

		private void EnsureFPS()
		{
			fps.InitializeStorage();

		}

		public void InterceptWrite(string name_, string version_, System.Xml.XmlNode node_, InterceptingConfigurationStore.DecoratedWriteConfiguration decoratedWriteConfiguration_)
		{
			if (!(fps is IPartialPersistenceStorage))
			{
				foreach (XmlElement element in node_.OwnerDocument.SelectNodes("MSDesktopLayouts/WindowManager/Layouts/Layout"))
				{
					XDocument xDocument = new XDocument();
					xDocument.Add(XmlHelper.XmlElementToXElement((XmlElement)element.FirstChild));
					fps.SaveState(xDocument, element.Attributes["name"].Value);
				}
				
			}
		}

		public ConfigurationSection InterceptRead(ConfigurationSection original_)
		{
			EnsureFPS();
			XmlDocument node = new XmlDocument
								{
									InnerXml =
										@"<MSDesktopLayouts><WindowManager type=""MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl.ConcordWindowManager, MSDotNet.MSGui.Impl""><Layouts></Layouts><DefaultLayout></DefaultLayout></WindowManager></MSDesktopLayouts>"
								};
			//<Layout name="Default">"
			foreach (var availableProfile in fps.AvailableProfiles)
			{
				var xxx = fps.LoadState(availableProfile);
				var attr = node.CreateAttribute("name");
				attr.InnerText = availableProfile;
				var element = node.CreateElement("Layout");
				element.Attributes.Append(attr);
				element.InnerXml = xxx.ToString();
				node.DocumentElement["WindowManager"]["Layouts"].AppendChild(element);
			    node.DocumentElement["WindowManager"]["DefaultLayout"].InnerText = fps.DefaultName ?? "Default";
			}

			return new ConfigurationSection(original_.Name, original_.Version, new XmlNode[] { }, node.DocumentElement);
		}
	}
}