#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.Configuration/Configurator.cs#1 $
  $DateTime: 2013/04/11 16:41:48 $
    $Change: 823942 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion

using System;
using System.Collections;
using System.Collections.Specialized;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Xml;
using MorganStanley.MSDotNet.MSLog;
using MorganStanley.MSDotNet.MSXml.XPath;


namespace MorganStanley.Desktop.Configuration
{
  /// <include file='xmldocs/Configurator.cs.xml' path='doc/doc[@for="Configurator"]/*'/>
  public class Configurator : MarshalByRefObject, IConfigurationSectionHandlerWriter
  {
    #region Constants
    /// <include file='xmldocs/Configurator.cs.xml' path='doc/doc[@for="Configurator.CONFIG_SETTING_USER_KEY"]/*'/>
    public static readonly string CONFIG_SETTING_USER_KEY = "user";

    /// <include file='xmldocs/Configurator.cs.xml' path='doc/doc[@for="Configurator.CONFIG_SETTING_REGION_KEY"]/*'/>
    public static readonly string CONFIG_SETTING_REGION_KEY = "region";

    /// <include file='xmldocs/Configurator.cs.xml' path='doc/doc[@for="Configurator.CONFIG_SETTING_APP_KEY"]/*'/>
    public static readonly string CONFIG_SETTING_APP_KEY = "app";

    /// <include file='xmldocs/Configurator.cs.xml' path='doc/doc[@for="Configurator.CONFIG_SETTING_ENV_KEY"]/*'/>
    public static readonly string CONFIG_SETTING_ENV_KEY = "env";
    
    /// <include file='xmldocs/Configurator.cs.xml' path='doc/doc[@for="Configurator.CONFIG_SETTING_ROLE_KEY"]/*'/>
    public static readonly string CONFIG_SETTING_ROLE_KEY = "role";   
    
    /// <include file='xmldocs/Configurator.cs.xml' path='doc/doc[@for="Configurator.CONFIG_SETTING_ETSCFG_KEY"]/*'/>
    public static readonly string CONFIG_SETTING_ETSCFG_KEY = "etscfg";

    /// <include file='xmldocs/Configurator.cs.xml' path='doc/doc[@for="Configurator.CONFIG_SETTING_LOCAL_ENV_KEY"]/*'/>
    public static readonly string CONFIG_SETTING_LOCAL_ENV_KEY = "localenv";
    
    /// <include file='xmldocs/Configurator.cs.xml' path='doc/doc[@for="Configurator.CONFIG_SETTING_LOCAL_REGION_KEY"]/*'/>
    public static readonly string CONFIG_SETTING_LOCAL_REGION_KEY = "localregion";

    /// <include file='xmldocs/Configurator.cs.xml' path='doc/doc[@for="Configurator.CONFIG_SETTING_LOCAL_APP_KEY"]/*'/>
    public static readonly string CONFIG_SETTING_LOCAL_APP_KEY = "localapp";
    #endregion Constants

    #region Declarations
    private bool _autoSave;
    private object _lock = new object();
    
    private ArrayList _readOnlySections;
    internal XmlNode _readWriteSection;

    private MSXmlDocXPathWriter _xPathWriter;

    public event EventHandler Saved;
    #endregion Declarations

    #region Constants
    private const bool DEFAULT_AUTOSAVE = false;
    //private const string CONFIG_ROOT_NODE = "configurator";
    //private const string CONFIG_ROOT_NAMESPACE = "http://xml.ms.com/ied/concordconfiguration/0.1";
    #endregion Constants

    #region Constructors
    /// <include file='xmldocs/Configurator.cs.xml' path='doc/doc[@for="Configurator.Configurator"]/*'/>
    public Configurator() 
    {
      _readOnlySections = new ArrayList();
      _autoSave = DEFAULT_AUTOSAVE;
    }
    #endregion Constructors

    #region Base Class Overrides
    public override object InitializeLifetimeService()
    {
      return null; // Infinite
    }

    #endregion Base Class Overrides

    #region Implementation of IConfigurationSectionHandler
    object IConfigurationSectionHandler.Create(object parent_, object configContext_, XmlNode section_) 
    {
      _readOnlySections.Add(section_);
      return this;
    }
    #endregion Implementation of IConfigurationSectionHandler

    #region Implementation of IConfigurationSectionHandlerWriter
    object IConfigurationSectionHandlerWriter.Create(object parent_, object configContext_, XmlNode section_) 
    {
      if (section_ == null) 
      {
        _readWriteSection = this.CreateConfig();
      } 
      else 
      {
        _readWriteSection = section_;
      }

      return this;
    }

    XmlNode IConfigurationSectionHandlerWriter.Serialize(object value_) 
    {
      return ((Configurator) value_)._readWriteSection;
    }
    #endregion Implementation of IConfigurationSectionHandlerWriter

    #region Public Properties
    /// <include file='xmldocs/Configurator.cs.xml' path='doc/doc[@for="Configurator.AutoSave"]/*'/>
    public bool AutoSave 
    {
      // AutoSave=True is invalid when a custom profile is not specified.  However,
      // property setting/getting should not throw exceptions.  The exception will be thrown
      // when Save is called for the first time.
      get
      { 
        lock(_lock) 
        {
          return _autoSave;
        }
      }
      set
      {
        lock(_lock) 
        {
          _autoSave = value;
        }
      }
    }

    /// <include file='xmldocs/Configurator.cs.xml' path='doc/doc[@for="Configurator.NameTable"]/*'/>
    public XmlNameTable NameTable 
    {
      get 
      {
        XmlDocument tempDoc = new XmlDocument();

        if (_readWriteSection != null)
        {
          tempDoc.ImportNode(_readWriteSection, true);
        }

        foreach(XmlNode node in _readOnlySections) 
        {
          tempDoc.ImportNode(node, true);
        }

        return tempDoc.NameTable;
      }
    }
    #endregion Public Properties

    #region Public Methods
    /// <include file='xmldocs/Configurator.cs.xml' path='doc/doc[@for="Configurator.GetValue"]/*'/>
    public string GetValue(string xpath_, XmlNamespaceManager nsmgr_) 
    {
      #region Logging
      string logMessage = null;
      if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
      {
        logMessage = string.Format(Res.GetString(Res.LOG_CONFIGURATOR_GETVALUE), xpath_);
      }
      #endregion Logging

      string xpath = this.PrepareXPath(xpath_);

      lock(_lock)
      {
        XmlNode node = null;

        if (_readWriteSection != null && _readWriteSection.OuterXml.Length > 0) 
        {
          node = _readWriteSection.SelectSingleNode(xpath, nsmgr_);
          if (node != null) 
          {
            #region Logging
            if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
            {
              LogLayer.Debug.Location("Configurator", "GetValue").Append(string.Format("{0}, Location='ReadWrite', Value='{1}'", logMessage, node.InnerText)).Send();
            }
            #endregion Logging

            return node.InnerText;
          }
        }

        for (int index = _readOnlySections.Count - 1; index >= 0; index--) 
        {
          node = ((XmlNode) _readOnlySections[index]).SelectSingleNode(xpath, nsmgr_);
          if (node != null) 
          {
            #region Logging
            if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
            {
              LogLayer.Debug.Location("Configurator", "GetValue").Append(string.Format("{0}, Location='ReadOnly', Value='{1}'", logMessage, node.InnerText)).Send();
            }
            #endregion Logging

            return node.InnerText;
          }
        }

        #region Logging
        if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
        {
            LogLayer.Debug.Location("Configurator", "GetValue").Append(string.Format("{0}, Value not found'", logMessage)).Send();
        }
        #endregion Logging

        return null;
      }
    }

    /// <include file='xmldocs/Configurator.cs.xml' path='doc/doc[@for="Configurator.GetNode"]/*'/>
    public XmlNode GetNode(string xpath_, XmlNamespaceManager nsmgr_) 
    {
      #region Logging
      string logMessage = null;
      if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
      {
        logMessage = string.Format(Res.GetString(Res.LOG_CONFIGURATOR_GETNODE), xpath_);
      }
      #endregion Logging

      string xpath = this.PrepareXPath(xpath_);

      lock(_lock) 
      {
        XmlNode customNode = null;
        XmlDocument mergedDoc = null;

        // Get the custom specific node
        if (_readWriteSection != null) 
        {
          customNode = _readWriteSection.SelectSingleNode(xpath, nsmgr_);
          if (customNode != null && customNode.NodeType == XmlNodeType.Document) 
          {
            customNode = ((XmlDocument) customNode).DocumentElement;
          }
        }

        // If it's not an element, just return it because there is nothing to merge
        if (customNode != null && (customNode.NodeType != XmlNodeType.Element || _readOnlySections == null)) 
        {
          #region Logging
          if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
          {
            LogLayer.Debug.Location("Configurator", "GetNode").Append(string.Format("{0}, Location='ReadWrite', Value='{1}'", logMessage, customNode.OuterXml)).Send();
          }
          #endregion Logging

          mergedDoc = new XmlDocument();
          XmlNode node = mergedDoc.ImportNode(customNode, true);
          return mergedDoc.AppendChild(node);
        } 
        else if (customNode != null)
        {
          mergedDoc = new XmlDocument();
          XmlNode node = mergedDoc.ImportNode(customNode, true);
          mergedDoc.AppendChild(node);
        }

        // Get the read only nodes
        foreach (XmlNode readOnlyNode in _readOnlySections) 
        {
          if (readOnlyNode == null) continue;

          XmlNode templateNode = readOnlyNode.SelectSingleNode(xpath, nsmgr_);

          if (templateNode != null) 
          {
            if (templateNode != null && customNode != null && templateNode.NodeType != customNode.NodeType) 
            {
              #region Logging
              if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Error)) 
              {
                  LogLayer.Error.Location("Configurator", "GetNode").Append(Res.GetString(Res.EXCEPTION_NON_MATCHING_NODETYPE)).Send();
              }
              #endregion Logging

              throw new ConfiguratorException(Res.GetString(Res.EXCEPTION_NON_MATCHING_NODETYPE));
            }
            else 
            {
              if (mergedDoc == null) 
              {
                #region Logging
                if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
                {
                  LogLayer.Debug.Location("Configurator", "GetNode").Append(string.Format("{0}, Location='ReadOnly', Value='{1}'", logMessage, templateNode.OuterXml)).Send();
                }
                #endregion Logging

                mergedDoc = new XmlDocument();
                XmlNode node = mergedDoc.ImportNode(templateNode, true);
                mergedDoc.AppendChild(node);
              } 
              else 
              {
                foreach (XmlNode child in templateNode.ChildNodes) 
                {
                  XmlNode node = mergedDoc.ImportNode(child, true);
                  mergedDoc.DocumentElement.AppendChild(node);
                }

                #region Logging
                if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
                {
                  LogLayer.Debug.Location("Configurator", "GetNode").Append(string.Format("{0}, Location='Merged', Value='{1}'", logMessage, mergedDoc.OuterXml)).Send();
                }
                #endregion Logging
              }
            }
          }
        }

        if (mergedDoc == null) 
        {
          #region Logging
          if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
          {
              LogLayer.Debug.Location("Configurator", "GetNode").Append(string.Format("{0}, Value not found'", logMessage)).Send();
          }
          #endregion Logging

          return null;
        } 
        else 
        {
          return mergedDoc.DocumentElement;
        }
      }
    }

    /// <include file='xmldocs/Configurator.cs.xml' path='doc/doc[@for="Configurator.SetValue"]/*'/>
    public void SetValue(string xpath_, XmlNamespaceManager nsmgr_, string value_) 
    {
      #region Logging
      if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
      {
          LogLayer.Debug.Location("Configurator", "SetValue").Append(string.Format(Res.GetString(Res.LOG_CONFIGURATOR_SETVALUE), xpath_, value_)).Send();
      }
      #endregion Logging

      if (_readWriteSection == null) 
      {
        #region Logging
        if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Error)) 
        {
            LogLayer.Error.Location("Configurator", "SetValue").Append(Res.GetString(Res.EXCEPTION_NEED_CUSTOMPROFILE_TO_EDIT)).Send();
        }
        #endregion Logging

        throw new InvalidOperationException(Res.GetString(Res.EXCEPTION_NEED_CUSTOMPROFILE_TO_EDIT));
      }

      lock(_lock) 
      {
        // Lazy Init
        if (_xPathWriter == null) 
        {
          _xPathWriter = new MSXmlDocXPathWriter(_readWriteSection);
        }

        // Delegate directly to MSXmlDocXPathWriter.SetValue()
        _xPathWriter.SetValue(this.PrepareXPath(xpath_), nsmgr_, value_);

        if (_autoSave) 
        {
          this.SaveInternal();
        }
      }
    }

    /// <include file='xmldocs/Configurator.cs.xml' path='doc/doc[@for="Configurator.DeleteNode"]/*'/>
    public void DeleteNode(string xpath_, XmlNamespaceManager nsmgr_) 
    {
      #region Logging
      if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
      {
        LogLayer.Debug.Location("Configurator", "DeleteNode").Append(string.Format(Res.GetString(Res.LOG_CONFIGURATOR_DELETENODE), xpath_)).Send();
      }
      #endregion Logging

      if (_readWriteSection == null) 
      {
        #region Logging
        if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Error)) 
        {
            LogLayer.Error.Location("Configurator", "DeleteNode").Append(Res.GetString(Res.EXCEPTION_NEED_CUSTOMPROFILE_TO_EDIT)).Send();
        }
        #endregion Logging

        throw new InvalidOperationException(Res.GetString(Res.EXCEPTION_NEED_CUSTOMPROFILE_TO_EDIT));
      }

      lock(_lock) 
      {
        XmlNode node = _readWriteSection.SelectSingleNode(this.PrepareXPath(xpath_), nsmgr_);

        if (node != null) 
        {
          if (node == _readWriteSection) 
          {
            #region Logging
            if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Warning)) 
            {
                LogLayer.Warning.Location("Configurator", "DeleteNode").Append(string.Format(Res.GetString(Res.LOG_DELETE_DOCUMENT_ELEMENT), xpath_)).Send();
            }
            #endregion Logging
          }

          node.ParentNode.RemoveChild(node);
        } 
        else 
        {
          #region Logging
          if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Error)) 
          {
              LogLayer.Error.Location("Configurator", "DeleteNode").Append(Res.GetString(Res.EXCEPTION_NODE_DOES_NOT_EXIST)).Send();
          }
          #endregion Logging

          throw new ConfiguratorException(Res.GetString(Res.EXCEPTION_NODE_DOES_NOT_EXIST));
        }

        if (_autoSave) 
        {
          this.SaveInternal();
        }
      }
    }

    /// <include file='xmldocs/Configurator.cs.xml' path='doc/doc[@for="Configurator.SetNode"]/*'/>
    public void SetNode(string xpath_, XmlNamespaceManager nsmgr_, XmlNode node_) 
    {
      #region Logging
      if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
      {
          LogLayer.Debug.Location("Configurator", "SetNode").Append(string.Format(Res.GetString(Res.LOG_CONFIGURATOR_SETNODE), xpath_, node_.OuterXml)).Send();
      }
      #endregion Logging

      if (_readWriteSection == null) 
      {
        #region Logging
        if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Error)) 
        {
            LogLayer.Error.Location("Configurator", "SetNode").Append(Res.GetString(Res.EXCEPTION_NEED_CUSTOMPROFILE_TO_EDIT)).Send();
        }
        #endregion Logging

        throw new InvalidOperationException(Res.GetString(Res.EXCEPTION_NEED_CUSTOMPROFILE_TO_EDIT));
      } 

      lock(_lock) 
      {
        // Lazy Init
        if (_xPathWriter == null) 
        {
          _xPathWriter = new MSXmlDocXPathWriter(_readWriteSection);
        }

        // Add the node to the current document
        XmlNode newNode = null;
        if (_readWriteSection.NodeType == XmlNodeType.Document)
        {
          newNode = ((XmlDocument) _readWriteSection).ImportNode(node_, true);
        } 
        else 
        {
          newNode = _readWriteSection.OwnerDocument.ImportNode(node_, true);
        }

        // Delegate directly to MSXmlDocXPathWriter.SetNode()
        _xPathWriter.SetNode(this.PrepareXPath(xpath_), nsmgr_, newNode);

        if (_autoSave) 
        {
          this.SaveInternal();
        }
      }
    }

    /// <include file='xmldocs/Configurator.cs.xml' path='doc/doc[@for="Configurator.AddNode"]/*'/>
    public void AddNode(string xpath_, XmlNamespaceManager nsmgr_, XmlNode node_) 
    {
      #region Logging
      if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug)) 
      {
          LogLayer.Debug.Location("Configurator", "AddNode").Append(string.Format(Res.GetString(Res.LOG_CONFIGURATOR_ADDNODE), xpath_, node_.OuterXml)).Send();
      }
      #endregion Logging

      if (_readWriteSection == null) 
      {
        #region Logging
        if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Error)) 
        {
            LogLayer.Error.Location("Configurator", "AddNode").Append(Res.GetString(Res.EXCEPTION_NEED_CUSTOMPROFILE_TO_EDIT)).Send();
        }
        #endregion Logging

        throw new InvalidOperationException(Res.GetString(Res.EXCEPTION_NEED_CUSTOMPROFILE_TO_EDIT));
      }

      lock(_lock) 
      {
        // Lazy Init
        if (_xPathWriter == null) 
        {
          _xPathWriter = new MSXmlDocXPathWriter(_readWriteSection);
        }

        // Add the node to the current document
        XmlNode newNode = null;
        if (_readWriteSection.NodeType == XmlNodeType.Document) 
        {
          newNode = ((XmlDocument) _readWriteSection).ImportNode(node_, true);
        } 
        else 
        {
          newNode = _readWriteSection.OwnerDocument.ImportNode(node_, true);
        }

        // Delegate directly to MSXmlDocXPathWriter.SetChild()
        _xPathWriter.SetChild(this.PrepareXPath(xpath_), nsmgr_, newNode);

        if (_autoSave) 
        {
          this.SaveInternal();
        }
      }
    }

    /// <include file='xmldocs/Configurator.cs.xml' path='doc/doc[@for="Configurator.Save"]/*'/>
    public void Save() 
    {
      #region Logging
      if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Info)) 
      {
          LogLayer.Info.Location("Configurator", "Save").Append(Res.GetString(Res.LOG_CONFIGURATOR_SAVE)).Send();
      }
      #endregion Logging

      if (_readWriteSection == null) 
      {
        #region Logging
        if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Error)) 
        {
            LogLayer.Error.Location("Configurator", "Save").Append(Res.GetString(Res.EXCEPTION_NEED_CUSTOMPROFILE_TO_SAVE)).Send();
        }
        #endregion Logging

        throw new InvalidOperationException(Res.GetString(Res.EXCEPTION_NEED_CUSTOMPROFILE_TO_SAVE));
      }

      lock(_lock) 
      {
        this.SaveInternal();
      }
    }
    #endregion Public Methods

    #region Protected Methods
    protected virtual void OnSaved(EventArgs args_) 
    {
      EventHandler handler = this.Saved;
      if (handler != null) 
      {
        handler(this, args_);
      }
    }
    #endregion Protected Methods

    #region Private Methods
    private XmlDocument CreateConfig() 
    {
      XmlDocument doc = new XmlDocument();
      return doc;
    }

    // This method is synchronized externally: see Save() and SetXXX()
    // If this was rolled up into Save() with method level locking, there would be
    // a race condition with SetXXX and Save because of autosaving.
    private void SaveInternal() 
    {
      if (_readWriteSection != null) 
      {
        ConfigurationManager.WriteConfig(this);
        this.OnSaved(EventArgs.Empty);
      }
    }

    private string PrepareXPath(string xpath_) 
    {
      if (xpath_.StartsWith("//")) 
      {
        return xpath_;
      }
      else if (xpath_.StartsWith("/"))
      {
        return xpath_.Substring(1);
      } 
      else 
      {
        return xpath_;
      }
    }
    #endregion Private Methods
  }
}
