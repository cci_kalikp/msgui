#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2007 Morgan Stanley, All Rights Reserved
  Unpublished copyright.  This material contains proprietary
  information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.Configuration/IConfigurationInterceptingWriter.cs#1 $
  $DateTime: 2013/04/11 16:41:48 $
    $Change: 823942 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion File Info Header

using System;

namespace MorganStanley.Desktop.Configuration
{
  /// <summary>
  /// Defines the contract that read-write configuration interceptors must implement.
  /// </summary>
  /// <remarks>
  /// <seealso cref="InterceptingConfigurationStore"/>
  /// </remarks>
  /// <example>
  /// Sample implementation that intercepts reads and converts a time attribute 
  /// to local time and intercepts writes to convert the attribute to universal time 
  /// 
  /// <code>
  ///  public class TimeZoneConfigurationInterceptor : IConfigurationInterceptingWriter
  ///  {
  ///    public void InterceptWrite(string name_, string version_, XmlNode node_, DecoratedWriteConfiguration decoratedWriteConfiguration_)
  ///    {
  ///      XmlNode converted = ConvertToUniversalTime(node_);
  ///      // Call WriteConfiguration on the actual storage provider.
  ///      decoratedWriteConfiguration_(name_, version_, converted);
  ///    }
  ///
  ///    public ConfigurationSection InterceptRead(ConfigurationSection original_)
  ///    {
  ///      XmlNode[] readOnlyNodes = new XmlNode[original_.ReadOnlyNodes.Length];
  ///      for (int i = 0; i  &lt; original_.ReadOnlyNodes.Length; i++)
  ///      {
  ///        if(original_.ReadOnlyNodes[i] !=null)
  ///        {
  ///          readOnlyNodes[i] = ConvertToLocalTime(original_.ReadOnlyNodes[i]);
  ///        }
  ///      }
  ///      XmlNode readWriteNode = null;
  ///      if(original_.ReadWriteNode !=null)
  ///      {
  ///        readWriteNode = ConvertToLocalTime(original_.ReadWriteNode);
  ///      }
  ///      return new ConfigurationSection(original_.Name, original_.Version, readOnlyNodes, readWriteNode);
  ///    }
  ///
  ///    private XmlNode ConvertToLocalTime(XmlNode node_)
  ///    {
  ///      XmlNode converted = node_.CloneNode(true);
  ///      string utcTime = converted.Attributes["time"].Value;
  ///      converted.Attributes["time"].Value = DateTime.Parse(utcTime).ToLocalTime().ToString();
  ///      return converted;
  ///    }
  ///
  ///    private XmlNode ConvertToUniversalTime(XmlNode node_)
  ///    {
  ///      XmlNode converted = node_.CloneNode(true);
  ///      string localTime = node_.Attributes["time"].Value;
  ///      node_.Attributes["time"].Value = DateTime.Parse(localTime).ToUniversalTime().ToString();
  ///      return converted;
  ///    }
  ///  }
  /// </code>
  /// 
  /// </example>
  public interface IConfigurationInterceptingWriter : IConfigurationInterceptingReader
  {
    /// <summary>
    /// Invoked when a request to write a configuration setting is made.
    /// </summary>
    /// <remarks>
    /// <note type="implementnotes">
    /// The <paramref name="decoratedWriteConfiguration_"/> paramater should be invoked after any modifications 
    /// to the node if you wish to write the modifications to the decorated provider. 
    /// </note>
    /// </remarks>
    /// <param name="name_">The name of the config</param>
    /// <param name="version_">The version of the config</param>
    /// <param name="node_">The xml to write</param>
    /// <param name="decoratedWriteConfiguration_">A delegate pointing to the original <see cref="IConfigurationStorageWriter.WriteConfiguration"/> method on the decorated provider</param>
    void InterceptWrite(string name_, string version_, System.Xml.XmlNode node_, InterceptingConfigurationStore.DecoratedWriteConfiguration decoratedWriteConfiguration_);
	}

 

}