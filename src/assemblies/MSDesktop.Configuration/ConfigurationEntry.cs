#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.Configuration/ConfigurationEntry.cs#1 $
  $DateTime: 2013/04/11 16:41:48 $
    $Change: 823942 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion

using System;
using System.Configuration;

namespace MorganStanley.Desktop.Configuration
{
  internal class ConfigurationEntry : ConfigurationIdentity
  {
    #region Declarations
    private object _configuration;
    private IConfigurationSectionHandler _sectionHandler;
    #endregion Declarations

    #region Constructors
    public ConfigurationEntry(string name_, string version_, Type handlerType_, Type providerType_, IConfigurationSectionHandler sectionHandler_, object configuration_) : base(name_, version_, handlerType_, providerType_) 
    {
      _configuration = configuration_;
      _sectionHandler = sectionHandler_;
    }
    #endregion Constructors

    #region Public Properties
    public object Configuration
    {
      get { return _configuration; }
    }

    public IConfigurationSectionHandler SectionHandler
    {
      get { return _sectionHandler; }
    }
    #endregion Public Properties
  }
}
