#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.Configuration/ConfigurationSectionAttribute.cs#1 $
  $DateTime: 2013/04/11 16:41:48 $
    $Change: 823942 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion

using System;

namespace MorganStanley.Desktop.Configuration
{
    /// <include file='xmldocs/ConfigurationSectionAttribute.cs.xml' path='doc/doc[@for="ConfigurationSectionAttribute"]/*'/>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class ConfigurationSectionAttribute : Attribute
    {
        #region Declarations

        #endregion Declarations

        #region Constructors
        /// <include file='xmldocs/ConfigurationSectionAttribute.cs.xml' path='doc/doc[@for="ConfigurationSectionAttribute.ConfigurationSectionAttribute"]/*'/>
        public ConfigurationSectionAttribute(string name_)
        {
            Name = name_;
        }

        /// <include file='xmldocs/ConfigurationSectionAttribute.cs.xml' path='doc/doc[@for="ConfigurationSectionAttribute.ConfigurationSectionAttribute1"]/*'/>
        public ConfigurationSectionAttribute(string name_, string version_)
            : this(name_)
        {
            Version = version_;
        }

        /// <include file='xmldocs/ConfigurationSectionAttribute.cs.xml' path='doc/doc[@for="ConfigurationSectionAttribute.ConfigurationSectionAttribute2"]/*'/>
        public ConfigurationSectionAttribute(string name_, string version_, Type handlerType_)
            : this(name_, version_)
        {
            HandlerType = handlerType_;
        }

        /// <include file='xmldocs/ConfigurationSectionAttribute.cs.xml' path='doc/doc[@for="ConfigurationSectionAttribute.ConfigurationSectionAttribute3"]/*'/>
        public ConfigurationSectionAttribute(string name_, string version_, Type handlerType_, Type providerType_)
            : this(name_, version_, handlerType_)
        {
            ProviderType = providerType_;
        }
        #endregion Constructors

        #region Public Properties

        /// <include file='xmldocs/ConfigurationSectionAttribute.cs.xml' path='doc/doc[@for="ConfigurationSectionAttribute.HandlerType"]/*'/>
        public Type HandlerType { get; set; }

        /// <include file='xmldocs/ConfigurationSectionAttribute.cs.xml' path='doc/doc[@for="ConfigurationSectionAttribute.ProviderType"]/*'/>
        public Type ProviderType { get; set; }

        /// <include file='xmldocs/ConfigurationSectionAttribute.cs.xml' path='doc/doc[@for="ConfigurationSectionAttribute.Name"]/*'/>
        public string Name { get; private set; }

        /// <include file='xmldocs/ConfigurationSectionAttribute.cs.xml' path='doc/doc[@for="ConfigurationSectionAttribute.Version"]/*'/>
        public string Version { get; private set; }

        #endregion Public Properties
    }
}
