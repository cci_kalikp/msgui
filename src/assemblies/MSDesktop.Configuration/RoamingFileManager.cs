#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.Configuration/RoamingFileManager.cs#1 $
  $DateTime: 2013/04/11 16:41:48 $
    $Change: 823942 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion File Info Header

using System;
using System.Collections;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Xml;
using MorganStanley.MSDotNet.MSLog;


using MorganStanley.MSDotNet.MSNet;
using MorganStanley.MSDotNet.MSSoap.Client;
using MorganStanley.MSDotNet.MSSoap.Server;
using MorganStanley.MSDotNet.MSSoap.Common;

using MorganStanley.IED.Concord.Runtime;

using ICSharpCode.SharpZipLib.GZip;

namespace MorganStanley.Desktop.Configuration
{
  #region RoamingFileManager Class
  /// <summary>
  /// Summary description for FileManagement.
  /// </summary>
  public class RoamingFileManager
  {
    #region Declarations

    private SoapHttpClient _client;
    private IMSNetLoop _loop;

    private string                  _application;       // Application SOAP Header
    private string                  _service = "";      // Service SOAP Header
    private string                  _serviceID = "";    // ServiceID SOAP Header
    private string                  _userID;            // UserID SOAP Header
    private string                  _version = "";      // Version SOAP Header
    private string                  _app = "";
    private static bool             _useNamespaceInHeader = true;

    #endregion Declarations

    #region Constants
    private string CLASS_NAME = "RoamingFileManager";

    private const int BLOCK_SIZE = 4096 * 4;

    private const string FILE_MANAGMENT_GET_FILES_SOAP_ACTION = "GetFiles";
    private const string FILE_MANAGMENT_GET_FILE_SOAP_ACTION = "GetFile";
    private const string FILE_MANAGMENT_SAVE_FILE_SOAP_ACTION = "SaveFileRequest";
    private const string FILE_MANAGMENT_DELETE_FILE_SOAP_ACTION = "DeleteFileRequest";
    private const string FILE_MANAGMENT_UPDATE_FILE_SOAP_ACTION = "UpdateFileRequest";
    private const string FILE_MANAGMENT_VALIDATE_FILE_SOAP_ACTION = "ValidateFilesRequest";
    private const string FILE_MANAGMENT_GET_FILE_NAMES_SOAP_ACTION = "GetUserFileNamesRequest";
    private const string FILE_MANAGEMENT_INSERT_USER_SOAP_ACTION = "InsertUserRequest";

    private const string NAMESPACE_MSDW_SOAP_URI = "http://xml.msdw.com/ns/soap/";
    private const string NAMESPACE_MSDW_SOAP_PREFIX = "MSDW-SOAP";
    private const string NAMESPACE_MSDW_AUTH_URI = "http://xml.msdw.com/ns/appmw/xgate/authentication";    
    private const string NAMESPACE_MSDW_AUTH_PREFIX = "MSDW-AUTH";

    private const string MSDW_SOAP_HEADER_APPLICATION_TAG = "Application";
    private const string MSDW_SOAP_HEADER_SERVICE_ID_TAG = "ServiceID";
    private const string MSDW_SOAP_HEADER_SERVICE_TAG = "Service";
    private const string MSDW_SOAP_HEADER_VERSION_TAG = "Version";
    private const string MSDW_SOAP_HEADER_USER_ID_TAG = "UserID";
    private const string MSDW_SOAP_HEADER_WIN32_USER_TAG = "Win32User";
    private const string MSDW_SOAP_HEADER_MACHINE_NAME_TAG = "MachineName";

    private const string MSDW_SOAP_HEADER_RETURN_TYPE_TAG = "ReturnType";
    private const string MSDW_SOAP_HEADER_RETURN_TYPE_VALUE = "SOAP";
    private const string MSDW_SOAP_HEADER_SENDING_TIME_TAG = "SendingTime";
    private const string MSDW_SOAP_HEADER_REQUEST_ID_TAG = "RequestID";
    private const string MSDW_SOAP_AUTH_HEADER_AUTHENTICATION_TAG = "Authentication";
    private const string MSDW_SOAP_AUTH_HEADER_USER_TAG = "User";
    private const string MSDW_SOAP_AUTH_HEADER_USER_XPATH = NAMESPACE_MSDW_AUTH_PREFIX + ":" + MSDW_SOAP_AUTH_HEADER_AUTHENTICATION_TAG + "/" + NAMESPACE_MSDW_AUTH_PREFIX + ":" + MSDW_SOAP_AUTH_HEADER_USER_TAG;
    private const string MSDW_SOAP_AUTH_HEADER_TOKEN_TAG = "Token";
    private const string MSDW_SOAP_AUTH_HEADER_TOKEN_XPATH = NAMESPACE_MSDW_AUTH_PREFIX + ":" + MSDW_SOAP_AUTH_HEADER_AUTHENTICATION_TAG + "/" + NAMESPACE_MSDW_AUTH_PREFIX + ":" + MSDW_SOAP_AUTH_HEADER_TOKEN_TAG;
    private const string NAMESPACE_HEADER = "xmlns:msdwHdr='http://xml.msdw.com/ns/appmw/soap/1.0/header'";

    private const string GET_ROAMING_FILES = "GetRoamingFiles";
    private const string SOAP_HEADER = "Soap-ENV:Header";
    private const string SOAP_BODY = "Soap-ENV:Body";
    private const string SERVICE_ID = "ServiceID";
    private const string REQUEST_ID = "RequestID";
    private const string SERVICE = "Service";
    private const string VERSION = "Version";
    private const string APPLICATION = "Application";
    private const string USER_ID = "UserID";
    private const string MACHINE_NAME = "MachineName";
    private const string WIN_32_USER = "Win32User";
    private const string RETURN_TYPE = "ReturnType";
    private const string TIMESTAMP = "Timestamp";
    private const string USERNAME = "UserName";
    private const string APP_VERSION = "AppVersion";
    private const string RELEASE_TYPE = "ReleaseType";

    #endregion Constants

    #region Constructors

    public RoamingFileManager()
    {
      if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug))
      {
        LogLayer.Debug.Location(CLASS_NAME, "RoamingFileManager").Append("RoamingFileManager Service created without a loop.");
      }
    }

    public RoamingFileManager(string hostPort_, string app_)
    {
      _loop = MSNetLoopPoolImpl.Instance();
      _client = new SoapHttpClient(_loop, hostPort_);

      _app = app_;

      if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug))
      {
        LogLayer.Debug.Location(CLASS_NAME, "RoamingFileManager").Append("RoamingFileManager Service created with host:port data");
      }
    }

    public RoamingFileManager(IMSNetLoop invokerLoop_)
    {
      _loop = invokerLoop_;

      if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Debug))
      {
        LogLayer.Debug.Location(CLASS_NAME, "RoamingFileManager").Append("RoamingFileManager Service created with a loop.");
      }
    }
    #endregion Constructors

    #region Public Methods
      
    #region GetUserFileNames
    public FileIdentityCollection GetUserFileNames(string user_) 
    {
      Debug.Assert(user_ != null, "user_ == null");

      FileIdentityCollection _fileIdentityCollection = new FileIdentityCollection();
      string template = CreateTemplateMessage();
      SoapMessage request = this.SetGetFileNamesRequest(template);
      
      try
      {
        SoapMessage response = this.SendMessage(request);
        _fileIdentityCollection = ExtractFileIdentitiesFromResponse(response);
      }
      catch (System.Net.WebException ex_)
      {
        if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Error)) 
        {
          LogLayer.Error.Location(CLASS_NAME, "SaveFile").Append("Unable to connect", ex_);
        }
      }
      catch (Exception ex_)
      {
        if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Error)) 
        {
          LogLayer.Error.Location(CLASS_NAME, "SaveFile").Append("Error sending message", ex_);
        }
      }

      return _fileIdentityCollection;
    }
    #endregion GetUserFileNames

    #region GetFile
    public RoamingFile GetFile(string user_, string name_, string path_, string version_) 
    {
      return this.GetFile(user_, new FileIdentity(name_, path_, version_, null, null));
    }

    public RoamingFile GetFile(string user_, FileIdentity file_) 
    {
      FileIdentityCollection files = new FileIdentityCollection();
      files.Add(file_);

      RoamingFile result = null;
      string template = CreateTemplateMessage();
      SoapMessage request = this.SetGetFilesRequest(files, template);
      try 
      {
        SoapMessage response = this.SendMessage(request);
        result = ExtractFileFromResponse(response);
      }
      catch (Exception ex_)
      {
        if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Error)) 
        {
          LogLayer.Error.Location(CLASS_NAME, "SaveFile").Append("Error sending message", ex_);
        }
      }

      return result;
    }
    #endregion GetFile

    #region GetFiles
    public RoamingFileCollection GetFiles(string user_, FileIdentityCollection files_) 
    {
      // TODO: GetFiles
      return null;
    }
    #endregion GetFiles

    #region InsertUser
    public string InsertUser(string userApplicationId_, string app_, string userId_, string machineName_, string envKey_, string envValue_, string resource_) 
    {   
      string status = null;
      
      string template = CreateTemplateMessage();
      SoapMessage request = this.SetInsertUserRequest(userApplicationId_, app_, userId_, machineName_, envKey_, envValue_, resource_, template);
      
      try
      {
        SoapMessage response = this.SendMessage(request);
        status = ((SoapActionDomImpl) response.Action).XmlElement.InnerText;
      }
      catch (Exception ex_)
      {
        if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Error)) 
        {
          LogLayer.Error.Location(CLASS_NAME, "InsertUser").Append("Unable to insert new user", ex_);
        }
      }
      return status;
    }
    #endregion InsertUser

    #region SaveFile
    public bool SaveFile(string user_, RoamingFile file_) 
    {   
      string status = null;
      string template = CreateTemplateMessage();
      SoapMessage request = this.SetSaveFileRequest(file_, template);
      
      try
      {
        SoapMessage response = this.SendMessage(request);
        status = ((SoapActionDomImpl) response.Action).XmlElement.InnerText;
      }
      catch (Exception ex_)
      {
        if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Error)) 
        {
          LogLayer.Error.Location(CLASS_NAME, "SaveFile").Append("Error sending message", ex_);
        }
      }

      if (status == "0")
      {
        return true;
      }
      else
      {
        return false;
      }
    }
    #endregion SaveFile

    #region DeflateAndEncodeFile
    public static string DeflateAndEncodeFile(string data_)
    {
      byte[] uncompressedData = System.Text.Encoding.UTF8.GetBytes(data_);
      byte[] compressedData = null;
      
      using (MemoryStream outputStream = new MemoryStream()) 
      {
        using (GZipOutputStream zipStream = new GZipOutputStream(outputStream))
        {
          zipStream.Write(uncompressedData, 0, uncompressedData.Length);
          zipStream.Finish();
          outputStream.Flush();
          compressedData = outputStream.ToArray();
        }
      }

      return Convert.ToBase64String(compressedData);
    }
    #endregion DeflateAndEncodeFile

    #region DeleteFile
    // TODO: DeleteFile
    #endregion DeleteFile

    #region UpdateFile
    // TODO: UpdateFile
    #endregion UpdateFile

    #region ValidateFiles
    // TODO: ValidateFiles
    #endregion ValidateFiles

    #endregion Public Methods

    #region Private Methods

    #region SendMessage
    public SoapMessage SendMessage(SoapMessage request_)
    {
      SoapMessage response = _client.SyncSendRequest(request_, 10000);
      return response;
    }
    #endregion SendMessage

    #region CreateHeaderEntry
    private SoapHeaderDomImpl.EntryImpl CreateHeaderEntry(SoapHeaderDomImpl header_, string entry_, string value_)
    {
      SoapHeaderDomImpl.EntryImpl entry;

      entry = header_.CreateEntry(entry_);
      entry.Value = value_;
      header_.AddEntry(entry);
      return entry;
    }
    #endregion CreateHeaderEntry

    #region CreateHeader
    private string CreateHeader(string title_, string content_)
    {
      return string.Format("<msdwHdr:{0} "+ NAMESPACE_HEADER+ ">{1}</msdwHdr:{0}>", title_, content_);
    }
    #endregion CreateHeader

    #region CreateHeaders
    private string CreateHeaders()
    {
      TimeSpan ts = DateTime.Now-new DateTime(1970,1,1);
      string timestamp = CreateHeader("Timestamp", Math.Round(new Decimal(ts.TotalMilliseconds)).ToString());

      MemoryStream soapStream = new MemoryStream();
      XmlTextWriter xmlWriter = new XmlTextWriter(soapStream, Encoding.ASCII);

      xmlWriter.WriteRaw("<Soap-ENV:Envelope xmlns:Soap-ENV='http://schemas.xmlsoap.org/soap/envelope/'>");
      xmlWriter.WriteStartElement(SOAP_HEADER);
      xmlWriter.WriteRaw(CreateHeader(SERVICE_ID, "Concord"));
      xmlWriter.WriteRaw(CreateHeader(REQUEST_ID, Guid.NewGuid().ToString()));
      xmlWriter.WriteRaw(CreateHeader(SERVICE, GET_ROAMING_FILES));
      xmlWriter.WriteRaw(CreateHeader(VERSION, "1.0"));
      xmlWriter.WriteRaw(CreateHeader(APPLICATION, _app));
      xmlWriter.WriteRaw(CreateHeader(USER_ID, ConcordRuntime.Current.UserInfo.Username));
      xmlWriter.WriteRaw(CreateHeader(MACHINE_NAME, "Concord"));
      xmlWriter.WriteRaw(CreateHeader(WIN_32_USER, Environment.UserName));
      xmlWriter.WriteRaw(CreateHeader(RETURN_TYPE, "SOAP"));
      xmlWriter.WriteRaw(timestamp);
      xmlWriter.WriteRaw(CreateHeader(USERNAME, ConcordRuntime.Current.UserInfo.Username));
      xmlWriter.WriteRaw(CreateHeader(APP_VERSION, "1.0"));
      xmlWriter.WriteEndElement();

      xmlWriter.Flush();
      soapStream.Seek(0, SeekOrigin.Begin);
      string headers = new String(ASCIIEncoding.ASCII.GetChars(soapStream.ToArray()));
      return headers;
    }
    #endregion CreateHeaders

    #region ExtractFileFromResponse
    public RoamingFile ExtractFileFromResponse(SoapMessage message_) 
    {
      XmlNodeList rows = ((SoapActionDomImpl) message_.Action).XmlElement.SelectNodes("File");

      if (0 == rows.Count || ((rows.Count > 0) && (0 == rows[0].SelectSingleNode("File").InnerText.Length)))
      {
        return null;
      } 
      else 
      {
        return new RoamingFile(rows[0].SelectSingleNode("FileName").InnerText, rows[0].SelectSingleNode("FileId").InnerText, rows[0].SelectSingleNode("UserId").InnerText, rows[0].SelectSingleNode("UserApplicationId").InnerText, rows[0].SelectSingleNode("Application").InnerText, rows[0].SelectSingleNode("Version").InnerText, rows[0].SelectSingleNode("EnvironmentKey").InnerText, rows[0].SelectSingleNode("EnvironmentValue").InnerText, rows[0].SelectSingleNode("Created").InnerText, rows[0].SelectSingleNode("Updated").InnerText, rows[0].SelectSingleNode("LastEdited").InnerText, rows[0].SelectSingleNode("CheckSum").InnerText, rows[0].SelectSingleNode("LocalFilePath").InnerText, rows[0].SelectSingleNode("LocalId").InnerText, rows[0].SelectSingleNode("File").InnerText);
      }
    }

    private RoamingFileCollection ExtractFilesFromResponse(SoapMessage message_) 
    {
      RoamingFileCollection files = new RoamingFileCollection();
      XmlNodeList rows = ((SoapActionDomImpl) message_.Action).XmlElement.SelectNodes("File");

      foreach (XmlNode row in rows) 
      {
        if (0 == row.SelectSingleNode("File").InnerText.Length) 
        {
          if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Info)) 
          {
            LogLayer.Info.Location(CLASS_NAME, "ExtractFilesFromResponse").Append("Configuration file not found: Name=" + row.SelectSingleNode("FileName").InnerText);
          }
        } 
        else 
        {
          string fileName = null;

          try 
          {
            fileName = row.SelectSingleNode("FileName").InnerText;
            files.Add(fileName, row.SelectSingleNode("FileId").InnerText, row.SelectSingleNode("UserId").InnerText, row.SelectSingleNode("UserApplicationId").InnerText, row.SelectSingleNode("Application").InnerText, row.SelectSingleNode("Version").InnerText, row.SelectSingleNode("EnvironmentKey").InnerText, row.SelectSingleNode("EnvironmentValue").InnerText, row.SelectSingleNode("Created").InnerText, row.SelectSingleNode("Updated").InnerText, row.SelectSingleNode("LastEdited").InnerText, row.SelectSingleNode("CheckSum").InnerText, row.SelectSingleNode("LocalFilePath").InnerText, row.SelectSingleNode("LocalId").InnerText, DecodeAndInflateFile(row.SelectSingleNode("File").InnerText));
          } 
          catch (Exception ex_) 
          {
            if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Error)) 
            {
              LogLayer.Error.Location(CLASS_NAME, "ExtractFilesFromResponse").Append("Error parsing roaming file: " + fileName + ex_);
            }
          }
        }
      }

      if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Info)) 
      {
        LogLayer.Info.Location(CLASS_NAME, "ExtractFilesFromResponse").Append("File Count: " + files.Count.ToString());
      }

      return files;
    }

    private FileIdentityCollection ExtractFileIdentitiesFromResponse(SoapMessage message_) 
    {
      FileIdentityCollection files = new FileIdentityCollection();
      XmlNodeList rows = ((SoapActionDomImpl) message_.Action).XmlElement.SelectNodes("File");

      foreach (XmlNode row in rows) 
      {
        if (0 == row.SelectSingleNode("File").InnerText.Length) 
        {
          if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Info)) 
          {
            LogLayer.Info.Location(CLASS_NAME, "ExtractFileIdentitiesFromResponse").Append("Configuration file not found: Name=" + row.SelectSingleNode("FileName").InnerText);
          }
        } 
        else 
        {
          string fileName = null;

          try 
          {
            fileName = row.SelectSingleNode("FileName").InnerText;
            files.Add(fileName, row.SelectSingleNode("LocalFilePath").InnerText, row.SelectSingleNode("Version").InnerText, row.SelectSingleNode("CheckSum").InnerText, row.SelectSingleNode("FileId").InnerText);
          } 
          catch (Exception ex_) 
          {
            if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Error)) 
            {
              LogLayer.Error.Location(CLASS_NAME, "ExtractFileIdentitiesFromResponse").Append("Error parsing roaming file: " + fileName + ex_);
            }
          }
        }
      }

      if ((MSLog.GetThresholdPolicy(MSLogTarget.Default).GetLayerThreshold(new MSLogLayer("MSDesktop/Configuration/*")) >= MSLogPriority.Info)) 
      {
        LogLayer.Info.Location(CLASS_NAME, "ExtractFileIdentitiesFromResponse").Append("File Count: " + files.Count.ToString());
      }

      return files;
    }

    #endregion ExtractFileFromResponse

    #region DecodeAndInflateFile
    internal static string DecodeAndInflateFile(string data_)
    {
      Debug.Assert(data_ != null, "Data was null.");
      
      byte[] data = Convert.FromBase64String(data_);

      using (MemoryStream outStream = new MemoryStream(), inStream = new MemoryStream(data)) 
      {
        using (GZipInputStream zipStream = new GZipInputStream(inStream))
        {
          byte[] buffer = new Byte[BLOCK_SIZE];
        
          int sizeRead = zipStream.Read(buffer, 0, buffer.Length);
          while(sizeRead > 0)
          {
            outStream.Write(buffer, 0, sizeRead);
            sizeRead = zipStream.Read(buffer, 0, buffer.Length);
          }
        }

        outStream.Flush();
        outStream.Position = 0;
        
        return new StreamReader(outStream).ReadToEnd();
      }
    }
    #endregion DecodeAndInflateFile

    #region CreateTemplateMessage
    private string CreateTemplateMessage()
    {
      return this.CreateHeaders();
    }
    #endregion CreateTemplateMessage

    #region SetSaveFileRequest
    private SoapMessage SetSaveFileRequest(RoamingFile file_, string message_) 
    {
      StringWriter writer = new StringWriter();
      XmlTextWriter xmlWriter = new XmlTextWriter(writer);

      xmlWriter.WriteStartElement(FILE_MANAGMENT_SAVE_FILE_SOAP_ACTION);
      xmlWriter.WriteElementString("UserId", ConcordRuntime.Current.UserInfo.Username);
      xmlWriter.WriteStartElement("File");
      if (file_.FileName != null) xmlWriter.WriteElementString("FileName", file_.FileName);
      if (file_.UserId != null) xmlWriter.WriteElementString("UserId", file_.UserId);
      if (file_.UserApplicationId != null) xmlWriter.WriteElementString("UserApplicationId", file_.UserApplicationId);
      if (file_.Application != null) xmlWriter.WriteElementString("Application", file_.Application);
      if (file_.FileVersion != null) xmlWriter.WriteElementString("Version", file_.FileVersion);
      if (file_.Updated != null) xmlWriter.WriteElementString("Updated", file_.Updated);
      if (file_.Created != null) xmlWriter.WriteElementString("Created", file_.Created);
      if (file_.EnvironmentKey != null) xmlWriter.WriteElementString("EnvironmentKey", file_.EnvironmentKey);
      if (file_.EnvironmentValue != null) xmlWriter.WriteElementString("EnvironmentValue", file_.EnvironmentValue);
      if (file_.LastEdited != null) xmlWriter.WriteElementString("LastEdited", file_.LastEdited);
      if (file_.FilePath != null) xmlWriter.WriteElementString("LocalFilePath", file_.FilePath);
      if (file_.FileCRC != null) xmlWriter.WriteElementString("CheckSum", file_.FileCRC);
      if (file_.FileContent != null) xmlWriter.WriteElementString("File", DeflateAndEncodeFile(file_.FileContent));
      if (file_.FileId != null) xmlWriter.WriteElementString("FileId", file_.FileId);
      if (file_.LocalId != null) xmlWriter.WriteElementString("LocalId", file_.LocalId);
      
      xmlWriter.WriteEndElement();
      xmlWriter.WriteEndElement();

      return this.InsertBody(message_, writer.ToString());
    }
    #endregion SetSaveFileRequest

    #region SetGetFileNamesRequest
    private SoapMessage SetGetFileNamesRequest(string message_) 
    {
      StringWriter writer = new StringWriter();
      XmlTextWriter xmlWriter = new XmlTextWriter(writer);

      xmlWriter.WriteStartElement(FILE_MANAGMENT_GET_FILE_NAMES_SOAP_ACTION);
      xmlWriter.WriteElementString("UserId", ConcordRuntime.Current.UserInfo.Username);
      xmlWriter.WriteEndElement();
      
      return this.InsertBody(message_, writer.ToString());
    }
    #endregion SetGetFileNamesRequest

    #region SetInsertUserRequest
    private SoapMessage SetInsertUserRequest(string userApplicationId_, string applicationId_, string userId_, string machineName_, string envKey_, string envValue_, string resource_, string message_) 
    {
      StringWriter writer = new StringWriter();
      XmlTextWriter xmlWriter = new XmlTextWriter(writer);

      xmlWriter.WriteStartElement(FILE_MANAGEMENT_INSERT_USER_SOAP_ACTION);
      xmlWriter.WriteElementString("UserId", ConcordRuntime.Current.UserInfo.Username);
      if (userApplicationId_ != null) xmlWriter.WriteElementString("UserApplicationId", userApplicationId_);
      if (applicationId_ != null) xmlWriter.WriteElementString("ApplicationId", applicationId_);
      if (machineName_ != null) xmlWriter.WriteElementString("MachineName", machineName_);
      if (envKey_ != null) xmlWriter.WriteElementString("EnvironmentKey", envKey_);
      if (envValue_ != null) xmlWriter.WriteElementString("EnvironmentValue", envValue_);
      if (resource_ != null) xmlWriter.WriteElementString("Resource", resource_);
      xmlWriter.WriteEndElement();
      
      return this.InsertBody(message_, writer.ToString());
    }
    #endregion SetInsertUserRequest

    #region SetGetFilesRequest
    private SoapMessage SetGetFilesRequest(FileIdentityCollection files, string message_) 
    {
      StringWriter writer = new StringWriter();
      XmlTextWriter xmlWriter = new XmlTextWriter(writer);

      xmlWriter.WriteStartElement(FILE_MANAGMENT_GET_FILES_SOAP_ACTION);
      xmlWriter.WriteElementString("UserId", ConcordRuntime.Current.UserInfo.Username);
      xmlWriter.WriteStartElement("FilesList");
      foreach (FileIdentity file in files) 
      {
        xmlWriter.WriteStartElement("File");
        xmlWriter.WriteElementString("FileName", file.Name);
        xmlWriter.WriteElementString("LocalFilePath", file.Path);
        xmlWriter.WriteElementString("Version", file.Version);
        xmlWriter.WriteEndElement();
      }
      xmlWriter.WriteEndElement();
      xmlWriter.WriteEndElement();
      
      return this.InsertBody(message_, writer.ToString());
    }
    #endregion SetGetFilesRequest

    #region InsertBody
    private SoapMessage InsertBody(string message_, string xml_) 
    {
      MemoryStream soapStream = new MemoryStream();
      XmlTextWriter xmlWriter = new XmlTextWriter(soapStream, Encoding.ASCII);
      xmlWriter.WriteRaw(message_);
      xmlWriter.WriteStartElement(SOAP_BODY);
      xmlWriter.WriteRaw(xml_);
      xmlWriter.WriteEndElement();
      xmlWriter.WriteRaw("</Soap-ENV:Envelope>");
      xmlWriter.Flush();

      byte[] data = new byte[soapStream.Length];
      Array.Copy(soapStream.GetBuffer(), 0, data, 0, Convert.ToInt32(soapStream.Length));
      SoapMessage fullMessage = new SoapMessage(new SoapIOBuffer(data));
      
      soapStream.Seek(0, SeekOrigin.Begin);
      string headers = new String(ASCIIEncoding.ASCII.GetChars(soapStream.ToArray()));

      return fullMessage;
    }
    #endregion InsertBody

    #endregion Private Methods

    #region Public Properties

    /// <include file='xmldocs/XmlServiceDispatcher.cs.xml' path='doc/doc[@for="XmlServiceDispatcher.Application"]/*'/>
    public virtual string Application 
    {
      get { return _application; }
      set { _application = value; }
    }

    /// <include file='xmldocs/XmlServiceDispatcher.cs.xml' path='doc/doc[@for="XmlServiceDispatcher.ServiceID"]/*'/>
    public virtual string ServiceID
    {
      get { return _serviceID; }
      set { _serviceID = value; }
    }

    /// <include file='xmldocs/XmlServiceDispatcher.cs.xml' path='doc/doc[@for="XmlServiceDispatcher.Service"]/*'/>
    public virtual string Service
    {
      get { return _service; }
      set { _service = value; }
    }

    /// <include file='xmldocs/XmlServiceDispatcher.cs.xml' path='doc/doc[@for="XmlServiceDispatcher.UserID"]/*'/>
    public virtual string UserID
    {
      get { return _userID; }
      set { _userID = value; }
    }

    /// <include file='xmldocs/XmlServiceDispatcher.cs.xml' path='doc/doc[@for="XmlServiceDispatcher.Version"]/*'/>
    public virtual string Version
    {
      get { return _version; }
      set { _version = value; }
    }

    /// <include file='xmldocs/XmlServiceDispatcher.cs.xml' path='doc/doc[@for="XmlServiceDispatcher.UseNamespaceInHeader"]/*'/>
    public static bool UseNamespaceInHeader
    {
      get { return _useNamespaceInHeader; }
      set { _useNamespaceInHeader = value; }
    }
    #endregion Public Properties

  }
  #endregion RoamingFileManager Class

  #region FileIdentity Class
  public class FileIdentity 
  {
    #region Private Members
    private string _name;
    private string _path;
    private string _version;
    private string _checksum;
    private string _fileId;
    #endregion Private Members

    #region Constructors

    public FileIdentity(string name_, string path_, string version_) 
    {
      _name = name_;
      _path = path_;
      _version = version_;
    }

    public FileIdentity(string name_, string path_, string version_, string checksum_, string fileId_) 
    {
      _name = name_;
      _path = path_;
      _version = version_;
      _checksum = checksum_;
      _fileId = fileId_;
    }
    #endregion Constructors

    #region Public Methods
    public static FileIdentity UpdateChecksum(FileIdentity file_, string checksum_) 
    {
      file_.Checksum = checksum_;
      return file_;
    }
    #endregion Public Methods

    #region Public Properties
    public string Name 
    {
      [DebuggerStepThrough]
      get { return _name; }
    }

    public string Path 
    {
      [DebuggerStepThrough]
      get { return _path; }
    }

    public string Version 
    {
      [DebuggerStepThrough]
      get { return _version; }
    }

    public string Checksum 
    {
      [DebuggerStepThrough]
      get { return _checksum; }
      set { _checksum = value; }
    }

    public string FileId 
    {
      [DebuggerStepThrough]
      get { return _fileId; }
    }

    #endregion Public Properties
  }
  #endregion FileIdentity Class

  #region FileIdentityCollection Class
  public class FileIdentityCollection: CollectionBase
  {
    #region Constructors
    public FileIdentityCollection() 
    {
    }
    #endregion Constructors
		
    #region Public Methods
    public FileIdentity this[int index_] 
    {
      get	{ return (FileIdentity) this.List[index_]; }
    }
		
    public int Add(FileIdentity file_) 
    {
      return this.List.Add(file_);
    }

    public int Add(string name_, string path_, string version_, string checksum_, string fileId_) 
    {
      return this.Add(new FileIdentity(name_, path_, version_, checksum_, fileId_));
    }
		
    public void Remove(FileIdentity file_) 
    {
      List.Remove(file_);
    }

    public bool Contains(FileIdentity file_)
    {
      Debug.Assert(null != file_, "null == file_");
      if (null != file_)
      {
        return List.Contains(file_);
      }
      return false;
    }

    /// <summary>
    /// Checks the existance of 1st file found with the matching name
    /// </summary>
    /// <param name="fileName_">File Name</param>
    /// <returns>RoamingFile</returns>
    public bool Contains(string fileName_)
    {
      Debug.Assert(fileName_ != null && fileName_ != string.Empty, "fileName_ == null || fileName_ == string.Empty");
      bool result = false;
      foreach (FileIdentity file in List)
      {
        if (file.Name == fileName_)
        {
          result = true;
          break;
        }
      }
      return result;
    }

    public FileIdentity Get(string fileName_)
    {
      Debug.Assert(fileName_ != null && fileName_ != string.Empty, "fileName_ == null || fileName_ == string.Empty");
      foreach (FileIdentity file in List)
      {
        if (file.Name == fileName_)
        {
          return file;
        }
      }
      return null;
    }
		
    public new FileIdentityCollectionEnumerator GetEnumerator()	
    {
      return new FileIdentityCollectionEnumerator(this);
    }
    #endregion Public Methods

    #region Public Inner Classes
    public class FileIdentityCollectionEnumerator : IEnumerator	
    {
      #region Declarations
      private	IEnumerator _enumerator;
      private	IEnumerable _temp;
      #endregion Declarations
			
      public FileIdentityCollectionEnumerator(FileIdentityCollection mappings_)
      {
        _temp =	((IEnumerable)(mappings_));
        _enumerator = _temp.GetEnumerator();
      }
			
      public FileIdentity Current
      {
        get { return (FileIdentity) _enumerator.Current; }
      }
			
      object IEnumerator.Current
      {
        get { return _enumerator.Current; }
      }
			
      public bool MoveNext()
      {
        return _enumerator.MoveNext();
      }
			
      bool IEnumerator.MoveNext()
      {
        return _enumerator.MoveNext();
      }
			
      public void Reset()
      {
        _enumerator.Reset();
      }
			
      void IEnumerator.Reset() 
      {
        _enumerator.Reset();
      }
    }
    #endregion Public Inner Classes
  }
  #endregion FileIdentityCollection Class

  #region RoamingFile Class
  public class RoamingFile 
  {
    #region Declarations

    private string _filename, _fileId, _userId, _userApplicationId, _application, _version, _environmentKey, _environmentValue, _updated, _created, _lastEdited, _checksum, _localFilePath, _localId, _content;

    #endregion Declarations

    #region Constructors
    internal RoamingFile(string filename_, string fileId_, string userId_, string userApplicationId_, string application_, string version_, string environmentKey_, string environmentValue_, string updated_, string created_, string lastEdited_, string checksum_, string localFilePath_, string localId_, string content_)
    {
      _filename = filename_;
      _fileId = fileId_;
      _userId = userId_;
      _userApplicationId = userApplicationId_;
      _application = application_;
      _version = version_;
      _environmentKey = environmentKey_;
      _environmentValue = environmentValue_;
      _created = created_;
      _updated = updated_;
      _lastEdited = lastEdited_;
      _checksum = checksum_;
      _localFilePath = localFilePath_;
      _localId = localId_;
      _content = content_;
    }

    #endregion Constructors

    #region Public Properties
    public string FileName 
    {
      [DebuggerStepThrough]
      get { return _filename; }
    }

    public string FileId 
    {
      [DebuggerStepThrough]
      get { return _fileId; }
    }

    public string UserId 
    {
      [DebuggerStepThrough]
      get { return _userId; }
    }

    public string UserApplicationId 
    {
      [DebuggerStepThrough]
      get { return _userApplicationId; }
    }

    public string Application 
    {
      [DebuggerStepThrough]
      get { return _application; }
    }

    public string FileVersion 
    {
      [DebuggerStepThrough]
      get { return _version; }
    }

    public string EnvironmentKey  
    {
      [DebuggerStepThrough]
      get { return _environmentKey ; }
    }

    public string EnvironmentValue  
    {
      [DebuggerStepThrough]
      get { return _environmentValue ; }
    }

    public string Created 
    {
      [DebuggerStepThrough]
      get { return _created; }
    }

    public string Updated 
    {
      [DebuggerStepThrough]
      get { return _updated; }
    }

    public string LastEdited 
    {
      [DebuggerStepThrough]
      get { return _lastEdited; }
    }

    public string FileCRC 
    {
      [DebuggerStepThrough]
      get { return _checksum; }
    }

    public string FilePath 
    {
      [DebuggerStepThrough]
      get { return _localFilePath; }
    }
    
    public string LocalId 
    {
      [DebuggerStepThrough]
      get { return _localId; }
    }

    public string FileContent 
    {
      [DebuggerStepThrough]
      get { return _content; }
    }
    #endregion Public Properties

    public override string ToString() 
    {
      return this.FileName;
    }
  }
  #endregion RoamingFile Class

  #region SharedFile Class

  public class SharedFile
  {
    #region Declarations
    public string Name; 
    public string FileId;
    public string Application;
    public string Owner;
    public string Version;
    public string Created;
    public string Updated;
    public string LastEdited;
    public string Checksum;
    public string File;
    #endregion Declarations

    #region Constructors
    public SharedFile(ArrayList fields_)
    {
      object[] file = (object[]) fields_[0];

      Name = file[0].ToString().Trim();
      FileId = file[1].ToString().Trim();
      Application = file[2].ToString().Trim();
      Owner = file[3].ToString().Trim();
      Version = file[4].ToString().Trim();
      Created = file[5].ToString().Trim();
      Updated = file[6].ToString().Trim();
      LastEdited = file[7].ToString().Trim();
      Checksum = file[8].ToString().Trim();
      File = file[9].ToString().Trim();
    }
        
    public SharedFile(string name_, string fileId_, string application_, string owner_, string version_, string created_, string updated_, string lastEdited_, string checksum_, string file_)
    {
      Name = name_;
      FileId = fileId_;
      Application = application_;
      Owner = owner_;
      Version = version_;
      Created = created_;
      Updated = updated_;
      LastEdited = lastEdited_;
      Checksum = checksum_;
      File = file_;
    }
    #endregion Constructors
  }

  #endregion SharedFile Class

  #region RoamingFileCollection Class 
  public class RoamingFileCollection: CollectionBase
  {
    #region Constructors
    public RoamingFileCollection() 
    {
    }
    #endregion Constructors
		
    #region Public Methods
    public RoamingFile this[int index_] 
    {
      get	{ return (RoamingFile) this.List[index_]; }
    }
		
    public int Add(RoamingFile file_) 
    {
      return this.List.Add(file_);
    }

    public int Add(string filename_, string fileId_, string userId_, string userApplicationId_, string application_, string version_, string environmentKey_, string environmentValue_, string updated_, string created_, string lastEdited_, string checksum_, string localFilePath_, string localId_, string content_)
    {
      return this.Add(new RoamingFile(filename_, fileId_, userId_, userApplicationId_, application_, version_, environmentKey_, environmentValue_, updated_, created_, lastEdited_, checksum_, localFilePath_, localId_, content_));
    }
		
    public void Remove(RoamingFile file_) 
    {
      if (List.Contains(file_)) 
      {
        List.Remove(file_);
      }
    }
		
    public new RoamingFileCollectionEnumerator GetEnumerator()	
    {
      return new RoamingFileCollectionEnumerator(this);
    }

    public bool Contains(RoamingFile file_)
    {
      Debug.Assert(null != file_, "null == file_");
      if (null != file_)
      {
        return List.Contains(file_);
      }
      return false;
    }

    /// <summary>
    /// Checks the existance of 1st file found with the matching name
    /// </summary>
    /// <param name="fileName_">File Name</param>
    /// <returns>RoamingFile</returns>
    public bool Contains(string fileName_)
    {
      Debug.Assert(fileName_ != null && fileName_ != string.Empty, "fileName_ == null || fileName_ == string.Empty");
      bool result = false;
      foreach (RoamingFile file in List)
      {
        if (file.FileName == fileName_)
        {
          result = true;
          break;
        }
      }
      return result;
    }

    /// <summary>
    /// Returns the 1st file found with the matching name
    /// </summary>
    /// <param name="fileName_">File Name</param>
    /// <returns>RoamingFile</returns>
    public RoamingFile GetFileByName(string fileName_)
    {
      Debug.Assert(fileName_ != null && fileName_ != string.Empty, "fileName_ == null || fileName_ == string.Empty");
      
      foreach (RoamingFile file in List)
      {
        if (file.FileName == fileName_)
        {
          return file;
        }
      }
      return null;
    }

    #endregion Public Methods

    #region Public Inner Classes
    public class RoamingFileCollectionEnumerator : IEnumerator	
    {
      #region Declarations
      private	IEnumerator _enumerator;
      private	IEnumerable _temp;
      #endregion Declarations
			
      public RoamingFileCollectionEnumerator(RoamingFileCollection mappings_)
      {
        _temp =	((IEnumerable)(mappings_));
        _enumerator = _temp.GetEnumerator();
      }
			
      public RoamingFile Current
      {
        get { return (RoamingFile) _enumerator.Current; }
      }
			
      object IEnumerator.Current
      {
        get { return _enumerator.Current; }
      }
			
      public bool MoveNext()
      {
        return _enumerator.MoveNext();
      }
			
      bool IEnumerator.MoveNext()
      {
        return _enumerator.MoveNext();
      }
			
      public void Reset()
      {
        _enumerator.Reset();
      }
			
      void IEnumerator.Reset() 
      {
        _enumerator.Reset();
      }
    }
    #endregion Public Inner Classes
  }
  #endregion RoamingFileCollection Class
}
