#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.Configuration/ConfiguratorException.cs#1 $
  $DateTime: 2013/04/11 16:41:48 $
    $Change: 823942 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion

using System;

namespace MorganStanley.Desktop.Configuration
{
  /// <include file='xmldocs/ConfiguratorException.cs.xml' path='doc/doc[@for="ConfiguratorException"]/*'/>
  public class ConfiguratorException : ApplicationException
  {
    /// <include file='xmldocs/ConfiguratorException.cs.xml' path='doc/doc[@for="ConfiguratorException.ConfiguratorException"]/*'/>
    public ConfiguratorException(string message_) : base(message_) 
    {
    }

    /// <include file='xmldocs/ConfiguratorException.cs.xml' path='doc/doc[@for="ConfiguratorException.ConfiguratorException1"]/*'/>
    public ConfiguratorException(string message_, Exception innerException_) : base(message_, innerException_) 
    {
    }
  }
}
