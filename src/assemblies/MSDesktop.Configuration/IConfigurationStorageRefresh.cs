#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2011 Morgan Stanley, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.Configuration/IConfigurationStorageRefresh.cs#1 $
  $DateTime: 2013/04/11 16:41:48 $
    $Change: 823942 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion File Info Header

using System;

namespace MorganStanley.Desktop.Configuration
{
  /// <include file='xmldocs/IConfigurationStorageRefresh.cs.xml' path='doc/doc[@for="IConfigurationStorageRefresh"]/*'/>
  public interface IConfigurationStorageRefresh
  {
    /// <include file='xmldocs/IConfigurationStorageRefresh.cs.xml' path='doc/doc[@for="IConfigurationStorageReader.RefreshConfiguration"]/*'/>
    void RefreshConfiguration(string name_, string version_);
  }
}