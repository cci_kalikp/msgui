﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Reflection;
using System.Security;
using System.Windows.Forms;
using Infragistics.Win;
using Infragistics.Win.UltraWinToolbars;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MSDesktop.Infragistics
{
    public class WpfProofUltraToolbarsManager : UltraToolbarsManager
    {
        private bool isFormActive;
        private Timer timer;

        public WpfProofUltraToolbarsManager()
            : base()
        { }

        public WpfProofUltraToolbarsManager(IContainer container)
            : base(container)
        { }
          
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (hookedEndInitForm != null)
            {
                hookedEndInitForm.Load -= OnFormLoadOverride;
            }
            if (this.timer != null)
            {
                this.timer.Enabled = false;
                this.timer.Dispose();
                this.timer = null;
            }
        }

        private Form hookedEndInitForm;

        protected override void OnEndInit()
        {
            base.OnEndInit(); 
            Form f = DockWithinContainer.FindForm(); 
            if (f is IFeedbackWindow && !f.IsHandleCreated)
            {  
                f.HandleCreated -= new EventHandler(f_HandleCreated);
                f.HandleCreated += new EventHandler(f_HandleCreated);
            }
            this.timer = new Timer();
            this.timer.Interval = 100;
            this.timer.Tick += new EventHandler(timer_Tick);
            this.timer.Enabled = true;
        }

        void f_HandleCreated(object sender, EventArgs e)
        {
            Form f = (Form) sender;
            f.HandleCreated -= new EventHandler(f_HandleCreated);
            IFeedbackWindow feedback = (IFeedbackWindow) f; 
            if (!feedback.IsLayoutLoadActive) return;
             
            var baseFormLoadMethod = GetBaseOnFormLoadMethod();
            if (baseFormLoadMethod != null)
            {
                hookedEndInitForm = f;
                f.Load -= Delegate.CreateDelegate(typeof(EventHandler), this, baseFormLoadMethod) as EventHandler;
                f.Load += OnFormLoadOverride;
            }
        }

        void OnFormLoadOverride(object sender, EventArgs e)
        { 
            NativeWindowMethods.LockSetForegroundWindow(NativeWindowMethods.LSFW_LOCK); 
            hookedEndInitForm.Load -= OnFormLoadOverride; 
            GetBaseOnFormLoadMethod().Invoke(this, new object[] { sender, e }); 

        }

        private MethodInfo GetBaseOnFormLoadMethod()
        {
            return typeof(UltraToolbarsManager)
                .GetMethod("OnFormLoad", BindingFlags.FlattenHierarchy | BindingFlags.Instance | BindingFlags.NonPublic);
        }


        private void timer_Tick(object sender, EventArgs e)
        {
            Control dockWithinContainer = this.DockWithinContainer;

            if (dockWithinContainer == null)
                return;

            bool isFormNowActive = IsControlOnActiveForm(dockWithinContainer);

            if (isFormNowActive != this.isFormActive)
            {
                this.isFormActive = isFormNowActive;

                if (this.isFormActive)
                    this.OnFormActivated();
                else
                    this.OnFormDeactivate();
            }
        }

        protected override bool ActivateForm()
        {
            bool succeeded = base.ActivateForm();

            if (succeeded)
                return true;

            Control dockWithinContainer = this.DockWithinContainer;

            if (dockWithinContainer == null || dockWithinContainer.IsHandleCreated == false)
                return false;

            IntPtr parentHwnd = NativeWindowMethods.FindTopLevelWindow(dockWithinContainer.Handle);

            if (parentHwnd == IntPtr.Zero)
                return false;

            NativeWindowMethods.SetActiveWindow(parentHwnd);
            return true;
        }

        protected override Control ActiveControlOfActiveForm
        {
            get
            {
                Control control = base.ActiveControlOfActiveForm;

                if (control != null)
                    return control;

                return Control.FromChildHandle(NativeWindowMethods.GetFocus());
            }
        }

        protected override FormWindowState FormWindowState
        {
            get
            {
                FormWindowState windowState = base.FormWindowState;

                if (windowState != FormWindowState.Normal)
                    return windowState;

                // TODO: Get the window state and return it
                return windowState;
            }
        }

        protected override bool IsControlOnActiveForm(Control control)
        {
            bool isOnActiveForm = base.IsControlOnActiveForm(control);

            if (isOnActiveForm)
                return true;

            IntPtr activeForm = NativeWindowMethods.GetForegroundWindow();

            IntPtr controlHandle = control.Handle;

            while (controlHandle != IntPtr.Zero)
            {
                if (controlHandle == activeForm)
                    return true;

                controlHandle = NativeWindowMethods.GetParent(controlHandle);
            }

            return false;
        }

        protected override bool IsFormActive
        {
            get
            {
                bool isFormActive = base.IsFormActive;

                if (isFormActive)
                    return true;

                Control dockWithinContainer = this.DockWithinContainer;

                if (dockWithinContainer == null || dockWithinContainer.IsHandleCreated == false)
                    return false;

                IntPtr activeForm = NativeWindowMethods.GetForegroundWindow();
                IntPtr form = NativeWindowMethods.FindTopLevelWindow(dockWithinContainer.Handle);

                while (activeForm != IntPtr.Zero)
                {
                    if (activeForm == form)
                        return true;

                    activeForm = NativeWindowMethods.GetWindowLong(activeForm, NativeWindowMethods.GWL_HWNDPARENT);
                }

                return false;
            }
        }

        protected override void OnFloatingToolbarWindowShown(FloatingToolbarWindowBase floatingToolbarWindow)
        {
            base.OnFloatingToolbarWindowShown(floatingToolbarWindow);

            if (floatingToolbarWindow.Owner != null)
                return;

            Control dockWithinContainer = this.DockWithinContainer;

            if (dockWithinContainer != null && dockWithinContainer.IsHandleCreated)
            {
                try
                {
                    IntPtr ownerHandle = NativeWindowMethods.FindTopLevelWindow(dockWithinContainer.Handle);

                    if (ownerHandle != IntPtr.Zero)
                    {
                        NativeWindowMethods.SetWindowLong(floatingToolbarWindow.Handle, NativeWindowMethods.GWL_HWNDPARENT, ownerHandle);
                        // TODO: If the owner is TopMost, make the floating toolbar window TopMost
                    }
                }
                catch (SecurityException) { }
            }
        }

    }
}
