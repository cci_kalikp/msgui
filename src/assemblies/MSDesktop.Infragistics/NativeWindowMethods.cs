﻿using System;
using System.Runtime.InteropServices;
using System.Security;

namespace MSDesktop.Infragistics
{
    [SuppressUnmanagedCodeSecurity]
    internal class NativeWindowMethods
    {
        internal const int GWL_HWNDPARENT = -8;
        internal const uint LSFW_LOCK = 1;
        internal const uint LSFW_UNLOCK = 2;
        [DllImport("user32.dll")]
        public static extern bool AllowSetForegroundWindow(int dwProcessId);

        [DllImport("user32")]
        public static extern bool LockSetForegroundWindow(uint ulockCode);

        [DllImport("user32")]
        internal static extern IntPtr GetFocus();

        [DllImport("user32")]
        internal static extern IntPtr GetForegroundWindow();

        [DllImport("user32")]
        internal static extern IntPtr GetParent(IntPtr childHwnd);

        [DllImport("user32")]
        internal static extern IntPtr GetWindowLong(IntPtr hWnd, int nIndex);

        [DllImport("user32")]
        internal static extern IntPtr SetActiveWindow(IntPtr hWnd);

        [DllImport("user32")]
        internal static extern IntPtr SetWindowLong(IntPtr hWnd, int nIndex, IntPtr newLong);

        internal static IntPtr FindTopLevelWindow(IntPtr childWindow)
        {
            IntPtr control = childWindow;

            while (true)
            {
                IntPtr nextControl = GetParent(control);

                if (nextControl == IntPtr.Zero)
                    break;

                control = nextControl;
            }

            return control;
        }
    }
}