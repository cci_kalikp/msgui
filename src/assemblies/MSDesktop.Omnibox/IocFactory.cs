﻿using Microsoft.Practices.Unity;
using MSDesktop.Omnibox.Interfaces;
using MSDesktop.Omnibox.UI.Grammar;

namespace MSDesktop.Omnibox.UI
{
   public static class IocFactory
    {
       public static void Setup(IUnityContainer moduleContainer,IUnityContainer globalContainer)
       {
           moduleContainer.RegisterType<IGrammarController, GrammarController>(new ContainerControlledLifetimeManager());
           globalContainer.RegisterType<IOmniboxRegistry, OmniboxRegistry>(new ContainerControlledLifetimeManager(), new InjectionConstructor(moduleContainer.Resolve<IGrammarController>()));
       }
    }
}
