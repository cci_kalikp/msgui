﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using MSDesktop.Omnibox.UI.Model;

namespace MSDesktop.Omnibox.UI
{
    public class OmniboxToolAndExampleModel
    {
        private string _toolName;
        private ObservableCollection<Example> _examples;

        public OmniboxToolAndExampleModel()
        {
            _examples = new ObservableCollection<Example>();
        }

        public OmniboxToolAndExampleModel(string toolName, IList<Example> examples)
        {
            _toolName = toolName;
            _examples = new ObservableCollection<Example>(examples);
        }

        public ObservableCollection<Example> Examples
        {
            get { return _examples; }
            set { _examples = value; }
        }

        public string ToolName
        {
            get { return _toolName; }
            set { _toolName = value; }
        }
    }
}
