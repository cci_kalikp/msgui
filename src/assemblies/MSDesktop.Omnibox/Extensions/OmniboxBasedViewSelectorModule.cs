﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Xml;
using MSDesktop.Omnibox.Grammar;
using MSDesktop.Omnibox.Interfaces;
using MSDesktop.Omnibox.Message;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;

namespace MSDesktop.Extensions
{

    public class OmniboxBasedViewSelectorModule : IModule
    {
        private readonly IChromeManager chromeManager;
        private readonly IChromeRegistry chromeRegistry;
        private readonly IOmniboxRegistry omniboxRegistry;
        private readonly ICommunicator communicator;

        public OmniboxBasedViewSelectorModule(IChromeManager chromeManager, 
            IChromeRegistry chromeRegistry,
            IOmniboxRegistry omniboxRegistry,
            ICommunicator communicator)
        {
            this.chromeManager = chromeManager;
            this.chromeRegistry = chromeRegistry;
            this.omniboxRegistry = omniboxRegistry;
            this.communicator = communicator;
        }

        public void Initialize()
        {
            var viewSelectionGrammarDocument = BuildGrammarDocument();

            var grammar = XMLGrammar.BuildXMLGrammar(viewSelectionGrammarDocument);
            omniboxRegistry.RegisterGrammar(grammar);

            communicator.GetSubscriber<OmniboxMessage>().GetObservable().Subscribe(OmniboxMessageRecevied);
        }

        private XmlDocument BuildGrammarDocument()
        {
            var doc = new XmlDocument();

            var rootElement = doc.CreateElement("module");
            rootElement.SetAttribute("name", "View Selector");
            rootElement.SetAttribute("description", "View selector grammar");

            var commandElement = doc.CreateElement("command");
            var actionEl = doc.CreateElement("choices");
            var openChoiceEl = doc.CreateElement("choice");
            var openKeywordChoiceEl = doc.CreateElement("keyword");
            openKeywordChoiceEl.InnerText = "op";
            openChoiceEl.AppendChild(openKeywordChoiceEl);
            actionEl.AppendChild(openChoiceEl);

            var viewTokenEl = doc.CreateElement("choices");
            foreach (var wfid in chromeRegistry.WindowFactoryIDs.Reverse())
            {
                var keywordEl = doc.CreateElement("keyword");
                keywordEl.InnerText = wfid;

                var choiceEl = doc.CreateElement("choice");
                choiceEl.AppendChild(keywordEl);
                viewTokenEl.AppendChild(choiceEl);
            }

            var initialLocationEl = doc.CreateElement("choices");
            initialLocationEl.SetAttribute("min", "0");
            initialLocationEl.SetAttribute("max", "1");
            SetupInitialLocationNode(doc, initialLocationEl);
            
            commandElement.AppendChild(actionEl);
            commandElement.AppendChild(viewTokenEl);
            commandElement.AppendChild(initialLocationEl);

            rootElement.AppendChild(commandElement);
            doc.AppendChild(rootElement);
            return doc;
        }

        private static void SetupInitialLocationNode(XmlDocument doc, XmlElement initialLocationEl)
        {
            var dockLeftChoice = doc.CreateElement("choice");
            var dockLeftKeyword = doc.CreateElement("keyword");
            dockLeftKeyword.InnerText = "docked";
            dockLeftChoice.AppendChild(dockLeftKeyword);
            initialLocationEl.AppendChild(dockLeftChoice);

            var dockRightChoice = doc.CreateElement("choice");
            var dockRightKeyword = doc.CreateElement("keyword");
            dockRightKeyword.InnerText = "floating";
            dockRightChoice.AppendChild(dockRightKeyword);
            initialLocationEl.AppendChild(dockRightChoice);
        }

        private void OmniboxMessageRecevied(OmniboxMessage omniboxMessage)
        {
            var tokens = (IList<object>)((Dictionary<string, object>) omniboxMessage.Value)["GrammarTokens"];
            
            var locationToken = (string) tokens[2];
            var initialLocation = locationToken != null && locationToken.Equals("docked")
                                      ? InitialLocation.DockLeft
                                      : InitialLocation.Floating;

            switch ((string)tokens[0])
            {
                case "op":
                    Application.Current.Dispatcher.BeginInvoke(new Action(() => 
                        chromeManager.CreateWindow((string)tokens[1], new InitialWindowParameters
                            {
                                InitialLocation = initialLocation
                            })));
                    break;
            }
        }
    }
}
