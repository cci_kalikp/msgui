﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSDesktop.Omnibox.UI
{
    /// <summary>
    /// This class is the mapping between the hint/autocomplete interface and the view.
    /// </summary>
    public class Suggestions
    {
        public string Hint { get; private set; }
        public List<string> Autocomplete { get; private set; }

        public Suggestions(string hint, List<string> autos)
        {
            Hint = hint;
            Autocomplete = autos;
        }
    }
}
