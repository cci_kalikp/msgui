﻿using System.Collections.Generic;

namespace MSDesktop.Omnibox.Interfaces
{
    public interface IGrammar
    {
        string Name { get; }

        string Description { get; }

        IEnumerable<string> Examples { get; }

        Dictionary<string, string> Commands { get; }

        bool IsQueryValid(string query, out int errorTokenIndex);

        bool IsQueryComplete(string query);

        Dictionary<string, SortedSet<string>> Autocompletions(string query);

        List<object> QueryToModuleInputs(string query);

        List<object> PartialQueryToModuleInputs(string partialQuery);

        List<string> GetTokens(string query);

        void ComputeTokenRange(string query, int tokenIndex, out int startIndex, out int tokenLength);

        bool ShouldAutoCompleteText { get; }
    }
}
