﻿using System.Collections.Generic;

namespace MSDesktop.Omnibox.Interfaces
{
    public interface IOmniboxRegistry
    {
        void RegisterGrammar(IGrammar grammar);

        void RegisterGrammar(string xmlDocument);		
    }
}
