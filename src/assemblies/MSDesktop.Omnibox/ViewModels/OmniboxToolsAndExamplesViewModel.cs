﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace MSDesktop.Omnibox.UI.ViewModels
{
    public class OmniboxToolsAndExamplesViewModel
    {
        private ObservableCollection<OmniboxToolAndExampleModel> _omniboxToolAndExampleModels;

        public OmniboxToolsAndExamplesViewModel()
        {
            _omniboxToolAndExampleModels = new ObservableCollection<OmniboxToolAndExampleModel>();
        }

        public OmniboxToolsAndExamplesViewModel(IList<OmniboxToolAndExampleModel> omniboxToolAndExampleModels)
        {
            _omniboxToolAndExampleModels = new ObservableCollection<OmniboxToolAndExampleModel>(omniboxToolAndExampleModels);
        }

        public ObservableCollection<OmniboxToolAndExampleModel> OmniboxToolAndExamples
        {
            get { return _omniboxToolAndExampleModels; }
            set { _omniboxToolAndExampleModels = value; }
        }
    }
}
