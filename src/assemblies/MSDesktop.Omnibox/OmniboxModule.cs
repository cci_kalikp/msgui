﻿using System;
using System.Windows;
using System.Windows.Controls;
using Infragistics.Windows.Ribbon;
using Infragistics.Windows.Ribbon.Internal;
using MSDesktop.Omnibox.UI.Views;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MSDesktop.Views;
using System.Windows.Controls.Primitives;


//TODO Omnibox TODOS
// Move to IG's grammar provider (EBNF)
// Ability to show icons in the list
// Work with communication, e.g. you should eb able to inject into other apps' omnibox
// Do exchange and stock nodes with lookups
namespace MSDesktop.Omnibox.UI
{
    public class OmniboxModule : IModule
    {
        private readonly IUnityContainer _globalContainer;
        private readonly IChromeRegistry _chromeRegistry;
        private readonly IUnityContainer _container;

        public const string WidgetFactoryName = "{94121B0F-F67D-4E6C-8228-1CB9A366D8FF}.OmniBox";

        public OmniboxModule(IUnityContainer unityContainer, IChromeRegistry chromeRegistry)
        {
            _chromeRegistry = chromeRegistry;
            _globalContainer = unityContainer;
            _container = _globalContainer.CreateChildContainer();
        }

        public void Initialize()
        {
            IocFactory.Setup(_container, _globalContainer);


            object content = (Func<object>)(() => _container.Resolve<OmniboxView>());

            _chromeRegistry.RegisterWidgetFactory(WidgetFactoryName,
                                                  (widgetViewContainer, state) =>
                                                      {

                                                          //widgetViewContainer.Content = (Func<object>)(() => _container.Resolve<OmniboxView>());

                                                          var widgetFactory = content as Func<object>;
                                                          widgetViewContainer.Content = widgetFactory != null ?
                                                                                                new ContentControlWrapper(widgetFactory) :
                                                                                                content;

                                                          return true;
                                                      });

            UserControl omniboxPopupUserControl = _container.Resolve<OmniboxPopup>();
            Popup omniboxPopupControl = (Popup)omniboxPopupUserControl.FindName("OmniboxPopupControl");
            ContentControl omniboxPopupContentControl = (ContentControl)omniboxPopupControl.FindName("OmniboxPopupContentControl");

            Func<object> omniboxViewGeneratorDelegate = content as Func<object>;
            object omnibox = omniboxViewGeneratorDelegate();

            omniboxPopupContentControl.Content = omnibox;

            OmniboxView oView = (OmniboxView)omnibox;
            oView.OmniboxPopup = omniboxPopupControl;
        }


        private class ContentControlWrapper : ContentControl, IRibbonTool
        {
            private readonly Func<object> _widgetFactory;

            public ContentControlWrapper(Func<object> widgetFactory)
            {
                _widgetFactory = widgetFactory;
                Content = _widgetFactory();
                VerticalAlignment = VerticalAlignment.Stretch;
                HorizontalAlignment = HorizontalAlignment.Stretch;
            }

            public RibbonToolProxy ToolProxy
            {
                get { return new ContentControlWrapperProxy(_widgetFactory); }
            }
        }

        private class ContentControlWrapperProxy : RibbonToolProxy<ContentControlWrapper>
        {
            private readonly Func<object> _widgetFactory;

            public ContentControlWrapperProxy(Func<object> widgetFactory)
            {
                _widgetFactory = widgetFactory;
            }

            protected override ContentControlWrapper Clone(ContentControlWrapper sourceTool)
            {
                return new ContentControlWrapper(_widgetFactory);
            }
        }
    }
}
