﻿using System.Windows.Input;

namespace MSDesktop.Omnibox.UI
{
    public static class Commands
    {
        public static RoutedCommand ShowOmniboxHelp = new RoutedCommand();
        public static RoutedCommand ShowSupportedTools = new RoutedCommand();
        public static RoutedCommand ClearOmniBox = new RoutedCommand()
        {
            InputGestures = { new KeyGesture(Key.Escape, ModifierKeys.None)}
        };
    }
}
