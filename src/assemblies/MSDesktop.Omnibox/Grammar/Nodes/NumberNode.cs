﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSDesktop.Omnibox.Grammar
{
    /// <summary>
    /// This class is an implementation of a Number node type.
    /// </summary>
    /// <author>cottrjon</author>
    
    public class NumberNode : AbstractGrammarNode
    {
        public override bool IsPartialMatch(string token)
        {
            if (token.Length == 1 && (token == "+" || token == "-"))
                return true;

            decimal number;

            if (decimal.TryParse(token, out number))
            {
                return true;
            }

            return false;
        }

        public override bool IsCompleteMatch(string token)
        {
            decimal number;
            if (decimal.TryParse(token, out number))
            {
                return true;
            }

            return false;
        }

        public override string Hint
        {
            get { return String.Format("{0}({1})", "+/-NUMBER", AdditionalHint); }

             
        }

        public override List<string> Autocompletion(string partialToken)
        {
            List<string> autocomplete = new List<string>();
            return autocomplete;
        }

        public override object GetModuleInput(string token)
        {
            decimal result;

            if (decimal.TryParse(token, out result))
                return result;
            else
                throw new ApplicationException("GetModuleInput was passed an invalid token");
        }
    }
}
