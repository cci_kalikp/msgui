﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MSDesktop.Omnibox.Grammar;
using System.Text.RegularExpressions;
using System.Globalization;

namespace MSDesktop.Grammar.Nodes
{
    public class FormattedDateNode : AbstractGrammarNode
    {
        public override string Hint
        {
            get
            {
                return "Date";
            }
        }

        public override bool IsPartialMatch(string token)
        {
            int length = token.Length;
            bool ret = false;
            int firstIndex;
            int lastIndex;
            int n;
            
            firstIndex = token.IndexOf('/');
            lastIndex = token.LastIndexOf('/');

            if(firstIndex != -1)
            {
                if(firstIndex == lastIndex)
                {
                    if (int.TryParse(token.Substring(0, firstIndex), out n))
                    {
                        if (token.EndsWith("/"))
                        {
                            ret = true;
                        }
                        else
                        {
                            ret = int.TryParse(token.Substring(firstIndex + 1), out n);
                        }
                    }                    
                }
                else
                {
                    if(int.TryParse(token.Substring(0, firstIndex), out n))
                    {
                        if(int.TryParse(token.Substring(firstIndex + 1, lastIndex - firstIndex - 1), out n))
                        {
                            if (token.EndsWith("/"))
                            {
                                ret = true;
                            }
                            else
                            {
                                ret = int.TryParse(token.Substring(lastIndex + 1), out n);
                            }
                        }
                    }
                }
            }
            else
            {
                ret = int.TryParse(token, out n);
            }

            return ret;
        }

        public override bool IsCompleteMatch(string token)
        {
            bool ret = false;
            DateTime date;
            if (DateTime.TryParse(token, CultureInfo.CurrentCulture, DateTimeStyles.None, out date))
            {
                if (IsPartialMatch(token))
                {
                    ret = true;
                }                
            }            
            return ret;
        }

        public override List<string> Autocompletion(string partialToken)
        {
            List<string> autocomplete = new List<string>();
            return autocomplete;
        }

        public override object GetModuleInput(string token)
        {
            return token;
        }
    }
}
