﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSDesktop.Omnibox.Grammar
{
    /// <summary>
    /// This class implements a Stock node. This is a placeholder class. 
    /// A proper implementation needs to be created.
    /// </summary>
    /// <author>cottrjon</author>
    public class StockNode : AbstractGrammarNode
    {
        private static readonly List<string> stocks = new List<string>();

        public StockNode()
        {
            // This is sample data, should be loaded from a proper locaation
            stocks.Add("ibm.n");
            stocks.Add("bt.l");
        }

        public override bool IsPartialMatch(string token)
        {
            foreach (string stock in stocks)
            {
                if (token.Length > stock.Length)
                    continue;



                bool valid = true;
                for (int i = 0; i < token.Length; i++)
                {
                    if (Char.ToLower(token[i]) != Char.ToLower(stock[i]))
                    {
                        valid = false;
                        break;
                    }
                }

                if(valid)
                    return true;
            }
            return false;
        }

        public override bool IsCompleteMatch(string token)
        {
            foreach (string stock in stocks)
            {
                if (string.Compare(stock, token, true) == 0)
                    return true;
            }
            return false;
        }

        public override string Hint
        {
            get
            {
                return "Stock";
            }
        }

        public override List<string> Autocompletion(string partialToken)
        {
            List<string> autocomplete = new List<string>();

            foreach (string stock in stocks)
            {
                if (partialToken.Length > stock.Length)
                    continue;

                bool valid = true;
                for (int i = 0; i < partialToken.Length; i++)
                {
                    if (Char.ToLower(partialToken[i]) != Char.ToLower(stock[i]))
                    {
                        valid = false;
                        break;
                    }
                }

                if(valid)
                    autocomplete.Add(stock);
                
            }
            return autocomplete;
        }

        public override object GetModuleInput(string token)
        {
            if (stocks.Contains(token))
                return token;
            else
                throw new ApplicationException("GetModuleInput was given invalid input");
        }
    }
}
