﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MSDesktop.Omnibox.Grammar;
using System.Text.RegularExpressions;

namespace MSDesktop.Grammar.Nodes
{
    public class RegexpNode : AbstractGrammarNode
    {
        private Regex _regex;

        public override string Hint
        {
            get
            {
                return "Regular Expression";
            }
        }

        public RegexpNode(string regexp)
        {
            _regex = new Regex(regexp);
        }

        public override bool IsPartialMatch(string token)
        {
            return true;
        }

        public override bool IsCompleteMatch(string token)
        {
            return _regex.IsMatch(token);
        }

        public override List<string> Autocompletion(string partialToken)
        {
            List<string> autocomplete = new List<string>();
            return autocomplete;
        }

        public override object GetModuleInput(string token)
        {
            return token;
        }
    }
}
