﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSDesktop.Omnibox.Grammar
{
    /// <summary>
    /// This class is an implementation of a Percent node type.
    /// </summary>
    /// <author>cottrjon</author>

    public class PercentNode : AbstractGrammarNode
    {
        public override bool IsPartialMatch(string token)
        {
            if (token.Length > 0 && token.Last() == '%')
            {
                token = token.Substring(0, token.Length - 1);
            }

            decimal number;

            if (decimal.TryParse(token, out number))
            {
                return true;
            }

            return false;
        }

        public override bool IsCompleteMatch(string token)
        {

            if (token.Last() != '%')
                return false;


            token = token.Substring(0, token.Length - 1);

            decimal number;
            if (decimal.TryParse(token, out number))
            {
                return true;
            }

            return false;
        }

        public override string Hint
        {
            get { return "NUMBER%"; }
        }

        public override List<string> Autocompletion(string partialToken)
        {
            List<string> autocomplete = new List<string>();
            return autocomplete;
        }

        public override object GetModuleInput(string token)
        {
            decimal result;

            if (decimal.TryParse(token, out result))
                return result;
            else
                throw new ApplicationException("GetModuleInput was passed an invalid token");
        }
    }
}
