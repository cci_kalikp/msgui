﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace MSDesktop.Omnibox.Grammar
{
    public class ExchangeNode : AbstractGrammarNode
    {
        private static readonly List<string> exchanges = new List<string>();

        public ExchangeNode()
        {
            // This is sample data, should be loaded from a proper locaation
            exchanges.Add("ftse.lder");
        }

        public override bool IsPartialMatch(string token)
        {
            return (from exchange in exchanges
                    where token.Length <= exchange.Length
                    select !token.Where((t, i) => Char.ToLower(t) != Char.ToLower(exchange[i])).Any()).Any(
                        valid => valid);
        }

        public override bool IsCompleteMatch(string token)
        {
            return exchanges.Any(exchange => string.Compare(exchange, token, true, CultureInfo.InvariantCulture) == 0);
        }

        public override string Hint
        {
            get
            {
                return "Exchange";
            }
        }

        public override List<string> Autocompletion(string partialToken)
        {
            return (from exchange in exchanges
                    where partialToken.Length <= exchange.Length
                    let valid = !partialToken.Where((t, i) => Char.ToLower(t) != Char.ToLower(exchange[i])).Any()
                    where valid
                    select exchange).ToList();
        }

        public override object GetModuleInput(string token)
        {
            if (exchanges.Contains(token))
                return token;
            else
                throw new ApplicationException("GetModuleInput was given invalid input");
        }
    }
}
