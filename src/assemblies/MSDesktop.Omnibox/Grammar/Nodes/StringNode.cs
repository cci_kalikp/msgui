﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSDesktop.Omnibox.Grammar
{
    /// <summary>
    /// This class implements a String type which allows any input. This should not be used much.
    /// </summary>
    /// <author>cottrjon</author>
    public class StringNode : AbstractGrammarNode
    {
        public override bool IsPartialMatch(string token)
        {
            return true;
        }

        public override bool IsCompleteMatch(string token)
        {
            return true;
        }

        public override string Hint
        {
            get
            {
                return String.Format("{0}({1})", "String",AdditionalHint);
            }
        }

        public override List<string> Autocompletion(string partialToken)
        {
            var autocomplete = new List<string>();
            return autocomplete;
        }

        public override object GetModuleInput(string token)
        {
            return token;
        }
    }
}
