﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSDesktop.Omnibox.Grammar
{
    /// <summary>
    /// This is an implementation of a keyword type.
    /// </summary>
    /// <author>cottrjon</author>
    public class KeywordNode : AbstractGrammarNode
    {
        private string _value;

        public KeywordNode(string value)
        {
            _value = value;
        }

        public override bool IsPartialMatch(string token)
        {
            if (token.Length > _value.Length)
                return false;

            for (int i = 0; i < token.Length; i++)
            {
                if ( Char.ToLower(token[i]) != Char.ToLower(_value[i]) )
                    return false;
            }

            return true;
        }

        public override bool IsCompleteMatch(string token)
        {
            if (string.Compare(_value, token, true) == 0)
                return true;

            return false;
        }

        public override List<string> Autocompletion(string partialToken)
        {
            List<string> autocomplete = new List<string>();

            if (this.IsPartialMatch(partialToken))
            {
                autocomplete.Add(_value);
            }
            return autocomplete;
        }

        public override string Hint
        {
            get
            {
                return "Keyword";
            }
        }

        public override object GetModuleInput(string token)
        {
            if (IsCompleteMatch(token))
                return _value;
            else
                throw new ApplicationException("GetModuleInput called on invalid input!");
        }
    }
}
