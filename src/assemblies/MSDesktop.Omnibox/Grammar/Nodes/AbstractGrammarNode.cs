﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSDesktop.Omnibox.Grammar

{
    /// <summary>
    /// This class represents a type of node to be used by XMLGrammar. 
    /// Derived classes must implement: IsPartialMatch, IsCompleteMatch, Hint, Autocompletion, and GetModuleInput
    /// </summary>
    /// <author>cottrjon</author>
    public abstract class AbstractGrammarNode
    {
        public void AddChild(AbstractGrammarNode node)
        {
            this._children.Add(node);
        }

        public override string ToString()
        {
            return String.Format("[Name={0},Min={2},Max={3}]", this.GetType(), Children, MinOccur, MaxOccur);
        }

        #region Properties
        private readonly IList<AbstractGrammarNode> _children = new List<AbstractGrammarNode>();
        public IList<AbstractGrammarNode> Children { get { return _children; } }

        public int MinOccur { get; set; }

        public int MaxOccur { get; set; }

        #endregion

        public abstract bool IsPartialMatch(string token);

        public abstract bool IsCompleteMatch(string token);

        public abstract string Hint { get; }

        public string AdditionalHint { get; set; }

        public abstract List<string> Autocompletion(string partialToken);

        public abstract object GetModuleInput(string token);

        public string NodeType { get; set; }
    }
}
