﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSDesktop.Omnibox.Grammar
{
    /// <summary>
    /// Temporary implementation for date. This was only used for testing
    /// </summary>
    /// <author>cottrjon</author>
    class DateNode : AbstractGrammarNode
    {
        private List<string> _months;
        public DateNode()
        {
            _months = new List<string>();
            _months.Add("jan");
            _months.Add("feb");
            _months.Add("mar");
            _months.Add("apr");
            _months.Add("may");
            _months.Add("jun");
            _months.Add("jul");
            _months.Add("aug");
            _months.Add("sep");
            _months.Add("oct");
            _months.Add("nov");
            _months.Add("dec");
        }

        //TODO: Implement proper partial Date matching
        public override bool IsPartialMatch(string token)
        {
            if (token.Length > 3)
                return false;

            var month = (from str in _months
                         where String.Compare(str.Substring(0, token.Length), token, true) == 0
            select str).FirstOrDefault();

            //if (String.Compare("jan".Substring(0, token.Length), token, true) == 0)
            //    return true;

            //if (String.Compare("feb".Substring(0, token.Length), token, true) == 0)
            //    return true;


            //if (String.Compare("mar".Substring(0, token.Length), token, true) == 0)
            //    return true;


            //if (String.Compare("dec".Substring(0, token.Length), token, true) == 0)
            //    return true;
            return !String.IsNullOrEmpty(month);
        }

        //TODO: Implement proper complete Date matching
        public override bool IsCompleteMatch(string token)
        {
            //if (String.Compare(token, "jan", true) == 0 || String.Compare(token, "feb", true) == 0
            //    || String.Compare(token, "mar", true) == 0 || String.Compare(token, "dec", true) == 0)
            //    return true;
            //return false;
            var month = (from str in _months
                         where String.Compare(str, token, true) == 0
                         select str).FirstOrDefault();
            return !String.IsNullOrEmpty(month);
        }

        //TODO: Implement proper Date suggestions
        public override string Hint
        {
            get
            {
                return "Date";
            }

        }

        //TODO: Implement autocomplete properly
        public override List<string> Autocompletion(string partialToken)
        {
           
            List<string> result = new List<string>();
            foreach (string value in _months)
            {
                if (partialToken.Length <= value.Length && partialToken == value.Substring(0, partialToken.Length))
                {
                    result.Add(value);
                }
            }
            return result;
        }

        //TODO: implement proper conversion for Date
        public override object GetModuleInput(string token)
        {
            /*
            if (String.Compare(token, "jan", true) == 0)
                return new DateTime(2011, 1, 1);

            if (String.Compare(token, "feb", true) == 0)
                return new DateTime(2011, 2, 1);
            */
            return token;
        }

    }
}
