﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MSDesktop.Grammar.Nodes;

namespace MSDesktop.Omnibox.Grammar
{
    /// <summary>
    /// This class is a utility class which allows the nodes to be built.
    /// In order to add a new node type, an entry in the switch statement must be added.
    /// </summary>
    /// <author>cottrjon</author>
    public class GrammarNodeGenerator
    {
        //UPDATE THIS METHOD WHEN YOU ADD A CONCRETE GRAMMAR NODE
        public static AbstractGrammarNode GetNode(string name, string value, int minOccur, int maxOccur)
        {
            AbstractGrammarNode node = null;
            switch (name)
            {
                case "keyword":
                    node = new KeywordNode(value);
                    break;
                case "string":
                    node = new StringNode();
                    break;
                case "number":
                    node = new NumberNode();
                    break;
                case "date":
                    node = new DateNode();
                    break;
                case "percent":
                    node = new PercentNode();
                    break;
                case "stock":
                    node = new StockNode();
                    break;
                case "exchange":
                    node = new ExchangeNode();
                    break;
                case "formattedDate":
                    node = new FormattedDateNode();
                    break;
                case "regexp":
                    node = new RegexpNode(value);
                    break;
            }

            if (node != null)
            {
                node.MinOccur = minOccur;
                node.MaxOccur = maxOccur;
                node.NodeType = name;
                if ((node as StringNode) != null)
                {
                    (node as StringNode).AdditionalHint = value;
                }
                if ((node as NumberNode) != null)
                {
                    (node as NumberNode).AdditionalHint = value;
                }
            }
            return node;
            
        }
    }
}
