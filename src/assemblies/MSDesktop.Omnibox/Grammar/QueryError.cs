﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSDesktop.Omnibox.UI.Grammar
{
    /// <summary>
    /// QueryError provides information on errors in parsing.
    /// Its primary use is for error highlighting and providing
    /// a useful error message to the user.
    /// </summary>
    /// <author>cottrjon</author>
    public class QueryError
    {
        public QueryError(int position, int length, string message)
        {
            ErrorPosition = position;
            ErrorLength = length;
            ErrorMessage = message;
        }

        public int ErrorPosition { get; set; }

        public int ErrorLength { get; set; }

        public string ErrorMessage { get; set; }
    }
}
