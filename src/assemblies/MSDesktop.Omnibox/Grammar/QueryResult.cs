﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSDesktop.Omnibox.UI.Grammar
{
    /// <summary>
    /// QueryResult is the result of a parsing attempt giving information on:
    /// - Errors
    /// - Name of the module (and grammar)
    /// - Module Inputs, the parsed values
    /// </summary>
    /// <author>cottrjon</author>
    public class QueryResult
    {
        #region Fields
        private QueryError _error;
        private List<object> _moduleInputs = new List<object>();
        private List<Suggestions> _suggestions = new List<Suggestions>();
        private string _command;

        #endregion

        public QueryError Error { get { return _error; } set { _error = value;  } }

        public List<Suggestions> Suggestions { get { return _suggestions; } }

        public List<object> ModuleInputs { get { return _moduleInputs; } set { _moduleInputs = value; } }

        public string Command { get { return _command; } set { _command = value; } }

        public string Name { get; set; }
    }
}
