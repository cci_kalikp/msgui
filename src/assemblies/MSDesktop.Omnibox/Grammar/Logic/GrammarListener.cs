﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using MSDesktop.Omnibox.Interfaces;

namespace MSDesktop.Omnibox.Grammar
{
    public interface IMessagePublisher
    {
        void Publish(string key, object value);
    }
    public interface IMessageListener
    {
        void Subscribe(string key, EventHandler<MessageArgs> e);
        void Unsubscribe(string key, EventHandler<MessageArgs> e);
    }

    public class MessageArgs : EventArgs
    {
        public MessageArgs(string key, object value) { Key = key; Value = value; }
        public string Key { get; private set; }
        public object Value { get; private set; }
    }

    public class GrammarListener
    {
        public static readonly string REGISTER_KEY = "GrammarRegister";
        public static readonly string MESSAGE_KEY = "GrammarMessage";
        public static readonly string NAME_KEY = "GrammarName";
        public static readonly string TOKEN_KEY = "GrammarTokens";
        public class ExecuteEventArgs : EventArgs
        {
            public ExecuteEventArgs(string cmd, int c, IEnumerable<object> tok) { Command = cmd; Count = c; Tokens = tok; }
            public string Command { get; private set; }
            public int Count { get; private set; }
            public IEnumerable<object> Tokens { get; private set; }
        }

        public event EventHandler<ExecuteEventArgs> Execute;

        EventHandler<MessageArgs> _listener;
        HashSet<string> _cmds = new HashSet<string>();

        /// <summary>
        /// This constructor subscribes to a message event on the message bus.  This grammar object will do filtering
        /// based on the message before it executes the IGrammar interface, just in case it is hooked up to a broadcast
        /// bus without selective filtering.
        /// </summary>
        /// <param name="name">The grammar's command; the first token the user types to denote the module.</param>
        /// <param name="grammar">Grammar object to register.</param>
        /// <param name="messageBusListener">
        /// Message bus listener.
        /// Use this if there's no specialized implementation of a message bus.
        /// For Concord applications, please use ConcordGrammarListener.
        /// This parameter should only be null for derived classes.
        /// </param>
        public GrammarListener(EventHandler<MessageArgs> messageBusListener)
        {
            _listener = messageBusListener;

            if (_listener != null)
                _listener += handleMessage;
        }

        public void Register(IGrammar grammar, IMessagePublisher publisher)
        {
            if(!_cmds.Contains(grammar.Name))
                _cmds.Add(grammar.Name);

            publisher.Publish(REGISTER_KEY, grammar);
        }

        protected void handleMessage(object sender, MessageArgs e)
        {
            //We expect a Dictionary of key/value pairs as our payload.
            if (e.Value is IDictionary<string, object>)
            {
                var dict = e.Value as IDictionary<string, object>;
                    
                //Check if our command is registered
                string cmd = dict[GrammarListener.NAME_KEY] as string;
                if(_cmds.Contains(cmd))
                {
                    IEnumerable<object> paramTokens = dict[GrammarListener.TOKEN_KEY] as IEnumerable<object>;
                    Execute(this, new ExecuteEventArgs(cmd, paramTokens.Count(), paramTokens));
                }
            }
            else
            {
                throw new ArgumentException("Grammar listener expects the payload to be of type IDictionary<string, object>");
            }
        }

        ~GrammarListener()
        {
            if (_listener != null)
                _listener -= handleMessage;
        }
    }
}
