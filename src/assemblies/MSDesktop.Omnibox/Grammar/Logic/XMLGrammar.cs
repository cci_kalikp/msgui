﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Text.RegularExpressions;
using MSDesktop.Omnibox.Interfaces;


namespace MSDesktop.Omnibox.Grammar
{
    /// <summary>
    /// This class is the implementation of IGrammar currently used by Bifrost.
    /// It uses XML files which adhere to the grammar.xml schema.
    /// </summary>
    /// <author>cottrjon</author>
    
    public class XMLGrammar : IGrammar
    {
        #region Fields
        private readonly Regex parseRegex = new Regex(@"((?:(?:\\\s)|(?:[^\s""]))+)\s*|""([^""]*)""\s*");
        private AbstractGrammarNode _grammarRoot;
        private string _name;
        private string _description;
        private IEnumerable<string> _examples = new List<string>();
        private Dictionary<string, string> _commands = new Dictionary<string, string>();

        #endregion

        #region Properties
        public AbstractGrammarNode GrammarRoot { get { return _grammarRoot; } }

        public string Name { get { return _name; } }

        public string Description
        {
            get { return _description; }
        }

        public IEnumerable<string> Examples
        {
            get { return _examples; }
        }

        public Dictionary<string, string> Commands
        {
            get { return _commands; }
        }
        #endregion

        #region XMLGrammar Builders
        public static XMLGrammar BuildXMLGrammar(string text)
        {
            //XmlTextReader xtr = new XmlTextReader(filename);
            XmlDocument xd = new XmlDocument();
            xd.LoadXml(text);
            //xd.Load(xtr);
            return BuildXMLGrammar(xd);
        }
        public static XMLGrammar BuildXMLGrammar(XmlDocument xmlDocument)
        {
            if (xmlDocument == null)
                throw new ArgumentNullException("xmlDocument must not be null");

            XMLGrammar grammar = new XMLGrammar();

            XmlSchema schema = new XmlSchema();
            xmlDocument.Schemas.Add(schema);

            grammar._grammarRoot = new StringNode();
            grammar._grammarRoot.MinOccur = 0;
            grammar._grammarRoot.MaxOccur = 0;

            XmlNode xmlRoot = xmlDocument.FirstChild;
            grammar._name = xmlRoot.Attributes["name"].Value;
            grammar._description = xmlRoot.Attributes["description"].Value;

            foreach (XmlNode cmd in xmlRoot)
            {
                if(cmd.Name == "examples")
                {
                    continue;
                }

                List<AbstractGrammarNode> topNodes = new List<AbstractGrammarNode>();
                List<AbstractGrammarNode> bottomNodes = new List<AbstractGrammarNode>();

                grammar.BuildTree(cmd.ChildNodes, out topNodes, out bottomNodes);

                foreach (var n in topNodes)
                {
                    grammar._grammarRoot.AddChild(n);
                }

            }

            var exampleNodes = xmlDocument.SelectSingleNode("module/examples");
            var examples = new List<string>();

            if (exampleNodes != null)
            {
                for (int nodeCount = 0; nodeCount < exampleNodes.ChildNodes.Count; nodeCount++)
                {
                    var example = exampleNodes.ChildNodes[nodeCount];
                    if (example != null && !string.IsNullOrEmpty(example.InnerText))
                    {
                        examples.Add(example.InnerText);
                    }
                }
            }

            grammar._examples = examples;

            return grammar;
        }
        #endregion

        public List<string> GetTokens(string query)
        {
            Match match = parseRegex.Match(query);
            List<string> tokens = new List<string>();

            while (match.Success)
            {
                if (match.Groups[1].Success)
                    tokens.Add(match.Groups[1].Value.Replace("\\ ", " "));
                else if (match.Groups[2].Success)
                    tokens.Add(match.Groups[2].Value);

                match = match.NextMatch();
            }
            return tokens;
        }


        public bool IsQueryValid(string query, out int errorTokenIndex)
        {
            errorTokenIndex = -1;

            if (query == null)
                return false;
            if (query == "")
                return true;

            bool endsInSpace = false;

            if (query[query.Length - 1] == ' ')
                endsInSpace = true;

            List<string> tokens = GetTokens(query);

            return IsValid(tokens, _grammarRoot, 0, 0, ref errorTokenIndex, endsInSpace);


        }

        public bool IsQueryComplete(string query)
        {
            if (query == null)
                return false;

            List<string> tokens = GetTokens(query);

            return IsComplete(tokens, _grammarRoot, 0);

        }

        public Dictionary<string, SortedSet<string>> Autocompletions(string partialQuery)
        {
          bool endsInSpace = false;

            if (partialQuery.Length > 0 && partialQuery[partialQuery.Length - 1] == ' ')
                endsInSpace = true;

            List<string> tokens = GetTokens(partialQuery);

            int errorTokenIndex = -1;
            var autocompletions = new Dictionary<string, SortedSet<string>>(); ;
            if (IsValid(tokens, _grammarRoot, 0, 0, ref errorTokenIndex, endsInSpace, null, autocompletions))
                return autocompletions;
            else
                throw new ApplicationException("AutoComplete received an invalid query.");
        }


        public List<object> PartialQueryToModuleInputs(string partialQuery)
        {
            List<string> tokens = GetTokens(partialQuery);

            bool endsInSpace = false;

            if (partialQuery.Length > 0 && partialQuery[partialQuery.Length - 1] == ' ')
                endsInSpace = true;

            Stack<object> moduleStack = new Stack<object>();

            int errorTokenIndex = -1;

            if (IsValid(tokens, _grammarRoot, 0, 0, ref errorTokenIndex, endsInSpace, moduleStack))
                return moduleStack.Reverse<object>().ToList<object>();
            else
                throw new ApplicationException("PartialQueryToModuleInputs was called on an invalid query.");
        }

        public List<object> QueryToModuleInputs(string query)
        {
            List<string> tokens = GetTokens(query);

            Stack<object> moduleStack = new Stack<object>();

            if (IsComplete(tokens, _grammarRoot, 0, moduleStack))
                return moduleStack.Reverse<object>().ToList<object>();
            else
                throw new ApplicationException("QueryToModuleInputs was called on an incomplete or invalid query.");
        }


        public void ComputeTokenRange(string query, int tokenIndex, out int startIndex, out int tokenLength)
        {
            Match match = parseRegex.Match(query);
            startIndex = -1;
            tokenLength = -1;

            int count = 0;

            while (match.Success)
            {
                if (count == tokenIndex)
                {
                    startIndex = match.Index;
                    tokenLength = match.Value.Trim().Length;
                    return;
                }

                match = match.NextMatch();
                count++;
            }

            throw new ArgumentOutOfRangeException("tokenIndex is out of bounds");
        }

        public bool ShouldAutoCompleteText
        {
            get { return true; }
        }

        #region Helper Methods

        // expects errorTokenIndex to be 0 or -1 to begin with
        private bool IsValid(List<string> tokens, AbstractGrammarNode node, int currOccur, int currTokenIndex, ref int errorTokenIndex, bool endsInSpace,
                             Stack<object> moduleStack = null, Dictionary<string, SortedSet<string>> autocompletions = null)
        {
            bool result = false;
            if (tokens.Count == 0)
            {
                foreach (var child in node.Children)
                {
                    if (autocompletions != null)
                    {
                        string hint = child.Hint;
                        SortedSet<string> ac;
                        if (!autocompletions.TryGetValue(hint, out ac))
                        {
                            autocompletions[hint] = new SortedSet<string>();
                        }

                        ac = autocompletions[hint];
                        foreach (string str in child.Autocompletion(""))
                        {
                            ac.Add(str);
                        }

                    }
                }
                return true;
            }
            if (tokens.Count == 1)
            {
                if (currOccur < node.MaxOccur)
                {
                    if (node.IsCompleteMatch(tokens[0]) && endsInSpace)
                    {
                        if (moduleStack != null)
                            moduleStack.Push(node.GetModuleInput(tokens[0]));
                        if (currOccur + 1 < node.MaxOccur)
                        {
                            if (autocompletions != null)
                            {
                                string hint = node.Hint;
                                SortedSet<string> ac;
                                if (!autocompletions.TryGetValue(hint, out ac))
                                {
                                    autocompletions[hint] = new SortedSet<string>();
                                }

                                ac = autocompletions[hint];
                                foreach (string str in node.Autocompletion(""))
                                {
                                    ac.Add(str);
                                }
                            }
                        }
                        foreach (var child in node.Children)
                        {
                            if (autocompletions != null)
                            {
                                string hint = child.Hint;
                                SortedSet<string> ac;
                                if (!autocompletions.TryGetValue(hint, out ac))
                                {
                                    autocompletions[hint] = new SortedSet<string>();
                                }

                                ac = autocompletions[hint];
                                foreach (string str in child.Autocompletion(""))
                                {
                                    ac.Add(str);
                                }
                            }

                        }

                        return true;
                    }
                    else if (node.IsPartialMatch(tokens[0]))
                    {
                        if (!endsInSpace)
                        {
                            if (autocompletions != null)
                            {
                                string hint = node.Hint;
                                SortedSet<string> ac;
                                if (!autocompletions.TryGetValue(hint, out ac))
                                {
                                    autocompletions[hint] = new SortedSet<string>();
                                }

                                ac = autocompletions[hint];
                                foreach (string str in node.Autocompletion(tokens[0]))
                                {
                                    ac.Add(str);
                                }
                            }
                        }
                        else
                            return false;

                        return true;
                    }


                }

                if (currOccur >= node.MinOccur)
                {
                    foreach (var child in node.Children)
                    {
                        List<string> tokensCopy = tokens.GetRange(0, tokens.Count);

                        if (IsValid(tokensCopy, child, 0, currTokenIndex, ref errorTokenIndex, endsInSpace, moduleStack, autocompletions))
                            result = true;
                    }
                }

                if (errorTokenIndex < currTokenIndex && !result)
                    errorTokenIndex = currTokenIndex;

                return result;
            }

            if (currOccur >= node.MinOccur)
            {
                foreach (var child in node.Children)
                {
                    List<string> tokensCopy = tokens.GetRange(0, tokens.Count);

                    if (IsValid(tokensCopy, child, 0, currTokenIndex, ref errorTokenIndex, endsInSpace, moduleStack, autocompletions))
                    {
                        return true;
                    }
                    else
                    {
                        if (errorTokenIndex < currTokenIndex)
                            errorTokenIndex = currTokenIndex;
                    }
                }
                if (node.Children.Count == 0 && tokens.Count > 0 && errorTokenIndex < currTokenIndex)
                    errorTokenIndex = currTokenIndex;



            }

            if (currOccur < node.MaxOccur)
            {
                if (node.IsCompleteMatch(tokens[0]))
                {
                    List<string> tokensCopy = tokens.GetRange(1, tokens.Count - 1);

                    if (moduleStack != null)
                        moduleStack.Push(node.GetModuleInput(tokens[0]));

                    if (IsValid(tokensCopy, node, currOccur + 1, currTokenIndex + 1, ref errorTokenIndex, endsInSpace, moduleStack, autocompletions))
                    {
                        result = true;
                        if (moduleStack != null)
                            return result;
                    }

                    else if (moduleStack != null)
                        moduleStack.Pop();
                }
            }

            return result;
        }

        private bool IsComplete(List<string> tokens, AbstractGrammarNode node, int currOccur, Stack<object> moduleStack = null)
        {
            if (tokens.Count == 0)
            {
                if (currOccur >= node.MinOccur && node.Children.Count == 0)
                {
                    return true;
                }
                else if (node.Children.Count > 0)
                {
                    foreach (var child in node.Children)
                    {
                        List<string> tokensCopy = tokens.GetRange(0, tokens.Count);

                        if (IsComplete(tokensCopy, child, 0, moduleStack))
                            return true;
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }

            if (currOccur >= node.MinOccur)
            {
                foreach (var child in node.Children)
                {
                    List<string> tokensCopy = tokens.GetRange(0, tokens.Count);

                    if (IsComplete(tokensCopy, child, 0, moduleStack))
                        return true;
                }
            }

            if (currOccur < node.MaxOccur && node.IsCompleteMatch(tokens[0]))
            {
                List<string> tokensCopy = tokens.GetRange(1, tokens.Count - 1);

                if (moduleStack != null)
                    moduleStack.Push(node.GetModuleInput(tokens[0]));

                if (IsComplete(tokensCopy, node, currOccur + 1, moduleStack))
                    return true;
                if (moduleStack != null)
                    moduleStack.Pop();
            }

            return false;
        }


        private void BuildTree(XmlNodeList xmlNodeList, out List<AbstractGrammarNode> topNodes, out List<AbstractGrammarNode> bottomNodes)
        {
            topNodes = new List<AbstractGrammarNode>();
            bottomNodes = new List<AbstractGrammarNode>();
            List<AbstractGrammarNode> parentNodes = new List<AbstractGrammarNode>();
            for (int i = 0; i < xmlNodeList.Count; ++i)
            {
                List<AbstractGrammarNode> top = new List<AbstractGrammarNode>();
                List<AbstractGrammarNode> bottom = new List<AbstractGrammarNode>();
                XmlNode currentNode = xmlNodeList[i];
                if (currentNode.Name == "choices")
                {
                    foreach (XmlNode choice in currentNode.ChildNodes)
                    {
                        List<AbstractGrammarNode> localTop = new List<AbstractGrammarNode>();
                        List<AbstractGrammarNode> localBottom = new List<AbstractGrammarNode>();
                        BuildTree(choice.ChildNodes, out localTop, out localBottom);
                        top.AddRange(localTop);
                        bottom.AddRange(localBottom);
                    }
                }
                else
                {

                    int minOccur = 1;
                    int maxOccur = 1;

                    if (currentNode.Attributes.GetNamedItem("min") != null)
                        minOccur = Convert.ToInt32(currentNode.Attributes["min"].Value);

                    if (currentNode.Attributes.GetNamedItem("max") != null)
                        maxOccur = Convert.ToInt32(currentNode.Attributes["max"].Value);

                    if (currentNode.Attributes.GetNamedItem("command") != null)
                    {
                        if(currentNode.Attributes.GetNamedItem("command").InnerText != string.Empty)
                        {                            
                            this._commands[currentNode.InnerText] = currentNode.Attributes.GetNamedItem("command").InnerText;
                        }
                    }

                    AbstractGrammarNode node = GrammarNodeGenerator.GetNode(currentNode.Name, currentNode.InnerText, minOccur, maxOccur);
                    top.Add(node);
                    bottom.Add(node);
                }

                if (i == 0)
                {
                    topNodes.AddRange(top);
                }

                if (i == xmlNodeList.Count - 1)
                {
                    bottomNodes.AddRange(bottom);
                }

                foreach (var p in parentNodes)
                {
                    foreach (var t in top)
                    {
                        p.AddChild(t);
                    }
                }
                parentNodes = bottom;
            }
        }
        #endregion
    }
}
