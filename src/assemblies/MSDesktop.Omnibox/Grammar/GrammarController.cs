﻿using System;
using System.Collections.Generic;
using System.Linq;
using MSDesktop.Omnibox.Interfaces;

namespace MSDesktop.Omnibox.UI.Grammar
{
    public interface IGrammarController
    {
        Dictionary<string, string> Aliases { get; set; }
        void RegisterModuleGrammar(IGrammar grammar);
        List<QueryResult> ValidateCompleteInput(string query, out string outputQuery, string toolName);
        List<QueryResult> ValidatePartialInput(string query, out string outputQuery, bool currentInputContainsPrevInput, string toolName);
        IDictionary<string, string> GetRegisteredGrammers();
        IDictionary<string, IList<string>> GetAllExamples();        
    }

    /// <summary>
    /// This is the top layer of the omnibox model, the controller of the MVC should talk to this
    /// </summary>
    public class GrammarController : IGrammarController
    {
        private readonly Dictionary<string, IGrammar> _grammars = new Dictionary<string, IGrammar>();
        private List<string> _currentValidTools = new List<string>();

        private const char ShortcutChar = '/';

        private Dictionary<string, string> _aliases = new Dictionary<string, string>();
        public Dictionary<string, string> Aliases { get { return _aliases; } set { _aliases = value; } }


        public void RegisterModuleGrammar(IGrammar grammar)
        {
            if (!_grammars.ContainsKey(grammar.Name))
            {
                _grammars[grammar.Name.ToUpper()] = grammar;
            }
            _currentValidTools = _grammars.Keys.ToList();
        }

        public IDictionary<string, string> GetRegisteredGrammers()
        {
            return _grammars.Values.ToDictionary(grammar => grammar.Name, grammar => grammar.Description);
        }

        public IDictionary<string, IList<string>> GetAllExamples()
        {
            return _grammars.Values.ToDictionary(value => string.Format("{0} - {1}", value.Name.ToLower(), value.Description),
                                                 value => value.Examples.ToList() as IList<string>);
        }

        public List<QueryResult> ValidateCompleteInput(string query, out string outputQuery, string toolName)
        {
            if (query == null)
            {
                throw new ArgumentNullException("query"); 
            }

            List<QueryResult> results = new List<QueryResult>();            
            query = query.Trim();
            outputQuery = query;

            if (toolName == string.Empty)
            {                
                foreach(string tool in _currentValidTools.FindAll(t => _grammars[t].IsQueryComplete(query)))
                {
                    results.Add(GetQueryResultWithoutError(query, tool, _grammars[tool], true));
                }
            }
            else
            {
                IGrammar g = _grammars[toolName];
                if (g.IsQueryComplete(query))
                {
                    results.Add(GetQueryResultWithoutError(query, toolName, g, true));
                }
            }

            return results;
        }

        public List<QueryResult> ValidatePartialInput(string query, out string outputQuery, bool currentInputContainsPrevInput, string toolName)
        {
            if (query == null)
            {
                throw new ArgumentNullException("query");
            }

            List<QueryResult> results = new List<QueryResult>();

            query = query.TrimStart(' ');
            outputQuery = query;

            if (query.Length == 0)
                return results;

            int errorTokenIndex;

            if(toolName == string.Empty)
            {
                if(!currentInputContainsPrevInput)
                {
                    _currentValidTools = _grammars.Keys.ToList();
                }

                _currentValidTools.RemoveAll(tool => !(_grammars[tool].IsQueryValid(query, out errorTokenIndex)));

                if (_currentValidTools.Count == 0)
                {
                    results.Add(GetQueryResultWithError("Invalid token"));
                }
                else
                {
                    _currentValidTools.ForEach(tool => results.Add(GetQueryResultWithoutError(query, tool, _grammars[tool], false)));
                }
            }
            else
            {
                IGrammar g = _grammars[toolName];
                if (!g.IsQueryValid(query, out errorTokenIndex))
                {                    
                    results.Add(GetQueryResultWithError("Invalid token for tool : " + toolName));
                }
                else
                {
                    results.Add(GetQueryResultWithoutError(query, toolName, g, false));
                }

            }

            return results;
        }

        private QueryResult GetQueryResultWithoutError(string query, string tool, IGrammar grammar, bool isQueryComplete)
        {
            QueryResult queryResult = new QueryResult();
            queryResult.Name = tool;
            Dictionary<string, SortedSet<string>> autocompletions = grammar.Autocompletions(query);

            foreach (string key in autocompletions.Keys)
            {
                queryResult.Suggestions.Add(new Suggestions(key, autocompletions[key].ToList()));
            }

            if (isQueryComplete)
            {
                queryResult.ModuleInputs = grammar.QueryToModuleInputs(query);
                foreach(object obj in queryResult.ModuleInputs)
                {
                    string token = (string)obj;
                    if (grammar.Commands.ContainsKey(token))
                    {
                        queryResult.Command = grammar.Commands[token];
                        break;
                    }
                }
            }
            else
            {
                queryResult.ModuleInputs = grammar.PartialQueryToModuleInputs(query);
            }            

            return queryResult;
        }

        private QueryResult GetQueryResultWithError(string errorMessage)
        {
            QueryResult errorQueryResult = new QueryResult();
            QueryError queryError = new QueryError(0, 0, errorMessage);
            errorQueryResult.Error = queryError;
            return errorQueryResult;
        }
    }
}
