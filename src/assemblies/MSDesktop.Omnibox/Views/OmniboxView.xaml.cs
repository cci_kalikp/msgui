﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Infragistics.Controls.Menus;
using MSDesktop.Omnibox.Message;
using Microsoft.Practices.Composite.UnityExtensions;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MSDesktop.Omnibox.UI;
using MSDesktop.Omnibox.UI.Grammar;
using MSDesktop.Omnibox.UI.Model;
using MSDesktop.Omnibox.UI.ViewModels;
using MSDesktop.Omnibox.UI.Views;
using Brush = System.Windows.Media.Brush;
using Brushes = System.Windows.Media.Brushes;
using PlacementMode = Infragistics.Controls.Menus.PlacementMode;
using Font = System.Windows.FontStyle;
using Fonts = System.Windows.FontStyles;
using MSDesktop.Omnibox.Grammar;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Controls.Primitives;

namespace MSDesktop.Omnibox.UI.Views
{
    /// <summary>
    /// Interaction logic for OmniboxView.xaml
    /// </summary>
    public partial class OmniboxView
    {
        private readonly IGrammarController _grammarController;
        private readonly ICommunicator _communicator;
        private IModulePublisher<OmniboxMessage> _publisher;
        private readonly IPublisher<OmniboxMessage> _omniboxCommunicator;

        private XamContextMenu _xamMenu = new XamContextMenu { Placement = PlacementMode.AlignedBelow };
        private string _lastInputCache = string.Empty;
        private bool _displayingTextCompletion = false;
        private bool _suppressTextChangedEvent = false;
        private string _specificTool = string.Empty;
        private string _savedQuery = string.Empty;
        private bool _showingError = false;
        private const Key CompletionKey = Key.Tab;

        private Popup _omniboxPopup;

        public OmniboxView(IGrammarController grammarController, IUnityContainer container, ICommunicator communicator)
        {
            _grammarController = grammarController;
            _communicator = communicator;
            _publisher = _communicator.GetPublisher<OmniboxMessage>();
            _omniboxCommunicator = container.IsRegistered<IPublisher<OmniboxMessage>>() ? container.Resolve<IPublisher<OmniboxMessage>>() : null;
            InitializeComponent();

            var cntxtMgr = new ContextMenuManager
            {
                ModifierKeys = ModifierKeys.Control | ModifierKeys.Shift,
                OpenMode = OpenMode.None
            };
            Infragistics.Controls.Menus.ContextMenuService.SetManager(InputTextEditor, cntxtMgr);
            cntxtMgr.ContextMenu = _xamMenu;

            CommandBindings.Add(new CommandBinding(Commands.ShowOmniboxHelp, ShowOmniboxHelpCommandExecuted));
            CommandBindings.Add(new CommandBinding(Commands.ShowSupportedTools, ShowSupportedToolsCommandexecuted));
            CommandBindings.Add(new CommandBinding(Commands.ClearOmniBox, ClearOmniBoxExecuted));
        }

        public Popup OmniboxPopup { get { return _omniboxPopup; } set { _omniboxPopup = value; } }

        private void ShowSupportedToolsCommandexecuted(object sender, ExecutedRoutedEventArgs e)
        {
            var examples = _grammarController.GetAllExamples();

            if (examples == null)
            {
                return;
            }

            var listOfToolExamples = (from example in examples
                                      let examplesColl =
                                          example.Value.Select(value => new Example() {ExampleDescription = value})
                                                 .ToList()
                                      select new OmniboxToolAndExampleModel(example.Key, examplesColl)).ToList();

            var viewModel = new OmniboxToolsAndExamplesViewModel(listOfToolExamples);
            var view = new OmniboxToolsAndExamplesView {DataContext = viewModel};

            view.ShowDialog();
        }

        private static void ShowOmniboxHelpCommandExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            Process.Start("http://wiki-eu.ms.com/twiki/cgi-bin/view/TD/Omnibox");
        }

        private void ClearOmniBoxExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            ResetOmnibox();
        }

        private void ResetOmnibox()
        {
            InputTextEditor.Clear();
            _xamMenu.IsOpen = false;
            InputTextEditor.Focus();

            _specificTool = string.Empty;
            _displayingTextCompletion = false;
            _showingError = false;
        }

        public void HideOmnibox()
        {
            _xamMenu.IsOpen = false;

            _specificTool = string.Empty;
            _displayingTextCompletion = false;
            _showingError = false;

            _omniboxPopup.IsOpen = false;
        }

        public void SetInputTextColour(int offset, int length, Brush color)
        {
            InputTextEditor.SelectionStart = offset;
            InputTextEditor.SelectionLength = length;
            InputTextEditor.SelectionBrush = color;
        }

        private void InputTextEditorPreviewKeyDown(object sender, KeyEventArgs e)
        {
            HandlePreviewKeyDown(sender,e);
        }

        private void ShowSuggestionsAsPopup(IEnumerable<QueryResult> qResults)
        {
            bool checkIfQueryComplete = false;
            string input = InputTextEditor.Text;
            string completionKeyword = string.Empty;


            _displayingTextCompletion = false;

            if(_xamMenu.HasItems)
            {
                _xamMenu.Items.Clear();
            }

            foreach(QueryResult qResult in qResults)
            {
                if(qResult.Suggestions.Count == 0)
                {
                    checkIfQueryComplete = true;
                }
                
                foreach(Suggestions suggestion in qResult.Suggestions)
                {
                    TextBlock hintText = new TextBlock()
                    {
                        Margin = new Thickness(-20, 0, 0, 0),
                        Text = qResult.Name + " : " + suggestion.Hint
                    };
                    XamMenuItem xmHintMenuItem = CreateXamMenuItem(hintText, Brushes.Black, Brushes.LightSteelBlue, Fonts.Italic, false);
                    _xamMenu.Items.Add(xmHintMenuItem);

                    if (suggestion.Autocomplete.Count > 0)
                    {
                        if (completionKeyword == string.Empty)
                        {
                            completionKeyword = suggestion.Autocomplete[0];
                        }

                        foreach(string autocomplete in suggestion.Autocomplete)
                        {
                            string textWithoutKeyword = input.Substring(0, input.LastIndexOf(' ') + 1);
                            TextBlock autocompleteText = CreateTextBlock(textWithoutKeyword, autocomplete, textWithoutKeyword + autocomplete);

                            XamMenuItem xmAutocompleteMenuItem = CreateXamMenuItem(autocompleteText, Brushes.Black, Brushes.Honeydew, Fonts.Normal, true, qResult.Name);
                            xmAutocompleteMenuItem.PreviewKeyDown += new KeyEventHandler(xmAutocompleteMenuItem_PreviewKeyDown);
                            xmAutocompleteMenuItem.PreviewMouseDown += new MouseButtonEventHandler(xmAutocompleteMenuItem_PreviewMouseDown);
                            _xamMenu.Items.Add(xmAutocompleteMenuItem);
                        }
                    }
                    else
                    {
                        TextBlock noAutocompleteText = CreateTextBlock(input, string.Empty, input);
                        XamMenuItem xmNoAutoCompleteMenuItem = CreateXamMenuItem(noAutocompleteText, Brushes.Black, Brushes.Honeydew, Fonts.Normal, true, qResult.Name);
                        _xamMenu.Items.Add(xmNoAutoCompleteMenuItem);
                    }
                }
            }

            if ((completionKeyword != string.Empty) && (input.StartsWith(_lastInputCache)) && (input.Length > _lastInputCache.Length))
            {
                DisplayCompletionText(completionKeyword);
            }

            if(checkIfQueryComplete)
            {
                CheckQueryForCompletion();
            }

            if (!_displayingTextCompletion)
            {
                InputTextEditor.CaretIndex = InputTextEditor.Text.Length;
            }

            checkIfQueryComplete = false;
            _xamMenu.Focusable = true;
            _xamMenu.IsOpen = true;
            InputTextEditor.Focus();
        }

        void xmAutocompleteMenuItem_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            XamMenuItem autocompleteOption = e.Source as XamMenuItem;
            if (autocompleteOption != null && autocompleteOption.IsEnabled)
            {
                SelectAutocompleteOption(autocompleteOption);
                e.Handled = true;
            }
        }

        void xmAutocompleteMenuItem_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key.Equals(Key.Enter))
            {
                XamMenuItem autocompleteOption = e.Source as XamMenuItem;
                if (autocompleteOption != null && autocompleteOption.IsEnabled)
                {
                    SelectAutocompleteOption(autocompleteOption);
                }
                e.Handled = true;
            }
        }

        private void SelectAutocompleteOption(XamMenuItem autocomplete)
        {
            TextBlock txtBlock = autocomplete.Header as TextBlock;
            _specificTool = (string)autocomplete.Tag;
            InputTextEditor.Text = (string)txtBlock.Tag + ' ';
        }

        private void CheckQueryForCompletion()
        {
            string outputQuery;
            List<QueryResult> queryResults = _grammarController.ValidateCompleteInput(InputTextEditor.Text, out outputQuery, _specificTool);
            
            foreach(QueryResult queryResult in queryResults)
            {
                TextBlock toolName = new TextBlock()
                {
                    Margin = new Thickness(-20, 0, 0, 0),
                    Text = queryResult.Name
                };

                XamMenuItem xmToolName = CreateXamMenuItem(toolName, Brushes.Black, Brushes.LightSteelBlue, Fonts.Italic, false);
                _xamMenu.Items.Add(xmToolName);

                TextBlock command = new TextBlock()
                {
                    Margin = new Thickness(-20, 0, 0, 0),
                    Text = "> " + InputTextEditor.Text
                };

                IDictionary<string, object> msg = new Dictionary<string, object>();
                msg[GrammarListener.NAME_KEY] = queryResult.Name;
                msg[GrammarListener.TOKEN_KEY] = queryResult.ModuleInputs;
                OmniboxMessage omniboxMsg = new OmniboxMessage(queryResult.Name, msg);
                if (queryResult.Command != string.Empty)
                {
                    omniboxMsg.Command = queryResult.Command;
                }

                XamMenuItem xmCommand = CreateXamMenuItem(command, Brushes.Green, Brushes.Honeydew, Fonts.Italic, true, omniboxMsg);

                xmCommand.PreviewKeyDown += new KeyEventHandler(xmCommand_PreviewKeyDown);
                xmCommand.PreviewMouseDown += new MouseButtonEventHandler(xmCommand_PreviewMouseDown);
                _xamMenu.Items.Add(xmCommand);
            }
        }

        void xmCommand_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            XamMenuItem xmExecuteCommand = e.Source as XamMenuItem;
            if (xmExecuteCommand != null && xmExecuteCommand.IsEnabled)
            {
                OmniboxMessage omniboxMsg = xmExecuteCommand.Tag as OmniboxMessage;
                SendMessage(omniboxMsg);
            }
            e.Handled = true;
        }

        void xmCommand_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key.Equals(Key.Enter))
            {
                XamMenuItem xmExecuteCommand = e.Source as XamMenuItem;
                if (xmExecuteCommand != null && xmExecuteCommand.IsEnabled)
                {
                    OmniboxMessage omniboxMsg = xmExecuteCommand.Tag as OmniboxMessage;
                    SendMessage(omniboxMsg);
                }
                e.Handled = true;
            }
        }

        private void ExecuteCommandOrShowError()
        {
            string outputQuery;
            List<QueryResult> queryResults = _grammarController.ValidateCompleteInput(InputTextEditor.Text, out outputQuery, _specificTool);
            
            if (queryResults.Count > 0)
            {
                IDictionary<string, object> msg = new Dictionary<string, object>();
                msg[GrammarListener.NAME_KEY] = queryResults[0].Name;
                msg[GrammarListener.TOKEN_KEY] = queryResults[0].ModuleInputs;
                OmniboxMessage omniboxMsg = new OmniboxMessage(queryResults[0].Name, msg);
                if (queryResults[0].Command != string.Empty)
                {
                    omniboxMsg.Command = queryResults[0].Command;
                }
                SendMessage(omniboxMsg);
            }
            else
            {
                string errorMsg;

                if(_specificTool == string.Empty)
                {
                    errorMsg = "Query not complete";
                }
                else
                {
                    errorMsg = "Query not complete for tool : " + _specificTool;
                }
                QueryError queryError = new QueryError(0, 0, errorMsg);
                ShowErrorAsPopup(queryError, false);
                _showingError = true;
            }
        }

        private void ShowErrorAsPopup(QueryError error, bool clearXamMenu)
        {
            if (_xamMenu.HasItems && clearXamMenu)
            {
                _xamMenu.Items.Clear();
            }

            XamMenuItem xmErrorMenuItem = CreateXamMenuItem(error.ErrorMessage, Brushes.DarkRed, Brushes.Honeydew, Fonts.Italic, true);
            _xamMenu.Items.Insert(0, xmErrorMenuItem);
            _xamMenu.IsOpen = true;
            InputTextEditor.Focus();
        }

        private XamMenuItem CreateXamMenuItem(object header, Brush foreground, Brush background, Font font,
                                              bool isEnabled, object tag = null)
        {
            XamMenuItem xmMenuItem = new XamMenuItem
            {
                Header = header,
                Foreground = foreground,
                Background = background,
                FontSize = 13,
                MinWidth = InputTextEditor.Width,
                FontStyle = font,
                Tag = tag,
                IsEnabled = isEnabled
            };
            return xmMenuItem;
        }

        private void DisplayCompletionText(string keyword)
        {
            string input = InputTextEditor.Text;
            int charactersMatched = input.Length - input.LastIndexOf(' ') - 1;
            string subHint = keyword.Substring(charactersMatched);

            _suppressTextChangedEvent = true;
            InputTextEditor.Text = input + subHint;
            InputTextEditor.CaretIndex = input.Length;
            SetInputTextColour(input.Length, subHint.Length, Brushes.DarkGreen);

            _displayingTextCompletion = true;
            _suppressTextChangedEvent = false;
        }

        private TextBlock CreateTextBlock(string normalText, string boldText, object tag)
        {
            TextBlock txtBlock = new TextBlock();

            Run normal = new Run(normalText);
            normal.FontWeight = System.Windows.FontWeights.Normal;
            txtBlock.Inlines.Add(normal);

            if(boldText != string.Empty)
            {
                Run bold = new Run(boldText);
                bold.FontWeight = System.Windows.FontWeights.ExtraBold;
                txtBlock.Inlines.Add(bold);
            }

            txtBlock.Tag = tag;

            return txtBlock;
        }

        public void HandlePreviewKeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case CompletionKey:
                    if (_displayingTextCompletion)
                    {
                        InputTextEditor.Text = InputTextEditor.Text + ' ';                        
                    }
                    break;
                case Key.Down:
                    if (_xamMenu != null && _xamMenu.IsOpen)
                    {
                        _xamMenu.Focus();

                    }
                    break;
                case Key.Up:
                    if (InputTextEditor.Text == string.Empty)
                    {
                        InputTextEditor.Text = _savedQuery;
                        InputTextEditor.CaretIndex = InputTextEditor.Text.Length;
                    }
                    break;
                case Key.Enter:
                    if(!_showingError && (InputTextEditor.Text != string.Empty))
                    {
                        ExecuteCommandOrShowError();
                    }
                    break;
                case Key.Escape:
                    if (_omniboxPopup != null && _omniboxPopup.IsOpen)
                    {
                        HideOmnibox();

                        e.Handled = true;
                    }
                    break;
            }
        }

        public void ValidatePartial()
        {
            string outputQuery;
            string input = InputTextEditor.Text;

            List<QueryResult> queryResults = _grammarController.ValidatePartialInput(input, out outputQuery, input.StartsWith(_lastInputCache), _specificTool);
            
            if ((queryResults.Count == 1) && (queryResults[0].Error != null))
            {
                ShowErrorAsPopup(queryResults[0].Error, true);
            }
            else
            {
                ShowSuggestionsAsPopup(queryResults);
            }

            _lastInputCache = input;
        }

        public void SendMessage(OmniboxMessage oMsg)
        {
            if(_omniboxCommunicator != null)
            {
                _omniboxCommunicator.Publish(oMsg);
            }
            if (_publisher != null)
            {
                _publisher.Publish(oMsg);
            }            
            _savedQuery = InputTextEditor.Text;
            ResetOmnibox();

            if(OmniboxPopup != null && OmniboxPopup.IsOpen)
            {
                OmniboxPopup.IsOpen = false;
            }            
        }

        private void InputTextEditor_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            _showingError = false;
            if(!_suppressTextChangedEvent)
            {                
                if (InputTextEditor.Text != string.Empty)
                {
                    ValidatePartial();
                }
                else
                {
                    _xamMenu.IsOpen = false;
                }
            }
        }
    }
}
