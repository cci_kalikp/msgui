﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;
using MSDesktop.Omnibox.UI.Views;

namespace MSDesktop.Views
{
    /// <summary>
    /// Interaction logic for OmniboxPopup.xaml
    /// </summary>
    public partial class OmniboxPopup : UserControl
    {
        public OmniboxPopup()
        {
            InitializeComponent();

            ToggleOmniboxPopup toggleOmniboxKey = new ToggleOmniboxPopup();

            KeyBinding toggleKeyBinding = new KeyBinding()
            {
                Command = toggleOmniboxKey,
                CommandParameter = OmniboxPopupControl,
                Key = Key.Q,
                Modifiers = ModifierKeys.Control
            };
            Application.Current.MainWindow.InputBindings.Add(toggleKeyBinding);
            OmniboxPopupUserControl.InputBindings.Add(toggleKeyBinding);

            HideOmniboxPopup hideOmniboxClick = new HideOmniboxPopup();

            MouseBinding hideMouseBinding = new MouseBinding()
            {
                Gesture = new MouseGesture(MouseAction.LeftClick, ModifierKeys.None),
                Command = hideOmniboxClick,
                CommandParameter = OmniboxPopupControl,                
            };
            Application.Current.MainWindow.InputBindings.Add(hideMouseBinding);

            HideOmniboxPopup hideOmniboxKey = new HideOmniboxPopup();

            KeyBinding hideKeyBinding = new KeyBinding()
            {
                Command = hideOmniboxKey,
                CommandParameter = OmniboxPopupControl,
                Key = Key.Escape
            };
            Application.Current.MainWindow.InputBindings.Add(hideKeyBinding);
            OmniboxPopupUserControl.InputBindings.Add(hideKeyBinding);
        }

        private class ToggleOmniboxPopup : ICommand
        {
            public void Execute(object parameter)
            {
                if (parameter is Popup)
                {
                    Popup p = parameter as Popup;

                    ContentControl c = (ContentControl)p.Child;
                    UserControl u = (UserControl)c.Content;
                    TextBox t = (TextBox)u.FindName("InputTextEditor");

                    if (p.IsOpen)
                    {
                        p.IsOpen = false;
                        t.Text = "";
                    }
                    else
                    {
                        Application.Current.MainWindow.Focus();
                        p.PlacementTarget = Application.Current.MainWindow;
                        p.Placement = PlacementMode.Center;
                        p.IsOpen = true;
                        t.Focus();
                        Keyboard.Focus(t);
                    }
                }
            }
            public bool CanExecute(object parameter)
            {
                return true;
            }

            public event EventHandler CanExecuteChanged;
        }

        private class HideOmniboxPopup : ICommand
        {
            public void Execute(object parameter)
            {
                if (parameter is Popup)
                {
                    Popup p = parameter as Popup;

                    ContentControl c = (ContentControl)p.Child;
                    OmniboxView ov = (OmniboxView)c.Content;
                    ov.HideOmnibox();
                }
            }
            public bool CanExecute(object parameter)
            {
                bool canExecute = false;
                if (parameter is Popup)
                {
                    Popup p = parameter as Popup;
                    if(p.IsOpen)
                    {
                        canExecute = true;
                    }
                }
                return canExecute;
            }

            public event EventHandler CanExecuteChanged;
        }
    }
}
