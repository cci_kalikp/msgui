﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MSDesktop.Omnibox.UI.ViewModels;

namespace MSDesktop.Omnibox.UI.Views
{
    /// <summary>
    /// Interaction logic for OmniboxToolsAndExamplesView.xaml
    /// </summary>
    public partial class OmniboxToolsAndExamplesView : Window
    {
        public OmniboxToolsAndExamplesView()
        {
            InitializeComponent();
            //var viewModel = new OmniboxToolsAndExamplesViewModel(new List<string>(){"abc","def"});
            //Resources.Add("viewModel", viewModel);
            //DataContext = viewModel;
        }
    }
}
