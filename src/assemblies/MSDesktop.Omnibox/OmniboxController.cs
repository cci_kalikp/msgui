﻿using System;
using System.Collections.Generic;
using MSDesktop.Omnibox.Interfaces;
using MSDesktop.Omnibox.Grammar;
using MSDesktop.Omnibox.UI.Grammar;

namespace MSDesktop.Omnibox.UI
{
    public class OmniboxRegistry : IOmniboxRegistry
    {
        private readonly IGrammarController _grammarController;

        public OmniboxRegistry(IGrammarController grammarController)
        {
            _grammarController = grammarController;
        }

        public void RegisterGrammar(IGrammar grammar)
        {
            _grammarController.RegisterModuleGrammar(grammar);
        }

        public void RegisterGrammar(string xmlDocument)
        {
            _grammarController.RegisterModuleGrammar(XMLGrammar.BuildXMLGrammar(xmlDocument));
        }
    }
}
