﻿using System;
using MorganStanley.Desktop.EntityServices.Entities;
using MorganStanley.Desktop.EntityServices.Extensions;
using MorganStanley.Desktop.EntityServices.Keys;

namespace MSDesktop.Omnibox.Message
{
    [Serializable]
    public class OmniboxMessage : MorganStanley.Desktop.EntityServices.Extensions.Message
    {
        private readonly object _value;
        private readonly string _moduleName;
        private string _command;

        public OmniboxMessage(string moduleName, object value)
        {
            _value = value;
            _moduleName = moduleName;
            _command = string.Empty;
        }

        public object Value { get { return _value; } }
        public string ModuleName { get { return _moduleName; } }
        public string Command { get { return _command; } set { _command = value; } }
    }

}
