﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MSDesktop.MessageRouting.ZeroConfig;
using MSDesktop.MessageRouting.Monitor.ViewModel;

namespace MSDesktop.MessageRouting.Monitor.View
{
    public partial class MessageRoutingMonitorContent : UserControl
    {
        public MessageRoutingMonitorContentViewModel MessageRoutingMonitorContentViewModel { get; private set; }

        public MessageRoutingMonitorContent(IRoutingCommunicator kraken, IRouteExplorer scout)
        {
            InitializeComponent();

            this.MessageRoutingMonitorContentViewModel = new MessageRoutingMonitorContentViewModel(kraken, scout, this.Dispatcher);
            this.DataContext = this.MessageRoutingMonitorContentViewModel;
            colInternal.Visibility = MessageRoutingMonitorContentViewModel.ShowInternalMessage
                                         ? Visibility.Visible
                                         : Visibility.Collapsed;
            dgvMessages.Items.SortDescriptions.Add(new System.ComponentModel.SortDescription("Added", System.ComponentModel.ListSortDirection.Descending));
        }
    }
}
