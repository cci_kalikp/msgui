﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MSDesktop.MessageRouting.Routing;
using System.ComponentModel;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;

namespace MSDesktop.MessageRouting.Monitor.Model
{
    public class RoutedCommunicationMonitorModel : ViewModelBase
    {
        public bool IsInternal { get; set; }
        private DeliveryInfo deliveryInfo;
        public DeliveryInfo DeliveryInfo
        {
            get
            {
                return deliveryInfo;
            }
            set
            {
                deliveryInfo = value;
                if (deliveryInfo != null)
                {
                    deliveryInfo.PropertyChanged += (object sender, PropertyChangedEventArgs e) => { this.Updated = DateTime.Now; };
                }
                OnPropertyChanged("DeliveryInfo");
            }
        }

        private string messageType;
        public string MessageType
        {
            get
            {
                return messageType;
            }
            set
            {
                messageType = value;
                OnPropertyChanged("MessageType");
            }
        }

        private DateTime added;
        public DateTime Added
        {
            get
            {
                return added;
            }
            set
            {
                added = value;
                OnPropertyChanged("Added");
            }
        }

        private DateTime? updated;
        public DateTime? Updated 
        { 
            get
            {
                return updated;
            }
            set
            {
                updated = value;
                OnPropertyChanged("Updated");
            }
        }

        public RoutedCommunicationMonitorModel()
        {
            Added = DateTime.Now;
        }
         
        protected override void OnPropertyChanged(string propertyName_)
        {
            base.OnPropertyChanged(propertyName_);
            if (propertyName_ != "Updated")
            {
                this.Updated = DateTime.Now;
            }
        } 
    }
}
