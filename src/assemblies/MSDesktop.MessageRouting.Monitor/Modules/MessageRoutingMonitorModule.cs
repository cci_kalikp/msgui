﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using MSDesktop.MessageRouting.Monitor.View;

using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MSDesktop.MessageRouting.ZeroConfig;

using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core;
using System.Windows.Media.Imaging;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;

namespace MSDesktop.MessageRouting.Monitor.Modules
{
    public class MessageRoutingMonitorModule : IMSDesktopModule
    {
        public const string MonitorWindowFactoryID = "MSDesktop.MessageRouting.Monitor.Window";
        public const string MonitorButtonID = "MSDesktop.MessageRouting.Monitor.ShowWindowButton";

        private readonly IChromeManager chromeManager;
        private readonly IChromeRegistry chromeRegistry;
        private readonly IRoutingCommunicator routingCommunicator;
        private readonly IRouteExplorer scout;

        public MessageRoutingMonitorModule(IChromeRegistry chromeRegistry, IChromeManager chromeManager, IRoutingCommunicator routingCommunicator, IRouteExplorer scout)
        {
            this.chromeRegistry = chromeRegistry;
            this.chromeManager = chromeManager;
            this.routingCommunicator = routingCommunicator;
            this.scout = scout;
        }

        public void Initialize()
        {
            var krakenIcon = new BitmapImage();
            krakenIcon.BeginInit();
            krakenIcon.UriSource = new Uri(@"/MSDesktop.MessageRouting.Monitor;component/Images/kraken32.png",UriKind.RelativeOrAbsolute);
            krakenIcon.EndInit();

            this.chromeRegistry.RegisterWindowFactory(MonitorWindowFactoryID, this.MonitorWindowFactory);

            this.chromeManager.AddWidget(MonitorButtonID, new InitialShowWindowButtonParameters
            {
                Text = string.Format("Routing Monitor (PID: {0})", Process.GetCurrentProcess().Id),
                ToolTip = string.Format("Routing Monitor ({0}, {1})", Assembly.GetExecutingAssembly().FullName, Process.GetCurrentProcess().Id),
                WindowFactoryID = MonitorWindowFactoryID,
                InitialParameters = new InitialWindowParameters()
                {
                    InitialLocation = InitialLocation.DockInNewTab
                },
                Image = krakenIcon
            }, chromeManager.GetParentContainer("Message Routing", "Debug"));
        }

        private bool MonitorWindowFactory(IWindowViewContainer viewContainer, XDocument state)
        {
            viewContainer.Title = string.Format("Routing Monitor ({0}, {1})", Assembly.GetExecutingAssembly().FullName, Process.GetCurrentProcess().Id);
            viewContainer.Content = new MessageRoutingMonitorContent(this.routingCommunicator, this.scout);
            return true;
        } 
        
    }
}
