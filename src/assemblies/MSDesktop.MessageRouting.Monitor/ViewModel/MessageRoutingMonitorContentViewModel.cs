﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using MSDesktop.MessageRouting.Monitor.Model;
using MSDesktop.MessageRouting.Routing;
using MSDesktop.MessageRouting.ZeroConfig;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel; 

namespace MSDesktop.MessageRouting.Monitor.ViewModel
{
    public class MessageRoutingMonitorContentViewModel : ViewModelBase
    {
        private readonly IRoutingCommunicator _routingCommunicator;
        private readonly IRouteExplorer _routeExplorer;
        private readonly Dispatcher _dispatcher;
        private bool _isRunning;

        public MessageRoutingMonitorContentViewModel(IRoutingCommunicator routingCommunicator,
                                                     IRouteExplorer routeExplorer, Dispatcher dispatcher)
        {
            _routingCommunicator = routingCommunicator;
            _routeExplorer = routeExplorer;
            _dispatcher = dispatcher;

            PauseCommand = new DelegateCommand(Pause);
            StartCommand = new DelegateCommand(Start);
            ClearCommand = new DelegateCommand(Clear);

            Messages = new ObservableCollection<RoutedCommunicationMonitorModel>();
            IsRunning = true;
            _routingCommunicator.MessagePublished += MessagePublished;
            ShowInternalMessage = RoutedMessageDispatcher.TrackInternalMessageEvents;
        }

        public ObservableCollection<RoutedCommunicationMonitorModel> Messages { get; set; }

        public bool IsRunning
        {
            get { return _isRunning; }
            set
            {
                _isRunning = value;
                OnPropertyChanged("IsRunning");
                OnPropertyChanged("IsNotRunning");
            }
        }

        public bool IsNotRunning
        {
            get { return !IsRunning; }
        }

        public bool ShowInternalMessage { get; set; }

        private void MessagePublished(object sender, MessagePublishedEventArgs e)
        {
            if (IsRunning)
            {
                _dispatcher.Invoke(new Action(() => 
                    Messages.Add(new RoutedCommunicationMonitorModel
                    {
                        MessageType = e.Message.GetType().FullName,
                        DeliveryInfo = e.DeliveryInfo,
                        IsInternal = e.Message.GetType().GetCustomAttributes(typeof(InternalMessageAttribute), true).Length > 0
                        
                    })));

            }
        }

        private void Pause(object parameter)
        {
            IsRunning = false;
        }

        private void Start(object parameter)
        {
            IsRunning = true;
        }

        private void Clear(object parameter)
        {
            Messages.Clear();
        }

        #region Commands

        public ICommand PauseCommand { get; private set; }

        public ICommand StartCommand { get; private set; }

        public ICommand ClearCommand { get; private set; }

        #endregion 
    }
}
