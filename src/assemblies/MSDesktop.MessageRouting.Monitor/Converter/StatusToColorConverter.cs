﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using MSDesktop.MessageRouting.Routing;
using System.Windows.Media;

namespace MSDesktop.MessageRouting.Monitor.Converter
{
    public class StatusToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            DeliveryStatus? status = value as DeliveryStatus?;
            if (status.HasValue)
            {
                switch (status.Value)
                {
                    case DeliveryStatus.Pending:
                        return new SolidColorBrush(Colors.Gainsboro);
                    case DeliveryStatus.WaitingForAck:
                        return new SolidColorBrush(Colors.LightYellow);
                    case DeliveryStatus.Delivered:
                        return new SolidColorBrush(Colors.GreenYellow);
                    case DeliveryStatus.Failed:
                        return new SolidColorBrush(Colors.OrangeRed);
                    default:
                        break;
                }
            }
            
            return new SolidColorBrush(Colors.White);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
