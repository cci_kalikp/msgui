﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MSDesktop.MessageRouting.ZeroConfig;
using MorganStanley.MSDotNet.MSGui.Impl;
using MSDesktop.MessageRouting.Monitor.Modules;

namespace MorganStanley.MSDotNet.MSGui.Impl.Extensions
{
    public static class FrameworkExtensions
    {
        public static void EnableMessageRoutingMonitor(this Framework framework)
        {
            framework.AddModule<MessageRoutingMonitorModule>();
        }
    }
}