///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2002 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
///////////////////////////////////////////////////////////////////////////////
//
//       Filename:  $RCSfile: HashUtils.cs,v $ 
//       Revision:  $Revision: #3 $ 
// Last edited by:  $Author: boqwang $ 
//             on:  $Date: 2013/02/28 $ 
//
// $Id: //eai/msdotnet/msgui/trunk/assemblies/Rambo/desktop/Utils/HashUtils.cs#3 $ 
//
///////////////////////////////////////////////////////////////////////////////
//
// $Log: HashUtils.cs,v $
// Revision 1.2  2003/07/30 09:06:18  perrys
// doco/namespace/tidy up
//
//
///////////////////////////////////////////////////////////////////////////////

using System;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils
{
  /// <summary>
  /// Good initial capacity numbers for hashtables.
  /// </summary>
  public class HashUtils
  {
    /// <summary>
    /// &lt;~10 elements
    /// </summary>
    public static readonly int SMALL_HASH_SIZE    = 11 ;

    /// <summary>
    /// &lt;~100 elements
    /// </summary>
    public static readonly int MEDIUM_HASH_SIZE   = 89 ;

    /// <summary>
    /// &lt;~1000 elements
    /// </summary>
    public static readonly int LARGE_HASH_SIZE    = 719 ;

    /// <summary>
    /// ~2500 elements
    /// </summary>
    public static readonly int X_LARGE_HASH_SIZE  = 2879 ;

    /// <summary>
    /// ~10000 elements
    /// </summary>
    public static readonly int XX_LARGE_HASH_SIZE = 11519 ;
    
    private HashUtils()
    {
    }
  }
}
