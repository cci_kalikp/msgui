using System;
using System.ComponentModel;

using MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.ComponentModel
{
  public partial class BindingTree
  {
    protected class NodeValuePropertyDescriptor : PropertyDescriptor
    {
      private readonly PropertyDescriptor m_baseDescriptor;

      public NodeValuePropertyDescriptor(PropertyDescriptor descriptor_) 
        : base(descriptor_)
      {
        m_baseDescriptor = descriptor_;
      }

      public override bool CanResetValue(object component)
      {
        ITreeNode node = (ITreeNode) component;
        return m_baseDescriptor.CanResetValue(node.Value);
      }

      public override Type ComponentType
      {
        get { return typeof(ITreeNode); }
      }

      public override object GetValue(object component)
      {
        ITreeNode node = (ITreeNode)component;
        return m_baseDescriptor.GetValue(node.Value);
      }

      public override bool IsReadOnly
      {
        get { return m_baseDescriptor.IsReadOnly; }
      }

      public override Type PropertyType
      {
        get { return m_baseDescriptor.PropertyType; }
      }

      public override void ResetValue(object component)
      {
        ITreeNode node = (ITreeNode)component;
        m_baseDescriptor.ResetValue(node.Value);
      }

      public override void SetValue(object component, object value)
      {
        ITreeNode node = (ITreeNode)component;
        m_baseDescriptor.SetValue(node.Value, value);
      }

      public override bool ShouldSerializeValue(object component)
      {
        ITreeNode node = (ITreeNode)component;
        return m_baseDescriptor.ShouldSerializeValue(node.Value);
      }
    }
  }
}