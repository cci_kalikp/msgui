using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections;
using MorganStanley.MSDotNet.MSNet;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.ComponentModel
{
  public partial class BindingTree : 
    IListSource, 
    ITree
  {
    #region Data
    protected readonly IMSNetLoop m_UILoop;
    protected readonly IBindingTreeRootNodeList m_rootNodeList;
    protected readonly Type[] m_typeHierarchy;
    protected readonly object m_syncRoot;
    protected readonly Type[] m_nodeTypes;
    protected readonly Type[] m_listTypes;
    #endregion

    #region Constructor
    /// <summary>
    /// Initializes a new instance of the <see cref="BindingTree"/> class with the specified UI Loop and type hierarchy.  
    /// </summary>
    /// <param name="UILoop_">The UI loop, ensuring that all <see cref="IBindingList.ListChanged"/> events are are created on the on the thread the UI control is bound to.</param>
    /// <param name="typeHierarchy_">Definition of type values for what types of values will be stored at each level of the tree, in hierarchical order.</param>
    /// <remarks>
    /// NOTE: if the levels of the tree extend beyond the type heirarchy length, the last type specified in the hierarchy will be used.  So, if a uniformly typed tree is desired, only one type need be specified.
    /// </remarks>
    public BindingTree(IMSNetLoop UILoop_, params Type[] typeHierarchy_)
    {
      if(UILoop_ == null)
      {
        throw new ArgumentNullException("UILoop_", "UI loop may not be null.");
      }

      if(typeHierarchy_ == null || typeHierarchy_.Length == 0)
      {
        throw new ArgumentNullException("typeHierarchy_", "Type hierarchy may not be null or empty.");
      }

      m_UILoop = UILoop_;
      m_typeHierarchy = typeHierarchy_;
      m_syncRoot = new object();

      m_listTypes = new Type[m_typeHierarchy.Length];
      m_nodeTypes = new Type[m_typeHierarchy.Length];

      for (int i = 0; i < m_typeHierarchy.Length; i++)
      {
        Type valueType = m_typeHierarchy[i];

        Type bindingListType = typeof(NodeList<>);
        m_listTypes[i] = bindingListType.MakeGenericType(valueType);

        Type bindingNodeType = typeof(Node<>);
        m_nodeTypes[i] = bindingNodeType.MakeGenericType(valueType);
      }

      m_rootNodeList = CreateRootNodeList();;

      ResetPropertyDescriptors( );
    }
    #endregion

    #region Public Methods

    /// <summary>
    /// Resets the property descriptors.
    /// </summary>
    public void ResetPropertyDescriptors()
    {
      m_rootNodeList.ResetPropertyDescriptors();
    }

    public ITreeNode CreateNode(int level_)
    {
      if (level_ < 0)
      {
        throw new ArgumentOutOfRangeException("level_", "Level can not be a negative number.");
      }

      int n = level_ >= m_nodeTypes.Length ? m_nodeTypes.Length - 1 : level_;

      Type nodeType = m_nodeTypes[n];
      IBindingTreeNodeList treeNodeList = CreateNodeList(level_ + 1);
      return (ITreeNode)Activator.CreateInstance(nodeType, this, treeNodeList);
    }

    #endregion

    #region ITree Members
    /// <summary>
    /// Occurs when the structure of tree has changed.
    /// </summary>
    public event EventHandler<TreeChangedEventArgs> TreeChanged;

    /// <summary>
    /// Gets the total number of nodes in the tree.
    /// </summary>
    /// <value>The node count.</value>
    public int Nodes
    {
      get { return m_rootNodeList.Descendants; }
    }

    /// <summary>
    /// Gets the total number of levels in the tree.
    /// </summary>
    /// <value>The level count.</value>
    public int Levels
    {
      get
      {
        return m_rootNodeList.Count > 0 ? m_rootNodeList.Generations : 0;
      }
    }

    /// <summary>
    /// Gets the list of nodes at the root level (level 0).
    /// </summary>
    /// <value>The root node list.</value>
    public ITreeNodeList RootNodeList
    {
      get { return m_rootNodeList; }
    }
    #endregion


    #region ITreeEnumerator Members
    /// <summary>
    /// Gets an enumerable collection for iterating over the values of a tree depth first.
    /// </summary>
    /// <value>The depth first value enumerator.</value>
    public IEnumerable DepthFirstEnumerator
    {
      get
      {
        foreach (ITreeNode node in this.DepthFirstNodeEnumerator)
        {
          yield return node.Value;
        }
      }
    }

    /// <summary>
    /// Gets an enumerable collection for iterating over the values of a tree breadth first.
    /// </summary>
    /// <value>The breadth first value enumerator.</value>
    public IEnumerable BreadthFirstEnumerator
    {
      get
      {
        foreach (ITreeNode node in this.BreadthFirstNodeEnumerator)
        {
          yield return node.Value;
        }
      }
    }

    /// <summary>
    /// Gets an enumerable collection for iterating over the nodes of a tree depth first.
    /// </summary>
    /// <value>The depth first node enumerator.</value>
    public IEnumerable<ITreeNode> DepthFirstNodeEnumerator
    {
      get
      {
        foreach (ITreeNode rootNode in m_rootNodeList)
        {
          foreach (ITreeNode node in rootNode.DepthFirstNodeEnumerator)
          {
            yield return node;
          }
        }
      }
    }

    /// <summary>
    /// Gets an enumerable collection for iterating over the nodes of a tree breadth first.
    /// </summary>
    /// <value>The breadth first node enumerator.</value>
    public IEnumerable<ITreeNode> BreadthFirstNodeEnumerator
    {
      get
      {
        Queue<ITreeNode> todo = new Queue<ITreeNode>();

        foreach (ITreeNode rootNode in m_rootNodeList)
        {
          todo.Enqueue(rootNode);
        }

        while (0 < todo.Count)
        {
          ITreeNode node = todo.Dequeue();

          foreach (ITreeNode child in node.ChildNodes)
          {
            todo.Enqueue(child);
          }

          yield return node;
        }
      }
    }
    #endregion

    #region ITreeNodeSearch Members
    /// <summary>
    /// Finds the node found withing the specified enumeration that matches the specified predicate.
    /// </summary>
    /// <param name="iterator">The enumeration to search.</param>
    /// <param name="comparer">The search condition.</param>
    /// <returns>The tree node matching the provided criteria; otherwise, null</returns>
    public ITreeNode Find(IEnumerable<ITreeNode> iterator, Predicate<ITreeNode> comparer)
    {
      foreach (ITreeNode node in iterator)
      {
        if (comparer(node))
        {
          return node;
        }
      }
      return null;
    }
    #endregion

    #region IListSource Members
    ///<summary>
    ///Returns an <see cref="T:System.Collections.IList"></see> that can be bound to a data source from an object that does not implement an <see cref="T:System.Collections.IList"></see> itself.
    ///</summary>
    ///
    ///<returns>
    ///An <see cref="T:System.Collections.IList"></see> that can be bound to a data source from the object.
    ///</returns>
    ///
    IList IListSource.GetList()
    {
      return m_rootNodeList;
    }

    ///<summary>
    ///Gets a value indicating whether the collection is a collection of <see cref="T:System.Collections.IList"></see> objects.
    ///</summary>
    ///
    ///<returns>
    ///true if the collection is a collection of <see cref="T:System.Collections.IList"></see> objects; otherwise, false.
    ///</returns>
    ///
    bool IListSource.ContainsListCollection
    {
      get { return true; }
    }
    #endregion

    #region Protected Methods
    protected Type GetTypeAtLevel(int level_)
    {
      int n = GetHierarchyLevel(level_);
      return m_typeHierarchy[n];
    }

    protected int GetHierarchyLevel(int level_)
    {
      if (level_ < 0)
      {
        throw new ArgumentOutOfRangeException("level_", "Level can not be a negative number.");
      }

      return level_ >= m_typeHierarchy.Length ? m_typeHierarchy.Length - 1 : level_;
    }

    protected Type GetGenericNodeType(Type valueType_)
    {
      Type bindingNodeType = typeof(Node<>);
      return bindingNodeType.MakeGenericType(valueType_);
    }

    protected Type GetGenericListType(Type valueType_)
    {
      Type bindinglistType = typeof(NodeList<>);
      return bindinglistType.MakeGenericType(bindinglistType);
    }

    protected IBindingTreeNodeList CreateNodeList(int level_)
    {
      if (level_ < 0)
      {
        throw new ArgumentOutOfRangeException("level_", "Level can not be a negative number.");
      }

      int n = level_ >= m_listTypes.Length ? m_listTypes.Length - 1 : level_;

      Type listType = m_listTypes[n];
      return (IBindingTreeNodeList)Activator.CreateInstance(listType, this);
    }

    protected IBindingTreeRootNodeList CreateRootNodeList()
    {
      Type genericType = m_typeHierarchy[0];
      Type bindingListType = typeof(RootList<>);
      Type listType = bindingListType.MakeGenericType(genericType);
      return (IBindingTreeRootNodeList)Activator.CreateInstance(listType, this);
    }

    protected void OnTreeChanged(TreeChangedEventArgs e)
    {
      if(TreeChanged != null)
      {
        TreeChanged(this, e);
      }
    }
    #endregion

   
  }
}