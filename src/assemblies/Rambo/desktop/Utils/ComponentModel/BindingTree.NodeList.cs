using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;

using MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.ComponentModel
{
  public partial class BindingTree
  {
    protected interface IBindingTreeNodeList : ITreeNodeList
    {
      void SetOwner(ITreeNode owner_);

      IList<int> Genealogy { get; }
      void UpdateGenealogy(int descendants_, int depth_, ITreeNode ancestor_);
    }

    protected class NodeList<T> :
      BindingList<Node<T>>,
      IBindingTreeNodeList
    {
      #region Data
      protected BindingTree m_tree;
      protected ITreeNode m_owner;

      /// <summary>
      /// Keeps track of the number of descendants in each generation, the [0] index is this generation, so m_genealogy[0] == this.Count.  This structure is used to describe the correct number of levels to the bound control.
      /// </summary>
      protected List<int> m_genealogy;
      #endregion

      #region Constructors
      public NodeList(BindingTree tree_)
      {
        m_tree = tree_;
        m_genealogy = new List<int>();
      }

      #endregion

      #region ITreeNodeList Members

      public ITreeNode Owner
      {
        get { return m_owner; }
      }

      public int Depth
      {
        get { return m_owner == null ? 0 : m_owner.Depth + 1; }
      }

      public int Generations
      {
        get { return m_genealogy.Count; }
      }

      public int Descendants
      {
        get
        {
          int sum = 0;
          foreach (int i in m_genealogy)
          {
            sum += i;
          }

          return sum;
        }
      }
      #endregion

      #region ICollection Members
      ///<summary>
      ///Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1"></see> is read-only.
      ///</summary>
      ///
      ///<returns>
      ///true if the <see cref="T:System.Collections.Generic.ICollection`1"></see> is read-only; otherwise, false.
      ///</returns>
      ///
      public virtual bool IsReadOnly
      {
        get { return false; }
      }
      #endregion

      #region ToString
      /// <summary>
      /// Returns a <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
      /// </summary>
      /// <returns>
      /// A <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
      /// </returns>
      public override string ToString()
      {
        string owner = m_owner == null ? "null" : m_owner.ToString();
        return string.Format("Count={0}, Depth={1}, Owner={2}", Count, Depth, owner);
      }
      #endregion

      #region BindingList Members

      /// <summary>
      /// Adds a new item to the end of the collection.
      /// </summary>
      /// <returns>
      /// The item that was added to the collection.
      /// </returns>
      /// <exception cref="T:System.InvalidCastException">The new item is not the same type as the objects contained in the <see cref="T:System.ComponentModel.BindingList`1"></see>.</exception>
      protected override object AddNewCore()
      {
#if TEST
        Console.WriteLine(string.Format("AddNew() {0}", this));
#endif
        return null;
      }

      private delegate void OnListChangedDelegate(ListChangedEventArgs e);

      /// <summary>
      /// Raises the <see cref="E:System.ComponentModel.BindingList`1.ListChanged"></see> event.
      /// </summary>
      /// <param name="e">A <see cref="T:System.ComponentModel.ListChangedEventArgs"></see> that contains the event data.</param>
      protected override void OnListChanged(ListChangedEventArgs e)
      {
        if(m_tree.m_UILoop.InvokeRequired)
        {
          m_tree.m_UILoop.InvokeLater(new OnListChangedDelegate(OnListChanged), e);
        }
        else
        {
#if TEST
          Console.WriteLine(string.Format("ListChanged() {0},{1},{2},{3},{4}", this, e.ListChangedType, e.NewIndex, e.OldIndex, e.PropertyDescriptor));
#endif
          base.OnListChanged(e);
        }
       
      }

      /// <summary>
      /// Removes all elements from the collection.
      /// </summary>
      protected override void ClearItems()
      {
        lock (m_tree.m_syncRoot)
        {
          for (int index = Count - 1; index > -1; --index)
          {
            RemoveNode(index);
          }

          m_tree.OnTreeChanged(new TreeChangedEventArgs(TreeChangedType.NodeListCleared, this));
        }
      }

      /// <summary>
      /// Inserts the specified item in the list at the specified index.
      /// </summary>
      /// <param name="index">The zero-based index where the item is to be inserted.</param>
      /// <param name="item">The item to insert in the list.</param>
      protected override void InsertItem(int index, Node<T> item)
      {
        lock (m_tree.m_syncRoot)
        {
          InsertNode(index, item);
          m_tree.OnTreeChanged(new TreeChangedEventArgs(TreeChangedType.NodeAdded, item, this, index));
        }
      }


      /// <summary>
      /// Removes the item at the specified index.
      /// </summary>
      /// <param name="index">The zero-based index of the item to remove.</param>
      /// <exception cref="T:System.NotSupportedException">You are removing a newly added item and <see cref="P:System.ComponentModel.IBindingList.AllowRemove"></see> is set to false. </exception>
      protected override void RemoveItem(int index)
      {
        lock (m_tree.m_syncRoot)
        {
          ITreeNode node = this[index];
          RemoveNode(index);
          m_tree.OnTreeChanged(new TreeChangedEventArgs(TreeChangedType.NodeRemoved, node, this, index));
        }
      }


      protected override void SetItem(int index, Node<T> item)
      {
        lock (m_tree.m_syncRoot)
        {
          RemoveNode(index);
          InsertNode(index, item);

          m_tree.OnTreeChanged(new TreeChangedEventArgs(TreeChangedType.NodeChanged, item, this, index));
        }
      }
      #endregion

      #region Protected Methods

      private void InsertNode(int index, Node<T> node)
      {
        lock (m_tree.m_syncRoot)
        {

          if (node == null)
          {
            throw new ArgumentException("The item to insert must be a Node type", "item");
          }

          node.OnInsertion(index, this);

          base.InsertItem(index, node);

          this.AddDescendant(node);
        }
      }

      private void RemoveNode(int index)
      {
        lock (m_tree.m_syncRoot)
        {
          Node<T> oldItem = this[index];
          base.RemoveItem(index);

          this.RemoveDescendant(oldItem);
          oldItem.OnRemoval(index, this);
        }
      }

     /// <summary>
     /// Updates the tree's genealogy by adding the specified node and its descendants.
     /// </summary>
     /// <remarks>
     /// The order in which this happens is extremely important, otherwise the bound control
     /// may become out of sync with the tree's structure.
     /// </remarks>
      protected void AddDescendant(ITreeNode descendant_)
      {
        IBindingTreeNodeList childNodeList = (IBindingTreeNodeList) descendant_.ChildNodes;
        List<int> descendantGenealogy = new List<int>(childNodeList.Genealogy);
        descendantGenealogy.Insert(0, 1);

        // go up the ancestry
        foreach (ITreeNode ancestor in descendant_.AncestorNodeEnumerator)
        {
          int offset = this.Depth - ancestor.Depth;

          // add the descendant hierarcy to this level in the ancestry one generation at a time 
          // from the level closest to the root to the furthest.
          for (int i = 0; i < descendantGenealogy.Count; i++)
          {
            IBindingTreeNodeList ancestorGenealogy = (IBindingTreeNodeList)ancestor.ParentList;
            ancestorGenealogy.UpdateGenealogy(descendantGenealogy[i], offset + i, ancestor);
          }
        }
      }

      /// <summary>
      /// Updates the tree's genealogy by removing the specified node and its descendants.
      /// </summary>
      /// <remarks>
      /// The order in which this happens is extremely important, otherwise the bound control
      /// may become out of sync with the tree's structure.
      /// </remarks>
      protected void RemoveDescendant(ITreeNode descendant_)
      {
        IBindingTreeNodeList childGenealogy = (IBindingTreeNodeList)descendant_.ChildNodes;
        List<int> descendantGenealogy = new List<int>(childGenealogy.Genealogy);
        descendantGenealogy.Insert(0, 1);

        // go up the ancestry chain
        foreach (ITreeNode ancestor in descendant_.AncestorNodeEnumerator)
        {
          int offset = this.Depth - ancestor.Depth;

          // This must be done in reverse order.  That is, removal of the descendants must occur
          // one generation at a time, in order from the deepest level up.
          for (int i = descendantGenealogy.Count - 1; i >= 0; i--)
          {
            IBindingTreeNodeList ancestorGenealogy = (IBindingTreeNodeList)ancestor.ParentList;
            ancestorGenealogy.UpdateGenealogy(-(descendantGenealogy[i]), offset + i, ancestor);
          }
        }
      }

      #endregion

      #region IBindingTreeNodeList Members
      void IBindingTreeNodeList.SetOwner(ITreeNode owner_)
      {
        m_owner = owner_;
      }

      IList<int> IBindingTreeNodeList.Genealogy
      {
        get { return m_genealogy; }
      }

      void IBindingTreeNodeList.UpdateGenealogy(int descendants_, int depth_, ITreeNode ancestor_)
      {
        bool genealogyChanged = false;

        if (Generations == depth_ && descendants_ > 0)
        {
          m_genealogy.Insert(depth_, 0);
          genealogyChanged = true;
        }

        m_genealogy[depth_] += descendants_;

        Debug.Assert(m_genealogy[depth_] >= 0);

        if (m_genealogy[depth_] == 0)
        {
          m_genealogy.RemoveAt(depth_);
          genealogyChanged = true;
        }

        if (genealogyChanged)
        {
          // Signal list changed events when the number of generations has changed.  This must be done
          // one generation at a time - bottom up for removals and top down for additions.
          OnListChanged(
            new ListChangedEventArgs(ListChangedType.PropertyDescriptorChanged, ancestor_.Index));
        }
      }
      #endregion
    }
  }
}