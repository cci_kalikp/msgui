using System;
using System.Collections.Generic;
using System.ComponentModel;

using MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.ComponentModel
{
  public partial class BindingTree
  {
    protected interface IBindingTreeRootNodeList : IBindingTreeNodeList
    {
      void ResetPropertyDescriptors();
    }

    protected class RootList<T> :
      NodeList<T>,
      IBindingTreeRootNodeList,
      ITypedList
    {
      protected readonly Dictionary<Type, PropertyDescriptorCollection> m_nodeSpecMap;
      protected readonly Dictionary<Type, PropertyDescriptorCollection> m_leafSpecMap;


      public RootList(BindingTree tree_) : base(tree_)
      {
        m_nodeSpecMap = new Dictionary<Type, PropertyDescriptorCollection>(m_tree.m_typeHierarchy.Length);
        m_leafSpecMap = new Dictionary<Type, PropertyDescriptorCollection>(m_tree.m_typeHierarchy.Length);

        ResetPropertyDescriptors( );

      }

      public void ResetPropertyDescriptors()
      {
        m_nodeSpecMap.Clear( );
        m_leafSpecMap.Clear( );

        Collections.ISet<Type> hierarchyTypes = new Set<Type>(m_tree.m_typeHierarchy);

        foreach (Type type in hierarchyTypes)
        {
          Type genericTreeNodeType = m_tree.GetGenericNodeType(type);
          PropertyDescriptorCollection nodeDescriptors =
            TypeDescriptor.GetProperties(genericTreeNodeType, new Attribute[] {new BindableAttribute(true)});
          List<PropertyDescriptor> allDescriptors = new List<PropertyDescriptor>( );

          foreach (PropertyDescriptor pd in nodeDescriptors)
          {
            if (pd.Attributes.Contains(new BindableTreeValueAttribute( )))
            {
              allDescriptors.AddRange(GetValueDescriptors(pd));
            }
            else if (pd.PropertyType == typeof(IBindingList))
            {
              allDescriptors.Insert(0, pd);
            }
            else
            {
              allDescriptors.Add(pd);
            }
          }

          PropertyDescriptor[] nodeSpecArray = allDescriptors.ToArray( );
          PropertyDescriptor[] leafSpecArray = new PropertyDescriptor[nodeSpecArray.Length - 1];
          Array.ConstrainedCopy(nodeSpecArray, 1, leafSpecArray, 0, leafSpecArray.Length);

          m_nodeSpecMap.Add(type, new PropertyDescriptorCollection(nodeSpecArray));
          m_leafSpecMap.Add(type, new PropertyDescriptorCollection(leafSpecArray));
        }

        this.OnListChanged(new ListChangedEventArgs(ListChangedType.PropertyDescriptorChanged, -1, -1));
      }
      
      /// <summary>
      /// Get the property descriptors for the Bindable properties of the type stored in the 
      /// Value of the BindingTreeNode
      /// </summary>
      private IEnumerable<PropertyDescriptor> GetValueDescriptors(PropertyDescriptor baseDescriptor_)
      {
        PropertyDescriptorCollection bindableProperties =
          TypeDescriptor.GetProperties(baseDescriptor_.PropertyType,
                                       new Attribute[]
                                         {
                                           new BindableAttribute(true)
                                         });

        List<PropertyDescriptor> properties = new List<PropertyDescriptor>( );

        if (bindableProperties.Count == 0)
        {
          properties.Add(baseDescriptor_);
        }
        else
        {
          foreach (
            PropertyDescriptor pd in
              TypeDescriptor.GetProperties(baseDescriptor_.PropertyType,
                                           new Attribute[] {new BindableAttribute(true)}))
          {
            properties.Add(new NodeValuePropertyDescriptor(pd));
          }
        }

        return properties;
      }

      #region ITypedList Members

      /// <summary>
      /// Returns the <see cref="T:System.ComponentModel.PropertyDescriptorCollection"></see> that represents the properties on each item used to bind data.
      /// </summary>
      /// <param name="listAccessors">An array of <see cref="T:System.ComponentModel.PropertyDescriptor"></see> objects to find in the collection as bindable. This can be null.</param>
      /// <returns>
      /// The <see cref="T:System.ComponentModel.PropertyDescriptorCollection"></see> that represents the properties on each item used to bind data.
      /// </returns>
      PropertyDescriptorCollection ITypedList.GetItemProperties(PropertyDescriptor[] listAccessors)
      {
        int level = listAccessors == null ? 0 : listAccessors.Length;

#if TEST
        Console.WriteLine(string.Format("GetItemProperties() Accessors:{0},List:{1}",
          listAccessors == null ? 0 : listAccessors.Length, this));
#endif

        if ((listAccessors == null && Generations > 1)
          || (listAccessors != null && listAccessors.Length < (Generations - 1)))
        {


#if TEST
          Console.WriteLine(" Returned node spec");
#endif
          return GetNodeSpec(level);
        }
        else if ((listAccessors == null && Generations == 1)
          || (listAccessors != null && listAccessors.Length == (Generations - 1)))
        {
#if TEST
          Console.WriteLine(" Returned leaf spec");
#endif
          return GetLeafSpec(level);
        }

#if TEST
        Console.WriteLine(" Returned empty spec");
#endif
        return PropertyDescriptorCollection.Empty;
      }


      /// <summary>
      /// Returns the name of the list.
      /// </summary>
      /// <param name="listAccessors">An array of <see cref="T:System.ComponentModel.PropertyDescriptor"></see> objects, for which the list name is returned. This can be null.</param>
      /// <returns>The name of the list.</returns>
      string ITypedList.GetListName(PropertyDescriptor[] listAccessors)
      {
        return GetType( ).ToString( );
      }

      #endregion

      private PropertyDescriptorCollection GetNodeSpec(int level_)
      {
        Type nodeType = m_tree.GetTypeAtLevel(level_);
        return m_nodeSpecMap[nodeType];
      }

      private PropertyDescriptorCollection GetLeafSpec(int level_)
      {
        Type nodeType = m_tree.GetTypeAtLevel(level_);
        return m_leafSpecMap[nodeType];
      }

    }
  }
}