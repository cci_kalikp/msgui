using System;
using System.ComponentModel;

using MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.ComponentModel
{
  public partial class BindingTree
  {
    /// <summary>
    /// Used to flag the Value property of the binding tree node.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    protected class BindableTreeValueAttribute : Attribute
    {

    }

    protected class Node<T> : 
      TreeNode,
      IDataErrorInfo,
      IEditableObject,
      INotifyPropertyChanged
    {
      #region Data
      protected T m_value;
      protected BindingTree m_tree;
      protected PropertyChangedEventHandler m_propertyChanged;
      #endregion

      #region Constructor
      public Node(BindingTree tree_, IBindingTreeNodeList childNodes_) 
        : base(childNodes_)
      {
        m_tree = tree_;
        childNodes_.SetOwner(this);
      }
      #endregion


      [Bindable(true), BindableTreeValue]
      public T Value
      {
        get
        {
          return (T) GetValue( );
        }
        set
        {
          SetValue(value);
        }
      }

#if TEST
      [Bindable(true)]
      public override int Depth
      {
        get
        {
          return base.Depth;
        }
      }
#endif

#if TEST
      [Bindable(true)]
      public override int Index
      {
        get { return base.Index; }
      }
#endif


#if TEST
      [Bindable(true)]
#endif
      public override int Descendants
      {
        get { return m_childNodes.Descendants; }
      }

#if TEST
      [Bindable(true)]
#endif
      public override int Generations
      {
        get { return m_childNodes.Generations; }
      }


      [Bindable(true)]
      public IBindingList BindingList
      {
        get
        {
          return (IBindingList)m_childNodes;
        }
      }

      public void OnInsertion(int index_, NodeList<T> list_)
      {

        if (m_parentList != null)
        {
          throw new InvalidOperationException("Attempt to insert an existing child node into another child list.  ITreeNodes may only have one parent.");
        }

        m_parentNode = list_.Owner;
        m_parentList = list_;
        m_index = index_;

        for (int i = index_; i < m_parentList.Count; ++i)
        {
          Node<T> node = list_[i];
          node.m_index++;
        }

        CalculateDepths();
      }

      public void OnRemoval(int index_, NodeList<T> list_)
      {
        for (int i = index_; i < m_parentList.Count; ++i)
        {
          Node<T> node = (Node<T>)list_[i];
          node.m_index--;
        }

        m_parentNode = null;
        m_parentList = null;
        m_index = -1;

        CalculateDepths();
      }

      protected void CalculateDepths()
      {
        m_rootNode = m_parentNode == null ? this : m_parentNode.RootNode;

        foreach (TreeNode node in BreadthFirstNodeEnumerator)
        {
          node.SetDepth(node.ParentNode == null ? 0 : node.ParentNode.Depth + 1);
          node.SetRoot(m_rootNode);
        }
      }

      protected override object GetValue( )
      {
        return m_value;
      }

      protected override void SetValue(object value)
      {
        INotifyPropertyChanged propertyChangedBefore = m_value as INotifyPropertyChanged;
        if(propertyChangedBefore != null)
        {
          propertyChangedBefore.PropertyChanged -= HandlePropertyChanged;
        }

        m_value = (T) value;

        INotifyPropertyChanged propertyChangedAfter = m_value as INotifyPropertyChanged;
        if (propertyChangedAfter != null)
        {
          propertyChangedAfter.PropertyChanged -= HandlePropertyChanged;
        }
      }

      #region IDataErrorInfo Members
      ///<summary>
      ///Gets the error message for the property with the given name.
      ///</summary>
      ///
      ///<returns>
      ///The error message for the property. The default is an empty string ("").
      ///</returns>
      ///
      ///<param name="columnName">The name of the property whose error message to get. </param>
      string IDataErrorInfo.this[string columnName]
      {
        get
        {
          IDataErrorInfo errorInfo = m_value as IDataErrorInfo;
          if(errorInfo != null)
          {
            return errorInfo[columnName];
          }

          return string.Empty;
        }
      }

      ///<summary>
      ///Gets an error message indicating what is wrong with this object.
      ///</summary>
      ///
      ///<returns>
      ///An error message indicating what is wrong with this object. The default is an empty string ("").
      ///</returns>
      ///
      string IDataErrorInfo.Error
      {
        get 
        {
          IDataErrorInfo errorInfo = m_value as IDataErrorInfo;
          if (errorInfo != null)
          {
            return errorInfo.Error;
          }

          return string.Empty; 
        }
      }
      #endregion

      #region IEditableObject Members
      ///<summary>
      ///Begins an edit on an object.
      ///</summary>
      ///
      void IEditableObject.BeginEdit( )
      {
        IEditableObject editable = m_value as IEditableObject;
        if(editable != null)
        {
          editable.BeginEdit( );
        }
      }

      ///<summary>
      ///Pushes changes since the last <see cref="M:System.ComponentModel.IEditableObject.BeginEdit"></see> or <see cref="M:System.ComponentModel.IBindingList.AddNew"></see> call into the underlying object.
      ///</summary>
      ///
      void IEditableObject.EndEdit( )
      {
        IEditableObject editable = m_value as IEditableObject;
        if (editable != null)
        {
          editable.EndEdit();
        }
      }

      ///<summary>
      ///Discards changes since the last <see cref="M:System.ComponentModel.IEditableObject.BeginEdit"></see> call.
      ///</summary>
      ///
      void IEditableObject.CancelEdit( )
      {
        IEditableObject editable = m_value as IEditableObject;
        if (editable != null)
        {
          editable.CancelEdit();
        }
      }
      #endregion

      #region INotifyPropertyChanged Members
      ///<summary>
      ///Occurs when a property value changes.
      ///</summary>
      ///
      event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged
      {
        add
        {
          m_propertyChanged += value;
        }
        remove
        {
          m_propertyChanged -= value;
        }
      }

      protected void HandlePropertyChanged(object sender, PropertyChangedEventArgs e)
      {
        OnPropertyChanged(e);
      }

      protected void OnPropertyChanged(PropertyChangedEventArgs e)
      {
        if(m_propertyChanged != null)
        {
          m_propertyChanged(this, e);
        }
      }
      #endregion
    }
  }
}