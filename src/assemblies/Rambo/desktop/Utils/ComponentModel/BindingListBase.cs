using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

using MorganStanley.MSDesktop.Rambo.AsyncUtils;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.ComponentModel
{
  public class BindingListBase<T> :
    BindingList<T>,
    IBindingList,
    IListSource
  {
    #region Fields
    private ListChangedEventHandler _listChanged;
    #endregion

    #region Constructors
    public BindingListBase( )
    {
    }

    public BindingListBase(IEnumerable<T> items_)
    {
      AddRange(items_);
    }
    #endregion

    #region Instance Methods
    public void AddRange(IEnumerable<T> items_)
    {
      foreach (T t in items_)
      {
        Add(t);
      }
    }

    protected override void OnListChanged(ListChangedEventArgs e)
    {
      ThreadingUtils.ThreadSafeInvoke(_listChanged, this, e);
    }
    #endregion

    #region Event Declarations
    public new event ListChangedEventHandler ListChanged
    {
      add { ( (IBindingList) this ).ListChanged += value; }
      remove { ( (IBindingList) this ).ListChanged -= value; }
    }
    #endregion

    #region IBindingList Members
    event ListChangedEventHandler IBindingList.ListChanged
    {
      add { _listChanged += value; }
      remove { _listChanged -= value; }
    }
    #endregion

    #region IListSource Members
    public virtual IList GetList( )
    {
      return this;
    }

    public virtual bool ContainsListCollection
    {
      get { return false; }
    }
    #endregion
  }
}