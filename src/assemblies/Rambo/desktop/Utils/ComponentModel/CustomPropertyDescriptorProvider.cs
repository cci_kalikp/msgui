using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.ComponentModel
{
  /// <summary>
  /// A Custom TypeDescriptionProvider for defining custom PropertyDescriptorCollections for a type.
  /// </summary>
  public class CustomPropertyDescriptorProvider :
    TypeDescriptionProvider
  {
    private static readonly Dictionary<Type, ICustomTypeDescriptor> m_typeDescriptorMap =
      new Dictionary<Type, ICustomTypeDescriptor>();

    private static readonly Dictionary<Type, PropertyDescriptorCollection> m_propertyDescriptorCollection =
      new Dictionary<Type, PropertyDescriptorCollection>();

    public override ICustomTypeDescriptor GetTypeDescriptor(Type objectType, object instance)
    {
      lock (typeof(CustomPropertyDescriptorProvider))
      {
        ICustomTypeDescriptor typeDescriptor;
        if (!m_typeDescriptorMap.TryGetValue(objectType, out typeDescriptor))
        {
          typeDescriptor = new CustomPropertyDescriptorTypeDescriptor(objectType);

          m_typeDescriptorMap.Add(objectType, typeDescriptor);
        }

        return typeDescriptor;
      }
    }

    /// <summary>
    /// Registers the spectified 
    /// </summary>
    /// <param name="objectType"></param>
    /// <param name="properties"></param>
    public static void SetProperties(Type objectType, PropertyDescriptorCollection properties)
    {
      lock (typeof(CustomPropertyDescriptorProvider))
      {
        m_propertyDescriptorCollection[objectType] = properties;
      }
    }

    private static PropertyDescriptorCollection GetProperties(Type objectType)
    {
      lock (typeof(CustomPropertyDescriptorProvider))
      {
        PropertyDescriptorCollection properties;
        if (m_propertyDescriptorCollection.TryGetValue(objectType, out properties))
        {
          return properties;
        }

        return PropertyDescriptorCollection.Empty;
      }
    }

    private class CustomPropertyDescriptorTypeDescriptor : CustomTypeDescriptor
    {
      private Type m_type;

      public CustomPropertyDescriptorTypeDescriptor(Type type)
      {
        m_type = type;
      }

      public override PropertyDescriptorCollection GetProperties()
      {
        return this.GetProperties(null);
      }

      public override PropertyDescriptorCollection GetProperties(Attribute[] attributes)
      {
        return CustomPropertyDescriptorProvider.GetProperties(m_type);
      }
    }
  }
}