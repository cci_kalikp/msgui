using System.Windows.Forms;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils
{
  /// <summary>
  /// Common Class with static methods/properties to help development on designer-based controls
  /// </summary>
  public sealed class DesignerUtils
  {
    #region Constructor

    private DesignerUtils() { }

    #endregion

    #region Properties

    private static readonly bool _isInDesignMode = (Application.ExecutablePath.ToLower().IndexOf("devenv.exe") != -1);
    /// <summary>
    /// Determines whether the current executing context is in component design mode.
    /// </summary>
    /// <value>
    /// 	<c>true</c> if the current execution context is of the designer; otherwise, <c>false</c>.
    /// </value>
    /// <remarks>
    /// Two other options to this, if this ever turns out unreliable :
    /// 1. GetService(typeof(IDesignerHost)) != null (this method needs to be contained within the form itself)
    /// 2. Application.ExecutablePath.ToLower().IndexOf("devenv.exe") != -1
    /// </remarks>
    public static bool IsInDesignMode
    {
      get
      {
        return _isInDesignMode;
      }
    }

    #endregion
  }
}
