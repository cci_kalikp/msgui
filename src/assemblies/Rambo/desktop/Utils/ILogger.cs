///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2004 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
///////////////////////////////////////////////////////////////////////////////
//
//       Filename:  $RCSfile: ILogger.cs,v $ 
//       Revision:  $Revision: #3 $ 
// Last edited by:  $Author: boqwang $ 
//             on:  $Date: 2013/02/28 $ 
//
// $Id: //eai/msdotnet/msgui/trunk/assemblies/Rambo/desktop/Utils/ILogger.cs#3 $ 
//
///////////////////////////////////////////////////////////////////////////////
//
// $Log: ILogger.cs,v $
// Revision 1.1  2004/02/27 10:57:16  perrys
// first cut
//
//
///////////////////////////////////////////////////////////////////////////////

using System;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils
{
  /// <summary>
  /// A logger is an abstraction designed to facilitate simple logging. See the 
  /// Logger implementation (based on MSLog) for more details.
  /// </summary>
  public interface ILogger
  {
    /// <summary>
    /// Log a debug level message
    /// </summary>
    void Debug(string method_, string msg_);

    /// <summary>
    /// Log an information message
    /// </summary>
    void Info(string method_, string msg_);

    /// <summary>
    /// Log a notice (significant) message
    /// </summary>
    void Notice(string method_, string msg_);
    
    /// <summary>
    /// Log a warning message
    /// </summary>
    void Warning(string method_, string msg_);
    
    /// <summary>
    /// Log a warning message with stacktrace
    /// </summary>
    void Warning(string method_, string msg_, Exception ex_);
    
    /// <summary>
    /// Log an error message
    /// </summary>
    void Error(string method_, string msg_);
    
    /// <summary>
    /// Log an error message with stacktrace
    /// </summary>
    void Error(string method_, string msg_, Exception ex_);

    /// <summary>
    /// Log a critical error message
    /// </summary>
    void Critical(string method_, string msg_);
    
    /// <summary>
    /// Log a critical error message with stacktrace
    /// </summary>
    void Critical(string method_, string msg_, Exception ex_);
  }
}