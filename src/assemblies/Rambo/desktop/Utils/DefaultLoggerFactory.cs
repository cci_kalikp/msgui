///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2004 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
///////////////////////////////////////////////////////////////////////////////
//
//       Filename:  $RCSfile: DefaultLoggerFactory.cs,v $ 
//       Revision:  $Revision: #3 $ 
// Last edited by:  $Author: boqwang $ 
//             on:  $Date: 2013/02/28 $ 
//
// $Id: //eai/msdotnet/msgui/trunk/assemblies/Rambo/desktop/Utils/DefaultLoggerFactory.cs#3 $ 
//
///////////////////////////////////////////////////////////////////////////////
//
// $Log: DefaultLoggerFactory.cs,v $
// Revision 1.2  2004/02/27 10:57:08  perrys
// allow use of default Logger sans MSLogLayer
//
//
///////////////////////////////////////////////////////////////////////////////

using System;
using MorganStanley.MSDotNet.MSLog ;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils
{
  /// <summary>
  /// Factory producing concrete (MSLog-based) logger objects.
  /// </summary>
  public class DefaultLoggerFactory : ILoggerFactory
  {
    private MSLogLayer _layer;

    /// <summary>
    /// Default ctr. Will produce Logger objects with layers obtained from 
    /// class information every time.
    /// </summary>
    public DefaultLoggerFactory()
    {
    }

    /// <summary>
    /// Ctr allowing one to specify the layer used for each logger produced.
    /// </summary>
    /// <param name="metaProj_">project meta</param>
    /// <param name="proj_">project name</param>
    /// <param name="release_">project release</param>
    public DefaultLoggerFactory( string metaProj_, string proj_, string release_)
    {
      _layer = new MSLogLayer( metaProj_, proj_, release_ ) ;
    }

    /// <summary>
    /// Ctr allowing one to specify the layer used for each logger produced.
    /// </summary>
    /// <param name="layer_">MSLog layer to be used for each product</param>
    public DefaultLoggerFactory(MSLogLayer layer_)
    {
      _layer = layer_;
    }
		
    public ILogger GetLogger( Type class_ )
    {
      Logger logger = _layer == null
        ? new Logger(class_) 
        : new Logger(class_, _layer);
      return ( logger ) ;
    }
  }
}
