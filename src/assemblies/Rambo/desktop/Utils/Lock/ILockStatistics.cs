﻿using System;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Lock
{
  public interface ILockStatistics
  {
    void RecordStart(string reason_, TimeSpan lockWaitTime_);
    void RecordEnd();
  }
}