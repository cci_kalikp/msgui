﻿using System;
using System.Threading;
using JetBrains.Annotations;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Lock 
{
  public class StatisticsLock : IDisposable
  {
    private readonly ILockStatistics _lockStatistics;
    private readonly object _syncRoot = new object();

    private int _reentrantCount;

    public StatisticsLock(string lockName_, ILockStatistics lockStatistics_ = null)
    {
      _lockStatistics = lockStatistics_ ?? new LockStatistics(lockName_);
    }

    public void Dispose()
    {
      if (_reentrantCount == 0) throw new InvalidOperationException("Invalid state, there was no usage reason provided");

      --_reentrantCount;

      if (_reentrantCount == 0)
      {
        _lockStatistics.RecordEnd();
      }

      Monitor.Exit(_syncRoot);
    }

    public void Lock([NotNull] string reason_)
    {
      if (reason_ == null) throw new ArgumentNullException("reason_");

      DateTime lockAcquireTime = DateTime.Now;

      Monitor.Enter(_syncRoot);

      RegisterStats(reason_, lockAcquireTime);
    }

    public bool TryLock(int timeout_, [NotNull] string reason_)
    {
      if (reason_ == null) throw new ArgumentNullException("reason_");

      DateTime lockAcquireTime = DateTime.Now;

      bool hasLock = Monitor.TryEnter(_syncRoot, timeout_);
      if (!hasLock) return false;

      RegisterStats(reason_, lockAcquireTime);

      return true;
    }

    private void RegisterStats(string reason_, DateTime lockAcquireTime_)
    {
      if (_reentrantCount == 0)
      {
        _lockStatistics.RecordStart(reason_, DateTime.Now - lockAcquireTime_);
      }

      ++_reentrantCount;
    }
  }
}
