﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Lock
{
  public class LockStatistics : ILockStatistics
  {
    private static readonly ILogger _log = new Logger(typeof(StatisticsLock));

    private readonly TimeSpan _forceLogThreshold = TimeSpan.FromSeconds(5);
    private readonly TimeSpan _logIntervalThreshold = TimeSpan.FromMinutes(5);

    private readonly Dictionary<string, TimeSpan> _waits = new Dictionary<string, TimeSpan>();
    private readonly Dictionary<string, TimeSpan> _usage = new Dictionary<string, TimeSpan>();

    private readonly string _usagePrefix;
    private readonly string _waitsPrefix;

    private TimeSpan _totalUsage;

    private string _currentReason;
    private DateTime _currentStartTime;
    private DateTime _lastLogTime;

    public LockStatistics(string lockName_)
    {
      _usagePrefix = lockName_ + " lock usage:";
      _waitsPrefix = lockName_ + " lock waits:";
    }

    public void RecordStart(string reason_, TimeSpan lockWaitTime_)
    {
      _currentReason = reason_;
      _currentStartTime = DateTime.Now;
      RegisterTimePerReason(_waits, lockWaitTime_);
    }

    public void RecordEnd()
    {
      TimeSpan lastUsage = DateTime.Now - _currentStartTime;
      RegisterUsage(lastUsage);
      bool forceLog = lastUsage > _forceLogThreshold;
      LogTopMetrics(forceLog);
      _currentReason = null;
    }

    private void LogTopMetrics(bool forceLog_)
    {
      if (!forceLog_ && DateTime.Now - _lastLogTime < _logIntervalThreshold)
      {
        return;
      }

      LogTopMetrics(_usage, _usagePrefix);
      LogTopMetrics(_waits, _waitsPrefix);

      _lastLogTime = DateTime.Now;
    }

    private static void LogTopMetrics(Dictionary<string, TimeSpan> metrics_, string metricType_)
    {
      string topMetricsLog = String.Join(Environment.NewLine, metrics_.OrderByDescending(p => p.Value).Take(5));
      _log.Info("LogTopMetrics", metricType_ + Environment.NewLine + topMetricsLog);
    }

    private void RegisterUsage(TimeSpan lastUsage_)
    {
      _totalUsage = _totalUsage + lastUsage_;
      RegisterTimePerReason(_usage, lastUsage_);
    }

    private void RegisterTimePerReason(Dictionary<string, TimeSpan> metricsPerReason_, TimeSpan lastRecord_)
    {
      TimeSpan totalMetrics;
      if (metricsPerReason_.TryGetValue(_currentReason, out totalMetrics))
      {
        totalMetrics = totalMetrics + lastRecord_;
      }
      else
      {
        totalMetrics = lastRecord_;
      }

      metricsPerReason_[_currentReason] = totalMetrics;
    }
  }
}