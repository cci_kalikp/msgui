﻿using System;
using System.Collections.Generic;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils
{
  public class Stopper<T>
  {
    private readonly List<Tuple<T, DateTime>> _events = new List<Tuple<T, DateTime>>();

    public Stopper()
    {
      _events.Add(new Tuple<T, DateTime>(default(T), DateTime.Now));
    }

    public void Split(T event_)
    {
      _events.Add(new Tuple<T, DateTime>(event_, DateTime.Now));
    }

    public IEnumerable<Tuple<T, TimeSpan>> GetResults()
    {
      var result = new List<Tuple<T, TimeSpan>>();

      for (int i = 1; i < _events.Count; ++i)
      {
        var prev = _events[i - 1];
        var curr = _events[i];

        TimeSpan elapsed = curr.Item2 - prev.Item2;
        result.Add(new Tuple<T, TimeSpan>(curr.Item1, elapsed));
      }

      return result;
    }
  }
}
