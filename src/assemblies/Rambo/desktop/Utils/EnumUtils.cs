using System;
using System.Collections.Generic;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils
{
  public sealed class EnumUtils
  {
    /// <summary>
    /// Tries to parse the given string into a value of the supplied Enumeration type, returns true if successful.
    /// Uses a case-insensitive match.
    /// </summary>
    /// <typeparam name="T">An enumeration type</typeparam>
    /// <param name="toParse_">String to be parsed</param>
    /// <param name="returnVal_">Enumeration parsed value</param>
    /// <returns>True if successfully parsed string</returns>
    public static bool TryParse<T>(string toParse_, out T returnVal_)
    {
      return TryParse(toParse_, false, out returnVal_);
    }

    /// <summary>
    /// Tries to parse the given string into a value of the supplied Enumeration type, returns true if successful.
    /// </summary>
    /// <typeparam name="T">An enumeration type</typeparam>
    /// <param name="toParse_">String to be parsed</param>
    /// <param name="caseSensitive_">If true, string matches against enumeration will be case-sensitive</param>
    /// <param name="returnVal_">Enumeration parsed value; If return value is <c>false</c>, enum is set back to its default.</param>
    /// <returns>True if successfully parsed string</returns>
    public static bool TryParse<T>(string toParse_, bool caseSensitive_, out T returnVal_)
    {
      returnVal_ = default(T);

      if (!typeof(T).IsEnum)
      {
        return false;
      }

      if (toParse_ == null)
      {
        return false;
      }

      IComparer<string> comparer = caseSensitive_ ? StringComparer.Ordinal : StringComparer.InvariantCultureIgnoreCase;

      string[] vals = Enum.GetNames(typeof(T));
      Array.Sort(vals, comparer);
      int index = Array.BinarySearch(vals, toParse_, comparer);

      if (index < 0)
      {
        return false;
      }

      returnVal_ = (T)Enum.Parse(typeof(T), toParse_, true);

      return true;
    }

    /// <summary>
    /// Do a TryParse but specify a default to return if the parse fails
    /// </summary>
    /// <typeparam name="T">The type of the enum</typeparam>
    /// <param name="toParse_">The string i want to parse</param>
    /// <param name="default_">The default that will get returned</param>
    /// <param name="caseSensitive_">Whether the match is case sensitive</param>
    /// <returns>The default if failed, otherwise the result of the parse</returns>
    public static T DefaultedParse<T>(string toParse_, T default_, bool caseSensitive_)
    {
      T output;

      if (TryParse(toParse_, caseSensitive_, out output)) return output;

      return default_;
    }
  }
}