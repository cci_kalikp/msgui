namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.FileSenders
{
  public interface IFileSender
  {
    void SendFile(string fileName_);
  }
}