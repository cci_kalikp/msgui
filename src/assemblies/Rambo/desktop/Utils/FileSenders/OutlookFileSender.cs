﻿using System;
using System.Runtime.InteropServices;
using Microsoft.Office.Interop.Outlook;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.FileSenders
{
  ///<summary>
  /// The class is used to display an Outlook create Message form with attachment preattached
  /// Please note you should remove references the Outlook 10.0 PIA if you have them in the cliaent app
  ///</summary>
  public class OutlookFileSender:IFileSender
  {
    private static readonly Logger _log = new Logger(typeof(OutlookFileSender));
    /// <summary>
    /// Attaches the file to Microsoft Outlook Email.
    /// </summary>
    /// <param name="fileName_">The file name to attach.</param>
    public void SendFile(string fileName_)
    {
      try
      {
        object missing = Type.Missing;
        var app = new Application();
        var mail = app.CreateItem(OlItemType.olMailItem) as MailItem;
        if (mail != null)
        {
          _Inspector inspector = mail.GetInspector;
          inspector.Activate();
          mail.Subject = "Excel Workbook exported from RiskViewer";
          mail.Attachments.Add(fileName_, OlAttachmentType.olByValue, missing, missing);
          mail.Display(false);
        }
      }
      catch (COMException ex)
      {
        _log.Error("AttachFileToOutlookMail", "Attaching Excel file to Outlook E-mail failed", ex);
      }
    }
  }
}
