using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using MorganStanley.MSDotNet.MSNet;
using MorganStanley.MSDesktop.Rambo.Desktop.Utils;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Netegrity
{
	/// <summary>
	/// Summary description for PasswordForm.
	/// </summary>
	public sealed class PasswordForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label _labelUserName;
		private System.Windows.Forms.PictureBox _pictureBoxKeys;
		private System.Windows.Forms.Label _labelPassword;
		private System.Windows.Forms.Button _btnCancel;
		private System.Windows.Forms.Button _btnOK;
		private System.Windows.Forms.TextBox _password;
		private System.Windows.Forms.Label _userName;
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.ErrorProvider _errorProvider;

		private IMSNetLoop _loop;
		private DialogResult _dialogResult = DialogResult.Cancel;

		private PasswordForm(IMSNetLoop loop_)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			_loop = loop_;

			string domain_username = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
			int indexOfBkSlash = domain_username.LastIndexOf(@"\");
			if (indexOfBkSlash != -1)
				_userName.Text = domain_username.Substring (indexOfBkSlash + 1);
			else
				_userName.Text = domain_username;
		}

		public static DialogResult ShowDialog (IWin32Window owner_, IMSNetLoop loop_)
		{
			PasswordForm passwordForm = new PasswordForm (loop_);
			DialogResult dialogResult = passwordForm.GetPassword (owner_);
			passwordForm.Dispose();
			return dialogResult;
		}

		public static DialogResult ShowDialog (IMSNetLoop loop_)
		{
			return ShowDialog (null, loop_);
		}

		private DialogResult GetPassword (IWin32Window owner_)
		{
			if (owner_ == null)
			{
				base.ShowDialog();
			}
			else
			{
				base.ShowDialog (owner_);
			}

			return _dialogResult;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(PasswordForm));
			this._labelUserName = new System.Windows.Forms.Label();
			this._pictureBoxKeys = new System.Windows.Forms.PictureBox();
			this._labelPassword = new System.Windows.Forms.Label();
			this._btnCancel = new System.Windows.Forms.Button();
			this._btnOK = new System.Windows.Forms.Button();
			this._password = new System.Windows.Forms.TextBox();
			this._userName = new System.Windows.Forms.Label();
			this._errorProvider = new System.Windows.Forms.ErrorProvider();
			this.SuspendLayout();
			// 
			// _labelUserName
			// 
			this._labelUserName.Location = new System.Drawing.Point(12, 76);
			this._labelUserName.Name = "_labelUserName";
			this._labelUserName.Size = new System.Drawing.Size(68, 16);
			this._labelUserName.TabIndex = 0;
			this._labelUserName.Text = "User name:";
			this._labelUserName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// _pictureBoxKeys
			// 
			this._pictureBoxKeys.Image = ((System.Drawing.Bitmap)(resources.GetObject("_pictureBoxKeys.Image")));
			this._pictureBoxKeys.Name = "_pictureBoxKeys";
			this._pictureBoxKeys.Size = new System.Drawing.Size(320, 60);
			this._pictureBoxKeys.TabIndex = 1;
			this._pictureBoxKeys.TabStop = false;
			// 
			// _labelPassword
			// 
			this._labelPassword.Location = new System.Drawing.Point(12, 108);
			this._labelPassword.Name = "_labelPassword";
			this._labelPassword.Size = new System.Drawing.Size(68, 16);
			this._labelPassword.TabIndex = 2;
			this._labelPassword.Text = "Password:";
			this._labelPassword.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// _btnCancel
			// 
			this._btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this._btnCancel.Location = new System.Drawing.Point(220, 140);
			this._btnCancel.Name = "_btnCancel";
			this._btnCancel.TabIndex = 3;
			this._btnCancel.Text = "Cancel";
			this._btnCancel.Click += new System.EventHandler(this._btnCancel_Click);
			// 
			// _btnOK
			// 
			this._btnOK.Location = new System.Drawing.Point(136, 140);
			this._btnOK.Name = "_btnOK";
			this._btnOK.TabIndex = 4;
			this._btnOK.Text = "OK";
			this._btnOK.Click += new System.EventHandler(this._btnOK_Click);
			// 
			// _password
			// 
			this._password.Location = new System.Drawing.Point(108, 104);
			this._password.Name = "_password";
			this._password.PasswordChar = '*';
			this._password.Size = new System.Drawing.Size(188, 20);
			this._password.TabIndex = 0;
			this._password.Text = "";
			// 
			// _userName
			// 
			this._userName.Location = new System.Drawing.Point(108, 76);
			this._userName.Name = "_userName";
			this._userName.Size = new System.Drawing.Size(188, 16);
			this._userName.TabIndex = 6;
			this._userName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// PasswordForm
			// 
			this.AcceptButton = this._btnOK;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.CancelButton = this._btnCancel;
			this.ClientSize = new System.Drawing.Size(314, 176);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																																	this._userName,
																																	this._password,
																																	this._btnOK,
																																	this._btnCancel,
																																	this._labelPassword,
																																	this._pictureBoxKeys,
																																	this._labelUserName});
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "PasswordForm";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Please log in...";
			this.ResumeLayout(false);

		}
		#endregion

		private void _btnOK_Click(object sender, System.EventArgs e)
		{
			try
			{
				Netegrity.Token.Refresh (_loop, _userName.Text, _password.Text, 5000);
				_dialogResult = DialogResult.OK;
				Close();
			}
			catch (Exception ex)
			{
				_errorProvider.SetError (_password, string.Format ("Unable to log in: {0}", ex.Message));
			}
		}

		private void _btnCancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}
	}
}
