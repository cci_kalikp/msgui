using System;
using MorganStanley.MSDotNet.MSAuth;
using MorganStanley.MSDotNet.MSNet;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Netegrity
{
	/// <summary>
	/// Class to faciliate retrieval of Netegrity tokens.
	/// </summary>
	public class Token
	{
		private static string _token = null;
		private static readonly string URL = "http://smauth.ms.com/webinfra/smauth/webapp/1.0?format=properties";

		private Token() {}

		/// <summary>
		/// Refresh the current user's token.  This method is synchronous.
		/// </summary>
		/// <param name="loop_">Loop on which to call the netegrity SOAP server.</param>
		/// <param name="kerberosPassword_">The Kerberos password of the user.</param>
		/// <param name="timeout_">Timeout in milliseconds.</param>
		public static void Refresh (IMSNetLoop loop_, string kerberosUserName_, string kerberosPassword_, int timeout_)
		{			
			lock (typeof (Token))
			{
				MSAuthNetegrityHttpClientImpl m_smclient = new MSAuthNetegrityHttpClientImpl (loop_, URL);
				_token = m_smclient.SyncGetSessionToken(kerberosUserName_, kerberosPassword_, timeout_);
			}
		}

		/// <summary>
		/// The Netegrity token string.
		/// </summary>
		/// <exception cref="NullReferenceException">If Refresh() has not been called then the token will be null.</exception>
		public static string Value
		{
			get
			{
				lock (typeof (Token))
				{
					if (_token == null)
					{
						throw new NullReferenceException ("Token is uninitialised!");
					}
					return _token;
				}
			}
		}
	}
}
