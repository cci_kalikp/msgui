﻿using System;
using MorganStanley.MSDotNet.Directory;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils
{
    public class UserVerification
    {
        private static Search _dirSearch;

        private static Search GetSearchObject()
        {
            return _dirSearch ?? (_dirSearch = new FWD2Search
                                                   {
                                                       Username = "cn=dsserverid, ou=ldapids, o=Morgan Stanley",
                                                       Password = "DS_Server"
                                                   });
        }

        public static Person GetPerson(string userName_)
        {
            return GetSearchObject().GetPersonByLogon(userName_);
        }

        public static Group GetGroup(string groupEmail_)
        {
            return GetSearchObject().GetGroup(groupEmail_);
        }

        public static Division GetDivision(Person person_)
        {
            return GetSearchObject().GetDivisionByCode(person_.DivisionCode);
        }

        public static bool IsInGroup(Person person_, Group group_)
        {
            string dn = GetSearchObject().GetPersonDN(person_);

            return group_.IsPersonMember(dn);
        }

        public static bool IsThisUserInGroup(string groupEmail_)
        {
            Person person = GetPerson(Environment.UserName);
            Group group = GetGroup(groupEmail_);

            return IsInGroup(person, group);
        }
    }
}
