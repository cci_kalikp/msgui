﻿
using System;
using MorganStanley.MSDesktop.Rambo.Desktop.Utils;

namespace MorganStanley.MSDesktop.Rambo.RiskViewer.UI
{
  public class LogTimer : IDisposable
  {
    private readonly DateTime _startTime;
    private readonly ILogger _logger;
    private readonly String _message;
    private readonly String _methodName;

    public LogTimer(ILogger logger_, String message_, string methodName_)
    {
      _logger = logger_;
      _startTime = DateTime.Now;
      _message = message_;
      _methodName = methodName_;
    }

    public void Dispose()
    {
      _logger.Info(_methodName,
                   String.Format("Monitoring: {0}, time taken: {1}",
                                 _message,
                                 DateTime.Now.Subtract(_startTime).TotalSeconds));
    }
  }
}
