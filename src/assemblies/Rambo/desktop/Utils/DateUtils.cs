///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2002 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
///////////////////////////////////////////////////////////////////////////////
//
//       Filename:  $RCSfile: DateUtils.cs,v $ 
//       Revision:  $Revision: #3 $ 
// Last edited by:  $Author: boqwang $ 
//             on:  $Date: 2013/02/28 $ 
//
// $Id: //eai/msdotnet/msgui/trunk/assemblies/Rambo/desktop/Utils/DateUtils.cs#3 $ 
//
///////////////////////////////////////////////////////////////////////////////
//
// $Log: DateUtils.cs,v $
// Revision 1.15  2003/09/11 13:46:22  perrys
// deprecated old stuff
//
// Revision 1.14  2003/09/04 09:03:52  perrys
// new logger api
//
// Revision 1.13  2003/07/30 09:06:18  perrys
// doco/namespace/tidy up
//
//
///////////////////////////////////////////////////////////////////////////////

using System;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils
{
  /// <summary>
  /// Quick, simple ISO date conversion utils, ported from C++ DateUtils class.
  /// </summary>
  public sealed class DateUtils
  {
    private string _format;
    private static Logger _cat = new Logger(typeof(DateUtils), "ecdev", "desktop");

    public DateUtils(String format_)
    {
      _format = format_;
    }

    public void DateToStringConverter(object sender_, System.Windows.Forms.ConvertEventArgs e)
    {
      if (e.Value is DateTime && e.DesiredType == typeof(string))
      {
        e.Value = ((DateTime)e.Value).ToString(_format);
      }
    }

    public void StringToDateConverter(object sender_, System.Windows.Forms.ConvertEventArgs e)
    {
      if (e.Value is string && e.DesiredType == typeof(DateTime))
      {
        try
        {
          e.Value = DateTime.Parse(e.Value.ToString());
        }
        catch(Exception ex)
        {
          _cat.Error("StringToDateConverter", "bad date", ex);
          e.Value = "#Error#";
        }      
      }
    }

    public static readonly string ISO_DATE_FORMAT = "yyyy-MM-dd";

    /// <summary>
    /// ISO date time format (does includes 'T' between date and time, but no zone info).
    /// Similar to DateTimeFormatInfo.UniversalSortableDateTimePattern.
    /// </summary>
    [Obsolete("Please use \"s\" format instead", false)]
    public static readonly string ISO_TIME_FORMAT = "yyyy-MM-ddTHH:mm:ss";

    public static readonly string DISPLAY_DATE_FORMAT = "dd MMM yy";

    public static readonly string DISPLAY_DATE_TIME_FORMAT = "dd MMM yy, HH:mm:ss";

    public static readonly string SHORT_DATE_FORMAT = "MMM dd";

	  	 
    /// <summary>
    /// Attempt to create a datetime from the given string. Positional parse means
    /// that separators and extraneous characters should not affect the result.
    /// </summary>
    /// <param name="str_">ISO date string, eg "2002-03-25 13:45:56"</param>
    /// <returns>a datetime object</returns>
    /// <exception cref="System.ArgumentException">bad args</exception>
    /// <exception cref="System.ArgumentOutofRangeException">inconsistent values</exception>
    [Obsolete("Use DateTime.Parse instead", false)]
    public static DateTime ParseDate(string str_)
    {
      // format: "yyyy-MM-dd HH:mm:ss"
      if (str_.Length < 10)
        throw new ArgumentException("\"" + str_ + "\" is not a valid date");

      int year = int.Parse(str_.Substring(0,4));
      int month = int.Parse(str_.Substring(5,2));
      int day = int.Parse(str_.Substring(8,2));

      int hour=0;
      int min=0;
      int secs=0;

      if (str_.Length >= 13)
      {
        hour = int.Parse(str_.Substring(11,2));
        if (str_.Length >= 16)
        {
          min = int.Parse(str_.Substring(14,2));
          if (str_.Length >= 19)
          {
            secs = int.Parse(str_.Substring(17,2));
          }
        }
      }
      return new DateTime(year,month,day,hour,min,secs);
    }

    /// <summary>
    /// Converts the difference between two DateTimes to years
    /// WIP some clever leap year algorythm.  
    /// </summary>
    /// <param name="nowDate_">the reference date</param>
    /// <param name="futureDate_">the date in the future</param>
    /// <returns></returns>
    public static double DateToYears(DateTime nowDate_, DateTime futureDate_)
    {
      TimeSpan diff = futureDate_.Subtract(nowDate_);		
      return diff.TotalDays/365;//.24;
    }
  }
}
