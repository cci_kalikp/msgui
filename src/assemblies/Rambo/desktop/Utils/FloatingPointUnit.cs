using System;
using System.Runtime.InteropServices;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils
{
  /// <summary>
  /// Summary description for FloatingPointUnit.
  /// </summary>
  public class FloatingPointUnit
  {
    [DllImport("msvcr70.dll", CallingConvention = CallingConvention.Cdecl)]
    private static extern int _controlfp(int n, int mask);
    const int _RC_NEAR       = 0x00000000;
    const int _PC_53         = 0x00010000;
    const int _EM_INVALID    = 0x00000010;
    const int _EM_UNDERFLOW  = 0x00000002;
    const int _EM_ZERODIVIDE = 0x00000008;
    const int _EM_OVERFLOW   = 0x00000004;
    const int _EM_INEXACT    = 0x00000001;
    const int _EM_DENORMAL   = 0x00080000;
    const int _CW_DEFAULT    = ( _RC_NEAR + _PC_53 +    _EM_INVALID + _EM_ZERODIVIDE + _EM_OVERFLOW + _EM_UNDERFLOW +    _EM_INEXACT + _EM_DENORMAL);    
    
    public static void Reset(){    _controlfp(_CW_DEFAULT ,0xfffff);} 
		
    public FloatingPointUnit()
    {		
    }
	}
}
