﻿using System;
using System.Collections;
using System.Linq;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.ObjectSearch
{
  public static class Extensions
  {
    /// <summary>
    /// This will go through the class and search if any of its value types or string properties that 
    /// are marked with [Search] attribute match se3arch string criteria.  
    /// 
    /// This function will recurse down the [Search] properties to look for <i>searchString_</i>
    /// if it is found it will return true else false.  If whole class is marked with [Search] attribute
    /// every property will be examined.
    /// </summary>
    /// <param name="obj_">object to search</param>
    /// <param name="searchString_">string to search for</param>
    /// <returns>true iff object has a string, else false </returns>
    public static bool SearchAny(this object obj_, string searchString_)
    {
      return obj_.SearchAny(searchString_, false);
    }

    /// <summary>
    /// This will go through the class and search if any of its value types or string properties that 
    /// are marked with [Search] attribute match se3arch string criteria.  
    /// 
    /// This function will recurse down the [Search] properties to look for <i>searchString_</i>
    /// if it is found it will return true else false.  If whole class is marked with [Search] attribute
    /// every property will be examined.
    /// </summary>
    /// <param name="obj_">object to search</param>
    /// <param name="searchString_">string to search for</param>
    ///  <param name="caseSensetive_">if false the search would not be case sensitive</param>
    /// <returns>true iff object has a string, else false </returns>
    public static bool SearchAny(this object obj_, string searchString_, bool caseSensetive_)
    {
      Predicate<object> searchFor;
      if (caseSensetive_)
        searchFor = obj => string.IsNullOrEmpty(searchString_) || obj.ToString().Contains(searchString_);
      else
        searchFor = obj => string.IsNullOrEmpty(searchString_) || obj.ToString().ToUpper().Contains(searchString_.ToUpper());

      return obj_.SearchAny(obj => obj is string || obj is ValueType, searchFor);
    }

    /// <summary>
    /// This will go through the class and search if any of its value types or string properties that 
    /// are marked with [Search] attribute match se3arch string criteria.  
    /// 
    /// This function will recurse down the [Search] properties untill stopCondition_ returns true and then it will check if 
    /// searchfor_ returns true once this is found the function returns true
    /// if it is found it will return true else false.  If whole class is marked with [Search] attribute
    /// every property will be examined.
    /// </summary>
    /// <param name="obj_">object to search</param>
    ///  <param name="stopCondition_"> stop searching condition (where to stop search recursion)</param>
    /// <param name="searchFor_">predicate to check </param>
    /// <returns>true iff any property of the object or its children(recurse) matches searchFor_ </returns>
    /// <example>
    ///        searchFor = obj => string.IsNullOrEmpty(searchString_) || obj.ToString().ToUpper().Contains(searchString_.ToUpper());
    //        return obj_.Search(obj => obj is string || obj is ValueType, searchFor);
    /// </example>


    public static bool SearchAny(this object obj_, Predicate<object> stopCondition_, Predicate<object> searchFor_)
    {
      if (obj_ == null)
      {
        return false;
      }

      if (stopCondition_(obj_))
      {
        if (searchFor_(obj_)) return true;
      }
      else if (obj_ is ISearchable)
      {
        if (((ISearchable)obj_).Search(stopCondition_, searchFor_))
          return true;
      }
      else if (obj_ is IEnumerable)
      {
        var enumerable = obj_ as IEnumerable;
        if (enumerable.Cast<object>().Any
          (item_ => item_.SearchAny(stopCondition_, searchFor_)))
        {
          return true;
        }
      }
      else
      {
        if (SearchProperties(obj_, stopCondition_, searchFor_)) return true;
      }
      return false;
    }

    private static bool SearchProperties(object obj_, Predicate<object> stopCondition_, Predicate<object> searchFor_)
    {
      var properties = obj_.GetType().GetProperties();
      var classAttributes = obj_.GetType().GetCustomAttributes(typeof(SearchAttribute), true);
      var searchAllClass = classAttributes.Length > 0;
      var allSearchableProperties = properties.Where(property_ => searchAllClass || property_.GetCustomAttributes(typeof(SearchAttribute), true).Any());
      return allSearchableProperties.Select(prop_ => prop_.GetValue(obj_, null)).Any(val_ => val_.SearchAny(stopCondition_, searchFor_));
    }

  }
}
