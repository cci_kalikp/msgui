﻿using System;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.ObjectSearch
{
  [AttributeUsage(AttributeTargets.Property | AttributeTargets.Class)]
  public class SearchAttribute: Attribute
  {
  }
}
