﻿using System;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.ObjectSearch
{
  public interface ISearchable
  {
    bool Search(string str_, bool caseSensetive_);
    bool Search(Predicate<object> stopCondition_, Predicate<object> searchFor_);
  }
}