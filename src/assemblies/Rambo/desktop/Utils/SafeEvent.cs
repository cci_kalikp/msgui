﻿using System;
using System.Collections.Generic;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils
{
  /// <summary>
  /// Wrapper around a generic event that invokes every subscriber inside a try/catch. So all the subscribers receive the event, even if one of them throws an exception in the event handler. 
  /// </summary>
  /// <typeparam name="T">The type of EventArgs of your wrapped event</typeparam>
  public class SafeEvent<T> where T : EventArgs
  {
    private readonly Action<Exception> _onFailure;
    private readonly Dictionary<EventHandler<T>, EventHandler<T>> _unsafe2safe = new Dictionary<EventHandler<T>, EventHandler<T>>();

    private EventHandler<T> _safe;

    /// <summary>
    /// </summary>
    /// <param name="onFailure_">What to do when one of the event invocations fails - should probably hook this into a logger call</param>
    public SafeEvent(Action<Exception> onFailure_)
    {
      _onFailure = onFailure_;
    }

    /// <summary>
    /// Wrapped event - subscribe/unsubscribe from this in your event's add/remove blocks
    /// </summary>
    public event EventHandler<T> Event
    {
      add
      {
        if (_unsafe2safe.ContainsKey(value))
        {
          return; // drop duplicate subscription
        }

        var safeHandler = new EventHandler<T>((s, e) =>
        {
          try
          {
            value.Invoke(s, e);
          }
          catch (Exception ex)
          {
            _onFailure(ex);
          }
        });
        _safe += safeHandler;
        _unsafe2safe[value] = safeHandler;
      }
      remove
      {
        EventHandler<T> result;
        if (!_unsafe2safe.TryGetValue(value, out result))
        {
          return; 
        }

        _safe -= result;
        _unsafe2safe.Remove(value);
      }
    }

    /// <summary>
    /// How to trigger the wrapped event from the client code
    /// </summary>
    public void Fire(object sender_, T args_)
    {
      if (_safe == null) return;

      _safe(sender_, args_);
    }

#if x

    private class Example
    {
      private readonly SafeEvent<EventArgs> _dummy = new SafeEvent<EventArgs>(e => System.Diagnostics.Debug.WriteLine(e.Message));

      // Clients subscribe to this
      public event EventHandler<EventArgs> Dummy
      {
        add { _dummy.Event += value; }
        remove { _dummy.Event -= value; }
      }

      public void Trigger()
      {
        _dummy.Fire(this, EventArgs.Empty);
      }
    }

#endif
  }
}