using System;
using System.Text;
using System.IO;
using System.Net;
using System.Collections.Specialized;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Web
{
  /// <summary>
  /// Class to facilitate scraping of web pages.
  /// </summary>
  public class WebPage
  {
    private WebPage() {}

    /// <summary>
    /// Retrieve a web page using POST parameters.
    /// </summary>
    /// <param name="url_">The URL to connect to.</param>
    /// <param name="postParams_">Name/value pairs of POST parameters.  These will be encoded for you.</param>
    /// <param name="timeout_">Timeout in milliseconds.</param>
    /// <returns>A string containing the HTTP response body.</returns>
    public static string Post(string url_, NameValueCollection postParams_, int timeout_)
    {
      StringBuilder postStr = new StringBuilder();
      foreach (string key in postParams_.Keys)
      {
        EncodeAndAddItem(ref postStr, key, postParams_[key]);
      }

      HttpWebResponse resp = null;

      try
      {
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url_);

        byte[] postDataBytes = Encoding.UTF8.GetBytes(postStr.ToString());
        request.Method = "POST";
        request.ContentType = "application/x-www-form-urlencoded";
        request.ContentLength = postDataBytes.Length;
        request.Timeout = timeout_;
        request.KeepAlive = false;

        Stream requestStream = request.GetRequestStream();
        requestStream.Write(postDataBytes, 0, postDataBytes.Length);
        requestStream.Close();

        resp = (HttpWebResponse)request.GetResponse();

        if (resp.StatusCode != HttpStatusCode.OK)
        {
          throw new Exception("Not OK retrieving " + url_ + ": " + resp.StatusCode + ": " + resp.StatusDescription);
        }

        StreamReader responseReader = new StreamReader(resp.GetResponseStream(), Encoding.UTF8);
        return responseReader.ReadToEnd();
      }
      finally
      {
        if (resp != null)
        {
          resp.Close();
        }
      }
    }

    private static void EncodeAndAddItem (ref StringBuilder baseRequest, string key, string dataItem)
    {
      if (baseRequest == null)
      {
        baseRequest = new StringBuilder();
      }
      if (baseRequest.Length != 0)
      {
        baseRequest.Append ("&");
      }
      baseRequest.Append (key);
      baseRequest.Append ("=");
      baseRequest.Append (System.Web.HttpUtility.UrlEncode (dataItem));
    }

    /// <summary>
    /// Retrieve a web page using GET parameters, ie as part of the URL.
    /// </summary>
    /// <param name="url_">The URL to connect to, containing the GET parameters.</param>
    /// <returns>A string containing the HTTP response body.</returns>
    public static string Get (string url_, int timeout_)
    {
      // used to build entire input
      StringBuilder sb  = new StringBuilder();

      // used on each read operation
      byte[]        buf = new byte[8192];

      // prepare the web page we will be asking for
      HttpWebRequest  request  = (HttpWebRequest)WebRequest.Create(url_);
      request.Timeout = timeout_;

      // execute the request
      HttpWebResponse response = (HttpWebResponse)request.GetResponse();

      if (response.StatusCode != HttpStatusCode.OK)
      {
        throw new Exception ("Not OK retrieving " + url_ + ": " + response.StatusCode + ": " + response.StatusDescription);
      }

      // we will read data via the response stream
      Stream resStream = response.GetResponseStream();

      string tempString = null;
      int    count      = 0;

      do
      {
        // fill the buffer with data
        count = resStream.Read(buf, 0, buf.Length);

        // make sure we read some data
        if (count != 0)
        {
          // translate from bytes to ASCII text
          tempString = Encoding.ASCII.GetString(buf, 0, count);

          // continue building the string
          sb.Append(tempString);
        }
      }
      while (count > 0); // any more data to read?

      return sb.ToString();
    }
  }
}
