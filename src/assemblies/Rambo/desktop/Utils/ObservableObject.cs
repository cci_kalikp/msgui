﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;
using MorganStanley.MSDesktop.Rambo.Desktop.Utils.Extensions;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils
{
  ///<summary>
  /// Base class for objects raising property changed events
  ///</summary>
  public abstract class ObservableObject : INotifyPropertyChanged
  {
    ///<summary>
    /// INotifyPropertyChanged Changed event
    ///</summary>
    public event PropertyChangedEventHandler PropertyChanged;

    protected void InvokePropertyChanged(Expression<Func<object>> property_)
    {
      this.InvokePropertyChangedUsingLambda(PropertyChanged, property_);
    }

    protected void InvokePropertyChanged()
    {
      InvokePropertyChanged(String.Empty);
    }

    protected void InvokePropertyChanged(string propertyName_)
    {
      PropertyChangedEventHandler temp = PropertyChanged;
      if (temp != null)
      {
        temp(this, new PropertyChangedEventArgs(propertyName_));
      }
    }
  }
}
