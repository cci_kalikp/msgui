using System;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils
{
	/// <summary>
	/// Class to hold information relating to a logged exception.
	/// </summary>
	public class LoggedExceptionEventArgs : EventArgs
	{
		private string _method;
		private string _msg;
		private Exception _ex;

		public LoggedExceptionEventArgs(string method_, string msg_, Exception ex_)
		{
			_method = method_;
			_msg = msg_;
			_ex = ex_;
		}

		public string Method
		{
			get { return _method; }
		}

		public string Message
		{
			get { return _msg; }
		}

		public Exception Exception
		{
			get { return _ex; }
		}
	}

	public delegate void LoggedExceptionEventHandler (object sender_, LoggedExceptionEventArgs ea_);
}
