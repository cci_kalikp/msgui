///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2004 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
///////////////////////////////////////////////////////////////////////////////
//
//       Filename:  $RCSfile: LoggerManager.cs,v $ 
//       Revision:  $Revision: #3 $ 
// Last edited by:  $Author: boqwang $ 
//             on:  $Date: 2013/02/28 $ 
//
// $Id: //eai/msdotnet/msgui/trunk/assemblies/Rambo/desktop/Utils/LoggerManager.cs#3 $ 
//
///////////////////////////////////////////////////////////////////////////////
//
// $Log: LoggerManager.cs,v $
// Revision 1.2  2004/02/27 10:59:07  perrys
// default factory implementation sans hardcoded layer
//
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Configuration;
using System.IO;
using MorganStanley.MSDotNet.MSLog;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils
{
  /// <summary>
  /// Summary description for LoggerManager.
  /// </summary>
  public class LoggerManager
  {
    private static ILoggerFactory _factoryImplementation = new DefaultLoggerFactory();

    /// <summary>
    /// Obtain a logger ovbject for the specifed class
    /// Delegates to our encapsulated ILoggerFactory, which defaults
    /// to an instance of DefaultLoggerFactory.
    /// </summary>
    /// <param name="class_"></param>
    /// <returns>The logger for the given class</returns>
    public static ILogger GetLogger( Type class_ )
    {
      ILogger logger = _factoryImplementation.GetLogger( class_ ) ;
      return ( logger ) ;
    }


    /// <summary>
    /// Get/Set the factory implementation.
    /// </summary>
    public static ILoggerFactory LoggerFactory
    {
      get
      {
        return ( _factoryImplementation ) ;
      }
      set
      {
        _factoryImplementation = value ;
      }
    }

    /// <summary>
    /// Gets the location of the Logger's log file
    /// </summary>
    /// <returns>The location of the Logger's log file; if nothing found, null is returned</returns>    
    public static FileInfo GetLogFileInfo()
    {
      foreach (var destination in MSLog.Destinations)
      {
        var fileDestination = destination as MSLogFileDestination;
        if (fileDestination != null && !string.IsNullOrEmpty(fileDestination.FileName))
        {
          return (new FileInfo(fileDestination.FileName));
        }
      }
      return null;
    }
  }
}
