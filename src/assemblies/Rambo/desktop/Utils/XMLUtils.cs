using System;
using System.Xml;
using System.Xml.XPath;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils
{
  public sealed class XMLUtils
  {

    /// <summary>
    /// Retrieves the string value output of the input <paramref name="xpathQuery"/> based on the
    /// passed <see cref="IXPathNavigable"/> (could be <see cref="XPathNavigator"/> or <see cref="XmlNode"/>)
    /// </summary>
    /// <param name="input">XML component to be queried upon</param>
    /// <param name="xpathQuery">XPATH query to perform in order to retrieve value</param>
    /// <returns>The string value output of the XPATH query if it succeeds; otherwise, false</returns>
    public static string GetNodeValue(IXPathNavigable input, string xpathQuery)
    {
      return GetNodeValue(input, xpathQuery, false);
    }

    /// <summary>
    /// Retrieves the string value output of the input <paramref name="xpathQuery"/> based on the
    /// passed <see cref="IXPathNavigable"/> (could be <see cref="XPathNavigator"/> or <see cref="XmlNode"/>)
    /// </summary>
    /// <param name="input">XML component to be queried upon</param>
    /// <param name="xpathQuery">XPATH query to perform in order to retrieve value</param>
    /// <param name="returnEmptyOnFailure">If the query returns no value, return an empty string; otherwise, return null</param>
    /// <returns>The string value output of the XPATH query if it succeeds; otherwise, null or string.Empty based on <paramref name="returnEmptyOnFailure"/>.</returns>
    public static string GetNodeValue(IXPathNavigable input, string xpathQuery, bool returnEmptyOnFailure)
    {
      if (input == null)
      {
        throw new ArgumentNullException("input", "Input xml node cannot be null");
      }
      
      if (xpathQuery == null)
      {
        throw new ArgumentNullException("xpathQuery", "XPath Query cannot be null");
      }

      XPathNavigator queryOutput = input.CreateNavigator().SelectSingleNode(xpathQuery);
      if (queryOutput != null)
      {
        return queryOutput.Value;
      }
      else if (returnEmptyOnFailure)
      {
        return string.Empty;
      }
      else
      {
        return null;
      }
    }

    /// <summary>
    /// Modifies the node passed in and adds or updates the node with the specified element and value
    /// </summary>
    /// <param name="parent_">The parent XML Element.</param>
    /// <param name="elementName_">The name of the node to create/modify.</param>
    /// <param name="value_">The value of the node to create/modify.</param>
    public static void SetElementValue(XmlNode parent_, string elementName_, string value_)
    {
      if (elementName_ == null)
      {
        throw new ArgumentNullException("elementName_", "Element name to create/modify cannot be null");
      }

      if (value_ == null)
      {
        throw new ArgumentNullException("value_", "Element value to set cannot be null");
      }

      if (parent_ != null)
      {
        XmlNode childNode = parent_.SelectSingleNode(elementName_);
        if (childNode != null)
        {
          childNode.InnerText = value_;
        }
        else
        {
          childNode = parent_.OwnerDocument.CreateElement(elementName_);
          childNode.InnerText = value_;
          parent_.AppendChild(childNode);
        }
      }
    }
  }
}
