using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using MorganStanley.MSDotNet.MSLog;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils
{
  /// <summary>
  /// Simple MSLog wrapper based on a single log layer per assembly. 
  /// Keep an instance in each class and call the methods
  /// below to log to the assembly's layer, setting the correct class
  /// and method parameters.
  /// NB: remember to call  MSLogConfig.Init(logElement_) asap after
  /// startup.
  /// </summary>
  public class Logger : ILogger
  {
    private string _class;
    private MSLogLayer _layer;

    /// <summary>
    /// Event that is fired when an exception is logged via Warning(), Error() or Critical().
    /// Note that it will only be fired if Logger.SendExceptionEvents is set to true - by default it is false.
    /// </summary>
    public static event LoggedExceptionEventHandler ExceptionLogged;

    /// <summary>
    /// Simplest ctr. Attempts to guess meta and project from class namespace,
    /// and release number from class assembly.
    /// </summary>
    /// <param name="claz_">Class from which log messages originate</param>
    public Logger(Type claz_)
    {
      String meta, proj;
      string rel = GetVersionString(claz_);
      GetMetaProject(claz_, out meta, out proj);
      Init(claz_, meta, proj, rel);
    }

    /// <summary>
    /// Ctr for a logging object. Gets version information from type's assembly.
    /// </summary>
    /// <param name="claz_">Class from which log messages originate</param>
    /// <param name="meta_">Project meta</param>
    /// <param name="proj_">Proejct name</param>
    public Logger(Type claz_, String meta_, String proj_)
    {
      String versionStr = GetVersionString(claz_);
      Init(claz_, meta_, proj_, versionStr);
    }

    /// <summary>
    /// Ctr for a logging object.
    /// </summary>
    /// <param name="claz_">Class from which log messages originate</param>
    /// <param name="meta_">Project meta</param>
    /// <param name="proj_">Proejct name</param>
    /// <param name="rel_">Project release name</param>
    public Logger(Type claz_, String meta_, String proj_, String rel_)
    {
      Init(claz_, meta_, proj_, rel_);
    }

    /// <summary>
    /// Ctr for a logging object.
    /// </summary>
    /// <param name="claz_">Class from which log messages originate</param>
    /// <param name="layer_">Layer to which messages are to be logged</param>
    public Logger(Type claz_, MSLogLayer layer_)
    {
      Init(claz_, layer_);
    }

    static Logger()
    {
      SendExceptionEvents = false;
    }

    public String Class
    {
      get { return _class; }
    }

    public MSLogLayer Layer
    {
      get { return _layer; }
    }

    public void Debug(string method_, string message_, params object[] params_)
    {
      Debug(method_, string.Format(message_, params_));
    }

    public void Debug(string method_, string msg_)
    {
      DoLog(method_, msg_, MSLog.Debug());
    }

    public void Info(string method_, string message_, params object[] params_)
    {
      Info(method_, string.Format(message_, params_));
    }

    public void Info(string method_, string msg_)
    {
      DoLog(method_, msg_, MSLog.Info());
    }

    public void Notice(string method_, string message_, params object[] params_)
    {
      Notice(method_, string.Format(message_, params_));
    }

    public void Notice(string method_, string msg_)
    {
      DoLog(method_, msg_, MSLog.Notice());
    }

    public void WarningF(string method_, string message_, params object[] params_)
    {
      Warning(method_, string.Format(message_, params_));
    }

    public void Warning(string method_, string message_, params string[] params_)
    {
      Warning(method_, string.Format(message_, params_));
    }

    public void Warning(string method_, Exception ex_, string message_, params object[] params_)
    {
      Warning(method_, string.Format(message_, params_), ex_);
    }

    public void Warning(string method_, string msg_)
    {
      DoLog(method_, msg_, MSLog.Warning());
    }

    public void Warning(string method_, string msg_, Exception ex_)
    {
      String exStr = RecursiveErrorSearch(ex_);
      DoLog(method_, String.Format("{0}\nException Dump Follows...\n{1}", msg_, exStr), MSLog.Warning());
      SendExceptionEvent(method_, msg_, ex_);
    }

    public void Error(string method_, string message_, params string[] params_)
    {
      Error(method_, string.Format(message_, params_));
    }

    public void ErrorF (string method_, string message_, params object[] params_)
    {
      Error(method_, string.Format(message_, params_));
    }

    public void Error(string method_, Exception ex_, string message_, params object[] params_)
    {
      Error(method_, string.Format(message_, params_), ex_);
    }

    public void Error(string method_, string msg_)
    {
      DoLog(method_, msg_, MSLog.Error());
    }

    public void Error(string method_, string msg_, Exception ex_)
    {
      String exStr = RecursiveErrorSearch(ex_);
      DoLog(method_, String.Format("{0}\nException Dump Follows...\n{1}", msg_, exStr), MSLog.Error());
      SendExceptionEvent(method_, msg_, ex_);
    }

    public void CriticalF(string method_, string message_, params object[] params_)
    {
      Critical(method_, string.Format(message_, params_));
    }

    public void Critical(string method_, string message_, params string[] params_)
    {
      Critical(method_, string.Format(message_, params_));
    }

    public void Critical(string method_, Exception ex_, string message_, params object[] params_)
    {
      Critical(method_, string.Format(message_, params_), ex_);
    }

    public void Critical(string method_, string msg_)
    {
      DoLog(method_, msg_, MSLog.Critical());
    }

    public void Critical(string method_, string msg_, Exception ex_)
    {
      String exStr = RecursiveErrorSearch(ex_);
      DoLog(method_, String.Format("{0}\nException Dump Follows...\n{1}", msg_, exStr), MSLog.Critical());
      SendExceptionEvent(method_, msg_, ex_);
    }

    public void DoLog(string method_, string msg_, MSLogMessage logger_)
    {
      logger_.SetLayer(_layer).Location(_class, method_);
      logger_.Append(msg_);

      //To make sure that logger does not override asigned threading information
      //the set AssignThreadId to false
      //If AssignThreadId is set to true, ThreadId will be asigned to Thread.Name
      //if Thread.Name(Default ThreadPool) is empty we have no way to differentiate
      //between threads in our logs) this is why we are assigning it to false and then 
      //actively assigning ThreadId to a value

      MSLog.AssignThreadId = false;
      if (string.IsNullOrEmpty(Thread.CurrentThread.Name))
      {
        logger_.ThreadId = "Thread Id: " + Thread.CurrentThread.ManagedThreadId;
      }
      else
      {
        logger_.ThreadId = Thread.CurrentThread.Name;
      }

      logger_.ThreadIntId = Thread.CurrentThread.ManagedThreadId;
      logger_.Send();
    }

    private void SendExceptionEvent(string method_, string msg_, Exception ex_)
    {
      if (SendExceptionEvents && ExceptionLogged != null)
      {
        ExceptionLogged(this, new LoggedExceptionEventArgs(method_, msg_, ex_));
      }
    }

    private void Init(Type claz_, String meta_, String proj_, String rel_)
    {
      Init(claz_, new MSLogLayer(meta_, proj_, rel_));
    }

    private void Init(Type claz_, MSLogLayer layer_)
    {
      _class = GetClassNameForLogging(claz_);
      _layer = layer_;
    }

    public static string GetClassNameForLogging(Type type_)
    {
      Type[] genericTypeArgs = null;
      if (type_.IsGenericType)
      {
        genericTypeArgs = type_.GetGenericArguments();
      }

      return GetClassNameForLoggingInternal(type_, genericTypeArgs);
    }

    private static string GetClassNameForLoggingInternal(Type type_, Type[] genericTypeArgs_)
    {
      if (type_ == null || type_.FullName == null)
      {
        // Don't fail on logging, just display the bad type (this should never happen)
        return "[UndefinedType]";
      }

      string fullName = type_.FullName;

      // strips the namespace part at the start, and the "." after the namespace, and 
      // then remove the part after "[[" (the assembly details etc.) if present.
      string name = (!string.IsNullOrEmpty(type_.Namespace)) ? fullName.Substring(type_.Namespace.Length + 1) : fullName;

      int indexOfDoubleBracket = name.IndexOf("[[");
      if (indexOfDoubleBracket >= 0)
      {
        name = name.Substring(0, indexOfDoubleBracket);
      }

      if (type_.IsGenericType)
      {
        name = ExpandGenericParameters(name, genericTypeArgs_);
      }
      // replace the '+' for the nested types by an easier to read '.' character
      name = name.Replace('+', '.');
      return name;
    }

    private static readonly Regex GENERIC_BACKTICK = new Regex(@"(`\d+)");
    private static string ExpandGenericParameters(string shorterName, Type[] genericTypeArgs)
    {
      var sb = new StringBuilder();
      int numTypesSeenSoFar = 0;
      int lastPosition = 0;
      Match match = GENERIC_BACKTICK.Match(shorterName);
      while (match.Success)
      {
        // copy input string upto the match
        sb.Append(shorterName.Substring(lastPosition, match.Index - lastPosition));
        int numParams = int.Parse(match.Value.Substring(1));
        Type[] typeArgs = new Type[numParams];
        Array.Copy(genericTypeArgs, numTypesSeenSoFar, typeArgs, 0, numParams);
        sb.Append("<" + string.Join(",", GetNames(typeArgs)) + ">");

        numTypesSeenSoFar += numParams;
        lastPosition = match.Index + match.Length;
        match = match.NextMatch();
      }

      // copy the final part
      if (lastPosition < shorterName.Length)
      {
        sb.Append(shorterName.Substring(lastPosition));
      }

      return sb.ToString();
    }

    private static string[] GetNames(Type[] types_)
    {
      var a = new string[types_.Length];
      for (int i = 0; i < types_.Length; i++)
      {
        a[i] = types_[i].Name;
      }
      return a;
    }

    /// <summary>
    /// Gets version information from the given class' assembly. Remember to set
    /// the correct version in your AssemblyInfo class!
    /// </summary>
    /// <param name="claz_">class for which the assembly is to be interrogated</param>
    /// <returns>Assembly version string</returns>
    public static String GetVersionString(Type claz_)
    {
      Version version = claz_.Assembly.GetName().Version;
      /*return (version.Build > 20)
        ? String.Format("{0}.{1}", version.Major, version.Minor)
        : String.Format("{0}.{1}.{2}", version.Major, version.Minor, version.Build);
       */
      return String.Format("{0}.{1}.{2}.{3}", version.Major, version.Minor, version.Build, version.Revision);
    }

    /// <summary>
    /// Attempt to guess the MSDE meta and project from the class's namespace. Assumes
    /// the namespace is of the form "MorganStanley.&lt;meta&gt;.&lt;proj&gt;
    /// </summary>
    /// <param name="claz_">class whose namespace is to be used</param>
    /// <param name="meta_">output meta string</param>
    /// <param name="proj_">output proj string</param>
    public static void GetMetaProject(Type claz_, out string meta_, out string proj_)
    {
      if (claz_.Namespace != null)
      {
        String[] elements = claz_.Namespace.Split(new[] { '.' }, 10);
        System.Diagnostics.Debug.Assert(elements.Length > 2 && elements[1] != null && elements[2] != null);
        meta_ = elements[1].ToLower();
        proj_ = elements[2].ToLower();
      }
      else
      {
        // Namespace will be null if claz_ is instantiated by RhinoMock framework
        meta_ = claz_.ToString();
        proj_ = claz_.ToString();
      }
    }

    private static string RecursiveErrorSearch(Exception ex_)
    {
      String msg = String.Format(
        "Exception type: {0}\n" +
        "Message:        {1}\n" +
        "Stack Dump:     \n{2}",
        ex_.GetType().FullName, ex_.Message, ex_.StackTrace);
      Exception inner = ex_.InnerException;
      if (inner != null)
        msg += "Inner Exception Dump Follows...\n" + RecursiveErrorSearch(inner);
      return msg;
    }

    public static bool SendExceptionEvents { get; set; }

    /// <summary>
    /// Initialize MSLog
    /// </summary>
    /// <param name="path_">The directory to put the file in. Pass in null to use the default.</param>
    /// <param name="filename_">The filename to use for the log. Pass in null to use the default.</param>
    /// <param name="appName_">The name of the app creating the log.</param>
    public static void Initialize(string path_, string filename_, string appName_)
    {
      MSLog.Program = new MSLogLayer();
      MSLog.Mode = MSLog.MSLogMode.Async;
      MSLog.SetThresholdPolicy("/", "ecdev/*/*:Debug");

      string logDir =
        string.IsNullOrEmpty(path_)
          ? string.Format(@"{0}\Morgan Stanley\{1}\Log",
                          Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                          appName_)
          : path_;

      var dir = new DirectoryInfo(logDir);
      if (dir.Exists)
      {
        // then clean up log files older than 7 days
        foreach (var file in dir.GetFiles())
        {
          if (file.CreationTime < DateTime.Now.Subtract(new TimeSpan(7, 0, 0, 0)))
          {
            file.Delete();
          }
        }
      }
      else
      {
        dir.Create();
      }

      string logFileName = string.IsNullOrEmpty(filename_)
                             ? string.Format(@"{0}\{1}.{2}.log",
                                             logDir,
                                             DateTime.Now.ToString("yyyy-MM-dd.HH-mm-ss"),
                                             System.Diagnostics.Process.GetCurrentProcess().Id)
                             : string.Format(@"{0}\{1}", logDir, filename_);

      MSLog.AddDestinationToTarget(new MSLogTarget("/"), new MSLogFileDestination("ecdev", logFileName));
      MSLog.Start();
    }
  }
}