﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils
{
  /// <summary>
  /// Simple class to measure and log the time spent in a block of code.
  /// Uses RAII pattern (i.e. <code>IDisposable</code> interface) to bracket the section of code to measure.
  /// </summary>
  public class LoggingTimer : IDisposable
  {
    private readonly ILogger _log;
    private readonly string _prefix;
    private readonly string _operationDescription;

    private readonly Stopwatch _stopwatch;
    private static readonly long Frequency = Stopwatch.Frequency;
    private readonly string _eventId;

    public LoggingTimer(ILogger log, string prefix, string operationDescription)
    {
      _log = log;
      _prefix = prefix;
      _operationDescription = operationDescription;
      _eventId = MakeNewEventId();

      // optional msg when starting the operation
      // FIXME: I use Info level here, because I don't know how to enable Debug level for this class 
      // only ...      
      // _log.Info(LogMethodName, GetFormattedLogEvent("START", ""));

      _stopwatch = new Stopwatch();
      _stopwatch.Start();
    }

    public void Dispose()
    {
      _stopwatch.Stop();
      // _log.Info(LogMethodName, GetFormattedLogEvent("END", ""));

      // calculate elapsed time in milliseconds
      double ms = ((double)this._stopwatch.ElapsedTicks / Frequency) * 1000.0;

      // output message to log
      // FIXME: should guard this with a flag, along with the creation of the Stopwatch instance and calculation of elapsed time...
      // and ability to switch on/off at runtime.
      string msg = GetFormattedLogDurationEvent(ms);
      _log.Info(LogMethodName, msg);
    }

    public static readonly string LogMethodName = "PERF";

    public string GetFormattedLogDurationEvent(double ms)
    {
      string dataStr = string.Format("{0:F3} ms", ms);
      return GetFormattedLogEvent("DURATION", dataStr);
    }

    public string GetFormattedLogEvent(string eventType, string dataStr)
    {
      string msg = string.Format("PERFLOG({0}|{1}|{2}|{3}|{4})", eventType, _eventId, dataStr, _prefix, _operationDescription);
      return msg;
    }


    // unsynchronized, but used in conjunction with a unique thread id, so discrepancies between thread would not be a problem.
    private static int _sEventIdCounter = 1;
    private static string MakeNewEventId()
    {
      _sEventIdCounter++;
      string id = string.Format("{0}-{1}", Thread.CurrentThread.ManagedThreadId, _sEventIdCounter);
      return id;
    }

  } // class LoggingTimer

  public class TimerResult
  {
    private double _duration;

    public double Duration
    {
      get { return _duration; }
      set { _duration = value; }
    }
  }

  public class BlockTimer : IDisposable
  {
    private TimerResult _result;

    private readonly Stopwatch _stopwatch;
    private static readonly long Frequency = Stopwatch.Frequency;

    public BlockTimer(TimerResult result)
    {
      _result = result;

      _stopwatch = new Stopwatch();
      _stopwatch.Start();
    }

    public void Dispose()
    {
      _stopwatch.Stop();

      // calculate elapsed time in milliseconds
      double ms = ((double)this._stopwatch.ElapsedTicks / Frequency) * 1000.0;
      _result.Duration = ms;
    }
  } // BlockTimer

}
