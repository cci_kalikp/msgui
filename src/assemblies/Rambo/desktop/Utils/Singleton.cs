﻿using System.Collections.Generic;
using Microsoft.Practices.Unity;
using MorganStanley.MSDesktop.Rambo.Desktop.Utils.Extensions;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils
{
  public interface IResettableSingleton
  {
    void ClearInstance();
  }

  /// <summary>
  /// Manages all the Singleton references
  /// </summary>
  public static class Singleton
  {
    private static readonly List<IResettableSingleton> _resettableSingletons;
    private static IUnityContainer _container;

    static Singleton()
    {
      _container = new UnityContainer();
      _resettableSingletons = new List<IResettableSingleton>();
    }

    /// <summary>
    /// Container where instances are stored
    /// </summary>
    public static IUnityContainer SingletonContainer
    {
      get { return _container; }
    }

    public static void ClearAllInstances()
    {
      lock (_resettableSingletons)
      {
        _container = new UnityContainer();

        // clear all references
        foreach (IResettableSingleton singleton in _resettableSingletons)
        {
          singleton.ClearInstance();
        }
        _resettableSingletons.Clear();
      }
    }

    /// <summary>
    /// Registers a singleton so it can later be cleared
    /// </summary>
    /// <param name="singletonInstance_"></param>
    public static void RegisterSingleton(IResettableSingleton singletonInstance_)
    {
      lock (_resettableSingletons)
      {
        _resettableSingletons.Add(singletonInstance_);
      }
    }
  }

  /// <summary>
  /// Creates a singlton class. There is no ability to "re-link" the singleton type since an interface isn't specified
  /// </summary>
  /// <typeparam name="T">Type of object</typeparam>
  public abstract class Singleton<T> : IResettableSingleton where T : class, new()
  {
    private static T _instance;

    public static T Instance
    {
      get
      {
        if (_instance == null)
        {
          lock (typeof(T))
          {
            if (_instance == null)
            {
              _instance = new T();
              Singleton.RegisterSingleton((IResettableSingleton)_instance);
              _instance.Log(string.Format("Singleton instance {0} constructed using default ctor", _instance.GetType()));
            }
          }
        }
        return _instance;
      }
    }

    #region IResettableSingleton Members

    public void ClearInstance()
    {
      _instance = null;
    }

    #endregion
  }

  /// <summary>
  /// This class creates a singleton instance of the generic type. The purpose of this is to give the ability to easily "re-link" singleton
  /// classes for the purposes of unit testing
  /// </summary>
  /// <typeparam name="I">The interface</typeparam>
  /// <typeparam name="T">The default instance to link to</typeparam>
  public abstract class Singleton<I, T> : IResettableSingleton
    where I : class
    where T : I, new()
  {
    private static I _interface;

    protected Singleton()
    {
      Log = new Logger(typeof(T));
    }

    public ILogger Log { get; private set; }

    /// <summary>
    /// Returns the instance of the template. If it does not exist, it will create it
    /// </summary>
    public static I Instance
    {
      get
      {
        if (_interface == null)
        {
          lock (typeof(I))
          {
            if (_interface == null)
              InternalReLinkSingleton(new T());
          }
        }

        return _interface;
      }
    }

    #region IResettableSingleton Members

    /// <summary>
    /// Sets the current instance of the Singleton to NULL
    /// </summary>
    public void ClearInstance()
    {
      lock (typeof(I))
      {
        _interface = null;
      }
    }

    #endregion

    /// <summary>
    /// Constructs a Singleton instance using Unity
    /// </summary>
    /// <returns></returns>
    private static I ContructSingletonInstance()
    {
      var instance = Singleton.SingletonContainer.Resolve<I>();
      instance.Log(string.Format("Singleton instance {0} constructed using Unity", instance.GetType()));
      return instance;
    }

    /// <summary>
    /// Links or relinks the current interface to the specified class.
    /// </summary>
    /// <param name="instance_"></param>
    private static void InternalReLinkSingleton(I instance_)
    {
      Singleton.SingletonContainer.RegisterInstance(typeof(I), instance_, new ContainerControlledLifetimeManager());
      _interface = ContructSingletonInstance();
      var resettableSingleton = _interface as IResettableSingleton;
      if (resettableSingleton != null)
        Singleton.RegisterSingleton(resettableSingleton);
    }

    /// <summary>
    /// Relinks the current singleton to the specified class. This should be used while unit testing.
    /// </summary>
    /// <param name="instance_">The instance</param>
    public static void ReLinkSingleton(I instance_)
    {
      lock (typeof(I))
      {
        // before you relink, there must be a default instance
        if (_interface == null)
          Instance.ToString();
        InternalReLinkSingleton(instance_);
      }
    }
  }
}