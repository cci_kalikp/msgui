﻿using System;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.AsyncResource
{
  public interface IAsyncResource
  {
    bool WaitForLoading(TimeSpan timeout_);
  }
}
