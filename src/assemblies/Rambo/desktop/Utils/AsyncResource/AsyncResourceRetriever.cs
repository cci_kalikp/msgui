﻿using System;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.AsyncResource
{
  public class AsyncResourceRetriever<T> where T : IAsyncResource
  {
    private readonly T _provider;

    public AsyncResourceRetriever(T provider_)
    {
      _provider = provider_;
    }

    public T GetResource(TimeSpan timeout_)
    {
      bool isLoaded = _provider.WaitForLoading(timeout_);
      if (!isLoaded)
      {
        throw new Exception("Cannot get resource. type: " + typeof(T));
      }
      return _provider;
    }
  }
}
