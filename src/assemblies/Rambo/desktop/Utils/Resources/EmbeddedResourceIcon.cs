using System;
using System.IO;
using System.Drawing;
using System.Reflection;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Resources
{
  /// <summary>
  /// Utility class for embedded icons.
  /// </summary>
  public class EmbeddedResourceIcon
  {
    private EmbeddedResourceIcon() {}

    /// <summary>
    /// Get an Icon that is an embedded resource of a specified assembly.
    /// </summary>
    /// <param name="assembly_">The Assembly the Icon is embedded in.</param>
    /// <param name="fileName_">The name of the Icon file.  Note that this should be a fully qualified name, such as "MorganStanley.MSDesktop.Rambo.YourProject.Subfolder.YourIcon.ico".</param>
    /// <param name="width_">The width of the icon in pixels.</param>
    /// <param name="height_">The height of the icon in pixels.</param>
    /// <returns>An icon, if the filename could not be found then an icon with a red X.</returns>
    public static Icon GetIcon (Assembly assembly_, string fileName_, int width_, int height_)
    {
      System.IO.Stream strm = null; 
      try  
      { 
        strm = assembly_.GetManifestResourceStream (fileName_); 
        Icon icon = new Icon (strm, width_, height_);
        return icon;
      } 
      catch (Exception) 
      { 
        strm = typeof(EmbeddedResourceIcon).Assembly.GetManifestResourceStream (string.Format ("{0}.NotFound.ico", typeof(EmbeddedResourceIcon).Namespace));
        Icon notFoundIcon = new Icon (strm, width_, height_);
        return notFoundIcon;
      } 
      finally
      { 
        if (strm != null)
        {
          strm.Close(); 
        }
      }
    }
  }
}