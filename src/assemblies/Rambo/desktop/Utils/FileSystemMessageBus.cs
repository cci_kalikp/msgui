using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

/**
 * The FileSystemMessageBus uses a directory on the U drive to pass messages back and forth between classes.
 * This is intended for when the publisher and the subscriber live on different PCs.
 **/
namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils
{
    public class FileSystemMessageBusConfiguration
    {
        private string _basePath;

        private ILogger _log = new Logger(typeof(FileSystemMessageBusConfiguration));

        private static string STANDARD_BASEPATH = @"u:\.fsmb";
        public static FileSystemMessageBusConfiguration Instance = new FileSystemMessageBusConfiguration();

        private FileSystemMessageBusConfiguration() : this(STANDARD_BASEPATH) {}

        private FileSystemMessageBusConfiguration(string basePath_) // might want to make this configurable one day, but not today
        {
            _basePath = basePath_;

            try
            {
                Directory.CreateDirectory(_basePath);
                new DirectoryInfo (_basePath).Attributes = FileAttributes.Hidden;
            }
            catch (Exception ex)
            {
                _log.Error("FileSystemMessageBusConfiguration()", "Could not create directory '" + _basePath + "'", ex);
            }

            // clean up old files
            string[] existingFiles = Directory.GetFiles(_basePath);
            foreach (string existingFile in existingFiles)
            {
                TimeSpan age = DateTime.Now.Subtract(File.GetCreationTime(existingFile));
                if (age.Minutes > 1)
                {
                    try { File.Delete(existingFile); }
                    catch (Exception ex)
                    {
                        _log.Error("FileSystemMessageBusConfiguration()", "Could not delete file '" + existingFile + "'", ex);
                    }
                }
            }
        }

        public string BasePath
        {
            get { return _basePath; }
        }
    }

    public class FileSystemMessageBusWriter
    {
        private string _processId;
        private bool _shouldPublish = true;

        private ILogger _log = new Logger(typeof(FileSystemMessageBusWriter));

        /// <summary>
        /// Create a new FileSystemMessageBusWriter.
        /// </summary>
        public FileSystemMessageBusWriter()
        {
            _processId = System.Diagnostics.Process.GetCurrentProcess().Id.ToString();
        }

        /// <summary>
        /// Write a message to the file system bus.
        /// </summary>
        /// <param name="topicName_">The name of the topic to publish.  The reader uses this to subscribe to messages.</param>
        /// <param name="message_">The message to publish.</param>
        public void Write(string topicName_, string message_)
        {
            if (!_shouldPublish)
            {
                return;
            }

            string fileName = FileSystemMessageBusConfiguration.Instance.BasePath + Path.DirectorySeparatorChar + topicName_ + "." + _processId + "_" + DateTime.Now.Ticks + ".msg";
            try
            {
                File.WriteAllText (fileName, message_);
            }
            catch (Exception ex)
            {
                _log.Error("Write", "Could not write message file '" + fileName + "'", ex);
            }
        }

        /// <summary>
        /// Flag to turn off/on publishing.  By default it is on.
        /// </summary>
        public bool ShouldPublish
        {
            get { return _shouldPublish; }
            set { _shouldPublish = value; }
        }
    }

    public class FileSystemMessageBusReader
    {
        private string _topicName;
        private FileSystemWatcher _fsw;

        private ILogger _log = new Logger(typeof(FileSystemMessageBusReader));

        /// <summary>
        /// When a FileSystemMessageBusWriter writes to the topic passed into the constructor of this FileSystemMessageBusReader,
        /// this event will be fired, containing the message.
        /// </summary>
        public event FileSystemMessageBusEventHandler FileSystemMessageBusEvent;

        /// <summary>
        /// Create a new FileSystemMessageBusReader.
        /// </summary>
        /// <param name="topicName_">The topic to subscribe to.</param>
        public FileSystemMessageBusReader(string topicName_)
        {
            _topicName = topicName_;
            _fsw = new FileSystemWatcher (FileSystemMessageBusConfiguration.Instance.BasePath, topicName_ + ".*.msg");
            _fsw.EnableRaisingEvents = true;
            _fsw.Created += new FileSystemEventHandler(_file_Created);
        }

        private void _file_Created(object sender, FileSystemEventArgs e)
        {
            StreamReader sr = null;
            try
            {
                sr = File.OpenText(e.FullPath);
                string message = sr.ReadToEnd();
                if (FileSystemMessageBusEvent != null)
                {
                    FileSystemMessageBusEvent(this, new FileSystemMessageBusEventArgs(_topicName, message));
                }
            }
            catch (Exception ex_)
            {
                _log.Error("_file_Created", "Could not read message file '" + e.FullPath + "'", ex_);
            }
            finally
            {
                sr.Close();
            }
        }

        /// <summary>
        /// Flag to turn on/off listening.  By default it is off.
        /// </summary>
        public bool ShouldListen
        {
            get { return _fsw.EnableRaisingEvents; }
            set { _fsw.EnableRaisingEvents = value; }
        }
    }

    public delegate void FileSystemMessageBusEventHandler (object sender_, FileSystemMessageBusEventArgs e_);

    public class FileSystemMessageBusEventArgs : EventArgs
    {
        private string _topicName;
        private string _message;

        public FileSystemMessageBusEventArgs(string topicName_, string message_)
        {
            _topicName = topicName_;
            _message = message_;
        }

        /// <summary>
        /// The topic that was published to.
        /// </summary>
        public string TopicName
        {
            get { return _topicName; }
        }

        /// <summary>
        /// The message that was published on the topic.
        /// </summary>
        public string Message
        {
            get { return _message; }
        }
    }
}
