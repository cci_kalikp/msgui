using System;
using System.IO;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Paths
{
	/// <summary>
	/// Class to hold and try to parse an AFS path.
	/// </summary>
	public class AFSPath
	{
		private string _path;

		private AFSType _afsType = AFSType.NOT_AFS;
		private string _meta = null;
		private string _project = null;
		private string _release = null;

    private string _resolvedRelease = null;

    private Exception _resolvedReleaseException = null;

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="path_">Path that you wish to attempt to parse into an AFS path.</param>
		public AFSPath (string path_)
		{
			_path = path_;

			string tweakedPath = path_.Replace (@"\\", @"\");

			string[] tokens = tweakedPath.Split (new char[]{Path.DirectorySeparatorChar});

			if (path_.ToLower().StartsWith (@"\\ms\dist"))
			{
				_afsType = AFSType.DIST;
				_meta =    tokens.Length > 3 ? tokens[3] : null;
				_project = tokens.Length > 5 ? tokens[5] : null;
				_release = tokens.Length > 6 ? tokens[6] : null;
			}
			else if (path_.ToLower().StartsWith (@"\\ms\dev"))
			{
				_afsType = AFSType.DEV;
				_meta =    tokens.Length > 3 ? tokens[3] : null;
				_project = tokens.Length > 4 ? tokens[4] : null;
				_release = tokens.Length > 5 ? tokens[5] : null;
			}
			else if (path_.ToLower().StartsWith (@"\\ms\user"))
			{
				_afsType = AFSType.USER;
			}
			else if (path_.ToLower().StartsWith (@"\\ms\group"))
			{
				_afsType = AFSType.GROUP;
			}
			else if (path_.ToLower().Equals (@"\\ms") || path_.ToLower().Equals (@"\\ms\"))
			{
				_afsType = AFSType.ALL;
			}
		}

		/// <summary>
		/// The path that was given to this object in its constructor.
		/// </summary>
		public string InputPath
		{
			get { return _path; }
		}

		/// <summary>
		/// Whether or not this is a valid AFS path.
		/// </summary>
		public bool IsAFS
		{
			get { return _afsType != AFSType.NOT_AFS; }
		}

		/// <summary>
		/// Whether or not this is a fully fledged VMS release in DEV or DIST.
		/// </summary>
		public bool IsVMSRelease
		{
			get
			{
				if (_afsType == AFSType.DEV || _afsType == AFSType.DIST)
				{
					if (_meta != null && _project != null && _release != null)
					{
						return true;
					}
				}
	
				return false;
			}
		}

		/// <summary>
		/// The VMS metaproject (if present, otherwise null).
		/// </summary>
		public string Meta
		{
			get { return _meta; }
		}

		/// <summary>
		/// The VMS project (if present, otherwise null).
		/// </summary>
		public string Project
		{
			get { return _project; }
		}

		/// <summary>
		/// The VMS release (if present, otherwise null).
		/// </summary>
		public string Release
		{
			get { return _release; }
		}

    public string ResolvedRelease
    {
      get
      {
        if (_resolvedRelease == null && _resolvedReleaseException == null)
        {
          _resolvedRelease = Release;

          try
          {
            if (IsVMSRelease)
            {
              string pointsTo = Web.WebPage.Get (string.Format ("http://groupweb.ms.com/rps/cgi-bin/traderdesktop/releaselinkresolver.cgi?meta={0}&proj={1}&rel={2}", Meta, Project, Release), 10000);
              if (pointsTo.Length > 0)
              {
                _resolvedRelease = pointsTo;
              }
            }
          }
          catch (Exception ex)
          {
            _resolvedReleaseException = ex;
            _resolvedRelease = null;
          }
        }

        return _resolvedRelease;
      }
    }

    public bool ResolvedReleaseOK
    {
      get { return _resolvedRelease != null && _resolvedReleaseException == null; }
    }

    public Exception ResolvedReleaseException
    {
      get { return _resolvedReleaseException; }
    }

      /// <summary>
      /// Get the AFS type (DEV, DIST, GROUP, USER)
      /// </summary>
      public AFSType AFSType
    {
      get { return _afsType; }
    }

		/// <summary>
		/// Get a string representation of this AFSPath object.
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			return string.Format ("{0} [{1} {2}/{3}/{4}]", _path, _afsType, _meta, _project, _release);
		}
	}

	/// <summary>
	/// Types of AFS.
	/// </summary>
	public enum AFSType
	{
		ALL, DEV, DIST, USER, GROUP, NOT_AFS
	}
}
