using System.Reflection;
using System.Runtime.CompilerServices;

//
// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
//
[assembly: AssemblyProduct("Ecdev.Desktop.Utils")]
[assembly: AssemblyTitle("Ecdev Desktop Utilities")]
[assembly: AssemblyDescription("General Utilities for Desktop apps")]
[assembly: AssemblyCompany("Morgan Stanley")]
[assembly: AssemblyCopyright("Copyright (c) Morgan Stanley, Inc., All Rights Reserved")]
[assembly: AssemblyCulture("")]		

//
// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:

[assembly: AssemblyVersion("36.1.0")]



