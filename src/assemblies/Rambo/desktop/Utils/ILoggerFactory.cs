///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2004 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
///////////////////////////////////////////////////////////////////////////////
//
//       Filename:  $RCSfile: ILoggerFactory.cs,v $ 
//       Revision:  $Revision: #3 $ 
// Last edited by:  $Author: boqwang $ 
//             on:  $Date: 2013/02/28 $ 
//
// $Id: //eai/msdotnet/msgui/trunk/assemblies/Rambo/desktop/Utils/ILoggerFactory.cs#3 $ 
//
///////////////////////////////////////////////////////////////////////////////
//
// $Log: ILoggerFactory.cs,v $
// Revision 1.2  2004/02/27 10:57:39  perrys
// factory returns ILogger
//
//
///////////////////////////////////////////////////////////////////////////////

using System;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils
{
  /// <summary>
  /// Defines the actual factory that will producr Logger instances.
  /// </summary>
  public interface ILoggerFactory
  {
    /// <summary>
    /// Obtain a Logger for the given class.
    /// </summary>
    /// <param name="class_"></param>
    /// <returns></returns>
    ILogger GetLogger( Type class_ ) ;
  }
}
