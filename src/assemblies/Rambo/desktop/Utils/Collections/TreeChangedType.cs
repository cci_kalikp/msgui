namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections
{
  public enum TreeChangedType
  {
    NodeAdded,
    NodeRemoved,
    NodeChanged,
    NodeListCleared
  }
}