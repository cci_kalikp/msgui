using System;
using System.Collections;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections
{
  /// <summary>
  /// Wraps a list in a read-only collection.
  /// </summary>
  public class ReadOnlyListAdapter : IList
  {
    private IList _list;

    public ReadOnlyListAdapter(IList list_)
    {
      _list = list_;
    }

    public int Count {get {return _list.Count; }}
    public bool IsFixedSize {get {return true; }}
    public bool IsReadOnly {get {return true; }}
    public bool IsSynchronized {get {return false;}}
    public object SyncRoot {get {return this;}}

    public bool Contains(object value_) {return _list.Contains(value_);}
    public void CopyTo( Array array_, int idx_) {_list.CopyTo(array_, idx_);}
    public IEnumerator GetEnumerator() {return _list.GetEnumerator();}
    public int IndexOf(object value_) {return _list.IndexOf(value_);}
    public virtual object this[int idx_] 
    {
      get {return _list[idx_];}
      set {throw new NotSupportedException();}
    }

    public int Add(object o) {throw new NotSupportedException();}
    public void Clear() {throw new NotSupportedException();}
    public void Insert(int i, object o) {throw new NotSupportedException();}
    public void Remove(object v_) {throw new NotSupportedException();}
    public void RemoveAt(int idx_) {throw new NotSupportedException();}

  } 
}
