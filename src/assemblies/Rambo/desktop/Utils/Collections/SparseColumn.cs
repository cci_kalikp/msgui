using System;
using System.Collections;
using System.Collections.Generic;
using MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections.TypedLists;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections
{
  internal class SparseColumn<T, R> : SparseColumn<R>
    where R : SparseRow
  {
    #region Fields
    protected IList _storage;
    protected object _defaultValue;
    #endregion

    #region Constructor
    public SparseColumn(string name_) 
      :this(name_, DataHint.NONE)
    {

    }

    public SparseColumn(string name_, DataHint hint_)
      : base(name_, typeof(T))
    {
      if (Array.IndexOf(SupportedTypes, DataType) < 0)
      {
        throw new NotSupportedException(string.Format("The type {0} is not supported.", DataType));
      }
      InitializeStorage(new []{hint_}, typeof(T), -1);

    }
    #endregion

    #region Public members
    public override object this[int rowIdx_]
    {
      get
      {
        if (IsStorageInitialized)
        {
          object value = _storage[rowIdx_];

          if (value != _defaultValue)
          {
            return value;
          }
        }

        return DBNull.Value;
      }
      set 
      { 
        _storage[rowIdx_] = value; 
      }
    }

    public override void Clear()
    {
      if (IsStorageInitialized)
      {
        _storage.Clear();
      }
    }

    public override IList CopyData()
    {
      if (!IsStorageInitialized)
      {
        return new ArrayList();
      }

      return new ArrayList(_storage);
    }
    #endregion

    #region Internal Members

    protected internal override void Add()
    {
      _storage.Add(DefaultValue);
    }

    protected internal override void RemoveAt(int idx_)
    {
      if (IsStorageInitialized)
      {
        _storage[idx_] = _defaultValue;
      }
    }

    public object DefaultValue
    {
      get
      {
        if (_defaultValue == null)
        {
          if (DataType.IsValueType)
          {
            if (DataType == typeof (Boolean))
              _defaultValue = false;
            else if (DataType == typeof (Double))
              _defaultValue = DoubleList.NULL_VALUE;
            else if (DataType == typeof (Single))
              _defaultValue = SingleList.NULL_VALUE;
            else if (DataType == typeof (Int32))
              _defaultValue = Int32List.NULL_VALUE;
            else if (DataType == typeof (DateTime))
              _defaultValue = DateTime.MinValue;
            else
            {
              System.Diagnostics.Debug.Fail("Unrecognized value type: " + DataType.FullName);
              _defaultValue = 0;
            }
          }
        }

        return _defaultValue;
      }
    }

    #endregion

    #region Private Members
    private bool IsStorageInitialized
    {
      get { return _storage != null; }
    }

    /// <summary>
    /// Create a concrete list instance to efficiently store data given the data type, 
    /// characteristics and size hints.
    /// </summary>
    /// <param name="hints_">expected data characteristics</param>
    /// <param name="dataType_">type of data to be stored</param>
    /// <param name="capacity_">hint on the number of elements to be stored.</param>
    /// <returns>A new IList instance.</returns>
    protected void InitializeStorage(DataHint[] hints_, Type dataType_, int capacity_)
    {
      /* if we have a primitive datatype, use a primitive list.
       * Otherwise, use an ArrayList, and wrap it with an interned list
       * if the data is repetitive.
       */

      if (hints_ == null)
        hints_ = new DataHint[0];
      bool isSparse = Array.IndexOf(hints_, DataHint.SPARSE) >= 0;
      bool isRepetitive = Array.IndexOf(hints_, DataHint.REPETITIVE) >= 0;

      _storage = isSparse ? GetSparseStorage(dataType_) : GetNonSparseStorage(isRepetitive, dataType_, capacity_);

    }

    private static IList GetNonSparseStorage(bool isRepetitive_, Type dataType_, int capacity_)
    {
      IList list;
    
      bool ignoreCapacity = capacity_ <= 0 || isRepetitive_;
      if (dataType_ == typeof (double))
      {

        list = ignoreCapacity
                 ? new DoubleList(DoubleList.NULL_VALUE)
                 : new DoubleList(DoubleList.NULL_VALUE, capacity_);
      }
      else if (dataType_ == typeof (int))
      {
        list = ignoreCapacity
                 ? new Int32List()
                 : new Int32List(capacity_);
      }
      else if (dataType_ == typeof (float))
      {
        list = ignoreCapacity
                 ? new SingleList()
                 : new SingleList(capacity_);
      }
      else if (dataType_ == typeof (DateTime))
      {
        list = ignoreCapacity
                 ? new DateTimeList()
                 : new DateTimeList(capacity_);
      }
      else
      {
        list = ignoreCapacity
                 ? new StringList()
                 : new StringList(capacity_);
        if (isRepetitive_)
          list = new InternedList(list);
      }
      return list;
    }

    private  IList GetSparseStorage(Type dataType_)
    {
      Type t = typeof (DictionarySparseList<>);
      t = t.MakeGenericType(dataType_);
      var list = (IList) Activator.CreateInstance(t, DefaultValue);
      return list;
    }

    #endregion
  }

  public interface IStorageAllocatable
  {
    bool IsAllocated { get; }
  }
}