using System;
using System.Collections;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections
{
  public interface IDataColumn
  {
    string ColumnName { get; }

    Type DataType { get; }

    int Ordinal { get; }

    object this[int rowIdx_] { get; set; }

    IList CopyData( );

    /// <summary>
    /// Clears the content of this column
    /// </summary>
    void Clear();
  }
}