using System;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections
{
  public class TreeChangedEventArgs : EventArgs
  {
    public readonly TreeChangedType TreeChangedType;
    public readonly ITreeNode Node;
    public readonly ITreeNodeList NodeList;
    public readonly int Index;

    public TreeChangedEventArgs(TreeChangedType treeChangedType, ITreeNodeList nodeList)
      : this(treeChangedType, null, nodeList, -1)
    {
    }

    public TreeChangedEventArgs(TreeChangedType treeChangedType, ITreeNode node, ITreeNodeList nodeList, int index)
    {
      TreeChangedType = treeChangedType;
      Node = node;
      NodeList = nodeList;
      Index = index;
    }
  }
}