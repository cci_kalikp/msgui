﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections
{
  ///<summary>
  /// This is an interface that provides random access on the collection
  ///</summary>
  ///<typeparam name="TIndex"></typeparam>
  ///<typeparam name="TValue"></typeparam>
  public interface IIndexableCollection<TIndex, TValue> : IEnumerable<TValue>
  {
    ///<summary>
    /// Returns the value of collection at index_
    ///</summary>
    ///<param name="index_"></param>
    TValue this[TIndex index_] { get; }
  }
}
