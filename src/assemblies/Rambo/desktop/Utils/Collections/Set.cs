using System.Collections.Generic;
using MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections.PreRelease;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections
{
  /// <summary>
  /// A collection of distinct objects.
  /// </summary>
  /// <typeparam name="T">
  /// The type of object contained as items in the collection.
  /// </typeparam>
  public class Set<T> : SynchronizedKeyedCollection<T, T>, ISet<T>
  {
    #region Defaults
    private static readonly int DEFAULT_CAPACITY = 3;
    private static readonly IEqualityComparer<T> DEFAULT_COMPARER = EqualityComparer<T>.Default;
    #endregion

    #region Constructors
    /// <summary>
    /// Initializes a new instance of the <see cref="Set{T}"></see> class that is empty, has the default initial capacity, and uses the default equality comparer for comparison.
    /// </summary>
    public Set()
      : this(DEFAULT_CAPACITY)
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="Set{T}"/> class.
    /// </summary>
    /// <param name="capacity">The initial number of elements that the <see cref="Set{T}"/> can contain.</param>
    public Set(int capacity)
      : this(null, capacity, DEFAULT_COMPARER)
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="Set{T}"></see> class that contains elements copied from the specified <see cref="T:System.Collections.Generic.IEnumerable`1"></see> and uses the default equality comparer for comparison.
    /// </summary>
    /// <param name="collection">
    /// The <see cref="T:System.Collections.Generic.IEnumerable`1"></see> whose distinct elements are copied to the new <see cref="Set{T}"></see>.
    /// </param>
    public Set(IEnumerable<T> collection)
      : this(collection, DEFAULT_COMPARER)
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="Set{T}"></see> class that is empty, has the default initial capacity, and uses the specified equality comparer for comparison.
    /// </summary>
    /// <param name="comparer">
    /// The <see cref="T:System.Collections.Generic.IEqualityComparer`1"></see> implementation to use when comparing keys, or a null reference to use the default <see cref="T:System.Collections.Generic.EqualityComparer`1"></see> for the type of the key.
    /// </param>
    public Set(IEqualityComparer<T> comparer)
      : this(DEFAULT_CAPACITY, comparer)
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="Set{T}"></see> class that is empty, has the specified initial capacity, and uses the specified equality comparer for comparison.
    /// </summary>
    /// <param name="capacity">
    /// The initial number of elements that the <see cref="Set{T}"></see> cl can contain.
    /// </param>
    /// <param name="comparer">
    /// The <see cref="T:System.Collections.Generic.IEqualityComparer`1"></see> implementation to use when comparing keys, or a null reference to use the default <see cref="T:System.Collections.Generic.EqualityComparer`1"></see> for the type of the key.
    /// </param>
    public Set(int capacity, IEqualityComparer<T> comparer)
      : this(null, capacity, comparer)
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="Set{T}"></see> class that contains elements copied from the specified <see cref="T:System.Collections.Generic.IEnumerable`1"></see> and uses the specified equality comparer for comparison.
    /// </summary>
    /// <param name="collection">
    /// The <see cref="T:System.Collections.Generic.IEnumerable`1"></see> whose distinct elements are copied to the new <see cref="Set{T}"></see>.
    /// </param>
    /// <param name="comparer">
    /// The <see cref="T:System.Collections.Generic.IEqualityComparer`1"></see> implementation to use when comparing keys, or a null reference to use the default <see cref="T:System.Collections.Generic.EqualityComparer`1"></see> for the type of the key.
    /// </param>
    public Set(IEnumerable<T> collection, IEqualityComparer<T> comparer)
      : this(collection, DEFAULT_CAPACITY, comparer)
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="Set{T}"></see> class that is empty, has the specified initial capacity, and uses the specified equality comparer for comparison.
    /// </summary>
    /// <param name="collection">
    /// The <see cref="T:System.Collections.Generic.IEnumerable`1"></see> whose distinct elements are copied to the new <see cref="Set{T}"></see>.
    /// </param>
    /// <param name="capacity">
    /// The initial number of elements that the <see cref="Set{T}"></see> cl can contain.
    /// </param>
    /// <param name="comparer">
    /// The <see cref="T:System.Collections.Generic.IEqualityComparer`1"></see> implementation to use when comparing keys, or a null reference to use the default <see cref="T:System.Collections.Generic.EqualityComparer`1"></see> for the type of the key.
    /// </param>
    public Set(IEnumerable<T> collection, int capacity, IEqualityComparer<T> comparer)
      : this(new object(), collection, capacity, comparer)
    {
    }


    /// <summary>
    /// Initializes a new instance of the <see cref="Set{T}"></see> class that is empty, has the default initial capacity, and uses the default equality comparer for comparison.
    /// </summary>
    /// <param name="syncroot">
    /// The object used to synchronize access to the thread-safe collection.
    /// </param>
    public Set(object syncroot)
      : this(syncroot, DEFAULT_CAPACITY)
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="Set{T}"></see> class that is empty, has the specified initial capacity, and uses the default equality comparer for comparison.
    /// </summary>
    /// <param name="syncroot">
    /// The object used to synchronize access to the thread-safe collection.
    /// </param>
    /// <param name="capacity">
    /// The initial number of elements that the <see cref="Set{T}"></see> cl can contain.
    /// </param>
    public Set(object syncroot, int capacity)
      : this(syncroot, null, capacity, DEFAULT_COMPARER)
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="Set{T}"></see> class that contains elements copied from the specified <see cref="T:System.Collections.Generic.IEnumerable`1"></see> and uses the default equality comparer for comparison.
    /// </summary>
    /// <param name="syncroot">
    /// The object used to synchronize access to the thread-safe collection.
    /// </param>
    /// <param name="collection">
    /// The <see cref="T:System.Collections.Generic.IEnumerable`1"></see> whose distinct elements are copied to the new <see cref="Set{T}"></see>.
    /// </param>
    public Set(object syncroot, IEnumerable<T> collection)
      : this(syncroot, collection, DEFAULT_COMPARER)
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="Set{T}"></see> class that is empty, has the default initial capacity, and uses the specified equality comparer for comparison.
    /// </summary>
    /// <param name="syncroot">
    /// The object used to synchronize access to the thread-safe collection.
    /// </param>
    /// <param name="comparer">
    /// The <see cref="T:System.Collections.Generic.IEqualityComparer`1"></see> implementation to use when comparing keys, or a null reference to use the default <see cref="T:System.Collections.Generic.EqualityComparer`1"></see> for the type of the key.
    /// </param>
    public Set(object syncroot, IEqualityComparer<T> comparer)
      : this(syncroot, DEFAULT_CAPACITY, comparer)
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="Set{T}"></see> class that is empty, has the specified initial capacity, and uses the specified equality comparer for comparison.
    /// </summary>
    /// <param name="syncroot">
    /// The object used to synchronize access to the thread-safe collection.
    /// </param>
    /// <param name="capacity">
    /// The initial number of elements that the <see cref="Set{T}"></see> cl can contain.
    /// </param>
    /// <param name="comparer">
    /// The <see cref="T:System.Collections.Generic.IEqualityComparer`1"></see> implementation to use when comparing keys, or a null reference to use the default <see cref="T:System.Collections.Generic.EqualityComparer`1"></see> for the type of the key.
    /// </param>
    public Set(object syncroot, int capacity, IEqualityComparer<T> comparer)
      : this(syncroot, null, capacity, comparer)
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="Set{T}"></see> class that contains elements copied from the specified <see cref="T:System.Collections.Generic.IEnumerable`1"></see> and uses the specified equality comparer for comparison.
    /// </summary>
    /// <param name="syncroot">
    /// The object used to synchronize access to the thread-safe collection.
    /// </param>
    /// <param name="collection">
    /// The <see cref="T:System.Collections.Generic.IEnumerable`1"></see> whose distinct elements are copied to the new <see cref="Set{T}"></see>.
    /// </param>
    /// <param name="comparer">
    /// The <see cref="T:System.Collections.Generic.IEqualityComparer`1"></see> implementation to use when comparing keys, or a null reference to use the default <see cref="T:System.Collections.Generic.EqualityComparer`1"></see> for the type of the key.
    /// </param>
    public Set(object syncroot, IEnumerable<T> collection, IEqualityComparer<T> comparer)
      : this(syncroot, collection, DEFAULT_CAPACITY, comparer)
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="Set{T}"></see> class that is empty, has the specified initial capacity, and uses the specified equality comparer for comparison.
    /// </summary>
    /// <param name="syncroot">
    /// The object used to synchronize access to the thread-safe collection.
    /// </param>
    /// <param name="collection">
    /// The <see cref="T:System.Collections.Generic.IEnumerable`1"></see> whose distinct elements are copied to the new <see cref="Set{T}"></see>.
    /// </param>
    /// <param name="capacity">
    /// The initial number of elements that the <see cref="Set{T}"></see> cl can contain.
    /// </param>
    /// <param name="comparer">
    /// The <see cref="T:System.Collections.Generic.IEqualityComparer`1"></see> implementation to use when comparing keys, or a null reference to use the default <see cref="T:System.Collections.Generic.EqualityComparer`1"></see> for the type of the key.
    /// </param>
    public Set(object syncroot, IEnumerable<T> collection, int capacity, IEqualityComparer<T> comparer)
      : base(syncroot, comparer, capacity)
    {
      if (collection == null)
        return;

      // we do this explicitly so we can inherit this class to make a read-only version
      // other-wise AddRange would throw a NotSupportedException
      foreach (T t in collection)
      {
        if (Dictionary.ContainsKey(GetKeyForItem(t)))
          continue;

        base.InsertItem(Count, t);
      }
    }


    #endregion

    #region Public Methods

    public virtual ISet<T> Union(ISet<T> set_)
    {
      lock (SyncRoot)
      {
        ISet<T> newSet = new Set<T>(this);

        newSet.AddRange(set_);

        return newSet;
      }
    }

    public virtual ISet<T> Intersection(ISet<T> set_)
    {
      lock (SyncRoot)
      {
        ISet<T> newSet = new Set<T>();

        // Iterate over shorter list
        if (Count < ((ICollection<T>)set_).Count)
        {
          foreach (T item in this)
          {
            if (set_.Contains(item))
            {
              newSet.Add(item);
            }
          }
        }
        else
        {
          foreach (T item in set_)
          {
            if (Contains(item))
            {
              newSet.Add(item);
            }
          }
        }

        return newSet;
      }
    }


    public virtual ISet<T> Difference(ISet<T> set_)
    {
      lock (SyncRoot)
      {
        ISet<T> newSet = new Set<T>(this);

        foreach (T item in set_)
        {
          // if the item exists in set, remove it, otherwise add it.
          if (!newSet.Remove(item))
          {
            newSet.Add(item);
          }
        }

        return newSet;
      }
    }


    public virtual ISet<T> Subtract(ISet<T> set_)
    {
      lock (SyncRoot)
      {
        ISet<T> newSet = new Set<T>(this);

        foreach (T item in set_)
        {
          // if the item exists in set, remove it.
          newSet.Remove(item);
        }

        return newSet;
      }
    }

    public virtual ISet<Pair<T, S>> CartesianProduct<S>(ISet<S> set_)
    {
      lock (SyncRoot)
      {
        Set<Pair<T, S>> pairs = new Set<Pair<T, S>>();

        foreach (T t in this)
        {
          foreach (S s in set_)
          {
            Pair<T, S> pair = new Pair<T, S>(t, s);

            pairs.Add(pair);
          }
        }

        return pairs;
      }
    }

    #region ISet<T> Members
    /// <summary>
    /// Determines whether this set is a subset of the specified set.
    /// </summary>
    /// <param name="set_">The set.</param>
    /// <returns>
    /// 	<c>true</c> if this set is a subset of the specified set; otherwise, <c>false</c>.
    /// </returns>
    public bool IsSubsetOf(ISet<T> set_)
    {
      foreach(T t in this)
      {
        if(!set_.Contains(t))
        {
          return false;
        }
      }

      return true;
    }
    #endregion

    public void AddRange(IEnumerable<T> collection)
    {
      lock (SyncRoot)
      {
        foreach (T item in collection)
        {
          Add(item);
        }
      }
    }

    #endregion

    public override string ToString()
    {
      return string.Format("Count={0}", Count);
    }

    #region Protected Methods
    protected override T GetKeyForItem(T item)
    {
      return item;
    }

    protected override void InsertItem(int index, T item)
    {
      if (!Contains(item))
      {
        base.InsertItem(index, item);
      }
    }
    #endregion

  }
}