using System.Collections.Generic;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections
{
  public interface IDataRowCollection :  IIndexableCollection<int, IDataRow>
  {
    int Count { get; }
    int AllocationCount { get; }

    IDataRow this[int idx_] { get; }

    IDataRow Add( );

    void Clear( );
    
    void Remove(IEnumerable<IDataRow> rows_);
    void Remove(IDataRow row_);
    void RemoveAt(int idx_);
  }

  public interface IDataRowCollection<R> : IDataRowCollection
  {
    new R this[int idx_] { get; }
    new R Add( );

    void Remove(R row_);
    void Remove(IEnumerable<R> rows_);
  }
}