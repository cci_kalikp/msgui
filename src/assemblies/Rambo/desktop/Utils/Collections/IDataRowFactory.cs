using MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections
{
  public interface IDataRowFactory
  {
    IDataRow Create(IDataTable table_, int rowIdx_);
  }

  public interface IDataRowFactory<T> : IDataRowFactory  
    where T : IDataRow
  {
    new T Create(IDataTable table_, int rowIdx_);
  }
}