using System.Collections.Generic;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections.PreRelease
{
  /// <summary>
  /// Provides a thread-safe collection that contains objects of a type specified by a generic parameter and that are grouped by keys.
  /// </summary>
  /// <typeparam name="K"></typeparam>
  /// <typeparam name="T"></typeparam>
  public abstract class SynchronizedKeyedCollection<K, T>
    : SynchronizedCollection<T>
  {
    #region Data Members
    private readonly Dictionary<K, T> _dictionary;
    #endregion

    #region Protected Constructors
    /// <summary>
    /// Initializes a new instance of the SynchronizedKeyedCollection class.
    /// </summary>
    protected SynchronizedKeyedCollection()
      : this(new object())
    {
    }

    /// <summary>
    /// Initializes a new instance of the SynchronizedKeyedCollection class with access synchronized by an explicitly specified object.
    /// </summary>
    /// <param name="syncRoot">
    /// The object used to synchronize access to the thread-safe collection.
    /// </param>
    protected SynchronizedKeyedCollection(object syncRoot)
      : base(syncRoot)
    {
      _dictionary = new Dictionary<K, T>( );
    }

    /// <summary>
    /// Initializes a new instance of the SynchronizedKeyedCollection class with access synchronized by an explicitly specified object and with keys compared in a specified way.
    /// </summary>
    /// <param name="syncRoot">
    /// The object used to synchronize access to the thread-safe collection.
    /// </param>
    /// <param name="comparer">
    /// The <see cref="T:System.Collections.Generic.IEqualityComparer`1"></see> of type K used to compare key objects of type K for equality.
    /// </param>
    protected SynchronizedKeyedCollection(object syncRoot,IEqualityComparer<K> comparer)
      : base(syncRoot)
    {
      _dictionary = new Dictionary<K, T>(comparer);
    }

    /// <summary>
    /// Initializes a new instance of the SynchronizedKeyedCollection class with access synchronized by an explicitly specified object and with keys compared in a specified way.
    /// </summary>
    /// <param name="syncRoot">
    /// The object used to synchronize access to the thread-safe collection.
    /// </param>
    /// <param name="comparer">
    /// The <see cref="T:System.Collections.Generic.IEqualityComparer`1"></see> of type K used to compare key objects of type K for equality.
    /// </param>
    /// <param name="capacity">
    /// The number of items required to create a dictionary for the collection.
    /// </param>
    protected SynchronizedKeyedCollection(object syncRoot,IEqualityComparer<K> comparer, int capacity)
      : base(syncRoot)
    {
      _dictionary = new Dictionary<K, T>(capacity, comparer);
    } 
    #endregion

    #region Public Properties
    /// <summary>
    /// Gets the items with a specified key from the collection.
    /// </summary>
    /// <param name="key">
    /// The key for the item being retrieved.
    /// </param>
    /// <returns>
    ///  The item of type T from the collection with the key specified.
    /// </returns>
    public T this[K key]
    {
      get
      {
        lock (SyncRoot)
        {
          return _dictionary[key];
        }
      }
    } 
    #endregion

    #region Protected Properties
    /// <summary>
    /// Gets the dictionary associated with the collection.
    /// </summary>
    protected IDictionary<K, T> Dictionary
    {
      get { return _dictionary; }
    } 
    #endregion

    #region Public Methods
    /// <summary>
    /// Returns a value that indicates whether the collection contains an item with a specified key.
    /// </summary>
    /// <param name="key">
    /// The key of type K being tested for.
    /// </param>
    /// <returns>
    /// true if the collection contains an item with the key specified; otherwise, false.
    /// </returns>
    public bool Contains(K key)
    {
      lock (SyncRoot)
      {
        return _dictionary.ContainsKey(key);
      }
    }

    /// <summary>
    /// Removes an item with a specified key from the collection and returns a value that indicates whether an item was removed.
    /// </summary>
    /// <param name="key">
    /// The key of the item to be removed.
    /// </param>
    /// <returns>
    /// true if an item with the specified key was removed; otherwise, false.
    /// </returns>
    public bool Remove(K key)
    {
      lock (SyncRoot)
      {
        if (Contains(key))
        {
          T item = this[key];
          int index = IndexOf(item);
          RemoveItem(index);
          return true;
        }

        return false;
      }
    } 
    #endregion

    #region Protected Methods
    /// <summary>
    /// Changes the key for a specified item in the synchronized collection.
    /// </summary>
    /// <param name="item">
    /// The item whose key is being changed.
    /// </param>
    /// <param name="newKey">
    /// The new key for the specified item.
    /// </param>
    protected void ChangeItemKey(T item, K newKey)
    {
      lock (SyncRoot)
      {
        K old = GetKeyForItem(item);
        _dictionary[old] = default( T );
        _dictionary[newKey] = item;
      }
    }

    /// <summary>
    /// Removes all items from the collection.
    /// </summary>
    protected override void ClearItems()
    {
      base.ClearItems();
      lock (SyncRoot)
      {
        _dictionary.Clear( );
      }
    }

    /// <summary>
    /// When overridden in a derived class, gets the key for a specified item.
    /// </summary>
    /// <param name="item">
    /// The item of type T whose key is being retrieved.
    /// </param>
    /// <returns>
    ///  The key of type K for the specified item of type T.
    /// </returns>
    protected abstract K GetKeyForItem(T item);

    /// <summary>
    /// Inserts an item into the collection at a specified index.
    /// </summary>
    /// <param name="index">
    /// The zero-based index of the collection where the object is to be inserted.
    /// </param>
    /// <param name="item">
    /// The object to be inserted into the collection.
    /// </param>
    ///<exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.Generic.IList`1"></see> is read-only.</exception>
    ///<exception cref="T:System.ArgumentOutOfRangeException">index is not a valid index in the <see cref="T:System.Collections.Generic.IList`1"></see>.</exception>
    protected override void InsertItem(int index, T item)
    {
      base.InsertItem(index, item);
      _dictionary.Add(GetKeyForItem(item), item);
    }

    /// <summary>
    /// Removes an item at a specified index from the collection.
    /// </summary>
    /// <param name="index">
    /// The zero-based index of the element to be retrieved from the collection.
    /// </param>
    ///<exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.Generic.IList`1"></see> is read-only.</exception>
    ///<exception cref="T:System.ArgumentOutOfRangeException">index is not a valid index in the <see cref="T:System.Collections.Generic.IList`1"></see>.</exception>
    protected override void RemoveItem(int index)
    {
      K key = GetKeyForItem(base[index]);
      base.RemoveItem(index);
      _dictionary.Remove(key);
    }

    /// <summary>
    /// Replaces the item at a specified index with another item.
    /// </summary>
    /// <param name="index">
    /// The zero-based index of the object to be replaced.
    /// </param>
    /// <param name="item">
    /// The object to replace
    /// </param>
    ///<exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.Generic.IList`1"></see> is read-only.</exception>
    ///<exception cref="T:System.ArgumentOutOfRangeException">index is not a valid index in the <see cref="T:System.Collections.Generic.IList`1"></see>.</exception>
    protected override void SetItem(int index, T item)
    {
      base.SetItem(index, item);
      _dictionary[GetKeyForItem(item)] = item;
    } 
    #endregion
  }
}