using System;
using System.Collections;
using System.Collections.Generic;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections.PreRelease
{
  /// <summary>
  /// Provides a thread-safe, read-only collection that contains objects of a type specified by the generic parameter as elements.
  /// </summary>
  public class SynchronizedReadOnlyCollection<T> :
    IList<T>,
    IList
  {
    #region DataMembers
    private readonly List<T> _list;
    private readonly object _syncRoot;
    #endregion

    #region Public Constructors
    /// <summary>
    /// Initializes a new instance of the SynchronizedReadOnlyCollection class.
    /// </summary>
    public SynchronizedReadOnlyCollection()
      : this(new object())
    {
    }

    /// <summary>
    /// Initializes a new instance of the SynchronizedReadOnlyCollection class with the object used to synchronize access to the thread-safe, read-only collection.
    /// </summary>
    /// <param name="sync_root">
    /// The object used to synchronize access to the thread-safe, read-only collection.
    /// </param>
    public SynchronizedReadOnlyCollection(object sync_root)
      : this(sync_root, new List<T>())
    {
    }

    /// <summary>
    /// Initializes a new instance of the SynchronizedReadOnlyCollection class from a specified enumerable list of elements and with the object used to synchronize access to the thread-safe, read-only collection.
    /// </summary>
    /// <param name="syncroot_">
    /// The object used to synchronize access to the thread-safe, read-only collection.
    /// </param>
    /// <param name="list_">
    /// The IEnumerable collection of elements used to initialize the thread-safe, read-only collection.
    /// </param>
    public SynchronizedReadOnlyCollection(object syncroot_, IEnumerable<T> list_)
    {
      if (syncroot_ == null)
      {
        throw new ArgumentNullException("syncroot_");
      }

      if (list_ == null)
      {
        throw new ArgumentNullException("list_");
      }

      _syncRoot = syncroot_;
      _list = new List<T>(list_);
    }

    /// <summary>
    /// Initializes a new instance of the SynchronizedReadOnlyCollection class from a specified array of elements and with the object used to synchronize access to the thread-safe, read-only collection.
    /// </summary>
    /// <param name="syncroot_"> 
    /// The object used to synchronize access to the thread-safe, read-only collection.
    /// </param>
    /// <param name="list_">
    /// The Array of type T elements used to initialize the thread-safe, read-only collection.
    /// </param>
    public SynchronizedReadOnlyCollection(object syncroot_, params T[] list_)
      : this(syncroot_, (IEnumerable<T>)list_)
    {
    }

    #endregion

    #region Public Properties

    /// <summary>
    /// Gets the number of elements contained in the thread-safe, read-only collection.
    /// </summary>
    public int Count
    {
      get
      {
        int retval;
        lock (_syncRoot)
        {
          retval = _list.Count;
        }
        return retval;
      }
    }

    /// <summary>
    /// Gets an element from the thread-safe, read-only collection with a specified index.
    /// </summary>
    /// <param name="index">
    /// The zero-based index of the element to be retrieved from the collection.
    /// </param>
    /// <returns>
    ///  The object in the collection that has the specified index.
    /// </returns>
    public T this[int index]
    {
      get
      {
        T retval;
        lock (_syncRoot)
        {
          retval = _list[index];
        }
        return retval;
      }
    }

    #endregion

    #region Protected Properties
    /// <summary>
    /// Gets the list of elements contained in the thread-safe, read-only collection.
    /// </summary>
    protected IList<T> Items
    {
      get { return _list; }
    }
    #endregion

    #region Public Methods
    ///<summary>
    ///Determines whether the <see cref="T:System.Collections.Generic.ICollection`1"></see> contains a specific item.
    ///</summary>
    ///
    ///<returns>
    ///true if item is found in the <see cref="T:System.Collections.Generic.ICollection`1"></see>; otherwise, false.
    ///</returns>
    ///
    ///<param name="item">The object to locate in the <see cref="T:System.Collections.Generic.ICollection`1"></see>.</param>
    public bool Contains(T item)
    {
      bool retval;

      lock (_syncRoot)
      {
        retval = _list.Contains(item);
      }

      return retval;
    }

    ///<summary>
    ///Copies the elements of the <see cref="T:System.Collections.Generic.ICollection`1"></see> to an <see cref="T:System.Array"></see>, starting at a particular <see cref="T:System.Array"></see> arrayIndex.
    ///</summary>
    ///
    ///<param name="array">The one-dimensional <see cref="T:System.Array"></see> that is the destination of the elements copied from <see cref="T:System.Collections.Generic.ICollection`1"></see>. The <see cref="T:System.Array"></see> must have zero-based indexing.</param>
    ///<param name="arrayIndex">The zero-based arrayIndex in array at which copying begins.</param>
    ///<exception cref="T:System.ArgumentOutOfRangeException">arrayIndex is less than 0.</exception>
    ///<exception cref="T:System.ArgumentNullException">array is null.</exception>
    ///<exception cref="T:System.ArgumentException">array is multidimensional.-or-arrayIndex is equal to or greater than the length of array.-or-The number of elements in the source <see cref="T:System.Collections.Generic.ICollection`1"></see> is greater than the available space from arrayIndex to the end of the destination array.-or-Type T cannot be cast automatically to the type of the destination array.</exception>
    public void CopyTo(T[] array, int arrayIndex)
    {
      lock (_syncRoot)
      {
        _list.CopyTo(array, arrayIndex);
      }
    }

    ///<summary>
    ///Returns an enumerator that iterates through the collection.
    ///</summary>
    ///
    ///<returns>
    ///A <see cref="T:System.Collections.Generic.IEnumerator`1"></see> that can be used to iterate through the collection.
    ///</returns>
    ///<filterpriority>1</filterpriority>
    public IEnumerator<T> GetEnumerator()
    {
      IEnumerator<T> retval;

      lock (_syncRoot)
      {
        retval = _list.GetEnumerator( );
      }

      return retval;
    }

    ///<summary>
    ///Determines the index of a specific item in the <see cref="T:System.Collections.Generic.IList`1"></see>.
    ///</summary>
    ///
    ///<returns>
    ///The index of item if found in the list; otherwise, -1.
    ///</returns>
    ///
    ///<param name="item">The object to locate in the <see cref="T:System.Collections.Generic.IList`1"></see>.</param>
    public int IndexOf(T item)
    {
      int retval;

      lock (_syncRoot)
      {
        retval = _list.IndexOf(item);
      }

      return retval;
    }

    #endregion

    #region Explicit Interface Implementations

    #region ICollection<T>
    void ICollection<T>.Add(T value)
    {
      throw new NotSupportedException( );
    }

    void ICollection<T>.Clear( )
    {
      throw new NotSupportedException( );
    }

    bool ICollection<T>.Remove(T value)
    {
      throw new NotSupportedException( );
    }

    bool ICollection<T>.IsReadOnly
    {
      get { return true; }
    }

    bool ICollection.IsSynchronized
    {
      get { return true; }
    }
    #endregion

    #region ICollection
    void ICollection.CopyTo(Array array, int index)
    {
      ICollection<T> a = array as ICollection<T>;

      if (a == null)
      {
        throw new ArgumentException("The array type is not compatible.");
      }

      lock (_syncRoot)
      {
        ( (ICollection) _list ).CopyTo(array, index);
      }
    }

    object ICollection.SyncRoot
    {
      get { return _syncRoot; }
    }
    #endregion

    #region IEnumerable
    IEnumerator IEnumerable.GetEnumerator()
    {
      return GetEnumerator();
    }
    #endregion

    #region IList<T>
    void IList<T>.Insert(int index, T value)
    {
      throw new NotSupportedException( );
    }

    void IList<T>.RemoveAt(int index)
    {
      throw new NotSupportedException( );
    }

    T IList<T>.this[int index]
    {
      get { return this[index]; }
      set { throw new NotSupportedException(); }
    }
    #endregion

    #region IList
    int IList.Add(object value)
    {
      throw new NotSupportedException( );
    }

    void IList.Clear( )
    {
      throw new NotSupportedException( );
    }

    bool IList.Contains(object value)
    {
      if (typeof(T).IsValueType)
      {
        throw new ArgumentException("This is a collection of ValueTypes.");
      }

      // null always gets thru
      if (value is T == false && value != null)
      {
        throw new ArgumentException("value is not of the same type as this collection.");
      }

      bool retval;
      T val = (T)value;
      lock (_syncRoot)
      {
        retval = _list.Contains(val);
      }

      return retval;
    }

    int IList.IndexOf(object value)
    {
      if (typeof(T).IsValueType)
      {
        throw new ArgumentException("This is a collection of ValueTypes.");
      }

      if (value is T == false)
      {
        throw new ArgumentException("value is not of the same type as this collection.");
      }

      int retval;
      T val = (T)value;
      lock (_syncRoot)
      {
        retval = _list.IndexOf(val);
      }

      return retval;
    }

    void IList.Insert(int index, object value)
    {
      throw new NotSupportedException( );
    }

    void IList.Remove(object value)
    {
      throw new NotSupportedException( );
    }

    void IList.RemoveAt(int index)
    {
      throw new NotSupportedException( );
    }

    bool IList.IsFixedSize
    {
      get { return true; }
    }

    bool IList.IsReadOnly
    {
      get { return true; }
    }

    object IList.this[int index]
    {
      get { return this[index]; }
      set { throw new NotSupportedException(); }
    }
    #endregion
    #endregion

  }
}