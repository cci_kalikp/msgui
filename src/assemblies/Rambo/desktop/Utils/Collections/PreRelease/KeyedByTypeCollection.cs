using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections.PreRelease
{
  /// <summary>
  /// Provides a collection whose items are types that serve as keys.
  /// </summary>
  /// <typeparam name="TItem">
  /// The item types contained in the collection that also serve as the keys for the collection.
  /// </typeparam>
  public class KeyedByTypeCollection<TItem>
    : KeyedCollection<Type, TItem>
  {
    #region Public Constructors
    /// <summary>
    /// Initializes a new instance of the KeyedByTypeCollection class.
    /// </summary>
    public KeyedByTypeCollection()
    {
    }

    /// <summary>
    /// Initializes a new instance of the KeyedByTypeCollection class for a specified enumeration of objects.
    /// </summary>
    /// <param name="items">
    /// The IEnumerable of generic type Object used to initialize the collection.
    /// </param>
    public KeyedByTypeCollection(IEnumerable<TItem> items)
    {
      if (items == null)
      {
        throw new ArgumentNullException("items");
      }

      foreach (TItem item in items)
      {
        Add(item);
    } 
    }
    #endregion

    #region Public Methods
    /// <summary>
    /// Returns the first item in the collection of a specified type.
    /// </summary>
    /// <typeparam name="T">
    /// The type of item in the collection to find.
    /// </typeparam>
    /// <returns>
    ///  The object of type T if it is a reference type and the value of type T if it is a value type. The default value of the type is returned if no object of type T is contained in the collection: null if it is a reference type and 0 if it is a value type.
    /// </returns>
    public T Find<T>()
    {
      foreach (TItem k in this)
      {
        if (k is T)
        {
          return (T)(object)k;
        }
      }
      return default(T);
    }

    /// <summary>
    /// Returns a collection of objects of type T that are contained in the KeyedByTypeCollection.
    /// </summary>
    /// <typeparam name="T">
    /// The type of item in the collection to find.
    /// </typeparam>
    /// <returns>
    ///  A Collection of type T that contains the objects of type T from the original collection.
    /// </returns>
    public Collection<T> FindAll<T>()
    {
      Collection<T> list = new Collection<T>();
      foreach (TItem k in this)
      {
        if (k is T)
        {
          list.Add((T)(object)k);
        }
      }
      return list;
    }

    /// <summary>
    /// Removes an object of a specified type from the collection.
    /// </summary>
    /// <typeparam name="T">
    /// The type of item in the collection to remove.
    /// </typeparam>
    /// <returns>
    ///  The object removed from the collection.
    /// </returns>
    public T Remove<T>()
    {
      foreach (TItem k in this)
      {
        if (k is T)
        {
          Remove(k);
          return (T)(object)k;
        }
      }
      return default(T);
    }

    /// <summary>
    /// Removes all of the elements of a specified type from the collection.
    /// </summary>
    /// <typeparam name="T">
    /// The type of item in the collection to remove.
    /// </typeparam>
    /// <returns>
    ///  The Collection that contains the objects of type T from the original collection.
    /// </returns>
    public Collection<T> RemoveAll<T>()
    {
      Collection<T> collection = new Collection<T>();

      foreach(TItem k in this)
      {
        if(k is T)
        {
          Remove(k);
          collection.Add((T)(object)k);
        }
      }

      return collection;
    } 
    #endregion

    #region Protected Methods
    ///<summary>
    ///When implemented in a derived class, extracts the key from the specified element.
    ///</summary>
    ///<returns>
    ///The key for the specified element.
    ///</returns>
    ///<param name="item">The element from which to extract the key.</param>
    protected override Type GetKeyForItem(TItem item)
    {
      return item.GetType();
    }

    ///<summary>
    ///Inserts an element into the <see cref="T:System.Collections.ObjectModel.KeyedCollection`2"></see> at the specified index.
    ///</summary>
    ///<param name="item">The object to insert.</param>
    ///<param name="index">The zero-based index at which item should be inserted.</param>
    ///<exception cref="T:System.ArgumentOutOfRangeException">index is less than 0.-or-index is greater than <see cref="P:System.Collections.ObjectModel.Collection`1.Count"></see>.</exception>
    protected override void InsertItem(int index, TItem item)
    {
      if (!typeof(TItem).IsValueType)
      {
        if (item == null)
        {
          throw new ArgumentNullException("item");
      }
      }

      base.InsertItem(index, item);
    }
    #endregion
  }
}