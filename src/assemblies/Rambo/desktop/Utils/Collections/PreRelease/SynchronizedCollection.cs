using System;
using System.Collections;
using System.Collections.Generic;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections.PreRelease
{
  /// <summary>
  /// Provides a thread-safe collection that contains objects of a type specified by the generic parameter as elements.
  /// </summary>
  /// <typeparam name="T">
  /// The type of object contained as items in the thread-safe collection.
  /// </typeparam>
  public class SynchronizedCollection<T> : 
    IList<T>,
    IList
  {
    #region Data Members
    private readonly object _syncroot;
    private readonly List<T> _list; 
    #endregion

    #region Public Constructors
    /// <summary>
    /// Initializes a new instance of the SynchronizedCollection class.
    /// </summary>
    public SynchronizedCollection()
      : this(new object())
    {
    }

    /// <summary>
    /// Initializes a new instance of the SynchronizedCollection class with the object used to synchronize access to the thread-safe collection.
    /// </summary>
    /// <param name="syncRoot">
    /// The object used to synchronize access the thread-safe collection.
    /// </param>
    public SynchronizedCollection(object syncRoot)
      :this(syncRoot, new List<T>())
    {
    }

    /// <summary>
    /// Initializes a new instance of the SynchronizedCollection class from a specified enumerable list of elements and with the object used to synchronize access to the thread-safe collection.
    /// </summary>
    /// <param name="syncRoot">
    /// The object used to synchronize access to the thread-safe collection.
    /// </param>
    /// <param name="list">
    /// The IEnumerable collection of elements used to initialize the thread-safe collection.
    /// </param>
    public SynchronizedCollection(object syncRoot,IEnumerable<T> list)
      : this(syncRoot, list == null ? null : new List<T>(list))
    {
    }

    /// <summary>
    /// Initializes a new instance of the SynchronizedCollection class from a specified array of elements and with the object used to synchronize access to the thread-safe collection.
    /// </summary>
    /// <param name="syncRoot">
    /// The object used to synchronize access the thread-safe collection.
    /// </param>
    /// <param name="list">
    ///     The Array of type T elements used to initialize the thread-safe collection.
    /// </param>
    public SynchronizedCollection(object syncRoot, params T[] list)
      : this(syncRoot, (IEnumerable<T>)list)
    {
    }

    private SynchronizedCollection(object syncRoot, List<T> list)
    {
      if (syncRoot == null)
      {
        throw new ArgumentNullException("syncRoot");
      }
      if (list == null)
      {
        throw new ArgumentNullException("list");
      }

      _syncroot = syncRoot;
      _list = list;
    }
    #endregion

    #region Public Properties
    /// <summary>
    /// Gets the number of elements contained in the thread-safe collection.
    /// </summary>
    public int Count
    {
      get
      {
        lock (_syncroot)
        {
          return _list.Count;
        }
      }
    }

    /// <summary>
    /// Gets an element from the thread-safe collection with a specified index.
    /// </summary>
    /// <param name="index">
    /// The zero-based index of the element to be retrieved from the collection.
    /// </param>
    /// <returns>
    ///  The object in the collection that has the specified index.
    /// </returns>
    public T this[int index]
    {
      get
      {
        lock (_syncroot)
        {
          return _list[index];
      }
      }
      set { SetItem(index, value); }
    }

    /// <summary>
    /// Gets the object used to synchronize access to the thread-safe collection.
    /// </summary>
    public object SyncRoot
    {
      get { return _syncroot; }
    }
    #endregion

    #region Protected Properties
    /// <summary>
    /// Gets the list of elements contained in the thread-safe collection.
    /// </summary>
    protected List<T> Items
    {
      get { return _list; }
    }
    #endregion

    #region Public Methods
    ///<summary>
    ///Adds an item to the <see cref="ICollection{T}"></see>.
    ///</summary>
    ///<param name="item">The object to add to the <see cref="ICollection{T}"></see>.</param>
    ///<exception cref="T:System.NotSupportedException">The <see cref="ICollection{T}"></see> is read-only.</exception>
    public void Add(T item)
    {
      InsertItem(_list.Count, item);
    }

    ///<summary>
    ///Removes all items from the <see cref="ICollection{T}"></see>.
    ///</summary>
    ///<exception cref="T:System.NotSupportedException">The <see cref="ICollection{T}"></see> is read-only. </exception>
    public void Clear()
    {
      ClearItems();
    }

    ///<summary>
    ///Determines whether the <see cref="ICollection{T}"></see> contains a specific value.
    ///</summary>
    ///<returns>
    ///true if item is found in the <see cref="ICollection{T}"></see>; otherwise, false.
    ///</returns>
    ///<param name="item">The object to locate in the <see cref="ICollection{T}"></see>.</param>
    public bool Contains(T item)
    {
      lock (_syncroot)
      {
        return _list.Contains(item);
      }
    }

    ///<summary>
    ///Copies the elements of the <see cref="ICollection{T}"></see> to an <see cref="T:System.Array"></see>, starting at a particular <see cref="T:System.Array"></see> arrayIndex.
    ///</summary>
    ///<param name="array">The one-dimensional <see cref="T:System.Array"></see> that is the destination of the elements copied from <see cref="ICollection{T}"></see>. The <see cref="T:System.Array"></see> must have zero-based indexing.</param>
    ///<param name="arrayIndex">The zero-based arrayIndex in array at which copying begins.</param>
    ///<exception cref="T:System.ArgumentOutOfRangeException">arrayIndex is less than 0.</exception>
    ///<exception cref="T:System.ArgumentNullException">array is null.</exception>
    ///<exception cref="T:System.ArgumentException">array is multidimensional.-or-arrayIndex is equal to or greater than the length of array.-or-The number of elements in the source <see cref="ICollection{T}"></see> is greater than the available space from arrayIndex to the end of the destination array.-or-Type T cannot be cast automatically to the type of the destination array.</exception>
    public void CopyTo(T[] array, int arrayIndex)
    {
      lock (_syncroot)
      {
        _list.CopyTo(array, arrayIndex);
      }
    }

    ///<summary>
    ///Returns an enumerator that iterates through the collection.
    ///</summary>
    ///<returns>
    ///A <see cref="T:System.Collections.Generic.IEnumerator`1"></see> that can be used to iterate through the collection.
    ///</returns>
    ///<filterpriority>1</filterpriority>
    public IEnumerator<T> GetEnumerator()
    {
      lock (_syncroot)
      {
        return _list.GetEnumerator( );
      }
    }

    ///<summary>
    ///Determines the index of a specific item in the <see cref="T:System.Collections.Generic.IList`1"></see>.
    ///</summary>
    ///<returns>
    ///The index of item if found in the list; otherwise, -1.
    ///</returns>
    ///<param name="item">The object to locate in the <see cref="T:System.Collections.Generic.IList`1"></see>.</param>
    public int IndexOf(T item)
    {
      lock (_syncroot)
      {
        return _list.IndexOf(item);
      }
    }

    ///<summary>
    ///Inserts an item to the <see cref="T:System.Collections.Generic.IList`1"></see> at the specified index.
    ///</summary>
    ///<param name="item">The object to insert into the <see cref="T:System.Collections.Generic.IList`1"></see>.</param>
    ///<param name="index">The zero-based index at which item should be inserted.</param>
    ///<exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.Generic.IList`1"></see> is read-only.</exception>
    ///<exception cref="T:System.ArgumentOutOfRangeException">index is not a valid index in the <see cref="T:System.Collections.Generic.IList`1"></see>.</exception>
    public void Insert(int index, T item)
    {
      InsertItem(index, item);
    }

    public bool Remove(T item)
    {
      int index = IndexOf(item);
      if (index < 0)
      {
        return false;
      }
      RemoveAt(index);
      return true;
    }

    ///<summary>
    ///Removes the <see cref="T:System.Collections.Generic.IList`1"></see> item at the specified index.
    ///</summary>
    ///<param name="index">The zero-based index of the item to remove.</param>
    ///<exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.Generic.IList`1"></see> is read-only.</exception>
    ///<exception cref="T:System.ArgumentOutOfRangeException">index is not a valid index in the <see cref="T:System.Collections.Generic.IList`1"></see>.</exception>
    public void RemoveAt(int index)
    {
      RemoveItem(index);
    }
    #endregion

    #region Protected Methods
    /// <summary>
    /// Removes all items from the collection.
    /// </summary>
    protected virtual void ClearItems()
    {
      lock (_syncroot)
      {
        _list.Clear( );
      }
    }

    /// <summary>
    /// Inserts an item into the collection at a specified index.
    /// </summary>
    /// <param name="index">
    /// The zero-based index of the collection where the object is to be inserted.
    /// </param>
    /// <param name="item">
    /// The object to be inserted into the collection.
    /// </param>
    ///<exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.Generic.IList`1"></see> is read-only.</exception>
    ///<exception cref="T:System.ArgumentOutOfRangeException">index is not a valid index in the <see cref="T:System.Collections.Generic.IList`1"></see>.</exception>
    protected virtual void InsertItem(int index, T item)
    {
      lock (_syncroot)
      {
        _list.Insert(index, item);
      }
    }

    /// <summary>
    /// Removes an item at a specified index from the collection.
    /// </summary>
    /// <param name="index">
    /// The zero-based index of the element to be retrieved from the collection.
    /// </param>
    ///<exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.Generic.IList`1"></see> is read-only.</exception>
    ///<exception cref="T:System.ArgumentOutOfRangeException">index is not a valid index in the <see cref="T:System.Collections.Generic.IList`1"></see>.</exception>
    protected virtual void RemoveItem(int index)
    {
      lock (_syncroot)
      {
        _list.RemoveAt(index);
      }
    }

    /// <summary>
    /// Replaces the item at a specified index with another item.
    /// </summary>
    /// <param name="index">
    /// The zero-based index of the object to be replaced.
    /// </param>
    /// <param name="item">
    /// The object to replace
    /// </param>
    ///<exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.Generic.IList`1"></see> is read-only.</exception>
    ///<exception cref="T:System.ArgumentOutOfRangeException">index is not a valid index in the <see cref="T:System.Collections.Generic.IList`1"></see>.</exception>
    protected virtual void SetItem(int index, T item)
    {
      lock (_syncroot)
      {
        _list[index] = item;
      }
    }
    #endregion

    #region Explicit Interface Implementations

    #region ICollection
    void ICollection.CopyTo(Array array, int index)
    {
      CopyTo((T[])array, index);
    }

    bool ICollection<T>.IsReadOnly
    {
      get { return false; }
    }

    bool ICollection.IsSynchronized
    {
      get { return true; }
    }

    object ICollection.SyncRoot
    {
      get { return _syncroot; }
    }
    #endregion

    #region IEnumerable
    IEnumerator IEnumerable.GetEnumerator()
    {
      return GetEnumerator();
    } 
    #endregion

    #region IList
    int IList.Add(object value)
    {
      lock (_syncroot)
      {
        Add((T)value);
        return _list.Count - 1;
      }
    }

    bool IList.Contains(object value)
    {
      return Contains((T)value);
    }

    int IList.IndexOf(object value)
    {
      return IndexOf((T)value);
    }

    void IList.Insert(int index, object value)
    {
      Insert(index, (T)value);
    }

    void IList.Remove(object value)
    {
      Remove((T)value);
    }

    bool IList.IsFixedSize
    {
      get { return false; }
    }

    bool IList.IsReadOnly
    {
      get { return false; }
    }

    object IList.this[int index]
    {
      get { return this[index]; }
      set { this[index] = (T)value; }
    } 
    #endregion

    #endregion
  }
}