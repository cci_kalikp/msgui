using System;
using System.Collections.Generic;
using MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections.TypedLists;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections
{
  public interface IDataColumnCollection : IIndexableCollection<int, IDataColumn>
  {
    int Count { get; }

    IDataColumn this[int i] { get; }

    IDataColumn this[String colName_] { get; }

    IDataColumn Add(string name_, Type type_);
    IDataColumn Add(string name_, Type type_, DataHint hint_);
    void Remove(String colName_);

    void Clear();
   
    bool Contains(IDataColumn item);
    bool Contains(String name_);
    
    void CopyTo(IDataColumn[] array, int arrayIndex);
  }
}