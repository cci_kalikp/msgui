using System.Collections.Generic;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections
{
  /// <summary>
  /// A utility class for creating new collections based on existing collections. The elements
  /// of the new collection are the result of a transformation of the individual values of the 
  /// existing collection.  This transformation can include changes in type as well as value.
  /// </summary>
  public static class Transformer
  {
    /// <summary>
    /// Creates the transformation of the specified <see cref="IEnumerable{T}"/> to the 
    /// output <see cref="IEnumerable{T}"/> using the specified 
    /// <see cref="ITransformable{InType,OutType}"/> to perform the transformation of the 
    /// individual items.
    /// </summary>
    /// <typeparam name="InType">The type contained by the input <see cref="IEnumerable{T}"/>.</typeparam>
    /// <typeparam name="OutType">The type contained by the output <see cref="IEnumerable{T}"/>.</typeparam>
    /// <param name="enumeration">The enumeration used to create the transformation.</param>
    /// <param name="transformable">The transform implementation used for converting each item.</param>
    /// <returns>The transformed collection</returns>
    public static IEnumerable<OutType> CreateTransformation<InType, OutType>(
     IEnumerable<InType> enumeration, ITransformable<InType, OutType> transformable)
    {
      return CreateTransformation(enumeration, 
        (TransformPredicate<InType, OutType>)transformable.Transform);
    }


    /// <summary>
    /// Creates the transformation of the specified <see cref="IEnumerable{T}"/> to the 
    /// output <see cref="IEnumerable{T}"/> using the specified <see cref="TransformPredicate{InType,OutType}"/> to perform the transformation of the individual items. 
    /// </summary>
    /// <typeparam name="InType">The type contained by the input <see cref="IEnumerable{T}"/>.</typeparam>
    /// <typeparam name="OutType">The type contained by the output <see cref="IEnumerable{T}"/>.</typeparam>
    /// <param name="enumeration">The enumeration used to create the transformation.</param>
    /// <param name="transformPredicate_">The transform implementation used for converting each item.</param>
    /// <returns>The transformed collection</returns>
    public static IEnumerable<OutType> CreateTransformation<InType, OutType>(
      IEnumerable<InType> enumeration, TransformPredicate<InType, OutType> transformPredicate_)
    {

      foreach (InType input in enumeration)
      {
        OutType output;
        if (transformPredicate_(input, out output))
        {
          yield return output;
        }
      }
    }

  }
}