namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections
{
  public interface IDataTable
  {
    string TableName
    { 
      get;
    }

    object SyncRoot
    {
      get;
    }

    IDataColumnCollection Columns
    {
      get;
    }

    IDataRowCollection Rows
    {
      get;
    }

    void Clear( );
    

    object this[int rowIndex_, string colName_]
    {
      get;
      set;
    }

    object this[int rowIndex_, int colIndex_]
    {
      get;
      set;
    }
  }
}