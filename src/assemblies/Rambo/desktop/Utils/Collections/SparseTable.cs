///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2005 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
///////////////////////////////////////////////////////////////////////////////


namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections
{
  public class SparseTable<R> : IDataTable
    where R : SparseRow
  {
    #region Fields
    private readonly SparseRowCollection<R> _rows;
    private readonly SparseColumnCollection<R> _columns;
    private readonly IDataRowFactory<R> _rowFactory;
    private readonly string _name;
    private readonly object _syncRoot = new object();
    #endregion

    #region Constructors
    public SparseTable(string name_, IDataRowFactory<R> rowFactory_)
    {
      _name = name_;
      _rowFactory = rowFactory_;

      _rows = new SparseRowCollection<R>(this);
      _columns = new SparseColumnCollection<R>(this);

    }
    #endregion

    #region Public Methods
    public void Clear( )
    {
      _rows.Clear( );
      _columns.Clear( );
    }

    public string TableName
    {
      get { return _name; }
    }

    public object SyncRoot
    {
      get { return _syncRoot; }
    }
    
    public IDataColumnCollection Columns 
    {
      get { return _columns; }
    }

    public IDataRowCollection<R> Rows
    {
      get { return _rows; }
    }

    public object this[int rowIndex_, string colName_]
    {
      get { return _rows[rowIndex_][colName_]; }
      set { _rows[rowIndex_][colName_] = value; }
    }

    public object this[int rowIndex_, int colIndex_]
    {
      get { return _rows[rowIndex_][colIndex_]; }
      set { _rows[rowIndex_][colIndex_] = value; }
    }
    #endregion

    #region IDataTable Members
    IDataRowCollection IDataTable.Rows
    {
      get { return _rows;  }
    }
    #endregion


    #region Internal Methods
    internal IDataRowFactory<R> RowFactory
    {
      get { return _rowFactory; }
    }

    #endregion

    #region Overrides
    public override string ToString()
    {
      return string.Format("{0} [{1},{2}]", TableName, ((IDataTable)this).Rows.Count, ((IDataTable)this).Columns.Count);
    }

    public override int GetHashCode()
    {
      return GetType().GetHashCode() ^ TableName.GetHashCode();
    }

    public override bool Equals(object obj)
    {
      if (ReferenceEquals(this, obj))
      {
        return true;
      }

      SparseTable<R> that = obj as SparseTable<R>;
      if (that == null)
      {
        return false;
      }

      return TableName == that.TableName;
    }
        #endregion

  }
}
