using System;
using System.Collections;
using System.Collections.Generic;
using MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections.TypedLists;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections.Generic
{
  internal class SparseColumnCollection<R> : IDataColumnCollection
    where R : SparseRow
  {
    #region Readonly & Static Fields
    private readonly List<SparseColumn<R>> _cols;
    private readonly Dictionary<string, SparseColumn<R>> _columnMap;
    private readonly SparseTable<R> _table;
    #endregion

    #region Constructors
    internal SparseColumnCollection(SparseTable<R> table_)
    {
      _table = table_;

      _columnMap = new Dictionary<string, SparseColumn<R>>( );
      _cols = new List<SparseColumn<R>>( );
    }
    #endregion

    #region Instance Methods
    public override string ToString( )
    {
      return string.Format("Count={0}", Count);
    }

    internal void AddRange(IEnumerable<SparseColumn<R>> cols_)
    {
      lock (_table.SyncRoot)
      {
        foreach (SparseColumn<R> column in cols_)
        {
          if (Contains(column.ColumnName))
          {
            throw new InvalidOperationException("Cannot add duplicate column names");
          }
        }
        int start = _cols.Count;
        _cols.AddRange(cols_);
        for (int i = start; i < _cols.Count; ++i)
        {
          ( _cols[i] ).ColumnIndex = i;
        }
        SetLookupTable(cols_);
      }
    }

    internal void ClearData( )
    {
      lock (_table.SyncRoot)
      {
        foreach (SparseColumn<R> col in _cols)
        {
          col.Clear( );
        }
      }
    }

    private void Add(SparseColumn<R> col_)
    {
      lock (_table.SyncRoot)
      {
        if (Contains(col_.ColumnName))
        {
          throw new IndexOutOfRangeException("Cannot add duplicate column names");
        }
        col_.ColumnIndex = _cols.Count;
        _cols.Add(col_);

        SetLookupTable(new SparseColumn<R>[] {col_});
      }
    }

    private void Remove(SparseColumn<R> col_)
    {
      lock (_table.SyncRoot)
      {
        for (int i = 0; i < _cols.Count; ++i)
        {
          if (_cols[i].Equals(col_))
          {
            RemoveAt(i);
            return;
          }
        }
      }
    }

    private void RemoveAt(int idx_)
    {
      lock (_table.SyncRoot)
      {
        SparseColumn<R> col = _cols[idx_];
        _columnMap.Remove(col.ColumnName);
        _cols.RemoveAt(idx_);
        col.Clear( );
        for (int i = idx_; i < _cols.Count; ++i)
        {
          _cols[i].ColumnIndex = i;
        }
      }
    }

    private void SetLookupTable(IEnumerable<SparseColumn<R>> cols_)
    {
      lock (_table.SyncRoot)
      {
        foreach (SparseColumn<R> column in cols_)
        {
          if (!_columnMap.ContainsKey(column.ColumnName))
          {
            _columnMap.Add(column.ColumnName, column);
            column.Table = _table;
          }
        }
      }
    }
    #endregion

    #region IDataColumnCollection Members
    public int Count
    {
      get { return _cols.Count; }
    }

    public IDataColumn this[int i]
    {
      get { return _cols[i]; }
    }

    public IDataColumn this[string colName_]
    {
      get
      {
        SparseColumn<R> value;
        _columnMap.TryGetValue(colName_, out value);
        return value;
      }
    }

    public IDataColumn Add(string name_, Type type_, DataHint hint_)
    {
      Type type = Array.IndexOf(SparseColumn<R>.SupportedTypes, type_) < 0 ? typeof(string) : type_;
      SparseColumn<R> col = CreateColumn(name_, type, hint_);
      lock (_table.SyncRoot)
      {
        Add(col);
      }
      return col;
    }

    public IDataColumn Add(string name_, Type type_)
    {
      return Add(name_, type_, DataHint.NONE);
    }

    public void Clear( )
    {
      lock (_table.SyncRoot)
      {
        _cols.Clear( );
        _columnMap.Clear( );
      }
    }

    public bool Contains(IDataColumn item)
    {
      if (item == null)
      {
        return false;
      }

      return Contains(item.ColumnName);
    }

    public void CopyTo(IDataColumn[] array, int arrayIndex)
    {
      ( (IList) _cols ).CopyTo(array, arrayIndex);
    }

    public void Remove(String colName_)
    {
      lock (_table.SyncRoot)
      {
        SparseColumn<R> col = this[colName_] as SparseColumn<R>;
        if (col != null)
        {
          Remove(col);
        }
      }
    }

    public bool Contains(string name_)
    {
      return _columnMap.ContainsKey(name_);
    }

    IEnumerator<IDataColumn> IEnumerable<IDataColumn>.GetEnumerator( )
    {
      foreach (SparseColumn<R> col in _cols)
      {
        yield return col;
      }
    }

    public IEnumerator GetEnumerator( )
    {
      return ( (IEnumerable<IDataColumn>) this ).GetEnumerator( );
    }
    #endregion

    #region Static Methods
    private static SparseColumn<R> CreateColumn(string name_, Type dataType_, DataHint hint_)
    {
      Type unboundColumn = typeof(SparseColumn<,>);
      Type genericColumn = unboundColumn.MakeGenericType(dataType_, typeof(R));
      return (SparseColumn<R>) Activator.CreateInstance(genericColumn, name_, hint_);
    }
    #endregion
  }
}