using System;
using System.Collections;
using System.Collections.Generic;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections.Generic
{
  internal class SparseRowCollection<R> : IDataRowCollection<R>
    where R : SparseRow
  {
    #region Readonly & Static Fields

    private readonly Queue<int> _freeIndices;
    private readonly List<int> _indexMap;
    private readonly List<R> _rows;
    private readonly SparseTable<R> _table;
    #endregion

    #region Constructors
    internal SparseRowCollection(SparseTable<R> table_)
    {
      _table = table_;
      _freeIndices = new Queue<int>( );
      _rows = new List<R>( );
      _indexMap = new List<int>( );
    }
    #endregion

    #region Instance Properties
    public int AllocationCount
    {
      get { return _rows.Count; }
    }
    #endregion

    #region Instance Methods



    public override string ToString( )
    {
      return string.Format("Count={0}", Count);
    }

    private int DescendingIndexComparison(R row1, R row2)
    {
      int index1 = row1.RowIndex;
      int index2 = row2.RowIndex;

      return index2 - index1;
    }

    private void Reindex(int startIndex_)
    {
      for (int i = startIndex_; i < _indexMap.Count; ++i)
      {
        _rows[_indexMap[i]].RowIndex = i;
      }
    }

    private void RemoveRow(R row_)
    {
      _indexMap.RemoveAt(row_.RowIndex);
      _rows[row_.AllocationIndex] = null;
      _freeIndices.Enqueue(row_.AllocationIndex);
    }

    private void RemoveRowFromColumns(R row_)
    {
      for (int i = 0; i < _table.Columns.Count; ++i)
      {
        SparseColumn<R> column = (SparseColumn<R>) _table.Columns[i];
        column.RemoveAt(row_.AllocationIndex);
      }
    }

    private bool TryDowncast(IDataRow input, out R output)
    {
      output = input as R;
      return input != null;
    }
    #endregion

    #region IDataRowCollection<R> Members
    public int Count
    {
      get { return _indexMap.Count; }
    }

    public R this[int idx_]
    {
      get { return _rows[_indexMap[idx_]]; }
    }

    public R Add( )
    {
      lock (_table.SyncRoot)
      {
        int idx = Count;
        bool newAllocation = _freeIndices.Count == 0;
        int allocation = newAllocation ? AllocationCount : _freeIndices.Dequeue( );

        R row = _table.RowFactory.Create(_table, idx);
        row.AllocationIndex = allocation;
        _indexMap.Add(allocation);

        if (newAllocation)
        {
          _rows.Add(row);
        }
        else
        {
          _rows[allocation] = row;
        }

        return row;
      }
    }

    public void Clear( )
    {
      lock (_table.SyncRoot)
      {
        ( (SparseColumnCollection<R>) _table.Columns ).ClearData( );
        _rows.Clear( );
        _indexMap.Clear( );
        _freeIndices.Clear( );
      }
    }

    public void Remove(IEnumerable<R> rows_)
    {
      lock (_table.SyncRoot)
      {
        List<R> rowList = new List<R>(rows_);
        rowList.Sort(DescendingIndexComparison);

        foreach (R row in rowList)
        {
          RemoveRow(row);
        }

        Reindex(rowList[rowList.Count - 1].RowIndex);

        foreach (R row in rowList)
        {
          RemoveRowFromColumns(row);
        }
      }
    }

    public void Remove(R row_)
    {
      lock (_table.SyncRoot)
      {
        RemoveRow(row_);

        Reindex(row_.RowIndex);

        RemoveRowFromColumns(row_);
      }
    }

    public void RemoveAt(int idx_)
    {
      lock (_table.SyncRoot)
      {
        Remove(_rows[_indexMap[idx_]]);
      }
    }

    IEnumerator<IDataRow> IEnumerable<IDataRow>.GetEnumerator( )
    {
      for (int i = 0; i < _rows.Count; i++)
      {
        IDataRow row = _rows[i];

        if(row != null)
        {
          yield return _rows[i];          
        }
      }
    }

    public IEnumerator GetEnumerator( )
    {
      return ( (IEnumerable<IDataRow>) this ).GetEnumerator( );
    }

    IDataRow IDataRowCollection.this[int idx_]
    {
      get { return this[idx_]; }
    }

    IDataRow IDataRowCollection.Add( )
    {
      return Add( );
    }

    void IDataRowCollection.Remove(IEnumerable<IDataRow> rows_)
    {
      Remove(Transformer.CreateTransformation<IDataRow, R>(rows_, TryDowncast));
    }

    void IDataRowCollection.Remove(IDataRow row_)
    {
      R row = row_ as R;
      if (row != null)
      {
        Remove(row);
      }
    }
    #endregion

    IDataRow IIndexableCollection<int, IDataRow>.this[int index_]
    {
      get { return this[index_]; }
    }
  }
}