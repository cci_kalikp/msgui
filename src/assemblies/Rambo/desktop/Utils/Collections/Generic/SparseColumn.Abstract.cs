using System;
using System.Collections;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections.Generic
{
  internal abstract class SparseColumn<R> : IDataColumn
    where R : SparseRow
  {
    public static readonly Type[] SupportedTypes = new Type[]
                                                     {
                                                       typeof(double),
                                                       typeof(string),
                                                       typeof(DateTime),
                                                       typeof(float),
                                                       typeof(int),
                                                       typeof(bool)
                                                     };


    #region Fields
    private readonly string _name;
    private readonly Type _type;
    private int _ordinal = -1;
    private SparseTable<R> _table;
    #endregion

    #region Constructors
    /// <summary>
    /// Low-level Ctr.
    /// </summary>
    /// <param name="name_">name of data column</param>
    /// <param name="type_">type of data objects to be stored</param>
    protected SparseColumn(String name_, Type type_)
    {
      _name = name_;
      _type = type_;
    }

    #endregion

    #region Properties
    public string ColumnName
    {
      get { return _name; }
    }

    public Type DataType
    {
      get { return _type; }
    }

    public int Ordinal
    {
      get { return _ordinal; }
    }

    public abstract object this[int rowIdx_]
    { get; set; }
    #endregion

    #region Public Methods
    public abstract IList CopyData( );

    public abstract void Clear( );
    
    #endregion

    #region Internal Members
    protected internal abstract void RemoveAt(int index_);

    protected internal int ColumnIndex
    {
      get { return _ordinal; }
      set { _ordinal = value; }
    }


    protected internal SparseTable<R> Table
    {
      get { return _table; }
      set { _table = value; }
    } 
    #endregion

    #region Overrides
    public override string ToString()
    {
      return _name;
    }

    public override int GetHashCode()
    {
      return _name.GetHashCode( ) ^ _type.GetHashCode( );
    }

    public override bool Equals(object obj)
    {
      if(ReferenceEquals(this, obj))
      {
        return true;
      }

      SparseColumn<R> that = obj as SparseColumn<R>;
      
      if(that== null)
      {
        return false;
      }

      return _name == that._name && _type == that._type;
    }
    #endregion


  }
}