namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections.Generic
{
  public class SparseRowFactory : 
    IDataRowFactory<SparseRow>
  {
    private static readonly SparseRowFactory _instance = new SparseRowFactory();

    public static SparseRowFactory Instance
    {
      get { return _instance; }
    }

    SparseRow IDataRowFactory<SparseRow>.Create(IDataTable table_, int rowIdx_)
    {
      return new SparseRow((SparseTable<SparseRow>)table_, rowIdx_);

    }

    IDataRow IDataRowFactory.Create(IDataTable table_, int rowIdx_)
    {
      return ((IDataRowFactory<SparseRow>)this).Create(table_, rowIdx_);
    }
  }
}