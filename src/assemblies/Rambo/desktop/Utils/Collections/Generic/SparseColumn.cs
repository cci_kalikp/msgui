using System;
using System.Collections;
using System.Globalization;


using MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections.TypedLists;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections.Generic
{
  internal class SparseColumn<T, R> : SparseColumn<R>
    where R : SparseRow
  {
    #region Fields
    protected readonly IColumnStorage<T> _storage;
    private bool _parseStringsAsValues = true;
    #endregion

    #region Constructor


    public SparseColumn(string name_, DataHint hint_)
      : base(name_, typeof(T))
    {
      if (Array.IndexOf(SupportedTypes, DataType) < 0)
      {
        throw new NotSupportedException(string.Format("The type {0} is not supported.", DataType));
      }

      _storage = CreateStorage( hint_);

    }

    public SparseColumn(string name_) 
      : this(name_, DataHint.NONE)
    {

    }
    #endregion

    #region Public members
    public override object this[int rowIdx_]
    {
      get
      {
        T value =  _storage[rowIdx_];

        if (!NullValue<T>.IsNullValue(value))
        {
          return value;
        }


        return DBNull.Value;
      }
      set 
      { 
        if(value == null || value == DBNull.Value)
        {
          _storage.ClearValue(rowIdx_);
          return;
        }

        if(_parseStringsAsValues)
        {
          string strValue = value as string;
          if(strValue != null)
          {
            T parsedValue;
            if(DataValueParser<T>.TryParse(strValue, out parsedValue))
            {
              _storage[rowIdx_] = parsedValue;
              
            }
            else
            {
              _storage.ClearValue(rowIdx_);
            }

            return;
          }
        }

        _storage[rowIdx_] = (T)value; 
      }
    }

    public override void Clear()
    {
     
      _storage.Clear();
    }

    public override IList CopyData()
    {
      return new ArrayList();
    }
    #endregion

    #region Internal Members
    protected internal override void RemoveAt(int idx_)
    {

      _storage.ClearValue(idx_);
      
    }

    #endregion

    #region Private Members
    protected IColumnStorage<T> CreateStorage(DataHint hint_)
    {
      switch(hint_)
      {
        case DataHint.SPARSE:
          return new SparseStorage<T>();

        case DataHint.REPETITIVE:
          return new PoolingStorage<T>();

        default:
          return new ArrayStorage<T>();
      }
    }
    #endregion

  }
}