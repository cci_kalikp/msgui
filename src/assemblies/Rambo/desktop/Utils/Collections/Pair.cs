using System;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections
{
  public struct Pair<T1,T2>
  {
    private readonly T1 _first;
    private readonly T2 _second;
    private readonly string _string;
    public Pair(T1 first_, T2 second_)
    {
      _first = first_;
      _second = second_;
      _string = string.Format("({0},{1})", _first, _second);
    }

    public T1 First
    {
      get { return _first; }
    }

    public T2 Second
    {
      get { return _second; }
    }

    public override bool Equals(object obj)
    {
      Type type = typeof(Pair<T1, T2>);

      if(!type.IsInstanceOfType(obj))
      {
        return false;
      }

      Pair<T1, T2> that = (Pair<T1, T2>) obj;

      return this.First.Equals(that.First) &&
             this.Second.Equals(that.Second);
    }

    public override int GetHashCode()
    {
      return _first.GetHashCode( ) ^ _second.GetHashCode( );
    }

    public override string ToString()
    {
      return _string;
    }
  }
}