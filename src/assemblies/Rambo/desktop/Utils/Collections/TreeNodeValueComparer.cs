using System.Collections;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections
{
  public class TreeNodeValueComparer
  {
    private readonly object m_value;
    private readonly IEqualityComparer m_comparer;

    public TreeNodeValueComparer(object value) : this(value, null)
    {
    }

    public TreeNodeValueComparer(object value, IEqualityComparer comparer)
    {
      m_value = value;
      m_comparer = comparer;
    }

    public bool Compare(ITreeNode node)
    {
      if(node == null)
      {
        return false;
      }

      if(m_comparer == null)
      {
        return m_value == node.Value;
      }
      else
      {
        return m_comparer.Equals(m_value, node.Value);
      }
    }
  }
}