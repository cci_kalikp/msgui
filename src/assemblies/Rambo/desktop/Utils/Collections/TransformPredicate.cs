namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections
{
  /// <summary>
  /// A delegate for methods that convert values from one type to another 
  /// if the conversion is valid.
  /// </summary>
  public delegate bool TransformPredicate<InType, OutType>(InType input, out OutType output);
}