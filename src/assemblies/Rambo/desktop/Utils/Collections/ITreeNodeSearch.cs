using System;
using System.Collections.Generic;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections
{
  public interface ITreeNodeSearch
  {
    /// <summary>
    /// Finds the node found withing the specified enumeration that matches the specified predicate.
    /// </summary>
    /// <param name="iterator">The enumeration to search.</param>
    /// <param name="comparer">The search condition.</param>
    /// <returns>The tree node matching the provided criteria; otherwise, null</returns>
    ITreeNode Find(IEnumerable<ITreeNode> iterator, Predicate<ITreeNode> comparer);
  }
}