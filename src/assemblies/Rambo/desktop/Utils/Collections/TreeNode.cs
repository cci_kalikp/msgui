using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections
{
  public abstract class TreeNode : 
    ITreeNode
  {
    #region Data
    protected ITreeNode m_parentNode;
    protected ITreeNode m_rootNode;
    protected ITreeNodeList m_childNodes;
    protected ITreeNodeList m_parentList;
    protected int m_depth;
    protected int m_index;
    #endregion

    #region Constructors
    protected TreeNode(ITreeNodeList childNodes_)
    {
      m_depth = 0;
      m_index = -1;
      m_rootNode = this;
      m_parentNode = null;
      m_parentList = null;
      m_childNodes = childNodes_;
    }
    #endregion

    #region ITreeNode Members
    object ITreeNode.Value
    {
      get
      {
        return GetValue();
      }
      set
      {
        SetValue(value);
      }
    }

    public virtual bool IsRoot
    {
      get { return m_parentNode == null && m_depth == 0; }
    }

    public virtual bool HasChildren
    {
      get { return m_childNodes != null && m_childNodes.Count > 0; }
    }

    public virtual bool HasSiblings
    {
      get { return m_parentList != null && m_parentList.Count > 1; }
    }

    public virtual int Depth
    {
      get { return m_depth; }
    }

    public virtual int Index
    {
      get { return m_index; }
    }

    public abstract int Descendants { get; }

    public abstract int Generations { get; }

    public ITreeNode RootNode
    {
      get { return m_rootNode; }
    }

    public ITreeNode ParentNode
    {
      get { return m_parentNode; }
    }

    public ITreeNodeList ChildNodes
    {
      get
      {
        return m_childNodes;
      }
    }

    public ITreeNodeList ParentList
    {
      get
      {
        return m_parentList;
      }
    }

    public IEnumerable<ITreeNode> Siblings
    {
      get
      {
        if(HasSiblings)
        {
          foreach(ITreeNode node in m_parentList)
          {
            if(node != this)
            {
              yield return node;
            }
          }
        }
      }
    }

    public IEnumerable AncestorEnumerator
    {
      get 
      {
        foreach (ITreeNode node in AncestorNodeEnumerator)
        {
          yield return node.Value;
        }
      }
    }

    public IEnumerable DepthFirstEnumerator
    {
      get
      {
        foreach (ITreeNode node in DepthFirstNodeEnumerator)
        {
          yield return node.Value;
        }
      }
    }

    public IEnumerable BreadthFirstEnumerator
    {
      get
      {
        foreach (ITreeNode node in BreadthFirstNodeEnumerator)
        {
          yield return node.Value;
        }
      }
    }

    public IEnumerable<ITreeNode> AncestorNodeEnumerator
    {
      get
      {
        for (ITreeNode node = this;
             node != null;
             node = node.ParentNode)
        {
          yield return node;
        }
      }
    }

    public IEnumerable<ITreeNode> DepthFirstNodeEnumerator
    {
      get
      {

        foreach (ITreeNode child in m_childNodes)
        {
          foreach (ITreeNode node in child.DepthFirstNodeEnumerator)
          {
            yield return node;
          }
        }

        yield return this;
      }
    }
    

    public IEnumerable<ITreeNode> BreadthFirstNodeEnumerator
    {
      get
      {
        Queue<ITreeNode> todo = new Queue<ITreeNode>();
        todo.Enqueue(this);
        while (0 < todo.Count)
        {
          ITreeNode node = todo.Dequeue();
          
          foreach (ITreeNode child in node.ChildNodes)
          {
            todo.Enqueue(child);
          }

          yield return node;
        }
      }
    }

    public override string ToString()
    {
      StringBuilder builder = new StringBuilder( );

      foreach(ITreeNode node in AncestorNodeEnumerator)
      {
        builder.Insert(0, node.Index);
        builder.Insert(0, ':');
      }

      return builder.ToString(1, builder.Length - 1);

    }
    #endregion


    #region ITreeNodeSearch Members
    /// <summary>
    /// Finds the node found withing the specified enumeration that matches the specified predicate.
    /// </summary>
    /// <param name="iterator">The enumeration to search.</param>
    /// <param name="comparer">The search condition.</param>
    /// <returns>The tree node matching the provided criteria; otherwise, null</returns>
    public ITreeNode Find(IEnumerable<ITreeNode> iterator, Predicate<ITreeNode> comparer)
    {
      foreach (ITreeNode node in iterator)
      {
        if (comparer(node))
        {
          return node;
        }
      }
      return null;
    }
    #endregion


    #region Protected Methods
    protected internal void SetDepth(int depth_)
    {
      m_depth = depth_; 
    }

    protected internal void SetRoot(ITreeNode root_)
    {
      m_rootNode = root_;
    }
    #endregion

    #region Abstract Methods
    protected abstract object GetValue( );
    protected abstract void SetValue(object value);
    #endregion

  }
}
