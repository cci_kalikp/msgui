using System;
using System.Collections.Generic;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections
{
  public class ReadOnlySet<T> : 
    Set<T>, 
    ICollection<T>
  {
    #region Constructors
    /// <summary>
    /// Initializes a new instance of the <see cref="ReadOnlySet{T}"></see> class that contains elements copied from the specified <see cref="T:System.Collections.Generic.IEnumerable`1"></see> and uses the default equality comparer for comparison.
    /// </summary>
    /// <param name="collection">
    /// The <see cref="T:System.Collections.Generic.IEnumerable`1"></see> whose distinct elements are copied to the new <see cref="ReadOnlySet{T}"></see>.
    /// </param>
    public ReadOnlySet(IEnumerable<T> collection)
      : base(collection)
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="ReadOnlySet{T}"></see> class that contains elements copied from the specified <see cref="T:System.Collections.Generic.IEnumerable`1"></see> and uses the specified equality comparer for comparison.
    /// </summary>
    /// <param name="collection">
    /// The <see cref="T:System.Collections.Generic.IEnumerable`1"></see> whose distinct elements are copied to the new <see cref="ReadOnlySet{T}"></see>.
    /// </param>
    /// <param name="comparer">
    /// The <see cref="T:System.Collections.Generic.IEqualityComparer`1"></see> implementation to use when comparing keys, or a null reference to use the default <see cref="T:System.Collections.Generic.EqualityComparer`1"></see> for the type of the key.
    /// </param>
    public ReadOnlySet(IEnumerable<T> collection, IEqualityComparer<T> comparer)
      : base(collection, comparer)
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="ReadOnlySet{T}"></see> class that contains elements copied from the specified <see cref="T:System.Collections.Generic.IEnumerable`1"></see> and uses the default equality comparer for comparison.
    /// </summary>
    /// <param name="syncroot">
    /// The object used to synchronize access to the thread-safe collection.
    /// </param>
    /// <param name="collection">
    /// The <see cref="T:System.Collections.Generic.IEnumerable`1"></see> whose distinct elements are copied to the new <see cref="ReadOnlySet{T}"></see>.
    /// </param>
    public ReadOnlySet(object syncroot, IEnumerable<T> collection)
      : base(syncroot, collection)
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="ReadOnlySet{T}"></see> class that contains elements copied from the specified <see cref="T:System.Collections.Generic.IEnumerable`1"></see> and uses the specified equality comparer for comparison.
    /// </summary>
    /// <param name="syncroot">
    /// The object used to synchronize access to the thread-safe collection.
    /// </param>
    /// <param name="collection">
    /// The <see cref="T:System.Collections.Generic.IEnumerable`1"></see> whose distinct elements are copied to the new <see cref="ReadOnlySet{T}"></see>.
    /// </param>
    /// <param name="comparer">
    /// The <see cref="T:System.Collections.Generic.IEqualityComparer`1"></see> implementation to use when comparing keys, or a null reference to use the default <see cref="T:System.Collections.Generic.EqualityComparer`1"></see> for the type of the key.
    /// </param>
    public ReadOnlySet(object syncroot, IEnumerable<T> collection, IEqualityComparer<T> comparer)
      : base(syncroot, collection, comparer)
    {
    }
    #endregion

    #region Public Properies
    public bool IsReadOnly
    {
      get { return true; }
    }
    #endregion

    #region Protected Methods
    protected override void ClearItems()
    {
      throw new NotSupportedException();
    }

    protected override void InsertItem(int index, T item)
    {
      throw new NotSupportedException();
    }

    protected override void RemoveItem(int index)
    {
      throw new NotSupportedException();
    }

    protected override void SetItem(int index, T item)
    {
      throw new NotSupportedException();
    }
    #endregion

    bool ICollection<T>.IsReadOnly
    {
      get{ return true; }
    }
  }
}