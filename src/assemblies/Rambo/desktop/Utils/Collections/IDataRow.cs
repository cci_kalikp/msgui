using System;
using System.ComponentModel;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections
{
  public interface IDataRow
  {
    [Browsable(false)]
    IDataTable Table { get; }

    [Browsable(false)]
    object[] ItemArray { get; set; }

    object this[int colIdx_] { get; set; }

    object this[String colName_] { get; set; }
  }
}