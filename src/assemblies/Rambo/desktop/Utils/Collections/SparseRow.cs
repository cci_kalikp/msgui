///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2005 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
///////////////////////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections
{
  public class SparseRow : IDataRow
  {
    #region Fields
    protected int _rowIdx; //logical index in the collection (can change as rows are added and removed from table)
    private int _allocationIndex; // unique index over the table, does not change.
    protected IDataTable _parentTable;
    #endregion

    #region Constructor
    protected internal SparseRow(IDataTable table_, int idx_)
    {
      _parentTable = table_;
      _rowIdx = idx_;
    }
    #endregion

    #region Public Properties
    [Browsable(false)]
    public IDataTable Table
    {
      get { return _parentTable; }
    }

    [Browsable(false)]
    public object[] ItemArray
    {
      get
      {
        object[] ret = new object[_parentTable.Columns.Count];
        for (int i = 0; i < ret.Length; ++i)
        {
          ret[i] = this[i];
        }
        return ret;
      }
      set
      {
        int len = value.Length;
        if (len > _parentTable.Columns.Count)
        {
          throw new ArgumentException("Too many elements passed to ItemArray");
        }
        for (int i = 0; i < len; ++i)
        {
          this[i] = value[i];
        }
      }
    }

    public virtual object this[int colIdx_]
    {
      get { return _parentTable.Columns[colIdx_][_allocationIndex]; }
      set { _parentTable.Columns[colIdx_][_allocationIndex] = value; }
    }

    public virtual object this[String colName_]
    {
      get { return _parentTable.Columns[colName_][_allocationIndex]; }
      set { _parentTable.Columns[colName_][_allocationIndex] = value; }
    }

    public int RowIndex
    {
      get { return _rowIdx; }
      internal set { _rowIdx = value; }
    }

    public int AllocationIndex
    {
      get { return _allocationIndex; }
      internal set { _allocationIndex = value; }
    }
    #endregion

    #region Overrides
    public override string ToString()
    {
      return string.Format("{0}-{1}", _allocationIndex, _parentTable);
    }

    public override int GetHashCode()
    {
      return _parentTable.GetHashCode( ) ^ GetType().GetHashCode( ) ^ _allocationIndex;
    }

    public override bool Equals(object obj)
    {
      if(ReferenceEquals(obj, this))
      {
        return true;
      }

      SparseRow that = obj as SparseRow;
      if(that == null)
      {
        return false;
      }

      if(_parentTable == null)
      {
        return false;
      }

      return _parentTable.Equals(that._parentTable) &&
             _allocationIndex == that._allocationIndex;
    }
    #endregion
  }
}
