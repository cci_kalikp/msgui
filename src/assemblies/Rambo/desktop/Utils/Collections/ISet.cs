using System.Collections.Generic;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections
{
  /// <summary>
  /// Defines methods to manipulate collections of distinct objects.
  /// </summary>
  /// <typeparam name="T">
  /// The type of object contained as items in the collection.
  /// </typeparam>
  public interface ISet<T> : ICollection<T>
  {
    /// <summary>
    ///  OR - Returns a new set containing all of the distinct elements found in both the current set and the specified set.
    /// </summary>
    /// <param name="set_">A collection of distinct objects.</param>
    /// <returns></returns>
    ISet<T> Union(ISet<T> set_);


    /// <summary>
    /// AND - Returns a new set containing all of the distinct elements common to the current set and the specified set.
    /// </summary>
    /// <param name="set_">
    /// A collection of distinct objects.
    /// </param>
    /// <returns>
    /// A new ISet containing all of the distinct elements common to the current ISet and the specified ISet.
    /// </returns>
    ISet<T> Intersection(ISet<T> set_);


    /// <summary>
    /// XOR - Returns a new set containing all of the distinct elements found in to the current set or the specified set, but not both.
    /// </summary>
    /// <remarks>
    /// This is the equivalent of subtracting the intersection from the union.
    /// </remarks>
    /// <param name="set_">
    /// A collection of distinct objects.
    /// </param>
    /// <returns>
    /// A new ISet containing all of the distinct elements found in to the current ISet or the specified ISet, but not both.
    /// </returns>
    ISet<T> Difference(ISet<T> set_);

    /// <summary>
    /// Compliment - Returns a new set containing all of the distinct element found in the current set that are not in the specified set. 
    /// </summary>
    /// A collection of distinct objects.
    /// </param>
    /// <returns>
    /// A new ISet containing all of the distinct element found in the current ISet that are not in the specified ISet. 
    /// </returns>
    ISet<T> Subtract(ISet<T> set_);

    /// <summary>
    /// Returns a new set containing pairs constructed by combining every element of the current set with every element of the specified set.
    /// </summary>
    /// <typeparam name="S">The type contained by the specified set</typeparam>
    /// <param name="set_">A collection of distinct objects of a type that may be different than the current sets type.</param>
    /// <returns>
    /// Returns a new ISet containing every possible combination of the two ISet in Pairs 
    /// </returns>
    ISet<Pair<T, S>> CartesianProduct<S>(ISet<S> set_);


    /// <summary>
    /// Determines whether this set is a subset of the specified set.
    /// </summary>
    /// <param name="set_">The set.</param>
    /// <returns>
    /// 	<c>true</c> if this set is a subset of the specified set; otherwise, <c>false</c>.
    /// </returns>
    bool IsSubsetOf(ISet<T> set_);

    /// <summary>
    /// Add the items in the collection to the set if they are not already
    /// included in the current set.
    /// </summary>
    void AddRange(IEnumerable<T> collection);
  }
}
