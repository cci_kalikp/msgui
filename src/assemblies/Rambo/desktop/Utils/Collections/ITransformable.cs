namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections
{
  /// <summary>
  /// An interface for converting values from one type to another if the conversion is valid.
  /// </summary>
  /// <typeparam name="InType">The type of the input value to be transformed.</typeparam>
  /// <typeparam name="OutType">The type of the transformed output value.</typeparam>
  public interface ITransformable<InType, OutType>
  {
    /// <summary>
    /// Transforms the specified input to the specified output type if the conversion is valid.
    /// </summary>
    /// <param name="input">The input.</param>
    /// <param name="output">The output.</param>
    /// <returns><c>true</c> if the conversion is valid; otherwise <c>false</c>.</returns>
    bool Transform(InType input, out OutType output);
  }
}