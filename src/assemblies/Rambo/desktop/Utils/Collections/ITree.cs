using System;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections
{
  public interface ITree
   : ITreeEnumerator, ITreeNodeSearch
  {
    /// <summary>
    /// Gets the total number of nodes in the tree.
    /// </summary>
    /// <value>The node count.</value>
    int Nodes { get; }

    /// <summary>
    /// Gets the total number of levels in the tree.
    /// </summary>
    /// <value>The level count.</value>
    int Levels { get; }

    /// <summary>
    /// Gets the list of nodes at the root level (level 0).
    /// </summary>
    /// <value>The root node list.</value>
    ITreeNodeList RootNodeList { get; }

    /// <summary>
    /// Occurs when the structure of tree has changed.
    /// </summary>
    event EventHandler<TreeChangedEventArgs> TreeChanged;
  }
}