﻿using System;
using System.Collections;
using System.Threading;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections.TypedLists
{
  internal sealed class StringList : IList
  {
    #region Constants

    private const int DEFAULT_SIZE = 2048;

    #endregion

    #region Readonly & Static Fields

    private readonly object _syncRoot = new object();

    #endregion

    #region Fields

    private string[] _array;
    private int _size;
    private readonly ReaderWriterLockSlim _resizeReadWriteLock = new ReaderWriterLockSlim();
    #endregion

    #region Constructors

    internal StringList()
    {
    }

    internal StringList(int capacity_)
    {
      _size = capacity_;
    }

    #endregion

    #region Instance Methods

    private void CheckCapacity()
    {
      lock (_syncRoot)
      {
        if (_array == null) return;
        if (_size == _array.Length)
        {
          Resize();
        }
      }
    }

    private int IndexOf(string value_)
    {
      lock (_syncRoot)
      {
        return _array != null ? Array.FindIndex(_array, x => x == value_) : ((value_ == null) ? 0 : -1);
      }
    }

    private void Resize()
    {
      Resize(_array.Length + 1 + DEFAULT_SIZE);
    }
    
    private void Resize(int newSize_)
    {
      lock (_syncRoot)
      {
        if (_array != null)
        {
          var newArray = new string[newSize_];
          try
          {
            _resizeReadWriteLock.EnterWriteLock();
            Array.Copy(_array, 0, newArray, 0, _size);
            _array = newArray;
          }
          finally 
          {
            _resizeReadWriteLock.ExitWriteLock(); 
          }
        }
      }
    }

    #endregion

    #region IList Members

    IEnumerator IEnumerable.GetEnumerator()
    {
      return new ListEnumerator(this);
    }

    void ICollection.CopyTo(Array array_, int idx_)
    {
      lock (_syncRoot)
      {
        if (_array != null)
        {
          Array.Copy(_array, 0, array_, idx_, _size);
        }
        else
        {
          for (int i = 0; i < _size; i++)
          {
            array_.SetValue(null, i + idx_);
          }
        }
      }
    }

    int ICollection.Count
    {
      get { return _size; }
    }

    object ICollection.SyncRoot
    {
      get { return this; }
    }

    bool ICollection.IsSynchronized
    {
      get { return false; }
    }

    int IList.Add(object value_)
    {
      lock (_syncRoot)
      {
        CheckCapacity();

        if (_array != null)
        {
          _array[_size] = value_ as string;
        }
        else
        {
          if (value_ != null)
          {
            ((IList)this)[_size] = value_;
          }
        }
        return _size++;
      }
    }

    bool IList.Contains(object value)
    {
      return IndexOf(value as string) >= 0;
    }

    void IList.Clear()
    {
      lock (_syncRoot)
      {
        _resizeReadWriteLock.EnterWriteLock();
        try
        {
          _array = null;
        }
        finally
        {
          _resizeReadWriteLock.ExitWriteLock();
        }
      }
    }

    int IList.IndexOf(object value)
    {
      return IndexOf(value as string);
    }

    void IList.Insert(int idx_, object value_)
    {
      lock (_syncRoot)
      {
        if (_size == _array.Length)
        {
          Resize();
        }
        _size++;

        if (_array != null)
        {
          for (int i = _size - 1; i > idx_; --i)
          {
            _array[i] = _array[i - 1];
          }
          _array[idx_] = value_ as string;
        }
        else
        {
          if (value_ != null)
          {
            ((IList)this)[idx_] = value_;
          }
        }
      }
    }

    void IList.Remove(object value_)
    {
      int idx = IndexOf(value_ as string);

      if (idx >= 0)
      {
        ((IList) this).RemoveAt(idx);
      }
    }

    void IList.RemoveAt(int idx_)
    {
      lock (_syncRoot)
      {
        if (idx_ >= _size)
        {
          throw new IndexOutOfRangeException();
        }
        _size--;

        if (_array != null)
        {
          for (int i = idx_; i < _size; ++i)
          {
            _array[i] = _array[i + 1];
          }
        }
      }
    }

    object IList.this[int pos_]
    {
      get { return (_array == null) ? null : _array[pos_]; }
      set
      {
        if (_array == null)
        {
          if (value != null)
          {
            lock (_syncRoot)
            {
              try
              {
                _resizeReadWriteLock.EnterWriteLock();
                if (_array == null)
                {
                  _array = new string[_size + 1];
                }
              }
              finally
              {
                _resizeReadWriteLock.ExitWriteLock();
              }
            }
          }
          else
          {
            return;
          }
        }

        try
        {
          _resizeReadWriteLock.EnterReadLock();
          _array[pos_] = value as string;
        }
        finally
        {
          _resizeReadWriteLock.ExitReadLock();
        }
      }
    }

    bool IList.IsReadOnly
    {
      get { return false; }
    }

    bool IList.IsFixedSize
    {
      get { return false; }
    }

    #endregion
  }
}