namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections.TypedLists
{
  internal class IntParser : IParser
  {
    public bool TryParse<T>(string parse_, out T value_)
    {
      int i;
      bool ret = int.TryParse(parse_, out i);
      value_ = (T)(object)i;
      return ret;
    }
  }

}