﻿///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2005 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
///////////////////////////////////////////////////////////////////////////////
//
//       Filename:  $File: //eai/msdotnet/msgui/trunk/assemblies/Rambo/desktop/Utils/Collections/TypedLists/Int32List.cs $ 
//       Revision:  $Revision: #3 $ 
// Last edited by:  $Author: boqwang $ 
//             on:  $Date: 2013/02/28 $ 
//
// $Id: //eai/msdotnet/msgui/trunk/assemblies/Rambo/desktop/Utils/Collections/TypedLists/Int32List.cs#3 $ 
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections.TypedLists
{
  /* 
   * *DO NOT MODIFY*
   * 
   * Generated from TemplateList.cs
   * 
   * To make changes, edit TemplateList.cs followed by "make primitive_lists" 
   * to regenerate all primitive list implementations.
   */

  /// <summary>
  /// Efficiently stores Int32 data values. Speciallized
  /// accessors and mutators are provided; if these are used there
  /// is no boxing during reads and modifications, respectively.
  /// </summary>
  public class Int32List : IEnumerable, ICollection, IList, ITrimable, IPrimitiveList
  {
    public const int DEFAULT_SIZE = 2048;
    public const Int32 NULL_VALUE = Int32.MinValue;

    Int32[] _array;

    int _size = 0;

    /// <summary>
    /// Ctr specifying initial capacity.
    /// </summary>
    /// <param name="capacity_">initial size of the array storage.</param>
    public Int32List(int capacity_)
    {
      _array = new Int32[capacity_];
    }

    /// <summary>
    /// Default Ctr. 
    /// </summary>
    public Int32List()
      : this(DEFAULT_SIZE)
    {
    }

    public void CopyFrom(IList list_)
    {
      Int32List list = list_ as Int32List;
      int newSize = list == null
        ? list_.Count
        : list._array.Length;
      if (_array.Length < newSize)
        _array = new Int32[newSize];
      else if (_array.Length > newSize)
        Array.Clear(_array, newSize, _array.Length - newSize);
      if (list != null)
      {
        Array.Copy(list._array, _array, newSize);
      }
      else
      {
        for (int i = 0; i < newSize; ++i)
          _array[i] = (Int32)list_[i];
      }
      _size = newSize;
    }


    // Informational:

    public int Count { get { return _size; } }
    public bool IsFixedSize { get { return false; } }
    public bool IsReadOnly { get { return false; } }
    public bool IsSynchronized { get { return false; } }
    public object SyncRoot { get { return this; } }

    // Accessors and predicates:

    public bool Contains(object value) { return this.Contains((Int32)value); }
    public void CopyTo(Array array_, int idx_) { Array.Copy(_array, 0, array_, idx_, _size); }
    public int IndexOf(object value) { return this.IndexOf((Int32)value); }
    public IEnumerator GetEnumerator() { return new ListEnumerator(this); }
    object IList.this[int i]
    {
      get
      {
        Int32 val = this[i];
        return val == NULL_VALUE
          ? (object)System.DBNull.Value
          : (object)val;
      }
      set
      {
        if (value == null || value == System.DBNull.Value)
        {
          this[i] = NULL_VALUE;
        }
        else
        {
          String s = value as String;
          this[i] = s == null
            ? (Int32)value
            : Int32.Parse(s);
        }
      }
    }

    // Mutators:

    int IList.Add(object value)
    {
      if (value == null || value == System.DBNull.Value)
      {
        CheckCapacity();
        _array[_size] = NULL_VALUE;
        ++_size;
      }
      else
      {
        if (value is string)
        {
          Add(Int32.Parse((string)value));
        }
        else
        {
          Add((Int32)value);
        }
      }
      return _size - 1;
    }
    public void Clear() { _size = 0; }
    void IList.Insert(int idx_, object value) { Insert(idx_, (value is string) ? Int32.Parse((string)value) : (Int32)value); }
    void IList.Remove(object value) { this.Remove((Int32)value); }
    public void RemoveAt(int idx_)
    {
      if (idx_ >= _size)
        throw new IndexOutOfRangeException();
      --_size;
      for (int i = idx_; i < _size; ++i)
        _array[i] = _array[i + 1];
    }


    // Specialized functions:

    public int Capacity
    {
      get { return _array.Length; }
      set
      {
        if (value <= _size)
          throw new ArgumentOutOfRangeException("new capacity must be greater than Count");
        Resize(value);
      }
    }

    public void TrimToSize()
    {
      if (_size == _array.Length)
        return;
      Resize(_size);
    }

    public int Add(Int32 value_)
    {
      CheckCapacity();
      _array[_size] = value_;
      return _size++;
    }

    public int BinarySearch(Int32 value)
    {
      return Array.BinarySearch(_array, 0, _size, value);
    }

    public bool Contains(Int32 value)
    {
      return IndexOf(value) >= 0;
    }

    public int IndexOf(Int32 value)
    {
      for (int i = 0; i < _size; ++i)
        if (_array[i] == value)
          return i;
      return -1;
    }

    public void Insert(int idx_, Int32 value)
    {
      if (_size == _array.Length)
        Resize();
      ++_size;
      for (int i = _size - 1; i > idx_; --i)
        _array[i] = _array[i - 1];
      _array[idx_] = value;
    }

    public void Remove(Int32 value)
    {
      int idx = this.IndexOf(value);
      if (idx >= 0)
        ((IList)this).RemoveAt(idx);
    }

    public Int32 this[int i]
    {
      get
      {
        return _array[i];
      }
      set
      {
        if (i >= _size)
          throw new IndexOutOfRangeException();
        _array[i] = value;
      }
    }

    protected void ResizeToMinimum(int minNewSize)
    {
      int standardRezizeLength = DEFAULT_SIZE + _array.Length + 1;
      int newSize = (standardRezizeLength > minNewSize) ? standardRezizeLength : DEFAULT_SIZE + minNewSize + 1;

      Resize(newSize);
    }

    protected void CheckCapacity()
    {
      if (_size == _array.Length)
        Resize();
    }

    protected void Resize()
    {
      Resize(DEFAULT_SIZE + _array.Length + 1);
    }

    protected void Resize(int newSize_)
    {
      Int32[] newArray = new Int32[newSize_];
      Array.Copy(_array, 0, newArray, 0, _size);
      _array = newArray;
    }
  }
}
