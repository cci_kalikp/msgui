﻿using System;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections.TypedLists
{
  ///<summary>
  ///</summary>
  ///<typeparam name="T"></typeparam>
  public class DictionarySparseList<T>: IList
  {
    private int _maxIndex;
    private readonly object _syncRoot = new object();
    private readonly Dictionary<int, T> _dictionary = new Dictionary<int,T>();
    private readonly T _defaultValue;

    ///<summary>
    ///</summary>
    ///<param name="defaultValue_"></param>
    public DictionarySparseList( T defaultValue_)
    {
      _defaultValue = defaultValue_;
    }
    public IEnumerator GetEnumerator()
    {
      throw new NotSupportedException();
    }

    public void CopyTo(Array array_, int index_)
    {
      throw new NotSupportedException();
    }

    public int Count
    {
      get { throw new NotSupportedException(); }
    }

    public object SyncRoot
    {
      get { return _syncRoot; }
    }

    public bool IsSynchronized
    {
      get { return false ; }
    }

    public int Add(object value_)
    {
      lock (_syncRoot)
      {
        _maxIndex++;
        return _maxIndex;
      }
    }

    public bool Contains(object value_)
    {
      throw new NotSupportedException();
    }

    public void Clear()
    {
      lock (_syncRoot)
      {
        _dictionary.Clear();
        _maxIndex = 0;
      }
    }

    public int IndexOf(object value_)
    {
      throw new NotSupportedException();
    }

    public void Insert(int index_, object value_)
    {
      this[index_] = value_;
    }

    public void Remove(object value_)
    {
      

      throw new NotSupportedException();
    }

    public void RemoveAt(int index_)
    {
      lock (SyncRoot)
      {
        if (_dictionary.ContainsKey(index_))
          _dictionary.Remove(index_);
      }
    }

    

    public object this[int index_]
    {
      get
      {
        lock (_syncRoot)
        {
          T outValue;
          if (!_dictionary.TryGetValue(index_, out outValue))
            return _defaultValue;
          return outValue;
        }
      }
      set
      {
        if (value == null) return;

        if (IsDefaultOrDBNull(value))
        {
          RemoveAt(index_);
          return;
        }

        lock (_syncRoot)
        {
          _dictionary[index_] = (T) value;
        }

      }
    }

    private bool IsDefaultOrDBNull(object value_)
    {
      return value_.Equals(_defaultValue) || value_ == DBNull.Value;
    }

    public bool IsReadOnly
    {
      get { return false; }
    }

    public bool IsFixedSize
    {
      get { return false; }
    }
  }
}
