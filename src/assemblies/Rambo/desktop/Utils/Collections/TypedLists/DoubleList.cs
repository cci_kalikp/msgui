﻿///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2005 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
///////////////////////////////////////////////////////////////////////////////
//
//       Filename:  $File: //eai/msdotnet/msgui/trunk/assemblies/Rambo/desktop/Utils/Collections/TypedLists/DoubleList.cs $ 
//       Revision:  $Revision: #3 $ 
// Last edited by:  $Author: boqwang $ 
//             on:  $Date: 2013/02/28 $ 
//
// $Id: //eai/msdotnet/msgui/trunk/assemblies/Rambo/desktop/Utils/Collections/TypedLists/DoubleList.cs#3 $ 
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Threading;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections.TypedLists
{
  /* 
   * *DO NOT MODIFY*
   * 
   * Generated from TemplateList.cs
   * 
   * To make changes, edit TemplateList.cs followed by "make primitive_lists" 
   * to regenerate all primitive list implementations.
   */

  /// <summary>
  /// Efficiently stores Double data values. Speciallized
  /// accessors and mutators are provided; if these are used there
  /// is no boxing during reads and modifications, respectively.
  /// </summary>
  public class DoubleList : IList, ITrimable, IPrimitiveList, IStorageAllocatable
  {

   

    public const int DEFAULT_SIZE = 2048;

    public const Double NULL_VALUE = Double.MinValue;

    double _defaultValue;

    Double[] _array = null;

    int _arrayLength = 0;

    int _size = 0;
    private readonly ReaderWriterLockSlim _resizeReadWriteLock = new ReaderWriterLockSlim();
    private object _syncRoot = new object();

    /// <summary>
    /// Ctr specifying initial capacity.
    /// </summary>
    /// <param name="capacity_">initial size of the array storage.</param>
    public DoubleList(double defaultValue_, int capacity_)
    {
      _arrayLength = capacity_;
      _defaultValue = defaultValue_;
    }

    /// <summary>
    /// Default Ctr. 
    /// </summary>
    public DoubleList(double defaultValue_)
      : this(defaultValue_, DEFAULT_SIZE)
    {
    }

    public void CopyFrom(IList list_)
    {
      lock (_syncRoot)
      {
        try
        {
          _resizeReadWriteLock.EnterWriteLock();
          DoubleList list = list_ as DoubleList;
          int newSize = (list == null) ? list_.Count : list._arrayLength;

          bool targetAllocated = (_array != null);
          bool sourceAllocated = (list != null) ? (list._array != null) : true;

          if (!targetAllocated)
          {
            if (!sourceAllocated)
            {
              // nothing to allocate
              _arrayLength = newSize;
              _size = newSize;
              return;
            }
            else
            {
              _array = new double[newSize];
              _arrayLength = newSize;
            }
          }

          if (_arrayLength < newSize)
          {
            _array = new double[newSize];
            for (int i = 0; i < _arrayLength; i++)
            {
              _array[i] = _defaultValue;
            }
            _arrayLength = newSize;

          }
          else if (_arrayLength > newSize)
          {
            Array.Clear(_array, newSize, _arrayLength - newSize);
          }

          if (list != null)
          {
            Array.Copy(list._array, _array, newSize);
          }
          else
          {
            for (int i = 0; i < newSize; ++i)
            {
              _array[i] = (double)list_[i];
            }
          }
          _size = newSize;
        }
        finally
        {
          _resizeReadWriteLock.ExitWriteLock();
        }
      }
    }

    // Informational:

    public bool IsAllocated
    {
      get
      {
        return (_array != null);
      }
    }

    public int Count { get { return _size; } }
    public bool IsFixedSize { get { return false; } }
    public bool IsReadOnly { get { return false; } }
    public bool IsSynchronized { get { return false; } }
    public object SyncRoot { get { return this; } }

    // Accessors and predicates:

    public bool Contains(object value_)
    {
      return this.Contains((double)value_);
    }

    public void CopyTo(Array array_, int idx_)
    {
      lock (_syncRoot)
      {
        if (_array != null)
        {
          Array.Copy(_array, 0, array_, idx_, _size);
        }
        else
        {
          for (int i = 0; i < _size; i++)
          {
            array_.SetValue(_defaultValue, i + idx_);
          }
        }
      }
    }

    public int IndexOf(object value)
    {
      return this.IndexOf((Double)value);
    }

    public IEnumerator GetEnumerator()
    {
      return new ListEnumerator(this);
    }

    object IList.this[int i]
    {
      get
      {
        double val = this[i];
        return (val == NULL_VALUE) ? DBNull.Value : (object)val;
      }
      set
      {
        if (value == null || value == DBNull.Value)
        {
          this[i] = NULL_VALUE;
        }
        else
        {
          String s = value as String;
          double setValue;
          if (s == null)
          {
            setValue = (double) value;
          }
          else
          {
            //defensive coding: +NaN could be received
            if (!Double.TryParse(s, out setValue))
            {
              setValue = Double.NaN;
            }
          }
          this[i] = setValue;
        }
      }
    }

    // Mutators:

    int IList.Add(object value_)
    {
      lock (_syncRoot)
      {
        if (value_ == null || value_ == DBNull.Value)
        {
          CheckCapacity();
          this[_size] = _defaultValue;
          _size++;
        }
        else
        {
          if (value_ is string)
          {
            Add(double.Parse((string)value_));
          }
          else
          {
            Add((double)value_);
          }
        }
        return _size - 1;
      }
    }

    public void Clear()
    {
      lock (_syncRoot)
      {
        _resizeReadWriteLock.EnterWriteLock();
        try
        {
          _array = null;
        }
        finally
        {
          _resizeReadWriteLock.ExitWriteLock();
        }
      }
    }

    void IList.Insert(int idx_, object value_)
    {
      Insert(idx_, (value_ is string) ? double.Parse((string)value_) : (double)value_);
    }

    void IList.Remove(object value_)
    {
      this.Remove((double)value_);
    }

    public void RemoveAt(int idx_)
    {
      lock (_syncRoot)
      {
        if (idx_ >= _size)
        {
          throw new IndexOutOfRangeException();
        }
        _size--;

        if (_array != null)
        {
          for (int i = idx_; i < _size; ++i)
          {
            _array[i] = _array[i + 1];
          }
        }
      }
    }

    // Specialized functions:

    public int Capacity
    {
      get
      {
        return _arrayLength;
      }
      set
      {
        lock (_syncRoot)
        {
          if (value <= _size)
          {
            throw new ArgumentOutOfRangeException("new capacity must be greater than Count");
          }
        }

        Resize(value);
      }
    }

    public void TrimToSize()
    {
      lock (_syncRoot)
      {
        if (_array != null)
        {
          if (_size == _arrayLength)
          {
            return;
          }
        }

        Resize(_size);
      }
    }

    public int Add(double value_)
    {
      lock (_syncRoot)
      {
        CheckCapacity();

        if (_array != null)
        {
          _array[_size] = value_;
        }
        else
        {
          if (value_ != _defaultValue)
          {
            this[_size] = value_;
          }
        }
        return _size++;
      }
    }

    public int BinarySearch(double value_)
    {
      lock (_syncRoot)
      {
        if (_array != null)
        {
          return Array.BinarySearch(_array, 0, _size, value_);
        }
        else
        {
          return (_size >= 0 && value_ == _defaultValue) ? 0 : -1;
        }
      }
    }

    public bool Contains(double value_)
    {
      return IndexOf(value_) >= 0;
    }

    public int IndexOf(double value_)
    {
      lock (_syncRoot)
      {
        if (_array != null)
        {
          for (int i = 0; i < _size; ++i)
          {
            if (_array[i] == value_)
            {
              return i;
            }
          }
          return -1;

        }
        else
        {
          return (value_ == _defaultValue) ? 0 : -1;
        }
      }
    }

    public void Insert(int idx_, double value_)
    {
      lock (_syncRoot)
      {
        if (_size == _arrayLength)
        {
          Resize();
        }
        _size++;

        if (_array != null)
        {
          for (int i = _size - 1; i > idx_; --i)
          {
            _array[i] = _array[i - 1];
          }
          _array[idx_] = value_;
        }
        else
        {
          if (value_ != _defaultValue)
          {
            this[idx_] = value_;
          }
        }
      }
    }

    public void Remove(double value_)
    {
      lock (_syncRoot)
      {
        int idx = this.IndexOf(value_);

        if (idx >= 0)
        {
          ((IList) this).RemoveAt(idx);
        }
      }
    }

    public double this[int pos_]
    {
      get
      {
        return (_array == null) ? _defaultValue : _array[pos_];
      }
      set
      {
        if (_array == null)
        {
          if (value != _defaultValue)
          {
            lock (_syncRoot)
            {
              try
              {
                _resizeReadWriteLock.EnterWriteLock();
                if (_array == null)
                {
                  _array = new double[_arrayLength];
                  for (int i = 0; i < _arrayLength; i++)
                  {
                    _array[i] = _defaultValue;
                  }
                }
              }
              finally
              {
                _resizeReadWriteLock.ExitWriteLock();
              }
            }
          }
          else
          {
            return;
          }
        }

        try
        {
          _resizeReadWriteLock.EnterReadLock();
          _array[pos_] = value;
        }
        finally
        {
          _resizeReadWriteLock.ExitReadLock();
        }
      }
    }

    protected void ResizeToMinimum(int minNewSize_)
    {
      int standardRezizeLength = DEFAULT_SIZE + _arrayLength + 1;

      int newSize = (standardRezizeLength > minNewSize_) ?
          standardRezizeLength : DEFAULT_SIZE + minNewSize_ + 1;

      Resize(newSize);
    }

    protected void CheckCapacity()
    {
      lock (_syncRoot)
      {
        if (_size == _arrayLength)
        {
          Resize();
        }
      }
    }

    protected void Resize()
    {
      Resize(_arrayLength + 1 + DEFAULT_SIZE);
    }


    protected void Resize(int newSize_)
    {
      lock (_syncRoot)
      {
        try
        {
          _resizeReadWriteLock.EnterWriteLock();
          if (_array != null)
          {
            double[] newArray = new double[newSize_];
            Array.Copy(_array, 0, newArray, 0, _size);
            _array = newArray;
          }
          _arrayLength = newSize_;
        }
        finally
        {
          _resizeReadWriteLock.ExitWriteLock();
        }
      }
    }
  }
}
