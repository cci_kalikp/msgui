using System;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections.TypedLists
{
  public static class NullValue<T>
  {
    private readonly static T _nullValue;


    static NullValue()
    {
      Type type = typeof(T);

      if (type == typeof(double))
      {
        _nullValue = (T)(object)double.NaN;
      }
      else if (type == typeof(string))
      {
        _nullValue = (T)(object)string.Empty;
      }
      else if (type == typeof(DateTime))
      {
        _nullValue = (T)(object)DateTime.MinValue;
      }
      else if (type == typeof(int))
      {
        _nullValue = (T)(object)int.MinValue;
      }
      else if (type == typeof(float))
      {
        _nullValue = (T)(object)float.NaN;
      }
      else
      {
        _nullValue = default(T); 
      }
    }

    public static T GetNullValue()
    {
      return _nullValue;
    }

    public static bool IsNullValue(T value)
    {
      return Equals(value, _nullValue);
    }
  }
}
