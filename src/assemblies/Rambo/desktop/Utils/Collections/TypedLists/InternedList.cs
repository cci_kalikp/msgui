﻿///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2005 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
///////////////////////////////////////////////////////////////////////////////
//
//       Filename:  $File: //eai/msdotnet/msgui/trunk/assemblies/Rambo/desktop/Utils/Collections/TypedLists/InternedList.cs $ 
//       Revision:  $Revision: #3 $ 
// Last edited by:  $Author: boqwang $ 
//             on:  $Date: 2013/02/28 $ 
//
// $Id: //eai/msdotnet/msgui/trunk/assemblies/Rambo/desktop/Utils/Collections/TypedLists/InternedList.cs#3 $ 
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Specialized;
using System.Collections;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections.TypedLists
{
  /// <summary>
  /// Decorates a list to store only interned values. Tests for equality are
  /// performed using objects' Equals() method.
  /// </summary>
  public class InternedList : ListDelegate, ITrimable
  {
    #region InternedMapCollection class

    /// <summary>
    /// Singleton class for inerning objects (that is, storing pointers to single 
    /// instances of duplicate values).
    /// </summary>
    private class InternedMapCollection
    {
      private static IDictionary _typeMaps;
      private static InternedMapCollection _instance = new InternedMapCollection();

      static InternedMapCollection()
      {
        _typeMaps = new ListDictionary();
        _typeMaps[typeof(string)] = new Hashtable();
      }

      public static InternedMapCollection Instance
      {
        get { return _instance; }
      }

      public object this[object obj_]
      {
        get
        {
          if (obj_ == null)
            return obj_;

          IDictionary typemap = (IDictionary)_typeMaps[obj_.GetType()];
          if (typemap == null)
          {
            lock (_typeMaps)
            {
              typemap = (IDictionary)_typeMaps[obj_.GetType()];
              if (typemap == null)
                _typeMaps[obj_.GetType()] = typemap = new Hashtable();
            }
          }
          object internedObj = typemap[obj_];
          if (internedObj == null)
          {
            lock (typemap)
            {
              internedObj = typemap[obj_];
              if (internedObj == null)
                typemap[obj_] = internedObj = obj_;
            }
          }
          return internedObj;
        }
      }
    }
    #endregion InternedMapCollection class

    /// <summary>
    /// Ctr.
    /// </summary>
    /// <param name="list_">List to decorate.</param>
    public InternedList(IList list_)
      : base(list_)
    {
    }

    /// <summary>
    /// Ctr.
    /// </summary>
    public InternedList(int capacity_)
      : this(new ArrayList(capacity_))
    {
    }

    /// <summary>
    /// Ctr.
    /// </summary>
    public InternedList()
      : this(new ArrayList())
    {
    }

    public virtual void TrimToSize()
    {
      ArrayList al = _delegate as ArrayList;
      if (al != null)
        al.TrimToSize();
      else
      {
        ITrimable tl = _delegate as ITrimable;
        if (tl != null)
          tl.TrimToSize();
      }
    }

    /// <summary>
    /// Stores an interned version of the object provided.
    /// </summary>
    /// <param name="o_">object to be stored.</param>
    /// <returns>position at which the object was inserted</returns>
    public override int Add(object o_)
    {
      o_ = InternedMapCollection.Instance[o_];
      return base.Add(o_);
    }

    /// <summary>
    /// Stores an interned version of the object provided.
    /// </summary>
    /// <param name="idx_">position at which the object is to be inserted</param>
    /// <param name="o_">object to be stored.</param>
    public override void Insert(int idx_, object o_)
    {
      o_ = InternedMapCollection.Instance[o_];
      base.Insert(idx_, o_);
    }
  }
}
