using System;
using System.Globalization;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections.TypedLists
{
  internal class DateTimeParser : IParser
  {
    private static readonly IFormatProvider _culture = new CultureInfo("en-US", true);
    private static readonly string[] _supportedDateFormats = new string[]
      {
        "d", "D", "g", "G", @"yyyyMMdd", @"yyyyMMdd/HH:mm/UTC"
      };

    public bool TryParse<T>(string parse_, out T value_)
    {      
      DateTime dateTime;

      // Try custom formats first
      if (DateTime.TryParseExact(parse_, _supportedDateFormats,
        _culture, DateTimeStyles.None, out dateTime))
      {
        value_ = (T)(object)dateTime;
        return true;
      }

      // Otherwise try ISO formats
      if (DateTime.TryParse(parse_, out dateTime))
      {
        value_ = (T)(object)dateTime;
        return true;
      }
      else
      {
        value_ = (T)(object)DateTime.Now;
        return false;
      }
    }
  }

}