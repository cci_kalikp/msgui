﻿///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2005 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
///////////////////////////////////////////////////////////////////////////////
//
//       Filename:  $File: //eai/msdotnet/msgui/trunk/assemblies/Rambo/desktop/Utils/Collections/TypedLists/DateTimeList.cs $ 
//       Revision:  $Revision: #3 $ 
// Last edited by:  $Author: boqwang $ 
//             on:  $Date: 2013/02/28 $ 
//
// $Id: //eai/msdotnet/msgui/trunk/assemblies/Rambo/desktop/Utils/Collections/TypedLists/DateTimeList.cs#3 $ 
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Globalization;
using System.Threading;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections.TypedLists
{
  /* 
   * *DO NOT MODIFY*
   * 
   * Generated from TemplateList.cs
   * 
   * To make changes, edit TemplateList.cs followed by "make primitive_lists" 
   * to regenerate all primitive list implementations.
   */

  /// <summary>
  /// Efficiently stores DateTime data values. Speciallized
  /// accessors and mutators are provided; if these are used there
  /// is no boxing during reads and modifications, respectively.
  /// </summary>
  public class DateTimeList : IEnumerable, ICollection, IList, ITrimable, IPrimitiveList
  {
    public const int DEFAULT_SIZE = 2048;
    public readonly DateTime NULL_VALUE = DateTime.MinValue;
    private IFormatProvider culture = new CultureInfo("en-US", true);
    public static readonly string[] SUPPORTED_DATE_FORMATS = new string[] { "d", "D", "g", "G", @"yyyyMMdd", @"yyyyMMdd/HH:mm/UTC", @"yyyyMMdd/HH:mm" };

    DateTime[] _array;

    int _size = 0;

    private readonly ReaderWriterLockSlim _resizeReadWriteLock = new ReaderWriterLockSlim();
    private object _syncRoot = new object();

    /// <summary>
    /// Ctr specifying initial capacity.
    /// </summary>
    /// <param name="capacity_">initial size of the array storage.</param>
    public DateTimeList(int capacity_)
    {
      _array = new DateTime[capacity_];
    }

    /// <summary>
    /// Default Ctr. 
    /// </summary>
    public DateTimeList()
      : this(DEFAULT_SIZE)
    {
    }

    public void CopyFrom(IList list_)
    {
      lock (_syncRoot)
      {
        try
        {
          _resizeReadWriteLock.EnterWriteLock();
          DateTimeList list = list_ as DateTimeList;
          int newSize = list == null
                          ? list_.Count
                          : list._array.Length;
          if (_array.Length < newSize)
            _array = new DateTime[newSize];
          else if (_array.Length > newSize)
            Array.Clear(_array, newSize, _array.Length - newSize);
          if (list != null)
          {
            Array.Copy(list._array, _array, newSize);
          }
          else
          {
            for (int i = 0; i < newSize; ++i)
              _array[i] = (DateTime) list_[i];
          }
          _size = newSize;
        }
        finally
        {
          _resizeReadWriteLock.ExitWriteLock();
        }
      }
    }


    // Informational:

    public int Count { get { return _size; } }
    public bool IsFixedSize { get { return false; } }
    public bool IsReadOnly { get { return false; } }
    public bool IsSynchronized { get { return false; } }
    public object SyncRoot { get { return this; } }

    // Accessors and predicates:

    public bool Contains(object value) { return this.Contains((DateTime)value); }
    public void CopyTo(Array array_, int idx_) 
    {
      lock (_syncRoot)
      {
        Array.Copy(_array, 0, array_, idx_, _size);
      }
    }
    public int IndexOf(object value) { return this.IndexOf((DateTime)value); }
    public IEnumerator GetEnumerator() { return new ListEnumerator(this); }
    object IList.this[int i]
    {
      get
      {
        DateTime val = this[i];
        return val == NULL_VALUE
          ? (object)System.DBNull.Value
          : (object)val;
      }
      set
      {
        String s = value as String;

        if (value == null || value == System.DBNull.Value || s == string.Empty)
        {
          this[i] = NULL_VALUE;
        }
        else
        {
          this[i] = s == null
            ? (DateTime)value
            : Parse(s);
        }
      }
    }

    protected DateTime Parse(string dateTimeString_)
    {
      DateTime dateTime;

      // Try custom formats first
      if(DateTime.TryParseExact(dateTimeString_, SUPPORTED_DATE_FORMATS, 
        culture, DateTimeStyles.None, out dateTime))
      {
        return dateTime;
      }

      // Otherwise try ISO formats
      if(DateTime.TryParse(dateTimeString_, out dateTime))
      {
        return dateTime;
      }
      else
      {
        throw new FormatException(string.Format("Unable to parse datetime string: {0}", dateTimeString_));
      }

    }

    // Mutators:

    int IList.Add(object value)
    {
      lock (_syncRoot)
      {
        if (value == null || value == System.DBNull.Value)
        {
          CheckCapacity();
          _array[_size] = NULL_VALUE;
          ++_size;
        }
        else
        {
          if (value is string)
          {
            Add(Parse((string) value));
          }
          else
          {
            Add((DateTime) value);
          }
        }
        return _size - 1;
      }
    }
    public void Clear()
    {
      lock (_syncRoot)
      {
        _resizeReadWriteLock.EnterWriteLock();
        try
        {
          _array = new DateTime[_size];
        }
        finally
        {
          _resizeReadWriteLock.ExitWriteLock();
        }
      }
    }
    void IList.Insert(int idx_, object value) { Insert(idx_, (value is string) ? Parse((string)value) : (DateTime)value); }
    void IList.Remove(object value) { this.Remove((DateTime)value); }
    public void RemoveAt(int idx_)
    {
      lock (_syncRoot)
      {
        if (idx_ >= _size)
          throw new IndexOutOfRangeException();
        --_size;
        for (int i = idx_; i < _size; ++i)
          _array[i] = _array[i + 1];
      }
    }


    // Specialized functions:

    public int Capacity
    {
      get { return _array.Length; }
      set
      {
        if (value <= _size)
          throw new ArgumentOutOfRangeException("new capacity must be greater than Count");
        Resize(value);
      }
    }

    public void TrimToSize()
    {
      lock (_syncRoot)
      {

        if (_array != null)
        {
          if (_size == _array.Length)
            return;
          Resize(_size);
        }
      }
    }

    public int Add(DateTime value_)
    {
      lock (_syncRoot)
      {
        CheckCapacity();
        _array[_size] = value_;
        return _size++;
      }
    }

    public int BinarySearch(DateTime value)
    {
      lock (_syncRoot)
      {
        return Array.BinarySearch(_array, 0, _size, value);  
      }
    }

    public bool Contains(DateTime value)
    {
      return IndexOf(value) >= 0;
    }

    public int IndexOf(DateTime value)
    {
      lock (_syncRoot)
      {
        for (int i = 0; i < _size; ++i)
          if (_array[i] == value)
            return i;
        return -1;
      }
    }

    public void Insert(int idx_, DateTime value)
    {
      lock (_syncRoot)
      {
        if (_size == _array.Length)
          Resize();
        ++_size;
        for (int i = _size - 1; i > idx_; --i)
          _array[i] = _array[i - 1];
        _array[idx_] = value;
      }
    }

    public void Remove(DateTime value)
    {
      lock (_syncRoot)
      {
        int idx = this.IndexOf(value);
        if (idx >= 0)
          ((IList) this).RemoveAt(idx);
      }
    }

    public DateTime this[int i]
    {
      get
      {
        return _array[i];
      }
      set
      {
        try
        {
          _resizeReadWriteLock.EnterReadLock();
          if (i >= _size)
            throw new IndexOutOfRangeException();
          _array[i] = value;
        }
        finally 
        {
          _resizeReadWriteLock.ExitReadLock();
        }
        
      }
    }

    protected void ResizeToMinimum(int minNewSize)
    {
      int standardRezizeLength = DEFAULT_SIZE + _array.Length + 1;
      int newSize = (standardRezizeLength > minNewSize) ? standardRezizeLength : DEFAULT_SIZE + minNewSize + 1;

      Resize(newSize);
    }

    protected void CheckCapacity()
    {
      lock (_syncRoot)
      {
        if (_size == _array.Length)
          Resize();
      }
    }

    protected void Resize()
    {
      Resize(DEFAULT_SIZE + _array.Length + 1);
    }

    protected void Resize(int newSize_)
    {
      lock (_syncRoot)
      {
        try
        {
          _resizeReadWriteLock.EnterWriteLock();
          DateTime[] newArray = new DateTime[newSize_];
          Array.Copy(_array, 0, newArray, 0, _size);
          _array = newArray;
        }
        finally
        {
          _resizeReadWriteLock.ExitWriteLock();
        }
      }
    }
  }
}
