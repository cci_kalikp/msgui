﻿using System;
using System.Collections;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections.TypedLists
{
  /// <summary>
  /// Provide assignment from another list.
  /// </summary>
  public interface IPrimitiveList
  {
    void CopyFrom(IList src_);
  }
}
