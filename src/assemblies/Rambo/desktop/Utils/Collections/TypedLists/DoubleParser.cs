namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections.TypedLists
{
  internal class DoubleParser : IParser
  {
    public bool TryParse<T>(string parse_, out T value_)
    {
      double d;
      bool ret = double.TryParse(parse_, out d);
      value_ = (T)(object)d;
      return ret;
    }
  }
}