﻿///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2005 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
///////////////////////////////////////////////////////////////////////////////
//
//       Filename:  $File: //eai/msdotnet/msgui/trunk/assemblies/Rambo/desktop/Utils/Collections/TypedLists/ListDelegate.cs $ 
//       Revision:  $Revision: #3 $ 
// Last edited by:  $Author: boqwang $ 
//             on:  $Date: 2013/02/28 $ 
//
// $Id: //eai/msdotnet/msgui/trunk/assemblies/Rambo/desktop/Utils/Collections/TypedLists/ListDelegate.cs#3 $ 
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections.TypedLists
{


  /// <summary>
  /// Base class implementation of a list delegator, suitable for inheritence
  /// by Decorator-type classes. 
  /// </summary>
  public class ListDelegate : IList
  {
    protected IList _delegate;

    public ListDelegate(IList list_)
    {
      _delegate = list_;
    }

    public IList Delegate
    {
      get { return _delegate; }
      set { _delegate = value; }
    }

    // Informational:

    public virtual int Count { get { return _delegate.Count; } }
    public virtual bool IsFixedSize { get { return _delegate.IsFixedSize; } }
    public virtual bool IsReadOnly { get { return _delegate.IsReadOnly; } }
    public virtual bool IsSynchronized { get { return _delegate.IsSynchronized; } }
    public virtual object SyncRoot { get { return _delegate.SyncRoot; } }

    // Accessors and predicates:

    public virtual bool Contains(object value_) { return _delegate.Contains(value_); }
    public virtual void CopyTo(Array array_, int idx_) { _delegate.CopyTo(array_, idx_); }
    public virtual IEnumerator GetEnumerator() { return new ListEnumerator(this); }  // Overriden as the delegate will usually return an enumerator which wraps itself...
    public virtual int IndexOf(object value_) { return _delegate.IndexOf(value_); }
    public virtual object this[int idx_]
    {
      get { return _delegate[idx_]; }
      set { _delegate[idx_] = value; }
    }

    // Mutators:

    public virtual int Add(object o) { return _delegate.Add(o); }
    public virtual void Clear() { _delegate.Clear(); }
    public virtual void Insert(int i, object o) { _delegate.Insert(i, o); }
    public virtual void Remove(object v_) { _delegate.Remove(v_); }
    public virtual void RemoveAt(int idx_) { _delegate.RemoveAt(idx_); }

  }
}
