using System.Collections.Generic;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections.TypedLists
{
  /// <summary>
  /// This method of storage is favorable when there are gaps in the data.  
  /// There will be a saving in memory but an expense on access time as compared to array indexing.
  /// </summary>
  /// <typeparam name="T"></typeparam>
  internal class SparseStorage<T> : IColumnStorage<T>
  {
    #region Readonly & Static Fields

    private static readonly T _NullValue = NullValue<T>.GetNullValue();

    #endregion

    #region Fields

    private Dictionary<int, T> _dictionary;
    private object _syncRoot = new object();

    #endregion

    #region IColumnStorage<T> Members

    T IColumnStorage<T>.this[int index]
    {
      get
      {
        lock (_syncRoot)
        {
          T value;
          if (_dictionary == null || !_dictionary.TryGetValue(index, out value))
          {
            return _NullValue;
          }

          return value;
        }
      }
      set
      {
        lock (_syncRoot)
        {
          bool isNullValue = NullValue<T>.IsNullValue(value);

          if (_dictionary == null)
          {
            if (isNullValue)
            {
              return;
            }

            _dictionary = new Dictionary<int, T>();
          }


          if (isNullValue)
          {
            _dictionary.Remove(index);
          }
          else
          {
            _dictionary[index] = value;
          }
        }
      }
    }

    void IColumnStorage<T>.ClearValue(int index)
    {
      lock (_syncRoot)
      {
        if (_dictionary != null)
        {
          _dictionary.Remove(index);
        }
      }
    }

    void IColumnStorage<T>.Clear()
    {
      lock (_syncRoot)
      {
        _dictionary.Clear();
        _dictionary = null;
      }
    }

    #endregion
  }
}