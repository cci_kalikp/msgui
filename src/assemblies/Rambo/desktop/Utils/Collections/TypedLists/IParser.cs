namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections.TypedLists
{
  internal interface IParser
  {
    bool TryParse<T>(string toParse_, out T value_);
  }
}