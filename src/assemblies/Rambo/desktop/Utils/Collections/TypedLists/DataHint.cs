﻿namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections.TypedLists
{
  public enum DataHint
  {
    /// <summary>
    /// Will assume array based storage
    /// </summary>
    NONE,  

    /// <summary>
    /// Used when data has high frequency of gaps.  Slower access time, but more efficient on memory consumption.
    /// </summary>
    SPARSE,

    /// <summary>
    /// Used when the same values are repeated with high frequency - stores distinct values once.
    /// </summary>
    REPETITIVE
  }
}
