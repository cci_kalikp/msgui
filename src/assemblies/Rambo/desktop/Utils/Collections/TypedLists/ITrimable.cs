﻿using System;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections.TypedLists
{
  /// <summary>
  /// Indicates that a list's storage capacity may be trimmed to its current size
  /// </summary>
  public interface ITrimable
  {
    void TrimToSize();
  }
}
