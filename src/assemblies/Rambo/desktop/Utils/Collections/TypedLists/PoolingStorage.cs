using System.Collections.Generic;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections.TypedLists
{
  public class PoolingStorage<T> : IColumnStorage<T>
  {
    #region Readonly & Static Fields

    private static readonly T _NullValue = NullValue<T>.GetNullValue();

    #endregion

    #region Fields

    private List<T> _dataPool;
    private Dictionary<int, int> _indexMap;
    private object _syncRoot = new object();
    private Dictionary<T, int> _valueMap;

    #endregion

    #region IColumnStorage<T> Members

    T IColumnStorage<T>.this[int index]
    {
      get
      {
        lock (_syncRoot)
        {
          if (_indexMap != null)
          {
            int poolIndex;
            if (_indexMap.TryGetValue(index, out poolIndex))
            {
              return _dataPool[poolIndex];
            }
          }
        }
        return _NullValue;
      }
      set
      {
        lock (_syncRoot)
        {
          if (NullValue<T>.IsNullValue(value))
          {
            if (_indexMap != null)
            {
              _indexMap.Remove(index);
            }

            return;
          }

          if (_indexMap == null)
          {
            _indexMap = new Dictionary<int, int>();
            _valueMap = new Dictionary<T, int>();
            _dataPool = new List<T>();
          }

          int poolIndex;
          if (!_valueMap.TryGetValue(value, out poolIndex))
          {
            poolIndex = _dataPool.Count;
            _dataPool.Add(value);
            _valueMap.Add(value, poolIndex);
          }

          _indexMap[index] = poolIndex;
        }
      }
    }

    void IColumnStorage<T>.ClearValue(int index)
    {
      lock (_syncRoot)
      {
        if (_indexMap != null)
        {
          _indexMap.Remove(index);
        }
      }
    }

    void IColumnStorage<T>.Clear()
    {
      lock (_syncRoot)
      {
        if (_indexMap != null)
        {
          _indexMap.Clear();
          _valueMap.Clear();
          _dataPool.Clear();
        }

        _indexMap = null;
        _valueMap = null;
        _dataPool = null;
      }
    }

    #endregion
  }
}