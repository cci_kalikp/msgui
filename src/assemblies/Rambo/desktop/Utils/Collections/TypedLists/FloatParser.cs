namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections.TypedLists
{
  internal class FloatParser : IParser
  {
    public bool TryParse<T>(string parse_, out T value_)
    {
      float f;
      bool ret = float.TryParse(parse_, out f);
      value_ = (T)(object)f;
      return ret;
    }
  }

}