using System;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections.TypedLists
{
  internal class ArrayStorage<T> : IColumnStorage<T>
  {
    #region Constants

    private const int RESIZING_INTERVAL = 2048;

    #endregion

    #region Readonly & Static Fields

    private static readonly T _NullValue = NullValue<T>.GetNullValue();
    private readonly object _syncRoot = new object();

    #endregion

    #region Fields

    private T[] _array = null;

    #endregion

    #region IColumnStorage<T> Members

    T IColumnStorage<T>.this[int index]
    {
      get
      {
        lock (_syncRoot)
        {
          if (_array == null || index >= _array.Length)
          {
            return _NullValue;
          }

          return _array[index];
        }
      }
      set
      {
        lock (_syncRoot)
        {
          if (_array == null || index >= _array.Length)
          {
            // don't bother resizing/creating array for null values.
            if (NullValue<T>.IsNullValue(value))
            {
              return;
            }

            _array = ResizeArray(_array, index);
          }

          _array[index] = value;
        }
      }
    }


    void IColumnStorage<T>.ClearValue(int index)
    {
      lock (_syncRoot)
      {
        if (_array != null && _array.Length > index)
        {
          _array[index] = _NullValue;
        }
      }
    }

    void IColumnStorage<T>.Clear()
    {
      lock (_syncRoot)
      {
        _array = null;
      }
    }

    #endregion

    #region Static Methods

    private static T[] ResizeArray(T[] arrayBefore_, int minIndex_)
    {
      // this computes the size of the array so that it can contain at least
      // the minIndex_, but is sized in increments of RESIZING_INTERVAL
      int size = ((minIndex_/RESIZING_INTERVAL) + 1)*RESIZING_INTERVAL;
      T[] arrayAfter = new T[size];

      int i = 0;
      if (arrayBefore_ != null)
      {
        i = arrayBefore_.Length;
        Array.Copy(arrayBefore_, arrayAfter, i);
      }

      while (i < arrayAfter.Length)
      {
        arrayAfter[i] = _NullValue;
        ++i;
      }

      return arrayAfter;
    }

    #endregion
  }
}