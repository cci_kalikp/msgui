TypedLists

As of RR31, we use TypedList for data storage in RiskViewer, specifically DoubleList, StringList and DateTimeList. This implements IList.

Each SparseColumn has typed list (IList) as a storage.

These list has a special storage implementation:
Each type has a default value.
If the list only contains default values, there is no memory allocated, it just maintains a counter of the size. When you query a valid (and invalid!) index, you get default value back.
When we first time write a non default value into the list, it will allocate the necessary space, fill up with default values and write non default value to the specified index.

TypedList._size: maintains the actual size of the list.
TypedList._array: the array of values, if there were non default values written.


When a new row comes in from filter:

We add a row (SparseRowCollection.Add())
This increases the size of all the columns (SparseColumn.Add())
SparseColumn.Add() adds a default value to the typed list.
  If the list only contains default values, _size increases, _array stays null.
  If the list contains other values, i.e. _array is not null, _size is increased. The array is reallocated if necessary.


Clear column contents:

Starting from RR31, there is a need to clear column contents (for P&L Explain).

SparseColumn.Clear() calls Clear() of typed list. This must not set _size to zero as the cells are still reserved for the rows, we are just clearing the content. Leaving the size intact and setting the array to null ensures that default value will be returned for all indices.



We have the same, generic implementation in the Generic namespace, which we should analyze and use.