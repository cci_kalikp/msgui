﻿///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2005 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
///////////////////////////////////////////////////////////////////////////////
//
//       Filename:  $File: //eai/msdotnet/msgui/trunk/assemblies/Rambo/desktop/Utils/Collections/TypedLists/CompressedList.cs $ 
//       Revision:  $Revision: #3 $ 
// Last edited by:  $Author: boqwang $ 
//             on:  $Date: 2013/02/28 $ 
//
// $Id: //eai/msdotnet/msgui/trunk/assemblies/Rambo/desktop/Utils/Collections/TypedLists/CompressedList.cs#3 $ 
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections.TypedLists
{
  /// <summary>
  /// Efficiently stores "sparse" data, that is, data which contains many null
  /// values. This list may be used as a decorator for other IList implementations.
  /// </summary>
  public class CompressedList : IList
  {
    // Actual data storage:
    private IList _wrapper;

    // Indexing array for bookkeeping - records, for each non-null value in the
    // data array, the externally visible index of the datum.
    private Int32List _index;

    // Extnerally visible size, which is always >= length of the index and data arrays.
    private int _size = 0;

    /// <summary>
    /// Default ctr.
    /// </summary>
    public CompressedList()
      : this(new ArrayList())
    {
      _index = new Int32List(((ArrayList)_wrapper).Capacity);
    }

    /// <summary>
    /// Ctr specifying initial capacity.
    /// </summary>
    /// <param name="capacity_">intial capacity of the internal lists</param>
    public CompressedList(int capacity_)
      : this(new ArrayList(capacity_))
    {
    }

    /// <summary>
    /// Decorator Ctr.
    /// </summary>
    /// <param name="impl_">List to be wrapped.</param>
    public CompressedList(IList impl_)
    {
      _wrapper = impl_;
      _index = new Int32List();
    }

    // Informational:

    public int Count { get { return _size; } }
    public bool IsFixedSize { get { return false; } }
    public bool IsReadOnly { get { return false; } }
    public bool IsSynchronized { get { return false; } }
    public object SyncRoot { get { return this; } }

    // Accessors and predicates:

    public bool Contains(Object value)
    {
      return IndexOf(value) >= 0;
    }

    public void CopyTo(Array array_, int idx_)
    {
      Array.Clear(array_, 0, array_.Length);
      for (int i = 0; i < _index.Count; ++i)
        array_.SetValue(_wrapper[i], idx_ + _index[i]);
    }

    public IEnumerator GetEnumerator()
    {
      return new ListEnumerator(this);
    }

    public int IndexOf(Object value)
    {
      for (int i = 0; i < _index.Count; ++i)
        if (_wrapper[i] == value)
          return _index[i];
      return -1;
    }

    public object this[int i]
    {
      get
      {
        if (i < 0 || i >= _size)
          throw new IndexOutOfRangeException();
        int idx = _index.BinarySearch(i);
        return idx >= 0
          ? _wrapper[idx]
          : null;
      }
      set
      {
        if (i < 0 || i >= _size)
          throw new IndexOutOfRangeException();
        int idx = _index.BinarySearch(i);
        if (idx >= 0)
        {
          _wrapper[idx] = value;
        }
        else
        {
          idx = ~idx;
          _wrapper.Insert(idx, value);
          _index.Insert(idx, i);
        }
      }
    }

    // Mutators:

    public int Add(object value)
    {
      if (value != null)
      {
        _index.Add(_wrapper.Count);
        _wrapper.Add(value);
      }
      return _size++;
    }

    public void Clear()
    {
      _size = 0;
      ((IList)_index).Clear();
      _wrapper.Clear();
    }

    public void Insert(int i, Object value)
    {
      if (i < 0 || i > _size)
        throw new IndexOutOfRangeException();
      int idx = _index.BinarySearch(i);
      if (idx < 0)
        idx = ~idx;

      _wrapper.Insert(idx, value);
      _index.Insert(idx, i);
      ++idx;
      for (; idx < _index.Count; ++idx)
        ++_index[idx];

      ++_size;
    }

    public void Remove(Object value)
    {
      int idx = this.IndexOf(value);
      if (idx >= 0)
        this.RemoveAt(idx);
    }

    public void RemoveAt(int i)
    {
      int idx = _index.BinarySearch(i);
      if (idx >= 0)
      {
        _index.RemoveAt(idx);
        _wrapper.RemoveAt(idx);
        for (; idx < _index.Count; ++idx)
        {
          --_index[idx];
        }
      }
      --_size;
    }

  }
}
