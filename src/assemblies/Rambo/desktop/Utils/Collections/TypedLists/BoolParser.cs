namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections.TypedLists
{
  internal class BoolParser : IParser
  {
    public bool TryParse<T>(string parse_, out T value_)
    {
      bool b;
      bool ret = bool.TryParse(parse_, out b);
      value_ = (T)(object)b;
      return ret;
    }
  }

}