using System;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections.TypedLists
{
  internal static class DataValueParser<T>
  {
    private static readonly IParser _parser;

    static DataValueParser()
    {
      Type type = typeof (T);


      if (type == typeof(double))
      {
        _parser = new DoubleParser();
      }
      else if (type == typeof(DateTime))
      {
        _parser = new DateTimeParser();
      }
      else if (type == typeof(int))
      {
        _parser = new IntParser();
      }
      else if (type == typeof(float))
      {
        _parser = new FloatParser();
      }
      else if(type == typeof(bool))
      {
        _parser = new BoolParser();
      }
      else
      {
        _parser = new StringParser();
      }
    }

  
    public static bool TryParse(string parse_, out T value_)
    {
      return _parser.TryParse(parse_, out value_);
    }
  }
}