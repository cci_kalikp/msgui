namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections.TypedLists
{
  internal interface IColumnStorage<T>
  {
    /// <summary>
    /// Gets/Sets the value at the index specified.
    /// </summary>
    T this[int index] { get; set; }

    /// <summary>
    /// Clears a cell value at the specified index, such that an subsequent access
    /// at this index will return the "NullValue" for this type.
    /// </summary>
    /// <param name="index"></param>
    void ClearValue(int index);

    /// <summary>
    /// Completely clears all of the storage.
    /// </summary>
    void Clear();
  }
}