﻿///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2005 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
///////////////////////////////////////////////////////////////////////////////
//
//       Filename:  $File: //eai/msdotnet/msgui/trunk/assemblies/Rambo/desktop/Utils/Collections/TypedLists/ListEnumerator.cs $ 
//       Revision:  $Revision: #3 $ 
// Last edited by:  $Author: boqwang $ 
//             on:  $Date: 2013/02/28 $ 
//
// $Id: //eai/msdotnet/msgui/trunk/assemblies/Rambo/desktop/Utils/Collections/TypedLists/ListEnumerator.cs#3 $ 
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections.TypedLists
{
  /// <summary>
  /// Provides Enumerator for a generic list.
  /// </summary>
  public class ListEnumerator : IEnumerator
  {
    public ListEnumerator(IList list_)
    {
      _list = list_;
    }

    public object Current { get { return _list[_idx]; } }
    public bool MoveNext() { return (++_idx < _list.Count); }
    public void Reset() { _idx = -1; }

    private int _idx = -1;
    private IList _list;
  }
}
