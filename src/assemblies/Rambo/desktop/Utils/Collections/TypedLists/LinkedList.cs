﻿///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2005 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
///////////////////////////////////////////////////////////////////////////////
//
//       Filename:  $File: //eai/msdotnet/msgui/trunk/assemblies/Rambo/desktop/Utils/Collections/TypedLists/LinkedList.cs $ 
//       Revision:  $Revision: #3 $ 
// Last edited by:  $Author: boqwang $ 
//             on:  $Date: 2013/02/28 $ 
//
// $Id: //eai/msdotnet/msgui/trunk/assemblies/Rambo/desktop/Utils/Collections/TypedLists/LinkedList.cs#3 $ 
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections.TypedLists
{
  /// <summary>
  /// IList implemented as a doubly-linked list. 
  /// 
  /// Note, System.Collections.Specialized.ListDictionary implements IDictionary
  /// as a Linked List - perhaps that is what you're looking for?
  /// </summary>
  public class LinkedList : IList
  {
    #region ListItem class
    class ListItem
    {
      public ListItem _prev;
      public ListItem _next;
      public object _datum;
      public ListItem(object datum_)
      {
        _datum = datum_;
      }
    }
    #endregion

    ListItem _first;  // front of the queue.
    ListItem _last;   // end of the queue.

    int _size = 0; // provides constant-time Count property.

    // Informational:

    public int Count { get { return _size; } }
    public virtual bool IsFixedSize { get { return false; } }
    public virtual bool IsReadOnly { get { return false; } }
    public virtual bool IsSynchronized { get { return false; } }
    public virtual object SyncRoot { get { return this; } }

    // Accessors and predicates:

    public bool Contains(Object value_)
    {
      int idx;
      return Find(value_, out idx) != null;
    }

    public void CopyTo(Array array_, int idx_)
    {
      for (ListItem current = _first; current != null; current = current._next)
        array_.SetValue(current._datum, idx_++);
    }

    public virtual IEnumerator GetEnumerator() { return new ListEnumerator(this); }

    public int IndexOf(Object value_)
    {
      int idx;
      Find(value_, out idx);
      return idx;
    }

    public Object this[int idx_]
    {
      get
      {
        if (idx_ < 0 || idx_ >= _size)
          throw new IndexOutOfRangeException();
        return Get(idx_)._datum;
      }
      set
      {
        if (idx_ < 0 || idx_ >= _size)
          throw new IndexOutOfRangeException();
        Get(idx_)._datum = value;
      }
    }

    // Mutators:

    public int Add(object value)
    {
      Push(value);
      return _size - 1;
    }

    public void Clear()
    {
      _first = _last = null;
      _size = 0;
    }

    public void Insert(int idx_, object value_)
    {
      if (idx_ < 0 || idx_ > _size)
        throw new IndexOutOfRangeException();
      if (idx_ == _size)
      {
        Insert(_last, null, value_);
      }
      else
      {
        ListItem item = Get(idx_);
        Insert(item._prev, item, value_);
      }
    }

    public void Remove(Object value_)
    {
      int idx;
      ListItem item = Find(value_, out idx);
      if (item != null)
        Remove(item);
    }

    public void RemoveAt(int idx_)
    {
      if (idx_ < 0 || idx_ >= _size)
        throw new IndexOutOfRangeException();
      Remove(Get(idx_));
    }

    // stack & queue APIs:

    /// <summary>
    /// Add an item to the end of the queue.
    /// </summary>
    /// <param name="datum_">object to add to the queue</param>
    public void Push(object datum_)
    {
      if (_last == null)
        Insert(_first, _last, datum_);
      else
        Insert(_last, _last._next, datum_);
    }

    /// <summary>
    /// Insert and object at the front of the queue.
    /// </summary>
    /// <param name="datum_">object to add to the queue</param>
    public void PushFront(object datum_)
    {
      if (_first == null)
        Insert(_first, _last, datum_);
      else
        Insert(_first._prev, _first, datum_);
    }


    /// <summary>
    /// Remove an object from the front of the queue
    /// </summary>
    /// <returns>the object at the front of the queue, or null if the list is empty.</returns>
    public object Shift()
    {
      if (_first == null)
        return null;
      object datum = _first._datum;
      Remove(_first);
      return datum;
    }

    /// <summary>
    /// Remove and return an object from the end of the queue
    /// </summary>
    /// <returns>the object at the end of the queue, or null if the list is empty.</returns>
    public object Pop()
    {
      if (_last == null)
        return null;
      object datum = _last._datum;
      Remove(_last);
      return datum;
    }

    private void Insert(ListItem prev_, ListItem next_, object value_)
    {
      ListItem item = new ListItem(value_);

      item._prev = prev_;
      item._next = next_;
      if (prev_ != null)
        prev_._next = item;
      else
        _first = item;
      if (next_ != null)
        next_._prev = item;
      else
        _last = item;
      ++_size;
    }

    private void Remove(ListItem item_)
    {
      if (item_._prev == null)
        _first = item_._next;
      else
        item_._prev._next = item_._next;

      if (item_._next == null)
        _last = item_._prev;
      else
        item_._next._prev = item_._prev;

      --_size;
    }

    private ListItem Get(int idx_)
    {
      ListItem item = null;
      if (idx_ > _size / 2)
      {
        item = _last;
        idx_ = _size - idx_ - 1;
        for (; idx_ > 0; --idx_)
          item = item._prev;
      }
      else
      {
        item = _first;
        for (; idx_ > 0; --idx_)
          item = item._next;
      }
      return item;
    }

    private ListItem Find(Object value_, out int idx_)
    {
      idx_ = 0;
      for (ListItem current = _first; current != null; current = current._next, ++idx_)
      {
        if (current._datum == value_)
          return current;
      }
      idx_ = -1;
      return null;
    }

  }
}
