using System;
using System.Collections.Generic;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections
{
  /// <summary>
  /// Provides static methods for creating and/or populating collections and keyed collections that are subsets of a specified collection and whose elements are determined by a supplied criteria.
  /// </summary>
  public static class Accumulator
  {
    /// <summary>
    ///  Creates a new <see cref="T:System.Collections.Generic.IEnumerable`1"></see> populated with the items of the specified <see cref="T:System.Collections.Generic.IEnumerable`1"></see> if the specified <see cref="IAccumable{T}"></see> returns true for the item.
    /// </summary>
    /// <typeparam name="T">
    /// The element type for the supplied enumeration and resulting collection
    /// </typeparam>
    /// <param name="enumeration">
    /// The <see cref="T:System.Collections.Generic.IEnumerable`1"></see> containing items to accumulate
    /// </param>
    /// <param name="accumable">
    /// The <see cref="IAccumable{T}"></see> that determines if an item will be added to the resulting collection.
    ///The <see cref="T:System.Collections.Generic.IEnumerable`1"></see> with the items of the specified <see cref="T:System.Collections.Generic.IEnumerable`1"></see> if the specified <see cref="IAccumable{T}"></see> returns true for the item.
    /// </returns>
    public static IEnumerable<T> CreateAccumulation<T>(IEnumerable<T> enumeration, 
                                                       IAccumable<T> accumable)
    {
      return CreateAccumulation(enumeration, accumable.Accumulate);
    }

    /// <summary>
    /// Populates the specified <see cref="T:System.Collections.Generic.IEnumerable`1"></see> with the items of the specified <see cref="T:System.Collections.Generic.IEnumerable`1"></see> if the specified <see cref="T:System.Predicate`1"></see> returns true for the item.
    /// </summary>
    /// <typeparam name="T">
    /// The element type for the supplied enumeration and resulting collection
    /// </typeparam>
    /// <param name="enumeration">
    /// The <see cref="T:System.Collections.Generic.IEnumerable`1"></see> containing items to accumulate
    /// 
    /// </param>
    /// <param name="accumulate">
    /// The <see cref="T:System.Predicate`1"></see> that determines if an item will be added to the resulting collection.
    /// </param>
    /// <returns>
    /// The populated <see cref="T:System.Collections.Generic.ICollection`1"></see> with the items of the specified <see cref="T:System.Collections.Generic.IEnumerable`1"></see> if the specified <see cref="T:System.Predicate`1"></see> returns true for the item.
    /// </returns>
    public static IEnumerable<T> CreateAccumulation<T>(IEnumerable<T> enumeration, Predicate<T> accumulate)
    {
      if (enumeration == null) throw new ArgumentNullException("enumeration");
      if (accumulate == null) throw new ArgumentNullException("accumulate");

      foreach(T item in enumeration)
      {
        if(accumulate(item))
        {
          yield return item;
        }
      }
    }
  }
}