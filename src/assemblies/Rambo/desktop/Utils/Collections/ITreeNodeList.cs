using System.Collections;
using System.Collections.Generic;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections
{
  public interface ITreeNodeList : IList
  {
    /// <summary>
    /// Gets the owner node of this list instance.
    /// </summary>
    ITreeNode Owner { get; }

    /// <summary>
    /// Gets the depth this list in relation to the root node.
    /// </summary>
    int Depth { get; }

    /// <summary>
    /// Gets the number of generations from this list on downwards.
    /// </summary>
    int Generations { get; }

    /// <summary>
    /// Gets the number of descendants.
    /// </summary>
    int Descendants { get; }
  }
}