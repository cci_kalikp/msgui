///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2005 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
///////////////////////////////////////////////////////////////////////////////

using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections
{
  internal class SparseRowCollection<T> : IDataRowCollection<T>
    where T : SparseRow
  {
    #region Readonly & Static Fields
    private readonly Queue<int> _freeIndices;
    private readonly List<int> _indexMap;
    private readonly List<T> _rows;
    private readonly SparseTable<T> _table;
    #endregion

    #region Constructors
    internal SparseRowCollection(SparseTable<T> table_)
    {
      _table = table_;
      _freeIndices = new Queue<int>( );
      _rows = new List<T>( );
      _indexMap = new List<int>( );
    }
    #endregion

    #region Instance Properties
    public int AllocationCount
    {
      get { return _rows.Count; }
    }
    #endregion

    #region Instance Methods

    public override string ToString( )
    {
      return string.Format("Count={0}", Count);
    }

    private static int DescendingIndexComparison(T row1_, T row2_)
    {
      int index1 = row1_.RowIndex;
      int index2 = row2_.RowIndex;

      return index2 - index1;
    }

    private void Reindex(int startIndex_)
    {
      for (int i = startIndex_; i < _indexMap.Count; ++i)
      {
        _rows[_indexMap[i]].RowIndex = i;
      }
    }

    private void RemoveRow(T row_)
    {
      _indexMap.RemoveAt(row_.RowIndex);
      _rows[row_.AllocationIndex] = null;
      _freeIndices.Enqueue(row_.AllocationIndex);
    }

    private void RemoveRowFromColumns(T row_)
    {
      for (int i = 0; i < _table.Columns.Count; ++i)
      {
        SparseColumn<T> column = (SparseColumn<T>) _table.Columns[i];
        column.RemoveAt(row_.AllocationIndex);
      }
    }

    private static bool TryDowncast(IDataRow input_, out T output_)
    {
      output_ = input_ as T;
      return input_ != null;
    }
    #endregion

    #region IDataRowCollection<R> Members
    public int Count
    {
      get { return _indexMap.Count; }
    }

    public T this[int idx_]
    {
      get { return _rows[_indexMap[idx_]]; }
    }

    


    public T Add( )
    {
      lock (_table.SyncRoot)
      {
        foreach (SparseColumn<T> col in _table.Columns.Cast<SparseColumn<T>>())
        {
          col.Add();
        }

        int idx = Count;
        bool newAllocation = _freeIndices.Count == 0;
        int allocation = newAllocation ? AllocationCount : _freeIndices.Dequeue( );

        T row = _table.RowFactory.Create(_table, idx);
        row.AllocationIndex = allocation;
        _indexMap.Add(allocation);

        if (newAllocation)
        {
          _rows.Add(row);
        }
        else
        {
          _rows[allocation] = row;
        }

        return row;
      }
    }

    public void Clear( )
    {
      lock (_table.SyncRoot)
      {
        ( (SparseColumnCollection<T>) _table.Columns ).ClearData( );
        _rows.Clear( );
        _indexMap.Clear( );
        _freeIndices.Clear( );
      }
    }

    public void Remove(IEnumerable<T> rows_)
    {
      lock (_table.SyncRoot)
      {
        List<T> rowList = new List<T>(rows_);
        rowList.Sort(DescendingIndexComparison);

        foreach (T row in rowList)
        {
          RemoveRow(row);
        }

        Reindex(rowList[rowList.Count - 1].RowIndex);

        foreach (T row in rowList)
        {
          RemoveRowFromColumns(row);
        }
      }
    }

    public void Remove(T row_)
    {
      lock (_table.SyncRoot)
      {
        RemoveRow(row_);

        Reindex(row_.RowIndex);

        RemoveRowFromColumns(row_);
      }
    }

    public void RemoveAt(int idx_)
    {
      lock (_table.SyncRoot)
      {
        Remove(_rows[_indexMap[idx_]]);
      }
    }

    IEnumerator<IDataRow> IEnumerable<IDataRow>.GetEnumerator( )
    {
      for (int i = 0; i < _rows.Count; i++)
      {
        IDataRow row = _rows[i];

        if(row != null)
        {
          yield return _rows[i];          
        }
      }
    }

    public IEnumerator GetEnumerator( )
    {
      return ( (IEnumerable<IDataRow>) this ).GetEnumerator( );
    }

    IDataRow IDataRowCollection.this[int idx_]
    {
      get { return this[idx_]; }
    }

    IDataRow IDataRowCollection.Add( )
    {
      return Add( );
    }

    void IDataRowCollection.Remove(IEnumerable<IDataRow> rows_)
    {
      Remove(Transformer.CreateTransformation<IDataRow, T>(rows_, TryDowncast));
    }

    void IDataRowCollection.Remove(IDataRow row_)
    {
      T row = row_ as T;
      if (row != null)
      {
        Remove(row);
      }
    }
    #endregion

    IDataRow IIndexableCollection<int, IDataRow>.this[int index_]
    {
      get { return this[index_]; }
    }
  }
}