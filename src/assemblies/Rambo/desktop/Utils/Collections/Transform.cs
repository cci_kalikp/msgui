namespace MorganStanley.Ecdev.Desktop.Utils.Collections
{
  /// <summary>
  /// A delegate for methods that convert values from one type to another 
  /// if the conversion is valid.
  /// </summary>
  public delegate bool Transform<InType, OutType>(InType input, out OutType output);
}