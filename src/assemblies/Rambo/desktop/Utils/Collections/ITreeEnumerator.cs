using System.Collections;
using System.Collections.Generic;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections
{
  /// <summary>
  /// An interface for enumerating over the nodes or values of a tree depth or breadth first.
  /// </summary>
  public interface ITreeEnumerator
  {
    /// <summary>
    /// Gets an enumerable collection for iterating over the values of a tree depth first.
    /// </summary>
    /// <value>The depth first value enumerator.</value>
    IEnumerable DepthFirstEnumerator { get; }

    /// <summary>
    /// Gets an enumerable collection for iterating over the values of a tree breadth first.
    /// </summary>
    /// <value>The breadth first value enumerator.</value>
    IEnumerable BreadthFirstEnumerator { get; }

    /// <summary>
    /// Gets an enumerable collection for iterating over the nodes of a tree depth first.
    /// </summary>
    /// <value>The depth first node enumerator.</value>
    IEnumerable<ITreeNode> DepthFirstNodeEnumerator { get; }

    /// <summary>
    /// Gets an enumerable collection for iterating over the nodes of a tree breadth first.
    /// </summary>
    /// <value>The breadth first node enumerator.</value>
    IEnumerable<ITreeNode> BreadthFirstNodeEnumerator { get; }
  }
}