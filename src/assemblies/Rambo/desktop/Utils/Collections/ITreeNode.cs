using System.Collections;
using System.Collections.Generic;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections
{
  public interface ITreeNode : ITreeEnumerator, ITreeNodeSearch
  {
    /// <summary>
    /// Gets or sets the contained value for this node
    /// </summary>
    object Value { get; set; }

    /// <summary>
    /// Gets a value indicating whether this node is the root.
    /// </summary>
    /// <value><c>true</c> if this node is root; otherwise, <c>false</c>.</value>
    bool IsRoot { get; }

    /// <summary>
    /// Gets a value indicating whether this node has children.
    /// </summary>
    /// <value>
    /// 	<c>true</c> if this node has children; otherwise, <c>false</c>.
    /// </value>
    bool HasChildren { get; }

    /// <summary>
    /// Gets a value indicating whether this node has siblings.
    /// </summary>
    /// <value>
    /// 	<c>true</c> if this node has siblings; otherwise, <c>false</c>.
    /// </value>
    bool HasSiblings { get; }

    /// <summary>
    /// Gets the index of this node within its parent list.
    /// </summary>
    int Index { get; }

    /// <summary>
    /// Gets the depth in relation to the root node.
    /// </summary>
    int Depth { get; }

    /// <summary>
    /// Gets the number of descendant nodes below this node, i.e. the number of nodes for which this node is an ancestor.
    /// </summary>
    int Descendants { get; }

    /// <summary>
    /// Gets the number of genererations of descendant nodes below this node.
    /// </summary>
    int Generations { get; }

    /// <summary>
    /// Gets the root node.
    /// </summary>
    ITreeNode RootNode { get; }

    /// <summary>
    /// Gets the parent node of this node, null if this node is root.
    /// </summary>
    ITreeNode ParentNode { get; }

    /// <summary>
    /// Gets the list of nodes that are direct descendants of this node.
    /// </summary>
    ITreeNodeList ChildNodes { get; }

    /// <summary>
    /// Gets the list of nodes this node is contained in.
    /// </summary>
    ITreeNodeList ParentList { get; }

    /// <summary>
    /// Gets the siblings of this node.
    /// </summary>
    IEnumerable<ITreeNode> Siblings { get; }

    /// <summary>
    /// Gets the ancestor enumerator, which iterates through the values from this node to the root node through this node's ancestry.
    /// </summary>
   IEnumerable AncestorEnumerator { get; }

    /// <summary>
    /// Gets the ancestor enumerator, which iterates through the nodes from this node to the root node through this node's ancestry.
    /// </summary>
    IEnumerable<ITreeNode> AncestorNodeEnumerator { get; }
  }
}