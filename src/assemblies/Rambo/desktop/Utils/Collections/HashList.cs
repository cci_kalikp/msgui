using System;
using System.Collections;
using System.Text;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections
{
  /// <summary>
  /// Hashtable whose values are lists, ie where there is a 1:N relationship between keys and values.
  /// </summary>
  public class HashList
  {
    private IDictionary _lists;

    /// <summary>
    /// Constructor.
    /// </summary>
    public HashList()
    {
      _lists = new Hashtable();
    }

    /// <summary>
    /// Constructor.
    /// </summary>
    /// <param name="capacity">Capacity to initialise the underlying hashtable with.</param>
    public HashList(int capacity)
    {
      _lists = new Hashtable (capacity);
    }

    /// <summary>
    /// Get the IList associated with a particular key.  Returns null if not found.
    /// </summary>
    public IList this[object key_]
    {
      get
      {
        return _lists [key_] as ArrayList;
      }
    }

    /// <summary>
    /// Add an object to the list associated with a particular key.
    /// </summary>
    /// <param name="key_">The key.</param>
    /// <param name="value_">The value to add.</param>
    public void Add (object key_, object value_)
    {
      if (!ContainsKey (key_))
        _lists.Add (key_, new ArrayList());

      IList list = _lists[key_] as ArrayList;

      if (!list.Contains (value_))
      {
        list.Add (value_);
      }
    }

    public ICollection Keys 
    {
      get { return _lists.Keys; }
    }

    /// <summary>
    /// Remove a particular value from the list associated with a key.
    /// </summary>
    /// <param name="key_"></param>
    /// <param name="value_"></param>
    public void Remove (object key_, object value_)
    {
      if (ContainsKey (key_))
      {
        IList list = _lists[key_] as ArrayList;
        list.Remove(value_);

        if (list.Count == 0)
        {
          _lists.Remove (key_);
        }
      }
    }

    /// <summary>
    /// Remove all values associated with a particular key.
    /// </summary>
    /// <param name="key_"></param>
    public void RemoveAll (object key_)
    {
      if (ContainsKey (key_))
      {
        _lists.Remove (key_);
      }
    }

    /// <summary>
    /// Return whether a key is contained in this Hashlist.
    /// </summary>
    /// <param name="key_"></param>
    /// <returns></returns>
    public bool ContainsKey (object key_)
    {
      return _lists.Contains (key_);
    }

    /// <summary>
    /// Get the count of values associated with a particular key.
    /// </summary>
    /// <param name="key_"></param>
    /// <returns></returns>
    public int Count (object key_)
    {
      return _lists.Contains (key_) ? (_lists[key_] as ArrayList).Count : 0;
    }

    /// <summary>
    /// Get the total count of all values.
    /// </summary>
    /// <returns></returns>
    public int Count ()
    {
      int total = 0;
      foreach (IList list in _lists.Values)
      {
        total += list.Count;
      }
      return total;
    }

    public override string ToString()
    {
      StringBuilder s = new StringBuilder();
      foreach (object key in _lists.Keys)
      {
        StringBuilder s2 = new StringBuilder();
        foreach (object val in this[key])
        {
          s2.Append (string.Format ("{0},", val));
        }
        s.Append (string.Format ("[{0}: {1}]\n", key.ToString(), s2.ToString()));
      }
      return s.ToString();
    }
  }
}