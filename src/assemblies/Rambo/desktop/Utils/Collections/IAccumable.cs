namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Collections
{
  /// <summary>
  /// Defines a method that a type implements to determine if an item should be 
  /// added to an accumulation.
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public interface IAccumable<T>
  {
    /// <summary>
    /// Returns true if the specified item should be added to an accumulation.
    /// </summary>
    bool Accumulate(T item);
  }
}