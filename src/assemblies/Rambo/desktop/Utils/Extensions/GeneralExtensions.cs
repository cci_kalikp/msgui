﻿using System;
using System.Text;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using JetBrains.Annotations;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Extensions
{  
  /// <summary>
  /// Assorted extension methods
  /// </summary>
  public static class General
  {
    private static readonly ILogger _log = LoggerManager.GetLogger(typeof(General));

    /// <summary>
    /// Executes <paramref name="action_"/> on each object in <paramref name="enumerable_"/>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="enumerable_"></param>
    /// <param name="action_"></param>
    public static void ForEach<T>(this IEnumerable<T> enumerable_, Action<T> action_)
    {
      foreach (T enumerable in enumerable_)
      {
        action_.Invoke(enumerable);
      }
    }

    ///<summary>
    ///Force an enumeration in a communicative manner (instead of simply calling ToArray, or ToList which don't tell the reader of the code
    ///that the reason you are doing the call is purely to force the enumeration instead of to specifically create an array or list)
    ///</summary>
    ///<param name="enumerable_"></param>
    ///<typeparam name="T"></typeparam>
    public static IEnumerable<T> ForceEnumeration<T>([NotNull] this IEnumerable<T> enumerable_)
    {
      if (enumerable_ == null) throw new ArgumentNullException("enumerable_");

      return enumerable_.ToArray();
    }

    /// <summary>
    /// Serilizes an object into a string using a serializer_
    /// </summary>
    /// <param name="objectToSerialize_">object to seialize</param>
    /// <returns></returns>
    public static string SerializeToString(this object objectToSerialize_)
    {
      var serializer = new XmlSerializer(objectToSerialize_.GetType());
      return serializer.SerializeToString(objectToSerialize_);
    }

    /// <summary>
    /// Serilizes an object into a string using a serializer_
    /// </summary>
    /// <param name="serializer_">XmlSerializer to use</param>
    /// <param name="objectToSerialize_">object to seialize</param>
    /// <returns></returns>
    public static string SerializeToString(this XmlSerializer serializer_, object objectToSerialize_)
    {
      var builder = new StringBuilder();
      var settings = new XmlWriterSettings { Encoding = Encoding.Unicode };
      using (XmlWriter writer = XmlWriter.Create(builder, settings))
      {
        serializer_.Serialize(writer, objectToSerialize_);
      }
      return builder.ToString();
    }

    /// <summary>
    /// Creates a loop n times starting from 0 and exectues an action
    /// </summary>
    /// <param name="n_"></param>
    /// <param name="action_"></param>
    public static void ForInt(this int n_, Action<int> action_)
    {
      for (int i = 0; i != n_; i++)
      {
        action_(i);
      }
    }

    /// <summary>
    /// Returns if an instance is null
    /// </summary>
    /// <param name="instance_"></param>
    /// <returns></returns>
    public static bool IsNull(this object instance_)
    {
      return instance_ == null;
    }

    /// <summary>
    /// Extension message to log message
    /// </summary>
    /// <param name="instance_"></param>
    /// <param name="message_"></param>
    public static void Log(this object instance_, string message_)
    {
      if (instance_.IsNull() == false)
        _log.Info(instance_.GetType().ToString(), message_);
      else
        _log.Info("NULL", message_);
    }

    /// <summary>
    /// This will return selected values for list of keys out of the idictionary
    /// </summary>
    /// <typeparam name="TKey">Key Type</typeparam>
    /// <typeparam name="TValue">Value type</typeparam>
    /// <param name="dictionary_">dictionary where you want to have values extracted from</param>
    /// <param name="keys_">keys_ that corespond to wanted values</param>
    /// <returns>a list of values that corresponds to keys_</returns>
    public static List<TValue> GetAllValues<TKey, TValue>(this IDictionary<TKey, TValue> dictionary_, IEnumerable<TKey> keys_)
    {
      return keys_.Aggregate(new List<TValue>(), (list, key) =>
      {
        list.Add(dictionary_[key]);
        return list;
      });
    }

    /// <summary>
    /// Checks if collection is empty
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="collection_"></param>
    /// <returns></returns>
    static public bool IsEmpty<T>(this ICollection<T> collection_)
    {
      return collection_ == null||collection_.Count == 0;
    }

    /// <summary>
    /// Checks if collection is empty
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="collection_"></param>
    /// <returns></returns>
    static public bool IsEmpty<T>(this IEnumerable<T> collection_)
    {
      return collection_== null || collection_ is ICollection<T> ? collection_.Count() == 0 : !collection_.Any();
    }


    /// <summary>
    /// This is to verify if a certain obj_ is null. if it is null 
    /// the NullReferenceException with type indicator of what is null
    /// </summary>
    /// <example>
    /// str.NullVerify().Concat("test").NullVerify();
    /// </example>
    /// <typeparam name="T"></typeparam>
    /// <param name="obj_"></param>
    /// <returns>obj_ if obj_ is not null else throwas the exception</returns>
    static public T NullVerify<T>(this T obj_) where T : class
    {
      if (obj_ == null)
      {
        throw new NullReferenceException("Object of type '" + typeof(T).Name + "' is null");
      }
      return obj_;
    }


    /// <summary>
    /// This is to verify if a certain obj_ is null. if it is null 
    /// the NullReferenceException with type indicator of what is null
    /// </summary>
    /// <example>
    /// str.NullVerify("oops").Concat("test").NullVerify();
    /// if str is null 
    /// you will get "Object of type 'string' is null: oops"
    /// </example>
    /// <typeparam name="T"></typeparam>
    /// <param name="obj_"></param>
    ///  <param name="name_">this is returned as part of exception for easy debugging</param>
    /// <returns>obj_ if obj_ is not null else throwas the exception</returns>
    static public T NullVerify<T>(this T obj_, string name_) where T : class
    {
      if (obj_ == null)
      {
        throw new NullReferenceException("Object of type '" + typeof(T).Name + "' is null: " + name_);
      }
      return obj_;
    }

    /// <summary>
    /// Converts and XmlNode to an XElement
    /// </summary>
    /// <param name="node_">The node</param>
    /// <returns>The XElement</returns>
    public static XElement GetXElement(this XmlNode node_)
    {
      var xDoc = new XDocument();
      using (XmlWriter xmlWriter = xDoc.CreateWriter())
      {
        node_.WriteTo(xmlWriter);
      }
      return xDoc.Root;
    }

    /// <summary>
    /// This function will aquire a Value with key_  from the dictionary_  if it exists. 
    /// If it does not the funcForItemToAdd_ is used to create a new item.
    /// Then inserts the item with the key_ into the dictionary and returned 
    /// </summary>
    /// <typeparam name="TKt">Type of the Key in the dictionary </typeparam>
    /// <typeparam name="TVt">Type of Value in the dictionary </typeparam>
    /// <param name="dictionary_">dictionary to aquire a value from</param>
    /// <param name="key_">key for a value to aquire or insert</param>
    /// <param name="funcForItemToAdd_">a function that is used to insert a value</param>
    /// <returns>a value with _key that exists in the dictionary_</returns>
    public static TVt GetOrAdd<TKt, TVt>(this IDictionary<TKt, TVt> dictionary_, TKt key_, Func<TVt> funcForItemToAdd_)
    {
      TVt outValue;
      if (!dictionary_.TryGetValue(key_, out outValue))
      {
        outValue = funcForItemToAdd_();
        dictionary_.Add(key_, outValue);
      }
      return outValue;
    }

    ///<summary>
    /// Adds multiple values to a dictionary
    ///</summary>
    ///<param name="dict_">Dictionary to add to</param>
    ///<param name="valuesToAdd_">new values to be added to the dictionary</param>
    ///<param name="keySelector_">function to generate a key for a value to add</param>
    ///<typeparam name="TKey">Type of the key in dthe dictionary</typeparam>
    ///<typeparam name="TValue">Type of the Value in the dictionary</typeparam>
    ///<returns>dict_ after values were added to it</returns>
    static public Dictionary<TKey, TValue> AddAll<TKey, TValue>(this Dictionary<TKey, TValue> dict_, IEnumerable<TValue> valuesToAdd_, Func<TValue, TKey> keySelector_)
    {
      foreach (var valueToAdd in valuesToAdd_)
      {
        dict_.Add(keySelector_(valueToAdd), valueToAdd);
      }
      return dict_;
    }
    
    /// <summary>
    /// Adds a range to ICollection interface (particlarly useful for HashSet)
    /// </summary>
    /// <typeparam name="T">Type of ICollection item</typeparam>
    /// <param name="targetSet_">ISet where to add values</param>
    /// <param name="values_">ISet with values to add</param>
    static public void AddRange<T>(this ISet<T> targetSet_, IEnumerable<T> values_)
    {
        foreach (var val in values_)
        {
            targetSet_.Add(val);
        }

    }

    /// <summary>
    /// Returns a difference between two sets
    /// </summary>
    /// <typeparam name="T">Type of collection items</typeparam>
    /// <param name="targetSet_">Set to work on</param>
    /// <param name="diffSet_">Second set</param>
    /// <returns></returns>
    static public ISet<T> Difference<T>(this ISet<T> targetSet_, IEnumerable<T> diffSet_)
    {
        ISet<T> newSet = new HashSet<T>(targetSet_);

        foreach (T item in diffSet_)
        {
            // if the item exists in set, remove it, otherwise add it.
            if (!newSet.Remove(item))
            {
                newSet.Add(item);
            }
        }
        return newSet;
    }
  }
}
