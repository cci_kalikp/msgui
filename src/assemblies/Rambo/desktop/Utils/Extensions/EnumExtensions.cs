﻿using System;
using System.ComponentModel;
using System.Reflection;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Extensions
{
  ///<summary>
  /// Extension methods for Enums
  ///</summary>
  public static class EnumExtensions
  {
    ///<summary>
    ///</summary>
    ///<param name="value_"></param>
    ///<returns></returns>
    public static string GetDescription(this Enum value_)
    {
      Type type = value_.GetType();
      string name = Enum.GetName(type, value_);
      
      FieldInfo field = type.GetField(name);
      if (field != null)
      {
        var attr = Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) as DescriptionAttribute;
        if (attr != null)
        {
          return attr.Description;
        }
      }
      
      return null;
    }
  }
}
