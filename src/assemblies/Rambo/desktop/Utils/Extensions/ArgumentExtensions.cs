using System;
using JetBrains.Annotations;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Extensions
{
  ///<summary>
  ///</summary>
  public static class ArgumentExtensions
  {
    ///<summary>
    /// Asserts that the object is not null and is of type <see cref="T"/>
    ///</summary>
    ///<param name="sender_">Object to assert against</param>
    ///<param name="argumentName_">Name of the argument being tested</param>
    ///<typeparam name="T">Type to ensure the object is of</typeparam>
    ///<returns>Object cast as the generic type specified: <see cref="T"/></returns>
    ///<exception cref="ArgumentNullException">Occurs if the object upon which the method is run is null</exception>
    ///<exception cref="ArgumentException">Occurs if the object upon which the method is run is not of the specified type</exception>
    [NotNull]
    public static T AssertArgumentIsNotNullAndIsOfType<T>([CanBeNull] this object sender_, [NotNull] string argumentName_)
    {
      if (sender_ == null) throw new ArgumentNullException(argumentName_);

      return AssertArgumentIsOfType<T>(sender_, argumentName_);
    }

    ///<summary>
    /// Asserts that the object is not null and is of type <see cref="T"/>
    ///</summary>
    ///<param name="sender_">Object to assert against</param>
    ///<typeparam name="T">Type to ensure the object is of</typeparam>
    ///<returns>Object cast as the generic type specified: <see cref="T"/></returns>
    ///<exception cref="ArgumentNullException">Occurs if the object upon which the method is run is null</exception>
    ///<exception cref="ArgumentException">Occurs if the object upon which the method is run is not of the specified type</exception>
    [NotNull]
    public static T AssertArgumentIsNotNullAndIsOfType<T>([CanBeNull] this object sender_)
    {
      return AssertArgumentIsNotNullAndIsOfType<T>(sender_, "[ArgumentNameNotSpecified]");
    }

    ///<summary>
    ///Asserts that the object is of type <see cref="T"/>
    ///</summary>
    ///<param name="sender_">Object to assert against</param>
    ///<param name="argumentName_">Name of the argument being tested</param>
    ///<typeparam name="T">Type to ensure the object is of</typeparam>
    ///<returns>Object cast as the generic type specified: <see cref="T"/></returns>
    ///<exception cref="ArgumentException">Occurs if the object upon which the method is run is not of the specified type</exception>
    [NotNull]
    private static T AssertArgumentIsOfType<T>([NotNull] object sender_, [NotNull] string argumentName_)
    {
      if (sender_ is T)
      {
        return (T)sender_;
      }

      throw new ArgumentException(String.Format(argumentName_ + " is not {0}: {1}", typeof(T), sender_.GetType()));
    }
  }
}