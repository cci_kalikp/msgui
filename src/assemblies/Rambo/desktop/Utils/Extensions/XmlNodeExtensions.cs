﻿using System;
using System.Xml;
using JetBrains.Annotations;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Extensions
{
  /// <summary>
  /// Extension methods for XmlNode.
  /// </summary>
  public static class XmlNodeExtensions
  {
    /// <summary>
    /// Returns value for given attribute. If attribute does not exist or value is null then returns string.Empty.
    /// </summary>
    /// <param name="node_"></param>
    /// <param name="attributeName_"></param>
    /// <returns></returns>
    [NotNull]
    public static string GetAttributeIfItExistsOrStringEmpty([NotNull] this XmlNode node_, [NotNull] string attributeName_)
    {
      if (node_ == null) throw new ArgumentNullException("node_");
      if (attributeName_ == null) throw new ArgumentNullException("attributeName_");
      if (node_.Attributes == null) return string.Empty;

      XmlAttribute attr = node_.Attributes[attributeName_];
      if (attr == null) return string.Empty;

      return attr.Value ?? string.Empty;
    }
  }
}
