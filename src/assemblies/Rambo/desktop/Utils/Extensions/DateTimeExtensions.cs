﻿using System;
using MorganStanley.MSDotNet.MSTools;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Extensions
{
  public static class DateTimeExtensions
  {
    private static readonly ILogger _log = LoggerManager.GetLogger(typeof(General));

    /// <summary>
    /// Checks if date_ is between From and To_
    /// </summary>
    /// <param name="date_">Input date</param>
    /// <param name="from_"></param>
    /// <param name="to_"></param>
    /// <returns>True if date less than to and greater than from_ </returns>
    public static bool Between(this DateTime date_, DateTime from_, DateTime to_)
    {
      return from_ < date_ && date_ < to_;
    }

    /// <summary>
    /// Checks if date_ is between inclusive From and To_
    /// </summary>
    /// <param name="date_">Input date</param>
    /// <param name="from_"></param>
    /// <param name="to_"></param>
    /// <returns>True if date less than to and greater than from_ </returns>
    public static bool BetweenInclusive(this DateTime date_, DateTime from_, DateTime to_)
    {
      return from_ <= date_ && date_ <= to_;
    }

    /// <summary>
    /// This function returns the date that is numberOfWeekDays_ away from date_
    /// </summary>
    /// <param name="date_">Date to add weekdays to</param>
    /// <param name="numberOfWeekDays_">postive number of dayse from date_</param>
    /// <returns>date + numberOfWeekDays_</returns>
    public static DateTime AddWeekDays(this DateTime date_, int numberOfWeekDays_)
    {
      if (numberOfWeekDays_ < 0)
      {
        throw new ArgumentException("Number of business days can not be negative");
      }
      if (numberOfWeekDays_ == 0)
        return date_;

      int weekOffset = 0;

      var dayOfWeek = date_.DayOfWeek;

      if (dayOfWeek == DayOfWeek.Saturday)
      {
        weekOffset = 1;
      }

      int numberToAdd = ((int)dayOfWeek + numberOfWeekDays_ - 1) / 5 * 2 - weekOffset + numberOfWeekDays_;

      return date_.AddDays(numberToAdd);
    }

    /// <summary>
    /// Get next trade date from MS calendar. NY, LN, HK & TK regions supported only. 
    /// Attention - the first resource code call can be very slow!
    /// </summary>
    /// <param name="date_">DateTime after next trade date is required</param>
    /// <param name="region_">The region to calculate next business day from. 
    /// Supported values: NY, LN, HK & TK. Any other value will call NY region calendar.</param>
    /// <param name="offset_">The number of valid trade days to offset by</param>
    /// <returns></returns>
    public static DateTime NextTradeDate(this DateTime date_, string region_, int offset_)
    {
      string resourceCode = "NYS";
      switch (region_.ToUpper())
      {
        case "NY":
          // default set to 'NYS'
          break;
        case "LN":
          resourceCode = "LNS";
          break;
        case "HK":
          resourceCode = "HKS";
          break;
        case "TK":
          resourceCode = "TKS";
          break;
        default:
          _log.Warning("DateTimeExtensions.NextTradeDate", String.Format("Not supported region[{0}]. Set to [{1}].", region_, resourceCode));
          break;
      }

      // Very slow when called first time per resource code
      return MSCalendar.NextTradeDate(date_, offset_, resourceCode);
    }

    /// <summary>
    /// Get next week day date ( Excluding weekend: Sat & Sun )
    /// </summary>
    /// <param name="date_">The current date.</param>
    /// <returns>Next week day</returns>
    public static DateTime NextWeekDayDate(this DateTime date_)
    {
      DateTime result = date_;
      while ((result = result.AddDays(1)).DayOfWeek == DayOfWeek.Saturday || result.DayOfWeek == DayOfWeek.Sunday)
      {
      }

      return result;
    }
  }
}