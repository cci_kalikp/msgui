using System;
using System.ComponentModel;
using System.Linq.Expressions;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Extensions
{
  ///<summary>
  /// Extensions to simply coding against <see cref="INotifyPropertyChanged" /> instances
  ///</summary>
  public static class NotifyPropertyChangedExtensions
  {

    ///<summary>
    /// Invoke the <see cref="INotifyPropertyChanged.PropertyChanged"/> event using a lambda expression
    ///</summary>
    ///<param name="viewModel_">ViewModel against which the event should be raised and upon which the property resides</param>
    ///<param name="propertyChanged_">PropertyChanged event which should be raised</param>
    ///<param name="properties_">Lambda expressions which points to the property that has changed</param>
    ///<typeparam name="T"></typeparam>
    public static void InvokePropertyChangedUsingLambda(
      this INotifyPropertyChanged viewModel_,
      PropertyChangedEventHandler propertyChanged_, params
      Expression<Func<object>>[] properties_)
    {
      foreach(var property in properties_)
      {
        string propertyName = GetPropertyNameFromExpression(property);
        var propertyChangedEventArgs = new PropertyChangedEventArgs(propertyName);
        var handler = propertyChanged_;
        if (handler != null) handler(viewModel_, propertyChangedEventArgs);
      }
    }

    private static string GetPropertyNameFromExpression<T>(Expression<Func<T>> property_)
    {
      var lambda = (LambdaExpression)property_;
      MemberExpression memberExpression;

      if (lambda.Body is UnaryExpression)
      {
        var unaryExpression = (UnaryExpression)lambda.Body;
        memberExpression = (MemberExpression)unaryExpression.Operand;
      }
      else
      {
        memberExpression = (MemberExpression)lambda.Body;
      }

      return memberExpression.Member.Name;
    }
  }
}