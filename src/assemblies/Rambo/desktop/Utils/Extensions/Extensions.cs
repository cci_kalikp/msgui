﻿using System;
using System.Text;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using JetBrains.Annotations;
using MorganStanley.MSDotNet.MSTools;

namespace MorganStanley.Ecdev.Desktop.Utils
{  
  /// <summary>
  /// Assorted extension methods
  /// </summary>
  public static class Extensions
  {
    /// <summary>
    /// Executes <paramref name="action_"/> on each object in <paramref name="enumerable_"/>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="enumerable_"></param>
    /// <param name="action_"></param>
    public static void ForEach<T>(this IEnumerable<T> enumerable_, Action<T> action_)
    {
      foreach (T enumerable in enumerable_)
      {
        action_.Invoke(enumerable);
      }
    }



    /// <summary>
    /// Serilizes an object into a string using a serializer_
    /// </summary>
    /// <param name="serializer_">XmlSerializer to use</param>
    /// <param name="objectToSerialize_">object to seialize</param>
    /// <returns></returns>
    public static string SerializeToString(this object objectToSerialize_)
    {
      var serializer = new XmlSerializer(objectToSerialize_.GetType());
      return serializer.SerializeToString(objectToSerialize_);
    }

    /// <summary>
    /// Serilizes an object into a string using a serializer_
    /// </summary>
    /// <param name="serializer_">XmlSerializer to use</param>
    /// <param name="objectToSerialize_">object to seialize</param>
    /// <returns></returns>
    public static string SerializeToString(this XmlSerializer serializer_, object objectToSerialize_)
    {
      var builder = new StringBuilder();
      var settings = new XmlWriterSettings { Encoding = Encoding.Unicode };
      using (XmlWriter writer = XmlWriter.Create(builder, settings))
      {
        serializer_.Serialize(writer, objectToSerialize_);
      }
      return builder.ToString();
    }

    /// <summary>
    /// This function returns the date that is numberOfWeekDays_ away from date_
    /// </summary>
    /// <param name="date_">Date to add weekdays to</param>
    /// <param name="numberOfWeekDays_">postive number of dayse from date_</param>
    /// <returns>date + numberOfWeekDays_</returns>
    public static DateTime AddWeekDays(this DateTime date_, int numberOfWeekDays_)
    {
      if(numberOfWeekDays_<0)
      {
        throw new ArgumentException("Number of business days can not be negative");
      }
      if (numberOfWeekDays_ == 0)
        return date_;

      int weekOffset = 0;

      var dayOfWeek = date_.DayOfWeek;

      if (dayOfWeek == DayOfWeek.Saturday)
      {
        weekOffset = 1;
      }

      int numberToAdd = ((int)dayOfWeek + numberOfWeekDays_ - 1) / 5 * 2 - weekOffset + numberOfWeekDays_;

      return date_.AddDays(numberToAdd);
    }

    /// <summary>
    /// Get next trade date from MS calendar. NY, LN, HK & TK regions supported only. 
    /// Attention - the first resource code call can be very slow!
    /// </summary>
    /// <param name="date_">DateTime after next trade date is required</param>
    /// <param name="region_">The region to calculate next business day from. 
    /// Supported values: NY, LN, HK & TK. Any other value will call NY region calendar.</param>
    /// <param name="offset_">The number of valid trade days to offset by</param>
    /// <returns></returns>
    public static DateTime NextTradeDate(this DateTime date_, string region_, int offset_)
    {
      string resourceCode = "NYS";
      switch(region_.ToUpper())
      {
        case "NY":
        // default set to 'NYS'
        break;
        case "LN":
          resourceCode = "LNS";
          break;
        case "HK":
          resourceCode = "HKS";
          break;
        case "TK":
          resourceCode = "TKS";
          break;        
        default:
          _log.Warning("Extensions.NextTradeDate", String.Format("Not supported region[{0}]. Set to [{1}].", region_, resourceCode ));
          break;
      }
      
      // Very slow when called first time per resource code
      return MSCalendar.NextTradeDate(date_, offset_, resourceCode);
    }

    /// <summary>
    /// Get next week day date ( Excluding weekend: Sat & Sun )
    /// </summary>
    /// <param name="date_">The current date.</param>
    /// <returns>Next week day</returns>
    public static DateTime NextWeekDayDate(this DateTime date_)
    {
      DateTime result = date_;
      while ((result = result.AddDays(1)).DayOfWeek == DayOfWeek.Saturday || result.DayOfWeek == DayOfWeek.Sunday)
      {
      }

      return result;
    }

    /// <summary>
    /// Creates a loop n times starting from 0 and exectues an action
    /// </summary>
    /// <param name="n"></param>
    /// <param name="action"></param>
    public static void ForInt(this int n, Action<int> action)
    {
      for (int i = 0; i != n; i++)
      {
        action(i);
      }
    }

    private static readonly ILogger _log = LoggerManager.GetLogger(typeof(Extensions));

    /// <summary>
    /// Returns if an instance is null
    /// </summary>
    /// <param name="instance"></param>
    /// <returns></returns>
    public static bool IsNull(this object instance)
    {
      return instance == null;
    }

    /// <summary>
    /// Checks if date_ is between From and To_
    /// </summary>
    /// <param name="date_">Input date</param>
    /// <param name="from_"></param>
    /// <param name="to_"></param>
    /// <returns>True if date less than to and greater than from_ </returns>
    public static bool Between(this DateTime date_, DateTime from_, DateTime to_)
    {
      return from_ < date_ && date_ < to_;
    }

    /// <summary>
    /// Extension message to log message
    /// </summary>
    /// <param name="instance"></param>
    /// <param name="message"></param>
    public static void Log(this object instance, string message)
    {
      if (instance.IsNull() == false)
        _log.Info(instance.GetType().ToString(), message);
      else
        _log.Info("NULL", message);
    }

    /// <summary>
    /// This will return selected values for list of keys out of the idictionary
    /// </summary>
    /// <typeparam name="K">Key Type</typeparam>
    /// <typeparam name="V">Value type</typeparam>
    /// <param name="dictionary_">dictionary where you want to have values extracted from</param>
    /// <param name="keys_">keys_ that corespond to wanted values</param>
    /// <returns>a list of values that corresponds to keys_</returns>
    public static List<V> GetAllValues<K, V>(this IDictionary<K, V> dictionary_, IEnumerable<K> keys_)
    {
      return keys_.Aggregate(new List<V>(), (list_, key_) =>
      {
        list_.Add(dictionary_[key_]);
        return list_;
      });
    }

    /// <summary>
    /// Checks if collection is empty
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="collection_"></param>
    /// <returns></returns>
    static public bool IsEmpty<T>(this ICollection<T> collection_)
    {
      return collection_ == null||collection_.Count == 0;
    }

    /// <summary>
    /// Checks if collection is empty
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="collection_"></param>
    /// <returns></returns>
    static public bool IsEmpty<T>(this IEnumerable<T> collection_)
    {
      return collection_== null || collection_ is ICollection<T> ? collection_.Count() == 0 : !collection_.Any();
    }


    /// <summary>
    /// This is to verify if a certain obj_ is null. if it is null 
    /// the NullReferenceException with type indicator of what is null
    /// </summary>
    /// <example>
    /// str.NullVerify().Concat("test").NullVerify();
    /// </example>
    /// <typeparam name="T"></typeparam>
    /// <param name="obj_"></param>
    /// <returns>obj_ if obj_ is not null else throwas the exception</returns>
    static public T NullVerify<T>(this T obj_) where T : class
    {
      if (obj_ == null)
      {
        throw new NullReferenceException("Object of type '" + typeof(T).Name + "' is null");
      }
      return obj_;
    }


    /// <summary>
    /// This is to verify if a certain obj_ is null. if it is null 
    /// the NullReferenceException with type indicator of what is null
    /// </summary>
    /// <example>
    /// str.NullVerify("oops").Concat("test").NullVerify();
    /// if str is null 
    /// you will get "Object of type 'string' is null: oops"
    /// </example>
    /// <typeparam name="T"></typeparam>
    /// <param name="obj_"></param>
    ///  <param name="name_">this is returned as part of exception for easy debugging</param>
    /// <returns>obj_ if obj_ is not null else throwas the exception</returns>
    static public T NullVerify<T>(this T obj_, string name_) where T : class
    {
      if (obj_ == null)
      {
        throw new NullReferenceException("Object of type '" + typeof(T).Name + "' is null: " + name_);
      }
      return obj_;
    }

    /// <summary>
    /// Checks if date_ is between inclusive From and To_
    /// </summary>
    /// <param name="date_">Input date</param>
    /// <param name="from_"></param>
    /// <param name="to_"></param>
    /// <returns>True if date less than to and greater than from_ </returns>
    public static bool BetweenInclusive(this DateTime date_, DateTime from_, DateTime to_)
    {
      return from_ <= date_ && date_ <= to_;
    }

    /// <summary>
    /// Converts and XmlNode to an XElement
    /// </summary>
    /// <param name="node">The node</param>
    /// <returns>The XElement</returns>
    public static XElement GetXElement(this XmlNode node)
    {
      XDocument xDoc = new XDocument();
      using (XmlWriter xmlWriter = xDoc.CreateWriter())
      {
        node.WriteTo(xmlWriter);
      }
      return xDoc.Root;
    }
  }

  /// <summary>
  /// Extension methods for XElement
  /// </summary>
  public static class XElementHelper
  {
    /// <summary>
    /// Returns the attribute value as string of the XML element
    /// </summary>
    /// <param name="element_">The XML element that may contain the attribute</param>
    /// <param name="attributeName_">The local name of the attribute</param>
    /// <returns>The value of <paramref name="attributeName_"/> or null if the attribute does not exist</returns>
    [CanBeNull]
    public static string GetAttributeValue([NotNull] this XElement element_, [NotNull] string attributeName_)
    {
      if (element_ == null) throw new ArgumentNullException("element_");
      if (attributeName_ == null) throw new ArgumentNullException("attributeName_");

      XAttribute attr = element_.Attribute(attributeName_);
      return attr == null ? null : attr.Value;
    }

    /// <summary>
    /// Returns the attribute value parsed to a double
    /// </summary>
    /// <param name="element_">The XML element that may contain the attribute</param>
    /// <param name="attributeName_">The local name of the attribute</param>
    /// <returns>The value of <paramref name="attributeName_"/> or NaN if the attribute does not exist or cannot be parsed</returns>
    [CanBeNull]
    public static double GetAttributeValueAsDouble([NotNull] this XElement element_, [NotNull] string attributeName_)
    {
      if (element_ == null) throw new ArgumentNullException("element_");
      if (attributeName_ == null) throw new ArgumentNullException("attributeName_");

      XAttribute attr = element_.Attribute(attributeName_);
      if (attr == null)
      {
        return Double.NaN;
      }

      if (String.IsNullOrEmpty(attr.Value))
      {
        return Double.NaN;
      }

      double val;
      return Double.TryParse(attr.Value, out val) ? val : Double.NaN;
    }

    /// <summary>
    /// Returns the attribute value parsed to a double
    /// </summary>
    /// <param name="element_">The XML element that may contain the attribute</param>
    /// <param name="attributeName_">The local name of the attribute</param>
    /// <returns>The value of <paramref name="attributeName_"/> or false if the attribute does not exist or cannot be parsed</returns>
    public static bool GetAttributeValueAsBoolean([NotNull] this XElement element_, [NotNull] string attributeName_)
    {
      if (element_ == null) throw new ArgumentNullException("element_");
      if (attributeName_ == null) throw new ArgumentNullException("attributeName_");

      const bool defaultValue = false;

      XAttribute attr = element_.Attribute(attributeName_);
      if (attr == null)
      {
        return defaultValue;
      }

      if (String.IsNullOrEmpty(attr.Value))
      {
        return defaultValue;
      }

      bool val;
      return bool.TryParse(attr.Value, out val) ? val : defaultValue;
    }

    /// <summary>
    /// Returns the attribute value parsed to a double
    /// </summary>
    /// <param name="element_">The XML element that may contain the attribute</param>
    /// <param name="attributeName_">The local name of the attribute</param>
    /// <returns>The value of <paramref name="attributeName_"/> or 0 if the attribute does not exist or cannot be parsed</returns>
    [CanBeNull]
    public static int GetAttributeValueAsInt([NotNull] this XElement element_, [NotNull] string attributeName_)
    {
      if (element_ == null) throw new ArgumentNullException("element_");
      if (attributeName_ == null) throw new ArgumentNullException("attributeName_");

      XAttribute attr = element_.Attribute(attributeName_);
      if (attr == null)
      {
        return 0;
      }

      if (String.IsNullOrEmpty(attr.Value))
      {
        return 0;
      }

      int val;
      return int.TryParse(attr.Value, out val) ? val : 0;
    }
  }
}
