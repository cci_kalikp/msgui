﻿using System.Xml.Linq;

// ReSharper disable CheckNamespace
// Allow this to exist in the wrong namespace as it makes it easier for finding
namespace System.Xml
// ReSharper restore CheckNamespace
{
  /// <summary>
  /// Extension methods for <see cref="XmlElement"/> class.
  /// </summary>
  public static class XmlElementExtensions
  {
    /// <summary>
    /// Converts <paramref name="xmlElement_"/> to a <see cref="XElement"/> instance.
    /// </summary>
    /// <param name="xmlElement_">The node to convert.</param>
    /// <returns>The new <see cref="XElement"/> object.</returns>
    public static XElement ToXElement(this XmlElement xmlElement_)
    {
      var xElement = XElement.Parse(xmlElement_.OuterXml);
      return xElement;
    }
  }
}