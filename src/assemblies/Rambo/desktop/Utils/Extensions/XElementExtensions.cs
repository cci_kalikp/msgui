﻿using System;
using System.Xml;
using System.Xml.Linq;
using JetBrains.Annotations;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Extensions
{
  /// <summary>
  /// Extension methods for XElement
  /// </summary>
  public static class XElementExtensions
  {
    /// <summary>
    /// Returns the attribute value as string of the XML element
    /// </summary>
    /// <param name="element_">The XML element that may contain the attribute</param>
    /// <param name="attributeName_">The local name of the attribute</param>
    /// <returns>The value of <paramref name="attributeName_"/> or null if the attribute does not exist</returns>
    [CanBeNull]
    public static string GetAttributeValue([NotNull] this XElement element_, [NotNull] string attributeName_)
    {
      if (element_ == null) throw new ArgumentNullException("element_");
      if (attributeName_ == null) throw new ArgumentNullException("attributeName_");

      XAttribute attr = element_.Attribute(attributeName_);
      return attr == null ? null : attr.Value;
    }

    /// <summary>
    /// Returns the attribute value parsed to a double
    /// </summary>
    /// <param name="element_">The XML element that may contain the attribute</param>
    /// <param name="attributeName_">The local name of the attribute</param>
    /// <returns>The value of <paramref name="attributeName_"/> or NaN if the attribute does not exist or cannot be parsed</returns>
    [CanBeNull]
    public static double GetAttributeValueAsDouble([NotNull] this XElement element_, [NotNull] string attributeName_)
    {
      if (element_ == null) throw new ArgumentNullException("element_");
      if (attributeName_ == null) throw new ArgumentNullException("attributeName_");

      XAttribute attr = element_.Attribute(attributeName_);
      if (attr == null)
      {
        return Double.NaN;
      }

      if (String.IsNullOrEmpty(attr.Value))
      {
        return Double.NaN;
      }

      double val;
      return Double.TryParse(attr.Value, out val) ? val : Double.NaN;
    }

    /// <summary>
    /// Returns the attribute value parsed to a double
    /// </summary>
    /// <param name="element_">The XML element that may contain the attribute</param>
    /// <param name="attributeName_">The local name of the attribute</param>
    /// <returns>The value of <paramref name="attributeName_"/> or false if the attribute does not exist or cannot be parsed</returns>
    public static bool GetAttributeValueAsBoolean([NotNull] this XElement element_, [NotNull] string attributeName_)
    {
      if (element_ == null) throw new ArgumentNullException("element_");
      if (attributeName_ == null) throw new ArgumentNullException("attributeName_");

      const bool defaultValue = false;

      XAttribute attr = element_.Attribute(attributeName_);
      if (attr == null)
      {
        return defaultValue;
      }

      if (String.IsNullOrEmpty(attr.Value))
      {
        return defaultValue;
      }

      bool val;
      return bool.TryParse(attr.Value, out val) ? val : defaultValue;
    }

    /// <summary>
    /// Returns the attribute value parsed to a double
    /// </summary>
    /// <param name="element_">The XML element that may contain the attribute</param>
    /// <param name="attributeName_">The local name of the attribute</param>
    /// <returns>The value of <paramref name="attributeName_"/> or 0 if the attribute does not exist or cannot be parsed</returns>
    [CanBeNull]
    public static int GetAttributeValueAsInt([NotNull] this XElement element_, [NotNull] string attributeName_)
    {
      if (element_ == null) throw new ArgumentNullException("element_");
      if (attributeName_ == null) throw new ArgumentNullException("attributeName_");

      XAttribute attr = element_.Attribute(attributeName_);
      if (attr == null)
      {
        return 0;
      }

      if (String.IsNullOrEmpty(attr.Value))
      {
        return 0;
      }

      int val;
      return int.TryParse(attr.Value, out val) ? val : 0;
    }

    public static XmlElement ToXmlElement(this XElement element_)
    {
      using (XmlReader reader = element_.CreateReader())
      {
        var doc = new XmlDocument();
        doc.Load(reader);
        return doc.DocumentElement;
      }
    }
  }
}