using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;

namespace MorganStanley.MSDesktop.Rambo.Desktop.Utils.Extensions
{
  ///<summary>
  /// Extension methods for asserting things about an IEnumerable
  ///</summary>
  public static class AssertExtensions
  {
    ///<summary>
    /// Assert that all entries in the first parameter are included in the second parameter
    ///</summary>
    public static void ShouldContainsAllEntriesIn<T>(
      [NotNull] this IEnumerable<T> firstCollection_, 
      [NotNull] IEnumerable<T> secondCollection_,
      string pluralOfEntryNames_ = "entries")
    {
      if (firstCollection_ == null) throw new ArgumentNullException("firstCollection_");
      if (secondCollection_ == null) throw new ArgumentNullException("secondCollection_");

      bool hasEntriesInFirstCollectionWhichAreNotInSecondCollection = secondCollection_.Except(firstCollection_).Any();

      if (hasEntriesInFirstCollectionWhichAreNotInSecondCollection)
      {
        string firstCollectionEntriesAsText = string.Join("; ", firstCollection_);
        string secondCollectionEntriesAsText = string.Join(";", secondCollection_);

        string message =
          string.Format("{0} in second collection '{1}', contains 1 or more {0} not in first collection '{2}'",
                        pluralOfEntryNames_ ,
                        secondCollectionEntriesAsText,
                        firstCollectionEntriesAsText);

        throw new Exception(message);
      }
    }
  }
}