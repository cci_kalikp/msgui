﻿using System;
using System.Windows;
using MorganStanley.MSDotNet.Runtime;

namespace MorganStanley.MSDesktop.Rambo.Admin
{
  /// <summary>
  /// Interaction logic for App.xaml
  /// </summary>
  public partial class App
  {
    [STAThread]
    public static void Main(string[] args_)
    {
      try
      {
        AssemblyResolver.Load();
        AssemblyResolver.RunMain(args_);
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message, "Startup Error", MessageBoxButton.OK, MessageBoxImage.Error);
        Current.Shutdown();
      }
    }

    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    [MSDEMain]
    public static void MSDEMain(string[] args_)
    {
      string location = null;
      string environment = null;
      string server = null;
      if (args_ != null)
      {
        for (int argIndex = 0; argIndex < args_.Length; argIndex++)
        {
          string parameter = args_[argIndex].ToLower();
          if (parameter.StartsWith("ds.env="))
          {
            environment = parameter.Substring(parameter.IndexOf("=") + 1);
          }
          else if (parameter.StartsWith("ds.loc="))
          {
            location = parameter.Substring(parameter.IndexOf("=") + 1);
          }
          else if (parameter.StartsWith("ds.serveroverrides="))
          {
            server = parameter.Substring(parameter.IndexOf("=") + 1);
          }
          //switch (args_[argIndex].ToLower())
          //{
          //  case "/l":
          //    location = args_[argIndex + 1].ToUpper();
          //    break;
          //  case "/e":
          //    environment = args_[argIndex + 1].ToUpper();
          //    break;
          //  case "/s":
          //    server = args_[argIndex + 1].ToUpper();
          //    break;
          //}
        }
      }
      App application = new App();
      AdminWindow adminWindow = new AdminWindow(location, environment, server);
      adminWindow.Show();
      application.Run();
    }
  }
}