﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using System.Xml;
using System.Xml.Xsl;
using Infragistics.Windows.DataPresenter;
using Infragistics.Windows.DataPresenter.Events;
using Microsoft.Practices.Unity;
using Microsoft.Win32;
using MorganStanley.MSDesktop.Rambo.DataServices;
using MorganStanley.MSDesktop.Rambo.API;
using MorganStanley.MSDesktop.Rambo.API.View;
using MorganStanley.MSDesktop.Rambo.API.ViewModel;
using MorganStanley.MSDesktop.Rambo.Admin.DataModels;
using MorganStanley.MSDesktop.Rambo.WpfUtils.Core.Controls;
using MorganStanley.MSDesktop.Rambo.WpfUtils.Core.ViewModel;
using MorganStanley.MSDotNet.MSNet;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;

namespace MorganStanley.MSDesktop.Rambo.Admin
{
  /// <summary>
  /// Interaction logic for AdminWindow.xaml
  /// </summary>
  public partial class AdminWindow
  {
    #region Commands

    public static readonly RoutedCommand RefreshParametersCommand = new RoutedCommand("Refresh Parameters", typeof(AdminWindow));

    public static readonly RoutedCommand CloneCommand = new RoutedCommand("Clone resource", typeof(AdminWindow));
    public static readonly RoutedCommand DeleteCommand = new RoutedCommand("Delete resource", typeof(AdminWindow));
    public static readonly RoutedCommand LoadXmlCommand = new RoutedCommand("Load resource Xml", typeof(AdminWindow));

    public static readonly RoutedCommand LoadListCommand = new RoutedCommand("Load resource list", typeof(AdminWindow));
    public static readonly RoutedCommand SaveCommand = new RoutedCommand("Save resource", typeof(AdminWindow));
    public static readonly RoutedCommand BrowseCommand = new RoutedCommand("View in Broser", typeof(AdminWindow));

    public static readonly RoutedCommand ApplyXsltCommand = new RoutedCommand("Apply Xslt", typeof(AdminWindow));
    public static readonly RoutedCommand UpdateXmlCommand = new RoutedCommand("Update Xml data", typeof(AdminWindow));
    public static readonly RoutedCommand ShareCommand = new RoutedCommand("Share resource", typeof(AdminWindow));

    public static readonly RoutedCommand LoadUserInfoCommand = new RoutedCommand("Load user info", typeof(AdminWindow));
    public static readonly RoutedCommand CopyResourceCommand = new RoutedCommand("Copy resource", typeof(AdminWindow));
    
    public static readonly RoutedCommand ExitCommand = new RoutedCommand("Exit", typeof(AdminWindow));
    #endregion

    #region Fields

    private readonly string _tempFileName = Environment.GetEnvironmentVariable("TEMP") + @"\RamboAdminTemp.xml";
    private string _selectedResourceID;
    private readonly string _location;
    private readonly string _environment;
    private readonly string _server;
    private IUnityContainer _container;
    private static RamboResourceManager _resourceManager;
    #endregion

    #region Properties

    #region HasUpdatePermission dependency property

    public bool HasUpdatePermission
    {
      get { return (bool)GetValue(HasUpdatePermissionProperty); }
      set { SetValue(HasUpdatePermissionProperty, value); }
    }

    // Using a DependencyProperty as the backing store for HasUpdatePermission.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty HasUpdatePermissionProperty =
        DependencyProperty.Register("HasUpdatePermission", typeof(bool), typeof(AdminWindow), new FrameworkPropertyMetadata(false));

    #endregion

    #region IsInEditMode dependency property

    public bool IsInEditMode
    {
      get { return (bool)GetValue(IsInEditModeProperty); }
      set { SetValue(IsInEditModeProperty, value); }
    }

    // Using a DependencyProperty as the backing store for IsInEditMode.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty IsInEditModeProperty =
        DependencyProperty.Register("IsInEditMode", typeof(bool), typeof(AdminWindow), new FrameworkPropertyMetadata(false));

    #endregion

    #region IsInProgressMessage dependency property

    public string IsInProgressMessage
    {
      get { return (string)GetValue(IsInProgressMessageProperty); }
      set { SetValue(IsInProgressMessageProperty, value); }
    }

    // Using a DependencyProperty as the backing store for IsInProgressMessage.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty IsInProgressMessageProperty =
        DependencyProperty.Register("IsInProgressMessage", typeof(string), typeof(AdminWindow), new FrameworkPropertyMetadata(String.Empty));

    #endregion

    #endregion

    #region Constructors

    internal AdminWindow()
    {
      InitializeComponent();

      CommandBindings.Add(new CommandBinding(RefreshParametersCommand, OnRefreshParameters, OnCanRefreshParameters));
      
      CommandBindings.Add(new CommandBinding(LoadXmlCommand, OnLoadXml, OnCanLoadXml));
      CommandBindings.Add(new CommandBinding(LoadListCommand, OnLoadList, OnCanLoadList));
      CommandBindings.Add(new CommandBinding(DeleteCommand, OnDelete, OnCanDelete));
      CommandBindings.Add(new CommandBinding(CloneCommand, OnClone, OnCanClone));
      CommandBindings.Add(new CommandBinding(BrowseCommand, OnBrowse, OnCanBrowse));
      CommandBindings.Add(new CommandBinding(SaveCommand, OnSave, OnCanSave));
      CommandBindings.Add(new CommandBinding(ShareCommand, OnShareResource, OnCanShareResource));

      CommandBindings.Add(new CommandBinding(ApplyXsltCommand, OnApply, OnCanApply));
      CommandBindings.Add(new CommandBinding(UpdateXmlCommand, OnUpdateXml, OnCanUpdateXml));

      CommandBindings.Add(new CommandBinding(LoadUserInfoCommand, OnLoadUserInfo));
      CommandBindings.Add(new CommandBinding(CopyResourceCommand, OnCopyResource, OnCanCopyResource));
      
      CommandBindings.Add(new CommandBinding(ExitCommand, OnExit));

      _gridResourceList.DataSource = new RamboResourceList().Items;
      _gridResourceList.MouseDoubleClick += GridResourceList_MouseDoubleClick;
      _gridResourceList.RecordActivated += _gridResourceList_RecordActivated;

      _buttonClearXml.Click += ButtonClearXmlClick;
      _buttonOpenXslt.Click += ButtonOpenXsltClick;
      _buttonClearAppliedXml.Click += ButtonClearAppliedXmlClick;

      _versionsCombo.DisplayMemberPath = "DisplayValue";
      _versionsCombo.SelectionChanged += _versionsCombo_SelectionChanged;
      ConfigContainer();
      HasUpdatePermission = CanUpdateResource();
    }

    private static bool CanUpdateResource()
    {
      return RamboResourceEntitlement.CanCurrentUserUpdateResource();
    }

    private void _gridResourceList_RecordActivated(object sender_, RecordActivatedEventArgs recordActivatedEventArgs_)
    {
      _versionsCombo.ItemsSource = null;
      LoadVersionListAsync();
    }

    private void _versionsCombo_SelectionChanged(object sender_, System.Windows.Controls.SelectionChangedEventArgs e_)
    {
      var resourceItem = GetResourceForSelectedRow();
      if (resourceItem != null)
      {
        var loadVersionResourceworkder = new BackgroundWorker();
        loadVersionResourceworkder.DoWork += loadVersionResourceworkder_DoWork;
        loadVersionResourceworkder.RunWorkerCompleted += loadVersionResourceworkder_RunWorkerCompleted;
        var item = _versionsCombo.SelectedItem as DataServiceTimeItem;
        if(item != null)
        {
          loadVersionResourceworkder.RunWorkerAsync(new VersionResourceArgs(resourceItem, item.DataServiceDate));
          _gridLoadingResourceData.Visibility = Visibility.Visible;
          _groupXml.DataContext = null;
        }   
      }
    }

    private static void loadVersionResourceworkder_DoWork(object sender_, DoWorkEventArgs e_)
    {
      var arg = (VersionResourceArgs)e_.Argument;
      if (arg == null) return;

      e_.Result = _resourceManager.LoadResourceVersion(arg.RamboResource, arg.UpdateTime);
    }

    private void loadVersionResourceworkder_RunWorkerCompleted(object sender_, RunWorkerCompletedEventArgs e_)
    {
      _gridLoadingResourceData.Visibility = Visibility.Collapsed;
      if(e_.Error != null)
      {
        TaskDialog.ShowMessage(string.Format("Error occured while loading the resource.\n {0}", e_.Error.Message), "Resource Error", TaskDialogCommonButtons.Close, VistaTaskDialogIcon.Error);
        return;
      }

      var resource = e_.Result as RamboResource;
      _groupXml.DataContext = resource;
      SetResourceIdTextBlock(resource, true);
      RefreshCommandCanExecuteStatus();
    }

    private void LoadVersionListAsync()
    {
      var resourceItem = GetResourceForSelectedRow();
      if (resourceItem != null)
      {
        var loadVersionListworkder = new BackgroundWorker();
        loadVersionListworkder.DoWork += loadVersionListworkder_DoWork;
        loadVersionListworkder.RunWorkerCompleted += loadVersionListworkder_RunWorkerCompleted;
        loadVersionListworkder.RunWorkerAsync(resourceItem);
      }
    }

    /// <summary>
    /// Set the resource Id Text Block
    /// </summary>
    /// <param name="resource_"></param>
    /// <param name="showTime_"></param>
    private void SetResourceIdTextBlock(RamboResource resource_, bool showTime_)
    {
      string resourceId = resource_ == null ? string.Empty : resource_.ResourceId;
      var item = _versionsCombo.SelectedItem as DataServiceTimeItem;
      _textBlockResourceId.Text = showTime_ && item != null ? item.DataServiceDate + " - " + resourceId : resourceId;
    }

    private static void loadVersionListworkder_DoWork(object sender_, DoWorkEventArgs e_)
    {
      var resource = e_.Argument as RamboResource;
      if (resource == null) return;

      e_.Result = _resourceManager.LoadResourceVersionList(resource);
    }

    private void loadVersionListworkder_RunWorkerCompleted(object sender_, RunWorkerCompletedEventArgs e_)
    {
      if (e_.Error != null)
      {
        TaskDialog.ShowMessage(string.Format("Error occured while loading the version list.\n{0}", e_.Error.Message), "Loading Version List Error", TaskDialogCommonButtons.Close, VistaTaskDialogIcon.Error);
        return;
      }

      var versionList = e_.Result as VersionDataKeyCollection;
      if (versionList == null) return;

      var list = new List<DataServiceTimeItem>();
      foreach (IVersionDataKey version in versionList)
      {
        var item = new DataServiceTimeItem(version.UpdateTime);
        if(versionList.IndexOf(version) == 0 )
        {
          item.DisplayValue = version.UpdateTime + " - Current";
        }
        list.Add(item);
      }
      //var updateTime = from version in versionList select version.UpdateTime;
      _versionsCombo.ItemsSource = list;
    }

    /// <summary>
    /// register the container.
    /// 
    /// </summary>
    private void ConfigContainer()
    {
      _container = new UnityContainer();
      _container.RegisterType<IShareResourceWindow, ShareResourceWindow>();
      _container.RegisterType<IErrorReportWindowView, ErrorReportWindow>();
      _container.RegisterType<ErrorReportWindowViewModel>();
      _container.RegisterType<IShareResourceViewModel, ShareResourceViewModel>();
      _container.RegisterType<UserInfoViewModel>();
      _container.RegisterType<IUserInfoWindow, UserInfoWindow>();
      _container.RegisterType<CopyResourceViewModel>();
      _container.RegisterType<ICopyResourceWindow, CopyResourceWindow>();
    }

    public AdminWindow(string location_, string environment_, string serverUrl_) : this()
    {
      if (!DataServiceManager.IsInitialised())
      {
        var loop = new MSNetLoopUIImpl();
        if (!String.IsNullOrEmpty(serverUrl_))
        {
          _server = serverUrl_;
          _location = null;
          _environment = null;
          DataServiceManager.Init(loop, new[] { _server });
          Title = string.Format("Rambo Admin {0} - Server: {1}", GetWPFApplicationVersion(), _server);
        }
        else
        {
          _location = String.IsNullOrEmpty(location_) ? Constants.DefaultLocation : location_;
          _environment = String.IsNullOrEmpty(environment_) ? Constants.DefaultEnvironment : environment_;
          _server = null;
          DataServiceManager.Init(loop, _location, _environment);
          Title = string.Format("Rambo Admin {0} - Location: {1}, Environment: {2}", GetWPFApplicationVersion(), _location, _environment);
        }
      }

      _resourceManager = new RamboResourceManager();
      FillDropDowns();
    }

    private static string GetWPFApplicationVersion()
    {
      Assembly assembly = Assembly.GetExecutingAssembly();
      FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
      return fvi.ProductVersion;
    }

    #endregion

    #region Common functions

    private void SetSelectedRow()
    {
      if (String.IsNullOrEmpty(_selectedResourceID))
      {
        return;
      }

      foreach (DataRecord record in _gridResourceList.Records)
      {
        RamboResource recordResource = record.DataItem as RamboResource;
        if (recordResource != null && recordResource.ResourceId == _selectedResourceID)
        {
          _gridResourceList.ActiveRecord = record;
          return;
        }
      }
    }

    private RamboResource GetResourceForSelectedRow()
    {
      DataRecord record = _gridResourceList.ActiveRecord as DataRecord;
      if (record != null)
      {
        return record.DataItem as RamboResource;
      }
      return null;
    }

    private static void RefreshCommandCanExecuteStatus()
    {
      CommandManager.InvalidateRequerySuggested();
    }

    private void FillDropDowns()
    {
      _comboApplet.LoadValues(ColumnName.RamboApplet);
      _comboApplication.LoadValues(ColumnName.RamboApplication);
      _comboResourceName.LoadValues(ColumnName.RamboResName);
      _comboProfile.LoadValues(ColumnName.RamboProfile);
      _comboResourceType.LoadValues(ColumnName.RamboResType);
      _comboProfile.LoadValues(ColumnName.RamboProfile);
      _comboUserName.LoadValues(ColumnName.RamboUserid);
      _comboResourceCategory.LoadValues(ColumnName.RamboResCategory);
    }

    #endregion

    #region Refresh parameters

    private void OnCanRefreshParameters(object sender_, CanExecuteRoutedEventArgs e_)
    {
      e_.CanExecute = !_comboApplet.IsLoading &&
                      !_comboApplication.IsLoading &&
                      !_comboResourceName.IsLoading &&
                      !_comboProfile.IsLoading &&
                      !_comboUserName.IsLoading;
    }

    private void OnRefreshParameters(object sender_, RoutedEventArgs e_)
    {
      FillDropDowns();
    }

    #endregion

    #region Load list of resources

    private void OnCanLoadList(object sender_, CanExecuteRoutedEventArgs e_)
    {
      e_.CanExecute = (!_comboApplet.IsVisible || _comboApplet.IsVisible && !_comboApplet.IsLoading) &&
                      (!_comboApplication.IsVisible || _comboApplication.IsVisible && !_comboApplication.IsLoading) &&
                      (!_comboResourceName.IsVisible || _comboResourceName.IsVisible && !_comboResourceName.IsLoading) &&
                      (!_comboProfile.IsVisible || _comboProfile.IsVisible && !_comboProfile.IsLoading) &&
                      (!_comboUserName.IsVisible || _comboUserName.IsVisible && !_comboUserName.IsLoading);
    }

    private void OnLoadList(object sender_, RoutedEventArgs e_)
    {
      IsInEditMode = false;
      _gridLoadingResourceList.Visibility = Visibility.Visible;
      RamboAPI.ListResources(_comboUserName.SelectedValue, _comboApplication.SelectedValue, _comboProfile.SelectedValue,
                             _comboApplet.SelectedValue, _comboResourceType.SelectedValue,
                             _comboResourceCategory.SelectedValue, _comboResourceName.SelectedValue, OnResourceListLoaded);
      _versionsCombo.ItemsSource = null;
    }

    private void OnResourceListLoaded(DSResponse response_)
    {
      _groupXml.DataContext = null;
      _gridResourceList.DataSource = new RamboResourceList().Items;
      if (response_ != null && response_.IsSuccessful)
      {
        RamboResourceList resourceList = response_.Response as RamboResourceList;
        if (resourceList != null)
        {
          _gridResourceList.DataSource = resourceList.Items;
        }
      }
      else
      {
        TaskDialog.ShowMessage("Error occured while loading the resource list.", "Resource List Error", TaskDialogCommonButtons.Close, VistaTaskDialogIcon.Error);
      }
      SetSelectedRow();
      _gridLoadingResourceList.Visibility = Visibility.Collapsed;
      RefreshCommandCanExecuteStatus();
    }

    #endregion

    #region Load resource Xml

    private void OnCanLoadXml(object sender_, CanExecuteRoutedEventArgs e_)
    {
      e_.CanExecute = _gridResourceList.ActiveRecord != null;
    }

    private void GridResourceList_MouseDoubleClick(object sender_, MouseButtonEventArgs e_)
    {
      FrameworkElement element = _gridResourceList.InputHitTest(e_.GetPosition(_gridResourceList)) as FrameworkElement;
      if (element != null)
      {
        if (_gridResourceList.ActiveRecord == element.DataContext as DataRecord)
        {
          OnLoadXml(sender_, null);
        }
      }
    }

    private void OnLoadXml(object sender_, RoutedEventArgs e_)
    {
      IsInEditMode = false;
      _gridLoadingResourceData.Visibility = Visibility.Visible;
      RamboResource resourceItem = GetResourceForSelectedRow();
      if (resourceItem != null)
      {
        RamboAPI.GetResource(resourceItem.ResourceId, OnResourceLoaded);
      }
    }

    //TODO: merge this with loadVersionResourceworkder_RunWorkerCompleted once we migrate 
    //Rambo Admin tool to use RamboResourceManager.
    private void OnResourceLoaded(DSResponse response_)
    {
      if (response_ != null )
      {
        if (response_.IsSuccessful)
        {
          RamboResource resource = response_.Response as RamboResource;
          _groupXml.DataContext = resource;
          SetResourceIdTextBlock(resource, false);
        }
        else
        {
          TaskDialog.ShowMessage("Error occured while loading the resource.", "Resource Error", TaskDialogCommonButtons.Close, VistaTaskDialogIcon.Error);
        }
      }
      _gridLoadingResourceData.Visibility = Visibility.Collapsed;
      RefreshCommandCanExecuteStatus();
    }

    #endregion

    #region Delete / undelete resource

    private void OnCanDelete(object sender_, CanExecuteRoutedEventArgs e_)
    {
      e_.CanExecute = _gridResourceList.ActiveRecord != null && HasUpdatePermission;
    }

    private void OnCanShareResource(object sender_, CanExecuteRoutedEventArgs e_)
    {
      e_.CanExecute = _gridResourceList.ActiveRecord != null && HasUpdatePermission;
    }

    private void OnShareResource(object sender_, RoutedEventArgs e_)
    {
      RamboResource selectedResource = GetResourceForSelectedRow();

      var vm = _container.Resolve<IShareResourceViewModel>();      
      vm.StartGrantPermission += StartGrantPermission;
      vm.EndGrantPermission += EndGrantPermission;
      vm.Run(selectedResource);
    }

    private void EndGrantPermission(object sender_, EventArgs e_)
    {
      if (!Dispatcher.CheckAccess())
      {
        Dispatcher.BeginInvoke(DispatcherPriority.Normal, new EventHandler(EndGrantPermission), sender_,e_);
        return;
      }
      IsInProgressMessage = null;
    }

    private void StartGrantPermission(object sender_, EventArgs e_)
    {
      IsInProgressMessage = "Sharing";
    }

    private void OnDelete(object sender_, RoutedEventArgs e_)
    {
      RamboResource selectedResource = GetResourceForSelectedRow();
      if (selectedResource != null)
      {
        TaskDialogSimpleResult answer = TaskDialog.ShowMessage(
          selectedResource.IsDeleted ? Constants.UndeleteQuestion : Constants.DeleteQuestion,
          selectedResource.IsDeleted ? Constants.UndeleteTitle : Constants.DeleteTitle,
          TaskDialogCommonButtons.YesNo,
          VistaTaskDialogIcon.Warning);
        if (answer == TaskDialogSimpleResult.No)
        {
          return;
        }
        _selectedResourceID = selectedResource.ResourceId;
        IsInProgressMessage = selectedResource.IsDeleted ? Constants.UndeletingMessage : Constants.DeletingMessage;
        RamboAPI.GetResource(selectedResource.ResourceId, OnGetDeleteableResource);
      }
    }

    private void OnGetDeleteableResource(DSResponse response_)
    {
      if (response_ != null && response_.IsSuccessful)
      {
        RamboResource resource = response_.Response as RamboResource;
        if (resource != null)
        {
          if (resource.IsDeleted)
          {
            RamboAPI.UnDeleteResource(resource, OnResourceDeleted);
          }
          else
          {
            RamboAPI.DeleteResource(resource, OnResourceDeleted);
          }
          return;
        }
      }
      OnResourceDeleted(response_);
    }

    private void OnResourceDeleted(DSResponse response_)
    {
      RamboResource resource = GetResourceForSelectedRow();
      if (response_ != null && response_.IsSuccessful)
      {
        TaskDialog.ShowMessage(
          resource.IsDeleted ? Constants.UndeleteResultSuccessful : Constants.DeleteResultSuccessful,
          resource.IsDeleted ? Constants.UndeleteTitle : Constants.DeleteTitle,
          TaskDialogCommonButtons.Close,
          VistaTaskDialogIcon.Information);
        OnLoadList(this, null);
      }
      else
      {
        TaskDialog.ShowMessage(
          resource.IsDeleted ? Constants.UndeleteResultFailed : Constants.DeleteResultFailed,
          resource.IsDeleted ? Constants.UndeleteTitle : Constants.DeleteTitle,
          TaskDialogCommonButtons.Close,
          VistaTaskDialogIcon.Error);
      }
      IsInProgressMessage = null;
    }

    #endregion

    #region Clone resource

    private void OnCanClone(object sender_, CanExecuteRoutedEventArgs e_)
    {
      e_.CanExecute = _gridResourceList.ActiveRecord != null && HasUpdatePermission;
    }

    private void OnClone(object sender_, RoutedEventArgs e_)
    {
      RamboResource selectedResource = GetResourceForSelectedRow();
      CloneParameterWindow parameterWindow;
      bool? dialogResult;

      if (selectedResource != null)
      {
        Dictionary<ColumnName, string> parameters;
        switch (selectedResource.ResourceType)
        {
          case "ConcordLayout":
            parameters = new Dictionary<ColumnName, string>
                           {
                             {ColumnName.RamboUserid, selectedResource.UserName},
                             {ColumnName.RamboProfile, selectedResource.Profile}
                           };
            parameterWindow = new CloneParameterWindow(this, parameters);
            dialogResult = parameterWindow.ShowDialog();
            if (dialogResult.HasValue && dialogResult.Value)
            {
              IsInProgressMessage = Constants.CloningMessage;
              RamboAPI.CloneConcordLayout(
                selectedResource.UserName,
                selectedResource.ApplicationName,
                selectedResource.Profile,
                parameterWindow.Parameters[ColumnName.RamboUserid].NewValue,
                parameterWindow.Parameters[ColumnName.RamboProfile].NewValue,
                OnResourceCloned);
            }
            break;
          case "Configurator":
            parameters = new Dictionary<ColumnName, string>
                           {
                             {ColumnName.RamboUserid, selectedResource.UserName},
                             {ColumnName.RamboProfile, selectedResource.Profile}
                           };
            parameterWindow = new CloneParameterWindow(this, parameters);
            dialogResult = parameterWindow.ShowDialog();
            if (dialogResult.HasValue && dialogResult.Value)
            {
              IsInProgressMessage = Constants.CloningMessage;
              RamboAPI.CloneConfigurator(
                selectedResource.UserName,
                selectedResource.ApplicationName,
                selectedResource.Profile,
                selectedResource.ConfigName,
                parameterWindow.Parameters[ColumnName.RamboUserid].NewValue,
                parameterWindow.Parameters[ColumnName.RamboProfile].NewValue,
                OnResourceCloned);
            }
            break;
          case "GridLayout":
            parameters = new Dictionary<ColumnName, string>
                           {
                             {ColumnName.RamboGridName, selectedResource.GridName},
                             {ColumnName.RamboGridLayout, selectedResource.GridLayout}
                           };
            parameterWindow = new CloneParameterWindow(this, parameters);
            dialogResult = parameterWindow.ShowDialog();
            if (dialogResult.HasValue && dialogResult.Value)
            {
              IsInProgressMessage = Constants.CloningMessage;
              RamboAPI.CloneGridLayout(
                selectedResource.AppletName,
                selectedResource.GridName,
                selectedResource.GridLayout,
                parameterWindow.Parameters[ColumnName.RamboGridName].NewValue,
                parameterWindow.Parameters[ColumnName.RamboGridLayout].NewValue,
                OnResourceCloned);
            }
            break;
          case "Miscellaneous":
            parameters = new Dictionary<ColumnName, string>
                           {
                             {ColumnName.RamboUserid, selectedResource.UserName},
                           };
            parameterWindow = new CloneParameterWindow(this, parameters);
            dialogResult = parameterWindow.ShowDialog();
            if (dialogResult.HasValue && dialogResult.Value)
            {
              IsInProgressMessage = Constants.CloningMessage;
              RamboAPI.CloneMiscellaneousLayout(
                selectedResource.UserName,
                selectedResource.ApplicationName,
                parameterWindow.Parameters[ColumnName.RamboUserid].NewValue,
                OnResourceCloned);
            }
            break;
        }
      }
    }

    private void OnResourceCloned(DSResponse response_)
    {
      if (response_ != null && response_.IsSuccessful)
      {
        TaskDialog.ShowMessage(Constants.CloneResultSuccessful, Constants.CloneResultTitle, TaskDialogCommonButtons.Close, VistaTaskDialogIcon.Information);
        OnLoadList(this, null);
      }
      else
      {
        TaskDialog.ShowMessage(Constants.CloneResultFailed, Constants.CloneResultTitle, TaskDialogCommonButtons.Close, VistaTaskDialogIcon.Error);
      }
      IsInProgressMessage = null;
    }

    #endregion

    #region View in browser

    private void OnCanBrowse(object sender_, CanExecuteRoutedEventArgs e_)
    {
      e_.CanExecute = (_groupXml.DataContext as RamboResource) != null;
    }

    private void OnBrowse(object sender_, RoutedEventArgs e_)
    {
      XmlTextWriter textWriter = new XmlTextWriter(_tempFileName, null);
      RamboResource resource = _groupXml.DataContext as RamboResource;
      SetResourceIdTextBlock(resource, true);
      if (resource != null)
      {
        textWriter.WriteRaw(resource.XmlDataStringIdented);
        textWriter.Flush();
        textWriter.Close();

        Process browserProcess = Process.Start("iexplore.exe", _tempFileName);
        if (browserProcess != null)
        {
          browserProcess.Exited += BrowserProcessExited;
        }
      }
    }

    private void BrowserProcessExited(object sender_, EventArgs e_)
    {
      try
      {
        File.Delete(_tempFileName);
      }
      catch(Exception e)
      {
        TaskDialog.ShowMessage(string.Format("Error during browsing.\n {0}", e.Message), "Browse Error", TaskDialogCommonButtons.Close,
                        VistaTaskDialogIcon.Error);
      }
    }

    #endregion

    #region Save resource

    private void OnCanSave(object sender_, CanExecuteRoutedEventArgs e_)
    {
      RamboResource editedResource = _groupXml.DataContext as RamboResource;
      e_.CanExecute = editedResource != null && !editedResource.IsDeleted && IsInEditMode && HasUpdatePermission;
    }

    private void OnSave(object sender_, RoutedEventArgs e_)
    {
      RamboResource xmlResource = _groupXml.DataContext as RamboResource;
      RamboResource selectedResource = GetResourceForSelectedRow();
      if (xmlResource != null)
      {
        if (selectedResource == null || xmlResource.ResourceId != selectedResource.ResourceId)
        {
          TaskDialogSimpleResult answer = TaskDialog.ShowMessage(
            Constants.DifferentResourcesMessage,
            Constants.DifferentResourcesTitle,
            TaskDialogCommonButtons.YesNo,
            VistaTaskDialogIcon.Warning);
          if (answer == TaskDialogSimpleResult.No)
          {
            return;
          }
        }
        // resource xml must comply with the basic xml schema of its resource type
        try
        {
          RamboResourceXmlValidator.ValidateResourceXml(xmlResource.ResourceType, xmlResource.XmlData);
        }
        catch (Exception ex)
        {
          TaskDialog.ShowMessage(
            string.Format("Invalid xml content: {0}", ex.InnerException != null ? ex.InnerException.Message : ex.Message),
            "Invalid xml content",
            TaskDialogCommonButtons.Close,
            VistaTaskDialogIcon.Error);
          return;
        }
        xmlResource.XmlData.LoadXml(_textXml.Text);
        IsInProgressMessage = Constants.SavingMessage;
        xmlResource.UpdateTime = xmlResource.DataTime = xmlResource.StartDataTime = xmlResource.EndDataTime = DateTime.Now.ToUniversalTime();
        xmlResource.ModifyBy = Environment.UserName;
        
        RamboAPI.SaveResource(xmlResource, OnResourceSaved);
      }
    }

    private void OnResourceSaved(DSResponse response_)
    {
      if (response_ != null && response_.IsSuccessful)
      {
        TaskDialog.ShowMessage(Constants.SaveResultSuccessful, Constants.SaveResultTitle, TaskDialogCommonButtons.Close, VistaTaskDialogIcon.Information);
      }
      else
      {
        TaskDialog.ShowMessage(Constants.SaveResultFailed, Constants.SaveResultTitle, TaskDialogCommonButtons.Close, VistaTaskDialogIcon.Error);
      }
      IsInProgressMessage = null;
    }

    #endregion

    #region Apply Xslt on Xml data

    private void OnCanApply(object sender_, CanExecuteRoutedEventArgs e_)
    {
      RamboResource editedResource = _groupXml.DataContext as RamboResource;
      e_.CanExecute = editedResource != null &&
                      !editedResource.IsDeleted &&
                      !String.IsNullOrEmpty(_textXsltFilePath.Text);
    }

    private void OnApply(object sender_, RoutedEventArgs e_)
    {
      try
      {
        RamboResource resource = _groupXml.DataContext as RamboResource;
        if (resource != null)
        {
          XslCompiledTransform xslt = new XslCompiledTransform();
          xslt.Load(_textXsltFilePath.Text);

          StringBuilder results = new StringBuilder();

          xslt.Transform(resource.XmlData, null, new StringWriter(results));

          _textAppliedXml.Text = results.ToString();
        }
      }
      catch (Exception exception)
      {
        TaskDialog.ShowMessage(
          Constants.ApplyXsltFailedMessage + exception.Message,
          Constants.ApplyXsltFailedTitle,
          TaskDialogCommonButtons.Close,
          VistaTaskDialogIcon.Error);
      }
    }

    #endregion

    #region Update Xml data

    private void OnCanUpdateXml(object sender_, CanExecuteRoutedEventArgs e_)
    {
      e_.CanExecute = !String.IsNullOrEmpty(_textAppliedXml.Text) && IsInEditMode;
    }

    private void OnUpdateXml(object sender_, RoutedEventArgs e_)
    {
      RamboResource resource = _groupXml.DataContext as RamboResource;
      if (resource != null)
      {
        resource.XmlDataStringIdented = _textAppliedXml.Text;
      }
    }

    #endregion

    #region Load user info

    private void OnLoadUserInfo(object sender_, RoutedEventArgs e_)
    {
      UserInfoViewModel vm = _container.Resolve<UserInfoViewModel>();

      vm.Run(_environment);
    }

    #endregion

    #region Copy Resource

    private void OnCanCopyResource(object sender_, CanExecuteRoutedEventArgs e_)
    {
      e_.CanExecute = _gridResourceList.ActiveRecord != null && HasUpdatePermission;
    }

    private void OnCopyResource(object sender_, RoutedEventArgs e_)
    {
      RamboResource resourceItem = GetResourceForSelectedRow();
      if (resourceItem != null)
      {
        CopyResourceViewModel vm = _container.Resolve<CopyResourceViewModel>();
        vm.Run(resourceItem);
      }
    }

    #endregion

    #region Exit
    private void OnExit(object sender_, RoutedEventArgs e_)
    {
        Close();
    }

    #endregion

    #region GUI event handler

    private void ButtonClearXmlClick(object sender_, RoutedEventArgs e_)
    {
      RamboResource resource = _groupXml.DataContext as RamboResource;
      if (resource != null)
      {
        resource.XmlDataStringIdented = String.Empty;
      }
    }

    private void ButtonOpenXsltClick(object sender_, RoutedEventArgs e_)
    {
      OpenFileDialog openFileDialog = new OpenFileDialog
                                        {
                                          InitialDirectory = Environment.SpecialFolder.MyDocuments.ToString(),
                                          Title = Constants.OpenXsltDialogTitle,
                                          CheckPathExists = true,
                                          CheckFileExists = true,
                                          Filter = Constants.OpenXsltDialogFilter,
                                        };
      if (openFileDialog.ShowDialog(this).Value)
      {
        _textXsltFilePath.Text = openFileDialog.FileName;
      }
    }

    private void ButtonClearAppliedXmlClick(object sender_, RoutedEventArgs e_)
    {
      _textAppliedXml.Text = String.Empty;
    }

    #endregion
  }
}
