﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using MorganStanley.MSDesktop.Rambo.API;

namespace MorganStanley.MSDesktop.Rambo.Admin
{
  /// <summary>
  /// Interaction logic for LoadingComboBox.xaml
  /// </summary>
  public partial class LoadingComboBox
  {
    #region Properties

    public string SelectedValue
    {
      get
      {
        if (_comboNormal.SelectedIndex == 0)
        {
          return Constants.AllItemString;
        }
        return _comboNormal.Text.Trim();
      }
    }

    #region IsLoading dependency property

    public bool IsLoading
    {
      get { return (bool)GetValue(IsLoadingProperty); }
      set { SetValue(IsLoadingProperty, value); }
    }

    // Using a DependencyProperty as the backing store for IsLoading.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty IsLoadingProperty =
        DependencyProperty.Register("IsLoading", typeof(bool), typeof(LoadingComboBox), new FrameworkPropertyMetadata(false, OnIsLoadingChanged));

    private static void OnIsLoadingChanged(DependencyObject obj_, DependencyPropertyChangedEventArgs arg_)
    {
      LoadingComboBox control = obj_ as LoadingComboBox;
      bool newValue = (bool) arg_.NewValue;
      if (control != null && newValue)
      {
        control._comboNormal.ItemsSource = Constants.LoadingItem;
        control._comboNormal.SelectedIndex = 0;
      }
      CommandManager.InvalidateRequerySuggested();
    }

    #endregion

    #endregion

    #region Constructors

    public LoadingComboBox()
    {
      InitializeComponent();
    }

    #endregion

    public void LoadValues(ColumnName columnName_)
    {
      IsLoading = true;
      RamboAPI.GetFieldValues(columnName_, OnLoadedCallback);
    }

    private void OnLoadedCallback(DSResponse response_)
    {
      List<string> dataSource=new List<string>();

      if (response_ != null && response_.IsSuccessful)
      {
        RamboFieldValueList itemList = response_.Response as RamboFieldValueList;
        if (itemList != null)
        {
          dataSource = itemList.ValueList;
        }
      }

      dataSource.Remove(Constants.AllItemString);
      dataSource.Sort();
      dataSource.Insert(0, Constants.AllDisplayString);

      _comboNormal.ItemsSource = dataSource;
      _comboNormal.SelectedIndex = 0;
      
      IsLoading = false;
    }
  }
}