﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows;

namespace MorganStanley.MSDesktop.Rambo.Admin.Converters
{
  [ValueConversion(typeof(string), typeof(Visibility))]
  public class IsInProgressMessageToVisibilityConverter : IValueConverter
  {
    public object Convert(object value_, Type targetType_, object parameter_, CultureInfo culture_)
    {
      if (!String.IsNullOrEmpty((string)value_))
      {
        return Visibility.Visible;
      }
      return Visibility.Collapsed;
    }

    public object ConvertBack(object value_, Type targetType_, object parameter_, CultureInfo culture_)
    {
      return null;
    }
  }
}