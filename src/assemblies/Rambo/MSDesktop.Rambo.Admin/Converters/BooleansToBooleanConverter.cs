﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace MorganStanley.MSDesktop.Rambo.Admin.Converters
{
  public class BooleansToBooleanConverter : IMultiValueConverter
  {
    private readonly object _lockObject = new object();

    private enum OperationType
    {
      And,
      Nand,
      Or,
      Nor
    }

    private static bool CalculateOneOperation(bool operand1_, bool operand2_, OperationType type_)
    {
      bool result = operand1_;

      switch (type_)
      {
        case OperationType.And:
        case OperationType.Nand:
          result = operand1_ && operand2_;
          break;
        case OperationType.Or:
        case OperationType.Nor:
          result = operand1_ || operand2_;
          break;
      }

      if (type_ == OperationType.Nand || type_ == OperationType.Nor)
      {
        result = !result;
      }

      return result;
    }

    public object Convert(object[] values_, Type targetType_, object parameter_, CultureInfo culture_)
    {
      lock (_lockObject)
      {
        if (targetType_ == typeof(bool))
        {
          // Immediately process array with only one element
          if (values_.Length == 1)
          {
            bool result = false;
            if (values_[0].GetType() == typeof(bool))
            {
              result = (bool)values_[0];
            }
            return result ? Visibility.Visible : Visibility.Collapsed;
          }

          // Set default operation
          OperationType operation = OperationType.Or;

          // Collect operation strings
          string[] operationStrings = null;
          if (parameter_ != null)
          {
            operationStrings = parameter_.ToString().Split(',');
          }

          // Do the calculation
          bool operand1 = (bool)values_[0];
          for (int index = 1; index < values_.Length; index++)
          {
            // Set the operation for the next calculation step
            if (operationStrings != null && operationStrings.Length >= index)
            {
              operation = (OperationType)Enum.Parse(typeof(OperationType), operationStrings[index - 1]);
            }

            if (values_[index].GetType() == typeof(bool))
            {
              // Get the second operand
              bool operand2 = (bool)values_[index];

              // Calculate value for next step
              operand1 = CalculateOneOperation(operand1, operand2, operation);
            }
          }

          return operand1;
        }
        throw new ArgumentException("Wrong parameters type");
      }
    }

    public object[] ConvertBack(object value_, Type[] targetTypes_, object parameter_, CultureInfo culture_)
    {
      throw new NotSupportedException();
    }
  }
}