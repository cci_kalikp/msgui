﻿using System;
using System.Globalization;
using System.Windows.Data;

using MorganStanley.MSDesktop.Rambo.API;

namespace MorganStanley.MSDesktop.Rambo.Admin.Converters
{
  [ValueConversion(typeof(ColumnName), typeof(string))]
  public class ColumnNameToTextConverter:IValueConverter
  {
    public object Convert(object value_, Type targetType_, object parameter_, CultureInfo culture_)
    {
      if(value_.GetType()!=typeof(ColumnName))
      {
        throw new ArgumentException("Argument type should be ColumnName");
      }

      return value_.ToString().Replace("Rambo", "")+":";
    }

    public object ConvertBack(object value_, Type targetType_, object parameter_, CultureInfo culture_)
    {
      return null;
    }
  }
}