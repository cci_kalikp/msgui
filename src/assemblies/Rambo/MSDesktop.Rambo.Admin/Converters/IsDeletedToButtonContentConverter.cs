﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace MorganStanley.MSDesktop.Rambo.Admin.Converters
{
  [ValueConversion(typeof(bool), typeof(string))]
  public class IsDeletedToButtonContentConverter : IValueConverter
  {
    public object Convert(object value_, Type targetType_, object parameter_, CultureInfo culture_)
    {
      if (value_.GetType() != typeof(bool))
      {
        return "Delete";
      }
      return (bool)value_ ? "Undelete" : "Delete";
    }

    public object ConvertBack(object value_, Type targetType_, object parameter_, CultureInfo culture_)
    {
      return false;
    }
  }
}