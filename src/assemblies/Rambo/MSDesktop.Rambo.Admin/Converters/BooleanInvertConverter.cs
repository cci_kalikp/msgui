﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace MorganStanley.MSDesktop.Rambo.Admin.Converters
{
  [ValueConversion(typeof(bool), typeof(bool))]
  public class BooleanInvertConverter : IValueConverter
  {
    public object Convert(object value_, Type targetType_, object parameter_, CultureInfo culture_)
    {
      return !(bool)value_;
    }

    public object ConvertBack(object value_, Type targetType_, object parameter_, CultureInfo culture_)
    {
      return !(bool)value_;
    }
  }
}