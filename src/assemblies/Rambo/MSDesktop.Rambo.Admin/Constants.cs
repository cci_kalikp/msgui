﻿using System.Collections.Generic;
using System.Windows.Media;

namespace MorganStanley.MSDesktop.Rambo.Admin
{
  public class Constants
  {
    #region Parameter constants

    internal const string DefaultLocation = "LN";
    internal const string DefaultEnvironment = "PROD";

    internal static readonly List<string> LoadingItem = new List<string> { "Loading..." };

    internal const string AllItemString = "all";
    internal const string AllDisplayString = "All values";

    public static readonly Brush DeletedResourceBackgroundColor = Brushes.Pink;

    #endregion

    #region GUI messages

    internal const string WindowTitle = "Rambo Admin - ";
    
    internal const string DeletingMessage = "Deleting selected resource is in progress...";
    internal const string DeleteTitle = "Delete resource";
    internal const string DeleteQuestion = "Are you sure you want to delete the seleted resource?";
    internal const string DeleteResultSuccessful = "Resource successfully deleted";
    internal const string DeleteResultFailed = "Deleting resource failed";
    
    internal const string UndeletingMessage = "Undeleting selected resource is in progress...";
    internal const string UndeleteTitle = "Undelete resource";
    internal const string UndeleteQuestion = "Are you sure you want to undelete the seleted resource?";
    internal const string UndeleteResultSuccessful = "Resource successfully undeleted";
    internal const string UndeleteResultFailed = "Undeleting resource failed";

    internal const string CloningMessage = "Cloning selected resource is in progress...";
    internal const string CloneResultTitle = "Cloning resource";
    internal const string CloneResultSuccessful = "Resource successfully cloned";
    internal const string CloneResultFailed = "Cloning resource failed";

    internal const string SavingMessage = "Saving resource data is in progress...";
    internal const string SaveResultTitle = "Saving resource";
    internal const string SaveResultSuccessful = "Resource succesfully saved";
    internal const string SaveResultFailed = "Saving resource failed";

    internal const string DifferentResourcesTitle = "Different resources";
    internal const string DifferentResourcesMessage = "The edited and the selected resources are not the same\n\nAre you sure you want to save changes even so?";

    internal const string OpenXsltDialogTitle = "Select an XSLT file";
    internal const string OpenXsltDialogFilter = "XSLT files (*.xslt)|*.xslt";

    internal const string ApplyXsltFailedTitle = "Apply Xslt failed";
    internal const string ApplyXsltFailedMessage = "Cannot apply Xslt on the Xml data\n\n";

    internal const string VIEW_NAME_SHARE_RESOURCE = "ShareResourceView";

    #endregion
  }
}