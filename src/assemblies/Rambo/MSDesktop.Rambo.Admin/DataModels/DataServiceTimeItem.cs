﻿// -- boqwang: JetBrain not installed
//using JetBrains.Annotations;
using MorganStanley.MSDesktop.Rambo.DataServices;

namespace MorganStanley.MSDesktop.Rambo.Admin.DataModels
{
  public class DataServiceTimeItem
  {
    // -- boqwang: JetBrain not installed
    //public DataServiceTimeItem([NotNull]DataServiceDate date_)
    public DataServiceTimeItem(DataServiceDate date_)
    {
      DataServiceDate = date_;
      //Default display value is date
      DisplayValue = DataServiceDate.ToString();
    }

    public DataServiceDate DataServiceDate { get; private set; }
    public string DisplayValue { get; set; }
  }
}
