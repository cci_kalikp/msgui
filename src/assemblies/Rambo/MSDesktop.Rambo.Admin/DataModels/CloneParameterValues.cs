﻿namespace MorganStanley.MSDesktop.Rambo.Admin.DataModels
{
  public class CloneParameterValues
  {
    public string CurrentValue { get; set; }
    public string NewValue { get; set; }
  }
}
