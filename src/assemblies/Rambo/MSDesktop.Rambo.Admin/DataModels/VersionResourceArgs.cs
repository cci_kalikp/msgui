﻿using MorganStanley.MSDesktop.Rambo.DataServices;
using MorganStanley.MSDesktop.Rambo.API;

namespace MorganStanley.MSDesktop.Rambo.Admin.DataModels
{
  public class VersionResourceArgs
  {
    public VersionResourceArgs(RamboResource resource_, DataServiceDate updateTime_)
    {
      RamboResource = resource_;
      UpdateTime = updateTime_;
    }

    public RamboResource RamboResource { get; private set; }
    public DataServiceDate UpdateTime { get; private set; }
  }
}
