﻿using System.Windows;
using System.Collections.Generic;

using MorganStanley.MSDesktop.Rambo.API;
using MorganStanley.MSDesktop.Rambo.Admin.DataModels;

namespace MorganStanley.MSDesktop.Rambo.Admin
{
  /// <summary>
  /// Interaction logic for CloneParameterWindow.xaml
  /// </summary>
  public partial class CloneParameterWindow
  {
    #region Properties

    public Dictionary<ColumnName, CloneParameterValues> Parameters { get; set; }

    #endregion

    #region Constructors

    public CloneParameterWindow()
    {
      InitializeComponent();

      _buttonClone.Click += ButtonCloneClick;
    }

    public CloneParameterWindow(Window parent_, Dictionary<ColumnName, string> parameters_) : this()
    {
      Owner = parent_; 
     
      Parameters = new Dictionary<ColumnName, CloneParameterValues>();
      foreach (KeyValuePair<ColumnName, string> parameter in parameters_)
      {
        Parameters.Add(parameter.Key, new CloneParameterValues { CurrentValue = parameter.Value });
      }
      _listParameters.ItemsSource = Parameters;
    }

    #endregion

    private void ButtonCloneClick(object sender_, RoutedEventArgs e_)
    {
      DialogResult = true;
      Close();
    }
  }
}