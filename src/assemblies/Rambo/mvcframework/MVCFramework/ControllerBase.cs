﻿using System.ComponentModel;

namespace MorganStanley.MSDesktop.Rambo.MVCFramework
{
  /// <summary>
  /// Base <see cref="Component"/> class implementing the <see cref="IController"/> interface.
  /// </summary>
  /// <remarks>
  /// By inheriting from the <see cref="Component"/> class, <see cref="ControllerBase"/> and its sub-classes
  /// can support design-time modifcation.
  /// </remarks>
  public class ControllerBase : Component, IController
  {
    #region Fields

    private IDataModel _dataSource;
    private IViewControl _viewControl;

    #endregion

    #region Instance Methods
    /// <summary>
    /// This method is called in the base class whenever the <see cref="IController.DataSource"/> property is used in an
    /// assignment.  The base functionality is to simply set the <see cref="IDataModel"/> so it can later be
    /// referenced through the getter of the <see cref="IController.DataSource"/> property.  Sub-classes should implement
    /// behavior necessary to bind the <see cref="IDataModel"/> to the <see cref="IViewControl"/>.
    /// </summary>
    /// <param name="dataSource_">The <see cref="IDataModel"/> to be bound to the <see cref="IViewControl"/>.</param>
    protected virtual void OnSetDataSource(IDataModel dataSource_)
    {
      _dataSource = dataSource_;
    }

    /// <summary>
    /// This method is called in the base class after the <see cref="ViewControl"/> property has been set, but before
    /// the IsInitialized property is set to true.  This method should contain custom initialization logic -
    /// such as, registering event handlers to the <see cref="IViewControl"/>'s events and/or setting some default data bindings.
    /// </summary>
    protected virtual void OnInitialize()
    {
      
    }

    #endregion

    #region IController Members

    /// <summary>
    /// Gets a value indicating whether this instance is initialized.
    /// </summary>
    /// <value>
    /// 	<c>true</c> if this instance is initialized; otherwise, <c>false</c>.
    /// </value>
    public bool IsInitialized { get; protected set; }

    /// <summary>
    /// Gets or sets the data source to be bound to the <see cref="IController.ViewControl"/>.
    /// </summary>
    [Bindable(false), Browsable(false), ReadOnly(true)]
    public IDataModel DataSource
    {
      get { return _dataSource; }
      set { OnSetDataSource(value); }
    }

    /// <summary>
    /// Gets the view control.
    /// </summary>
    /// <value>The view control.</value>
    IViewControl IController.ViewControl
    {
      get { return _viewControl; }
    }

    /// <summary>
    /// Initializes the specified view control.
    /// </summary>
    /// <param name="viewControl_">The view control.</param>
    void IController.Initialize(IViewControl viewControl_)
    {
      _viewControl = viewControl_;
      OnInitialize();
      IsInitialized = true;
    }

    #endregion
  }
}