using System.Collections;
using System.ComponentModel;
using MorganStanley.MSDesktop.Rambo.DataDictionary;

namespace MorganStanley.MSDesktop.Rambo.MVCFramework
{
  /// <summary>
  /// Base implementation of the <see cref="IDataModel"/>.
  /// </summary>
  public abstract class DataModelBase : IDataModel
  {
    #region Constructors

    /// <summary>
    /// Initializes a new instance of the <see cref="DataModelBase"/> class.
    /// </summary>
    protected DataModelBase()
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="DataModelBase"/> class.
    /// </summary>
    /// <param name="viewType_">The view type.</param>
    protected DataModelBase(ViewTypeInfo viewType_)
    {
      ViewType = viewType_;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="DataModelBase"/> class.
    /// </summary>
    /// <param name="viewType_">The view type.</param>
    /// <param name="list_">The list.</param>
    protected DataModelBase(ViewTypeInfo viewType_, IList list_) : this(viewType_)
    {
      List = list_;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="DataModelBase"/> class.
    /// </summary>
    /// <param name="viewType_">The view type.</param>
    /// <param name="list_">The list.</param>
    /// <param name="containsListCollection_">if set to <c>true</c> the list is a list of lists - i.e. hierarchical data.</param>
    protected DataModelBase(ViewTypeInfo viewType_, IList list_, bool containsListCollection_)
      : this(viewType_, list_)
    {
      ContainsListCollection = containsListCollection_;
    }

    #endregion

    #region Instance Properties

    /// <summary>
    /// Gets or sets the list returned by the <see cref="IListSource"/> interface.
    /// </summary>
    /// <value>The list.</value>
    protected IList List { get; set; }

    #endregion

    #region IDataModel Members

    /// <summary>
    /// Returns an <see cref="T:System.Collections.IList" /> that can be bound to a data source from an object that does not implement an <see cref="T:System.Collections.IList" /> itself.
    /// </summary>
    /// <returns>
    /// An <see cref="T:System.Collections.IList" /> that can be bound to a data source from the object.
    /// </returns>
    public IList GetList()
    {
      return List;
    }

    /// <summary>
    /// Gets a value indicating whether the collection is a collection of <see cref="T:System.Collections.IList" /> objects.
    /// </summary>
    /// <returns>
    /// true if the collection is a collection of <see cref="T:System.Collections.IList" /> objects; otherwise, false.
    /// </returns>
    public bool ContainsListCollection { get; protected set; }

    /// <summary>
    /// Gets the schema of the data.
    /// </summary>
    /// <value>The view type.</value>
    public ViewTypeInfo ViewType { get; private set; }

    #endregion
  }
}