using System.ComponentModel;
using System.Windows.Forms;

namespace MorganStanley.MSDesktop.Rambo.MVCFramework
{
  /// <summary>
  /// Generic interface used for View component of the Model-View-Controller pattern.  
  /// </summary>
  /// <remarks>
  /// This interface is used to associate a paticular <see cref="IController{T}"/> implementation with a <see cref="IViewControl{T}"/>.  The generic type is used as a self-referential template instantiation, for example:
  /// <code>
  /// <![CDATA[public class MyViewControl : ViewControlBase, IViewControl<MyViewControl>]]>
  /// ...
  /// </code>
  /// </remarks>
  /// <typeparam name="T">The secific type of <see cref="ViewControlBase"/> whose events will be processed and data will be bound to by the <see cref="IController{T}"/> implementation</typeparam>
  public interface IViewControl<T> : IViewControl
    where T : ViewControlBase
  {
    #region Instance Properties

    /// <summary>
    /// Gets the controller.
    /// </summary>
    /// <value>The controller.</value>
    new IController<T> Controller { get; }

    #endregion

    #region Instance Methods

    /// <summary>
    /// Initializes the <see cref="IController{T}"/> that will process the events of and bind data to this <see cref="Control"/>.
    /// </summary>
    /// <param name="controller_">The controller.</param>
    void InitializeController(IController<T> controller_);

    #endregion
  }

  /// <summary>
  /// Interface used for View component of the Model-View-Controller pattern.  
  /// </summary>
  public interface IViewControl : IComponent
  {
    #region Instance Properties

    /// <summary>
    /// Gets the controller.
    /// </summary>
    /// <value>The controller.</value>
    IController Controller { get; }

    #endregion

    #region Instance Methods

    /// <summary>
    /// Initializes the controller.
    /// </summary>
    /// <param name="controller_">The controller.</param>
    void InitializeController(IController controller_);

    #endregion
  }
}