﻿using System.Windows.Forms;

namespace MorganStanley.MSDesktop.Rambo.MVCFramework
{
  /// <summary>
  /// Base implementation of <see cref="IViewControl"/> interface.
  /// </summary>
  public class ViewControlBase : UserControl, IViewControl
  {
    #region Fields

    private IController _controller;

    #endregion

    #region IViewControl Members

    /// <summary>
    /// Gets the controller.
    /// </summary>
    /// <value>The controller.</value>
    IController IViewControl.Controller
    {
      get { return _controller; }
    }

    /// <summary>
    /// Initializes the controller.
    /// </summary>
    /// <param name="controller_">The controller.</param>
    void IViewControl.InitializeController(IController controller_)
    {
      _controller = controller_;
      _controller.Initialize(this);
    }

    #endregion
  }
}