using System.ComponentModel;
using MorganStanley.MSDesktop.Rambo.DataDictionary;

namespace MorganStanley.MSDesktop.Rambo.MVCFramework
{
  /// <summary>
  /// Interface used for Model component of the Model-View-Controller pattern.  
  /// </summary>
  /// <remarks>
  /// By implementing the <see cref="IListSource"/> interface, the <see cref="IDataModel"/> instances will be
  /// able to be bound directly to UI components, such as: grids, combo boxes, list boxes, etc.  The <see cref="ViewType"/>
  /// property exposes the schema of the data as defined in the <see cref="DataDictionary"/> library, and can be useful in
  /// extracting the data.
  /// </remarks>
  public interface IDataModel : IListSource
  {
    #region Instance Properties

    /// <summary>
    /// Gets the schema of the data.
    /// </summary>
    /// <value>The view type.</value>
    ViewTypeInfo ViewType { get; }

    #endregion
  }
}