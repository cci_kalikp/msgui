using System.ComponentModel;
using System.Windows.Forms;

namespace MorganStanley.MSDesktop.Rambo.MVCFramework
{
  /// <summary>
  /// Generic interface used for Controller component of the Model-View-Controller pattern.  
  /// </summary>
  /// <remarks>
  /// In the <see cref="Initialize"/> method, the <see cref="IController{T}"/> implementation will typically hook up to the events of the <see cref="ViewControlBase"/> and act appropriately on the <see cref="IDataModel"/>.  Implementations of this interface will typically have the <see cref="Initialize"/> method implemented explicitly, while all others are public.
  /// </remarks>
  /// <typeparam name="T">The specific type of <see cref="ViewControlBase"/> whose events will be processed and data will be bound to by the <see cref="IController{T}"/> implementation.</typeparam>
  public interface IController<T> : IController
    where T : ViewControlBase
  {
    #region Instance Properties

    /// <summary>
    /// Gets the <see cref="ViewControlBase"/>.
    /// </summary>
    /// <value>The view control.</value>
    new T ViewControl { get; }

    #endregion

    #region Instance Methods

    /// <summary>
    /// Initializes the specified  <see cref="ViewControlBase"/>.
    /// </summary>
    /// <param name="viewControl_">The view control.</param>
    void Initialize(T viewControl_);

    #endregion
  }

  /// <summary>
  /// Interface used for Controller component of the Model-View-Controller pattern. 
  /// </summary>
  public interface IController : IComponent
  {
    #region Instance Properties

    /// <summary>
    /// Gets or sets the data source to be bound to the <see cref="ViewControl"/>.
    /// </summary>
    IDataModel DataSource { get; set; }

    /// <summary>
    /// Gets a value indicating whether this instance is initialized.
    /// </summary>
    /// <value>
    /// 	<c>true</c> if this instance is initialized; otherwise, <c>false</c>.
    /// </value>
    bool IsInitialized { get; }

    /// <summary>
    /// Gets the view control.
    /// </summary>
    /// <value>The view control.</value>
    IViewControl ViewControl { get; }

    #endregion

    #region Instance Methods

    /// <summary>
    /// Initializes the specified view control.
    /// </summary>
    /// <param name="viewControl_">The view control.</param>
    void Initialize(IViewControl viewControl_);

    #endregion
  }
}