﻿using System.Collections;
using Infragistics.Win.UltraWinGrid;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  class CategoryOrderComparer: IComparer
  {
    public int Compare(object x, object y)
    {
      var row1 = (UltraGridGroupByRow)x;
      var row2 = (UltraGridGroupByRow)y;

      //Pick the first row in the GroupbyRow, since all the row in the
      //same category has the same category order.
      if (row1.Rows.Count > 0 && row2.Rows.Count > 0)
      {
        var col1 = (IClassWrapper)row1.Rows[0].ListObject;
        var col2 = (IClassWrapper)row2.Rows[0].ListObject;
        return col1.CategoryOrder.CompareTo(col2.CategoryOrder);
      }
      return -1;
    }
  }
}
