﻿using System.Drawing;


namespace MorganStanley.MSDesktop.Rambo.WinGridUtils.GridLayoutEditor.DataModel
{
  public interface IColumnFilterProvider
  {
    Color GetHighlightColor(ColumnLayout columnLayout_);
    bool IsFilterable(ColumnLayout columnLayout_);
  }
}
