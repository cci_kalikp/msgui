﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using JetBrains.Annotations;
using MorganStanley.MSDesktop.Rambo.AsyncUtils;
using MorganStanley.MSDesktop.Rambo.Desktop.Utils;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  public class LayoutExplorerStore //: ILayoutExplorerStore
  {
    #region Constants

    private const string ALL_LAYOUTS = "All";
    private const string COST_CENTER_LAYOUTS = "Cost Center";
    private const string OWNER_LAYOUTS = "Current User";

    #endregion

    #region Readonly & Static Fields

    private readonly IDictionary<string, GridLayoutListItem> _layoutMap;
    private readonly IDictionary<string, BindingList<GridLayoutListItem>> _layoutSettings;
    private static readonly ILogger _log = new Logger(typeof(LayoutExplorerStore));
    private ILayoutExplorer _view;
    private readonly string _applicationName;
    private readonly string _gridName;
    private readonly GridLayout _initialGridLayout;
    private readonly string _userName;

    #endregion

    #region Fields

    private AsynchronizerBackgroundWorker _currentBackGroundWorker;

    #endregion

    #region Constructors

    public LayoutExplorerStore(ILayoutExplorer view_)
    {
      _view = view_;
    }

    public LayoutExplorerStore(GridLayout initialGridLayout_,string updateId_,ILayoutExplorer view_)
      : this(view_)
    {
      _initialGridLayout = initialGridLayout_;
      _applicationName = initialGridLayout_.Application;
      _gridName = initialGridLayout_.GridName;
      _userName = updateId_;
      _layoutMap = new Dictionary<string, GridLayoutListItem>();
      _layoutSettings = new Dictionary<string, BindingList<GridLayoutListItem>>();
    }

    #endregion

    #region Instance Methods

    public void ClearStore()
    {
      _layoutSettings.Clear();
      _layoutMap.Clear();
    }

    [CanBeNull]
    public BindingList<GridLayoutListItem> GetForTypes(string layoutType_)
    {
      BindingList<GridLayoutListItem> bindingList;
      _layoutSettings.TryGetValue(layoutType_, out bindingList);
      return bindingList;
    }

    /// <summary>
    /// Gets the list item that is in the binding lists, by the name of the layout passed in
    /// </summary>
    /// <param name="gridLayout_"></param>
    /// <returns></returns>
    public GridLayoutListItem GetListItem(GridLayout gridLayout_)
    {
      GridLayoutListItem listItem;
      if (_layoutMap.TryGetValue(gridLayout_.LayoutName.ToUpper(), out listItem))
      {
        return listItem;
      }

      return null;
    }

    public void Load()
    {
      Load(OnLoadAllGridLayoutsList, DSGridLoader.PermissionType.ALL, _gridName);
    }

    #region Virual Methods

    protected virtual void Load(AsyncCallback callback_, DSGridLoader.PermissionType permissions_, string gridName_)
    {
      DSGridLoader.BeginLoadAllGridLayoutsList(_applicationName, gridName_, permissions_, callback_);
    }

    protected virtual GridLayout[] EndLoad(IAsyncResult result_)
    {
      return DSGridLoader.EndLoadAllGridLayoutsList(result_);
    }

    protected virtual GridLayout[] LoadList(DSGridLoader.PermissionType permissions_, string gridName_)
    {
      return DSGridLoader.LoadAllGridLayoutsList(_applicationName, gridName_, permissions_);
    }

    [CanBeNull]
    public virtual GridLayout LoadGridLayout(string applicationName_, string gridName_, string layoutName_, bool forceReload_)
    {
      GridLayout cachedGridLayout = DSGridLoader.LoadGridLayout(_initialGridLayout.Application, _initialGridLayout.GridName, _initialGridLayout.LayoutName, false);
      return cachedGridLayout;
    }

    public virtual void BeginLoadGridLayout(string applicationName_, string gridName_, string layoutName_, AsyncCallback asyncCallback_)
    {
      DSGridLoader.BeginLoadGridLayout(applicationName_,
                                        gridName_,
                                        layoutName_,
                                        asyncCallback_);
    }

    public virtual GridLayout EndLoadGridLayout(IAsyncResult result_)
    {
      return DSGridLoader.EndLoadGridLayout(result_);
    }

    public virtual void ClearCache()
    {
      DSGridLoader.ClearCache();
    }

    public virtual void UpdateLocalCache(GridLayout layout_)
    {
      DSGridLoader.UpdateLocalCache(layout_);
    }
    
    protected virtual void SaveLayout(GridLayout layout_, string userName_)
    {
      DSGridLoader.SaveGridLayout(layout_, userName_);
    }

    protected virtual void DeleteLayout(GridLayout layout_, string userName_)
    {
    }

    protected virtual IDictionary<string, string> GetEntitlement(GridLayout layout_)
    {
      return new Dictionary<string, string> {{layout_.UpdateID, "user"}};
    }

    public virtual bool HasEntitlement(IDictionary<string, string> groups_, string userName_)
    {
      foreach (string key in groups_.Keys)
      {
        if(key == userName_) return true;
      }
      return false;
    }

    protected virtual void GrantEntitlement(GridLayout gridLayout_, IDictionary<string, string> shareGroups_)
    {
    }

   #endregion

    public void Save(GridLayout gridLayout_)
    {
      _currentBackGroundWorker =
        new AsynchronizerBackgroundWorker();
      _currentBackGroundWorker.RunWorkerCompleted += SaveWorkerRunWorkerCompleted;
      _currentBackGroundWorker.DoWork += SaveWorkerDoWork;

      gridLayout_.LayoutName = _view.GetGridLayoutName();
      gridLayout_.UpdateID = _userName;
      _view.EnableControls(false);
      _currentBackGroundWorker.RunWorkerAsync(gridLayout_);
    }

    public void SaveAs(string layoutName_)
    {
      _currentBackGroundWorker = new AsynchronizerBackgroundWorker();

      _currentBackGroundWorker.DoWork += SaveAsVerificationBackgroundDoWork;
      _currentBackGroundWorker.RunWorkerCompleted += SaveAsVerificationBackgroundRunWorkerCompleted;
      _view.EnableControls(false);
      _currentBackGroundWorker.RunWorkerAsync(layoutName_);
    }

    public IAsyncResult BeginGetEntitlement(GridLayout gridLayout_, AsyncCallback callback_)
    {
      var async = new Asynchronizer(callback_, gridLayout_, new AsyncSettings(true));
      return async.BeginInvoke(new Func<GridLayout,IDictionary<string, string>>(GetEntitlement),gridLayout_);
    }

    public IAsyncResult BeginGrantEntitlement(GridLayout gridLayout_, IDictionary<string, string> groupsDic_, AsyncCallback callback_)
    {
      var async = new Asynchronizer(callback_, gridLayout_, new AsyncSettings(true));
      return async.BeginInvoke(new Action<GridLayout, IDictionary<string, string>>(GrantEntitlement), gridLayout_, groupsDic_);
    }

    public void EndGrantEntitlement(IAsyncResult result_)
    {
      var asr = (AsynchronizerResult)result_;
      asr.AsynchronizerParent.EndInvoke(result_);
    }

    /// <summary>
    /// Persists a particular grid grouping_ for later retrieval.
    /// </summary>
    public IDictionary<string, string> EndGetEntitlement(IAsyncResult result_)
    {
      var asr = (AsynchronizerResult)result_;
      return (IDictionary<string, string>)asr.AsynchronizerParent.EndInvoke(result_);
    }

    public void Delete(GridLayout gridLayout_)
    {
      _currentBackGroundWorker = new AsynchronizerBackgroundWorker();
      _currentBackGroundWorker.RunWorkerCompleted += DeleteWorkerRunWorkerCompleted;
      _view.EnableControls(false);
      _currentBackGroundWorker.DoWork += DeleteWorkerDoWork;
      _currentBackGroundWorker.RunWorkerAsync(gridLayout_);
    }

    /// <summary>
    /// Adds the specified layout to the binding lists
    /// </summary>
    /// <param name="layoutToAdd_"></param>
    /// <param name="isUserOwned_"></param>
    private void AddLayoutToLists(GridLayout layoutToAdd_, bool isUserOwned_, bool addToCostCenterList_)
    {
      GridLayoutListItem item = GetListItem(layoutToAdd_);
      if (item == null)
      {
        item = new GridLayoutListItem(layoutToAdd_);
        _layoutMap.Add(layoutToAdd_.LayoutName.ToUpper(), item);
      }

      BindingList<GridLayoutListItem> allLayouts;
      if (!_layoutSettings.TryGetValue(ALL_LAYOUTS, out allLayouts))
      {
        allLayouts = new BindingList<GridLayoutListItem>();
        _layoutSettings.Add(ALL_LAYOUTS, allLayouts);
      }
      if (!allLayouts.Contains(item))
        allLayouts.Add(item);

      BindingList<GridLayoutListItem> costcenter;
      if (addToCostCenterList_)
      {
        if (!_layoutSettings.TryGetValue(COST_CENTER_LAYOUTS, out costcenter))
        {
          costcenter = new BindingList<GridLayoutListItem>();
          _layoutSettings.Add(COST_CENTER_LAYOUTS, costcenter);
        }
        if (!costcenter.Contains(item))
        {
          costcenter.Add(item);
        }
        costcenter.RaiseListChangedEvents = true;
      }

      BindingList<GridLayoutListItem> owner;
      if (isUserOwned_)
      {
        if (!_layoutSettings.TryGetValue(OWNER_LAYOUTS, out owner))
        {
          owner = new BindingList<GridLayoutListItem>();
          _layoutSettings.Add(OWNER_LAYOUTS, owner);
        }
        if (!owner.Contains(item))
        {
          owner.Add(item);
        }
      }

      allLayouts.RaiseListChangedEvents = true;
    }

    /// <summary>
    /// Completes the loading of all user layouts.  This will not load the column specs, but will 
    /// populate the owner info.
    /// </summary>
    /// <param name="result_"></param>
    private void OnLoadAllGridLayoutUserList(IAsyncResult result_)
    {
      if (_view.InvokeRequired)
      {
        _view.BeginInvoke(new AsyncCallback(OnLoadAllGridLayoutUserList), new Object[] {result_});
        return;
      }

      var userGridLayouts = EndLoad(result_);

      if (userGridLayouts != null)
      {
        foreach (GridLayout gridLayout in userGridLayouts)
        {
          AddLayoutToLists(gridLayout, true, false);
          gridLayout.UpdateID = _userName;
        }
      }

      _view.EnableControls(true);
    }

    /// <summary>
    /// completes the initial loading of all layouts.  this will just have list information not the full 
    /// layout spec
    /// </summary>
    /// <param name="result_"></param>
    private void OnLoadAllGridLayoutsList(IAsyncResult result_)
    {
      if (_view.InvokeRequired)
      {
        _view.BeginInvoke(new AsyncCallback(OnLoadAllGridLayoutsList), new Object[] {result_});
        return;
      }
      try
      {
        ClearStore();
        // Retrieve layouts and sort by layout name order
        var gridLayouts = EndLoad(result_);

        if (gridLayouts != null)
        {
          foreach (GridLayout layout in gridLayouts)
          {
            if (!_layoutMap.ContainsKey(layout.LayoutName.ToUpper()))
            {
              GridLayout layoutToAdd = layout;

              // If layout is the intial layout, don't overwrite, 
              // we want to keep the layout details provided
              if (layout.LayoutName.Equals(_initialGridLayout.LayoutName))
              {
                _initialGridLayout.UpdateID = layout.UpdateID;
                layoutToAdd = _initialGridLayout;
              }

              AddLayoutToLists(layoutToAdd, false, false);
            }
          }

          // set the combo datasrouce for all layout again
          BindingList<GridLayoutListItem> list = GetForTypes(ALL_LAYOUTS);
          _view.DataSource.DataSource = list;
          _view.BindLayoutCombo(list);

          if (!_layoutMap.ContainsKey(_initialGridLayout.LayoutName.ToUpper()))
            //check if initial layout was retrieved from ds
            //if not there create a layout with the same name
          {
            AddLayoutToLists(_initialGridLayout, _initialGridLayout.UpdateID == _userName, true);
            _layoutMap[_initialGridLayout.LayoutName.ToUpper()].IsNew = true;
          }

          _view.EnableControls(false);
          // Loads the data for all layouts that belong to this user
          Load(OnLoadAllGridLayoutUserList, DSGridLoader.PermissionType.User, _gridName);

          _view.EnableControls(false);
          //Loads Costcenter layouts that user belongs to
          Load(OnLoadCostCenterGridLayoutList, DSGridLoader.PermissionType.CostCenter, _gridName);
        }
        else if (_initialGridLayout != null)
          //in case no layouts get a dummy layout from initial layout
          // this means layout was not saved
        {
          AddLayoutToLists(_initialGridLayout, _initialGridLayout.UpdateID == _userName, true);
          _layoutMap[_initialGridLayout.LayoutName.ToUpper()].IsNew = true;

          _view.SetCostCenterRadioButton(true);
          _view.EnableControls(true);
          _view.EnableControls(true);
        }
      }
      catch (Exception e)
      {
        // This may happen if DataServices have not been initialised by the 
        // calling application. Don't fail carry on without loading layouts.
        _log.Error("OnLoadAllGridLayoutsList", "Error Loading All GridLayoutsList", e);
      }
      //}
    }

    /// <summary>
    /// completes the initial loading of all layouts.  this will just have list information not the full 
    /// layout spec
    /// </summary>
    /// <param name="result_"></param>
    private void OnLoadCostCenterGridLayoutList(IAsyncResult result_)
    {
      if (_view.InvokeRequired)
      {
        _view.BeginInvoke(new AsyncCallback(OnLoadCostCenterGridLayoutList), new Object[] {result_});
        return;
      }

      bool isInitialLayoutInCostCenter = false;
      var userGridLayouts = EndLoad(result_);
      if (userGridLayouts != null)
      {
        foreach (GridLayout gridLayout in userGridLayouts)
        {
          if (gridLayout.LayoutName.Equals(_initialGridLayout.LayoutName))
          {
            isInitialLayoutInCostCenter = true;
          }
          AddLayoutToLists(gridLayout, false, true);
        }
      }

      if (DSGridLoader.DoCostCentreSubscription && isInitialLayoutInCostCenter)
      {
        _view.SetCostCenterRadioButton(true);
      }
      else
      {
        _view.SetAllLayoutRadioButton(true);
      }

      _view.EnableControls(true);
    }

    /// <summary>
    /// this will fetch a layout from DS with a given name needed to make sure that when you save
    /// some user did not create a layout with the same name
    /// 
    /// </summary>
    private bool TryToGetFromDs(string layoutName_, out GridLayoutListItem gridLayoutListItem_)
    {
      gridLayoutListItem_ = null;
      ClearCache();
      var layouts = LoadList(DSGridLoader.PermissionType.ALL, _gridName);
      //We need to do this because of casing issue (we are not allowing different casing because of the problem with 
      //get string exact. 

      //TODO: We need to do additional loads to make sure we have valid entries in cache for lists 
      //this needs to be changed and logic needs to be moved out of here into DSGridLoader
      //THis  function is better done in the DSClient code in the event based model.
      LoadList(DSGridLoader.PermissionType.CostCenter, _gridName);
      LoadList(DSGridLoader.PermissionType.Group, _gridName);
      LoadList(DSGridLoader.PermissionType.User, _gridName);

      if (layouts != null)
      {
        foreach (GridLayout gridLayout in layouts)
        {
          if (gridLayout.LayoutName.ToUpper().Equals(layoutName_.ToUpper()))
          {
            gridLayoutListItem_ = new GridLayoutListItem(gridLayout);
            ClearStore();
            Load();
            return true;
          }
        }
      }
      return false;
    }

    private void SaveLayoutAs(string newLayoutName_)
    {
      _view.EnableControls(false);
      _currentBackGroundWorker = new AsynchronizerBackgroundWorker();

      _currentBackGroundWorker.DoWork += SaveAsLayoutDoWork;
      _currentBackGroundWorker.RunWorkerCompleted += SaveAsLayoutRunWorkerCompleted;

      var args = new LayoutExplorerController.SaveLayoutAsEventArg
                   {
                     CurrentLayout = _view.SelectedLayout,
                     NewLayoutName = newLayoutName_,
                     NewLayout = _view.GetGridLayout()
                   };
      _currentBackGroundWorker.RunWorkerAsync(args);
    }

    #endregion

    #region Event Handling
    
    private void SaveAsLayoutDoWork(object sender_, DoWorkEventArgs e_)
    {
      GridLayoutListItem listItem;
      var args = (LayoutExplorerController.SaveLayoutAsEventArg) e_.Argument;
      //GridLayout newGrid = _view.GetGridLayout();
      GridLayout newGrid = args.NewLayout;
      newGrid.LayoutName = args.NewLayoutName;

      //args.CurrentLayout = LoadGridLayout(args.CurrentLayout.Application,
      //                                         args.CurrentLayout.GridName,
      //                                         args.CurrentLayout.LayoutName,
      //                                         false);

      var worker = (IBackgroundWorker) sender_;
      if (worker.CancellationPending)
      {
        e_.Cancel = true;
      }
      else
      {
        if (_layoutMap.TryGetValue(args.NewLayoutName.ToUpper(), out listItem))
        {
          listItem.GridLayout = newGrid;
          listItem.IsLoaded = true;
          listItem.HasChanges = true;
        }
        newGrid.UpdateID = _userName;
        SaveLayout(newGrid, _userName);
        e_.Result = newGrid;
      }
    }

    private void SaveAsLayoutRunWorkerCompleted(object sender_, RunWorkerCompletedEventArgs e_)
    {
      if (!e_.Cancelled && e_.Result != null && e_.Error == null)
      {
        //AddLayoutToLists(e_.Result as GridLayout, true, true);
        //_view.SetComboSelectedIndex(((GridLayout) e_.Result).LayoutName);
        _view.EnableControls(true);
        _view.SetDialogResult(true);
      }
      else if (!e_.Cancelled)
      {
        MessageBox.Show("Error Saving Columns.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        _view.SetDialogResult(false);
      }
    }

    private void SaveAsVerificationBackgroundDoWork(object sender_, DoWorkEventArgs e_)
    {
      var result = new LayoutExplorerController.VerificationSaveAsyncResult {LayoutName = e_.Argument as string};
      GridLayoutListItem listItem;
      var worker = (IBackgroundWorker) sender_;
      if (worker.CancellationPending)
      {
        e_.Cancel = true;
      }
      else
      {
        if (_layoutMap.TryGetValue(result.LayoutName.ToUpper(), out listItem) ||
            TryToGetFromDs(result.LayoutName, out listItem))
        {
          result.IsExisting = true;
          result.ExistingLayoutItem = listItem;
          //Make sure we never create layouts with the same name 
          //and different casings
          result.LayoutName = listItem.GridLayout.LayoutName;
        }
        else
        {
          result.IsExisting = false;
        }
        e_.Result = result;
      }
    }


    private void SaveAsVerificationBackgroundRunWorkerCompleted(object sender_, RunWorkerCompletedEventArgs e_)
    {
      if (!e_.Cancelled && e_.Error == null)
      {
        var doWorkResult = (LayoutExplorerController.VerificationSaveAsyncResult) e_.Result;
        GridLayoutListItem listItem = doWorkResult.ExistingLayoutItem;
        _view.EnableControls(true);
        if (doWorkResult.IsExisting)
        {
          BeginGetEntitlement(listItem.GridLayout, DoneGetEntitlementForSaveAs);
          return;
        }

        SaveLayoutAs(doWorkResult.LayoutName);

        //_view.SetDialogResult(true);
      }
    }

    private void DoneGetEntitlementForSaveAs(IAsyncResult result_)
    {
      if (_view.InvokeRequired)
      {
        _view.BeginInvoke(new AsyncCallback(DoneGetEntitlementForSaveAs), new Object[] { result_ });
        return;
      }

      try
      {
        var asr = (AsynchronizerResult) result_;
        IDictionary<string, string> groups = asr.AsynchronizerParent.EndInvoke(result_) as IDictionary<string, string>;

        if (groups != null)
        {
          if (!HasEntitlement(groups, Environment.UserName))
          {
            // Todo: move this to view
            MessageBox.Show("Can not save the Columns setting.\nColumns setting with this name is owned by another user.",
                "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
          }

          // Todo: move this to view
          if (MessageBox.Show("You already own a Columns setting with this name. Do you want to overwrite?", "Overwrite?",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.No)
          {
            return;
          }

          GridLayout layout = result_.AsyncState as GridLayout;
          if(layout != null)
          {
            SaveLayoutAs(layout.LayoutName);
          }
        }
      }
      catch (Exception e_)
      {
        _log.Error("DoneGetEntitlement", e_.Message, e_);
      }
    }

    private void SaveWorkerRunWorkerCompleted(object sender_, RunWorkerCompletedEventArgs e_)
    {
      //m_stopClose = false;
      _view.EnableControls(true);
      if (!e_.Cancelled)
      {
        if (e_.Error != null)
        {
          //TODO: move this logic to view.
          MessageBox.Show(
            "Unable to save Columns changes currently.",
            "Columns Save Failed",
            MessageBoxButtons.OK, MessageBoxIcon.Error,
            MessageBoxDefaultButton.Button1);
          _view.SetDialogResult(false);
        }

        bool hasSaveEntitlement = e_.Result is bool ? (bool) e_.Result : false;
        if(!hasSaveEntitlement)
        {
          _view.ShowWarningMessage("The Columns could not be saved, since you are not the current owner.\nPlease use 'Save As' to save the Columns under a new name.", "The Columns could not be saved");
        }
        _view.SetDialogResult(true);
      }
    }

    private void DeleteWorkerRunWorkerCompleted(object sender_, RunWorkerCompletedEventArgs e_)
    {
      //m_stopClose = false;
      _view.EnableControls(true);

      if (!e_.Cancelled)
      {
        _view.IsLayoutDeleted = true;
        //Refresh the dropdown list after delete
        Load();
      }
    }

    #endregion

    #region Static Methods

    private void SaveWorkerDoWork(object sender_, DoWorkEventArgs e_)
    {
      var backgroundWorker = (IBackgroundWorker) sender_;
      if (backgroundWorker.CancellationPending)
      {
        e_.Cancel = true;
      }
      else
      {
        // Get the layout name - may have been modified and so will need to persist the
        // grid layout under the new name.
        if (e_.Argument != null)
        { 
          var gridL = (GridLayout) e_.Argument;
          //check the entitlement first before save.
          IDictionary<string, string> groups = GetEntitlement(gridL);
          bool haspermission = HasEntitlement(groups, gridL.UpdateID);
          e_.Result = haspermission;
          if (!haspermission) return;
          SaveLayout(gridL, gridL.UpdateID);
        }
      }
    }

    private void DeleteWorkerDoWork(object sender_, DoWorkEventArgs e_)
    {
      var backgroundWorker = (IBackgroundWorker)sender_;
      if (backgroundWorker.CancellationPending)
      {
        e_.Cancel = true;
      }
      else
      {
        // Get the layout name - may have been modified and so will need to persist the
        // grid layout under the new name.
        if (e_.Argument != null)
        {
          var gridL = (GridLayout)e_.Argument;
          DeleteLayout(gridL, gridL.UpdateID);
        }
      }
    }

    #endregion

    #region Properties
    public ILayoutExplorer View 
    { 
      set
      {
        _view = value;
      }
    }
    #endregion
  }
}