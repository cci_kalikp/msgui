﻿using System;
using System.ComponentModel;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  public interface IClassWrapper : IEquatable<IClassWrapper>
  {
    string ColumnName { get; }

    [Browsable(false)]
    bool Select { get; set; }

    [Browsable(false)]
    int Index { get; set; }

    string Category { get; set; }

    [Browsable(false)]
    int CategoryOrder { get; set; }
  }
}
