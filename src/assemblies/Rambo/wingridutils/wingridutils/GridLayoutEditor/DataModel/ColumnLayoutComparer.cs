﻿using System;
using System.Collections.Generic;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils.GridLayoutEditor.DataModel
{
  public class ColumnLayoutComparer : IEqualityComparer<ColumnLayout>
  {
    // only compare Properties that can be modified from grid by the user
    public bool Equals(ColumnLayout x_, ColumnLayout y_)
    {
      return x_.BackColorValue == y_.BackColorValue
        && x_.FontName == y_.FontName
        && x_.ForeColorValue == y_.ForeColorValue
        && x_.Format == y_.Format
        && x_.Hidden == y_.Hidden
        && (x_.Hidden || x_.Index == y_.Index)
        && x_.Key == y_.Key
        && x_.SortDirection == y_.SortDirection
        && (x_.SortDirection == ColumnLayout.SortDirections.None || x_.SortAbsolute == y_.SortAbsolute)
        && x_.SortOrder == y_.SortOrder
        && x_.Width == y_.Width;
    }

    public int GetHashCode(ColumnLayout obj_)
    {
      return obj_.GetHashCode();
    }
  }
}
