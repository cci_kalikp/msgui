﻿using System.ComponentModel;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  public class AvailableItemsSelectorDataSource<T> :IAvailItemsSelectorDataSource
  {
    public AvailableItemsSelectorDataSource(BindingList<T> leftGridDataSource_,BindingList<T> rightGridDataSource_)
    {
      LeftGridDataSource = leftGridDataSource_;
      RightGridDataSource = rightGridDataSource_;
    }

    public BindingList<T> LeftGridDataSource { get; set; }
    public BindingList<T> RightGridDataSource { get; set; }

    public static AvailableItemsSelectorDataSource<T> CreateDataSource(BindingList<T> leftGridDataSource_, BindingList<T> rightGridDataSource_)
    {
      return new AvailableItemsSelectorDataSource<T>(leftGridDataSource_,rightGridDataSource_);
    }

  }
}
