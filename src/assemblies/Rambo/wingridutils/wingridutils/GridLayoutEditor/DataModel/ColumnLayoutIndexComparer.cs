using System;
using System.Collections;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  /// <summary>
  /// Class to perform comparison operations on two ColummLayout objects. Used to perform index-driven
  /// sorting operations on a ColumnLayout collection
  /// </summary>
  internal class ColumnLayoutIndexComparer : IComparer
  {
    int IComparer.Compare(Object x, Object y)
    {
      ColumnLayout col1 = (ColumnLayout)x;
      ColumnLayout col2 = (ColumnLayout)y;

      if(col1.Index == col2.Index)
      {
        return string.Compare(col1.Caption, col2.Caption);
      }

      return (col1.Index < col2.Index) ? -1 : 1;
    }
  }

}
