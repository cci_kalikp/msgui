﻿using System;
using System.ComponentModel;
using Infragistics.Win.UltraWinGrid;


namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  public class ColumnWrapper : IClassWrapper
  {
    #region Readonly & Static Fields

    #endregion

    #region Constructors

    public ColumnWrapper(ColumnLayout col)
    {
      ColumnLayout = col;
    }

    #endregion

    #region Instance Properties

    [Browsable(false)]
    public ColumnLayout ColumnLayout { get; private set; }

    [ReadOnly(true)]
    public string ColumnName
    {
      get { return ColumnLayout.Caption.Replace('\n', ' '); }
    }

    [Browsable(false)]
    public string Description
    {
      get { return ColumnLayout.Description; }
      set { ColumnLayout.Description = value; }
    }

    [Browsable(false)]
    public bool Select { get; set; }

    [Browsable(false)]
    public bool Hidden
    {
      get { return ColumnLayout.Hidden; }
      set { ColumnLayout.Hidden = value; }
    }

    [Browsable(false)]
    public int Index
    {
      get { return ColumnLayout.Index; }
      set { ColumnLayout.Index = value; }
    }

    [DisplayName("Asc/Desc")]
    public string SortOrder
    {
      get
      {
        switch (ColumnLayout.SortDirection)
        {
          case ColumnLayout.SortDirections.None:
            return "None";
          case ColumnLayout.SortDirections.Ascending:
            return "Asc";
          case ColumnLayout.SortDirections.Descending:
            return "Desc";
        }
        return "None";
      }
    }

    [DisplayName("Abs")]
    public string SortAbs
    {
      get
      {
        if(ColumnLayout.SortAbsolute)
        {
          return "Abs";
        }
        return string.Empty;
      }
      set
      {
        if(value == "Abs")
        {
          ColumnLayout.SortAbsolute = true;
        }
        ColumnLayout.SortAbsolute = false;
      }
    }

    public string Category
    {
      get { return ColumnLayout.FieldCategory; }
      set { ColumnLayout.FieldCategory = value; }
    }

    [Browsable(false)]
    public int CategoryOrder
    {
      get { return ColumnLayout.FieldCategoryOrder; }
      set { ColumnLayout.FieldCategoryOrder = value; }
    }

    public static ColumnLayout[] GetColumnLayouts(SelectedRowsCollection rows_)
    {
      var colLayouts = new ColumnLayout[rows_.Count];

      for (int i = 0; i < rows_.Count; i++)
      {
        colLayouts[i] = ((ColumnWrapper)rows_[i].ListObject).ColumnLayout;
      }
      return colLayouts;
    }

    #endregion
    
    public bool Equals(IClassWrapper other)
    {
      return ColumnName == other.ColumnName;
    }     
  }
}
