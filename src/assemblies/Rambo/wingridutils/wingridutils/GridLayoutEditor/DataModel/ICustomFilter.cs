using System.Collections;
using System.Collections.Generic;
using Infragistics.Win.UltraWinGrid;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  public interface ICustomFilter
  {

    string FilterName { get; }  
    List<FilterComparisionOperator> AllowedComparisonOperators
    {
      get;
    }
    string SearchCriteria { get; set;}
    FilterComparisionOperator Operator { get;set;}
    IList Filter(IList listToFilter_);
  }

}
