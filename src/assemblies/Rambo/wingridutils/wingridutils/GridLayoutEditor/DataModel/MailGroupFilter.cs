using System.Collections.Generic;
using System.Collections;
using Infragistics.Win.UltraWinGrid;
using MorganStanley.MSDotNet.Directory;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{

  public class MailGroupFilter: ICustomFilter
  {
    private string _searchCriteria;
    private string _userName;
    public MailGroupFilter(string userName_)
    {
      _userName = userName_; 
    }


    public MailGroupFilter()
    {
      
    }
    public string FilterName
    {
      get { return "Mail Group Filter"; }
    }

    public override string ToString()
    {
      return FilterName;
    }

    public List<FilterComparisionOperator> AllowedComparisonOperators
    {
      get { 
        return new List<FilterComparisionOperator>(new FilterComparisionOperator[]{
          FilterComparisionOperator.Equals});
      }
    }

    public string SearchCriteria
    {
      get { return _searchCriteria; }
      set { _searchCriteria = value; }
    }

    private FilterComparisionOperator _operator;
    public FilterComparisionOperator Operator
    {
      get { return _operator; }
      set { _operator = value; }
    }

    public IList Filter(IList listIn)
    {
      if (!SearchCriteria.Equals(string.Empty))
      {
        Search theSearch = new FWD2Search();
        theSearch.Username = "cn=dsserverid, ou=ldapids, o=Morgan Stanley";
        theSearch.Password = "DS_Server";
        List<GridLayoutListItem> listOut = new List<GridLayoutListItem>();

        Group theGroup = theSearch.GetGroup(SearchCriteria);
        if (theGroup != null)
        {
          var mapOfPeopleInTheGroup = theGroup.PeopleInGroup;
          foreach (object resource in listIn)
          {
            if (resource != null && (resource as GridLayoutListItem).GridLayout.UpdateID!= null &&
                mapOfPeopleInTheGroup.ContainsKey((resource as GridLayoutListItem).GridLayout.UpdateID))
            {
              listOut.Add(resource as GridLayoutListItem);
            }
          }
        }
        return listOut;
      }
      else
      {
        return listIn;
      }
    }

  }
}
