using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinTabControl;
using MorganStanley.MSDesktop.Rambo.WinGridUtils.GridLayoutEditor.DataModel;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  public partial class LayoutEditor : UserControl, ILayoutEditor
  {
    #region Constructors

    public event EventHandler<EventArgs> ImportLiveSortingChanged;
    /// <summary>
    /// Initialises a new instance of the CntrlFormatDetails User Control.
    /// </summary>
    public LayoutEditor()
    {
      // This call is required by the Windows.Forms Form Designer.
      InitializeComponent();

      if (!GridLayoutFactory.AllowGridLevelSetting)
      {
        ultraTabControl.Controls.Remove(_tabPage_GridLevel);
      }
      Controller = new LayoutEditorController(this, _availItemsSelectorControl.Controller);
      _availItemsSelectorControl.SetDescriptions += SetDescriptionforTwoGrids;
      _layoutEditorSortingControl.AvailItemsSelector.SetDescriptions += SetDescriptionforTwoGrids;
      EnableDerivedFieldFiltering = false;
    }

    #endregion

    #region Instance Properties

    public IDictionary ColumnCategories
    {
      get { return Controller.ColumnCategories; }
      set { Controller.ColumnCategories = value; }
    }

    public IDictionary ColumnDescriptions
    {
      get { return Controller.ColumnDescriptions; }
      set { Controller.ColumnDescriptions = value; }
    }

    public IDictionary TurboRiskGroups
    {
      get { return Controller.TurboRiskGroups; }
      set { Controller.TurboRiskGroups = value; }
    }

    public LayoutEditorController Controller { get; set; }

    public LayoutEditorSortingController LayoutEditorSortingController
    {
      get { return _layoutEditorSortingControl.Controller; }
    }

    public GridLayout EditableLayout
    {
      set { Controller.EditableLayout = value; }
    }

    public bool EnableFormatChanges
    {
      set { _tabPage_Format.Enabled = value; }
      get { return _tabPage_Format.Enabled; }
    }

    public bool EnableSortChanges
    {
      set { _tabPage_Sorting.Enabled = value; }
      get { return _tabPage_Sorting.Enabled; }
    }

    public bool StopClose
    {
      get { return Controller.StopClose; }
      set { Controller.StopClose = value; }
    }

    public string UserName
    {
      get { return Controller.UserName; }
      set { Controller.UserName = value; }
    }

    public IList<string> VerifyColumnKeysList
    {
      get { return Controller.VerifyColumnKeysList; }
      set { Controller.VerifyColumnKeysList = value; }
    }

    public IColumnFilterProvider ColumnFilterProvider
    {
      get { return _availItemsSelectorControl.ColumnFilterProvider;
        }
      set { 
        _availItemsSelectorControl.ColumnFilterProvider = value;
        Controller.ColumnFilterProvider = value;
      }
    }

    public bool EnableDerivedFieldFiltering
    {
      get { return _showDeivedColumnsChkBox.Visible; }
      set { 
        _showDeivedColumnsChkBox.Visible = value;
        Controller.ShowDerivedColumns = !value;
      }
    }

    #endregion

    #region Instance Methods

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    protected override void Dispose(bool disposing)
    {
      if (disposing && components != null)
        components.Dispose();

      base.Dispose(disposing);
    }

    /// <summary>
    /// Set the description for each columnLayout
    /// </summary>
    /// <param name="columnLayout_"></param>
    private void SetDescription(ColumnLayout columnLayout_)
    {
      if (columnLayout_ == null)
      {
        rtfDescription.Text = string.Empty;
        return;
      }

      Controller.SetDescription(columnLayout_);
    }

    protected void OnImportLiveSortingChanged(EventArgs e)
    {
      EventHandler<EventArgs> importLiveSortingChanged = ImportLiveSortingChanged;
      if (importLiveSortingChanged != null)
        importLiveSortingChanged(this, e);
    }

    #endregion

    #region Event Handling

    private void BtnAvailGroupby_Click(object sender, EventArgs e)
    {
      _layoutEditorSortingControl.Groupby(true, "Category");
    }

    private void BtnAvailUnGroupby_Click(object sender, EventArgs e)
    {
      _layoutEditorSortingControl.UnGroupby(true, "ColumnName");
    }

    private void BtnHiddenGroupby_Click(object sender, EventArgs e)
    {
      _availItemsSelectorControl.Groupby(true, "Category");
    }

    private void BtnHiddenUnGroupby_Click(object sender, EventArgs e)
    {
      _availItemsSelectorControl.UnGroupby(true, "ColumnName");
    }

    private void BtnImportLiveSorting_Click(object sender, EventArgs e)
    {
      OnImportLiveSortingChanged(EventArgs.Empty);
    }

    private void FormatedultraGrid_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
    {
      if (_formattedUltraGrid.Selected.Rows.Count == 1)
      {
        var colWrapper = (ColumnWrapper) _formattedUltraGrid.Selected.Rows[0].ListObject;
        ColumnLayout colLayout = colWrapper.ColumnLayout;

        pgrdVisibleColumns.SelectedObject = colLayout;
        SetDescription(colLayout);
      }
      else
      {
        pgrdVisibleColumns.SelectedObject = null;
        pgrdVisibleColumns.SelectedObjects =
          ColumnWrapper.GetColumnLayouts(_formattedUltraGrid.Selected.Rows);
        SetDescription(null);
      }
    }

    private void FormattedutraGrid_InitializeLayout(object sender, InitializeLayoutEventArgs e)
    {
      var ultraGrid = sender as UltraGrid;
      if (ultraGrid == null) return;

      ultraGrid.DisplayLayout.Bands[0].Columns[0].SortIndicator = SortIndicator.Disabled;
      ultraGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
      ultraGrid.DisplayLayout.Override.CellClickAction = CellClickAction.RowSelect;
      ultraGrid.DisplayLayout.Bands[0].Columns["ColumnName"].Header.Caption = "Visible Columns";

      foreach (var column in ultraGrid.DisplayLayout.Bands[0].Columns)
      {
        if (column.Key != "ColumnName")
        {
          column.Hidden = true;
        }
      }
    }

    private void SelectedTabChanged(object sender_, SelectedTabChangedEventArgs e_)
    {
      _descriptionGroupBox.Visible = e_.Tab.Index != 3;
    }

    private void SetDescriptionforTwoGrids(object sender_, SetDescriptionEventArgs eventArgs_)
    {
      IClassWrapper columnWrapper = eventArgs_.Wrapper;
      if (columnWrapper == null) return;

      ColumnLayout columnLayout = ((ColumnWrapper) columnWrapper).ColumnLayout;
      SetDescription(columnLayout);
    }    

    #endregion

    #region ILayoutEditor Members

    public void DoSetDescription(string caption_, string description_, string category_, string turboRiskId)
    {
      if (turboRiskId != null)
      {
        rtfDescription.Text = string.Format("{0}: {1}" + Environment.NewLine + "Category: {2}" + Environment.NewLine + "Turbo Risk Group: {3}",
          caption_, description_, category_, turboRiskId);
      }
      else
        rtfDescription.Text = string.Format("{0}: {1}" + Environment.NewLine + "Category: {2}", caption_, description_, category_);

      //Delete the new line of caption.
      if (caption_.Contains("\n"))
      {
        caption_ = caption_.Replace('\n', ' ');
      }
      int categoryIndex = rtfDescription.Text.IndexOf("Category");
      int turboRiskIndex = rtfDescription.Text.IndexOf("Turbo Risk Group");

      //Set the color of text in decription.
      rtfDescription.Select(0, caption_.Length + 2);
      rtfDescription.SelectionColor = Color.Blue;
      rtfDescription.Select(caption_.Length + 2, description_.Length);
      rtfDescription.SelectionColor = Color.Black;
      rtfDescription.Select(categoryIndex, 9);
      rtfDescription.SelectionColor = Color.Blue;
      rtfDescription.Select(categoryIndex + 9, category_.Length + 1);
      rtfDescription.SelectionColor = Color.Black;

      if (turboRiskIndex != -1)
      {
        rtfDescription.Select(turboRiskIndex + 17, turboRiskId.Length + 1);
        rtfDescription.SelectionColor = Color.Black;
      }

      rtfDescription.Select(0, 0);
    }

    public void SetDataSourceForGridLevel(GridLayout layout_)
    {
      _gridLevelPropertyGrid.SelectedObject = layout_;
    }

    public void SetDataSourcesForHideVisible(AvailableItemsSelectorDataSource<IClassWrapper> dataSource_)
    {
      _availItemsSelectorControl.Controller.DataSource = dataSource_;
    }

    public void SetDataSourcesForSorting(AvailableItemsSelectorDataSource<ColumnWrapper> dataSource_)
    {
      //_layoutEditorSortingControl.AvailItemsSelector.Controller.DataSource = dataSource_;
      _layoutEditorSortingControl.Controller.DataSource = dataSource_;
    }

    public void SetDataSourceForFormattedGrid(BindingList<IClassWrapper> datasource_)
    {
      _formattedUltraGrid.DataSource = datasource_;
    }

    public void SetNumFrozenColumns(GridLayout gridLayout_)
    {
      numFrozenColumns.DataBindings.Clear();
      numFrozenColumns.DataBindings.Add("Value", gridLayout_, "LockedColumns", true);
      numFrozenColumns.Maximum = gridLayout_.Columns.Count;
    }

    public void HideAllGroupByButtons()
    {
      //We do not have any categories, hide the groupby and ungroupby button.
      _btnHiddenGoupby.Hide();
      _btnHiddenUnGroupby.Hide();
      _btnAvailGroupby.Hide();
      _btnAvailUnGroupby.Hide();
    }

    #endregion    

    private void ShowDerivedColumnsChanged(object sender_, EventArgs _)
    {
      Controller.ShowDerivedColumns = _showDeivedColumnsChkBox.Checked;
    }
  }
}