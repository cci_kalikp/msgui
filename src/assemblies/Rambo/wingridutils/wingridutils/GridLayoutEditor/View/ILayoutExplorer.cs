﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  public interface ILayoutExplorer : ISynchronizeInvoke
  {
    void EnableControls(bool enable_);
    void SetDataSourceForMailGroup(ICustomFilter filter_);
    void SetEditableGridLayout(GridLayout gridLayout_);
    void SetCursor(bool default_);
    void SetCostCenterRadioButton(bool checked_);
    void SetFilterStatusTextBox(string text_);
    void EnableMailGroupButtons(bool enable_);
    GridLayout GetGridLayout();
    string GetGridLayoutName();
    GridLayout SelectedLayout { get; }
    BindingSource DataSource { get; set; }
    void SetDialogResult(bool ok_);
    void BindLayoutCombo(ICollection<GridLayoutListItem> list_);
    void SetAllLayoutRadioButton(bool checked_);
    string GetMailGroupText();
    void SetComboSelectedIndex(string startString_);
    void ShowWarningMessage(string message_, string title_);
    void SetCurrentWriteGroup(IDictionary<string, string> groups_, GridLayout layout_);
    bool IsLayoutDeleted { get; set; }
  }
}
