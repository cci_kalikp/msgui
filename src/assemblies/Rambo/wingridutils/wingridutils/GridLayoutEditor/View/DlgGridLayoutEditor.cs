﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using MorganStanley.MSDesktop.Rambo.WinGridUtils.GridLayoutEditor.DataModel;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  public partial class DlgGridLayoutEditor : Form
  {
    #region Constructors

    public DlgGridLayoutEditor()
    {
      //
      // Required for Windows Form Designer support
      //
      InitializeComponent();
    }

        /// <summary>
    /// Initialises an instance of the DlgGridLayoutEditor class.
    /// </summary>
    /// <param name="gridLayout_">The XML representation of the underlying grid/column
    /// layout. This XML should initally be generated using the GridLayoutFactory and
    /// associated classes.</param>
    /// <param name="userName_">The user's kerberos ID.</param>
    /// <param name="enableFormatChanges_">Whether or not to display the column format editor section.</param>
    /// <param name="enableSortChanges_">if set to <c>true</c> [enable sort changes_].</param>
    /// <param name="columnDescriptions_">string => string mapping from the column ID (SourceField ID for RV) to description text (LongDescription for RV)</param>
    /// <param name="turboRiskGroups_">string => string mapping from the column ID (SourceField ID for RV) to Turbo Risk group</param>
    public DlgGridLayoutEditor(GridLayout gridLayout_, string userName_, bool enableFormatChanges_, bool enableSortChanges_, IDictionary columnDescriptions_, IDictionary turboRiskGroups_)
      : this()
		{

      Text = "Edit Columns: " + gridLayout_.LayoutName;
      _layoutExplorer.UserName = userName_;
      _layoutExplorer.InitialGridLayout = gridLayout_;
		  _layoutExplorer.EnableFormatChanges = enableFormatChanges_;
		  _layoutExplorer.EnableSortChanges = enableSortChanges_;
      _layoutExplorer.ColumnDescriptions = columnDescriptions_;
      _layoutExplorer.TurboRiskGroups = turboRiskGroups_;

		}

    /// <summary>
    /// Initialises an instance of the DlgGridLayoutEditor class.
    /// </summary>
    /// <param name="gridLayout_">The XML representation of the underlying grid/column
    /// layout. This XML should initally be generated using the GridLayoutFactory and
    /// associated classes.</param>
    /// <param name="userName_">The user's kerberos ID.</param>
    /// <param name="enableFormatChanges_">Whether or not to display the column format editor section.</param>
    /// <param name="enableSortChanges_">if set to <c>true</c> [enable sort changes_].</param>
    /// <param name="columnDescriptions_">string => string mapping from the column ID (SourceField ID for RV) to description text (LongDescription for RV)</param>
    public DlgGridLayoutEditor(GridLayout gridLayout_, string userName_, bool enableFormatChanges_, bool enableSortChanges_, IDictionary columnDescriptions_)
      : this()
    {

      Text = "Edit Columns: " + gridLayout_.LayoutName;
      _layoutExplorer.UserName = userName_;
      _layoutExplorer.InitialGridLayout = gridLayout_;
      _layoutExplorer.EnableFormatChanges = enableFormatChanges_;
      _layoutExplorer.EnableSortChanges = enableSortChanges_;
      _layoutExplorer.ColumnDescriptions = columnDescriptions_;
      _layoutExplorer.TurboRiskGroups = null;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="DlgGridLayoutEditor"/> class.
    /// </summary>
    /// <param name="gridLayout_">The grid layout_.</param>
    /// <param name="userName_">The user's kerberos ID.</param>
    /// <param name="columnDescriptions_">string => string mapping from the column ID (SourceField ID for RV) to description text (LongDescription for RV)</param>
    /// <param name="columnCategories_">string => MorganStanley.MSDesktop.Rambo.DataDictionary.Category mapping from the column ID (SourceFIeld ID for RV) to column category. e.g PositionDetails</param>
    public DlgGridLayoutEditor(GridLayout gridLayout_, string userName_, IDictionary columnDescriptions_,
	                             IDictionary columnCategories_, IDictionary turboRiskGroups_) : this(gridLayout_, userName_, columnDescriptions_)
	  {
      _layoutExplorer.ColumnCategories = columnCategories_;
      _layoutExplorer.TurboRiskGroups = turboRiskGroups_;
	  }

    /// <summary>
    /// Initializes a new instance of the <see cref="DlgGridLayoutEditor"/> class.
    /// </summary>
    /// <param name="gridLayout_">The grid layout_.</param>
    /// <param name="userName_">The user's kerberos ID.</param>
    /// <param name="columnDescriptions_">string => string mapping from the column ID (SourceField ID for RV) to description text (LongDescription for RV)</param>
    /// <param name="columnCategories_">string => MorganStanley.MSDesktop.Rambo.DataDictionary.Category mapping from the column ID (SourceFIeld ID for RV) to column category. e.g PositionDetails</param>
    /// <param name="turboRiskGroups_">string => string mapping from the column ID (SourceField ID for RV) to Turbo Risk group</param>
    public DlgGridLayoutEditor(GridLayout gridLayout_, 
                                string userName_, 
                                IDictionary columnDescriptions_,
                                IDictionary columnCategories_, 
                                IDictionary turboRiskGroups_, 
                                LayoutExplorerStore store_)
                                : this(gridLayout_, userName_, columnDescriptions_, columnCategories_, turboRiskGroups_)
    {
      _layoutExplorer.Store = store_;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="DlgGridLayoutEditor"/> class.
    /// </summary>
    /// <param name="gridLayout_">The grid layout_.</param>
    /// <param name="userName_">The user's kerberos ID.</param>
    /// <param name="columnDescriptions_">string => string mapping from the column ID (SourceField ID for RV) to description text (LongDescription for RV)</param>
    /// <param name="columnCategories_">string => MorganStanley.MSDesktop.Rambo.DataDictionary.Category mapping from the column ID (SourceFIeld ID for RV) to column category. e.g PositionDetails</param>
    public DlgGridLayoutEditor(GridLayout gridLayout_, string userName_, IDictionary columnDescriptions_,
                               IDictionary columnCategories_)
      : this(gridLayout_, userName_, columnDescriptions_)
    {
      _layoutExplorer.ColumnCategories = columnCategories_;
      _layoutExplorer.TurboRiskGroups = null;
    }

    public DlgGridLayoutEditor(GridLayout gridLayout_, string userName_, bool enableFormatChanges_, bool enabledSortChanges_) : this(gridLayout_, userName_, enableFormatChanges_, enabledSortChanges_, new Hashtable(), new Hashtable()) { }  

		/// <summary>
		/// Initialises an instance of the DlgGridLayoutEditor class.
		/// </summary>
		/// <param name="gridLayout_">The XML representation of the underlying grid/column 
		/// layout. This XML should initally be generated using the GridLayoutFactory and 
		/// associated classes.</param>		
		/// <param name="userName_">The current user.</param>
    /// <param name="columnDescriptions_">string => string mapping from the column ID (SourceField ID for RV) to description text (LongDescription for RV)</param>
    public DlgGridLayoutEditor(GridLayout gridLayout_, string userName_, IDictionary columnDescriptions_) : this(gridLayout_, userName_, true, true, columnDescriptions_, new Hashtable()) { }

    /// <summary>
    /// Initialises an instance of the DlgGridLayoutEditor class.
    /// </summary>
    /// <param name="gridLayout_"></param>
    /// <param name="userName_"></param>
    /// <param name="columnDescriptions_">string => string mapping from the column ID (SourceField ID for RV) to description text (LongDescription for RV)</param>
    /// <param name="store_">Class that will store layouts.</param>
    public DlgGridLayoutEditor(GridLayout gridLayout_, 
                                string userName_, 
                                IDictionary columnDescriptions_, 
                                LayoutExplorerStore store_) 
                                : this(gridLayout_, userName_, columnDescriptions_)
    {
      _layoutExplorer.Store = store_;
    }

    public DlgGridLayoutEditor(GridLayout gridLayout_, string userName_) : this(gridLayout_, userName_, true, true, new Hashtable(), new Hashtable()) { }

    public DlgGridLayoutEditor(GridLayout gridLayout_,
                                string userName_,
                                LayoutExplorerStore store_)
                                : this(gridLayout_, userName_)
    {
      _layoutExplorer.Store = store_;
    }

    #endregion

    #region Instance Properties

    /// <summary>
    /// If property is set to a IList when populating columns from Data service the verification will 
    /// be performed:
    ///   if a given columnkey is not in the list then the dlgbox will not be populated with that particular column
    /// If property is set to null no verification will be performed 
    /// </summary>
    public IList<string> VerifyColumnKeysList
    {
      set
      {
        _layoutExplorer.VerifyColumnKeysList = value;
      }
    }
    /// <summary>
    /// Indicate if we deleted any layout
    /// </summary>
    public bool IsLayoutDeleted
    {
      get { return _layoutExplorer.IsLayoutDeleted; }
    }


    public IColumnFilterProvider ColumnFilterProvider
    {
      get { return _layoutExplorer.ColumnFilterProvider; }
      set { _layoutExplorer.ColumnFilterProvider = value; }
    }

    /// <summary>
    /// Indicate if we need to display a filter by derived fields
    /// </summary>
    public bool EnableDerivedFieldFiltering
    {
      get { return _layoutExplorer.EnableDerivedFieldFiltering; }
      set { _layoutExplorer.EnableDerivedFieldFiltering = value; }
    }
    #endregion

    #region Instance Methods

    /// <summary>
    /// Returns the underlying grid layout object.
    /// </summary>
    /// <returns>A grid layout object.</returns>
    public GridLayout GetGridLayout()
    {
      return _layoutExplorer.GetGridLayout();
    }

    #endregion

    public event EventHandler DeleteGridLayout;

    public void InvokeDeleteGridLayout(EventArgs e)
    {
      EventHandler handler = DeleteGridLayout;
      if (handler != null) handler(this, e);
    }
  }
}
