﻿using System.ComponentModel;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  public interface ILayoutEditor : ISynchronizeInvoke
  {
    void DoSetDescription(string caption, string description, string category, string turboRiskId);
    void SetDataSourcesForHideVisible(AvailableItemsSelectorDataSource<IClassWrapper> dataSource_);
    void SetDataSourcesForSorting(AvailableItemsSelectorDataSource<ColumnWrapper> dataSource_);
    void SetDataSourceForFormattedGrid(BindingList<IClassWrapper> datasource_);
    void SetNumFrozenColumns(GridLayout gridLayout_);
    void HideAllGroupByButtons();
    void SetDataSourceForGridLevel(GridLayout layout_);
  }
}
