﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using Infragistics.Win.UltraWinGrid;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  public partial class LayoutEditorSortingControl : UserControl, ILayoutEditorSortingControl
  {
    private LayoutEditorSortingController _controller;
    #region Constructors

    public LayoutEditorSortingControl()
    {
      InitializeComponent();
      Controller = new LayoutEditorSortingController(this);
    }

    #endregion

    #region Instance Properties

    [Browsable(false)]
    public IAvailableItemSelector AvailItemsSelector
    {
      get { return _availItemsSelectorControl; }
    }

    public LayoutEditorSortingController Controller
    {
      get { return _controller; }
      set
      {
        _controller = value;
        _availItemsSelectorControl.Controller = value;
      }
    }

    [Browsable(false)]
    public UltraGrid LeftGrid
    {
      get { return _availItemsSelectorControl.LeftUltraGrid; }
    }

    [Browsable(false)]
    public UltraGrid RightGrid
    {
      get { return _availItemsSelectorControl.RightUltraGrid; }
    }

    #endregion

    #region Event Handling

    private void _chkAbsolute_Clicked(object sender_, EventArgs e_)
    {
      if (RightGrid.Selected.Rows.Count == 0) return;
      foreach (UltraGridRow row in RightGrid.Selected.Rows)
      {
        var clw = row.ListObject as ColumnWrapper;
        if (clw != null)
        {
          clw.ColumnLayout.SortAbsolute = _chkAbsolute.Checked;
        }
      }
      RightGrid.Refresh();
    }

    private void _rdoDescAsc_Clicked(object sender_, EventArgs e_)
    {
      if (RightGrid.Selected.Rows.Count == 0) return;
      foreach (UltraGridRow row in RightGrid.Selected.Rows)
      {
        var clw = row.ListObject as ColumnWrapper;

        if (clw != null)
        {
          clw.ColumnLayout.SortDirection = _rdoAsc.Checked
                                             ? ColumnLayout.SortDirections.Ascending
                                             :
                                               ColumnLayout.SortDirections.Descending;
        }
      }
      RightGrid.Refresh();
    }

    #endregion

    #region ILayoutEditorSortingControl Members

    public void EnableNavButtons(bool up_, bool down_, bool left_, bool right_)
    {
      _availItemsSelectorControl.EnableNavButtons(up_, down_, left_, right_);
    }

    public void ScrollRowIntoView(bool left_)
    {
      _availItemsSelectorControl.ScrollRowIntoView(left_);
    }

    public void UnSelectAllRows(bool left_)
    {
      _availItemsSelectorControl.UnSelectAllRows(left_);
    }

    public void SelectAllRows(bool left_)
    {
      _availItemsSelectorControl.SelectAllRows(left_);
    }

    public void SelectRows(bool left_, IList<IClassWrapper> wrappedColumns_)
    {
      _availItemsSelectorControl.SelectRows(left_, wrappedColumns_);
    }

    public void ReGroupGrid(bool left_)
    {
      _availItemsSelectorControl.ReGroupGrid(left_);
    }

    public void SetDataSource<T>(BindingList<T> leftDataSource_, BindingList<T> rightDataSource_)
    {
      _availItemsSelectorControl.SetDataSource(leftDataSource_, rightDataSource_);
      _availItemsSelectorControl.RightUltraGrid.DisplayLayout.Bands[0].Columns["SortOrder"].Header.ToolTipText = "Sort column by ascending or descending order";
      _availItemsSelectorControl.RightUltraGrid.DisplayLayout.Bands[0].Columns["SortAbs"].Header.ToolTipText = "Sort column by absolute values";
    }

    public void SetSelectedRows(bool left_)
    {
      _availItemsSelectorControl.SetSelectedRows(left_);
    }

    public T[] GetSelectedRows<T>(bool left_) where T : IClassWrapper
    {
      return _availItemsSelectorControl.GetSelectedRows<T>(left_);
    }

    public void Groupby(bool left_, string columnName_)
    {
      _availItemsSelectorControl.Groupby(left_, columnName_);
    }

    public void UnGroupby(bool left_, string columnName_)
    {
      _availItemsSelectorControl.UnGroupby(left_, columnName_);
    }

    public void SortNameColumn(bool left_)
    {
      _availItemsSelectorControl.SortNameColumn(left_);
    }

    public void SetCColLayoutProperties()
    {
      foreach (UltraGridRow row in LeftGrid.Selected.Rows)
      {
        var clw = row.ListObject as ColumnWrapper;
        if (clw != null)
        {
          clw.ColumnLayout.SortDirection = _rdoAsc.Checked
                                             ? ColumnLayout.SortDirections.Ascending
                                             :
                                               ColumnLayout.SortDirections.Descending;

          clw.ColumnLayout.SortAbsolute = _chkAbsolute.Checked;
        }
      }
    }

    public void UnSetColLayoutProperties()
    {
      //Set the SortDirctions to None when we drag a row to available ultraGrid
      foreach (UltraGridRow row in RightGrid.Selected.Rows)
      {
        var clw = row.ListObject as ColumnWrapper;
        if (clw != null)
        {
          clw.ColumnLayout.SortDirection = ColumnLayout.SortDirections.None;
        }
      }
    }
    #endregion
  }
}