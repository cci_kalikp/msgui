﻿using System;
using System.Collections;
using System.Drawing;
using System.Windows.Forms;
using MorganStanley.MSDesktop.Rambo.WinGridUtils.GridLayoutEditor.DataModel;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  public partial class DlgInsertColumns : Form, IInsertColumnsForm
  {
    #region Constructors

    public DlgInsertColumns()
    {
      InitializeComponent();
    }

    public DlgInsertColumns(GridLayout gridLayout_, Infragistics.Win.UltraWinGrid.ColumnHeader colHeader_,
      IDictionary columnDescriptions_ = null,
      IDictionary columnCategories_ = null, 
      IDictionary turboRiskGroups_ = null)
      : this()
    {
      Controller = new InsertColumnsController(this, _gridSelector.Controller)
                     {
                       GridLayout = gridLayout_,
                       InsertIndex = colHeader_.VisiblePosition
                     };
      
      _lblLayout.Text = gridLayout_.LayoutName;
      _lblOwnerName.Text = gridLayout_.UpdateID;
      _gridSelector.SetDescriptions += SetDescription;
      Controller.ColumnDescriptions = columnDescriptions_;
      Controller.ColumnCategories = columnCategories_;
      Controller.TurboRiskGroups = turboRiskGroups_;
      EnableDerivedFieldFiltering = false;
    }


    #endregion

    #region Instance Properties

    public InsertColumnsController Controller { get; set; }
    public bool InsertNothing
    {
      get
      {
        return _gridSelector.RightUltraGrid.Rows.Count == 0;
      }
    }

    #endregion

    #region Instance Methods

    public GridLayout GetCurrentGridLayout()
    {
      return Controller.GridLayout;
    }

    protected override void OnLoad(EventArgs e)
    {
      Controller.ClassifyColumns(Controller.GridLayout);
      base.OnLoad(e);
    }

    #endregion

    #region Event Handling

    /// <summary>
    /// Set the description for each columnLayout
    /// </summary>
    public void SetDescription(object sender, SetDescriptionEventArgs args_)
    {
      IClassWrapper columnWrapper = args_.Wrapper;
      if (columnWrapper == null || ((ColumnWrapper) columnWrapper).ColumnLayout == null)
      {
        rtfDescription.Text = string.Empty;
        return;
      }

      ColumnLayout columnLayout = ((ColumnWrapper) columnWrapper).ColumnLayout;
      Controller.SetDescription(columnLayout);
    }

    private void _btnApply_Click(object sender, EventArgs e)
    {
      DialogResult = DialogResult.OK;
      if (InsertNothing)
      {
        DialogResult = MessageBox.Show(
          "You have not added any columns to the Selected Columns list, nothing will be inserted" + Environment.NewLine +
          "Do you want to continue?",
          "Warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
      }
      if (DialogResult == DialogResult.OK)
        Controller.ApplyClick(_radioBtnBefore.Checked);
    }

    private void _btnHiddenGoupby_Click(object sender, EventArgs e)
    {
      _gridSelector.Groupby(true, "Category");
    }

    private void _btnHiddenUnGroupby_Click(object sender, EventArgs e)
    {
      _gridSelector.UnGroupby(true, "ColumnName");
    }

    private void _radioBtn_CheckedChanged(object sender, EventArgs e)
    {
      if (_radioBtnAfter.Checked)
      {
        _radioBtnBefore.Checked = false;
      }

      if (_radioBtnBefore.Checked)
      {
        _radioBtnAfter.Checked = false;
      }
    }

    #endregion

    #region IInsertColumnsForm Members

    public void DoSetDescription(string caption_, string description_, string category_, string turboRiskId_)
    {
      if (turboRiskId_ != null)
      {
        rtfDescription.Text = string.Format("{0}: {1}" + Environment.NewLine + "Category: {2}" + Environment.NewLine + "Turbo Risk Group: {3}",
          caption_, description_, category_, turboRiskId_);
      }else
      rtfDescription.Text = string.Format("{0}: {1}" + Environment.NewLine + "Category: {2}", caption_, description_,
                                          category_);

      //Delete the new line of caption.
      if (caption_.Contains("\n"))
      {
        caption_ = caption_.Replace('\n', ' ');
      }
      int categoryIndex = rtfDescription.Text.IndexOf("Category");
      int turboRiskIndex = rtfDescription.Text.IndexOf("Turbo Risk Group");

      //Set the color of text in decription.
      rtfDescription.Select(0, caption_.Length + 2);
      rtfDescription.SelectionColor = Color.Blue;
      rtfDescription.Select(caption_.Length + 2, description_.Length);
      rtfDescription.SelectionColor = Color.Black;
      rtfDescription.Select(categoryIndex, 9);
      rtfDescription.SelectionColor = Color.Blue;
      rtfDescription.Select(categoryIndex + 9, category_.Length + 1);
      rtfDescription.SelectionColor = Color.Black;

      if (turboRiskIndex != -1)
      {
        rtfDescription.Select(turboRiskIndex + 17, turboRiskId_.Length + 1);
        rtfDescription.SelectionColor = Color.Black;
      }

      rtfDescription.Select(0, 0);
    }

    public void HideGroupUnGroupButtons()
    {
      //We do not have any categories, hide the groupby and ungroupby button.
      _btnHiddenGoupby.Hide();
      _btnHiddenUnGroupby.Hide();
    }


    public IColumnFilterProvider ColumnFilterProvider
    {
      get { return _gridSelector.ColumnFilterProvider; }
      set { _gridSelector.ColumnFilterProvider = value;
        Controller.ColumnFilterProvider = value;

      }
    }
    public bool EnableDerivedFieldFiltering
    {
      get { return _showDerivedColumns.Visible; }
      set {
        _showDerivedColumns.Visible = value;
        Controller.ShowDerivedColumns = !value;
      }
    }

    public void SetDataSource(AvailableItemsSelectorDataSource<IClassWrapper> dataSource_)
    {
      _gridSelector.Controller.DataSource = dataSource_;
    }

    #endregion

    private void _showDerivedColumns_CheckedChanged(object sender, EventArgs e)
    {
      Controller.ShowDerivedColumns = _showDerivedColumns.Checked;
    }
  }
}