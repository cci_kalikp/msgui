using System;
using System.ComponentModel;
using System.Windows.Forms;
using Infragistics.Win.UltraWinTabControl;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  partial class LayoutEditor
  {

    #region Designer members
    private IContainer components;
    private ErrorProvider errorProvider1;
    private ToolTip _toolTip;
    #endregion

    #region Component Designer generated code
    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
      Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
      Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
      Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
      Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
      Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
      Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
      Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
      Infragistics.Win.UltraWinTabControl.UltraTab ultraTab3 = new Infragistics.Win.UltraWinTabControl.UltraTab();
      Infragistics.Win.UltraWinTabControl.UltraTab ultraTab4 = new Infragistics.Win.UltraWinTabControl.UltraTab();
      this._tabPage_ShowHide = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
      this._availItemsSelectorControl = new MorganStanley.MSDesktop.Rambo.WinGridUtils.AvailableItemsSelector();
      this.panel2 = new System.Windows.Forms.Panel();
      this._btnHiddenGoupby = new System.Windows.Forms.Button();
      this.numFrozenColumns = new System.Windows.Forms.NumericUpDown();
      this._btnHiddenUnGroupby = new System.Windows.Forms.Button();
      this.lblFrozenColumns = new System.Windows.Forms.Label();
      this._tabPage_Sorting = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
      this._layoutEditorSortingControl = new MorganStanley.MSDesktop.Rambo.WinGridUtils.LayoutEditorSortingControl();
      this.panel1 = new System.Windows.Forms.Panel();
      this._btnImportLiveSorting = new System.Windows.Forms.Button();
      this._btnAvailGroupby = new System.Windows.Forms.Button();
      this._btnAvailUnGroupby = new System.Windows.Forms.Button();
      this._tabPage_Format = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
      this.panel3 = new System.Windows.Forms.Panel();
      this.pgrdVisibleColumns = new System.Windows.Forms.PropertyGrid();
      this._formattedUltraGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
      this._tabPage_GridLevel = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
      this._gridLevelPropertyGrid = new System.Windows.Forms.PropertyGrid();
      this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
      this.rtfDescription = new System.Windows.Forms.RichTextBox();
      this.ultraTabControl = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
      this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
      this._toolTip = new System.Windows.Forms.ToolTip(this.components);
      this._descriptionGroupBox = new System.Windows.Forms.GroupBox();
      this.ultraButton1 = new Infragistics.Win.Misc.UltraButton();
      this.ultraButton2 = new Infragistics.Win.Misc.UltraButton();
      this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
      this.splitter1 = new System.Windows.Forms.Splitter();
      this._showDeivedColumnsChkBox = new System.Windows.Forms.CheckBox();
      this._tabPage_ShowHide.SuspendLayout();
      this.panel2.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.numFrozenColumns)).BeginInit();
      this._tabPage_Sorting.SuspendLayout();
      this.panel1.SuspendLayout();
      this._tabPage_Format.SuspendLayout();
      this.panel3.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this._formattedUltraGrid)).BeginInit();
      this._tabPage_GridLevel.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl)).BeginInit();
      this.ultraTabControl.SuspendLayout();
      this._descriptionGroupBox.SuspendLayout();
      this.SuspendLayout();
      // 
      // _tabPage_ShowHide
      // 
      this._tabPage_ShowHide.AllowDrop = true;
      this._tabPage_ShowHide.Controls.Add(this._availItemsSelectorControl);
      this._tabPage_ShowHide.Controls.Add(this.panel2);
      this._tabPage_ShowHide.Location = new System.Drawing.Point(1, 23);
      this._tabPage_ShowHide.Name = "_tabPage_ShowHide";
      this._tabPage_ShowHide.Size = new System.Drawing.Size(697, 517);
      // 
      // _availItemsSelectorControl
      // 
      this._availItemsSelectorControl.AllowDrop = true;
      this._availItemsSelectorControl.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._availItemsSelectorControl.Dock = System.Windows.Forms.DockStyle.Fill;
      this._availItemsSelectorControl.LeftGridHeaderCaption = "Hidden Columns";
      this._availItemsSelectorControl.LeftShowMultiColumns = false;
      this._availItemsSelectorControl.Location = new System.Drawing.Point(0, 30);
      this._availItemsSelectorControl.Name = "_availItemsSelectorControl";
      this._availItemsSelectorControl.RightGridHeaderCaption = "Visible Columns";
      this._availItemsSelectorControl.RightShowMultiColumns = false;
      this._availItemsSelectorControl.Size = new System.Drawing.Size(697, 487);
      this._availItemsSelectorControl.TabIndex = 0;
      // 
      // panel2
      // 
      this.panel2.Controls.Add(this._showDeivedColumnsChkBox);
      this.panel2.Controls.Add(this._btnHiddenGoupby);
      this.panel2.Controls.Add(this.numFrozenColumns);
      this.panel2.Controls.Add(this._btnHiddenUnGroupby);
      this.panel2.Controls.Add(this.lblFrozenColumns);
      this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel2.Location = new System.Drawing.Point(0, 0);
      this.panel2.Name = "panel2";
      this.panel2.Size = new System.Drawing.Size(697, 30);
      this.panel2.TabIndex = 49;
      // 
      // _btnHiddenGoupby
      // 
      this._btnHiddenGoupby.BackColor = System.Drawing.SystemColors.Control;
      this._btnHiddenGoupby.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
      this._btnHiddenGoupby.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightSteelBlue;
      this._btnHiddenGoupby.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSteelBlue;
      this._btnHiddenGoupby.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this._btnHiddenGoupby.Image = global::MorganStanley.MSDesktop.Rambo.WinGridUtils.Properties.Resources.groupby;
      this._btnHiddenGoupby.Location = new System.Drawing.Point(3, 3);
      this._btnHiddenGoupby.Name = "_btnHiddenGoupby";
      this._btnHiddenGoupby.Size = new System.Drawing.Size(22, 23);
      this._btnHiddenGoupby.TabIndex = 47;
      this._toolTip.SetToolTip(this._btnHiddenGoupby, "Group columns by category");
      this._btnHiddenGoupby.UseVisualStyleBackColor = false;
      this._btnHiddenGoupby.Click += new System.EventHandler(this.BtnHiddenGroupby_Click);
      // 
      // numFrozenColumns
      // 
      this.numFrozenColumns.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.numFrozenColumns.Location = new System.Drawing.Point(645, 4);
      this.numFrozenColumns.Name = "numFrozenColumns";
      this.numFrozenColumns.Size = new System.Drawing.Size(48, 20);
      this.numFrozenColumns.TabIndex = 39;
      this.numFrozenColumns.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      // 
      // _btnHiddenUnGroupby
      // 
      this._btnHiddenUnGroupby.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
      this._btnHiddenUnGroupby.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Control;
      this._btnHiddenUnGroupby.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.Control;
      this._btnHiddenUnGroupby.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this._btnHiddenUnGroupby.Image = global::MorganStanley.MSDesktop.Rambo.WinGridUtils.Properties.Resources.sort;
      this._btnHiddenUnGroupby.Location = new System.Drawing.Point(31, 3);
      this._btnHiddenUnGroupby.Name = "_btnHiddenUnGroupby";
      this._btnHiddenUnGroupby.Size = new System.Drawing.Size(22, 23);
      this._btnHiddenUnGroupby.TabIndex = 48;
      this._toolTip.SetToolTip(this._btnHiddenUnGroupby, "Alphabetical");
      this._btnHiddenUnGroupby.UseVisualStyleBackColor = true;
      this._btnHiddenUnGroupby.Click += new System.EventHandler(this.BtnHiddenUnGroupby_Click);
      // 
      // lblFrozenColumns
      // 
      this.lblFrozenColumns.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.lblFrozenColumns.BackColor = System.Drawing.Color.Transparent;
      this.lblFrozenColumns.Location = new System.Drawing.Point(541, 6);
      this.lblFrozenColumns.Name = "lblFrozenColumns";
      this.lblFrozenColumns.Size = new System.Drawing.Size(96, 16);
      this.lblFrozenColumns.TabIndex = 40;
      this.lblFrozenColumns.Text = "Frozen Columns:";
      this.lblFrozenColumns.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // _tabPage_Sorting
      // 
      this._tabPage_Sorting.Controls.Add(this._layoutEditorSortingControl);
      this._tabPage_Sorting.Controls.Add(this.panel1);
      this._tabPage_Sorting.Location = new System.Drawing.Point(-10000, -10000);
      this._tabPage_Sorting.Name = "_tabPage_Sorting";
      this._tabPage_Sorting.Size = new System.Drawing.Size(697, 517);
      // 
      // _layoutEditorSortingControl
      // 
      this._layoutEditorSortingControl.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this._layoutEditorSortingControl.Dock = System.Windows.Forms.DockStyle.Fill;
      this._layoutEditorSortingControl.Location = new System.Drawing.Point(0, 30);
      this._layoutEditorSortingControl.Name = "_layoutEditorSortingControl";
      this._layoutEditorSortingControl.Size = new System.Drawing.Size(697, 487);
      this._layoutEditorSortingControl.TabIndex = 0;
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this._btnImportLiveSorting);
      this.panel1.Controls.Add(this._btnAvailGroupby);
      this.panel1.Controls.Add(this._btnAvailUnGroupby);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel1.Location = new System.Drawing.Point(0, 0);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(697, 30);
      this.panel1.TabIndex = 50;
      // 
      // _btnImportLiveSorting
      // 
      this._btnImportLiveSorting.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this._btnImportLiveSorting.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
      this._btnImportLiveSorting.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this._btnImportLiveSorting.Location = new System.Drawing.Point(619, 3);
      this._btnImportLiveSorting.Name = "_btnImportLiveSorting";
      this._btnImportLiveSorting.Size = new System.Drawing.Size(75, 23);
      this._btnImportLiveSorting.TabIndex = 2;
      this._btnImportLiveSorting.Text = "Import Live";
      this._toolTip.SetToolTip(this._btnImportLiveSorting, "Update the Sort settings to the currently active sort settings");
      this._btnImportLiveSorting.UseVisualStyleBackColor = true;
      this._btnImportLiveSorting.Click += new System.EventHandler(this.BtnImportLiveSorting_Click);
      // 
      // _btnAvailGroupby
      // 
      this._btnAvailGroupby.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
      this._btnAvailGroupby.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this._btnAvailGroupby.Image = global::MorganStanley.MSDesktop.Rambo.WinGridUtils.Properties.Resources.groupby;
      this._btnAvailGroupby.Location = new System.Drawing.Point(3, 3);
      this._btnAvailGroupby.Name = "_btnAvailGroupby";
      this._btnAvailGroupby.Size = new System.Drawing.Size(22, 23);
      this._btnAvailGroupby.TabIndex = 48;
      this._toolTip.SetToolTip(this._btnAvailGroupby, "Group columns by category");
      this._btnAvailGroupby.UseVisualStyleBackColor = true;
      this._btnAvailGroupby.Click += new System.EventHandler(this.BtnAvailGroupby_Click);
      // 
      // _btnAvailUnGroupby
      // 
      this._btnAvailUnGroupby.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
      this._btnAvailUnGroupby.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this._btnAvailUnGroupby.Image = global::MorganStanley.MSDesktop.Rambo.WinGridUtils.Properties.Resources.sort;
      this._btnAvailUnGroupby.Location = new System.Drawing.Point(31, 3);
      this._btnAvailUnGroupby.Name = "_btnAvailUnGroupby";
      this._btnAvailUnGroupby.Size = new System.Drawing.Size(22, 23);
      this._btnAvailUnGroupby.TabIndex = 49;
      this._toolTip.SetToolTip(this._btnAvailUnGroupby, "Alphabetical");
      this._btnAvailUnGroupby.UseVisualStyleBackColor = true;
      this._btnAvailUnGroupby.Click += new System.EventHandler(this.BtnAvailUnGroupby_Click);
      // 
      // _tabPage_Format
      // 
      this._tabPage_Format.Controls.Add(this.panel3);
      this._tabPage_Format.Location = new System.Drawing.Point(-10000, -10000);
      this._tabPage_Format.Name = "_tabPage_Format";
      this._tabPage_Format.Size = new System.Drawing.Size(697, 517);
      // 
      // panel3
      // 
      this.panel3.BackColor = System.Drawing.Color.Transparent;
      this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.panel3.Controls.Add(this.pgrdVisibleColumns);
      this.panel3.Controls.Add(this._formattedUltraGrid);
      this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel3.Location = new System.Drawing.Point(0, 0);
      this.panel3.Name = "panel3";
      this.panel3.Size = new System.Drawing.Size(697, 517);
      this.panel3.TabIndex = 46;
      // 
      // pgrdVisibleColumns
      // 
      this.pgrdVisibleColumns.BackColor = System.Drawing.SystemColors.Control;
      this.pgrdVisibleColumns.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pgrdVisibleColumns.HelpVisible = false;
      this.pgrdVisibleColumns.LineColor = System.Drawing.SystemColors.ScrollBar;
      this.pgrdVisibleColumns.Location = new System.Drawing.Point(186, 0);
      this.pgrdVisibleColumns.Name = "pgrdVisibleColumns";
      this.pgrdVisibleColumns.Size = new System.Drawing.Size(509, 515);
      this.pgrdVisibleColumns.TabIndex = 37;
      this.pgrdVisibleColumns.ToolbarVisible = false;
      // 
      // _formattedUltraGrid
      // 
      this._formattedUltraGrid.AllowDrop = true;
      this._formattedUltraGrid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
      appearance1.BorderColor = System.Drawing.Color.White;
      this._formattedUltraGrid.DisplayLayout.EmptyRowSettings.CellAppearance = appearance1;
      appearance2.BorderColor = System.Drawing.Color.White;
      this._formattedUltraGrid.DisplayLayout.EmptyRowSettings.RowAppearance = appearance2;
      this._formattedUltraGrid.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
      this._formattedUltraGrid.DisplayLayout.EmptyRowSettings.Style = Infragistics.Win.UltraWinGrid.EmptyRowStyle.AlignWithDataRows;
      this._formattedUltraGrid.DisplayLayout.MaxColScrollRegions = 1;
      this._formattedUltraGrid.DisplayLayout.MaxRowScrollRegions = 1;
      this._formattedUltraGrid.DisplayLayout.Override.AllowColSizing = Infragistics.Win.UltraWinGrid.AllowColSizing.None;
      appearance3.BorderColor = System.Drawing.Color.Gainsboro;
      this._formattedUltraGrid.DisplayLayout.Override.CellAppearance = appearance3;
      this._formattedUltraGrid.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Cell;
      this._formattedUltraGrid.DisplayLayout.Override.FilterOperatorDefaultValue = Infragistics.Win.UltraWinGrid.FilterOperatorDefaultValue.Contains;
      appearance4.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
      appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
      appearance4.BorderColor = System.Drawing.Color.Gray;
      this._formattedUltraGrid.DisplayLayout.Override.FilterRowAppearance = appearance4;
      this._formattedUltraGrid.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
      this._formattedUltraGrid.DisplayLayout.Override.FixedRowStyle = Infragistics.Win.UltraWinGrid.FixedRowStyle.Top;
      appearance5.BorderColor = System.Drawing.Color.Gainsboro;
      this._formattedUltraGrid.DisplayLayout.Override.RowAppearance = appearance5;
      this._formattedUltraGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
      this._formattedUltraGrid.DisplayLayout.Override.RowSizing = Infragistics.Win.UltraWinGrid.RowSizing.Fixed;
      this._formattedUltraGrid.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.ExtendedAutoDrag;
      this._formattedUltraGrid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.ExtendedAutoDrag;
      this._formattedUltraGrid.DisplayLayout.UseFixedHeaders = true;
      this._formattedUltraGrid.Dock = System.Windows.Forms.DockStyle.Left;
      this._formattedUltraGrid.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this._formattedUltraGrid.Location = new System.Drawing.Point(0, 0);
      this._formattedUltraGrid.Name = "_formattedUltraGrid";
      this._formattedUltraGrid.Size = new System.Drawing.Size(186, 515);
      this._formattedUltraGrid.TabIndex = 41;
      this._formattedUltraGrid.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.FormattedutraGrid_InitializeLayout);
      this._formattedUltraGrid.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler(this.FormatedultraGrid_AfterSelectChange);
      // 
      // _tabPage_GridLevel
      // 
      this._tabPage_GridLevel.Controls.Add(this._gridLevelPropertyGrid);
      this._tabPage_GridLevel.Location = new System.Drawing.Point(-10000, -10000);
      this._tabPage_GridLevel.Name = "_tabPage_GridLevel";
      this._tabPage_GridLevel.Size = new System.Drawing.Size(697, 517);
      // 
      // _gridLevelPropertyGrid
      // 
      this._gridLevelPropertyGrid.BackColor = System.Drawing.SystemColors.Control;
      this._gridLevelPropertyGrid.Dock = System.Windows.Forms.DockStyle.Fill;
      this._gridLevelPropertyGrid.HelpVisible = false;
      this._gridLevelPropertyGrid.LineColor = System.Drawing.SystemColors.ScrollBar;
      this._gridLevelPropertyGrid.Location = new System.Drawing.Point(0, 0);
      this._gridLevelPropertyGrid.Name = "_gridLevelPropertyGrid";
      this._gridLevelPropertyGrid.Size = new System.Drawing.Size(697, 517);
      this._gridLevelPropertyGrid.TabIndex = 38;
      this._gridLevelPropertyGrid.ToolbarVisible = false;
      // 
      // errorProvider1
      // 
      this.errorProvider1.ContainerControl = this;
      this.errorProvider1.DataMember = "";
      // 
      // rtfDescription
      // 
      this.rtfDescription.BackColor = System.Drawing.SystemColors.Window;
      this.rtfDescription.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.rtfDescription.Cursor = System.Windows.Forms.Cursors.Default;
      this.rtfDescription.Dock = System.Windows.Forms.DockStyle.Fill;
      this.rtfDescription.ForeColor = System.Drawing.Color.Black;
      this.rtfDescription.Location = new System.Drawing.Point(3, 16);
      this.rtfDescription.Name = "rtfDescription";
      this.rtfDescription.ReadOnly = true;
      this.rtfDescription.Size = new System.Drawing.Size(695, 65);
      this.rtfDescription.TabIndex = 0;
      this.rtfDescription.Text = "";
      // 
      // ultraTabControl
      // 
      this.ultraTabControl.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
      this.ultraTabControl.AllowDrop = true;
      this.ultraTabControl.Controls.Add(this.ultraTabSharedControlsPage1);
      this.ultraTabControl.Controls.Add(this._tabPage_ShowHide);
      this.ultraTabControl.Controls.Add(this._tabPage_Sorting);
      this.ultraTabControl.Controls.Add(this._tabPage_Format);
      this.ultraTabControl.Controls.Add(this._tabPage_GridLevel);
      this.ultraTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
      this.ultraTabControl.Location = new System.Drawing.Point(0, 0);
      this.ultraTabControl.Name = "ultraTabControl";
      appearance6.BackColor = System.Drawing.SystemColors.Control;
      appearance6.BackColor2 = System.Drawing.SystemColors.Control;
      this.ultraTabControl.SelectedTabAppearance = appearance6;
      this.ultraTabControl.SharedControlsPage = this.ultraTabSharedControlsPage1;
      this.ultraTabControl.ShowTabListButton = Infragistics.Win.DefaultableBoolean.False;
      this.ultraTabControl.Size = new System.Drawing.Size(701, 543);
      this.ultraTabControl.TabIndex = 1;
      ultraTab1.AllowMoving = Infragistics.Win.DefaultableBoolean.False;
      ultraTab1.TabPage = this._tabPage_ShowHide;
      ultraTab1.Text = "Show/Hide";
      ultraTab1.ToolTipText = "Set column visibility.";
      ultraTab2.AllowMoving = Infragistics.Win.DefaultableBoolean.False;
      ultraTab2.TabPage = this._tabPage_Sorting;
      ultraTab2.Text = "Sorting";
      ultraTab2.ToolTipText = "Set the sort order of grid columns.";
      ultraTab3.AllowMoving = Infragistics.Win.DefaultableBoolean.False;
      ultraTab3.TabPage = this._tabPage_Format;
      ultraTab3.Text = "Format";
      ultraTab3.ToolTipText = "Set column formatting.";
      ultraTab4.TabPage = this._tabPage_GridLevel;
      ultraTab4.Text = "Grid Level";
      this.ultraTabControl.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2,
            ultraTab3,
            ultraTab4});
      this.ultraTabControl.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.Standard;
      this.ultraTabControl.SelectedTabChanged += new Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventHandler(this.SelectedTabChanged);
      // 
      // ultraTabSharedControlsPage1
      // 
      this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
      this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
      this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(697, 517);
      // 
      // _toolTip
      // 
      this._toolTip.ToolTipTitle = "Sort the column as ascending";
      // 
      // _descriptionGroupBox
      // 
      this._descriptionGroupBox.Controls.Add(this.rtfDescription);
      this._descriptionGroupBox.Dock = System.Windows.Forms.DockStyle.Bottom;
      this._descriptionGroupBox.Location = new System.Drawing.Point(0, 548);
      this._descriptionGroupBox.Name = "_descriptionGroupBox";
      this._descriptionGroupBox.Size = new System.Drawing.Size(701, 84);
      this._descriptionGroupBox.TabIndex = 1;
      this._descriptionGroupBox.TabStop = false;
      this._descriptionGroupBox.Text = "Description";
      // 
      // ultraButton1
      // 
      this.ultraButton1.Location = new System.Drawing.Point(0, 0);
      this.ultraButton1.Name = "ultraButton1";
      this.ultraButton1.Size = new System.Drawing.Size(75, 23);
      this.ultraButton1.TabIndex = 0;
      // 
      // ultraButton2
      // 
      this.ultraButton2.Location = new System.Drawing.Point(0, 0);
      this.ultraButton2.Name = "ultraButton2";
      this.ultraButton2.Size = new System.Drawing.Size(75, 23);
      this.ultraButton2.TabIndex = 0;
      // 
      // ultraLabel1
      // 
      this.ultraLabel1.Location = new System.Drawing.Point(0, 0);
      this.ultraLabel1.Name = "ultraLabel1";
      this.ultraLabel1.Size = new System.Drawing.Size(100, 23);
      this.ultraLabel1.TabIndex = 0;
      // 
      // splitter1
      // 
      this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.splitter1.Location = new System.Drawing.Point(0, 543);
      this.splitter1.Name = "splitter1";
      this.splitter1.Size = new System.Drawing.Size(701, 5);
      this.splitter1.TabIndex = 2;
      this.splitter1.TabStop = false;
      // 
      // _showDeivedColumnsChkBox
      // 
      this._showDeivedColumnsChkBox.AutoSize = true;
      this._showDeivedColumnsChkBox.Location = new System.Drawing.Point(59, 7);
      this._showDeivedColumnsChkBox.Name = "_showDeivedColumnsChkBox";
      this._showDeivedColumnsChkBox.Size = new System.Drawing.Size(123, 17);
      this._showDeivedColumnsChkBox.TabIndex = 49;
      this._showDeivedColumnsChkBox.Text = "Show Derived Fields";
      this._showDeivedColumnsChkBox.UseVisualStyleBackColor = true;
      this._showDeivedColumnsChkBox.CheckedChanged += new System.EventHandler(this.ShowDerivedColumnsChanged);
      // 
      // LayoutEditor
      // 
      this.AllowDrop = true;
      this.Controls.Add(this.ultraTabControl);
      this.Controls.Add(this.splitter1);
      this.Controls.Add(this._descriptionGroupBox);
      this.Name = "LayoutEditor";
      this.Size = new System.Drawing.Size(701, 632);
      this._tabPage_ShowHide.ResumeLayout(false);
      this.panel2.ResumeLayout(false);
      this.panel2.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.numFrozenColumns)).EndInit();
      this._tabPage_Sorting.ResumeLayout(false);
      this.panel1.ResumeLayout(false);
      this._tabPage_Format.ResumeLayout(false);
      this.panel3.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this._formattedUltraGrid)).EndInit();
      this._tabPage_GridLevel.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.ultraTabControl)).EndInit();
      this.ultraTabControl.ResumeLayout(false);
      this._descriptionGroupBox.ResumeLayout(false);
      this.ResumeLayout(false);

    }
    #endregion

    private RichTextBox rtfDescription;
    private UltraTabControl ultraTabControl;
    private UltraTabSharedControlsPage ultraTabSharedControlsPage1;
    private UltraTabPageControl _tabPage_ShowHide;
    private Button _btnHiddenUnGroupby;
    private AvailableItemsSelector _availItemsSelectorControl;
    private NumericUpDown numFrozenColumns;
    private Button _btnHiddenGoupby;
    private Label lblFrozenColumns;
    private UltraTabPageControl _tabPage_Sorting;
    private Button _btnAvailUnGroupby;
    private Button _btnAvailGroupby;
    private LayoutEditorSortingControl _layoutEditorSortingControl;
    private UltraTabPageControl _tabPage_Format;
    private Panel panel3;
    private Infragistics.Win.UltraWinGrid.UltraGrid _formattedUltraGrid;
    private PropertyGrid pgrdVisibleColumns;
    private UltraTabPageControl _tabPage_GridLevel;
    private PropertyGrid _gridLevelPropertyGrid;
    private Button _btnImportLiveSorting;
    private GroupBox _descriptionGroupBox;
    private Infragistics.Win.Misc.UltraButton ultraButton1;
    private Infragistics.Win.Misc.UltraButton ultraButton2;
    private Infragistics.Win.Misc.UltraLabel ultraLabel1;
    private Panel panel1;
    private Panel panel2;
    private Splitter splitter1;
    private CheckBox _showDeivedColumnsChkBox;
  }
}