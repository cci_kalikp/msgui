﻿namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  partial class DlgGridLayoutEditor
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this._layoutExplorer = new MorganStanley.MSDesktop.Rambo.WinGridUtils.LayoutExplorer();
      this.SuspendLayout();
      // 
      // _layoutExplorer
      // 
      this._layoutExplorer.AutoSize = true;
      this._layoutExplorer.ColumnCategories = null;
      this._layoutExplorer.ColumnDescriptions = null;
      this._layoutExplorer.Dock = System.Windows.Forms.DockStyle.Fill;
      this._layoutExplorer.EnableFormatChanges = true;
      this._layoutExplorer.EnableSortChanges = true;
      this._layoutExplorer.InitialGridLayout = null;
      this._layoutExplorer.Location = new System.Drawing.Point(0, 0);
      this._layoutExplorer.Name = "_layoutExplorer";
      this._layoutExplorer.Padding = new System.Windows.Forms.Padding(5, 3, 5, 3);
      this._layoutExplorer.Size = new System.Drawing.Size(515, 663);
      this._layoutExplorer.StopClose = false;
      this._layoutExplorer.TabIndex = 0;
      this._layoutExplorer.UserName = null;
      // 
      // DlgGridLayoutEditor
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(515, 663);
      this.Controls.Add(this._layoutExplorer);
      this.Name = "DlgGridLayoutEditor";
      this.Text = "DlgGridLayoutEditor1";
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private LayoutExplorer _layoutExplorer;
  }
}