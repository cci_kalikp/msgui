using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using JetBrains.Annotations;
using MorganStanley.MSDesktop.Rambo.Desktop.Utils;
using MorganStanley.MSDesktop.Rambo.WinGridUtils.GridLayoutEditor.DataModel;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  /// <summary>
  /// The CntrlFormatDetails User Controls allows customization of a selected grid layout.
  /// </summary>
  public partial class LayoutExplorer : UserControl, ILayoutExplorer
  {
    #region Readonly & Static Fields

    private readonly IDictionary<RadioButton, RadioButton> _filterRadioButtonsMap;
    private static readonly ILogger _log = new Logger(typeof(LayoutExplorer));
    private IDictionary<string, string> _currentWriteGroups;

    #endregion

    #region Fields

    private int _enableCounter;
    private bool _isBindingList;
    private GridLayout m_previouslySelectedLayout;

    #endregion

    #region Constructors
    public LayoutExplorer()
    {
      // This call is required by the Windows.Forms Form Designer.
      InitializeComponent();
      cmbLayouts.SortStyle = Infragistics.Win.ValueListSortStyle.Ascending;
      _filterRadioButtonsMap = new Dictionary<RadioButton, RadioButton>
                                 {
                                   {_ownedByMeRadioButton, _ownedByMeRadioButton},
                                   {_costCenterLayoutRadioButton, _costCenterLayoutRadioButton},
                                   {_allLayoutsRadioButton, _allLayoutsRadioButton},
                                   {_filterMailGroupRadioBtn, _filterMailGroupRadioBtn}
                                 };

      Controller = new LayoutExplorerController(this);
      _layoutEditor.ImportLiveSortingChanged += LayoutEditor_ImportLiveSortingChanged;
    }

    #endregion

    #region Instance Properties

    internal LayoutExplorerStore Store
    {
      set
      {
        //Giving controller a reference to view as per the constructor's code
        Controller.LayoutExplorer = this;
        Controller.Store = value;
      }
    }

    public IDictionary ColumnCategories
    {
      get { return _layoutEditor.ColumnCategories; }
      set { _layoutEditor.ColumnCategories = value; }
    }

    public IDictionary ColumnDescriptions
    {
      get { return _layoutEditor.ColumnDescriptions; }
      set { _layoutEditor.ColumnDescriptions = value; }
    }

    public IDictionary TurboRiskGroups
    {
      get { return _layoutEditor.TurboRiskGroups; }
      set { _layoutEditor.TurboRiskGroups = value; }
    }

    public LayoutExplorerController Controller { get; set; }

    public bool EnableFormatChanges
    {
      get { return _layoutEditor.EnableFormatChanges; }
      set { _layoutEditor.EnableFormatChanges = value; }
    }

    public bool EnableSortChanges
    {
      get { return _layoutEditor.EnableSortChanges; }
      set { _layoutEditor.EnableSortChanges = value; }
    }

    public GridLayout InitialGridLayout
    {
      get { return Controller.InitialGridLayout; }
      set
      {
        if (value != null)
        {
          Controller.InitialGridLayout = value;
        }
      }
    }

    public GridLayoutListItem SelectedListItem
    {
      get
      {
        if (cmbLayouts.DataSource == null || cmbLayouts.SelectedItem == null)
        {
          return null;
        }
        return cmbLayouts.SelectedItem.ListObject as GridLayoutListItem;
      }
    }

    public bool StopClose
    {
      get { return Controller.StopClose; }
      set { Controller.StopClose = value; }
    }

    public string UserName
    {
      get { return Controller.UserName; }
      set
      {
        Controller.UserName = value;
        _layoutEditor.UserName = value;
      }
    }

    public IList<string> VerifyColumnKeysList
    {
      set { _layoutEditor.VerifyColumnKeysList = value; }
    }

    #endregion

    #region Instance Methods

    /// <summary>
    /// Returns true if modifications have been made to existing layout.
    /// </summary>
    /// <returns></returns>
    public bool GridHasChanges()
    {
      GridLayoutListItem listItem = SelectedListItem;
      return listItem != null && listItem.HasChanges;
    }

    protected override void OnLoad(EventArgs e)
    {
      Controller.InitController();
      base.OnLoad(e);
    }

    /// <summary>
    /// Changes enabled flag of the btnSave to appropriate status.
    /// </summary>
    public void ChangeBtnSaveDeleteShareStatus()
    {
      bool hasWritePermission = _currentWriteGroups == null ? false : Controller.HasEntitlement(_currentWriteGroups, Environment.UserName);
      btnSave.Enabled = hasWritePermission;

      bool isOwner = txtOwnerName.Text == Environment.UserName || UserVerification.IsThisUserInGroup("tradeplant-ww-risk");
      _btnDelete.Enabled = btnShare.Enabled = isOwner;
    }

    private void EnableUIControls(bool _enable)
    {
      _filterPanel.Enabled = _enable;
      cmbLayouts.Enabled = _enable;
      //Buttons
      _saveAsUltraButton.Enabled = _enable;
      btnApply.Enabled = _enable;
      if (_enable)
      {
        ChangeBtnSaveDeleteShareStatus();
      }
      else
      {
        btnShare.Enabled = false;
        _btnDelete.Enabled = false;
        btnSave.Enabled = false;
      }
      _refreshButton.Enabled = _enable;
    }

    #endregion

    #region Event Handling

    private void _refreshButton_Click(object sender, EventArgs e)
    {
      foreach (RadioButton radio in _filterRadioButtonsMap.Values)
      {
        radio.Checked = false;
      }
      Controller.RefreshButtonClick();
    }

    private void ApplyMailGroupFilter_Click(object sender, EventArgs e)
    {
      if (!string.IsNullOrEmpty(Controller.MailGroupFilter.SearchCriteria))
      {
        Controller.PerformFilteringByMailGroup();
      }
    }

    /// <summary>
    /// Apply button event handler.
    /// </summary>
    private void BtnApply_Click(object sender, EventArgs e)
    {
      try
      {
        Controller.BtnApplyClick();
      }
      catch (Exception)
      {
        MessageBox.Show(this,
                        "Unable to apply Columns changes currently.",
                        "Columns Apply Failed",
                        MessageBoxButtons.OK, MessageBoxIcon.Error,
                        MessageBoxDefaultButton.Button1);
        if (ParentForm != null) ParentForm.DialogResult = DialogResult.None;
      }
    }

    private void BtnCancel_Click(object sender, EventArgs e)
    {
      Controller.BtnCancelClick();
    }

    /// <summary>
    /// Apply button event handler.
    /// </summary>
    private void btnSave_Click(object sender, EventArgs e)
    {
      Controller.BtnSaveClick();
    }

    private void CmbLayouts_GotFocus(object sender, EventArgs e)
    {
      btnSave.Enabled = false;
    }

    private void CmbLayouts_LostFocus(object sender, EventArgs e)
    {
      ChangeBtnSaveDeleteShareStatus();
    }

    /// <summary>
    /// Gridlayout collection combo-box index changed handler.
    /// </summary>
    private void CmbLayouts_SelectionChanged(object sender, EventArgs e)
    {
      GridLayoutListItem listItem = SelectedListItem;
      if (!_isBindingList)
      {
        // set text to empty before load layout.
        txtSharedBy.Text = string.Empty;
        txtLastUpdateId.Text = string.Empty;
        txtOwnerName.Text = string.Empty;
        Controller.LoadSelectedLayout(listItem);
      }

      if (cmbLayouts.SelectedItem != null)
      {
        m_previouslySelectedLayout = SelectedLayout;
      }

      if (cmbLayouts.Focused)
      {
        btnApply.Focus();
      }
    }

    private void FilterRadioButtons_StatusChanged(object sender, EventArgs e)
    {
      var currentlyClickedRadioButton = (RadioButton) sender;
      if (currentlyClickedRadioButton.Checked)
      {
        //unchecking all combo boxes
        foreach (RadioButton rdBtn in _filterRadioButtonsMap.Keys)
        {
          if (!rdBtn.Equals(sender))
          {
            rdBtn.Checked = false;
          }
        } //foreach
        string radioButton = currentlyClickedRadioButton.Text;
        //Performing a filtering action depending on what 
        //filtering criteria was selected
        Controller.FilterRadioButtonsStatusChanged(radioButton);
      }
    }

    private void MailGroupTextBox_KeyDown(object sender, KeyEventArgs e)
    {
      var senderBox = (TextBox) sender;
      if (e.KeyCode == Keys.Enter)
      {
        Controller.MailGroupFilter.SearchCriteria = senderBox.Text;
        Controller.PerformFilteringByMailGroup();
      }
    }

    private void SaveAsUltraButton_Click(object sender, EventArgs e)
    {
      using (var saveAsDialog = new DlgSaveAs())
      {
        saveAsDialog.InitialText = cmbLayouts.Text;
        if (saveAsDialog.ShowDialog() == DialogResult.OK)
        {
          if (string.IsNullOrWhiteSpace(saveAsDialog.LayoutName) || saveAsDialog.LayoutName.ToLower() == "all")
          {
            ShowWarningMessage(string.Format("The name '{0}' cannot be used to save a RV Columns setting.\nPlease try another name.", saveAsDialog.LayoutName), "Unable to save Columns setting");
            _log.Warning("SaveAsUltraButton_Click", "Unable to save layout. The name '" + saveAsDialog.LayoutName + "' cannot be used to save a RV layout.");
          }
          else
          {
            Controller.SaveAsUltraButtonClick(saveAsDialog.LayoutName);
          }                
        } //(saveAsDialog.ShowDialog() == DialogResult.OK)
      } // using (DlgSaveAs saveAsDialog = new DlgSaveAs())
    }

    private void LayoutEditor_ImportLiveSortingChanged(object sender, EventArgs e)
    {            
      _layoutEditor.LayoutEditorSortingController.ImportLiveSorting(Controller.InitialSortedColumns);
    }
    #endregion

    #region ILayoutExplorer Members

    public GridLayout SelectedLayout
    {
      get
      {
        GridLayoutListItem item = SelectedListItem;
        if (item == null)
        {
          return m_previouslySelectedLayout;
        }
        return item.GridLayout;
      }
    }

    public BindingSource DataSource
    {
      get {return (BindingSource) cmbLayouts.DataSource;}
      set { cmbLayouts.DataSource = value; }
    }

    public void SetDialogResult(bool ok_)
    {
      if (ParentForm != null)
        ParentForm.DialogResult = ok_ ? DialogResult.OK : DialogResult.None;
    }

    public void BindLayoutCombo([CanBeNull] ICollection<GridLayoutListItem> list_)
    {
      _isBindingList = true;
      GridLayoutListItem selecteditem = SelectedListItem;
      bool update = selecteditem != null;

      // reset the position on the binding source so that the same layout remains
      // as the active value
      int selectedIndex = 0;
      if (update)
      {
        selectedIndex = cmbLayouts.FindStringExact(selecteditem.DisplayName);
      }
      else if (Controller.InitialGridLayout != null)
      {
        selectedIndex = cmbLayouts.FindString(Controller.InitialGridLayout.LayoutName);
      }

      _isBindingList = false;

      if (list_ != null && list_.Count > 0) // checking if list contains any layouts
      {
        //cmbLayouts.SelectionChangeCommitted -= CmbLayouts_SelectionChanged;
        //try
        //{
        if (selectedIndex < 0)
        {
          selectedIndex = 0;
        }

        cmbLayouts.SelectedIndex = selectedIndex;
        CmbLayouts_SelectionChanged(cmbLayouts, EventArgs.Empty);
        //}
        //finally
        //{
        //  cmbLayouts.SelectionChangeCommitted += CmbLayouts_SelectionChanged;
        //}
      }

      _isBindingList = false;
    }

    public void SetAllLayoutRadioButton(bool checked_)
    {
      _allLayoutsRadioButton.Checked = checked_;
    }

    public string GetMailGroupText()
    {
      return _mailGroupTextBox.Text;
    }


    public void SetComboSelectedIndex(string startString_)
    {
      cmbLayouts.SelectedIndex = cmbLayouts.FindString(startString_);
    }

    public void ShowWarningMessage(string message_, string title_)
    {
      MessageBox.Show(message_, title_, MessageBoxButtons.OK, MessageBoxIcon.Warning);
    }

    public void SetCurrentWriteGroup(IDictionary<string, string> groups_, GridLayout layout_)
    {
      _currentWriteGroups = groups_;
      string shared = string.Empty;
      if (_currentWriteGroups != null)
      {
        string[] groups = new string[_currentWriteGroups.Count];
        _currentWriteGroups.Keys.CopyTo(groups, 0);
        shared = string.Join(",", groups);
      }
      txtSharedBy.Text = shared;
      if(layout_ != null)
      {
        txtLastUpdateId.Text = layout_.UpdateID;
        txtOwnerName.Text = layout_.Owner;
      }
      ChangeBtnSaveDeleteShareStatus();
    }

    /// <summary>
    /// Indicate if any layout is deleted.
    /// </summary>
    public bool IsLayoutDeleted { get; set; }

    public bool EnableDerivedFieldFiltering
    {
      get { return _layoutEditor.EnableDerivedFieldFiltering; }
      set { _layoutEditor.EnableDerivedFieldFiltering = value; }
    }


    public IColumnFilterProvider  ColumnFilterProvider
    {
      get { return _layoutEditor.ColumnFilterProvider; }
      set { _layoutEditor.ColumnFilterProvider = value; }
    }

    public void EnableControls(bool enable_)
    {
      _enableCounter = _enableCounter + (enable_ ? -1 : 1);
      if (_enableCounter == 0)
      {
        _layoutProgressBarWrapper.Stop();
        EnableUIControls(true);
      }
      else
      {
        _layoutProgressBarWrapper.Start();
        EnableUIControls(false);
      }
    }

    public void SetDataSourceForMailGroup(ICustomFilter filter_)
    {
      _mailGroupTextBox.DataBindings.Add("Text", filter_, "SearchCriteria");
    }

    public void SetEditableGridLayout(GridLayout gridLayout_)
    {
      _layoutEditor.EditableLayout = gridLayout_;
    }

    public void SetCursor(bool default_)
    {
      Cursor = default_ ? Cursors.Default : Cursors.WaitCursor;
    }

    public void SetCostCenterRadioButton(bool checked_)
    {
      _costCenterLayoutRadioButton.Checked = checked_;
    }

    public void SetFilterStatusTextBox(string text_)
    {
      _filteringStatusTextBox.Text = text_;
    }

    public void EnableMailGroupButtons(bool enable_)
    {
      _applyMailGroupFilter.Enabled = enable_;
      _mailGroupTextBox.Enabled = enable_;
    }

    public GridLayout GetGridLayout()
    {
      return _layoutEditor.Controller.GetGridLayout();
    }

    /// <summary>
    /// Returns the name of the current grid layout.
    /// </summary>
    /// <returns></returns>
    public string GetGridLayoutName()
    {
      return cmbLayouts.Text.Trim();
    }

    #endregion

    private void _btnDelete_Click(object sender, EventArgs e)
    {
      GridLayout layout = Controller.GetGridLayout();
      DialogResult result = MessageBox.Show(
        string.Format( "Are you sure you want to delete Columns setting '{0}'?\n\n If there are Tabs setting or snap tool or Grouping settings dependent on this Columns setting, that Columns setting will be ignored.\n", layout.LayoutName),
        "Delete Columns setting", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

      if (result == DialogResult.No) return;
      Controller.BtnDeleteClick(layout);
    }

    private void _btnShare_Click(object sender, EventArgs e)
    {
      using (DlgRamboShare shareDialog = new DlgRamboShare(_currentWriteGroups))
      {
        if (shareDialog.ShowDialog() == DialogResult.OK)
        {
          Controller.BeginShare(Controller.GetGridLayout(), shareDialog.ShareGroups, DoneGrantPermission);
        }
      }
    }

    private void DoneGrantPermission(IAsyncResult ar)
    {
      try
      {
        Controller.EndShare(ar);
        MessageBox.Show("Resource is successuflly shared", "Share Resource", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
        // get the entitlement from updated cache
        Controller.BeginGetEntitlement(SelectedLayout, false);
      }
      catch (Exception e_)
      {
        _log.Error("DoneGetEntitlement", e_.Message, e_);
        MessageBox.Show("Error occured while share resource", "Share Resource", MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
      }
    }
  }
}
