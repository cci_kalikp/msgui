﻿using System.ComponentModel;
using System.Windows.Forms;
using Infragistics.Win.UltraWinTabControl;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  partial class AvailableItemsSelector
  {

    #region Designer members
    private IContainer components;
    private Timer visibleListScrollTimer;
    private ImageList imageList1;
    private ErrorProvider errorProvider1;
    private System.Windows.Forms.ToolTip _toolTip;
    #endregion

    #region Component Designer generated code
    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand2 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Band 0", -1);
      Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn1 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Column 0");
      Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
      Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
      Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
      Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand1 = new Infragistics.Win.UltraWinGrid.UltraGridBand("Band 0", -1);
      Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn2 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Column 0");
      Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
      Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
      Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
      this.visibleListScrollTimer = new System.Windows.Forms.Timer(this.components);
      this.imageList1 = new System.Windows.Forms.ImageList(this.components);
      this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
      this._toolTip = new System.Windows.Forms.ToolTip(this.components);
      this._rightultraGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
      this._leftultraGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
      this.rightLeftPanel = new System.Windows.Forms.Panel();
      this._btnMoveRightDown = new System.Windows.Forms.Button();
      this._btnMoveRightUp = new System.Windows.Forms.Button();
      this._btnAddToRight = new System.Windows.Forms.Button();
      this._btnAddToLeft = new System.Windows.Forms.Button();
      this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
      ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this._rightultraGrid)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this._leftultraGrid)).BeginInit();
      this.rightLeftPanel.SuspendLayout();
      this.tableLayoutPanel1.SuspendLayout();
      this.SuspendLayout();
      // 
      // imageList1
      // 
      this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
      this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
      this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
      // 
      // errorProvider1
      // 
      this.errorProvider1.ContainerControl = this;
      this.errorProvider1.DataMember = "";
      // 
      // _toolTip
      // 
      this._toolTip.ToolTipTitle = "Sort the column as ascending";
      // 
      // _rightultraGrid
      // 
      this._rightultraGrid.AllowDrop = true;
      ultraGridColumn1.Header.VisiblePosition = 0;
      ultraGridColumn1.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(214, 0);
      ultraGridColumn1.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 25);
      ultraGridBand2.Columns.AddRange(new object[] {
            ultraGridColumn1});
      ultraGridBand2.RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.ColumnLayout;
      this._rightultraGrid.DisplayLayout.BandsSerializer.Add(ultraGridBand2);
      this._rightultraGrid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
      appearance1.BorderColor = System.Drawing.Color.White;
      this._rightultraGrid.DisplayLayout.EmptyRowSettings.RowAppearance = appearance1;
      this._rightultraGrid.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
      this._rightultraGrid.DisplayLayout.EmptyRowSettings.Style = Infragistics.Win.UltraWinGrid.EmptyRowStyle.AlignWithDataRows;
      this._rightultraGrid.DisplayLayout.MaxColScrollRegions = 1;
      this._rightultraGrid.DisplayLayout.MaxRowScrollRegions = 1;
      this._rightultraGrid.DisplayLayout.Override.AllowColSizing = Infragistics.Win.UltraWinGrid.AllowColSizing.None;
      this._rightultraGrid.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Cell;
      this._rightultraGrid.DisplayLayout.Override.FilterOperatorDefaultValue = Infragistics.Win.UltraWinGrid.FilterOperatorDefaultValue.Contains;
      appearance2.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(129)))));
      appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
      appearance2.BorderColor = System.Drawing.Color.Gray;
      this._rightultraGrid.DisplayLayout.Override.FilterRowAppearance = appearance2;
      this._rightultraGrid.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
      this._rightultraGrid.DisplayLayout.Override.FixedRowStyle = Infragistics.Win.UltraWinGrid.FixedRowStyle.Top;
      appearance3.BorderColor = System.Drawing.Color.Gainsboro;
      this._rightultraGrid.DisplayLayout.Override.RowAppearance = appearance3;
      this._rightultraGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
      this._rightultraGrid.DisplayLayout.Override.RowSizing = Infragistics.Win.UltraWinGrid.RowSizing.Fixed;
      this._rightultraGrid.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.ExtendedAutoDrag;
      this._rightultraGrid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.ExtendedAutoDrag;
      this._rightultraGrid.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
      this._rightultraGrid.Dock = System.Windows.Forms.DockStyle.Fill;
      this._rightultraGrid.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this._rightultraGrid.Location = new System.Drawing.Point(328, 3);
      this._rightultraGrid.Name = "_rightultraGrid";
      this._rightultraGrid.Size = new System.Drawing.Size(220, 339);
      this._rightultraGrid.TabIndex = 40;
      this._rightultraGrid.Enter += new System.EventHandler(this.UltraGrid_Enter);
      this._rightultraGrid.DragDrop += new System.Windows.Forms.DragEventHandler(this.RightultraGrid_DragDrop);
      this._rightultraGrid.DragOver += new System.Windows.Forms.DragEventHandler(this.UltraGrid_DragOver);
      this._rightultraGrid.SelectionDrag += new System.ComponentModel.CancelEventHandler(this._leftultraGrid_SelectionDrag);
      this._rightultraGrid.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.UltraGrid_KeyPress);
      this._rightultraGrid.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.RightutraGrid_InitializeLayout);
      this._rightultraGrid.DragLeave += new System.EventHandler(this.UltraGrid_DragLeave);
      this._rightultraGrid.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler(this.RightultraGrid_AfterSelectChange);
      // 
      // _leftultraGrid
      // 
      this._leftultraGrid.AllowDrop = true;
      ultraGridColumn2.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.Append;
      ultraGridColumn2.Header.VisiblePosition = 0;
      ultraGridColumn2.RowLayoutColumnInfo.PreferredCellSize = new System.Drawing.Size(213, 0);
      ultraGridColumn2.RowLayoutColumnInfo.PreferredLabelSize = new System.Drawing.Size(0, 26);
      ultraGridBand1.Columns.AddRange(new object[] {
            ultraGridColumn2});
      ultraGridBand1.RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.ColumnLayout;
      this._leftultraGrid.DisplayLayout.BandsSerializer.Add(ultraGridBand1);
      this._leftultraGrid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
      appearance4.BorderColor = System.Drawing.Color.White;
      this._leftultraGrid.DisplayLayout.EmptyRowSettings.RowAppearance = appearance4;
      this._leftultraGrid.DisplayLayout.EmptyRowSettings.ShowEmptyRows = true;
      this._leftultraGrid.DisplayLayout.EmptyRowSettings.Style = Infragistics.Win.UltraWinGrid.EmptyRowStyle.AlignWithDataRows;
      this._leftultraGrid.DisplayLayout.GroupByBox.Hidden = true;
      this._leftultraGrid.DisplayLayout.MaxColScrollRegions = 1;
      this._leftultraGrid.DisplayLayout.MaxRowScrollRegions = 1;
      this._leftultraGrid.DisplayLayout.Override.AllowColMoving = Infragistics.Win.UltraWinGrid.AllowColMoving.NotAllowed;
      this._leftultraGrid.DisplayLayout.Override.AllowColSizing = Infragistics.Win.UltraWinGrid.AllowColSizing.None;
      this._leftultraGrid.DisplayLayout.Override.AllowColSwapping = Infragistics.Win.UltraWinGrid.AllowColSwapping.NotAllowed;
      this._leftultraGrid.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
      this._leftultraGrid.DisplayLayout.Override.FilterClearButtonLocation = Infragistics.Win.UltraWinGrid.FilterClearButtonLocation.Cell;
      this._leftultraGrid.DisplayLayout.Override.FilterOperatorDefaultValue = Infragistics.Win.UltraWinGrid.FilterOperatorDefaultValue.Contains;
      appearance5.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(129)))));
      appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
      appearance5.BorderColor = System.Drawing.Color.Gray;
      this._leftultraGrid.DisplayLayout.Override.FilterRowAppearance = appearance5;
      this._leftultraGrid.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
      appearance6.BorderColor = System.Drawing.Color.Gainsboro;
      this._leftultraGrid.DisplayLayout.Override.RowAppearance = appearance6;
      this._leftultraGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
      this._leftultraGrid.DisplayLayout.Override.RowSizing = Infragistics.Win.UltraWinGrid.RowSizing.Fixed;
      this._leftultraGrid.DisplayLayout.Override.RowSpacingAfter = 0;
      this._leftultraGrid.DisplayLayout.Override.RowSpacingBefore = 0;
      this._leftultraGrid.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
      this._leftultraGrid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.ExtendedAutoDrag;
      this._leftultraGrid.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
      this._leftultraGrid.DisplayLayout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.SingleBand;
      this._leftultraGrid.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
      this._leftultraGrid.Dock = System.Windows.Forms.DockStyle.Fill;
      this._leftultraGrid.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this._leftultraGrid.Location = new System.Drawing.Point(3, 3);
      this._leftultraGrid.Name = "_leftultraGrid";
      this._leftultraGrid.Size = new System.Drawing.Size(219, 339);
      this._leftultraGrid.TabIndex = 0;
      this._leftultraGrid.Enter += new System.EventHandler(this.UltraGrid_Enter);
      this._leftultraGrid.DragDrop += new System.Windows.Forms.DragEventHandler(this.LeftultraGrid_DragDrop);
      this._leftultraGrid.DragOver += new System.Windows.Forms.DragEventHandler(this.UltraGrid_DragOver);
      this._leftultraGrid.SelectionDrag += new System.ComponentModel.CancelEventHandler(this._leftultraGrid_SelectionDrag);
      this._leftultraGrid.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.UltraGrid_KeyPress);
      this._leftultraGrid.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.LeftultraGrid_InitializeLayout);
      this._leftultraGrid.DragLeave += new System.EventHandler(this.UltraGrid_DragLeave);
      this._leftultraGrid.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler(this.LeftultraGrid_AfterSelectChange);
      // 
      // rightLeftPanel
      // 
      this.rightLeftPanel.AllowDrop = true;
      this.rightLeftPanel.Controls.Add(this._btnMoveRightDown);
      this.rightLeftPanel.Controls.Add(this._btnMoveRightUp);
      this.rightLeftPanel.Controls.Add(this._btnAddToRight);
      this.rightLeftPanel.Controls.Add(this._btnAddToLeft);
      this.rightLeftPanel.Dock = System.Windows.Forms.DockStyle.Fill;
      this.rightLeftPanel.Location = new System.Drawing.Point(228, 3);
      this.rightLeftPanel.Name = "rightLeftPanel";
      this.rightLeftPanel.Size = new System.Drawing.Size(94, 339);
      this.rightLeftPanel.TabIndex = 38;
      // 
      // _btnMoveRightDown
      // 
      this._btnMoveRightDown.AllowDrop = true;
      this._btnMoveRightDown.Anchor = System.Windows.Forms.AnchorStyles.None;
      this._btnMoveRightDown.Enabled = false;
      this._btnMoveRightDown.Image = global::MorganStanley.MSDesktop.Rambo.WinGridUtils.Properties.Resources.downArrow;
      this._btnMoveRightDown.Location = new System.Drawing.Point(35, 179);
      this._btnMoveRightDown.Name = "_btnMoveRightDown";
      this._btnMoveRightDown.Size = new System.Drawing.Size(24, 24);
      this._btnMoveRightDown.TabIndex = 37;
      this._btnMoveRightDown.Click += new System.EventHandler(this.BtnMoveRightDown_Click);
      // 
      // _btnMoveRightUp
      // 
      this._btnMoveRightUp.AllowDrop = true;
      this._btnMoveRightUp.Anchor = System.Windows.Forms.AnchorStyles.None;
      this._btnMoveRightUp.Enabled = false;
      this._btnMoveRightUp.Image = global::MorganStanley.MSDesktop.Rambo.WinGridUtils.Properties.Resources.upArrow;
      this._btnMoveRightUp.Location = new System.Drawing.Point(35, 137);
      this._btnMoveRightUp.Name = "_btnMoveRightUp";
      this._btnMoveRightUp.Size = new System.Drawing.Size(24, 24);
      this._btnMoveRightUp.TabIndex = 36;
      this._btnMoveRightUp.Click += new System.EventHandler(this.BtnMoveRightUp_Click);
      // 
      // _btnAddToRight
      // 
      this._btnAddToRight.AllowDrop = true;
      this._btnAddToRight.Anchor = System.Windows.Forms.AnchorStyles.None;
      this._btnAddToRight.Enabled = false;
      this._btnAddToRight.Image = global::MorganStanley.MSDesktop.Rambo.WinGridUtils.Properties.Resources.rightArrow;
      this._btnAddToRight.Location = new System.Drawing.Point(60, 159);
      this._btnAddToRight.Name = "_btnAddToRight";
      this._btnAddToRight.Size = new System.Drawing.Size(24, 24);
      this._btnAddToRight.TabIndex = 34;
      this._btnAddToRight.Click += new System.EventHandler(this.BtnAddToRight_Click);
      // 
      // _btnAddToLeft
      // 
      this._btnAddToLeft.AllowDrop = true;
      this._btnAddToLeft.Anchor = System.Windows.Forms.AnchorStyles.None;
      this._btnAddToLeft.Enabled = false;
      this._btnAddToLeft.Image = global::MorganStanley.MSDesktop.Rambo.WinGridUtils.Properties.Resources.leftArrow;
      this._btnAddToLeft.Location = new System.Drawing.Point(10, 159);
      this._btnAddToLeft.Name = "_btnAddToLeft";
      this._btnAddToLeft.Size = new System.Drawing.Size(24, 24);
      this._btnAddToLeft.TabIndex = 35;
      this._btnAddToLeft.Click += new System.EventHandler(this.BtnAddToLeft_Click);
      // 
      // tableLayoutPanel1
      // 
      this.tableLayoutPanel1.ColumnCount = 3;
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel1.Controls.Add(this.rightLeftPanel, 1, 0);
      this.tableLayoutPanel1.Controls.Add(this._leftultraGrid, 0, 0);
      this.tableLayoutPanel1.Controls.Add(this._rightultraGrid, 2, 0);
      this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
      this.tableLayoutPanel1.Name = "tableLayoutPanel1";
      this.tableLayoutPanel1.RowCount = 1;
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.tableLayoutPanel1.Size = new System.Drawing.Size(551, 345);
      this.tableLayoutPanel1.TabIndex = 41;
      // 
      // AvailableItemsSelector
      // 
      this.AllowDrop = true;
      this.Controls.Add(this.tableLayoutPanel1);
      this.Name = "AvailableItemsSelector";
      this.Size = new System.Drawing.Size(551, 345);
      ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this._rightultraGrid)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this._leftultraGrid)).EndInit();
      this.rightLeftPanel.ResumeLayout(false);
      this.tableLayoutPanel1.ResumeLayout(false);
      this.ResumeLayout(false);

    }
    #endregion

    private Infragistics.Win.UltraWinGrid.UltraGrid _rightultraGrid;
    private Infragistics.Win.UltraWinGrid.UltraGrid _leftultraGrid;
    private Panel rightLeftPanel;
    private Button _btnMoveRightDown;
    private Button _btnMoveRightUp;
    private Button _btnAddToRight;
    private Button _btnAddToLeft;
    private TableLayoutPanel tableLayoutPanel1;
  }
}
