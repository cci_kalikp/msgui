namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  partial class LayoutExplorer
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing)
      {
        if(components != null)
        components.Dispose();

        if(cmbLayouts != null)
        cmbLayouts.SelectionChanged -= CmbLayouts_SelectionChanged;
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
      Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
      Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
      this._selectLayoutGroupBox = new System.Windows.Forms.GroupBox();
      this.btnShare = new System.Windows.Forms.Button();
      this.txtLastUpdateId = new System.Windows.Forms.TextBox();
      this.lblLastModifyBy = new System.Windows.Forms.Label();
      this.txtSharedBy = new System.Windows.Forms.TextBox();
      this.lblSharedBy = new System.Windows.Forms.Label();
      this.cmbLayouts = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
      this._refreshButton = new Infragistics.Win.Misc.UltraButton();
      this._btnDelete = new Infragistics.Win.Misc.UltraButton();
      this._layoutProgressBarWrapper = new MorganStanley.MSDesktop.Rambo.WinGridUtils.ProgressBarWrapper();
      this._filterListingLabel = new System.Windows.Forms.Label();
      this._filteringStatusTextBox = new System.Windows.Forms.TextBox();
      this.lblOwner = new System.Windows.Forms.Label();
      this.txtOwnerName = new System.Windows.Forms.TextBox();
      this.lblDetails = new System.Windows.Forms.Label();
      this._filterPanel = new System.Windows.Forms.Panel();
      this._applyMailGroupFilter = new Infragistics.Win.Misc.UltraButton();
      this._mailGroupTextBox = new System.Windows.Forms.TextBox();
      this._ownedByMeRadioButton = new System.Windows.Forms.RadioButton();
      this._costCenterLayoutRadioButton = new System.Windows.Forms.RadioButton();
      this._allLayoutsRadioButton = new System.Windows.Forms.RadioButton();
      this.filterByLabel = new System.Windows.Forms.Label();
      this._filterMailGroupRadioBtn = new System.Windows.Forms.RadioButton();
      this._saveAsUltraButton = new Infragistics.Win.Misc.UltraButton();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.btnSave = new Infragistics.Win.Misc.UltraButton();
      this.btnApply = new Infragistics.Win.Misc.UltraButton();
      this.btnCancel = new Infragistics.Win.Misc.UltraButton();
      this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
      this._layoutEditor = new MorganStanley.MSDesktop.Rambo.WinGridUtils.LayoutEditor();
      this._selectLayoutGroupBox.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.cmbLayouts)).BeginInit();
      this._filterPanel.SuspendLayout();
      this.groupBox1.SuspendLayout();
      this.SuspendLayout();
      // 
      // _selectLayoutGroupBox
      // 
      this._selectLayoutGroupBox.BackColor = System.Drawing.SystemColors.Control;
      this._selectLayoutGroupBox.Controls.Add(this.btnShare);
      this._selectLayoutGroupBox.Controls.Add(this.txtLastUpdateId);
      this._selectLayoutGroupBox.Controls.Add(this.lblLastModifyBy);
      this._selectLayoutGroupBox.Controls.Add(this.txtSharedBy);
      this._selectLayoutGroupBox.Controls.Add(this.lblSharedBy);
      this._selectLayoutGroupBox.Controls.Add(this.cmbLayouts);
      this._selectLayoutGroupBox.Controls.Add(this._refreshButton);
      this._selectLayoutGroupBox.Controls.Add(this._btnDelete);
      this._selectLayoutGroupBox.Controls.Add(this._layoutProgressBarWrapper);
      this._selectLayoutGroupBox.Controls.Add(this._filterListingLabel);
      this._selectLayoutGroupBox.Controls.Add(this._filteringStatusTextBox);
      this._selectLayoutGroupBox.Controls.Add(this.lblOwner);
      this._selectLayoutGroupBox.Controls.Add(this.txtOwnerName);
      this._selectLayoutGroupBox.Controls.Add(this.lblDetails);
      this._selectLayoutGroupBox.Controls.Add(this._filterPanel);
      this._selectLayoutGroupBox.Dock = System.Windows.Forms.DockStyle.Top;
      this._selectLayoutGroupBox.ForeColor = System.Drawing.SystemColors.ActiveCaption;
      this._selectLayoutGroupBox.Location = new System.Drawing.Point(0, 0);
      this._selectLayoutGroupBox.Name = "_selectLayoutGroupBox";
      this._selectLayoutGroupBox.Size = new System.Drawing.Size(571, 158);
      this._selectLayoutGroupBox.TabIndex = 62;
      this._selectLayoutGroupBox.TabStop = false;
      this._selectLayoutGroupBox.Text = "Selected Columns:";
      // 
      // btnShare
      // 
      this.btnShare.Enabled = false;
      this.btnShare.ForeColor = System.Drawing.SystemColors.ControlText;
      this.btnShare.Image = global::MorganStanley.MSDesktop.Rambo.WinGridUtils.Properties.Resources.group_16;
      this.btnShare.Location = new System.Drawing.Point(386, 18);
      this.btnShare.Name = "btnShare";
      this.btnShare.Size = new System.Drawing.Size(25, 24);
      this.btnShare.TabIndex = 75;
      this.toolTip1.SetToolTip(this.btnShare, "Grant write permission for current Columns setting");
      this.btnShare.UseVisualStyleBackColor = true;
      this.btnShare.Click += new System.EventHandler(this._btnShare_Click);
      // 
      // txtLastUpdateId
      // 
      this.txtLastUpdateId.BackColor = System.Drawing.SystemColors.Control;
      this.txtLastUpdateId.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.txtLastUpdateId.Cursor = System.Windows.Forms.Cursors.Arrow;
      this.txtLastUpdateId.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.txtLastUpdateId.ForeColor = System.Drawing.SystemColors.WindowText;
      this.txtLastUpdateId.Location = new System.Drawing.Point(265, 67);
      this.txtLastUpdateId.Name = "txtLastUpdateId";
      this.txtLastUpdateId.ReadOnly = true;
      this.txtLastUpdateId.Size = new System.Drawing.Size(112, 13);
      this.txtLastUpdateId.TabIndex = 74;
      this.txtLastUpdateId.Text = "-";
      // 
      // lblLastModifyBy
      // 
      this.lblLastModifyBy.AutoSize = true;
      this.lblLastModifyBy.ForeColor = System.Drawing.SystemColors.ControlText;
      this.lblLastModifyBy.Location = new System.Drawing.Point(173, 67);
      this.lblLastModifyBy.Name = "lblLastModifyBy";
      this.lblLastModifyBy.Size = new System.Drawing.Size(86, 13);
      this.lblLastModifyBy.TabIndex = 73;
      this.lblLastModifyBy.Text = "Last modified by:";
      // 
      // txtSharedBy
      // 
      this.txtSharedBy.BackColor = System.Drawing.SystemColors.Control;
      this.txtSharedBy.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.txtSharedBy.Cursor = System.Windows.Forms.Cursors.Arrow;
      this.txtSharedBy.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.txtSharedBy.ForeColor = System.Drawing.SystemColors.WindowText;
      this.txtSharedBy.Location = new System.Drawing.Point(68, 87);
      this.txtSharedBy.Name = "txtSharedBy";
      this.txtSharedBy.ReadOnly = true;
      this.txtSharedBy.Size = new System.Drawing.Size(309, 13);
      this.txtSharedBy.TabIndex = 72;
      this.txtSharedBy.Text = "-";
      // 
      // lblSharedBy
      // 
      this.lblSharedBy.AutoSize = true;
      this.lblSharedBy.ForeColor = System.Drawing.SystemColors.ControlText;
      this.lblSharedBy.Location = new System.Drawing.Point(6, 87);
      this.lblSharedBy.Name = "lblSharedBy";
      this.lblSharedBy.Size = new System.Drawing.Size(59, 13);
      this.lblSharedBy.TabIndex = 71;
      this.lblSharedBy.Text = "Shared By:";
      // 
      // cmbLayouts
      // 
      this.cmbLayouts.AlwaysInEditMode = true;
      this.cmbLayouts.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.SuggestAppend;
      this.cmbLayouts.LimitToList = true;
      this.cmbLayouts.Location = new System.Drawing.Point(52, 19);
      this.cmbLayouts.Name = "cmbLayouts";
      this.cmbLayouts.Size = new System.Drawing.Size(305, 21);
      this.cmbLayouts.TabIndex = 70;
      this.cmbLayouts.SelectionChanged += new System.EventHandler(this.CmbLayouts_SelectionChanged);
      this.cmbLayouts.LostFocus += new System.EventHandler(this.CmbLayouts_LostFocus);
      this.cmbLayouts.GotFocus += new System.EventHandler(this.CmbLayouts_GotFocus);
      // 
      // _refreshButton
      // 
      this._refreshButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      appearance1.ForeColor = System.Drawing.Color.Black;
      this._refreshButton.Appearance = appearance1;
      this._refreshButton.Location = new System.Drawing.Point(510, 18);
      this._refreshButton.Name = "_refreshButton";
      this._refreshButton.Size = new System.Drawing.Size(55, 24);
      this._refreshButton.TabIndex = 69;
      this._refreshButton.Text = "Refresh";
      this._refreshButton.Click += new System.EventHandler(this._refreshButton_Click);
      // 
      // _btnDelete
      // 
      this._btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      appearance2.ForeColor = System.Drawing.Color.Black;
      appearance2.Image = global::MorganStanley.MSDesktop.Rambo.WinGridUtils.Properties.Resources.cancel;
      this._btnDelete.Appearance = appearance2;
      this._btnDelete.Enabled = false;
      this._btnDelete.Location = new System.Drawing.Point(359, 18);
      this._btnDelete.Name = "_btnDelete";
      this._btnDelete.Size = new System.Drawing.Size(25, 24);
      this._btnDelete.TabIndex = 69;
      this.toolTip1.SetToolTip(this._btnDelete, "Delete Columns setting");
      this._btnDelete.Click += new System.EventHandler(this._btnDelete_Click);
      // 
      // _layoutProgressBarWrapper
      // 
      this._layoutProgressBarWrapper.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this._layoutProgressBarWrapper.AutoIncrementMilliseconds = 30;
      this._layoutProgressBarWrapper.AutoRestart = true;
      this._layoutProgressBarWrapper.HideWhenStopped = true;
      this._layoutProgressBarWrapper.Location = new System.Drawing.Point(416, 45);
      this._layoutProgressBarWrapper.Name = "_layoutProgressBarWrapper";
      this._layoutProgressBarWrapper.ProgressBarText = "";
      this._layoutProgressBarWrapper.Size = new System.Drawing.Size(55, 23);
      this._layoutProgressBarWrapper.Style = MorganStanley.MSDesktop.Rambo.WinGridUtils.ProgressBarWrapper.ProgressBarWrapperStyle.AsyncSpinner;
      this._layoutProgressBarWrapper.TabIndex = 69;
      // 
      // _filterListingLabel
      // 
      this._filterListingLabel.AutoSize = true;
      this._filterListingLabel.ForeColor = System.Drawing.SystemColors.ControlText;
      this._filterListingLabel.Location = new System.Drawing.Point(7, 45);
      this._filterListingLabel.Name = "_filterListingLabel";
      this._filterListingLabel.Size = new System.Drawing.Size(40, 13);
      this._filterListingLabel.TabIndex = 66;
      this._filterListingLabel.Text = "Listing:";
      // 
      // _filteringStatusTextBox
      // 
      this._filteringStatusTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this._filteringStatusTextBox.Cursor = System.Windows.Forms.Cursors.Arrow;
      this._filteringStatusTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this._filteringStatusTextBox.ForeColor = System.Drawing.Color.Red;
      this._filteringStatusTextBox.Location = new System.Drawing.Point(55, 45);
      this._filteringStatusTextBox.Name = "_filteringStatusTextBox";
      this._filteringStatusTextBox.ReadOnly = true;
      this._filteringStatusTextBox.Size = new System.Drawing.Size(433, 13);
      this._filteringStatusTextBox.TabIndex = 65;
      this._filteringStatusTextBox.Text = "-";
      // 
      // lblOwner
      // 
      this.lblOwner.AutoSize = true;
      this.lblOwner.ForeColor = System.Drawing.SystemColors.ControlText;
      this.lblOwner.Location = new System.Drawing.Point(6, 67);
      this.lblOwner.Name = "lblOwner";
      this.lblOwner.Size = new System.Drawing.Size(41, 13);
      this.lblOwner.TabIndex = 0;
      this.lblOwner.Text = "Owner:";
      // 
      // txtOwnerName
      // 
      this.txtOwnerName.BackColor = System.Drawing.SystemColors.Control;
      this.txtOwnerName.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.txtOwnerName.Cursor = System.Windows.Forms.Cursors.Arrow;
      this.txtOwnerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.txtOwnerName.ForeColor = System.Drawing.SystemColors.WindowText;
      this.txtOwnerName.Location = new System.Drawing.Point(55, 67);
      this.txtOwnerName.Name = "txtOwnerName";
      this.txtOwnerName.ReadOnly = true;
      this.txtOwnerName.Size = new System.Drawing.Size(112, 13);
      this.txtOwnerName.TabIndex = 55;
      this.txtOwnerName.Text = "-";
      // 
      // lblDetails
      // 
      this.lblDetails.ForeColor = System.Drawing.SystemColors.ControlText;
      this.lblDetails.Location = new System.Drawing.Point(7, 23);
      this.lblDetails.Name = "lblDetails";
      this.lblDetails.Size = new System.Drawing.Size(44, 19);
      this.lblDetails.TabIndex = 0;
      this.lblDetails.Text = "Name:";
      // 
      // _filterPanel
      // 
      this._filterPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this._filterPanel.Controls.Add(this._applyMailGroupFilter);
      this._filterPanel.Controls.Add(this._mailGroupTextBox);
      this._filterPanel.Controls.Add(this._ownedByMeRadioButton);
      this._filterPanel.Controls.Add(this._costCenterLayoutRadioButton);
      this._filterPanel.Controls.Add(this._allLayoutsRadioButton);
      this._filterPanel.Controls.Add(this.filterByLabel);
      this._filterPanel.Controls.Add(this._filterMailGroupRadioBtn);
      this._filterPanel.Location = new System.Drawing.Point(6, 106);
      this._filterPanel.Name = "_filterPanel";
      this._filterPanel.Size = new System.Drawing.Size(562, 49);
      this._filterPanel.TabIndex = 68;
      // 
      // _applyMailGroupFilter
      // 
      this._applyMailGroupFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      appearance3.ForeColor = System.Drawing.Color.Black;
      this._applyMailGroupFilter.Appearance = appearance3;
      this._applyMailGroupFilter.DialogResult = System.Windows.Forms.DialogResult.OK;
      this._applyMailGroupFilter.Location = new System.Drawing.Point(504, 19);
      this._applyMailGroupFilter.Name = "_applyMailGroupFilter";
      this._applyMailGroupFilter.Size = new System.Drawing.Size(55, 23);
      this._applyMailGroupFilter.TabIndex = 69;
      this._applyMailGroupFilter.Text = "Apply";
      this._applyMailGroupFilter.Click += new System.EventHandler(this.ApplyMailGroupFilter_Click);
      // 
      // _mailGroupTextBox
      // 
      this._mailGroupTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this._mailGroupTextBox.Location = new System.Drawing.Point(321, 21);
      this._mailGroupTextBox.Name = "_mailGroupTextBox";
      this._mailGroupTextBox.Size = new System.Drawing.Size(177, 20);
      this._mailGroupTextBox.TabIndex = 7;
      this._mailGroupTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MailGroupTextBox_KeyDown);
      // 
      // _ownedByMeRadioButton
      // 
      this._ownedByMeRadioButton.AutoSize = true;
      this._ownedByMeRadioButton.ForeColor = System.Drawing.SystemColors.ControlText;
      this._ownedByMeRadioButton.Location = new System.Drawing.Point(110, 22);
      this._ownedByMeRadioButton.Name = "_ownedByMeRadioButton";
      this._ownedByMeRadioButton.Size = new System.Drawing.Size(84, 17);
      this._ownedByMeRadioButton.TabIndex = 4;
      this._ownedByMeRadioButton.TabStop = true;
      this._ownedByMeRadioButton.Text = "Current User";
      this._ownedByMeRadioButton.UseVisualStyleBackColor = true;
      this._ownedByMeRadioButton.CheckedChanged += new System.EventHandler(this.FilterRadioButtons_StatusChanged);
      // 
      // _costCenterLayoutRadioButton
      // 
      this._costCenterLayoutRadioButton.AutoSize = true;
      this._costCenterLayoutRadioButton.ForeColor = System.Drawing.SystemColors.ControlText;
      this._costCenterLayoutRadioButton.Location = new System.Drawing.Point(23, 22);
      this._costCenterLayoutRadioButton.Name = "_costCenterLayoutRadioButton";
      this._costCenterLayoutRadioButton.Size = new System.Drawing.Size(80, 17);
      this._costCenterLayoutRadioButton.TabIndex = 3;
      this._costCenterLayoutRadioButton.TabStop = true;
      this._costCenterLayoutRadioButton.Text = "Cost Center";
      this._costCenterLayoutRadioButton.UseVisualStyleBackColor = true;
      this._costCenterLayoutRadioButton.CheckedChanged += new System.EventHandler(this.FilterRadioButtons_StatusChanged);
      // 
      // _allLayoutsRadioButton
      // 
      this._allLayoutsRadioButton.AutoSize = true;
      this._allLayoutsRadioButton.ForeColor = System.Drawing.SystemColors.ControlText;
      this._allLayoutsRadioButton.Location = new System.Drawing.Point(201, 22);
      this._allLayoutsRadioButton.Name = "_allLayoutsRadioButton";
      this._allLayoutsRadioButton.Size = new System.Drawing.Size(36, 17);
      this._allLayoutsRadioButton.TabIndex = 5;
      this._allLayoutsRadioButton.TabStop = true;
      this._allLayoutsRadioButton.Text = "All";
      this._allLayoutsRadioButton.UseVisualStyleBackColor = true;
      this._allLayoutsRadioButton.CheckedChanged += new System.EventHandler(this.FilterRadioButtons_StatusChanged);
      // 
      // filterByLabel
      // 
      this.filterByLabel.ForeColor = System.Drawing.SystemColors.ControlText;
      this.filterByLabel.Location = new System.Drawing.Point(1, 1);
      this.filterByLabel.Name = "filterByLabel";
      this.filterByLabel.Size = new System.Drawing.Size(150, 19);
      this.filterByLabel.TabIndex = 64;
      this.filterByLabel.Text = "Filter Columns settings by:";
      // 
      // _filterMailGroupRadioBtn
      // 
      this._filterMailGroupRadioBtn.AutoSize = true;
      this._filterMailGroupRadioBtn.ForeColor = System.Drawing.SystemColors.ControlText;
      this._filterMailGroupRadioBtn.Location = new System.Drawing.Point(244, 22);
      this._filterMailGroupRadioBtn.Name = "_filterMailGroupRadioBtn";
      this._filterMailGroupRadioBtn.Size = new System.Drawing.Size(74, 17);
      this._filterMailGroupRadioBtn.TabIndex = 6;
      this._filterMailGroupRadioBtn.TabStop = true;
      this._filterMailGroupRadioBtn.Text = "Mailgroup:";
      this._filterMailGroupRadioBtn.UseVisualStyleBackColor = true;
      this._filterMailGroupRadioBtn.CheckedChanged += new System.EventHandler(this.FilterRadioButtons_StatusChanged);
      // 
      // _saveAsUltraButton
      // 
      this._saveAsUltraButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this._saveAsUltraButton.Location = new System.Drawing.Point(248, 16);
      this._saveAsUltraButton.Name = "_saveAsUltraButton";
      this._saveAsUltraButton.ShowFocusRect = false;
      this._saveAsUltraButton.Size = new System.Drawing.Size(75, 23);
      this._saveAsUltraButton.TabIndex = 67;
      this._saveAsUltraButton.Text = "Save As ...";
      this._saveAsUltraButton.Click += new System.EventHandler(this.SaveAsUltraButton_Click);
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.btnSave);
      this.groupBox1.Controls.Add(this._saveAsUltraButton);
      this.groupBox1.Controls.Add(this.btnApply);
      this.groupBox1.Controls.Add(this.btnCancel);
      this.groupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.groupBox1.Location = new System.Drawing.Point(0, 555);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(571, 45);
      this.groupBox1.TabIndex = 66;
      this.groupBox1.TabStop = false;
      // 
      // btnSave
      // 
      this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btnSave.Enabled = false;
      this.btnSave.Location = new System.Drawing.Point(329, 16);
      this.btnSave.Name = "btnSave";
      this.btnSave.Size = new System.Drawing.Size(75, 23);
      this.btnSave.TabIndex = 63;
      this.btnSave.Text = "Save";
      this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
      // 
      // btnApply
      // 
      this.btnApply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btnApply.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.btnApply.Location = new System.Drawing.Point(410, 16);
      this.btnApply.Name = "btnApply";
      this.btnApply.Size = new System.Drawing.Size(75, 23);
      this.btnApply.TabIndex = 64;
      this.btnApply.Text = "Apply";
      this.btnApply.Click += new System.EventHandler(this.BtnApply_Click);
      // 
      // btnCancel
      // 
      this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.Location = new System.Drawing.Point(491, 16);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(75, 23);
      this.btnCancel.TabIndex = 65;
      this.btnCancel.Text = "Cancel";
      this.btnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
      // 
      // _layoutEditor
      // 
      this._layoutEditor.AllowDrop = true;
      this._layoutEditor.ColumnCategories = null;
      this._layoutEditor.ColumnDescriptions = null;
      this._layoutEditor.Dock = System.Windows.Forms.DockStyle.Fill;
      this._layoutEditor.EnableFormatChanges = true;
      this._layoutEditor.EnableSortChanges = true;
      this._layoutEditor.Location = new System.Drawing.Point(0, 158);
      this._layoutEditor.Name = "_layoutEditor";
      this._layoutEditor.Size = new System.Drawing.Size(571, 397);
      this._layoutEditor.StopClose = false;
      this._layoutEditor.TabIndex = 68;
      this._layoutEditor.TurboRiskGroups = null;
      this._layoutEditor.UserName = null;
      this._layoutEditor.VerifyColumnKeysList = null;
      // 
      // LayoutExplorer
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.AutoSize = true;
      this.Controls.Add(this._layoutEditor);
      this.Controls.Add(this.groupBox1);
      this.Controls.Add(this._selectLayoutGroupBox);
      this.Name = "LayoutExplorer";
      this.Size = new System.Drawing.Size(571, 600);
      this._selectLayoutGroupBox.ResumeLayout(false);
      this._selectLayoutGroupBox.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.cmbLayouts)).EndInit();
      this._filterPanel.ResumeLayout(false);
      this._filterPanel.PerformLayout();
      this.groupBox1.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.GroupBox _selectLayoutGroupBox;
    private MorganStanley.MSDesktop.Rambo.WinGridUtils.ProgressBarWrapper _layoutProgressBarWrapper;
    private System.Windows.Forms.Panel _filterPanel;
    private System.Windows.Forms.RadioButton _ownedByMeRadioButton;
    private System.Windows.Forms.RadioButton _costCenterLayoutRadioButton;
    private System.Windows.Forms.RadioButton _allLayoutsRadioButton;
    private System.Windows.Forms.Label filterByLabel;
    private System.Windows.Forms.RadioButton _filterMailGroupRadioBtn;
    private System.Windows.Forms.TextBox _mailGroupTextBox;
    private System.Windows.Forms.Label _filterListingLabel;
    private System.Windows.Forms.TextBox _filteringStatusTextBox;
    private System.Windows.Forms.Label lblOwner;
    private System.Windows.Forms.TextBox txtOwnerName;
    private System.Windows.Forms.Label lblDetails;
    private Infragistics.Win.Misc.UltraButton _saveAsUltraButton;
    private System.Windows.Forms.GroupBox groupBox1;
    private Infragistics.Win.Misc.UltraButton btnCancel;
    private Infragistics.Win.Misc.UltraButton btnSave;
    private Infragistics.Win.Misc.UltraButton btnApply;
    private LayoutEditor _layoutEditor;
    private Infragistics.Win.Misc.UltraButton _refreshButton;
    private Infragistics.Win.Misc.UltraButton _applyMailGroupFilter;
    private Infragistics.Win.UltraWinEditors.UltraComboEditor cmbLayouts;
    private Infragistics.Win.Misc.UltraButton _btnDelete;
    private System.Windows.Forms.Label lblSharedBy;
    private System.Windows.Forms.TextBox txtSharedBy;
    private System.Windows.Forms.Label lblLastModifyBy;
    private System.Windows.Forms.Button btnShare;
    private System.Windows.Forms.TextBox txtLastUpdateId;
    private System.Windows.Forms.ToolTip toolTip1;
  }
}
