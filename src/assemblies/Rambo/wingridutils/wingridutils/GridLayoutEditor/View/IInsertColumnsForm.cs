﻿namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  public interface IInsertColumnsForm
  {
    void DoSetDescription(string caption, string description, string category,string turboRiskId);
    void HideGroupUnGroupButtons();
    void SetDataSource(AvailableItemsSelectorDataSource<IClassWrapper> dataSource_);
  }
}
