using System;
using System.Windows.Forms;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  public partial class DlgSaveAs : Form
  {
    public DlgSaveAs()
    {
      InitializeComponent();
    }

    public string LayoutName
    {
      get
      {
         return _layoutNameTextBox.Text;
      }
    }

    public string InitialText
    {
      set
      {
        if (_layoutNameTextBox.Text.Length==0)
        {
          _layoutNameTextBox.Text = value;
        }
      }
    }

    private void _saveButton_Click(object sender, EventArgs e)
    {
      DialogResult = DialogResult.OK;
      Close();
    }

  }
}