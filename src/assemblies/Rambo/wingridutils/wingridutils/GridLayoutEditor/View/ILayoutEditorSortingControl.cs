﻿namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  public interface ILayoutEditorSortingControl:IAvailableItemSelector
  {
    IAvailableItemSelector AvailItemsSelector { get;}
    void SetCColLayoutProperties();
    void UnSetColLayoutProperties();
  }
}
