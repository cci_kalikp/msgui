﻿using System;
using System.Drawing;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;


namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  /// <summary>
  /// DrawFilter on the UltraTree to draw a line representing a drag-drop spot, to
  /// make drag/drop events on a tree extremely clear to the user
  /// </summary>
  public class DragDropHighlightDrawFilter : IUIElementDrawFilter
  {

    #region Fields

    private UltraGridRow _mvarDropHighLightRow;
    private Color _mvarDropLineColor;
    private int _mvarDropLineWidth;
    private Point _point;

    private readonly IUIElementDrawFilter _derivedColumnHighLightingDrawFilter;

    #endregion

    #region Constructors

    public DragDropHighlightDrawFilter()
    {
      //Initialize the properties to the defaults
      InitProperties();
    }

    public DragDropHighlightDrawFilter(GridLayoutEditor.DataModel.IColumnFilterProvider columnFilterProvider_)
      : this()
    {
      if (columnFilterProvider_ != null)
        _derivedColumnHighLightingDrawFilter = new ColumnSelectorDrawFilter(columnFilterProvider_);
    }

    #endregion

    #region Instance Properties

    public UltraGridRow DropHightLightNode
    {
      get { return _mvarDropHighLightRow; }
      set
      {
        //If the Row is being set to the same value,
        // just exit
        if (value.Equals(_mvarDropHighLightRow))
        {
          return;
        }
        _mvarDropHighLightRow = value;
        //The DropNode has changed.
        PositionChanged();
      }
    }

    //The color of the DropLine
    public Color DropLineColor
    {
      get { return _mvarDropLineColor; }
      set { _mvarDropLineColor = value; }
    }

    public int DropLineWidth
    {
      get { return _mvarDropLineWidth; }
      set { _mvarDropLineWidth = value; }
    }

    #endregion

    #region Instance Methods

    public void ClearDropHighlight()
    {
      SetDropHighlightNode(null, Point.Empty);
    }

    public void Dispose()
    {
      _mvarDropHighLightRow = null;
    }

    //Call this proc every time the DragOver event of the 
    //UltraGrid fires. 
    public void SetDropHighlightNode(UltraGridRow row_, Point point_)
    {
      _point = point_;
      //Use to store whether there have been any changes in 
      //DropNode or DropLinePosition
      bool isPositionChanged = false;

      try
      {
        //Check to see if the nodes are equal and if 
        //the dropline position are equal
        if (_mvarDropHighLightRow == null || !_mvarDropHighLightRow.Equals(row_))
        {
          //They are both equal. Nothing has changed. 
          isPositionChanged = true;
        }
      }
      catch
      {
        //If we reach this code, it means _mvarDropHighLightRow 
        //is null, so it could not be compared
        if (_mvarDropHighLightRow == null)
        {
          //Check to see if row_ is nothing, so we//ll know
          //if row_ = _mvarDropHighLightRow
          isPositionChanged = row_ != null;
        }
      }

      //Set both properties without calling PositionChanged
      _mvarDropHighLightRow = row_;

      //Check to see if the PositionChanged
      if (isPositionChanged)
      {
        //Position did change.
        PositionChanged();
      }
    }

    private void InitProperties()
    {
      _mvarDropHighLightRow = null;
      _mvarDropLineColor = SystemColors.ControlText;
      _mvarDropLineWidth = 2;
    }

    /// <summary>
    /// When the DropRow or DropPosition change, we fire the
    /// Invalidate event to let the program know to invalidate
    /// the UltraGrid control. 
    /// This is neccessary since the DrawFilter does not have a 
    /// reference to the UltraGrid Control (although it probably could)
    /// Set the DropRow to Nothing and the position to None. This
    /// Will clear whatever Drophighlight is in the tree
    /// </summary>
    private void PositionChanged()
    {
      // if nobody is listening then just return
      //
      if (null == Invalidate)
        return;

      EventArgs e = EventArgs.Empty;

      Invalidate(this, e);
    }

    /// <summary>
    /// Check whether there is a vertical scroll bar in the grid
    /// </summary>
    public bool HasVertScrollBar(UltraGrid grid_)
    {
      var aRowScrollbarUIElement = (RowScrollbarUIElement)grid_.DisplayLayout.UIElement.GetDescendant(typeof(RowScrollbarUIElement));
      return (aRowScrollbarUIElement != null);
    }

    #endregion

    #region Event Declarations

    public event EventHandler Invalidate;

    #endregion

    #region IUIElementDrawFilter Members

    /// <summary>
    /// Only need to trap for 2 phases:
    ///   AfterDrawElement: for drawing the DropLine
    ///   BeforeDrawElement: for drawing the DropHighlight
    /// </summary>
    /// <param name="drawParams_">The <see cref="T:Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
    /// <returns>
    /// Bit flags indicating which phases of the drawing operation to filter. The DrawElement method will be called only for those phases.
    /// </returns>
    DrawPhase IUIElementDrawFilter.GetPhasesToFilter(ref UIElementDrawParams drawParams_)
    {
      const DrawPhase localResult = DrawPhase.AfterDrawElement | DrawPhase.BeforeDrawElement;

      return _derivedColumnHighLightingDrawFilter != null ? localResult | 
        _derivedColumnHighLightingDrawFilter.GetPhasesToFilter(ref drawParams_) : localResult;
    }

    //The actual drawing code
    bool IUIElementDrawFilter.DrawElement(DrawPhase drawPhase_, ref UIElementDrawParams drawParams_)
    {
      if (_derivedColumnHighLightingDrawFilter != null)
      {
        _derivedColumnHighLightingDrawFilter.DrawElement(drawPhase_, ref drawParams_);
      }

      Graphics g;

      //If there//s no DropHighlight node or no position
      //specified, then we don//t need to draw anything special. 
      //Just exit Function
      if (_mvarDropHighLightRow == null)
      {
        return false;
      }

      if (drawPhase_ == DrawPhase.AfterDrawElement)
      {
        var p = new Pen(_mvarDropLineColor, _mvarDropLineWidth);

        //Get a reference to the Graphics object
        //we are drawing to. 
        g = drawParams_.Graphics;

        //Get the RowUIElement for the 
        //current UltraGrid. We will use this for
        //positioning and sizing the DropLine
        var tElement = (RowUIElement)
                                drawParams_.Element.GetDescendant(typeof(RowUIElement), _mvarDropHighLightRow);

        if (null == tElement)
        {
          return false;
        }

        //The left edge of the DropLine
        int leftEdge = tElement.Rect.Left + 1;

        //We need a reference to the control to 
        //determine the right edge of the line
        var grid = (UltraGrid)tElement.GetContext(typeof(UltraGrid));
        int rightEdge;

        //Check whether this grid has vertical scrollBar.
        if (HasVertScrollBar(grid))
        {
          rightEdge = grid.DisplayRectangle.Right - 19;
        }
        else
        {
          rightEdge = grid.DisplayRectangle.Right - 3;
        }

        //Draw line
        int lineVPosition = _point.Y > tElement.Rect.Bottom ? tElement.Rect.Bottom : tElement.Rect.Top;
        g.DrawLine(p, leftEdge, lineVPosition, rightEdge, lineVPosition);
        p.Width = 1;
        g.DrawLine(p, leftEdge, lineVPosition - 3, leftEdge, lineVPosition + 2);
        g.DrawLine(p, leftEdge + 1, lineVPosition - 2, leftEdge + 1, lineVPosition + 1);
        g.DrawLine(p, rightEdge, lineVPosition - 3, rightEdge, lineVPosition + 2);
        g.DrawLine(p, rightEdge - 1, lineVPosition - 2, rightEdge - 1, lineVPosition + 1);
      }
      return false;
    }

    #endregion
  }
}

