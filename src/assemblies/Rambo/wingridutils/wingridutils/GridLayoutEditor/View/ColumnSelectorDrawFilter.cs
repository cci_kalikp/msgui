﻿using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using MorganStanley.MSDesktop.Rambo.WinGridUtils.GridLayoutEditor.DataModel;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  class ColumnSelectorDrawFilter:IUIElementDrawFilter
  {
    private readonly IColumnFilterProvider _columnFilterProvider;

    public ColumnSelectorDrawFilter(IColumnFilterProvider columnFilterProvider_)
    {
      _columnFilterProvider = columnFilterProvider_;
    }
    public DrawPhase GetPhasesToFilter(ref UIElementDrawParams drawParams_)
    {
      if (drawParams_.Element is EmbeddableUIElementBase)
      {
        return DrawPhase.BeforeDrawBackColor | DrawPhase.AfterDrawBackColor;
      }

      return DrawPhase.None;
    }

    public bool DrawElement(DrawPhase drawPhase_, ref UIElementDrawParams drawParams_)
    {
      switch (drawPhase_)
      {
        case (DrawPhase.BeforeDrawBackColor):
          {
            return BeforeDrawBackground(drawPhase_, ref drawParams_);
          }
      }
      return false;
    }

    private bool BeforeDrawBackground(DrawPhase drawPhase_, ref UIElementDrawParams drawParams_)
    {
      var row = drawParams_.Element.SelectableItem as UltraGridRow;
      if (row == null)
        return false;
      if (row.Selected)
        return false;
      var columnWrapper = row.ListObject as ColumnWrapper;

      if (columnWrapper != null && _columnFilterProvider.IsFilterable(columnWrapper.ColumnLayout))
        drawParams_.AppearanceData.BackColor = _columnFilterProvider.GetHighlightColor(columnWrapper.ColumnLayout);
      return false;
    }
  }
}