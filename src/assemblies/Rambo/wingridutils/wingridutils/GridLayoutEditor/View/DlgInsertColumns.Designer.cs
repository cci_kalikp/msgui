﻿namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  partial class DlgInsertColumns
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
      this._groupBoxLayout = new System.Windows.Forms.GroupBox();
      this._radioBtnAfter = new System.Windows.Forms.RadioButton();
      this._radioBtnBefore = new System.Windows.Forms.RadioButton();
      this._lblInsertPosition = new System.Windows.Forms.Label();
      this._lblOwnerName = new System.Windows.Forms.Label();
      this._lblOwner = new System.Windows.Forms.Label();
      this._lblLayout = new System.Windows.Forms.Label();
      this._lblInsert = new System.Windows.Forms.Label();
      this._groupBoxConfig = new System.Windows.Forms.GroupBox();
      this._gridSelector = new MorganStanley.MSDesktop.Rambo.WinGridUtils.AvailableItemsSelector();
      this._btnHiddenUnGroupby = new System.Windows.Forms.Button();
      this._btnHiddenGoupby = new System.Windows.Forms.Button();
      this._btnApply = new System.Windows.Forms.Button();
      this._btnCancel = new System.Windows.Forms.Button();
      this.ultraGroupBoxDescription = new Infragistics.Win.Misc.UltraGroupBox();
      this.pnlRtfDescription = new System.Windows.Forms.Panel();
      this.rtfDescription = new System.Windows.Forms.RichTextBox();
      this._showDerivedColumns = new System.Windows.Forms.CheckBox();
      this._groupBoxLayout.SuspendLayout();
      this._groupBoxConfig.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxDescription)).BeginInit();
      this.ultraGroupBoxDescription.SuspendLayout();
      this.pnlRtfDescription.SuspendLayout();
      this.SuspendLayout();
      // 
      // _groupBoxLayout
      // 
      this._groupBoxLayout.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this._groupBoxLayout.Controls.Add(this._radioBtnAfter);
      this._groupBoxLayout.Controls.Add(this._radioBtnBefore);
      this._groupBoxLayout.Controls.Add(this._lblInsertPosition);
      this._groupBoxLayout.Controls.Add(this._lblOwnerName);
      this._groupBoxLayout.Controls.Add(this._lblOwner);
      this._groupBoxLayout.Controls.Add(this._lblLayout);
      this._groupBoxLayout.Controls.Add(this._lblInsert);
      this._groupBoxLayout.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this._groupBoxLayout.ForeColor = System.Drawing.SystemColors.ActiveCaption;
      this._groupBoxLayout.Location = new System.Drawing.Point(8, 12);
      this._groupBoxLayout.Name = "_groupBoxLayout";
      this._groupBoxLayout.Size = new System.Drawing.Size(494, 72);
      this._groupBoxLayout.TabIndex = 2;
      this._groupBoxLayout.TabStop = false;
      this._groupBoxLayout.Text = "Current Columns";
      // 
      // _radioBtnAfter
      // 
      this._radioBtnAfter.AutoSize = true;
      this._radioBtnAfter.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this._radioBtnAfter.ForeColor = System.Drawing.SystemColors.ControlText;
      this._radioBtnAfter.Location = new System.Drawing.Point(227, 48);
      this._radioBtnAfter.Name = "_radioBtnAfter";
      this._radioBtnAfter.Size = new System.Drawing.Size(49, 19);
      this._radioBtnAfter.TabIndex = 7;
      this._radioBtnAfter.TabStop = true;
      this._radioBtnAfter.Text = "After";
      this._radioBtnAfter.UseVisualStyleBackColor = true;
      this._radioBtnAfter.CheckedChanged += new System.EventHandler(this._radioBtn_CheckedChanged);
      // 
      // _radioBtnBefore
      // 
      this._radioBtnBefore.AutoSize = true;
      this._radioBtnBefore.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this._radioBtnBefore.ForeColor = System.Drawing.SystemColors.ControlText;
      this._radioBtnBefore.Location = new System.Drawing.Point(161, 48);
      this._radioBtnBefore.Name = "_radioBtnBefore";
      this._radioBtnBefore.Size = new System.Drawing.Size(61, 19);
      this._radioBtnBefore.TabIndex = 6;
      this._radioBtnBefore.TabStop = true;
      this._radioBtnBefore.Text = "Before";
      this._radioBtnBefore.UseVisualStyleBackColor = true;
      this._radioBtnBefore.CheckedChanged += new System.EventHandler(this._radioBtn_CheckedChanged);
      // 
      // _lblInsertPosition
      // 
      this._lblInsertPosition.AutoSize = true;
      this._lblInsertPosition.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this._lblInsertPosition.ForeColor = System.Drawing.SystemColors.ControlText;
      this._lblInsertPosition.Location = new System.Drawing.Point(11, 50);
      this._lblInsertPosition.Name = "_lblInsertPosition";
      this._lblInsertPosition.Size = new System.Drawing.Size(144, 15);
      this._lblInsertPosition.TabIndex = 4;
      this._lblInsertPosition.Text = "Where to Insert Columns:";
      // 
      // _lblOwnerName
      // 
      this._lblOwnerName.AutoSize = true;
      this._lblOwnerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this._lblOwnerName.ForeColor = System.Drawing.SystemColors.ControlText;
      this._lblOwnerName.Location = new System.Drawing.Point(66, 35);
      this._lblOwnerName.Name = "_lblOwnerName";
      this._lblOwnerName.Size = new System.Drawing.Size(41, 13);
      this._lblOwnerName.TabIndex = 3;
      this._lblOwnerName.Text = "owner";
      // 
      // _lblOwner
      // 
      this._lblOwner.AutoSize = true;
      this._lblOwner.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this._lblOwner.ForeColor = System.Drawing.SystemColors.ControlText;
      this._lblOwner.Location = new System.Drawing.Point(11, 33);
      this._lblOwner.Name = "_lblOwner";
      this._lblOwner.Size = new System.Drawing.Size(49, 15);
      this._lblOwner.TabIndex = 2;
      this._lblOwner.Text = "Owner: ";
      // 
      // _lblLayout
      // 
      this._lblLayout.AutoSize = true;
      this._lblLayout.ForeColor = System.Drawing.SystemColors.ControlText;
      this._lblLayout.Location = new System.Drawing.Point(205, 16);
      this._lblLayout.Name = "_lblLayout";
      this._lblLayout.Size = new System.Drawing.Size(35, 13);
      this._lblLayout.TabIndex = 1;
      this._lblLayout.Text = "Columns";
      // 
      // _lblInsert
      // 
      this._lblInsert.AutoSize = true;
      this._lblInsert.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this._lblInsert.ForeColor = System.Drawing.SystemColors.ControlText;
      this._lblInsert.Location = new System.Drawing.Point(11, 16);
      this._lblInsert.Name = "_lblInsert";
      this._lblInsert.Size = new System.Drawing.Size(180, 15);
      this._lblInsert.TabIndex = 0;
      this._lblInsert.Text = "Insert to current Columns setting: ";
      // 
      // _groupBoxConfig
      // 
      this._groupBoxConfig.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this._groupBoxConfig.BackColor = System.Drawing.SystemColors.Control;
      this._groupBoxConfig.Controls.Add(this._showDerivedColumns);
      this._groupBoxConfig.Controls.Add(this._gridSelector);
      this._groupBoxConfig.Controls.Add(this._btnHiddenUnGroupby);
      this._groupBoxConfig.Controls.Add(this._btnHiddenGoupby);
      this._groupBoxConfig.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this._groupBoxConfig.ForeColor = System.Drawing.SystemColors.ActiveCaption;
      this._groupBoxConfig.Location = new System.Drawing.Point(8, 90);
      this._groupBoxConfig.Name = "_groupBoxConfig";
      this._groupBoxConfig.Size = new System.Drawing.Size(494, 464);
      this._groupBoxConfig.TabIndex = 3;
      this._groupBoxConfig.TabStop = false;
      this._groupBoxConfig.Text = "Settings";
      // 
      // _gridSelector
      // 
      this._gridSelector.AllowDrop = true;
      this._gridSelector.LeftGridHeaderCaption = "Available Columns";
      this._gridSelector.LeftShowMultiColumns = false;
      this._gridSelector.Location = new System.Drawing.Point(10, 48);
      this._gridSelector.Name = "_gridSelector";
      this._gridSelector.RightGridHeaderCaption = "Inserted Columns";
      this._gridSelector.RightShowMultiColumns = false;
      this._gridSelector.Size = new System.Drawing.Size(472, 400);
      this._gridSelector.TabIndex = 51;
      // 
      // _btnHiddenUnGroupby
      // 
      this._btnHiddenUnGroupby.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
      this._btnHiddenUnGroupby.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.Control;
      this._btnHiddenUnGroupby.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.Control;
      this._btnHiddenUnGroupby.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this._btnHiddenUnGroupby.Image = global::MorganStanley.MSDesktop.Rambo.WinGridUtils.Properties.Resources.sort;
      this._btnHiddenUnGroupby.Location = new System.Drawing.Point(38, 19);
      this._btnHiddenUnGroupby.Name = "_btnHiddenUnGroupby";
      this._btnHiddenUnGroupby.Size = new System.Drawing.Size(22, 23);
      this._btnHiddenUnGroupby.TabIndex = 50;
      this._btnHiddenUnGroupby.UseVisualStyleBackColor = true;
      this._btnHiddenUnGroupby.Click += new System.EventHandler(this._btnHiddenUnGroupby_Click);
      // 
      // _btnHiddenGoupby
      // 
      this._btnHiddenGoupby.BackColor = System.Drawing.SystemColors.Control;
      this._btnHiddenGoupby.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlDarkDark;
      this._btnHiddenGoupby.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightSteelBlue;
      this._btnHiddenGoupby.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSteelBlue;
      this._btnHiddenGoupby.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this._btnHiddenGoupby.Image = global::MorganStanley.MSDesktop.Rambo.WinGridUtils.Properties.Resources.groupby;
      this._btnHiddenGoupby.Location = new System.Drawing.Point(10, 19);
      this._btnHiddenGoupby.Name = "_btnHiddenGoupby";
      this._btnHiddenGoupby.Size = new System.Drawing.Size(22, 23);
      this._btnHiddenGoupby.TabIndex = 49;
      this._btnHiddenGoupby.UseVisualStyleBackColor = false;
      this._btnHiddenGoupby.Click += new System.EventHandler(this._btnHiddenGoupby_Click);
      // 
      // _btnApply
      // 
      this._btnApply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this._btnApply.Location = new System.Drawing.Point(344, 643);
      this._btnApply.Name = "_btnApply";
      this._btnApply.Size = new System.Drawing.Size(72, 26);
      this._btnApply.TabIndex = 4;
      this._btnApply.Text = "Apply";
      this._btnApply.UseVisualStyleBackColor = true;
      this._btnApply.Click += new System.EventHandler(this._btnApply_Click);
      // 
      // _btnCancel
      // 
      this._btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this._btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this._btnCancel.Location = new System.Drawing.Point(430, 643);
      this._btnCancel.Name = "_btnCancel";
      this._btnCancel.Size = new System.Drawing.Size(67, 26);
      this._btnCancel.TabIndex = 5;
      this._btnCancel.Text = "Cancel";
      this._btnCancel.UseVisualStyleBackColor = true;
      // 
      // ultraGroupBoxDescription
      // 
      this.ultraGroupBoxDescription.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.Header3D;
      this.ultraGroupBoxDescription.Controls.Add(this.pnlRtfDescription);
      this.ultraGroupBoxDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F);
      appearance70.ForeColor = System.Drawing.SystemColors.ActiveCaption;
      this.ultraGroupBoxDescription.HeaderAppearance = appearance70;
      this.ultraGroupBoxDescription.Location = new System.Drawing.Point(8, 558);
      this.ultraGroupBoxDescription.Margin = new System.Windows.Forms.Padding(5);
      this.ultraGroupBoxDescription.Name = "ultraGroupBoxDescription";
      this.ultraGroupBoxDescription.Size = new System.Drawing.Size(491, 77);
      this.ultraGroupBoxDescription.TabIndex = 7;
      this.ultraGroupBoxDescription.Text = "Description";
      // 
      // pnlRtfDescription
      // 
      this.pnlRtfDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pnlRtfDescription.Controls.Add(this.rtfDescription);
      this.pnlRtfDescription.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnlRtfDescription.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
      this.pnlRtfDescription.Location = new System.Drawing.Point(1, 17);
      this.pnlRtfDescription.Name = "pnlRtfDescription";
      this.pnlRtfDescription.Size = new System.Drawing.Size(489, 59);
      this.pnlRtfDescription.TabIndex = 0;
      // 
      // rtfDescription
      // 
      this.rtfDescription.BackColor = System.Drawing.SystemColors.Window;
      this.rtfDescription.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.rtfDescription.Dock = System.Windows.Forms.DockStyle.Fill;
      this.rtfDescription.ForeColor = System.Drawing.Color.Black;
      this.rtfDescription.Location = new System.Drawing.Point(0, 0);
      this.rtfDescription.Name = "rtfDescription";
      this.rtfDescription.ReadOnly = true;
      this.rtfDescription.Size = new System.Drawing.Size(487, 57);
      this.rtfDescription.TabIndex = 0;
      this.rtfDescription.Text = "";
      // 
      // _showDerivedColumns
      // 
      this._showDerivedColumns.AutoSize = true;
      this._showDerivedColumns.ForeColor = System.Drawing.Color.Black;
      this._showDerivedColumns.Location = new System.Drawing.Point(69, 23);
      this._showDerivedColumns.Name = "_showDerivedColumns";
      this._showDerivedColumns.Size = new System.Drawing.Size(136, 17);
      this._showDerivedColumns.TabIndex = 52;
      this._showDerivedColumns.Text = "Show Derived Columns";
      this._showDerivedColumns.UseVisualStyleBackColor = true;
      this._showDerivedColumns.CheckedChanged += new System.EventHandler(this._showDerivedColumns_CheckedChanged);
      // 
      // DlgInsertColumns
      // 
      this.AcceptButton = this._btnApply;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this._btnCancel;
      this.ClientSize = new System.Drawing.Size(510, 676);
      this.Controls.Add(this.ultraGroupBoxDescription);
      this.Controls.Add(this._btnCancel);
      this.Controls.Add(this._groupBoxConfig);
      this.Controls.Add(this._groupBoxLayout);
      this.Controls.Add(this._btnApply);
      this.Name = "DlgInsertColumns";
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.Text = "Insert Columns";
      this._groupBoxLayout.ResumeLayout(false);
      this._groupBoxLayout.PerformLayout();
      this._groupBoxConfig.ResumeLayout(false);
      this._groupBoxConfig.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBoxDescription)).EndInit();
      this.ultraGroupBoxDescription.ResumeLayout(false);
      this.pnlRtfDescription.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.GroupBox _groupBoxLayout;
    private System.Windows.Forms.Label _lblLayout;
    private System.Windows.Forms.Label _lblInsert;
    private System.Windows.Forms.GroupBox _groupBoxConfig;
    private System.Windows.Forms.Button _btnApply;
    private System.Windows.Forms.Button _btnCancel;
    private System.Windows.Forms.Label _lblOwnerName;
    private System.Windows.Forms.Label _lblOwner;
    private System.Windows.Forms.Label _lblInsertPosition;
    private System.Windows.Forms.RadioButton _radioBtnAfter;
    private System.Windows.Forms.RadioButton _radioBtnBefore;
    private System.Windows.Forms.Button _btnHiddenUnGroupby;
    private System.Windows.Forms.Button _btnHiddenGoupby;
    private Infragistics.Win.Misc.UltraGroupBox ultraGroupBoxDescription;
    private System.Windows.Forms.Panel pnlRtfDescription;
    private System.Windows.Forms.RichTextBox rtfDescription;
    private AvailableItemsSelector _gridSelector;
    private System.Windows.Forms.CheckBox _showDerivedColumns;
  }
}