﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using MorganStanley.MSDesktop.Rambo.WinGridUtils.GridLayoutEditor.DataModel;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  public partial class AvailableItemsSelector : UserControl, IAvailableItemSelector
  {
    #region Constants

    private const string KEY_COLUMNCATEGORY = "Category";
    private const string KEY_COLUMNNAME = "ColumnName";

    #endregion

    #region Fields

    protected DragDropHighlightDrawFilter _dragDropHighlightDrawFilter;
    private IDictionary<string, int> _pressCount;
    private IColumnFilterProvider _columnFilterProvider;
    #endregion

    #region Constructors

    /// <summary>
    /// Initialises a new instance of the CntrlFormatDetails User Control.
    /// </summary>
    public AvailableItemsSelector()
    {
      InitializeComponent();
      Controller = new AvailableItemsSelectorController(this);
      _pressCount = new Dictionary<string, int>();
      _rightultraGrid.DrawFilter = _dragDropHighlightDrawFilter = new DragDropHighlightDrawFilter(null);
      _dragDropHighlightDrawFilter.Invalidate += _dragDropHighlightDrawFilter_Invalidate;
    }

    #endregion

    #region Instance Properties

    public AvailableItemsSelectorController Controller { get; set; }
    public string LeftGridHeaderCaption { get; set; }
    public bool LeftShowMultiColumns { get; set; }

    [Browsable(false)]
    public UltraGrid LeftUltraGrid
    {
      get { return _leftultraGrid; }
    }

    public string RightGridHeaderCaption { get; set; }
    public bool RightShowMultiColumns { get; set; }

    [Browsable(false)]
    public UltraGrid RightUltraGrid
    {
      get { return _rightultraGrid; }
    }




    public IColumnFilterProvider ColumnFilterProvider
    {
      get { return _columnFilterProvider; }
      set
      {
        _columnFilterProvider = value;

        if(_dragDropHighlightDrawFilter!=null)
        {
          _dragDropHighlightDrawFilter.Invalidate  -= _dragDropHighlightDrawFilter_Invalidate;
        }

        _leftultraGrid.DrawFilter = _columnFilterProvider != null?
        _leftultraGrid.DrawFilter = new ColumnSelectorDrawFilter(_columnFilterProvider):null;
        _rightultraGrid.DrawFilter = _dragDropHighlightDrawFilter = new DragDropHighlightDrawFilter(_columnFilterProvider);

        _dragDropHighlightDrawFilter.Invalidate += _dragDropHighlightDrawFilter_Invalidate;
      }
    }

    #endregion

    #region Instance Methods

    /// <summary>
    /// Clears the drop highlight.
    /// </summary>
    public void ClearDropHighlight()
    {
      _dragDropHighlightDrawFilter.ClearDropHighlight();
    }

    /// <summary>
    /// Fires the set descriptions event.
    /// </summary>
    /// <param name="wrapper_">The wrapper_.</param>
    public void FireSetDescriptions(IClassWrapper wrapper_)
    {
      EventHandler<SetDescriptionEventArgs> temp = SetDescriptions;
      if (temp != null)
      {
        temp(this, new SetDescriptionEventArgs(wrapper_));
      }
    }

    protected override void OnLoad(EventArgs e)
    {
      BeginInvoke(new GridMethodDelegate(EditLayoutHelper.SetFocusToFilterRow), new object[] {_leftultraGrid});
      base.OnLoad(e);
    }

    private void AfterSelectionChange(UltraGrid grid_, bool left_)
    {
      if (grid_.Selected.Rows.Count == 1)
      {
        var colWrapper = grid_.Selected.Rows[0].ListObject as IClassWrapper;
        if (colWrapper == null) return;
        FireSetDescriptions(colWrapper);
        EnableNavButtons(left_, left_, left_, !left_);
      }
      else
      {
        FireSetDescriptions(null);
        if (_leftultraGrid.Selected.Rows.Count <= 0 && _rightultraGrid.Selected.Rows.Count <= 0)
          EnableNavButtons(false, false, false, false);
        else
        {
          EnableNavButtons(left_, left_, left_, !left_);
        }
      }

      UnSelectAllRows(left_);
    }

    #endregion

    #region Event Handling

    /// <summary>
    /// Add items from right ultragrid to left ultragrid
    /// </summary>
    public void BtnAddToLeft_Click(object sender, EventArgs e)
    {
      Controller.AddItemToLeftRight(true, _leftultraGrid.Rows.IsGroupByRows);
    }

    /// <summary>
    /// Add items from left ultragrid to right ultragrid
    /// </summary>
    public void BtnAddToRight_Click(object sender, EventArgs e)
    {
      Controller.AddItemToLeftRight(false, false);
    }

    /// <summary>
    /// Move the item(s) in right ultragrid down
    /// </summary>
    public void BtnMoveRightDown_Click(object sender, EventArgs e)
    {
      Controller.MoveRightUpDown(true);
      //_btnMoveRightDown.Focus();
    }

    /// <summary>
    /// Move the item(s) in right ultragrid up
    /// </summary>
    public void BtnMoveRightUp_Click(object sender, EventArgs e)
    {
      Controller.MoveRightUpDown(false);
      //_btnMoveRightUp.Focus();
    }

    /// <summary>
    /// Handles the DragDrop event of the LeftultraGrid control.
    /// </summary>
    public void LeftultraGrid_DragDrop(object sender, DragEventArgs e)
    {
      Controller.UltraGridDragDrop(true, false, -1, _leftultraGrid.Rows.IsGroupByRows);
    }

    public void RightultraGrid_DragDrop(object sender, DragEventArgs e)
    {
      // Get the position on the grid where the dragged row(s) are to be dropped.
      //get the grid coordinates of the row (the drop zone)
      Point pointInGridCoords = _rightultraGrid.PointToClient(new Point(e.X, e.Y));
      UIElement uieOver =
        _rightultraGrid.DisplayLayout.UIElement.ElementFromPoint(
          pointInGridCoords);

      //get the row that is the drop zone/or where the dragged row is to be dropped
      var ugrOver = uieOver.GetContext(typeof (UltraGridRow), true) as UltraGridRow;
      bool selfdrag = false;
      int dropIndex = 0;
      if (ugrOver != null)
      {
        dropIndex = ugrOver.Index == -1
                      ? (pointInGridCoords.Y < 55 ? 0 : _rightultraGrid.Rows.Count)
                      : ugrOver.Index; //index/position of drop zone in grid
        selfdrag = _rightultraGrid.Selected.Rows.Count > 0;
      }

      Controller.UltraGridDragDrop(false, selfdrag, dropIndex, false);
      ClearDropHighlight();
    }

    private void _dragDropHighlightDrawFilter_Invalidate(object sender, EventArgs e)
    {

      _leftultraGrid.Invalidate();
      _rightultraGrid.Invalidate();
    }

    private void _leftultraGrid_SelectionDrag(object sender, CancelEventArgs e)
    {
      var grid = sender as UltraGrid;
      if (grid == null) return;

      grid.DoDragDrop(grid.Selected.Rows, DragDropEffects.Move);
    }

    /// <summary>
    /// Handles the AfterSelectChange event of the LeftultraGrid control.
    /// </summary>
    private void LeftultraGrid_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
    {
      var grid = sender as UltraGrid;

      if (grid == null) return;
      AfterSelectionChange(grid, false);
    }


    private void LeftultraGrid_InitializeLayout(object sender, InitializeLayoutEventArgs e)
    {
      var ultraGrid = sender as UltraGrid;
      if (ultraGrid == null) return;

      InitializeColumns(ultraGrid, LeftShowMultiColumns);
      ultraGrid.DisplayLayout.Bands[0].Columns[KEY_COLUMNNAME].CellClickAction = CellClickAction.RowSelect;
      ultraGrid.DisplayLayout.Override.GroupByRowDescriptionMask = "[value]: [count] item(s).";
      ultraGrid.DisplayLayout.Bands[0].Columns[0].SortIndicator = SortIndicator.Ascending;
      ultraGrid.DisplayLayout.Bands[0].Columns[KEY_COLUMNNAME].Header.Caption = LeftGridHeaderCaption;
      ultraGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;
      //Set the group by rule for Category Column
      UltraGridColumn categoryCol = ultraGrid.DisplayLayout.Bands[0].Columns[KEY_COLUMNCATEGORY];
      categoryCol.GroupByComparer = new CategoryOrderComparer();
    }

    private void RightultraGrid_AfterSelectChange(object sender, AfterSelectChangeEventArgs e)
    {
      var grid = sender as UltraGrid;
      if (grid == null) return;
      AfterSelectionChange(grid, true);
    }

    private void RightutraGrid_InitializeLayout(object sender, InitializeLayoutEventArgs e)
    {
      var ultraGrid = sender as UltraGrid;
      if (ultraGrid == null) return;
      InitializeColumns(ultraGrid, RightShowMultiColumns);
      ultraGrid.DisplayLayout.Bands[0].Columns[KEY_COLUMNNAME].CellClickAction = CellClickAction.RowSelect;
      ultraGrid.DisplayLayout.Bands[0].Columns[KEY_COLUMNNAME].SortIndicator = SortIndicator.Disabled;
      ultraGrid.DisplayLayout.Bands[0].Columns[KEY_COLUMNNAME].Header.Caption = RightGridHeaderCaption;
      ultraGrid.DisplayLayout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;

    }

    private void UltraGrid_DragLeave(object sender, EventArgs e)
    {
      ClearDropHighlight();
    }

    

    private void UltraGrid_DragOver(object sender, DragEventArgs e)
    {
      e.Effect = DragDropEffects.Move;
      var grid = sender as UltraGrid;
      if (grid == null || grid.Rows.Count == 0) return;


      Point pointInGridCoords = grid.PointToClient(new Point(e.X, e.Y));
      if (pointInGridCoords.Y < 20)
        // Scroll up.
        grid.ActiveRowScrollRegion.Scroll(RowScrollAction.LineUp);
      else if (pointInGridCoords.Y > grid.Height - 20)
        // Scroll down.
        grid.ActiveRowScrollRegion.Scroll(RowScrollAction.LineDown);

      //Set the drop Highlight for the row we are dragging to
      UIElement uieOver =
        grid.DisplayLayout.UIElement.ElementFromPoint(pointInGridCoords);
      var ugrOver = uieOver.GetContext(typeof (UltraGridRow), true) as UltraGridRow;
      if (ugrOver != null)
      {
        int dropIndex = ugrOver.Index == -1
                      ? (pointInGridCoords.Y < 55 ? 0 : grid.Rows.Count - 1)
                      : ugrOver.Index;
        _dragDropHighlightDrawFilter.SetDropHighlightNode(grid.Rows[dropIndex], pointInGridCoords);
      } 
    }

    private void UltraGrid_Enter(object sender, EventArgs e)
    {
      //Create a new dictionary for pressed key and count when we enter the grid
      IDictionary<string, int> hiddenPressCount = new Dictionary<string, int>();
      _pressCount = hiddenPressCount;
    }

    private void UltraGrid_KeyPress(object sender, KeyPressEventArgs e)
    {
      var grid = sender as UltraGrid;
      if (grid == null) return;
      //If we are press the key for filter row, return.
      if (grid.Rows.FilterRow.Activated) return;

      char currentKey = e.KeyChar;
      var lower = new string(new[] {currentKey});
      string upper = lower.ToUpper();
      var matchRows = new ArrayList();

      foreach (UltraGridRow row in grid.Rows)
      {
        var col = (IClassWrapper) row.ListObject;
        if (col != null && (col.ColumnName.StartsWith(lower) || col.ColumnName.StartsWith(upper)))
        {
          EditLayoutHelper.UnselectAllRows(grid);
          matchRows.Add(row);
        }
      }

      //if can not find any match, return
      if (matchRows.Count == 0) return;

      //press the same key to search for the next apperence
      if (_pressCount.ContainsKey(lower))
      {
        int previousCount = _pressCount[lower];
        _pressCount[lower] = previousCount + 1;
      }
      else //We press a new key to start another search
      {
        _pressCount.Clear();
        _pressCount.Add(lower, 0);
      }

      UltraGridRow selectRow;
      //We are at the end of the search
      if (_pressCount[lower] > matchRows.Count - 1)
      {
        _pressCount[lower] = 0;
        selectRow = (UltraGridRow) matchRows[0];
      }
      else
      {
        selectRow = (UltraGridRow) matchRows[_pressCount[lower]];
      }

      selectRow.Selected = true;
      grid.ActiveRowScrollRegion.ScrollRowIntoView(selectRow);
    }

    #endregion

    #region Event Declarations

    public event EventHandler<SetDescriptionEventArgs> SetDescriptions;

    #endregion

    #region IAvailableItemSelector Members

    public void Groupby(bool left_, string columnKey_)
    {
      if (left_)
      {
        _leftultraGrid.DisplayLayout.Bands[0].SortedColumns.Add(columnKey_, false, true);
      }
      else
      {
        _rightultraGrid.DisplayLayout.Bands[0].SortedColumns.Add(columnKey_, false, true);
      }
    }

    public void UnGroupby(bool left_, string columnKey_)
    {
      if (left_)
      {
        _leftultraGrid.DisplayLayout.Bands[0].SortedColumns.Clear();
        _leftultraGrid.DisplayLayout.Bands[0].Columns[columnKey_].SortIndicator = SortIndicator.Ascending;
      }
      else
      {
        _rightultraGrid.DisplayLayout.Bands[0].SortedColumns.Clear();
        _rightultraGrid.DisplayLayout.Bands[0].Columns[columnKey_].SortIndicator = SortIndicator.Ascending;
      }
    }

    public void SortNameColumn(bool left_)
    {
      if (left_)
      {
        _leftultraGrid.DisplayLayout.Bands[0].Columns[KEY_COLUMNNAME].SortIndicator = SortIndicator.Ascending;
      }
      else
      {
        _rightultraGrid.DisplayLayout.Bands[0].Columns[KEY_COLUMNNAME].SortIndicator = SortIndicator.Ascending;
      }
    }

    public void ScrollRowIntoView(bool left_)
    {
      if (left_)
      {
        if (_leftultraGrid.Selected.Rows.Count > 0)
          _leftultraGrid.ActiveRowScrollRegion.ScrollRowIntoView(_leftultraGrid.Selected.Rows[0]);
      }
      else
      {
        if (_rightultraGrid.Selected.Rows.Count > 0)
          _rightultraGrid.ActiveRowScrollRegion.ScrollRowIntoView(_rightultraGrid.Selected.Rows[0]);
      }
    }

    public void UnSelectAllRows(bool left_)
    {
      EditLayoutHelper.UnselectAllRows(left_ ? _leftultraGrid : _rightultraGrid);
    }

    public void SelectAllRows(bool left_)
    {
      EditLayoutHelper.SelectAllRows(left_ ? _leftultraGrid : _rightultraGrid);
    }

    public void SelectRows(bool left_, IList<IClassWrapper> wrappedColumns_)
    {
      EditLayoutHelper.SelectRows(left_ ? _leftultraGrid : _rightultraGrid, wrappedColumns_);
    }

    public void ReGroupGrid(bool left_)
    {
      if (left_)
      {
        if (_leftultraGrid.Rows.IsGroupByRows)
        {
          _leftultraGrid.DisplayLayout.Bands[0].SortedColumns.Clear();
          _leftultraGrid.DisplayLayout.Bands[0].SortedColumns.Add(KEY_COLUMNCATEGORY, false, true);
        }
      }
      else
      {
        if (_rightultraGrid.Rows.IsGroupByRows)
        {
          _rightultraGrid.DisplayLayout.Bands[0].SortedColumns.Clear();
          _rightultraGrid.DisplayLayout.Bands[0].SortedColumns.Add(KEY_COLUMNCATEGORY, false, true);
        }
      }
    }

    public void SetDataSource<T>(BindingList<T> leftDataSource_, BindingList<T> rightDataSource_)
    {
      _leftultraGrid.DataSource = leftDataSource_;
      _rightultraGrid.DataSource = rightDataSource_;
    }

    public void SetSelectedRows(bool left_)
    {
      EditLayoutHelper.SetSelectedRows(left_ ? _leftultraGrid : _rightultraGrid);
    }

    public T[] GetSelectedRows<T>(bool left_) where T : IClassWrapper
    {
      if (left_)
      {
        return EditLayoutHelper.GetSelectedRows<T>(_leftultraGrid);
      }
      return EditLayoutHelper.GetSelectedRows<T>(_rightultraGrid);
    }

    /// <summary>
    /// Helper method to enable/disable navigation buttons.
    /// </summary>
    public void EnableNavButtons(bool up_, bool down_, bool left_, bool right_)
    {
      _btnMoveRightUp.Enabled = up_;
      _btnMoveRightDown.Enabled = down_;
      _btnAddToLeft.Enabled = left_;
      _btnAddToRight.Enabled = right_;
    }

    #endregion

    #region Static Methods

    /// <summary>
    /// Initializes the columns.Display only columName column if showMultiColumns set to false.
    /// </summary>
    /// <param name="grid_">The grid_.</param>
    private static void InitializeColumns(UltraGrid grid_, bool showMultiColumns_)
    {
      ColumnsCollection columns = grid_.DisplayLayout.Bands[0].Columns;
      if (!showMultiColumns_)
      {
        foreach (UltraGridColumn column in columns)
        {
          if (column.Key != KEY_COLUMNNAME)
          {
            column.Hidden = true;
          }
        }
      }
      else //Just Hide category column
      {
        columns[KEY_COLUMNCATEGORY].Hidden = true;
      }
    }

    #endregion

    #region Nested type: GridMethodDelegate

    private delegate void GridMethodDelegate(UltraGrid grid_);

    #endregion
  }
}