namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  partial class DlgSaveAs
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this._layoutNameTextBox = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this._cancelButton = new System.Windows.Forms.Button();
      this._saveButton = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // _layoutNameTextBox
      // 
      this._layoutNameTextBox.Location = new System.Drawing.Point(86, 17);
      this._layoutNameTextBox.Name = "_layoutNameTextBox";
      this._layoutNameTextBox.Size = new System.Drawing.Size(252, 20);
      this._layoutNameTextBox.TabIndex = 2;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(3, 20);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(73, 13);
      this.label1.TabIndex = 3;
      this.label1.Text = "Columns Name:";
      // 
      // _cancelButton
      // 
      this._cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this._cancelButton.Location = new System.Drawing.Point(263, 43);
      this._cancelButton.Name = "_cancelButton";
      this._cancelButton.Size = new System.Drawing.Size(75, 23);
      this._cancelButton.TabIndex = 4;
      this._cancelButton.Text = "Cancel";
      this._cancelButton.UseVisualStyleBackColor = true;
      // 
      // _saveButton
      // 
      this._saveButton.Location = new System.Drawing.Point(182, 43);
      this._saveButton.Name = "_saveButton";
      this._saveButton.Size = new System.Drawing.Size(75, 23);
      this._saveButton.TabIndex = 5;
      this._saveButton.Text = "Save";
      this._saveButton.UseVisualStyleBackColor = true;
      this._saveButton.Click += new System.EventHandler(this._saveButton_Click);
      // 
      // DlgSaveAs
      // 
      this.AcceptButton = this._saveButton;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this._cancelButton;
      this.ClientSize = new System.Drawing.Size(351, 76);
      this.Controls.Add(this._saveButton);
      this.Controls.Add(this._cancelButton);
      this.Controls.Add(this.label1);
      this.Controls.Add(this._layoutNameTextBox);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "DlgSaveAs";
      this.ShowIcon = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Save As ...";
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.TextBox _layoutNameTextBox;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Button _cancelButton;
    private System.Windows.Forms.Button _saveButton;
  }
}