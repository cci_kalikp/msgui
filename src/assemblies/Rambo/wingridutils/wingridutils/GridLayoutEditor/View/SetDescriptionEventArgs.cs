﻿using System;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  public class SetDescriptionEventArgs : EventArgs
  {
    #region Constructors

    public SetDescriptionEventArgs(IClassWrapper wrapper_)
    {
      Wrapper = wrapper_;
    }

    #endregion

    #region Instance Properties

    public IClassWrapper Wrapper { get; private set; }

    #endregion
  }
}
