﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  public interface IAvailableItemSelector
  {
    event EventHandler<SetDescriptionEventArgs> SetDescriptions;
    void EnableNavButtons(bool up_, bool down_, bool left_, bool right_);
    void ScrollRowIntoView(bool left_);
    void UnSelectAllRows(bool left_);
    void SelectAllRows(bool left_);
    void SelectRows(bool left_, IList<IClassWrapper> wrappedColumns_);
    void ReGroupGrid(bool left_);
    void SetDataSource<T>(BindingList<T> leftDataSource_, BindingList<T> rightDataSource_);
    void SetSelectedRows(bool left_);
    T[] GetSelectedRows<T>(bool left_) where T:IClassWrapper;
    void Groupby(bool left_, string columnName_);
    void UnGroupby(bool left_, string columnName_);
    void SortNameColumn(bool left_);
  }
}
