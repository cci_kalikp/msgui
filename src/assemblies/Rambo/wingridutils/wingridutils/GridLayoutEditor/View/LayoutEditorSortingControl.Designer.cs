﻿using System;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  partial class LayoutEditorSortingControl
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this._chkAbsolute = new System.Windows.Forms.CheckBox();
      this.grpSelectAs = new System.Windows.Forms.GroupBox();
      this._rdoDesc = new System.Windows.Forms.RadioButton();
      this._rdoAsc = new System.Windows.Forms.RadioButton();
      this.panel1 = new System.Windows.Forms.Panel();
      this._availItemsSelectorControl = new MorganStanley.MSDesktop.Rambo.WinGridUtils.AvailableItemsSelector();
      this.grpSelectAs.SuspendLayout();
      this.panel1.SuspendLayout();
      this.SuspendLayout();
      // 
      // _chkAbsolute
      // 
      this._chkAbsolute.Location = new System.Drawing.Point(11, 87);
      this._chkAbsolute.Name = "_chkAbsolute";
      this._chkAbsolute.Size = new System.Drawing.Size(74, 22);
      this._chkAbsolute.TabIndex = 36;
      this._chkAbsolute.Text = "Absolute";
      this._chkAbsolute.Click += new System.EventHandler(this._chkAbsolute_Clicked);
      // 
      // grpSelectAs
      // 
      this.grpSelectAs.Controls.Add(this._rdoDesc);
      this.grpSelectAs.Controls.Add(this._rdoAsc);
      this.grpSelectAs.Location = new System.Drawing.Point(3, 3);
      this.grpSelectAs.Name = "grpSelectAs";
      this.grpSelectAs.Size = new System.Drawing.Size(88, 80);
      this.grpSelectAs.TabIndex = 35;
      this.grpSelectAs.TabStop = false;
      this.grpSelectAs.Text = "Select as:";
      // 
      // _rdoDesc
      // 
      this._rdoDesc.Location = new System.Drawing.Point(8, 48);
      this._rdoDesc.Name = "_rdoDesc";
      this._rdoDesc.Size = new System.Drawing.Size(92, 18);
      this._rdoDesc.TabIndex = 50;
      this._rdoDesc.Text = "Descending";
      this._rdoDesc.Click += new System.EventHandler(this._rdoDescAsc_Clicked);
      // 
      // _rdoAsc
      // 
      this._rdoAsc.Checked = true;
      this._rdoAsc.Location = new System.Drawing.Point(8, 24);
      this._rdoAsc.Name = "_rdoAsc";
      this._rdoAsc.Size = new System.Drawing.Size(76, 18);
      this._rdoAsc.TabIndex = 49;
      this._rdoAsc.TabStop = true;
      this._rdoAsc.Text = "Ascending";
      this._rdoAsc.Click += new System.EventHandler(this._rdoDescAsc_Clicked);
      // 
      // panel1
      // 
      this.panel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
      this.panel1.BackColor = System.Drawing.Color.Transparent;
      this.panel1.Controls.Add(this.grpSelectAs);
      this.panel1.Controls.Add(this._chkAbsolute);
      this.panel1.Location = new System.Drawing.Point(243, 0);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(92, 112);
      this.panel1.TabIndex = 37;
      // 
      // _availItemsSelectorControl
      // 
      this._availItemsSelectorControl.AllowDrop = true;
      this._availItemsSelectorControl.BackColor = System.Drawing.Color.Transparent;
      this._availItemsSelectorControl.Dock = System.Windows.Forms.DockStyle.Fill;
      this._availItemsSelectorControl.LeftGridHeaderCaption = "Available Columns";
      this._availItemsSelectorControl.LeftShowMultiColumns = false;
      this._availItemsSelectorControl.Location = new System.Drawing.Point(0, 0);
      this._availItemsSelectorControl.Name = "_availItemsSelectorControl";
      this._availItemsSelectorControl.RightGridHeaderCaption = "Sorted Columns";
      this._availItemsSelectorControl.RightShowMultiColumns = true;
      this._availItemsSelectorControl.Size = new System.Drawing.Size(578, 705);
      this._availItemsSelectorControl.TabIndex = 0;
      // 
      // LayoutEditorSortingControl
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.panel1);
      this.Controls.Add(this._availItemsSelectorControl);
      this.Name = "LayoutEditorSortingControl";
      this.Size = new System.Drawing.Size(578, 705);
      this.grpSelectAs.ResumeLayout(false);
      this.panel1.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private AvailableItemsSelector _availItemsSelectorControl;
    private System.Windows.Forms.CheckBox _chkAbsolute;
    private System.Windows.Forms.GroupBox grpSelectAs;
    private System.Windows.Forms.RadioButton _rdoDesc;
    private System.Windows.Forms.RadioButton _rdoAsc;
    private System.Windows.Forms.Panel panel1;
    public event EventHandler<SetDescriptionEventArgs> SetDescriptions;
  }
}
