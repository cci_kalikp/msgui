﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using MorganStanley.MSDesktop.Rambo.DataDictionary;
using MorganStanley.MSDesktop.Rambo.WinGridUtils.GridLayoutEditor.DataModel;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  public class InsertColumnsController
  {
    #region Readonly & Static Fields

    private readonly BindingList<IClassWrapper> _hiddenColumns = new BindingList<IClassWrapper>();
    private readonly BindingList<IClassWrapper> _insertColumns = new BindingList<IClassWrapper>();
    private readonly BindingList<IClassWrapper> _visibleColumns = new BindingList<IClassWrapper>();
    private readonly List<ColumnWrapper> _filteredOutColumns = new List<ColumnWrapper>();    
    private AvailableItemsSelectorDataSource<IClassWrapper> _datasource;
    #endregion

    #region Constructors



    public InsertColumnsController(IInsertColumnsForm insertColumnsForm_, AvailableItemsSelectorController availableItemsSelectorcontroller_)
    {
     
      InsertColumnsForm = insertColumnsForm_;
      _availableItemsSelectorcontroller = availableItemsSelectorcontroller_;
      _availableItemsSelectorcontroller.BeforeItemMovedToTheRightColumn +=
        _availableItemsSelectorcontroller_BeforeItemMovedToTheRightColumn;
    }

    void _availableItemsSelectorcontroller_BeforeItemMovedToTheRightColumn(object sender, ItemMovedToEventArgs e)
    {
      var column = e.ItemToBeMoved as ColumnWrapper;

      if (column == null || ShowDerivedColumns || ColumnFilterProvider == null) return;
     
      e.Cancel = ColumnFilterProvider.IsFilterable(column.ColumnLayout);
      if (e.Cancel)
      {
        _filteredOutColumns.Add(column);
      }

    }

    #endregion

    #region Instance Properties

    public IColumnFilterProvider ColumnFilterProvider { get; set; }

    public IDictionary ColumnCategories { get; set; }
    public IDictionary ColumnDescriptions { get; set; }
    public IDictionary TurboRiskGroups { get; set; }
    public GridLayout GridLayout { get; set; }
    public AvailableItemsSelectorDataSource<IClassWrapper> DataSource
    {
      get { return _datasource; }
      set { 
           _datasource = value;
           InsertColumnsForm.SetDataSource(_datasource);
      }
    }

    public IInsertColumnsForm InsertColumnsForm { get; set; }

    public int InsertIndex { get; set; }

    private bool _showDerivedColumns;
    private AvailableItemsSelectorController _availableItemsSelectorcontroller;

    public bool ShowDerivedColumns
    {
      get { return _showDerivedColumns; }
      set
      {
        _showDerivedColumns = value;
        ClassifyColumns(GridLayout);
      }
    }

    #endregion

    #region Instance Methods

    public void ApplyClick(bool before_)
    {
      //Update the order of the columns of layout
      UpdateColumnIndex(before_);
      try
      {
        //Only applying changes
        DSGridLoader.UpdateLocalCache(GridLayout);
      }
      catch (Exception)
      {
        MessageBox.Show(
                        "Unable to apply Columns changes currently.",
                        "Columns Apply Failed",
                        MessageBoxButtons.OK, MessageBoxIcon.Error,
                        MessageBoxDefaultButton.Button1);
      }
    }


    /// <summary>
    /// Classify hidden columns and visible columns according to a provided grid layout
    /// </summary>
    /// <param name="gridLayout_"></param>
    public void ClassifyColumns(GridLayout gridLayout_)
    {
      var colLayouts = new ColumnLayout[gridLayout_.Columns.Count];
      var list = new ArrayList(gridLayout_.Columns);
      list.Sort(new ColumnLayoutIndexComparer());
      list.CopyTo(colLayouts, 0);

      _hiddenColumns.Clear();
      _visibleColumns.Clear();
      _filteredOutColumns.Clear();

      foreach (ColumnLayout colLayout in colLayouts)
      {
        if (colLayout.Hidden)
        {

          bool doNotFilterColumnDueToDerivedField = ShowDerivedColumns || ColumnFilterProvider == null ||
                                           !ColumnFilterProvider.IsFilterable(colLayout);
          if (doNotFilterColumnDueToDerivedField)
            _hiddenColumns.Add(new ColumnWrapper(colLayout));
          else
            _filteredOutColumns.Add(new ColumnWrapper(colLayout));
        }
        else
        {
          _visibleColumns.Add(new ColumnWrapper(colLayout));
        }

        SetCategory(colLayout);
      }

      DataSource = new AvailableItemsSelectorDataSource<IClassWrapper>(_hiddenColumns, _insertColumns);
    }

    /// <summary>
    /// Helper method to set the category for each columnLayout
    /// </summary>
    private void SetCategory(ColumnLayout colLayout_)
    {
      if (ColumnCategories == null)
      {
        InsertColumnsForm.HideGroupUnGroupButtons();
      }
      else if (ColumnCategories.Contains(colLayout_.Key))
      {
        var category = ColumnCategories[colLayout_.Key] as Category;
        if (category != null)
        {
          colLayout_.FieldCategory = category.Name;
          colLayout_.FieldCategoryOrder = category.Order;
        }
      }
    }

    /// <summary>
    /// Update the column index of grid layout
    /// so that we can put the column in the right insert place.
    /// </summary>
    /// <summary>
    /// Update the column index of grid layout
    /// so that we can put the column in the right insert place.
    /// </summary>
    private void UpdateColumnIndex(bool before_)
    {
      int currentVisiblePostion;
      int insertPosition;
      if (before_)
      {
        insertPosition = InsertIndex;
        currentVisiblePostion = InsertIndex;
      }
      else
      {
        insertPosition = InsertIndex + 1;
        currentVisiblePostion = InsertIndex + 1;
      }

      BindingList<IClassWrapper> rightDataSource = DataSource.RightGridDataSource;
      int visiblePosition = insertPosition + rightDataSource.Count;


      //Update the insert columns' index in GridLayout;
      for (int i = 0; i < rightDataSource.Count; i++)
      {
        var col = rightDataSource[i];
        if (col == null) return;

        //Change the visibility of insert columns
        ((ColumnWrapper)col).ColumnLayout.Hidden = false;
        col.Index = insertPosition;
        insertPosition++;
      }

      //Update visible columns' indexes which are after the insert point
      for (int i = currentVisiblePostion; i < _visibleColumns.Count; i++)
      {
        _visibleColumns[i].Index = visiblePosition;
        visiblePosition++;
      }

      BindingList<IClassWrapper> leftDataSource = DataSource.LeftGridDataSource; //LeftUltraDataSource;
      //Update the hidden Columns' index
      for (int i = 0; i < leftDataSource.Count; i++) //_hiddenColumns.Count; i++)
      {
        IClassWrapper col = leftDataSource[i];
        if (col == null) return;

        //Keep the hidden columsn to be hided.
        ((ColumnWrapper)col).ColumnLayout.Hidden = true;
        col.Index = visiblePosition;
        visiblePosition++;
      }

      foreach (ColumnWrapper col in _filteredOutColumns)
      {
        //Keep the hidden columsn to be hided.
        col.ColumnLayout.Hidden = true;
        col.Index = visiblePosition;
        visiblePosition++;
      }
    }

    public void SetDescription(ColumnLayout columnLayout_)
    {
      string caption = columnLayout_.Caption;
      string category = columnLayout_.FieldCategory;
      string turboRiskCategory;
      if (TurboRiskGroups == null || TurboRiskGroups.Count == 0)
        turboRiskCategory = null;
      else
        turboRiskCategory = TurboRiskGroups.Contains(columnLayout_.Key) ? TurboRiskGroups[columnLayout_.Key].ToString() : "(None)";

      string description = "";
      if (ColumnDescriptions != null && ColumnDescriptions.Contains(columnLayout_.Key))
      {
        description = ColumnDescriptions[columnLayout_.Key].ToString();
      }
      if (description == string.Empty)
      {
        description = columnLayout_.Description;
      }
      InsertColumnsForm.DoSetDescription(caption, description,
                                          category, turboRiskCategory);
    }
    #endregion
  }
}
