﻿using System.Collections.Generic;
using System.ComponentModel;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  public class LayoutEditorSortingController : AvailableItemsSelectorController
  {
    #region Fields

    private IAvailItemsSelectorDataSource _datasource;

    #endregion

    #region Constructors

    public LayoutEditorSortingController(ILayoutEditorSortingControl availableItemSelector_)
      : base(availableItemSelector_)
    {
      LayoutSortingControl = availableItemSelector_;
    }

    #endregion

    #region Instance Properties

    public ILayoutEditorSortingControl LayoutSortingControl { get; set; }
    public override IAvailItemsSelectorDataSource DataSource
    {
      get { return _datasource; }
      set { base.DataSource = value; }
    }
    #endregion

    #region Instance Methods

    public override void AddItemToLeftRight(bool left_, bool isGrouped_)
    {   
      if(!left_)
      {
        LayoutSortingControl.SetCColLayoutProperties();
      }
      else
      {
        LayoutSortingControl.UnSetColLayoutProperties();
      }
      base.AddItemToLeftRight(left_, isGrouped_);
      ReIndexSortOrder();
    }

    public override void UltraGridDragDrop(bool left_, bool selfdrag_, int dropIndex_, bool isGrouped_)
    {
      if (!selfdrag_ && !left_)
      {
        LayoutSortingControl.SetCColLayoutProperties();
      }

      if (left_)
      {
        LayoutSortingControl.UnSetColLayoutProperties();
      }
      base.UltraGridDragDrop(left_, selfdrag_, dropIndex_, isGrouped_);
      ReIndexSortOrder();
    }

    /// <summary>
    /// Reorders the columns. It is used when self drag and drop rows in one ultraGrid
    /// </summary>
    /// <param name="left_">left or right grid</param>
    /// <param name="dropIndex_">The index indicate where we drop the row to.</param>
    public override void ReorderColumns(bool left_, int dropIndex_)
    {
      var datasource = (AvailableItemsSelectorDataSource<ColumnWrapper>) _datasource;
      BindingList<ColumnWrapper> bingdingList = left_ ? datasource.LeftGridDataSource : datasource.RightGridDataSource;
      ColumnWrapper[] selRows = LayoutSortingControl.GetSelectedRows<ColumnWrapper>(left_);
      EditLayoutHelper.ReorderColumns(bingdingList, selRows, dropIndex_);
      AvailItemsSelector.SetSelectedRows(left_);
      //grid_.Refresh();
    }


    /// <summary>
    /// Shifts the columns.It is used when shif up or down rows in one ultraGrid
    /// </summary>
    /// <param name="left_">Left or right grid</param>
    /// <param name="shiftDown_">if set to <c>true</c> [shift down_].</param>
    public override void ShiftColumns(bool left_, bool shiftDown_)
    {
      var datasource = (AvailableItemsSelectorDataSource<ColumnWrapper>) _datasource;
      BindingList<ColumnWrapper> bingdingList = left_ ? datasource.LeftGridDataSource : datasource.RightGridDataSource;
      ColumnWrapper[] selRows = LayoutSortingControl.GetSelectedRows<ColumnWrapper>(left_);
      EditLayoutHelper.ShiftColumns(bingdingList, selRows, shiftDown_);
      AvailItemsSelector.SetSelectedRows(left_);
    }


    /// <summary>
    /// Transfer the columns between two ultraGrid
    /// </summary>
    /// <param name="toLeftColumn_">the transfer direction of two grids</param>
    /// <param name="toLeftColumn_">whether it is from right ultragird to left ultragird</param>
    /// <param name="insertPostition_">where we want to insert the column</param>
    public override void TransferColumns(bool toLeftColumn_, int insertPostition_)
    {
      var datasource = (AvailableItemsSelectorDataSource<ColumnWrapper>) _datasource;

      var leftBindingList = datasource.LeftGridDataSource;
      var rightbindingList = datasource.RightGridDataSource;

      if (!toLeftColumn_)
      {
        ColumnWrapper[] selRows = LayoutSortingControl.GetSelectedRows<ColumnWrapper>(true);
        EditLayoutHelper.TransferColumns(leftBindingList, rightbindingList, selRows, insertPostition_,null);
        AvailItemsSelector.SetSelectedRows(false);
      }
      else
      {
        ColumnWrapper[] selRows = LayoutSortingControl.GetSelectedRows<ColumnWrapper>(false);
        EditLayoutHelper.TransferColumns(rightbindingList, leftBindingList, selRows, insertPostition_, null);
        AvailItemsSelector.SetSelectedRows(true);
      }
    }

    /// <summary>
    /// Apply sorted columns cached while initializing the grid layout.
    /// </summary>
    /// <param name="initialSortedColumns_">Columns to apply</param>
    public void ImportLiveSorting(IList<ColumnLayout> initialSortedColumns_)
    {
      if(initialSortedColumns_ == null)
        return;
      //Reset sorting for current selected criteria
      var datasource = (AvailableItemsSelectorDataSource<ColumnWrapper>)_datasource;
      if(datasource.RightGridDataSource != null)
      {
        foreach(ColumnWrapper selRow in datasource.RightGridDataSource)
        {
          selRow.ColumnLayout.SortOrder = -1;
          selRow.ColumnLayout.SortDirection = ColumnLayout.SortDirections.None;
          selRow.ColumnLayout.SortAbsolute = false;
        }
      }

      //Select all rows in right grid
      AvailItemsSelector.SelectAllRows(false);
      //Transfer selected rows to left grid
      TransferColumns(true, -1);
      //Select persisted sorted columns on left grid
      AvailItemsSelector.SelectRows(true, new List<IClassWrapper>(EditLayoutHelper.GetColumnWrapperEnumerator(initialSortedColumns_)));      
      //Transfer selected rows to right grid
      TransferColumns(false, -1);
      //Apply initial sorting settings on right source
      
      foreach (ColumnWrapper selRow in datasource.RightGridDataSource)
      {
        int index = initialSortedColumns_.IndexOf(selRow.ColumnLayout);
        if(index < 0) 
          continue;

        selRow.ColumnLayout.SortOrder = initialSortedColumns_[index].SortOrder;
        selRow.ColumnLayout.SortDirection = initialSortedColumns_[index].SortDirection;
        selRow.ColumnLayout.SortAbsolute = initialSortedColumns_[index].SortAbsolute;
      }
      //Unselect right grid
      LayoutSortingControl.UnSelectAllRows(false);
    }

    public void ReIndexSortOrder()
    {
      var rightDataSource = ((AvailableItemsSelectorDataSource<ColumnWrapper>) _datasource).RightGridDataSource;
      for (int i = 0; i < rightDataSource.Count; ++i)
      {
        var clw = rightDataSource[i];
        if (clw != null)
        {
          clw.ColumnLayout.SortOrder = i;
        }
      }
    }

    protected override void OnSetDataSource(IAvailItemsSelectorDataSource dataSource_)
    {
      var twoGridsDataSource = (AvailableItemsSelectorDataSource<ColumnWrapper>) dataSource_;
      if (twoGridsDataSource == null) return;
      _datasource = twoGridsDataSource;
      AvailItemsSelector.SetDataSource(twoGridsDataSource.LeftGridDataSource, twoGridsDataSource.RightGridDataSource);
    }

    #endregion
  }
}