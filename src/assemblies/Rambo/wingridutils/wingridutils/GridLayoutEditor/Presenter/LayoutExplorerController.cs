﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using JetBrains.Annotations;
using MorganStanley.MSDesktop.Rambo.AsyncUtils;
using MorganStanley.MSDesktop.Rambo.Desktop.Utils;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  public class LayoutExplorerController
  {
    #region Constants

    private const string ALL_LAYOUTS = "All";
    private const string COST_CENTER_LAYOUTS = "Cost Center";
    private const string OWNER_LAYOUTS = "Current User";

    #endregion

    #region Fields

    private LayoutExplorerStore _layoutExplorerStore;
    private BindingSource _comboBoxBindSource;
    private AsynchronizerBackgroundWorker _currentBackGroundWorker;
    private ICustomFilter _emailGroupFilter;
    private string m_ApplicationName;
    private ColumnLayoutCollection m_columns;
    private string m_GridName;
    private GridLayout m_initialGridLayout;
    private GridLayout m_newGridLayout;
    private bool m_stopClose;
    private string m_userName;
    private static readonly ILogger _log = new Logger(typeof(LayoutExplorerController));

    #endregion

    #region Constructors

    public LayoutExplorerController(ILayoutExplorer layoutExplorer_)
    {
      LayoutExplorer = layoutExplorer_;
    }

    #endregion

    #region Instance Properties

    /// <summary>
    /// Allows custom store providers to be provided
    /// </summary>
    internal LayoutExplorerStore Store
    {
      set
      {
        _layoutExplorerStore = value ?? new LayoutExplorerStore(m_initialGridLayout, UserName, LayoutExplorer);

        //Clients of DlgGridLayoutEditor don't have access to LayoutExplorer
        //So this need to be initialized here when they provide custom store providers
        _layoutExplorerStore.View = LayoutExplorer;
      }
      get { return _layoutExplorerStore; }
    }

    /// <summary>
    /// Determines whether the specified grouops_ has entitlement.
    /// </summary>
    public bool HasEntitlement(IDictionary<string,string> grouops_, string userName_)
    {
      return _layoutExplorerStore.HasEntitlement(grouops_, userName_);
    }

    public GridLayout InitialGridLayout
    {
      get { return m_initialGridLayout; }
      set
      {
        if (value != null)
        {
          m_initialGridLayout = value;
          //Cache initial sorted columns
          InitialSortedColumns = EditLayoutHelper.GetSortedColumns(m_initialGridLayout);
          m_ApplicationName = value.Application;
          m_GridName = value.GridName;
          m_columns = value.Columns;
          m_newGridLayout = new GridLayout
                              {
                                UpdateID = UserName,
                                Application = m_ApplicationName,
                                GridName = m_GridName,
                                Columns = new ColumnLayoutCollection()
                              };
        }
      }
    }

    public IList<ColumnLayout> InitialSortedColumns { get; set; }
    public ILayoutExplorer LayoutExplorer { get; set; }
    public ICustomFilter MailGroupFilter { get; set; }

    public bool StopClose
    {
      get { return m_stopClose; }
      set { m_stopClose = value; }
    }

    public string UserName
    {
      get { return m_userName; }
      set
      {
        if (m_newGridLayout != null)
        {
          m_newGridLayout.UpdateID = m_userName;
        }
        m_userName = value;
      }
    }

    #endregion

    #region Instance Methods

    /// <summary>
    /// Apply button event handler.
    /// </summary>
    public void BtnApplyClick()
    {
      //Only applying changes
      _layoutExplorerStore.UpdateLocalCache(GetGridLayout());
      m_stopClose = false;
    }

    public void BtnCancelClick()
    {
      if (_currentBackGroundWorker != null && _currentBackGroundWorker.IsBusy)
      {
        _currentBackGroundWorker.WorkerSupportsCancellation = true;
        _currentBackGroundWorker.CancelAsync();
        LayoutExplorer.ShowWarningMessage("Attempted to cancel operation.\n  Cancellation may have ocurred after data was sent to server.\n  Please resave any layout before continuing.","Cancelling...");
      }
    }

    public void BtnDeleteClick(GridLayout layout_)
    {
      if (layout_ == null) return;
      _layoutExplorerStore.Delete(layout_);
    }

    /// <summary>
    /// Begin share resource
    /// </summary>
    public void BeginShare(GridLayout layout_, IDictionary<string, string> shareGroups_, AsyncCallback callback_)
    {
      // Begin Grant Write Permission
      _layoutExplorerStore.BeginGrantEntitlement(layout_, shareGroups_, callback_);
    }

    /// <summary>
    /// End share resource
    /// </summary>
    public void EndShare(IAsyncResult ar_)
    {
      // Begin Grant Write Permission
      _layoutExplorerStore.EndGrantEntitlement(ar_);
    }

    /// <summary>
    /// Apply button event handler.
    /// </summary>
    public void BtnSaveClick()
    {
      GridLayout gridLayout = GetGridLayout();

      if (gridLayout != null)
      {
        _layoutExplorerStore.Save(gridLayout);
      }
      else
      {
        LayoutExplorer.ShowWarningMessage("Columns setting does not exist: click 'Save As' to create a new Columns setting", "Columns setting does not exist");
      }
    }

    public void FilterRadioButtonsStatusChanged(string radioName_)
    {
       //foreach
      //Performing a filtering action depending on what 
      //filtering criteria was selected
      if (radioName_ == ALL_LAYOUTS)
      {
        var allLayouts = _layoutExplorerStore.GetForTypes(ALL_LAYOUTS);
        BindLayoutCombo(allLayouts);
        LayoutExplorer.SetFilterStatusTextBox(allLayouts.Count + " Columns settings");
      }
      else if (radioName_ == COST_CENTER_LAYOUTS)
      {
        var costcenterLayouts = _layoutExplorerStore.GetForTypes(COST_CENTER_LAYOUTS);
        BindLayoutCombo(costcenterLayouts);
        LayoutExplorer.SetFilterStatusTextBox(costcenterLayouts.Count + " Columns settings in your cost center");
      }
      else if (radioName_ == OWNER_LAYOUTS)
      {
        var ownerLayouts = _layoutExplorerStore.GetForTypes(OWNER_LAYOUTS);
        if (ownerLayouts != null && ownerLayouts.Count > 0)
        {
          BindLayoutCombo(ownerLayouts);
          LayoutExplorer.SetFilterStatusTextBox(ownerLayouts.Count + " Columns settings that belong to you");
        }
        else
        {
          //TODO: move this to view logic
          MessageBox.Show("User '" + m_userName + "' owns no Columns settings");
          LayoutExplorer.SetCostCenterRadioButton(true);
        }
      }
      if (radioName_ == "Mailgroup:")
      {
        PerformFilteringByMailGroup();
        LayoutExplorer.EnableMailGroupButtons(true);
      }
      else
      {
        LayoutExplorer.EnableMailGroupButtons(false);
      }
    }

    /// <summary>
    /// Provides the currently adjusted grid/column formatting data in standard XML 
    /// representation.
    /// </summary>
    /// <returns></returns>
    public GridLayout GetGridLayout()
    {
      return LayoutExplorer.GetGridLayout();
    }

    public LayoutExplorerStore GetStore()
    {
      return _layoutExplorerStore;
    }

    public void InitController()
    {
      _comboBoxBindSource = new BindingSource();
      LayoutExplorer.DataSource = _comboBoxBindSource;

      //Default to LayoutExplorerStore if a store is not provided in the constructor
      if (_layoutExplorerStore == null)
      {
        _layoutExplorerStore = new LayoutExplorerStore(m_initialGridLayout, UserName, LayoutExplorer);  
      }

      _emailGroupFilter = new MailGroupFilter();
      MailGroupFilter = _emailGroupFilter;
      LayoutExplorer.SetDataSourceForMailGroup(_emailGroupFilter);
      _comboBoxBindSource.DataSource = null;
      _layoutExplorerStore.Load();
    }

    /// <summary>
    /// Updates the interface according to the supplied GridLayout object.
    /// </summary>
    public void LoadSelectedLayout(GridLayoutListItem listItem_)
    {
      //
      GridLayout gridLayout = listItem_ == null ? m_initialGridLayout : listItem_.GridLayout;
      if (listItem_ != null)
      {
        LayoutExplorer.SetCursor(false);
        LayoutExplorer.EnableControls(false);
        //This case is for initial layout we want to make sure 
        //that layout( apart from sorted columns) is not loaded from cache because 
        //changes made to the initlial layout(column sizes)
        //would be lost overwise. 
        //Sorted columns should be loaded from cache
        if (gridLayout.LayoutName.Equals(m_initialGridLayout.LayoutName))
        {
          //Override initial grid sorting with cached values
          GridLayout cachedGridLayout = _layoutExplorerStore.LoadGridLayout(m_initialGridLayout.Application, 
                                                                            m_initialGridLayout.GridName, 
                                                                            m_initialGridLayout.LayoutName, 
                                                                            false);

          IList<ColumnLayout> cachedSortedColumns = EditLayoutHelper.GetSortedColumns(cachedGridLayout);
          EditLayoutHelper.ApplySortingOnGrid(gridLayout, cachedSortedColumns);

          LayoutExplorer.SetEditableGridLayout(gridLayout);
          if (cachedGridLayout != null)
          {
            // Get entitlement for current layout
            BeginGetEntitlement(cachedGridLayout, false);
          }
          LayoutExplorer.SetCursor(true);
          LoadGridLayoutIntoListItem(gridLayout);
          LayoutExplorer.EnableControls(true);
        }
        else
        {
          _layoutExplorerStore.BeginLoadGridLayout(m_ApplicationName, 
                                                    m_GridName, 
                                                    gridLayout.LayoutName,
                                                    OnLayoutLoaded);
        }
      }
    }

    /// <summary>
    /// Begins the get entitlement for layout
    /// </summary>
    /// <param name="layout_">The layout_.</param>
    /// <param name="forceReload_">if force reload</param>
    public void BeginGetEntitlement([NotNull] GridLayout layout_, bool forceReload_)
    {
      if(forceReload_) _layoutExplorerStore.ClearCache();
      _layoutExplorerStore.BeginGetEntitlement(layout_, DoneGetEntitlement);
    }

    private void DoneGetEntitlement(IAsyncResult ar)
    {
      try
      {
        IDictionary<string, string> grouops = _layoutExplorerStore.EndGetEntitlement(ar);
        GridLayout layout = ar.AsyncState as GridLayout;
        LayoutExplorer.SetCurrentWriteGroup(grouops, layout);
      }
      catch (Exception e_)
      {
        _log.Error("DoneGetEntitlement",e_.Message,e_);
      }
    }

    public void PerformFilteringByMailGroup()
    {
      var m_allLayouts = _layoutExplorerStore.GetForTypes("All");
      if (m_allLayouts != null &&
          !string.IsNullOrEmpty(_emailGroupFilter.SearchCriteria))
      {
        _currentBackGroundWorker = new AsynchronizerBackgroundWorker();
        _currentBackGroundWorker.DoWork += _mailFilterBackgroundWorker_DoWork;
        _currentBackGroundWorker.RunWorkerCompleted += _mailFilterBackgroundWorker_RunWorkerCompleted;
        LayoutExplorer.EnableControls(false);
        _currentBackGroundWorker.RunWorkerAsync(m_allLayouts);
      }
    }

    public void RefreshButtonClick()
    {
      LayoutExplorer.EnableControls(false);
      _layoutExplorerStore.ClearStore();
      _layoutExplorerStore.ClearCache();
      _layoutExplorerStore.Load();
      LayoutExplorer.EnableControls(true);
    }

    public void SaveAsUltraButtonClick(string layoutName_)
    {
      _layoutExplorerStore.SaveAs(layoutName_);
    }

    /// <summary>
    /// Adds any columns that are in the intial layout but not in the supplied layout
    /// </summary>
    /// <param name="gridLayout_"></param>
    private void AddMissingColumns(GridLayout gridLayout_)
    {
      // Add columns in original grid that do not exist in the persisted layout to the 
      // hidden columns array.
      if (m_columns != null)
      {
        foreach (ColumnLayout columnLayout in m_columns)
        {
          if (!gridLayout_.Columns.Contains(columnLayout))
          {
            var newColLayout = columnLayout.Clone() as ColumnLayout;
            if (newColLayout != null)
            {
              newColLayout.Hidden = true;
              gridLayout_.Columns.Add(newColLayout);
            }
          }
        }
      }
      else if (gridLayout_.Columns != null)
      {
        m_columns = gridLayout_.Columns;
      }
    }

    /// <summary>
    /// Changes the binding source used by the cmbLayout and other controls - used to switch back
    /// and forth between user layouts and all layouts.
    /// </summary>
    /// <param name="list_"></param>
    private void BindLayoutCombo(ICollection<GridLayoutListItem> list_)
    {
      _comboBoxBindSource.DataSource = list_;
      LayoutExplorer.BindLayoutCombo(list_);
    }

    /// <summary>
    /// Modifies the lists with the fully specified GridLayout.  Adds any columns that may have
    /// not been persisted in the last save, but are available now.  
    /// </summary>
    /// <param name="gridLayout_"></param>
    private void LoadGridLayoutIntoListItem(GridLayout gridLayout_)
    {
      GridLayoutListItem listItem = _layoutExplorerStore.GetListItem(gridLayout_);
      if (listItem != null)
      {
        listItem.GridLayout = gridLayout_;
        listItem.IsLoaded = true;
        AddMissingColumns(listItem.GridLayout);
      }
    }

    /// <summary>
    /// Completes the loading of a layout by updating structures and bound controls
    /// </summary>
    /// <param name="result_"></param>
    private void OnLayoutLoaded(IAsyncResult result_)
    {
      if (LayoutExplorer.InvokeRequired)
      {
        LayoutExplorer.BeginInvoke(new AsyncCallback(OnLayoutLoaded), new Object[] {result_});
        return;
      }

      try
      {
        GridLayout gridLayout = _layoutExplorerStore.EndLoadGridLayout(result_);
        if (gridLayout != null)
        {
          LoadGridLayoutIntoListItem(gridLayout);
          LayoutExplorer.SetEditableGridLayout(gridLayout);
          //Get the entitlement for current layout.
          BeginGetEntitlement(gridLayout, false);
        }
      }
      finally
      {
        LayoutExplorer.SetCursor(true);
        LayoutExplorer.EnableControls(true);
      }
    }

    #endregion

    #region Event Handling

    private void _mailFilterBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
    {
      var worker = (AsynchronizerBackgroundWorker) sender;
      if (worker.CancellationPending)
      {
        e.Cancel = true;
      }
      else
      {
        e.Result = _emailGroupFilter.Filter(e.Argument as IList);
      }
    }

    private void _mailFilterBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
    {
      if (!e.Cancelled)
      {
        IList<GridLayoutListItem> filteredByMailList;
        if (e.Result != null)
        {
          filteredByMailList = (IList<GridLayoutListItem>) e.Result;
          if (filteredByMailList.Count >= 0)
          {
            BindLayoutCombo(filteredByMailList);
            LayoutExplorer.SetFilterStatusTextBox(String.Format("{0} column settings based on mailgroup '{1}'",
                                                                filteredByMailList.Count,
                                                                LayoutExplorer.GetMailGroupText()));
          }
        }
      }
      LayoutExplorer.EnableControls(true);
    }

    #endregion

    #region Nested type: SaveLayoutAsEventArg

    internal struct SaveLayoutAsEventArg
    {
      #region Fields

      public GridLayout CurrentLayout;
      public string NewLayoutName;
      public GridLayout NewLayout;

      #endregion
    }

    #endregion

    #region Nested type: VerificationSaveAsyncResult

    internal struct VerificationSaveAsyncResult
    {
      #region Fields

      public GridLayoutListItem ExistingLayoutItem;
      public bool IsExisting;
      public string LayoutName;

      #endregion
    }

    #endregion
  }
}