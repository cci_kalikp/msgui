﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  public class AvailableItemsSelectorController
  {
    #region Fields

    private IAvailItemsSelectorDataSource _datasource;
    public event EventHandler<ItemMovedToEventArgs> BeforeItemMovedToTheRightColumn;

    private void OnBeforeItemMovedToTheRightColumn(ItemMovedToEventArgs e_)
    {
      EventHandler<ItemMovedToEventArgs> handler = BeforeItemMovedToTheRightColumn;
      if (handler != null) handler(this, e_);
    }

    #endregion

    #region Constructors

    public AvailableItemsSelectorController(IAvailableItemSelector availableItemSelector_)
    {
      AvailItemsSelector = availableItemSelector_;
    }

    #endregion

    #region Instance Properties

    public IAvailableItemSelector AvailItemsSelector { get; set; }

    public virtual IAvailItemsSelectorDataSource DataSource
    {
      get { return _datasource; }
      set { OnSetDataSource(value); }
    }

    #endregion

    #region Instance Methods

    /// <summary>
    /// Adds the item to left grid
    /// </summary>
    public virtual void AddItemToLeftRight(bool left_,bool isGrouped_)
    {
      TransferColumns(left_, -1);
      AvailItemsSelector.ScrollRowIntoView(left_);
      if (isGrouped_)
      {
        AvailItemsSelector.ReGroupGrid(true);
      }
      if(left_)
      {
        AvailItemsSelector.SortNameColumn(true);
      }
    }

    /// <summary>
    /// Hanlde right grid the grid drag drop. 
    /// </summary>
    public virtual void UltraGridDragDrop(bool left_,bool selfdrag_, int dropIndex_,bool isGrouped_)
    {
      if (selfdrag_)
      {
        ReorderColumns(false, dropIndex_);
      }
      else
      {
        AvailItemsSelector.UnSelectAllRows(left_);
        TransferColumns(left_, dropIndex_);
      }

      if(isGrouped_)
      {
        AvailItemsSelector.ReGroupGrid(left_);
      }
      if(left_)
      {
        AvailItemsSelector.SortNameColumn(true);
      }
      AvailItemsSelector.ScrollRowIntoView(left_);
    }

    /// <summary>
    /// Reorders the columns. It is used when self drag and drop rows in one ultraGrid
    /// </summary>
    /// <param name="left_">left or right grid</param>
    /// <param name="dropIndex_">The index indicate where we drop the row to.</param>
    public virtual void ReorderColumns(bool left_, int dropIndex_)
    {
      var datasource = (AvailableItemsSelectorDataSource<IClassWrapper>) _datasource;
      BindingList<IClassWrapper> bingdingList = left_ ? datasource.LeftGridDataSource : datasource.RightGridDataSource;
      IClassWrapper[] selRows = AvailItemsSelector.GetSelectedRows<IClassWrapper>(left_);
      EditLayoutHelper.ReorderColumns(bingdingList, selRows, dropIndex_);
      AvailItemsSelector.SetSelectedRows(left_);
    }

    /// <summary>
    /// Shifts the columns.It is used when shif up or down rows in one ultraGrid
    /// </summary>
    /// <param name="left_">Left or right grid</param>
    /// <param name="shiftDown_">if set to <c>true</c> [shift down_].</param>
    public virtual void ShiftColumns(bool left_, bool shiftDown_)
    {
      var datasource = (AvailableItemsSelectorDataSource<IClassWrapper>) _datasource;
      BindingList<IClassWrapper> bingdingList = left_ ? datasource.LeftGridDataSource : datasource.RightGridDataSource;
      IClassWrapper[] selRows = AvailItemsSelector.GetSelectedRows<IClassWrapper>(left_);
      EditLayoutHelper.ShiftColumns(bingdingList, selRows, shiftDown_);
      AvailItemsSelector.SetSelectedRows(left_);
    }

    /// <summary>
    /// Transfer the columns between two ultraGrid
    /// </summary>
    /// <param name="toLeftColumn_">the transfer direction of two grids</param>
    /// <param name="toLeftColumn_">whether it is from right ultragird to left ultragird</param>
    /// <param name="insertPostition_">where we want to insert the column</param>
    public virtual void TransferColumns(bool toLeftColumn_, int insertPostition_)
    {
      var datasource = (AvailableItemsSelectorDataSource<IClassWrapper>) _datasource;

      var leftBindingList = datasource.LeftGridDataSource;
      var rightbindingList = datasource.RightGridDataSource;

      if (!toLeftColumn_)
      {
        IClassWrapper[] selRows = AvailItemsSelector.GetSelectedRows<IClassWrapper>(true);
        EditLayoutHelper.TransferColumns(leftBindingList, rightbindingList, selRows, insertPostition_, OnBeforeItemMovedToTheRightColumn);
        AvailItemsSelector.SetSelectedRows(false);
      }
      else
      {
        IClassWrapper[] selRows = AvailItemsSelector.GetSelectedRows<IClassWrapper>(false);
        EditLayoutHelper.TransferColumns(rightbindingList, leftBindingList, selRows, insertPostition_, OnBeforeItemMovedToTheRightColumn);
        AvailItemsSelector.SetSelectedRows(true);
      }
    }  

    /// <summary>
    /// Moves the row(s) in right grid up or down
    /// </summary>
    /// <param name="movedown_">if set to <c>true</c> [movedown_].</param>
    public void MoveRightUpDown(bool movedown_)
    {
      if (movedown_)
      {
        ShiftColumns(false, true);
      }
      else
      {
        ShiftColumns(false, false);
      }
      AvailItemsSelector.ScrollRowIntoView(false);
    }    

    /// <summary>
    /// When set availableItemSelector data source, set the data source
    /// of the two grids inside it as well
    /// </summary>
    /// <param name="dataSource_">The data source_.</param>
    protected virtual void OnSetDataSource(IAvailItemsSelectorDataSource dataSource_)
    {
      _datasource = dataSource_;
      var twoGridsDataSource = (AvailableItemsSelectorDataSource<IClassWrapper>) dataSource_;
      if (twoGridsDataSource == null) return;

      AvailItemsSelector.SetDataSource(twoGridsDataSource.LeftGridDataSource, twoGridsDataSource.RightGridDataSource);
    }

    #endregion
  }

  public class ItemMovedToEventArgs:CancelEventArgs
  {
    public ItemMovedToEventArgs(IClassWrapper classWrapper_)
    {
      ItemToBeMoved = classWrapper_;
    }

    public IClassWrapper ItemToBeMoved
    {
      get; set;
    }
  }
}