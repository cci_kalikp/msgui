﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MorganStanley.MSDesktop.Rambo.DataDictionary;
using System.Linq;
using MorganStanley.MSDesktop.Rambo.Desktop.Utils.Extensions;
using MorganStanley.MSDesktop.Rambo.WinGridUtils.GridLayoutEditor.DataModel;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  public class LayoutEditorController
  {
    #region Readonly & Static Fields

    private readonly BindingList<ColumnWrapper> _availableBindingList = new BindingList<ColumnWrapper>();
    private readonly BindingList<IClassWrapper> _hiddenBindingList = new BindingList<IClassWrapper>();
    private readonly BindingList<ColumnWrapper> _sortedBindingList = new BindingList<ColumnWrapper>();
    private readonly BindingList<IClassWrapper> _visibleBindingList = new BindingList<IClassWrapper>();

    #endregion

    #region Fields

    private GridLayout _layoutToEdit;

    #endregion

    #region Constructors

    public LayoutEditorController(ILayoutEditor layoutEditor_, AvailableItemsSelectorController availableItemsSelectorController_)
    {
      availableItemsSelectorController_.BeforeItemMovedToTheRightColumn += AvailableItemsSelectorControllerBeforeItemMovedToTheRightColumn;
      LayoutEditor = layoutEditor_;
    }

    void AvailableItemsSelectorControllerBeforeItemMovedToTheRightColumn(object sender_, ItemMovedToEventArgs e_)
    {
      var columnToBeMoved = e_.ItemToBeMoved as ColumnWrapper;
      if (columnToBeMoved == null || ShowDerivedColumns || ColumnFilterProvider == null) return;

      e_.Cancel = ColumnFilterProvider.IsFilterable(columnToBeMoved.ColumnLayout);
      columnToBeMoved.Hidden = true;
    }

    #endregion

    #region Instance Properties

    public IDictionary ColumnCategories { get; set; }
    public IDictionary ColumnDescriptions { get; set; }
    
    public IDictionary TurboRiskGroups { get; set; }
    public IColumnFilterProvider ColumnFilterProvider { get; set; }

    public GridLayout EditableLayout
    {
      set
      {
        if (value != null)
        {
          _layoutToEdit = EditLayoutHelper.CloneLayout(value, value.LayoutName);
          PopulateControls(_layoutToEdit);
        }
      }
    }

    public ILayoutEditor LayoutEditor { get; set; }

    public bool StopClose { get; set; }
    public string UserName { get; set; }
    public IList<string> VerifyColumnKeysList { get; set; }

    #endregion

    #region Instance Methods

    /// <summary>
    /// Provides the currently adjusted grid/column formatting data in standard XML 
    /// representation.
    /// </summary>
    /// <returns></returns>
    public GridLayout GetGridLayout()
    {
      RecomputeColumnIndices();
      ReIndexSortOrder();
      return _layoutToEdit;
    }

    /// <summary>
    /// Reorder the sort order of each columnLayout in GridLayout
    /// </summary>
    public void ReIndexSortOrder()
    {
      for (int i = 0; i < _sortedBindingList.Count; ++i)
      {
        var clw = _sortedBindingList[i];
        if (clw != null)
        {
          clw.ColumnLayout.SortOrder = i;
        }
      }
    }

    /// <summary>
    /// Sets the description for text editor in UI
    /// </summary>
    public void SetDescription(ColumnLayout columnLayout_)
    {
      string caption = columnLayout_.Caption;
      string category = columnLayout_.FieldCategory;
      string turboRiskCategory;
      if (TurboRiskGroups == null || TurboRiskGroups.Count == 0)
        turboRiskCategory = null;
      else
        turboRiskCategory = TurboRiskGroups.Contains(columnLayout_.Key) ? TurboRiskGroups[columnLayout_.Key].ToString() : "(None)";

      string description = "";
      if (ColumnDescriptions != null && ColumnDescriptions.Contains(columnLayout_.Key))
      {
        description = ColumnDescriptions[columnLayout_.Key].ToString();
      }
      if (description == string.Empty)
      {
        description = columnLayout_.Description;
      }
      LayoutEditor.DoSetDescription(caption, description, category, turboRiskCategory);
    }

    private bool IsAvailableLayout(ColumnLayout columnLayout_)
    {
      bool doNotFilterColumnDueToDerivedField = ShowDerivedColumns || ColumnFilterProvider == null ||
                                            !ColumnFilterProvider.IsFilterable(columnLayout_);

      return columnLayout_.Hidden && doNotFilterColumnDueToDerivedField;
    }
    /// <summary>
    /// Classify hidden columns and visible columns according to a provided grid layout
    /// </summary>
    /// <param name="gridLayout_">The grid layout.</param>
    private void ClassifyColumns(GridLayout gridLayout_)
    {
      var colLayouts = new ColumnLayout[gridLayout_.Columns.Count];
      var list = new ArrayList(gridLayout_.Columns);
      list.Sort(new ColumnLayoutIndexComparer());
      list.CopyTo(colLayouts, 0);
    

      var filteredLayouts = colLayouts.Where(colLayout => VerifyColumnKeysList == null || VerifyColumnKeysList.Contains(colLayout.Key));
      filteredLayouts.ForEach(SetCategory);


      filteredLayouts.Where(IsAvailableLayout)
        .ForEach(colLayout => _hiddenBindingList.Add(new ColumnWrapper(colLayout)));

      filteredLayouts.Where(colLayout => !colLayout.Hidden)
        .ForEach(colLayout => _visibleBindingList.Add(new ColumnWrapper(colLayout)));


      RefreshSortedOrder(filteredLayouts);
    }

    private void RefreshSortedOrder(IEnumerable<ColumnLayout> filteredLayouts)
    {
      IList<ColumnWrapper> tempSortedList = new List<ColumnWrapper>();
      foreach (ColumnLayout colLayout in filteredLayouts)
      {
        if (colLayout.SortDirection == ColumnLayout.SortDirections.None || colLayout.SortOrder < 0)
        {
          _availableBindingList.Add(new ColumnWrapper(colLayout));
          colLayout.SortOrder = -1;
          colLayout.SortDirection = ColumnLayout.SortDirections.None;
        }
        else
        {
          if (colLayout.SortOrder >= 0)
          {
            tempSortedList.Add(new ColumnWrapper(colLayout));
          }
        }
      }
      //Order the sort layout by using Sort Order
      IEnumerable<ColumnWrapper> sortedList = tempSortedList.OrderBy(item => item.ColumnLayout.SortOrder);
      sortedList.ForEach(item => _sortedBindingList.Add(item));
    }

    /// <summary>
    /// Updates the bound controls
    /// </summary>
    /// <param name="gridLayout_"></param>
    private void PopulateControls(GridLayout gridLayout_)
    {
      if (gridLayout_ != null)
      {
        // this forces controls bound to the m_cmbListBindingSource to sync with 
        // the current selection in the dropdown.
        LayoutEditor.SetNumFrozenColumns(gridLayout_);
        _hiddenBindingList.Clear();
        _visibleBindingList.Clear();
        _sortedBindingList.Clear();
        _availableBindingList.Clear();
        ClassifyColumns(gridLayout_);
        LayoutEditor.SetDataSourcesForHideVisible(AvailableItemsSelectorDataSource<IClassWrapper>.CreateDataSource(
                                                    _hiddenBindingList,
                                                    _visibleBindingList));
        LayoutEditor.SetDataSourcesForSorting(
          AvailableItemsSelectorDataSource<ColumnWrapper>.CreateDataSource(_availableBindingList,
                                                                           _sortedBindingList));
        LayoutEditor.SetDataSourceForFormattedGrid(_visibleBindingList);
        LayoutEditor.SetDataSourceForGridLevel(gridLayout_);
      }
    }

    /// <summary>
    /// Sets the Index property for relevant ColumnLayout objects bound to the list boxes based
    /// on their current position in the list.
    /// </summary>
    private void RecomputeColumnIndices()
    {
      if (LayoutEditor.InvokeRequired)
      {
        LayoutEditor.BeginInvoke(new voidDelegate(RecomputeColumnIndices), null);
        return;
      }
      int newIndex = 0;

      foreach (ColumnWrapper colWrapper in _visibleBindingList)
      {
        colWrapper.ColumnLayout.Hidden = false;
        colWrapper.ColumnLayout.Index = newIndex++;
      }

      foreach (ColumnWrapper colWrapper in _hiddenBindingList)
      {
        colWrapper.ColumnLayout.Hidden = true;
        colWrapper.ColumnLayout.Index = newIndex++;
      }
    }

    /// <summary>
    /// Helper method to set the category for each columnLayout
    /// </summary>
    private void SetCategory(ColumnLayout colLayout_)
    {
      if (ColumnCategories == null)
      {
        LayoutEditor.HideAllGroupByButtons();
      }
      else if (ColumnCategories.Contains(colLayout_.Key))
      {
        var category = ColumnCategories[colLayout_.Key] as Category;
        if (category != null)
        {
          colLayout_.FieldCategory = category.Name;
          colLayout_.FieldCategoryOrder = category.Order;
        }
      }
    }

    #endregion

    #region Nested type: voidDelegate

    private delegate void voidDelegate();

    #endregion

    private bool _showDerivedColumns;
    public bool ShowDerivedColumns
    {
      get { return _showDerivedColumns; }
      set { _showDerivedColumns = value;
        PopulateControls(GetGridLayout());
      }
    }
  }
}