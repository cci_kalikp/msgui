﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Infragistics.Win.UltraWinGrid;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{

  public class EditLayoutHelper
  {
    /// <summary>
    /// Clones the layout.
    /// </summary>
    /// <param name="layoutToCopy_">The layout to copy.</param>
    /// <param name="newLayoutName_">The new layout name.</param>
    /// <returns></returns>
    public static GridLayout CloneLayout(GridLayout layoutToCopy_, string newLayoutName_)
    {
      var columnCollection = new ColumnLayoutCollection();
      var newGrid = new GridLayout
                      {
                        BandId = layoutToCopy_.BandId,
                        Font = layoutToCopy_.Font,
                        LockedColumns = layoutToCopy_.LockedColumns,
                        GridName = layoutToCopy_.GridName,
                        Application = layoutToCopy_.Application,
                        Columns = columnCollection,
                        LayoutName = newLayoutName_,
                        UpdateID = layoutToCopy_.UpdateID
                      };

      if (layoutToCopy_.Columns == null)
      {
        return newGrid;
      }

      foreach (ColumnLayout columnLayout in layoutToCopy_.Columns)
      {
        columnCollection.Add((ColumnLayout)columnLayout.Clone());
      }
      return newGrid;
    }

    /// <summary>
    /// Helper method to udpate the binding list.
    /// </summary>
    /// <param name="ultragridBindingList_">The ultragrid binding list.</param>
    /// <param name="clwArray_">The IClassWrapper array.</param>
    /// <param name="indexArray_">The index array.</param>
    public static void UpdateBingList<T>(BindingList<T> ultragridBindingList_, T[] clwArray_, int[] indexArray_)where T:IClassWrapper
    {

      for (int j = 0; j < clwArray_.Length; j++)
      {
        ultragridBindingList_.Remove(clwArray_[j]);
      }

      for (int j = 0; j < clwArray_.Length; j++)
      {
        int toIndex = indexArray_[j];
        if (toIndex > 0 && toIndex < ultragridBindingList_.Count)
        {
          ultragridBindingList_.Insert(toIndex, clwArray_[j]);
          ultragridBindingList_[toIndex].Select= true;
          clwArray_[j].Index = toIndex;
        }
        else
        {
          if (toIndex <= 0)
          {
            ultragridBindingList_.Insert(0, clwArray_[j]);
            ultragridBindingList_[0].Select = true;
            clwArray_[j].Index = 0;
          }
          else
          {
            ultragridBindingList_.Insert(ultragridBindingList_.Count, clwArray_[j]);
            ultragridBindingList_[ultragridBindingList_.Count - 1].Select = true;
            clwArray_[j].Index = ultragridBindingList_.Count - 1;
          }
        }
      }
    }

    /// <summary>
    /// Helper method to reorder columns in a list.
    /// </summary>
    /// <param name="ultragridBindingList_">The ultragrid binding list_.</param>
    /// <param name="selRows_">The seleted rows_.</param>
    /// <param name="toIndex_">To index_.</param>
    public static void ReorderColumns<T>(BindingList<T> ultragridBindingList_, T[] selRows_, int toIndex_) where T:IClassWrapper
    {
      //it to Index equals -1, it means it is draged over the last row.
      //Set the toIndex in bindinglist as the last one.
      if (toIndex_ == -1)
        toIndex_ = ultragridBindingList_.Count - 1;

      foreach (var row in selRows_)
      {
        //If we are moving down, we need to reorder the index
        //before we remove column and insert column according to the index again.
        if (toIndex_ > ultragridBindingList_.IndexOf(row))
        {
          toIndex_--;
        }
      }

      int i = 0;
      var clwArray = new T[selRows_.Length];
      var indexArray = new int[selRows_.Length];
      foreach (var item in selRows_)
      {
        clwArray[i] = item;
        indexArray[i++] = toIndex_;
        toIndex_++;
      }

      UpdateBingList(ultragridBindingList_, clwArray, indexArray);
    }

    /// <summary>
    /// Change the bindinglist for both ultragrid
    /// </summary>
    /// <param name="fromBindingList_">the bindinglist which we drag the column from</param>
    /// <param name="tobindingList_">the bindinglist which we want to drop the column</param>
    /// <param name="selRows_">The selected rows_.</param>
    /// <param name="insertPostition_">where we want to insert the column</param>
    public static void TransferColumns<T>(BindingList<T> fromBindingList_, BindingList<T> tobindingList_, T[] selRows_, int insertPostition_, Action<ItemMovedToEventArgs> filterProvider_) where T : class, IClassWrapper
    {
      try
      {
        foreach (var colLayout in selRows_)
        {
          TransferColumn(colLayout, fromBindingList_, tobindingList_, filterProvider_, insertPostition_);
        }
      }
      catch (Exception e)
      {
        throw new Exception("Error loading xml layout data. Data not in correct format.", e);
      }
    }

    private static void TransferColumn<T>(T colLayout_, BindingList<T> fromBindingList_, BindingList<T> tobindingList_, Action<ItemMovedToEventArgs> filterProvider_, int insertPostition_) where T : class, IClassWrapper
    {
      if (colLayout_ == null|| fromBindingList_ == null || tobindingList_ == null) return;
      colLayout_.Select = false;
      var itemMovedCancelableArgs = new ItemMovedToEventArgs(colLayout_);
      if (filterProvider_ != null) filterProvider_(itemMovedCancelableArgs);
      if (tobindingList_.Contains(colLayout_)) return;
      if (insertPostition_ > -1)
      {
        //Set the row as selected when drag and drop to another column;           
        if (!itemMovedCancelableArgs.Cancel)
        {
          tobindingList_.Insert(insertPostition_, colLayout_);
        }
        colLayout_.Select = true;
        fromBindingList_.Remove(colLayout_);
      }
      else
      {
        if (!itemMovedCancelableArgs.Cancel)
        {
          tobindingList_.Add(colLayout_);
        }
        colLayout_.Select = true;
        fromBindingList_.Remove(colLayout_);
      }
    }

    /// <summary>
    /// Helper method to shift columns down or up.
    /// </summary>
    /// <param name="ultragridBindingList_">The ultragrid binding list_.</param>
    /// <param name="selRows_">The selected rows_.</param>
    /// <param name="shiftDown_">if set to <c>true</c> [shift down_].</param>
    public static void ShiftColumns<T>(BindingList<T> ultragridBindingList_, T[] selRows_, bool shiftDown_)where T:IClassWrapper
    {
      var indexArray = new int[selRows_.Length];
      int i = 0;
      int delta = (shiftDown_) ? 1 : -1;

      foreach (var item in selRows_)
      {
        //if we are shif dowm the last row, put it to the first row
        if (shiftDown_ && ultragridBindingList_.IndexOf(item) == ultragridBindingList_.Count - 1)
        {
          indexArray[i++] = 0;
        }
        //if we are shif up the first row, put it to the last row
        else if (!shiftDown_ && ultragridBindingList_.IndexOf(item) == 0)
        {
          indexArray[i++] = ultragridBindingList_.Count - 1;     
        }
        else
        {
          indexArray[i++] = ultragridBindingList_.IndexOf(item) + delta;
        }
      }
      UpdateBingList(ultragridBindingList_, selRows_, indexArray);
    }

    /// <summary>
    /// Helper method to unselect all rows in ultraGrid.
    /// </summary>
    /// <param name="grid_">The grid.</param>
    public static void UnselectAllRows(UltraGrid grid_)
    {
      foreach (var row in grid_.Rows)
      {
        if(row is UltraGridGroupByRow && row.ChildBands.Count > 0)
        {
          //Unselect group by row
          row.Selected = false;

          //Unselect all child rows below group by rows
          foreach (UltraGridRow childRow in row.ChildBands[0].Rows)
          {
            childRow.Selected = false;
          }
        }
        else
        {
          row.Selected = false;         
        }
      }
    }

    /// <summary>
    /// Helper method to select all rows in ultraGrid.
    /// </summary>
    /// <param name="grid_">The grid.</param>
    public static void SelectAllRows(UltraGrid grid_)
    {
      foreach (var row in grid_.Rows)
      {
        if (row is UltraGridGroupByRow && row.ChildBands.Count > 0)
        {
          //Select group by row
          row.Selected = true;

          //Select all child rows below group by rows
          foreach (UltraGridRow childRow in row.ChildBands[0].Rows)
          {
            childRow.Selected = true;
          }
        }
        else
        {
          row.Selected = true;
        }
      }
    }

    /// <summary>
    /// Helper method to select all rows in ultraGrid.
    /// </summary>
    /// <param name="grid_">The grid.</param>
    public static void SelectRows(UltraGrid grid_, IList<IClassWrapper> wrappedColumns_)
    {
      if (wrappedColumns_ == null)
        return;

      foreach (var row in grid_.Rows)
      {
        if (row is UltraGridGroupByRow && row.ChildBands.Count > 0)
        {
          if (row.ListObject == null) continue;        

          //Select all child rows below group by rows
          foreach (UltraGridRow childRow in row.ChildBands[0].Rows)
          {
            var childcol = (IClassWrapper)row.ListObject;            
            childRow.Selected = wrappedColumns_.Contains(childcol);            
          }
        }
        else
        {
          var col = (IClassWrapper)row.ListObject;          
          row.Selected = wrappedColumns_.Contains(col);
        }
      }
    }

    /// <summary>
    /// Helper method to set selected rows in BindingList to be seleted in ultraGrid as well
    /// </summary>
    /// <param name="grid_">The grid_.</param>
    public static void SetSelectedRows(UltraGrid grid_)
    {
      //Set selected rows in from ultragrid also selectd in to ultragrid.
      foreach (var row in grid_.Rows)
      {
        if (row is UltraGridGroupByRow && row.ChildBands.Count > 0)
        {
          if (row.ListObject == null) continue;

          foreach (var childrow in row.ChildBands[0].Rows)
          {
            var childcol = (IClassWrapper)row.ListObject;
            if (childcol.Select)
            {
              childrow.Selected = true;
            }
            childcol.Select = false;
          }
        }
        else
        {
          var col = (IClassWrapper)row.ListObject;
          if(col == null) return;
          if (col.Select)
          {
            row.Selected = true;
          }
          col.Select = false;
        }
      }
    }

    /// <summary>
    /// Helper method to get all the selected IClassWrapper in ultraGrid
    /// </summary>
    /// <param name="grid_">The grid_.</param>
    /// <returns></returns>
    public static T[] GetSelectedRows<T>(UltraGrid grid_) where T:IClassWrapper
    {
      var selRows = new T[grid_.Selected.Rows.Count];

      //Sort the selected row first.
      grid_.Selected.Rows.Sort();
      int i = 0;
      //Set selected rows in from ultragrid also selectd in to ultragrid.
      foreach (var row in grid_.Rows)
      {
        //if it is groupby row, we need to treverse the child row
        if (row is UltraGridGroupByRow && row.ChildBands.Count > 0)
        {
          foreach (var childrow in row.ChildBands[0].Rows)
          {
            var childcol = (T)childrow.ListObject;
            if (childrow.Selected)
            {
              selRows[i] = childcol;
              childcol.Select = true;
              i++;
            }
            else
            {
              childcol.Select = false;
            }
             
          }
        }
        else
        {
          var col = (T)row.ListObject;
          if (row.Selected)
          {
            selRows[i] = col;
            col.Select = true;
            i++;
          }
          else
          {
            col.Select = false;
          }
        }
       }
      return selRows;
    }


    /// <summary>
    /// Set focus to filter row
    /// </summary>
    /// <param name="grid_"></param>
    public static void SetFocusToFilterRow(UltraGrid grid_)
    {
      grid_.Rows.FilterRow.Cells[0].Activate();
      grid_.PerformAction(UltraGridAction.EnterEditMode);
    }

    /// <summary>
    /// Resturn sorted columns from a gridLayout
    /// </summary>
    /// <param name="gridLayout_"></param>
    /// <returns></returns>
    public static IList<ColumnLayout> GetSortedColumns(GridLayout gridLayout_)
    {
      if(gridLayout_ == null || gridLayout_.Columns == null)
        return null;

      var result = new List<ColumnLayout>();      
      foreach (ColumnLayout columnLayout in gridLayout_.Columns)
      {
        if (columnLayout.SortOrder >= 0 && columnLayout.SortDirection != ColumnLayout.SortDirections.None)
        {
          result.Add((ColumnLayout)columnLayout.Clone());
        }
      }
      
      return result.Count > 0 ? result : null;
    }

    /// <summary>
    /// Apply sorting on a given grid. If sorted column list is blank, reset sorting in the grid.
    /// </summary>
    /// <param name="gridLayout_">Target grid layout</param>
    /// <param name="sortedColumns_">A list of sorted columns</param>
    public static void ApplySortingOnGrid(GridLayout gridLayout_, IList<ColumnLayout> sortedColumns_)
    {
      if (gridLayout_ == null || gridLayout_.Columns == null)
        return;

      if(sortedColumns_ == null)
      {        
        //reset all sorted columns
        foreach (ColumnLayout columnLayout in gridLayout_.Columns)
        {          
          if(columnLayout.SortOrder >= 0 )
          {
             //Reset sorting
          columnLayout.SortAbsolute = false;
          columnLayout.SortDirection = ColumnLayout.SortDirections.None;
          columnLayout.SortOrder = -1;
          }
        }
      }
      else
      {
        ColumnLayout source;
        foreach (ColumnLayout columnLayout in gridLayout_.Columns)
        {
          int sourceIndex = sortedColumns_.IndexOf(columnLayout);          
          if (sourceIndex < 0)
          {
            //Reset sorting
            columnLayout.SortAbsolute = false;
            columnLayout.SortDirection = ColumnLayout.SortDirections.None;
            columnLayout.SortOrder = -1;
          }
          else
          {
            source = sortedColumns_[sourceIndex];
            columnLayout.SortAbsolute = source.SortAbsolute;
            columnLayout.SortDirection = source.SortDirection;
            columnLayout.SortOrder = source.SortOrder;
          }
        }
      }      
    }

    /// <summary>
    /// Get an enumerator over a list of wrapped column layouts 
    /// </summary>
    /// <param name="columnLayouts_">A list of column layouts.</param>
    /// <returns></returns>
    public static IEnumerable<IClassWrapper> GetColumnWrapperEnumerator(IList<ColumnLayout> columnLayouts_)
    {
      foreach (var columnLayout in columnLayouts_)
      {
        yield return new ColumnWrapper(columnLayout);  
      }      
    }
  }
}
