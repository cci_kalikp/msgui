using System;
using System.Xml;
using System.IO;
using System.Collections;
using System.Xml.Serialization;
using System.Runtime.CompilerServices;
using MorganStanley.MSDesktop.Rambo.AsyncUtils;
using MorganStanley.MSDesktop.Rambo.DataServices;
using MorganStanley.MSDesktop.Rambo.Desktop.Utils;
using MorganStanley.MSDotNet.Directory;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  /// <summary>
  /// Interface for a simple Serializer class which is used to generate an array of dummy
  /// list objects from the XML returned by the List Data Service
  /// </summary>
  public interface IXmlListBuilder
  {
    /// <summary>
    /// Returns the list of dummy values for the passed element.
    /// </summary>
    object[] DeserializeList(XmlElement value_);
  }

	/// <summary>
	/// Abstract base implementation of a resource manager which manages
	/// a) Caching of both Resource List and Resource data
	/// b) Reading Data from DataServices
	/// c) Async calls only use the thread pool if the data is not already in the cache
	/// d) If a request has already been made to ds for some data queues up callbacks
	///    on that instead of sending multiple requests.
	/// e) Include CostCentre logic to retrieve a User's Grouping
	/// </summary>
	public abstract class ResourceManager
	{
    public delegate void ResourceSaveNotifyDelegate(object sender_, ResourceSaveNotifyArgs args_);
    protected delegate object LoadValueDelegate(string key_);
    protected delegate object[] LoadValuesDelegate(string[] keys_);
    protected delegate object[] LoadValuesAttributesDelegate(IDictionary attributes_);
    protected delegate object[] LoadListDelegate(string listKey_);
    protected delegate object[] LoadListAttributesDelegate(IDictionary attributes_);
    protected delegate void SaveValueDelegate(object value_) ;

    //_cache.SyncRoot used as Cache lock
    protected IDictionary _cache = Hashtable.Synchronized(new Hashtable());
    protected IDictionary _listCache = new Hashtable();
    protected Hashtable _waitingRequests = new Hashtable();
    private bool _costCentreSubscription = false;
    private string _costCentre;

    private static ILogger _log = LoggerManager.GetLogger(typeof(ResourceManager));

    /// <summary>
    /// Event fired when the cache is updated with a value
    /// </summary>
    public event ResourceSaveNotifyDelegate NotifyResourceSave;

		protected ResourceManager()
		{
		}

    /////////////////////////////Asynchronous methods////////////////////////////////////

    /// <summary>
    /// Retrieves Asynchronously a single value for the passed key. If the data is already in the cache
    /// will callback synchronously
    /// </summary>
    /// <param name="key_">Resource data key</param>
    /// <param name="callback_">Async callback</param>
    /// <param name="state_">state</param>
    /// <returns>AsyncResult for the request</returns>
    protected virtual IAsyncResult BeginGetValue(string key_, AsyncCallback callback_, object state_)
    {
      object cacheValue = null;
      ManualAsyncResult result = null;
      lock(_cache.SyncRoot)
      {
        // Look up in cache first
        if(_cache.Contains(key_))
        {
          object cached = _cache[key_];
          if (IsLoaded(cached))
          {
            //Build Async result and dummy Asynchroniser
            Asynchronizer async = new Asynchronizer(callback_, state_);
            result = new ManualAsyncResult(callback_, state_, async);
            cacheValue = cached;
          }
        }
        else if (_waitingRequests.ContainsKey(key_))
        {
          IList reqs = _waitingRequests[key_] as IList;
          Asynchronizer async = new Asynchronizer(callback_, state_);
          result = new ManualAsyncResult(callback_, state_, async);
          reqs.Add(result);
          return result;
        }
        else
        {
          _waitingRequests[key_] = new ArrayList();
        }
      }
      if (cacheValue != null)
      {
        result.SetMethodReturnedValue(cacheValue);
        return result;
      }

      Asynchronizer ssi = new Asynchronizer(callback_, state_); 
      return ssi.BeginInvoke(new LoadValueDelegate(LoadValue), new object[] {key_});
    }

    /// <summary>
    /// Retrieves Asynchronously a set of values for the passed keys. If the data is already in the cache
    /// will callback synchronously. If no data is returned for a particular key a result will not be returned
    /// for it so the result object[] may be smaller then the keys string[].
    /// </summary>
    /// <param name="keys_">Resource data keys</param>
    /// <param name="callback_">Async callback</param>
    /// <param name="state_">state</param>
    /// <returns>AsyncResult for the request</returns>
    protected virtual IAsyncResult BeginGetValues(string[] keys_, AsyncCallback callback_, object state_)
    {
      if (keys_ == null || keys_.Length == 0)
      {
        Asynchronizer async = new Asynchronizer(callback_, state_);
        ManualAsyncResult emptyResult = new ManualAsyncResult(callback_, state_, async);
        emptyResult.SetMethodReturnedValue(Array.CreateInstance(ResourceType, 0));
        return emptyResult;
      }

      string[] sortedKeys = (string[]) keys_.Clone();
      Array.Sort(sortedKeys);
      string waitingKey = string.Join(",", sortedKeys);

      object[] vals = Array.CreateInstance(ResourceType, keys_.Length) as object[];
      ManualAsyncResult result = null;
      
      lock(_cache.SyncRoot)
      {
        for (int i=0; i<keys_.Length; ++i)
        {
          if(_cache.Contains(keys_[i]))
          {
            vals[i] = _cache[keys_[i]];
            if (!IsLoaded(vals[i]))
            {
              vals = null;
              break;
            }
          }
          else
          {
            vals = null;
            break;
          }
        }

        if (vals == null)
        {
          if (_waitingRequests.ContainsKey(waitingKey))
          {
            IList reqs = _waitingRequests[waitingKey] as IList;
            Asynchronizer async = new Asynchronizer(callback_, state_);
            result = new ManualAsyncResult(callback_, state_, async);
            reqs.Add(result);
            return result;
          }
          else
          {
            _waitingRequests[waitingKey] = new ArrayList();
          }
        }
        else
        {
          Asynchronizer async = new Asynchronizer(callback_, state_);
          result = new ManualAsyncResult(callback_, state_, async);
        }
      }
      if (vals != null)
      {
        result.SetMethodReturnedValue(vals); //Causes callbacks to be fired
        return result;
      }

      Asynchronizer ssi = new Asynchronizer(callback_, state_);
      return ssi.BeginInvoke(new LoadValuesDelegate(LoadValues), new object[] {keys_});
    }

    /// <summary>
    /// Retrieves Asynchronously a set of value for the attributes. If the data is already in the cache
    /// will callback synchronously.
    /// </summary>
    /// <param name="attributes_">Resource attributes</param>
    /// <param name="callback_">Async callback</param>
    /// <param name="state_">state</param>
    /// <returns>AsyncResult for the request</returns>
    protected virtual IAsyncResult BeginGetValues(IDictionary attributes_, AsyncCallback callback_, object state_)
    {
      object[] vals = null;
      ManualAsyncResult result = null;
      lock(_cache.SyncRoot)
      {
        string waitingkey = BuildAttributeWaitingKey(attributes_);
        // Look up in cache first
        if(_listCache.Contains(waitingkey))
        {
          //Build Async result and dummy Asynchroniser
          Asynchronizer async = new Asynchronizer(callback_, state_);
          result = new ManualAsyncResult(callback_, state_, async);
          vals = BuildFromCache((IList) _listCache[waitingkey]);
        }
        else if (_waitingRequests.Contains(waitingkey))
        {
          IList reqs = _waitingRequests[waitingkey] as IList;
          Asynchronizer async = new Asynchronizer(callback_, state_);
          result = new ManualAsyncResult(callback_, state_, async);
          reqs.Add(result);
          return result;
        }
        else
        {
          _waitingRequests[waitingkey] = new ArrayList();
        }
      }
      if (vals != null)
      {
        result.SetMethodReturnedValue(vals);
        return result;
      }

      Asynchronizer ssi = new Asynchronizer(callback_, state_); 
      return ssi.BeginInvoke(new LoadValuesAttributesDelegate(LoadValues), 
        new object[] {attributes_});
    }

    /// <summary>
    /// Retrieves Asynchronously a list of value for the passed list key. The returned values are built
    /// from the ListDataService result by the ListSerializer which controls how much of them is populated.
    /// If the data is already in the cache will callback synchronously.
    /// </summary>
    /// <param name="listKey_">The key for the ListDataService</param>
    /// <param name="callback_">Async callback</param>
    /// <param name="state_">state</param>
    /// <returns>AsyncResult for the request</returns>
    protected virtual IAsyncResult BeginGetList(string listKey_, AsyncCallback callback_, object state_)
    {
      object cacheValue = null;
      ManualAsyncResult result = null;
      lock(_cache.SyncRoot)
      {
        // Look up in cache first
        if(_listCache.Contains(listKey_))
        {
          //Build Async result and dummy Asynchroniser
          Asynchronizer async = new Asynchronizer(callback_, state_);
          result = new ManualAsyncResult(callback_, state_, async);
          cacheValue = BuildValuesList((IList) _listCache[listKey_]);
        }
        if (cacheValue == null)
        {
          if (_waitingRequests.ContainsKey(listKey_))
          {
            IList reqs = _waitingRequests[listKey_] as IList;
            Asynchronizer async = new Asynchronizer(callback_, state_);
            result = new ManualAsyncResult(callback_, state_, async);
            reqs.Add(result);
            return result;
          }
          else
          {
            _waitingRequests[listKey_] = new ArrayList();
          }
        }
      }
      if (cacheValue != null)
      {
        result.SetMethodReturnedValue(cacheValue);
        return result;
      }

      Asynchronizer ssi = new Asynchronizer(callback_, state_); 
      return ssi.BeginInvoke(new LoadListDelegate(LoadList), new object[] {listKey_});
    }

    /// <summary>
    /// Retrieves Asynchronously a list of values for the passed attributes. The returned values are built
    /// from the ListDataService result by the ListSerializer which controls how much of them is populated.
    /// If the data is already in the cache will callback synchronously.
    /// </summary>
    /// <param name="attributes_">The attributes for the ListDataService</param>
    /// <param name="callback_">Async callback</param>
    /// <param name="state_">state</param>
    /// <returns>AsyncResult for the request</returns>
    protected virtual IAsyncResult BeginGetList(IDictionary attributes_, AsyncCallback callback_, object state_)
    {
      string waitingKey = BuildAttributeWaitingKey(attributes_);
      object cacheValue = null;
      ManualAsyncResult result = null;
      lock(_cache.SyncRoot)
      {
        // Look up in cache first
        if(_listCache.Contains(waitingKey))
        {
          //Build Async result and dummy Asynchroniser
          Asynchronizer async = new Asynchronizer(callback_, state_);
          result = new ManualAsyncResult(callback_, state_, async);
          cacheValue = BuildValuesList((IList) _listCache[waitingKey]);
        }
        else if (_waitingRequests.ContainsKey(waitingKey))
        {
          IList reqs = _waitingRequests[waitingKey] as IList;
          Asynchronizer async = new Asynchronizer(callback_, state_);
          result = new ManualAsyncResult(callback_, state_, async);
          reqs.Add(result);
          return result;
        }
        else
        {
          _waitingRequests[waitingKey] = new ArrayList();
        }
      }
      if (cacheValue != null)
      {
        result.SetMethodReturnedValue(cacheValue);
        return result;
      }

      Asynchronizer ssi = new Asynchronizer(callback_, state_); 
      return ssi.BeginInvoke(new LoadListAttributesDelegate(LoadList), new object[] {attributes_});
    }

    /// <summary>
    /// Saves Asynchronously the passed value and updates the caches.
    /// </summary>
    /// <param name="value_">The Value to save</param>
    /// <param name="callback_">Async callback</param>
    /// <param name="state_">state</param>
    /// <returns>AsyncResult for the request</returns>
    protected virtual IAsyncResult BeginSaveValue(object value_, AsyncCallback callback_, object state_)
    {
      Asynchronizer ssi = new Asynchronizer( callback_, state_); 
      return ssi.BeginInvoke(new SaveValueDelegate(SaveValue), new Object[] {value_});
    }

    /// <summary>
    /// Returns the result of BeginGetValue for the passed AsyncResult.
    /// </summary>
    /// <param name="ar_">result returned by BeginGetValue</param>
    /// <returns>The requested Value or null if it wasn't found</returns>
    protected virtual object EndGetValue(IAsyncResult ar_)
    {
      AsynchronizerResult asr = (AsynchronizerResult)ar_;
      return asr.AsynchronizerParent.EndInvoke(ar_);
    }

    /// <summary>
    /// Returns the result of BeginGetValues for the passed AsyncResult.
    /// </summary>
    /// <param name="ar_">result returned by BeginGetValues</param>
    /// <returns>The requested values or null if none were found</returns>
    protected virtual object[] EndGetValues(IAsyncResult ar_)
    {
      AsynchronizerResult asr = (AsynchronizerResult)ar_;
      return asr.AsynchronizerParent.EndInvoke(ar_) as object[];
    }

    /// <summary>
    /// Returns the result of BeginGetList for the passed AsyncResult.
    /// </summary>
    /// <param name="ar_">result returned by BeginGetList</param>
    /// <returns>The requested values or null if none were found</returns>
    protected virtual object[] EndGetList(IAsyncResult ar_)
    {
      AsynchronizerResult asr = (AsynchronizerResult)ar_;
      return asr.AsynchronizerParent.EndInvoke(ar_) as object[];
    }

    /// <summary>
    /// Completes a Save call.
    /// </summary>
    /// <param name="ar_">result returned by BeginSaveValue</param>
    protected virtual void EndSaveValue(IAsyncResult ar_)
    {
      AsynchronizerResult asr = (AsynchronizerResult)ar_;
      asr.AsynchronizerParent.EndInvoke(ar_);
    }

    /////////////////////////////Synchronous methods////////////////////////////////////

    /// <summary>
    /// Synchronously retrieves a single value for the passed key from either the cache
    /// or the DataService. This is called by BeginGetValue.
    /// </summary>
    /// <param name="key_">the value's key</param>
    /// <returns>The requested value or null if not found</returns>
    protected virtual object LoadValue(string key_)
    {
      return LoadValue(key_, false);
    }

    /// <summary>
    /// Synchronously retrieves a single value for the passed key from either the cache
    /// or the DataService. This is called by BeginGetValue.
    /// </summary>
    /// <param name="key_">the value's key</param>
    /// <param name="forceReload_">if set to <c>true</c> force the reload of the value from the DS.</param>
    /// <returns>The requested value or null if not found</returns>
    protected virtual object LoadValue(string key_, bool forceReload_)
    {
      return LoadValue(key_, forceReload_, true);
    }

    protected virtual object LoadValue(string key_, bool forceReload_, bool useDS_)
    {
      try
      {
        if(!DataServiceManager.IsInitialised())
        {
          throw new Exception("DataService Manager was not initialised.");
        }

        object val = null;
        if (!forceReload_)
        {
          lock (_cache.SyncRoot)
          {
            // Look up in cache first
            if (_cache.Contains(key_))
            {
              val = _cache[key_];
              if (!IsLoaded(val))
              {
                val = null; //Still needs to load from server
              }
            }
          }
        }

        if (useDS_)
        {
          if (val == null)
          {
            IDataKey dsKey = DataKeyFactory.BuildDataKey(key_);

            DataEvent dataEvent = DataService.GetSnapshot(dsKey);

            if (dataEvent != null)
            {
              //HACK: allowing subclasses to define custom deserialization code.
              if (Serializer == null)
              {
                val = Deserialize(((XMLDataObject)dataEvent.DataObject).Value.OuterXml);
              }
              else
              {
                val = Serializer.Deserialize(new StringReader(
                                          ((XMLDataObject)dataEvent.DataObject).Value.OuterXml));
              }
              lock (_cache.SyncRoot)
              {
                _cache[key_] = val;
              }
            }
          }
          CallbackWaitingRequests(key_, val);
        }
        return val;
      }
      catch (Exception ex_)
      {
        Exception e = new Exception("Error retrieving resource "+key_, ex_);
        SetWaitingRequestException(key_, e);
        throw e;
      }
    }

    /// <summary>
    /// Synchronously retrieves a set of values for the passed keys from either the cache
    /// or the DataService. This is called by BeginGetValues.
    /// </summary>
    /// <param name="keys_">the requested keys</param>
    /// <returns>The requested values or null if none were found</returns>
    protected virtual object[] LoadValues(string[] keys_)
    {
      string[] sortedKeys = (string[]) keys_.Clone();
      Array.Sort(sortedKeys);
      string waitingKey = string.Join(",", sortedKeys);
      try
      {
        if(!DataServiceManager.IsInitialised())
        {
          throw new Exception("DataService Manager was not initialised.");
        }	

        object[] vals = Array.CreateInstance(ResourceType, keys_.Length) as object[];
          IList remainingKeys = new ArrayList();
        lock(_cache.SyncRoot)
        {
          for(int i = 0; i < keys_.Length; ++i)
          {
            string key = keys_[i];
            if(_cache.Contains(key))
            {
              vals[i] = _cache[key] as GridLayout;
              if (!IsLoaded(vals[i]))
              {
                remainingKeys.Add(DataKeyFactory.BuildDataKey(key));
              }
            }
            else
            {
              remainingKeys.Add(DataKeyFactory.BuildDataKey(key));
            }
          }
        }

        if (remainingKeys.Count > 0)
        {
          ISubscription sub = DataService.CreateSubscription(remainingKeys);
          IDictionary dict = DataService.GetSnapshot(sub);

          if (dict != null)
          {
            lock (_cache.SyncRoot)
            {
              foreach(DataEvent de in dict.Values)
              {
                object val = Serializer.Deserialize(new StringReader(
                  ((XMLDataObject)de.DataObject).Value.OuterXml));
                for(int index = 0; index < keys_.Length; ++index)
                {
                  if(keys_[index] == ExtractKey(val))
                  {
                    vals[index] = val;
                    break;
                  }
                }
              }
            }
            if (dict.Count != remainingKeys.Count)
            {
              object[] foundVals = Array.CreateInstance(ResourceType, keys_.Length - (remainingKeys.Count - dict.Count)) as object[];
                int c=0;
              for (int i=0; i<vals.Length; ++i)
              {
                if (vals[i] != null)
                {
                  foundVals[c++] = vals[i];
                }
              }
              vals = foundVals;
            }
          }
          else
          {
            object[] foundVals = Array.CreateInstance(ResourceType, keys_.Length - remainingKeys.Count) as object[];
              int c=0;
            for (int i=0; i<vals.Length; ++i)
            {
              if (vals[i] != null)
              {
                foundVals[c++] = vals[i];
              }
            }
            vals = foundVals;
          }
        }
        CallbackWaitingRequests(waitingKey, vals);
        return vals;
      }
      catch (Exception ex_)
      {
        Exception e = new Exception("Error retrieving Resources "+waitingKey, ex_);
        SetWaitingRequestException(waitingKey, e);
        throw e;
      }
    }

    /// <summary>
    /// Synchronously retrieves a set of values for the passed attributes from either the cache
    /// or the DataService. This is called by BeginGetValues.
    /// </summary>
    /// <param name="attributes_">the attributes to use</param>
    /// <returns>The requested values or null if not found</returns>
    protected virtual object[] LoadValues(IDictionary attributes_)
    {
      string waitingKey = BuildAttributeWaitingKey(attributes_);
      try
      {
        if(!DataServiceManager.IsInitialised())
        {
          throw new Exception("DataService Manager was not initialised.");
        }
        object[] vals = null;
        lock(_cache.SyncRoot)
        {
          if (_listCache.Contains(waitingKey))
          {
            IList keys = _listCache[waitingKey] as IList;
            vals = BuildFromCache(keys);
          }
        }
        if (vals == null)
        {
          ISubscription subscription = DataService.CreateSubscription(attributes_);
          IDictionary snapshot = DataService.GetSnapshot(subscription);
          if (snapshot != null && snapshot.Count > 0)
          {
            lock (_cache.SyncRoot)
            {
              IList keys = new ArrayList(snapshot.Count);
              vals = Array.CreateInstance(ResourceType, snapshot.Count) as object[];
                int cnt = 0;
              foreach (IDataKey dsKey in snapshot.Keys)
              {
                string key = dsKey.DSKey;
                DataEvent de = snapshot[dsKey] as DataEvent;
                keys.Add(key);
                //Only use returned layout if there is no local version in the cache as
                //the local version may have been edited
                if (_cache.Contains(key))
                {
                  object val = _cache[key];
                  if (!IsLoaded(val))
                  {
                    val = Serializer.Deserialize(new StringReader(((XMLDataObject)de.DataObject).Value.OuterXml));
                    _cache[key] = val;
                  }
                  vals[cnt++] = val;
                }
                else
                {
                  object val = Serializer.Deserialize(new StringReader(((XMLDataObject)de.DataObject).Value.OuterXml));
                  _cache[key] = val;
                  vals[cnt++] = val;
                }
              }
              _listCache.Add(waitingKey,keys);
            }
          }
          else
          {
            lock (_cache.SyncRoot)
            {
              _listCache.Add(waitingKey, new ArrayList());
            }
          }
        }
        CallbackWaitingRequests(waitingKey, vals);
        return vals;
      }
      catch (Exception ex_)
      {
        Exception e = new Exception("Error retrieving Resources "+waitingKey, ex_);
        SetWaitingRequestException(waitingKey, e);
        throw e;
      }
    }

    /// <summary>
    /// Synchronously load the requested value
    /// </summary>
    /// <param name="key_">The requested key.</param>
    /// <returns>The resulting list of requested values or null if none were found</returns>
    public virtual object SyncGetValue(string key_)
    {
      return SyncGetValue(key_, false);
    }

    /// <summary>
    /// Synchronously load the requested value
    /// </summary>
    /// <param name="key_">The requested key.</param>
    /// <param name="forceReload_">if set to <c>true</c> force the reload of the value from the DS.</param>
    /// <returns>
    /// The resulting list of requested values or null if none were found
    /// </returns>
    public virtual object SyncGetValue(string key_, bool forceReload_)
    {
      return LoadValue(key_, forceReload_);
    }

    /// <summary>
    /// Synchronously load the requested values
    /// </summary>
    /// <param name="keys_">The string array of requested keys.</param>
    /// <returns>The resulting list of requested values or null if none were found</returns>
    public virtual object[] SyncGetValues(string[] keys_)
    {
      return LoadValues(keys_);
    }

    /// <summary>
    /// Synchronously retrieves a set of values from the cache or the dataservice
    /// </summary>
    /// <param name="attributes_">The attributes for the values request.</param>
    /// <returns>The resulting list of requested values or null if none were found</returns>
    public virtual object[] SyncGetValues(IDictionary attributes_)
    {
      return LoadValues(attributes_);
    }

    /// <summary>
    /// Synchronously load the requested list
    /// </summary>
    /// <param name="listKey_">The requested key.</param>
    /// <returns>The resulting list of requested values or null if none were found</returns>
    public virtual object[] SyncLoadList(string listKey_)
    {
      return LoadList(listKey_);
    }

    /// <summary>
    /// Synchronously retrieves a set of list values for the passed attributes from either the internal cache or the DataService
    /// </summary>
    /// <param name="attributes_">The attributes for requesting the list values.</param>
    /// <returns>The resulting list of requested values or null if none were found</returns>
    public virtual object[] SyncLoadList(IDictionary attributes_)
    {
      return LoadList(attributes_);
    }

    /// <summary>
    /// Synchronously publishes the passed value to the dataservice and updates the internal caches.
    /// </summary>
    /// <param name="value_">The value to publish.</param>
    public virtual void SyncSaveValue(object value_)
    {
      SaveValue(value_);
    }

    /// <summary>
    /// Synchronously retrieves a set of list values for the passed key from either the cache
    /// or the ListDataService. This is called by BeginGetList.
    /// </summary>
    /// <param name="listKey_">the requested key</param>
    /// <returns>The requested values or null if none were found</returns>
    protected virtual object[] LoadList(string listKey_)
    {
      try
      {
        if(!DataServiceManager.IsInitialised())
        {
          throw new InvalidOperationException("DataService Manager was not initialised.");
        }	      
       
        object[] vals = null;
        lock (_cache.SyncRoot)
        {
          if (_listCache.Contains(listKey_))
          {
            vals = BuildValuesList((IList) _listCache[listKey_]);
          }
        }

        if (vals == null)
        {
          IDataKey dsKey = DataKeyFactory.BuildDataKey(listKey_);
          DataEvent dataEvent = ListDataService.GetSnapshot(dsKey);
          if (dataEvent != null)
          {
            vals = ListSerializer.DeserializeList(((XMLDataObject)dataEvent.DataObject).Value);

            if (vals != null)
            {
              IList keys = new ArrayList(vals.Length > 0 ? vals.Length : 1);
              lock (_cache.SyncRoot)
              {
                foreach (object val in vals)
                {
                  string key = ExtractKey(val);
                  keys.Add(key);
                  //Add loaded dummy entry to cache if not already loaded
                  if (!_cache.Contains(key))
                  {
                    _cache[key] = val;
                  }
                }
                _listCache[listKey_] = keys;
              }
            }
            else
            {
              lock (_cache.SyncRoot)
              {
                _listCache[listKey_] = new ArrayList();
              }
            }
          }
          else
          {
            lock (_cache.SyncRoot)
            {
              _listCache[listKey_] = new ArrayList();
            }
          }
        }
        CallbackWaitingRequests(listKey_, vals);
        return vals;
      }
      catch (Exception ex_)
      {
        Exception e = new Exception("Error retrieving Resource List "+listKey_, ex_);
        SetWaitingRequestException(listKey_, e);
        throw e;
      }
    }

    /// <summary>
    /// Synchronously retrieves a set of list values for the passed attributes from either the cache
    /// or the ListDataService. This is called by BeginGetList.
    /// </summary>
    /// <param name="attributes_">the requested attributes</param>
    /// <returns>The requested values or null if none were found</returns>
    protected virtual object[] LoadList(IDictionary attributes_)
    {
      string waitingKey = BuildAttributeWaitingKey(attributes_);
      try
      {
        if(!DataServiceManager.IsInitialised())
        {
          throw new Exception("DataService Manager was not initialised.");
        }	      
       
        object[] vals = null;
        lock (_cache.SyncRoot)
        {
          if (_listCache.Contains(waitingKey))
          {
            vals = BuildValuesList((IList) _listCache[waitingKey]);
          }
        }

        if (vals == null)
        {
          ISubscription sub = ListDataService.CreateSubscription(attributes_);
          IDictionary snapshot = ListDataService.GetSnapshot(sub);
          if (snapshot != null)
          {
            ArrayList tmp = new ArrayList();
            foreach (DataEvent evnt in snapshot.Values)
            {
              tmp.AddRange(ListSerializer.DeserializeList(((XMLDataObject)evnt.DataObject).Value));
            }
            vals = tmp.ToArray(ResourceType) as object[];
            IList keys = new ArrayList(vals.Length > 0 ? vals.Length : 1);
            lock (_cache.SyncRoot)
            {
              foreach (object val in vals)
              {
                string key = ExtractKey(val);
                keys.Add(key);
                if (!_cache.Contains(key))
                {
                  _cache[key] = val;
                }
              }
              _listCache[waitingKey] = keys;
            }
          }
          else
          {
            lock (_cache.SyncRoot)
            {
              _listCache[waitingKey] = new ArrayList();
            }
          }
        }
        CallbackWaitingRequests(waitingKey, vals);
        return vals;
      }
      catch (Exception ex_)
      {
        Exception e = new Exception("Error retrieving Resource List "+waitingKey, ex_);
        SetWaitingRequestException(waitingKey, e);
        throw e;
      }
    }

    /// <summary>
    /// Synchronously publishes the passed value to the DataService and updates the
    /// internal caches. This is called by BeginSaveValue
    /// </summary>
    /// <param name="value_">the value to publish</param>
    protected virtual void SaveValue(object value_)
    {
      if(!DataServiceManager.IsInitialised())
      {
        throw new Exception("DataService Manager was not initialised.");
      }

      string key = ExtractKey(value_);

      try
      {		
        IDataKey dsKey = DataKeyFactory.BuildDataKey(key);

        XMLDataObject xmlObject = new XMLDataObject(dsKey, GetXml(value_));
        DataService.Publish(xmlObject);

      }
      catch(Exception ex)
      {
        throw new Exception("Error saving grid layout.", ex);
      }

      UpdateLocalCache(value_);
    }

    /// <summary>
    /// Adds the passed key to the list cache if it does not already exist.
    /// Note: assumes the cache lock is already held.
    /// </summary>
    /// <param name="listKey_">ListDataService key</param>
    /// <param name="key_">Value key to add</param>
    protected void AddToListCache(string listKey_, string key_)
    {
      if (_listCache.Contains(listKey_))
      {
        IList list = _listCache[listKey_] as IList;
        if (list != null && !list.Contains(key_))
        {
          list.Add(key_);
        }
      }
    }


    /// <summary>
    /// Builds a key for the passed attributes request so that it can
    /// be used in the cache.
    /// </summary>
    /// <param name="attributes_">attributes request</param>
    /// <returns>the key</returns>
    protected string BuildAttributeWaitingKey(IDictionary attributes_)
    {
      string[] sorted = new string[attributes_.Count];
      int i=0;
      foreach (string key in attributes_.Keys)
      {
        sorted[i] = key+"="+(string)attributes_[key];
        i++;
      }
      Array.Sort(sorted);
      return string.Join(",",sorted);
    }

    /// <summary>
    /// Attempts to find all the passed keys in the cache and returns them if
    /// they are all fully loaded. Otherwise returns null.
    /// 
    /// Note: Assumes already holding the cache lock
    /// </summary>
    /// <param name="keys_">The keys to lookup</param>
    /// <returns>the cached values if all found else null</returns>
    protected object[] BuildFromCache(IList keys_)
    {
      object[] vals = Array.CreateInstance(ResourceType, keys_.Count) as object[];
      for (int i=0; i<keys_.Count; ++i)
      {
        if (_cache.Contains(keys_[i]))
        {
          object val = _cache[keys_[i]] as GridLayout;
          if (!IsLoaded(val))
          {
            return null;
          }
          else
          {
            vals[i] = val;
          }
        }
        else
        {
          return null;
        }
      }
      return vals;
    }

    ////////////////////////////////Helper Methods//////////////////////////////////////

    /// <summary>
    /// Removes a cached resource list from cache.
    /// </summary>
    /// <param name="listKey_"></param>
    public virtual  void ClearListCache(String listKey_)
    {
      lock (_listCache.SyncRoot)
      {
        if (_listCache.Contains(listKey_))
        {
          _listCache.Remove(listKey_);
        }
      }
    }

    /// <summary>
    /// Clears the internal List and Value caches
    /// </summary>
    public virtual void ClearCache()
    {
      lock( _cache.SyncRoot )
      { 
        _cache.Clear();
        _listCache.Clear();
      }
    }

    /// <summary>
    /// Returns true if there is a value in the cache for the passed key.
    /// (The value may not yet be fully loaded)
    /// </summary>
    /// <param name="key_">The key to lookup</param>
    /// <returns>true if an entry was found</returns>
    public virtual bool IsInCache(string key_)
    {
      return _cache.Contains(key_);
    }

    /// <summary>
    /// Updates the caches without publishing out the updates to the DataService.
    /// Causes a NotifyResourceSave event to be fired.
    /// </summary>
    /// <param name="value_">the value to update</param>
    public virtual void UpdateLocalCache(object value_)
    {
      string key = ExtractKey(value_);
      lock(_cache.SyncRoot)
      {
        _cache[key] = value_;
        string[] listKeys = ExtractListKeys(value_);
        foreach (string listKey in listKeys)
        {
          AddToListCache(listKey, key);
        }
      }

      //Notify
      if (NotifyResourceSave != null)
      {
        NotifyResourceSave(this, new ResourceSaveNotifyArgs(key, value_));
      }
    }

    /// <summary>
    /// Whether or not CostCentreSubscriptions should be used.
    /// </summary>
    public virtual bool DoCostCentreSubscription
    {
      [MethodImpl(MethodImplOptions.Synchronized)]
      get
      {
        return _costCentreSubscription;
      }
      [MethodImpl(MethodImplOptions.Synchronized)]
      set
      {
        _costCentreSubscription = value;
      }
    }

    /// <summary>
    /// Returns the cost centre for the current user retrieved from
    /// Environment.UserName and the firmwide directory.
    /// </summary>
    [MethodImpl(MethodImplOptions.Synchronized)]
    protected virtual string GetCostCentre()
    {     
      if ( _costCentre != null )
      {
        return _costCentre;
      }
      string userId = Environment.UserName;     
      try
      {                           
        Search s = new FWD2Search(); 
        s.Username = "cn=dsserverid, ou=ldapids, o=Morgan Stanley";
        s.Password = "DS_Server";

        Person person = s.GetPersonByLogon( userId );
        _costCentre = person.CostCenter;       
      }
      catch ( Exception ex )
      {
        _log.Warning("GetCostCentre", string.Format("LDAP lookup for user '{0}' failed.",userId) , ex);
      }    
      return _costCentre;
    }

    /// <summary>
    /// Returns the UserGroupings string for the passed permissionType. Returns
    /// For Group: if (DoCostCentreSubscription) CC-costcentre else all
    /// For User: U-username
    /// 
    /// return all if the user was not found in the directory
    /// </summary>
    /// <param name="permissionType_">PermissionType to retrieve grouping for</param>
    /// <returns>grouping string</returns>
    protected virtual string GetUserGrouping(DSGridLoader.PermissionType permissionType_)
    {
      string userGrouping = "all";
      if ( DoCostCentreSubscription )
      {
        if(permissionType_ == DSGridLoader.PermissionType.Group)
        {
          userGrouping = GetCostCentre();
          if ( userGrouping != null && userGrouping != string.Empty )
          {
            userGrouping = "CC-" + userGrouping;        
          }
        }
        else
        {
          userGrouping = "U-" + Environment.UserName;
        }
      }
      else
      {
        if(permissionType_ == DSGridLoader.PermissionType.User)
        {
          userGrouping = "U-" + Environment.UserName;
        }
      }
      return userGrouping;
    }

    //////////////////////////Internal Helper Methods/////////////////////////////////

    /// <summary>
    /// Calls back all waiting AsyncResults for the passed key with the passed result.
    /// </summary>
    /// <param name="key_">key for result</param>
    /// <param name="result_">result data</param>
    protected void CallbackWaitingRequests(string key_, object result_)
    {
      IList waitingResults = null;
      lock (_cache.SyncRoot)
      {
        if (_waitingRequests.Contains(key_))
        {
          waitingResults = _waitingRequests[key_] as IList;
          _waitingRequests.Remove(key_);
        }
      }
      if (waitingResults != null)
      {
        foreach (ManualAsyncResult ar in waitingResults)
        {
          ar.SetMethodReturnedValue(result_);
        }
      }
    }

    /// <summary>
    /// Sets the Exception marker on any AsyncResults waiting for the passed key.
    /// This causes a callback to be fired.
    /// </summary>
    /// <param name="key_">The key for the exception</param>
    /// <param name="ex_">the exception</param>
    protected void SetWaitingRequestException(string key_, Exception ex_)
    {
      IList waitingResults = null;
      lock (_cache.SyncRoot)
      {
        if (_waitingRequests.Contains(key_))
        {
          waitingResults = _waitingRequests[key_] as IList;
          _waitingRequests.Remove(key_);
        }
      }
      if (waitingResults != null)
      {
        foreach (ManualAsyncResult ar in waitingResults)
        {
          ar.SetInvokeException(ex_);
        }
      }
    }

    /// <summary>
    /// Attempts to build a values array for the passed keys from the cache. Returns
    /// null if any value was not found.
    /// </summary>
    /// <param name="keys_">keys to build the list from</param>
    /// <returns>values from cache or null if all were not found</returns>
    private object[] BuildValuesList(IList keys_)
    {
      object[] vals = Array.CreateInstance(ResourceType, keys_.Count) as object[];
      for(int i = 0; i < keys_.Count; ++i)
      {
        if (_cache.Contains(keys_[i]))
        {
          vals[i] = _cache[keys_[i]];
        }
        else
        {
          return null;
        }
      }
      return vals;      
    }

    /// <summary>
    /// Converts the passed value object into it's XML representation using the Serializer
    /// property.
    /// </summary>
    /// <param name="value_">value to convert</param>
    /// <returns>the xml representation</returns>
    protected XmlElement GetXml(object value_)
    {
      StringWriter writer = new StringWriter();
      try
      {
        if (Serializer == null)
        {
          Serialize(writer, value_);				
        }
        else
        {
          Serializer.Serialize(writer, value_);				
        }
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(writer.ToString());
        return xmlDoc.DocumentElement;				
      }
      catch(IOException ioe)
      {
        throw new Exception("Error during serialization of XML data.", ioe);
      }
      catch(Exception ex)
      {
        throw new Exception("Unable to generate XML data from supplied Resource.", ex);
      }      
    }

    /// <summary>
    /// Gets the list DS node name
    /// e.g The ds name for gridLayout is "GridLayouts"
    /// the root node for GridLayoutsList ds is "GridLayouts"(the same as gridlayout ds name)
    /// sample GridLayoutsList xml:
    //// <GridLayouts>
    ////    <GridLayout>...</GridLayout>
    ////    <GridLayout>...</GridLayout>
    //// </GridLayouts>
    /// </summary>
    /// <param name="dsName_">The ds name_ for.</param>
    /// <returns></returns>
    public static string GetListDSNode(string dsName_)
    {
      string node= DataServiceManager.GetDSName(dsName_);
      if(string.IsNullOrEmpty(node))
      {
        _log.Error("GetListDSNode",string.Format("The node for ds{0} is null or empty",dsName_));
        return null;
      }

      return node.TrimEnd('s');
    }

    /////////////////////////////Abstract Methods///////////////////////////////////////
    
    /// <summary>
    /// Extracts the key from the passed value object.
    /// </summary>
    /// <param name="value_">value object</param>
    /// <returns>the value's key</returns>
    protected abstract string ExtractKey(object value_);

    /// <summary>
    /// Extracts the list key's which will contain the passed value_ so that their
    /// caches can be updated on a save.
    /// </summary>
    /// <param name="value_">value to extract keys from</param>
    /// <returns>the relievent list keys</returns>
    protected abstract string[] ExtractListKeys(object value_);
    
    /// <summary>
    /// Checks whether the value has been loaded or whether it is just the dummy
    /// value created from the ListDataService.
    /// </summary>
    /// <param name="value_">the value to check</param>
    /// <returns>true if value is fully loaded</returns>
    protected abstract bool IsLoaded(object value_);
    
    /// <summary>
    /// The main DataService which is used to retrieve the values.
    /// </summary>
    protected  abstract IDataService DataService
    { get;
    }

    /// <summary>
    /// The List DataService used to retrive the value lists
    /// </summary>
    protected abstract IDataService ListDataService
    { get;
    }

    /// <summary>
    /// Serializer for the full xml return by the main DataService.
    /// </summary>
    protected abstract XmlSerializer Serializer
    {
      get;
    }

    /// <summary>
    /// Serializer for the XML returned by the ListDataService. see IXmlListBuilder
    /// </summary>
    protected abstract IXmlListBuilder ListSerializer
    {
      get;
    }

    /// <summary>
    /// The type of the resource values this manager controls (used to build arrays).
    /// </summary>
    protected abstract Type ResourceType
    {
      get;
    }

	  /// <summary>
	  /// Custom implementation to deserialize xml.
	  /// </summary>
	  protected virtual object Deserialize(string xml)
	  {
	    throw new NotImplementedException();
	  }

    /// <summary>
    /// Custom implementation to deserialize xml.
    /// </summary>  
    protected virtual void Serialize(TextWriter text, object o)
    {
      throw new NotImplementedException();
    }
	}
}
