﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using MorganStanley.MSDesktop.Rambo.Desktop.Utils;
using MorganStanley.MSDesktop.Rambo.Desktop.Utils.Extensions;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  public partial class DlgRamboShare : Form
  {
    #region Fields

    private IDictionary<string, string> _shareGroupsDic;

    #endregion

    #region Constructors

    public DlgRamboShare(IDictionary<string, string> currentGroups_)
    {
      InitializeComponent();
      _listBoxShareGroups.DataSource = new List<string>(currentGroups_.Keys);
      _listBoxShareGroups.SelectionMode = SelectionMode.None;
    }

    #endregion

    #region Instance Methods

    public IDictionary<string, string> ShareGroups { get { return _shareGroupsDic; } }

    /// <summary>
    /// Check if current group is valid user/group
    /// </summary>
    private bool CheckValid(string key_)
    {
      if (_shareGroupsDic == null) _shareGroupsDic = new Dictionary<string, string>();

      if (UserVerification.GetGroup(key_) != null)
      {
        _shareGroupsDic.Add(key_, "group");
        return true;
      }
      if (UserVerification.GetPerson(key_) != null)
      {
        _shareGroupsDic.Add(key_, "user");
        return true;
      }
      return false;
    }

    /// <summary>
    /// Get all invalid Items
    /// </summary>
    private List<string> GetInValidItems(string groups_)
    {
      _shareGroupsDic = new Dictionary<string, string>();
      List<string> invalidGroups = new List<string>();
      string[] sharedGroups = groups_.Split(',');
      sharedGroups.ForEach(group =>
                             {
                               if (!CheckValid(group))
                               {
                                 invalidGroups.Add(group);
                               }
                             });

      return invalidGroups;
    }

    #endregion

    #region Event Handling

    private void _btnOK_Click(object sender, EventArgs e)
    {
      if (!string.IsNullOrEmpty(_txtShareWith.Text))
      {
        List<string> invalidItems = GetInValidItems(_txtShareWith.Text);
        if (invalidItems.Count > 0)
        {
          errorProvider1.SetError(_txtShareWith,"Contains invalid groups/users");
          return;
        }
      }

      //Set DialogResult as OK
      DialogResult = DialogResult.OK;
    }

    #endregion
  }
}