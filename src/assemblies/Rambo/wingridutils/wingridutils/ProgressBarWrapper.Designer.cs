namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  partial class ProgressBarWrapper
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this._progressBar = new Infragistics.Win.UltraWinProgressBar.UltraProgressBar();
      this._timer = new System.Windows.Forms.Timer(this.components);
      this.SuspendLayout();
      // 
      // _progressBar
      // 
      this._progressBar.Dock = System.Windows.Forms.DockStyle.Fill;
      this._progressBar.Location = new System.Drawing.Point(0, 0);
      this._progressBar.Name = "_progressBar";
      this._progressBar.Size = new System.Drawing.Size(75, 23);
      this._progressBar.TabIndex = 0;
      this._progressBar.Text = "[Formatted]";
      // 
      // ProgressBarWrapper
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this._progressBar);
      this.Name = "ProgressBarWrapper";
      this.Size = new System.Drawing.Size(75, 23);
      this.VisibleChanged += new System.EventHandler(this.ProgressBarWrapper_VisibleChanged);
      this.Load += new System.EventHandler(this.ProgressBarWrapper_Load);
      this.ResumeLayout(false);

    }

    #endregion

    private Infragistics.Win.UltraWinProgressBar.UltraProgressBar _progressBar;
    private System.Windows.Forms.Timer _timer;
  }
}