﻿using System;
using System.Windows.Forms;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinToolbars;

using MorganStanley.MSDesktop.Rambo.Desktop.Utils;
using ColumnHeader = Infragistics.Win.UltraWinGrid.ColumnHeader;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils.WinGridHeaderMenu
{
  /// <summary>
  /// Context menu providing column sort-by functionality.
  /// </summary>
  public class ColumnHeaderContextMenu : PopupMenuTool
  {
    [Flags]
    public enum DisplayMode
    {
      Sorting = 1,
      GroupSorting = 2
    }

    FormattingMenuHelper _formatting;

    public FormattingMenuHelper Formatting
    {
      get
      {
        return _formatting;
      }
    }

    protected FreezeSortMenuItem _menuItemFreezeSort;
    protected WrapHeaderMenuItem _menuItemWrapHeader;
    protected StateButtonTool _menuItemSortAsc;
    protected StateButtonTool _menuItemSortDes;
    protected StateButtonTool _menuItemSortAscAbs;
    protected StateButtonTool _menuItemSortDesAbs;
    protected StateButtonTool _menuItemSortGrpAsc;
    protected StateButtonTool _menuItemSortGrpDes;
    protected StateButtonTool _menuItemSortGrpAscAbs;
    protected StateButtonTool _menuItemSortGrpDesAbs;
    protected StateButtonTool _menuItemNoSort;
    protected ColumnHeader _columnHeader;
    private string _columnFormatString;
    private UltraGridColumn _column;
    private readonly UltraGrid _grid;
    private readonly UltraToolbarsManager _manager;
    protected ColumnFormat _columnFormat;
    protected DisplayMode _displayMode = DisplayMode.GroupSorting | DisplayMode.Sorting;

    protected static readonly Logger _log = new Logger(typeof(ColumnHeaderContextMenu));

    public delegate void FormatAppliedEventHandler(object sender_, string formatString_);
    public event FormatAppliedEventHandler FormatApplied;

    private const string SORT_SETKEY = "Sort";
    private const string SORT_NO = "No Sort";
    private const string SORT_ASC = "Sort Ascending";
    private const string SORT_ASC_ABS = "Sort Ascending - Absolute";
    private const string SORT_GRP_ASC = "Sort Groups Ascending";
    private const string SORT_GRP_ASC_ABS = "Sort Groups Ascending - Absolute";
    private const string SORT_DSC = "Sort Descending";
    private const string SORT_DSC_ABS = "Sort Descending - Absolute";
    private const string SORT_GRP_DSC = "Sort Groups Descending";
    private const string SORT_GRP_DSC_ABS = "Sort Groups Descending - Absolute";

    public ColumnHeaderContextMenu(UltraGrid grid_, string key_, UltraToolbarsManager manager_)
      : base(key_)
    {
      manager_.Tools.Add(this);
      SharedProps.ShowInCustomizer = false;
      SharedProps.Caption = key_;
      _manager = manager_;
      _grid = grid_;
      InitializeComponent();
    }

    /// <summary>
    /// Initialises the context menu GUI components.
    /// </summary>
    protected void InitializeComponent()
    {
      _menuItemFreezeSort = new FreezeSortMenuItem(_grid);
      _menuItemWrapHeader = new WrapHeaderMenuItem(_grid);

      // appends menu items for formatting
      _formatting = new FormattingMenuHelper(_manager, Tools);
      _formatting.OnHandleFormatting += HandleFormattingElementUpdate;

      _manager.Tools.AddRange(new ToolBase[] { _menuItemFreezeSort, _menuItemWrapHeader });
      _menuItemFreezeSort = (FreezeSortMenuItem)Tools.AddTool(FreezeSortMenuItem.KEY);
      _menuItemWrapHeader = (WrapHeaderMenuItem)Tools.AddTool(WrapHeaderMenuItem.KEY);

      // Sorting
      OptionSet optionSet = new OptionSet(SORT_SETKEY);
      _manager.OptionSets.Add(optionSet);

      // No sort
      _menuItemNoSort = new StateButtonTool(SORT_NO, SORT_SETKEY) { MenuDisplayStyle = StateButtonMenuDisplayStyle.DisplayCheckmark };
      _menuItemNoSort.SharedProps.Caption = SORT_NO;
      _menuItemNoSort.SharedProps.ShowInCustomizer = false;
      _menuItemNoSort.InitializeChecked(true);

      // Standard sort
      _menuItemSortAsc = new StateButtonTool(SORT_ASC, SORT_SETKEY) { MenuDisplayStyle = StateButtonMenuDisplayStyle.DisplayCheckmark };
      _menuItemSortAsc.SharedProps.Caption = SORT_ASC;
      _menuItemSortAsc.SharedProps.ShowInCustomizer = false;

      _menuItemSortDes = new StateButtonTool(SORT_DSC, SORT_SETKEY) { MenuDisplayStyle = StateButtonMenuDisplayStyle.DisplayCheckmark };
      _menuItemSortDes.SharedProps.Caption = SORT_DSC;
      _menuItemSortDes.SharedProps.ShowInCustomizer = false;
      _menuItemSortDes.InstanceProps.IsFirstInGroup = true;

      _manager.Tools.AddRange(new [] { _menuItemSortAsc, _menuItemSortDes });
      Tools.AddRange(new [] { _menuItemSortAsc, _menuItemSortDes });      

      // Absolute sort for doubles
      _menuItemSortAscAbs = new StateButtonTool(SORT_ASC_ABS, SORT_SETKEY);
      _menuItemSortAscAbs.SharedProps.Caption = SORT_ASC_ABS;
      _menuItemSortAscAbs.SharedProps.ShowInCustomizer = false;

      _menuItemSortDesAbs = new StateButtonTool(SORT_DSC_ABS, SORT_SETKEY);
      _menuItemSortDesAbs.SharedProps.Caption = SORT_DSC_ABS;
      _menuItemSortDesAbs.SharedProps.ShowInCustomizer = false;

      _manager.Tools.AddRange(new [] { _menuItemSortAscAbs, _menuItemSortDesAbs });
      Tools.AddRange(new [] { _menuItemSortAscAbs, _menuItemSortDesAbs });

      // Standard group sort
      _menuItemSortGrpAsc = new StateButtonTool(SORT_GRP_ASC, SORT_SETKEY) { MenuDisplayStyle = StateButtonMenuDisplayStyle.DisplayCheckmark };      
      _menuItemSortGrpAsc.SharedProps.Caption = SORT_GRP_ASC;
      _menuItemSortGrpAsc.SharedProps.ShowInCustomizer = false;

      _menuItemSortGrpDes = new StateButtonTool(SORT_GRP_DSC, SORT_SETKEY) { MenuDisplayStyle = StateButtonMenuDisplayStyle.DisplayCheckmark };
      _menuItemSortGrpDes.SharedProps.Caption = SORT_GRP_DSC;
      _menuItemSortGrpDes.SharedProps.ShowInCustomizer = false;

      _manager.Tools.AddRange(new [] { _menuItemSortGrpAsc, _menuItemSortGrpDes });
      Tools.AddRange(new [] { _menuItemSortGrpAsc, _menuItemSortGrpDes });

      // Absolute sort for doubles
      _menuItemSortGrpAscAbs = new StateButtonTool(SORT_GRP_ASC_ABS, SORT_SETKEY) { MenuDisplayStyle = StateButtonMenuDisplayStyle.DisplayCheckmark };
      _menuItemSortGrpAscAbs.SharedProps.Caption = SORT_GRP_ASC_ABS;
      _menuItemSortGrpAscAbs.SharedProps.ShowInCustomizer = false;

      _menuItemSortGrpDesAbs = new StateButtonTool(SORT_GRP_DSC_ABS, SORT_SETKEY) { MenuDisplayStyle = StateButtonMenuDisplayStyle.DisplayCheckmark };
      _menuItemSortGrpDesAbs.SharedProps.Caption = SORT_GRP_DSC_ABS;
      _menuItemSortGrpDesAbs.SharedProps.ShowInCustomizer = false;

      _manager.Tools.AddRange(new[] { _menuItemSortGrpAscAbs, _menuItemSortGrpDesAbs });
      Tools.AddRange(new[] { _menuItemSortGrpAscAbs, _menuItemSortGrpDesAbs });

      // No sorting
      _manager.Tools.Add(_menuItemNoSort);
      Tools.Add(_menuItemNoSort);

      // Add separators
      Tools[SORT_ASC].InstanceProps.IsFirstInGroup = true;
      Tools[SORT_GRP_ASC].InstanceProps.IsFirstInGroup = true;
      Tools[SORT_NO].InstanceProps.IsFirstInGroup = true;

      SubsribeSortItemsOnClick();
    }

    public void RefreshMenu(ColumnHeader columnHeader_, DisplayMode displayMode_)
    {
      _displayMode = displayMode_;
      _columnHeader = columnHeader_;
      _column = columnHeader_.Column;
      _columnFormatString = _columnHeader.Column.Format;
      _columnFormat = new ColumnFormat(_columnFormatString, columnHeader_.Column.DataType);

      _menuItemFreezeSort.InstanceProps.IsFirstInGroup = _formatting.PopulateFromFormat(_columnHeader.Column.DataType, _columnFormat.GetUnits(), (int)_columnFormat.GetPrecision(), _columnFormat.HasCommas());

      // Standard sort
      _menuItemSortAsc.SharedProps.Visible = false;
      _menuItemSortDes.SharedProps.Visible = false;
      _menuItemSortAscAbs.SharedProps.Visible = false;
      _menuItemSortDesAbs.SharedProps.Visible = false;
      if ((_displayMode & DisplayMode.Sorting) == DisplayMode.Sorting)
      {
        _menuItemSortAsc.SharedProps.Visible = true;
        _menuItemSortDes.SharedProps.Visible = true;

        // Absolute sort for doubles
        if (_columnHeader.Column.DataType == typeof(double))
        {
          _menuItemSortAscAbs.SharedProps.Visible = true;
          _menuItemSortDesAbs.SharedProps.Visible = true;
        }
      }

      // Standard group sort
      _menuItemSortGrpAsc.SharedProps.Visible = false;
      _menuItemSortGrpDes.SharedProps.Visible = false;
      _menuItemSortGrpAscAbs.SharedProps.Visible = false;
      _menuItemSortGrpDesAbs.SharedProps.Visible = false;
      if ((_displayMode & DisplayMode.GroupSorting) == DisplayMode.GroupSorting)
      {        
        _menuItemSortGrpAsc.SharedProps.Visible = true;
        _menuItemSortGrpDes.SharedProps.Visible = true;

        // Absolute sort for doubles
        if (_columnHeader.Column.DataType == typeof(double))
        {
          _menuItemSortGrpAscAbs.SharedProps.Visible = true;
          _menuItemSortGrpDesAbs.SharedProps.Visible = true;
        }
      }

      UnSubsribeSortItemsOnClick();
      HighlightMenuSort();
      SubsribeSortItemsOnClick();
    }

    private void UnSubsribeSortItemsOnClick()
    {
      _menuItemNoSort.ToolClick -= OnSortByItemClick;
      _menuItemSortAsc.ToolClick -= OnSortByItemClick;
      _menuItemSortDes.ToolClick -= OnSortByItemClick;
      _menuItemSortAscAbs.ToolClick -= OnSortByItemClick;
      _menuItemSortDesAbs.ToolClick -= OnSortByItemClick;
      _menuItemSortGrpAsc.ToolClick -= OnSortByItemClick;
      _menuItemSortGrpDes.ToolClick -= OnSortByItemClick;
      _menuItemSortGrpAscAbs.ToolClick -= OnSortByItemClick;
      _menuItemSortGrpDesAbs.ToolClick -= OnSortByItemClick;
    }

    private void SubsribeSortItemsOnClick()
    {
      _menuItemNoSort.ToolClick += OnSortByItemClick;
      _menuItemSortAsc.ToolClick += OnSortByItemClick;
      _menuItemSortDes.ToolClick += OnSortByItemClick;
      _menuItemSortAscAbs.ToolClick += OnSortByItemClick;
      _menuItemSortDesAbs.ToolClick += OnSortByItemClick;
      _menuItemSortGrpAsc.ToolClick += OnSortByItemClick;
      _menuItemSortGrpDes.ToolClick += OnSortByItemClick;
      _menuItemSortGrpAscAbs.ToolClick += OnSortByItemClick;
      _menuItemSortGrpDesAbs.ToolClick += OnSortByItemClick;
    }

    private void HandleFormattingElementUpdate(FormattingMenuHelper.FormatElement elementType_)
    {
      ApplyFormat();
      if (FormattingMenuHelper.FormatElement.Units == elementType_)
      {
        // as units information affects headers - need to handle this case
        UpdateColumnHeaders();
      }
    }

    private void UpdateColumnHeaders()
    {
      foreach (UltraGridBand band in _grid.DisplayLayout.Bands)
      {
        band.ColHeaderLines = 1;

        foreach (UltraGridColumn column in band.Columns)
        {
          if (column.Hidden)
          {
            continue;
          }

          if (column.DataType == typeof(double))
          {
            var columnFormat = new ColumnFormat(column.Format, column.DataType);

            string oldCaption = column.Header.Caption;

            int index = oldCaption.IndexOf("\n");
            string newCaption = index >= 0 ? column.Header.Caption.Substring(0, index) : string.Copy(oldCaption);

            ColumnFormat.UnitType unitType = columnFormat.GetUnits();

            if (unitType != ColumnFormat.UnitType.One)
            {
              string units = columnFormat.GetUnitsString();
              newCaption += String.Format("\n({0})", units);
              if (band.ColHeaderLines == 1)
              {
                band.ColHeaderLines = 2;
              }
            }

            if (oldCaption != newCaption)
            {
              column.Header.Caption = newCaption;
            }
          }
        }
      }
    }

    public void ApplyFormat()
    {
      string formatString = _columnFormat.GenerateFormatString(_formatting.HasCommas(),
                                                               (ColumnFormat.PrecisionType)
                                                               _formatting.GetPrecision(), _formatting.GetUnits());
      _columnFormat = new ColumnFormat(formatString, _columnHeader.Column.DataType);

      foreach (UltraGridBand band in _columnHeader.Column.Band.Layout.Grid.DisplayLayout.Bands)
      {

        if (_column == null)
        {
          _log.Info("ApplyFormat", string.Format("Column is null while applying format to band {0}", band.Index));
          continue;
        }

        UltraGridColumn columnToFormat;

        try
        {

          _log.Info("ApplyFormat", string.Format("Applying formatting string {2} on band {0} for column {1}",
                                                 band.Index, _column.Key, formatString));

          if (band.Columns.Exists(_column.Key))
          {
            columnToFormat = band.Columns[_column.Key];
            columnToFormat.Format = formatString;
          }
          else
          {
            _log.Info("ApplyFormat", "Column does not exist in this band");
          }

        }
        catch (Exception e)
        {
          _log.Error("ApplyFormat", "Erorr while applying formatting to ", e);
          MessageBox.Show("Error while applying formatting please try to reapply", "Error");
          return;
        }
      }      

      if (FormatApplied != null)
      {
        FormatApplied(this, formatString);
      }
    }

    /// <summary>
    /// Handles the menu item click event and applies the selected sort option.
    /// </summary>
    /// <param name="sender_">The MenuItem that has been clicked.</param>
    /// <param name="e_">Empty event args.</param>
    private void OnSortByItemClick(object sender_, ToolClickEventArgs e_)
    {
      ButtonTool menuItem = sender_ as ButtonTool;

      switch (menuItem.CaptionResolved)
      {
        case SORT_ASC:
          ApplySort(false, false, false);
          break;
        case SORT_DSC:
          ApplySort(true, false, false);
          break;
        case SORT_ASC_ABS:
          ApplySort(false, true, false);
          break;
        case SORT_DSC_ABS:
          ApplySort(true, true, false);
          break;
        case SORT_GRP_ASC:
          ApplySort(false, false, true);
          break;
        case SORT_GRP_DSC:
          ApplySort(true, false, true);
          break;
        case SORT_GRP_ASC_ABS:
          ApplySort(false, true, true);
          break;
        case SORT_GRP_DSC_ABS:
          ApplySort(true, true, true);
          break;
        default:
          ClearSort();
          break;
      }
    }

    /// <summary>
    /// Checks the current sort option and then places a check next to the relevant MenuItem option.
    /// This method is depending on what sort method(ApplySort) you applied to the grid.
    /// </summary>
    public virtual void HighlightMenuSort()
    {
      if (_columnHeader.Column.SortComparer is GridColumnComparer)
      {
        if (((GridColumnComparer)_columnHeader.Column.SortComparer).Absolute)
        {
          switch (_columnHeader.Column.SortIndicator)
          {
            case SortIndicator.Ascending:
              _menuItemSortAscAbs.Checked = true;
              break;
            case SortIndicator.Descending:
              _menuItemSortDesAbs.Checked = true;
              break;
            default:
              _menuItemNoSort.Checked = true;
              break;
          }
        }
        else
        {
          switch (_columnHeader.Column.SortIndicator)
          {
            case SortIndicator.Ascending:
              _menuItemSortAsc.Checked = true;
              break;
            case SortIndicator.Descending:
              _menuItemSortDes.Checked = true;
              break;
            default:
              _menuItemNoSort.Checked = true;
              break;
          }
        }
      }
      else
      {
        _menuItemNoSort.Checked = true;
      }
    }

    /// <summary>
    /// Applies the sort to the selected column according to the given sort parameters.
    /// </summary>
    /// <param name="descending_">True if to sort descending, false if ascending.</param>
    /// <param name="absolute_">True if the sort should be based on absolute values.</param>
    /// <param name="sortGroups_">Not supported</param>
    protected virtual void ApplySort(bool descending_, bool absolute_, bool sortGroups_)
    {
      ApplySort(_grid.Rows[0], _columnHeader.Column.Key, descending_, sortGroups_, absolute_,
                (Control.ModifierKeys & Keys.Shift) == Keys.Shift);
    }


    /// <summary>
    /// Applies the sort to the specified grid column, recursively down the grid.
    /// </summary>
    /// <param name="row_">The current row for the <see cref="UltraGridBand"/> being sorted.</param>
    /// <param name="column_">The column key being sorted.</param>
    /// <param name="descending_">if set to <c>true</c> sort descending.</param>
    /// <param name="sortGroups_">if sort on group or on record row</param>
    /// <param name="absolute_">if set to <c>true</c> sort on the absolute numeric values.</param>
    /// <param name="multiSort_">if set to <c>true</c> sort on multiple columns at once.</param>
    private void ApplySort(UltraGridRow row_, string column_,bool descending_, bool sortGroups_,
                                  bool absolute_, bool multiSort_)
    {
      if (row_ == null || row_.ListObject == null)
      {
        return;
      }

      bool isRecord = !row_.HasChild();

      if (isRecord && sortGroups_)
      {
        return;
      }

      if (!isRecord)
      {
        ApplySort(row_.GetChild(ChildRow.First), column_, descending_, false, absolute_, multiSort_);
      }

      if (!isRecord && !sortGroups_)
      {
        return;
      }

      // find the right band, apply sort.
      UltraGridBand band = row_.Band;

      if (!multiSort_)
      {
        band.SortedColumns.Clear();
      }

      UltraGridColumn columnToAdd = band.Columns.Exists(column_) ? band.Columns[column_] : null;

      if (null != columnToAdd)
      {
        if (band.SortedColumns.Contains(columnToAdd))
        {
          band.SortedColumns.Remove(columnToAdd);
        }

        if (!absolute_ || columnToAdd.Index == 0)
        {
          if (columnToAdd.Index == 0 && columnToAdd.SortComparer != null) { } // Do not overwrite custom comparers
          else
          {
            columnToAdd.SortComparer = new GridColumnComparer();
          }
        }
        else
        {
          columnToAdd.SortComparer = new GridColumnComparer(true);
        }

        band.SortedColumns.Add(columnToAdd, descending_);
      }
    }



    /// <summary>
    /// Clears the selected sort.
    /// </summary>
    protected void ClearSort()
    {
      _columnHeader.Column.SortIndicator = SortIndicator.None;
      HighlightMenuSort();
    }
  }
}