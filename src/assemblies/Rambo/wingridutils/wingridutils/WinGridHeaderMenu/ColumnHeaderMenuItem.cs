﻿using System.Windows.Forms;
using UltraWinGridColumnHeader = Infragistics.Win.UltraWinGrid.ColumnHeader;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils.WinGridHeaderMenu
{
  /// <summary>
  /// Menu item in a ColumnHeader context menu that has knowledge of which column header it was created from
  /// </summary>
  public class ColumnHeaderMenuItem : MenuItem
  {
    public readonly UltraWinGridColumnHeader ColumnHeader;

    public ColumnHeaderMenuItem(UltraWinGridColumnHeader columnHeader_, string text_)
      : base(text_)
    {
      ColumnHeader = columnHeader_;
    }

    public ColumnHeaderMenuItem(UltraWinGridColumnHeader columnHeader_, string text_,
                                System.EventHandler callback_)
      : base(text_, callback_)
    {
      ColumnHeader = columnHeader_;
    }

    public ColumnHeaderMenuItem(UltraWinGridColumnHeader columnHeader_, string text_,
                                System.EventHandler callback_, Shortcut shortcut_)
      : base(text_, callback_, shortcut_)
    {
      ColumnHeader = columnHeader_;
    }


  }
}