﻿using System;
using Infragistics.Win.UltraWinToolbars;
using MorganStanley.MSDesktop.Rambo.WinGridUtils;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils.WinGridHeaderMenu
{
  public class FormattingMenuHelper
  {
    public delegate void HandleFormattingUpdate(FormatElement element_);

    public enum FormatElement
    {
      Precision,
      Units,
      Comma
    }

    protected UltraToolbarsManager _manager;
    protected PopupMenuTool _menuItemFormatUnits;
    protected PopupMenuTool _menuItemFormatPrecision;
    protected StateButtonTool _menuItemFormatComma;

    public event HandleFormattingUpdate OnHandleFormatting;

    public string FormatString
    {
      get
      {
        const string dummyFormat = "0.00";
        ColumnFormat format = new ColumnFormat(dummyFormat, typeof(double));
        return format.GenerateFormatString(HasCommas(), (ColumnFormat.PrecisionType)GetPrecision(), GetUnits());
      }
    }

    public PopupMenuTool UnitsMenuItem
    {
      get { return _menuItemFormatUnits; }
    }

    public PopupMenuTool PrecisionMenuItem
    {
      get { return _menuItemFormatPrecision; }
    }

    public StateButtonTool CommasMenuItem
    {
      get { return _menuItemFormatComma; }
    }

    public FormattingMenuHelper(UltraToolbarsManager manager_, ToolsCollectionBase items_)
    {
      _manager = manager_;

      _menuItemFormatUnits = new PopupMenuTool("Units");
      _menuItemFormatUnits.SharedProps.Caption = "Units";
      _menuItemFormatUnits.SharedProps.ShowInCustomizer = false;

      _menuItemFormatPrecision = new PopupMenuTool("Precision");
      _menuItemFormatPrecision.SharedProps.Caption = "Precision";
      _menuItemFormatPrecision.SharedProps.ShowInCustomizer = false;

      _menuItemFormatComma = new StateButtonTool("Comma")
                               {
                                 MenuDisplayStyle = StateButtonMenuDisplayStyle.DisplayCheckmark
                               };
      _menuItemFormatComma.SharedProps.Caption = "Comma";
      _menuItemFormatComma.SharedProps.ShowInCustomizer = false;
      _menuItemFormatComma.ToolClick += new ToolClickEventHandler(OnFormatCommaMenuItemClicked);

      try
      {
        CreateFormatUnitsMenuItem();
        CreateFormatPrecisionMenuItem();
      }
      catch (Exception)
      {
        // If an unsupported column format string is encountered, assume that no-editing facilities 
        // should be enabled and disable the appropriate menu items.
        _menuItemFormatUnits.SharedProps.Enabled = false;
        _menuItemFormatPrecision.SharedProps.Enabled = false;
        _menuItemFormatComma.SharedProps.Enabled = false;
      }

      manager_.Tools.AddRange(new ToolBase[] { _menuItemFormatUnits, _menuItemFormatPrecision, _menuItemFormatComma });
      items_.AddRange(new ToolBase[] { _menuItemFormatUnits, _menuItemFormatPrecision, _menuItemFormatComma });
    }

    public bool PopulateFromFormat(Type columnType_, ColumnFormat.UnitType units_, int precision_, bool hasComma_)
    {
      var isFormatItemsVisible = columnType_ == typeof(double);

      if (isFormatItemsVisible)
      {
        RefreshUnitsMenuItem(units_);
        RefreshPrecisionMenuItem(precision_);
        RefreshFormatCommaMenuItem(hasComma_);
      }

      // Allow precision for doubles
      _menuItemFormatUnits.SharedProps.Visible = isFormatItemsVisible;
      _menuItemFormatPrecision.SharedProps.Visible = isFormatItemsVisible;
      _menuItemFormatComma.SharedProps.Visible = isFormatItemsVisible;

      return isFormatItemsVisible;
    }

    protected void RefreshUnitsMenuItem(ColumnFormat.UnitType unitType_)
    {
      switch (unitType_)
      {
        case ColumnFormat.UnitType.One:
          ((StateButtonTool)_menuItemFormatUnits.Tools["one"]).InitializeChecked(true);
          break;
        case ColumnFormat.UnitType.Thousand:
          ((StateButtonTool)_menuItemFormatUnits.Tools["thousand"]).InitializeChecked(true);
          break;
        case ColumnFormat.UnitType.Million:
          ((StateButtonTool)_menuItemFormatUnits.Tools["million"]).InitializeChecked(true);
          break;
        case ColumnFormat.UnitType.Percentage:
          ((StateButtonTool)_menuItemFormatUnits.Tools["percent"]).InitializeChecked(true);
          break;
        default:
          break;
      }
    }

    protected void CreateFormatUnitsMenuItem()
    {
      const string STATESET = "FormatUnits";

      OptionSet optionSet = new OptionSet(STATESET);
      _manager.OptionSets.Add(optionSet);

      StateButtonTool one = new StateButtonTool("one", STATESET)
                              {
                                MenuDisplayStyle = StateButtonMenuDisplayStyle.DisplayCheckmark
                              };
      one.SharedProps.Caption = ColumnFormat.ConvertUnitTypeToString(ColumnFormat.UnitType.One);
      one.SharedProps.ShowInCustomizer = false;
      one.ToolClick += new ToolClickEventHandler(OnFormatUnitsMenuItemClicked);

      StateButtonTool thousand = new StateButtonTool("thousand", STATESET)
                              {
                                MenuDisplayStyle = StateButtonMenuDisplayStyle.DisplayCheckmark
                              };
      thousand.SharedProps.Caption = ColumnFormat.ConvertUnitTypeToString(ColumnFormat.UnitType.Thousand);
      thousand.SharedProps.ShowInCustomizer = false;
      thousand.ToolClick += new ToolClickEventHandler(OnFormatUnitsMenuItemClicked);

      StateButtonTool million = new StateButtonTool("million", STATESET)
                              {
                                MenuDisplayStyle = StateButtonMenuDisplayStyle.DisplayCheckmark
                              };
      million.SharedProps.Caption = ColumnFormat.ConvertUnitTypeToString(ColumnFormat.UnitType.Million);
      million.SharedProps.ShowInCustomizer = false;
      million.ToolClick += new ToolClickEventHandler(OnFormatUnitsMenuItemClicked);

      StateButtonTool percent = new StateButtonTool("percent", STATESET)
                              {
                                MenuDisplayStyle = StateButtonMenuDisplayStyle.DisplayCheckmark
                              };
      percent.SharedProps.Caption = ColumnFormat.ConvertUnitTypeToString(ColumnFormat.UnitType.Percentage);
      percent.SharedProps.ShowInCustomizer = false;
      percent.ToolClick += new ToolClickEventHandler(OnFormatUnitsMenuItemClicked);

      _manager.Tools.AddRange(new ToolBase[] { one, thousand, million, percent });
      _menuItemFormatUnits.Tools.AddRange(new ToolBase[] { one, thousand, million, percent });
    }

    protected void RefreshPrecisionMenuItem(int precison_)
    {
      ((StateButtonTool)_menuItemFormatPrecision.Tools[precison_.ToString()]).InitializeChecked(true);
    }

    protected void CreateFormatPrecisionMenuItem()
    {
      const string STATEKEY = "FormatPrecision";

      OptionSet optionSet = new OptionSet(STATEKEY);
      _manager.OptionSets.Add(optionSet);

      for (int i = 0; i <= 8; ++i)
      {
        StateButtonTool item = new StateButtonTool(i.ToString(), STATEKEY)
                              {
                                MenuDisplayStyle = StateButtonMenuDisplayStyle.DisplayCheckmark
                              };
        item.SharedProps.Caption = i.ToString();
        item.SharedProps.ShowInCustomizer = false;
        item.ToolClick += new ToolClickEventHandler(OnFormatPrecisionMenuItemClicked);

        _manager.Tools.Add(item);
        _menuItemFormatPrecision.Tools.Add(item);
      }
    }

    protected void RefreshFormatCommaMenuItem(bool hasCommas_)
    {
      _menuItemFormatComma.InitializeChecked(hasCommas_);
    }

    public int GetPrecision()
    {
      foreach (StateButtonTool item in _menuItemFormatPrecision.Tools)
      {
        if (item.Checked)
        {
          return int.Parse(item.CaptionResolved);
        }
      }

      return -1;
    }

    public ColumnFormat.UnitType GetUnits()
    {
      foreach (StateButtonTool item in _menuItemFormatUnits.Tools)
      {
        if (item.Checked)
        {
          return ColumnFormat.ConvertStringToUnitType(item.CaptionResolved);
        }
      }
      return ColumnFormat.UnitType.One;
    }

    public bool HasCommas()
    {
      return _menuItemFormatComma.Checked;
    }

    protected string GetUnitsString()
    {
      foreach (StateButtonTool item in _menuItemFormatUnits.Tools)
      {
        if (item.Checked)
        {
          return item.CaptionResolved;
        }
      }
      return string.Empty;
    }

    protected void OnFormatUnitsMenuItemClicked(object sender_, ToolClickEventArgs e_)
    {
      if (OnHandleFormatting != null)
      {
        OnHandleFormatting(FormatElement.Units);
      }
    }

    protected void OnFormatPrecisionMenuItemClicked(object sender_, ToolClickEventArgs e_)
    {
      if (OnHandleFormatting != null)
      {
        OnHandleFormatting(FormatElement.Precision);
      }
    }

    protected void OnFormatCommaMenuItemClicked(object sender_, ToolClickEventArgs e_)
    {
      if (OnHandleFormatting != null)
      {
        OnHandleFormatting(FormatElement.Comma);
      }
    }
  }
}
