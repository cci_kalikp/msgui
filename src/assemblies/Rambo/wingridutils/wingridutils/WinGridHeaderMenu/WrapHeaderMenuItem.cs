﻿using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinToolbars;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils.WinGridHeaderMenu
{
  public class WrapHeaderMenuItem : StateButtonTool
  {
    public const string KEY = "Wrap Column Headers";
    public const string SETKEY = "Wrap Column Headers";
    public const string CAPTION = "Wrap Column Headers";

    readonly UltraGrid _grid;

    public WrapHeaderMenuItem(UltraGrid grid_)
      : base(KEY, SETKEY)
    {
      this.SharedProps.Caption = CAPTION;
      this.SharedProps.ShowInCustomizer = false;
      this.MenuDisplayStyle = StateButtonMenuDisplayStyle.DisplayCheckmark;
      _grid = grid_;
      //Checked = Preferences.Instance.WrapColumnHeaders;
      this.ToolClick += new ToolClickEventHandler(ToggleWrapColumnHeaders);
    }

    private void ToggleWrapColumnHeaders(object sender_, ToolClickEventArgs e_)
    {
      SetWrapColumnHeadersPreference(Checked);
      DoWrapColumnHeaders(Checked);
    }

    private static void SetWrapColumnHeadersPreference(bool wrap_)
    {
      //Preferences.Instance.BeginEdit();
      //Preferences.Instance.WrapColumnHeaders = wrap_;
      //Preferences.Instance.EndEdit();
    }

    private void DoWrapColumnHeaders(bool wrap_)
    {
      DefaultableBoolean dbool = wrap_ ? DefaultableBoolean.True : DefaultableBoolean.False;
      _grid.DisplayLayout.Override.WrapHeaderText = dbool;
    }

    /// <summary>
    /// http://news.infragistics.com/forums/p/11286/43440.aspx
    /// </summary>
    /// <param name="cloneNewInstance"></param>
    /// <returns></returns>
    protected override ToolBase Clone(bool cloneNewInstance)
    {
      WrapHeaderMenuItem newTool = new WrapHeaderMenuItem(_grid);
      newTool.InitializeFrom(this, cloneNewInstance);
      return newTool;
    }
  }  
}
