﻿using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinToolbars;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils.WinGridHeaderMenu
{
  /// <summary>
  /// Simple menu item for providing grid sort freeze functionality.
  /// </summary>
  public class FreezeSortMenuItem : StateButtonTool
  {
    public const string KEY = "Freeze Current Sort Settings";
    public const string SETKEY = "Freeze Current Sort Settings";
    public const string CAPTION = "Freeze Current Sort Settings";

    private readonly UltraGrid grid;

    public FreezeSortMenuItem(UltraGrid grid)
      : base(KEY, SETKEY)
    {
      this.grid = grid;
      this.SharedProps.Caption = CAPTION;
      this.SharedProps.ShowInCustomizer = false;
      this.MenuDisplayStyle = StateButtonMenuDisplayStyle.DisplayCheckmark;
      
      this.ToolClick += new ToolClickEventHandler(ToggleSortFreeze);
 
      SetCheckedFlag();
    }

    private void ToggleSortFreeze(object sender_, ToolClickEventArgs e_)
    {
      //Preferences.Instance.BeginEdit();
      //Preferences.Instance.FreezeSort = !Preferences.Instance.FreezeSort;
      //Preferences.Instance.EndEdit();

      if (grid != null)
      {
        grid.DisplayLayout.Override.HeaderClickAction = Checked ? HeaderClickAction.Select : HeaderClickAction.SortMulti;
      }
    }

    internal void SetCheckedFlag()
    {
      //this.Checked = RVPreferences.Instance.FreezeSort;
      if (grid != null)
      {
        Checked = grid.DisplayLayout.Override.HeaderClickAction == HeaderClickAction.Select;
      }
    }
  
    /// <summary>
    /// http://news.infragistics.com/forums/p/11286/43440.aspx
    /// </summary>
    /// <param name="cloneNewInstance"></param>
    /// <returns></returns>
    protected override ToolBase Clone(bool cloneNewInstance)
    {
      FreezeSortMenuItem newTool = new FreezeSortMenuItem(this.grid);
      newTool.InitializeFrom(this, cloneNewInstance);
      return newTool;
    }
  }
}