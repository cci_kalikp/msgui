using System;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
	/// <summary>
	/// Save Notification Event Arguements used by the ResourceManager.
	/// </summary>
	public class ResourceSaveNotifyArgs
	{
    private string _key;
    private object _value;

		public ResourceSaveNotifyArgs(string key_, object value_)
		{
      _key = key_;
      _value = value_;
		}

    public string Key
    {
      get
      {
        return _key;
      }
    }

    public object Value
    {
      get
      {
        return _value;
      }
    }
	}
}
