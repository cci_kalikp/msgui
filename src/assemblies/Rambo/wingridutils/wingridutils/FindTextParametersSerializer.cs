namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  using System.Xml.Serialization;
  using System;
    
    
  /// <summary>Custom serializer for FindTextParameters type.</summary>
  internal class FindTextParametersSerializer : System.Xml.Serialization.XmlSerializer
  {
        
        
    FindTextReader _reader;
    FindTextWriter _writer;

    /// <summary>Constructs the serializer with default reader and writer instances.</summary>
    public FindTextParametersSerializer()
    {
    }

    /// <summary>Constructs the serializer with a pre-built reader.</summary>
    public FindTextParametersSerializer(FindTextReader reader)
    {
      _reader = reader;
    }

    /// <summary>Constructs the serializer with a pre-writer reader.</summary>
    public FindTextParametersSerializer(FindTextWriter writer)
    {
      _writer = writer;
    }

    /// <summary>Constructs the serializer with pre-built reader and writer.</summary>
    public FindTextParametersSerializer(FindTextReader reader, FindTextWriter writer)
    {
      _reader = reader;
      _writer = writer;
    }

    /// <summary>See <see cref="XmlSerializer.CreateReader"/>.</summary>
    protected override XmlSerializationReader CreateReader()
    {
      if (_reader != null) 
        return _reader;
      else
        return new FindTextReader();
    }

    /// <summary>See <see cref="XmlSerializer.CreateWriter"/>.</summary>
    protected override XmlSerializationWriter CreateWriter()
    {
      if (_writer != null) 
        return _writer;
      else
        return new FindTextWriter();
    }

    /// <summary>See <see cref="XmlSerializer.Deserialize"/>.</summary>
    protected override object Deserialize(XmlSerializationReader reader)
    {
      if (!(reader is FindTextReader))
        throw new ArgumentException("reader");

      return ((FindTextReader)reader).Read();
    }

    /// <summary>See <see cref="XmlSerializer.Serialize"/>.</summary>
    protected override void Serialize(object o, XmlSerializationWriter writer)
    {
      if (!(writer is FindTextWriter))
        throw new ArgumentException("writer");

      ((FindTextWriter)writer).Write((MorganStanley.MSDesktop.Rambo.WinGridUtils.FindTextParameters)o);
    }
    public class Reader : System.Xml.Serialization.XmlSerializationReader 
    {

      /// <remarks/>
      protected virtual MorganStanley.MSDesktop.Rambo.WinGridUtils.FindTextParameters Read1_FindTextParameters(bool isNullable, bool checkType) 
      {
        if (isNullable && ReadNull()) return null;
        if (checkType) 
        {
          System.Xml.XmlQualifiedName t = GetXsiType();
          if (t == null || ((object) ((System.Xml.XmlQualifiedName)t).Name == (object)id1_FindTextParameters && (object) ((System.Xml.XmlQualifiedName)t).Namespace == (object)id2_Item))
            ;
          else
            throw CreateUnknownTypeException((System.Xml.XmlQualifiedName)t);
        }
        MorganStanley.MSDesktop.Rambo.WinGridUtils.FindTextParameters o = new MorganStanley.MSDesktop.Rambo.WinGridUtils.FindTextParameters();
        while (Reader.MoveToNextAttribute()) 
        {
          if (!IsXmlnsAttribute(Reader.Name)) 
          {
            UnknownNode((object)o);
          }
        }
        Reader.MoveToElement();
        if (Reader.IsEmptyElement) 
        {
          Reader.Skip();
          return o;
        }
        Reader.ReadStartElement();
        bool[] paramsRead = new bool[6];
        Reader.MoveToContent();
        while (Reader.NodeType != System.Xml.XmlNodeType.EndElement) 
        {
          if (Reader.NodeType == System.Xml.XmlNodeType.Element) 
          {
            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id3_CaseSensitive && (object) Reader.NamespaceURI == (object)id2_Item)) 
            {
              o.@CaseSensitive = System.Xml.XmlConvert.ToBoolean(Reader.ReadElementString());
              paramsRead[0] = true;
            }
            else if (!paramsRead[1] && ((object) Reader.LocalName == (object)id4_ReverseSearch && (object) Reader.NamespaceURI == (object)id2_Item)) 
            {
              o.@ReverseSearch = System.Xml.XmlConvert.ToBoolean(Reader.ReadElementString());
              paramsRead[1] = true;
            }
            else if (!paramsRead[2] && ((object) Reader.LocalName == (object)id5_SearchCollapsed && (object) Reader.NamespaceURI == (object)id2_Item)) 
            {
              o.@SearchCollapsed = System.Xml.XmlConvert.ToBoolean(Reader.ReadElementString());
              paramsRead[2] = true;
            }
            else if (!paramsRead[3] && ((object) Reader.LocalName == (object)id6_SearchColumn && (object) Reader.NamespaceURI == (object)id2_Item)) 
            {
              o.@SearchColumn = Reader.ReadElementString();
              paramsRead[3] = true;
            }
            else if (!paramsRead[4] && ((object) Reader.LocalName == (object)id7_Match && (object) Reader.NamespaceURI == (object)id2_Item)) 
            {
              o.@Match = Read2_MatchType(Reader.ReadElementString());
              paramsRead[4] = true;
            }
            else if (!paramsRead[5] && ((object) Reader.LocalName == (object)id8_SearchText && (object) Reader.NamespaceURI == (object)id2_Item)) 
            {
              o.@SearchText = Reader.ReadElementString();
              paramsRead[5] = true;
            }
            else 
            {
              UnknownNode((object)o);
            }
          }
          else 
          {
            UnknownNode((object)o);
          }
          Reader.MoveToContent();
        }
        ReadEndElement();
        return o;
      }

      /// <remarks/>
      protected virtual MorganStanley.MSDesktop.Rambo.WinGridUtils.FindTextParameters.MatchType Read2_MatchType(string s) 
      {
        switch (s) 
        {
          case @"AnyPartOfField": return MorganStanley.MSDesktop.Rambo.WinGridUtils.FindTextParameters.MatchType.@AnyPartOfField;
          case @"WholeField": return MorganStanley.MSDesktop.Rambo.WinGridUtils.FindTextParameters.MatchType.@WholeField;
          case @"StartOfField": return MorganStanley.MSDesktop.Rambo.WinGridUtils.FindTextParameters.MatchType.@StartOfField;
          case @"EndOfField": return MorganStanley.MSDesktop.Rambo.WinGridUtils.FindTextParameters.MatchType.@EndOfField;
          default: throw CreateUnknownConstantException(s, typeof(MorganStanley.MSDesktop.Rambo.WinGridUtils.FindTextParameters.MatchType));
        }
      }

      /// <remarks/>
      protected virtual System.Object Read3_Object(bool isNullable, bool checkType) 
      {
        if (isNullable && ReadNull()) return null;
        if (checkType) 
        {
          System.Xml.XmlQualifiedName t = GetXsiType();
          if (t == null)
            return ReadTypedPrimitive(new System.Xml.XmlQualifiedName("anyType", "http://www.w3.org/2001/XMLSchema"));
          else if (((object) ((System.Xml.XmlQualifiedName)t).Name == (object)id1_FindTextParameters && (object) ((System.Xml.XmlQualifiedName)t).Namespace == (object)id2_Item))
            return Read1_FindTextParameters(isNullable, false);
          else if (((object) ((System.Xml.XmlQualifiedName)t).Name == (object)id9_MatchType && (object) ((System.Xml.XmlQualifiedName)t).Namespace == (object)id2_Item)) 
          {
            Reader.ReadStartElement();
            object e = Read2_MatchType(Reader.ReadString());
            ReadEndElement();
            return e;
          }
          else
            return ReadTypedPrimitive((System.Xml.XmlQualifiedName)t);
        }
        System.Object o = new System.Object();
        while (Reader.MoveToNextAttribute()) 
        {
          if (!IsXmlnsAttribute(Reader.Name)) 
          {
            UnknownNode((object)o);
          }
        }
        Reader.MoveToElement();
        if (Reader.IsEmptyElement) 
        {
          Reader.Skip();
          return o;
        }
        Reader.ReadStartElement();
        bool[] paramsRead = new bool[0];
        Reader.MoveToContent();
        while (Reader.NodeType != System.Xml.XmlNodeType.EndElement) 
        {
          if (Reader.NodeType == System.Xml.XmlNodeType.Element) 
          {
            UnknownNode((object)o);
          }
          else 
          {
            UnknownNode((object)o);
          }
          Reader.MoveToContent();
        }
        ReadEndElement();
        return o;
      }
      protected override void InitCallbacks() 
      {
      }

      protected object Read4_FindText() 
      {
        object o = null;
        Reader.MoveToContent();
        if (Reader.NodeType == System.Xml.XmlNodeType.Element) 
        {
          if (((object) Reader.LocalName == (object)id10_FindText && (object) Reader.NamespaceURI == (object)id2_Item)) 
          {
            o = Read1_FindTextParameters(true, true);
          }
          else 
          {
            throw CreateUnknownNodeException();
          }
        }
        else 
        {
          UnknownNode(null);
        }
        return (object)o;
      }

      System.String id2_Item;
      System.String id8_SearchText;
      System.String id6_SearchColumn;
      System.String id7_Match;
      System.String id4_ReverseSearch;
      System.String id10_FindText;
      System.String id9_MatchType;
      System.String id5_SearchCollapsed;
      System.String id1_FindTextParameters;
      System.String id3_CaseSensitive;

      protected override void InitIDs() 
      {
        id2_Item = Reader.NameTable.Add(@"");
        id8_SearchText = Reader.NameTable.Add(@"SearchText");
        id6_SearchColumn = Reader.NameTable.Add(@"SearchColumn");
        id7_Match = Reader.NameTable.Add(@"Match");
        id4_ReverseSearch = Reader.NameTable.Add(@"ReverseSearch");
        id10_FindText = Reader.NameTable.Add(@"FindText");
        id9_MatchType = Reader.NameTable.Add(@"MatchType");
        id5_SearchCollapsed = Reader.NameTable.Add(@"SearchCollapsed");
        id1_FindTextParameters = Reader.NameTable.Add(@"FindTextParameters");
        id3_CaseSensitive = Reader.NameTable.Add(@"CaseSensitive");
      }
    }

    public class Writer : System.Xml.Serialization.XmlSerializationWriter 
    {

      void Write1_FindTextParameters(string n, string ns, MorganStanley.MSDesktop.Rambo.WinGridUtils.FindTextParameters o, bool isNullable, bool needType) 
      {
        if ((object)o == null) 
        {
          if (isNullable) WriteNullTagLiteral(n, ns);
          return;
        }
        if (!needType) 
        {
          System.Type t = o.GetType();
          if (t == typeof(MorganStanley.MSDesktop.Rambo.WinGridUtils.FindTextParameters))
            ;
          else 
          {
            throw CreateUnknownTypeException(o);
          }
        }
        WriteStartElement(n, ns, o);
        if (needType) WriteXsiType(@"FindTextParameters", @"");
        WriteElementStringRaw(@"CaseSensitive", @"", System.Xml.XmlConvert.ToString((System.Boolean)((System.Boolean)o.@CaseSensitive)));
        WriteElementStringRaw(@"ReverseSearch", @"", System.Xml.XmlConvert.ToString((System.Boolean)((System.Boolean)o.@ReverseSearch)));
        WriteElementStringRaw(@"SearchCollapsed", @"", System.Xml.XmlConvert.ToString((System.Boolean)((System.Boolean)o.@SearchCollapsed)));
        WriteElementString(@"SearchColumn", @"", ((System.String)o.@SearchColumn));
        WriteElementString(@"Match", @"", Write2_MatchType(((MorganStanley.MSDesktop.Rambo.WinGridUtils.FindTextParameters.MatchType)o.@Match)));
        WriteElementString(@"SearchText", @"", ((System.String)o.@SearchText));
        WriteEndElement(o);
      }

      string Write2_MatchType(MorganStanley.MSDesktop.Rambo.WinGridUtils.FindTextParameters.MatchType v) 
      {
        string s = null;
        switch (v) 
        {
          case MorganStanley.MSDesktop.Rambo.WinGridUtils.FindTextParameters.MatchType.@AnyPartOfField: s = @"AnyPartOfField"; break;
          case MorganStanley.MSDesktop.Rambo.WinGridUtils.FindTextParameters.MatchType.@WholeField: s = @"WholeField"; break;
          case MorganStanley.MSDesktop.Rambo.WinGridUtils.FindTextParameters.MatchType.@StartOfField: s = @"StartOfField"; break;
          case MorganStanley.MSDesktop.Rambo.WinGridUtils.FindTextParameters.MatchType.@EndOfField: s = @"EndOfField"; break;
          default: s = ((System.Int64)v).ToString(); break;
        }
        return s;
      }

      void Write3_Object(string n, string ns, System.Object o, bool isNullable, bool needType) 
      {
        if ((object)o == null) 
        {
          if (isNullable) WriteNullTagLiteral(n, ns);
          return;
        }
        if (!needType) 
        {
          System.Type t = o.GetType();
          if (t == typeof(System.Object))
            ;
          else if (t == typeof(MorganStanley.MSDesktop.Rambo.WinGridUtils.FindTextParameters)) 
          {
            Write1_FindTextParameters(n, ns, (MorganStanley.MSDesktop.Rambo.WinGridUtils.FindTextParameters)o, isNullable, true);
            return;
          }
          else if (t == typeof(MorganStanley.MSDesktop.Rambo.WinGridUtils.FindTextParameters.MatchType)) 
          {
            Writer.WriteStartElement(n, ns);
            WriteXsiType(@"MatchType", @"");
            Writer.WriteString(Write2_MatchType((MorganStanley.MSDesktop.Rambo.WinGridUtils.FindTextParameters.MatchType)o));
            Writer.WriteEndElement();
            return;
          }
          else 
          {
            WriteTypedPrimitive(n, ns, o, true);
            return;
          }
        }
        WriteStartElement(n, ns, o);
        WriteEndElement(o);
      }

      protected override void InitCallbacks() 
      {
      }

      protected void Write4_FindText(object o) 
      {
        WriteStartDocument();
        if (o == null) 
        {
          WriteNullTagLiteral(@"FindText", @"");
          return;
        }
        TopLevelElement();
        Write1_FindTextParameters(@"FindText", @"", ((MorganStanley.MSDesktop.Rambo.WinGridUtils.FindTextParameters)o), true, false);
      }
    }

    ///<remarks/>
  }
    
  internal class FindTextReader : FindTextParametersSerializer.Reader
  {
        
        
    /// <remarks/>
    protected override MorganStanley.MSDesktop.Rambo.WinGridUtils.FindTextParameters Read1_FindTextParameters(bool isNullable, bool checkType)
    {
      MorganStanley.MSDesktop.Rambo.WinGridUtils.FindTextParameters obj = base.Read1_FindTextParameters(isNullable, checkType);
      FindTextParametersDeserializedHandler handler = FindTextParametersDeserialized;
      if (handler != null)
        handler(obj);

      return obj;
    }
        
    /// <remarks/>
    protected override MorganStanley.MSDesktop.Rambo.WinGridUtils.FindTextParameters.MatchType Read2_MatchType(string s)
    {
      MorganStanley.MSDesktop.Rambo.WinGridUtils.FindTextParameters.MatchType obj = base.Read2_MatchType(s);
      MatchTypeDeserializedHandler handler = MatchTypeDeserialized;
      if (handler != null)
        handler(obj);

      return obj;
    }
        
    /// <summary>Reads an object of type MorganStanley.MSDesktop.Rambo.WinGridUtils.FindTextParameters.</summary>
    internal MorganStanley.MSDesktop.Rambo.WinGridUtils.FindTextParameters Read()
    {
      return (MorganStanley.MSDesktop.Rambo.WinGridUtils.FindTextParameters) Read4_FindText();
    }
    public event FindTextParametersDeserializedHandler FindTextParametersDeserialized;
        
    public event MatchTypeDeserializedHandler MatchTypeDeserialized;
  }
    
  internal delegate void FindTextParametersDeserializedHandler(MorganStanley.MSDesktop.Rambo.WinGridUtils.FindTextParameters findtextparameters);
    
  internal delegate void MatchTypeDeserializedHandler(MorganStanley.MSDesktop.Rambo.WinGridUtils.FindTextParameters.MatchType matchtype);
    
  internal class FindTextWriter : FindTextParametersSerializer.Writer
  {
        
        
    /// <summary>Writes an object of type MorganStanley.MSDesktop.Rambo.WinGridUtils.FindTextParameters.</summary>
    internal void Write(MorganStanley.MSDesktop.Rambo.WinGridUtils.FindTextParameters obj)
    {
      Write4_FindText(obj);
    }
  }
}
