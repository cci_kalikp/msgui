using System;
using System.Windows.Forms;
using Infragistics.Win.UltraWinGrid;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
	/// <summary>
	/// A form for selecting a column of a ctrlViewBase object to navigate to.
	/// </summary>
	public class frmGoToColumn : NavigationUtilFormBase
	{
    #region Data Members

    public enum FormStyles
    {
      ListBox,
      ListView
    }

    private System.Windows.Forms.ListBox _lstColumns;
    private System.Windows.Forms.Button _btnGo;
    private System.Windows.Forms.Button _btnClose;
    private System.Windows.Forms.Label _lblSelectCol;
    private System.Windows.Forms.RadioButton _radAlphabetical;
    private System.Windows.Forms.RadioButton _radColumn;
    private System.Windows.Forms.GroupBox groupBox1;
    private ListView _lsvColumns;
    private System.Windows.Forms.ColumnHeader columnHeader1;
    private System.ComponentModel.IContainer components;
    private FormStyles formStyle = FormStyles.ListBox;

    #endregion

    #region Constructor

		public frmGoToColumn()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
      _xmlSerializer = new GoToColumnParametersSerializer(new GoToColumnReader(), new GoToColumnWriter());
    }

    public frmGoToColumn(FormStyles style)
      : this()
    {
      this.FormStyle = style;
		}

    #endregion

    #region Properties

    public override object State
    {
      get
      {
        GoToColumnParameters state = new GoToColumnParameters();
        state.Column = this.Column;
        state.SortOrder = this.SortOrder;

        return state;
      }
      set
      {
        if(value == null)
        {
          this.SortOrder = GoToColumnParameters.ColumnSortType.ALPHABETIC;
          this.Column = null;
        }
        else
        {
          GoToColumnParameters state = (GoToColumnParameters)value;
          this.SortOrder = state.SortOrder;
          this.Column = state.Column;
        }
      }
    }

    internal string Column
    {
      get 
      {
        return GetColumnKey(_lstColumns.Text);
      }
      set
      {
        string caption = GetCaption(value);

        if(caption != null && _lstColumns.Items.Contains(caption))
        {
          _lstColumns.Text = caption;
        }
      }
    }

    internal GoToColumnParameters.ColumnSortType SortOrder
    {
      get
      {
        if(_radAlphabetical.Checked)
        {
          return GoToColumnParameters.ColumnSortType.ALPHABETIC;
        }
        else
        {
          return GoToColumnParameters.ColumnSortType.GRID_ORDER;
        }
      }
      set
      {
        _radAlphabetical.Checked = (value == GoToColumnParameters.ColumnSortType.ALPHABETIC);
        _radColumn.Checked = (value == GoToColumnParameters.ColumnSortType.GRID_ORDER);
      }
    }

    public FormStyles FormStyle
    {
      get
      {
        return this.formStyle;
      }
      set
      {
        switch (value)
        {
          case FormStyles.ListBox:
            this._lstColumns.Visible = true;
            this._lstColumns.BringToFront();
            this._lsvColumns.Visible = false;
            this._lsvColumns.SendToBack();
            this._lstColumns.Focus();
            break;

          case FormStyles.ListView:
            this._lstColumns.Visible = false;
            this._lstColumns.SendToBack();
            this._lsvColumns.Visible = true;
            this._lsvColumns.BringToFront();
            this._lsvColumns.Focus();
            break;
        }
        this.formStyle = value;
      }
    }

    #endregion

    #region Dispose
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
    #endregion

    #region Public Methods
    /// <summary>
    /// Makes the form visible, lists are filled with current column information.
    /// </summary>
    public override void ShowForm()
    {
      PopulateColumnsListBox();

      base.ShowForm();

      _lstColumns.Focus();
    }

    /// <summary>
    /// Resets the current ctrlViewBase and refreshes the window with the appropriate column
    /// info
    /// </summary>
    public override void ResetGrid(UltraGrid grid_)
    {
      base.ResetGrid(grid_);

      if(Visible)
      {
        PopulateColumnsListBox();
        Refresh();
      }
    }

    #endregion

    #region  Utilities
    /// <summary>
    /// Fills in the list box with the names of the visible columns of the grid
    /// </summary>
    private void PopulateColumnsListBox()
    {
      string selected = (string)_lstColumns.SelectedItem;

      // ListBox
      _lstColumns.Items.Clear();
      _lstColumns.Sorted = _radAlphabetical.Checked;

      // ListView
      _lsvColumns.BeginUpdate();
      _lsvColumns.Items.Clear();
      _lsvColumns.Sorting = _radAlphabetical.Checked ? System.Windows.Forms.SortOrder.Ascending : System.Windows.Forms.SortOrder.None;

      if(_grid != null)
      {
        string[] columnNames = ColumnCaptions;

        if(columnNames != null)
        {
          _lstColumns.Items.AddRange(columnNames);
          
          // Populate ListView
          foreach (string cName in columnNames)
          {
            _lsvColumns.Items.Add(new ListViewItem(cName));
          }

          if(Array.IndexOf(columnNames,selected) >= 0)
          {
            _lstColumns.SelectedItem = selected;
          }
          else
          {
            _lstColumns.SelectedIndex = 0;
        }
      }
    }

      _lsvColumns.EndUpdate();
    }

    /// <summary>
    /// Searches the grid for a column with a key identical to the parameter.
    /// If found, highlights the cell in that column and ActiveRow and scrolls to it if necessary.
    /// </summary>
    /// <param name="column_"></param>
    protected virtual void GoToColumn(string column_)
    {
      UltraGridRow oRow = _grid.ActiveRow;
      
      if(oRow == null)
      {
        oRow = _grid.ActiveRow = _grid.GetRow(ChildRow.First);
      }

      try
      {
        _grid.ActiveCell = oRow.Cells[column_];
        highlightActiveCell();
      }
      catch
      {
        string message = "The column named " + ColumnCaptions[Array.IndexOf(ColumnKeys, column_)] + 
            " does not exist in the current Columns setting";
        MessageBox.Show(this._grid.FindForm(), message, "Go To Column", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
      }
    }

    #endregion

    #region Event Handlers
    /// <summary>
    /// Responds to a "Go" button click event by column GoToColumn with 
    /// the selected column, then hides the form.
    /// </summary>
    /// <param name="sender">Event sending object</param>
    /// <param name="e">EventArgs</param>
    private void btnGo_Click(object sender, System.EventArgs e)
    {
      if(this._beforeNavigateHandler != null)
      {
        this._beforeNavigateHandler(this, EventArgs.Empty);
      }

      int index = 0;
      switch (FormStyle)
      {
        case FormStyles.ListBox:
          index = Array.IndexOf(ColumnCaptions, _lstColumns.SelectedItem);
          break;

        case FormStyles.ListView:
          index = Array.IndexOf(ColumnCaptions, _lsvColumns.SelectedItems[0].Text);
          break;
      }
      GoToColumn(ColumnKeys[index]);

      if(this._afterNavigateHandler != null)
      {
        this._afterNavigateHandler(this, EventArgs.Empty);
      }
    }

    /// <summary>
    /// Responds to a "Close" button click event by hiding the form from view. 
    /// </summary>
    /// <param name="sender">Event sending object</param>
    /// <param name="e">EventArgs</param>
    private void btnClose_Click(object sender, System.EventArgs e)
    {
      Hide();
    }
    
    /// <summary>
    /// Repopulates the Columns combo with the updated change
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void PopulateColumns(object sender, System.EventArgs e)
    {
      PopulateColumnsListBox();
      _lstColumns.Focus();
    }

    protected override void UpdateComponents(object sender, System.EventArgs e)
    {
      PopulateColumnsListBox();
      Refresh();
    }

    /// <summary>
    /// Set the focus to the List as soon as the form is activated.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void frmGoToColumn_Activated(object sender, EventArgs e)
    {
      switch (FormStyle)
      {
        case FormStyles.ListBox:
          _lstColumns.Focus();
          break;
        case FormStyles.ListView:
          _lsvColumns.Focus();
          break;
      }
    }

    #endregion

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
      System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem("_lsvColumns");
      this._lstColumns = new System.Windows.Forms.ListBox();
      this._btnGo = new System.Windows.Forms.Button();
      this._btnClose = new System.Windows.Forms.Button();
      this._lblSelectCol = new System.Windows.Forms.Label();
      this._radAlphabetical = new System.Windows.Forms.RadioButton();
      this._radColumn = new System.Windows.Forms.RadioButton();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this._lsvColumns = new System.Windows.Forms.ListView();
      this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
      this.SuspendLayout();
      // 
      // _lstColumns
      // 
      this._lstColumns.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this._lstColumns.Cursor = System.Windows.Forms.Cursors.Hand;
      this._lstColumns.Location = new System.Drawing.Point(113, 8);
      this._lstColumns.Name = "_lstColumns";
      this._lstColumns.Size = new System.Drawing.Size(179, 160);
      this._lstColumns.TabIndex = 1;
      this._lstColumns.TabStop = false;
      // 
      // _btnGo
      // 
      this._btnGo.Location = new System.Drawing.Point(16, 118);
      this._btnGo.Name = "_btnGo";
      this._btnGo.Size = new System.Drawing.Size(75, 23);
      this._btnGo.TabIndex = 5;
      this._btnGo.TabStop = false;
      this._btnGo.Text = "&Go";
      this._btnGo.Click += new System.EventHandler(this.btnGo_Click);
      // 
      // _btnClose
      // 
      this._btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this._btnClose.Location = new System.Drawing.Point(16, 150);
      this._btnClose.Name = "_btnClose";
      this._btnClose.Size = new System.Drawing.Size(75, 23);
      this._btnClose.TabIndex = 6;
      this._btnClose.TabStop = false;
      this._btnClose.Text = "Close";
      this._btnClose.Click += new System.EventHandler(this.btnClose_Click);
      // 
      // _lblSelectCol
      // 
      this._lblSelectCol.Location = new System.Drawing.Point(8, 8);
      this._lblSelectCol.Name = "_lblSelectCol";
      this._lblSelectCol.Size = new System.Drawing.Size(96, 16);
      this._lblSelectCol.TabIndex = 0;
      this._lblSelectCol.Text = "Select &column:";
      // 
      // _radAlphabetical
      // 
      this._radAlphabetical.Checked = true;
      this._radAlphabetical.Location = new System.Drawing.Point(16, 48);
      this._radAlphabetical.Name = "_radAlphabetical";
      this._radAlphabetical.Size = new System.Drawing.Size(80, 32);
      this._radAlphabetical.TabIndex = 3;
      this._radAlphabetical.TabStop = true;
      this._radAlphabetical.Text = "&Alphabetic";
      this._radAlphabetical.CheckedChanged += new System.EventHandler(this.PopulateColumns);
      // 
      // _radColumn
      // 
      this._radColumn.Location = new System.Drawing.Point(16, 72);
      this._radColumn.Name = "_radColumn";
      this._radColumn.Size = new System.Drawing.Size(80, 32);
      this._radColumn.TabIndex = 4;
      this._radColumn.Text = "Order in &table";
      // 
      // groupBox1
      // 
      this.groupBox1.Location = new System.Drawing.Point(8, 32);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(96, 80);
      this.groupBox1.TabIndex = 2;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "View columns";
      // 
      // _lsvColumns
      // 
      this._lsvColumns.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this._lsvColumns.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
      this._lsvColumns.FullRowSelect = true;
      this._lsvColumns.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
      this._lsvColumns.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1});
      this._lsvColumns.Location = new System.Drawing.Point(113, 8);
      this._lsvColumns.MultiSelect = false;
      this._lsvColumns.Name = "_lsvColumns";
      this._lsvColumns.Size = new System.Drawing.Size(179, 165);
      this._lsvColumns.TabIndex = 7;
      this._lsvColumns.UseCompatibleStateImageBehavior = false;
      this._lsvColumns.View = System.Windows.Forms.View.Details;
      this._lsvColumns.Visible = false;
      // 
      // columnHeader1
      // 
      this.columnHeader1.Text = "Column name";
      this.columnHeader1.Width = 155;
      // 
      // frmGoToColumn
      // 
      this.AcceptButton = this._btnGo;
      this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
      this.CancelButton = this._btnClose;
      this.ClientSize = new System.Drawing.Size(304, 182);
      this.Controls.Add(this._btnGo);
      this.Controls.Add(this._btnClose);
      this.Controls.Add(this._lblSelectCol);
      this.Controls.Add(this._radColumn);
      this.Controls.Add(this._radAlphabetical);
      this.Controls.Add(this.groupBox1);
      this.Controls.Add(this._lstColumns);
      this.Controls.Add(this._lsvColumns);
      this.MaximizeBox = false;
      this.Name = "frmGoToColumn";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Go To Column";
      this.Activated += new System.EventHandler(this.frmGoToColumn_Activated);
      this.ResumeLayout(false);

    }
		#endregion
    
	}
}
