//---------------------------------------------------------------------------
//
// Copyright (c) 2004 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
//---------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using System.Globalization;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  /// <summary>
  /// Summary description for WinGridUtil.
  /// </summary>
  public class WinGridUtil
  {
    protected static readonly IFormatProvider DATE_FORMAT_PROVIDER = new System.Globalization.CultureInfo("en-US", true);
    protected const string DATE_FORMAT = "MM/dd/yyyy";

    private const int INT_TICK_SIZE = 1;
    private const double DOUBLE_TICK_SIZE = 0.25;

    private WinGridUtil()
    {
    }

    public static void InitializeLayout(InitializeLayoutEventArgs e_)
    {
      e_.Layout.Override.RowSizing = RowSizing.Fixed;
      e_.Layout.Override.RowSpacingAfter = 0;
      e_.Layout.Override.CellSpacing = 0;
      e_.Layout.Override.CellPadding = 0;
      e_.Layout.Override.CellAppearance.TextVAlign = Infragistics.Win.VAlign.Middle;
      e_.Layout.ScrollStyle = ScrollStyle.Immediate;
      e_.Layout.MaxRowScrollRegions = 1;
      e_.Layout.Override.HeaderClickAction = HeaderClickAction.SortMulti;
    }

    public static void AutoResizeColumns(Infragistics.Win.UltraWinGrid.ColumnsCollection columns_)
    {
      foreach (Infragistics.Win.UltraWinGrid.UltraGridColumn col in columns_)
      {
        col.PerformAutoResize();
      }
    }

    public static void OnKeyDown(object sender_, System.Windows.Forms.KeyEventArgs e_)
    {
      UltraGrid grid = sender_ as UltraGrid;
      if (grid == null || grid.ActiveCell == null)
      {
        e_.Handled = false;
        return;
      }

      e_.Handled = true;
      switch (e_.KeyCode)
      {
        case Keys.Up:
          grid.PerformAction(UltraGridAction.ExitEditMode);
          grid.PerformAction(UltraGridAction.AboveCell);
          grid.PerformAction(UltraGridAction.EnterEditMode);
          break;
        case Keys.Down:
          grid.PerformAction(UltraGridAction.ExitEditMode);
          grid.PerformAction(UltraGridAction.BelowCell);
          grid.PerformAction(UltraGridAction.EnterEditMode);
          break;
        case Keys.Right:
          grid.PerformAction(UltraGridAction.ExitEditMode);
          grid.PerformAction(UltraGridAction.NextCellByTab);
          grid.PerformAction(UltraGridAction.EnterEditMode);
          break;
        case Keys.Left:
          grid.PerformAction(UltraGridAction.ExitEditMode);
          grid.PerformAction(UltraGridAction.PrevCellByTab);
          grid.PerformAction(UltraGridAction.EnterEditMode);
          break;
        case Keys.N: NextCellValue(grid.ActiveCell); break;
        case Keys.Add: NextCellValue(grid.ActiveCell); break;
        case Keys.P: PreviousCellValue(grid.ActiveCell); break;
        case Keys.Subtract: PreviousCellValue(grid.ActiveCell); break;
        default: e_.Handled = false; break;
      }

      if (e_.Handled)
      {
        SelectAllTextInEditor(grid.ActiveCell);
      }
    }


    protected static void PreviousCellValue(UltraGridCell cell_)
    {
      if (cell_ != null && cell_.CanEnterEditMode)
      {
        if (cell_.Column.ValueList != null && cell_.Column.ValueList.ItemCount > 0)
        {
          IValueList vl = cell_.Column.ValueList;
          int index = 0;
          object obj = vl.GetValue(cell_.Value.ToString(), ref index);
          index = (index <= 0) ? vl.ItemCount - 1 : index - 1;
          cell_.Value = vl.GetValue(index);
        }
        else
        {
          PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(cell_.Row.ListObject.GetType());
          PropertyDescriptor property = properties[cell_.Column.Key];
          object obj = cell_.Row.ListObject;
          if (property != null && property.IsBrowsable && !property.IsReadOnly)
          {
            cell_.CancelUpdate();
            if (property.PropertyType == typeof(int))
            {
              property.SetValue(obj, (int)property.GetValue(obj) - INT_TICK_SIZE);
            }
            else if (property.PropertyType == typeof(double))
            {
              property.SetValue(obj, (double)property.GetValue(obj) - DOUBLE_TICK_SIZE);
            }
            cell_.Refresh();
          }
        }
      }
    }

    public static void NextCellValue(UltraGridCell cell_)
    {
      if (cell_ != null && cell_.CanEnterEditMode)
      {
        if (cell_.Column.ValueList != null && cell_.Column.ValueList.ItemCount > 0)
        {
          IValueList vl = cell_.Column.ValueList;
          int index = 0;
          object obj = vl.GetValue(cell_.Value.ToString(), ref index);
          index = (index + 1) % vl.ItemCount;
          cell_.Value = vl.GetValue(index);
        }
        else
        {
          PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(cell_.Row.ListObject.GetType());
          PropertyDescriptor property = properties[cell_.Column.Key];
          object obj = cell_.Row.ListObject;
          if (property != null && property.IsBrowsable && !property.IsReadOnly)
          {
            cell_.CancelUpdate();
            if (property.PropertyType == typeof(int))
            {
              property.SetValue(obj, (int)property.GetValue(obj) + INT_TICK_SIZE);
            }
            else if (property.PropertyType == typeof(double))
            {
              property.SetValue(obj, (double)property.GetValue(obj) + DOUBLE_TICK_SIZE);
            }
            cell_.Refresh();
          }
        }
      }
    }

    public static void SelectAllTextInEditor(UltraGridCell cell_)
    {
      if (cell_ != null)
      {
        EditorWithText editor = cell_.Column.Editor as EditorWithText;
        if (editor != null && editor.IsInEditMode)
        {
          editor.TextBox.SelectAll();
        }
      }
    }

    public static void FormatColumns(Infragistics.Win.UltraWinGrid.ColumnsCollection columns_)
    {
      foreach (Infragistics.Win.UltraWinGrid.UltraGridColumn col in columns_)
      {
        if (col.DataType == typeof(float) || col.DataType == typeof(double))
        {
          col.Format = "#,##0.000;;0";
          col.CellAppearance.TextHAlign = Infragistics.Win.HAlign.Right;
        }
        else if (col.DataType == typeof(int) || col.DataType == typeof(long))
        {
          col.Format = "#,#;;0";
          col.CellAppearance.TextHAlign = Infragistics.Win.HAlign.Right;
        }
        else if (col.DataType == typeof(DateTime))
        {
          col.Format = DATE_FORMAT;
          col.FormatInfo = DATE_FORMAT_PROVIDER;
        }
      }
    }

    /// <summary>
    /// Gets the list of UltraGridRows from the grid's selected Cells if any
    /// </summary>
    /// <param name="grid_">The UltraGrid to inspect</param>
    /// <returns>The UltraGridRows of the selected cells</returns>
    public static IList<UltraGridRow> GetSelectedRowsFromCells(UltraGrid grid_)
    {
      List<UltraGridRow> rowList = new List<UltraGridRow>();

      // attempts to use cells insted of columns to find selected rows
      if (grid_.Selected.Cells != null || grid_.Selected.Cells.Count > 0)
      {
        // uses cells to select columns
        foreach (UltraGridCell selCell in grid_.Selected.Cells)
        {
          if (!rowList.Contains(selCell.Row))
            rowList.Add(selCell.Row);
        }
      }
      return rowList;
    }

    /// <summary>
    /// Gets the list of selected rows from or, if no rows are selected, 
    /// optionally gets the rows from the selected cells. 
    /// </summary>
    /// <param name="grid_">The UltraGrid to inspect</param>
    /// <param name="useSelectedCellRows_">if true and no rows are selected, get rows from selected cells</param>
    /// <returns>List of selected Rows</returns>
    public static IList<UltraGridRow> GetSelectedRows(UltraGrid grid_, bool useSelectedCellRows_)
    {
      List<UltraGridRow> rowList = new List<UltraGridRow>();

      if (grid_.Selected.Rows != null && grid_.Selected.Rows.Count > 0)
      {
        foreach (UltraGridRow row in grid_.Selected.Rows)
        {
          rowList.Add(row);
        }
      }
      else if (useSelectedCellRows_)
      {
        return GetSelectedRowsFromCells(grid_);
      }

      return rowList;
    }

    public static IList<UltraGridRow> GetSelectedRowsInDisplayOrder(UltraGrid grid_, bool useSelectedCellRows_)
    {
      List<UltraGridRow> rows = grid_.Selected.Rows.OfType<UltraGridRow>().OrderBy(r => r.VisibleIndex).ToList();

      if (rows.Count > 0)
      {
        return rows;
      }
      return useSelectedCellRows_ ? GetSelectedRowsFromCells(grid_) : rows;
    }

    /// <summary>
    /// Get the editor for a double value, with the appropriate in-cell values.  By default a normal price value layout is given.
    /// </summary>
    /// <returns>The <see cref="EmbeddableEditorBase"/> that can be passed to a cell editor.</returns>
    public static EmbeddableEditorBase GetDoubleMaskCellEditor()
    {
      return GetDoubleMaskCellEditor("-nnnnnnnn.nnnnnnnn");
    }

    /// <summary>
    /// Get the editor for a double value, with the appropriate in-cell values.
    /// </summary>
    /// <param name="mask_">The format for the double to be displayed in.</param>
    /// <returns>The <see cref="EmbeddableEditorBase"/> that can be passed to a cell editor.</returns>
    public static EmbeddableEditorBase GetDoubleMaskCellEditor(string mask_)
    {
      DefaultEditorOwnerSettings editorSettings = new DefaultEditorOwnerSettings();
      editorSettings.DataType = typeof(double);
      editorSettings.MaskInput = mask_;
      return new EditorWithMask(new DefaultEditorOwner(editorSettings));
    }
  }
}
