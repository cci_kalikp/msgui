
//---------------------------------------------------------------------------
//
// Copyright (c) 2004 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
//---------------------------------------------------------------------------

using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Specialized;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
	/// <summary>
	/// The AbstractGridLayoutFactory class. Acts as the base class for all GridLayoutFactory classes.
	/// </summary>
	public abstract class AbstractGridLayoutFactory
	{

    public AbstractGridLayoutFactory()
    {

    }
    protected void InitializeDefaultValues()
    {
      _applyCaptions = true;
      this.ApplyCaptions = true;
      this.ApplyColumnWidth = true;
      this.ApplySortBool = true;
      StartDepth = 0;
    }

    /// <summary>
    /// Returns an XML representation of the Grid provided.
    /// </summary>		
    /// <param name="applicationName_">The current application name.</param>	
    /// <param name="gridName_">The name of the current grid.</param>	
    /// <param name="layoutName_">The current layout name.</param>   
    /// <returns>An XML representation of the grid provided.</returns>	
		abstract public GridLayout GetGridLayout(string applicationName_, string gridName_, string layoutName_);
    
    /// <summary>
		/// Returns an XML representation of the Grid provided.
		/// </summary>		
		/// <param name="applicationName_">The current application name.</param>	
		/// <param name="gridName_">The name of the current grid.</param>	
		/// <param name="layoutName_">The current layout name.</param>
		/// <param name="omitKeys_">A collection of columns keys to omit from the grid layout.</param>
		/// <returns>An XML representation of the grid provided.</returns>		
    abstract public GridLayout GetGridLayout(string applicationName_, string gridName_, string layoutName_, StringCollection omitKeys_);
    
    /// <summary>
    /// Returns an XML representation of the Grid provided.
    /// </summary>		
    /// <param name="applicationName_">The current application name.</param>	
    /// <param name="gridName_">The name of the current grid.</param>	
    /// <param name="layoutName_">The current layout name.</param>
    /// <param name="omitKeys_">A collection of columns keys to omit from the grid layout.</param>
    /// <param name="band_">The grid band the layout applies to</param>
    /// <returns>An XML representation of the grid provided.</returns>		
    abstract public GridLayout GetGridLayout(string applicationName_, string gridName_, string layoutName_, StringCollection omitKeys_, int band_);

    /// <summary>
    /// Loads grid format information into the associated grid.
    /// </summary>		
    /// <param name="gridLayout_">The XML format data required to update the grid.</param>		
    virtual public void ApplyGridLayout(GridLayout gridLayout_)
    {
      ApplyGridLayout(gridLayout_, StartDepth, ApplySortBool, ApplyColumnWidth);
    }

		/// <summary>
		/// Loads grid format information into the associated grid.
		/// </summary>		
		/// <param name="gridLayout_">The XML format data required to update the grid.</param>		
		/// <param name="applySort_">When set to true applies column sort operations.</param>
    virtual public void ApplyGridLayout(GridLayout gridLayout_, bool applySort_)
    {
      ApplyGridLayout(gridLayout_,StartDepth, applySort_, ApplyColumnWidth);
    }

    /// <summary>
    /// Loads grid format information into the associated grid.
    /// </summary>		
    /// <param name="gridLayout_">The XML format data required to update the grid.</param>		
    /// <param name="applySort_">When set to true applies column sort operations.</param>
    /// <param name="applyColumnWidths_">When set to true applies column widths.</param>
    virtual public void ApplyGridLayout(GridLayout gridLayout_, bool applySort_, bool applyColumnWidths_)
    {
      ApplyGridLayout(gridLayout_, StartDepth, applySort_, applyColumnWidths_);
    }

		/// <summary>
		/// Loads grid format information into the associated grid.
		/// </summary>		
		/// <param name="gridLayout_">The XML format data required to update the grid.</param>		
		/// <param name="startDepth_">The depth to start layout application within a hierichial grid.</param>
    virtual public void ApplyGridLayout(GridLayout gridLayout_, int startDepth_)
    {
      ApplyGridLayout(gridLayout_, startDepth_, ApplySortBool, ApplyColumnWidth);
    }

		/// <summary>
		/// Loads grid format information into the associated grid.
		/// </summary>		
		/// <param name="gridLayout_">The XML format data required to update the grid.</param>		
		/// <param name="startDepth_">The depth to start layout application within a hierichial grid.</param>
		/// <param name="applySort_">When set to true applies column sort operations.</param>
    virtual public void ApplyGridLayout(GridLayout gridLayout_, int startDepth_, bool applySort_)
    {
      ApplyGridLayout(gridLayout_, startDepth_, applySort_, ApplyColumnWidth);
    }

    /// <summary>
    /// Loads grid format information into the associated grid.
    /// </summary>		
    /// <param name="gridLayout_">The XML format data required to update the grid.</param>		
    /// <param name="startDepth_">The depth to start layout application within a hierichial grid.</param>
    /// <param name="applySort_">When set to true applies column sort operations.</param>
    /// <param name="applyColumnWidths_">When set to true applies column widths.</param>
    abstract public void ApplyGridLayout(GridLayout gridLayout_, int startDepth_, 
      bool applySort_, bool applyColumnWidths_);

		/// <summary>
		/// Loads grid format information into the associated grid.
		/// </summary>		
		/// <param name="gridLayout_">The XML format data required to update the grid.</param>		
		/// <param name="startDepth_">The depth to start layout application within a hierichial grid.</param>
		/// <param name="length_">The number of bands to apply the layout starting at the specified depth.</param>
    virtual public void ApplyGridLayout(GridLayout gridLayout_, int startDepth_, int length_)
    {
      ApplyGridLayout(gridLayout_, startDepth_, length_,ApplySortBool, ApplyColumnWidth);
    }
		
		/// <summary>
		/// Loads grid format information into the associated grid.
		/// </summary>		
		/// <param name="gridLayout_">The XML format data required to update the grid.</param>		
		/// <param name="startDepth_">The depth to start layout application within a hierichial grid.</param>
		/// <param name="length_">The number of bands to apply the layout starting at the specified depth.</param>
		/// <param name="applySort_">When set to true applies column sort operations.</param>
    virtual public void ApplyGridLayout(GridLayout gridLayout_, int startDepth_, int length_, bool applySort_)
    {
      ApplyGridLayout(gridLayout_, startDepth_, length_, applySort_, ApplyColumnWidth);
    }

    /// <summary>
    /// Loads grid format information into the associated grid.
    /// </summary>		
    /// <param name="gridLayout_">The XML format data required to update the grid.</param>		
    /// <param name="startDepth_">The depth to start layout application within a hierichial grid.</param>
    /// <param name="length_">The number of bands to apply the layout starting at the specified depth.</param>
    /// <param name="applySort_">When set to true applies column sort operations.</param>
    /// <param name="applyColumnWidths_">When set to true applies column widths.</param>
    virtual public void ApplyGridLayout(GridLayout gridLayout_, int startDepth_, int length_, bool applySort_, bool applyColumnWidths_)
    {
      ApplyGridLayout(gridLayout_, startDepth_, length_, applySort_, applyColumnWidths_,ApplyCaptions);
    }

    /// <summary>
    /// Loads grid format information into the associated grid.
    /// </summary>		
    /// <param name="gridLayout_">The XML format data required to update the grid.</param>		
    /// <param name="startDepth_">The depth to start layout application within a hierichial grid.</param>
    /// <param name="length_">The number of bands to apply the layout starting at the specified depth.</param>
    /// <param name="applySort_">When set to true applies column sort operations.</param>
    /// <param name="applyColumnWidths_">When set to true applies column widths.</param>
    abstract public void ApplyGridLayout(GridLayout gridLayout_, int startDepth_, int length_, bool applySort_, bool applyColumnWidths_, bool applyCaptionsFromDS_);
  
    

		/// <summary>
		/// Applies sorting properties to the the associated grid.
		/// </summary>		
		/// <param name="gridLayout_">The XML format data required to update the grid.</param>		
    virtual public void ApplySort(GridLayout gridLayout_)
    {
      ApplySort(gridLayout_, StartDepth);
    }

		/// <summary>
		/// Applies sorting properties to the the associated grid.
		/// </summary>		
		/// <param name="gridLayout_">The XML format data required to update the grid.</param>		
		/// <param name="startDepth_">The depth to start layout application within a hierichial grid.</param>
		abstract public void ApplySort(GridLayout gridLayout_, int startDepth_);

		/// <summary>
		/// Applies sorting properties to the the associated grid.
		/// </summary>		
		/// <param name="gridLayout_">The XML format data required to update the grid.</param>		
		/// <param name="startDepth_">The depth to start layout application within a hierichial grid.</param>
		/// <param name="length_">The number of bands to apply the layout starting at the specified depth.</param>
		abstract public void ApplySort(GridLayout gridLayout_, int startDepth_, int length_);

    /// <summary>
    /// Applies column width properties to the the associated grid.
    /// </summary>		
    /// <param name="gridLayout_">The XML format data required to update the grid.</param>		
    virtual public void ApplyColumnWidths(GridLayout gridLayout_)
    {
      ApplyColumnWidths(gridLayout_,StartDepth);
    }

    /// <summary>
    /// Applies column width properties to the the associated grid.
    /// </summary>		
    /// <param name="gridLayout_">The XML format data required to update the grid.</param>		
    /// <param name="startDepth_">The depth to start layout application within a hierichial grid.</param>
    abstract public void ApplyColumnWidths(GridLayout gridLayout_, int startDepth_);

    /// <summary>
    /// Applies column width properties to the the associated grid.
    /// </summary>		
    /// <param name="gridLayout_">The XML format data required to update the grid.</param>		
    /// <param name="startDepth_">The depth to start layout application within a hierichial grid.</param>
    /// <param name="length_">The number of bands to apply the layout starting at the specified depth.</param>
    abstract public void ApplyColumnWidths(GridLayout gridLayout_, int startDepth_, int length_);

    /// <summary>
    /// Sets the underlying grid being used as the source by the GridLayoutFactory instance.
    /// </summary>
    abstract public object Grid { set; }

    /// <summary>
    /// Sets whether new columns added to the layout are hidden (defaulted to true)
    /// </summary>
    abstract public bool HideNewColumns
    {
      get;
      set;
    }


    #region public Properties

    private int _startDepth;

    virtual public int StartDepth
    {
      get { return _startDepth; }
      set { _startDepth = value; }
    }

    private int _length;

    virtual public int Length
    {
      get { return _length; }
      set { _length = value; }
    }

    private bool _applySort;

    virtual public bool ApplySortBool
    {
      get { return _applySort; }
      set { _applySort = value; }
    }

    private bool _applyColumnWidth;

    virtual public bool ApplyColumnWidth
    {
      get { return _applyColumnWidth; }
      set { _applyColumnWidth = value; }
    }

    private bool _applyCaptions;
        
    virtual public bool ApplyCaptions
    {
      get { return _applyCaptions; }
      set { _applyCaptions = value; }
    }
	
    #endregion

    #region Internal Static Methods

    /// <summary>
    /// Returns an XML representation of the GridLayout object provided.
    /// </summary>
    /// <param name="gridLayout_">The gridlayout to return format information on.</param>
    /// <returns>An XML representation of the gridlayout provided.</returns>
    internal static XmlElement GetXml(GridLayout gridLayout_)
    {
      StringWriter writer = new StringWriter();
			
      try
      {
        XmlSerializer serializer = new GridLayoutSerializer(new GridReader(), new GridWriter());										
        serializer.Serialize(writer, gridLayout_);				
        XmlDocument xmlDoc = new XmlDocument();
        String xmlString = writer.ToString();
        
        System.Text.RegularExpressions.Regex regEx = new System.Text.RegularExpressions.Regex("<Caption>");
        xmlDoc.LoadXml(xmlString);
        return xmlDoc.DocumentElement;	
			
      }
      catch(IOException ioe)
      {
        throw new Exception("Error during serialization of XML data.", ioe);
      }
      catch(Exception ex)
      {
        throw new Exception("Unable to generate XML data from supplied Grid Layout.", ex);
      }      
    }    

		#endregion


  }
}
