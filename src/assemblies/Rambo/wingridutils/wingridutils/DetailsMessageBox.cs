﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  /// <summary>
  /// This is a class that represents a dialog box with 3 text fields
  /// Caption(Form Header), Message, and Details( a scrollable and selectable
  /// textbox to make sure Ctrl+C is available)
  /// </summary>
  public partial class DetailsMessageBox : Form
  {
    private DetailsMessageBox(string caption_, string summaryMessage_, string htmlDetails_)
    {
      InitializeComponent();
      Text = caption_;
      _messageLabel.Text = summaryMessage_;
      _detailsTextBox.Value = htmlDetails_;


      string[] lines = Regex.Split(htmlDetails_, "<br />", 
        RegexOptions.IgnoreCase);
      string longestLine = summaryMessage_;
      
      foreach(string str in lines)
      {
        Regex rg = new Regex("<(.)*>");
        string newStr = rg.Replace(str, String.Empty);
        if (longestLine.Length < newStr.Length)
        {
          longestLine = newStr;
        }
      }

      using (Graphics graphics = this.CreateGraphics())
      {
          SizeF textSize = graphics.MeasureString(longestLine, _detailsTextBox.Font);
          this.Size = new Size((int)textSize.Width + 50, Size.Height);
      }
    }

    /// <summary>
    /// Shows a Dialog box
    /// </summary>
    /// <param name="caption_">Caption of the message box</param>
    /// <param name="summaryMessage_">Unselectable max 2 lines</param>
    /// <param name="details_">A message to appear in selectable textbox</param>
    public static void Show(string caption_, string summaryMessage_, string details_)
    {
      using(DetailsMessageBox messageBox=new DetailsMessageBox(caption_,summaryMessage_,details_))
      {
        messageBox.ShowDialog();
      }
    }

    private void _OKButton_Click(object sender, EventArgs e)
    {
      DialogResult = DialogResult.OK;
      this.Close();
    }

  }
}
