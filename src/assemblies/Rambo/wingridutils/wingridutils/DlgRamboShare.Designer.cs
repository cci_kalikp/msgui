﻿namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  partial class DlgRamboShare
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this._listBoxShareGroups = new System.Windows.Forms.ListBox();
      this._lblAlsoShare = new System.Windows.Forms.Label();
      this._txtShareWith = new System.Windows.Forms.TextBox();
      this._lblShareTip = new System.Windows.Forms.Label();
      this._btnOK = new System.Windows.Forms.Button();
      this._btnCancel = new System.Windows.Forms.Button();
      this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
      this.groupBox1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
      this.SuspendLayout();
      // 
      // groupBox1
      // 
      this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.groupBox1.Controls.Add(this._listBoxShareGroups);
      this.groupBox1.Location = new System.Drawing.Point(12, 12);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(268, 123);
      this.groupBox1.TabIndex = 0;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Current Share Groups";
      // 
      // _listBoxShareGroups
      // 
      this._listBoxShareGroups.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this._listBoxShareGroups.BackColor = System.Drawing.SystemColors.Control;
      this._listBoxShareGroups.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this._listBoxShareGroups.FormattingEnabled = true;
      this._listBoxShareGroups.Location = new System.Drawing.Point(11, 19);
      this._listBoxShareGroups.Name = "_listBoxShareGroups";
      this._listBoxShareGroups.Size = new System.Drawing.Size(246, 91);
      this._listBoxShareGroups.TabIndex = 0;
      // 
      // _lblAlsoShare
      // 
      this._lblAlsoShare.AutoSize = true;
      this._lblAlsoShare.Location = new System.Drawing.Point(15, 166);
      this._lblAlsoShare.Name = "_lblAlsoShare";
      this._lblAlsoShare.Size = new System.Drawing.Size(86, 13);
      this._lblAlsoShare.TabIndex = 1;
      this._lblAlsoShare.Text = "Also Share With:";
      // 
      // _txtShareWith
      // 
      this._txtShareWith.AcceptsTab = true;
      this._txtShareWith.Location = new System.Drawing.Point(131, 166);
      this._txtShareWith.Name = "_txtShareWith";
      this._txtShareWith.Size = new System.Drawing.Size(138, 20);
      this._txtShareWith.TabIndex = 2;
      // 
      // _lblShareTip
      // 
      this._lblShareTip.AutoSize = true;
      this._lblShareTip.Location = new System.Drawing.Point(12, 179);
      this._lblShareTip.Name = "_lblShareTip";
      this._lblShareTip.Size = new System.Drawing.Size(111, 13);
      this._lblShareTip.TabIndex = 3;
      this._lblShareTip.Text = "(seperated by comma)";
      // 
      // _btnOK
      // 
      this._btnOK.Location = new System.Drawing.Point(155, 229);
      this._btnOK.Name = "_btnOK";
      this._btnOK.Size = new System.Drawing.Size(60, 25);
      this._btnOK.TabIndex = 4;
      this._btnOK.Text = "OK";
      this._btnOK.UseVisualStyleBackColor = true;
      this._btnOK.Click += new System.EventHandler(this._btnOK_Click);
      // 
      // _btnCancel
      // 
      this._btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this._btnCancel.Location = new System.Drawing.Point(221, 229);
      this._btnCancel.Name = "_btnCancel";
      this._btnCancel.Size = new System.Drawing.Size(59, 25);
      this._btnCancel.TabIndex = 5;
      this._btnCancel.Text = "Cancel";
      this._btnCancel.UseVisualStyleBackColor = true;
      // 
      // errorProvider1
      // 
      this.errorProvider1.ContainerControl = this;
      // 
      // DlgRamboShare
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Control;
      this.ClientSize = new System.Drawing.Size(292, 266);
      this.Controls.Add(this._btnCancel);
      this.Controls.Add(this._btnOK);
      this.Controls.Add(this._lblShareTip);
      this.Controls.Add(this._txtShareWith);
      this.Controls.Add(this._lblAlsoShare);
      this.Controls.Add(this.groupBox1);
      this.Name = "DlgRamboShare";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Share Resource";
      this.groupBox1.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.ListBox _listBoxShareGroups;
    private System.Windows.Forms.Label _lblAlsoShare;
    private System.Windows.Forms.TextBox _txtShareWith;
    private System.Windows.Forms.Label _lblShareTip;
    private System.Windows.Forms.Button _btnOK;
    private System.Windows.Forms.Button _btnCancel;
    private System.Windows.Forms.ErrorProvider errorProvider1;
  }
}