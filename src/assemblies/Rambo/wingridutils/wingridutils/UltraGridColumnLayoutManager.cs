//---------------------------------------------------------------------------
//
// Copyright (c) 2004 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
//---------------------------------------------------------------------------

using System;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Collections;
using System.Collections.Specialized;
using System.Drawing;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  /// <summary>
  /// The UltraGridColumnLayoutManager manager bridges the gap between UltraGrid-specific classes and the
  /// generic GridLayout and ColumnLayout object types that represent layout in a standard
  /// XML format.
  /// <remarks>This class loads an UltraGridColumn and passes the appropiate parameters into
  /// the standard Layout structures. It can also return the underlying XML representation
  /// that these classes comprise.</remarks>
  /// </summary>
  internal class UltraGridColumnLayoutManager
  {
    #region Member Variables

    protected ColumnLayout m_infraColSerialiser;
    protected UltraGridColumn m_infraCol;


    #endregion

    #region Constructors



    /// <summary>
    /// Initialises an instance of the UltraGridColumnLayoutManager.
    /// </summary>
    /// <param name="column_">An instance of an UltraGridColumn</param>
    public UltraGridColumnLayoutManager(UltraGridColumn column_)
    {

      m_infraCol = column_;
      m_infraColSerialiser = new ColumnLayout();
      LoadSerialiser();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Serialises an instance of a column into XML format.
    /// </summary>
    private void LoadSerialiser()
    {

      m_infraColSerialiser.Caption = m_infraCol.Header.Caption;
      m_infraColSerialiser.Format = m_infraCol.Format;
      m_infraColSerialiser.Hidden = m_infraCol.Hidden;
      m_infraColSerialiser.Index = m_infraCol.Header.VisiblePosition;
      m_infraColSerialiser.Key = m_infraCol.Key;

      if (m_infraColSerialiser.Hidden && m_infraCol.Width < 10)
      {
        m_infraColSerialiser.Width = 50;
      }
      else
      {
        m_infraColSerialiser.Width = m_infraCol.Width;
      }
      m_infraColSerialiser.DataType = m_infraCol.DataType.ToString();
      m_infraColSerialiser.BackColorValue = m_infraCol.CellAppearance.BackColor.ToArgb();
      m_infraColSerialiser.ForeColorValue = m_infraCol.CellAppearance.ForeColor.ToArgb();

      if (m_infraCol.CellAppearance.FontData != null && m_infraCol.CellAppearance.FontData.Name != null)
      {
        FontStyle fontStyle = FontStyle.Regular;

        if (m_infraCol.CellAppearance.FontData.Bold == DefaultableBoolean.True)
        {
          fontStyle = fontStyle | FontStyle.Bold;
        }

        if (m_infraCol.CellAppearance.FontData.Italic == DefaultableBoolean.True)
        {
          fontStyle = fontStyle | FontStyle.Italic;
        }

        if (m_infraCol.CellAppearance.FontData.Underline == DefaultableBoolean.True)
        {
          fontStyle = fontStyle | FontStyle.Underline;
        }

        FontFamily fontFamily = new FontFamily(m_infraCol.CellAppearance.FontData.Name);
        Font font = new Font(fontFamily, m_infraCol.CellAppearance.FontData.SizeInPoints, fontStyle);
        m_infraColSerialiser.Font = font;
      }
      //			m_infraColSerialiser.ReadOnly = (m_infraCol.CellActivation == Activation.NoEdit);

      if (m_infraCol.Band.SortedColumns.Contains(m_infraCol))
      {
        m_infraColSerialiser.SortOrder = m_infraCol.Band.SortedColumns.IndexOf(m_infraCol);
      }
      else
      {
        m_infraColSerialiser.SortOrder = -1;
      }

      switch (m_infraCol.SortIndicator)
      {
        case SortIndicator.Ascending:
          m_infraColSerialiser.SortDirection = ColumnLayout.SortDirections.Ascending;
          break;
        case SortIndicator.Descending:
          m_infraColSerialiser.SortDirection = ColumnLayout.SortDirections.Descending;
          break;
        default:
          m_infraColSerialiser.SortDirection = ColumnLayout.SortDirections.None;
          break;
      }

      if (m_infraCol.SortComparer is GridColumnComparer)
      {
        GridColumnComparer colComparer = (GridColumnComparer)m_infraCol.SortComparer;
        m_infraColSerialiser.SortAbsolute = colComparer.Absolute;
      }
      else
      {
        m_infraColSerialiser.SortAbsolute = false;
      }

      if (m_infraCol.Header.Tag != null)
      {
        m_infraColSerialiser.HeaderTag = (string)m_infraCol.Header.Tag;
      }
    }

    #endregion

    #region Public Methods

    /// <summary>
    /// Gets the underlying ColumnLayout object.
    /// </summary>
    public ColumnLayout ColumnSerialiser
    {
      get { return m_infraColSerialiser; }
    }

    #endregion
  }
}

