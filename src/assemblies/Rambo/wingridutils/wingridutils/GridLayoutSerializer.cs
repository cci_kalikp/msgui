namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  using System.Xml.Serialization;
  using System;
    
    
  /// <summary>Custom serializer for GridLayout type.</summary>
  public class GridLayoutSerializer : System.Xml.Serialization.XmlSerializer
  {
        
        
    GridReader _reader;
    GridWriter _writer;

    /// <summary>Constructs the serializer with default reader and writer instances.</summary>
    public GridLayoutSerializer()
    {
    }

    /// <summary>Constructs the serializer with a pre-built reader.</summary>
    public GridLayoutSerializer(GridReader reader)
    {
      _reader = reader;
    }

    /// <summary>Constructs the serializer with a pre-writer reader.</summary>
    public GridLayoutSerializer(GridWriter writer)
    {
      _writer = writer;
    }

    /// <summary>Constructs the serializer with pre-built reader and writer.</summary>
    public GridLayoutSerializer(GridReader reader, GridWriter writer)
    {
      _reader = reader;
      _writer = writer;
    }

    /// <summary>See <see cref="XmlSerializer.CreateReader"/>.</summary>
    protected override XmlSerializationReader CreateReader()
    {
      if (_reader != null) 
        return _reader;
      else
        return new GridReader();
    }

    /// <summary>See <see cref="XmlSerializer.CreateWriter"/>.</summary>
    protected override XmlSerializationWriter CreateWriter()
    {
      if (_writer != null) 
        return _writer;
      else
        return new GridWriter();
    }

    /// <summary>See <see cref="XmlSerializer.Deserialize"/>.</summary>
    protected override object Deserialize(XmlSerializationReader reader)
    {
      if (!(reader is GridReader))
        throw new ArgumentException("reader");

      return ((GridReader)reader).Read();
    }

    /// <summary>See <see cref="XmlSerializer.Serialize"/>.</summary>
    protected override void Serialize(object o, XmlSerializationWriter writer)
    {
      if (!(writer is GridWriter))
        throw new ArgumentException("writer");

      ((GridWriter)writer).Write((MorganStanley.MSDesktop.Rambo.WinGridUtils.GridLayout)o);
    }
    public class Reader : System.Xml.Serialization.XmlSerializationReader 
    {

      /// <remarks/>
      protected virtual MorganStanley.MSDesktop.Rambo.WinGridUtils.GridLayout Read1_GridLayout(bool isNullable, bool checkType) 
      {
        if (isNullable && ReadNull()) return null;
        if (checkType) 
        {
          System.Xml.XmlQualifiedName t = GetXsiType();
          if (t == null || ((object) ((System.Xml.XmlQualifiedName)t).Name == (object)id1_GridLayout && (object) ((System.Xml.XmlQualifiedName)t).Namespace == (object)id2_Item))
            ;
          else
            throw CreateUnknownTypeException((System.Xml.XmlQualifiedName)t);
        }
        MorganStanley.MSDesktop.Rambo.WinGridUtils.GridLayout o = new MorganStanley.MSDesktop.Rambo.WinGridUtils.GridLayout();
        if ((object)(o.@Columns) == null) o.@Columns = new MorganStanley.MSDesktop.Rambo.WinGridUtils.ColumnLayoutCollection();
        MorganStanley.MSDesktop.Rambo.WinGridUtils.ColumnLayoutCollection a_5 = (MorganStanley.MSDesktop.Rambo.WinGridUtils.ColumnLayoutCollection)o.@Columns;
        while (Reader.MoveToNextAttribute()) 
        {
          if (!IsXmlnsAttribute(Reader.Name)) 
          {
            UnknownNode((object)o);
          }
        }
        Reader.MoveToElement();
        if (Reader.IsEmptyElement) 
        {
          Reader.Skip();
          return o;
        }
        Reader.ReadStartElement();
        bool[] paramsRead = new bool[8];
        Reader.MoveToContent();
        while (Reader.NodeType != System.Xml.XmlNodeType.EndElement) 
        {
          if (Reader.NodeType == System.Xml.XmlNodeType.Element) 
          {
            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id3_Application && (object) Reader.NamespaceURI == (object)id2_Item)) 
            {
              o.@Application = Reader.ReadElementString();
              paramsRead[0] = true;
            }
            else if (!paramsRead[1] && ((object) Reader.LocalName == (object)id4_GridName && (object) Reader.NamespaceURI == (object)id2_Item)) 
            {
              o.@GridName = Reader.ReadElementString();
              paramsRead[1] = true;
            }
            else if (!paramsRead[2] && ((object) Reader.LocalName == (object)id5_LayoutName && (object) Reader.NamespaceURI == (object)id2_Item)) 
            {
              o.@LayoutName = Reader.ReadElementString();
              paramsRead[2] = true;
            }
            else if (!paramsRead[3] && ((object) Reader.LocalName == (object)id6_LockedColumns && (object) Reader.NamespaceURI == (object)id2_Item)) 
            {
              o.@LockedColumns = System.Xml.XmlConvert.ToInt32(Reader.ReadElementString());
              paramsRead[3] = true;
            }
            else if (!paramsRead[4] && ((object) Reader.LocalName == (object)id7_UpdateID && (object) Reader.NamespaceURI == (object)id2_Item)) 
            {
              o.@UpdateID = Reader.ReadElementString();
              paramsRead[4] = true;
            }
            else if (((object) Reader.LocalName == (object)id8_Columns && (object) Reader.NamespaceURI == (object)id2_Item)) 
            {
              if (!ReadNull()) 
              {
                if ((object)(o.@Columns) == null) o.@Columns = new MorganStanley.MSDesktop.Rambo.WinGridUtils.ColumnLayoutCollection();
                MorganStanley.MSDesktop.Rambo.WinGridUtils.ColumnLayoutCollection a_5_0 = (MorganStanley.MSDesktop.Rambo.WinGridUtils.ColumnLayoutCollection)o.@Columns;
                if (Reader.IsEmptyElement) 
                {
                  Reader.Skip();
                }
                else 
                {
                  Reader.ReadStartElement();
                  Reader.MoveToContent();
                  while (Reader.NodeType != System.Xml.XmlNodeType.EndElement) 
                  {
                    if (Reader.NodeType == System.Xml.XmlNodeType.Element) 
                    {
                      if (((object) Reader.LocalName == (object)id9_ColumnLayout && (object) Reader.NamespaceURI == (object)id2_Item)) 
                      {
                        if ((object)(a_5_0) == null) Reader.Skip(); else a_5_0.Add(Read2_ColumnLayout(true, true));
                      }
                      else 
                      {
                        UnknownNode(null);
                      }
                    }
                    else 
                    {
                      UnknownNode(null);
                    }
                    Reader.MoveToContent();
                  }
                  ReadEndElement();
                }
              }
            }
            else if (!paramsRead[6] && ((object) Reader.LocalName == (object)id10_BandId && (object) Reader.NamespaceURI == (object)id2_Item)) 
            {
              o.@BandId = System.Xml.XmlConvert.ToInt32(Reader.ReadElementString());
              paramsRead[6] = true;
            }
            else if (!paramsRead[7] && ((object) Reader.LocalName == (object)id11_Font && (object) Reader.NamespaceURI == (object)id2_Item)) 
            {
              o.@Font = Read5_RowFontStyleEnum(Reader.ReadElementString());
              paramsRead[7] = true;
            }
            else 
            {
              UnknownNode((object)o);
            }
          }
          else 
          {
            UnknownNode((object)o);
          }
          Reader.MoveToContent();
        }
        ReadEndElement();
        return o;
      }

      /// <remarks/>
      protected virtual MorganStanley.MSDesktop.Rambo.WinGridUtils.ColumnLayout Read2_ColumnLayout(bool isNullable, bool checkType) 
      {
        if (isNullable && ReadNull()) return null;
        if (checkType) 
        {
          System.Xml.XmlQualifiedName t = GetXsiType();
          if (t == null || ((object) ((System.Xml.XmlQualifiedName)t).Name == (object)id9_ColumnLayout && (object) ((System.Xml.XmlQualifiedName)t).Namespace == (object)id2_Item))
            ;
          else
            throw CreateUnknownTypeException((System.Xml.XmlQualifiedName)t);
        }
        MorganStanley.MSDesktop.Rambo.WinGridUtils.ColumnLayout o = new MorganStanley.MSDesktop.Rambo.WinGridUtils.ColumnLayout();
        while (Reader.MoveToNextAttribute()) 
        {
          if (!IsXmlnsAttribute(Reader.Name)) 
          {
            UnknownNode((object)o);
          }
        }
        Reader.MoveToElement();
        if (Reader.IsEmptyElement) 
        {
          Reader.Skip();
          return o;
        }
        Reader.ReadStartElement();
        bool[] paramsRead = new bool[15];
        Reader.MoveToContent();
        while (Reader.NodeType != System.Xml.XmlNodeType.EndElement) 
        {
          if (Reader.NodeType == System.Xml.XmlNodeType.Element) 
          {
            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id12_Width && (object) Reader.NamespaceURI == (object)id2_Item)) 
            {
              o.@Width = System.Xml.XmlConvert.ToInt32(Reader.ReadElementString());
              paramsRead[0] = true;
            }
            else if (!paramsRead[1] && ((object) Reader.LocalName == (object)id13_Hidden && (object) Reader.NamespaceURI == (object)id2_Item)) 
            {
              o.@Hidden = System.Xml.XmlConvert.ToBoolean(Reader.ReadElementString());
              paramsRead[1] = true;
            }
            else if (!paramsRead[2] && ((object) Reader.LocalName == (object)id14_Index && (object) Reader.NamespaceURI == (object)id2_Item)) 
            {
              o.@Index = System.Xml.XmlConvert.ToInt32(Reader.ReadElementString());
              paramsRead[2] = true;
            }
            else if (!paramsRead[3] && ((object) Reader.LocalName == (object)id15_Format && (object) Reader.NamespaceURI == (object)id2_Item)) 
            {
              o.@Format = Reader.ReadElementString();
              paramsRead[3] = true;
            }
            else if (!paramsRead[4] && ((object) Reader.LocalName == (object)id16_Caption && (object) Reader.NamespaceURI == (object)id2_Item)) 
            {
              o.@Caption = Reader.ReadElementString();
              paramsRead[4] = true;
            }
            else if (!paramsRead[5] && ((object) Reader.LocalName == (object)id17_Key && (object) Reader.NamespaceURI == (object)id2_Item)) 
            {
              o.@Key = Reader.ReadElementString();
              paramsRead[5] = true;
            }
            else if (!paramsRead[6] && ((object) Reader.LocalName == (object)id18_DataType && (object) Reader.NamespaceURI == (object)id2_Item)) 
            {
              o.@DataType = Reader.ReadElementString();
              paramsRead[6] = true;
            }
            else if (!paramsRead[7] && ((object) Reader.LocalName == (object)id19_SortDirection && (object) Reader.NamespaceURI == (object)id2_Item)) 
            {
              o.@SortDirection = Read3_SortDirections(Reader.ReadElementString());
              paramsRead[7] = true;
            }
            else if (!paramsRead[8] && ((object) Reader.LocalName == (object)id20_SortAbsolute && (object) Reader.NamespaceURI == (object)id2_Item)) 
            {
              o.@SortAbsolute = System.Xml.XmlConvert.ToBoolean(Reader.ReadElementString());
              paramsRead[8] = true;
            }
            else if (!paramsRead[9] && ((object) Reader.LocalName == (object)id21_SortOrder && (object) Reader.NamespaceURI == (object)id2_Item)) 
            {
              o.@SortOrder = System.Xml.XmlConvert.ToInt32(Reader.ReadElementString());
              paramsRead[9] = true;
            }
            else if (!paramsRead[10] && ((object) Reader.LocalName == (object)id22_BackColorValue && (object) Reader.NamespaceURI == (object)id2_Item)) 
            {
              o.@BackColorValue = System.Xml.XmlConvert.ToInt32(Reader.ReadElementString());
              paramsRead[10] = true;
            }
            else if (!paramsRead[11] && ((object) Reader.LocalName == (object)id23_ForeColorValue && (object) Reader.NamespaceURI == (object)id2_Item)) 
            {
              o.@ForeColorValue = System.Xml.XmlConvert.ToInt32(Reader.ReadElementString());
              paramsRead[11] = true;
            }
            else if (!paramsRead[12] && ((object) Reader.LocalName == (object)id24_ReadOnly && (object) Reader.NamespaceURI == (object)id2_Item)) 
            {
              o.@ReadOnly = System.Xml.XmlConvert.ToBoolean(Reader.ReadElementString());
              paramsRead[12] = true;
            }
            else if (!paramsRead[13] && ((object) Reader.LocalName == (object)id25_HeaderTag && (object) Reader.NamespaceURI == (object)id2_Item)) 
            {
              o.@HeaderTag = Reader.ReadElementString();
              paramsRead[13] = true;
            }
            else if (!paramsRead[14] && ((object) Reader.LocalName == (object)id26_FontName && (object) Reader.NamespaceURI == (object)id2_Item)) 
            {
              o.@FontName = Reader.ReadElementString();
              paramsRead[14] = true;
            }
            else 
            {
              UnknownNode((object)o);
            }
          }
          else 
          {
            UnknownNode((object)o);
          }
          Reader.MoveToContent();
        }
        ReadEndElement();
        return o;
      }

      /// <remarks/>
      protected virtual MorganStanley.MSDesktop.Rambo.WinGridUtils.ColumnLayout.SortDirections Read3_SortDirections(string s) 
      {
        switch (s) 
        {
          case @"None": return MorganStanley.MSDesktop.Rambo.WinGridUtils.ColumnLayout.SortDirections.@None;
          case @"Ascending": return MorganStanley.MSDesktop.Rambo.WinGridUtils.ColumnLayout.SortDirections.@Ascending;
          case @"Descending": return MorganStanley.MSDesktop.Rambo.WinGridUtils.ColumnLayout.SortDirections.@Descending;
          default: throw CreateUnknownConstantException(s, typeof(MorganStanley.MSDesktop.Rambo.WinGridUtils.ColumnLayout.SortDirections));
        }
      }

      /// <remarks/>
      protected virtual System.Object Read4_Object(bool isNullable, bool checkType) 
      {
        if (isNullable && ReadNull()) return null;
        if (checkType) 
        {
          System.Xml.XmlQualifiedName t = GetXsiType();
          if (t == null)
            return ReadTypedPrimitive(new System.Xml.XmlQualifiedName("anyType", "http://www.w3.org/2001/XMLSchema"));
          else if (((object) ((System.Xml.XmlQualifiedName)t).Name == (object)id1_GridLayout && (object) ((System.Xml.XmlQualifiedName)t).Namespace == (object)id2_Item))
            return Read1_GridLayout(isNullable, false);
          else if (((object) ((System.Xml.XmlQualifiedName)t).Name == (object)id9_ColumnLayout && (object) ((System.Xml.XmlQualifiedName)t).Namespace == (object)id2_Item))
            return Read2_ColumnLayout(isNullable, false);
          else if (((object) ((System.Xml.XmlQualifiedName)t).Name == (object)id27_SortDirections && (object) ((System.Xml.XmlQualifiedName)t).Namespace == (object)id2_Item)) 
          {
            Reader.ReadStartElement();
            object e = Read3_SortDirections(Reader.ReadString());
            ReadEndElement();
            return e;
          }
          else if (((object) ((System.Xml.XmlQualifiedName)t).Name == (object)id28_ArrayOfColumnLayout && (object) ((System.Xml.XmlQualifiedName)t).Namespace == (object)id2_Item)) 
          {
            MorganStanley.MSDesktop.Rambo.WinGridUtils.ColumnLayoutCollection a = null;
            if (!ReadNull()) 
            {
              if ((object)(a) == null) a = new MorganStanley.MSDesktop.Rambo.WinGridUtils.ColumnLayoutCollection();
              MorganStanley.MSDesktop.Rambo.WinGridUtils.ColumnLayoutCollection z_0_0 = (MorganStanley.MSDesktop.Rambo.WinGridUtils.ColumnLayoutCollection)a;
              if (Reader.IsEmptyElement) 
              {
                Reader.Skip();
              }
              else 
              {
                Reader.ReadStartElement();
                Reader.MoveToContent();
                while (Reader.NodeType != System.Xml.XmlNodeType.EndElement) 
                {
                  if (Reader.NodeType == System.Xml.XmlNodeType.Element) 
                  {
                    if (((object) Reader.LocalName == (object)id9_ColumnLayout && (object) Reader.NamespaceURI == (object)id2_Item)) 
                    {
                      if ((object)(z_0_0) == null) Reader.Skip(); else z_0_0.Add(Read2_ColumnLayout(true, true));
                    }
                    else 
                    {
                      UnknownNode(null);
                    }
                  }
                  else 
                  {
                    UnknownNode(null);
                  }
                  Reader.MoveToContent();
                }
                ReadEndElement();
              }
            }
            return a;
          }
          else if (((object) ((System.Xml.XmlQualifiedName)t).Name == (object)id29_RowFontStyleEnum && (object) ((System.Xml.XmlQualifiedName)t).Namespace == (object)id2_Item)) 
          {
            Reader.ReadStartElement();
            object e = Read5_RowFontStyleEnum(Reader.ReadString());
            ReadEndElement();
            return e;
          }
          else
            return ReadTypedPrimitive((System.Xml.XmlQualifiedName)t);
        }
        System.Object o = new System.Object();
        while (Reader.MoveToNextAttribute()) 
        {
          if (!IsXmlnsAttribute(Reader.Name)) 
          {
            UnknownNode((object)o);
          }
        }
        Reader.MoveToElement();
        if (Reader.IsEmptyElement) 
        {
          Reader.Skip();
          return o;
        }
        Reader.ReadStartElement();
        bool[] paramsRead = new bool[0];
        Reader.MoveToContent();
        while (Reader.NodeType != System.Xml.XmlNodeType.EndElement) 
        {
          if (Reader.NodeType == System.Xml.XmlNodeType.Element) 
          {
            UnknownNode((object)o);
          }
          else 
          {
            UnknownNode((object)o);
          }
          Reader.MoveToContent();
        }
        ReadEndElement();
        return o;
      }

      /// <remarks/>
      protected virtual MorganStanley.MSDesktop.Rambo.WinGridUtils.RowFontStyleEnum Read5_RowFontStyleEnum(string s) 
      {
        switch (s) 
        {
          case @"Default": return MorganStanley.MSDesktop.Rambo.WinGridUtils.RowFontStyleEnum.@Default;
          case @"Smallest": return MorganStanley.MSDesktop.Rambo.WinGridUtils.RowFontStyleEnum.@Smallest;
          case @"Smaller": return MorganStanley.MSDesktop.Rambo.WinGridUtils.RowFontStyleEnum.@Smaller;
          case @"Medium": return MorganStanley.MSDesktop.Rambo.WinGridUtils.RowFontStyleEnum.@Medium;
          case @"Larger": return MorganStanley.MSDesktop.Rambo.WinGridUtils.RowFontStyleEnum.@Larger;
          case @"Largest": return MorganStanley.MSDesktop.Rambo.WinGridUtils.RowFontStyleEnum.@Largest;
          default: throw CreateUnknownConstantException(s, typeof(MorganStanley.MSDesktop.Rambo.WinGridUtils.RowFontStyleEnum));
        }
      }
      protected override void InitCallbacks() 
      {
      }

      protected object Read6_Grid() 
      {
        object o = null;
        Reader.MoveToContent();
        if (Reader.NodeType == System.Xml.XmlNodeType.Element) 
        {
          if (((object) Reader.LocalName == (object)id30_Grid && (object) Reader.NamespaceURI == (object)id2_Item)) 
          {
            o = Read1_GridLayout(true, true);
          }
          else 
          {
            throw CreateUnknownNodeException();
          }
        }
        else 
        {
          UnknownNode(null);
        }
        return (object)o;
      }

      System.String id12_Width;
      System.String id18_DataType;
      System.String id13_Hidden;
      System.String id2_Item;
      System.String id15_Format;
      System.String id24_ReadOnly;
      System.String id17_Key;
      System.String id26_FontName;
      System.String id11_Font;
      System.String id20_SortAbsolute;
      System.String id23_ForeColorValue;
      System.String id27_SortDirections;
      System.String id16_Caption;
      System.String id21_SortOrder;
      System.String id4_GridName;
      System.String id29_RowFontStyleEnum;
      System.String id10_BandId;
      System.String id19_SortDirection;
      System.String id22_BackColorValue;
      System.String id25_HeaderTag;
      System.String id5_LayoutName;
      System.String id3_Application;
      System.String id30_Grid;
      System.String id1_GridLayout;
      System.String id8_Columns;
      System.String id14_Index;
      System.String id9_ColumnLayout;
      System.String id7_UpdateID;
      System.String id28_ArrayOfColumnLayout;
      System.String id6_LockedColumns;

      protected override void InitIDs() 
      {
        id12_Width = Reader.NameTable.Add(@"Width");
        id18_DataType = Reader.NameTable.Add(@"DataType");
        id13_Hidden = Reader.NameTable.Add(@"Hidden");
        id2_Item = Reader.NameTable.Add(@"");
        id15_Format = Reader.NameTable.Add(@"Format");
        id24_ReadOnly = Reader.NameTable.Add(@"ReadOnly");
        id17_Key = Reader.NameTable.Add(@"Key");
        id26_FontName = Reader.NameTable.Add(@"FontName");
        id11_Font = Reader.NameTable.Add(@"Font");
        id20_SortAbsolute = Reader.NameTable.Add(@"SortAbsolute");
        id23_ForeColorValue = Reader.NameTable.Add(@"ForeColorValue");
        id27_SortDirections = Reader.NameTable.Add(@"SortDirections");
        id16_Caption = Reader.NameTable.Add(@"Caption");
        id21_SortOrder = Reader.NameTable.Add(@"SortOrder");
        id4_GridName = Reader.NameTable.Add(@"GridName");
        id29_RowFontStyleEnum = Reader.NameTable.Add(@"RowFontStyleEnum");
        id10_BandId = Reader.NameTable.Add(@"BandId");
        id19_SortDirection = Reader.NameTable.Add(@"SortDirection");
        id22_BackColorValue = Reader.NameTable.Add(@"BackColorValue");
        id25_HeaderTag = Reader.NameTable.Add(@"HeaderTag");
        id5_LayoutName = Reader.NameTable.Add(@"LayoutName");
        id3_Application = Reader.NameTable.Add(@"Application");
        id30_Grid = Reader.NameTable.Add(@"Grid");
        id1_GridLayout = Reader.NameTable.Add(@"GridLayout");
        id8_Columns = Reader.NameTable.Add(@"Columns");
        id14_Index = Reader.NameTable.Add(@"Index");
        id9_ColumnLayout = Reader.NameTable.Add(@"ColumnLayout");
        id7_UpdateID = Reader.NameTable.Add(@"UpdateID");
        id28_ArrayOfColumnLayout = Reader.NameTable.Add(@"ArrayOfColumnLayout");
        id6_LockedColumns = Reader.NameTable.Add(@"LockedColumns");
      }
    }

    public class Writer : System.Xml.Serialization.XmlSerializationWriter 
    {

      void Write1_GridLayout(string n, string ns, MorganStanley.MSDesktop.Rambo.WinGridUtils.GridLayout o, bool isNullable, bool needType) 
      {
        if ((object)o == null) 
        {
          if (isNullable) WriteNullTagLiteral(n, ns);
          return;
        }
        if (!needType) 
        {
          System.Type t = o.GetType();
          if (t == typeof(MorganStanley.MSDesktop.Rambo.WinGridUtils.GridLayout))
            ;
          else 
          {
            throw CreateUnknownTypeException(o);
          }
        }
        WriteStartElement(n, ns, o);
        if (needType) WriteXsiType(@"GridLayout", @"");
        WriteElementString(@"Application", @"", ((System.String)o.@Application));
        WriteElementString(@"GridName", @"", ((System.String)o.@GridName));
        WriteElementString(@"LayoutName", @"", ((System.String)o.@LayoutName));
        WriteElementStringRaw(@"LockedColumns", @"", System.Xml.XmlConvert.ToString((System.Int32)((System.Int32)o.@LockedColumns)));
        WriteElementString(@"UpdateID", @"", ((System.String)o.@UpdateID));
      {
        MorganStanley.MSDesktop.Rambo.WinGridUtils.ColumnLayoutCollection a = (MorganStanley.MSDesktop.Rambo.WinGridUtils.ColumnLayoutCollection)((MorganStanley.MSDesktop.Rambo.WinGridUtils.ColumnLayoutCollection)o.@Columns);
        if (a != null && a.Columns != null)
        {
          WriteStartElement(@"Columns", @"");
          foreach (var columnLayout in a.Columns)
          {
            //omit columns that are hidden and do not participate in sorting
            if (columnLayout.Hidden && columnLayout.SortDirection == ColumnLayout.SortDirections.None)
              continue;

            Write2_ColumnLayout(@"ColumnLayout", @"", columnLayout, true, false);
          }
          WriteEndElement();
        }
      }
        WriteElementStringRaw(@"BandId", @"", System.Xml.XmlConvert.ToString((System.Int32)((System.Int32)o.@BandId)));
        WriteElementString(@"Font", @"", Write5_RowFontStyleEnum(((MorganStanley.MSDesktop.Rambo.WinGridUtils.RowFontStyleEnum)o.@Font)));
        WriteEndElement(o);
      }

      void Write2_ColumnLayout(string n, string ns, MorganStanley.MSDesktop.Rambo.WinGridUtils.ColumnLayout o, bool isNullable, bool needType) 
      {
        if ((object)o == null) 
        {
          if (isNullable) WriteNullTagLiteral(n, ns);
          return;
        }
        if (!needType) 
        {
          System.Type t = o.GetType();
          if (t == typeof(MorganStanley.MSDesktop.Rambo.WinGridUtils.ColumnLayout))
            ;
          else 
          {
            throw CreateUnknownTypeException(o);
          }
        }
        WriteStartElement(n, ns, o);
        if (needType) WriteXsiType(@"ColumnLayout", @"");
        WriteElementStringRaw(@"Width", @"", System.Xml.XmlConvert.ToString((System.Int32)((System.Int32)o.@Width)));
        WriteElementStringRaw(@"Hidden", @"", System.Xml.XmlConvert.ToString((System.Boolean)((System.Boolean)o.@Hidden)));
        WriteElementStringRaw(@"Index", @"", System.Xml.XmlConvert.ToString((System.Int32)((System.Int32)o.@Index)));
        WriteElementString(@"Format", @"", ((System.String)o.@Format));
        WriteElementString(@"Caption", @"", ((System.String)o.@Caption));
        
        WriteElementString(@"Key", @"", ((System.String)o.@Key));
        WriteElementString(@"DataType", @"", ((System.String)o.@DataType));
        WriteElementString(@"SortDirection", @"", Write3_SortDirections(((MorganStanley.MSDesktop.Rambo.WinGridUtils.ColumnLayout.SortDirections)o.@SortDirection)));
        WriteElementStringRaw(@"SortAbsolute", @"", System.Xml.XmlConvert.ToString((System.Boolean)((System.Boolean)o.@SortAbsolute)));
        WriteElementStringRaw(@"SortOrder", @"", System.Xml.XmlConvert.ToString((System.Int32)((System.Int32)o.@SortOrder)));
        WriteElementStringRaw(@"BackColorValue", @"", System.Xml.XmlConvert.ToString((System.Int32)((System.Int32)o.@BackColorValue)));
        WriteElementStringRaw(@"ForeColorValue", @"", System.Xml.XmlConvert.ToString((System.Int32)((System.Int32)o.@ForeColorValue)));
        WriteElementStringRaw(@"ReadOnly", @"", System.Xml.XmlConvert.ToString((System.Boolean)((System.Boolean)o.@ReadOnly)));
        WriteElementString(@"HeaderTag", @"", ((System.String)o.@HeaderTag));
        WriteElementString(@"FontName", @"", ((System.String)o.@FontName));
        WriteEndElement(o);
      }

      string Write3_SortDirections(MorganStanley.MSDesktop.Rambo.WinGridUtils.ColumnLayout.SortDirections v) 
      {
        string s = null;
        switch (v) 
        {
          case MorganStanley.MSDesktop.Rambo.WinGridUtils.ColumnLayout.SortDirections.@None: s = @"None"; break;
          case MorganStanley.MSDesktop.Rambo.WinGridUtils.ColumnLayout.SortDirections.@Ascending: s = @"Ascending"; break;
          case MorganStanley.MSDesktop.Rambo.WinGridUtils.ColumnLayout.SortDirections.@Descending: s = @"Descending"; break;
          default: s = ((System.Int64)v).ToString(); break;
        }
        return s;
      }

      void Write4_Object(string n, string ns, System.Object o, bool isNullable, bool needType) 
      {
        if ((object)o == null) 
        {
          if (isNullable) WriteNullTagLiteral(n, ns);
          return;
        }
        if (!needType) 
        {
          System.Type t = o.GetType();
          if (t == typeof(System.Object))
            ;
          else if (t == typeof(MorganStanley.MSDesktop.Rambo.WinGridUtils.GridLayout)) 
          {
            Write1_GridLayout(n, ns, (MorganStanley.MSDesktop.Rambo.WinGridUtils.GridLayout)o, isNullable, true);
            return;
          }
          else if (t == typeof(MorganStanley.MSDesktop.Rambo.WinGridUtils.ColumnLayout)) 
          {
            Write2_ColumnLayout(n, ns, (MorganStanley.MSDesktop.Rambo.WinGridUtils.ColumnLayout)o, isNullable, true);
            return;
          }
          else if (t == typeof(MorganStanley.MSDesktop.Rambo.WinGridUtils.ColumnLayout.SortDirections)) 
          {
            Writer.WriteStartElement(n, ns);
            WriteXsiType(@"SortDirections", @"");
            Writer.WriteString(Write3_SortDirections((MorganStanley.MSDesktop.Rambo.WinGridUtils.ColumnLayout.SortDirections)o));
            Writer.WriteEndElement();
            return;
          }
          else if (t == typeof(MorganStanley.MSDesktop.Rambo.WinGridUtils.ColumnLayoutCollection)) 
          {
            Writer.WriteStartElement(n, ns);
            WriteXsiType(@"ArrayOfColumnLayout", @"");
          {
            MorganStanley.MSDesktop.Rambo.WinGridUtils.ColumnLayoutCollection a = (MorganStanley.MSDesktop.Rambo.WinGridUtils.ColumnLayoutCollection)o;
            if (a != null) 
            {
              for (int ia = 0; ia < a.Count; ia++) 
              {
                Write2_ColumnLayout(@"ColumnLayout", @"", ((MorganStanley.MSDesktop.Rambo.WinGridUtils.ColumnLayout)a[ia]), true, false);
              }
            }
          }
            Writer.WriteEndElement();
            return;
          }
          else if (t == typeof(MorganStanley.MSDesktop.Rambo.WinGridUtils.RowFontStyleEnum)) 
          {
            Writer.WriteStartElement(n, ns);
            WriteXsiType(@"RowFontStyleEnum", @"");
            Writer.WriteString(Write5_RowFontStyleEnum((MorganStanley.MSDesktop.Rambo.WinGridUtils.RowFontStyleEnum)o));
            Writer.WriteEndElement();
            return;
          }
          else 
          {
            WriteTypedPrimitive(n, ns, o, true);
            return;
          }
        }
        WriteStartElement(n, ns, o);
        WriteEndElement(o);
      }

      string Write5_RowFontStyleEnum(MorganStanley.MSDesktop.Rambo.WinGridUtils.RowFontStyleEnum v) 
      {
        string s = null;
        switch (v) 
        {
          case MorganStanley.MSDesktop.Rambo.WinGridUtils.RowFontStyleEnum.@Default: s = @"Default"; break;
          case MorganStanley.MSDesktop.Rambo.WinGridUtils.RowFontStyleEnum.@Smallest: s = @"Smallest"; break;
          case MorganStanley.MSDesktop.Rambo.WinGridUtils.RowFontStyleEnum.@Smaller: s = @"Smaller"; break;
          case MorganStanley.MSDesktop.Rambo.WinGridUtils.RowFontStyleEnum.@Medium: s = @"Medium"; break;
          case MorganStanley.MSDesktop.Rambo.WinGridUtils.RowFontStyleEnum.@Larger: s = @"Larger"; break;
          case MorganStanley.MSDesktop.Rambo.WinGridUtils.RowFontStyleEnum.@Largest: s = @"Largest"; break;
          default: s = ((System.Int64)v).ToString(); break;
        }
        return s;
      }

      protected override void InitCallbacks() 
      {
      }

      protected void Write6_Grid(object o) 
      {
        WriteStartDocument();
        if (o == null) 
        {
          WriteNullTagLiteral(@"Grid", @"");
          return;
        }
        TopLevelElement();
        Write1_GridLayout(@"Grid", @"", ((MorganStanley.MSDesktop.Rambo.WinGridUtils.GridLayout)o), true, false);
      }
    }

    /// <remarks/>
  }
    
  public class GridReader : GridLayoutSerializer.Reader
  {
        
        
    /// <remarks/>
    protected override MorganStanley.MSDesktop.Rambo.WinGridUtils.GridLayout Read1_GridLayout(bool isNullable, bool checkType)
    {
      MorganStanley.MSDesktop.Rambo.WinGridUtils.GridLayout obj = base.Read1_GridLayout(isNullable, checkType);
      GridLayoutDeserializedHandler handler = GridLayoutDeserialized;
      if (handler != null)
        handler(obj);

      return obj;
    }
        
    /// <remarks/>
    protected override MorganStanley.MSDesktop.Rambo.WinGridUtils.ColumnLayout Read2_ColumnLayout(bool isNullable, bool checkType)
    {
      MorganStanley.MSDesktop.Rambo.WinGridUtils.ColumnLayout obj = base.Read2_ColumnLayout(isNullable, checkType);
      ColumnLayoutDeserializedHandler handler = ColumnLayoutDeserialized;
      if (handler != null)
        handler(obj);

      return obj;
    }
        
    /// <remarks/>
    protected override MorganStanley.MSDesktop.Rambo.WinGridUtils.ColumnLayout.SortDirections Read3_SortDirections(string s)
    {
      MorganStanley.MSDesktop.Rambo.WinGridUtils.ColumnLayout.SortDirections obj = base.Read3_SortDirections(s);
      SortDirectionsDeserializedHandler handler = SortDirectionsDeserialized;
      if (handler != null)
        handler(obj);

      return obj;
    }
        
    /// <remarks/>
    protected override MorganStanley.MSDesktop.Rambo.WinGridUtils.RowFontStyleEnum Read5_RowFontStyleEnum(string s)
    {
      MorganStanley.MSDesktop.Rambo.WinGridUtils.RowFontStyleEnum obj = base.Read5_RowFontStyleEnum(s);
      RowFontStyleEnumDeserializedHandler handler = RowFontStyleEnumDeserialized;
      if (handler != null)
        handler(obj);

      return obj;
    }
        
    /// <summary>Reads an object of type MorganStanley.MSDesktop.Rambo.WinGridUtils.GridLayout.</summary>
    internal MorganStanley.MSDesktop.Rambo.WinGridUtils.GridLayout Read()
    {
      return (MorganStanley.MSDesktop.Rambo.WinGridUtils.GridLayout) Read6_Grid();
    }
    public event GridLayoutDeserializedHandler GridLayoutDeserialized;
        
    public event ColumnLayoutDeserializedHandler ColumnLayoutDeserialized;
        
    public event SortDirectionsDeserializedHandler SortDirectionsDeserialized;
        
    public event RowFontStyleEnumDeserializedHandler RowFontStyleEnumDeserialized;
  }
    
  public delegate void GridLayoutDeserializedHandler(MorganStanley.MSDesktop.Rambo.WinGridUtils.GridLayout gridlayout);
    
  public delegate void ColumnLayoutDeserializedHandler(MorganStanley.MSDesktop.Rambo.WinGridUtils.ColumnLayout columnlayout);
    
  public delegate void SortDirectionsDeserializedHandler(MorganStanley.MSDesktop.Rambo.WinGridUtils.ColumnLayout.SortDirections sortdirections);
    
  public delegate void RowFontStyleEnumDeserializedHandler(MorganStanley.MSDesktop.Rambo.WinGridUtils.RowFontStyleEnum rowfontstyleenum);
    
  public class GridWriter : GridLayoutSerializer.Writer
  {
        
        
    /// <summary>Writes an object of type MorganStanley.MSDesktop.Rambo.WinGridUtils.GridLayout.</summary>
    internal void Write(MorganStanley.MSDesktop.Rambo.WinGridUtils.GridLayout obj)
    {
      Write6_Grid(obj);
    }
    }
}
