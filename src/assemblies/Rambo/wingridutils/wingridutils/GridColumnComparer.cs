using System;
using System.Collections;
using Infragistics.Win.UltraWinGrid;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  /// <summary>
  /// Summary description for GridColumnComparer.
  /// </summary>
  public class GridColumnComparer : IComparer
  {
    private bool _absolute;

    public GridColumnComparer() : this(false)
    {}

    public GridColumnComparer(bool absolute_)
    {
      _absolute = absolute_;
    }

    public bool Absolute
    {
      get { return _absolute; }
    }

    public int Compare( object x, object y )
    {
      return CompareObject(x, y);
    }

    public virtual int CompareObject(object x, object y)
    {
            // Passed in objects are cells. So you have to typecast them to UltraGridCell objects first. 
      UltraGridCell xCell = (UltraGridCell)x; 
      UltraGridCell yCell = (UltraGridCell)y; 
 
      // Do your own comparision between the values of xCell and yCell and return a negative  
      // number if xCell is less than yCell, positive number if xCell is greater than yCell,  
      // and 0 if xCell and yCell are equal. 


      if (xCell == null && yCell == null)
      {
        return 0;
      }
      else if (xCell == null && yCell != null)
      {
        return -1;
      }
      else if (xCell != null && yCell == null)
      {
        return 1;
      }
      else
      {
        object xVal = xCell.Value;
        object yVal = yCell.Value;

        if (xVal == DBNull.Value) xVal = null;
        if (yVal == DBNull.Value) yVal = null;

        if (xVal == null && yVal == null)
        {
          return 0;
        }
        else if (xVal == null && yVal != null)
        {
          return -1;
        }
        else if (xVal != null && yVal == null)
        {
          return 1;
        }
        else if (xVal.GetType() != yVal.GetType())
        {
          //This is a specific case when we do not have a data for this column for rows, for instance 
          //if we are sorted by exporation date then stocks will not have a Expdate hence the x or y Val will be 
          //DefaultGrouping.Empty (the ToString value of the DefaultGrouping.Empty is equal to string.empty)
          return (xVal.GetType() == typeof(string) || xVal == DBNull.Value || xVal.ToString().Equals(string.Empty)) ? -1 :
            ((yVal.GetType() == typeof(string) || yVal == DBNull.Value || yVal.ToString().Equals(string.Empty)) ? 1 :
            string.Compare(xVal.GetType().ToString(), yVal.GetType().ToString()));
        }
        else
        {
          if (xVal.GetType() == typeof(DateTime))
          {
            return DateTime.Compare((DateTime) xVal, (DateTime) yVal);
          }
          else if (xVal.GetType() == typeof(Double))
          {
            double xd = _absolute ? Math.Abs((double)xVal) : (double)xVal;
            double yd = _absolute ? Math.Abs((double)yVal) : (double)yVal;

            return xd.CompareTo(yd);
          }
          else if(xVal is IComparable)
          {
            IComparable xCompare = xVal as IComparable;

            return xCompare.CompareTo(yVal);
          }
          else
          {
            return string.Compare(xVal.ToString(), yVal.ToString());
          }
        }
      }
    }
  }
}
