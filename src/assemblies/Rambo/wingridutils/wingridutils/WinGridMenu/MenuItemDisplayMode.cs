﻿namespace MorganStanley.MSDesktop.Rambo.WinGridUtils.WinGridMenu
{
  /// <summary>
  /// Denotes the mechanism in which the menu should be displayed
  /// </summary>
  public enum MenuItemDisplayMode
  {
    /// <summary>
    /// Normal display - Visible and Enabled
    /// </summary>
    Enabled,
    /// <summary>
    /// Hidden from view - should be used with the menu should never be used
    /// </summary>
    Hidden,
    /// <summary>
    /// Disabled - should be used with a corresponding tooltip displaying why this menu item is invalid
    /// </summary>
    Disabled
  }
}
