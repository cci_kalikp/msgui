using System;
using MorganStanley.MSDesktop.Rambo.Desktop.Utils;
using Infragistics.Win.UltraWinToolbars;
using MorganStanley.MSDesktop.Rambo.MVCFramework;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils.WinGridMenu
{
  /// <summary>
  /// Action driven context menu.
  /// </summary>
  public class ActionContextMenu : PopupMenuTool
  {
    protected IViewControl _view;
    protected ILogger _log = LoggerManager.GetLogger(typeof(ActionContextMenu));

    /// <summary>
    /// Creates a new action driven context menu for the provided view.
    /// </summary>
    /// <param name="view_">The view owning this menu.</param>
    /// <param name="key">The id string of the Context Menu</param>
    public ActionContextMenu(IViewControl view_, string key) : base(key)
    {    
      _view = view_;
      SharedProps.Caption = key;
    }

    /// <summary>
    /// Gets the parent view for this menu.
    /// </summary>
    public IViewControl View
    {
      get { return _view; }
    }

    /// <summary>
    /// Handles the on popup event and ensure menu items are up-to-date.
    /// </summary>  
    protected override void OnBeforeToolDropdown(BeforeToolDropdownEventArgs e)
    {
      base.OnBeforeToolDropdown(e);
      if (VisibleResolved)
      {
        if (_view == null)
        {
          throw new InvalidOperationException("Current view could not be determined.");
        }

        RefreshMenuItems(Tools);
      }
    }

    /// <summary>
    /// Helper method to refresh the menu items.
    /// </summary>
    /// <param name="items_">The item collection to refresh.</param>
    protected void RefreshMenuItems(ToolsCollection items_)
    {
      if (items_ == null)
      {
        return;
      }

      ToolBase previousMenuItem = null;

      foreach (var item in items_)
      {

        var menuItem = item;
        if (menuItem != null)
        {
          var actionMenuItem = item as ActionContextMenuItem;
          if (actionMenuItem != null)
          {
            if (actionMenuItem.Action != null)
            {
              MenuItemDisplayMode displayMode = actionMenuItem.Action.GetMenuItemDisplayMode();

              switch (displayMode)
              {
                case MenuItemDisplayMode.Hidden:
                  actionMenuItem.SharedProps.Visible = false;
                  actionMenuItem.SharedProps.Enabled = false;
                  break;
                case MenuItemDisplayMode.Disabled:
                  actionMenuItem.SharedProps.Visible = true;
                  actionMenuItem.SharedProps.Enabled = false;
                  break;
                default:
                  actionMenuItem.SharedProps.Visible = true;
                  actionMenuItem.SharedProps.Enabled = true;
                  break;
              }
            }
            else
            {
              actionMenuItem.SharedProps.Visible = true;
              actionMenuItem.SharedProps.Enabled = true;
            }

            try
            {
              actionMenuItem.Refresh(_view);
            }
            catch (Exception ex_)
            {
              _log.Error("RefreshMenuItems", "Error refreshing menu item.", ex_);
              actionMenuItem.SharedProps.Enabled = false;
            }
          }
        }

        if (item.CaptionResolved == "-")
        {
          if (previousMenuItem == null || previousMenuItem.CaptionResolved == "-")
          {
            item.SharedProps.Visible = false;
          }
          else
          {
            item.SharedProps.Visible = true;
          }
        }

        if (item.VisibleResolved)
        {
          previousMenuItem = item;
        }
      }
    }
  }
}