using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Xml;
using Infragistics.Win.UltraWinToolbars;
using MorganStanley.MSDesktop.Rambo.Desktop.Utils;
using MorganStanley.MSDesktop.Rambo.MVCFramework;
using MorganStanley.IED.Concord.Configuration;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils.WinGridMenu
{
  /// <summary>
  /// Factory singleton for creatin action context menus.
  /// </summary>
  public class ContextMenuFactory
  {
    private static ContextMenuFactory _instance;
    private readonly Dictionary<string, ContextConfigMenu> _configMap = new Dictionary<string, ContextConfigMenu>();
    protected ILogger _log = LoggerManager.GetLogger(typeof(ContextMenuFactory));
    private static readonly Configurator _configurator = (Configurator)ConfigurationManager.GetConfig("ContextMenus");
    private static readonly object _lock = new object();

    private ContextMenuFactory()
    {
      Configure(_configurator);
    }

    /// <summary>
    /// Returns the singleton factory instance.
    /// </summary>
    public static ContextMenuFactory Instance
    {
      get
      {
        lock (_lock)
        {
          if (_instance == null)
          {
            _instance = new ContextMenuFactory();
          }
        }
        return _instance;
      }
    }

    public static bool IsConfigAvailable()
    {
      return (_configurator.GetNode("ContextActions", null) != null);
    }

    /// <summary>
    /// Configures the singleton instance based on the provided configuration path.
    /// </summary>
    /// <param name="configPath_">The configuration path.</param>
    public void Configure(string configPath_)
    {
      List<ContextConfigMenu> contextConfigs = ContextMenuSerializer.Deserialize(new XmlTextReader(configPath_));
      _configMap.Clear();

      foreach (ContextConfigMenu config in contextConfigs)
      {
        _configMap.Add(config.Id, config);
      }
    }

    /// <summary>
    /// Configures the singleton instance based on the provided configurator.
    /// </summary>
    /// <param name="configurator_">The configurator to use.</param>
    public void Configure(Configurator configurator_)
    {
      List<ContextConfigMenu> contextConfigs = ContextMenuSerializer.Deserialize(configurator_);
      _configMap.Clear();

      foreach (ContextConfigMenu config in contextConfigs)
      {
        _configMap.Add(config.Id, config);
      }
    }

    /// <summary>
    /// Build up an action driven context menu for the provided view and id.
    /// </summary>
    /// <param name="view_">The view to build the context menu for.</param>
    /// <param name="id_">The ID used to describe this context menu type.</param>
    /// <returns>A new context menu instance.</returns>
    public PopupMenuTool CreateContextMenu(IViewControl view_, string id_, UltraToolbarsManager toolbarsManager_)
    {
      if (_configMap.ContainsKey(id_))
      {
        ContextConfigMenu config = _configMap[id_];
        PopupMenuTool menu = ToContextMenu(config, view_, id_, toolbarsManager_);
        menu.SharedProps.Caption = config.Caption;
        return menu;
      }

      return null;
    }

    /// <summary>
    /// Converts the contextconfigmenu instance into a new context menu instance.
    /// </summary>
    /// <param name="config_">The context config instance.</param>
    /// <param name="view_">The view for this context menu.</param>
    /// <param name="key_">The id string of the menu</param>
    /// <returns>A new context menu instance.</returns>
    protected PopupMenuTool ToContextMenu(ContextConfigMenu config_, IViewControl view_, string key_, UltraToolbarsManager toolbarsManager)
    {
      var menu = new ActionContextMenu(view_, key_);

      AddContextItems(menu, config_.ContextItems, null, toolbarsManager);

      return menu;
    }

    /// <summary>
    /// Helper method to add context menu items to the current context strip.
    /// </summary>
    /// <param name="contextStrip_">The context strip to add to.</param>
    /// <param name="contextItems_">The conetxt menu items to add.</param>
    /// <param name="parentItem_">The current parent context menu item.</param>
    protected void AddContextItems(ActionContextMenu contextStrip_, List<ContextConfigItem> contextItems_, PopupMenuTool parentItem_, UltraToolbarsManager toolbarsManager)
    {
      bool isSeparator = false;

      foreach (ContextConfigItem configItem in contextItems_)
      {
        ToolBase menuItem = null;

        if (configItem.Text == ContextConfigItem.SEPARATOR)
        {
          isSeparator = true;
          continue;
        }

        if (!string.IsNullOrEmpty(configItem.ActionType))
        {
          ContextActionBase action = null;
          try
          {
            action = Activator.CreateInstance(Type.GetType(configItem.ActionType)) as ContextActionBase;
            if (action == null)
            {
              continue;
            }
          }
          catch (Exception ex_)
          {
            _log.Error("AddContextItems", "Unable to create action type: " + configItem.ActionType, ex_);
          }

          // Custom menu type - requires reflection to load the menu type instance.
          if (!string.IsNullOrEmpty(configItem.MenuType))
          {
            try
            {
              object menu =
                Activator.CreateInstance(Type.GetType(configItem.MenuType),
                                         new object[] { configItem.Text, configItem.Image, contextStrip_.View, action });
              if (menu != null && menu is ActionContextMenuItem)
              {
                menuItem = menu as ActionContextMenuItem;
              }
              else
              {
                continue;
              }
            }
            catch (Exception ex_)
            {
              _log.Error("AddContextItems", "Unable to create menu type: " + configItem.MenuType, ex_);
            }
          }
          // Create a default action context menu item - no specific type supplied
          else
          {
            try
            {
              menuItem = new ActionContextMenuItem(configItem.Text, configItem.Image, contextStrip_.View, action);
            }
            catch (Exception ex_)
            {
              _log.Error("AddContextItems", "Unable to create context menu: " + configItem.Text, ex_);
            }
          }
        }
        else if (configItem.ChildItems != null && configItem.ChildItems.Count > 0)
        {
          menuItem = new PopupMenuTool(configItem.Text);
          menuItem.SharedProps.Caption = configItem.Text;
        }
        else
        {
          menuItem = new LabelTool(configItem.Text);
          menuItem.SharedProps.Caption = configItem.Text;
        }

        if (menuItem != null)
        {
          if (isSeparator)
          {
            menuItem.InstanceProps.IsFirstInGroup = true;
            isSeparator = false;
          }

          if (configItem.ShortCut != Shortcut.None)
          {
            menuItem.SharedProps.Shortcut = configItem.ShortCut;
          }

          /*
          if (configItem.Image != null)
          {
            resolvedMenuItem.Image = configItem.Image;
          }
           */

          toolbarsManager.Tools.Add(menuItem);

          if (parentItem_ == null)
          {
            contextStrip_.Tools.Add(menuItem);
          }
          else
          {
            parentItem_.Tools.Add(menuItem);
          }

          if (configItem.ChildItems != null && configItem.ChildItems.Count > 0)
          {
            AddContextItems(contextStrip_, configItem.ChildItems, menuItem as PopupMenuTool, toolbarsManager);
          }
        }
      }
    }
  }
}