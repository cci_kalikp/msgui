using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using System.Xml.XPath;

using MorganStanley.MSDesktop.Rambo.Desktop.Utils;
using MorganStanley.IED.Concord.Configuration;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils.WinGridMenu
{
  /// <summary>
  /// Serializer class to convert context configuration into context menu instances.
  /// </summary>
  internal class ContextMenuSerializer
  {
    private static readonly Dictionary<string, XmlNode> _menuRefHash = new Dictionary<string, XmlNode>();

    #region Static Methods

    /// <summary>
    /// Deserializes the context configuration into context config menus.
    /// </summary>
    /// <param name="configurator_">The configuator to deserialise from.</param>
    /// <returns>A list of context config menus.</returns>
    public static List<ContextConfigMenu> Deserialize(Configurator configurator_)
    {
      XmlNode contextActionsNode = configurator_.GetNode("ContextActions", null);
      var actionMap = new Dictionary<string, string>();
      XmlNodeList contextActionList = contextActionsNode.SelectNodes("ContextAction");
      foreach (XmlNode actionNode in contextActionList)
      {
        actionMap[XMLUtils.GetNodeValue(actionNode, "@id")] = XMLUtils.GetNodeValue(actionNode, "@Type");
      }

      XmlNode menuNodes = configurator_.GetNode("ContextMenus", null);
      
      var contextMenuRefList = menuNodes.SelectNodes("ContextMenuRef");
      foreach (XmlNode refNode in contextMenuRefList)
      {
        _menuRefHash[XMLUtils.GetNodeValue(refNode, "@refId")] = refNode;
      }

      var configList = new List<ContextConfigMenu>();

      XmlNodeList contextMenuList = menuNodes.SelectNodes("ContextMenu");
      foreach (XmlNode menuNode in contextMenuList)
      {
        var config = new ContextConfigMenu
                       {
                         ContextItems = new List<ContextConfigItem>(),
                         Id = XMLUtils.GetNodeValue(menuNode, "@id"),
                         Caption = XMLUtils.GetNodeValue(menuNode, "@Caption")
                       };

        XmlNodeList menuItemList = menuNode.SelectNodes("*[name() = 'MenuItem' or name() = 'MenuRef']");
        config.ContextItems = CreateContextItems(menuItemList, actionMap);

        configList.Add(config);
      }

      return configList;
    }

    /// <summary>
    /// Deserializes the context configuration into context config menus.
    /// </summary>
    /// <param name="reader_">The reader to deserialise from.</param>
    /// <returns>A list of context config menus.</returns>
    public static List<ContextConfigMenu> Deserialize(XmlReader reader_)
    {
      var doc = new XmlDocument();
      doc.Load(reader_);

      XmlNodeList actionNodes = doc.SelectNodes("//Configuration/ContextActions/ContextAction");
      var actionMap = new Dictionary<string, string>();
      foreach (XmlNode actionNode in actionNodes)
      {
        actionMap[XMLUtils.GetNodeValue(actionNode, "@id")] = XMLUtils.GetNodeValue(actionNode, "@Type");
      }

      XmlNodeList menuNodes = doc.SelectNodes("//Configuration/ContextMenus/ContextMenu");

      var configList = new List<ContextConfigMenu>();

      foreach (XmlNode menuNode in menuNodes)
      {
        var config = new ContextConfigMenu
                       {
                         ContextItems = new List<ContextConfigItem>(),
                         Id = XMLUtils.GetNodeValue(menuNode, "@id")
                       };

        XmlNodeList menuItemList = menuNode.SelectNodes("MenuItem");
        config.ContextItems = CreateContextItems(menuItemList, actionMap);

        configList.Add(config);
      }

      return configList;
    }

    /// <summary>
    /// Helper method to build a list of context config items from the provided XML.
    /// </summary>
    /// <param name="nodes_"></param>
    /// <param name="actionMap_"></param>
    /// <returns></returns>
    private static List<ContextConfigItem> CreateContextItems(XmlNodeList nodes_, IDictionary<string, string> actionMap_)
    {
      var itemList = new List<ContextConfigItem>();

      foreach (XmlNode itemNode in nodes_)
      {
        switch (itemNode.Name)
        {
          case "MenuItem":
            var item = new ContextConfigItem {Text = XMLUtils.GetNodeValue(itemNode, "@Text")};
            if (item.Text != ContextConfigItem.SEPARATOR)
            {
              InitializeMenuItem(itemNode, item, actionMap_);
              XmlNodeList menuItemList = itemNode.SelectNodes("MenuItem");
              if (menuItemList != null && menuItemList.Count > 0)
              {
                item.ChildItems = CreateContextItems(menuItemList, actionMap_);
              }
            }

            itemList.Add(item);
            break;
          case "MenuRef":
            var refId = XMLUtils.GetNodeValue(itemNode, "@refId");
            if (_menuRefHash.ContainsKey(refId))
            {
              XmlNodeList menuItemList = _menuRefHash[refId].SelectNodes("MenuItem");
              if (menuItemList != null && menuItemList.Count > 0)
              {
                itemList.AddRange(CreateContextItems(menuItemList, actionMap_));
              }
            }
            break;
        }        
      }
      return itemList;
    }

    private static void InitializeMenuItem(IXPathNavigable itemNode_, ContextConfigItem item_, IDictionary<string, string> actionMap_)
    {
      string actionType = XMLUtils.GetNodeValue(itemNode_, "@Action");
      if (!string.IsNullOrEmpty(actionType) && actionMap_.ContainsKey(actionType))
      {
        item_.ActionType = actionMap_[actionType];
      }

      string imageName = XMLUtils.GetNodeValue(itemNode_, "@Image");
      if (!string.IsNullOrEmpty(imageName))
      {
        item_.Image = new Bitmap(imageName);
      }

      string shortCutString = XMLUtils.GetNodeValue(itemNode_, "@Shortcut");
      if (!string.IsNullOrEmpty(shortCutString))
      {
        Match matchCS = Regex.Match(shortCutString, "Control\\+Shift\\+([A-Z0-9])");
        Match matchC = Regex.Match(shortCutString, "Control\\+([A-Z0-9])");
        Match matchS = Regex.Match(shortCutString, "Shift\\+([A-Z0-9])");
        Shortcut shortcut;
        if (matchCS.Success &&
            EnumUtils.TryParse(string.Format("CtrlShift{0}", matchCS.Groups[1]), out shortcut))
        {
          item_.ShortCut = shortcut;
        }
        else if (matchC.Success && EnumUtils.TryParse(string.Format("Ctrl{0}", matchC.Groups[1]), out shortcut))
        {
          item_.ShortCut = shortcut;
        }
        else if (matchS.Success && EnumUtils.TryParse(string.Format("Shift{0}", matchS.Groups[1]), out shortcut))
        {
          item_.ShortCut = shortcut;
        }
      }

      item_.MenuType = XMLUtils.GetNodeValue(itemNode_, "@Type");
    }

    #endregion
  }
}