using System.Collections.Generic;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils.WinGridMenu
{
  /// <summary>
  /// Simple configuration representation of a context menu.
  /// </summary>
  public class ContextConfigMenu
  {
    public const string TEXT_ATTRIB = "Text";

    public string Id;
    public string Caption;
    public List<ContextConfigItem> ContextItems;
  }
}