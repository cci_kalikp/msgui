using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils.WinGridMenu
{
  /// <summary>
  /// Simple configuration representation of a context menu item.
  /// </summary>
  public class ContextConfigItem
  {
    public const string SEPARATOR = "-";

    public string Text;
    public List<ContextConfigItem> ChildItems;
    public string ActionType;
    public string MenuType;
    public Image Image;
    public Shortcut ShortCut;
  }
}