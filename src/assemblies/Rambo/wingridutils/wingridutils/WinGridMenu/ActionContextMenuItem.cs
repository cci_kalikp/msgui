using System;
using System.Drawing;
using MorganStanley.MSDesktop.Rambo.Desktop.Utils;
using Infragistics.Win.UltraWinToolbars;
using MorganStanley.MSDesktop.Rambo.MVCFramework;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils.WinGridMenu
{
  /// <summary>
  /// A simple action context driven menu item - based on a tool strip menu item.
  /// </summary>
  public class ActionContextMenuItem : ButtonTool
  {
    protected IViewControl _view;
    protected ContextActionBase _action;
    protected ILogger _log = LoggerManager.GetLogger(typeof(ActionContextMenuItem));
    protected Image _image;

    /// <summary>
    /// Creates a new action driven context menu item.
    /// </summary>
    /// <param name="text_">The Menu item text.</param>
    /// <param name="image_">The Menu item image.</param>
    /// <param name="view_">The Menu item view.</param>
    /// <param name="action_">The action to apply for this menu item.</param>
    public ActionContextMenuItem(string text_, Image image_, IViewControl view_, ContextActionBase action_)
      : base(text_)
    {
      Init(view_, action_);
      SharedProps.Caption = text_;
      this._image = image_;
    }

    /// <summary>
    /// Helper method to initialise this menu item.
    /// </summary>
    /// <param name="view_">The parent view.</param>
    /// <param name="action_">The action to apply.</param>
    protected virtual void Init(IViewControl view_, ContextActionBase action_)
    {
      _view = view_;
      _action = action_;
      _action.Init(view_, this);
      ToolClick += HandleClick;
    }

    /// <summary>
    /// Event handler for the click event of this menu item.
    /// </summary>
    /// <param name="sender_">The sender.</param>
    /// <param name="e_">Empty event args.</param>
    protected void HandleClick(object sender_, EventArgs e_)
    {
      var menuItem = sender_ as ToolBase;
      if (menuItem != null)
      {
        if (menuItem is PopupMenuTool && ((PopupMenuTool)menuItem).Tools.Count != 0)
        {
          try
          {
            Refresh(_view);
          }
          catch (Exception ex_)
          {
            _log.Error("HandleClick", "Error refreshing child items for" + menuItem.SharedProps.Caption, ex_);
          }
        }
        else
        {
          try
          {
            _action.Execute(menuItem.Tag);
          }
          catch (Exception ex_)
          {
            _log.Error("HandleClick", "Error executing action for: " + menuItem.SharedProps.Caption, ex_);
          }
        }
      }
    }

    /// <summary>
    /// Gets the menu item hierarchy for the logging display.
    /// </summary>
    /// <param name="menuItem">The menu item that was selected.</param>
    /// <returns>The hierarchy of menu items for the clicked menu, from highest to lowest, separated by '->'</returns>
    public static string GetMenuHierarchy(ToolBase menuItem)
    {
      ToolBase tempMenuItem = menuItem;
      string returnString = menuItem.SharedProps.Caption;
      while (tempMenuItem != null && tempMenuItem.Owner != null)
      {
        tempMenuItem = tempMenuItem.Owner as ToolBase;
        if (tempMenuItem != null)
        {
          returnString = string.Format("{0}->{1}", tempMenuItem.CaptionResolved, returnString);
        }
      }
      return returnString;
    }

    /// <summary>
    /// Refreshes the given menu item.
    /// </summary>
    /// <param name="view_">The view tied to this menu item.</param>
    public virtual void Refresh(IViewControl view_)
    {
    }

    /// <summary>
    /// Gets the action linked to this menu item.
    /// </summary>
    public ContextActionBase Action
    {
      get { return _action; }
    }

    /// <summary>
    /// http://news.infragistics.com/forums/p/11286/43440.aspx
    /// </summary>
    /// <param name="cloneNewInstance"></param>
    /// <returns></returns>
    protected override ToolBase Clone(bool cloneNewInstance)
    {
      ActionContextMenuItem newTool = new ActionContextMenuItem(this.SharedProps.Caption, _image, _view, _action);
      newTool.InitializeFrom(this, cloneNewInstance);
      return newTool;
    }
  }
}