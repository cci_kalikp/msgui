using System;
using System.Windows.Input;
using MorganStanley.MSDesktop.Rambo.Desktop.Utils;
using Infragistics.Win.UltraWinToolbars;
using MorganStanley.MSDesktop.Rambo.MVCFramework;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils.WinGridMenu
{
  public abstract class ContextActionBase : ICommand
  {
    #region Fields

    protected readonly ILogger _log;
    protected IViewControl _view;
    protected ToolBase _command;

    #endregion

    #region Constructors

    protected ContextActionBase()
    {
      _log = new Logger(GetType());
    }

    #endregion

    #region Instance Properties

    protected ILogger Log
    {
      get { return _log; }
    }

    #endregion

    #region Instance Methods

    protected virtual void LogBeforeExecution(string command_, object state_)
    {
      if (_view != null)
      {
        Log.Info("Execute",
                 string.Format("Executing command: '{0}' on view: {1} with state {2}", command_, _view.GetType(), state_));
      }
      else
      {
        Log.Info("Execute",
                 string.Format("Executing command: '{0}' with state {1}", command_, state_));

      }
    }

    protected virtual void LogAfterExecution(string command_, object state_)
    {
      if (_view != null)
      {
        Log.Info("Execute",
                 string.Format("Completed command: '{0}' on view: {1} with state {2}", command_, _view.GetType(), state_));
      }
      else
      {
        Log.Info("Execute",
                 string.Format("Completed command: '{0}' with state {1}", command_, state_));
      }
    }

    protected abstract void ExecuteStrategy(object state_);

    #endregion

    #region IContextAction Members

    public virtual void Init(IViewControl view_, ToolBase command_)
    {
      _view = view_;
      _command = command_;
    }

    public abstract MenuItemDisplayMode GetMenuItemDisplayMode();

    public void Execute(object state_)
    {
      string commandHierarchy = null;
      if (_command != null)
      {
        commandHierarchy = ActionContextMenuItem.GetMenuHierarchy(_command);
      }
      LogBeforeExecution(commandHierarchy, state_);
      ExecuteStrategy(state_);
      LogAfterExecution(commandHierarchy, state_);
    }

    #endregion

    #region ICommand members

    public event EventHandler CanExecuteChanged;
    
    public bool CanExecute(object parameter)
    {     
      return GetMenuItemDisplayMode() == MenuItemDisplayMode.Enabled;
    }

    #endregion
  }
}