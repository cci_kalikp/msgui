using System.Xml.Serialization;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  /// <summary>
  /// For storage of the state of search parameters on the Find Text form
  /// </summary>
  [XmlRoot("FindText")]
  internal class FindTextParameters
  {
    public const string ALL_COLUMNS = "All Columns";

    public enum MatchType
    {
      AnyPartOfField,
      WholeField,
      StartOfField,
      EndOfField
    }

    [XmlElement("CaseSensitive", typeof(bool))]
    public bool CaseSensitive = false;

    [XmlElement("ReverseSearch", typeof(bool))]
    public bool ReverseSearch = false;

    [XmlElement("SearchCollapsed", typeof(bool))]
    public bool SearchCollapsed = false;

    [XmlElement("SearchColumn", typeof(string))]
    public string SearchColumn = ALL_COLUMNS;

    [XmlElement("Match", typeof(MatchType))]
    public MatchType Match = MatchType.AnyPartOfField;

    [XmlElement("SearchText", typeof(string))]
    public string SearchText = string.Empty;

    public override bool Equals(object obj)
    {
      if (obj == null)
        return false;

      if (GetType() != obj.GetType())
        return false;

      FindTextParameters that = (FindTextParameters)obj;

      return SearchText == that.SearchText &&
             CaseSensitive == that.CaseSensitive &&
             ReverseSearch == that.ReverseSearch &&
             SearchCollapsed == that.SearchCollapsed &&
             SearchColumn == that.SearchColumn &&
             Match == that.Match;
    }

    public override int GetHashCode()
    {
      return
        SearchText.GetHashCode() ^ CaseSensitive.GetHashCode() ^ ReverseSearch.GetHashCode() ^
        SearchCollapsed.GetHashCode() ^ SearchColumn.GetHashCode() ^ Match.GetHashCode();
    }
  }
}