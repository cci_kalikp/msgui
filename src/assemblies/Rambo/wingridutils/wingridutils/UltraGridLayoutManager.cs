//---------------------------------------------------------------------------
//
// Copyright (c) 2004 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
//---------------------------------------------------------------------------

using System;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Collections;
using System.Collections.Specialized;
using Infragistics.Win.UltraWinGrid;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
	/// <summary>
	/// The GridLayout manager bridges the gap between UltraGrid-specific classes and the
	/// generic GridLayout and ColumnLayout object types that represent layout in a standard
	/// XML format.
	/// <remarks>This class loads an UltraGrid and passes the appropiate parameters into
	/// the standard Layout structures. It can also return the underlying XML representation
	/// that these classes comprise.</remarks>
	/// </summary>
	internal class UltraGridLayoutManager
	{
		#region Member Variables

		protected GridLayout m_infraGridSerialiser;
		protected UltraGrid m_infraGrid;
		private XmlElement m_xmlFormat;
		private StringCollection m_omitKeys;

		#endregion

		#region Constructors
		
		/// <summary>
		/// Initialises an instance of a UltradGridLayoutManager.
		/// </summary>
		/// <param name="grid_">An instance of an UltraGrid.</param>
		/// <param name="applicationName_">The current application name.</param>
		/// <param name="gridName_">The name of the current grid.</param>
		/// <param name="layoutName_">The name of the layout.</param>
		/// <param name="omitKeys_">A collection of columns keys to omit from the grid layout.</param>
		public UltraGridLayoutManager(UltraGrid grid_, string applicationName_, string gridName_, string layoutName_, StringCollection omitKeys_)
		{
			m_infraGrid = grid_;
			m_infraGridSerialiser = new GridLayout();
			m_infraGridSerialiser.GridName = gridName_;
			m_infraGridSerialiser.Application = applicationName_;
			m_infraGridSerialiser.LayoutName = layoutName_;
      m_infraGridSerialiser.BandId = 0;
			m_omitKeys = omitKeys_;
			LoadSerialiser();
		}

    /// <summary>
    /// Initialises an instance of a UltradGridLayoutManager.
    /// </summary>
    /// <param name="grid_">An instance of an UltraGrid.</param>
    /// <param name="applicationName_">The current application name.</param>
    /// <param name="gridName_">The name of the current grid.</param>
    /// <param name="layoutName_">The name of the layout.</param>
    /// <param name="omitKeys_">A collection of columns keys to omit from the grid layout.</param>
    /// <param name="band_">The grid band the layout applies to</param>
    public UltraGridLayoutManager(UltraGrid grid_, string applicationName_, string gridName_, string layoutName_, StringCollection omitKeys_, int band_)
    {
      m_infraGrid = grid_;
      m_infraGridSerialiser = new GridLayout();
      m_infraGridSerialiser.GridName = gridName_;
      m_infraGridSerialiser.Application = applicationName_;
      m_infraGridSerialiser.LayoutName = layoutName_;
      m_infraGridSerialiser.BandId = band_;
      m_omitKeys = omitKeys_;
      LoadSerialiser();
    }
		#endregion

		#region Private Methods

		/// <summary>
		/// Loads column layout objects into the master grid layout object.
		/// </summary>
		private void LoadSerialiser()
		{
			
			try
			{
				//ColumnLayout[] colLayout = new ColumnLayout[m_infraGrid.DisplayLayout.Bands[0].Columns.Count];
				ArrayList colLayout = new ArrayList(m_infraGrid.DisplayLayout.Bands[m_infraGridSerialiser.BandId].Columns.Count);
				for(int i = 0; i < m_infraGrid.DisplayLayout.Bands[m_infraGridSerialiser.BandId].Columns.Count; ++i)
				{				
					UltraGridColumnLayoutManager colLayoutManager = new UltraGridColumnLayoutManager(m_infraGrid.DisplayLayout.Bands[m_infraGridSerialiser.BandId].Columns[i]);
					if(m_omitKeys == null || !m_omitKeys.Contains(colLayoutManager.ColumnSerialiser.Key))
					{
						colLayout.Add(colLayoutManager.ColumnSerialiser);
					}				
					
				}				
				colLayout.TrimToSize();
				ColumnLayout[] colLayoutArray = new ColumnLayout[colLayout.Count];
				colLayout.CopyTo(colLayoutArray);															   

        RowFontStyleEnum en ;
        try
        {
          en = RowFontStyle.GetEnumFromGrid( m_infraGrid ) ;
        }
        catch ( Exception )
        {
          en = RowFontStyleEnum.Medium ;
        }
        
        m_infraGridSerialiser.Font = en ;
        

				m_infraGridSerialiser.Columns = new ColumnLayoutCollection(colLayoutArray);
				m_infraGridSerialiser.LockedColumns = GetLockedColumns();
				m_xmlFormat = UltraGridLayoutFactory.GetXml(m_infraGridSerialiser);				
			}
			catch(Exception ex)
			{
				throw new Exception("Error parsing grid layout.", ex);
			}
		}

		private int GetLockedColumns()
		{
			int cnt = 0;

			for(int i = 0; i < m_infraGrid.DisplayLayout.Bands[m_infraGridSerialiser.BandId].Columns.Count; ++i)
			{
				UltraGridColumn col = m_infraGrid.DisplayLayout.Bands[m_infraGridSerialiser.BandId].Columns[i];
				
				if(col.Header.Fixed)
				{
					++cnt;
				}				
			}
			return cnt;
		}

		#endregion

		#region Public Methods

		/// <summary>
		/// Returns the XML representation of a Grid Format.
		/// </summary>
		/// <returns>XML grid format string.</returns>
		public XmlElement GetXml()
		{
			return m_xmlFormat;
		}

		/// <summary>
		/// Gets the current GridLayout
		/// </summary>
		public GridLayout GridLayout
		{
			get { return m_infraGridSerialiser; }
		}

		#endregion
	}
}
