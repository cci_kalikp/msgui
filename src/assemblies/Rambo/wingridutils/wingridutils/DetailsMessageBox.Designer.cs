﻿namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  partial class DetailsMessageBox
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this._messageLabel = new System.Windows.Forms.Label();
      this._OKButton = new System.Windows.Forms.Button();
      this._detailsTextBox = new Infragistics.Win.FormattedLinkLabel.UltraFormattedTextEditor();
      this.SuspendLayout();
      // 
      // _messageLabel
      // 
      this._messageLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this._messageLabel.AutoSize = true;
      this._messageLabel.Location = new System.Drawing.Point(12, 18);
      this._messageLabel.Name = "_messageLabel";
      this._messageLabel.Size = new System.Drawing.Size(55, 13);
      this._messageLabel.TabIndex = 0;
      this._messageLabel.Text = "Messsage";
      // 
      // _OKButton
      // 
      this._OKButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
      this._OKButton.Location = new System.Drawing.Point(178, 135);
      this._OKButton.Name = "_OKButton";
      this._OKButton.Size = new System.Drawing.Size(75, 23);
      this._OKButton.TabIndex = 2;
      this._OKButton.Text = "OK";
      this._OKButton.UseVisualStyleBackColor = true;
      this._OKButton.Click += new System.EventHandler(this._OKButton_Click);
      // 
      // _detailsText
      // 
      this._detailsTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this._detailsTextBox.Location = new System.Drawing.Point(12, 43);
      this._detailsTextBox.Name = "_detailsText";
      this._detailsTextBox.ReadOnly = true;
      this._detailsTextBox.Size = new System.Drawing.Size(406, 86);
      this._detailsTextBox.TabIndex = 3;
      this._detailsTextBox.Value = "Details<a>test</a>";
      // 
      // DetailsMessageBox
      // 
      this.AcceptButton = this._OKButton;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(430, 170);
      this.Controls.Add(this._detailsTextBox);
      this.Controls.Add(this._OKButton);
      this.Controls.Add(this._messageLabel);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
      this.Name = "DetailsMessageBox";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Caption";
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Label _messageLabel;
    private System.Windows.Forms.Button _OKButton;
    private Infragistics.Win.FormattedLinkLabel.UltraFormattedTextEditor _detailsTextBox;
  }
}