using System;
using System.Windows.Forms;
using MorganStanley.MSDesktop.Rambo.WinGridUtils.Resource;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils.GridFilter
{
  public partial class ActionGridFormatItemControl : UserControl
  {
    

    public ActionGridFormatItemControl()
    {
      InitializeComponent();
      _gridFormatItemChooser.AddToDisabled(_btnAction);
      _btnAction.Focus(); 
    }
    /// <summary>
    /// Set the FilterManager for GridFormatItemExplorer
    /// </summary>
    public IGridFormatItemResourceManager FilterManager
    {
      set { _gridFormatItemChooser.FilterManager = value; }
    }

    public string ApplicationName
    {
      get { return _gridFormatItemChooser.ApplicationName; }
      set {_gridFormatItemChooser.ApplicationName = value; }
    }

    public string GridName
    {
      get { return _gridFormatItemChooser.GridName; }
      set { _gridFormatItemChooser.GridName = value; }
    }

    DialogType _dialogType;

    public DialogType DialogType
    {
      set
      {
        _dialogType = value;
        switch (_dialogType)
        {
          case DialogType.SAVE:
              _btnAction.Text = "Save";
            break;
          case DialogType.OPEN:
            _btnAction.Text = "Open";
          break;
        }
      }
      get { return _dialogType; }
    }

    public IGridFormatItem SelectedItem
    {
      get { return _gridFormatItemChooser.SelectedItem; }
      set
      {
        if (value != null)
        {
          _gridFormatItemChooser.SelectedItem = value;
          _gridFormatItemChooser.ItemToSave = value;
        }
      }
    }
  
    private void _btnAction_Click(object sender, EventArgs e)
    {
      LaunchAction();
    }


    private void _btnCancel_Click(object sender, EventArgs e)
    {
      ParentForm.DialogResult = DialogResult.Cancel;
      ParentForm.Hide();
    }

    private void _gridFormatItemChooser_OperationCompleted(object sender, OperationCompletedEventArgs e)
    {
      if ( e.ResultStatus == ItemChooserOperationResultStatus.SUCCESS)
      {
        ParentForm.DialogResult = DialogResult.OK;
        ParentForm.Hide();
      }
    }

    private void _gridFormatItemChooser_DoubleClickedOnItem(object sender, EventArgs e)
    { 
      LaunchAction();
    }

    private void LaunchAction()
    {
      if(DialogType == DialogType.SAVE)
      {
        _gridFormatItemChooser.SaveGridFormatItem();  
      }
      else if(DialogType== DialogType.OPEN)
      {
        _gridFormatItemChooser.OpenGridFormatItem();
      }
    }
  }

  public enum DialogType
  {
    OPEN,SAVE
  }
}