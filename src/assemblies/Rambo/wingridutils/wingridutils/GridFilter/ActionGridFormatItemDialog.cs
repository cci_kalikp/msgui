using System.Windows.Forms;
using MorganStanley.MSDesktop.Rambo.WinGridUtils.Resource;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils.GridFilter
{
  public partial class ActionGridFormatItemDialog : Form
  {
    public ActionGridFormatItemDialog(IGridFormatItem gridFormatItem_,DialogType _dialogType)
    {
      InitializeComponent();
      _actionControl.DialogType = _dialogType;
      _actionControl.SelectedItem = gridFormatItem_;
    }

    public string ApplicationName
    {
      get { return _actionControl.ApplicationName; }
      set { _actionControl.ApplicationName = value; }
    }

    public string GridName
    {
      get { return _actionControl.GridName; }
      set { _actionControl.GridName = value; }
    }


    public IGridFormatItem SelectedItem
    {
      get
      {
        return _actionControl.SelectedItem;
      }
    }

    public IGridFormatItemResourceManager FilterManager
    {
      set { _actionControl.FilterManager = value; }
    }
  }
}