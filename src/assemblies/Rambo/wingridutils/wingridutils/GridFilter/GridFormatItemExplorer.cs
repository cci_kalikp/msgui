using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using Infragistics.Win.UltraWinGrid;
using MorganStanley.MSDesktop.Rambo.AsyncUtils;
using MorganStanley.MSDesktop.Rambo.Desktop.Utils;
using MorganStanley.MSDesktop.Rambo.WinGridUtils.Resource;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils.GridFilter
{
  public partial class GridFormatItemExplorer : UserControl
  {
    private string _applicationName;
    private List<Control> _controlsToDisableWhileBusy;
    private string _gridName;
    private IGridFormatItem _itemToSave;
    private ILogger _log;
    private IGridFormatItemResourceManager _resourseManager;

    private IGridFormatItem _selectedItem;

    public GridFormatItemExplorer()
    {
      _log = LoggerManager.GetLogger(typeof(GridFormatItemExplorer));
      _resourseManager = GridFormatItemResourceManager.Instance;
      InitializeComponent();
      _controlsToDisableWhileBusy = new List<Control>();
      _controlsToDisableWhileBusy.Add(_chooserGrid);
      _controlsToDisableWhileBusy.Add(_nameComboBox);
    }

    public IGridFormatItem SelectedItem
    {
      get { return _selectedItem; }
      set
      {
        if (value != null)
        {
          _selectedItem = value;
          _applicationName = value.Application;
          _gridName = value.GridName;
        }
      }
    }

    public string ApplicationName
    {
      get { return _applicationName; }
      set { _applicationName = value; }
    }

    public string GridName
    {
      get { return _gridName; }
      set { _gridName = value; }
    }

    public IGridFormatItem ItemToSave
    {
      get { return _itemToSave; }
      set { _itemToSave = value; }
    }

    public IGridFormatItemResourceManager FilterManager
    {
      set { _resourseManager = value; }
    }

    public event EventHandler StartedLoadingFormatItemList;
    public event EventHandler FinishedLoadingFormatItemList;
    public event EventHandler<OperationCompletedEventArgs> OperationCompleted;
    public event EventHandler SelectionComitted;

    public event EventHandler<GridFormatSelectionChangedEventArgs> SelectedItemChanged;

    private void LoadListFromService()
    {
      if (_resourseManager != null)
      {
        OnStartedLoadingFormatItemList();
        StartProgressBar();
        Func<string, string, string,GridFilter[]> loadFilterListDel = _resourseManager.LoadFiltersList;
        loadFilterListDel.BeginInvoke(_applicationName, _gridName, "all", EndLoadListFromService, loadFilterListDel);
      }
    }

    protected void OnStartedLoadingFormatItemList()
    {
      if (StartedLoadingFormatItemList != null)
      {
        StartedLoadingFormatItemList(this, EventArgs.Empty);
      }
    }

    public void StartProgressBar()
    {
      //_progressBarWrapper.Start();
      foreach (Control c in _controlsToDisableWhileBusy)
      {
        c.Enabled = false;
      }
    }

    public void StopProgressBar()
    {
      //_progressBarWrapper.Stop();
      foreach (Control c in _controlsToDisableWhileBusy)
      {
        c.Enabled = true;
      }
    }

    private void EndLoadListFromService(IAsyncResult result_)
    {
      if (InvokeRequired)
      {
        BeginInvoke(new AsyncCallback(EndLoadListFromService), result_);
        return;
      }

      try
      {
        Func<string, string, string, GridFilter[]> del = (Func<string, string, string, GridFilter[]>) result_.AsyncState;
        IGridFormatItem[] gridFormatList = del.EndInvoke(result_);
        Array.Sort(gridFormatList, new GridFilterComparer());
        if (gridFormatList != null)
        {
          iGridFormatItemBindingSource.DataSource = gridFormatList;
        }
      }
      catch (Exception e)
      {
        _log.Error("EndLoadListFromFilter", "Error loading list of filters from " + _resourseManager.GetType(), e );
        MessageBox.Show("Error loading list of saved grid filters", "Error loading list of saved grid filters",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
      }

      int selectedRowIndex = 0;
      if (SelectedItem != null)
      {
        _chooserGrid.Selected.Rows.Clear();
        selectedRowIndex = iGridFormatItemBindingSource.List.IndexOf(SelectedItem);

        if (selectedRowIndex < 0)
        {
          selectedRowIndex = 0;
        }

      }

      iGridFormatItemBindingSource.CurrencyManager.Position = selectedRowIndex;
      StopProgressBar();
    }


    protected void OnFinishedLoadingFormatItemList()
    {
      if (FinishedLoadingFormatItemList != null)
      {
        FinishedLoadingFormatItemList(this, EventArgs.Empty);
      }
    }

    private void GridFormatItemChooser_Load(object sender, EventArgs e)
    {
      LoadListFromService();
    }

    private void _chooserGrid_AfterRowActivate(object sender, EventArgs e)
    {
      if (SelectedItemChanged != null)
      {
        SelectedItem = iGridFormatItemBindingSource.Current as IGridFormatItem;
        OnItemSelectionChanged();
      }
    }

    protected void OnItemSelectionChanged()
    {
      if (SelectedItemChanged != null && SelectedItem != null)
      {
        SelectedItemChanged(this, new GridFormatSelectionChangedEventArgs(SelectedItem));
      }
    }


    private void RefreshItemList()
    {
      _resourseManager.ClearCache();
      LoadListFromService();
    }

    /// <summary>
    /// control will be disabled then StartProgressBar is executing
    /// StopProgress bar will reenable control
    /// </summary>
    /// <param name="controlToDisable"></param>
    public void AddToDisabled(Control controlToDisable)
    {
      _controlsToDisableWhileBusy.Add(controlToDisable);
    }

    public void SaveGridFormatItem()
    {
      StartProgressBar();
      ItemToSave.Name = _nameComboBox.Text;
      if (string.IsNullOrWhiteSpace(ItemToSave.Name) || ItemToSave.Name.ToLower() == "all")
      {
        MessageBox.Show(
          string.Format("The name '{0}' cannot be used to save filters in RV.\nPlease try a different name.", ItemToSave.Name),
          "Unable to save filter", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        _log.Warning("SaveGridFormatItem", string.Format("The name '{0}' cannot be used to save filters in RV.", ItemToSave.Name));
        StopProgressBar();
        return;
      }

      AsynchronizerBackgroundWorker VerifyOwnershipBackgroundWorker = new AsynchronizerBackgroundWorker();
      VerifyOwnershipBackgroundWorker.DoWork += VerifyOwnershipBackgroundWorker_DoWork;
      VerifyOwnershipBackgroundWorker.RunWorkerCompleted += VerifyOwnershipBackgroundWorker_RunWorkerCompleted;
      VerifyOwnershipBackgroundWorker.RunWorkerAsync(_nameComboBox.Text);
    }

    private void VerifyOwnershipBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
    {
      if ((bool)e.Result)
      {
        Action<GridFilter, string> saveFilterDel = _resourseManager.SaveFilter;
        saveFilterDel.BeginInvoke((GridFilter) ItemToSave, Environment.UserName, EndSaveGridFormatItem, saveFilterDel);
      }
      else
      {
        MessageBox.Show(
          "You can only save over filters that belong/shared to you.\n Please try saving the filter under a different name",
          "Permission error", MessageBoxButtons.OK, MessageBoxIcon.Warning);

        StopProgressBar();
      }
    }

    private void VerifyOwnershipBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
    {
      GridFilter filter = _resourseManager.LoadFilter(_applicationName, _gridName, e.Argument as string);
      e.Result =  filter == null ? true : _resourseManager.VerifyWritePermission(filter);
    }

    private void EndSaveGridFormatItem(IAsyncResult result_)
    {
      if (InvokeRequired)
      {
        BeginInvoke(new AsyncCallback(EndSaveGridFormatItem), result_);
        return;
      }

      ItemChooserOperationResultStatus status = ItemChooserOperationResultStatus.SUCCESS;
      try
      {
        Action<GridFilter, string> saveFilterDel = (Action<GridFilter, string>) result_.AsyncState;
        saveFilterDel.EndInvoke(result_);
        SelectedItem = ItemToSave;
      }
      catch (Exception e)
      {
        _log.Error("EndSaveGridFormatItem", "Error saving GridFilter", e);
        MessageBox.Show("Error savig grid filter", "Error saving grid filter", MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
        status = ItemChooserOperationResultStatus.ERROR;
      }
      StopProgressBar();
      OnOperationCompleted(new OperationCompletedEventArgs(ItemChooserOperations.SAVE_ITEM, status));
    }

    private void OnOperationCompleted(OperationCompletedEventArgs operationCompletedEventArgs_)
    {
      if (OperationCompleted != null)
      {
        OperationCompleted(this, operationCompletedEventArgs_);
      }
    }


    public void OpenGridFormatItem()
    {
      StartProgressBar();
      Func<string, string, string, GridFilter> loadFilterDel = _resourseManager.LoadFilter;
      loadFilterDel.BeginInvoke(_applicationName, _gridName, _nameComboBox.Text, EndOpenGridFormatItem, loadFilterDel);
    }

    private void EndOpenGridFormatItem(IAsyncResult result_)
    {
      if (InvokeRequired)
      {
        BeginInvoke(new AsyncCallback(EndOpenGridFormatItem), result_);
        return;
      }

      ItemChooserOperationResultStatus status = ItemChooserOperationResultStatus.SUCCESS;
      try
      {
        Func<string, string, string, GridFilter> del = (Func<string, string, string, GridFilter>) result_.AsyncState;
        _selectedItem = del.EndInvoke(result_);
      }
      catch (Exception e)
      {
        _log.Error("EndOpenGridFormatItem", "Error load grid filter", e);
        MessageBox.Show("Grid filter does not exist", "Grid filter does not exist", MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
        status = ItemChooserOperationResultStatus.ERROR;
      }

      OnOperationCompleted(new OperationCompletedEventArgs(ItemChooserOperations.LOAD_ITEM, status));
      StopProgressBar();
    }

    private void DeleteItem()
    {
      if (_chooserGrid.Selected.Rows.Count > 0)
      {
        IGridFormatItem itemToDelete = (IGridFormatItem) iGridFormatItemBindingSource.Current;
        if (itemToDelete != null && (itemToDelete.Owner.Equals(Environment.UserName) || UserVerification.IsThisUserInGroup("tradeplant-ww-risk")))
        {
          if (MessageBox.Show(String.Format("Are you sure that you want to delete \"{0}\" filter?", itemToDelete.Name), "Delete?", MessageBoxButtons.YesNo) == DialogResult.Yes)
          {
            StartProgressBar();
            Action<IGridFormatItem> deleteFilterDel = _resourseManager.DeleteGridFilter;
            deleteFilterDel.BeginInvoke(itemToDelete, EndDeleteGridFormatItem, null);
          }
        }
        else
        {
          MessageBox.Show
            ("You can not remove filters that do not belong to you", "Permission error",
             MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
      }
    }

    private void EndDeleteGridFormatItem(IAsyncResult result_)
    {
      IGridFormatItem itemToDelete =
        iGridFormatItemBindingSource.Current as IGridFormatItem;
      if (itemToDelete != null)
        _resourseManager.RemoveItemFromCache(itemToDelete);
      LoadListFromService();
    }

    private void ShareItem()
    {
      if (_chooserGrid.Selected.Rows.Count > 0)
      {
        IGridFormatItem itemToShare= (IGridFormatItem)iGridFormatItemBindingSource.Current;
        if (itemToShare != null && (itemToShare.Owner.Equals(Environment.UserName) || UserVerification.IsThisUserInGroup("tradeplant-ww-risk")))
        {
          Func<IGridFormatItem, IDictionary<string, string>> getEntitlementDel = _resourseManager.GetEntitlement;
          getEntitlementDel.BeginInvoke(itemToShare, DoneGetEntitlement, getEntitlementDel);
        }
        else
        {
          MessageBox.Show
            ("You can not share filters that do not belong to you", "Permission error",
             MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
      }
    }

    private void DoneGetEntitlement(IAsyncResult ar_)
    {
      if(InvokeRequired)
      {
        BeginInvoke(new AsyncCallback(DoneGetEntitlement), ar_);
        return;
      }

      try
      {
        Func<IGridFormatItem, IDictionary<string, string>> del = (Func<IGridFormatItem, IDictionary<string, string>>) ar_.AsyncState;
        IGridFormatItem item = (IGridFormatItem) iGridFormatItemBindingSource.Current;
        if (item == null) return;
        IDictionary<string, string> currentWriteGroups = del.EndInvoke(ar_);
        using (DlgRamboShare shareDialog = new DlgRamboShare(currentWriteGroups))
        {
          if (shareDialog.ShowDialog() == DialogResult.OK)
          {
            Action<IGridFormatItem, IDictionary<string, string>> shareDel = _resourseManager.Share;
            shareDel.BeginInvoke(item, shareDialog.ShareGroups, DoneGrantPermission, shareDel);
          }
        }
      }
      catch (Exception e_)
      {
        _log.Error("DoneGetEntitlement", "Error get entitlement", e_);
        MessageBox.Show("Error get entitlement", "Error get filter entitlement", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }

    private void DoneGrantPermission(IAsyncResult ar_)
    {
      try
      {
        IGridFormatItem gridFilter = (IGridFormatItem)iGridFormatItemBindingSource.Current;
        Action<IGridFormatItem, IDictionary<string, string>> del = (Action<IGridFormatItem, IDictionary<string, string>>) ar_.AsyncState;
        del.EndInvoke(ar_);
        MessageBox.Show
          (string.Format("Filter '{0}' is shared successfully", gridFilter == null? null :gridFilter.Name), "Share Grid Filter Succeed",
           MessageBoxButtons.OK, MessageBoxIcon.Information);
      }
      catch (Exception e_)
      {
        _log.Error("DoneGrantPermission", "Error share resource", e_);
        MessageBox.Show("Error share filter", "Error share filter", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }
 
    private void _chooserGrid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
    {
      OnSelectionComitted(sender);
    }

    private void OnSelectionComitted(object sender)
    {
      if (SelectionComitted != null)
      {
        SelectionComitted(sender, EventArgs.Empty);
      }
    }

    private void _chooserGrid_AfterPerformAction(object sender, AfterUltraGridPerformActionEventArgs e)
    {
      _chooserGrid.DisplayLayout.RefreshFilters();
    }

    private void _nameComboBox_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == Keys.Enter && _nameComboBox.DroppedDown == false)
      {
        OnSelectionComitted(sender);
      }
    }

    private void _chooserGrid_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == Keys.Enter && _chooserGrid.Selected.Rows.Count > 0)
      {
        OnSelectionComitted(sender);
      }
      else if (e.KeyCode == Keys.Delete && _chooserGrid.Selected.Rows.Count > 0)
      {
        DeleteItem();
      }
      else if (e.KeyCode == Keys.F5)
      {
        Refresh();
      }
    }

    private void _ultraToolbarsManager_ToolClick(object sender, Infragistics.Win.UltraWinToolbars.ToolClickEventArgs e)
    {
      switch (e.Tool.Key)
      {
        case "Refresh List":    // ButtonTool
          RefreshItemList();
          break;
        case "RemoveItem":    // ButtonTool
          DeleteItem();
          break;
        case "Share":  // ButtonTool
          ShareItem();
          break;
      }
    }
  }


  public class GridFormatSelectionChangedEventArgs : EventArgs
  {
    private IGridFormatItem selectedGridFormatItem;

    public GridFormatSelectionChangedEventArgs(IGridFormatItem gridFormatItem)
    {
      selectedGridFormatItem = gridFormatItem;
    }

    public IGridFormatItem SelectedGridFormatItem
    {
      get { return selectedGridFormatItem; }
    }
  }

  public class OperationCompletedEventArgs : EventArgs
  {
    private ItemChooserOperations _operation;
    private ItemChooserOperationResultStatus _resultStatus;

    public OperationCompletedEventArgs(ItemChooserOperations operation_, ItemChooserOperationResultStatus resultStatus_)
    {
      _operation = operation_;
      _resultStatus = resultStatus_;
    }

    public ItemChooserOperations Operation
    {
      get { return _operation; }
    }

    public ItemChooserOperationResultStatus ResultStatus
    {
      get { return _resultStatus; }
    }
  }


  public enum ItemChooserOperations
  {
    SAVE_ITEM,
    LOAD_ITEM,
    LOAD_LIST
  }

  public enum ItemChooserOperationResultStatus
  {
    SUCCESS,
    CANCELED,
    ERROR
  }
}