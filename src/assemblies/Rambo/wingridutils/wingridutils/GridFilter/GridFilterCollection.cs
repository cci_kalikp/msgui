using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using Infragistics.Win.UltraWinGrid;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils.GridFilter
{
  public class GridFilterCollection : IDisposable
  {
    private bool _deserializationJustCompleted;
    private FilterLogicalOperator _filterLogicalOperator;
    private UltraGrid _grid;
    private IDictionary<string, GridColumnFilter> _gridColumnFilters;
    private bool _isActivated;


    private List<GridColumnFilter> serializtionColumnFilters;


    public GridFilterCollection()
    {
      _deserializationJustCompleted = true;
      _gridColumnFilters = new Dictionary<string, GridColumnFilter>();
    }

    [XmlIgnore]
    public IDictionary<string,GridColumnFilter> ColumnFilters
    {
      get
      {
        if (_deserializationJustCompleted && serializtionColumnFilters != null && serializtionColumnFilters.Count > 0)
        {
          _gridColumnFilters = new Dictionary<string,GridColumnFilter>();
          foreach (GridColumnFilter filter in serializtionColumnFilters)
          {
            _gridColumnFilters.Add(filter.ColumnKey,filter);
          }
          _deserializationJustCompleted = false;
        }
        return _gridColumnFilters;
      }
    }

    public List<GridColumnFilter> ColumnFiltersList
    {
      get { return serializtionColumnFilters; }
      set { serializtionColumnFilters = value; }
    }

    public FilterLogicalOperator LogicalOperator
    {
      get { return _filterLogicalOperator; }
      set { _filterLogicalOperator = value; }
    }

    [XmlIgnore]
    public bool IsActivated
    {
      get
      {
        return _isActivated;
      }
    }
    
    #region IDisposable Members

    public void Dispose()
    {
      DeactivateInvisibleFilter();
    }

    #endregion

    public static GridFilterCollection Create(ColumnFiltersCollection filterCollection_)
    {
      GridFilterCollection outFilterCollection = new GridFilterCollection();
      outFilterCollection.LogicalOperator = filterCollection_.LogicalOperator;
      outFilterCollection._gridColumnFilters = new Dictionary<string,GridColumnFilter>();

      foreach (ColumnFilter columnFilter in filterCollection_.All)
      {
        if (columnFilter.FilterConditions.Count > 0)
        {
          outFilterCollection.ColumnFilters.Add(columnFilter.Key,GridColumnFilter.Create(columnFilter));
        }
      }
      outFilterCollection._deserializationJustCompleted = true;
      return outFilterCollection;
    }

    public void PrepareForSaving()
    {
      serializtionColumnFilters = new List<GridColumnFilter>();
      if (ColumnFilters != null)
      {
        foreach (GridColumnFilter filter in ColumnFilters.Values)
        {
          serializtionColumnFilters.Add(filter);
        }
      }
    }

    public void Add(GridColumnFilter columnFilter_)
    {
      if (!_gridColumnFilters.ContainsKey(columnFilter_.ColumnKey))
      {
        _gridColumnFilters.Add(columnFilter_.ColumnKey,columnFilter_);
      }
    }


    public bool MeetsCriteria(UltraGridRow row_)
    {
      if (_gridColumnFilters.Count == 0)
      {
        return true;
      }

      foreach (GridColumnFilter filterColumn in _gridColumnFilters.Values)
      {
        bool conditionMet = filterColumn.MeetsCriteria(row_);
        if (conditionMet && LogicalOperator == FilterLogicalOperator.Or)
        {
          return true;
        }
        else if (!conditionMet && LogicalOperator == FilterLogicalOperator.And)
        {
          return false;
        }
      }
      return LogicalOperator == FilterLogicalOperator.Or ? false : true;
    }


    public void ApplyInvisibleFilter(UltraGrid grid_)
    {
      if (!IsActivated && grid_ != null)
      {
        _grid = grid_;
        foreach (GridColumnFilter columnFilter in _gridColumnFilters.Values)
        {
          if (_grid.Rows.Band.Columns.Exists(columnFilter.ColumnKey)&&_grid.Rows.Band.Columns[columnFilter.ColumnKey] != null)
          {
            columnFilter.Column = _grid.Rows.Band.Columns[columnFilter.ColumnKey];
          }
        }
        _isActivated = true;
        _grid.FilterRow += new FilterRowEventHandler(_grid_FilterRow);
      }
    }

    public void DeactivateInvisibleFilter()
    {
      if (IsActivated && _grid != null)
      {
        _isActivated = false;
        _grid.FilterRow -= _grid_FilterRow;
        _grid = null;
      }
    }

    private void _grid_FilterRow(object sender, FilterRowEventArgs e)
    {
      if (e.RowFilteredOut == false)
      {
        if (IsActivated)
        {
          e.RowFilteredOut = !MeetsCriteria(e.Row);
        }
      }
    }

    public void ApplyToGridVisible(ColumnFiltersCollection columnsCollection)
    {
      columnsCollection.LogicalOperator = LogicalOperator;
      foreach (GridColumnFilter columnFilter in _gridColumnFilters.Values)
      {
        if (columnsCollection.Exists(columnFilter.ColumnKey) && 
          columnsCollection[columnFilter.ColumnKey]!=null)
        {
          columnFilter.ApplyToGrid(columnsCollection[columnFilter.ColumnKey]);
        }
      }
    }

    public GridFilterCollection Clone()
    {
      GridFilterCollection newGridFilterCollection = new GridFilterCollection();

      newGridFilterCollection.LogicalOperator = LogicalOperator;
      foreach (KeyValuePair<string, GridColumnFilter> pair in ColumnFilters)
      {
         newGridFilterCollection._gridColumnFilters.Add(pair.Key,pair.Value.Clone());
      }
     
      newGridFilterCollection._deserializationJustCompleted = false;
      return newGridFilterCollection;
    }
  }
}