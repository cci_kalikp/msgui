using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using MorganStanley.MSDesktop.Rambo.DataServices;
using MorganStanley.MSDesktop.Rambo.WinGridUtils.Resource;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils.GridFilter
{
  /// <summary>
  /// Resource Manager class for Filters.
  /// </summary>
  public class GridFormatItemResourceManager : ResourceManager, IXmlListBuilder, IGridFormatItemResourceManager
  {
    private const string GRIDFILTER_DATASERVICE_NAME = "GridFilters";
    private const string GRIDFILTER_LIST_DATASERVICE_NAME = "GridFiltersList";
    private static readonly GridFormatItemResourceManager _instance = new GridFormatItemResourceManager();

    private GridFormatItemResourceManager()
    {
    }

    protected override IDataService DataService
    {
      get { return DataServiceFactoryLayer.GetDataService(GRIDFILTER_DATASERVICE_NAME); }
    }

    protected override IDataService ListDataService
    {
      get { return DataServiceFactoryLayer.GetDataService(GRIDFILTER_LIST_DATASERVICE_NAME); }
    }

    protected override XmlSerializer Serializer
    {
      get { return new XmlSerializer(typeof (GridFilter)); }
    }

    protected override IXmlListBuilder ListSerializer
    {
      get { return this; }
    }

    protected override Type ResourceType
    {
      get { return typeof (GridFilter); }
    }

    public static GridFormatItemResourceManager Instance
    {
      get { return _instance; }
    }

    #region IXmlListBuilder Members

    public object[] DeserializeList(XmlElement value_)
    {
      List<GridFilter> filters = new List<GridFilter>();
      string application = value_.SelectSingleNode("Application").InnerText;
      string gridName = value_.SelectSingleNode("GridName").InnerText;
      string filtersubnode = GetListDSNode(GRIDFILTER_DATASERVICE_NAME);
      XmlNodeList nodeList = value_.SelectNodes(string.Format("//{0}", filtersubnode));
      if (nodeList != null)
      {
        for (int i = 0; i < nodeList.Count; ++i)
        {
          GridFilter filter = new GridFilter();
          filter.Application = application;
          filter.GridName = gridName;
          filter.Name = nodeList[i].Attributes["filterName"].Value;
          XmlNode node = nodeList[i].SelectSingleNode("UpdateID");
          if (node != null)
          {
            filter.UpdateID = node.FirstChild.Value;
          }

          filters.Add(filter);
        }
      }
      return filters.ToArray();
    }

    #endregion

    public void DeleteGridFilter(IGridFormatItem gridFilter_)
    {
      throw new NotImplementedException();
    }

    public IDictionary<string, string> GetEntitlement(IGridFormatItem gridFilter_)
    {
      return new Dictionary<string, string> { { gridFilter_.Owner, "user" } };
    }

    public void Share(IGridFormatItem gridFilter_, IDictionary<string, string> groups_)
    {
      throw new NotImplementedException();
    }

    public void RemoveItemFromCache(IGridFormatItem gridFilter_)
    {
      //Removes values from caches
      foreach (string  key in  ExtractListKeys(gridFilter_))
      {
        IList cacheList = _listCache[key] as IList;
        if (cacheList != null)
        {
          cacheList.Remove(ExtractKey(gridFilter_));
          _cache.Remove(ExtractKey(gridFilter_));
        }
      }
    }

    public bool VerifyWritePermission(IGridFormatItem gridFilter_)
    {
      return (gridFilter_ == null || gridFilter_.UpdateID == Environment.UserName);
    }

    /// <summary>
    /// Persists a particular grid grouping for later retrieval.
    /// </summary>
    public IAsyncResult BeginSaveFilter(GridFilter gridFilter_, string updateID, AsyncCallback callback_)
    {
      return BeginSaveValue(gridFilter_, callback_, gridFilter_);
    }

    /// <summary>
    /// Persists a particular grid grouping for later retrieval.
    /// </summary>
    public void EndSaveFilter(IAsyncResult result_)
    {
      EndSaveValue(result_);
    }

    /// <summary>
    /// Retrieves a particular grouping that has been saved using the SaveFilter method.
    /// </summary>	
    /// <param name="applicationName_">The current application name.</param>
    /// <param name="gridName_">The name of the grid whose grouping is to be retrieved.</param>	
    /// <param name="filterName_">The current grouping name.</param>		
    public GridFilter LoadFilter(string applicationName_, string gridName_, string filterName_)
    {
      string key = applicationName_ + "/" + gridName_ + "/" + filterName_;

      return LoadValue(key) as GridFilter;
    }

    /// <summary>
    /// Retrieves all grid groupings (as a list) that match the application and grid name attributes.
    /// </summary> 
    /// <param name="applicationName_">The application name.</param>
    /// <param name="gridName_">The name of the grid whose groupings are to be retrieved.</param>	
    /// <param name="loadingType_">Permission type settings.</param>
    public GridFilter[] LoadFiltersList(string applicationName_, string gridName_,
                                        string loadingType_)
    {
      string key = applicationName_ + "/" + gridName_ + "/" + loadingType_;

      return LoadList(key) as GridFilter[];
    }

    /// <summary>
    /// Persists a particular grouping for later retrieval.
    /// </summary>		
    public void SaveFilter(GridFilter gridFilter_, string updateID)
    {
      gridFilter_.UpdateID = updateID;
      SaveValue(gridFilter_);
    }

    public void ApplyFilter(GridFilter gridFilter_, string updateID)
    {
      gridFilter_.UpdateID = updateID;
      UpdateLocalCache(gridFilter_);
    }

    /////////////////////////////Abstract Method impls///////////////////////////////////////
    protected override string ExtractKey(object value_)
    {
      GridFilter gridFilter = value_ as GridFilter;
      return gridFilter.Application + "/" + gridFilter.GridName + "/" + gridFilter.Name;
    }

    protected override string[] ExtractListKeys(object value_)
    {
      GridFilter gridFilter = value_ as GridFilter;
      string[] keys = new string[4];
      keys[0] = gridFilter.Application + "/" + gridFilter.GridName;
      keys[1] = gridFilter.Application + "/" + gridFilter.GridName + "/all";
      //Assumes saved layout was saved by current user
      keys[2] = gridFilter.Application + "/" + gridFilter.GridName + "/" + DSGridLoader.PermissionType.User;
      keys[3] = gridFilter.Application + "/" + gridFilter.GridName + "/" + DSGridLoader.PermissionType.Group;
      return keys;
    }

    protected override bool IsLoaded(object value_)
    {
      GridFilter _gridFilter = value_ as GridFilter;
      return _gridFilter.GridFilters != null;
    }
  }
}