using System;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils.GridFilter
{
  public class GridFilter : IGridFilter
  {
    private string _name;

    [Browsable(false)]
    public string Key
    {
      get { return string.Format("{0}/{1}/{2}", Application, GridName, Name); }
    }

    #region IGridFilter Members

    [Browsable(false)]
    public GridFilterCollection GridFilters { get; set; }

    [Browsable(false)]
    public string Application { get; set; }

    [Browsable(false)]
    public string GridName { get; set; }

    [XmlElement("FilterName")]
    public string Name
    {
      get { return _name; }
      set { _name = value; }
    }

    public string UpdateID { get; set; }

    [Browsable(false)]
    public bool IsDeleted { get; set; }
    [XmlIgnore]
    public string Owner { get; set; }

    public object Clone()
    {
      GridFilter newFormatItem = new GridFilter();
      newFormatItem.Application = Application;
      newFormatItem.GridName = GridName;
      newFormatItem.Name = Name;
      newFormatItem.UpdateID = UpdateID;

      if (GridFilters != null)
      {
        newFormatItem.GridFilters = GridFilters.Clone();
      }
      return newFormatItem;
    }

    #endregion

    public override bool Equals(object obj)
    {
      if (obj != null && obj != DBNull.Value)
      {
        return ((IGridFilter) obj).Key.Equals(Key);
      }
      return false;
    }

    public override int GetHashCode()
    {
      return Key.GetHashCode();
    }

    public override string ToString()
    {
      try
      {
        if (GridFilters != null)
        {
          GridFilters.PrepareForSaving();
          XmlSerializer ser = new XmlSerializer(typeof(GridFilter));
          StringBuilder strBuilder = new StringBuilder();
          TextWriter tx = new StringWriter(strBuilder);          
          ser.Serialize(tx, this);
          tx.Flush();
          return strBuilder.ToString();
        }
        return Name;
      }
      catch (Exception e)
      {
        return "Error serilizing the filter: " + e;
      }
     
    }
  }
}