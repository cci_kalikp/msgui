using Infragistics.Win.UltraWinGrid;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils.GridFilter
{
  public class GridFilterCondition
  {
    private string _columnKey;

    private FilterCondition _filterCondition;

    public object CompareValue { get; set; }

    public FilterComparisionOperator ComparisionOperator { get; set; }

    public virtual string ColumnKey
    {
      get { return _columnKey; }
      set { _columnKey = value; }
    }

    public virtual bool MeetsCriteria(UltraGridRow row_)
    {
      if (row_.Band.Columns[_columnKey] != null)
      {
        _filterCondition = new FilterCondition(row_.Band.Columns[_columnKey], ComparisionOperator, CompareValue);
        return _filterCondition.MeetsCriteria(row_);
      }
      return true;
    }

    public static GridFilterCondition Create(FilterCondition filterCondition)
    {
      GridFilterCondition newFilterCondition = new GridFilterCondition();
      newFilterCondition.CompareValue = filterCondition.CompareValue;
      newFilterCondition.ComparisionOperator = filterCondition.ComparisionOperator;
      newFilterCondition.ColumnKey = filterCondition.Column.Key;
      return newFilterCondition;
    }

    public void InitializeColumn(UltraGridColumn value)
    {
      _filterCondition = new FilterCondition(value, ComparisionOperator, CompareValue);
    }

    public GridFilterCondition Clone()
    {
      GridFilterCondition newGridFilterCondition = new GridFilterCondition();
      newGridFilterCondition.ColumnKey = ColumnKey;
      newGridFilterCondition.ComparisionOperator = ComparisionOperator;
      newGridFilterCondition.CompareValue = CompareValue;
      return newGridFilterCondition;
    }
  }
}