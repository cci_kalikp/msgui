namespace MorganStanley.MSDesktop.Rambo.WinGridUtils.GridFilter
{
  partial class ActionGridFormatItemDialog
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this._actionControl = new MorganStanley.MSDesktop.Rambo.WinGridUtils.GridFilter.ActionGridFormatItemControl();
        this.SuspendLayout();
        // 
        // _actionControl
        // 
        this._actionControl.ApplicationName = null;
        this._actionControl.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
        this._actionControl.DialogType = MorganStanley.MSDesktop.Rambo.WinGridUtils.GridFilter.DialogType.OPEN;
        this._actionControl.Dock = System.Windows.Forms.DockStyle.Fill;
        this._actionControl.GridName = null;
        this._actionControl.Location = new System.Drawing.Point(0, 0);
        this._actionControl.Name = "_actionControl";
        this._actionControl.SelectedItem = null;
        this._actionControl.Size = new System.Drawing.Size(342, 314);
        this._actionControl.TabIndex = 0;
        // 
        // ActionGridFormatItemDialog
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.ClientSize = new System.Drawing.Size(342, 314);
        this.Controls.Add(this._actionControl);
        this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
        this.MinimumSize = new System.Drawing.Size(350, 338);
        this.Name = "ActionGridFormatItemDialog";
        this.ShowIcon = false;
        this.ShowInTaskbar = false;
        this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        this.Text = "Save";
        this.ResumeLayout(false);

    }

    #endregion

    private ActionGridFormatItemControl _actionControl;
  }
}