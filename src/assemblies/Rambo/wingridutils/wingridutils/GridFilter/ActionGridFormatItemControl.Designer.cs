namespace MorganStanley.MSDesktop.Rambo.WinGridUtils.GridFilter
{
  partial class ActionGridFormatItemControl
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this._btnCancel = new System.Windows.Forms.Button();
        this._btnAction = new System.Windows.Forms.Button();
        this._gridFormatItemChooser = new MorganStanley.MSDesktop.Rambo.WinGridUtils.GridFilter.GridFormatItemExplorer();
        this.SuspendLayout();
        // 
        // _btnCancel
        // 
        this._btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
        this._btnCancel.Location = new System.Drawing.Point(410, 347);
        this._btnCancel.Name = "_btnCancel";
        this._btnCancel.Size = new System.Drawing.Size(75, 22);
        this._btnCancel.TabIndex = 0;
        this._btnCancel.Text = "Cancel";
        this._btnCancel.UseVisualStyleBackColor = true;
        this._btnCancel.Click += new System.EventHandler(this._btnCancel_Click);
        // 
        // _btnAction
        // 
        this._btnAction.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
        this._btnAction.Location = new System.Drawing.Point(328, 347);
        this._btnAction.Name = "_btnAction";
        this._btnAction.Size = new System.Drawing.Size(75, 22);
        this._btnAction.TabIndex = 2;
        this._btnAction.Text = "Action";
        this._btnAction.UseVisualStyleBackColor = true;
        this._btnAction.Click += new System.EventHandler(this._btnAction_Click);
        // 
        // _gridFormatItemChooser
        // 
        this._gridFormatItemChooser.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                    | System.Windows.Forms.AnchorStyles.Left)
                    | System.Windows.Forms.AnchorStyles.Right)));
        this._gridFormatItemChooser.ApplicationName = null;
        this._gridFormatItemChooser.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
        this._gridFormatItemChooser.GridName = null;
        this._gridFormatItemChooser.ItemToSave = null;
        this._gridFormatItemChooser.Location = new System.Drawing.Point(3, 3);
        this._gridFormatItemChooser.Name = "_gridFormatItemChooser";
        this._gridFormatItemChooser.SelectedItem = null;
        this._gridFormatItemChooser.Size = new System.Drawing.Size(482, 371);
        this._gridFormatItemChooser.TabIndex = 3;
        this._gridFormatItemChooser.OperationCompleted += new System.EventHandler<MorganStanley.MSDesktop.Rambo.WinGridUtils.GridFilter.OperationCompletedEventArgs>(this._gridFormatItemChooser_OperationCompleted);
        this._gridFormatItemChooser.SelectionComitted += new System.EventHandler(this._gridFormatItemChooser_DoubleClickedOnItem);
        // 
        // ActionGridFormatItemControl
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
        this.Controls.Add(this._btnCancel);
        this.Controls.Add(this._btnAction);
        this.Controls.Add(this._gridFormatItemChooser);
        this.Name = "ActionGridFormatItemControl";
        this.Size = new System.Drawing.Size(488, 374);
        this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Button _btnCancel;
      private System.Windows.Forms.Button _btnAction;
    private GridFormatItemExplorer _gridFormatItemChooser;
  }
}
