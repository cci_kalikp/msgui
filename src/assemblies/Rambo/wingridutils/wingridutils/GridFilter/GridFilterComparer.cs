﻿using System.Collections;
using MorganStanley.MSDesktop.Rambo.WinGridUtils.Resource;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils.GridFilter
{
  /// <summary>
  /// Class to perform comparison operations on two IGridFormatItem objects. Used to sort a collection
  /// of IGridFormatItem objects in alphabetical order.
  /// </summary>
  public class GridFilterComparer: IComparer
  {
     public int Compare(object x_, object y_)
    {
      IGridFormatItem gridFormatItem1 = (IGridFormatItem)x_;
      IGridFormatItem gridFormatItem2 = (IGridFormatItem)y_;

      return (gridFormatItem1.Name.ToUpper().CompareTo(gridFormatItem2.Name.ToUpper())) < 0 ? -1 : 1;
    }
  }
}
