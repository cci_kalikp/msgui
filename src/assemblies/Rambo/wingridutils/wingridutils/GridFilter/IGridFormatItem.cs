using System.ComponentModel;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils.Resource
{
  public interface IGridFormatItem
  {
    string Name { get; set; }
    string UpdateID { get; set; }
    string GridName { get; set; }
    string Application { get; set; }
    bool IsDeleted { get; set; }
    [Browsable(false)]
    string Owner { get; set; }
  }
}