using MorganStanley.MSDesktop.Rambo.WinGridUtils.Resource;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils.GridFilter
{
  public interface IGridFilter : IGridFormatItem
  {
    GridFilterCollection GridFilters { get; set; }
    object Clone();
    string Key { get; }
  }
}