using System.Collections.Generic;
using System.Xml.Serialization;
using Infragistics.Win.UltraWinGrid;
using MorganStanley.MSDesktop.Rambo.Desktop.Utils;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils.GridFilter
{
  public class GridColumnFilter
  {
    private string _columnKey;
    private List<GridFilterCondition> _conditions;
    private FilterLogicalOperator _logicalOperator;
    private UltraGridColumn _ultraGridColumn;
    private Logger _log = new Logger(typeof (GridColumnFilter));

    [XmlIgnore]
    public UltraGridColumn Column
    {
      get { return _ultraGridColumn; }
      set
      {
        _ultraGridColumn = value;
        foreach (GridFilterCondition condition in FilterConditions)
        {
          condition.InitializeColumn(value);
        }
      }
    }

    public FilterLogicalOperator LogicalOperator
    {
      get { return _logicalOperator; }
      set { _logicalOperator = value; }
    }

    public List<GridFilterCondition> FilterConditions
    {
      get { return _conditions; }
      set { _conditions = value; }
    }

    public string ColumnKey
    {
      get { return _columnKey; }
      set { _columnKey = value; }
    }

    public bool MeetsCriteria(UltraGridRow row_)
    {
      if (_conditions.Count == 0)
      {
        return true;
      }

      foreach (GridFilterCondition filterConfition in _conditions)
      {
        bool conditionMet = filterConfition.MeetsCriteria(row_);
        if (conditionMet && LogicalOperator == FilterLogicalOperator.Or)
        {
          return true;
        }
        else if (!conditionMet && LogicalOperator == FilterLogicalOperator.And)
        {
          return false;
        }
      }
      return LogicalOperator == FilterLogicalOperator.Or ? false : true;
    }

    public static GridColumnFilter Create(ColumnFilter filter_)
    {
      GridColumnFilter newColumnFilter = new GridColumnFilter();
      newColumnFilter.ColumnKey = filter_.Column.Key;
      newColumnFilter.LogicalOperator = filter_.LogicalOperator;
      newColumnFilter.FilterConditions = new List<GridFilterCondition>();
      foreach (FilterCondition filterCondition in filter_.FilterConditions)
      {
        newColumnFilter.FilterConditions.Add(GridFilterCondition.Create(filterCondition));
      }

      return newColumnFilter;
    }

    public void ApplyToGrid(ColumnFilter columnFilter_)
    {
      //TODO: 25th Aug 2011 - Probably remove this pointless and frankly confusing check from this code or throw an exception if the code ever does pass in a non-matching ColumnFilter 
      if (columnFilter_.Column.Key.Equals(ColumnKey))
      {
        columnFilter_.LogicalOperator = LogicalOperator;
        foreach (GridFilterCondition condition in FilterConditions)
        {
          columnFilter_.FilterConditions.Add(new FilterCondition(condition.ComparisionOperator, condition.CompareValue));
        }
      }
      else
      {
        _log.Error("ApplyToGrid", "How could this code ever get a columnFilter_ which doesn't have the same key as me? My Key = " + ColumnKey + " and the key passed in is " + columnFilter_.Key);
      }
    }

    internal GridColumnFilter Clone()
    {
      GridColumnFilter newGridColumnFilter = new GridColumnFilter();
      newGridColumnFilter.ColumnKey = ColumnKey;
      newGridColumnFilter.FilterConditions = new List<GridFilterCondition>();
      foreach (GridFilterCondition gridFilterCondition in FilterConditions)
      {
        newGridColumnFilter.FilterConditions.Add(gridFilterCondition.Clone());
      }
      return newGridColumnFilter;
    }
  }
}