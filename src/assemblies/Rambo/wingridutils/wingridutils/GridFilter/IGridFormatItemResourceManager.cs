﻿using System.Collections.Generic;
using MorganStanley.MSDesktop.Rambo.WinGridUtils.Resource;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils.GridFilter
{
  public interface IGridFormatItemResourceManager
  {
    /// <summary>
    /// Load Filter list
    /// </summary>
    /// <param name="applicationName_"></param>
    /// <param name="gridName_"></param>
    /// <param name="loadingType_"></param>
    /// <param name="?"></param>
    /// <returns></returns>
    GridFilter[] LoadFiltersList(string applicationName_, string gridName_, string loadingType_);
    /// <summary>
    /// Load Filter
    /// </summary>
    /// <param name="applicationName_"></param>
    /// <param name="gridName_"></param>
    /// <param name="filterName_"></param>
    /// <returns></returns>
    GridFilter LoadFilter(string applicationName_, string gridName_, string filterName_);
    /// <summary>
    /// Save Filter
    /// </summary>
    /// <param name="gridFilter_"></param>
    /// <param name="updateID"></param>
    /// <returns></returns>
    void SaveFilter(GridFilter gridFilter_, string updateID);
    /// <summary>
    /// Delete Filter
    /// </summary>
    /// <param name="gridFilter_"></param>
    /// <returns></returns>
    void DeleteGridFilter(IGridFormatItem gridFilter_);
    /// <summary>
    /// Get Entitlement
    /// </summary>
    IDictionary<string, string> GetEntitlement(IGridFormatItem gridFilter_);
    /// <summary>
    /// share grid filter
    /// </summary>
    void Share(IGridFormatItem gridFilter_, IDictionary<string, string> groups_);
    /// <summary>
    /// Apply Filter
    /// </summary>
    /// <param name="gridFilter_"></param>
    /// <param name="updateID"></param>
    void ApplyFilter(GridFilter gridFilter_, string updateID);
    /// <summary>
    /// Clear Cache for Filter
    /// </summary>
    void ClearCache();
    /// <summary>
    /// Remove paticular filter from cache
    /// </summary>
    /// <param name="gridFilter_"></param>
    void RemoveItemFromCache(IGridFormatItem gridFilter_);
    /// <summary>
    /// Verify if user has write permission
    /// </summary>
    /// <returns></returns>
    bool VerifyWritePermission(IGridFormatItem gridFilter_);
  }
}
