﻿using Infragistics.Win.UltraWinGrid;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils.GridFilter
{
  internal class CustomGridFilterCondition : GridFilterCondition
  {
    private readonly IGridMeetCriteria _criteria;

    public CustomGridFilterCondition(IGridMeetCriteria criteria_)
    {
      _criteria = criteria_;
    }

    public override bool MeetsCriteria(UltraGridRow row_)
    {
      return _criteria.MeetsCriteria(row_);
    }
  }
}