﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;
using Infragistics.Win.UltraWinGrid;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils.GridFilter
{
  internal interface IGridMeetCriteria
  {
    bool MeetsCriteria(UltraGridRow row);
  }

  internal interface IActiveFilter
  {
    bool ShouldReapplyFilter(string field);
  }

  public abstract class CustomGridFilter : IGridFilter, IGridMeetCriteria, IActiveFilter
  {
    private CustomGridFilterCondition _condition;
    private GridColumnFilter _gridColumnFilter;
    private GridFilter _gridFilter;
    private GridFilterCollection _gridFilterCollection;
    public abstract string ColumnKey { get; }

    #region IGridFilter Members

    public abstract object Clone();

    [Browsable(false)]
    public string Key
    {
      get { return string.Format("{0}/{1}/{2}", Application, GridName, Name); }
    }

    public GridFilterCollection GridFilters
    {
      get
      {
        if (_gridFilter == null)
        {
          _gridFilter = new GridFilter();
          _gridFilterCollection = new GridFilterCollection();
          _gridColumnFilter = new GridColumnFilter();
          _condition = new CustomGridFilterCondition(this);
          _gridColumnFilter.ColumnKey = ColumnKey;
          _gridColumnFilter.FilterConditions = new List<GridFilterCondition>();

          _gridColumnFilter.FilterConditions.Add(_condition);

          _gridFilterCollection.Add(_gridColumnFilter);
        }

        return _gridFilterCollection;
      }
      set { _gridFilterCollection = value; }
    }

    public abstract string Name { get; set; }

    public string UpdateID
    {
      get { return "Custom"; }
      set { }
    }

    public abstract string GridName { get; set; }

    public abstract string Application { get; set; }

    public bool IsDeleted
    {
      get { return false; }
      set { }
    }
    [XmlIgnore]
    public string Owner { get; set; }

    #endregion

    #region IGridMeetCriteria Members

    public abstract bool MeetsCriteria(UltraGridRow row);

    #endregion

    #region Object Members
    public override string ToString()
    {
      return Name;
    }
    #endregion

    #region IActiveFilter Members

    public virtual bool ShouldReapplyFilter(string field)
    {
      return false;
    }

    #endregion
  }
}