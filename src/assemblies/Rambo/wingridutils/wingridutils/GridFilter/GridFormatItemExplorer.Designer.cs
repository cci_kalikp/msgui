namespace MorganStanley.MSDesktop.Rambo.WinGridUtils.GridFilter
{
  partial class GridFormatItemExplorer
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
      Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand1 = new Infragistics.Win.UltraWinGrid.UltraGridBand("IGridFormatItem", -1);
      Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn11 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Name", -1, null, 0, Infragistics.Win.UltraWinGrid.SortIndicator.Ascending, false);
      Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn12 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("UpdateID");
      Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn13 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("GridName");
      Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn14 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("Application");
      Infragistics.Win.UltraWinGrid.UltraGridColumn ultraGridColumn15 = new Infragistics.Win.UltraWinGrid.UltraGridColumn("IsDeleted");
      Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
      Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
      Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
      Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
      Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
      Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
      Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
      Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
      Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
      Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
      Infragistics.Win.UltraWinToolbars.UltraToolbar ultraToolbar1 = new Infragistics.Win.UltraWinToolbars.UltraToolbar("ExplorerGridToolBar");
      Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool1 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Refresh List");
      Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool2 = new Infragistics.Win.UltraWinToolbars.ButtonTool("RemoveItem");
      Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool7 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Share");
      Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool3 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Refresh List");
      Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
      Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
      Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool4 = new Infragistics.Win.UltraWinToolbars.ButtonTool("RemoveItem");
      Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GridFormatItemExplorer));
      Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
      Infragistics.Win.UltraWinToolbars.PopupMenuTool popupMenuTool1 = new Infragistics.Win.UltraWinToolbars.PopupMenuTool("_popupMenu");
      Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool5 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Refresh List");
      Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool6 = new Infragistics.Win.UltraWinToolbars.ButtonTool("RemoveItem");
      Infragistics.Win.UltraWinToolbars.ButtonTool buttonTool8 = new Infragistics.Win.UltraWinToolbars.ButtonTool("Share");
      Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
      this._nameComboBox = new System.Windows.Forms.ComboBox();
      this.iGridFormatItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
      this._lblName = new System.Windows.Forms.Label();
      this._explorerGridContaineerPanel = new System.Windows.Forms.Panel();
      this._chooserGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
      this.@__explorerGridContaineerPanel_Toolbars_Dock_Area_Left = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
      this._ultraToolbarsManager = new Infragistics.Win.UltraWinToolbars.UltraToolbarsManager(this.components);
      this.@__explorerGridContaineerPanel_Toolbars_Dock_Area_Right = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
      this.@__explorerGridContaineerPanel_Toolbars_Dock_Area_Top = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
      this.@__explorerGridContaineerPanel_Toolbars_Dock_Area_Bottom = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
      ((System.ComponentModel.ISupportInitialize)(this.iGridFormatItemBindingSource)).BeginInit();
      this._explorerGridContaineerPanel.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this._chooserGrid)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this._ultraToolbarsManager)).BeginInit();
      this.SuspendLayout();
      // 
      // _nameComboBox
      // 
      this._nameComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this._nameComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
      this._nameComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
      this._nameComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.iGridFormatItemBindingSource, "Name", true, System.Windows.Forms.DataSourceUpdateMode.Never));
      this._nameComboBox.DataSource = this.iGridFormatItemBindingSource;
      this._nameComboBox.DisplayMember = "Name";
      this._nameComboBox.FormattingEnabled = true;
      this._nameComboBox.Location = new System.Drawing.Point(44, 4);
      this._nameComboBox.Name = "_nameComboBox";
      this._nameComboBox.Size = new System.Drawing.Size(296, 21);
      this._nameComboBox.TabIndex = 72;
      this._nameComboBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this._nameComboBox_KeyDown);
      // 
      // iGridFormatItemBindingSource
      // 
      this.iGridFormatItemBindingSource.DataSource = typeof(MorganStanley.MSDesktop.Rambo.WinGridUtils.Resource.IGridFormatItem);
      // 
      // _lblName
      // 
      this._lblName.AutoSize = true;
      this._lblName.Location = new System.Drawing.Point(0, 7);
      this._lblName.Name = "_lblName";
      this._lblName.Size = new System.Drawing.Size(38, 13);
      this._lblName.TabIndex = 73;
      this._lblName.Text = "Name:";
      // 
      // _explorerGridContaineerPanel
      // 
      this._explorerGridContaineerPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this._explorerGridContaineerPanel.Controls.Add(this._chooserGrid);
      this._explorerGridContaineerPanel.Controls.Add(this.@__explorerGridContaineerPanel_Toolbars_Dock_Area_Left);
      this._explorerGridContaineerPanel.Controls.Add(this.@__explorerGridContaineerPanel_Toolbars_Dock_Area_Right);
      this._explorerGridContaineerPanel.Controls.Add(this.@__explorerGridContaineerPanel_Toolbars_Dock_Area_Top);
      this._explorerGridContaineerPanel.Controls.Add(this.@__explorerGridContaineerPanel_Toolbars_Dock_Area_Bottom);
      this._explorerGridContaineerPanel.Location = new System.Drawing.Point(0, 31);
      this._explorerGridContaineerPanel.Name = "_explorerGridContaineerPanel";
      this._explorerGridContaineerPanel.Size = new System.Drawing.Size(337, 289);
      this._explorerGridContaineerPanel.TabIndex = 74;
      // 
      // _chooserGrid
      // 
      this._ultraToolbarsManager.SetContextMenuUltra(this._chooserGrid, "_popupMenu");
      this._chooserGrid.DataSource = this.iGridFormatItemBindingSource;
      appearance38.BackColor = System.Drawing.SystemColors.Window;
      appearance38.BorderColor = System.Drawing.SystemColors.InactiveCaption;
      this._chooserGrid.DisplayLayout.Appearance = appearance38;
      this._chooserGrid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
      ultraGridColumn11.Header.Caption = "Filter Name";
      ultraGridColumn11.Header.VisiblePosition = 0;
      ultraGridColumn11.Width = 180;
      ultraGridColumn12.Header.VisiblePosition = 1;
      ultraGridColumn12.Width = 136;
      ultraGridColumn13.Header.VisiblePosition = 3;
      ultraGridColumn13.Hidden = true;
      ultraGridColumn14.Header.VisiblePosition = 4;
      ultraGridColumn14.Hidden = true;
      ultraGridColumn15.Header.VisiblePosition = 2;
      ultraGridColumn15.Hidden = true;
      ultraGridBand1.Columns.AddRange(new object[] {
            ultraGridColumn11,
            ultraGridColumn12,
            ultraGridColumn13,
            ultraGridColumn14,
            ultraGridColumn15});
      this._chooserGrid.DisplayLayout.BandsSerializer.Add(ultraGridBand1);
      this._chooserGrid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
      this._chooserGrid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
      appearance39.BackColor = System.Drawing.SystemColors.ActiveBorder;
      appearance39.BackColor2 = System.Drawing.SystemColors.ControlDark;
      appearance39.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
      appearance39.BorderColor = System.Drawing.SystemColors.Window;
      this._chooserGrid.DisplayLayout.GroupByBox.Appearance = appearance39;
      appearance40.ForeColor = System.Drawing.SystemColors.GrayText;
      this._chooserGrid.DisplayLayout.GroupByBox.BandLabelAppearance = appearance40;
      this._chooserGrid.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
      appearance41.BackColor = System.Drawing.SystemColors.ControlLightLight;
      appearance41.BackColor2 = System.Drawing.SystemColors.Control;
      appearance41.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
      appearance41.ForeColor = System.Drawing.SystemColors.GrayText;
      this._chooserGrid.DisplayLayout.GroupByBox.PromptAppearance = appearance41;
      this._chooserGrid.DisplayLayout.MaxColScrollRegions = 1;
      this._chooserGrid.DisplayLayout.MaxRowScrollRegions = 1;
      appearance42.BackColor = System.Drawing.SystemColors.Window;
      appearance42.ForeColor = System.Drawing.SystemColors.ControlText;
      this._chooserGrid.DisplayLayout.Override.ActiveCellAppearance = appearance42;
      appearance43.BackColor = System.Drawing.SystemColors.Highlight;
      appearance43.ForeColor = System.Drawing.SystemColors.HighlightText;
      this._chooserGrid.DisplayLayout.Override.ActiveRowAppearance = appearance43;
      this._chooserGrid.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
      this._chooserGrid.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
      this._chooserGrid.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
      this._chooserGrid.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
      appearance44.BackColor = System.Drawing.SystemColors.Window;
      this._chooserGrid.DisplayLayout.Override.CardAreaAppearance = appearance44;
      appearance45.BorderColor = System.Drawing.Color.Silver;
      appearance45.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
      this._chooserGrid.DisplayLayout.Override.CellAppearance = appearance45;
      this._chooserGrid.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.RowSelect;
      this._chooserGrid.DisplayLayout.Override.CellPadding = 0;
      this._chooserGrid.DisplayLayout.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow;
      appearance46.BackColor = System.Drawing.SystemColors.Control;
      appearance46.BackColor2 = System.Drawing.SystemColors.ControlDark;
      appearance46.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
      appearance46.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
      appearance46.BorderColor = System.Drawing.SystemColors.Window;
      this._chooserGrid.DisplayLayout.Override.GroupByRowAppearance = appearance46;
      appearance47.TextHAlignAsString = "Left";
      this._chooserGrid.DisplayLayout.Override.HeaderAppearance = appearance47;
      this._chooserGrid.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
      this._chooserGrid.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
      appearance48.BackColor = System.Drawing.Color.Gainsboro;
      this._chooserGrid.DisplayLayout.Override.RowAlternateAppearance = appearance48;
      this._chooserGrid.DisplayLayout.Override.RowSelectorHeaderStyle = Infragistics.Win.UltraWinGrid.RowSelectorHeaderStyle.None;
      this._chooserGrid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
      this._chooserGrid.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
      this._chooserGrid.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
      this._chooserGrid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
      this._chooserGrid.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
      this._chooserGrid.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
      this._chooserGrid.Dock = System.Windows.Forms.DockStyle.Fill;
      this._chooserGrid.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this._chooserGrid.Location = new System.Drawing.Point(0, 103);
      this._chooserGrid.Name = "_chooserGrid";
      this._chooserGrid.Size = new System.Drawing.Size(337, 186);
      this._chooserGrid.TabIndex = 71;
      this._chooserGrid.Text = "ultraGrid1";
      this._chooserGrid.AfterPerformAction += new Infragistics.Win.UltraWinGrid.AfterUltraGridPerformActionEventHandler(this._chooserGrid_AfterPerformAction);
      this._chooserGrid.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this._chooserGrid_DoubleClickRow);
      this._chooserGrid.KeyDown += new System.Windows.Forms.KeyEventHandler(this._chooserGrid_KeyDown);
      this._chooserGrid.AfterRowActivate += new System.EventHandler(this._chooserGrid_AfterRowActivate);
      // 
      // __explorerGridContaineerPanel_Toolbars_Dock_Area_Left
      // 
      this.@__explorerGridContaineerPanel_Toolbars_Dock_Area_Left.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
      this.@__explorerGridContaineerPanel_Toolbars_Dock_Area_Left.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(190)))), ((int)(((byte)(245)))));
      this.@__explorerGridContaineerPanel_Toolbars_Dock_Area_Left.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Left;
      this.@__explorerGridContaineerPanel_Toolbars_Dock_Area_Left.ForeColor = System.Drawing.SystemColors.ControlText;
      this.@__explorerGridContaineerPanel_Toolbars_Dock_Area_Left.Location = new System.Drawing.Point(0, 103);
      this.@__explorerGridContaineerPanel_Toolbars_Dock_Area_Left.Name = "__explorerGridContaineerPanel_Toolbars_Dock_Area_Left";
      this.@__explorerGridContaineerPanel_Toolbars_Dock_Area_Left.Size = new System.Drawing.Size(0, 186);
      this.@__explorerGridContaineerPanel_Toolbars_Dock_Area_Left.ToolbarsManager = this._ultraToolbarsManager;
      // 
      // _ultraToolbarsManager
      // 
      this._ultraToolbarsManager.DesignerFlags = 1;
      this._ultraToolbarsManager.DockWithinContainer = this._explorerGridContaineerPanel;
      this._ultraToolbarsManager.LockToolbars = true;
      this._ultraToolbarsManager.ShowFullMenusDelay = 500;
      this._ultraToolbarsManager.ShowQuickCustomizeButton = false;
      this._ultraToolbarsManager.Style = Infragistics.Win.UltraWinToolbars.ToolbarStyle.Office2003;
      ultraToolbar1.DockedColumn = 0;
      ultraToolbar1.DockedRow = 0;
      ultraToolbar1.NonInheritedTools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool1,
            buttonTool2,
            buttonTool7});
      ultraToolbar1.Settings.AllowCustomize = Infragistics.Win.DefaultableBoolean.False;
      ultraToolbar1.Settings.AllowDockBottom = Infragistics.Win.DefaultableBoolean.False;
      ultraToolbar1.Settings.AllowDockLeft = Infragistics.Win.DefaultableBoolean.False;
      ultraToolbar1.Settings.AllowDockRight = Infragistics.Win.DefaultableBoolean.False;
      ultraToolbar1.Settings.AllowDockTop = Infragistics.Win.DefaultableBoolean.False;
      ultraToolbar1.Settings.AllowFloating = Infragistics.Win.DefaultableBoolean.False;
      ultraToolbar1.Settings.AllowHiding = Infragistics.Win.DefaultableBoolean.False;
      ultraToolbar1.Settings.FillEntireRow = Infragistics.Win.DefaultableBoolean.True;
      ultraToolbar1.Settings.ShowToolTips = Infragistics.Win.DefaultableBoolean.True;
      ultraToolbar1.Settings.ToolDisplayStyle = Infragistics.Win.UltraWinToolbars.ToolDisplayStyle.ImageAndText;
      ultraToolbar1.Settings.ToolOrientation = Infragistics.Win.UltraWinToolbars.ToolOrientation.Horizontal;
      ultraToolbar1.Settings.UseLargeImages = Infragistics.Win.DefaultableBoolean.False;
      ultraToolbar1.Text = "ExplorerGridToolBar";
      this._ultraToolbarsManager.Toolbars.AddRange(new Infragistics.Win.UltraWinToolbars.UltraToolbar[] {
            ultraToolbar1});
      this._ultraToolbarsManager.ToolbarSettings.UseLargeImages = Infragistics.Win.DefaultableBoolean.False;
      appearance25.Image = global::MorganStanley.MSDesktop.Rambo.WinGridUtils.Properties.Resources.reload_16;
      buttonTool3.SharedPropsInternal.AppearancesLarge.Appearance = appearance25;
      appearance23.Image = global::MorganStanley.MSDesktop.Rambo.WinGridUtils.Properties.Resources.reload_16;
      buttonTool3.SharedPropsInternal.AppearancesSmall.Appearance = appearance23;
      buttonTool3.SharedPropsInternal.Caption = "Refresh List";
      buttonTool3.SharedPropsInternal.DisplayStyle = Infragistics.Win.UltraWinToolbars.ToolDisplayStyle.ImageAndText;
      appearance24.Image = ((object)(resources.GetObject("appearance24.Image")));
      buttonTool4.SharedPropsInternal.AppearancesLarge.Appearance = appearance24;
      appearance22.Image = ((object)(resources.GetObject("appearance22.Image")));
      buttonTool4.SharedPropsInternal.AppearancesSmall.Appearance = appearance22;
      buttonTool4.SharedPropsInternal.Caption = "Remove";
      buttonTool4.SharedPropsInternal.DisplayStyle = Infragistics.Win.UltraWinToolbars.ToolDisplayStyle.ImageAndText;
      popupMenuTool1.SharedPropsInternal.Caption = "_popupMenu";
      popupMenuTool1.SharedPropsInternal.CustomizerCaption = "Context Menu";
      popupMenuTool1.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool5,
            buttonTool6});
      appearance11.Image = global::MorganStanley.MSDesktop.Rambo.WinGridUtils.Properties.Resources.group_16;
      buttonTool8.SharedPropsInternal.AppearancesSmall.Appearance = appearance11;
      buttonTool8.SharedPropsInternal.Caption = "Share";
      this._ultraToolbarsManager.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            buttonTool3,
            buttonTool4,
            popupMenuTool1,
            buttonTool8});
      this._ultraToolbarsManager.ToolClick += new Infragistics.Win.UltraWinToolbars.ToolClickEventHandler(this._ultraToolbarsManager_ToolClick);
      // 
      // __explorerGridContaineerPanel_Toolbars_Dock_Area_Right
      // 
      this.@__explorerGridContaineerPanel_Toolbars_Dock_Area_Right.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
      this.@__explorerGridContaineerPanel_Toolbars_Dock_Area_Right.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(190)))), ((int)(((byte)(245)))));
      this.@__explorerGridContaineerPanel_Toolbars_Dock_Area_Right.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Right;
      this.@__explorerGridContaineerPanel_Toolbars_Dock_Area_Right.ForeColor = System.Drawing.SystemColors.ControlText;
      this.@__explorerGridContaineerPanel_Toolbars_Dock_Area_Right.Location = new System.Drawing.Point(337, 103);
      this.@__explorerGridContaineerPanel_Toolbars_Dock_Area_Right.Name = "__explorerGridContaineerPanel_Toolbars_Dock_Area_Right";
      this.@__explorerGridContaineerPanel_Toolbars_Dock_Area_Right.Size = new System.Drawing.Size(0, 186);
      this.@__explorerGridContaineerPanel_Toolbars_Dock_Area_Right.ToolbarsManager = this._ultraToolbarsManager;
      // 
      // __explorerGridContaineerPanel_Toolbars_Dock_Area_Top
      // 
      this.@__explorerGridContaineerPanel_Toolbars_Dock_Area_Top.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
      this.@__explorerGridContaineerPanel_Toolbars_Dock_Area_Top.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(190)))), ((int)(((byte)(245)))));
      this.@__explorerGridContaineerPanel_Toolbars_Dock_Area_Top.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Top;
      this.@__explorerGridContaineerPanel_Toolbars_Dock_Area_Top.ForeColor = System.Drawing.SystemColors.ControlText;
      this.@__explorerGridContaineerPanel_Toolbars_Dock_Area_Top.Location = new System.Drawing.Point(0, 0);
      this.@__explorerGridContaineerPanel_Toolbars_Dock_Area_Top.Name = "__explorerGridContaineerPanel_Toolbars_Dock_Area_Top";
      this.@__explorerGridContaineerPanel_Toolbars_Dock_Area_Top.Size = new System.Drawing.Size(337, 103);
      this.@__explorerGridContaineerPanel_Toolbars_Dock_Area_Top.ToolbarsManager = this._ultraToolbarsManager;
      // 
      // __explorerGridContaineerPanel_Toolbars_Dock_Area_Bottom
      // 
      this.@__explorerGridContaineerPanel_Toolbars_Dock_Area_Bottom.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
      this.@__explorerGridContaineerPanel_Toolbars_Dock_Area_Bottom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(190)))), ((int)(((byte)(245)))));
      this.@__explorerGridContaineerPanel_Toolbars_Dock_Area_Bottom.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Bottom;
      this.@__explorerGridContaineerPanel_Toolbars_Dock_Area_Bottom.ForeColor = System.Drawing.SystemColors.ControlText;
      this.@__explorerGridContaineerPanel_Toolbars_Dock_Area_Bottom.Location = new System.Drawing.Point(0, 289);
      this.@__explorerGridContaineerPanel_Toolbars_Dock_Area_Bottom.Name = "__explorerGridContaineerPanel_Toolbars_Dock_Area_Bottom";
      this.@__explorerGridContaineerPanel_Toolbars_Dock_Area_Bottom.Size = new System.Drawing.Size(337, 0);
      this.@__explorerGridContaineerPanel_Toolbars_Dock_Area_Bottom.ToolbarsManager = this._ultraToolbarsManager;
      // 
      // GridFormatItemExplorer
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
      this.Controls.Add(this._explorerGridContaineerPanel);
      this.Controls.Add(this._nameComboBox);
      this.Controls.Add(this._lblName);
      this.Name = "GridFormatItemExplorer";
      this.Size = new System.Drawing.Size(340, 348);
      this.Load += new System.EventHandler(this.GridFormatItemChooser_Load);
      ((System.ComponentModel.ISupportInitialize)(this.iGridFormatItemBindingSource)).EndInit();
      this._explorerGridContaineerPanel.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this._chooserGrid)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this._ultraToolbarsManager)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    //private MorganStanley.MSDesktop.Rambo.Desktop.FormsUtil.ProgressBarWrapper _progressBarWrapper;
    private System.Windows.Forms.BindingSource iGridFormatItemBindingSource;
      private System.Windows.Forms.ComboBox _nameComboBox;
      private System.Windows.Forms.Label _lblName;
      private System.Windows.Forms.Panel _explorerGridContaineerPanel;
      private Infragistics.Win.UltraWinGrid.UltraGrid _chooserGrid;
      private Infragistics.Win.UltraWinToolbars.UltraToolbarsManager _ultraToolbarsManager;
      private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea __explorerGridContaineerPanel_Toolbars_Dock_Area_Left;
      private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea __explorerGridContaineerPanel_Toolbars_Dock_Area_Right;
      private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea __explorerGridContaineerPanel_Toolbars_Dock_Area_Top;
      private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea __explorerGridContaineerPanel_Toolbars_Dock_Area_Bottom;
  }
}
