﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Infragistics.Win.UltraWinGrid;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils.GridExporter
{

  public interface IRowSelectionStrategy
  {
    IList<RowExport> GetSelectedRows(UltraGrid grid_);
  }

  public class AllRowsSelectionStrategy : IRowSelectionStrategy
  {
    public IList<RowExport> GetSelectedRows(UltraGrid grid_)
    {
      IList<RowExport> accum = new List<RowExport>();
      IEnumerable allRows = grid_.Rows.GetRowEnumerator(GridRowType.DataRow, null, null);
      foreach (UltraGridRow row in allRows)
      {
        accum.Add(new RowExport(row, row.Band.Index));
      }
      return accum;
    }
  }

  public class SelectedAndChildrenSelectionStrategy : IRowSelectionStrategy
  {
    public IList<RowExport> GetSelectedRows(UltraGrid grid_)
    {
      IList<RowExport> accum = new List<RowExport>();


      foreach (UltraGridRow selectedRow in grid_.Selected.Rows)
      {
        int selectedRowLevel = selectedRow.Band.Index;
        accum.Add(new RowExport(selectedRow, 0));
        if (selectedRow.ChildBands != null && selectedRow.ChildBands.Count > 0)
        {
          System.Collections.IEnumerable childRows = selectedRow.ChildBands[0].Rows.GetRowEnumerator(GridRowType.DataRow, null, null);
          foreach (UltraGridRow row in childRows)
          {
            accum.Add(new RowExport(row, row.Band.Index - selectedRowLevel));
          }
        }
      }
      return accum;
    }
  }

  public class SelectedRowsSelectionStrategy : IRowSelectionStrategy
  {
    public IList<RowExport> GetSelectedRows(UltraGrid grid_)
    {
      IList<RowExport> accum = new List<RowExport>();
      foreach (UltraGridRow selectedRow in grid_.Selected.Rows)
      {
        accum.Add(new RowExport(selectedRow, 0));
      }
      return accum;
    }
  }

  public class RowsInCurrentLevelSelectionStrategy : IRowSelectionStrategy
  {
    public IList<RowExport> GetSelectedRows(UltraGrid grid_)
    {
      IList<RowExport> accum = new List<RowExport>();
      System.Collections.IEnumerable allRows =
        grid_.Rows.GetRowEnumerator(GridRowType.DataRow, grid_.ActiveRow.Band,
                                           grid_.ActiveRow.Band);
      foreach (UltraGridRow row in allRows)
      {
        accum.Add(new RowExport(row, 0));
      }
      return accum;
    }
  }

  public class ExpandedRowsSelectionStrategy : IRowSelectionStrategy
  {
    public IList<RowExport> GetSelectedRows(UltraGrid grid_)
    {
      IList<RowExport> accum = new List<RowExport>();
      System.Collections.IEnumerable allRows =
        grid_.Rows.GetRowEnumerator(GridRowType.DataRow, null, null);
      foreach (UltraGridRow row in allRows)
      {
        if (row.Expanded || row.ParentRow == null || row.ParentRow.Expanded)
        {
          accum.Add(new RowExport(row, row.Band.Index));
        }
      }
      return accum;
    }
  }

  public class FilteredRowsSelectionStrategy : IRowSelectionStrategy
  {
    public IList<RowExport> GetSelectedRows(UltraGrid grid_)
    {
      IList<RowExport> accum = new List<RowExport>();
      var filterRows = grid_.Rows.GetFilteredInNonGroupByRows();
      foreach (var row in filterRows)
      {
        accum.Add(new RowExport(row, row.Band.Index));
      }
      return accum;
    }
  }
}
