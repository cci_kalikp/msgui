﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Infragistics.Documents.Excel;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils.GridExporter
{
  public class ExcelGridExporter : IGridExporter
  {

    #region IExportStrategy Members

    public event EventHandler<BeforeSavingEventArgs> BeforeSaving;

    public event EventHandler<ExportCompletedEventArg> ExportCompleted;

    public event EventHandler<ProgressChangedArgs> StatusChanged;

    public Workbook Workbook{ get; set;}

    public Worksheet Worksheet{ get; set;}

    public ExcelGridExporter()
    {
      Workbook=new Workbook();
    }

    protected void OnBeforeSaving(BeforeSavingEventArgs args_)
    {
      if (BeforeSaving != null)
      {
        BeforeSaving(this, args_);
      }
    }

    protected void OnExportCompleted(ExportCompletedEventArg args_)
    {
      if (ExportCompleted != null)
      {
        ExportCompleted(this, args_);
      }

    }

    protected void OnStatusChanged(ProgressChangedArgs args_)
    {
      if (StatusChanged != null)
      {
        StatusChanged(this, args_);
      }

    }

    public void Export(UltraGrid _currentGrid, 
      IRowSelectionStrategy selectionStrategy, string fileName_,string captionName_)
    {

      Exception error=null;
      var canceled = false;
      int? totalRows = null;
      int? blankRows = null;

      try
      {
        var rows = selectionStrategy.GetSelectedRows(_currentGrid);
        totalRows = rows.Count;
        var progress = new ProgressTracker(rows.Count,1,this);
        progress.ProgressChanged += StatusChanged;
        if (captionName_ == null)
        {
          captionName_ = "Worksheet1";
        }
        Worksheet = Workbook.Worksheets.Add(captionName_);
        var columnCaptions = ExportUtils.GetColumnsCaptions(_currentGrid.GetRow(ChildRow.First));
        var columnKeys = ExportUtils.GetColumnsKeys(_currentGrid.GetRow(ChildRow.First));
        var maxColumnSizes = new int[columnKeys.Length];
        var columnColors = new Color[columnKeys.Length];
        
        // Export Column Headers 
        int rowNumber = 0;
        Color headerColor = Color.FromArgb(SystemColors.Control.ToArgb());
        
        for (int i = 0; i < columnCaptions.Length; ++i)
        {
          var worksheetCell = Worksheet.Rows[rowNumber].Cells[i];
          worksheetCell.Value = columnCaptions[i];
          maxColumnSizes[i] = worksheetCell.Value.ToString().Length + 4;
          columnColors[i] = _currentGrid.DisplayLayout.Bands[0].Columns[columnKeys[i]].CellAppearance.BackColor;
          worksheetCell.CellFormat.FillPatternForegroundColor = headerColor;
          worksheetCell.CellFormat.FillPattern = FillPatternStyle.Solid;
          worksheetCell.CellFormat.Alignment = HorizontalCellAlignment.Center;
          worksheetCell.CellFormat.VerticalAlignment = VerticalCellAlignment.Center;
          worksheetCell.CellFormat.Font.Bold = ExcelDefaultableBoolean.True;
          worksheetCell.CellFormat.RightBorderColor = Color.Black;
          worksheetCell.CellFormat.RightBorderStyle = CellBorderLineStyle.Medium;
          worksheetCell.CellFormat.BottomBorderColor = Color.Black;
          worksheetCell.CellFormat.BottomBorderStyle = CellBorderLineStyle.Double;
        }

        var knownFormats = new Dictionary<string, string>();

        foreach (RowExport rowEx in rows)
        {
          if (rowEx.Row.IsDeleted)
          {
            blankRows++;
            continue;
          }

          rowNumber++;
          //RiskViewerApplet.Loop.Invoke(new ExportRowToExcelDelegate(ExportRowToExcel),columnCaptions, worksheet, rowNumber, maxColumnSizes, columnColors, rowEx, knownFormats);
          ExportRowToExcel(columnCaptions, rowNumber, maxColumnSizes, columnColors, rowEx, knownFormats);
          progress.IncrementCompletedWorkItems(1);
          if(progress.Canceled)
          {
            canceled = true;
            return;
          }
          
        }

        // Attempt to Autofit column widths 
        int k = 0;

        foreach (WorksheetColumn column in Worksheet.Columns)
        {
          //Column.Width is 1/256 of Avg Charecter size 
          //Very obscure
          column.Width = maxColumnSizes[k]*256;
          k++;
        }
        // Excel 2007 opens the sheet with a corrupted name unless this is set
        OnStatusChanged(new ProgressChangedArgs(99));
        Worksheet.Name = captionName_;
        var beforeSavingArgs = new BeforeSavingEventArgs();
        OnBeforeSaving(beforeSavingArgs);

        if (!beforeSavingArgs.CancelSave)
        {
          // Save out file - may throw an exception if the workbook is already open, 
          // and we are saving over it
          
          Workbook.Save(fileName_);
        }
        else
        {
          canceled = true;
        }
      }
      catch (Exception e)
      {
        error = e;
      }
      finally
      {
        OnExportCompleted(new ExportCompletedEventArg(error, canceled, fileName_, totalRows, blankRows));
      }
    }

    #endregion


    private void ExportRowToExcel(string[] columnCaptions, int rowNumber, int[] maxColumnSizes, Color[] columnColors, RowExport rowEx, IDictionary<string, string> knownFormats)
    {
      string[] columnKeys;
      columnKeys = ExportUtils.GetColumnsKeys(rowEx.Row);

      if (columnKeys.Length != columnCaptions.Length)
      {
        throw new InvalidOperationException(
          "Excel export does not support grids using multiple levels with different Columns settings.\n\n" +
          "Please change your Columns settings or groupings to successfully export the grid.");
      }

      if (columnKeys.Length > 256)
      {
        throw new InvalidOperationException(
          "Excel can not support more than 256 columns.\n\nPlease change your Columns setting to successfully export the grid.");
      }

      for (int j = 0; j < columnKeys.Length; ++j)
      {

        UltraGridCell cell = rowEx.Row.Cells[columnKeys[j]];
        WorksheetCell worksheetCell = Worksheet.Rows[rowNumber].Cells[j];

        object value;

        value = cell.Value;

        if (value != DBNull.Value && value != null)
        {
          // detect and format numbers 
          if ((cell.Column.DataType == typeof(decimal)) ||
              (cell.Column.DataType == typeof(double)) ||
              (cell.Column.DataType == typeof(float)) ||
              (cell.Column.DataType == typeof(int)) ||
              (cell.Column.DataType == typeof(uint)) ||
              (cell.Column.DataType == typeof(long)) ||
              (cell.Column.DataType == typeof(ulong)) ||
              (cell.Column.DataType == typeof(short)) ||
              (cell.Column.DataType == typeof(ushort)))
          {
            worksheetCell.Value = Convert.ToDouble(value);

            string cellFormat = ExportUtils.GetCellFormat(cell, knownFormats);
            worksheetCell.CellFormat.FormatString = cellFormat;
          }
          else if (cell.Column.DataType == typeof(DateTime))
          {
            if (cell.Value != null && cell.Value != DBNull.Value)
            {
              worksheetCell.Value = (DateTime)value;
            }
          }
          else
          {
            worksheetCell.Value = value.ToString();
          }
        }

        //Getting bachground color form the layoput of the column
        Color backGround = columnColors[j];

        if (backGround != Color.Empty)
        {
          worksheetCell.CellFormat.FillPattern = FillPatternStyle.Solid;
          worksheetCell.CellFormat.FillPatternForegroundColor = backGround;
          worksheetCell.CellFormat.RightBorderColor = Color.White;
          worksheetCell.CellFormat.RightBorderStyle = CellBorderLineStyle.Medium;
          worksheetCell.CellFormat.BottomBorderColor = Color.White;
          worksheetCell.CellFormat.BottomBorderStyle = CellBorderLineStyle.Medium;
        }

        worksheetCell.CellFormat.Font.Bold = rowEx.Row.Appearance.FontData.Bold == DefaultableBoolean.True
                                               ? ExcelDefaultableBoolean.True : ExcelDefaultableBoolean.False;

        if (j == 0)
        {
          worksheetCell.CellFormat.Alignment = HorizontalCellAlignment.Left;
          worksheetCell.CellFormat.Indent = rowEx.IndentLevel;
        }
        else
        {
          worksheetCell.CellFormat.Alignment = HorizontalCellAlignment.Right;
        }

        if (worksheetCell.Value != null)
        {
          int length = worksheetCell.Value.ToString().Length;
          if (maxColumnSizes[j] < length)
          {
            maxColumnSizes[j] = length;
          }
        }
      }
    }
  }
}


