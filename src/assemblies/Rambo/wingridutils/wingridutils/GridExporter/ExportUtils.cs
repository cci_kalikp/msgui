﻿using System;
using System.Collections.Generic;
using System.Text;
using Infragistics.Win.UltraWinGrid;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils.GridExporter
{
	public class ExportUtils
	{

    /// <summary>
    /// Returns the columns for each non-hidden cell in the supplied row in the order they appear in the grid
    /// </summary>
    /// <param name="row_">The row to return columns for.</param>
    /// <returns>The visible set of columns.</returns>
    public static UltraGridColumn[] GetVisibleColumns(UltraGridRow row_)
    {
      ColumnsCollection colsCollect = row_.Band.Columns;
      List<UltraGridColumn> cols = new List<UltraGridColumn>(colsCollect.Count);
      UltraGridColumn[] returnVal;

      for (int i = 0; i < colsCollect.Count; ++i)
      {
        if (colsCollect[i].Hidden)
          continue;

        cols.Add(colsCollect[i]);
      }

      cols.Sort(new VisiblePositionComparer());
      returnVal = cols.ToArray();

      return returnVal;
    }

    public static string[] GetColumnsCaptions(UltraGridRow row_)
    {
      if (row_ == null)
        return null;

      UltraGridColumn[] cols = GetVisibleColumns(row_);


      var names = new string[cols.Length];

      for (int i = 0; i < cols.Length; ++i)
      {
        names[i] = cols[i].Header.Caption;
      }

      return names;
    }

    /// <summary>
    /// Returns the column keys for each non-hidden column in row_ in the order they appear in the grid
    /// </summary>
    /// <param name="row_"></param>
    /// <returns></returns>
    public static string[] GetColumnsKeys(UltraGridRow row_)
    {
      if (row_ == null)
        return null;

      UltraGridColumn[] cols = GetVisibleColumns(row_);


      var names = new string[cols.Length];

      for (int i = 0; i < cols.Length; ++i)
      {
        names[i] = cols[i].Key;
      }

      return names;
    }


    static public string GetCellFormat(UltraGridCell cell, IDictionary<string, string> knownFormats)
    {
      // preserve formatting based on cell (.net formatting)
      // needs to translate foratting to excel style formatting so
      // for each new format used in DDSource table a special case 
      String originalFormat = cell.Column.Format;
      // caches translated formats to speed up the process
      String cellFormat;
      if (originalFormat == null || !knownFormats.TryGetValue(originalFormat, out cellFormat))
      {
        cellFormat = null;
      }
      if (cellFormat == null && originalFormat != null)
      {
        cellFormat = originalFormat;
        // special case 'p' - percentage formatting not recognized by excel
        if (cellFormat.IndexOf("p") >= 0)
        {
          string precisionText = cellFormat.Replace("p", "");
          if (precisionText == String.Empty)
          {
            cellFormat = "0.00%";
          }
          else
          {
            int precision = Convert.ToInt32(precisionText);
            cellFormat = "#,##0.";
            cellFormat = cellFormat.PadRight(cellFormat.Length + precision, '0');
            cellFormat += "%";
          }
          cellFormat = cellFormat + ";[Red]-" + cellFormat + ";" + cellFormat;
        }
        else
        {
          // applies red color to negative values
          if (cellFormat.IndexOf(";") >= 0)
          {
            String[] parts = cellFormat.Split(';');
            if (parts.Length > 1)
            {
              parts[1] = "[Red]" + parts[1];
              cellFormat = string.Join(";", parts);
            }
          }
        }
        knownFormats[originalFormat] = cellFormat;
      }
      if (string.IsNullOrEmpty(cellFormat))
      {
        cellFormat = "General";
      }
      return cellFormat;
    }
    private class VisiblePositionComparer : IComparer<UltraGridColumn>
    {
      public int Compare(UltraGridColumn x, UltraGridColumn y)
      {
        int visx, visy;

        visx = (x).Header.VisiblePositionWithinBand;
        visy = (y).Header.VisiblePositionWithinBand;

        return visx - visy;
      }
    }
	}
}
