﻿using System;
using Infragistics.Win.UltraWinGrid;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils.GridExporter
{
  public interface IGridExporter
  {
    event EventHandler<BeforeSavingEventArgs> BeforeSaving;
    event EventHandler<ExportCompletedEventArg> ExportCompleted;
    event EventHandler<ProgressChangedArgs> StatusChanged;
    void Export(UltraGrid grid_,
      IRowSelectionStrategy selectionStrategy,
      string fileName_,string captionName_);
    
  }

  public class BeforeSavingEventArgs:EventArgs
  {
    public bool CancelSave{ get; set; }
  }
  
  public class StatusChangedEventArgs:EventArgs
  {
    public StatusChangedEventArgs(int percentCompleted_)
    {
      _percentCompleted = percentCompleted_;
    }

    public int _percentCompleted;
    public int PercentCompleted { get { return _percentCompleted; } }
    public bool CancelExport { get; set; }
  }

  public class ExportCompletedEventArg:EventArgs
  {    
    public ExportCompletedEventArg(Exception error_, bool canceled_, string fileName_, int? totalRows_, int? blankRows_)
    {
      FileName = fileName_;
      Error = error_;
      Canceled = canceled_;
      TotalRows = totalRows_;
      BlankRows = blankRows_;
    }

    public ExportCompletedEventArg(Exception error_, bool canceled_, string fileName_) : 
      this (error_, canceled_, fileName_, null, null)
    {
    }

    public string FileName { get; private set; }
    public Exception Error { get; private set; }
    public bool Canceled { get; private set; }
    public int? TotalRows { get; private set; }
    public int? BlankRows { get; private set; }
  }

  public struct RowExport
  {
    #region Readonly & Static Fields

    public readonly int IndentLevel;
    public readonly UltraGridRow Row;
    #endregion

    #region Constructors

    public RowExport(UltraGridRow urow, int level)
    {
      Row = urow;
      IndentLevel = level;
    }

    #endregion
  } ;

}
