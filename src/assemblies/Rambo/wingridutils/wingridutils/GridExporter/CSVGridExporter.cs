﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using Infragistics.Win.UltraWinGrid;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils.GridExporter
{
  public class CSVGridExporter : IGridExporter
  {
    public event EventHandler<BeforeSavingEventArgs> BeforeSaving;

    protected void OnBeforeSaving(BeforeSavingEventArgs e)
    {
      EventHandler<BeforeSavingEventArgs> beforeSavingHandler = BeforeSaving;
      if (beforeSavingHandler != null) beforeSavingHandler(this, e);
    }

    public event EventHandler<ExportCompletedEventArg> ExportCompleted;

    protected void OnExportCompleted(ExportCompletedEventArg e)
    {
      EventHandler<ExportCompletedEventArg> exportCompletedHandler = ExportCompleted;
      if (exportCompletedHandler != null) exportCompletedHandler(this, e);
    }

    public event EventHandler<ProgressChangedArgs> StatusChanged;

    public void Export(UltraGrid _currentGrid, IRowSelectionStrategy selectionStrategy, string fileName_,string captionName_)
    {
      var canceled = false;
      Exception error = null;
      int? totalRows = null;
      int? blankRows = null;
      
      try
      {
        var rows = selectionStrategy.GetSelectedRows(_currentGrid);
        totalRows = rows.Count;
        var progress = new ProgressTracker(rows.Count,1,this);
        progress.ProgressChanged += StatusChanged;
        using (var sw = new StreamWriter(fileName_))
        {
          var columnCaptions = ExportUtils.GetColumnsCaptions(_currentGrid.GetRow(ChildRow.First));
          var columnsLength = columnCaptions.Length;
          var sb = new StringBuilder();

          for (int i = 0; i < columnsLength; ++i)
          {
            sb.Append(Regex.Replace(columnCaptions[i], @"[,\r\n\f\t\v]", string.Empty));
            sb.Append(",");
          }

          // sb.Append("Level"); - disabled because of RVR-1721


          sw.WriteLine(sb.ToString());

          foreach(RowExport exRow in rows)
          {
            if(exRow.Row.IsDeleted)
            {
              blankRows++;
              continue;
            }

            var columnKeys = ExportUtils.GetColumnsKeys(exRow.Row);

            if (columnKeys.Length != columnCaptions.Length)
            {
              throw new InvalidOperationException(
                "Excel export does not support grids using multiple levels with different Columns settings.\n\n" +
                "Please change your Columns settings or Groupings to successfully export the grid.");
            }

            sb = new StringBuilder();

            for (int i = 0; i < columnsLength; ++i)
            {
              var cell = exRow.Row.Cells[columnKeys[i]];

              var text = string.Empty;
              if (cell.Value != DBNull.Value && cell.Value != null)
              {
                text = cell.Value.ToString();
              }

              sb.Append(Regex.Replace(text, @"[,\r\n\f\t\v]", string.Empty));
              sb.Append(",");
            }

            // sb.Append(exRow.IndentLevel); - disabled because of RVR-1721

            sw.WriteLine(sb.ToString());
            progress.IncrementCompletedWorkItems(1);
          }
        }
      }
      catch (Exception ex)
      {
        error = ex;
      }
      finally
      {
        OnExportCompleted(new ExportCompletedEventArg(error, canceled, fileName_, totalRows, blankRows));
      }
    }
  }
}