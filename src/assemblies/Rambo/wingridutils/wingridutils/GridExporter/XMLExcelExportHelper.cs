using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Collections.Generic;
using System.Drawing;
using System.Diagnostics;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils.GridExporter
{
  /// <summary>
  /// Provides basic functionality to export table to excel using XML file.
  /// </summary>
  public class XMLExcelExportHelper
  {
    private static char[] XML_CHARS = new char[] { '&', '<', '>', '"', '\'' };
    private static string[] XML_CHARS_PATTERN_MATCH = new string[] { "&", "<", ">", "\"", "'" };
    private static string[] XML_CHARS_PATTERN_SUBST = new string[] { "&amp;", "&lt;", "&gt;", "&quot;", "&apos;" };

    internal XmlTextWriter _writerHeader;
    internal XmlTextWriter _writerFooter;
    internal XmlTextWriter _writerStyles;

    private string _filename;
    private string _caption;
    private string _nameHeader;
    private string _nameFooter;
    private string _nameStyles;

    private XmlDocument _style;

    private string _namespace;
    private string _namespaceId;
    private string _namespacePattern;

    private object _cellValue;

    private Dictionary<string, string> _styleIdByContent;

    private int _styleCounter = 36;

    // temporary stores output for caption's row
    StringBuilder _columnRow;

    public XmlElement Style
    {
      get
      {
        return _style.DocumentElement;
      }
    }

    public string StyleNamespace
    {
      get
      {
        return _namespace;
      }
    }

    private string GetNewStyleId()
    {
      _styleCounter++;
      return "s" + _styleCounter.ToString();
    }

    public static void OpenExcelWithXmlFile(string filename_)
    {
      Process proc = new Process();
      proc.StartInfo.UseShellExecute = true;
      proc.StartInfo.FileName = "excel";
      // prevent spaces in full directory names from causing issues
      proc.StartInfo.Arguments = string.Format("\"{0}\"", filename_);
      proc.Start();
    }


    public XMLExcelExportHelper(string filename_, string caption_)
    {
      _filename = filename_;
      _caption = caption_;
      _nameHeader = Path.ChangeExtension(_filename, ".xml");
      _nameFooter = Path.GetTempFileName();
      _nameStyles = Path.GetTempFileName();
      _writerHeader = new XmlTextWriter(_nameHeader, Encoding.Unicode);
      _writerFooter = new XmlTextWriter(_nameFooter, Encoding.Unicode);
      _writerStyles = new XmlTextWriter(_nameStyles, Encoding.Unicode);
      _styleIdByContent = new Dictionary<string, string>();
      _style = new XmlDocument();
      _style.XmlResolver = null;
      _namespace = "none";
      _namespaceId = "xmlns:ss";
      _namespacePattern = _namespaceId + "=\"" + _namespace + "\"";
      XmlNamespaceManager nm = new XmlNamespaceManager(_style.NameTable);
      nm.AddNamespace(_namespaceId, _namespace);
      ResetStyle();
    }

    public void ResetStyle()
    {
      _style.RemoveAll();
      XmlElement styleNode = _style.CreateElement("Style");
      _style.AppendChild(styleNode);
      _style.DocumentElement.SetAttribute(_namespaceId, _namespace);
      // styleNode.SetAttribute("theName", _namespace, "theVal");
    }

    private string ToXmlValue(string input_)
    {
      if (input_.IndexOfAny(XML_CHARS) >= 0)
      {
        for (int i = 0; i < XML_CHARS.Length; i++)
        {
          input_ = input_.Replace(XML_CHARS_PATTERN_MATCH[i], XML_CHARS_PATTERN_SUBST[i]);
        }
      }
      return input_;
    }

    public void WriteHeader()
    {
      _writerHeader.WriteRaw(FormatConstant(
                               @"
                    <?xml version='1.0'?>
                    <?mso-application progid='Excel.Sheet'?>
                "
                               ));

      _writerHeader.WriteRaw(FormatConstant(
                               @"
                    <Workbook xmlns='urn:schemas-microsoft-com:office:spreadsheet'
                    xmlns:o='urn:schemas-microsoft-com:office:office'
                    xmlns:x='urn:schemas-microsoft-com:office:excel'
                    xmlns:ss='urn:schemas-microsoft-com:office:spreadsheet'
                    xmlns:html='http://www.w3.org/TR/REC-html40'>
                "
                               ));

      // all data inside workbook
      WriteDocumentProperties(_writerHeader);
      WriteExcelWorkbook(_writerHeader);

    }

    public void WriteFooter()
    {
      WriteStyles(_writerStyles);
      _writerFooter.WriteRaw("</Workbook>");
    }

    public void Close()
    {
      _writerHeader.Close();
      _writerFooter.Close();
      _writerStyles.Close();

      FileStream streamHeader = new FileStream(_nameHeader, FileMode.Append);
      AppendToStream(streamHeader, _nameStyles);
      AppendToStream(streamHeader, _nameFooter);
      streamHeader.Close();
      File.Delete(_nameStyles);
      File.Delete(_nameFooter);
    }

    private void AppendToStream(FileStream destination_, string sourceFilename_)
    {
      const int BUFF_SIZE = 32768;
      byte[] buff = new byte[BUFF_SIZE];
      FileStream source = new FileStream(sourceFilename_, FileMode.Open, FileAccess.Read);

      int available = 0;
      while ((available = source.Read(buff, 0, buff.Length)) > 0)
      {
        destination_.Write(buff, 0, available);
      }
      source.Close();
    }

    private void WriteDocumentProperties(XmlTextWriter writer_)
    {
      writer_.WriteRaw(FormatConstant(
                         @"
                    <DocumentProperties xmlns='urn:schemas-microsoft-com:office:office'>
                    <Author>Unknown</Author>
                    <LastAuthor>Unknown</LastAuthor>
                    <Created>2007-01-01T00:00:00Z</Created>
                    <LastSaved>2007-01-01T00:00:00Z</LastSaved>
                    <Manager>Unknown</Manager>
                    <Version>11.8132</Version>
                    </DocumentProperties>
            "));
    }

    private void WriteExcelWorkbook(XmlTextWriter writer_)
    {
      writer_.WriteRaw(FormatConstant(
                         @"
                   <ExcelWorkbook xmlns='urn:schemas-microsoft-com:office:excel'>
                   <WindowHeight>6795</WindowHeight>
                   <WindowWidth>11340</WindowWidth>
                   <WindowTopX>1</WindowTopX>
                   <WindowTopY>1</WindowTopY>
                   <ProtectStructure>False</ProtectStructure>
                   <ProtectWindows>False</ProtectWindows>
                   </ExcelWorkbook>
            "));
    }

    private void WriteWorkSheetOptions(XmlTextWriter writer_)
    {
      writer_.WriteRaw(FormatConstant(
                         @"
                    <WorksheetOptions xmlns='urn:schemas-microsoft-com:office:excel'>
                    <Selected/>
                    <TopRowVisible>1</TopRowVisible>
                    <LeftColumnVisible>1</LeftColumnVisible>
                    <Panes>
                    <Pane>
                    <Number>3</Number>
                    <ActiveRow>1</ActiveRow>
                    </Pane>
                    </Panes>
                    <ProtectObjects>False</ProtectObjects>
                    <ProtectScenarios>False</ProtectScenarios>
                    </WorksheetOptions>
            "));
    }

    private void WriteStyles(XmlTextWriter writer_)
    {
      writer_.WriteRaw("<Styles>");
      foreach (string key in _styleIdByContent.Keys)
      {
        String styleId = _styleIdByContent[key];
        // removes extra attribute from style and writes style to stream
        string styleIdNamePart = String.Format("ss:ID=\"{0}\" ss:Name=\"Normal_{0}\"", styleId);
        writer_.WriteRaw(key.Replace(_namespacePattern, styleIdNamePart));
        writer_.WriteRaw("\r\n");
      }
      writer_.WriteRaw("</Styles>");
    }

    public void WriteTableHeader()
    {
      _writerFooter.WriteRaw(String.Format("<Worksheet ss:Name=\"{0}\">", _caption));
      _writerFooter.WriteRaw(FormatConstant(
                               @"
                       <Table x:FullColumns='1' x:FullRows='1'>
                 "));
    }

    public void WriteTableFooter()
    {
      _writerFooter.WriteRaw("</Table>");
      WriteWorkSheetOptions(_writerFooter);
      _writerFooter.WriteRaw("</Worksheet>");
    }

    private static string FormatConstant(string value_)
    {
      StringBuilder sb = new StringBuilder();
      string[] lines = value_.Split('\r');
      for (int i = 0; i < lines.Length; i++)
      {
        string line = lines[i].Replace("\t", string.Empty);
        line = line.Replace("\n", string.Empty);
        line = line.Replace("'", "\"");
        line = line.Trim();

        if (line != string.Empty)
        {
          sb.Append(line + "\r\n");
        }
      }
      return sb.ToString();
    }


    public void ColumnOpen()
    {
      _columnRow = new StringBuilder();
      _columnRow.Append("<Row ss:AutoFitHeight=\"0\" ss:Height=\"25.5\">\r\n");
    }

    public void ColumnWrite(string styleId_, string column_)
    {
      _writerFooter.WriteRaw("<Column ss:AutoFitWidth=\"1\" ss:Width=\"100.00\"/>\r\n");
      _columnRow.AppendFormat("<Cell ss:StyleID=\"{0}\"><Data ss:Type=\"String\">{1}</Data></Cell>",
                              styleId_, ToXmlValue(column_));
    }

    public void ColumnClose()
    {
      _columnRow.Append("</Row>\r\n");
      _writerFooter.WriteRaw(_columnRow.ToString());
      _columnRow = null;
    }

    public string RegisterStyle()
    {
      string content = _style.OuterXml;
      string styleId;
      if (!_styleIdByContent.ContainsKey(content))
      {
        styleId = GetNewStyleId();
        _styleIdByContent[content] = styleId;
      }
      else
      {
        styleId = _styleIdByContent[content];
      }
      ResetStyle();
      return styleId;
    }

    public void SetColumnsCount(int columns_)
    {
      // ignored
    }

    public string ConvertColor(Color color_)
    {
      return System.Drawing.ColorTranslator.ToHtml(color_);
    }

    public void CellOpen()
    {
      ResetStyle();
      _cellValue = string.Empty;
    }

    public void CellWriteValue(object data_)
    {
      _cellValue = data_;
    }

    public void CellClose()
    {
      string cellStyleId = RegisterStyle();
      string convertedValue = string.Empty;
      string typeAttributeValue = string.Empty;
      Type type = _cellValue.GetType();

      if (type == typeof(string))
      {
        typeAttributeValue = "String";
        convertedValue = (string)_cellValue;
      }
      else if (type == typeof(DateTime))
      {
        typeAttributeValue = "DateTime";
        convertedValue = Convert.ToDateTime(Convert.ToString(_cellValue)).ToString("yyyy-MM-ddTHH:mm:ss.FFF");
      }
      else if (type == typeof(double))
      {
        typeAttributeValue = "Number";
        convertedValue = Convert.ToString(_cellValue);
      }

      _writerFooter.WriteRaw(String.Format(
                               "<Cell ss:StyleID=\"{0}\"><Data ss:Type=\"{1}\">{2}</Data></Cell>\r\n",
                               cellStyleId, ToXmlValue(typeAttributeValue), ToXmlValue(convertedValue)));
    }

    public void RowOpen()
    {
      _writerFooter.WriteRaw("<Row>\r\n");
    }

    public void RowClose()
    {
      _writerFooter.WriteRaw("</Row>");
    }

    public void SetStyle(bool useExistingNode_, string nodeName_, params string[] attributeValuePairs_)
    {
      SetStyle(_style.DocumentElement, useExistingNode_, nodeName_, attributeValuePairs_);
    }

    public void SetStyle(XmlElement root_, bool useExistingNode_, string nodeName_, params string[] attributeValuePairs_)
    {
      XmlElement node = null;
      if (useExistingNode_)
      {
        node = (XmlElement)root_.SelectSingleNode(nodeName_);
      }
      if (node == null)
      {
        node = _style.CreateElement(nodeName_);
        root_.AppendChild(node);
      }
      foreach (string pair in attributeValuePairs_)
      {
        string[] parts = pair.Split('=');
        if (parts.Length > 1)
        {
          node.SetAttribute(parts[0], _namespace, parts[1]);
        }
      }
    }
  }
}