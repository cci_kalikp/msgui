﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using Infragistics.Win.UltraWinGrid;
using MorganStanley.MSDesktop.Rambo.Desktop.Utils;
using System.Xml;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils.GridExporter
{
  public class XMLGridExporter : IGridExporter
  {
    
    private Logger _log = new Logger(typeof(XMLGridExporter));

    #region IGridExporterMembers

    public event EventHandler<BeforeSavingEventArgs> BeforeSaving;

    protected void OnBeforeSaving(BeforeSavingEventArgs e)
    {
      EventHandler<BeforeSavingEventArgs> beforeSavingHandler = BeforeSaving;
      if (beforeSavingHandler != null) beforeSavingHandler(this, e);
    }

    public event EventHandler<ExportCompletedEventArg> ExportCompleted;

    protected void OnExportCompleted(ExportCompletedEventArg e)
    {
      EventHandler<ExportCompletedEventArg> exportCompletedHandler = ExportCompleted;
      if (exportCompletedHandler != null) exportCompletedHandler(this, e);
    }

    public event EventHandler<ProgressChangedArgs> StatusChanged;

    protected void OnStatusChanged(ProgressChangedArgs e)
    {
      EventHandler<ProgressChangedArgs> statusChangedHandler = StatusChanged;
      if (statusChangedHandler != null) statusChangedHandler(this, e);
    }

    public void Export(UltraGrid _currentGrid, IRowSelectionStrategy selectionStrategy, string fileName_, string captionName_)
    {
      var canceled = false;
      Exception error=null;
      var startTime = DateTime.Now;      
      int? blankRows = null;
      
      var rows_ = selectionStrategy.GetSelectedRows(_currentGrid);
      int? totalRows = rows_.Count;
      var progress = new ProgressTracker(rows_.Count,1,this);
      progress.ProgressChanged += StatusChanged;
      try
      {
        if (string.IsNullOrEmpty(captionName_))
        {
          captionName_ = "Sheet1";
        }
        var helper = new XMLExcelExportHelper(fileName_, captionName_);
        _log.Info("ExportToXml", "Exporting to excel");
        var knownFormats = new Dictionary<string, string>();

        var columnCaptions = ExportUtils.GetColumnsCaptions(_currentGrid.GetRow(ChildRow.First));

        // sets number of columns
        helper.SetColumnsCount(columnCaptions.Length);

        helper.WriteHeader();

        // creates formatting in advance for each column
        Color headerColor = Color.FromArgb(SystemColors.Control.ToArgb());

        helper.SetStyle(true, "Alignment", "Horizontal=Center", "Vertical=Center");
        helper.SetStyle(true, "Interior", "Color=" + helper.ConvertColor(headerColor), "Pattern=Solid");
        helper.SetStyle(true, "Font", "FontName=Tahoma", "Bold=1");
        //helper.SetStyle(true,"Border/Borders","Position=Bottom","LineStyle=Double","Weight=3");
        // helper.SetStyle(false,"Borders","Position=Right");
        string headerStyleId = helper.RegisterStyle();

        helper.WriteTableHeader();

        // creates column definition
        helper.ColumnOpen();
        for (int i = 0; i < columnCaptions.Length; ++i)
        {
          helper.ColumnWrite(headerStyleId, columnCaptions[i]);
        }
        helper.ColumnClose();


        // creates format definitions for all rows 
        // Export Rows 
        foreach (RowExport rowEx in rows_)
        {
          if (rowEx.Row.IsDeleted)
          {
            blankRows++;
            continue;
          }

          helper.RowOpen();
          string[] columnKeys = ExportUtils.GetColumnsKeys(rowEx.Row);

          if (columnKeys.Length != columnCaptions.Length)
          {
            throw new InvalidOperationException(
              "Excel export does not support grids using multiple levels with different Columns settings.\n\n" +
              "Please change your Columns settings or Groupings to successfully export the grid.");
          }

          if (columnKeys.Length > 256)
          {
            throw new InvalidOperationException(
              "Excel can not support more than 256 columns.\n\nPlease change your Columns setting to successfully export the grid.");
          }

          for (int j = 0; j < columnKeys.Length; ++j)
          {
            UltraGridCell cell = rowEx.Row.Cells[columnKeys[j]];
            helper.CellOpen();
            helper.ResetStyle();
            Color backGround = rowEx.Row.Appearance.BackColor;
            if (backGround != Color.Empty)
            {
              XmlElement borders = helper.Style.OwnerDocument.CreateElement("Borders");
              helper.Style.AppendChild(borders);

              helper.SetStyle(borders, true, "Border", "Position=Bottom", "LineStyle=Double", "Weight=3",
                              "Color=" + helper.ConvertColor(Color.White));

              helper.SetStyle(borders, true, "Border", "Position=Right", "LineStyle=Double", "Weight=3",
                              "Color=" + helper.ConvertColor(Color.White));

              helper.SetStyle(true, "Interior", "Pattern=Solid",
                              "Color=" + helper.ConvertColor(rowEx.Row.Appearance.BackColor));
            }

            string HAlignment = (j == 0) ? "Left" : "Right";
            string indentLevel = (j != 0) ? "0" : rowEx.IndentLevel.ToString();

            helper.SetStyle(true, "Alignment",
                            "Horizontal=" + HAlignment, "Vertical=Center", "Indent=" + indentLevel);

            if (cell.Value != DBNull.Value && cell.Value != null)
            {
              // detect and format numbers 
              if ((cell.Column.DataType == typeof(decimal)) ||
                  (cell.Column.DataType == typeof(double)) ||
                  (cell.Column.DataType == typeof(float)) ||
                  (cell.Column.DataType == typeof(int)) ||
                  (cell.Column.DataType == typeof(uint)) ||
                  (cell.Column.DataType == typeof(long)) ||
                  (cell.Column.DataType == typeof(ulong)) ||
                  (cell.Column.DataType == typeof(short)) ||
                  (cell.Column.DataType == typeof(ushort)))
              {
                if ((cell.Column.DataType == typeof(decimal)))
                {
                  var dec = (decimal)cell.Value;
                  helper.CellWriteValue(decimal.ToDouble(dec));
                }
                else
                {
                  helper.CellWriteValue((double)cell.Value);
                }

                // preserve formatting based on cell (.net formatting)
                // needs to translate foratting to excel style formatting so
                // for each new format used in DDSource table a special case 
                String originalFormat = cell.Column.Format;
                // caches translated formats to speed up the process
                String cellFormat;
                if (originalFormat == null || !knownFormats.TryGetValue(originalFormat, out cellFormat))
                {
                  cellFormat = null;
                }

                if (cellFormat == null && originalFormat != null)
                {
                  cellFormat = originalFormat;
                  // special case 'p' - percentage formatting not recognized by excel
                  if (cellFormat.IndexOf("p") >= 0)
                  {
                    string precisionText = cellFormat.Replace("p", "");
                    if (precisionText == String.Empty)
                    {
                      cellFormat = "0.00%";
                    }
                    else
                    {
                      int precision = Convert.ToInt32(precisionText);
                      cellFormat = "#,##0.";
                      cellFormat = cellFormat.PadRight(cellFormat.Length + precision, '0');
                      cellFormat += "%";
                    }
                    cellFormat = cellFormat + ";[Red]-" + cellFormat + ";" + cellFormat;
                  }
                  else
                  {
                    // applies red color to negative values
                    if (cellFormat.IndexOf(";") >= 0)
                    {
                      String[] parts = cellFormat.Split(';');
                      if (parts.Length > 1)
                      {
                        parts[1] = "[Red]" + parts[1];
                        cellFormat = string.Join(";", parts);
                      }
                    }
                  }
                  knownFormats[originalFormat] = cellFormat;
                }
                if (string.IsNullOrEmpty(cellFormat))
                {
                  cellFormat = "General";
                }
                if (cellFormat != string.Empty)
                {
                  helper.SetStyle(true, "NumberFormat", "Format=" + cellFormat);
                }
              }
              else if (cell.Column.DataType == typeof(DateTime))
              {
                if (cell.Value != null && cell.Value != DBNull.Value)
                {
                  helper.SetStyle(true, "NumberFormat", "Format=" + "Short Date");
                  helper.CellWriteValue((DateTime)cell.Value);
                }
              }
              else
              {
                helper.CellWriteValue(cell.Text);
              }
            }
            helper.CellClose();
          }
          helper.RowClose();

          progress.IncrementCompletedWorkItems(1);
          if(progress.Canceled)
          {
            canceled = true;
            return;
          }

        }
        var beforeSavingArgs = new BeforeSavingEventArgs();
        OnBeforeSaving(beforeSavingArgs);
      
        if (!beforeSavingArgs.CancelSave)
        {
          helper.WriteTableFooter();
          helper.WriteFooter();
          helper.Close();
        }

        TimeSpan ts = DateTime.Now - startTime;
        _log.Info("ExportToXml", String.Format("Exporting to excel completed, time spent = {0} sec(s)", ts.TotalSeconds));
      }
      catch (Exception ex_)
      {
        string message = string.Format("Error creating/saving file '{0}'.\n\nPlease close Excel and retry", fileName_);
        _log.Error("ExportToXml", message, ex_);
        error = ex_;
      }
      finally
      {        
        OnExportCompleted(new ExportCompletedEventArg(error,canceled,fileName_, totalRows, blankRows));
      }
    }

    #endregion
  }
}

