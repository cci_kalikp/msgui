﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils.GridExporter
{

  /// <summary>
  /// This class is used to track the progress of the series of work items
  /// It will treage an event once specified percentage of all work items is completed
  /// </summary>
  public class ProgressTracker
  {
    private bool _canceled;
    private int _percentCompleted;
    private int _percentPerWorkItem;
    private int _previouslyReported;
    private int _reportPercent;
    private object _owner;
   private int _workItemsProcessed;

    /// <summary>
    /// Creates an instance of type ProgressTracker
    /// </summary>
    /// <param name="numberOfWorkItems_">Total number of work Items to be completed</param>
    /// <param name="reportPercent_">Event will be triggered every reportPercent_'th percent</param>
    /// <param name="owner">Owner of the progress tracker</param>
    public ProgressTracker(int numberOfWorkItems_, int reportPercent_,object owner)
    {
      _owner = owner;
      NumberOfWorkItems = numberOfWorkItems_;
      _reportPercent = reportPercent_;
      _percentPerWorkItem = 100/NumberOfWorkItems;
    }
    /// <summary>
    /// Returns an owner of the ProgressTracker
    /// </summary>
    public object Owner
    {
      get { return _owner; }
    }

    /// <summary>
    /// Increments the total number of completed work items
    /// </summary>
    /// <param name="incermentBy_"> </param>
    public void IncrementCompletedWorkItems(int incermentBy_)
    {
      _workItemsProcessed += incermentBy_;
      _percentCompleted += _workItemsProcessed * _percentPerWorkItem;
      
      if((_percentCompleted-_previouslyReported)>=ReportPercent)
      {
        ProgressChangedArgs args = new ProgressChangedArgs(_percentCompleted);
        InvokeProgressChanged(args);
        _canceled = args.Canceled;
      }
    }


    /// <summary>
    /// This event is fired every ReportPercent'th of total job completed
    /// </summary>
    public event EventHandler<ProgressChangedArgs> ProgressChanged;

    private void InvokeProgressChanged(ProgressChangedArgs e)
    {
      EventHandler<ProgressChangedArgs> progressChangedHandler = ProgressChanged;
      if (progressChangedHandler != null) progressChangedHandler(this, e);
    }

    /// <summary>
    /// Total number of Work Items to be processed
    /// </summary>
    public int NumberOfWorkItems{ get; set;}
    /// <summary>
    /// Total Number of work items processed
    /// </summary>
    public int WorkItemsProcessed { get
    {
      return _workItemsProcessed;
    }
    }
    /// <summary>
    /// total percent of the work completed
    /// </summary>
    public int PercentCompleted { get{ return _percentCompleted;}}

    /// <summary>
    /// Indicates if the job was canceled via ProgressChanged event
    /// </summary>
    public bool Canceled{ get { return _canceled;}}

    /// <summary>
    /// Indicatates how often the ProgressChangedEvent is fired 
    /// </summary>
    public int ReportPercent {get{ return _reportPercent;} }
  }

  /// <summary>
  /// Arguments that are passed in ProgressTracker's ProgressChanged event
  /// </summary>
  public class ProgressChangedArgs:EventArgs
  {
    
    private int _percentCompleted;
    
    /// <summary>
    ///
    /// </summary>
    /// <param name="percentCompleted_"> Total percent of the job completed</param>
    public ProgressChangedArgs(int percentCompleted_ )
    {
      _percentCompleted = percentCompleted_;
    }
    /// <summary>
    /// Percent completed of the total work items
    /// </summary>
    public double PercentCompleted { get { return _percentCompleted; } }
    /// <summary>
    /// Cancelled flag, if supported has to be checked by a client to indicate if 
    /// Job should stop processing
    /// </summary>
    public bool Canceled { get; set; }

  }
}
