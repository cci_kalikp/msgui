//---------------------------------------------------------------------------
//
// Copyright (c) 2004 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
//---------------------------------------------------------------------------

using System;
using System.Xml.Serialization;
using JetBrains.Annotations;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{

	/// <summary>
	/// XML representation of a GUI DataGrid.
	/// </summary>
	[XmlRootAttribute("Grid")]
	public class GridLayout
	{
	  public GridLayout()
	  {
	    Font = RowFontStyleEnum.Default;
	  }

	  public string LayoutName { get; set; }

	  [XmlIgnore, Obsolete("Do not use this property - it returns GridName. To get  Name of the layout use LayoutName. To get Name of the grid use GridName")]
    public string Name
    {
      get
      {
        return ( GridName ) ;
      }
    }

    [XmlIgnore]
    public string Owner { get; set; }

	  public string Application { get; set; }

	  public string GridName { get; set; }

	  public int LockedColumns { get; set; }

	  public string UpdateID { get; set; }

	  [CanBeNull]
	  public ColumnLayoutCollection Columns { get; set; }

	  public int BandId { get; set; }

	  public RowFontStyleEnum Font { get; set; }

	  public override string ToString()
		{
			return LayoutName;
		}

    public static GridLayout Copy(GridLayout gl)
    {
      GridLayout newGL = new GridLayout
                           {
                             Application = String.Copy(gl.Application),
                             GridName = String.Copy(gl.GridName),
                             LayoutName = String.Copy(gl.LayoutName),
                             LockedColumns = gl.LockedColumns,
                             UpdateID = String.Copy(gl.UpdateID),
                             Font = gl.Font,
                             BandId = gl.BandId
                           };

      if (gl.Columns != null)
        newGL.Columns = new ColumnLayoutCollection(gl.Columns);
      return newGL;

    }
	}	
}
