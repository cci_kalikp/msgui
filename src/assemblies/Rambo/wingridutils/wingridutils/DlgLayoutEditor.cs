using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using Infragistics.Win.UltraWinGrid;

namespace MorganStanley.Ecdev.WinGridUtils
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class DlgLayoutEditor : System.Windows.Forms.Form
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
    private System.Windows.Forms.TabControl _tabControl;   
     private UltraGridBand _band; 
    private CntrlFormat _cntrlFormat;
    private CntrlShowHide _cntrlShowHide;
    private System.Windows.Forms.TabPage _tabShowHide;
    private System.Windows.Forms.TabPage _tabSort;
    private System.Windows.Forms.TabPage _tabFormat;
    private System.Windows.Forms.Panel _panelButtons;
    private System.Windows.Forms.Button _btnOk;
    private System.Windows.Forms.Button _btnCancel;
    private CntrlSort _cntrlSort;

    private DlgLayoutEditor()
    {
      //
      // Required for Windows Form Designer support
      //
      InitializeComponent();
    }	

    public DlgLayoutEditor( UltraGridBand band_ ):this()
    {
      _band = band_;

      _cntrlFormat = new CntrlFormat( _band );
      _cntrlShowHide = new CntrlShowHide( _band );
      _cntrlSort = new CntrlSort( _band );

      _cntrlFormat.Dock = DockStyle.Fill;
      _cntrlShowHide.Dock = DockStyle.Fill;
      _cntrlSort.Dock = DockStyle.Fill;

      _tabFormat.Controls.Add( _cntrlFormat );
      _tabShowHide.Controls.Add( _cntrlShowHide );
      _tabSort.Controls.Add( _cntrlSort );  
    }

    public void AcceptChanges()
    {   
      _cntrlFormat.AcceptChanges();
      _cntrlShowHide.AcceptChanges();
      _cntrlSort.AcceptChanges();
    }

    
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this._tabControl = new System.Windows.Forms.TabControl();
			this._tabShowHide = new System.Windows.Forms.TabPage();
			this._tabSort = new System.Windows.Forms.TabPage();
			this._tabFormat = new System.Windows.Forms.TabPage();
			this._panelButtons = new System.Windows.Forms.Panel();
			this._btnCancel = new System.Windows.Forms.Button();
			this._btnOk = new System.Windows.Forms.Button();
			this._tabControl.SuspendLayout();
			this._panelButtons.SuspendLayout();
			this.SuspendLayout();
			// 
			// _tabControl
			//
			/*
			this._tabControl.Controls.AddRange(new System.Windows.Forms.Control[] {
																					  this._tabShowHide,
																					  this._tabSort,
																					  this._tabFormat});
																					  */
			this._tabControl.Controls.AddRange(new System.Windows.Forms.Control[] {
																					  this._tabShowHide});
			this._tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this._tabControl.Name = "_tabControl";
			this._tabControl.SelectedIndex = 0;
			this._tabControl.Size = new System.Drawing.Size(378, 304);
			this._tabControl.TabIndex = 0;
			// 
			// _tabShowHide
			// 
			this._tabShowHide.Location = new System.Drawing.Point(4, 22);
			this._tabShowHide.Name = "_tabShowHide";
			this._tabShowHide.Size = new System.Drawing.Size(370, 278);
			this._tabShowHide.TabIndex = 0;
			this._tabShowHide.Text = "Show/Hide";
			// 
			// _tabSort
			// 
			this._tabSort.Location = new System.Drawing.Point(4, 22);
			this._tabSort.Name = "_tabSort";
			this._tabSort.Size = new System.Drawing.Size(370, 278);
			this._tabSort.TabIndex = 1;
			this._tabSort.Text = "Sort";
			// 
			// _tabFormat
			// 
			this._tabFormat.Location = new System.Drawing.Point(4, 22);
			this._tabFormat.Name = "_tabFormat";
			this._tabFormat.Size = new System.Drawing.Size(370, 278);
			this._tabFormat.TabIndex = 2;
			this._tabFormat.Text = "Format";
			// 
			// _panelButtons
			// 
			this._panelButtons.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this._panelButtons.Controls.AddRange(new System.Windows.Forms.Control[] {
																						this._btnCancel,
																						this._btnOk});
			this._panelButtons.Dock = System.Windows.Forms.DockStyle.Bottom;
			this._panelButtons.Location = new System.Drawing.Point(0, 264);
			this._panelButtons.Name = "_panelButtons";
			this._panelButtons.Size = new System.Drawing.Size(378, 40);
			this._panelButtons.TabIndex = 1;
			// 
			// _btnCancel
			// 
			this._btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this._btnCancel.Location = new System.Drawing.Point(304, 8);
			this._btnCancel.Name = "_btnCancel";
			this._btnCancel.Size = new System.Drawing.Size(64, 24);
			this._btnCancel.TabIndex = 1;
			this._btnCancel.Text = "Cancel";
			// 
			// _btnOk
			// 
			this._btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
			this._btnOk.Location = new System.Drawing.Point(232, 8);
			this._btnOk.Name = "_btnOk";
			this._btnOk.Size = new System.Drawing.Size(64, 24);
			this._btnOk.TabIndex = 0;
			this._btnOk.Text = "OK";
			// 
			// DlgLayoutEditor
			// 
			this.AcceptButton = this._btnOk;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.CancelButton = this._btnCancel;
			this.ClientSize = new System.Drawing.Size(378, 304);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this._panelButtons,
																		  this._tabControl});
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "DlgLayoutEditor";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Layout Editor";
			this._tabControl.ResumeLayout(false);
			this._panelButtons.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion	
	}
}
