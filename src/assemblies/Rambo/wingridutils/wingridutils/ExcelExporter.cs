using System;
using System.Collections.Generic;
using System.Text;

using Infragistics.Win.UltraWinGrid.ExcelExport;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{ 
  public class ExcelExporter
  {
    private static UltraGridExcelExporter _exporter;
    private static object _syncLock = new object();

    public static UltraGridExcelExporter Exporter
    {
      get
      {
        if (_exporter == null)
        {
          _exporter = new UltraGridExcelExporter();
        }
        return _exporter;
      }
    }
  }
}
