using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using Infragistics.Win.UltraWinGrid;

namespace MorganStanley.Ecdev.WinGridUtils
{
	/// <summary>
	/// Summary description for CntlColShowHide.
	/// </summary>
  public class CntrlShowHide : System.Windows.Forms.UserControl
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.Container components = null;
    private System.Windows.Forms.ListBox _lstHidden;
    private System.Windows.Forms.ListBox _lstVisible;
    private System.Windows.Forms.Button _btnShow;
    private System.Windows.Forms.Button _btnHide;
    private UltraGridBand _band;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.NumericUpDown _nudLockedCols; 
    private ArrayList _sortedCaptions;

    private CntrlShowHide()
    {
      // This call is required by the Windows.Forms Form Designer.
      InitializeComponent(); 

      // TODO: Add any initialization after the InitForm call
    }

    public CntrlShowHide( UltraGridBand band_ ):this()
    {
      _band = band_;
    }

    protected override void OnLoad( EventArgs e_ )
    {     
      _sortedCaptions = new ArrayList( _band.Columns.Count );
      UltraGridColumn[] columns = new UltraGridColumn[_band.Columns.Count];
      for ( int i = 0; i < _band.Columns.Count; i++ )
      {     
        UltraGridColumn col = _band.Columns[i];
          
        columns[i] = col;
        string caption =  col.Header.Caption;       

        _sortedCaptions.Add( caption );
        if ( col.Hidden )
        {
          _lstHidden.Items.Add( caption );      
        }
        else
        {
          _lstVisible.Items.Add( caption );
        }
      }      
      _sortedCaptions.Sort();

      //select the first item
      if ( _lstHidden.Items.Count > 0 )
      {
        _lstHidden.SelectedIndex = 0;
      }

      if ( _lstVisible.Items.Count  > 0 )
      {
        _lstVisible.SelectedIndex = 0;
      }

      HandleButtons();

      Array.Sort( columns, new ColumnComparer() );

      //fixed columns
      for ( int i = 0; i < columns.Length; i++ )
      {
        UltraGridColumn col = columns[i];
        if ( col.Header.Fixed )
        {
          _nudLockedCols.Value += 1;
        }
        else
        {
          break;
        }
      }
    }

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    protected override void Dispose( bool disposing )
    {
      if( disposing )
      {
        if(components != null)
        {
          components.Dispose();
        }
      }
      base.Dispose( disposing );
    }

		#region Component Designer generated code
    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
		this._lstHidden = new System.Windows.Forms.ListBox();
		this._lstVisible = new System.Windows.Forms.ListBox();
		this._btnShow = new System.Windows.Forms.Button();
		this._btnHide = new System.Windows.Forms.Button();
		this.label1 = new System.Windows.Forms.Label();
		this.label2 = new System.Windows.Forms.Label();
		this._nudLockedCols = new System.Windows.Forms.NumericUpDown();
		this.label3 = new System.Windows.Forms.Label();
		((System.ComponentModel.ISupportInitialize)(this._nudLockedCols)).BeginInit();
		this.SuspendLayout();
		// 
		// _lstHidden
		// 
		this._lstHidden.Location = new System.Drawing.Point(8, 48);
		this._lstHidden.Name = "_lstHidden";
		this._lstHidden.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
		this._lstHidden.Size = new System.Drawing.Size(128, 173);
		this._lstHidden.Sorted = true;
		this._lstHidden.TabIndex = 0;
		// 
		// _lstVisible
		// 
		this._lstVisible.Location = new System.Drawing.Point(224, 48);
		this._lstVisible.Name = "_lstVisible";
		this._lstVisible.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
		this._lstVisible.Size = new System.Drawing.Size(128, 173);
		this._lstVisible.Sorted = true;
		this._lstVisible.TabIndex = 1;
		// 
		// _btnShow
		// 
		this._btnShow.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
		this._btnShow.Location = new System.Drawing.Point(144, 88);
		this._btnShow.Name = "_btnShow";
		this._btnShow.Size = new System.Drawing.Size(72, 24);
		this._btnShow.TabIndex = 2;
		this._btnShow.Text = "Show >>";
		this._btnShow.Click += new System.EventHandler(this._btnShow_Click);
		// 
		// _btnHide
		// 
		this._btnHide.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
		this._btnHide.Location = new System.Drawing.Point(144, 120);
		this._btnHide.Name = "_btnHide";
		this._btnHide.Size = new System.Drawing.Size(72, 24);
		this._btnHide.TabIndex = 3;
		this._btnHide.Text = "<< Hide";
		this._btnHide.Click += new System.EventHandler(this._btnHide_Click);
		// 
		// label1
		// 
		this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
		this.label1.Location = new System.Drawing.Point(232, 32);
		this.label1.Name = "label1";
		this.label1.Size = new System.Drawing.Size(112, 16);
		this.label1.TabIndex = 4;
		this.label1.Text = "Shown";
		this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
		// 
		// label2
		// 
		this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
		this.label2.Location = new System.Drawing.Point(8, 32);
		this.label2.Name = "label2";
		this.label2.Size = new System.Drawing.Size(128, 16);
		this.label2.TabIndex = 5;
		this.label2.Text = "Hidden";
		this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
		// 
		// _nudLockedCols
		// 
		this._nudLockedCols.Location = new System.Drawing.Point(224, 8);
		this._nudLockedCols.Name = "_nudLockedCols";
		this._nudLockedCols.Size = new System.Drawing.Size(48, 20);
		this._nudLockedCols.TabIndex = 6;
		this._nudLockedCols.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
		// 
		// label3
		// 
		this.label3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
		this.label3.Location = new System.Drawing.Point(8, 8);
		this.label3.Name = "label3";
		this.label3.Size = new System.Drawing.Size(216, 20);
		this.label3.TabIndex = 7;
		this.label3.Text = "Number of locked columns (no scrolling):";
		this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
		// 
		// CntrlShowHide
		// 
		this.Controls.AddRange(new System.Windows.Forms.Control[] {
																	  this.label3,
																	  this._nudLockedCols,
																	  this.label2,
																	  this.label1,
																	  this._btnHide,
																	  this._btnShow,
																	  this._lstVisible,
																	  this._lstHidden});
		this.Name = "CntrlShowHide";
		this.Size = new System.Drawing.Size(360, 232);
		((System.ComponentModel.ISupportInitialize)(this._nudLockedCols)).EndInit();
		this.ResumeLayout(false);

	}
		#endregion
   
    public void AcceptChanges()
    {   
      UltraGridColumn[] columns = new UltraGridColumn[_band.Columns.Count];
   
      //column visibility
      for ( int i = 0; i < _band.Columns.Count; i++ )
      {
        UltraGridColumn col = _band.Columns[i];
        col.Hidden = ( _lstHidden.FindString(col.Header.Caption) != -1 );
        columns[i] = col;
      }
      
      Array.Sort( columns, new ColumnComparer() );

      //fixed columns            
      int totalLockedCols = Convert.ToInt32( _nudLockedCols.Value );    
      for ( int i = 0; i < columns.Length; i++ )
      { 
        UltraGridColumn col = columns[i];
        col.Header.Fixed = (totalLockedCols > 0 && i < totalLockedCols);
      }   
    }

    private void HandleButtons()
    {
      _btnShow.Enabled = ( _lstHidden.Items.Count > 0 );
      _btnHide.Enabled = ( _lstVisible.Items.Count > 0 );
    }

    private void MoveColumn( ListBox source_, ListBox destination_ )
    {
      destination_.ClearSelected();
        
      string lastCaption = null;
      foreach ( UltraGridColumn col in _band.Columns )
      { 
        string caption = col.Header.Caption;
        int index = source_.FindString( caption );

        if ( index != -1 && source_.GetSelected(index) )
        {        
          if ( lastCaption == null || string.Compare(lastCaption,caption) == -1 )
          {
            lastCaption = caption;
          }

          source_.Items.RemoveAt( index );          
          destination_.SetSelected( destination_.Items.Add(caption), true );
        }             
      }
  
      if ( destination_.SelectedItem == null && destination_.Items.Count > 0 )
      {
        destination_.SelectedIndex = 0;
      }

      HandleButtons();
      
      //find the next item to select.
      if ( lastCaption != null )
      {
        foreach ( string caption in _sortedCaptions )
        {
          int index = source_.FindString( caption );
          if ( index != -1 && string.Compare(caption,lastCaption) == 1 )
          {
            source_.SelectedIndex = index;
            return;
          }
        }
      }

      if ( source_.Items.Count > 0 )
      {
        source_.SelectedIndex = 0;
      }    
    }
    
    private void _btnShow_Click( object sender_, System.EventArgs e_ )
    {      
      MoveColumn( _lstHidden, _lstVisible  );
    }

    private void _btnHide_Click(object sender, System.EventArgs e)
    {
      MoveColumn( _lstVisible, _lstHidden );
    }

    //--------------------------------------------------------------------------------

    protected class ColumnComparer : IComparer
    {
      public int Compare( object a_, object b_ )
      {
        UltraGridColumn a = (UltraGridColumn)a_;
        UltraGridColumn b = (UltraGridColumn)b_;
        
        if ( !a.Hidden && !b.Hidden )
        {         
          if ( a.Header.VisiblePosition < b.Header.VisiblePosition )
          {
            return -1;
          }
          else if ( a.Header.VisiblePosition > b.Header.VisiblePosition )
          {
            return 1;
          }
        }
        else if ( a.Hidden && !b.Hidden )
        {
          return 1;
        }
        else if ( !a.Hidden && b.Hidden )
        {
          return -1;
        }    
        
        return 0;      
      }
    }
	}
}
