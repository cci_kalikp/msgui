
//---------------------------------------------------------------------------
//
// Copyright (c) 2004 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
//---------------------------------------------------------------------------

using System;
using System.Text;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  /// <summary>
  /// Class to provide column format string interpretation and generation.
  /// </summary>
  public class ColumnFormat
  {

    public static bool PARENTHESIZE_NEGATIVE_NUMBERS;
    #region Private Member Variables
    private string m_originalFormat;
    private Type m_dataType;
    private string[] m_originalFormatSections;


    private const string FORMAT_EXCEL = "d";

    #endregion



    #region Constructors





    /// <summary>
    /// Initialises an instance of the ColumnFormat class.
    /// </summary>
    /// <param name="format_">Format string of column.</param>
    /// <param name="dataType_">Data type of column that format is for.</param>

    public ColumnFormat(string format_, Type dataType_)
    {
      m_originalFormat = format_;


      if (m_originalFormat == null)
      {
        m_originalFormat = GenerateFormatString(false, PrecisionType.Default, UnitType.One);
      }


      m_originalFormatSections = m_originalFormat.Split(';');
      m_dataType = dataType_;
    }

    #endregion

    #region Enumerations

    /// <summary>
    /// Column format unit type enumeration.
    /// </summary>
    public enum UnitType
    {
      /// <summary>
      /// Standard numeric unit type.
      /// </summary>
      One,
      /// <summary>
      /// Original Number multiplied by 1000.
      /// </summary>
      Thousand,
      /// <summary>
      ///  Original Number multiplied by 1,000,000.
      /// </summary>
      Million,
      /// <summary>
      /// Original Number as a percentage.
      /// </summary>
      Percentage,
      /// <summary>
      /// Original Number divided by 100.
      /// </summary>
      Hundredth
    }

    /// <summary>
    /// Column format precision type enumeration.
    /// </summary>
    public enum PrecisionType
    {
      Default = -1,
      Zero = 0,
      One = 1,
      Two = 2,
      Three = 3,
      Four = 4,
      Five = 5,
      Six = 6,
      Seven = 7,
      Eight = 8,
      Nine = 9,
      Ten = 10
    };

    #endregion


    #region Public Methods

    /// <summary>
    /// Returns true if format uses comma seperation.
    /// </summary>
    /// <returns>A boolean indicating use of comma seperation.</returns>
    public bool HasCommas()
    {
      int index = m_originalFormatSections[0].IndexOf(",");
      return (index == 1);
    }

    /// <summary>
    /// Returns the precision of the formatting options - significant figures after the
    /// decmial place.
    /// </summary>
    /// <returns>The format precision as an integer.</returns>
    public PrecisionType GetPrecision()
    {
      //This is the case for FLow desk when the actual format is "d"
      //in this case we will use default precision number is displayed exactly
      if (m_originalFormatSections[0].Length < 2 )
      {
        return PrecisionType.Default;
      }
      int precision = -1;

      int index = m_originalFormatSections[0].IndexOf(".");
      if (index == -1)
      {
        index = m_originalFormatSections[0].IndexOfAny(new char[] { 'g', 'f', 'n', 'p', 'e', FORMAT_EXCEL[0] });
        if (index < m_originalFormatSections[0].Length - 1 &&
          Char.IsDigit(m_originalFormatSections[0], index + 1))
        {
          precision = int.Parse(m_originalFormatSections[0].ToCharArray()[index + 1].ToString());
        }
        else if(index>0)
        {
          return PrecisionType.Default;
        }
        else
        {
          precision = 0;
        }
      }
      else
      {
        // skip over % as because it should not affect precision
        string precisionFilteredString = m_originalFormatSections[0];
        precisionFilteredString = precisionFilteredString.Replace("\"%\"", string.Empty);

        precision = (precisionFilteredString.Length - 1) - index;
        string precisionString = precisionFilteredString.Substring(index + 1);

        foreach (char c in precisionString.ToCharArray())
        {
          if (c != ',' && c != '#' && c != '0')
          {
            throw new Exception(string.Format("Parse Error for precision string: {0}", precisionString));
          }
        }
      }

      if (precision < -1 || precision > 10)
        precision = 0;
      
      return (PrecisionType)precision;
    }

    /// <summary>
    /// Returns the unit type of the Column Format.
    /// </summary>
    /// <returns></returns>
    public UnitType GetUnits()
    {
      if (m_originalFormatSections[0].IndexOf("p") > -1)
      {
        return UnitType.Percentage;
      }

      int commas = 0;
      int position = 0;

      // Loop through format string and locate number of commas
      while (position != -1 && position < m_originalFormatSections[0].Length)
      {
        position = m_originalFormatSections[0].IndexOf(",", position);
        if (position != -1)
        {
          ++commas;
          ++position;
        }
      }

      // Remove comma delineating sepeator for thousands leaving any trailing commas
      if (HasCommas()) --commas;

      switch (commas)
      {
        case 0:
          return UnitType.One;
        case 1:
          return UnitType.Thousand;
        case 2:
          return UnitType.Million;
        default:
          throw new Exception("Parse Error");
      }
    }


    /// <summary>
    /// Will convert instance of string into a ColumnFormat.UnitType representation
    /// </summary>
    /// <param name="val_">String value to convert</param>
    /// <returns>ColumnFormat.UnitType representation </returns>
    public static ColumnFormat.UnitType ConvertStringToUnitType(string val_)
    {
      switch (val_)
      {
        case "1,000":
          return ColumnFormat.UnitType.Thousand;
        case "1,000,000":
          return ColumnFormat.UnitType.Million;
        case "%":
          return ColumnFormat.UnitType.Percentage;
        case "0.01":
          return ColumnFormat.UnitType.Hundredth;
        default:
          // handles "1" and string.empty cases
          return ColumnFormat.UnitType.One;
      }
    }

    /// <summary>
    /// Will convert instance of type ColumnFormat.UnitType into a string representation
    /// </summary>
    /// <param name="unitType_">value to convert</param>
    /// <returns>String representation </returns>
    public static string ConvertUnitTypeToString(ColumnFormat.UnitType unitType_)
    {
      string units = string.Empty;

      switch (unitType_)
      {
        case ColumnFormat.UnitType.One:
          units = "1";
          break;
        case ColumnFormat.UnitType.Thousand:
          units = "1,000";
          break;
        case ColumnFormat.UnitType.Million:
          units = "1,000,000";
          break;
        case ColumnFormat.UnitType.Percentage:
          units = "%";
          break;
        case ColumnFormat.UnitType.Hundredth:
          units = "0.01";
          break;
        default:
          units = string.Empty;
          break;
      }
      return units;
    }


    /// <summary>
    /// Gets a syntacting string representation of the format Unit;
    /// </summary>
    /// <returns>String representation of the unit</returns>
    public string GetUnitsString()
    {
      UnitType tmpUnits = GetUnits();
      return ConvertUnitTypeToString(tmpUnits);
    }

    /// <summary>
    /// Given defined format options generates and returns a format string that can be applied
    /// to a particular data grid column.
    /// </summary>
    /// <param name="commas_">Comma seperation</param>
    /// <param name="precision_">Precision - significant figures after decimal point.</param>
    /// <param name="unitType_">Unit Type e.g. Percentage, Thousand etc.</param>
    /// <returns></returns>
    public string GenerateFormatString(bool commas_, PrecisionType precision_, UnitType unitType_)
    {
      if (m_dataType == typeof(DateTime))
        return null;

      if (m_dataType == typeof(double) && precision_ == PrecisionType.Default)
      {
        //NOTE: Format string "d" or "D" is supported for integral types only.
        return String.Empty;
      }

      if (precision_ == PrecisionType.Default)
        return FORMAT_EXCEL;

      int precision = Convert.ToInt32(precision_);

      StringBuilder formatStringBuilder = new StringBuilder(20);

      switch (unitType_)
      {
        case (UnitType.One):
          formatStringBuilder.Append((commas_) ? "#,##0" : "###0");
          break;
        case (UnitType.Thousand):
          formatStringBuilder.Append((commas_) ? "#,##0," : "###0,");
          break;
        case (UnitType.Million):
          formatStringBuilder.Append((commas_) ? "#,##0,," : "###0,,");
          break;
        case (UnitType.Percentage):
          return "p" + precision;
        case (UnitType.Hundredth):
          break;
        default:
          return "";
      }

      // Add precision to format string
      if (precision != 0)
      {
        formatStringBuilder.Append(".");
        for (int i = 0; i < precision; ++i)
        {
          formatStringBuilder.Append("0");
        }
      }

      //int sections = m_originalFormatSections.Length;


      string positiveFormatString = formatStringBuilder.ToString();
      formatStringBuilder.Append(";");
      if (PARENTHESIZE_NEGATIVE_NUMBERS)
      {
        formatStringBuilder.Append("(" + positiveFormatString + ")");
      }
      else
      {
        formatStringBuilder.Append("-" + positiveFormatString);
      }


      return formatStringBuilder.ToString();
    }

    #endregion
  }
}

