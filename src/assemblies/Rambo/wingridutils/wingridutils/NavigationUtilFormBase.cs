using System;
using System.Drawing;
using System.Collections;
using System.Xml.Serialization;
using System.ComponentModel;
using System.Windows.Forms;
using MorganStanley.MSDesktop.Rambo.WinGridUtils;
using Infragistics.Win.UltraWinGrid;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
	/// <summary>
	/// This class acts as a base class for ctrlViewBase Navigational forms, initially Find and 
	/// Go To Column.  Most noteabley, it contains a reference to a UltraGrid object to Navigate
	/// and is a FixedToolWindow.
	/// </summary>
	public class NavigationUtilFormBase : System.Windows.Forms.Form
	{
    #region Events
    public event EventHandler BeforeNavigate
    {
      add
      {
        _beforeNavigateHandler += value;
      }
      remove
      {
        _beforeNavigateHandler -= value;
      }
    }
    public event EventHandler AfterNavigate
    {
      add
      {
        _afterNavigateHandler += value;
      }
      remove
      {
        _afterNavigateHandler -= value;
      }
    }
    #endregion

    #region Data Members

    /// <summary>
    /// Object to navigate through
    /// </summary>
    protected UltraGrid _grid = null;

    /// <summary>
    ///  XML Serialzier
    /// </summary>
    protected XmlSerializer _xmlSerializer = null;

    protected EventHandler _beforeNavigateHandler;
    protected EventHandler _afterNavigateHandler;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

    #endregion

    #region Constructor
		public NavigationUtilFormBase()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}
    #endregion

    #region IDisposable
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
    #endregion

    #region Public Methods
    /// <summary>
    /// Brings the form to the front and makes it visible
    /// </summary>
    public virtual void ShowForm()
    {
      this.Show();
      this.BringToFront();
    }

    /// <summary>
    /// Sets the UltraGrid to navigate
    /// </summary>
    /// <param name="grid_"></param>
    public virtual void ResetGrid(UltraGrid grid_)
    {
      if(_grid != null)
      {
        _grid.AfterRowActivate -= new EventHandler(UpdateComponents);
      }

      _grid = grid_;

      if(_grid != null)
      {
        _grid.AfterRowActivate += new EventHandler(UpdateComponents);
      }
    }
    #endregion

    #region Properties

    public UltraGrid Grid
    {
      get { return _grid; }
      set { ResetGrid(value); }
    }

    public virtual object State
    {
      get{ return null;}
      set{ ; }
    }

    public virtual XmlSerializer XmlSerializer
    {
      get { return _xmlSerializer; }
    }

    /// <summary>
    /// Returns the list of column captions in the active row of the grid 
    /// </summary>
    protected string[] ColumnCaptions
    {
      get
      {
        if(_grid == null)
          return null;

        return getColumnCaptions(_grid.ActiveRow);
      }
    }

    /// <summary>
    /// Returns the list of column keys in the active row of the grid
    /// </summary>
    protected string[] ColumnKeys
    {
      get
      {
        if(_grid == null)
          return null;

        return getColumnKeys(_grid.ActiveRow);
      }
    }
    #endregion

    #region Utilities

    /// <summary>
    /// Highlights and scrolls to the active cell in the grid
    /// </summary>
    protected void highlightActiveCell()
    {
      if(_grid == null)
        return;


      if(_grid.ActiveCell == null)
        return;

      _grid.Selected.Cells.Clear();
      _grid.Selected.Rows.Clear();

      _grid.PerformAction(UltraGridAction.ActivateCell);
      _grid.PerformAction(UltraGridAction.ToggleCellSel);

      _grid.ActiveColScrollRegion.ScrollCellIntoView(_grid.ActiveCell, 
        _grid.ActiveRowScrollRegion,true);
    }

    /// <summary>
    /// Returns the columns for each non-hidden cell in row_ in the order they appear in the grid
    /// </summary>
    /// <param name="row_"></param>
    /// <returns></returns>
    private UltraGridColumn[] getColumns(UltraGridRow row_)
    {
      ColumnsCollection colsCollect = row_.Band.Columns;
      //UltraGridColumn[] cols = new UltraGridColumn[colsCollect.Count];
      ArrayList cols = new ArrayList(colsCollect.Count);
      UltraGridColumn[] returnVal;

      for(int i = 0; i < colsCollect.Count; ++i)
      {
        if(colsCollect[i].Hidden)
          continue;

        cols.Add(colsCollect[i]);
      }

      cols.Sort(new VisiblePositionComparer());
      returnVal = (UltraGridColumn[])cols.ToArray(typeof(UltraGridColumn));

      return returnVal;
    }

    private class VisiblePositionComparer : IComparer
    {
      public int Compare(object x, object y)
      {
        int visx, visy;

        visx = ((UltraGridColumn)x).Header.VisiblePositionWithinBand;
        visy = ((UltraGridColumn)y).Header.VisiblePositionWithinBand;

        return visx - visy;
      }
    }

    /// <summary>
    /// Returns the Column Captions for each non-hidden cell in row_ in the order they appear in the grid
    /// </summary>
    /// <param name="row_"></param>
    /// <returns></returns>
    protected string[] getColumnCaptions(UltraGridRow row_)
    {
      if(row_ == null)
        return null;

      UltraGridColumn[] cols = getColumns(row_);
      

      string[] names = new string[cols.Length];

      for(int i = 0; i < cols.Length; ++i)
      {
        names[i] = cols[i].Header.Caption;
      }

      return names;

    }

    /// <summary>
    /// Returns the column keys for each non-hidden column in row_ in the order they appear in the grid
    /// </summary>
    /// <param name="row_"></param>
    /// <returns></returns>
    protected string[] getColumnKeys(UltraGridRow row_)
    {
      if(row_ == null)
        return null;
      
      UltraGridColumn[] cols = getColumns(row_);
      

      string[] names = new string[cols.Length];

      for(int i = 0; i < cols.Length; ++i)
      {
        names[i] = cols[i].Key;
      }

      return names;

    }

    protected string GetColumnKey(string columnCaption_)
    { 
      int index = -1;
      string[] columnKeys = this.ColumnKeys;
      
      if(columnKeys != null)
        index = Array.IndexOf(ColumnCaptions, columnCaption_);
      
      if(index < 0)
        return null;
      else
        return columnKeys[index];
    }

    protected string GetCaption(string columnKey_)
    {
      if(_grid == null || _grid.DisplayLayout.Bands.Count == 0)
        return null;

      UltraGridRow row = _grid.ActiveRow;

      if(row == null)
      {
        row = _grid.ActiveCell == null ? null : _grid.ActiveCell.Row;
      }

      UltraGridBand band;

      if(row == null)
      {
        band = _grid.DisplayLayout.Bands[0];
      }
      else
      {
        band = row.Band;
      }

      UltraGridColumn column = band.Columns.Exists(columnKey_) ? band.Columns[columnKey_] : null;

      return column == null ? null : column.Header.Caption;
    }

    #endregion

    #region Event Handlers
    /// <summary>
    /// Cancels the closing of the form proper and hides it from view
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected virtual void CloseForm(object sender, System.ComponentModel.CancelEventArgs e)
    {
      e.Cancel = true;
      this.Hide();
    }

    /// <summary>
    /// Updates the components that need to be changed when the grid object changes
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected virtual void UpdateComponents(object sender, System.EventArgs e)
    {
    }
    #endregion

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
      // 
      // NavigationUtilFormBase
      // 
      this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
      this.ClientSize = new System.Drawing.Size(292, 266);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
      this.MinimizeBox = false;
      this.Name = "NavigationUtilFormBase";
      this.ShowInTaskbar = false;
      this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
      this.Text = "NavigationUtilFormBase";
      this.TopMost = false;
      this.Closing += new System.ComponentModel.CancelEventHandler(this.CloseForm);

    }
		#endregion
	}
}
