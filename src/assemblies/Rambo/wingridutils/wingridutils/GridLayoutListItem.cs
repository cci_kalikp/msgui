
using System.ComponentModel;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  public class GridLayoutListItem
  {
    protected GridLayout m_layout;
    protected bool m_hasChanges;
    protected bool m_isLoaded;
    protected bool m_isNew;


    public GridLayoutListItem(GridLayout layout_)
    {
      m_hasChanges = false;
      m_isLoaded = false;
      m_isNew = false;
      m_layout = layout_;
    }

    [Browsable(false)]
    public bool IsNew
    {
      get { return m_isNew;  }
      set { m_isNew = value; }
    }

    [Browsable(false)]
    public bool IsLoaded
    {
      get { return m_isLoaded; }
      set { m_isLoaded = value; }
    }

    [Browsable(false)]
    public bool HasChanges
    {
      get { return m_hasChanges; }
      set { m_hasChanges = value; }
    }

    public string DisplayName
    {
      get { return m_layout.LayoutName; }
    }

    [Browsable(false)]
    public GridLayout GridLayout
    {
      get { return m_layout;  }
      set { m_layout = value; }
    }

    public override string ToString()
    {
      return DisplayName;
    }
	
  }
}
