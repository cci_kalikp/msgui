//---------------------------------------------------------------------------
//
// Copyright (c) 2004 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
//---------------------------------------------------------------------------

using System;
using System.Xml;
using System.Xml.Serialization;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  /// <summary>
  /// XML representation of a GUI Data Grid column.
  /// </summary>
  [XmlRootAttribute("ColumnLayout"), DefaultProperty("Caption")]
  public class ColumnLayout : ICloneable
  {
    protected const string CATEGORY_FORMAT = "Format";
    protected const string CATEGORY_COLUMN = "Column";

    protected int _width;
    protected bool _hidden;
    protected int _index;
    protected string _format;
    protected string _caption;
    protected string _key;
    protected string _dataType;
    protected SortDirections _sortDirection;
    protected int _sortOrder;
    protected bool _sortAbsolute;
    protected string _description;
    protected int _backColourValue;
    protected int _foreColourValue;
    protected bool _isReadOnly;
    protected string _headerTag;
    protected Font theFont = null;
    protected string _columnCategory;
    protected string _category;
    protected int _categoryOrder;

    [XmlElement, Browsable(false)]
    public int FieldCategoryOrder
    {
      get { return _categoryOrder; }
      set { _categoryOrder = value; }
    }

    [XmlElement, Browsable(false)]
    public string FieldCategory
    {
      get { return (_columnCategory != null && _columnCategory.Length > 0) ? _columnCategory : "(No category available)"; }
      set { _columnCategory = value; }
    }

    [XmlElement, Browsable(false)]
    public int Width
    {
      get { return _width; }
      set { _width = value; }
    }

    [XmlElement, Browsable(false)]
    public bool Hidden
    {
      get { return _hidden; }
      set { _hidden = value; }
    }

    [XmlElement, Browsable(false)]
    public int Index
    {
      get { return _index; }
      set { _index = value; }
    }

    [XmlElement, Browsable(false)]
    public string Category
    {
      get { return _category; }
      set { _category = value; }
    }

    [XmlElement, Browsable(false)]
    public string Format
    {
      get
      {
        if ((_format == null || _format.Equals("")) && DataType.Equals("System.Double"))
        {
          ColumnFormat tmp = new ColumnFormat(_format, Type.GetType(DataType));
          _format = tmp.GenerateFormatString(false,
            ColumnFormat.PrecisionType.Default, ColumnFormat.UnitType.One);
        }
        return _format;
      }
      set
      {
        _format = value;
      }
    }

    [XmlElement, Category(CATEGORY_COLUMN), ReadOnly(true)]
    public string Caption
    {
      get { return _caption; }
      set { _caption = value; }
    }

    [XmlElement, Category(CATEGORY_COLUMN), ReadOnly(true)]
    public string Key
    {
      get { return _key; }
      set { _key = value; }
    }

    [XmlElement, Browsable(false)]
    public string DataType
    {
      get { return _dataType; }
      set { _dataType = value; }
    }

    [XmlElement, Browsable(false)]
    public SortDirections SortDirection
    {
      get { return _sortDirection; }
      set { _sortDirection = value; }
    }

    [XmlElement, Browsable(false)]
    public bool SortAbsolute
    {
      get { return _sortAbsolute; }
      set { _sortAbsolute = value; }
    }

    [XmlElement, Browsable(false)]
    public int SortOrder
    {
      get { return _sortOrder; }
      set { _sortOrder = value; }
    }

    [XmlIgnore, Browsable(false)]
    public string Description
    {
      get
      {
        return (_description != null && _description.Length > 0) ? _description : "(No description available)";
      }
      set { _description = value; }
    }

    [XmlElement, Browsable(false)]
    public int BackColorValue
    {
      get { return _backColourValue; }
      set { _backColourValue = value; }
    }

    [XmlElement, Browsable(false)]
    public int ForeColorValue
    {
      get { return _foreColourValue; }
      set { _foreColourValue = value; }
    }

    [XmlElement, Browsable(false)]
    public bool ReadOnly
    {
      get { return _isReadOnly; }
      set { _isReadOnly = value; }
    }

    [XmlElement, Browsable(false)]
    public string HeaderTag
    {
      get { return _headerTag; }
      set { _headerTag = value; }
    }

    public enum SortDirections : int
    {
      None = 0, Ascending = 1, Descending = 2
    }

    [XmlIgnore, CategoryAttribute(CATEGORY_FORMAT)]
    public bool Commas
    {
      get { return GetFormatObj().HasCommas(); }
      set
      {
        ColumnFormat tmp = GetFormatObj();
        Format = tmp.GenerateFormatString(value, tmp.GetPrecision(), tmp.GetUnits());
      }
    }

    [XmlIgnore, CategoryAttribute(CATEGORY_FORMAT)]
    public ColumnFormat.UnitType Units
    {
      get { return GetFormatObj().GetUnits(); }
      set
      {
        ColumnFormat tmp = GetFormatObj();
        Format =
          tmp.GenerateFormatString(tmp.HasCommas(), tmp.GetPrecision(), value);
      }
    }

    [XmlIgnore, CategoryAttribute(CATEGORY_FORMAT)]
    public ColumnFormat.PrecisionType Precision
    {
      get { return GetFormatObj().GetPrecision(); }
      set
      {
        ColumnFormat tmp = GetFormatObj();
        Format =
          tmp.GenerateFormatString(tmp.HasCommas(), value, tmp.GetUnits());
      }
    }

    [XmlIgnore, CategoryAttribute(CATEGORY_FORMAT)]
    public Color BackColor
    {
      get { return Color.FromArgb(BackColorValue); }
      set { BackColorValue = value.ToArgb(); }
    }

    [XmlIgnore, CategoryAttribute(CATEGORY_FORMAT)]
    public Color ForeColor
    {
      get { return Color.FromArgb(ForeColorValue); }
      set { ForeColorValue = value.ToArgb(); }
    }

    public void ResetForeColor()
    {
      ForeColorValue = 0;
    }

    public void ResetBackColor()
    {
      BackColorValue = 0;
    }

    #region Font Features
    [XmlIgnore, CategoryAttribute(CATEGORY_FORMAT)]
    public Font Font
    {
      get { return theFont; }
      set { this.theFont = value; }
    }

    public void ResetFont()
    {
      theFont = null;
    }

    [XmlElement, Browsable(false), CategoryAttribute(CATEGORY_FORMAT)]
    public string FontName
    {
      get
      {
        return TypeDescriptor.GetConverter(typeof(Font)).ConvertToString(Font);
      }
      set
      {
        if (value != "(none)")
        {
          Font = (Font)(TypeDescriptor.GetConverter(typeof(Font)).ConvertFromString(value));
        }
        else
        {
          Font = null;
        }
      }
    }


    #endregion




    public override bool Equals(object obj_)
    {
      if (!(obj_ is ColumnLayout))
      {
        return false;
      }

      ColumnLayout colLayout = obj_ as ColumnLayout;
      return (colLayout.Key == this.Key);
    }

    public override int GetHashCode()
    {
      return this.Key.GetHashCode();
    }

    public object Clone()
    {

      return this.MemberwiseClone();
    }

    // Returns a format ColumnFormat object constructed from the current Format string.
    private ColumnFormat GetFormatObj()
    {
      return new ColumnFormat(Format, Type.GetType(DataType));
    }

    private int GetAdjustedSortIndex(int index_)
    {
      return (SortDirection == SortDirections.None) ? -1 : index_;
    }

    public override string ToString()
    {
      return "ColumnLayout- Key:" + Key;
    }
  }
}