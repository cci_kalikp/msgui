using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using MorganStanley.Ecdev.Desktop.Utils;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;

namespace MorganStanley.Ecdev.WinGridUtils
{
  public enum FormatUnits : int { NONE, UNIT = 1, PERCENTAGE = 100, THOUSANDS = 1000, MILLIONS = 1000000 }

	/// <summary>
	/// Summary description for CntrlFormat.
	/// </summary>
	public class CntrlFormat : System.Windows.Forms.UserControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
    private Infragistics.Win.UltraWinGrid.UltraGrid _grid;
    private UltraGridBand _band; 
    private FormatSpecCollection _collection;
    private ILogger _log = LoggerManager.GetLogger( typeof(CntrlFormat) );   

    private const string NOT_APPLICABLE = "n/a";
    private const string YES = "Yes";
    private const string NO = "No";

    private const string UNIT_SINGLE = "1";
    private const string UNIT_PERCENT = "%";

		private CntrlFormat()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitForm call
		}
          
    public CntrlFormat( UltraGridBand band_ ):this()
    {
      _band = band_;
    }

    protected override void OnLoad( EventArgs e_ )
    {     
      _collection = new FormatSpecCollection();

      for ( int i = 0; i < _band.Columns.Count; i++ )
      {
        UltraGridColumn col = _band.Columns[i];
        FormatSpec fs = new FormatSpec();
        //col.Format
        fs.Caption = col.Header.Caption;
        fs.Key = col.Key;
        fs.DataType = col.DataType;
        fs.Width = col.Width;
                       
        //precision
        if ( col.DataType == typeof(double) )
        {         
          double val = Math.PI;
          string valString = val.ToString( col.Format, col.FormatInfo );

          int index = valString.LastIndexOf(".");

          bool hasPercent = ( valString.LastIndexOf("%") > -1 );       
          int minus = hasPercent ? 2 : 1;
        
          fs.Precision = ( index != -1 ) ? valString.Length - index - minus : 0;         
        }  
      
        //comma, percent
        if ( col.DataType == typeof(double) || col.DataType == typeof(int) )
        { 
          double val = 10000000000;
          string valString = val.ToString( col.Format, col.FormatInfo );
          
          fs.Comma = (valString.IndexOf(",") > -1) ? YES : NO;        

          fs.Units = (valString.IndexOf("%") > -1) ? UNIT_PERCENT : UNIT_SINGLE;
        }     
        
    
        _collection.Add( fs );
      }
      _grid.DataSource = _collection;
    }

    public void AcceptChanges()
    {
      if ( _grid.DataSource == null )
      {
        return;
      }

      if ( _grid.ActiveCell != null && _grid.ActiveCell.IsInEditMode )
      {
        _grid.ActiveCell.Row.Update();
      }

      foreach( FormatSpec fs in _collection )
      {
        UltraGridColumn col = _band.Columns[fs.Key];
        col.Width = fs.Width;      

        if ( col.DataType == typeof(double) || col.DataType == typeof(int) )
        { 
          string formatString = GetFormatString( fs );
          col.Format = formatString;
        }   
      }
    }

    protected string GetFormatString( FormatSpec fs_ )
    {
      System.Text.StringBuilder sb = new System.Text.StringBuilder("#");      
      if ( fs_.Comma == YES )
      {
        sb.Append(",");
      }      
      sb.Append("#");

      if ( fs_.Precision > 0 )
      {
        sb.Append("#0.");
        for ( int i = 1; i <= fs_.Precision; i++ )
        {
          sb.Append("0");
        }
      }

      if ( fs_.Units == UNIT_PERCENT )
      {
        sb.Append("%");
      }

      sb.Append(";;0");
      return sb.ToString();
    }

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

    #region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
      this._grid = new Infragistics.Win.UltraWinGrid.UltraGrid();
      ((System.ComponentModel.ISupportInitialize)(this._grid)).BeginInit();
      this.SuspendLayout();
      // 
      // _grid
      // 
      this._grid.Dock = System.Windows.Forms.DockStyle.Fill;
      this._grid.Name = "_grid";
      this._grid.Size = new System.Drawing.Size(528, 368);
      this._grid.TabIndex = 0;
      this._grid.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this._grid_InitializeRow);
      this._grid.AfterEnterEditMode += new System.EventHandler(this._grid_AfterEnterEditMode);
      this._grid.KeyDown += new System.Windows.Forms.KeyEventHandler(this._grid_KeyDown);
      this._grid.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this._grid_AfterCellUpdate);
      this._grid.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this._grid_InitializeLayout);
      // 
      // CntrlFormat
      // 
      this.Controls.AddRange(new System.Windows.Forms.Control[] {
                                                                  this._grid});
      this.Name = "CntrlFormat";
      this.Size = new System.Drawing.Size(528, 368);
      ((System.ComponentModel.ISupportInitialize)(this._grid)).EndInit();
      this.ResumeLayout(false);

    }
		#endregion

    private void _grid_InitializeLayout( object sender_, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e_ )
    {
      WinGridUtil.InitializeLayout( e_ );
      WinGridUtil.AutoResizeColumns( e_.Layout.Bands[0].Columns );
     
      e_.Layout.Override.AllowColMoving = AllowColMoving.NotAllowed;   
      e_.Layout.Bands[0].Columns[FormatSpec.PROPERTY_CAPTION].Header.Caption = "Column";     

      //column ordering
      string[] columnsInOrder = new String[] {                     
                                                FormatSpec.PROPERTY_CAPTION,
                                                FormatSpec.PROPERTY_WIDTH, 
                                                FormatSpec.PROPERTY_UNITS,
                                                FormatSpec.PROPERTY_PRECISION,
                                                FormatSpec.PROPERTY_COMMA                                    
                                             };

      for ( int i = 0; i < columnsInOrder.Length; i++ )
      {            
        e_.Layout.Bands[0].Columns[columnsInOrder[i]].Header.VisiblePosition = i;      
      }

      e_.Layout.Bands[0].Columns[FormatSpec.PROPERTY_PRECISION].Format = "#,##0;'n/a';0"; 
   
      e_.Layout.Bands[0].Columns[FormatSpec.PROPERTY_COMMA].Style = ColumnStyle.DropDownList;
       e_.Layout.Bands[0].Columns[FormatSpec.PROPERTY_UNITS].Style = ColumnStyle.DropDownList;

      ValueList commaList = e_.Layout.ValueLists.Add("Comma");    
      commaList.ValueListItems.Add( NO );
      commaList.ValueListItems.Add( YES );
      e_.Layout.Bands[0].Columns[FormatSpec.PROPERTY_COMMA].ValueList = commaList; 

      ValueList unitList = e_.Layout.ValueLists.Add("Units");
      unitList.ValueListItems.Add( UNIT_SINGLE );
      unitList.ValueListItems.Add( UNIT_PERCENT );
      e_.Layout.Bands[0].Columns[FormatSpec.PROPERTY_UNITS].ValueList = unitList; 

    
      //column colouring
      PropertyDescriptorCollection properties = TypeDescriptor.GetProperties( typeof(FormatSpec) );
      foreach ( PropertyDescriptor pd in properties )
      { 
        if ( pd.IsReadOnly && pd.IsBrowsable )
        {
          e_.Layout.Bands[0].Columns[pd.Name].CellAppearance.BackColor = Color.Wheat;   
          e_.Layout.Bands[0].Columns[pd.Name].CellActivation = Activation.NoEdit;
        } 
      }           
    }

    private void _grid_InitializeRow( object sender_, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e_ )
    {
      FormatSpec fs = (FormatSpec)e_.Row.ListObject;    
      e_.Row.Cells[FormatSpec.PROPERTY_PRECISION].Activation = fs.Precision < 0 ? Activation.NoEdit : Activation.AllowEdit;   
      e_.Row.Cells[FormatSpec.PROPERTY_COMMA].Activation = (fs.Comma == NOT_APPLICABLE) ? Activation.NoEdit : Activation.AllowEdit;
      e_.Row.Cells[FormatSpec.PROPERTY_UNITS].Activation = (fs.Units == NOT_APPLICABLE) ? Activation.NoEdit : Activation.AllowEdit;

      for ( int i = 0; i < e_.Row.Cells.Count; i++ )
      {
        UltraGridCell cell = e_.Row.Cells[i];
        if ( cell.Activation == Activation.NoEdit )
        {
          cell.Appearance.BackColor = Color.Wheat;
        }
        else
        {
          cell.Appearance.ResetBackColor();
        }
      }
    }

    private void _grid_AfterCellUpdate(object sender_, Infragistics.Win.UltraWinGrid.CellEventArgs e_)
    {
      e_.Cell.Row.Update();
    }

    private void _grid_KeyDown( object sender_, System.Windows.Forms.KeyEventArgs e_ )
    {
      WinGridUtil.OnKeyDown( sender_, e_ );
    }

    private void _grid_AfterEnterEditMode( object sender_, System.EventArgs e_ )
    {
//      WinGridUtil.SelectAllTextInEditor( _grid.ActiveCell );
    }

    #region FormatSpec class
    protected class FormatSpec
    {      
      public const string PROPERTY_CAPTION = "Caption";
      public const string PROPERTY_WIDTH = "Width";
      public const string PROPERTY_PRECISION = "Precision";
      public const string PROPERTY_COMMA = "Comma";
      public const string PROPERTY_UNITS = "Units";

      private string _key;
      public string _caption;
      private int _width;   
      private int _precision = -1;
      private string _comma = NOT_APPLICABLE;
      private string _units = NOT_APPLICABLE;
      private Type _type;
   
      [Browsable(false)]
      public string Key
      {
        get{ return _key; }
        set{ _key = value; }
      }

      [Browsable(false)]
      public Type DataType
      { 
        get{ return _type; }
        set{ _type = value; }
      } 

      public string Units
      {
        get{ return _units; }
        set{ _units = value; }
      }
      [ReadOnly(true)]
      public string Caption
      {
        get{ return _caption;}
        set{ _caption = value;}
      }

      public int Width
      {
        get{ return _width; }
        set{ _width = value; }
      }
         
      public int Precision
      {
        get{ return _precision; }
        set{ _precision = value; }
      }

      public string Comma
      {
        get { return _comma; }
        set{ _comma = value; }
      }
    }
    #endregion

    #region FormatSpecCollection class

    protected class FormatSpecCollection: CollectionBase, ITypedList, IListSource, IBindingList
    {  
    #region constructors
      /// <summary>
      /// Initializes a new empty instance of the FormatSpecCollection class.
      /// </summary>
      public FormatSpecCollection()
      {
      }

  
      /// <summary>
      /// Initializes a new instance of the FormatSpecCollection class, containing elements
      /// copied from an array.
      /// </summary>
      /// <param name="items">
      /// The array whose elements are to be added to the new FormatSpecCollection.
      /// </param>
      public FormatSpecCollection(FormatSpec[] items)
      {
        this.AddRange(items);
      }

      /// <summary>
      /// Initializes a new instance of the FormatSpecCollection class, containing elements
      /// copied from another instance of FormatSpecCollection
      /// </summary>
      /// <param name="items">
      /// The FormatSpecCollection whose elements are to be added to the new FormatSpecCollection.
      /// </param>
      public FormatSpecCollection(FormatSpecCollection items)
      {
        this.AddRange(items);
      }
    #endregion 
       
    #region CollectionBase members

      /// <summary>
      /// Adds the elements of an array to the end of this FormatSpecCollection.
      /// </summary>
      /// <param name="items">
      /// The array whose elements are to be added to the end of this FormatSpecCollection.
      /// </param>
      public virtual void AddRange(FormatSpec[] items)
      {
        foreach (FormatSpec item in items)
        {
          this.List.Add(item);
        }
      }

      /// <summary>
      /// Adds the elements of another FormatSpecCollection to the end of this FormatSpecCollection.
      /// </summary>
      /// <param name="items">
      /// The FormatSpecCollection whose elements are to be added to the end of this FormatSpecCollection.
      /// </param>
      public virtual void AddRange(FormatSpecCollection items)
      {
        foreach (FormatSpec item in items)
        {
          this.List.Add(item);
        }
      }

      /// <summary>
      /// Adds an instance of type FormatSpec to the end of this FormatSpecCollection.
      /// </summary>
      /// <param name="value">
      /// The FormatSpec to be added to the end of this FormatSpecCollection.
      /// </param>
      public virtual void Add(FormatSpec value)
      {
        this.List.Add(value);         
      }

      /// <summary>
      /// Determines whether a specfic FormatSpec value is in this FormatSpecCollection.
      /// </summary>
      /// <param name="value">
      /// The FormatSpec value to locate in this FormatSpecCollection.
      /// </param>
      /// <returns>
      /// true if value is found in this FormatSpecCollection;
      /// false otherwise.
      /// </returns>
      public virtual bool Contains(FormatSpec value)
      {
        return this.List.Contains(value);
      }

      /// <summary>
      /// Return the zero-based index of the first occurrence of a specific value
      /// in this FormatSpecCollection
      /// </summary>
      /// <param name="value">
      /// The FormatSpec value to locate in the FormatSpecCollection.
      /// </param>
      /// <returns>
      /// The zero-based index of the first occurrence of the _ELEMENT value if found;
      /// -1 otherwise.
      /// </returns>
      public virtual int IndexOf(FormatSpec value)
      {
        return this.List.IndexOf(value);
      }

      /// <summary>
      /// Inserts an element into the FormatSpecCollection at the specified index
      /// </summary>
      /// <param name="index">
      /// The index at which the FormatSpec is to be inserted.
      /// </param>
      /// <param name="value">
      /// The FormatSpec to insert.
      /// </param>
      public virtual void Insert(int index, FormatSpec value)
      {
        this.List.Insert(index, value);
      }

      /// <summary>
      /// Gets or sets the FormatSpec at the given index in this FormatSpecCollection.
      /// </summary>
      public virtual FormatSpec this[int index]
      {
        get
        {
          return (FormatSpec) this.List[index];
        }
        set
        {
          this.List[index] = value;
        }
      }

      /// <summary>
      /// Removes the first occurrence of a specific FormatSpec from this FormatSpecCollection.
      /// </summary>
      /// <param name="value">
      /// The FormatSpec value to remove from this FormatSpecCollection.
      /// </param>
      public virtual void Remove(FormatSpec value)
      {
        this.List.Remove(value);    
      }

      /// <summary>
      /// Type-specific enumeration class, used by FormatSpecCollection.GetEnumerator.
      /// </summary>
      public class Enumerator: System.Collections.IEnumerator
      {
        private System.Collections.IEnumerator wrapped;

        public Enumerator(FormatSpecCollection collection)
        {
          this.wrapped = ((System.Collections.CollectionBase)collection).GetEnumerator();
        }

        public FormatSpec Current
        {
          get
          {
            return (FormatSpec) (this.wrapped.Current);
          }
        }

        object System.Collections.IEnumerator.Current
        {
          get
          {
            return (FormatSpec) (this.wrapped.Current);
          }
        }

        public bool MoveNext()
        {
          return this.wrapped.MoveNext();
        }

        public void Reset()
        {
          this.wrapped.Reset();
        }
      }

      /// <summary>
      /// Returns an enumerator that can iteFormatSpec through the elements of this FormatSpecCollection.
      /// </summary>
      /// <returns>
      /// An object that implements System.Collections.IEnumerator.
      /// </returns>        
      public new virtual FormatSpecCollection.Enumerator GetEnumerator()
      {
        return new FormatSpecCollection.Enumerator(this);
      }
    #endregion

    #region CollectionBase Notification Overrides

      protected override void OnClearComplete()
      {    
        if (ListChanged != null)
        {
          ListChangedEventArgs args = new ListChangedEventArgs (ListChangedType.Reset, -1, -1);
          ListChanged (this, args);
        }      
      }


      protected override void OnInsertComplete(int index_, object value_ )
      {
        if (ListChanged != null)
        {
          ListChangedEventArgs args = new ListChangedEventArgs (ListChangedType.ItemAdded, index_, -1);
          ListChanged (this, args);
        }     
      }

      protected override void OnRemoveComplete( int index_, object value_ )
      {       
        if (ListChanged != null)
        {
          ListChangedEventArgs args = new ListChangedEventArgs (ListChangedType.ItemDeleted, index_, -1);
          ListChanged (this, args);
        }           
      }


      protected override void OnSetComplete( int index_, object oldValue_, object newValue_ )
      {
        if ( ListChanged != null )
        {     
          ListChangedEventArgs args = new ListChangedEventArgs( ListChangedType.ItemChanged, index_, -1 );
          ListChanged( this, args );           
        }
      }

    #endregion

    #region ITypedList Members
      public PropertyDescriptorCollection GetItemProperties( PropertyDescriptor[] pd_ )
      {     
        return TypeDescriptor.GetProperties( typeof(FormatSpec) );
      }

      public string GetListName( PropertyDescriptor[] pd_ )
      {
        return "FormatSpecCollection";
      }
    #endregion

    #region IListSource Members
      public IList GetList()
      {
        return this;
      }

      public bool ContainsListCollection
      {
        get
        {
          return false;
        }
      }
    #endregion
    
    #region IBindList Members

      public event System.ComponentModel.ListChangedEventHandler ListChanged;

      public object AddNew()
      {
        throw new NotSupportedException();
      }
   
      public bool AllowEdit
      {
        get
        {
          return true;
        }
      }

      public bool AllowNew
      {
        get
        {
          return false;
        }
      }

      public bool AllowRemove
      {
        get
        {
          return true;
        }
      }

      public bool SupportsChangeNotification
      {
        get
        {
          return true;
        }
      }

      public bool SupportsSearching
      {
        get
        {
          return false;
        }
      }

      public bool SupportsSorting
      {
        get
        {
          return false;
        }
      }

      public bool IsSorted
      {
        get
        {
          return false;
        }
      }

      public void ApplySort(PropertyDescriptor property, System.ComponentModel.ListSortDirection direction)
      {      
        throw new NotSupportedException();
      }
    
      public PropertyDescriptor SortProperty
      {
        get
        {
          return null;
        }
      }
    
      public int Find(PropertyDescriptor property, object key)
      {      
        throw new NotSupportedException();   
      }

      public void AddIndex(PropertyDescriptor property)
      {      
        throw new NotSupportedException();
      }
    
      public ListSortDirection SortDirection
      {
        get
        {        
          return new ListSortDirection ();
        }
      }

      public void RemoveSort()
      { 
        throw new NotSupportedException();
      }

      public void RemoveIndex(PropertyDescriptor property)
      {      
        throw new NotSupportedException();
      }
    #endregion
    }


    #endregion
	}
}
