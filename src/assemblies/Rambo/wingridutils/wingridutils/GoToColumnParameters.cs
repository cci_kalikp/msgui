using System.Xml.Serialization;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  /// <summary>
  /// For storage of the state of search parameters on the Go To Column form
  /// </summary>
  [XmlRoot("GoToColumn")]
  internal class GoToColumnParameters
  {
    public enum ColumnSortType
    {
      ALPHABETIC,
      GRID_ORDER
    } ;

    [XmlElement("Column", typeof(string))]
    public string Column = string.Empty;

    [XmlElement("SortOrder", typeof(ColumnSortType))]
    public ColumnSortType SortOrder = ColumnSortType.ALPHABETIC;

    public override bool Equals(object obj)
    {
      if (obj == null)
        return false;

      if (GetType() != obj.GetType())
        return false;

      GoToColumnParameters that = (GoToColumnParameters)obj;

      return Column == that.Column && SortOrder == that.SortOrder;
    }

    public override int GetHashCode()
    {
      return (Column.GetHashCode() ^ SortOrder.GetHashCode());
    }
  }
}