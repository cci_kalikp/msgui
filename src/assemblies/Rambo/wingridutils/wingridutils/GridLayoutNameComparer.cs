﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  /// <summary>
  /// Class to perform comparison operations on two GridLayout objects. Used to sort a collection
  /// of GridLayout objects in alphabetical order.
  /// </summary>
  public class GridLayoutNameComparer : IComparer
  {
    public int Compare(object x_, object y_)
    {
      GridLayout layout1 = (GridLayout)x_;
      GridLayout layout2 = (GridLayout)y_;

      return (layout1.LayoutName.ToUpper().CompareTo(layout2.LayoutName.ToUpper())) < 0 ? -1 : 1;
    }
  }
}
