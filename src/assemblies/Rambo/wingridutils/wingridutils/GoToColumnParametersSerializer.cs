namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  using System.Xml.Serialization;
  using System;
    
    
  /// <summary>Custom serializer for GoToColumnParameters type.</summary>
  internal class GoToColumnParametersSerializer : System.Xml.Serialization.XmlSerializer
  {
        
        
    GoToColumnReader _reader;
    GoToColumnWriter _writer;

    /// <summary>Constructs the serializer with default reader and writer instances.</summary>
    public GoToColumnParametersSerializer()
    {
    }

    /// <summary>Constructs the serializer with a pre-built reader.</summary>
    public GoToColumnParametersSerializer(GoToColumnReader reader)
    {
      _reader = reader;
    }

    /// <summary>Constructs the serializer with a pre-writer reader.</summary>
    public GoToColumnParametersSerializer(GoToColumnWriter writer)
    {
      _writer = writer;
    }

    /// <summary>Constructs the serializer with pre-built reader and writer.</summary>
    public GoToColumnParametersSerializer(GoToColumnReader reader, GoToColumnWriter writer)
    {
      _reader = reader;
      _writer = writer;
    }

    /// <summary>See <see cref="XmlSerializer.CreateReader"/>.</summary>
    protected override XmlSerializationReader CreateReader()
    {
      if (_reader != null) 
        return _reader;
      else
        return new GoToColumnReader();
    }

    /// <summary>See <see cref="XmlSerializer.CreateWriter"/>.</summary>
    protected override XmlSerializationWriter CreateWriter()
    {
      if (_writer != null) 
        return _writer;
      else
        return new GoToColumnWriter();
    }

    /// <summary>See <see cref="XmlSerializer.Deserialize"/>.</summary>
    protected override object Deserialize(XmlSerializationReader reader)
    {
      if (!(reader is GoToColumnReader))
        throw new ArgumentException("reader");

      return ((GoToColumnReader)reader).Read();
    }

    /// <summary>See <see cref="XmlSerializer.Serialize"/>.</summary>
    protected override void Serialize(object o, XmlSerializationWriter writer)
    {
      if (!(writer is GoToColumnWriter))
        throw new ArgumentException("writer");

      ((GoToColumnWriter)writer).Write((MorganStanley.MSDesktop.Rambo.WinGridUtils.GoToColumnParameters)o);
    }
    public class Reader : System.Xml.Serialization.XmlSerializationReader 
    {

      /// <remarks/>
      protected virtual MorganStanley.MSDesktop.Rambo.WinGridUtils.GoToColumnParameters Read1_GoToColumnParameters(bool isNullable, bool checkType) 
      {
        if (isNullable && ReadNull()) return null;
        if (checkType) 
        {
          System.Xml.XmlQualifiedName t = GetXsiType();
          if (t == null || ((object) ((System.Xml.XmlQualifiedName)t).Name == (object)id1_GoToColumnParameters && (object) ((System.Xml.XmlQualifiedName)t).Namespace == (object)id2_Item))
            ;
          else
            throw CreateUnknownTypeException((System.Xml.XmlQualifiedName)t);
        }
        MorganStanley.MSDesktop.Rambo.WinGridUtils.GoToColumnParameters o = new MorganStanley.MSDesktop.Rambo.WinGridUtils.GoToColumnParameters();
        while (Reader.MoveToNextAttribute()) 
        {
          if (!IsXmlnsAttribute(Reader.Name)) 
          {
            UnknownNode((object)o);
          }
        }
        Reader.MoveToElement();
        if (Reader.IsEmptyElement) 
        {
          Reader.Skip();
          return o;
        }
        Reader.ReadStartElement();
        bool[] paramsRead = new bool[2];
        Reader.MoveToContent();
        while (Reader.NodeType != System.Xml.XmlNodeType.EndElement) 
        {
          if (Reader.NodeType == System.Xml.XmlNodeType.Element) 
          {
            if (!paramsRead[0] && ((object) Reader.LocalName == (object)id3_Column && (object) Reader.NamespaceURI == (object)id2_Item)) 
            {
              o.@Column = Reader.ReadElementString();
              paramsRead[0] = true;
            }
            else if (!paramsRead[1] && ((object) Reader.LocalName == (object)id4_SortOrder && (object) Reader.NamespaceURI == (object)id2_Item)) 
            {
              o.@SortOrder = Read2_ColumnSortType(Reader.ReadElementString());
              paramsRead[1] = true;
            }
            else 
            {
              UnknownNode((object)o);
            }
          }
          else 
          {
            UnknownNode((object)o);
          }
          Reader.MoveToContent();
        }
        ReadEndElement();
        return o;
      }

      /// <remarks/>
      protected virtual MorganStanley.MSDesktop.Rambo.WinGridUtils.GoToColumnParameters.ColumnSortType Read2_ColumnSortType(string s) 
      {
        switch (s) 
        {
          case @"ALPHABETIC": return MorganStanley.MSDesktop.Rambo.WinGridUtils.GoToColumnParameters.ColumnSortType.@ALPHABETIC;
          case @"GRID_ORDER": return MorganStanley.MSDesktop.Rambo.WinGridUtils.GoToColumnParameters.ColumnSortType.@GRID_ORDER;
          default: throw CreateUnknownConstantException(s, typeof(MorganStanley.MSDesktop.Rambo.WinGridUtils.GoToColumnParameters.ColumnSortType));
        }
      }

      /// <remarks/>
      protected virtual System.Object Read3_Object(bool isNullable, bool checkType) 
      {
        if (isNullable && ReadNull()) return null;
        if (checkType) 
        {
          System.Xml.XmlQualifiedName t = GetXsiType();
          if (t == null)
            return ReadTypedPrimitive(new System.Xml.XmlQualifiedName("anyType", "http://www.w3.org/2001/XMLSchema"));
          else if (((object) ((System.Xml.XmlQualifiedName)t).Name == (object)id1_GoToColumnParameters && (object) ((System.Xml.XmlQualifiedName)t).Namespace == (object)id2_Item))
            return Read1_GoToColumnParameters(isNullable, false);
          else if (((object) ((System.Xml.XmlQualifiedName)t).Name == (object)id5_ColumnSortType && (object) ((System.Xml.XmlQualifiedName)t).Namespace == (object)id2_Item)) 
          {
            Reader.ReadStartElement();
            object e = Read2_ColumnSortType(Reader.ReadString());
            ReadEndElement();
            return e;
          }
          else
            return ReadTypedPrimitive((System.Xml.XmlQualifiedName)t);
        }
        System.Object o = new System.Object();
        while (Reader.MoveToNextAttribute()) 
        {
          if (!IsXmlnsAttribute(Reader.Name)) 
          {
            UnknownNode((object)o);
          }
        }
        Reader.MoveToElement();
        if (Reader.IsEmptyElement) 
        {
          Reader.Skip();
          return o;
        }
        Reader.ReadStartElement();
        bool[] paramsRead = new bool[0];
        Reader.MoveToContent();
        while (Reader.NodeType != System.Xml.XmlNodeType.EndElement) 
        {
          if (Reader.NodeType == System.Xml.XmlNodeType.Element) 
          {
            UnknownNode((object)o);
          }
          else 
          {
            UnknownNode((object)o);
          }
          Reader.MoveToContent();
        }
        ReadEndElement();
        return o;
      }
      protected override void InitCallbacks() 
      {
      }

      protected object Read4_GoToColumn() 
      {
        object o = null;
        Reader.MoveToContent();
        if (Reader.NodeType == System.Xml.XmlNodeType.Element) 
        {
          if (((object) Reader.LocalName == (object)id6_GoToColumn && (object) Reader.NamespaceURI == (object)id2_Item)) 
          {
            o = Read1_GoToColumnParameters(true, true);
          }
          else 
          {
            throw CreateUnknownNodeException();
          }
        }
        else 
        {
          UnknownNode(null);
        }
        return (object)o;
      }

      System.String id6_GoToColumn;
      System.String id5_ColumnSortType;
      System.String id1_GoToColumnParameters;
      System.String id3_Column;
      System.String id4_SortOrder;
      System.String id2_Item;

      protected override void InitIDs() 
      {
        id6_GoToColumn = Reader.NameTable.Add(@"GoToColumn");
        id5_ColumnSortType = Reader.NameTable.Add(@"ColumnSortType");
        id1_GoToColumnParameters = Reader.NameTable.Add(@"GoToColumnParameters");
        id3_Column = Reader.NameTable.Add(@"Column");
        id4_SortOrder = Reader.NameTable.Add(@"SortOrder");
        id2_Item = Reader.NameTable.Add(@"");
      }
    }

    public class Writer : System.Xml.Serialization.XmlSerializationWriter 
    {

      void Write1_GoToColumnParameters(string n, string ns, MorganStanley.MSDesktop.Rambo.WinGridUtils.GoToColumnParameters o, bool isNullable, bool needType) 
      {
        if ((object)o == null) 
        {
          if (isNullable) WriteNullTagLiteral(n, ns);
          return;
        }
        if (!needType) 
        {
          System.Type t = o.GetType();
          if (t == typeof(MorganStanley.MSDesktop.Rambo.WinGridUtils.GoToColumnParameters))
            ;
          else 
          {
            throw CreateUnknownTypeException(o);
          }
        }
        WriteStartElement(n, ns, o);
        if (needType) WriteXsiType(@"GoToColumnParameters", @"");
        WriteElementString(@"Column", @"", ((System.String)o.@Column));
        WriteElementString(@"SortOrder", @"", Write2_ColumnSortType(((MorganStanley.MSDesktop.Rambo.WinGridUtils.GoToColumnParameters.ColumnSortType)o.@SortOrder)));
        WriteEndElement(o);
      }

      string Write2_ColumnSortType(MorganStanley.MSDesktop.Rambo.WinGridUtils.GoToColumnParameters.ColumnSortType v) 
      {
        string s = null;
        switch (v) 
        {
          case MorganStanley.MSDesktop.Rambo.WinGridUtils.GoToColumnParameters.ColumnSortType.@ALPHABETIC: s = @"ALPHABETIC"; break;
          case MorganStanley.MSDesktop.Rambo.WinGridUtils.GoToColumnParameters.ColumnSortType.@GRID_ORDER: s = @"GRID_ORDER"; break;
          default: s = ((System.Int64)v).ToString(); break;
        }
        return s;
      }

      void Write3_Object(string n, string ns, System.Object o, bool isNullable, bool needType) 
      {
        if ((object)o == null) 
        {
          if (isNullable) WriteNullTagLiteral(n, ns);
          return;
        }
        if (!needType) 
        {
          System.Type t = o.GetType();
          if (t == typeof(System.Object))
            ;
          else if (t == typeof(MorganStanley.MSDesktop.Rambo.WinGridUtils.GoToColumnParameters)) 
          {
            Write1_GoToColumnParameters(n, ns, (MorganStanley.MSDesktop.Rambo.WinGridUtils.GoToColumnParameters)o, isNullable, true);
            return;
          }
          else if (t == typeof(MorganStanley.MSDesktop.Rambo.WinGridUtils.GoToColumnParameters.ColumnSortType)) 
          {
            Writer.WriteStartElement(n, ns);
            WriteXsiType(@"ColumnSortType", @"");
            Writer.WriteString(Write2_ColumnSortType((MorganStanley.MSDesktop.Rambo.WinGridUtils.GoToColumnParameters.ColumnSortType)o));
            Writer.WriteEndElement();
            return;
          }
          else 
          {
            WriteTypedPrimitive(n, ns, o, true);
            return;
          }
        }
        WriteStartElement(n, ns, o);
        WriteEndElement(o);
      }

      protected override void InitCallbacks() 
      {
      }

      protected void Write4_GoToColumn(object o) 
      {
        WriteStartDocument();
        if (o == null) 
        {
          WriteNullTagLiteral(@"GoToColumn", @"");
          return;
        }
        TopLevelElement();
        Write1_GoToColumnParameters(@"GoToColumn", @"", ((MorganStanley.MSDesktop.Rambo.WinGridUtils.GoToColumnParameters)o), true, false);
      }
    }

    /// <remarks/>
  }
    
  internal class GoToColumnReader : GoToColumnParametersSerializer.Reader
  {
        
        
    /// <remarks/>
    protected override MorganStanley.MSDesktop.Rambo.WinGridUtils.GoToColumnParameters Read1_GoToColumnParameters(bool isNullable, bool checkType)
    {
      MorganStanley.MSDesktop.Rambo.WinGridUtils.GoToColumnParameters obj = base.Read1_GoToColumnParameters(isNullable, checkType);
      GoToColumnParametersDeserializedHandler handler = GoToColumnParametersDeserialized;
      if (handler != null)
        handler(obj);

      return obj;
    }
        
    /// <remarks/>
    protected override MorganStanley.MSDesktop.Rambo.WinGridUtils.GoToColumnParameters.ColumnSortType Read2_ColumnSortType(string s)
    {
      MorganStanley.MSDesktop.Rambo.WinGridUtils.GoToColumnParameters.ColumnSortType obj = base.Read2_ColumnSortType(s);
      ColumnSortTypeDeserializedHandler handler = ColumnSortTypeDeserialized;
      if (handler != null)
        handler(obj);

      return obj;
    }
        
    /// <summary>Reads an object of type MorganStanley.MSDesktop.Rambo.WinGridUtils.GoToColumnParameters.</summary>
    internal MorganStanley.MSDesktop.Rambo.WinGridUtils.GoToColumnParameters Read()
    {
      return (MorganStanley.MSDesktop.Rambo.WinGridUtils.GoToColumnParameters) Read4_GoToColumn();
    }
    public event GoToColumnParametersDeserializedHandler GoToColumnParametersDeserialized;
        
    public event ColumnSortTypeDeserializedHandler ColumnSortTypeDeserialized;
  }
    
  internal delegate void GoToColumnParametersDeserializedHandler(MorganStanley.MSDesktop.Rambo.WinGridUtils.GoToColumnParameters gotocolumnparameters);
    
  internal delegate void ColumnSortTypeDeserializedHandler(MorganStanley.MSDesktop.Rambo.WinGridUtils.GoToColumnParameters.ColumnSortType columnsorttype);
    
  internal class GoToColumnWriter : GoToColumnParametersSerializer.Writer
  {
        
        
    /// <summary>Writes an object of type MorganStanley.MSDesktop.Rambo.WinGridUtils.GoToColumnParameters.</summary>
    internal void Write(MorganStanley.MSDesktop.Rambo.WinGridUtils.GoToColumnParameters obj)
    {
      Write4_GoToColumn(obj);
    }
    }
}

