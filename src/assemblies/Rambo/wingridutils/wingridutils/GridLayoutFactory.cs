
//---------------------------------------------------------------------------
//
// Copyright (c) 2004 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
//---------------------------------------------------------------------------

using System;
using System.Data;
using System.Collections.Specialized;
using Infragistics.Win.UltraWinGrid;
//using MorganStanley.IED.Concord.Configuration;
using System.Xml;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  /// <summary>
  /// The GridLayoutFactory is a generic class for constructing and manipulating data grid layouts.
  /// </summary>
  /// <remarks>
  /// This GridLayoutFactory acts as a wrapper so that users of the API are unaware of the underlying
  /// LayoutFactory type. The grid source is specified in the Factory constructor at which point the 
  /// appropriate factory is created. All calls are then applied to this factory and the results returned.
  /// </remarks>
  public sealed class GridLayoutFactory : AbstractGridLayoutFactory
  {
    #region Private Members

    private AbstractGridLayoutFactory m_layoutFactory;

    #endregion

    #region Constructors

    public static bool AllowGridLevelSetting = false;

    /// <summary>
    /// Creates an instance of the generic GridLayoutFactory.
    /// </summary>
    /// <param name="grid_">The underlying datagrid source.</param>
    public GridLayoutFactory(object grid_)
    {
      if (grid_ is UltraGrid)
      {
        m_layoutFactory = new UltraGridLayoutFactory((UltraGrid)grid_);
      }
      else if (grid_ is DataSet)
      {
        throw new NotSupportedException("DataSets are not currently suppored.");
      }
      else
      {
        throw new NotSupportedException("Unrecoginised grid source - not supported.");
      }
      InitializeDefaultValues();
    }

    #endregion

    #region Public Methods

    /// <summary>
    /// Returns an XML representation of the Grid provided.
    /// </summary>		
    /// <param name="applicationName_">The current application name.</param>	
    /// <param name="gridName_">The name of the current grid.</param>	
    /// <param name="layoutName_">The current layout name.</param>   
    /// <returns>An XML representation of the grid provided.</returns>	
    override public GridLayout GetGridLayout(string applicationName_, string gridName_, string layoutName_)
    {
      return m_layoutFactory.GetGridLayout(applicationName_, gridName_, layoutName_);
    }

    /// <summary>
    /// Returns an XML representation of the Grid provided.
    /// </summary>		
    /// <param name="applicationName_">The current application name.</param>	
    /// <param name="gridName_">The name of the current grid.</param>	
    /// <param name="layoutName_">The current layout name.</param>
    /// <param name="omitKeys_">A collection of columns keys to omit from the grid layout.</param>
    /// <returns>An XML representation of the grid provided.</returns>		
    override public GridLayout GetGridLayout(string applicationName_, string gridName_, string layoutName_, StringCollection omitKeys_)
    {
      return m_layoutFactory.GetGridLayout(applicationName_, gridName_, layoutName_, omitKeys_);
    }

    /// <summary>
    /// Returns an XML representation of the Grid provided.
    /// </summary>		
    /// <param name="applicationName_">The current application name.</param>	
    /// <param name="gridName_">The name of the current grid.</param>	
    /// <param name="layoutName_">The current layout name.</param>
    /// <param name="omitKeys_">A collection of columns keys to omit from the grid layout.</param>
    /// <param name="band_">The grid band the layout applies to</param>
    /// <returns>An XML representation of the grid provided.</returns>		
    override public GridLayout GetGridLayout(string applicationName_, string gridName_, string layoutName_, StringCollection omitKeys_, int band_)
    {
      return m_layoutFactory.GetGridLayout(applicationName_, gridName_, layoutName_, omitKeys_, band_);
    }

    /// <summary>
    /// Loads grid format information into the associated grid.
    /// </summary>		
    /// <param name="gridLayout_">The XML format data required to update the grid.</param>		
    /// <param name="startDepth_">The depth to start layout application within a hierichial grid.</param>
    /// <param name="applySort_">When set to true applies column sort operations.</param>
    /// <param name="applyColumnWidths_">When set to true applies column widths.</param>
    override public void ApplyGridLayout(GridLayout gridLayout_, int startDepth_, bool applySort_,
      bool applyColumnWidths_)
    {
      m_layoutFactory.ApplyGridLayout(gridLayout_, startDepth_, applySort_, applyColumnWidths_);
    }

    /// <summary>
    /// Loads grid format information into the associated grid.
    /// </summary>		
    /// <param name="gridLayout_">The XML format data required to update the grid.</param>		
    /// <param name="startDepth_">The depth to start layout application within a hierichial grid.</param>
    /// <param name="length_">The number of bands to apply the layout to.</param>
    /// <param name="applySort_">When set to true applies column sort operations.</param>
    /// <param name="applyColumnWidths_">When set to true applies column widths.</param>
    override public void ApplyGridLayout(GridLayout gridLayout_, int startDepth_, int length_,
      bool applySort_, bool applyColumnWidths_)
    {
      m_layoutFactory.ApplyGridLayout(gridLayout_, startDepth_, length_, applySort_, applyColumnWidths_);
    }


    /// <summary>
    /// Loads grid format information into the associated grid.
    /// </summary>		
    /// <param name="gridLayout_">The XML format data required to update the grid.</param>		
    /// <param name="startDepth_">The depth to start layout application within a hierichial grid.</param>
    /// <param name="length_">The number of bands to apply the layout to.</param>
    /// <param name="applySort_">When set to true applies column sort operations.</param>
    /// <param name="applyColumnWidths_">When set to true applies column widths.</param>
    /// <param name="applyColumnCaption_">When set to true applies column caption.</param>
    override public void ApplyGridLayout(GridLayout gridLayout_, int startDepth_, int length_,
      bool applySort_, bool applyColumnWidths_, bool applyColumnCaption_)
    {
      m_layoutFactory.ApplyGridLayout(gridLayout_, startDepth_, length_, applySort_, applyColumnWidths_, applyColumnCaption_);
    }



    /// <summary>
    /// Applies sorting properties to the the associated grid.
    /// </summary>		
    /// <param name="gridLayout_">The XML format data required to update the grid.</param>		
    /// <param name="startDepth_">The depth to start layout application within a hierichial grid.</param>
    public override void ApplySort(GridLayout gridLayout_, int startDepth_)
    {
      m_layoutFactory.ApplySort(gridLayout_, startDepth_);
    }

    /// <summary>
    /// Applies sorting properties to the the associated grid.
    /// </summary>		
    /// <param name="gridLayout_">The XML format data required to update the grid.</param>		
    /// <param name="startDepth_">The depth to start layout application within a hierichial grid.</param>
    /// <param name="length_">The number of bands to apply the layout starting at the specified depth.</param>
    public override void ApplySort(GridLayout gridLayout_, int startDepth_, int length_)
    {
      m_layoutFactory.ApplySort(gridLayout_, startDepth_, length_);
    }

    /// <summary>
    /// Applies column width properties to the the associated grid.
    /// </summary>		
    /// <param name="gridLayout_">The XML format data required to update the grid.</param>		
    /// <param name="startDepth_">The depth to start layout application within a hierichial grid.</param>
    public override void ApplyColumnWidths(GridLayout gridLayout_, int startDepth_)
    {
      m_layoutFactory.ApplyColumnWidths(gridLayout_, startDepth_);
    }

    /// <summary>
    /// Applies column width properties to the the associated grid.
    /// </summary>		
    /// <param name="gridLayout_">The XML format data required to update the grid.</param>		
    /// <param name="startDepth_">The depth to start layout application within a hierichial grid.</param>
    /// <param name="length_">The number of bands to apply the layout starting at the specified depth.</param>
    public override void ApplyColumnWidths(GridLayout gridLayout_, int startDepth_, int length_)
    {
      m_layoutFactory.ApplyColumnWidths(gridLayout_, startDepth_, length_);
    }

    /// <summary>
    /// Sets the underlying grid being used as the source by the GridLayoutFactory instance.
    /// </summary>
    override public object Grid
    {
      set { m_layoutFactory.Grid = value; }
    }

    /// <summary>
    /// Sets whether new columns added to the layout are hidden (defaulted to true)
    /// </summary>
    override public bool HideNewColumns
    {
      get
      {
        return m_layoutFactory.HideNewColumns;
      }
      set
      {
        m_layoutFactory.HideNewColumns = value;
      }
    }

    override public int StartDepth
    {
      get { return m_layoutFactory.StartDepth; }
      set { m_layoutFactory.StartDepth = value; }
    }

    override public int Length
    {
      get { return m_layoutFactory.Length; }
      set { m_layoutFactory.Length = value; }
    }

    override public bool ApplySortBool
    {
      get { return m_layoutFactory.ApplySortBool; }
      set { m_layoutFactory.ApplySortBool = value; }
    }

    override public bool ApplyColumnWidth
    {
      get { return m_layoutFactory.ApplyColumnWidth; }
      set { m_layoutFactory.ApplyColumnWidth = value; }
    }

    override public bool ApplyCaptions
    {
      get { return m_layoutFactory.ApplyCaptions; }
      set { m_layoutFactory.ApplyCaptions = value; }
    }
    #endregion


  }
}
