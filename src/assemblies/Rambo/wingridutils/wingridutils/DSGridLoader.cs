//---------------------------------------------------------------------------
//
// Copyright (c) 2004 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
//---------------------------------------------------------------------------

using System;
using System.IO;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.CompilerServices;
using System.ComponentModel;
using JetBrains.Annotations;
using MorganStanley.MSDesktop.Rambo.DataServices;
using MorganStanley.MSDesktop.Rambo.AsyncUtils;
using MorganStanley.MSDesktop.Rambo.Desktop.Utils;
using MorganStanley.MSDotNet.Directory;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  /// <summary>
  /// Class that enables data grid layout persistence via use of DataServices.
  /// </summary>
  public class DSGridLoader
  {
    public enum PermissionType { Group, User, CostCenter, ALL };

    #region Member Variables

    private const string GRID_FORMATTER_DATASERVICE_NAME = "GridLayouts";
    private const string GRID_LAYOUT_LIST_DATASERVICE_NAME = "GridLayoutsList";
    private delegate void SaveGridLayoutDelegate(GridLayout grid_, string userName_);
    private delegate GridLayout LoadGridLayoutDelegate(string applicationName_, string gridName_, string layoutName_, bool forceReload_);    
    private delegate GridLayout[] LoadGridLayoutsDelegate(string applicationName_, string gridName_, string[] layoutNames_);
    private delegate GridLayout[] LoadAllGridLayoutsDelegate(string applicationName_, string gridName_);
    private delegate GridLayout[] LoadAllGridLayoutsListDelegate(string applicationName_, string gridName_, PermissionType permissionType_);
    private const string APPLICATION_NAME_KEY = "application";
    private const string GRID_NAME_KEY = "gridName";
    private const string COST_CENTRE_KEY = "CC";
    private static bool _costCentreSubscription = false;
    private static string _costCentre;
    private static Hashtable _cache = Hashtable.Synchronized(new Hashtable());
    private static ILogger _log = LoggerManager.GetLogger(typeof(DSGridLoader));

    private static Hashtable _waitingRequests = new Hashtable();
    private static Hashtable _listCache = new Hashtable();

    #endregion

    #region Constructors

    /// <summary>
    /// No pubiic constructor - static method functionality only.
    /// </summary>
    private DSGridLoader() { }

    #endregion

    #region Public Methods

    /// <summary>
    /// specifies whether or not to include a cost-centre attribute subscription in LoadAllGridLayouts()
    /// if yes, the result is that the list of layouts will be restricted to those saved by people
    /// in the same cost-centre as the user.
    /// </summary>
    public static bool DoCostCentreSubscription
    {
      [MethodImpl(MethodImplOptions.Synchronized)]
      get { return _costCentreSubscription; }

      [MethodImpl(MethodImplOptions.Synchronized)]
      set { _costCentreSubscription = value; }
    }

    internal static void ClearCache()
    {
      lock (_cache.SyncRoot)
      {
        _cache.Clear();
        _listCache.Clear();
      }
    }


    /// <summary>
    /// Clears a list cache without clearing item cache thus 
    /// all in memory changes would remain intact
    /// </summary>
    internal static void ClearListCache()
    {
      lock (_cache.SyncRoot)
      {
        _listCache.Clear();
      }
    }

    public static void ClearGridLayoutCache()
    {
      ClearCache();
      ClearListCache();
    }

    /// <summary>
    /// Persists a particular grid layout for later retrieval.
    /// </summary>		
    public static void SaveGridLayout(GridLayout gridLayout_, string updateID)
    {
      if (!DataServiceManager.IsInitialised())
      {
        throw new Exception("DataService Manager was not initialised.");
      }
      try
      {
        gridLayout_.UpdateID = updateID;
        IDataService dataService = DataServiceFactoryLayer.GetDataService(GRID_FORMATTER_DATASERVICE_NAME);
        IDataKey dsKey = DataKeyFactory.BuildDataKey(GenerateGridDataKey(gridLayout_.Application,
          gridLayout_.GridName, gridLayout_.LayoutName));
        XmlElement layoutXmlElement = UltraGridLayoutFactory.GetXml(gridLayout_);

        _log.Info("SaveGridLayout", String.Format("Saving layout {0}: \n{1}",
          dsKey.KeyString, layoutXmlElement.OuterXml));

        dataService.Publish(new XMLDataObject(dsKey, layoutXmlElement));

      }
      catch (Exception ex)
      {
        throw new Exception("Error saving grid layout.", ex);
      }

      UpdateLocalCache(gridLayout_);
    }

    /// <summary>
    /// Assumes cache lock is already held
    /// </summary>
    private static void AddToListCache(string listKey_, string key_)
    {
      if (_listCache.Contains(listKey_))
      {
        IList list = _listCache[listKey_] as IList;
        if (list != null && !list.Contains(key_))
        {
          list.Add(key_);
        }
      }
    }

    /// <summary>
    /// Updates internal cache for layout without saving to the ds
    /// </summary>
    /// <param name="gridLayout_"></param>
    public static void UpdateLocalCache(GridLayout gridLayout_)
    {
      string key = GenerateGridDataKey(gridLayout_.Application, gridLayout_.GridName, gridLayout_.LayoutName);

      lock (_cache.SyncRoot)
      {
        _cache[key] = gridLayout_;

        AddToListCache(gridLayout_.Application + "/" + gridLayout_.GridName + ":" + PermissionType.ALL.ToString(), key);
        //Assumes published by current user (This is valid for RV and TD)
        AddToListCache(gridLayout_.Application + "/" + gridLayout_.GridName + ":" +
                       PermissionType.Group.ToString(), key);

        if (Environment.UserName.Equals(gridLayout_.UpdateID, StringComparison.OrdinalIgnoreCase))
        {
          AddToListCache(gridLayout_.Application + "/" + gridLayout_.GridName + ":" +
                         PermissionType.User.ToString(),
                         key);
        }
      }
    }

    private static void CallbackWaitingRequests(string key_, object result_)
    {
      IList waitingResults = null;
      lock (_cache.SyncRoot)
      {
        if (_waitingRequests.Contains(key_))
        {
          waitingResults = _waitingRequests[key_] as IList;
          _waitingRequests.Remove(key_);
        }
      }
      if (waitingResults != null)
      {
        foreach (ManualAsyncResult ar in waitingResults)
        {
          ar.SetMethodReturnedValue(result_);
        }
      }
    }

    private static void SetWaitingRequestException(string key_, Exception ex_)
    {
      IList waitingResults = null;
      lock (_cache.SyncRoot)
      {
        if (_waitingRequests.Contains(key_))
        {
          waitingResults = _waitingRequests[key_] as IList;
          _waitingRequests.Remove(key_);
        }
      }
      if (waitingResults != null)
      {
        foreach (ManualAsyncResult ar in waitingResults)
        {
          ar.SetInvokeException(ex_);
        }
      }
    }

    /// <summary>
    /// Retrieves a particular grid layout that has been saved using the SaveGridLayout method.
    /// </summary>
    /// <param name="applicationName_">The current application name.</param>
    /// <param name="gridName_">The name of the grid whose layout is to be retrieved.</param>
    /// <param name="layoutName_">The current layout name.</param>
    /// <param name="forceReload_">if set to <c>true</c>, force the reload of the layout from the DS.</param>
    /// <returns>The GridLayout retreived from the DS, or null if nothing found</returns>
    /// <remarks>The functionality in LoadGridLayout ustilises the DataServices library
    /// to retrieve any persisted grid layouts. The DataService Manager must be initialised
    /// by any calling applications prior to using this method. If a particular grid layout is
    /// not located via DataServices then null will be returned.
    /// The grid layout is returned in standard XML representation; to apply this format to
    /// a grid see the LoadGridLayout method of the GridLayoutFactory.</remarks>
    public static GridLayout LoadGridLayout(string applicationName_, string gridName_, string layoutName_)
    {
      return LoadGridLayout(applicationName_, gridName_, layoutName_, false);
    }

    /// <summary>
    /// Retrieves a particular grid layout that has been saved using the SaveGridLayout method.
    /// </summary>
    /// <param name="applicationName_">The current application name.</param>
    /// <param name="gridName_">The name of the grid whose layout is to be retrieved.</param>
    /// <param name="layoutName_">The current layout name.</param>
    /// <param name="forceReload_">if set to <c>true</c>, force the reload of the layout from the DS.</param>
    /// <returns>The GridLayout retreived from the DS, or null if nothing found</returns>
    /// <remarks>The functionality in LoadGridLayout ustilises the DataServices library
    /// to retrieve any persisted grid layouts. The DataService Manager must be initialised
    /// by any calling applications prior to using this method. If a particular grid layout is
    /// not located via DataServices then null will be returned.
    /// The grid layout is returned in standard XML representation; to apply this format to
    /// a grid see the LoadGridLayout method of the GridLayoutFactory.</remarks>
    [CanBeNull]
    public static GridLayout LoadGridLayout(string applicationName_, string gridName_, string layoutName_, bool forceReload_)
    {
      string key = GenerateGridDataKey(applicationName_, gridName_, layoutName_);
      try
      {
        if (!DataServiceManager.IsInitialised())
        {
          throw new Exception("DataService Manager was not initialised.");
        }

        GridLayout layout = null;
        if (!forceReload_)
        {
          lock (_cache.SyncRoot)
          {
            // Look up in cache first
            if (_cache.ContainsKey(key))
            {
              layout = (GridLayout) _cache[key];
              if (layout.Columns == null)
              {
                layout = null; //Still needs to load from server
              }
            }
          }
        }

        if (layout == null)
        {
          IDataService dataService = DataServiceFactoryLayer.GetDataService(GRID_FORMATTER_DATASERVICE_NAME);
          IDataKey dsKey = DataKeyFactory.BuildDataKey(key);

          DataEvent dataEvent = dataService.GetSnapshot(dsKey);

          if (dataEvent != null)
          {
            var xmlLayoutObject = (XMLDataObject)dataEvent.DataObject;

            _log.Info("LoadGridLayout", String.Format("Retrieved layout {0}: \n {1}",
              dsKey.KeyString, xmlLayoutObject.Value.OuterXml));

            var serializer = new GridLayoutSerializer();

            layout = (GridLayout)serializer.Deserialize
              (new StringReader(xmlLayoutObject.Value.OuterXml));
            lock (_cache.SyncRoot)
            {
              _cache[key] = layout;
            }
          }
        }

        if (layout != null)
        {
          StripFormatSeparatorFromCaption(layout);
        }
        CallbackWaitingRequests(key, layout);

        return layout;
      }
      catch (Exception ex_)
      {
        Exception e = new Exception("Error retrieving grid layout.", ex_);
        SetWaitingRequestException(key, e);
        throw e;
      }
    }

    /// <summary>
    /// Removes � from all caption properties in the layout and replaces it with "\n"
    /// </summary>
    /// <param name="gridLayout"></param>
    static private void StripFormatSeparatorFromCaption(GridLayout gridLayout)
    {

      foreach (ColumnLayout columnLayout in gridLayout.Columns)
      {
        columnLayout.Caption = columnLayout.Caption.Replace('�', '\n');
        columnLayout.Key = columnLayout.Key.Replace('�', '\n');

      }
    }


    /// <summary>
    /// Retrieves a particular set of grid layouts that have been saved using the SaveGridLayout method.
    /// </summary>		
    /// <param name="applicationName_">The current application name.</param>
    /// <param name="gridName_">The name of the grid whose layout is to be retrieved.</param>	
    /// <param name="layoutNames_">The current layout names.</param>		
    public static GridLayout[] LoadGridLayouts(string applicationName_, string gridName_, string[] layoutNames_)
    {
      string[] sortedLayouts = (string[])layoutNames_.Clone();
      Array.Sort(sortedLayouts);
      string waitingKey = GenerateGridDataKey(applicationName_, gridName_, string.Join(",", sortedLayouts));
      try
      {
        if (!DataServiceManager.IsInitialised())
        {
          throw new Exception("DataService Manager was not initialised.");
        }

        string subKey = applicationName_ + "/" + gridName_;
        GridLayout[] layouts = new GridLayout[layoutNames_.Length];
        IList remainingKeys = new ArrayList();
        lock (_cache.SyncRoot)
        {
          for (int i = 0; i < layoutNames_.Length; ++i)
          {
            string key = subKey + "/" + layoutNames_[i];
            if (_cache.ContainsKey(key))
            {
              layouts[i] = _cache[key] as GridLayout;
              if (layouts[i].Columns == null)
              {
                remainingKeys.Add(DataKeyFactory.BuildDataKey(key));
              }
            }
            else
            {
              remainingKeys.Add(DataKeyFactory.BuildDataKey(key));
            }
          }
        }

        if (remainingKeys.Count > 0)
        {
          IDataService dataService = DataServiceFactoryLayer.GetDataService(GRID_FORMATTER_DATASERVICE_NAME);
          ISubscription sub = dataService.CreateSubscription(remainingKeys);
          IDictionary dict = dataService.GetSnapshot(sub);

          if (dict != null)
          {
            lock (_cache.SyncRoot)
            {
              foreach (DataEvent de in dict.Values)
              {
                GridLayoutSerializer serializer = new GridLayoutSerializer();
                GridLayout layout = (GridLayout)serializer.Deserialize
                  (new StringReader(((XMLDataObject)de.DataObject).Value.OuterXml));

                for (int index = 0; index < layoutNames_.Length; ++index)
                {
                  if (layoutNames_[index] == layout.LayoutName)
                  {
                    layouts[index] = layout;
                    break;
                  }
                }
              }
            }
            if (dict.Count != remainingKeys.Count)
            {
              GridLayout[] foundLayouts = new GridLayout[layoutNames_.Length - (remainingKeys.Count - dict.Count)];
              int c = 0;
              for (int i = 0; i < layouts.Length; ++i)
              {
                if (layouts[i] != null)
                {
                  foundLayouts[c++] = layouts[i];
                }
              }
              layouts = foundLayouts;
            }
          }
          else
          {
            GridLayout[] foundLayouts = new GridLayout[layoutNames_.Length - remainingKeys.Count];
            int c = 0;
            for (int i = 0; i < layouts.Length; ++i)
            {
              if (layouts[i] != null)
              {
                foundLayouts[c++] = layouts[i];
              }
            }
            layouts = foundLayouts;
          }
        }
        CallbackWaitingRequests(waitingKey, layouts);
        return layouts;
      }
      catch (Exception ex_)
      {
        Exception e = new Exception("Error retrieving grid layout.", ex_);
        SetWaitingRequestException(waitingKey, e);
        throw e;
      }
    }

    /// <summary>
    /// Attempts to create find all the past layouts in the cache and returns them if
    /// they all have their columns populated. Otherwise returns null.
    /// 
    /// Assumes already holding the cache lock
    /// </summary>
    /// <param name="keys_"></param>
    /// <returns></returns>
    private static GridLayout[] BuildFromCache(IList keys_)
    {
      GridLayout[] layouts = new GridLayout[keys_.Count];
      for (int i = 0; i < keys_.Count; ++i)
      {
        if (_cache.ContainsKey(keys_[i]))
        {
          GridLayout layout = _cache[keys_[i]] as GridLayout;
          if (layout.Columns == null)
          {
            return null;
          }
          else
          {
            layouts[i] = layout;
          }
        }
        else
        {
          return null;
        }
      }
      return layouts;
    }

    /// <summary>
    /// Retrieves all grid layouts that match the application and grid name attributes.
    /// </summary> 
    /// <param name="applicationName_">The application name.</param>
    /// <param name="gridName_">The name of the grid whose layouts are to be retrieved.</param>	
    public static GridLayout[] LoadAllGridLayouts(string applicationName_, string gridName_)
    {
      string key = applicationName_ + "/" + gridName_;
      try
      {
        if (!DataServiceManager.IsInitialised())
        {
          throw new Exception("DataService Manager was not initialised.");
        }

        GridLayout[] layouts = null;
        lock (_cache.SyncRoot)
        {
          // Look up in cache first
          if (_listCache.ContainsKey(key))
          {
            IList keys = _listCache[key] as IList;
            layouts = BuildFromCache(keys);
          }
        }

        if (layouts == null)
        {
          IDataService dataService = DataServiceFactoryLayer.GetDataService(GRID_FORMATTER_DATASERVICE_NAME);
          IDictionary dict = new Hashtable();
          dict[APPLICATION_NAME_KEY] = applicationName_;
          dict[GRID_NAME_KEY] = gridName_;

          if (DoCostCentreSubscription)
          {
            string costCentre = GetCostCentre();
            if (costCentre != null && costCentre != string.Empty)
            {
              dict[COST_CENTRE_KEY] = GetCostCentre();
            }
          }

          ISubscription subscription = dataService.CreateSubscription(dict);
          IDictionary snapshot = dataService.GetSnapshot(subscription);

          if (snapshot != null)
          {
            lock (_cache.SyncRoot)
            {
              IList keys = new ArrayList(snapshot.Count);
              layouts = new GridLayout[snapshot.Count];
              int cnt = 0;
              foreach (IDataKey dsKey in snapshot.Keys)
              {
                string lkey = dsKey.DSKey;
                DataEvent de = snapshot[dsKey] as DataEvent;
                keys.Add(lkey);
                //Only use returned layout if there is no local version in the cache as
                //the local version may have been edited
                if (_cache.ContainsKey(lkey))
                {
                  GridLayout layout = _cache[lkey] as GridLayout;
                  if (layout.Columns == null)
                  {
                    GridLayoutSerializer serializer = new GridLayoutSerializer();
                    layouts[cnt] = (GridLayout)serializer.Deserialize
                      (new StringReader(((XMLDataObject)de.DataObject).Value.OuterXml));
                    _cache[lkey] = layouts[cnt++];
                  }
                  else
                  {
                    layouts[cnt++] = layout;
                  }
                }
                else
                {
                  GridLayoutSerializer serializer = new GridLayoutSerializer();
                  layouts[cnt] = (GridLayout)serializer.Deserialize
                    (new StringReader(((XMLDataObject)de.DataObject).Value.OuterXml));
                  _cache[lkey] = layouts[cnt++];
                }
              }
              _listCache.Add(key, keys);
            }
          }
        }

        if (layouts != null)
        {
          foreach (GridLayout layout in layouts)
          {
            StripFormatSeparatorFromCaption(layout);
          }
        }
        CallbackWaitingRequests(key, layouts);
        return layouts;
      }
      catch (Exception ex_)
      {
        Exception e = new Exception("Error retrieving grid layouts.", ex_);
        SetWaitingRequestException(key, e);
        throw e;
      }
    }

    /// <summary>
    /// Retrieves all grid layouts that match the application and grid name attributes.
    /// </summary> 
    /// <param name="applicationName_">The application name.</param>
    /// <param name="gridName_">The name of the grid whose layouts are to be retrieved.</param>	
    public static GridLayout[] UpdateCacheWithNewNames(string applicationName_, string gridName_,
      PermissionType permissionType_)
    {
      string key = applicationName_ + "/" + gridName_ + ":" + permissionType_.ToString();
      try
      {
        if (!DataServiceManager.IsInitialised())
        {
          throw new Exception("DataService Manager was not initialised.");
        }

        GridLayout[] layouts = null;
        lock (_cache.SyncRoot)
        {
          if (_listCache.Contains(key))
          {
            layouts = BuildGridLayoutList((IList)_listCache[key]);
          }
        }

        if (layouts == null)
        {
          string costCentre = "all";
          if (permissionType_ != PermissionType.ALL && DoCostCentreSubscription || (permissionType_ == PermissionType.CostCenter))
          {
            if ((permissionType_ == PermissionType.Group) || (permissionType_ == PermissionType.CostCenter))
            {
              costCentre = GetCostCentre();
              if (costCentre == null || costCentre == string.Empty)
              {
                CallbackWaitingRequests(key, layouts);
                return null;
              }
              else
              {
                costCentre = "CC-" + costCentre;
              }
            }
            else
            {
              costCentre = "U-" + Environment.UserName;
            }
          }
          else
          {
            if (permissionType_ == PermissionType.User)
            {
              costCentre = "U-" + Environment.UserName;
            }
          }

          IDataService dataService = DataServiceFactoryLayer.GetDataService(GRID_LAYOUT_LIST_DATASERVICE_NAME);
          IDataKey dsKey = DataKeyFactory.BuildDataKey(string.Format("{0}/{1}/{2}", applicationName_, gridName_, costCentre));

          DataEvent dataEvent = dataService.GetSnapshot(dsKey);


          if (dataEvent != null)
          {
            XmlNodeList nodeList = ((XmlElement)dataEvent.DataObject.Value).SelectNodes("//GridLayout");
            IList keys = new ArrayList(nodeList.Count);
            foreach (XmlNode layoutXMLNode in nodeList)
            {
              string newKey = GenerateGridDataKey(applicationName_, gridName_,
                                           layoutXMLNode.Attributes["layoutName"].Value);
              if (layoutXMLNode.SelectNodes("./UpdateID").Count > 0)
              {
                newKey += "/" + layoutXMLNode.SelectNodes("./UpdateID")[0].FirstChild.Value;
              }
              keys.Add(newKey);

            }
            lock (_cache.SyncRoot)
            {
              _listCache[key] = keys;
              layouts = BuildGridLayoutList(keys);
            }
          }
        }
        CallbackWaitingRequests(key, layouts);
        return layouts;
      }
      catch (Exception ex_)
      {
        Exception e = new Exception("Error retrieving grid layouts.", ex_);
        SetWaitingRequestException(key, e);
        throw e;
      }
    }
    /// <summary>
    /// Retrieves all grid layouts that match the application and grid name attributes.
    /// </summary> 
    /// <param name="applicationName_">The application name.</param>
    /// <param name="gridName_">The name of the grid whose layouts are to be retrieved.</param>	
    public static GridLayout[] LoadAllGridLayoutsList(string applicationName_, string gridName_,
      PermissionType permissionType_)
    {
      string key = applicationName_ + "/" + gridName_ + ":" + permissionType_.ToString();
      try
      {
        if (!DataServiceManager.IsInitialised())
        {
          throw new Exception("DataService Manager was not initialised.");
        }

        GridLayout[] layouts = null;
        lock (_cache.SyncRoot)
        {
          if (_listCache.Contains(key))
          {
            layouts = BuildGridLayoutList((IList)_listCache[key]);
          }
        }

        if (layouts == null)
        {
          string costCentre = "all";
          if (permissionType_ != PermissionType.ALL && DoCostCentreSubscription || (permissionType_ == PermissionType.CostCenter))
          {
            if ((permissionType_ == PermissionType.Group) || (permissionType_ == PermissionType.CostCenter))
            {
              costCentre = GetCostCentre();
              if (costCentre == null || costCentre == string.Empty)
              {
                CallbackWaitingRequests(key, layouts);
                return null;
              }
              else
              {
                costCentre = "CC-" + costCentre;
              }
            }
            else
            {
              costCentre = "U-" + Environment.UserName;
            }
          }
          else
          {
            if (permissionType_ == PermissionType.User)
            {
              costCentre = "U-" + Environment.UserName;
            }
          }

          IDataService dataService = DataServiceFactoryLayer.GetDataService(GRID_LAYOUT_LIST_DATASERVICE_NAME);
          IDataKey dsKey = DataKeyFactory.BuildDataKey(string.Format("{0}/{1}/{2}", applicationName_, gridName_, costCentre));

          DataEvent dataEvent = dataService.GetSnapshot(dsKey);


          if (dataEvent != null)
          {
            string node = ResourceManager.GetListDSNode(GRID_FORMATTER_DATASERVICE_NAME);
            XmlNodeList nodeList = ((XmlElement)dataEvent.DataObject.Value).SelectNodes("//"+node);
            IList keys = new ArrayList(nodeList.Count);
            foreach (XmlNode layoutXMLNode in nodeList)
            {
              string newKey = GenerateGridDataKey(applicationName_, gridName_,
                                           layoutXMLNode.Attributes["layoutName"].Value);
              if (layoutXMLNode.SelectNodes("./UpdateID").Count > 0)
              {
                newKey += "/" + layoutXMLNode.SelectNodes("./UpdateID")[0].FirstChild.Value;
              }
              keys.Add(newKey);

            }
            lock (_cache.SyncRoot)
            {
              _listCache[key] = keys;
              layouts = BuildGridLayoutList(keys);
            }
          }
        }
        CallbackWaitingRequests(key, layouts);
        return layouts;
      }
      catch (Exception ex_)
      {
        Exception e = new Exception("Error retrieving grid layouts.", ex_);
        SetWaitingRequestException(key, e);
        throw e;
      }
    }

    //Assumes already inside _cache lock
    private static GridLayout[] BuildGridLayoutList(IList keys_)
    {

      GridLayout[] gridLayouts = new GridLayout[keys_.Count];
      for (int i = 0; i < keys_.Count; ++i)
      {

        //This is needed to remove updateId from the key
        //because it is stored without update key in the cache
        //TODO: Change this to make eather all keys include user ID or pass it separately

        string[] parts = ((string)keys_[i]).Split('/');
        keys_[i] = String.Format("{0}/{1}/{2}", parts[0],
               parts[1], parts[2]);
        if (_cache.ContainsKey(keys_[i]))
        {
          gridLayouts[i] = _cache[keys_[i]] as GridLayout;
        }
        else
        {
          gridLayouts[i] = new GridLayout();
          gridLayouts[i].Application = parts[0];
          gridLayouts[i].GridName = parts[1];
          gridLayouts[i].LayoutName = parts[2];
          if (parts.Length > 3)
          {
            gridLayouts[i].UpdateID = parts[3];
          }
          _cache[keys_[i]] = gridLayouts[i];
        }
      }
      return gridLayouts;
    }

    [MethodImpl(MethodImplOptions.Synchronized)]
    public static string GetCostCentre()
    {
      if (_costCentre != null)
      {
        return _costCentre;
      }

      string userId = Environment.UserName;

      try
      {
        Search s = new FWD2Search();
        s.Username = "cn=dsserverid, ou=ldapids, o=Morgan Stanley";
        s.Password = "DS_Server";

        Person person = s.GetPersonByLogon(userId);
        _costCentre = person.CostCenter;
      }
      catch (Exception ex)
      {
        _log.Warning("GetCostCentre", string.Format("LDAP lookup for user '{0}' failed.", userId), ex);
      }

      return _costCentre;

    }

    /// <summary>
    /// Retrieves a particular grid layout that has been saved using the SaveGridLayout
    /// method.
    /// </summary>		
    /// <param name="applicationName_">The current application name.</param>
    /// <param name="gridName_">The name of the grid whose layout is to be retrieved.</param>
    /// <param name="layoutName_">The current layout name.</param>	
    /// <param name="callback_">The call-back method to be called.</param>
    /// <returns>IAsyncResult to co-ordinate async call</returns>
     public static IAsyncResult BeginLoadGridLayout(string applicationName_, string gridName_,
        string layoutName_, AsyncCallback callback_)
     {
        return BeginLoadGridLayout(applicationName_, gridName_, layoutName_, false, gridName_, callback_);
     }

     /// <summary>
     /// Retrieves a particular grid layout that has been saved using the SaveGridLayout
     /// method.
     /// </summary>		
     /// <param name="applicationName_">The current application name.</param>
     /// <param name="gridName_">The name of the grid whose layout is to be retrieved.</param>
     /// <param name="layoutName_">The current layout name.</param>	
     /// <param name="callback_">The call-back method to be called.</param>
     /// <param name="forceReload_">Force reload layout from server.</param>     
     /// <returns>IAsyncResult to co-ordinate async call</returns>
     public static IAsyncResult BeginLoadGridLayout(string applicationName_, string gridName_,
      string layoutName_, bool forceReload_, AsyncCallback callback_)
     {
       return BeginLoadGridLayout(applicationName_, gridName_, layoutName_, forceReload_, gridName_, callback_);
     }


    /// <summary>
    /// Retrieves a particular grid layout that has been saved using the SaveGridLayout
    /// method.
    /// </summary>		
    /// <param name="applicationName_">The current application name.</param>
    /// <param name="gridName_">The name of the grid whose layout is to be retrieved.</param>
    /// <param name="layoutName_">The current layout name.</param>	
    /// <param name="callback_">The call-back method to be called.</param>
     /// <param name="forceReload_">Force reload layout from server.</param>
    /// <param name="state_">State object for async call.</param>
    /// <returns>IAsyncResult to co-ordinate async call</returns>
    public static IAsyncResult BeginLoadGridLayout(string applicationName_, string gridName_,
      string layoutName_, bool forceReload_, object state_, AsyncCallback callback_)
    {
      object cacheValue = null;
      ManualAsyncResult result = null;
      lock (_cache.SyncRoot)
      {
        string key = applicationName_ + "/" + gridName_ + "/" + layoutName_;
        // Look up in cache first
        if (_cache.ContainsKey(key))
        {
          GridLayout cachedlayout = _cache[key] as GridLayout;
          if (cachedlayout.Columns != null)
          {
            //Build Async result and dummy Asynchroniser
            Asynchronizer async = new Asynchronizer(callback_, state_);
            result = new ManualAsyncResult(callback_, gridName_, async);
            cacheValue = cachedlayout;
          }
        }
        else if (_waitingRequests.ContainsKey(key))
        {
          IList reqs = _waitingRequests[key] as IList;
          Asynchronizer async = new Asynchronizer(callback_, state_);
          result = new ManualAsyncResult(callback_, gridName_, async);
          reqs.Add(result);
          return result;
        }
        else
        {
          _waitingRequests[key] = new ArrayList();
        }
      }
      if (cacheValue != null)
      {
        result.SetMethodReturnedValue(cacheValue);
        return result;
      }

      Asynchronizer ssi = new Asynchronizer(callback_, state_);
      return ssi.BeginInvoke(new LoadGridLayoutDelegate(LoadGridLayout),
        new Object[] { applicationName_, gridName_, layoutName_, forceReload_ });
    }


    /// <summary>
    /// Retrieves a particular grid layout that has been saved using the SaveGridLayout
    /// method.
    /// </summary>		
    /// <param name="applicationName_">The current application name.</param>
    /// <param name="gridName_">The name of the grid whose layout is to be retrieved.</param>
    /// <param name="layoutNames_">The layout names.</param>	
    /// <param name="callback_">The call-back method to be called.</param>
    /// <returns>IAsyncResult to co-ordinate asyn call</returns>
    public static IAsyncResult BeginLoadGridLayouts(string applicationName_, string gridName_,
      string[] layoutNames_, AsyncCallback callback_)
    {
      string[] sortedLayouts = (string[])layoutNames_.Clone();
      Array.Sort(sortedLayouts);
      string waitingKey = GenerateGridDataKey(applicationName_, gridName_, string.Join(",", sortedLayouts));

      GridLayout[] layouts = new GridLayout[layoutNames_.Length];
      ManualAsyncResult result = null;

      lock (_cache.SyncRoot)
      {
        for (int i = 0; i < layoutNames_.Length; ++i)
        {
          string key = applicationName_ + "/" + gridName_ + "/" + layoutNames_[i];
          if (_cache.ContainsKey(key))
          {
            layouts[i] = _cache[key] as GridLayout;
            if (layouts[i].Columns == null)
            {
              layouts = null;
              break;
            }
          }
          else
          {
            layouts = null;
            break;
          }
        }

        if (layouts == null)
        {
          if (_waitingRequests.ContainsKey(waitingKey))
          {
            IList reqs = _waitingRequests[waitingKey] as IList;
            Asynchronizer async = new Asynchronizer(callback_, gridName_);
            result = new ManualAsyncResult(callback_, gridName_, async);
            reqs.Add(result);
            return result;
          }
          else
          {
            _waitingRequests[waitingKey] = new ArrayList();
          }
        }
        else
        {
          Asynchronizer async = new Asynchronizer(callback_, gridName_);
          result = new ManualAsyncResult(callback_, gridName_, async);
        }
      }
      if (layouts != null)
      {
        result.SetMethodReturnedValue(layouts); //Causes callbacks to be fired
        return result;
      }

      Asynchronizer ssi = new Asynchronizer(callback_, gridName_);
      return ssi.BeginInvoke(new LoadGridLayoutsDelegate(LoadGridLayouts),
        new Object[] { applicationName_, gridName_, layoutNames_ });
    }

    /// <summary>
    /// Retrieves a set of grid layouts that has been saved using the SaveGridLayout
    /// method provided by the GridLayoutFactory.
    /// </summary>	
    /// <param name="ar_">The IAyncResult object.</param>
    /// <returns>XML representation of grid layout.</returns>
    public static GridLayout[] EndLoadGridLayouts(IAsyncResult ar_)
    {
      AsynchronizerResult asr = (AsynchronizerResult)ar_;
      return (GridLayout[])asr.AsynchronizerParent.EndInvoke(ar_);
    }

    /// <summary>
    /// Retrieves a particular grid layout that has been saved using the SaveGridLayout
    /// method provided by the GridLayoutFactory.
    /// </summary>	
    /// <param name="result_">The IAyncResult object.</param>
    /// <returns>XML representation of grid layout.</returns>
    public static GridLayout EndLoadGridLayout(IAsyncResult result_)
    {
      AsynchronizerResult asr = (AsynchronizerResult)result_;
      object tempLayout = asr.AsynchronizerParent.EndInvoke(result_);
      if (tempLayout == null)
      {
        return null;
      }
      if (tempLayout is GridLayout)
      {
        return (GridLayout)tempLayout;
      }
      else if ((tempLayout is GridLayout[]) && ((tempLayout as GridLayout[]).Length > 0))
      {
        return ((GridLayout[])tempLayout)[0];
      }
      else
      {
        return null;
      }
    }

    /// <summary>
    /// Retrieves all grid layouts that match the application and grid name attributes.
    /// </summary> 
    /// <param name="applicationName_">The application name.</param>
    /// <param name="gridName_">The name of the grid whose layouts are to be retrieved.</param>	
    /// <param name="callback_">The async callback method.</param>
    /// <returns>The async result.</returns>
    public static IAsyncResult BeginLoadAllGridLayouts(string applicationName_, string gridName_,
      AsyncCallback callback_)
    {
      GridLayout[] layouts = null;
      ManualAsyncResult result = null;
      lock (_cache.SyncRoot)
      {
        string key = applicationName_ + "/" + gridName_;
        // Look up in cache first
        if (_listCache.ContainsKey(key))
        {
          //Build Async result and dummy Asynchroniser
          Asynchronizer async = new Asynchronizer(callback_, gridName_);
          result = new ManualAsyncResult(callback_, gridName_, async);
          layouts = BuildFromCache((IList)_listCache[key]);
        }
        else if (_waitingRequests.ContainsKey(key))
        {
          IList reqs = _waitingRequests[key] as IList;
          Asynchronizer async = new Asynchronizer(callback_, gridName_);
          result = new ManualAsyncResult(callback_, gridName_, async);
          reqs.Add(result);
          return result;
        }
        else
        {
          _waitingRequests[key] = new ArrayList();
        }
      }
      if (layouts != null)
      {
        result.SetMethodReturnedValue(layouts);
        return result;
      }

      Asynchronizer ssi = new Asynchronizer(callback_, gridName_);
      return ssi.BeginInvoke(new LoadAllGridLayoutsDelegate(LoadAllGridLayouts),
        new Object[] { applicationName_, gridName_ });
    }

    public static IAsyncResult BeginLoadAllGridLayoutsList(string applicationName_, string gridName_,
      PermissionType permissionType_, AsyncCallback callback_)
    {
      object cacheValue = null;
      ManualAsyncResult result = null;
      lock (_cache.SyncRoot)
      {
        string key = applicationName_ + "/" + gridName_ + ":" + permissionType_.ToString();
        // Look up in cache first
        if (_listCache.ContainsKey(key))
        {
          //Build Async result and dummy Asynchroniser
          Asynchronizer async = new Asynchronizer(callback_, gridName_);
          result = new ManualAsyncResult(callback_, gridName_, async);
          cacheValue = BuildGridLayoutList((IList)_listCache[key]);
        }
        else if (_waitingRequests.ContainsKey(key))
        {
          IList reqs = _waitingRequests[key] as IList;
          Asynchronizer async = new Asynchronizer(callback_, gridName_);
          result = new ManualAsyncResult(callback_, gridName_, async);
          reqs.Add(result);
          return result;
        }
        else
        {
          _waitingRequests[key] = new ArrayList();
        }
      }
      if (cacheValue != null)
      {
        result.SetMethodReturnedValue(cacheValue);
        return result;
      }

      Asynchronizer ssi = new Asynchronizer(callback_, gridName_);
      return ssi.BeginInvoke(new LoadAllGridLayoutsListDelegate(LoadAllGridLayoutsList),
        new Object[] { applicationName_, gridName_, permissionType_ });
    }

    /// <summary>
    /// Retrieves all grid layouts that match the application and grid name attributes.
    /// </summary> 
    /// <param name="result_">The astnc result</param>
    /// <returns>Array of gridlayout objects.</returns>
    public static GridLayout[] EndLoadAllGridLayouts(IAsyncResult result_)
    {
      AsynchronizerResult asr = (AsynchronizerResult)result_;
      return (GridLayout[])asr.AsynchronizerParent.EndInvoke(result_);
    }

    /// <summary>
    /// Retrieves all grid layouts that match the application and grid name attributes as a list (no layout details).
    /// </summary> 
    /// <param name="result_">The astnc result</param>
    /// <returns>Array of gridlayout objects.</returns>
    public static GridLayout[] EndLoadAllGridLayoutsList(IAsyncResult result_)
    {
      AsynchronizerResult asr = (AsynchronizerResult)result_;
      return (GridLayout[])asr.AsynchronizerParent.EndInvoke(result_);
    }

    /// <summary>
    /// Persists a particular grid layout for later retrieval.
    /// </summary>		
    public static IAsyncResult BeginSaveGridLayout(GridLayout gridLayout_, string userName, AsyncCallback callback_)
    {
      Asynchronizer ssi = new Asynchronizer(callback_, gridLayout_);
      return ssi.BeginInvoke(new SaveGridLayoutDelegate(SaveGridLayout),
        new Object[] { gridLayout_, userName });
    }

    /// <summary>
    /// Persists a particular grid layout for later retrieval.
    /// </summary>	
    public static void EndSaveGridLayout(IAsyncResult result_)
    {
      AsynchronizerResult asr = (AsynchronizerResult)result_;
      asr.AsynchronizerParent.EndInvoke(result_);
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Generates a data key to represent a given grid layout. Used for persisting
    /// user-grid layouts via DataServices.
    /// </summary>
    /// <param name="applicationName_">The current application name.</param>
    /// <param name="gridName_">The name of the grid whose layout is to be retrieved.</param>		
    /// <param name="layoutName_">The current layout name.</param>	
    /// <returns>The grid data key to store layout details against.</returns>
    private static string GenerateGridDataKey(string applicationName_, string gridName_, string layoutName_)
    {
      return string.Format("{0}/{1}/{2}", applicationName_, gridName_, layoutName_);
    }

    /// <summary>
    /// Creates a deep copy of a GridLayout array.
    /// </summary>
    /// <remarks>
    /// Used as a utility for creating a copy of a cache entry so GridLayouts in cache 
    /// will not be altered outside of the class
    /// </remarks>
    /// <param name="arr_">The array to be copied</param>
    /// <returns>The new copy.</returns>
    private static GridLayout[] CopyLayouts(GridLayout[] arr_)
    {
      GridLayout[] newArr = new GridLayout[arr_.Length];

      for (int i = 0; i < arr_.Length; ++i)
      {
        newArr[i] = GridLayout.Copy(arr_[i]);
      }

      return newArr;
    }



    #endregion
  }
}
