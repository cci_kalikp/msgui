//---------------------------------------------------------------------------
//
// Copyright (c) 2004 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
//---------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Xml.Serialization;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  /// <summary>
  /// A collection of ColumnLayout objects.
  /// </summary>	
  public class ColumnLayoutCollection : CollectionBase, ITypedList, IListSource, IBindingList
  {
    #region Constructors

    /// <summary>
    /// Initializes a new empty instance of the ColumnLayoutCollection class.
    /// </summary>
    public ColumnLayoutCollection()
    {
    }


    /// <summary>
    /// Initializes a new instance of the ColumnLayoutCollection class, containing elements
    /// copied from an array.
    /// </summary>
    /// <param name="items">
    /// The array whose elements are to be added to the new ColumnLayoutCollection.
    /// </param>
    public ColumnLayoutCollection(ColumnLayout[] items)
    {
      AddRange(items);
    }

    /// <summary>
    /// Initializes a new instance of the ColumnLayoutCollection class, containing elements
    /// copied from another instance of ColumnLayoutCollection
    /// </summary>
    /// <param name="items">
    /// The ColumnLayoutCollection whose elements are to be added to the new ColumnLayoutCollection.
    /// </param>
    public ColumnLayoutCollection(ColumnLayoutCollection items)
    {
      AddRange(items);
    }

    #endregion

    #region Public Properties

    [XmlElement("Columns")]
    public ColumnLayout[] Columns
    {
      get
      {
        ColumnLayout[] tempLayouts = new ColumnLayout[List.Count];
        List.CopyTo(tempLayouts, 0);
        return tempLayouts.OrderBy(column => column.Index).ToArray();
      }
    }

    [XmlIgnore]
    public IEnumerable<ColumnLayout> RequiredColumns
    {
      get { return Columns.Where(col => !(col.Hidden && col.SortDirection == ColumnLayout.SortDirections.None)); }
    }

    #endregion

    #region CollectionBase members

    /// <summary>
    /// Adds the elements of an array to the end of this ColumnLayoutCollection.
    /// </summary>
    /// <param name="items">
    /// The array whose elements are to be added to the end of this ColumnLayoutCollection.
    /// </param>
    public virtual void AddRange(ColumnLayout[] items)
    {
      foreach (ColumnLayout item in items)
      {
        List.Add(item);
      }
    }

    /// <summary>
    /// Adds the elements of another ColumnLayoutCollection to the end of this ColumnLayoutCollection.
    /// </summary>
    /// <param name="items">
    /// The ColumnLayoutCollection whose elements are to be added to the end of this ColumnLayoutCollection.
    /// </param>
    public virtual void AddRange(ColumnLayoutCollection items)
    {
      foreach (ColumnLayout item in items)
      {
        List.Add(item);
      }
    }

    /// <summary>
    /// Adds an instance of type ColumnLayout to the end of this ColumnLayoutCollection.
    /// </summary>
    /// <param name="value">
    /// The ColumnLayout to be added to the end of this ColumnLayoutCollection.
    /// </param>
    public virtual void Add(ColumnLayout value)
    {
      List.Add(value);
    }

    /// <summary>
    /// Determines whether a specfic ColumnLayout value is in this ColumnLayoutCollection.
    /// </summary>
    /// <param name="value">
    /// The ColumnLayout value to locate in this ColumnLayoutCollection.
    /// </param>
    /// <returns>
    /// true if value is found in this ColumnLayoutCollection;
    /// false otherwise.
    /// </returns>
    public virtual bool Contains(ColumnLayout value)
    {
      return List.Contains(value);
    }

    /// <summary>
    /// Return the zero-based index of the first occurrence of a specific value
    /// in this ColumnLayoutCollection
    /// </summary>
    /// <param name="value">
    /// The ColumnLayout value to locate in the ColumnLayoutCollection.
    /// </param>
    /// <returns>
    /// The zero-based index of the first occurrence of the _ELEMENT value if found;
    /// -1 otherwise.
    /// </returns>
    public virtual int IndexOf(ColumnLayout value)
    {
      return List.IndexOf(value);
    }

    /// <summary>
    /// Inserts an element into the ColumnLayoutCollection at the specified index
    /// </summary>
    /// <param name="index">
    /// The index at which the ColumnLayout is to be inserted.
    /// </param>
    /// <param name="value">
    /// The ColumnLayout to insert.
    /// </param>
    public virtual void Insert(int index, ColumnLayout value)
    {
      List.Insert(index, value);
    }

    object IList.this[int index]
    {
      get { return InnerList[index]; }
      set { InnerList[index] = value; }
    }

    /// <summary>
    /// Gets or sets the ColumnLayout at the given index in this ColumnLayoutCollection.
    /// </summary>
    public virtual ColumnLayout this[int index]
    {
      get { return (ColumnLayout) List[index]; }
      set { List[index] = value; }
    }

    /// <summary>
    /// Removes the first occurrence of a specific ColumnLayout from this ColumnLayoutCollection.
    /// </summary>
    /// <param name="value">
    /// The ColumnLayout value to remove from this ColumnLayoutCollection.
    /// </param>
    public virtual void Remove(ColumnLayout value)
    {
      List.Remove(value);
    }

    /// <summary>
    /// Type-specific enumeration class, used by ColumnLayoutCollection.GetEnumerator.
    /// </summary>
    public class Enumerator : IEnumerator
    {
      private IEnumerator wrapped;

      public Enumerator(ColumnLayoutCollection collection)
      {
        wrapped = ((CollectionBase) collection).GetEnumerator();
      }

      public ColumnLayout Current
      {
        get { return (ColumnLayout) (wrapped.Current); }
      }

      object IEnumerator.Current
      {
        get { return (ColumnLayout) (wrapped.Current); }
      }

      public bool MoveNext()
      {
        return wrapped.MoveNext();
      }

      public void Reset()
      {
        wrapped.Reset();
      }
    }

    /// <summary>
    /// Returns an enumerator that iterates through the <see cref="T:System.Collections.CollectionBase"/> instance.
    /// </summary>
    /// <returns>
    /// An <see cref="T:System.Collections.IEnumerator"/> for the <see cref="T:System.Collections.CollectionBase"/> instance.
    /// </returns>
    IEnumerator IEnumerable.GetEnumerator()
    {
      return GetEnumerator();
    }

    /// <summary>
    /// Returns an enumerator that can iteColumnLayout through the elements of this ColumnLayoutCollection.
    /// </summary>
    /// <returns>
    /// An object that implements System.Collections.IEnumerator.
    /// </returns>        
    public new virtual Enumerator GetEnumerator()
    {
      return new Enumerator(this);
    }

    #endregion

    #region CollectionBase Notification Overrides

    protected override void OnClearComplete()
    {
      if (ListChanged != null)
      {
        ListChangedEventArgs args = new ListChangedEventArgs(ListChangedType.Reset, -1, -1);
        ListChanged(this, args);
      }
    }


    protected override void OnInsertComplete(int index_, object value_)
    {
      if (ListChanged != null)
      {
        ListChangedEventArgs args = new ListChangedEventArgs(ListChangedType.ItemAdded, index_, -1);
        ListChanged(this, args);
      }
    }

    protected override void OnRemoveComplete(int index_, object value_)
    {
      if (ListChanged != null)
      {
        ListChangedEventArgs args = new ListChangedEventArgs(ListChangedType.ItemDeleted, index_, -1);
        ListChanged(this, args);
      }
    }


    protected override void OnSetComplete(int index_, object oldValue_, object newValue_)
    {
      if (ListChanged != null)
      {
        ListChangedEventArgs args = new ListChangedEventArgs(ListChangedType.ItemChanged, index_, -1);
        ListChanged(this, args);
      }
    }

    #endregion

    #region ITypedList Members

    public PropertyDescriptorCollection GetItemProperties(PropertyDescriptor[] pd_)
    {
      return TypeDescriptor.GetProperties(typeof (ColumnLayout));
    }

    public string GetListName(PropertyDescriptor[] pd_)
    {
      return "ColumnLayoutCollection";
    }

    #endregion

    #region IListSource Members

    public IList GetList()
    {
      return this;
    }

    public bool ContainsListCollection
    {
      get { return false; }
    }

    #endregion

    #region IBindList Members

    public event ListChangedEventHandler ListChanged;

    public object AddNew()
    {
      throw new NotSupportedException();
    }

    public bool AllowEdit
    {
      get { return true; }
    }

    public bool AllowNew
    {
      get { return false; }
    }

    public bool AllowRemove
    {
      get { return true; }
    }

    public bool SupportsChangeNotification
    {
      get { return true; }
    }

    public bool SupportsSearching
    {
      get { return false; }
    }

    public bool SupportsSorting
    {
      get { return false; }
    }

    public bool IsSorted
    {
      get { return false; }
    }

    public void ApplySort(PropertyDescriptor property, ListSortDirection direction)
    {
      throw new NotSupportedException();
    }

    public PropertyDescriptor SortProperty
    {
      get { return null; }
    }

    public int Find(PropertyDescriptor property, object key)
    {
      throw new NotSupportedException();
    }

    public void AddIndex(PropertyDescriptor property)
    {
      throw new NotSupportedException();
    }

    public ListSortDirection SortDirection
    {
      get { return new ListSortDirection(); }
    }

    public void RemoveSort()
    {
      throw new NotSupportedException();
    }

    public void RemoveIndex(PropertyDescriptor property)
    {
      throw new NotSupportedException();
    }

    #endregion
  }
}