using System;
using System.ComponentModel;
using System.Windows.Forms;
using Infragistics.Win.UltraWinProgressBar;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  /// <summary>
  /// Progress Bar-based User Control which provides simple
  /// auto-increment and auto-restart functionality to 
  /// give a "spinning" look to asyncronous processes.
  /// </summary>
  public partial class ProgressBarWrapper : UserControl
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="ProgressBarWrapper"/> class.
    /// </summary>
    public ProgressBarWrapper()
    {
      InitializeComponent();
    }

    private int _autoIncrementMilliseconds = 50;
    /// <summary>
    /// Gets or sets a value indicating how many milliseconds the progress bar should take before incrementing itself.
    /// </summary>
    [Browsable(true)]
    [Category("Behavior")]
    [Description("Auto-Increment Time")]
    [DefaultValue(50)]
    public int AutoIncrementMilliseconds
    {
      get { return _autoIncrementMilliseconds; }
      set { _autoIncrementMilliseconds = value; }
    }

    private bool _autoRestart = false;
    /// <summary>
    /// Gets or sets a value indicating whether the Progress Bar should Reatart from 0 and continue filling after reaching 100%.
    /// </summary>
    /// <value><c>true</c> if control should auto-restart; otherwise, <c>false</c>.</value>
    [Browsable(true)]
    [Category("Behavior")]
    [Description("Restart from 0 and continue after reaching 100%")]
    [DefaultValue(false)]
    public bool AutoRestart
    {
      get { return _autoRestart; }
      set { _autoRestart = value; }
    }

    private bool _hideWhenStopped;
    /// <summary>
    /// Gets or sets a value indicating whether the progress bar should hide itself when stopped.
    /// </summary>
    /// <value><c>true</c> if progress bar is hiddent when stopped; otherwise, <c>false</c>.</value>
    [Browsable(true)]
    [Description("Determines whether the progress bar should hide itself when stopped")]
    [Category("Behavior")]
    [DefaultValue(false)]
    public bool HideWhenStopped
    {
      get { return _hideWhenStopped; }
      set { _hideWhenStopped = value; }
    }

    /// <summary>
    /// Gets/Sets the contents of the label displayed for the Infragistics Progress Bar Control.
    /// See Infragistics UltraProgressBar documentation for specific values, or leave blank for no text.
    /// </summary>
    /// <value>The progress bar text.</value>
    [Browsable(true)]
    [Description("Determines the contents of the label displayed for the Infragistics Progress Bar Control")]
    [Category("Behavior")]
    public string ProgressBarText
    {
      get
      {
        return _progressBar.Text;
      }
      set
      {
        _progressBar.Text = value;
      }
    }

    /// <summary>
    /// Automatic Styling of Progress Bar
    /// </summary>
    public enum ProgressBarWrapperStyle
    {
      /// <summary>
      /// Default - No specific style
      /// </summary>
      None,
      /// <summary>
      /// Progress Bar for Asynchronous processes - Grows up to 100% and then sets to 0
      /// </summary>
      AsyncSpinner
    }

    private ProgressBarWrapperStyle _style = ProgressBarWrapperStyle.None;
    /// <summary>
    /// Gets/Sets the behavior for the Progress Bar Wrapper
    /// </summary>
    [Browsable(true)]
    [Description("Determines the behavior of the Progress Bar Control")]
    [DefaultValue(ProgressBarWrapperStyle.None)]
    [Category("Behavior")]
    public ProgressBarWrapperStyle Style
    {
      get
      {
        return _style;
      }
      set
      {
        _style = value;
        if (_style == ProgressBarWrapperStyle.AsyncSpinner)
        {
          _autoRestart = true;
          _hideWhenStopped = true;
          ProgressBarText = string.Empty;
        }
      }
    }

    /// <summary>
    /// Gets the progress bar instance for further customization.
    /// </summary>
    /// <value>The progress bar for further customization.</value>
    public UltraProgressBar ProgressBar
    {
      get { return _progressBar; }
      set { _progressBar = value; }
    }

    private bool _isRunning = false;

    #region Progress Bar Handling

    /// <summary>
    /// Starts the progress bar.
    /// </summary>
    public void Start()
    {
      if (!_isRunning)
      {
        _isRunning = true;
        if (_progressPercentage >= 100)
        {
          _progressPercentage = 0;
          UpdateProgressBar(_progressPercentage);
        }
        if (_hideWhenStopped)
        {
          Visible = true;
        }
        _timer.Interval = _autoIncrementMilliseconds;
        _timer.Tick += _timer_Tick;
        _timer.Start();
      }
    }

    private int _progressPercentage;
    private void _timer_Tick(object sender, EventArgs e)
    {
      if (_progressPercentage >= 100)
      {
        if (_autoRestart)
        {
          _progressPercentage = 0;
        }
        else
        {
          // no auto-restart, stop (fill) the progress bar
          Stop(true);
          return;
        }
      }
      else
      {
        _progressPercentage++;
      }
      UpdateProgressBar(_progressPercentage);
    }

    /// <summary>
    /// Stops the progress bar.
    /// </summary>
    public void Stop()
    {
      Stop(false);
    }

    /// <summary>
    /// Stops the progress bar.
    /// </summary>
    /// <param name="fill_">if set to <c>true</c> fill the progress bar; otherwise, clear it.</param>
    public void Stop(bool fill_)
    {
      if (_isRunning)
      {
        if (_hideWhenStopped)
        {
          Visible = false;
        }
        _timer.Tick -= _timer_Tick;
        _timer.Stop();
        // set progress percentage so that future runs use proper values
        _progressPercentage = fill_ ? 100 : 0;
        UpdateProgressBar(_progressPercentage);
        _isRunning = false;
      }
    }

    /// <summary>
    /// Empties the progress bar (sets the value to 0).
    /// </summary>
    public void Clear()
    {
      UpdateProgressBar(0);
    }

    /// <summary>
    /// Fills the progress bar (sets the value to 100).
    /// </summary>
    public void Fill()
    {
      UpdateProgressBar(100);
    }

    /// <summary>
    /// Sets the progress bar value (must be between 0 and 100)
    /// </summary>
    /// <param name="percentage_">The percentage (between 0 and 100) to set the value to.</param>
    public void UpdateProgressBar(int percentage_)
    {
      _progressBar.Value = percentage_;
    }

    /// <summary>
    /// Gets a value indicating whether the progress bar has been "started" and is running thru its <see cref="ProgressBarWrapperStyle.AsyncSpinner"/> cycle.
    /// </summary>
    /// <value>
    /// 	<c>true</c> if this instance is running; otherwise, <c>false</c>.
    /// </value>
    public bool IsRunning
    {
      get
      {
        return _isRunning;
      }
    }

    #endregion

    private void ProgressBarWrapper_Load(object sender, EventArgs e)
    {
      if (!Desktop.Utils.DesignerUtils.IsInDesignMode)
      {
        if (_hideWhenStopped && !_isRunning)
        {
          Visible = false;
        }
      }
    }

    private void ProgressBarWrapper_VisibleChanged(object sender, EventArgs e)
    {
      if (_hideWhenStopped && !_isRunning)
      {
        Visible = false;
      }
    }
  }
}