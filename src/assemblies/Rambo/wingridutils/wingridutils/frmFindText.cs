using System;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Infragistics.Win.UltraWinGrid;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  /// <summary>
  /// Form for specifying search parameters for the fields of an UltraGrid
  /// 
  /// </summary>
  
  public class frmFindText : NavigationUtilFormBase
  {
    #region Data Members

    

    // Search text
    private System.Windows.Forms.ComboBox _cboFindWhat;
    private System.Windows.Forms.Label _lblFindWhat;
    // Check options
    private System.Windows.Forms.CheckBox _chkMatchCase;
    private System.Windows.Forms.CheckBox _chkMatchHidden;
    private System.Windows.Forms.CheckBox _chkReverse;
    // Combo options
     private System.Windows.Forms.GroupBox _grpBxSearchParams; 
    private System.Windows.Forms.ComboBox _cboColumn;
    private System.Windows.Forms.Label _lblColumn;
    private System.Windows.Forms.ComboBox _cboMatch;
    private System.Windows.Forms.Label _lblMatch;
    // Buttons
    private System.Windows.Forms.Button _cmdCancel;
    private System.Windows.Forms.Button _cmdFindNext;

    // Initial search row and cell
    UltraGridRow _startRow;
    UltraGridCell _startCell;

    // Column Key of the column specific search
    string _column;

    // Whether or not to start searching rows during recursive search
    bool _searchRow;

    // The regular expression used for matching
    string _matchText;
    RegexOptions _matchOptions;
    Regex _matchRegEx;


    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.Container components = null;
    #endregion
   
    #region Constructor
    public frmFindText()
    {
      //
      // Required for Windows Form Designer support
      //
      InitializeComponent();

      PopulateColumnCombo();
      PopulateSearchContentCombo();

      _xmlSerializer = new FindTextParametersSerializer(new FindTextReader(), new FindTextWriter());
    }
    #endregion 

    #region Properties

    public override object State
    {
      get
      {
        FindTextParameters state = new FindTextParameters();
        state.CaseSensitive = this.CaseSensitive;
        state.ReverseSearch = this.ReverseSearch;
        state.SearchCollapsed = this.SearchCollapsed;
        state.SearchColumn = this.SearchColumn;
        state.Match = this.Match;
        state.SearchText = this.SearchText;

        return state;
      }

      set
      {
        if(value == null)
        {
          this.CaseSensitive = false;
          this.ReverseSearch = false;
          this.SearchCollapsed = false;
          this.Match = FindTextParameters.MatchType.AnyPartOfField;
          this.SearchColumn = FindTextParameters.ALL_COLUMNS;
        }
        else
        {
          FindTextParameters state = (FindTextParameters)value;
          this.CaseSensitive = state.CaseSensitive;
          this.ReverseSearch = state.ReverseSearch;
          this.SearchCollapsed = state.SearchCollapsed;
          this.Match = state.Match;
          this.SearchColumn = state.SearchColumn;
          this.SearchText = state.SearchText;
        }
      }
    }

    internal bool CaseSensitive
    {
      get { return _chkMatchCase.Checked; } 
      set { _chkMatchCase.Checked = value; }
    }

    internal bool ReverseSearch
    {
      get { return _chkReverse.Checked; }
      set { _chkReverse.Checked = value; }
    }

    internal bool SearchCollapsed
    {
      get { return _chkMatchHidden.Checked; }
      set { _chkMatchHidden.Checked = value; }
    }

    internal string SearchColumn
    {
      get 
      {
        string key = GetColumnKey(_cboColumn.Text);

        if(key == null || key == string.Empty)
        {
          return FindTextParameters.ALL_COLUMNS;
        }

        return key;
      }
      set
      {
        string caption = GetCaption(value);

        if(caption != null && _cboColumn.Items.Contains(caption))
        {
          _cboColumn.Text = caption;
        }
        else
        {
          _cboColumn.Text = FindTextParameters.ALL_COLUMNS;
        }
      }
    }

    internal FindTextParameters.MatchType Match
    {
      get { return (FindTextParameters.MatchType)_cboMatch.SelectedIndex; }
      set { _cboMatch.SelectedIndex = (int)value; } 
    }

    internal string SearchText
    {
      get { return _cboFindWhat.Text; }
      set { _cboFindWhat.Text = value; }
    }

    #endregion

    #region Dispose
    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    protected override void Dispose( bool disposing_ )
    {
      if( disposing_ )
      {
        if(components != null)
        {
          components.Dispose();
        }
      }
      base.Dispose( disposing_ );
    }
    #endregion

    #region Public Methods
    /// <summary>
    /// Populate the form with the current column names and make the form visible.  
    /// </summary>
    public override void ShowForm()
    {
      KeyPreview = true;

      PopulateColumnCombo();

      base.ShowForm();

      _cboFindWhat.Focus();
      _cboFindWhat.SelectAll();
    }

    /// <summary>
    /// Reset the UltraGrid object to be searched and updates the display info for the form 
    /// if it is not hidden. 
    /// </summary>
    public override void ResetGrid(UltraGrid grid_)
    {
      base.ResetGrid(grid_);

      if(Visible)
      {
        PopulateColumnCombo();
        Refresh();
      }
    } 
    
    #endregion

    #region Private Utilities
    /// <summary>
    /// Fills in _cboColumn with the current column caption of the grid.
    /// </summary>
    private void PopulateColumnCombo()
    {
      string selected = (string)_cboColumn.Text;

      _cboColumn.Items.Clear();
      _cboColumn.Items.Add(FindTextParameters.ALL_COLUMNS);

      string[] captions = ColumnCaptions;

      if(captions != null)
      {
        Array.Sort(captions);

        _cboColumn.Items.AddRange(captions);
         
        if(Array.IndexOf(captions,selected) >= 0)
          _cboColumn.SelectedItem = selected;
        else
          _cboColumn.SelectedIndex = 0;
      }
    }

    /// <summary>
    /// Fills in _cboMatch with the different matching options
    /// </summary>
    private void PopulateSearchContentCombo()
    {
      _cboMatch.Items.Clear();
      _cboMatch.Items.AddRange(new string[]{"Any part of field",
                                             "Whole field",
                                             "Start of field",
                                             "End of field"});

      _cboMatch.Tag = System.Enum.GetValues(typeof(FindTextParameters.MatchType));
      _cboMatch.SelectedIndex = 0;
    }

    /// <summary>
    /// Initiates the search, highlights and scrolls to cell if found, otherwise displays
    /// a message.
    /// </summary>
    private void ProcessSearch()
    { 
      UltraGridRow rowIter;
      bool found;

      if(this.SearchText == string.Empty)
        return;

      Cursor.Current = Cursors.WaitCursor;

      _column = this.SearchColumn;

      //   Add the search string to the combobox and limit its capacity to 10 items
      if ( ! _cboFindWhat.Items.Contains(this.SearchText) )
      {
        _cboFindWhat.Items.Insert(0, this.SearchText);
        if ( _cboFindWhat.Items.Count > 10 )
          _cboFindWhat.Items.RemoveAt(10);
      }


      // Don't bother searching if there is nothing to search
      if(_grid == null || _grid.Rows.Count == 0)
      {
        showNotFound();
        return;
      }
 
      _matchRegEx = CreateRegularExpression();
      _startRow = getBeginningOrEndRow();
      _startCell = _grid.ActiveCell;
      rowIter = _startRow;
      _searchRow = false;

      found = ReverseSearch ? ReverseDeepSearch(_startRow) : DeepSearch(_startRow);

      bool cancelled = false;

      // if not found then hit the end of the grid -- dialog to continue from top
      if(!found)
      {
        DialogResult result = MessageBox.Show(this._grid.FindForm(),
          "Search has reached end of table.\nContinue from the beginning?", 
          Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        
        if(result == DialogResult.Yes)
        {
          _startRow = getBeginningOrEndRow();
          _startCell = null;
          _searchRow = true;
          found = ReverseSearch ? ReverseDeepSearch(_startRow) : DeepSearch(_startRow);
        }
        else
        {
          cancelled = true;
        }

      }

      Cursor.Current = Cursors.Default;

      if(!cancelled)
      {
        // Go to the cell if found or show a message to indicate not found.
        if( found )
          highlightActiveCell();
        else
          showNotFound();
      }
    }

    /// <summary>
    /// Returns the top-most or bottom-most non-hidden depending on depending on the direction of the search 
    /// </summary>
    /// <returns></returns>
    private UltraGridRow getBeginningOrEndRow()
    {
      if(!this.ReverseSearch)
        return _grid.GetRow(ChildRow.First);
      else
      {
        UltraGridRow rowIter = _grid.GetRow(ChildRow.Last);
        while(rowIter.Expanded)
          rowIter = rowIter.GetChild(ChildRow.Last);

        return rowIter;
      }
    }

    /// <summary>
    /// Shows the "not found" message 
    /// </summary>
    private void showNotFound()
    {
      MessageBox.Show(this._grid.FindForm(), "The specified text was not found.", "Find Result",MessageBoxButtons.OK,
        MessageBoxIcon.Exclamation);
    }

    /// <summary>
    /// Recursive function for traversing through the grid given a starting row and finding the 
    /// search text in a cell.
    /// </summary>
    /// <param name="currRow_">The Current row that is being traversed and/or searched</param>
    /// <returns></returns>
    private bool DeepSearch(UltraGridRow currRow_)
    {
      // end the search
      if(currRow_ == null)
        return false;

      // Test for a match between search text and cell text
      if(SearchRow(currRow_))
        return true;
     
      // Depth-first search
      if(SearchChildren(currRow_) && DeepSearch(currRow_.GetChild(ChildRow.First)))
        return true;
     
      if(DeepSearch(currRow_.GetSibling(SiblingRow.Next)))
        return true; 
      
      return false;
    }

    private bool ReverseDeepSearch(UltraGridRow currRow_)
    {
      if(currRow_ == null)
        return false;

      if(ReverseDeepSearchHelper(currRow_))
        return true;

      if(currRow_.ParentRow != null && !(currRow_.ParentRow is UltraGridGroupByRow))
      {
        if(SearchRow(currRow_.ParentRow))
          return true;

        if(ReverseDeepSearch(currRow_.ParentRow.GetSibling(SiblingRow.Previous)))
          return true;
      }

      return false;
    }

    /// <summary>
    /// This searches an node childeren first, then siblings
    /// </summary>
    /// <param name="currRow_"></param>
    /// <returns></returns>
    private bool ReverseDeepSearchHelper(UltraGridRow currRow_)
    {
      // end the search
      if(currRow_ == null)
        return false;

      if(SearchChildren(currRow_) && ReverseDeepSearchHelper(currRow_.GetChild(ChildRow.Last)))
        return true;

      // Test for a match between search text and cell text
      if(SearchRow(currRow_))
        return true;
      
      if(ReverseDeepSearchHelper(currRow_.GetSibling(SiblingRow.Previous)))
        return true;

      return false;
    }

    private bool IsActiveRow(UltraGridRow currRow_)
    {
      // retruns true if the currRow is the active row or has the active cell
      return currRow_ == _grid.ActiveRow || 
        (_grid.ActiveCell != null && currRow_ == _grid.ActiveCell.Row);
    }

    private bool SearchChildren(UltraGridRow currRow_)
    {
      return !currRow_.Hidden && 
        !currRow_.IsFilteredOut &&
        currRow_.HasChild() && 
        (this.SearchCollapsed ||  currRow_.IsExpanded);
    }

    /// <summary>
    /// Returns true if a cell in Row_ matches the search text.
    /// As a side effect, sets the ActiveRow and ActiveCell of the grid if there is a match.
    /// </summary>
    private bool SearchRow(UltraGridRow Row_)
    {
      if ( Row_ == null || Row_.Hidden || Row_.IsFilteredOut)
        return false;

      if(_searchRow || IsActiveRow(Row_))
      {
        _searchRow = true;
      }
      else
      {
        // the search has not been initiated yet because we have not passed the point of 
        // where the search should start, ie this got called during the initial traversal.
        return false;
      }

      // Do not scan GroupBy rows
      if (Row_ is UltraGridGroupByRow)
      {
        return false;
      }

      string[] columnKeys;
      string key;
      int i, n, colIndex;
      UltraGridCell Cell;

      // If the column field matches a column, only look in state column
      // otherwise, assume searching all columns in layout
      columnKeys = getColumnKeys(Row_);
      if(_column != FindTextParameters.ALL_COLUMNS)
      {
        if(Array.IndexOf(columnKeys, _column) < 0)
          return false;

        columnKeys = new string[1];
        columnKeys[0] = _column;
      }

      // Iterate forward or backward through the columns
      n = columnKeys.Length;

      if(n <= 1)
      {
        // only searching one column
        i = 0;
      }
      else if(IsActiveRow(Row_) && _grid.ActiveCell != null)
      {
        // starting from the column of the currently selected cell
        i = _grid.ActiveCell.Column.Header.VisiblePositionWithinBand;
      }
      else
      {
        // start from the first/last cell depending on direction
        i = (!this.ReverseSearch ? 0 : n - 1);
      }
      
      for(;
        !this.ReverseSearch ? i < n : i >= 0;        
        i += !this.ReverseSearch ? 1 : -1)
      {
        // only look forward/backward of the startCell if we are in the startRow
        if(_startCell != null && Row_ == _startRow )
        {
          colIndex = Array.IndexOf(columnKeys, _startCell.Column.Key);

          if(!this.ReverseSearch)
          {
            // Is the column we are currently searching to the right of the _startCell
            if(colIndex >= i)
              continue;
          }
          else
          {
            // or if we are searching backwards, is it to the left?
            if(colIndex <= i)
              continue;
          }
        }
        
        key = columnKeys[i];
        Cell = Row_.Cells[key];

        if ( Cell != null && Cell != _grid.ActiveCell && Cell.Value != null && _matchRegEx.IsMatch(Cell.Text))
        {
          // The text was found! Set the active row and cell.
          _grid.ActiveRow = Row_;
          _grid.ActiveCell = Cell;
          return true;
        }      
      }
      
      return false;
    }

    private Regex CreateRegularExpression()
    {
      string exp = string.Empty;
      RegexOptions opts;
      string searchTxt = Regex.Escape(this.SearchText);
      
      opts = this.CaseSensitive ? RegexOptions.None : RegexOptions.IgnoreCase;

      switch(this.Match)
      {
        case FindTextParameters.MatchType.AnyPartOfField:
          exp = ".*" + searchTxt + ".*";
          break;

        case FindTextParameters.MatchType.WholeField:
          exp = "^" + searchTxt + "$";
          break;
      
        case FindTextParameters.MatchType.StartOfField:
          exp = "^" + searchTxt + ".*";
          break;

        case FindTextParameters.MatchType.EndOfField:
          exp = ".*" + searchTxt + "$";
          break;
      }

      if(_matchText == exp &&  _matchOptions == opts && _matchRegEx != null)
      {
        return _matchRegEx;
      }

      _matchText = exp;
      _matchOptions = opts;

      return new Regex(_matchText, _matchOptions);

    }
    #endregion

    #region Event Handlers
    private void Find_Click(object sender, System.EventArgs e)
    {
      if(_beforeNavigateHandler != null)
      {
        _beforeNavigateHandler(this, EventArgs.Empty);
      }

      ProcessSearch();

      if(_afterNavigateHandler != null)
      {
        _afterNavigateHandler(this, EventArgs.Empty);
      }
    }
    
    private void cmdCancel_Click(object sender, System.EventArgs e)
    {
      Hide();
    }

    protected override void UpdateComponents(object sender, System.EventArgs e)
    {
      PopulateColumnCombo();
      Refresh();
    }
    
    #endregion 
    
    #region Windows Form Designer generated code
    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this._chkMatchCase = new System.Windows.Forms.CheckBox();
      this._cboMatch = new System.Windows.Forms.ComboBox();
      this._lblMatch = new System.Windows.Forms.Label();
      this._cboColumn = new System.Windows.Forms.ComboBox();
      this._lblColumn = new System.Windows.Forms.Label();
      this._cmdCancel = new System.Windows.Forms.Button();
      this._cmdFindNext = new System.Windows.Forms.Button();
      this._cboFindWhat = new System.Windows.Forms.ComboBox();
      this._lblFindWhat = new System.Windows.Forms.Label();
      this._grpBxSearchParams = new System.Windows.Forms.GroupBox();
      this._chkMatchHidden = new System.Windows.Forms.CheckBox();
      this._chkReverse = new System.Windows.Forms.CheckBox();
      this._grpBxSearchParams.SuspendLayout();
      this.SuspendLayout();
      // 
      // _chkMatchCase
      // 
      this._chkMatchCase.Location = new System.Drawing.Point(8, 35);
      this._chkMatchCase.Name = "_chkMatchCase";
      this._chkMatchCase.Size = new System.Drawing.Size(96, 24);
      this._chkMatchCase.TabIndex = 2;
      this._chkMatchCase.TabStop = false;
      this._chkMatchCase.Text = "Match &case";
      // 
      // _cboMatch
      // 
      this._cboMatch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this._cboMatch.Location = new System.Drawing.Point(56, 44);
      this._cboMatch.Name = "_cboMatch";
      this._cboMatch.Size = new System.Drawing.Size(120, 21);
      this._cboMatch.TabIndex = 3;
      this._cboMatch.TabStop = false;
      // 
      // _lblMatch
      // 
      this._lblMatch.AutoSize = true;
      this._lblMatch.Location = new System.Drawing.Point(8, 52);
      this._lblMatch.Name = "_lblMatch";
      this._lblMatch.Size = new System.Drawing.Size(40, 13);
      this._lblMatch.TabIndex = 2;
      this._lblMatch.Text = "&Match:";
      // 
      // _cboColumn
      // 
      this._cboColumn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this._cboColumn.Location = new System.Drawing.Point(56, 12);
      this._cboColumn.Name = "_cboColumn";
      this._cboColumn.Size = new System.Drawing.Size(120, 21);
      this._cboColumn.TabIndex = 1;
      this._cboColumn.TabStop = false;
      // 
      // _lblColumn
      // 
      this._lblColumn.AutoSize = true;
      this._lblColumn.Location = new System.Drawing.Point(8, 20);
      this._lblColumn.Name = "_lblColumn";
      this._lblColumn.Size = new System.Drawing.Size(45, 13);
      this._lblColumn.TabIndex = 0;
      this._lblColumn.Text = "Co&lumn:";
      // 
      // _cmdCancel
      // 
      this._cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this._cmdCancel.Location = new System.Drawing.Point(326, 36);
      this._cmdCancel.Name = "_cmdCancel";
      this._cmdCancel.Size = new System.Drawing.Size(88, 23);
      this._cmdCancel.TabIndex = 8;
      this._cmdCancel.TabStop = false;
      this._cmdCancel.Text = "Close";
      this._cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
      // 
      // _cmdFindNext
      // 
      this._cmdFindNext.Location = new System.Drawing.Point(326, 7);
      this._cmdFindNext.Name = "_cmdFindNext";
      this._cmdFindNext.Size = new System.Drawing.Size(88, 23);
      this._cmdFindNext.TabIndex = 6;
      this._cmdFindNext.TabStop = false;
      this._cmdFindNext.Text = "Find &Next";
      this._cmdFindNext.Click += new System.EventHandler(this.Find_Click);
      // 
      // _cboFindWhat
      // 
      this._cboFindWhat.Location = new System.Drawing.Point(72, 8);
      this._cboFindWhat.Name = "_cboFindWhat";
      this._cboFindWhat.Size = new System.Drawing.Size(228, 21);
      this._cboFindWhat.TabIndex = 1;
      this._cboFindWhat.TabStop = false;
      // 
      // _lblFindWhat
      // 
      this._lblFindWhat.AutoSize = true;
      this._lblFindWhat.CausesValidation = false;
      this._lblFindWhat.Location = new System.Drawing.Point(5, 13);
      this._lblFindWhat.Name = "_lblFindWhat";
      this._lblFindWhat.Size = new System.Drawing.Size(56, 13);
      this._lblFindWhat.TabIndex = 0;
      this._lblFindWhat.Text = "&Find what:";
      // 
      // _grpBxSearchParams
      // 
      this._grpBxSearchParams.Controls.Add(this._cboColumn);
      this._grpBxSearchParams.Controls.Add(this._lblColumn);
      this._grpBxSearchParams.Controls.Add(this._cboMatch);
      this._grpBxSearchParams.Controls.Add(this._lblMatch);
      this._grpBxSearchParams.Location = new System.Drawing.Point(141, 37);
      this._grpBxSearchParams.Name = "_grpBxSearchParams";
      this._grpBxSearchParams.Size = new System.Drawing.Size(180, 72);
      this._grpBxSearchParams.TabIndex = 5;
      this._grpBxSearchParams.TabStop = false;
      this._grpBxSearchParams.Text = "Search";
      // 
      // _chkMatchHidden
      // 
      this._chkMatchHidden.Location = new System.Drawing.Point(8, 56);
      this._chkMatchHidden.Name = "_chkMatchHidden";
      this._chkMatchHidden.Size = new System.Drawing.Size(136, 28);
      this._chkMatchHidden.TabIndex = 3;
      this._chkMatchHidden.TabStop = false;
      this._chkMatchHidden.Text = "Search c&ollapsed rows";
      // 
      // _chkReverse
      // 
      this._chkReverse.Location = new System.Drawing.Point(8, 81);
      this._chkReverse.Name = "_chkReverse";
      this._chkReverse.Size = new System.Drawing.Size(116, 24);
      this._chkReverse.TabIndex = 4;
      this._chkReverse.TabStop = false;
      this._chkReverse.Text = "Search &up";
      // 
      // frmFindText
      // 
      this.AcceptButton = this._cmdFindNext;
      this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
      this.CancelButton = this._cmdCancel;
      this.CausesValidation = false;
      this.ClientSize = new System.Drawing.Size(417, 114);
      this.Controls.Add(this._cmdCancel);
      this.Controls.Add(this._cmdFindNext);
      this.Controls.Add(this._cboFindWhat);
      this.Controls.Add(this._lblFindWhat);
      this.Controls.Add(this._grpBxSearchParams);
      this.Controls.Add(this._chkMatchCase);
      this.Controls.Add(this._chkMatchHidden);
      this.Controls.Add(this._chkReverse);
      this.MaximizeBox = false;
      this.Name = "frmFindText";
      this.Text = "Find";
      this._grpBxSearchParams.ResumeLayout(false);
      this._grpBxSearchParams.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }
		#endregion

  }
}
