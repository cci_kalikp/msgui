using System;
using System.Collections ;
using System.Windows.Forms;
using System.Drawing;
using Infragistics.Win.UltraWinGrid;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  public  enum RowFontStyleEnum { Default, Smallest, Smaller, Medium, Larger, Largest } ;

  /// <summary>
  /// Menu item to apply selected font size for the grid.
  /// </summary>
  public class RowFontStyle
  {
    private static IDictionary ENUM_TO_ROWFONTSTYLE ; 
    private static IDictionary FONTPOINTTOENUM; 

    private readonly string _name;
    private readonly int    _columnHeight;
    private readonly float  _fontInPoints;

    static RowFontStyle()
    {
      ENUM_TO_ROWFONTSTYLE = new Hashtable() ;
			ENUM_TO_ROWFONTSTYLE[ RowFontStyleEnum.Default ] = null;
      ENUM_TO_ROWFONTSTYLE[ RowFontStyleEnum.Smallest ] = new RowFontStyle("Smallest",8,6F) ;
      ENUM_TO_ROWFONTSTYLE[ RowFontStyleEnum.Smaller ] = new RowFontStyle("Smaller",10,7F) ;
      ENUM_TO_ROWFONTSTYLE[ RowFontStyleEnum.Medium ] = new RowFontStyle("Medium",12,8.25F) ;
      ENUM_TO_ROWFONTSTYLE[ RowFontStyleEnum.Larger ] = new RowFontStyle("Larger",10,10F) ;
      ENUM_TO_ROWFONTSTYLE[ RowFontStyleEnum.Largest ] = new RowFontStyle("Largest",10,12F) ;

      FONTPOINTTOENUM = new Hashtable() ;
			FONTPOINTTOENUM[ 0F ] = RowFontStyleEnum.Default  ;
      FONTPOINTTOENUM[ 6F ] = RowFontStyleEnum.Smallest  ;
      FONTPOINTTOENUM[ 7F ] = RowFontStyleEnum.Smaller  ;
      FONTPOINTTOENUM[ 8.25F ] = RowFontStyleEnum.Medium  ;
      FONTPOINTTOENUM[ 10F ] = RowFontStyleEnum.Larger  ;
      FONTPOINTTOENUM[ 12F ] = RowFontStyleEnum.Largest  ;

    }

    public RowFontStyle(string name_, int columnHeight_, float fontInPoints_) 
    {
      _name         = name_ ;
      _columnHeight = columnHeight_;
      _fontInPoints = fontInPoints_;
    }

    public int ColumnHeight
    {
      get
      {
        return ( _columnHeight )  ;
      }
    }

    public float FontInPoints
    {
      get
      {
        return ( _fontInPoints ) ;
      }
    }

    public static void UpdateGridByFontStyle(UltraGrid grid_, RowFontStyleEnum style_) 
    {
			if(style_ == RowFontStyleEnum.Default) return;

      Graphics g = grid_.CreateGraphics();
      RowFontStyle style = ENUM_TO_ROWFONTSTYLE[ style_ ] as RowFontStyle ;

      float fontSize = style.FontInPoints;
      // calculates required space for text and uses ColumnHeight
      // to fine-tune total row height to minimize remaining frame
      int rowHeight = Convert.ToInt32(72.0F / g.DpiY * fontSize) + style.ColumnHeight;

      grid_.DisplayLayout.Override.RowAppearance.FontData.SizeInPoints = fontSize;
      grid_.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = fontSize;
      grid_.DisplayLayout.Override.MinRowHeight = rowHeight;
      grid_.DisplayLayout.Override.DefaultRowHeight = rowHeight;
      g.Dispose();
    }

    public static RowFontStyleEnum GetEnumFromGrid( UltraGrid grid_ )
    {
      float points = grid_.DisplayLayout.Override.RowAppearance.FontData.SizeInPoints ;

      RowFontStyleEnum val = (RowFontStyleEnum)FONTPOINTTOENUM[ points ];
      return ( val ) ;
    }
  }
}
