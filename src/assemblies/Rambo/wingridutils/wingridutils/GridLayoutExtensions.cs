﻿using System;
using System.Linq;
using System.Collections.Generic;
using MorganStanley.MSDesktop.Rambo.DataDictionary;

namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  public static class GridLayoutExtensions
  {
    public static void AddColumns(this GridLayout gridLayout_, IEnumerable<SourceField> sourceFields_)
    {
      foreach (SourceField sourceField in sourceFields_)
      {
        AddColumn(gridLayout_, sourceField);
      }
    }

    public static void AddColumn(this GridLayout gridLayout_, SourceField sf_)
    {
      ColumnLayoutCollection columnLayoutCollection = gridLayout_.Columns;
      if (columnLayoutCollection == null)
      {
        throw new ArgumentException("GridLayout doesn't have column layout collection");
      }

      IEnumerable<ColumnLayout> columnLayouts = columnLayoutCollection.Cast<ColumnLayout>();
      if (columnLayouts.Any(c => c.Key == sf_.Id)) return;

      var columnLayout = new ColumnLayout
                           {
                             Caption = sf_.ShortDescription,
                             Category = sf_.CategoryName,
                             DataType = sf_.DataType,
                             Description = sf_.LongDescription,
                             Format = sf_.Format,
                             Hidden = false,
                             Key = sf_.Id,
                             ReadOnly = true,
                             Width = 50,
                             Index = columnLayoutCollection.Cast<ColumnLayout>().Max(c => c.Index) + 1,
                           };

      columnLayoutCollection.Add(columnLayout);
    }
  }
}
