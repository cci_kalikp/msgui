//---------------------------------------------------------------------------
//
// Copyright (c) 2004 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
//---------------------------------------------------------------------------

using System;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Collections;
using System.Collections.Specialized;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win;
using System.ComponentModel;
using System.Drawing;
using MorganStanley.MSDesktop.Rambo.Desktop.Utils;
using System.Windows.Forms;
namespace MorganStanley.MSDesktop.Rambo.WinGridUtils
{
  /// <summary>
  /// Provides functionality to manipulate and re-load existing DataGrid layouts.
  /// </summary>
  internal sealed class UltraGridLayoutFactory : AbstractGridLayoutFactory
  {
    private static ILogger _log = LoggerManager.GetLogger(typeof(UltraGridLayoutFactory));
    private UltraGrid m_grid;
    private bool _hideNewColumns = true;


    #region Constructors

    /// <summary>
    /// The UltraGrids-specific implementation of the AbstractGridLayoutFactory class.
    /// </summary>
    /// <remarks>Performs operations on the supplied UltraGrid to generate GridLayout objects that
    /// can be manipulated independent of the original grid source.</remarks>
    /// <param name="grid_">An UltraGrid data grid to act as the underlying grid source.</param>
    public UltraGridLayoutFactory(UltraGrid grid_)
    {
      m_grid = grid_;
      InitializeDefaultValues();
    }

    #endregion

    #region Public Properties

    /// <summary>
    /// Sets the underlying DataGrid the factory uses to create GridLayout objects
    /// </summary>
    public override object Grid
    {
      set { m_grid = (UltraGrid)value; }
    }
    /// <summary>
    /// Sets whether new columns added to the layout are hidden (defaulted to true) 
    /// </summary>
    override public bool HideNewColumns
    {
      get
      {
        return this._hideNewColumns;
      }
      set
      {
        this._hideNewColumns = value;
      }
    }
    #endregion

    #region Public Methods

    /// <summary>
    /// Returns an XML representation of the Grid provided.
    /// </summary>		
    /// <param name="applicationName_">The current application name.</param>	
    /// <param name="gridName_">The name of the current grid.</param>	
    /// <param name="layoutName_">The current layout name.</param>
    /// <param name="omitKeys_">A collection of columns keys to omit from the grid layout.</param>
    /// <param name="band_">The grid band the layout applies to</param>
    /// <returns>An XML representation of the grid provided.</returns>
    public override GridLayout GetGridLayout(string applicationName_, string gridName_, string layoutName_, StringCollection omitKeys_, int band_)
    {
      UltraGridLayoutManager layout = new UltraGridLayoutManager(m_grid, applicationName_, gridName_, layoutName_, omitKeys_, band_);
      return layout.GridLayout;
    }

    /// <summary>
    /// Returns an XML representation of the Grid provided.
    /// </summary>		
    /// <param name="applicationName_">The current application name.</param>	
    /// <param name="gridName_">The name of the current grid.</param>	
    /// <param name="layoutName_">The current layout name.</param>
    /// <param name="omitKeys_">A collection of columns keys to omit from the grid layout.</param>
    /// <returns>An XML representation of the grid provided.</returns>
    public override GridLayout GetGridLayout(string applicationName_, string gridName_, string layoutName_, StringCollection omitKeys_)
    {
      UltraGridLayoutManager layout = new UltraGridLayoutManager(m_grid, applicationName_, gridName_, layoutName_, omitKeys_);
      return layout.GridLayout;
    }

    /// <summary>
    /// Returns an XML representation of the Grid provided.
    /// </summary>		
    /// <param name="applicationName_">The current application name.</param>
    /// <param name="gridName_">The name of the current grid.</param>		
    /// <param name="layoutName_">The current layout name.</param>
    /// <returns>An XML representation of the grid provided.</returns>
    public override GridLayout GetGridLayout(string applicationName_, string gridName_, string layoutName_)
    {
      UltraGridLayoutManager layout = new UltraGridLayoutManager(m_grid, applicationName_, gridName_, layoutName_, null);
      return layout.GridLayout;
    }


    /// <summary>
    /// Loads grid format information into the associated grid.
    /// </summary>		
    /// <param name="xmlLayout_">The XML format data required to update the grid.</param>		
    /// <param name="startDepth_">The depth to start layout application within a hierichial grid.</param>
    /// <param name="applySort_">When set to true applies column sort operations.</param>
    /// <param name="applyColumnWidths_">When set to true applies column widths.</param>
    public override void ApplyGridLayout(GridLayout xmlLayout_, int startDepth_,
      bool applySort_, bool applyColumnWidths_)
    {
      int length = m_grid.DisplayLayout.Bands.Count - startDepth_;
      ApplyGridLayout(xmlLayout_, startDepth_, length, applySort_, applyColumnWidths_, ApplyCaptions);
    }

    /// <summary>
    /// Loads grid format information into the associated grid.
    /// </summary>		
    /// <param name="xmlLayout_">The XML format data required to update the grid.</param>		
    /// <param name="startDepth_">The depth to start layout application within a hierichial grid.</param>
    /// <param name="length_">The number of bands to apply the layout starting at the specified depth.</param>
    /// <param name="applySort_">When set to true applies column sort operations.</param>
    /// <param name="applyColumnWidths_">When set to true applies column widths.</param>
    /// <param name="applyColumnCaption_">When set to true applies column Caption.</param>
    public override void ApplyGridLayout(GridLayout xmlLayout_, int startDepth_, int length_,
      bool applySort_, bool applyColumnWidths_, bool applyColumnCaption_)
    {
      try
      {
        if (GridLayoutFactory.AllowGridLevelSetting)
        {
          RowFontStyle.UpdateGridByFontStyle(m_grid, xmlLayout_.Font);
        }
        //first, hide all grid columns
        int endDepth = startDepth_ + length_;
        for (int i = startDepth_; i < endDepth && i < m_grid.DisplayLayout.Bands.Count; ++i)
        {
          UltraGridBand band = m_grid.DisplayLayout.Bands[i];
          foreach (UltraGridColumn col in band.Columns)
          {
            col.Hidden = HideNewColumns;
          }
        }

        SortColumns(ref xmlLayout_);
        ColumnLayout[] sortedColumns = new ColumnLayout[xmlLayout_.Columns.Count];

        ResetFixedColumns(startDepth_, endDepth);

        // Apply the layout information to the grid supplied
        for (int i = 0; i < xmlLayout_.Columns.Count; ++i)
        {
          LoadUltradGridColumnLayout(xmlLayout_.Columns[i], sortedColumns, startDepth_, length_, applySort_, applyColumnWidths_, applyColumnCaption_);
        }

        SetFixedColumns(xmlLayout_, startDepth_, endDepth);

        if (applySort_)
        {
          SetSorting(sortedColumns, startDepth_, endDepth);
        }
      }
      catch (Exception ex)
      {
        _log.Error("ApplyGridLayout", ex.Message, ex);
      }
    }

    /// <summary>
    /// Applies sorting properties to the the associated grid.
    /// </summary>		
    /// <param name="gridLayout_">The XML format data required to update the grid.</param>		
    /// <param name="startDepth_">The depth to start layout application within a hierichial grid.</param>
    public override void ApplySort(GridLayout gridLayout_, int startDepth_)
    {
      int length = m_grid.DisplayLayout.Bands.Count - startDepth_;
      this.ApplySort(gridLayout_, startDepth_, length);
    }

    /// <summary>
    /// Applies sorting properties to the the associated grid.
    /// </summary>		
    /// <param name="gridLayout_">The XML format data required to update the grid.</param>		
    /// <param name="startDepth_">The depth to start layout application within a hierichial grid.</param>
    /// <param name="length_">The number of bands to apply the layout starting at the specified depth.</param>
    public override void ApplySort(GridLayout gridLayout_, int startDepth_, int length_)
    {
      this.SetSorting(this.GetSortedColumns(gridLayout_), startDepth_, startDepth_ + length_);
    }

    /// <summary>
    /// Applies sorting properties to the the associated grid.
    /// </summary>		
    /// <param name="gridLayout_">The XML format data required to update the grid.</param>		
    /// <param name="startDepth_">The depth to start layout application within a hierichial grid.</param>
    public override void ApplyColumnWidths(GridLayout gridLayout_, int startDepth_)
    {
      int length = m_grid.DisplayLayout.Bands.Count - startDepth_;
      this.ApplyColumnWidths(gridLayout_, startDepth_, length);
    }

    /// <summary>
    /// Applies sorting properties to the the associated grid.
    /// </summary>		
    /// <param name="gridLayout_">The XML format data required to update the grid.</param>		
    /// <param name="startDepth_">The depth to start layout application within a hierichial grid.</param>
    /// <param name="length_">The number of bands to apply the layout starting at the specified depth.</param>
    public override void ApplyColumnWidths(GridLayout gridLayout_, int startDepth_, int length_)
    {
      this.SetColumnWidths(gridLayout_, startDepth_, startDepth_ + length_);
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Loads grid column layout data into the associated UltraGrid.
    /// </summary>		
    /// <param name="colLayout_">The ColumnLayout containing formatting information.</param>
    private void LoadUltradGridColumnLayout(ColumnLayout colLayout_, ColumnLayout[] sortedColumns_, int startDepth_, bool applySort_, bool applyColumnWidths_, bool applyColumnCaption_)
    {
      LoadUltradGridColumnLayout(colLayout_, sortedColumns_, startDepth_, m_grid.DisplayLayout.Bands.Count - startDepth_, applySort_, applyColumnWidths_, applyColumnCaption_);
    }

    /// <summary>
    /// Loads grid column layout data into the associated UltraGrid.
    /// </summary>		
    /// <param name="colLayout_">The ColumnLayout containing formatting information.</param>
    private void LoadUltradGridColumnLayout(ColumnLayout colLayout_, ColumnLayout[] sortedColumns_,
      int startDepth_, int length_, bool applySort_, bool applyColumnWidths_, bool applyColumnCaption_)
    {
      for (int i = startDepth_; i < startDepth_ + length_; ++i)
      {
        try
        {
          string key = colLayout_.Key;

          UltraGridBand band = m_grid.DisplayLayout.Bands[i];
          if (!band.Columns.Exists(key))
          {
            continue;
          }

          UltraGridColumn ugColumn = band.Columns[key];



          //Displaying text with correct Font , size, and style
          if (colLayout_.Font != null)
          {
            ugColumn.CellAppearance.FontData.Name = colLayout_.Font.Name;
            ugColumn.CellAppearance.FontData.Italic = colLayout_.Font.Italic ? Infragistics.Win.DefaultableBoolean.True : Infragistics.Win.DefaultableBoolean.False;
            ugColumn.CellAppearance.FontData.Bold = colLayout_.Font.Bold ? Infragistics.Win.DefaultableBoolean.True : Infragistics.Win.DefaultableBoolean.False;
            ugColumn.CellAppearance.FontData.Underline = colLayout_.Font.Underline ? Infragistics.Win.DefaultableBoolean.True : Infragistics.Win.DefaultableBoolean.False;
            ugColumn.CellAppearance.FontData.SizeInPoints = colLayout_.Font.SizeInPoints;
          }
          else
          {
            ugColumn.CellAppearance.ResetFontData();
          }

          if (applyColumnCaption_)
          {
            string caption = colLayout_.Caption;


            int lines = caption.Split('\n').Length;

            if (lines > 1 && ugColumn.Band.ColHeaderLines != lines)
            {
              ugColumn.Band.ColHeaderLines = lines;
            }


            if (ugColumn.Header.Caption != caption)
              ugColumn.Header.Caption = caption;
          }
          ugColumn.Format = colLayout_.Format;
          ugColumn.Hidden = colLayout_.Hidden;
          ugColumn.Header.Fixed = false;
          if (ugColumn.Header.VisiblePosition != colLayout_.Index)
            ugColumn.Header.VisiblePosition = colLayout_.Index;

          if (colLayout_.BackColorValue != 0)
          {
            ugColumn.CellAppearance.BackColor = Color.FromArgb(colLayout_.BackColorValue);
          }
          else
          {
            ugColumn.CellAppearance.ResetBackColor();
          }

          if (colLayout_.ForeColorValue != 0)
          {
            ugColumn.CellAppearance.ForeColor = Color.FromArgb(colLayout_.ForeColorValue);
          }
          else
          {
            ugColumn.CellAppearance.ResetForeColor();
          }


          if (colLayout_.SortOrder >= 0 && colLayout_.SortDirection != ColumnLayout.SortDirections.None)
          {
            sortedColumns_[colLayout_.SortOrder] = colLayout_;
          }

          /*
           * Need to ensure column widths are applied from the layout, but that the zero-eth 
           * column is not expanded each time you move down a band. For this reason, for column
           * zero, do not copy the width across for bands other than band[0].
           */
          if (applyColumnWidths_ && ugColumn.Width != colLayout_.Width &&
            !(ugColumn.Header.VisiblePosition == 0 && i > 0))
          {
            ugColumn.Width = colLayout_.Width;
          }
          if (colLayout_.HeaderTag != null)
          {
            ugColumn.Header.Tag = colLayout_.HeaderTag;
          }
        }
        catch (Exception ex)
        {
          _log.Error("LoadUltradGridColumnLayout", ex.Message, ex);
        }
      }
    }

    private ColumnLayout[] GetSortedColumns(GridLayout gridLayout_)
    {
      ColumnLayout[] sortedColumns = new ColumnLayout[gridLayout_.Columns.Count];

      foreach (ColumnLayout colLayout in gridLayout_.Columns)
      {
        if (colLayout.SortOrder >= 0 && colLayout.SortDirection != ColumnLayout.SortDirections.None)
        {
          sortedColumns[colLayout.SortOrder] = colLayout;
        }
      }

      return sortedColumns;
    }

    /// <summary>
    /// Sets the number of locked columns for the grid using the supplied GridLayout.
    /// </summary>		
    private void SetFixedColumns(GridLayout xmlLayout_, int startDepth_, int endDepth_)
    {
      int lockedColumns = xmlLayout_.LockedColumns;

      if (lockedColumns == 0)
      {
        m_grid.DisplayLayout.UseFixedHeaders = false;
        return;
      }

      m_grid.DisplayLayout.UseFixedHeaders = true;
      m_grid.DisplayLayout.Override.FixedHeaderIndicator = FixedHeaderIndicator.None;

      for (int j = startDepth_; j < endDepth_ && j < m_grid.DisplayLayout.Bands.Count; ++j)
      {
        UltraGridBand band = m_grid.DisplayLayout.Bands[j];
        for (int i = 0; i < xmlLayout_.Columns.Count; ++i)
        {
          if (band.Columns.Exists(xmlLayout_.Columns[i].Key))
          {

            UltraGridColumn col = band.Columns[xmlLayout_.Columns[i].Key];

            if (col.Header.VisiblePosition < lockedColumns)
            {
              col.Header.Fixed = true;
            }
            else
            {
              col.Header.Fixed = false;
            }
          }
        }

      }
    }

    private void ResetFixedColumns(int startDepth_, int endDepth_)
    {
      for (int j = startDepth_; j < endDepth_ && j < m_grid.DisplayLayout.Bands.Count; ++j)
      {
        UltraGridBand band = m_grid.DisplayLayout.Bands[j];

        for (int i = band.Columns.Count - 1; i >= 0; --i)
        {
          band.Columns[i].Header.ResetFixed();
        }
      }
    }

    private void SortColumns(ref GridLayout gridlayout_)
    {
      ColumnLayout[] colLayouts = new ColumnLayout[gridlayout_.Columns.Count];
      ArrayList list = new ArrayList(gridlayout_.Columns);
      list.Sort(new ColumnLayoutIndexComparer());
      list.CopyTo(colLayouts, 0);
      gridlayout_.Columns = new ColumnLayoutCollection(colLayouts);
    }

    /// <summary>
    /// Iterates through the supplied list of ordered sort-by fields and applies to the Factory's
    /// underlying grid.
    /// </summary>		
    private void SetSorting(ColumnLayout[] sortedColumns_, int startDepth_, int endDepth_)
    {
      for (int i = 0; i < sortedColumns_.Length; ++i)
      {
        if (sortedColumns_[i] == null) return;
        ColumnLayout col = sortedColumns_[i];

        for (int j = startDepth_; j < endDepth_ && j < m_grid.DisplayLayout.Bands.Count; ++j)
        {
          string key = col.Key;
          UltraGridBand band = m_grid.DisplayLayout.Bands[j];
          if (i == 0)
          {
            band.SortedColumns.Clear();
          }
          if (band.Columns.Exists(key))
          {
            band.Columns[key].SortComparer = new GridColumnComparer(col.SortAbsolute);
            band.SortedColumns.Add(band.Columns[key], col.SortDirection == ColumnLayout.SortDirections.Descending);
          }
        }
      }
    }

    private void SetColumnWidths(GridLayout xmlLayout_, int startDepth_, int endDepth_)
    {
      try
      {
        SortColumns(ref xmlLayout_);

        for (int c = 0; c < xmlLayout_.Columns.Count; ++c)
        {
          ColumnLayout colLayout = xmlLayout_.Columns[c];

          for (int i = startDepth_; i < endDepth_; ++i)
          {
            string key = colLayout.Key;

            UltraGridBand band = m_grid.DisplayLayout.Bands[i];
            if (!band.Columns.Exists(key))
            {
              continue;
            }

            UltraGridColumn ugColumn = band.Columns[key];

            /*
               * Need to ensure column widths are applied from the layout, but that the zero-eth 
               * column is not expanded each time you move down a band. For this reason, for column
               * zero, do not copy the width across for bands other than band[0].
               */
            if (ugColumn.Width != colLayout.Width &&
              !(ugColumn.Header.VisiblePosition == 0 && i > 0))
            {
              ugColumn.Width = colLayout.Width;
            }
          }
        }
      }
      catch (Exception ex)
      {
        _log.Error("SetColumnWidths", ex.Message, ex);
      }
    }

    #endregion

  }
}
