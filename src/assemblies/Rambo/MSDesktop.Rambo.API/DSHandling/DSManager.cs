﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

using MorganStanley.MSDesktop.Rambo.DataServices;
using MorganStanley.MSDesktop.Rambo.Desktop.Utils;
using MorganStanley.MSDotNet.MSNet;

namespace MorganStanley.MSDesktop.Rambo.API
{
  public delegate void DSRequestCallbackDelegate(DSResponse result_);

  public class DSManager
  {
    #region Fields

    private static readonly ILogger Log = new Logger(typeof(DSManager));
    private IDataService _service;
    private IDataKey _key;
    //private IVersionDataKey _versionKey;
    private ISubscription _subscription;
    private DSRequestCallbackDelegate _callbackDelegate;

    #endregion

    #region Properties

    internal IDataService Service { get { return _service; } }

    internal IDataKey Key { get { return _key; } }

    /// <summary>
    /// <para>Result value of the async call</para>
    /// <para>Need to use only if the common way - using the callback method - isn't appropriate</para>
    /// </summary>
    internal IAsyncResult AsyncResult { get; set; }
    /// <summary>
    /// <para>Response object of the finished DS call</para>
    /// <para>Need to use only if the common way - using the callback method - isn't appropriate</para>
    /// </summary>
    internal DSResponse CallbackResponse { get; set; }

    #endregion

    #region Prepare Data Service Calls

    private static DSManager PrepareDSCall(string serviceName_, DSRequestCallbackDelegate callbackDelegate_)
    {
      // Initilise DSManager before first use it
      IMSNetLoop loop = MSNetLoopPoolImpl.Instance();

      if (!DataServiceManager.IsInitialised())
      {
        DataServiceManager.Init(loop);
      }

      DSManager manager = new DSManager
      {
        _callbackDelegate = callbackDelegate_,
        _service = DataServiceManager.GetDataService(serviceName_)
      };

      return manager;
    }

    private static DSManager PrepareDSAttributeCall(string serviceName_, IDictionary attributes_, DSRequestCallbackDelegate callbackDelegate_)
    {
      DSManager manager = PrepareDSCall(serviceName_, callbackDelegate_);

      manager._subscription = manager._service.CreateSubscription(attributes_);

      return manager;
    }

    private static DSManager PrepareDSKeyCall(string serviceName_, string keyString_, DSRequestCallbackDelegate callbackDelegate_)
    {
      DSManager manager = PrepareDSCall(serviceName_, callbackDelegate_);
      manager._key = DataKeyFactory.BuildDataKey(keyString_);

      manager._subscription = manager._service.CreateSubscription(manager._key);

      return manager;
    }

    #endregion

    #region Static Methods

    internal static DSManager BeginGetFieldValues(ColumnName columnName_, DSRequestCallbackDelegate callback_)
    {
      // Get column name
      string columnName = columnName_.ToString();
      // Convert first character to lower case
      columnName = columnName.Substring(0, 1).ToLower() + columnName.Substring(1);

      // Call DS
      DSManager manager = PrepareDSKeyCall(Constants.RamboFieldValueListDS, columnName, callback_);
      Log.Info("GetResource", "Getting field value list of " + columnName);
      manager.AsyncResult = manager._service.BeginSnapshot(manager._subscription, manager.EndDSCall<RamboFieldValueList>, manager._key);
      return manager;
    }

    private static IDictionary GetAttributes(string userName_,
                                              string applicationName_,
                                              string resourceType_,
                                              string appletName_,
                                              string resourceCategory_,
                                              string profileName_,
                                              string resourceName_)
    {
      var attributes = new Dictionary<string, string>();
      if (!String.IsNullOrEmpty(userName_)) attributes.Add(ColumnName.RamboUserid.ToString(), userName_);
      if (!String.IsNullOrEmpty(applicationName_)) attributes.Add(ColumnName.RamboApplication.ToString(), applicationName_);
      if (!String.IsNullOrEmpty(resourceType_)) attributes.Add(ColumnName.RamboResType.ToString(), resourceType_);
      if (!String.IsNullOrEmpty(appletName_)) attributes.Add(ColumnName.RamboApplet.ToString(), appletName_);
      if (!String.IsNullOrEmpty(resourceCategory_)) attributes.Add(ColumnName.RamboResCategory.ToString(), resourceCategory_);
      if (!String.IsNullOrEmpty(profileName_)) attributes.Add(ColumnName.RamboProfile.ToString(), profileName_);
      if (!String.IsNullOrEmpty(resourceName_)) attributes.Add(ColumnName.RamboResName.ToString(), resourceName_);
      return attributes;
    }

    internal static DSManager BeginGetResourceList(string userName_,
                                                    string applicationName_,
                                                    string resourceType_,
                                                    string appletName_,
                                                    string resourceCategory_,
                                                    string profileName_,
                                                    string resourceName_,
                                                    DSRequestCallbackDelegate callback_)
    {
      var attributes = GetAttributes(userName_, applicationName_, resourceType_, appletName_, resourceCategory_, profileName_, resourceName_);
      DSManager manager = PrepareDSAttributeCall(Constants.RamboResourceListDS, attributes, callback_);
      Log.Info("BeginGetResourceList", "Getting resource list");
      manager.AsyncResult = manager._service.BeginSnapshot(manager._subscription, manager.EndDSCall<RamboResourceList>, manager._key);
      return manager;
    }

    internal static DSResponse GetResourceList(string userName_,
                                              string applicationName_,
                                              string resourceType_,
                                              string appletName_,
                                              string resourceCategory_,
                                              string profileName_,
                                              string resourceName_)
    {
      var attributes = GetAttributes(userName_, applicationName_, resourceType_, appletName_, resourceCategory_, profileName_, resourceName_);
      DSManager manager = PrepareDSAttributeCall(Constants.RamboResourceListDS, attributes, null);
      Log.Info("GetResourceList", "Getting resource list");
      IDictionary snapshot = manager._service.GetSnapshot(manager._subscription);
      var reader = GetReaderForSnaphot(snapshot);
      object snapshotResult = null;
      if (reader != null) snapshotResult = CommonBase<RamboResourceList>.Deserialize(reader);
      return new DSResponse(snapshotResult != null, snapshotResult);
    }


    internal static RamboResource GetResource(string dataServiceName_, string resourceID_)
    {
      DSManager manager = PrepareDSKeyCall(dataServiceName_, resourceID_, null);
      IDictionary snapshot = null;
      try
      {
        Log.Info("GetResource", "Getting resource with key: " + resourceID_);
        snapshot = manager._service.GetSnapshot(manager._subscription);
      }
      catch (Exception exception)
      {
        Log.Error("GetResource", exception.Message);
      }
      XmlReader reader = GetReaderForSnaphot(snapshot);

      if (reader != null)
      {
        Log.Info("GetResource", "Getting resource finished successfully");
        return RamboResource.ReadXml(reader);
      }
      return null;
    }
    internal static DSManager BeginGetResource(string resourceID_, DSRequestCallbackDelegate callback_)
    {
      DSManager manager = PrepareDSKeyCall(Constants.RamboResourceDS, resourceID_, callback_);
      Log.Info("BeginGetResource", "Getting resource with key: " + resourceID_);
      manager.AsyncResult = manager._service.BeginSnapshot(manager._subscription, manager.EndDSCall<RamboResource>, manager._key);
      return manager;
    }

    internal static DSManager BeginSaveResource(String key, RamboResource resource_, DSRequestCallbackDelegate callback_)
    {
      DSManager manager = PrepareDSKeyCall(Constants.RamboResourceDS, key, callback_);
      Log.Info("BeginSaveResource", "Saving resource with key: " + key);
      manager.AsyncResult = manager._service.BeginPublish(new XMLDataObject(manager.Key, resource_.XmlDataWithParameters.DocumentElement), manager.EndDSCall<EmptyClass>, key);
      return manager;
    }

    #endregion

    #region Callback handling

    private static XmlReader GetReaderForSnaphot(IDictionary snapshot_)
    {
      if (snapshot_ != null)
      {
        foreach (object key in snapshot_.Keys)
        {
          DataEvent dataEvent = (DataEvent)snapshot_[key];
          if (dataEvent != null)
          {
            XMLDataObject dataobject = dataEvent.DataObject as XMLDataObject;

            if (dataobject != null)
            {
              XmlElement resultElement = dataobject.Value;
              return new XmlNodeReader(resultElement);
            }
          }
        }
      }

      return null;
    }

    /// <summary>
    /// Callback method to process DS callback result and create the response object
    /// </summary>
    /// <typeparam name="T">Target type of the deserialization</typeparam>
    /// <param name="result_">Result of the asnyc callback</param>
    internal void EndDSCall<T>(IAsyncResult result_) where T : class
    {
      CallbackResponse = null;
      try
      {
        T snapshotResult = null;
        IDictionary snapshot = _service.EndSnapshot(result_);
        XmlReader reader = GetReaderForSnaphot(snapshot);

        if (typeof(T) == typeof(EmptyClass))
        {
          string message = result_.AsyncState as String;
          snapshotResult = new EmptyClass(message) as T;
        }
        else
        {
          if (reader != null)
          {
            if (typeof(T) == typeof(RamboResource))
            {
              snapshotResult = RamboResource.ReadXml(reader) as T;
            }
            else
            {
              snapshotResult = CommonBase<T>.Deserialize(reader);
            }
          }
        }

        CallbackResponse = new DSResponse(snapshotResult != null, snapshotResult);
      }
      catch (Exception ex_)
      {
        Exception returnException = ex_;
        string message = ex_.Message + "\n\n" + ex_.StackTrace;
        if (ex_.InnerException != null)
        {
          returnException = ex_.InnerException;
          message = ex_.InnerException.Message + "\n\n" + ex_.InnerException.StackTrace;
        }
        Log.Error("EndDSCall<" + typeof(T) + ">", message);
        CallbackResponse = new DSResponse(this, returnException);
      }
      finally
      {
        if (_callbackDelegate != null)
        {
          DataServiceManager.NetLoop.InvokeLater(_callbackDelegate, CallbackResponse);
        }
      }
    }

    #endregion
  }
}