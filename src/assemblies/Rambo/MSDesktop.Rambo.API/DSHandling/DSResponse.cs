﻿using System;

namespace MorganStanley.MSDesktop.Rambo.API
{
  /// <summary>
  /// Contains the result of the DS call
  /// </summary>
  public class DSResponse
  {
    #region Fields

    private readonly bool _isSuccessful;
    private readonly object _response;

    #endregion

    #region Constructors

    /// <summary>
    /// Create DSResponse instance with given successful indicator and null response object
    /// </summary>
    /// <param name="isSuccessful_">Indicates if the response was successful</param>
    internal DSResponse(bool isSuccessful_)
    {
      _isSuccessful = isSuccessful_;
      _response = null;
    }

    /// <summary>
    /// Create DSResponse instance with given successful inidcator and response object
    /// </summary>
    /// <param name="isSuccessful_">Indicates if the response was successful</param>
    /// <param name="response_">Specify the response object of the data service call</param>
    internal DSResponse(bool isSuccessful_, object response_)
    {
      _isSuccessful = isSuccessful_;
      _response = response_;
    }

    /// <summary>
    /// Create a DSResponse instance if an exception was returned from the data service
    /// </summary>
    /// <param name="manager_">DSManager instance which handled the DS call</param>
    /// <param name="exception_">Original exception object what returned from the DS call</param>
    internal DSResponse(DSManager manager_, Exception exception_)
    {
      _isSuccessful = false;

      Exception customException = new Exception("Operation failed", exception_);
      // Add data service name and key to the exception
      customException.Data.Add("ServiceID", manager_.Service.Id);
      customException.Data.Add("Key", manager_.Key.KeyString);
      _response = customException;
    }

    #endregion

    #region Instance Properties

    /// <summary>
    /// Indicates it the data service request was successful
    /// </summary>
    public bool IsSuccessful
    {
      get { return _isSuccessful; }
    }

    /// <summary>
    /// <para>Gets the return object of the data service call</para>
    /// <para>Return value is an exception if there was any problem during the DS call</para>
    /// </summary>
    public object Response
    {
      get { return _response; }
    }

    #endregion
  }
}