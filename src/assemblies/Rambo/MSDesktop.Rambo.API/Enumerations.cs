﻿namespace MorganStanley.MSDesktop.Rambo.API
{
  /// <summary>
  /// Column names in Rambo database
  /// </summary>
  public enum ColumnName
  {
      RamboId,
      RamboUserid,
      RamboApplication,
      RamboProfile,
      RamboApplet,
      RamboResType,
      RamboResCategory,
      RamboResName,
      RamboIsDeleted,
      RamboModifyBy,
      RamboModifyTime,
      IsDeleted,
     //TODO: dhruvp - The following need to be deleted.
      RamboGridName,
      RamboGridLayout
  }
 
  internal enum DSKeyParts
  {
    Application,
    Applet,
    ConfigName,
    GridName,
    GridLayoutName,
    Profile,
    UserName
  }

  internal enum CommandType
  {
    Delete,
    Clone
  }

  internal enum WriteableResourceProperties
  {
    UserName,
    Profile,
    GridName,
    GridLayout
  }
}