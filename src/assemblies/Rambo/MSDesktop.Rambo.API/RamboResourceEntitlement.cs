﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using JetBrains.Annotations;
using MorganStanley.MSDesktop.Rambo.DataServices;
using MorganStanley.MSDesktop.Rambo.Desktop.Utils;
using MorganStanley.MSDesktop.Rambo.Desktop.Utils.Extensions;
using MorganStanley.MSDesktop.Rambo.API.DataObjects;
using MorganStanley.MSDotNet.Directory;
using System.Collections.Concurrent;

namespace MorganStanley.MSDesktop.Rambo.API
{
  /// <summary>
  /// Class to check if user have write permission for RamboResource
  /// </summary>
  public class RamboResourceEntitlement
  {
    #region Readonly & Static Fields

    private readonly IDataService _ds;
    private static readonly ILogger _log = LoggerManager.GetLogger(typeof (RamboResourceEntitlement));
    // -- boqwang
    // ported from  2796039 by tranph
    //private readonly IDictionary<string, IDictionary<string, string>> _ramboResourcePermision =
    //  new Dictionary<string, IDictionary<string, string>>();
    private readonly ConcurrentDictionary<string, IDictionary<string, string>> _ramboResourcePermision =
      new ConcurrentDictionary<string, IDictionary<string, string>>();
    private readonly XmlSerializer _serializer = new XmlSerializer(typeof (RamboSharing));
    private const string XPATH_RAMBO_GROUP = "RamboGroups/RamboGroup";
    private const string NODE_RAMBO_SHARING = "RVRamboSharing";
    private const string NODE_RAMBO_GROUP = "RamboGroups";

    #endregion

    #region Constructors

    public RamboResourceEntitlement()
    {
      _ds = DataServiceFactoryLayer.GetDataService(Constants.RAMBO_SHARING_DS);
    }

    #endregion

    #region Instance Methods

    /// <summary>
    /// return the permission groups
    /// </summary>
    [CanBeNull]
    public IDictionary<string, string> GetPermission(string ramboKey_)
    {
      IDictionary<string, string> groups = GetWriteGroupFromCache(ramboKey_);
      if (groups == null) // get groups from data service if it's no in cache.
      {
        XmlElement xml = GetXmlFromService(ramboKey_);
        if (xml == null) return null;
        groups = DeserializeXml(xml, ramboKey_.Trim());
      }

      _log.Info("GetPermission", string.Format("Get write permission for {0}", ramboKey_));
      return groups;
    }

    /// <summary>
    /// Check if user has write permission for rambo resource
    /// </summary>
    /// <param name="ramboKey_"></param>
    /// <param name="userId_"></param>
    public bool HasPermission(string ramboKey_, string userId_)
    {
      IDictionary<string, string> groups = GetPermission(ramboKey_);
      if (groups == null) return false;
      return IfUserInGroup(groups, userId_);
    }

    /// <summary>
    /// Grant Permission for Rambo Write group
    /// </summary>
    /// <param name="key_"></param>
    /// <param name="groups_"></param>
    /// <returns></returns>
    public void GrantPermission(string key_, IDictionary<string, string> groups_)
    {
      UpdateRamboSharingGroups(key_, groups_);
    }

    public void DeletePermissions(string key_, IDictionary<string, string> groups_)
    {
      UpdateRamboSharingGroups(key_, groups_, true);
    }

    private void UpdateRamboSharingGroups(string key_, IDictionary<string, string> groups_, bool isDeleted_ = false)
    {
      if (groups_ == null || groups_.Count == 0) return;

      var doc = new XmlDocument();
      XmlElement root = doc.CreateElement(NODE_RAMBO_SHARING);
      doc.AppendChild(root);
      XmlElement groups = doc.CreateElement(NODE_RAMBO_GROUP);
      root.AppendChild(groups);

      foreach (KeyValuePair<string, string> group in groups_)
      {
        var sharing = new RamboSharing { RamboKey = key_, GroupName = group.Key, GroupType = group.Value, IsDeleted = isDeleted_ };
        sharing.WriteXml(groups);
      }

      var stringGroups = new string[groups_.Count];
      groups_.Keys.CopyTo(stringGroups, 0);
      _log.Info("UpdateRamboSharingGroups",
                isDeleted_
                  ? string.Format("Deleting {0} Write Permission for {1}", string.Join(",", stringGroups), key_)
                  : string.Format("Granting {0} Write Permission for {1}", string.Join(",", stringGroups), key_));

      IDataObject dataObject = new XMLDataObject(new StringDataKey(key_), doc.DocumentElement);
      _ds.Publish(dataObject);
      UpdateCache(key_, groups_, isDeleted_);
    }

    /// <summary>
    /// Check if user has write permission for rambo resource
    /// </summary>
    public void ClearCache()
    {
      _ramboResourcePermision.Clear();
    }

    /// <summary>
    /// Check if user has write permission for rambo resource
    /// </summary>
    public void UpdateCache(string key_, IDictionary<string, string> groups_, bool isDeleted_ = false)
    {
      IDictionary<string, string> currentgroups;
      if (_ramboResourcePermision.TryGetValue(key_, out currentgroups))
      {
        if (isDeleted_)
        {
          groups_.ForEach(group => currentgroups.Remove(group));
        }
        else
        {
          foreach (KeyValuePair<string, string> group in groups_)
          {
            if(!currentgroups.ContainsKey(group.Key))
            {
              currentgroups.Add(group);
            }
          }          
        }        
      }
      _ramboResourcePermision.TryAdd(key_, groups_);
    }

    /// <summary>
    /// Get the write groups from cache for ramboo resource
    /// </summary>
    /// <param name="key_"></param>
    private IDictionary<string, string> GetWriteGroupFromCache(string key_)
    {
      if (_ramboResourcePermision == null) return null;

      IDictionary<string, string> groups;
      _ramboResourcePermision.TryGetValue(key_, out groups);
      return groups;
    }

    /// <summary>
    /// Build the dictionary from xml result
    /// </summary>
    /// <param name="xml_"></param>
    /// <param name="ramboKey_"></param>
    private IDictionary<string, string> DeserializeXml([NotNull] XmlElement xml_, string ramboKey_)
    {
      IDictionary<string, string> groups = new Dictionary<string, string>();
      XmlNodeList nodes = xml_.SelectNodes(XPATH_RAMBO_GROUP);
      if (nodes == null) return groups;

      foreach (XmlNode node in nodes)
      {
        var reader = new XmlNodeReader(node);
        var ramboSharing = _serializer.Deserialize(reader) as RamboSharing;

        if (ramboSharing != null && ramboSharing.RamboKey == ramboKey_ && !string.IsNullOrEmpty(ramboSharing.GroupName) && !groups.ContainsKey(ramboSharing.GroupName))
        {
          groups.Add(ramboSharing.GroupName, ramboSharing.GroupType);
        }
      }

      _ramboResourcePermision.TryAdd(ramboKey_, groups);
      return groups;
    }

    /// <summary>
    /// Get RamboSharing xml from RVRamboSharing data service
    /// </summary>
    /// <param name="key_"></param>
    private XmlElement GetXmlFromService(string key_)
    {
      IDataKey key = DataKeyFactory.BuildDataKey(key_);
      DataEvent result = _ds.GetSnapshot(key);
      return result == null ? null : (XmlElement) result.DataObject.Value;
    }

    #endregion

    #region Static Methods

    /// <summary>
    /// Check if current user is in write groups
    /// ASG person can do every action.
    /// </summary>
    /// <param name="groups_"></param>
    /// <param name="userId_"></param>
    public static bool IfUserInGroup([NotNull]IDictionary<string, string> groups_, string userId_)
    {
      Person person = UserVerification.GetPerson(userId_);
      Group asggroup = UserVerification.GetGroup(Constants.ASG_MAIL_GROUP);
      if (UserVerification.IsInGroup(person, asggroup)) return true;

      foreach (KeyValuePair<string, string> personGroup in groups_)
      {
        switch (personGroup.Value)
        {
          case Constants.USER_TYPE:
            if (personGroup.Key == userId_) return true;
            break;
          case Constants.GROUP_TYPE:
            Group group = UserVerification.GetGroup(personGroup.Key);
            if (UserVerification.IsInGroup(person, group)) return true;
            break;
        }
      }
      return false;
    }

    public static bool HasOwnerPermission(string owner_)
    {
      return owner_ == Environment.UserName || UserVerification.IsThisUserInGroup(Constants.ASG_MAIL_GROUP);
    }
      
    public static bool CanCurrentUserUpdateResource()
    {
      return CanUpdateResource(Environment.UserName, DSEnvironment.Environment);
    }
     
    public static bool CanUpdateResource(string user_, DSEnvironment.DSEnv env_)
    {
      if (env_ != DSEnvironment.DSEnv.Prod)
      {
        return !DSEnvironment.HasPotentialProdOverride();
      }

      return DirectoryHandler.IsInASGMailGroup(user_);
    }

    #endregion

  }
}
