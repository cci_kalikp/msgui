﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.Remoting.Messaging;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using JetBrains.Annotations;
using MorganStanley.MSDesktop.Rambo.Desktop.Utils;
using MorganStanley.MSDesktop.Rambo.API.DataObjects;
using MorganStanley.MSDesktop.Rambo.API.View;
using MorganStanley.MSDesktop.Rambo.WpfUtils.Core.Commands;
using MorganStanley.MSDotNet.Directory;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;

namespace MorganStanley.MSDesktop.Rambo.API.ViewModel
{
  public class CopyResourceViewModel : ObservableObject
  {
    private readonly ICopyResourceWindow _copyResourceWindow;
    private readonly Dispatcher _dispatcher = Dispatcher.CurrentDispatcher;
    private static readonly ILogger _log = LoggerManager.GetLogger(typeof(CopyResourceViewModel));
    private IDictionary<string, string> _parameters;
    private IDictionary<string, EditableParameterValues> _editableParams;
    private RamboResource _resource;

    private const string USER_NAME = "UserName";
    private const string APPLICATION = "Application";
    private const string RESOURCE_TYPE = "ResourceType";
    private const string RESOURCE_CATEGORY = "ResourceCategory";
    private const string APPLET_NAME = "AppletName";
    private const string PROFILE = "Profile";
    private const string RESOURCE_NAME = "ResourceName";

    #region Properties

    public IDictionary<string, string> Parameters
    {
      get { return _parameters; }

      set
      {
        if (_parameters == value) return;
        _parameters = value;
        InvokePropertyChanged(() => Parameters);
      }
    }

    public IDictionary<string, EditableParameterValues> EditableParameters
    {
      get { return _editableParams; }
      set
      {
        if (_editableParams == value) return;
        _editableParams = value;
        InvokePropertyChanged(() => EditableParameters);
      }
    }

    public ICommand CopyCommand { get; private set; }

    #endregion

    #region Constructors

    public CopyResourceViewModel([NotNull] ICopyResourceWindow copyResourceWindow_)
    {
      if (copyResourceWindow_ == null) throw new ArgumentNullException("copyResourceWindow_");

      _copyResourceWindow = copyResourceWindow_;
      CopyCommand = new RelayCommand(ExecuteCopyCommand);
    }

    #endregion

    #region Methods

    public void Run(RamboResource resource_)
    {
      _resource = resource_;
      _parameters = new Dictionary<string, string>
                      {
                        {USER_NAME, resource_.UserName},
                        {APPLICATION, resource_.ApplicationName},
                        {RESOURCE_TYPE, resource_.ResourceType},
                        {RESOURCE_CATEGORY, resource_.ResourceCategory},
                        {APPLET_NAME, resource_.AppletName},
                        {PROFILE, resource_.Profile},
                        {RESOURCE_NAME, resource_.ResourceName}
                      };

      switch (resource_.ResourceType)
      {
        case "AppPreference":
        case "Configurator":
          _editableParams = new Dictionary<string, EditableParameterValues>
                              {
                                {USER_NAME, new EditableParameterValues {CurrentValue = resource_.UserName}}
                              };
          break;
        case "AppScreen":
        case "CustomGrouping":
        case "GridFilter":
        case "GridGrouping":
        case "GridLayout":
        case "RangeGrouping":
          _editableParams = new Dictionary<string, EditableParameterValues>
                              {
                                {USER_NAME, new EditableParameterValues {CurrentValue = resource_.UserName}},
                                {RESOURCE_NAME, new EditableParameterValues {CurrentValue = resource_.ResourceName}}
                              };
          break;
        case "ScenarioAnalysisSetting":
          _editableParams = new Dictionary<string, EditableParameterValues>
                              {
                                {RESOURCE_NAME, new EditableParameterValues {CurrentValue = resource_.ResourceName}}
                              };
          break;
      }

      _copyResourceWindow.Model = this;
      _copyResourceWindow.ShowModal();
    }

    internal void ExecuteCopyCommand(object o_)
    {
      // validate params
      foreach (string key in _editableParams.Keys)
      {
        if (string.IsNullOrEmpty(_editableParams[key].NewValue))
        {
          TaskDialog.ShowMessage(string.Format("Please specify the new value for {0}", key), "Invalid input",
                          TaskDialogCommonButtons.Close,
                          VistaTaskDialogIcon.Warning);
          return;
        }

        if (key == USER_NAME)
        {
          using (FWD2Search search = new FWD2Search())
          {
            search.Connect(Constants.LdapAccessDn, Constants.LdapAccessPassword, null);
            Person person = search.GetPersonByLogon(_editableParams[key].NewValue);
            if (person == null)
            {
              ShowCopyResourceMessage(ResourceCopyState.InvalidUserName);
              return;
            }
          }
        }
      }

      Func<ResourceCopyState> validateResourceDelegate = ValidateSourceResource;
      validateResourceDelegate.BeginInvoke(DoneValidateResource, null);
    }

    private ResourceCopyState ValidateSourceResource()
    {
      // get the source resource
      _resource = RamboAPI.GetResource(Constants.RamboResourceDS, _resource.UserName, _resource.ApplicationName,
                                         _resource.Profile, _resource.AppletName,
                                         _resource.ResourceType, _resource.ResourceCategory, _resource.ResourceName);
      if (_resource == null)
        return ResourceCopyState.Failed; 

      // set new values for the parameters
      foreach (string key in _editableParams.Keys)
      {
        PropertyInfo property = _resource.GetType().GetProperty(key);
        if (property != null)
          property.SetValue(_resource, _editableParams[key].NewValue, null);
        else
        {
          _log.Error("ValidateSourceResource", string.Format("rambo resource property '{0}' not exist", key));
          return ResourceCopyState.Failed;
        }
      }
      
      // check if target already exists
      var target = RamboAPI.GetResource(Constants.RamboResourceDS, _resource.UserName, _resource.ApplicationName,
                                        _resource.Profile, _resource.AppletName,
                                        _resource.ResourceType, _resource.ResourceCategory, _resource.ResourceName);
      if (target != null)
      {
        return ResourceCopyState.OnOverwrite; 
      }

      return ResourceCopyState.OnAddNew;    
      
    }

    private void DoneValidateResource(IAsyncResult result_)
    {
      if (!_dispatcher.CheckAccess())
      {
        Action<IAsyncResult> doneValidateDelegate = DoneValidateResource;
        _dispatcher.BeginInvoke(DispatcherPriority.Normal, doneValidateDelegate, result_);
        return;
      }

      var dlg = (Func<ResourceCopyState>) (((AsyncResult) result_).AsyncDelegate);
      try
      {
        ResourceCopyState state = dlg.EndInvoke(result_);
        switch (state)
        {
          case ResourceCopyState.OnOverwrite:
            if (TaskDialogSimpleResult.Yes == ShowCopyResourceMessage(ResourceCopyState.OnOverwrite))
            {
              RamboAPI.SaveResource(_resource, DoneCopyResource);
            }
            break;
          case ResourceCopyState.OnAddNew:
            if (TaskDialogSimpleResult.Yes == ShowCopyResourceMessage(ResourceCopyState.OnAddNew))
            {
              RamboAPI.SaveResource(_resource, DoneCopyResource);
            }
            break;
          case ResourceCopyState.Failed:
            ShowCopyResourceMessage(ResourceCopyState.Failed);
            break;
        }
      }
      catch (Exception ex)
      {
        _log.Error("DoneValidateResource", ex.Message, ex);
        ShowCopyResourceMessage(ResourceCopyState.Failed);
      }
    }

    private void DoneCopyResource(DSResponse response_)
    {
      if (!_dispatcher.CheckAccess())
      {
        Action<DSResponse> doneCopyDelegate = DoneCopyResource;
        _dispatcher.BeginInvoke(DispatcherPriority.Normal, doneCopyDelegate, response_);
        return;
      }

      if (response_ != null && response_.IsSuccessful)
      {
        ShowCopyResourceMessage(ResourceCopyState.Copied);
      }
      else
      {
        ShowCopyResourceMessage(ResourceCopyState.Failed);
      }

      _copyResourceWindow.CloseView();
    }

    private TaskDialogSimpleResult ShowCopyResourceMessage(ResourceCopyState state_)
    {
      switch (state_)
      {
        case ResourceCopyState.InvalidUserName:
          return TaskDialog.ShowMessage("Please input a valid username", "Invalid Username", TaskDialogCommonButtons.Close,
                                 VistaTaskDialogIcon.Warning);
        case ResourceCopyState.OnAddNew:
          return TaskDialog.ShowMessage("Your are going to add a new resource. Do you want to proceed?", "Copy Resource",
                                 TaskDialogCommonButtons.YesNo, VistaTaskDialogIcon.Warning);
        case ResourceCopyState.OnOverwrite:
          return TaskDialog.ShowMessage("Resource with the same properties already exists. Do you want to overwrite?",
                                 "Copy Resource", TaskDialogCommonButtons.YesNo, VistaTaskDialogIcon.Warning);
        case ResourceCopyState.Copied:
          return TaskDialog.ShowMessage("Resource is successfully copied.", "Copy Resource", TaskDialogCommonButtons.Close,
                                 VistaTaskDialogIcon.Information);
        case ResourceCopyState.Failed:
          return TaskDialog.ShowMessage("Failed to copy resource", "Copy Resource", TaskDialogCommonButtons.Close, VistaTaskDialogIcon.Error);
      }
      return TaskDialogSimpleResult.None;
    }

    #endregion
  }
}