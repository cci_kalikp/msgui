﻿using System;
using System.Windows.Input;
using JetBrains.Annotations;
using MorganStanley.MSDesktop.Rambo.Desktop.Utils;
using MorganStanley.MSDesktop.Rambo.WpfUtils.Core.Commands;

namespace MorganStanley.MSDesktop.Rambo.API.ViewModel
{
  public class UserOrGroupViewModel : ObservableObject
  {
    private string _userOrGroup;
    public event EventHandler<EventArgs> Deleted;

    public UserOrGroupViewModel([NotNull] string userOrGroup_)
    {      
      if (userOrGroup_ == null) throw new ArgumentNullException("userOrGroup_");
      _userOrGroup = userOrGroup_;

      Delete = new RelayCommand(ExecuteDeleteCommand);
    }

    public string UserOrGroup 
    {
      get { return _userOrGroup;  }

      set
      {
        if (_userOrGroup == value) return;        
        _userOrGroup = value;
        InvokePropertyChanged(() => UserOrGroup);                  
      }
    }

    public ICommand Delete { get; private set; }

    private void ExecuteDeleteCommand(object o_)
    {
      var tmp = Deleted;
      if(tmp != null)
      {
        tmp(this, EventArgs.Empty);
      }
    }
  }
}
