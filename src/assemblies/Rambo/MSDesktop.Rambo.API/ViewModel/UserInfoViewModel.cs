﻿using System;
using System.Diagnostics;
using System.Runtime.Remoting.Messaging;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using JetBrains.Annotations;
using MorganStanley.MSDesktop.Rambo.Desktop.Utils;
using MorganStanley.MSDesktop.Rambo.API.DataAccess;
using MorganStanley.MSDesktop.Rambo.API.View;
using MorganStanley.MSDesktop.Rambo.WpfUtils.Core.Commands;
using MorganStanley.MSDesktop.Rambo.WpfUtils.Core.ViewModel;
using MorganStanley.MSDotNet.Directory;
using MorganStanley.WinAurora;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;

namespace MorganStanley.MSDesktop.Rambo.API.ViewModel
{
  public class UserInfoViewModel : ObservableObject
  {
    #region Fields

    private readonly UserInfoWindow _userInfoWindow;
    private readonly IErrorReportWindowView _errorReportWindowView;
    private readonly Dispatcher _dispatcher = Dispatcher.CurrentDispatcher;
    private static readonly ILogger _log = LoggerManager.GetLogger(typeof(UserInfoViewModel));

    private string _userInfo;
    private string _userName;
    private string _environment;

    #endregion

    #region Properties

    public ICommand LoadCommand { get; private set; }

    public string UserInfo
    {
      get { return _userInfo; }

      set
      {
        if (_userInfo == value) return;
        _userInfo = value;
        InvokePropertyChanged(() => UserInfo);
      }
    }

    public string UserName
    {
      get { return _userName; }

      set
      {
        if (_userName == value) return;
        _userName = value;
        InvokePropertyChanged(() => UserName);
      }
    }

    #endregion

    #region Constructors

    public UserInfoViewModel([NotNull] UserInfoWindow userInfoWindow_, [NotNull] IErrorReportWindowView errorReportWindowView_)
    {
      if (userInfoWindow_ == null) throw new ArgumentNullException("userInfoWindow_");
      if (errorReportWindowView_ == null) throw new ArgumentNullException("errorReportWindowView_");

      _userInfoWindow = userInfoWindow_;
      _errorReportWindowView = errorReportWindowView_;

      LoadCommand = new RelayCommand(ExecuteLoadCommand);
    }

    #endregion

    #region Methods

    public void Run(string environment_)
    {
      _environment = environment_;
      _userInfoWindow.Model = this;
      _userInfoWindow.ShowModal();
    }

    internal void ExecuteLoadCommand(object o_)
    {
      if (!string.IsNullOrEmpty(_userName))
      {

        FWD2Search _fwd2search = new FWD2Search(); ;
        _fwd2search.Connect(Constants.LdapAccessDn, Constants.LdapAccessPassword, null);
        Person person = _fwd2search.GetPersonByLogon(_userName);
        if (person == null)
		{
          TaskDialog.ShowMessage("Please input a valid username", "Invalid Username", TaskDialogCommonButtons.Close,
                          VistaTaskDialogIcon.Warning);
          UserInfo = string.Empty;
          _fwd2search.Dispose();
        }
        else
        {
          Func<string, FWD2Search, string, string> loadUserInfoDelegate = UserInfoProvider.GetUserInfoReport;
          loadUserInfoDelegate.BeginInvoke(_userName, _fwd2search, _environment, DoneLoadUserInfo, loadUserInfoDelegate);

        }

      }
    }

    private void DoneLoadUserInfo(IAsyncResult result_)
    {
      if (!_dispatcher.CheckAccess())
      {
        Action<IAsyncResult> doneLoadUserInfoDelegate = DoneLoadUserInfo;
        _dispatcher.BeginInvoke(DispatcherPriority.Normal, doneLoadUserInfoDelegate, result_);
        return;
      }

      var dlg = (Func<string, FWD2Search, string, string>)(((AsyncResult)result_).AsyncDelegate);
      try
      {
        UserInfo = dlg.EndInvoke(result_);

        _log.Info("DoneLoadUserInfo", "Loaded user information for: " + _userName);
      }
      catch (Exception ex)
      {
        ReportError("DoneDeletePermission", ex);
      }
    }

    private void ReportError(string methodName_, Exception e_)
    {
      Exception e = e_.InnerException ?? e_;
      _log.Error(methodName_, e.Message, e);
      ErrorReportWindowViewModel errorReportWindowViewModel = new ErrorReportWindowViewModel(_errorReportWindowView);
      errorReportWindowViewModel.ShowErrorReport(e);
    }

    #endregion
  }
}
