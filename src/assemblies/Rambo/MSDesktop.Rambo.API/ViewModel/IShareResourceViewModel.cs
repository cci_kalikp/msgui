﻿using System;
using System.Collections.Generic;

namespace MorganStanley.MSDesktop.Rambo.API.ViewModel
{
  public interface IShareResourceViewModel
  {
    IDictionary<string, string> Run(RamboResource ramboResource_);
    event EventHandler<EventArgs> EndGrantPermission;
    event EventHandler<EventArgs> StartGrantPermission;
  }
}
