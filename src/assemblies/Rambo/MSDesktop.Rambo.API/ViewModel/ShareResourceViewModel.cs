﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using System.Windows.Input;
using System.Windows.Threading;
using JetBrains.Annotations;
using MorganStanley.MSDesktop.Rambo.Desktop.Utils;
using MorganStanley.MSDesktop.Rambo.Desktop.Utils.Extensions;
using MorganStanley.MSDesktop.Rambo.API.View;
using MorganStanley.MSDesktop.Rambo.WpfUtils.Core.Commands;
using MorganStanley.MSDesktop.Rambo.WpfUtils.Core.ViewModel;
using MorganStanley.MSDesktop.Rambo.WpfUtils.Core.Controls;

namespace MorganStanley.MSDesktop.Rambo.API.ViewModel
{
  public class ShareResourceViewModel : ObservableObject, IShareResourceViewModel
  {
    #region Readonly & Static Fields

    private static readonly ILogger _log = LoggerManager.GetLogger(typeof (ShareResourceViewModel));
    private readonly Dispatcher _dispatcher = Dispatcher.CurrentDispatcher;
    private readonly IShareResourceWindow _shareResourceWindow;
    private readonly IErrorReportWindowView _errorReportWindowView;    

    #endregion

    #region Fields

    private ObservableCollection<UserOrGroupViewModel> _userOrGroupViewModels;
    private UserOrGroupViewModel _selectedUserOrGroupViewModel;
    private bool _isInvalid;    
    private string _shareGroups;
    private IDictionary<string, string> _shareGroupsDic;
    private string _tooltip;
    private RamboResourceEntitlement _entitlementManager;

    #endregion

    #region Constructors

    public ShareResourceViewModel([NotNull] IShareResourceWindow shareResourceWindow_, [NotNull] IErrorReportWindowView errorReportWindowView_)
    {
      if (shareResourceWindow_ == null) throw new ArgumentNullException("shareResourceWindow_");
      if (errorReportWindowView_ == null) throw new ArgumentNullException("errorReportWindowView_");
      
      _shareResourceWindow = shareResourceWindow_;
      _errorReportWindowView = errorReportWindowView_;

      OKCommand = new RelayCommand(ExecuteOKCommand);
      DeleteCommand = new RelayCommand(ExecuteDeleteCommand);
    }

    #endregion

    #region Instance Properties    

    /// <summary>
    /// Current write groups
    /// </summary>
    public ObservableCollection<UserOrGroupViewModel> UserOrGroupViewModels
    {
      get { return _userOrGroupViewModels; }
      set
      {
        _userOrGroupViewModels = value;
        InvokePropertyChanged(() => UserOrGroupViewModels);
      }
    }

    public UserOrGroupViewModel SelectedUserOrGroupViewModel
    {
      get { return _selectedUserOrGroupViewModel; }
      set
      {
        _selectedUserOrGroupViewModel = value;
        InvokePropertyChanged(() => SelectedUserOrGroupViewModel);
      }
    }

    public RamboResource RamboResource { get; private set; }

    /// <summary>
    /// Share with more groups
    /// </summary>
    public bool IsInvalid
    {
      get { return _isInvalid; }
      set
      {
        _isInvalid = value;
        InvokePropertyChanged(() => IsInvalid);
      }
    }

    /// <summary>
    /// OK Command
    /// </summary>
    public ICommand OKCommand { get; private set; }

    /// <summary>
    /// OK Command
    /// </summary>
    public ICommand DeleteCommand { get; private set; }

    /// <summary>
    /// Share with more groups
    /// </summary>
    public string ShareGroups
    {
      get { return _shareGroups; }
      set
      {
        _shareGroups = value;
        InvokePropertyChanged(() => ShareGroups);
      }
    }

    /// <summary>
    /// Invalid user/group tooltip message
    /// </summary>
    public string ToolTipMessage
    {
      get { return _tooltip; }
      set
      {
        _tooltip = value;
        InvokePropertyChanged(() => ToolTipMessage);
      }
    }

    #endregion

    #region Instance Methods

    public IDictionary<string, string> Run(RamboResource ramboResource_)
    {
      _shareResourceWindow.Model = this;
      RamboResource = ramboResource_;

      _entitlementManager = new RamboResourceEntitlement();
      Func<string, IDictionary<string, string>> getEntitlementDel = _entitlementManager.GetPermission;
      getEntitlementDel.BeginInvoke(ramboResource_.ResourceId, DoneGetPermission, getEntitlementDel);
      _shareResourceWindow.ShowModal();
      return _entitlementManager.GetPermission(ramboResource_.ResourceId);
    }

    private void DoneGetPermission(IAsyncResult result_)
    {
      try
      {
        var getEntitlementDel = (Func<string, IDictionary<string, string>>)result_.AsyncState;
        IDictionary<string, string> groups = getEntitlementDel.EndInvoke(result_);
        if (groups == null) return;

        CreateUserOrGroupViewModels(groups);
      }
      catch (Exception ex)
      {
        ReportError("DoneGetPermission", ex);
      }
    }

    private void CreateUserOrGroupViewModels(IDictionary<string, string> groups_)
    {
      List<UserOrGroupViewModel> userOrGroupViewModels =
        groups_.Keys.Select(group => new UserOrGroupViewModel(group)).ToList();
      userOrGroupViewModels.ForEach(
        userOrGroupViewModel => userOrGroupViewModel.Deleted += UserOrGroupViewModelDeleted);
      UserOrGroupViewModels = new ObservableCollection<UserOrGroupViewModel>(userOrGroupViewModels);
    }

    private void ExecuteOKCommand(object o)
    {
      if (string.IsNullOrEmpty(ShareGroups))
      {
        return;
      }
      List<string> invalidGroups = GetInValidItems(ShareGroups);
      if (invalidGroups.Count > 0)
      {
        IsInvalid = true;
        ToolTipMessage = string.Format("{0} are not valid user(s)/group(s)", string.Join(",", invalidGroups.ToArray()));
        return;
      }

      InvokeStartGrantPermission(null);
      // Begin Grant Write Permission
      Action<string, IDictionary<string, string>> grantPermissionDel = _entitlementManager.GrantPermission;
      grantPermissionDel.BeginInvoke(RamboResource.ResourceId, _shareGroupsDic, DoneGrantPermission, grantPermissionDel);      
    }

    private void DoneGrantPermission(IAsyncResult result_)
    {
      if (!_dispatcher.CheckAccess())
      {
        Action<IAsyncResult> DoneGrantPermissionDelegate = DoneGrantPermission;
        _dispatcher.BeginInvoke(DispatcherPriority.Normal, DoneGrantPermissionDelegate, result_);
        return;
      }

      try
      {
        InvokeEndGrantPermission(null);
        var grantPermissionDel = (Action<string, IDictionary<string, string>>)result_.AsyncState;
        grantPermissionDel.EndInvoke(result_);
      }
      catch (Exception ex)
      {
        ReportError("DoneGrantPermission", ex);
      }
      finally
      {
        _shareResourceWindow.CloseView();
      }
    }

    private void ExecuteDeleteCommand(object o_)
    {
      if (SelectedUserOrGroupViewModel == null)
      {
        return;
      }
      if (WpfMessageBoxResult.OK ==
            WpfMessageBox.Show(
              "Are you sure you want to remove sharing for '" + SelectedUserOrGroupViewModel.UserOrGroup + "'?\nThis will take place immediately.",
              "Delete resource sharing",
              WpfMessageBoxButton.OKCancel,
              WpfMessageBoxImage.Question))
      {
        GetInValidItems(SelectedUserOrGroupViewModel.UserOrGroup);
        Action<string, IDictionary<string, string>> deletePermissionDel = _entitlementManager.DeletePermissions;
        deletePermissionDel.BeginInvoke(RamboResource.ResourceId, _shareGroupsDic, DoneDeletePermission, _shareGroupsDic);
      }                                            
    } 

    private void DoneDeletePermission(IAsyncResult result_)
    {
      if (!_dispatcher.CheckAccess())
      {
        Action<IAsyncResult> DoneDeletePermissionDelegate = DoneDeletePermission;
        _dispatcher.BeginInvoke(DispatcherPriority.Normal, DoneDeletePermissionDelegate, result_);
        return;
      }

      var dlg = (Action<string, IDictionary<string, string>>)(((AsyncResult)result_).AsyncDelegate);
      IDictionary<string, string> shareGroupsDic = (IDictionary<string, string>)result_.AsyncState;
      try
      {        
        dlg.EndInvoke(result_);
        shareGroupsDic.Keys.ForEach(RemoveUserOrGroupViewModel);
        _log.Info("DoneDeletePermission", "Deleted permissions for: " + string.Join(",", shareGroupsDic.Keys));        
      }
      catch (Exception ex)
      {
        ReportError("DoneDeletePermission", ex);
      }      
    }

    private void RemoveUserOrGroupViewModel(string userOrGroup_)
    {
      UserOrGroupViewModel userOrGroupViewModel = UserOrGroupViewModels.Where(item => item.UserOrGroup == userOrGroup_).FirstOrDefault();
      if(userOrGroupViewModel != null)
      {
        userOrGroupViewModel.Deleted -= UserOrGroupViewModelDeleted;
        UserOrGroupViewModels.Remove(userOrGroupViewModel);
      }
    }

    private void UserOrGroupViewModelDeleted(object sender_, EventArgs e_)
    {
      SelectedUserOrGroupViewModel = (UserOrGroupViewModel)sender_;
      ExecuteDeleteCommand(null);
    }

    private void ReportError(string methodName_, Exception e_)
    {
      Exception e = e_.InnerException ?? e_;
      _log.Error(methodName_, e.Message, e);
      ErrorReportWindowViewModel errorReportWindowViewModel = new ErrorReportWindowViewModel(_errorReportWindowView);
      errorReportWindowViewModel.ShowErrorReport(e);
    }

    public void InvokeEndGrantPermission(EventArgs e)
    {
      EventHandler<EventArgs> handler = EndGrantPermission;
      if (handler != null) handler(this, e);
    }

    public void InvokeStartGrantPermission(EventArgs e)
    {
      EventHandler<EventArgs> handler = StartGrantPermission;
      if (handler != null) handler(this, e);
    }

    /// <summary>
    /// Check if current group is valid user/group
    /// </summary>
    private bool CheckValid(string key_)
    {
      if (_shareGroupsDic == null) _shareGroupsDic = new Dictionary<string, string>();
      
      if (UserVerification.GetGroup(key_) != null)
      {
        _shareGroupsDic[key_] = Constants.GROUP_TYPE;
        return true;
      }
      if (UserVerification.GetPerson(key_) != null)
      {
        _shareGroupsDic[key_] = Constants.USER_TYPE;
        return true;
      }

      return false;
    }

    /// <summary>
    /// Get all invalid Items
    /// </summary>
    [NotNull]
    private List<string> GetInValidItems([NotNull] string groups_)
    {
      if (groups_ == null) throw new ArgumentNullException("groups_");

      _shareGroupsDic = new Dictionary<string, string>();
      List<string> invalidGroups = new List<string>();
      string[] sharedGroups = groups_.Split(',');
      sharedGroups.ForEach(group =>
                             {
                               if (!CheckValid(group))
                               {
                                 invalidGroups.Add(group);
                               }
                             });

      return invalidGroups;
    }

    #endregion

    #region Event Declarations

    public event EventHandler<EventArgs> EndGrantPermission;
    public event EventHandler<EventArgs> StartGrantPermission;

    #endregion
  }
}