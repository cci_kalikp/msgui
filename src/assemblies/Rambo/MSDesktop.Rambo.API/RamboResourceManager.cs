﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using JetBrains.Annotations;
using MorganStanley.MSDesktop.Rambo.CafGuiAdmin;
using MorganStanley.MSDesktop.Rambo.DataServices;
using MorganStanley.MSDesktop.Rambo.Desktop.Utils;
using MorganStanley.MSDesktop.Rambo.API.DataObjects;
using MorganStanley.MSDesktop.Rambo.WinGridUtils;

namespace MorganStanley.MSDesktop.Rambo.API
{
  public class RamboResourceManager : ResourceManager, IXmlListBuilder
  {
    #region Readonly & Static Fields

    private static readonly ILogger _log = LoggerManager.GetLogger(typeof(RamboResourceManager));
    private readonly Dictionary<object, RamboResource> _objectResourceMap;
    private readonly RamboResourceEntitlement _ramboResourceEntitlement;
    private const string DATA_SERVICE_TIME_FORMAT = "yyyy-MM-ddTHH:mm:ss.fff";

    #endregion

    #region Fields

    private DateTime _loadStartTime;
    protected ICafGuiAdminProxy _cafGuiAdminProxy;
    protected IDataService _dataService;
    protected IDataService _listDataService;
    #endregion

    #region Constructors

    public RamboResourceManager(ICafGuiAdminProxy cafGuiAdminProxy_ = null)
    {
      _cafGuiAdminProxy = cafGuiAdminProxy_;
      _objectResourceMap = new Dictionary<object, RamboResource>();
      _ramboResourceEntitlement = new RamboResourceEntitlement();
      _log.Info("RamboResourceManager", "objectResourceMap initialized.");
    }

    public RamboResourceManager(IDataService dataService_,IDataService listDataService_, ICafGuiAdminProxy cafGuiAdminProxy_ = null)
      :this(cafGuiAdminProxy_)
    {
      _dataService = dataService_;
      _listDataService = listDataService_;
    }

    #endregion

    #region Instance Properties

    protected override IDataService DataService
    {
      get
      {
        return _dataService ?? DataServiceFactoryLayer.GetDataService(Constants.RamboResourceDS);
      }
    }

    protected override IDataService ListDataService
    {
      get
      {
        return _listDataService ?? DataServiceManager.GetDataService(Constants.RamboResourceListDS);
      }
    }

    protected override IXmlListBuilder ListSerializer
    {
      get { return this; }
    }

    protected override Type ResourceType
    {
      get { return typeof (RamboResource); }
    }

    protected override XmlSerializer Serializer
    {
      get { return null; }
    }

    #endregion

    #region Instance Methods
    /// <summary>
    /// Get write permission for rambo resource
    /// </summary>
    /// <param name="o_"></param>
    /// <param name="userName_"></param>
    /// <param name="applicationName_"></param>
    /// <param name="region_"></param>
    /// <param name="environment_"></param>
    /// <param name="appletName_"></param>
    /// <param name="resourceType_"></param>
    /// <param name="resourceCategory_"></param>
    /// <param name="resourceName_"></param>
    [CanBeNull]
    public IDictionary<string, string> GetEntitlement(object o_,
                                            string userName_,
                                            string applicationName_,
                                            string region_,
                                            string environment_,
                                            string appletName_,
                                            string resourceType_,
                                            string resourceCategory_,
                                            string resourceName_)
    {
      RamboResource resource = GetFromMap(o_) ?? new RamboResource
      {
        UserName = userName_,
        ApplicationName = applicationName_,
        Profile = GetProfile(region_, environment_),
        AppletName = appletName_,
        ResourceType = resourceType_,
        ResourceCategory = resourceCategory_,
        ResourceName = resourceName_
      };
      return _ramboResourceEntitlement.GetPermission(resource.ResourceId);
    }
    
    /// <summary>
    /// Grant write permission for resource
    /// </summary>
    /// <param name="o_">The resource used to retrieve the corresponding RamboResource. This should be the same as the resource we want to share.</param>
    /// <param name="userName_">Part of Rambo key. Used to create RamboResource object in case it can't do lookup based on the input resource.</param>
    /// <param name="applicationName_">Part of Rambo key. Used to create RamboResource object in case it can't do lookup based on the input resource.</param>
    /// <param name="profile_">Part of Rambo key. Used to create RamboResource object in case it can't do lookup based on the input resource.</param>
    /// <param name="appletName_">Part of Rambo key. Used to create RamboResource object in case it can't do lookup based on the input resource.</param>
    /// <param name="resourceType_">Part of Rambo key. Used to create RamboResource object in case it can't do lookup based on the input resource.</param>
    /// <param name="resourceCategory_">Part of Rambo key. Used to create RamboResource object in case it can't do lookup based on the input resource.</param>
    /// <param name="resourceName_">Part of Rambo key. Used to create RamboResource object in case it can't do lookup based on the input resource.</param>
    /// <param name="groupsDic"></param>
    public void GrantEntitlement(object o_, string userName_,
                                            string applicationName_,
                                            string profile_,
                                            string appletName_,
                                            string resourceType_,
                                            string resourceCategory_,
                                            string resourceName_, IDictionary<string, string> groupsDic)
    {
      RamboResource resource = GetFromMap(o_) ?? new RamboResource
      {
        UserName = userName_,
        ApplicationName = applicationName_,
        Profile = profile_,
        AppletName = appletName_,
        ResourceType = resourceType_,
        ResourceCategory = resourceCategory_,
        ResourceName = resourceName_
      };
      _ramboResourceEntitlement.GrantPermission(resource.ResourceId, groupsDic);
    }

    /// <summary>
    /// Delete resource in Rambo by loading the resource and mark it as deleted.
    /// </summary>
    /// <param name="o_"></param>
    /// <param name="forceReload_">If true, the API reloads the resource from server, otherwise just tries retrieving from cache</param>
    /// <param name="userName_">Part of the key. Used to load the resource</param>
    /// <param name="applicationName_">Part of the key. Used to load the resource</param>
    /// <param name="region_">Used to load the resource. The profile part of the key is generated based on this information. If you want to ignore, use null, do not use EmptyKeyString or "all"</param>
    /// <param name="environment_">Used to load the resource. The profile part of the key is generated based on this information. If you want to ignore, use null, do not use EmptyKeyString or "all"</param>
    /// <param name="appletName_">Part of the key. Used to load the resource</param>
    /// <param name="resourceType_">Part of the key. Used to load the resource</param>
    /// <param name="resourceCategory_">Part of the key. Used to load the resource</param>
    /// <param name="resourceName_">Part of the key. Used to load the resource</param>
    /// <returns></returns>
    public void DeleteResource(object o_,
                               bool forceReload_,
                               string userName_,
                               string applicationName_,
                               string region_,
                               string environment_,
                               string appletName_,
                               string resourceType_,
                               string resourceCategory_,
                               string resourceName_)
    {
      RamboResource loadedResource = LoadRamboResource(o_, forceReload_, userName_, applicationName_, region_,
                                                       environment_,
                                                       appletName_, resourceType_, resourceCategory_, resourceName_);

      if (loadedResource == null || loadedResource.IsDeleted) return;

      loadedResource.IsDeleted = true;
      loadedResource.ModifyBy = Environment.UserName;
      base.SaveValue(loadedResource);
      RemoveItemFromList(loadedResource.UserName, loadedResource.ApplicationName, region_, environment_,
                         loadedResource.AppletName, loadedResource.ResourceType, loadedResource.ResourceCategory,
                         loadedResource.ResourceName);
      _log.Info("DeleteResource", String.Format("Delete Resource {0} to dataservice by {1}", loadedResource.ResourceId, Environment.UserName));
    }

    /// <summary>
    /// Loads the definition of the resource that matches the given criteria.
    /// TODO: deserialize to RamboResource before returning
    /// </summary>
    /// <param name="objectToLoad_"></param>
    /// <param name="forceReload_"></param>
    /// <param name="userName_"></param>
    /// <param name="applicationName_"></param>
    /// <param name="region_"></param>
    /// <param name="environment_"></param>
    /// <param name="appletName_"></param>
    /// <param name="resourceType_"></param>
    /// <param name="resourceCategory_"></param>
    /// <param name="resourceName_"></param>
    /// <returns></returns>
    public object LoadResource(Object objectToLoad_,
                               bool forceReload_,
                               string userName_,
                               string applicationName_,
                               string region_,
                               string environment_,
                               string appletName_,
                               string resourceType_,
                               string resourceCategory_,
                               string resourceName_)
    {

      return LoadResource(objectToLoad_,
                          forceReload_,
                          userName_,
                          applicationName_,
                          region_,
                          environment_,
                          appletName_,
                          resourceType_,
                          resourceCategory_,
                          resourceName_,
                          true);
    }

    /// <summary>
    /// Loads the definition of the resource that matches the given criteria.
    /// </summary>
    /// <param name="objectToLoad_"></param>
    /// <param name="forceReload_"></param>
    /// <param name="userName_"></param>
    /// <param name="applicationName_"></param>
    /// <param name="region_"></param>
    /// <param name="environment_"></param>
    /// <param name="appletName_"></param>
    /// <param name="resourceType_"></param>
    /// <param name="resourceCategory_"></param>
    /// <param name="resourceName_"></param>
    /// <param name="loadDeletedResource_"></param>
    /// <returns></returns>
    public object LoadResource(Object objectToLoad_,
                               bool forceReload_,
                               string userName_,
                               string applicationName_,
                               string region_,
                               string environment_,
                               string appletName_,
                               string resourceType_,
                               string resourceCategory_,
                               string resourceName_,
                               bool loadDeletedResource_)
    {
      _log.Info("LoadResource",
                String.Format("Loading resource. Parameters are {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}",
                              userName_, applicationName_, region_, environment_, appletName_, resourceType_, resourceCategory_,
                              resourceName_, loadDeletedResource_));

      //Get RamboResource
      //If resource has not been previously loaded through LoadResource or LoadResourceList
      //then we can add it 
      RamboResource loadedResource = LoadRamboResource(objectToLoad_, forceReload_, userName_, applicationName_, region_, environment_,
                                                       appletName_, resourceType_, resourceCategory_, resourceName_);
      if (loadedResource == null) return null;

      if (loadedResource.IsDeleted && !loadDeletedResource_)
      {
        _log.Info("LoadResource", String.Format("Resource not loaded as it is deleted in Rambo. Id: {0}", loadedResource.ResourceId));
        return null;
      }

      //Deserialize RamboResource.XmlData
      var loadedObject = DeserializeObject(loadedResource);

      //Add to map
      AddToMap(objectToLoad_, loadedResource);

      //Report to CafGuiAdmin
      ReportToCafGuiAdmin(string.Format("LOAD:{0}", loadedResource.ResourceType), loadedResource.ResourceId);

      _log.Info("LoadResource", String.Format("Resource loaded from Rambo. Id: {0}", loadedResource.ResourceId));

      //Return deserialized object
      return loadedObject;
    }
     

    /// <summary>
    /// Loads a resource list based on the given attributes.
    /// Primarily created to allow subscription using attributes that are not part of the key e.g. userGrouping
    /// </summary>
    /// <param name="attributes_"></param>
    /// <param name="forceReload_"></param>
    /// <returns></returns>
    public Object[] LoadResourceList(Dictionary<string, string> attributes_, bool forceReload_)
    {
      // log load resource attribute.
      var attributes = new string[attributes_.Values.Count];
      attributes_.Values.CopyTo(attributes,0);
      string parameter = string.Join(", ", attributes);
      _log.Info("LoadResourceList", "Loading resource list. Parameters are " + parameter);

      //Clear the resource list cache for this key only
      if (forceReload_)
      {
        var listKey = BuildAttributeWaitingKey(attributes_);
        ClearListCache(listKey);
      }

      _loadStartTime = DateTime.UtcNow;
      var resources = base.LoadList(attributes_) as RamboResource[];
      if (resources == null) return null;

      //Deserialize the objects and populate map
      var objects = new Object[resources.Length];
      for (int i = 0; i < resources.Length; i++)
      {
        var res = resources[i];

        //Deserialize RamboResource.XmlData
        var o = GetObject(res);

        //Add to map
        AddToMap(o, res);

        //Cache in map so that RamboResource can be easily retrieved when saving and loading grouping definition
        objects[i] = o;
      }

      _log.Info("LoadResourceList", String.Format("{0} resource loaded from Rambo.", objects.Length));
      return objects;
    }

    /// <summary>
    /// Loads resources from Rambo that match the given attributes.
    /// </summary>
    /// <param name="userName_"></param>
    /// <param name="applicationName_"></param>
    /// <param name="region_"></param>
    /// <param name="environment_"></param>
    /// <param name="appletName_"></param>
    /// <param name="resourceType_"></param>
    /// <param name="resourceCategory_"></param>
    /// <param name="showDeleted_"></param>
    /// <param name="forceReload_"></param>
    /// <returns></returns>
    public Object[] LoadResourceList(string userName_,
                                     string applicationName_,
                                     string region_,
                                     string environment_,
                                     string appletName_,
                                     string resourceType_,
                                     string resourceCategory_,
                                     bool showDeleted_,
                                     bool forceReload_)
    {
      var attributes = PrepareDictionary(userName_,
                                          applicationName_,
                                          region_,
                                          environment_,
                                          appletName_,
                                          resourceType_,
                                          resourceCategory_,
                                          null,
                                          showDeleted_);

      //Fetch a fresh list from the dataservice
      return LoadResourceList(attributes, forceReload_);
    }

    /// <summary>
    /// Loads resources from Rambo that match the given attributes.
    /// </summary>
    /// <param name="userName_"></param>
    /// <param name="applicationName_"></param>
    /// <param name="region_"></param>
    /// <param name="environment_"></param>
    /// <param name="appletName_"></param>
    /// <param name="resourceType_"></param>
    /// <param name="resourceCategory_"></param>
    /// <param name="showDeleted_"></param>
    /// <returns></returns>
    public Object[] LoadResourceList(string userName_,
                                     string applicationName_,
                                     string region_,
                                     string environment_,
                                     string appletName_,
                                     string resourceType_,
                                     string resourceCategory_,
                                     bool showDeleted_)
    {
      return LoadResourceList(userName_,
                              applicationName_,
                              region_,
                              environment_,
                              appletName_,
                              resourceType_,
                              resourceCategory_,
                              showDeleted_,
                              false);
    }

    /// <summary>
    /// Load version list for rambo resource.
    /// </summary>
    /// <param name="resource_"></param>
    /// <returns></returns>
    public VersionDataKeyCollection LoadResourceVersionList(RamboResource resource_)
    {
      IDataKey key = DataKeyFactory.BuildDataKey(resource_.ResourceId);
      return DataService.GetVersionsList(key, true);
    }

    /// <summary>
    /// Load a paricular rambo resource
    /// </summary>
    /// <param name="resource_"></param>
    /// <param name="updateTime_"></param>
    /// <returns></returns>
    public RamboResource LoadResourceVersion(RamboResource resource_, [NotNull]DataServiceDate updateTime_)
    {
      //Ceiling the update time, the reason to do this is bacause we floor the updateTime's milisecond when retrieve from data service
      //e.g the date saved in data base is 2011-02-24T11:01:00.174, the time we get is 2011-02-24T11:01:00.170
      //if we don't ceiling here, it will return the previous  version resource rather than the current version resource.
      DateTime updateTime = updateTime_.Date.AddMilliseconds(-updateTime_.Date.Millisecond).AddSeconds(1);
      DataServiceDate updateDataServiceTime = new DataServiceDate(updateTime.ToString(DATA_SERVICE_TIME_FORMAT));
      DataServiceDate dataTime = resource_.DataTime == null
                                     ? updateDataServiceTime
                                     : new DataServiceDate(resource_.DataTime.ToString());
      IVersionDataKey versionDataKey = DataKeyFactory.BuildDataKey(resource_.ResourceId, dataTime, updateDataServiceTime, false);

      _log.Info("LoadResourceVersion", string.Format("Loading version resource {0}", versionDataKey));
      DataEvent dataevent = DataService.GetVersion(versionDataKey);
      if (dataevent == null) return null;
      _log.Info("LoadResourceVersion", string.Format("{0} version resource loaded from rambo", dataevent.DataKey));
      object val = Deserialize(((XMLDataObject)dataevent.DataObject).Value.OuterXml);
      return val as RamboResource;
    }

    /// <summary>
    /// Saves resource to Rambo.
    /// </summary>
    /// <param name="objectToSave_"></param>
    /// <param name="userName_"></param>
    /// <param name="applicationName_"></param>
    /// <param name="region_"></param>
    /// <param name="environment_"></param>
    /// <param name="appletName_"></param>
    /// <param name="resourceType_"></param>
    /// <param name="resourceCategory_"></param>
    /// <param name="resourceName_"></param>
    /// <returns></returns>
    public ResourceSaveState SaveResource(Object objectToSave_,
                                           string userName_,
                                           string applicationName_,
                                           string region_,
                                           string environment_,
                                           string appletName_,
                                           string resourceType_,
                                           string resourceCategory_,
                                           string resourceName_)
    {
      return SaveResource(objectToSave_, userName_, applicationName_, region_, environment_, appletName_, resourceType_,
                   resourceCategory_, resourceName_, true);
    }

    /// <summary>
    /// Saves resource to Rambo.
    /// </summary>
    /// <param name="objectToSave_"></param>
    /// <param name="userName_"></param>
    /// <param name="applicationName_"></param>
    /// <param name="profile_"></param>
    /// <param name="appletName_"></param>
    /// <param name="resourceType_"></param>
    /// <param name="resourceCategory_"></param>
    /// <param name="resourceName_"></param>
    /// <returns></returns>
     public ResourceSaveState SaveResource(Object objectToSave_,
                                           string userName_,
                                           string applicationName_,
                                           string profile_,
                                           string appletName_,
                                           string resourceType_,
                                           string resourceCategory_,
                                           string resourceName_)
     {
       return SaveResource(objectToSave_, userName_, applicationName_, profile_, appletName_, resourceType_,
                           resourceCategory_, resourceName_, true);
     }

    /// <summary>
    /// Saves resource to Rambo
    /// </summary>
    /// <param name="objectToSave_"></param>
    /// <param name="userName_"></param>
    /// <param name="applicationName_"></param>
    /// <param name="region_"></param>
    /// <param name="environment_"></param>
    /// <param name="appletName_"></param>
    /// <param name="resourceType_"></param>
    /// <param name="resourceCategory_"></param>
    /// <param name="resourceName_"></param>
    /// <param name="saveToDataService_">If set to true, the resource will be saved in the dataservice. Otherwise only the cache will be updated.</param>
    public ResourceSaveState SaveResource(Object objectToSave_,
                             string userName_,
                             string applicationName_,
                             string region_,
                             string environment_,
                             string appletName_,
                             string resourceType_,
                             string resourceCategory_,
                             string resourceName_,
                             bool saveToDataService_)
    {
      return SaveResource(objectToSave_, userName_, applicationName_, GetProfile(region_, environment_),
                          appletName_, resourceType_, resourceCategory_, resourceName_, saveToDataService_);
    }

    /// <summary>
    /// Saves resource to Rambo.
    /// </summary>
    /// <param name="objectToSave_"></param>
    /// <param name="userName_"></param>
    /// <param name="applicationName_"></param>
    /// <param name="profile_"></param>
    /// <param name="appletName_"></param>
    /// <param name="resourceType_"></param>
    /// <param name="resourceCategory_"></param>
    /// <param name="resourceName_"></param>
    /// <param name="saveToDataService_">If set to true, the resource will be saved in the dataservice. Otherwise only the cache will be updated.</param>
    public ResourceSaveState SaveResource(Object objectToSave_,
                             string userName_,
                             string applicationName_,
                             string profile_,
                             string appletName_,
                             string resourceType_,
                             string resourceCategory_,
                             string resourceName_,
                             bool saveToDataService_)
    {
      if (resourceType_ != "AppPreference" && resourceType_ != "Configurator")
      {
        if(string.IsNullOrWhiteSpace(resourceName_) || resourceName_.ToLower() == "all" )
        {
          return ResourceSaveState.InvalidParameter;
        }
      }
      string key = RamboResource.GenerateKey(userName_, applicationName_, profile_, appletName_, resourceType_,
                                            resourceCategory_, resourceName_);
      _log.Info("SaveResource", String.Format("Saving. Parameters are {0}",key));

      //Get rambo resource
      bool isInCache = GetFromMap(objectToSave_) != null;
      var resource = isInCache ? GetFromMap(objectToSave_) : new RamboResource
                                        {
                                          UserName = userName_,
                                          ApplicationName = applicationName_,
                                          Profile = profile_,
                                          AppletName = appletName_,
                                          ResourceType = resourceType_,
                                          ResourceCategory = resourceCategory_,
                                          ResourceName = resourceName_
                                        };

      // if it's not a new layout and don't have permission.))
      if (isInCache && !_ramboResourceEntitlement.HasPermission(resource.ResourceId, Environment.UserName))
      {
        return ResourceSaveState.NoWritePermission;
      }

      //Serialize object
      var writer = new StringWriter();
      SerializeObject(writer, objectToSave_);
      var doc = new XmlDocument();
      doc.LoadXml(writer.ToString());
      //We are updating an existing resource
      resource.XmlData = doc;
      resource.ModifyBy = Environment.UserName;
      //set the IsDeleted flag to false in case we save on some resource deleted before.
      resource.IsDeleted = false;

      // validate if xml comply with basic xml schema before save
      RamboResourceXmlValidator.ValidateResourceXml(resource.ResourceType, resource.XmlData);
      
      //Save to ds and/or cache)
      if (saveToDataService_)
      {
        base.SaveValue(resource);
        _log.Info("SaveResource", String.Format("Saved resource to dataservice and updating cache. Id: {0}", resource.ResourceId));
      }

      UpdateLocalCache(resource);
      _log.Info("SaveResource", String.Format("Updating cache with new version of resource. Id: {0}", resource.ResourceId));
      
      //Update object/resource map
      AddToMap(objectToSave_, resource);

      //Report to CafGuiAdmin
      ReportToCafGuiAdmin(string.Format("SAVE:{0}", resource.ResourceType), resource.ResourceId);

      _log.Info("SaveResource", String.Format("Resource saved to Rambo. Id: {0}", resource.ResourceId));
      return ResourceSaveState.Saved;
    }

    /// <summary>
    /// Clear Entitlement cache
    /// </summary>
    public void ClearEntitlmentCache()
    {
      _ramboResourceEntitlement.ClearCache();
    }

    /// <summary>
    /// Subclass needs to provide its own deserialization
    /// TODO: change the inheritance parttern for rambo so that we don't need so many override method
    /// TODO: RamboResourceManager should takes parameter of RamboResource, all other resource, e.g GridLayout should inherited from RamboResouce.
    /// Subclass needs to provide its own deserialization
    /// </summary>
    /// <param name="resource_"></param>
    /// <returns></returns>
    protected virtual object DeserializeObject(RamboResource resource_)
    {
      return resource_;
    }

    /// <summary>
    /// Helper to generate key that is used to maintain object-resource dictionary
    /// </summary>
    /// <returns></returns>
    protected virtual string GetMapKey(Object o)
    {
      return string.Empty;
    }

    /// <summary>
    /// Creates a skeleton object from RamboResource
    /// </summary>
    /// <returns></returns>
    protected virtual Object GetObject(RamboResource r)
    {
      return null;
    }

    /// <summary>
    /// Subclass needs to provide its own serialization
    /// </summary>
    /// <param name="reader"></param>
    /// <param name="o"></param>
    protected virtual void SerializeObject(StringWriter reader, Object o){}

    protected override object Deserialize(string xml)
    {
      var doc = new XmlDocument();
      doc.LoadXml(xml);
      var resource = RamboResource.ReadXml(doc.SelectSingleNode("."));
      return resource;
    }

    protected override string ExtractKey(object value)
    {
      var resource = value as RamboResource;
      return (resource == null ? null : resource.ResourceId);
    }

    protected override string[] ExtractListKeys(object value_)
    {
      //I can't find any use of this in Rambo
      return new string[0];
    }

    protected override bool IsLoaded(object value_)
    {
      var res = value_ as RamboResource;
      return (res == null) ? false : !String.IsNullOrEmpty(res.XmlData.OuterXml);
    }

    ///// <summary>
    ///// Custom implementation to deserialize xml.
    ///// </summary>
    protected override void Serialize(TextWriter text_, object object_)
    {
      var res = object_ as RamboResource;
      if (res != null)
      {
        var writer = XmlWriter.Create(text_);
        res.WriteXml(writer);
        writer.Flush();
      }
    }

    /// <summary>
    /// Adds an entry to Grouping-RamboResource map
    /// </summary>
    /// <param name="object_"></param>
    /// <param name="resource_"></param>
    private void AddToMap(Object object_, RamboResource resource_)
    {
      lock (_objectResourceMap)
      {
        _objectResourceMap[GetMapKey(object_)] = resource_;
      }
    }

    /// <summary>
    /// Helper method to retrieve RamboResource from map
    /// </summary>
    /// <param name="object_"></param>
    /// <returns></returns>
    protected RamboResource GetFromMap(Object object_)
    {
      lock (_objectResourceMap) 
      {
        if (_objectResourceMap != null && _objectResourceMap.Count > 0)
        {
          var key = GetMapKey(object_);
          if (_objectResourceMap.ContainsKey(key))
          {
            return _objectResourceMap[key];
          }
        }
        return null;
      }
    }

    private RamboResource LoadRamboResource(object o_, bool forceReload_, string userName_, string applicationName_,
                                            string region_, string environment_,
                                            string appletName_, string resourceType_, string resourceCategory_,
                                            string resourceName_)
    {
      RamboResource resource = GetFromMap(o_) ?? new RamboResource
                                                   {
                                                     UserName = userName_,
                                                     ApplicationName = applicationName_,
                                                     Profile = GetProfile(region_, environment_),
                                                     AppletName = appletName_,
                                                     ResourceType = resourceType_,
                                                     ResourceCategory = resourceCategory_,
                                                     ResourceName = resourceName_
                                                   };

      //Query RamboResource ds
      var key = RamboResource.GenerateKey(resource.UserName,
                                          resource.ApplicationName,
                                          resource.Profile,
                                          resource.AppletName,
                                          resource.ResourceType,
                                          resource.ResourceCategory,
                                          resource.ResourceName);
      return LoadValue(key, forceReload_) as RamboResource;
    }

    public override void ClearCache()
    {
      ClearEntitlmentCache();
      base.ClearCache();
    }

    public override void UpdateLocalCache(object value_)
    {
      var resource = value_ as RamboResource;
      if (resource != null)
      {
        //Add/Update resource
        _cache[resource.ResourceId] = resource;
      }

      //Update list cache if any, and raise events
      base.UpdateLocalCache(value_);
    }

    protected void ReportToCafGuiAdmin(string eventName_, string resourceId_)
    {
      if(_cafGuiAdminProxy != null)
        _cafGuiAdminProxy.ReportEvent(EventDomain.ResourceUsage, eventName_, resourceId_);
    }

    #endregion

    #region IXmlListBuilder Members

    /// <summary>
    /// Deserializes xml into array of RamboResource
    /// </summary>
    /// <param name="value_"></param>
    /// <returns></returns>
    public object[] DeserializeList(XmlElement value_)
    {
      var resources = new RamboResourceList();
      var t1 = DateTime.UtcNow;
      resources.ReadXml(value_);
      var t2 = DateTime.UtcNow;
      _log.Info("DeserializeList", String.Format("Time to load list {0}", t1.Subtract(_loadStartTime).Seconds));
      _log.Info("DeserializeList", String.Format("Time to deserialize resource {0}", t2.Subtract(t1).Seconds));
      return resources.ToArray();
    }

    #endregion

    #region Static Methods

    protected static void AddKey(Dictionary<string, string> attributes_, string key_, string value_)
    {
      if (!String.IsNullOrEmpty(value_))
      {
        attributes_[key_] = value_;
      }
    }

    protected static string AdjustFirstLetter(string value_)
    {
      var firstLetter = value_.Substring(0, 1).ToLower();
      return firstLetter + value_.Substring(1);
    }

    public static string GetProfile(string region_, string environment_)
    {
      if (region_ == null || environment_ == null) return Constants.EmptyKeyString;
      return String.Format("{0}-{1}", region_.ToLower(), environment_);
    }

    /// <summary>
    /// Creates attribute dictionary.
    /// </summary>
    /// <param name="userName_"></param>
    /// <param name="applicationName_"></param>
    /// <param name="region_"></param>
    /// <param name="environment_"></param>
    /// <param name="appletName_"></param>
    /// <param name="resourceType_"></param>
    /// <param name="resourceCategory_"></param>
    /// <param name="resourceName_"></param>
    /// <param name="showDeleted_"></param>
    /// <returns></returns>
    protected static Dictionary<string, string> PrepareDictionary(string userName_,
                                                                  string applicationName_,
                                                                  string region_,
                                                                  string environment_,
                                                                  string appletName_,
                                                                  string resourceType_,
                                                                  string resourceCategory_,
                                                                  string resourceName_,
                                                                  bool showDeleted_)
    {
      var attributes = new Dictionary<string, string>();
      AddKey(attributes, AdjustFirstLetter(ColumnName.RamboUserid.ToString()), userName_);
      AddKey(attributes, AdjustFirstLetter(ColumnName.RamboApplication.ToString()), applicationName_);
      AddKey(attributes, AdjustFirstLetter(ColumnName.RamboProfile.ToString()), GetProfile(region_, environment_));
      AddKey(attributes, AdjustFirstLetter(ColumnName.RamboApplet.ToString()), appletName_);
      AddKey(attributes, AdjustFirstLetter(ColumnName.RamboResType.ToString()), resourceType_);
      AddKey(attributes, AdjustFirstLetter(ColumnName.RamboResCategory.ToString()), resourceCategory_);
      AddKey(attributes, AdjustFirstLetter(ColumnName.RamboResName.ToString()), resourceName_);
      if (!showDeleted_) // if don't want to show deleted resource, only select resource with IsDeleted flag as 1
      {
        AddKey(attributes, AdjustFirstLetter(ColumnName.IsDeleted.ToString()), "0");
      }
      return attributes;
    }
    /// <summary>
    /// Remove Resource from cached list
    /// </summary>
    /// <param name="userName_"></param>
    /// <param name="applicationName_"></param>
    /// <param name="region_"></param>
    /// <param name="environment_"></param>
    /// <param name="appletName_"></param>
    /// <param name="resourceType_"></param>
    /// <param name="resourceCategory_"></param>
    /// <param name="resourceName_"></param>
    public void RemoveItemFromList(string userName_,
                                      string applicationName_,
                                      string region_,
                                      string environment_,
                                      string appletName_,
                                      string resourceType_,
                                      string resourceCategory_,
                                      string resourceName_)
    {
      Dictionary<string, string> attributes = PrepareDictionary(Constants.EmptyKeyString,
                                                                applicationName_,
                                                                region_,
                                                                environment_,
                                                                appletName_,
                                                                resourceType_,
                                                                resourceCategory_,
                                                                null, false);

      string key = BuildAttributeWaitingKey(attributes);
      //remove from list cache
      var cacheList = _listCache[key] as IList;
      if (cacheList != null)
      {
        string cacheKey = RamboResource.GenerateKey(userName_, applicationName_, GetProfile(region_, environment_),
                                                    applicationName_, resourceType_,
                                                    resourceCategory_,
                                                    resourceName_);
        cacheList.Remove(cacheKey);
        _cache.Remove(cacheKey);
      }
    }
    #endregion
  }
}