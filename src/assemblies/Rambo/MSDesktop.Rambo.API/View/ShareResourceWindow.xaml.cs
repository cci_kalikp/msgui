﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Interop;

namespace MorganStanley.MSDesktop.Rambo.API.View
{
  /// <summary>
  /// Interaction logic for ShareResourceWindow.xaml
  /// </summary>
  public partial class ShareResourceWindow : IShareResourceWindow
  {
    [DllImport("user32.dll")]
    public static extern IntPtr GetActiveWindow();

    public ShareResourceWindow()
    {
      InitializeComponent();

      new WindowInteropHelper(this)
      {
        Owner = GetActiveWindow(),
      };
    }

    public object Model
    {
      get { return DataContext; }
      set { DataContext = value; }
    }

    public bool ShowModal()
    {
      bool? result = ShowDialog();
      return result.HasValue && result.Value;
    }

    public void CloseView()
    {
      Close();
    }
  }
}
