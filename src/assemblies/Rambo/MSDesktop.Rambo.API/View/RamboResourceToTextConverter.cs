﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace MorganStanley.MSDesktop.Rambo.API.View
{
  [ValueConversion(typeof(RamboResource), typeof(string))]
  public class RamboResourceToTextConverter : IValueConverter
  {
    public object Convert(object value_, Type targetType_, object parameter_, CultureInfo culture_)
    {
      RamboResource ramboResource = (RamboResource) value_;
      return "'" + ramboResource.ResourceName + "' resource is currently shared with:";
    }

    public object ConvertBack(object value_, Type targetType_, object parameter_, CultureInfo culture_)
    {
      return null;
    }
  }
}
