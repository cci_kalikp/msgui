﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using MorganStanley.MSDesktop.Rambo.API.ViewModel;

namespace MorganStanley.MSDesktop.Rambo.API.View
{
  /// <summary>
  /// Interaction logic for UserInfoControl.xaml
  /// </summary>
  public partial class UserInfoControl
  {
    public UserInfoControl()
    {
      InitializeComponent();
      Loaded += OnLoaded;
    }

    private void OnLoaded(object sender_, RoutedEventArgs e_)
    {
      _textBoxUserName.Focus();
    }

    private void OnUserInfoTextChanged(object sender_, TextChangedEventArgs e_)
    {
      if (e_.Source == _textBoxUserInfo)
      {
        _textBoxUserInfo.ScrollToHome();
      }
    }

    protected override void OnKeyDown(KeyEventArgs e_)
    {
      if (e_.Key == Key.Enter)
      {
        var dc = DataContext as UserInfoViewModel;
        if (dc != null)
        {
          dc.UserName = _textBoxUserName.Text.Trim();
          dc.ExecuteLoadCommand(null);
        }
      }
    }
  }
}