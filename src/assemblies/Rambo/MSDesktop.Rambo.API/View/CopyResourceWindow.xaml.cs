﻿namespace MorganStanley.MSDesktop.Rambo.API.View
{
  /// <summary>
  /// Interaction logic for CopyResourceWindow.xaml
  /// </summary>
  public partial class CopyResourceWindow : ICopyResourceWindow
  {
    public CopyResourceWindow()
    {
      InitializeComponent();
    }

    public object Model
    {
      get { return DataContext; }
      set { DataContext = value; }
    }

    public bool ShowModal()
    {
      bool? result = ShowDialog();
      return result.HasValue && result.Value;
    }

    public void CloseView()
    {
      Close();
    }
  }
}
