﻿using System.Windows.Input;

namespace MorganStanley.MSDesktop.Rambo.API.View
{
  /// <summary>
  /// Interaction logic for UserInfolWindow.xaml
  /// </summary>
  public partial class UserInfoWindow : IUserInfoWindow
  {
    public UserInfoWindow()
    {
      InitializeComponent();
    }

    public object Model
    {
      get { return DataContext; }
      set { DataContext = value; }
    }

    public bool ShowModal()
    {
      bool? result = ShowDialog();
      return result.HasValue && result.Value;
    }

    public void CloseView()
    {
      Close();
    }

    protected override void OnKeyDown(KeyEventArgs e_)
    {
      if (e_.Key == Key.Escape)
      {
        CloseView();
      }
    }
  }
}