﻿using System;
using MorganStanley.MSDesktop.Rambo.DataServices;
using MorganStanley.MSDotNet.Directory;

namespace MorganStanley.MSDesktop.Rambo.API
{
  /// <summary>
  ///  Directory Access Manager
  /// </summary>
  public class DirectoryHandler
  { 
    /// <summary>
    /// <para>Check if the given user is member of the ASG Mail Group </para>
    /// <para>Exception could be thrown if connecting to the LDAP system fails</para>
    /// </summary>
    /// <param name="login_">Login name of the user to verify.</param>
    /// <returns>True if the user is member of the group</returns>
    /// <exception cref="Exception">Exeption occurs if connecting to the LDAP system fails</exception>
    public static bool IsInASGMailGroup(string login_)
    {
      return IsPersonOrProdIDInGroup(login_, Constants.ASG_MAIL_GROUP);
    }
   
    private static bool IsPersonOrProdIDInGroup(string login_, string group_)
    {
      var search = new FWD2Search();
      search.Connect(Constants.LdapAccessDn, Constants.LdapAccessPassword, null);
      return search.IsPersonOrProdIDInGroup(login_, group_);
    }

  }
}