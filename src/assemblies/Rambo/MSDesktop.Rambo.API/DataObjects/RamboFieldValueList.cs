﻿using System;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace MorganStanley.MSDesktop.Rambo.API
{
  [XmlRoot(ElementName = "RVRamboFieldValueList")]
  public class RamboFieldValueList : CommonBase<RamboFieldValueList>, IXmlSerializable
  {
    #region Constructor

    public RamboFieldValueList()
    {
      ValueList = new List<string>();
    }

    #endregion

    #region Properties

    /// <summary>
    /// Name of the column which was requested from DS
    /// </summary>
    [XmlElement(ElementName = "ColumnName")]
    public ColumnName ColumnName { get; set; }

    /// <summary>
    /// List of the values in the column specified in the ColumnName field
    /// </summary>
    public List<string> ValueList { get; set; }

    #endregion

    #region IXmlSerializable Members

    /// <summary>
    /// Not implemented
    /// </summary>
    /// <returns>Always returns NotImplementedException</returns>
    public XmlSchema GetSchema()
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// Method to support xml serialization
    /// </summary>
    /// <param name="reader_"></param>
    public void ReadXml(XmlReader reader_)
    {
      while (!reader_.EOF)
      {
        if (reader_.Name == "ColumnName")
        {
          ColumnName = (ColumnName)Enum.Parse(typeof(ColumnName), reader_.ReadElementString("ColumnName"), true);
          continue;
        }
        if (Enum.GetName(typeof(ColumnName), ColumnName).ToUpper() == reader_.Name.ToUpper())
        {
          ValueList.Add(reader_.ReadElementString(reader_.Name));
          continue;
        }
        reader_.Read();
      }
    }

    /// <summary>
    /// Not implemented
    /// </summary>
    /// <param name="writer_">This parameter is not in use</param>
    public void WriteXml(XmlWriter writer_)
    {
      string columnName = Enum.GetName(typeof (ColumnName), ColumnName);

      writer_.WriteElementString("ColumnName", columnName.Substring(0, 1).ToLower() + columnName.Substring(1));

      foreach (string valueItem in ValueList)
      {
        writer_.WriteElementString(columnName, valueItem);
      }
    }

    #endregion
  }
}