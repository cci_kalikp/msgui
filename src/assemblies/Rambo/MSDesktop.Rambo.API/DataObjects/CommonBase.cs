﻿using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace MorganStanley.MSDesktop.Rambo.API
{
  /// <summary>
  /// Helper class for common data object operations
  /// </summary>
  /// <typeparam name="T">Class type on which the operation will work</typeparam>
  public abstract class CommonBase<T> where T : class
  {
    /// <summary>
    /// Build up the instance from the xml stream
    /// </summary>
    /// <param name="reader_">Xml stream reader</param>
    /// <returns>The class instance created by the source stream</returns>
    public static T Deserialize(XmlReader reader_)
    {
      if (reader_ == null)
      {
        return null;
      }
      XmlSerializer serializer = new XmlSerializer(typeof(T));
      T deserializedObject = serializer.Deserialize(reader_) as T;
      return deserializedObject;
    }

    /// <summary>
    /// Create xml document from the class instance
    /// </summary>
    /// <returns>Created xml stream</returns>
    public XmlDocument Serialize()
    {
      XmlDocument serializedData = new XmlDocument();
      XmlSerializer serializer = new XmlSerializer(typeof(T));
      MemoryStream xmlStream = new MemoryStream();
      XmlSerializerNamespaces defaultNamespaces = new XmlSerializerNamespaces();
      defaultNamespaces.Add("", "");

      serializer.Serialize(xmlStream, this, defaultNamespaces);
      xmlStream.Flush();
      xmlStream.Seek(0, SeekOrigin.Begin);

      serializedData.Load(xmlStream);

      return serializedData;
    }
  }

  /// <summary>
  /// Technical class for empty callbacks
  /// </summary>
  internal class EmptyClass
  {
    public string Message { get; set; }

    public EmptyClass(string message_)
    {
      Message = message_;
    }
  }
}