﻿namespace MorganStanley.MSDesktop.Rambo.API.DataObjects
{
  public enum ResourceSaveState
  {
    NoWritePermission,
    Saved,
    FailSave,
    ///<summary>
    /// indicates ResourceName is "all" or empty, users cannot save resource using such name.
    ///</summary>
    InvalidParameter
  }
}
