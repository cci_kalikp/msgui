﻿using System.Xml;
using MorganStanley.MSDesktop.Rambo.Desktop.Utils;
using System.Xml.Serialization;

namespace MorganStanley.MSDesktop.Rambo.API.DataObjects
{
  [XmlRoot(ElementName = "RamboGroup")]
  public class RamboSharing
  {
    #region Serializing

    [XmlElement]
    public bool IsDeleted { get; set; }

    [XmlElement]
    public string GroupName { get; set; }

    [XmlElement]
    public string GroupType { get; set; }

    [XmlElement]
    public string RamboKey { get; set; }

    public void WriteXml(XmlElement ramboGroupsXml_)
    {
      XmlDocument doc = ramboGroupsXml_.OwnerDocument;
      if (doc == null) return;

      //doc.CreateElement("RamboGroups");
      XmlElement group = doc.CreateElement(Constants.RAMBO_SHARE_GROUP);
      XmlElement ramboKey = doc.CreateElement(Constants.RAMBO_SHARE_KEY);
      XmlElement groupName = doc.CreateElement(Constants.RAMBO_SHARE_GROUP_NAME);
      XmlElement groupType = doc.CreateElement(Constants.RAMBO_SHARE_GROUP_TYPE);
      
      group.AppendChild(ramboKey);
      group.AppendChild(groupName);
      group.AppendChild(groupType);
      
      XMLUtils.SetElementValue(group, Constants.RAMBO_SHARE_KEY, RamboKey);
      XMLUtils.SetElementValue(group, Constants.RAMBO_SHARE_GROUP_NAME, GroupName);
      XMLUtils.SetElementValue(group, Constants.RAMBO_SHARE_GROUP_TYPE, GroupType);
      
      if(IsDeleted)
      {
        XmlElement isDeleted = doc.CreateElement(Constants.RAMBO_SHARE_IS_DELETED);
        group.AppendChild(isDeleted);
        XMLUtils.SetElementValue(group, Constants.RAMBO_SHARE_IS_DELETED, Constants.RAMBO_SHARE_IS_DELETED_TRUE);
      }

      ramboGroupsXml_.AppendChild(group);
    }

    #endregion
  }
}
