﻿namespace MorganStanley.MSDesktop.Rambo.API.DataObjects
{
  public enum ResourceCopyState
  {
    InvalidUserName,
    OnAddNew,
    OnOverwrite,
    Copied,
    Failed
  }
}
