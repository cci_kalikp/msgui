﻿using System;
using System.Text;
using System.IO;
using System.Xml;
using System.Reflection;
using System.Xml.Serialization;
using MorganStanley.MSDesktop.Rambo.Desktop.Utils;

namespace MorganStanley.MSDesktop.Rambo.API
{
  /// <summary>
  /// Represents a resource stored in Rambo.
  /// 
  /// The xml serialization attributes are used by the XmlSerializer in Wingridutils.ResourceManager.
  /// </summary>
  public class RamboResource : ObservableObject
  {
    #region Fields

    private string _resourceID;

    private string _userName;
    private string _modifyUser;
    private string _applicationName;
    private string _resourceType;
    private string _appletName;

    //TODO: dhruvp - clean this up
    private string _gridName;
    private string _gridLayout;

    private string _profile;
    private string _configName;
    private string _resourceCategory;

    private int _isDeleted;

    private DateTime? _dataTime;
    private DateTime? _updateTime;
    private int? _exact;
    private DateTime? _startDataTime;
    private DateTime? _endDataTime;

    private XmlDocument _xmlData;

    #endregion

    #region Properties

    /// <summary>
    /// <para>ID of the resource</para>
    /// <para>This identifies one resource entry in the database</para>
    /// </summary>
    [XmlElement (ElementName = "Key")]
    public string ResourceId
    {
      get
      {
        if (String.IsNullOrEmpty(_resourceID))
        {
          _resourceID = GenerateKey(this.UserName,
                        this.ApplicationName,
                        this.Profile,
                        this.AppletName,
                        this.ResourceType,
                        this.ResourceCategory,
                        this.ResourceName);
        }
        return _resourceID;
      }
      set
      {
        if (_resourceID == value) return;
        _resourceID = value;
        InvokePropertyChanged(() => ResourceId);
      }
    }

    /// <summary>
    /// <para>Username of the resource object</para>
    /// <para>Value is set to "all" if the database contains NULL for this field</para>
    /// </summary>
    [XmlElement (ElementName = "RamboUserid")]
    public string UserName
    {
      get
      {
        return _userName ?? Constants.EmptyKeyString;
      }
      set
      {
        if (_userName == value) return;
        _userName = value;
        InvokePropertyChanged(() => UserName);
      }
    }

    /// <summary>
    /// Indicates who is the last Modify user
    /// </summary>
    [XmlElement(ElementName = "RamboModifyBy")]
    public string ModifyBy
    {
      get { return _modifyUser; }
      set
      {
        _modifyUser = value;
        InvokePropertyChanged(() => ModifyBy);
      }
    }

    /// <summary>
    /// <para>Application name of the resource object</para>
    /// <para>Value is set to "all" if the database contains NULL for this field</para>
    /// </summary>
    [XmlElement(ElementName = "RamboApplication")]
    public string ApplicationName
    {
      get
      {
        return _applicationName ?? Constants.EmptyKeyString;
      }
      set
      {
        if (_applicationName == value) return;
        _applicationName = value;
        InvokePropertyChanged(() => ApplicationName);
      }
    }
    /// <summary>
    /// Type of the resource object
    /// </summary>
    [XmlElement(ElementName = "RamboResType")]
    public string ResourceType
    {
      get
      {
        return _resourceType;
      }
      set
      {
        if (_resourceType == value) return;
        _resourceType = value;
        InvokePropertyChanged(() => ResourceType);
      }
    }
    /// <summary>
    /// <para>Applet name of the resource object</para>
    /// <para>Value is set to "all" if the database contains NULL for this field</para>
    /// </summary>
    [XmlElement(ElementName = "RamboApplet")]
    public string AppletName
    {
      get
      {
        return _appletName ?? Constants.EmptyKeyString;
      }
      set
      {
        if (_appletName == value) return;
        _appletName = value;
        InvokePropertyChanged(() => AppletName);
      }
    }
    /// <summary>
    /// <para>Grid name of the resource object</para>
    /// <para>Value is set to "all" if the database contains NULL for this field</para>
    /// </summary>
    public string GridName
    {
      get
      {
        return _gridName ?? Constants.EmptyKeyString;
      }
      set
      {
        if (_gridName == value) return;
        _gridName = value;
        InvokePropertyChanged(() => GridName);        
      }
    }
    /// <summary>
    /// <para>Grid layout of the resource object</para>
    /// <para>Value is set to "all" if the database contains NULL for this field</para>
    /// </summary>
    public string GridLayout
    {
      get
      {
        return _gridLayout ?? Constants.EmptyKeyString;
      }
      set
      {
        if (_gridLayout == value) return;
        _gridLayout = value;
        InvokePropertyChanged(() => GridLayout);
      }
    }
    /// <summary>
    /// <para>Profile name of the resource object</para>
    /// <para>Value is set to "all" if the database contains NULL for this field</para>
    /// </summary>
    [XmlElement(ElementName = "RamboProfile")]
    public string Profile
    {
      get
      {
        return _profile ?? Constants.EmptyKeyString;
      }
      set
      {
        if (_profile == value) return;
        _profile = value;
        InvokePropertyChanged(() => Profile);
      }
    }
    /// <summary>
    /// <para>Config name of the resource object</para>
    /// <para>Value is set to "all" if the database contains NULL for this field</para>
    /// </summary>
    public string ConfigName
    {
      get
      {
        return _configName ?? Constants.EmptyKeyString;
      }
      set
      {
        if (_configName == value) return;
        _configName = value;
        InvokePropertyChanged(() => ConfigName);
      }
    }

    /// <summary>
    /// <para>Name of the resource object</para>
    /// <para>Value is set to "all" if the database contains NULL for this field</para>
    /// </summary>
    [XmlElement(ElementName = "RamboResName")]
    public string ResourceName
    {
      get
      {
        return _configName ?? Constants.EmptyKeyString;
      }
      set
      {
        if (_configName == value) return;
        _configName = value;
        InvokePropertyChanged(() => ResourceName);
      }
    }


    /// <summary>
    /// Indicates if this resource item is marked as deleted
    /// </summary>
    [XmlElement(ElementName = "RamboIsDeleted")]
    public bool IsDeleted
    {
      get
      {
        return _isDeleted != 0;
      }
      set
      {
        _isDeleted = value ? 1 : 0;
        InvokePropertyChanged(() => IsDeleted);
      }
    }

    [XmlElement(ElementName = "DataTime")]
    public DateTime? DataTime
    {
      get { return _dataTime; }
      set
      {
        if (_dataTime == value) return;
        _dataTime = value;
        InvokePropertyChanged(() => DataTime);
      }
    }

    [XmlElement(ElementName = "UpdateTime")]
    public DateTime? UpdateTime
    {
      get { return _updateTime; }
      set
      {
        if (_updateTime == value) return;
        _updateTime = value;
        InvokePropertyChanged(() => UpdateTime);
      }
    }

    [XmlElement(ElementName = "Exact")]
    public int? Exact
    {
      get { return _exact; }
      set
      {
        if (_exact == value) return;
        _exact = value;
        InvokePropertyChanged(() => Exact);
      }
    }

    [XmlElement(ElementName = "StartDataTime")]
    public DateTime? StartDataTime
    {
      get { return _startDataTime; }
      set
      {
        if (_startDataTime == value) return;
        _startDataTime = value;
        InvokePropertyChanged(() => StartDataTime);
      }
    }

    [XmlElement(ElementName = "EndDataTime")]
    public DateTime? EndDataTime
    {
      get { return _endDataTime; }
      set
      {
        if (_endDataTime == value) return;
        _endDataTime = value;
        InvokePropertyChanged(() => EndDataTime);
      }
    }

    /// <summary>
    /// <para>Category of the resource object</para>
    /// <para>Value is set to "all" if the database contains NULL for this field</para>
    /// </summary>
    [XmlElement(ElementName = "RamboResCategory")]
    public string ResourceCategory
    {
      get
      {
        return _resourceCategory ?? Constants.EmptyKeyString;
      }
      set
      {
        if (_resourceCategory == value) return;
        _resourceCategory = value;
        InvokePropertyChanged(() => ResourceCategory);
      }
    }

    /// <summary>
    /// Xml data stored in the resource item
    /// </summary>
    public XmlDocument XmlData
    {
      get { return _xmlData; }
      set
      {
        if (_xmlData == value) return;
        _xmlData = value;
        InvokePropertyChanged(() => XmlData);
      }
    }
    /// <summary>
    /// Xml data stored in the resource item completed with the metadata
    /// </summary>
    public XmlDocument XmlDataWithParameters
    {
      get
      {
        XmlDocument document = new XmlDocument();
        if (XmlData.OuterXml.Length > 0)
        {
          document.LoadXml(XmlData.OuterXml);
        }

        Exact = Exact ?? 0;
        DataTime = DataTime ?? DateTime.Now.ToUniversalTime();
        UpdateTime = UpdateTime ?? DateTime.Now.ToUniversalTime();
        StartDataTime = StartDataTime ?? DateTime.Now.ToUniversalTime();
        EndDataTime = EndDataTime ?? DateTime.Now.ToUniversalTime();

        AddItemToDocument(document, "UserName", ColumnName.RamboUserid.ToString());
        AddItemToDocument(document, "ApplicationName", ColumnName.RamboApplication.ToString());
        AddItemToDocument(document, "Profile", ColumnName.RamboProfile.ToString());
        AddItemToDocument(document, "AppletName", ColumnName.RamboApplet.ToString());
        AddItemToDocument(document, "_resourceType", ColumnName.RamboResType.ToString());
        AddItemToDocument(document, "ResourceCategory", ColumnName.RamboResCategory.ToString());
        AddItemToDocument(document, "ResourceName", ColumnName.RamboResName.ToString());
        AddItemToDocument(document, "_isDeleted", ColumnName.RamboIsDeleted.ToString());
        AddItemToDocument(document, "ModifyBy", ColumnName.RamboModifyBy.ToString());

        AddItemToDocument(document, "DataTime", "DataTime");
        AddItemToDocument(document, "UpdateTime", "UpdateTime");
        AddItemToDocument(document, "StartDataTime", "StartDataTime");
        AddItemToDocument(document, "EndDataTime", "EndDataTime");
        AddItemToDocument(document, "Exact", "Exact");
        return document;
      }
    }
    /// <summary>
    /// Xml data stored in the resource item formatted as idented text
    /// </summary>
    public string XmlDataStringIdented
    {
      get
      {
        StringBuilder stringBuilder = new StringBuilder();
        StringWriter stringWriter = new StringWriter(stringBuilder);
        XmlTextWriter xmlTextWriter = null;

        try
        {
          xmlTextWriter = new XmlTextWriter(stringWriter) { Formatting = Formatting.Indented };
          XmlData.WriteTo(xmlTextWriter);
        }
        finally
        {
          if (xmlTextWriter != null)
          {
            xmlTextWriter.Close();
          }
        }

        return stringBuilder.ToString();
      }
      set
      {
        if (String.IsNullOrEmpty(value))
        {
          XmlData = new XmlDocument();
        }
        else
        {
          XmlData.LoadXml(value);
        }
        InvokePropertyChanged(() => XmlDataStringIdented);
      }
    }
    #endregion

    #region Constructors

    public RamboResource()
    {
      XmlData = new XmlDocument();
    }

    #endregion

    /// <summary>
    /// Method for serialization
    /// </summary>
    /// <param name="reader_">XmlReader to run through the xml document</param>
    /// <returns>Generated resource object</returns>
    public static RamboResource ReadXml(XmlReader reader_)
    {
      RamboResource resource = new RamboResource();
      XmlDocument xmlResult = new XmlDocument();
      xmlResult.Load(reader_);
      resource.GetAndRemoveNode(xmlResult.DocumentElement, ColumnName.RamboUserid.ToString(), "UserName");
      resource.GetAndRemoveNode(xmlResult.DocumentElement, ColumnName.RamboApplication.ToString(), "ApplicationName");
      resource.GetAndRemoveNode(xmlResult.DocumentElement, ColumnName.RamboApplet.ToString(), "AppletName");
      resource.GetAndRemoveNode(xmlResult.DocumentElement, ColumnName.RamboProfile.ToString(), "Profile");
      resource.GetAndRemoveNode(xmlResult.DocumentElement, ColumnName.RamboResType.ToString(), "_resourceType");
      resource.GetAndRemoveNode(xmlResult.DocumentElement, ColumnName.RamboResCategory.ToString(), "ResourceCategory");
      resource.GetAndRemoveNode(xmlResult.DocumentElement, ColumnName.RamboResName.ToString(), "ResourceName");
      resource.GetAndRemoveNode(xmlResult.DocumentElement, ColumnName.RamboIsDeleted.ToString(), "_isDeleted");
      resource.GetAndRemoveNode(xmlResult.DocumentElement, ColumnName.RamboModifyBy.ToString(), "ModifyBy");

      resource.GetAndRemoveNode(xmlResult.DocumentElement, "KeyStr", "ResourceId");
      resource.GetAndRemoveNode(xmlResult.DocumentElement, "Exact", "Exact");
      resource.GetAndRemoveNode(xmlResult.DocumentElement, "DataTime", "DataTime");
      resource.GetAndRemoveNode(xmlResult.DocumentElement, "UpdateTime", "UpdateTime");
      resource.GetAndRemoveNode(xmlResult.DocumentElement, "StartDataTime", "StartDataTime");
      resource.GetAndRemoveNode(xmlResult.DocumentElement, "EndDataTime", "EndDataTime");
      resource.XmlData = xmlResult;
      return resource;
    }

    private static DateTime? ParseDate(string value)
    {
      if (value != null)
      {
        //Not using TryParse because the value is expected to be a valid date
        //If the value is not a valid date then FormatException will occur and this is expected behaviour
        return DateTime.Parse(value);
      }
      return null;
    }

    private static int? ParseInt(string value)
    {
      if (value != null)
      {
        //Not using TryParse because the value is expected to be a valid int
        //If the value is not a valid int then FormatException will occur and this is expected behaviour
        return Int32.Parse(value);
      }
      return null;
    }

    /// <summary>
    /// Parses the information from the xml element, and removes the corresponding node. 
    /// The removal of node has been added to be compatible with legacy code.
    /// </summary>
    /// <param name="element"></param>
    /// <param name="colName"></param>
    /// <param name="removeNode"></param>
    /// <returns></returns>
    private static string ParseAndRemoveNode(XmlNode element, string colName, bool removeNode)
    {
      var node = element.SelectSingleNode("//" + colName);
      if (node != null)
      {
        var value = node.InnerText;
        if (removeNode && node.ParentNode != null) node.ParentNode.RemoveChild(node);
        return value;
      }
      return null;
    }

    /// <summary>
    /// Method for deserialization
    /// </summary>
    /// <param name="node"></param>
    /// <returns>Generated resource object</returns>
    public static RamboResource ReadXml(XmlNode node)
    {
      var resource = new RamboResource();
      resource.UserName = ParseAndRemoveNode(node, ColumnName.RamboUserid.ToString(), true);
      resource.ModifyBy = ParseAndRemoveNode(node, ColumnName.RamboModifyBy.ToString(), true);
      resource.ApplicationName = ParseAndRemoveNode(node, ColumnName.RamboApplication.ToString(), true);
      resource.AppletName = ParseAndRemoveNode(node, ColumnName.RamboApplet.ToString(), true);
      resource.Profile = ParseAndRemoveNode(node, ColumnName.RamboProfile.ToString(), true);
      resource.ResourceType = ParseAndRemoveNode(node, ColumnName.RamboResType.ToString(), true);
      resource.ResourceCategory = ParseAndRemoveNode(node, ColumnName.RamboResCategory.ToString(), true);
      resource.ResourceName = ParseAndRemoveNode(node, ColumnName.RamboResName.ToString(), true);
      resource._isDeleted = Int32.Parse(ParseAndRemoveNode(node, ColumnName.RamboIsDeleted.ToString(), true));

      //These properties can be null
      resource.DataTime = ParseDate(ParseAndRemoveNode(node, "DataTime", true));
      resource.UpdateTime = ParseDate(ParseAndRemoveNode(node, "UpdateTime", true));
      resource.StartDataTime = ParseDate(ParseAndRemoveNode(node, "StartDataTime", true));
      resource.EndDataTime = ParseDate(ParseAndRemoveNode(node, "EndDataTime", true));
      resource.Exact = ParseInt(ParseAndRemoveNode(node, "Exact", true));

      //remove rambo modify time and KeyStr node from xml
      ParseAndRemoveNode(node, "KeyStr", true);
      ParseAndRemoveNode(node, ColumnName.RamboModifyTime.ToString(), true);
      var doc = new XmlDocument();
      doc.LoadXml(node.OuterXml);
      resource.XmlData = doc;
      return resource;
    }

    private static object ConvertValue(string valueText_, Type valueType_)
    {
      Type realType = Nullable.GetUnderlyingType(valueType_) ?? valueType_;

      return Convert.ChangeType(valueText_, realType);
    }

    private MemberInfo GetPropertyOrField(string propertyName_)
    {
      PropertyInfo property = GetType().GetProperty(propertyName_);

      if (property != null)
      {
        return property;
      }
      return GetType().GetField(propertyName_, BindingFlags.NonPublic | BindingFlags.Instance);
    }

    private void GetAndRemoveNode(XmlNode node_, string nodeName_, string propertyName_)
    {
      XmlNode propertyNode = node_.SelectSingleNode("./" + nodeName_);
      MemberInfo member = GetPropertyOrField(propertyName_);

      if (propertyNode != null)
      {
        if (member != null)
        {
          PropertyInfo property = member as PropertyInfo;
          if (property != null)
          {
            object value = ConvertValue(propertyNode.InnerText, property.PropertyType);
            property.SetValue(this, value, null);
          }
          else
          {
            FieldInfo field = member as FieldInfo;
            if (field != null)
            {
              object value = ConvertValue(propertyNode.InnerText, field.FieldType);
              field.SetValue(this, value);
            }
          }
        }

        node_.RemoveChild(propertyNode);
      }
    }

    private void AddItemToDocument(XmlDocument document_, string propertyName_, string nodeName_)
    {
      if (document_.DocumentElement == null) return;

      MemberInfo member = GetPropertyOrField(propertyName_);
      string nodeInnerText = null;

      if (member != null)
      {
        object value = null;
        PropertyInfo property = member as PropertyInfo;
        if (property != null)
        {
          value = property.GetValue(this, null);
        }
        else
        {
          FieldInfo field = member as FieldInfo;
          if (field != null)
          {
            value = field.GetValue(this);
          }
        }

        if (value != null)
        {
          if (value is DateTime)
          {
            nodeInnerText = ((DateTime)value).ToString(Constants.FormatTimestamp);
          }
          else
          {
            nodeInnerText = value.ToString();
          }
        }
      }

      if (nodeInnerText == null) return;
      XmlNode existingNode = document_.DocumentElement.SelectSingleNode(nodeName_);
      if (existingNode != null) // if nodes already exist, update on the existing node
      {
        existingNode.InnerText = nodeInnerText;
      }
      else // otherwise, create a new node
      {
        XmlNode node = document_.CreateNode(XmlNodeType.Element, nodeName_, null);
        node.InnerText = nodeInnerText;
        document_.DocumentElement.AppendChild(node);
      }
    }

    #region Serializing

    public void WriteXml(XmlWriter writer_)
    {
      if (XmlData != null && !String.IsNullOrEmpty(XmlData.OuterXml))
      {
        XmlDataWithParameters.WriteTo(writer_);
      }
      else
      {
        writer_.WriteStartElement("Resource");
        writer_.WriteAttributeString(ColumnName.RamboId.ToString(), ResourceId);
        writer_.WriteAttributeString(ColumnName.RamboUserid.ToString(), UserName);
        writer_.WriteAttributeString(ColumnName.RamboApplication.ToString(), ApplicationName);
        writer_.WriteAttributeString(ColumnName.RamboProfile.ToString(), Profile);
        writer_.WriteAttributeString(ColumnName.RamboApplet.ToString(), AppletName);
        writer_.WriteAttributeString(ColumnName.RamboResType.ToString(), ResourceType);
        writer_.WriteAttributeString(ColumnName.RamboResCategory.ToString(), ResourceCategory);
        writer_.WriteAttributeString(ColumnName.RamboResName.ToString(), ResourceName);
        writer_.WriteAttributeString(ColumnName.RamboIsDeleted.ToString(), IsDeleted.ToString().ToLower());
        writer_.WriteEndElement();
      }
    }

    #endregion

    /// <summary>
    /// Generates a unique key based on the given parameters
    /// </summary>
    /// <param name="userName_"></param>
    /// <param name="applicationName_"></param>
    /// <param name="profileName_"></param>
    /// <param name="appletName_"></param>
    /// <param name="resourceType_"></param>
    /// <param name="resourceCategory_"></param>
    /// <param name="resourceName_"></param>
    /// <returns></returns>
    public static string GenerateKey(string userName_, string applicationName_, string profileName_, string appletName_, string resourceType_, string resourceCategory_, string resourceName_)
    {
      string key = string.Format("{0}/{1}/{2}/{3}/{4}/{5}/{6}",
        userName_ ?? Constants.EmptyKeyString,
        applicationName_ ?? Constants.EmptyKeyString,
        profileName_ ?? Constants.EmptyKeyString,
        appletName_ ?? Constants.EmptyKeyString,
        resourceType_ ?? Constants.EmptyKeyString,
        resourceCategory_ ?? Constants.EmptyKeyString,
        resourceName_ ?? Constants.EmptyKeyString);
      return key;
    }
  }
}