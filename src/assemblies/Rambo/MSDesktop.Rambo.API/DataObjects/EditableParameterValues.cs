﻿namespace MorganStanley.MSDesktop.Rambo.API.DataObjects
{
  ///<summary>
  /// stores the original value and the new value of parameters used for copying resources.
  /// A parameter can be any part of a resource key, i.e. username/resource type/resource category/resource name, etc
  ///</summary>
  public class EditableParameterValues
  {
    ///<summary>
    /// the value of the parameter before copying
    ///</summary>
    public string CurrentValue { get; set; }
    ///<summary>
    /// the value of the parameter user would like to rename as
    ///</summary>
    public string NewValue { get; set; }
  }
}
