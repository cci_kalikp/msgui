﻿using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using System.Xml.XPath;

namespace MorganStanley.MSDesktop.Rambo.API
{
  [XmlRoot(ElementName = "RVRamboResourceList")]
  public class RamboResourceList : CommonBase<RamboResourceList>, IXmlSerializable
  {
    private XmlSerializer serializer; //Used for deserializing RamboResource objects
    #region Properties

    public ObservableCollection<RamboResource> Items { get; set; }

    #endregion

    #region Constructor

    public RamboResourceList()
    {
      Items = new ObservableCollection<RamboResource>();
      serializer = new XmlSerializer(typeof(RamboResource), new XmlRootAttribute() { ElementName = "Resource" });
    }

    #endregion

    #region IXmlSerializable Members

    /// <summary>
    /// Not implemented
    /// </summary>
    /// <returns>NotImplementedException</returns>
    public System.Xml.Schema.XmlSchema GetSchema()
    {
      throw new System.NotImplementedException();
    }

    /// <summary>
    /// Function for serialization
    /// </summary>
    /// <param name="reader_">XmlReader to read the xml document</param>
    public void ReadXml(XmlReader reader_)
    {
      //Pick out the individual nodes for each RamboResouce and ask RamboResource to parse it.
      XmlDocument doc = new XmlDocument();
      XmlNode listNode = doc.ReadNode(reader_);
      if (listNode != null)
      {
        XmlNodeList resourceNodes = listNode.SelectNodes("Resource");
        if (resourceNodes != null)
        {
          foreach (XmlNode resourceNode in resourceNodes)
          {
            RamboResource newItem = RamboResource.ReadXml(new XmlNodeReader(resourceNode));
            Items.Add(newItem);
          } 
        }
      }
    }

    public void ReadXml(XmlElement element)
    {
      if (element != null)
      {
        var resourceNodes = element.SelectNodes("Resource");
        if (resourceNodes != null)
        {
          foreach (XmlNode resourceNode in resourceNodes)
          {
            //RamboResource newItem = RamboResource.ReadXml(resourceNode);
            //Items.Add(newItem);
            var newItem = serializer.Deserialize(new XmlNodeReader(resourceNode)) as RamboResource;
            Items.Add(newItem);
          } 
        }
      }
    }

    /// <summary>
    /// <para>Deserialization of this object is not supported</para>
    /// <para>This fuction always throw NotImplementedException</para>
    /// </summary>
    /// <param name="writer_"></param>
    public void WriteXml(XmlWriter writer_)
    {
      foreach (RamboResource resource in Items)
      {
        resource.WriteXml(writer_);
      }
    }

    #endregion

    public RamboResource[] ToArray()
    {
      var items = from RamboResource i in this.Items select i;
      return items.ToArray();
    }
  }
}