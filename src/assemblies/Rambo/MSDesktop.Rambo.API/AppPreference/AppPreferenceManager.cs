﻿using System;
using System.IO;
using System.Xml;
using MorganStanley.MSDesktop.Rambo.DataServices;
using MorganStanley.MSDesktop.Rambo.Desktop.Utils;

namespace MorganStanley.MSDesktop.Rambo.API.AppPreference
{
  public class AppPreferenceManager : IAppPreferenceManager
  {
    private const string DS_RAMBO_RESOURCE_APPPREFERENCE_KEY = "{0}/{1}/all/all/AppPreference/all/all";

    private const string PREFERENCE_RESOURCE_TYPE = "AppPreference";
    private static readonly AppPreferenceManager _instance = new AppPreferenceManager();
    private readonly IDataService _prefsDS = DataServiceFactoryLayer.GetDataService(Constants.RamboResourceDS);
    private readonly ILogger _log = new Logger(typeof(AppPreferenceManager));

    public static AppPreferenceManager Instance
    {
      get { return _instance; }
    }

    /// <summary>
    /// Save Perference
    /// </summary>
    /// <param name="user_"></param>
    /// <param name="applicationName_"></param>
    /// <param name="xmlElement_"></param>
    public void SavePreference(string user_, string applicationName_, XmlElement xmlElement_)
    {
      var resourceDocument = new XmlDocument();
      var sw = new StringWriter();
      var xw = new XmlTextWriter(sw);
      xmlElement_.WriteTo(xw);
      resourceDocument.LoadXml(sw.ToString());
      XmlDocument fullprefElement = CreateRamboResourceAppPreferenceXML(resourceDocument, user_, applicationName_);
      var obj =
        new XMLDataObject(
          new StringDataKey(string.Format(DS_RAMBO_RESOURCE_APPPREFERENCE_KEY, user_, applicationName_)),
          fullprefElement.DocumentElement);
      _prefsDS.Publish(obj);
    }

    /// <summary>
    /// Get Preference
    /// </summary>
    /// <param name="user_"></param>
    /// <param name="applicationName_"></param>
    /// <returns></returns>
    public XmlElement GetPreference(string user_, string applicationName_)
    {
      IDataKey key = DataKeyFactory.BuildDataKey(string.Format(DS_RAMBO_RESOURCE_APPPREFERENCE_KEY, user_, applicationName_));
      DataEvent snapshot = _prefsDS.GetSnapshot(key);
      XmlElement prefElement = null;
      try
      {
        if (snapshot != null)
        {
          prefElement = ((XMLDataObject)snapshot.DataObject).Value;
        }
      }
      catch (Exception ex)
      {
        _log.Critical("GetPreference", "Exception reading data out of dsResults", ex);
      }
      return prefElement;
    }

    /// <summary>
    /// Create RamboResource xml
    /// </summary>
    /// <param name="currentXML_"></param>
    /// <param name="userId_"></param>
    /// <param name="applicationName_"></param>
    /// <returns></returns>
    private static XmlDocument CreateRamboResourceAppPreferenceXML(XmlDocument currentXML_, string userId_,
                                                                   string applicationName_)
    {
      RamboResource screenResrouce = new RamboResource
                                       {
                                         AppletName = Constants.EmptyKeyString,
                                         ApplicationName = applicationName_,
                                         Profile = Constants.EmptyKeyString,
                                         UserName = userId_,
                                         ResourceType = PREFERENCE_RESOURCE_TYPE,
                                         ResourceCategory = Constants.EmptyKeyString,
                                         ResourceName = Constants.EmptyKeyString,
                                         IsDeleted = false,
                                         ModifyBy = Environment.UserName,
                                         XmlData = currentXML_
                                       };
      return screenResrouce.XmlDataWithParameters;
    }
  }
}