﻿using System.Xml;

namespace MorganStanley.MSDesktop.Rambo.API.AppPreference
{
  public interface IAppPreferenceManager
  {  
    /// <summary>
    /// Sync save preference
    /// </summary>
    /// <param name="user_"></param>
    /// <param name="applicationName_"></param>
    /// <param name="xmlElement_"></param>                               
    void SavePreference(string user_, string applicationName_, XmlElement xmlElement_);
    /// <summary>
    /// Sync get preference
    /// </summary>
    /// <param name="user_"></param>
    /// <param name="applicationName_"></param>
    /// <returns></returns>
    XmlElement GetPreference(string user_, string applicationName_);
  }
}
