﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Xml;
using System.Xml.Schema;
using MorganStanley.MSDesktop.Rambo.Desktop.Utils;

namespace MorganStanley.MSDesktop.Rambo.API
{
  public class RamboResourceXmlValidator
  {
    private static readonly ILogger _log = LoggerManager.GetLogger(typeof(RamboResourceXmlValidator));
    private static readonly IList<string> _resourceTypesToValidate = new List<string>(new[] { "GridLayout", "GridGrouping", "CustomGrouping", "AppScreen", "BasketTradingConfig" });

    public static void ValidateResourceXml(string resourceType_, XmlDocument doc_)
    {
      if (!_resourceTypesToValidate.Contains(resourceType_))
        return;

      var schemaFilePath = GetSchemaFile(resourceType_);
      if (!File.Exists(schemaFilePath))
      {
        _log.Error("ValidateResourceXml", string.Format("File {0} not found for validating resource type {1}", schemaFilePath, resourceType_));
        throw new FileNotFoundException();
      }
      doc_.Schemas.Add(string.Empty, schemaFilePath);
      doc_.Validate(OnInvalidResourceXml);
    }

    private static void OnInvalidResourceXml(object sender_, ValidationEventArgs e_)
    {
      _log.Error("XMLValidationCompleted", string.Format("Error when validating xml schema: {0}", e_.Message));
      throw new ApplicationException("Cannot save resource, error occurred while validating xml schema", e_.Exception);
    }

    private static string GetSchemaFile(string resourceType_)
    {
      return GetAssemblyPath() + string.Format(@"\MSDesktop.RamboAPI.{0}.xsd", resourceType_);
    }

    private static string GetAssemblyPath()
    {
      var path = System.Reflection.Assembly.GetExecutingAssembly().CodeBase;
      var uri = new Uri(path);
      var file = new FileInfo(uri.LocalPath);
      return file.DirectoryName;
    }
  }
}
