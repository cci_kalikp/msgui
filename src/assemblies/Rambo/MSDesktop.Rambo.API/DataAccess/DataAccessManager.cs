﻿using System.Data;
using System.Globalization;
using System.IO;
using System.Xml;
using MorganStanley.MSDotNet.MSDB;

namespace MorganStanley.MSDesktop.Rambo.API.DataAccess
{
  public class DataAccessManager
  {
    private static readonly MSConnectionManager _connectionManager;
    private static readonly IDbDataAdapter _adapter;

    private const string ACCESS_INFO_TAG = "SybaseAccess";
    private static readonly string _accessInfoFilePath = GetAssemblyPath() + @"\MSDesktop.RamboAdmin.SybaseAccess.xml";

    static DataAccessManager()
    {
      _connectionManager = new MSConnectionManager();
      var doc = new XmlDocument();
      doc.Load(_accessInfoFilePath);
      _connectionManager.AddAccessInfo(doc);

      _adapter = _connectionManager.GetDataAdapter(ACCESS_INFO_TAG);
    }

    internal static IDbConnection OpenConnection(string environment_)
    {
      _connectionManager.Context.Environment = environment_.ToLower();
      return _connectionManager.OpenConnection(ACCESS_INFO_TAG);
    }

    internal static DataSet GetFilledSetFromCommand(IDbConnection connection_, string command_)
    {
      var set = new DataSet
                  {
                    Locale = CultureInfo.InvariantCulture
                  };

      IDbCommand dbCommand = connection_.CreateCommand();
      dbCommand.CommandText = command_;
      dbCommand.ExecuteReader();

      _adapter.SelectCommand = dbCommand;
      _adapter.Fill(set);
      return set;
    }

    internal static string GetStringResultFromCommand(IDbConnection connection_, string command_)
    {
      IDbCommand dbCommand = connection_.CreateCommand();
      dbCommand.CommandText = command_;
      IDataReader reader = dbCommand.ExecuteReader();

      string result = null;
      if (reader.Read())
      {
        result = reader.GetString(0);
      }
      return result;
    }

    private static string GetAssemblyPath()
    {
      string path = System.Reflection.Assembly.GetExecutingAssembly().CodeBase.Replace(@"file:///", "");
      path = new FileInfo(path).DirectoryName;
      return path;
    }
  }
}