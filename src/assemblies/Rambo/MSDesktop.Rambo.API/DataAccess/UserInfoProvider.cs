﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows;
using System.Xml.Linq;
using System.Xml.XPath;
using JetBrains.Annotations;
using MorganStanley.MSDotNet.Directory;
using MorganStanley.MSDotNet.MSDB;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;

namespace MorganStanley.MSDesktop.Rambo.API.DataAccess
{
  internal static class UserInfoProvider
  {
    private static readonly string[] _profiles = {
                                                   "ln-ecdev", "ny-tradingPROD", "ln-tradingPROD", "tk-tradingPROD",
                                                   "hk-tradingPROD"
                                                 };


    public static string GetUserInfoReport([NotNull] string username_, [NotNull] FWD2Search search_,
                                           [NotNull] string environment_)
    {
      if (username_ == null) throw new ArgumentNullException("username_");
      if (search_ == null) throw new ArgumentNullException("search_");

      StringBuilder sb = new StringBuilder();
      try
      {
        using (IDbConnection dbConnection = DataAccessManager.OpenConnection(environment_))
        {
          sb.Append("User Info\n========\n");
          sb.Append(GetPersonData(username_, search_));
          sb.Append("\n");
          IList<string> screens = new List<string>();
          sb.Append("Layouts\n========\n");
          sb.Append(GetLayoutInfo(dbConnection, username_, ref screens));
          sb.Append("\n");
          sb.Append("Screen Data\n========\n");
          sb.Append(GetScreenInfo(dbConnection, screens));
          dbConnection.Close();
        }
        return sb.ToString();
      }
      catch (MSDBMultiException)
      {
        TaskDialog.ShowMessage("Failed to connect to database", "Error in database connection", TaskDialogCommonButtons.Close, VistaTaskDialogIcon.Error);
      }
      return null;
    }

    private static string GetPersonData(string username_, FWD2Search search_)
    {
      StringBuilder sb = new StringBuilder();
      Person person = search_.GetPersonByLogon(username_);
      string city = person.Location;
      string divisionDesc = search_.GetDivisionByCode(person.DivisionCode).Description;
      string departmentDesc = search_.GetDepartmentByCode(person.DepartmentCode).Description;
      string costCenter = search_.GetCostCenterByDepartment(person.CostCenter).Description;

      sb.AppendFormat("{0}, {1}, {2}, {3}, {4}\n", username_, city, divisionDesc, departmentDesc, costCenter);
      return sb.ToString();
    }

    private static string GetLayoutInfo(IDbConnection dbConnection_, string userId_, ref IList<string> screens_)
    {
      StringBuilder sb = new StringBuilder();
      foreach (var profile in _profiles)
      {
        string getLayoutCommand = "select res.xml " +
                                  "from Rambo2Meta r NOHOLDLOCK " +
                                  "join Rambo2Resource res NOHOLDLOCK " +
                                  "on res.id = r.id " +
                                  "where r.ramboApplication=\"riskviewerconfig\" " +
                                  "and r.ramboResType=\"Configurator\" " +
                                  "and r.ramboResName=\"MSDesktopLayouts\" " +
                                  "and r.ramboUserid=\"" + userId_ + "\"" +
                                  "and r.ramboProfile=\"" + profile + "\"";
        string result = DataAccessManager.GetStringResultFromCommand(dbConnection_, getLayoutCommand);
        if (result != null)
        {
          sb.Append(ExtractMSDesktopLayoutInfoFromXml(result, profile, ref screens_)); 
        }
        else
        {
          getLayoutCommand = "select res.xml " +
                                  "from Rambo2Meta r NOHOLDLOCK " +
                                  "join Rambo2Resource res NOHOLDLOCK " +
                                  "on res.id = r.id " +
                                  "where r.ramboApplication=\"riskviewerconfig\" " +
                                  "and r.ramboResType=\"Configurator\" " +
                                  "and r.ramboResName=\"Layouts\" " +
                                  "and r.ramboUserid=\"" + userId_ + "\"" +
                                  "and r.ramboProfile=\"" + profile + "\"";
          result = DataAccessManager.GetStringResultFromCommand(dbConnection_, getLayoutCommand);
          sb.Append(ExtractLayoutInfoFromXml(result, profile, ref screens_)); 
        }
        
      }
      return sb.ToString();
    }

    private static string GetScreenInfo(IDbConnection dbConnection_, IList<string> screens_)
    {
      StringBuilder sb = new StringBuilder();
      foreach (var screen in screens_)
      {
        string getScreenDataCommand = "select res.xml " +
                                      "from Rambo2Meta r NOHOLDLOCK " +
                                      "join Rambo2Resource res NOHOLDLOCK " +
                                      "on res.id = r.id " +
                                      "where r.ramboApplication=\"Ecdev.RiskViewer\" " +
                                      "and r.ramboResType=\"AppScreen\" " +
                                      "and r.ramboResName=\"" + screen + "\"";
        string result = DataAccessManager.GetStringResultFromCommand(dbConnection_, getScreenDataCommand);
        sb.Append(ExtractScreenInfoFromXml(result, screen));
      }
      return sb.ToString();
    }

    private static string ExtractMSDesktopLayoutInfoFromXml(string xmlString_, string profile_, ref IList<string> screens_)
    {
      if (xmlString_ == null)
        return string.Empty;

      StringBuilder sb = new StringBuilder();
      sb.Append(string.Format("Profile: {0}\n", profile_));

      XElement root = XElement.Parse(xmlString_);
      var defaultLayout = root.XPathSelectElement("./WindowManager/DefaultLayout");
      sb.Append(string.Format("Default Layout: {0}\n\n", defaultLayout != null ? defaultLayout.Value : "<none>"));
      var layouts = root.XPathSelectElements("./WindowManager/Layouts/Layout");
      foreach (var layout in layouts)
      {
        var name = layout.Attribute("name");
        if (name != null)
          sb.Append(string.Format("Layout: {0}\n\n", name.Value));

        var tabs = layout.XPathSelectElements("./State/Item/TabbedDock/Tabs/Tab");
        foreach (var tab in tabs)
        {
          var caption = tab.Attribute("Name");
          if (caption != null)
          {
            sb.Append(string.Format("Tab: {0}", caption.Value));
            var isActive = tab.Attribute("IsActive");
            if (isActive != null && isActive.Value == "true")
              sb.Append(string.Format(" (default tab)"));
            sb.Append("\n");
          }

          foreach (var view in tab.XPathSelectElements(".//IView"))
          {
            var title = view.Attribute("Title");
            if (title != null)
            {
              if (title.Value == "Risk Viewer")
              {
                sb.Append(string.Format("\tApplet: RiskViewer\n"));

                var screen = view.XPathSelectElement("./ConcordWindow/ConcordForm/State/RVScreen");
                if (screen != null)
                {
                  if (!string.IsNullOrEmpty(screen.Value))
                  {
                    screens_.Add(screen.Value);
                    sb.Append(string.Format("\t\tScreen: {0}\n", screen.Value));
                  }
                }
              }
              else if (!string.IsNullOrEmpty(title.Value))
              {
                sb.Append(string.Format("\tApplet: {0}\n", title.Value));
              }
            }
          }
        }
        sb.Append("\n");
      }

      return sb.ToString();
    }

    private static string ExtractLayoutInfoFromXml(string xmlString_, string profile_, ref IList<string> screens_)
    {
      if (xmlString_ == null)
        return string.Empty;

      StringBuilder sb = new StringBuilder();
      sb.Append(string.Format("Profile: {0}\n", profile_));

      XElement root = XElement.Parse(xmlString_);
      var defaultLayout = root.XPathSelectElement("./WindowManager/DefaultLayout");
      sb.Append(string.Format("Default Layout: {0}\n\n", defaultLayout != null ? defaultLayout.Value : "<none>"));
      var layouts = root.XPathSelectElements("./WindowManager/Layouts/Layout");
      foreach (var layout in layouts)
      {
        var name = layout.Attribute("name");
        if (name != null)
          sb.Append(string.Format("Layout: {0}\n\n", name.Value));

        string defaultTab = null;
        var selectedTab = layout.XPathSelectElement("./TabSplitter").Attribute("selectedTab");
        if (selectedTab != null)
          defaultTab = selectedTab.Value;
        var tabs = layout.XPathSelectElements("./TabSplitter/Tab");
        foreach (var tab in tabs)
        {
          var caption = tab.Attribute("caption");
          if (caption != null)
          {
            sb.Append(string.Format("Tab: {0}", caption.Value));
            if (caption.Value == defaultTab)
              sb.Append(string.Format(" (default tab)"));
            sb.Append("\n");
          }

          foreach (var pane in tab.XPathSelectElements(".//Pane"))
          {
            var title = pane.Attribute("Title");
            if (title != null)
            {
              if (title.Value == "Risk Viewer")
              {
                sb.Append(string.Format("\tApplet: RiskViewer\n"));

                var screen = pane.XPathSelectElement("./ConcordForm/State/RVScreen");
                if (screen != null)
                {
                  if (!string.IsNullOrEmpty(screen.Value))
                  {
                    screens_.Add(screen.Value);
                    sb.Append(string.Format("\t\tScreen: {0}\n", screen.Value));
                  }
                }
              }
              else if (!string.IsNullOrEmpty(title.Value))
              {
                sb.Append(string.Format("\tApplet: {0}\n", title.Value));
              }
            }
          }
        }
        sb.Append("\n");
      }

      return sb.ToString();
    }

    private static string ExtractScreenInfoFromXml(string xmlString_, string screen_)
    {
      if (xmlString_ == null)
        return string.Empty;

      StringBuilder sb = new StringBuilder();
      sb.Append(string.Format("Screen: {0}\n", screen_));

      XElement root = XElement.Parse(xmlString_);
      var tabs = root.XPathSelectElements("./Tabs/Tab");
      foreach (var tab in tabs)
      {
        sb.Append(string.Format("Tab: {0}\n", tab.XPathSelectElement("Name").Value));
        XElement layout = tab.XPathSelectElement("Layout");
        if (layout != null)
          sb.Append(string.Format("\tLayout: {0}\n", layout.Value));
        XElement grouping = tab.XPathSelectElement("Grouping");
        if (grouping != null)
          sb.Append(string.Format("\tGrouping: {0}\n", grouping.Value));
        IList<string> portfolios = new List<string>();
        foreach (var session in tab.XPathSelectElements("./Sessions/Session"))
        {
          XElement sessionName = session.XPathSelectElement("Name");
          if (sessionName != null && !string.IsNullOrEmpty(sessionName.Value) && !sessionName.Value.StartsWith("Regex:"))
            portfolios.Add(sessionName.Value);
          else
          {
            sb.Append(string.Format("\tSubscription:\n"));
            foreach (var attribute in session.Attributes())
            {
              sb.Append(string.Format("\t\t{0}: {1}\n", attribute.Name, attribute.Value));
            }
          }
        }

        foreach (var portfolio in portfolios)
        {
          sb.Append(string.Format("\tPortfolio: {0}\n", portfolio));
        }
      }

      sb.Append("\n");
      return sb.ToString();
    }
  }
}