﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Collections.Specialized;

using MorganStanley.MSDesktop.Rambo.Desktop.Utils;
using MorganStanley.IED.Concord.Runtime;
using MorganStanley.IED.Concord.Configuration;

namespace MorganStanley.MSDesktop.Rambo.API
{
  public class ConcordInterface : FileConfigurationStoreEx
  {
    #region Fields

    private NameValueCollection _settings;

    private string _application;
    private string _username;
    private string _region;
    private string _environment;
    private string _profile;

    internal readonly List<string> _excludeList = new List<string>();

    private static readonly ILogger Logger = new Logger(typeof(ConcordInterface));

    #endregion

    #region Constants

    internal const string CONFIGURATOR = "Configurator";

    #endregion

    #region Properties
    /// <summary>
    /// get ResourceDSName. By default it's RVRamboResource DS
    /// </summary>
    protected virtual string ResourceDSName
    {
      get
      {
        return Constants.RamboResourceDS;
      }
    }
    #endregion

    #region Overrides

    private void CommonInitialize(string settings_)
    {
      _settings = ParseProfileString(settings_);
      ExtractSettings();
      BuildExcludeList();

      if (_username == null)
      {
        _username = ConcordRuntime.Current.UserInfo != null ?
                    ConcordRuntime.Current.UserInfo.Username :
                    Environment.UserName;
      }
    }

    public override void Initialize(string settings_)
    {
      base.Initialize(settings_);
      CommonInitialize(settings_);
    }
    public override void Initialize(string settings_, XmlNode config_)
    {
      base.Initialize(settings_, config_);
      CommonInitialize(settings_);
    }

    private void BuildExcludeList()
    {
      Logger.Info("BuildExcludeList", "Reading exclude list from " + Constants.RamboConfigFileName + ".config file");
      _excludeList.Clear();

      XmlNode[] ramboSettingsNodes = GetFiles(_settings, Constants.RamboConfigFileName, null);
      if (ramboSettingsNodes == null)
      {
        return;
      }

      foreach (XmlNode ramboSettingsNode in ramboSettingsNodes)
      {
        if (ramboSettingsNode == null)
        {
          continue;
        }

        XmlNodeList excludeNodes = ramboSettingsNode.SelectNodes(Constants.RamboConfigSettingsXPath);
        if (excludeNodes == null)
        {
          continue;
        }

        foreach (XmlNode node in excludeNodes)
        {
          string configName = node.Attributes["Name"].Value;
          _excludeList.Add(configName);
        }
      }
    }

    /// <summary>
    /// Overridden method to use Rambo API by default to get Concord config settings
    /// </summary>
    /// <param name="configs_"></param>
    /// <returns></returns>
    public override ConfigurationSection[] ReadConfigurations(ConfigurationIdentityCollection configs_)
    {
      Logger.Info("ReadConfigurations", "Start reading configuration settings");
      ConfigurationSection[] configSections = new ConfigurationSection[configs_.Count];

      int index = 0;
      foreach (ConfigurationIdentity config in configs_)
      {
        configSections[index++] = ReadConfiguration(config.Name, config.Version);
      }

      return configSections;
    }

    private static XmlNode GetDefaultUserFileNode(string name_)
    {
      // Note: below method is copied from GetUserFile from local drive when there is no config found.
      // Since we are not using local file anymore, the default xml we created should match.
      XmlDocument doc = new XmlDocument();
      doc.LoadXml(String.Format("<{0} />", name_));
      return doc.DocumentElement;
    }

    /// <summary>
    /// Helper method that generates default xml based on resource name
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    private static XmlNode GetDefaultWriteNode(string name)
    {
      if (name == null) return null;
      var xmlReader = new XmlTextReader(new StringReader(String.Format("<{0}/>", name)));
      var xmlDocument = new XmlDocument();
      var node = xmlDocument.ReadNode(xmlReader);
      return node;
    }

    /// <summary>
    /// Overridden method to use Rambo API by default to get Concord config settings
    /// </summary>
    /// <param name="name_"></param>
    /// <param name="version_"></param>
    /// <returns></returns>
    public override ConfigurationSection ReadConfiguration(string name_, string version_)
    {
      Logger.Info("ReadConfiguration", "Load start. Read configuration settings for " + name_);

      try
      {
        XmlNode[] readOnlyNodes = GetFiles(_settings, name_, version_);
        if (_excludeList.Contains(name_))
        {
          Logger.Info("ReadConfiguration", "Load complete (success). Load read only resource data of " + name_ + " from file because it's in the exclude list");
          return new ConfigurationSection(name_, version_, readOnlyNodes, GetDefaultWriteNode(name_));
        }

        XmlNode writeNode = null;
        RamboResource resource = RamboAPI.GetResource(ResourceDSName, _username, _application, _profile, null, CONFIGURATOR, null, name_);

        if (resource != null && !resource.IsDeleted)
        {
          Logger.Info("ReadConfiguration", "Resource data of " + name_ + " loaded from Rambo database");
          writeNode = resource.XmlData.DocumentElement;
        }
        else
        {
          Logger.Info("ReadConfiguration", "Resource for " + name_ + " was not found in database");
        }

        if (writeNode == null)
        {
          writeNode = GetDefaultUserFileNode(name_);
        }

        Logger.Info("ReadConfiguration", "Load complete (success). Configuration settings for " + name_ + " successfully loaded");
        return new ConfigurationSection(name_, version_, readOnlyNodes, writeNode);
      }
      catch (Exception exception)
      {
        Logger.Error("ReadConfiguration", "Load complete (error)." + exception.Message + "\n" + exception.StackTrace);
        return new ConfigurationSection(name_, version_, null, GetDefaultWriteNode(name_));
      }
    }

    /// <summary>
    /// Overridden method to use Rambo API by default to save Concord config settings
    /// </summary>
    /// <param name="layoutName_"></param>
    /// <param name="version_"></param>
    /// <param name="node_"></param>
    public override void WriteConfiguration(string layoutName_, string version_, XmlNode node_)
    {
      Logger.Info("WriteConfiguration", "Writing configuration data of " + layoutName_);

      if (_username == null)
      {
        _username = ConcordRuntime.Current.UserInfo.Username;
      }

      RamboResource resource = new RamboResource
      {
        ResourceType = CONFIGURATOR,
        UserName = _username,
        ApplicationName = _application,
        Profile = _profile,
        ConfigName = layoutName_
      };

      resource.XmlData.LoadXml(node_.OuterXml);
      Logger.Info("WriteConfiguration", "Write configuration data of " + layoutName_ + " into database");
      RamboAPI.SaveResource(resource, OnReturnSaveResource);
    }

    #endregion

    private static void OnReturnSaveResource(DSResponse response_)
    {
      string errorMessage = "Cannot save resource";

      if (response_ != null && !response_.IsSuccessful && response_.Response is Exception)
      {
        errorMessage = (response_.Response as Exception).Message;
      }

      if (response_ == null || !response_.IsSuccessful)
      {
        Logger.Error("OnReturnSaveResource", errorMessage);
        return;
      }

      if (response_.IsSuccessful && response_.Response is EmptyClass)
      {
        string message = (response_.Response as EmptyClass).Message;
        Logger.Info("OnReturnSaveResource", "Resource saved successfully\nKey: " + message);
        return;
      }

      Logger.Info("OnReturnSaveResource", "Resource saved successfully");
    }

    private void ExtractSettings()
    {
      _application = _settings[Configurator.CONFIG_SETTING_APP_KEY];
      _username = Environment.GetEnvironmentVariable(Constants.EnvironmentVariableImpersonatedUser);
      _region = _settings[Configurator.CONFIG_SETTING_REGION_KEY];
      _environment = _settings[Configurator.CONFIG_SETTING_ENV_KEY];

      _profile = _region + "-" + _environment;
    }

    private string _servers;

    
  }
}


