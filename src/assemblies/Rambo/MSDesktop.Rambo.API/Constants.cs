﻿namespace MorganStanley.MSDesktop.Rambo.API
{
  public class Constants
  {
    #region Data service names

    internal const string RamboFieldValueListDS = "RVRamboFieldValueList";
    internal const string RamboResourceListDS = "RVRamboResourceList";
    internal const string RamboResourceDS = "RVRamboResource";
    public const string REGION_FIELD = "Region";
    public const string ENVIRONMENT_FIELD = "Environment";
    internal const string RAMBO_SHARING_DS = "RVRamboSharing";
    public const string APPLICATION_NAME = "Ecdev.RiskViewer";

    #endregion

    #region Error messages

    /// <summary>
    /// Error message in the case if DSManager cannot generate DS key because any part of the key is missing
    /// </summary>
    public const string ErrorMessageGeneratingDSKey = "One or more part of the DS key is missing";
    /// <summary>
    /// Error message in the case of API cannot get the item list for deletion
    /// </summary>
    public const string ErrorMessageGetDeletableItemList = "Cannot get the resource item before delete it";
    
    #endregion

    #region Constans for Rambo Entitlement

    public const string ASG_MAIL_GROUP = "tradeplant-ww-risk";

    #endregion

    /// <summary>
    /// Specifies how to format DateTime values in the objects
    /// </summary>
    public const string FormatTimestamp = "yyyy-MM-ddTHH:mm:ss";

    public const string EmptyKeyString = "all";
    internal const string EnvironmentVariableImpersonatedUser = "IMPERSONATED_USER";

    internal const string RamboConfigFileName = "Rambo";
    internal const string RamboConfigSettingsXPath = "/Rambo/ReadOnlyConfigs/Config";

    internal const string LdapAccessDn = "cn=rambo_api,ou=ldapids,o=Morgan Stanley";
    internal const string LdapAccessPassword = "RamboApi1";
    internal const string AdminUserGoupEntitled = "rambo_admin_access";

    public const string USER_TYPE = "user";
    public const string GROUP_TYPE = "group";

    internal const string RAMBO_SHARE_GROUPS = "RamboGroups";
    internal const string RAMBO_SHARE_GROUP = "RamboGroup";
    internal const string RAMBO_SHARE_GROUP_NAME = "GroupName";
    internal const string RAMBO_SHARE_GROUP_TYPE = "GroupType";
    internal const string RAMBO_SHARE_IS_DELETED = "IsDeleted";
    internal const string RAMBO_SHARE_IS_DELETED_TRUE = "Y";    
    internal const string RAMBO_SHARE_KEY = "RamboKey";
  }
}