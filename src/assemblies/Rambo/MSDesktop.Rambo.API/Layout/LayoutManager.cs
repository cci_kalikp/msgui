﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Xml.Serialization;
using MorganStanley.MSDesktop.Rambo.CafGuiAdmin;
using MorganStanley.MSDesktop.Rambo.Desktop.Utils;
using MorganStanley.MSDesktop.Rambo.API.DataObjects;
using MorganStanley.MSDesktop.Rambo.WinGridUtils;
using MorganStanley.MSDesktop.Rambo.AsyncUtils;

namespace MorganStanley.MSDesktop.Rambo.API.Layout
{
  /// <summary>
  /// This class is responsible for saving and retrieving layouts from Rambo.
  /// </summary>
  public class LayoutManager : RamboResourceManager, ILayoutManager
  {
    private readonly XmlSerializer _serializer;
    public const string GRID_LAYOUT_RESOURCE_TYPE = "GridLayout";
    private readonly ILogger _log = new Logger(typeof(LayoutManager));
    private delegate GridLayout[] LoadAllGridLayoutsListDelegate(string userName_,
                                                                string applicationName_,
                                                                string region_,
                                                                string environment_,
                                                                string appletName_,
                                                                string gridName_,
                                                                DSGridLoader.PermissionType permissionType_);
    private delegate GridLayout LoadGridLayoutDelegate(string userName_,
                                                       string applicationName_,
                                                       string region_,
                                                       string environment_,
                                                       string appletName_,
                                                       string gridName_,
                                                       string layoutName_,
                                                       bool forceReload_);

    public LayoutManager(ICafGuiAdminProxy cafGuiAdminProxy_ = null)
      : base(cafGuiAdminProxy_)
    {
      _serializer = new GridLayoutSerializer();
    }

    private static string GetUserParameter(string userName_, DSGridLoader.PermissionType permissionType_ )
    {
      switch (permissionType_)
      {
        case  DSGridLoader.PermissionType.ALL:
        case DSGridLoader.PermissionType.Group: //UserGrouping ds does not provide the correct user list for Group so we default to ALL behaviour
        case  DSGridLoader.PermissionType.CostCenter:
          return null; //We want to download layouts owned by everyone or we don't care about this attribute
       
        case DSGridLoader.PermissionType.User:
          return userName_;

        default: //default to ALL behaviour assuming user prefers to see more resources than less resources
          return null;
      }
    }

    public GridLayout[] LoadAllGridLayoutsList(string userName_,
                                                string applicationName_,
                                                string region_,
                                                string environment_,
                                                string appletName_,
                                                string gridName_,
                                                DSGridLoader.PermissionType permissionType_,
                                                bool forceReload_)
    {
      object[] resources;

      //Massage user parameter depending on type of subscription that needs to be done
      userName_ = GetUserParameter(userName_, permissionType_); //We want to download layouts owned by everyone

      if (permissionType_ == DSGridLoader.PermissionType.ALL || 
          permissionType_ == DSGridLoader.PermissionType.User ||
          permissionType_ == DSGridLoader.PermissionType.Group)
      {
        //Fetch the rambo resources
        resources = LoadResourceList(userName_,
                                      applicationName_,
                                      region_,
                                      environment_,
                                      appletName_,
                                      GRID_LAYOUT_RESOURCE_TYPE,
                                      gridName_,
                                      false,
                                      forceReload_);
      }
      else
      { //Special request to ds for cost centre subscription
        //As of RR33, the group filter is disabled
        //because the UserGrouping ds does not return a sensible result
        string costCentre = DSGridLoader.GetCostCentre();
        if (String.IsNullOrEmpty(costCentre))
        {
          return null;
        }

        //Get default attribute dictionary
        var attributes = PrepareDictionary(userName_,
                                           applicationName_,
                                           region_,
                                           environment_,
                                           appletName_,
                                           GRID_LAYOUT_RESOURCE_TYPE,
                                           gridName_,
                                           null,
                                           false);
        //Add the usergrouping
        costCentre = "CC-" + costCentre;
        attributes.Add("userGrouping", costCentre);
        resources = LoadResourceList(attributes, forceReload_);
      }
      
      //Cast to Layout
      var layouts = from r in resources select r as GridLayout;

      return layouts.ToArray();
    }

    /// <summary>
    /// Retrieves all grid layouts that match the application and grid name attributes.
    /// </summary>
    /// <param name="userName_"></param>
    /// <param name="applicationName_"></param>
    /// <param name="region_"></param>
    /// <param name="environment_"></param>
    /// <param name="appletName_"></param>
    /// <param name="gridName_"></param>
    /// <param name="permissionType_"></param>
    /// <returns></returns>
    public GridLayout[] LoadAllGridLayoutsList(string userName_,
                                                string applicationName_,
                                                string region_,
                                                string environment_,
                                                string appletName_,
                                                string gridName_,
                                                DSGridLoader.PermissionType permissionType_)
    {
      return LoadAllGridLayoutsList(userName_,
                             applicationName_,
                             region_,
                             environment_,
                             appletName_,
                             gridName_,
                             permissionType_,
                             false);
    }

    /// <summary>
    /// Initiates an async load of list of grid layouts.
    /// </summary>
    /// <param name="userName_"></param>
    /// <param name="applicationName_"></param>
    /// <param name="region_"></param>
    /// <param name="environment_"></param>
    /// <param name="appletName_"></param>
    /// <param name="gridName_"></param>
    /// <param name="permissionType_"></param>
    /// <param name="callback_"></param>
    /// <param name="state_"></param>
    /// <returns></returns>
    public IAsyncResult BeginLoadAllGridLayoutsList(string userName_,
                                                    string applicationName_,
                                                    string region_,
                                                    string environment_,
                                                    string appletName_,
                                                    string gridName_,
                                                    DSGridLoader.PermissionType permissionType_,
                                                    AsyncCallback callback_,
                                                    object state_)
    {
      var async = new Asynchronizer(callback_, state_, new AsyncSettings(true));
      return async.BeginInvoke(new LoadAllGridLayoutsListDelegate(LoadAllGridLayoutsList), Constants.EmptyKeyString,
                               applicationName_,
                               region_,
                               environment_,
                               appletName_,
                               gridName_,
                               permissionType_);
    }

    /// <summary>
    /// Retrieves all grid groupings (as a list) that match the application and grid name attributes.
    /// </summary> 
    /// <param name="result_">The async result_</param>
    /// <returns>Array of (empty) grouping objects.</returns>
    public GridLayout[] EndLoadAllGridLayoutsList(IAsyncResult result_)
    {
      var asr = (AsynchronizerResult)result_;
      return (GridLayout[])asr.AsynchronizerParent.EndInvoke(result_);
    }

    /// <summary>
    /// Loads a grid layout.
    /// </summary>
    /// <param name="userName_"></param>
    /// <param name="applicationName_"></param>
    /// <param name="region_"></param>
    /// <param name="environment_"></param>
    /// <param name="appletName_"></param>
    /// <param name="gridName_"></param>
    /// <param name="layoutName_"></param>
    /// <param name="forceReload_"></param>
    /// <returns></returns>
    public GridLayout LoadGridLayout(string userName_,
                                      string applicationName_,
                                      string region_,
                                      string environment_,
                                      string appletName_,
                                      string gridName_,
                                      string layoutName_, 
                                      bool forceReload_)
    {
      //Query Rambo
      //Providing null for location and environment variables so that profile defaults to "all"
      var layout = LoadResource(new GridLayout {Application = applicationName_, LayoutName = layoutName_, GridName = gridName_ },
                                forceReload_, 
                                userName_, 
                                applicationName_, 
                                region_, 
                                environment_, 
                                appletName_, 
                                GRID_LAYOUT_RESOURCE_TYPE, 
                                gridName_, 
                                layoutName_);
      return layout as GridLayout;
    }

    /// <summary>
    /// Inititates an async load of a grid layout.
    /// </summary>
    /// <param name="userName_"></param>
    /// <param name="applicationName_"></param>
    /// <param name="region_"></param>
    /// <param name="environment_"></param>
    /// <param name="appletName_"></param>
    /// <param name="gridName_"></param>
    /// <param name="layoutName_"></param>
    /// <param name="forceReload_"></param>
    /// <param name="callback_"></param>
    /// <param name="state_"></param>
    /// <returns></returns>
    public IAsyncResult BeginLoadGridLayout(string userName_,
                                              string applicationName_,
                                              string region_,
                                              string environment_,
                                              string appletName_,
                                              string gridName_,
                                              string layoutName_,
                                              bool forceReload_,
                                              AsyncCallback callback_,
                                              object state_)
    {
      var async = new Asynchronizer(callback_, state_, new AsyncSettings(true));
      return async.BeginInvoke(new LoadGridLayoutDelegate(LoadGridLayout), userName_,
                                                                            applicationName_,
                                                                            region_,
                                                                            environment_,
                                                                            appletName_,
                                                                            gridName_,
                                                                            layoutName_,
                                                                            forceReload_);
    }

    /// <summary>
    /// Completes the async call to load a layout. 
    /// </summary>	
    /// <param name="result_">The IAyncResult object.</param>
    /// <returns></returns>
    public GridLayout EndLoadGridLayout(IAsyncResult result_)
    {
      var asr = (AsynchronizerResult)result_;
      return (GridLayout)asr.AsynchronizerParent.EndInvoke(result_);
    }

    /// <summary>
    /// Saves a layout to Rambo.
    /// </summary>
    /// <param name="userName_"></param>
    /// <param name="applicationName_"></param>
    /// <param name="region_"></param>
    /// <param name="environment_"></param>
    /// <param name="appletName_"></param>
    /// <param name="gridName_"></param>
    /// <param name="layoutName_"></param>
    /// <param name="saveToDataService_">If set to false only the cache is updated. If set to true then cache and dataservice are updated.</param>
    /// <param name="layout_"></param>
    public ResourceSaveState SaveGridLayout(string userName_,
                                string applicationName_,
                                string region_,
                                string environment_,
                                string appletName_,
                                string gridName_,
                                string layoutName_,
                                bool saveToDataService_,
                                GridLayout layout_)
    {
      if (layout_ == null)
      {
        _log.Info("SaveGridLayout", "Layout is null so not saving anything.");
        return ResourceSaveState.FailSave;
      }

      _log.Info("SaveGridLayout", String.Format("Layout saved. Name: {0} ", layoutName_));
      return SaveResource(layout_,
                          userName_,
                          applicationName_,
                          region_,
                          environment_,
                          appletName_,
                          GRID_LAYOUT_RESOURCE_TYPE,
                          gridName_,
                          layoutName_,
                          saveToDataService_);
    }

    public IDictionary<string, string> GetEntitlement(GridLayout layout_, string userName_)
    {
      return GetEntitlement(layout_, userName_, layout_.Application, null, null, layout_.Application, GRID_LAYOUT_RESOURCE_TYPE, layout_.GridName, layout_.LayoutName);
    }

    protected override object DeserializeObject(RamboResource resource_)
    {
      var reader = new StringReader(resource_.XmlData.OuterXml);
      var layout =  _serializer.Deserialize(reader) as GridLayout;
      if(layout != null)
      {
        layout.Owner = resource_.UserName;
      }
      return layout;
    }

    protected override void SerializeObject(StringWriter writer_, Object o_)
    {
      _serializer.Serialize(writer_, o_);
    }

    protected override string GetMapKey(object obj_)
    {
      var layout = obj_ as GridLayout;
      var key = (layout == null) ? null : String.Format("{0},{1},{2}", layout.Application, 
                                                                        layout.GridName, 
                                                                        layout.LayoutName);
      return key;
    }

    protected override object GetObject(RamboResource res_)
    {
      return new GridLayout
      {
        Application = res_.ApplicationName,
        GridName = res_.ResourceCategory,
        UpdateID = res_.ModifyBy ?? res_.UserName,
        Owner = res_.UserName,
        LayoutName = res_.ResourceName
      };
    }
  }
}
