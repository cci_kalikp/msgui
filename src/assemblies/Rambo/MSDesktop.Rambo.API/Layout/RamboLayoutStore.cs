﻿using System;
using System.Collections.Generic;
using MorganStanley.MSDesktop.Rambo.WinGridUtils;

namespace MorganStanley.MSDesktop.Rambo.API.Layout
{
  /// <summary>
  /// A class that adapts LayoutManager for use with Wingridutils' DlgGridLayoutEditor
  /// </summary>
  public class RamboLayoutStore : LayoutExplorerStore
  {
    private readonly ILayoutManager _layoutManager;
    private RamboParameters _parameters;

    public RamboLayoutStore(GridLayout initialGridLayout_, string updateId_, ILayoutExplorer view_, ILayoutManager layoutManager_)
                            : base(initialGridLayout_, updateId_, view_)
    {
      _layoutManager = layoutManager_;
    }  
   
    public override void UpdateLocalCache(GridLayout layout_)
    {
      //The saveToDataService parameter is false so only the cache gets updated, ds is not
      _layoutManager.SaveGridLayout(_parameters.UserName,
                                    _parameters.ApplicationName, 
                                    null, 
                                    null, 
                                    _parameters.AppletName,
                                    layout_.GridName, 
                                    layout_.LayoutName, 
                                    false, 
                                    layout_ );
    }

    protected override void SaveLayout(GridLayout layout_, string userName_)
    {
      _layoutManager.SaveGridLayout(userName_,
                                   _parameters.ApplicationName,
                                   null,
                                   null,
                                   _parameters.AppletName,
                                   layout_.GridName,
                                   layout_.LayoutName,
                                   true,
                                   layout_);
    }

    protected override void Load(AsyncCallback callback_, DSGridLoader.PermissionType permissions_, string gridName_)
    {
      _layoutManager.BeginLoadAllGridLayoutsList(_parameters.UserName,
                                                 _parameters.ApplicationName,
                                                 _parameters.Region,
                                                 _parameters.Environment,
                                                 _parameters.AppletName,
                                                 gridName_,
                                                 permissions_,
                                                 callback_,
                                                 null
        );
    }

    protected override GridLayout[] EndLoad(IAsyncResult result_)
    {
      return _layoutManager.EndLoadAllGridLayoutsList(result_);
    }

    protected override GridLayout[] LoadList(DSGridLoader.PermissionType permissions_, string gridName_)
    {
      return _layoutManager.LoadAllGridLayoutsList(_parameters.UserName,
                                                   _parameters.ApplicationName,
                                                   _parameters.Region,
                                                   _parameters.Environment,
                                                   _parameters.AppletName,
                                                   gridName_,
                                                   permissions_);
    }

    public override GridLayout LoadGridLayout(string applicationName_, string gridName_, string layoutName_, bool forceReload_)
    {
      return _layoutManager.LoadGridLayout(_parameters.UserName,
                                            applicationName_,
                                            _parameters.Region,
                                            _parameters.Environment,
                                            _parameters.AppletName,
                                            gridName_,
                                            layoutName_,
                                            forceReload_);
    }

    public override void BeginLoadGridLayout(string applicationName_, string gridName_, string layoutName_, AsyncCallback callback_)
    {
      _layoutManager.BeginLoadGridLayout(_parameters.UserName,
                                         applicationName_,
                                         _parameters.Region,
                                         _parameters.Environment,
                                         _parameters.AppletName,
                                         gridName_,
                                         layoutName_,
                                         false,
                                         callback_,
                                         null);
    }

    public override GridLayout EndLoadGridLayout(IAsyncResult result_)
    {
      return _layoutManager.EndLoadGridLayout(result_);
    }

    protected override void DeleteLayout(GridLayout layout_, string userName_)
    {
      _layoutManager.DeleteResource(layout_, false, _parameters.UserName,
                                    _parameters.ApplicationName,
                                    _parameters.Region,
                                    _parameters.Environment,
                                    _parameters.AppletName,
                                    LayoutManager.GRID_LAYOUT_RESOURCE_TYPE,
                                    layout_.GridName,
                                    layout_.LayoutName);
    }

    protected override IDictionary<string, string> GetEntitlement(GridLayout layout_)
    {
      return _layoutManager.GetEntitlement(layout_,
                                           _parameters.UserName,
                                           _parameters.ApplicationName,
                                           _parameters.Region,
                                           _parameters.Environment,
                                           _parameters.AppletName,
                                           LayoutManager.GRID_LAYOUT_RESOURCE_TYPE,
                                           layout_.GridName,
                                           layout_.LayoutName);
    }

    public override bool HasEntitlement(IDictionary<string, string> groups_, string userName_)
    {
      return RamboResourceEntitlement.IfUserInGroup(groups_, userName_);
    }


    public override void ClearCache()
    {
      _layoutManager.ClearCache();
    }

    protected override void GrantEntitlement(GridLayout layout_, IDictionary<string, string> shareGroups_)
    {
      _layoutManager.GrantEntitlement(layout_, _parameters.UserName,
                                      _parameters.ApplicationName,
                                      Constants.EmptyKeyString,
                                      _parameters.AppletName,
                                      LayoutManager.GRID_LAYOUT_RESOURCE_TYPE,
                                      layout_.GridName,
                                      layout_.LayoutName, shareGroups_);
    }

    public IDictionary<string, string> StoreParameters
    {
      set
      {
        _parameters = new RamboParameters(value);
      }
    }

    /// <summary>
    /// Helper class that stores rambo paramters
    /// </summary>
    private class RamboParameters
    {
      public readonly string UserName;
      public readonly string ApplicationName;
      public readonly string Region;
      public readonly string Environment;
      public readonly string AppletName;
      public readonly string GridName;

      public RamboParameters(IDictionary<string, string> data_)
      {
        data_.TryGetValue(ColumnName.RamboUserid.ToString(), out UserName);
        data_.TryGetValue(ColumnName.RamboApplication.ToString(), out ApplicationName);
        data_.TryGetValue(Constants.REGION_FIELD, out Region);
        data_.TryGetValue(Constants.ENVIRONMENT_FIELD, out Environment);
        data_.TryGetValue(ColumnName.RamboApplet.ToString(), out AppletName);
        data_.TryGetValue(ColumnName.RamboResCategory.ToString(), out GridName);
      }
    }
  }
}
