﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDesktop.Rambo.API.DataObjects;
using MorganStanley.MSDesktop.Rambo.WinGridUtils;

namespace MorganStanley.MSDesktop.Rambo.API.Layout
{
  public interface ILayoutManager
  {
    GridLayout[] LoadAllGridLayoutsList(string userName_,
                                        string applicationName_,
                                        string region_,
                                        string environment_,
                                        string appletName_,
                                        string gridName_,
                                        DSGridLoader.PermissionType permissionType_,
                                        bool forceReload_);

    /// <summary>
    /// Retrieves all grid layouts that match the application and grid name attributes.
    /// </summary>
    /// <param name="userName_"></param>
    /// <param name="applicationName_"></param>
    /// <param name="region_"></param>
    /// <param name="environment_"></param>
    /// <param name="appletName_"></param>
    /// <param name="gridName_"></param>
    /// <param name="permissionType_"></param>
    /// <returns></returns>
    GridLayout[] LoadAllGridLayoutsList(string userName_,
                                        string applicationName_,
                                        string region_,
                                        string environment_,
                                        string appletName_,
                                        string gridName_,
                                        DSGridLoader.PermissionType permissionType_);

    /// <summary>
    /// Initiates an async load of list of grid layouts.
    /// </summary>
    /// <param name="userName_"></param>
    /// <param name="applicationName_"></param>
    /// <param name="region_"></param>
    /// <param name="environment_"></param>
    /// <param name="appletName_"></param>
    /// <param name="gridName_"></param>
    /// <param name="permissionType_"></param>
    /// <param name="callback_"></param>
    /// <param name="state_"></param>
    /// <returns></returns>
    IAsyncResult BeginLoadAllGridLayoutsList(string userName_,
                                             string applicationName_,
                                             string region_,
                                             string environment_,
                                             string appletName_,
                                             string gridName_,
                                             DSGridLoader.PermissionType permissionType_,
                                             AsyncCallback callback_,
                                             object state_);

    /// <summary>
    /// Retrieves all grid groupings (as a list) that match the application and grid name attributes.
    /// </summary> 
    /// <param name="result_">The async result_</param>
    /// <returns>Array of (empty) grouping objects.</returns>
    GridLayout[] EndLoadAllGridLayoutsList(IAsyncResult result_);

    /// <summary>
    /// Loads a grid layout.
    /// </summary>
    /// <param name="userName_"></param>
    /// <param name="applicationName_"></param>
    /// <param name="region_"></param>
    /// <param name="environment_"></param>
    /// <param name="appletName_"></param>
    /// <param name="gridName_"></param>
    /// <param name="layoutName_"></param>
    /// <param name="forceReload_"></param>
    /// <returns></returns>
    GridLayout LoadGridLayout(string userName_,
                              string applicationName_,
                              string region_,
                              string environment_,
                              string appletName_,
                              string gridName_,
                              string layoutName_,
                              bool forceReload_);

    /// <summary>
    /// Inititates an async load of a grid layout.
    /// </summary>
    /// <param name="userName_"></param>
    /// <param name="applicationName_"></param>
    /// <param name="region_"></param>
    /// <param name="environment_"></param>
    /// <param name="appletName_"></param>
    /// <param name="gridName_"></param>
    /// <param name="layoutName_"></param>
    /// <param name="forceReload_"></param>
    /// <param name="callback_"></param>
    /// <param name="state_"></param>
    /// <returns></returns>
    IAsyncResult BeginLoadGridLayout(string userName_,
                                     string applicationName_,
                                     string region_,
                                     string environment_,
                                     string appletName_,
                                     string gridName_,
                                     string layoutName_,
                                     bool forceReload_,
                                     AsyncCallback callback_,
                                     object state_);

    /// <summary>
    /// Completes the async call to load a layout. 
    /// </summary>	
    /// <param name="result_">The IAyncResult object.</param>
    /// <returns></returns>
    GridLayout EndLoadGridLayout(IAsyncResult result_);

    /// <summary>
    /// Saves a layout to Rambo.
    /// </summary>
    /// <param name="userName_"></param>
    /// <param name="applicationName_"></param>
    /// <param name="region_"></param>
    /// <param name="environment_"></param>
    /// <param name="appletName_"></param>
    /// <param name="gridName_"></param>
    /// <param name="layoutName_"></param>
    /// <param name="saveToDataService_">If set to false only the cache is updated. If set to true then cache and dataservice are updated.</param>
    /// <param name="layout_"></param>
    ResourceSaveState SaveGridLayout(string userName_,
                                     string applicationName_,
                                     string region_,
                                     string environment_,
                                     string appletName_,
                                     string gridName_,
                                     string layoutName_,
                                     bool saveToDataService_,
                                     GridLayout layout_);

    void ClearCache();

    /// <summary>
    /// Delete resource in Rambo by loading the resource and mark it as deleted.
    /// </summary>
    /// <param name="o_"></param>
    /// <param name="forceReload_">If true, the API reloads the resource from server, otherwise just tries retrieving from cache</param>
    /// <param name="userName_">Part of the key. Used to load the resource</param>
    /// <param name="applicationName_">Part of the key. Used to load the resource</param>
    /// <param name="region_">Used to load the resource. The profile part of the key is generated based on this information. If you want to ignore, use null, do not use EmptyKeyString or "all"</param>
    /// <param name="environment_">Used to load the resource. The profile part of the key is generated based on this information. If you want to ignore, use null, do not use EmptyKeyString or "all"</param>
    /// <param name="appletName_">Part of the key. Used to load the resource</param>
    /// <param name="resourceType_">Part of the key. Used to load the resource</param>
    /// <param name="resourceCategory_">Part of the key. Used to load the resource</param>
    /// <param name="resourceName_">Part of the key. Used to load the resource</param>
    /// <returns></returns>
    void DeleteResource(object o_,
                        bool forceReload_,
                        string userName_,
                        string applicationName_,
                        string region_,
                        string environment_,
                        string appletName_,
                        string resourceType_,
                        string resourceCategory_,
                        string resourceName_);

    ///<summary>
    /// Get Entitlement for a GridLayout
    ///</summary>
    ///<param name="gridLayout_"></param>
    ///<param name="userName_"></param>
    ///<returns></returns>
    IDictionary<string, string> GetEntitlement(GridLayout gridLayout_, string userName_);

    /// <summary>
    /// Get write permission for rambo resource
    /// </summary>
    /// <param name="o_"></param>
    /// <param name="userName_"></param>
    /// <param name="applicationName_"></param>
    /// <param name="region_"></param>
    /// <param name="environment_"></param>
    /// <param name="appletName_"></param>
    /// <param name="resourceType_"></param>
    /// <param name="resourceCategory_"></param>
    /// <param name="resourceName_"></param>
    IDictionary<string, string> GetEntitlement(object o_,
                                               string userName_,
                                               string applicationName_,
                                               string region_,
                                               string environment_,
                                               string appletName_,
                                               string resourceType_,
                                               string resourceCategory_,
                                               string resourceName_);

    /// <summary>
    /// Grant write permission for resource
    /// </summary>
    /// <param name="o_">The resource used to retrieve the corresponding RamboResource. This should be the same as the resource we want to share.</param>
    /// <param name="userName_">Part of Rambo key. Used to create RamboResource object in case it can't do lookup based on the input resource.</param>
    /// <param name="applicationName_">Part of Rambo key. Used to create RamboResource object in case it can't do lookup based on the input resource.</param>
    /// <param name="profile_">Part of Rambo key. Used to create RamboResource object in case it can't do lookup based on the input resource.</param>
    /// <param name="appletName_">Part of Rambo key. Used to create RamboResource object in case it can't do lookup based on the input resource.</param>
    /// <param name="resourceType_">Part of Rambo key. Used to create RamboResource object in case it can't do lookup based on the input resource.</param>
    /// <param name="resourceCategory_">Part of Rambo key. Used to create RamboResource object in case it can't do lookup based on the input resource.</param>
    /// <param name="resourceName_">Part of Rambo key. Used to create RamboResource object in case it can't do lookup based on the input resource.</param>
    /// <param name="groupsDic"></param>
    void GrantEntitlement(object o_, string userName_,
                          string applicationName_,
                          string profile_,
                          string appletName_,
                          string resourceType_,
                          string resourceCategory_,
                          string resourceName_, IDictionary<string, string> groupsDic);
  }
}