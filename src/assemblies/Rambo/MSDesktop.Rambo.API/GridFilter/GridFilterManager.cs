﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using JetBrains.Annotations;
using MorganStanley.MSDesktop.Rambo.CafGuiAdmin;
using MorganStanley.MSDesktop.Rambo.WinGridUtils.GridFilter;
using MorganStanley.MSDesktop.Rambo.WinGridUtils.Resource;
using System.Linq;

namespace MorganStanley.MSDesktop.Rambo.API.GridFilter
{
  public class GridFilterManager : RamboResourceManager, IGridFormatItemResourceManager
  {
    #region Constants

    private const string GRID_FILTER_RESOURCE_TYPE = "GridFilter";

    #endregion

    #region Readonly & Static Fields

    private readonly XmlSerializer _serializer = new XmlSerializer(typeof (WinGridUtils.GridFilter.GridFilter));

    #endregion

    #region Constructor

    public GridFilterManager(ICafGuiAdminProxy cafGuiAdminProxy_)
      : base(cafGuiAdminProxy_) {}

    #endregion

    #region Instance Methods
    /// <summary>
    /// Load all grid filters
    /// </summary>
    /// <param name="applicationName_"></param>
    /// <param name="gridName_"></param>
    /// <param name="loadingType_"></param>
    /// <returns></returns>
    public WinGridUtils.GridFilter.GridFilter[] LoadFiltersList(string applicationName_, string gridName_,
                                                                string loadingType_)
    {
      var resources = LoadResourceList(string.IsNullOrEmpty(loadingType_) ? Constants.EmptyKeyString : loadingType_,
                                       applicationName_,
                                       null,
                                       null,
                                       applicationName_,
                                       GRID_FILTER_RESOURCE_TYPE,
                                       gridName_, false);

      //Cast to Filter
      var filter = from r in resources select r as WinGridUtils.GridFilter.GridFilter;

      return filter.ToArray();
    }
    /// <summary>
    /// Save grid filter to Rambo Data service
    /// </summary>
    /// <param name="gridFilter_"></param>
    /// <param name="updateID"></param>
    public void SaveFilter(Rambo.WinGridUtils.GridFilter.GridFilter gridFilter_, string updateID)
    {
      SaveResource(gridFilter_,
                   gridFilter_.Owner,
                   gridFilter_.Application,
                   null,
                   null,
                   gridFilter_.Application,
                   GRID_FILTER_RESOURCE_TYPE,
                   gridFilter_.GridName,
                   gridFilter_.Name,
                   true);
    }

    public void DeleteGridFilter(IGridFormatItem gridFilter_)
    {
      if (gridFilter_.IsDeleted) return;
      gridFilter_.IsDeleted = true;
      DeleteResource(gridFilter_, true, gridFilter_.Owner, gridFilter_.Application, null, null, gridFilter_.Application,
                     GRID_FILTER_RESOURCE_TYPE, gridFilter_.GridName, gridFilter_.Name);
    }

    public IDictionary<string, string> GetEntitlement(IGridFormatItem gridFilter_)
    {
      return GetEntitlement(gridFilter_, gridFilter_.Owner, gridFilter_.Application, null, null,
                            gridFilter_.Application,
                            GRID_FILTER_RESOURCE_TYPE,
                            gridFilter_.GridName, gridFilter_.Name);
    }

    public void Share(IGridFormatItem gridFilter_, IDictionary<string, string> groups_)
    {
      GrantEntitlement(gridFilter_, gridFilter_.Owner, gridFilter_.Application, Constants.EmptyKeyString,
                                 gridFilter_.Application,
                                 GRID_FILTER_RESOURCE_TYPE, gridFilter_.GridName, gridFilter_.Name, groups_);
    }

    /// <summary>
    /// Derserialize grid filter from RamboResource object
    /// </summary>
    /// <param name="resource_"></param>
    /// <returns></returns>
    protected override object DeserializeObject(RamboResource resource_)
    {
      StringReader reader = new StringReader(resource_.XmlData.OuterXml);
      var filter = _serializer.Deserialize(reader) as WinGridUtils.GridFilter.GridFilter;
      if (filter != null)
      {
        filter.Owner = resource_.UserName;
      }
      return filter;
    }

    protected override string GetMapKey(object obj_)
    {
      var filter = obj_ as WinGridUtils.GridFilter.GridFilter;
      return (filter == null) ? null : filter.Key;
    }

    protected override object GetObject(RamboResource res_)
    {
      return new WinGridUtils.GridFilter.GridFilter
               {
                 Application = res_.ApplicationName,
                 GridName = res_.ResourceCategory,
                 UpdateID = res_.ModifyBy ?? res_.UserName,
                 Owner = res_.UserName,
                 Name = res_.ResourceName
               };
    }

    protected override void SerializeObject(StringWriter writer_, object o_)
    {
      _serializer.Serialize(writer_, o_);
    }

    #endregion

    #region IGridFormatItemResourceManager Members
    /// <summary>
    /// Load method for Grid Filter
    /// </summary>
    /// <param name="applicationName_"></param>
    /// <param name="gridName_"></param>
    /// <param name="filterName_"></param>
    /// <returns></returns>
    public WinGridUtils.GridFilter.GridFilter LoadFilter(string applicationName_, string gridName_, string filterName_)
    {
      LoadFiltersList(applicationName_, gridName_, string.Empty);

      var filter = new WinGridUtils.GridFilter.GridFilter { Application = applicationName_, Name = filterName_, GridName = gridName_ };

      object loadedFilter = LoadResource(
        filter,
        false,
        Constants.EmptyKeyString,
        applicationName_,
        null,
        null,
        applicationName_,
        GRID_FILTER_RESOURCE_TYPE,
        gridName_,
        filterName_);

      return loadedFilter as WinGridUtils.GridFilter.GridFilter;
    }

    /// <summary>
    /// Apply Filter and update the local cache
    /// </summary>
    /// <param name="gridFilter_"></param>
    /// <param name="updateID"></param>
    public void ApplyFilter(WinGridUtils.GridFilter.GridFilter gridFilter_, string updateID)
    {
      gridFilter_.UpdateID = updateID;
      UpdateLocalCache(gridFilter_);
    }

    /// <summary>
    /// Remove filter from cache
    /// </summary>
    /// <param name="gridFilter_"></param>
    public void RemoveItemFromCache(IGridFormatItem gridFilter_)
    {
      RemoveItemFromList(Constants.EmptyKeyString,
                         gridFilter_.Application,
                         null,
                         null,
                         gridFilter_.Application,
                         GRID_FILTER_RESOURCE_TYPE,
                         gridFilter_.GridName,
                         gridFilter_.Name);
    }
    /// <summary>
    /// Verify if user has write permission to grid filter
    /// </summary>
    /// <param name="gridFilter_"></param>
    /// <returns></returns>
    public bool VerifyWritePermission([NotNull] IGridFormatItem gridFilter_)
    {
      IDictionary<string, string> groups = GetEntitlement(gridFilter_, gridFilter_.Owner, gridFilter_.Application, null,
                                                          null, gridFilter_.Application, GRID_FILTER_RESOURCE_TYPE,
                                                          gridFilter_.GridName, gridFilter_.Name);
      return RamboResourceEntitlement.IfUserInGroup(groups, Environment.UserName);
    }

    #endregion
  }
}