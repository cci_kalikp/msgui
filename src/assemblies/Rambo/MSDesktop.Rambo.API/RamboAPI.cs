﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("RamboAPITest, PublicKey=0024000004800000940000000602000000240000525341310004000001000100A53E632AB17B4AE228F55617FA35B87735382F10F640472009578FEA68385EB2E18DA0E33ACCED3D638FC4F6405EE021A63D2A6A9FDDA7BA4F8C281211E0C74DCBEC713FA74686DBE7B4606F4464C5669B810FD5951104D6B72779C9E45EDFD512E6400F3C429AFFD681E8BAA8B108908610BD62F76ECAF2DD8807BA32B3A5EC")]

namespace MorganStanley.MSDesktop.Rambo.API
{
  /// <summary>
  /// <para>API to get Rambo data from the database</para>
  /// <para>Also contains overridden methods for Concord configuration handling</para>
  /// </summary>
  public class RamboAPI
  {

    private DSRequestCallbackDelegate _callback;
    private CommandType _commandType;
    private Dictionary<WriteableResourceProperties, string> _cloneParameters;

    /// <summary>
    /// Gets the resource item for the given ID
    /// </summary>
    /// <param name="resourceID_">Resource ID of the requested item</param>
    /// <param name="callback_">Callback method for standard callback handling</param>
    /// <returns>Data service manager object for custom callback handling</returns>
    public static void GetResource(string resourceID_, DSRequestCallbackDelegate callback_)
    {
      DSManager.BeginGetResource(resourceID_, callback_);
    }

    /// <summary>
    /// Gets the resource item defined by the given parameters
    /// </summary>
    /// <param name="userName_"></param>
    /// <param name="applicationName_"></param>
    /// <param name="profileName_"></param>
    /// <param name="appletName_"></param>
    /// <param name="resourceType_"></param>
    /// <param name="resourceCategory_"></param>
    /// <param name="resourceName_"></param>
    /// <param name="callback_"></param>
    public static void GetResource(string userName_, string applicationName_, string profileName_, string appletName_, string resourceType_, string resourceCategory_, string resourceName_, DSRequestCallbackDelegate callback_)
    {
      String resourceId = RamboResource.GenerateKey(userName_,
                                              applicationName_,
                                              profileName_, 
                                              appletName_,
                                              resourceType_,
                                              resourceCategory_,
                                              resourceName_);
      GetResource(resourceId, callback_);
    }

    /// <summary>
    /// Synchronously gets the resource item defined by the given parameters.
    /// </summary>
    /// <param name="serviceName_"></param>
    /// <param name="userName_"></param>
    /// <param name="applicationName_"></param>
    /// <param name="profileName_"></param>
    /// <param name="appletName_"></param>
    /// <param name="resourceType_"></param>
    /// <param name="resourceCategory_"></param>
    /// <param name="resourceName_"></param>
    public static RamboResource GetResource(string serviceName_, string userName_, string applicationName_, string profileName_, string appletName_, string resourceType_, string resourceCategory_, string resourceName_)
    {
      String resourceId = RamboResource.GenerateKey(userName_,
                                              applicationName_,
                                              profileName_,
                                              appletName_,
                                              resourceType_,
                                              resourceCategory_,
                                              resourceName_);
      return DSManager.GetResource(serviceName_, resourceId);
    }

    /// <summary>
    /// Get the list of the values for a given column
    /// </summary>
    /// <param name="columnName_">Name of the column for which the value list want to get</param>
    /// <param name="callback_">Callback method for standard callback handling</param>
    /// <returns>Data service manager object for custom callback handling</returns>
    public static void GetFieldValues(ColumnName columnName_, DSRequestCallbackDelegate callback_)
    {
      DSManager.BeginGetFieldValues(columnName_, callback_);
    }

    /// <summary>
    /// Lists resources that match criteria based on the given parameters.
    /// </summary>
    /// <param name="userName_"></param>
    /// <param name="applicationName_"></param>
    /// <param name="profileName_"></param>
    /// <param name="appletName_"></param>
    /// <param name="resourceType_"></param>
    /// <param name="resourceCategory_"></param>
    /// <param name="resourceName_"></param>
    /// <param name="callback_"></param>
    public static void ListResources(string userName_, string applicationName_, string profileName_, string appletName_, string resourceType_, string resourceCategory_, string resourceName_, DSRequestCallbackDelegate callback_)
    {
      DSManager.BeginGetResourceList(userName_, applicationName_, resourceType_, appletName_, resourceCategory_, profileName_, resourceName_, callback_);
    }

    /// <summary>
    /// Synchonously lists resources that match criteria based on the given parameters.
    /// </summary>
    /// <param name="userName_"></param>
    /// <param name="applicationName_"></param>
    /// <param name="profileName_"></param>
    /// <param name="appletName_"></param>
    /// <param name="resourceType_"></param>
    /// <param name="resourceCategory_"></param>
    /// <param name="resourceName_"></param>
    public static DSResponse ListResources(string userName_, string applicationName_, string profileName_, string appletName_, string resourceType_, string resourceCategory_, string resourceName_)
    {
      return DSManager.GetResourceList(userName_, applicationName_, resourceType_, appletName_, resourceCategory_, profileName_, resourceName_);
    }

    /// <summary>
    /// Saves the given resource's data into the database
    /// </summary>
    /// <param name="resource_">Resource item to save</param>
    /// <param name="callback_">Callback method for standard callback handling</param>
    /// <returns>Data service manager object for custom callback handling</returns>
    public static void SaveResource(RamboResource resource_, DSRequestCallbackDelegate callback_)
    {
      ValidateResource(resource_);
      DSManager.BeginSaveResource(resource_.ResourceId, resource_, callback_);
    }
    
    /// <summary>
    /// Deletes the given resource from Rambo.
    /// </summary>
    /// <param name="resource_"></param>
    public static void DeleteResource(RamboResource resource_, DSRequestCallbackDelegate callback_)
    {
      resource_.IsDeleted = true;
      SaveResource(resource_,callback_);
    }

    /// <summary>
    /// Undoes a previous delete operation.
    /// </summary>
    /// <param name="resource_"></param>
    public static void UnDeleteResource(RamboResource resource_, DSRequestCallbackDelegate callback_)
    {
      resource_.IsDeleted = false;
      SaveResource(resource_, callback_);
    }
    
    #region Clone resource

    /// <summary>
    /// Copies the specified Concord type resource item with the given new metadata
    /// </summary>
    /// <param name="username_">Requested username</param>
    /// <param name="application_">Requested application name</param>
    /// <param name="profile_">Requested profile name</param>
    /// <param name="newUserName_">New username for the copied item</param>
    /// <param name="newProfileName_">New profile name for the copied item</param>
    /// <param name="callback_">Callback method invoked after the DS request finished</param>
    /// <returns>Data service manager object for custom callback handling</returns>
    public static void CloneConcordLayout(string username_, string application_, string profile_, string newUserName_, string newProfileName_, DSRequestCallbackDelegate callback_)
    {
      RamboAPI api = new RamboAPI
                       {
                         _callback = callback_,
                         _commandType = CommandType.Clone,
                         _cloneParameters = new Dictionary<WriteableResourceProperties, string>
                                              {
                                                {WriteableResourceProperties.UserName, newUserName_},
                                                {WriteableResourceProperties.Profile, newProfileName_}
                                              }
                       };
      //GetConcordLayoutResource(username_, application_, profile_, api.OnCommandResourceLoaded);
      GetResource(username_, application_, profile_, null, "ConcordLayout", null, null, api.OnCommandResourceLoaded);
    }

    /// <summary>
    /// Copies the specified Configurator type resource item with the given new metadata
    /// </summary>
    /// <param name="username_">Requested username</param>
    /// <param name="application_">Requested application name</param>
    /// <param name="profile_">Requested profile name</param>
    /// <param name="configName_">Requested config name</param>
    /// <param name="newUserName_">New username for the copied item</param>
    /// <param name="newProfileName_">New profile name for the copied item</param>
    /// <param name="callback_">Callback method invoked after the DS request finished</param>
    /// <returns>Data service manager object for custom callback handling</returns>
    public static void CloneConfigurator(string username_, string application_, string profile_, string configName_, string newUserName_, string newProfileName_, DSRequestCallbackDelegate callback_)
    {
      RamboAPI api = new RamboAPI
                       {
                         _callback = callback_,
                         _commandType = CommandType.Clone,
                         _cloneParameters = new Dictionary<WriteableResourceProperties, string>
                                              {
                                                {WriteableResourceProperties.UserName, newUserName_},
                                                {WriteableResourceProperties.Profile, newProfileName_}
                                              }
                       };
      //GetConfiguratorResource(username_, application_, profile_, configName_, api.OnCommandResourceLoaded);
      GetResource(username_, application_, profile_, null, "Configurator", null, configName_, callback_);
    }

    /// <summary>
    /// Copies the specified Grid Layout type resource item with the given new metadata
    /// </summary>
    /// <param name="applet_">Requested applet name</param>
    /// <param name="gridName_">Requested grid name</param>
    /// <param name="gridLayoutName_">Reested grid layout name</param>
    /// <param name="newGridLayoutName_">New layout name for the copied item</param>
    /// <param name="newGridName_">New grid name for the copid item</param>
    /// <param name="callback_">Callback method invoked after the DS request finished</param>
    /// <returns>Data service manager object for custom callback handling</returns>
    public static void CloneGridLayout(string applet_, string gridName_, string gridLayoutName_, string newGridName_, string newGridLayoutName_, DSRequestCallbackDelegate callback_)
    {
      RamboAPI api = new RamboAPI
                       {
                         _callback = callback_,
                         _commandType = CommandType.Clone,
                         _cloneParameters = new Dictionary<WriteableResourceProperties, string>
                                              {
                                                {WriteableResourceProperties.GridLayout, newGridLayoutName_},
                                                {WriteableResourceProperties.GridName, newGridName_},
                                              }
                       };
      //GetGridLayoutResource(applet_, gridName_, gridLayoutName_, api.OnCommandResourceLoaded);
      GetResource(null, null, null, applet_, "GridLayout", gridName_, gridLayoutName_,
                  api.OnCommandResourceLoaded);
    }

    /// <summary>
    /// Copies the specified Miscellaneous type resource item with the given new metadata
    /// </summary>
    /// <param name="userName_">Requested username</param>
    /// <param name="application_">Requested application name</param>
    /// <param name="newUserName_">New username for the copied item</param>
    /// <param name="callback_">Callback method invoked after the DS request finished</param>
    /// <returns>Data service manager object for custom callback handling</returns>
    public static void CloneMiscellaneousLayout(string userName_, string application_, string newUserName_, DSRequestCallbackDelegate callback_)
    {
      RamboAPI api = new RamboAPI
                       {
                         _callback = callback_,
                         _commandType = CommandType.Clone,
                         _cloneParameters = new Dictionary<WriteableResourceProperties, string>
                                              {
                                                {WriteableResourceProperties.UserName, newUserName_},
                                              }
                       };
      //GetMiscellaneousResource(userName_, application_, api.OnCommandResourceLoaded);
      GetResource(userName_, application_, null, null, "Miscellaneous", null, null, callback_);
    }

    private void CloneResource(RamboResource resource_)
    {
      // Change property values
      foreach (WriteableResourceProperties propertyName in _cloneParameters.Keys)
      {
        PropertyInfo property = resource_.GetType().GetProperty(Enum.GetName(typeof(WriteableResourceProperties), propertyName));
        if (property != null)
        {
          property.SetValue(resource_, _cloneParameters[propertyName], null);
        }
      }
      // Save changes
      //DSManager.BeginSaveResource(GenerateDSKey(resource_), resource_, _callback);
      DSManager.BeginSaveResource(resource_.ResourceId, resource_, _callback);
    }

    #endregion

    private void OnCommandResourceLoaded(DSResponse response_)
    {
      // Create default callback response
      DSResponse errorResponse = new DSResponse(false);

      if (response_ != null && response_.IsSuccessful)
      {
        // Get resource from the response
        RamboResource resource = response_.Response as RamboResource;
        if (resource != null)
        {
          switch (_commandType)
          {
            case CommandType.Delete:
              //DeleteResource(resource);
              return;
            case CommandType.Clone:
              CloneResource(resource);
              return;
          }
        }
      }
      else
      {
        // Something went wrong during getting deletable / cloneable item but the reson is unknown
        errorResponse = response_;
      }
      // Callback with error message
      if (_callback != null)
      {
        _callback(errorResponse);
      }
    }

    /// <summary>
    /// Validates a RamboResource
    /// </summary>
    /// <param name="resource"></param>
    public static void ValidateResource(RamboResource resource)
    {
      //resource must not be null
      if (resource == null) throw new ApplicationException("Resource has not been set.");

      //A resource's type must be set.
      if (resource.ResourceType == null || resource.ResourceType == Constants.EmptyKeyString) 
          throw  new ApplicationException("Resource Type must be set.");

      //If applicationName is not set then category must be set.
      if ((resource.ResourceCategory == null || resource.ResourceCategory == Constants.EmptyKeyString) 
          &&
          (resource.ApplicationName == null || resource.ApplicationName == Constants.EmptyKeyString)
          ) throw new ApplicationException("Resource Category must be set when Application Name is not set.");
    }
  }
}
