//---------------------------------------------------------------------------
// ($Id $)
//---------------------------------------------------------------------------
//
// Copyright (c) 2003 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
//---------------------------------------------------------------------------

using System;

namespace MorganStanley.MSDesktop.Rambo.DataServices
{
	/// <summary cref="MorganStanley.MSDesktop.Rambo.DataServices.XMLDataEvent">
	/// DataServices that implement this interface guarantee
	/// to publish and fire XMLDataEvents
	/// </summary>
	/// <author><a href="mailto:Kunle.Adetona@morganstanley.com">Kunle Adetona</a></author>
  public interface IXMLDataService :IDataService 
  {
		/// <summary>
		/// Returns XML schema for this DataService
		/// </summary>
		/// <exception cref="MorganStanley.MSDesktop.Rambo.DataServices.DataServiceException">
		/// if any problems e.g. server-connection error 
		/// </exception>
    string Schema{ get; }
	}
}
