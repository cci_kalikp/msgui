//---------------------------------------------------------------------------
// ($Id $)
//---------------------------------------------------------------------------
//
// Copyright (c) 2003 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
//---------------------------------------------------------------------------

using System;

namespace MorganStanley.MSDesktop.Rambo.DataServices
{
  /// <summary>
  /// An interface for metadata-objects.
  /// Every implementation must override ToString(), GetHashCode(), Equals() of the base Object
  /// </summary>
  public interface IMetaDataObject : ICloneable 
  {
    /// <summary>
    /// Accessor to this object's value
    /// </summary>
    object Value { get; }
  }
}
