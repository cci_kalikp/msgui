//---------------------------------------------------------------------------
// ($Id $)
//---------------------------------------------------------------------------
//
// Copyright (c) 2003 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
//---------------------------------------------------------------------------

using System;

namespace MorganStanley.MSDesktop.Rambo.DataServices
{
	/// <summary>
	/// Represents a unique identifier for a Data Object.
	/// By definition, a data key is immutable.  
	/// </summary>
	/// <author><a href="mailto:Kunle.Adetona@morganstanley.com">Kunle Adetona</a></author>
	public interface IDataKey
	{
    /// <summary>
    /// String representation of the DataService Key
    /// </summary>
    string DSKey{ get; }

    /// <summary>
    /// String representation of the DataService Key including any addional key info like DataTime & UpdateTime.
    /// This is only really provided for debug purposes.
    /// </summary>
    string KeyString{ get; }

  }
}
