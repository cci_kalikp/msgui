using System;

namespace MorganStanley.MSDesktop.Rambo.DataServices
{
	/// <summary>
	/// Summary description for IVersionDataKey.
	/// </summary>
	public interface IVersionDataKey : IDataKey
	{
		DataServiceDate DataTime {	get; 	}

    DataServiceDate UpdateTime { get; }

    bool IsExactMatch { get; }

    string UserId { get; }

    new string DSKey{ get; }

    new string KeyString{ get; }
	}
}
