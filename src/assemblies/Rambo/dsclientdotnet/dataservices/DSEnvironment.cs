using System;
using System.Linq;
using System.Xml;
using System.Reflection;
using MorganStanley.MSDesktop.Rambo.Desktop.Utils;
using MorganStanley.IED.Concord.Configuration;
using MorganStanley.MSDesktop.Rambo.AsyncUtils;

namespace MorganStanley.MSDesktop.Rambo.DataServices
{
  /// <summary>
  /// This class contains information about the DataService environment.  This is initialized by defaults and environment variables 
  /// but is overriden by command line arguments.
  /// </summary>
  public static class DSEnvironment
  {
    private static readonly ILogger _log = new Logger(typeof(DSEnvironment));

    public enum DSLoc { LN, NY, TK, HK }
    public enum DSEnv { Prod, QA, Test }
    public enum DSTransport { Ktcp, Tcp, Http }

    public enum DSFilterLocation { Server, Client }

    internal static string USER_NAME = System.Environment.UserName;
    internal static readonly string OS_PLATFORM = System.Environment.OSVersion.Platform.ToString();
    internal static readonly string OS_VERSION = System.Environment.OSVersion.Version.ToString();
    internal static readonly string SYS_LOC = System.Environment.GetEnvironmentVariable("SYS_LOC");
    internal static readonly string HOST = System.Environment.MachineName;
    internal static string APP_PATH = System.Windows.Forms.Application.ExecutablePath;
    internal static readonly string PID = System.Runtime.Remoting.RemotingConfiguration.ProcessId;
    public static readonly string LIB_PATH;
    public static readonly string LIB_VERSION;

    internal static DSLoc DS_LOCATION = DSLoc.LN;
    internal static DSEnv DS_ENVIRONMENT = DSEnv.QA;
    internal static DSTransport DS_PREFERRED_TRANSPORT = DSTransport.Ktcp;
    internal static string DS_THREAD_POOL = ManagedThreadPool.NAME;
    internal static int DS_THREAD_POOL_SIZE = 5;
    internal static string DS_THREAD_POOL_CONFIG = null;

    internal static bool DS_USEDSSERVICES = true;

    internal static string DS_SERVERS_CONFIG = null;

    static string[] DS_SERVER_LIST = null;

    static string[] DSSERVICES_SERVER_LIST = null;

    internal static string DS_SERVER_OVERRIDES = null;

    internal static string DS_SERVER_OVERRIDES_CONFIG = null;

    internal static string DS_SERVER_OVERRIDES_CONFIG_NAME = null;

    internal static DSFilterLocation DS_FILTER_LOCATION = DSFilterLocation.Server;

    private static long SOAP_REQUEST_TIMEOUT = 200000;

    private static bool _cmdLineOverride = false;

    static DSEnvironment()
    {
      #region Attempt to get dll info
      Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
      for (int i = 0; i < assemblies.Length; i++)
      {
        string[] assemblyInfo = assemblies[i].FullName.Split(',', ' '); // all FullNames contain string like - "Ecdev.DataServices, Version=1.3.7.28617, Culture=neutral, PublicKeyToken=1d54ce5ab4216a2f"
        if (assemblyInfo[0].StartsWith("Ecdev.DataServices")) // then found our dll
        {
          LIB_PATH = assemblies[i].CodeBase;
          _log.Info("DSEnvironment", "LIB_PATH=" + LIB_PATH);

          // in future should be able to get this from assembly name - eg. Ecdev.DataServices.1.0.dll
          string[] versionInfo = assemblyInfo[2].Split('=');
          LIB_VERSION = versionInfo[1];
          _log.Info("DSEnvironment", "LIB_VERSION=" + LIB_VERSION);

          break;
        }
      }
      #endregion

      #region Attempt to set DS_LOCATION from SYS_LOC
      if (null != SYS_LOC)
      {
        string sysloc = SYS_LOC.ToLower();
        _log.Info("DSEnvironment", "SYS_LOC=" + sysloc);

        //Use the SYS_LOC variable to find the location
        //Make this mapping
        //eu -> ln
        //af -> ln
        //na -> ny
        //sa -> ny
        //au -> hk
        //as -> hk
        // ag) Amba, Concorde Block,UB City, Vittal Mallya Road
        // bg) Bangalore
        // bj) Beijing
        // by) Bombay
        // ci) Chennai
        // dy) Delhi
        // hy) Hyderabad
        // jk) Jakarta           
        // kk) kolkatta            
        // mc) MSE Building, Ayala Ave, 1220 Makati City Philippines
        // ml) Manila
        // mr) Union Bank Plaza, 
        //Meralco Ave, Mandaluyong City, Metro Manila
        // no) Noida           
        // pn) pune
        // se) Seoul
        // sg) Singapore
        // sh) Shanghai
        // sn) Sendai
        // tw) Taipei
        // zh) Zhuhai
        //as -> tk
        //tk.as -> tk
        //kj.as -> tk
        //kw.as -> tk
        //os.as -> tk
        //yo.as -> tk 

        string[] fields = sysloc.Split('.');

        if (fields.Length < 2)
        {
          String err = "SYS_LOC has been set incorrectly to " + sysloc + ". Source enviroment or set DS_LOC accordinlgy. Exiting !!!";
          _log.Error("DSEnvironment", err);
          throw new InvalidOperationException("DSEnvironment" + err);
        }
        else
        {
          //Get the last element
          string continent = fields[fields.Length - 1];
          string city = fields[fields.Length - 2];
          if (continent == "eu" || continent == "af")
            DS_LOCATION = DSLoc.LN;
          else if (continent == "na" || continent == "sa")
            DS_LOCATION = DSLoc.NY;
          else if (continent == "au")
            DS_LOCATION = DSLoc.HK;
          else if (continent == "as")
          {
            if (city == "tk" ||
                city == "kd" ||
                city == "kj" ||
                city == "os" ||
                city == "kw" ||
                city == "yo")
              DS_LOCATION = DSLoc.TK;

            else if (city == "ag" ||
                     city == "bg" ||
                     city == "bj" ||
                     city == "by" ||
                     city == "ci" ||
                     city == "dy" ||
                     city == "hk" ||
                     city == "hy" ||
                     city == "jk" ||
                     city == "kk" ||
                     city == "mc" ||
                     city == "ml" ||
                     city == "mr" ||
                     city == "no" ||
                     city == "pn" ||
                     city == "se" ||
                     city == "sg" ||
                     city == "sh" ||
                     city == "sn" ||
                     city == "tw" ||
                     city == "zh")
              DS_LOCATION = DSLoc.HK;
            else
            {
              //If we are in any other AS city, hard fail
              const string err = "AS SYS_LOC unsupported. Contact rpsds or set DS_LOC accordingly. Exiting !!!";
              _log.Error("DSEnvironment", err);
              throw new InvalidOperationException("DSEnvironment" + err);
            }
          }
          else
          {
            //If we are in any other continent, hard fail
            const string err = "SYS_LOC unsupported. Contact rpsds or set DS_LOC accordingly. Exiting !!!";
            _log.Error("DSEnvironment", err);
            throw new InvalidOperationException("DSEnvironment" + err);
          }
        }
      }
      else
      {
        const string err = "SYS_LOC variable not set. Please SYS_LOC environment accordingly.";
        _log.Error("DSEnvironment", err);
        throw new InvalidOperationException("DSEnvironment: " + err);
      }
      #endregion

      #region Apply app settings overrides if they exist

      SetEnvironmentOverrides("AppSettings", arg => System.Configuration.ConfigurationManager.AppSettings[arg]);

      if (System.Configuration.ConfigurationManager.AppSettings["ds.profileconfig"] == "true")
      {
        var dsNode = ((Configurator)ConfigurationManager.GetConfig("DataServices")).GetNode(".", null);
        if (dsNode != null)
        {
          SetEnvironmentOverrides("ConcordConfig",
                                  arg =>
                                  {
                                    var argNode = dsNode.SelectSingleNode(arg);
                                    return argNode == null ? null : argNode.InnerText;
                                  });
        }
      }

      var commandLineArgs = (from arg in System.Environment.GetCommandLineArgs()
                             let parts = arg.Split(new[] { '=' }, 2)
                             where parts.Length == 2 && (parts[0].StartsWith("ds.") || parts[0].StartsWith("/ds."))
                             select parts).ToDictionary(parts => parts[0], parts => parts[1], StringComparer.OrdinalIgnoreCase);
      if (commandLineArgs.Count > 0)
      {
        SetEnvironmentOverrides("Command line",
                                arg =>
                                {
                                  string value;
                                  if (!commandLineArgs.TryGetValue(arg, out value))
                                  {
                                    commandLineArgs.TryGetValue("/" + arg, out value);
                                  }
                                  return value;
                                });
      }

      #endregion



      #region setup thread pool
      if (DS_THREAD_POOL != ManagedThreadPool.NAME)
      {
        if (!ThreadPoolManager.Instance.ThreadPoolExists(DS_THREAD_POOL))
        {
          _log.Info("DSEnvironment", "Creating threadpool " + DS_THREAD_POOL + " with " + DS_THREAD_POOL_SIZE + " threads");
          ThreadPoolManager.Instance.CreateDynamicThreadPool(DS_THREAD_POOL, DS_THREAD_POOL_SIZE, DS_THREAD_POOL_SIZE);
        }
      }
      else
      {
        _log.Info("DSEnvironment", "Initialising ManagedThreadPool with " + DS_THREAD_POOL_SIZE + " threads");
        ManagedThreadPoolConfig.MaxWorkerThreads = DS_THREAD_POOL_SIZE;
      }
      //This is done after the ManagedThreadPoolConfig.MaxWorkerThreads else that will overwrite the config changes
      if (DS_THREAD_POOL_CONFIG != null)
      {
        try
        {
          XmlDocument doc = new XmlDocument();
          doc.Load(DS_THREAD_POOL_CONFIG);
          ThreadPoolManager.Instance.ConfigureThreadPools(doc.DocumentElement);
        }
        catch (Exception ex_)
        {
          _log.Error("DSEnvironment", "Couldn't configure ThreadPools from: " + DS_THREAD_POOL_CONFIG, ex_);
        }
      }
      #endregion
    }

    private static void SetEnvironmentOverrides(string from, Func<string, string> getValue)
    {
      GetEnumArgument<DSEnv>(from,
                             getValue,
                             val =>
                             {
                               DS_ENVIRONMENT = val;
                               _cmdLineOverride = true;
                             },
                             "ds.env");


      GetEnumArgument<DSLoc>(from,
                             getValue,
                             val =>
                             {
                               DS_LOCATION = val;
                               _cmdLineOverride = true;
                             },
                             "ds.region",
                             "ds.loc");


      GetEnumArgument<DSTransport>(from, getValue, value => DS_PREFERRED_TRANSPORT = value, "ds.preferredtransport");
      GetIntArgument(from, getValue, value => DS_THREAD_POOL_SIZE = value, "ds.threads");
      GetArgument(from, getValue, value => DS_THREAD_POOL = value, "ds.threadpool");
      GetArgument(from, getValue, value => DS_THREAD_POOL_CONFIG = value, "ds.threadpoolconfig");
      GetArgument(from, getValue, value => DS_SERVER_LIST = value.Split(','), "ds.servers"); // NBYRNE could to more checks to ensure 'tcp://' or 'http://'
      GetEnumArgument<DSFilterLocation>(from, getValue, value => DS_FILTER_LOCATION = value, "ds.cps");
      GetArgument(from, getValue, value => DSSERVICES_SERVER_LIST = value.Split(','), "ds.dsservices");
      GetArgument(from, getValue, value => DS_SERVER_OVERRIDES = value, "ds.serveroverrides");
      GetArgument(from, getValue, value => DS_SERVER_OVERRIDES_CONFIG = value, "ds.serveroverridesconfig");
      GetArgument(from, getValue, value => DS_SERVER_OVERRIDES_CONFIG_NAME = value, "ds.serveroverridesconfigname");
      GetArgument(from, getValue, value => DS_USEDSSERVICES = (value != "false"), "ds.usedsservices");
      GetArgument(from, getValue, value => DS_SERVERS_CONFIG = value, "ds.serversconfig");
    }

    private static void GetEnumArgument<T>(string from, Func<string, string> getValue, Action<T> assign, params string[] arguments)
    {
      GetArgument(from, getValue, val => assign((T)Enum.Parse(typeof(T), val, true)), arguments);
    }

    private static void GetIntArgument(string from, Func<string, string> getValue, Action<int> assign, params string[] arguments)
    {
      GetArgument(from, getValue, val => assign(Convert.ToInt32(val)), arguments);
    }

    private static void GetArgument(string from, Func<string, string> getValue, Action<string> assign, params string[] arguments)
    {
      var values = from arg in arguments
                   let val = getValue(arg)
                   where !string.IsNullOrWhiteSpace(val)
                   select new { Value = val.Trim(), Argument = arg };

      var value = values.FirstOrDefault();
      if (value != null)
      {
        try
        {
          _log.Info("SetEnvironmentOverrides", "argument: " + value.Argument + " = " + value + "from: " + from);
          assign(value.Value);
        }
        catch (ArgumentException)
        {
          _log.Error("SetEnvironmentOverrides", "Could not set " + value.Argument + " from:" + from);
        }
      }
    }


    internal static bool HasServerList
    {
      get { return (DS_SERVER_LIST != null); }
    }

    /// <summary>
    /// Indicates if the location (ds.loc) or environment (ds.env) parameters were specified, and overridden, by
    /// command line arguments to the application using this library.
    /// </summary>
    public static bool HasCommandLineOverride
    {
      get { return _cmdLineOverride; }
    }

    public static bool HasPotentialProdOverride()
    {
      if (DS_SERVER_LIST != null && DS_SERVER_LIST.Any())
      {
        return true;
      }

      if (DSSERVICES_SERVER_LIST != null && DSSERVICES_SERVER_LIST.Any())
      {
        return true;
      }

      if (!String.IsNullOrEmpty(DS_SERVERS_CONFIG))
      {
        return true;
      }

      if (!String.IsNullOrEmpty(DS_SERVER_OVERRIDES))
      {
        return true;
      }

      if (!String.IsNullOrEmpty(DS_SERVER_OVERRIDES_CONFIG))
      {
        return true;
      }

      if (!String.IsNullOrEmpty(DS_SERVER_OVERRIDES_CONFIG_NAME))
      {
        return true;
      }

      return false;
    }

    public static string[] ServerList
    {
      get
      {
        if (DS_SERVER_LIST == null)
        {
          return null;
        }
        else
        {
          string[] serverList = new string[DS_SERVER_LIST.Length];
          DS_SERVER_LIST.CopyTo(serverList, 0);
          return serverList;
        }
      }
    }

    internal static bool HasDSServicesServerList
    {
      get { return (DSSERVICES_SERVER_LIST != null); }
    }

    public static string[] DSServicesServerList
    {
      get
      {
        if (DSSERVICES_SERVER_LIST == null)
        {
          return null;
        }
        else
        {
          string[] serverList = new string[DSSERVICES_SERVER_LIST.Length];
          DSSERVICES_SERVER_LIST.CopyTo(serverList, 0);
          return serverList;
        }
      }
    }

    public static DSEnv Environment
    {
      get { return DS_ENVIRONMENT; }
    }

    public static DSLoc Location
    {
      get { return DS_LOCATION; }
    }

    public static DSTransport PreferredTransport
    {
      get { return DS_PREFERRED_TRANSPORT; }
      set { DS_PREFERRED_TRANSPORT = value; }
    }

    public static int ThreadPoolSize
    {
      get { return DS_THREAD_POOL_SIZE; }
    }

    public static long SoapRequestTimeout
    {
      get { return SOAP_REQUEST_TIMEOUT; }
      set { SOAP_REQUEST_TIMEOUT = value; }
    }

    public static DSFilterLocation FilterLocation
    {
      get { return DS_FILTER_LOCATION; }
    }

    /**
     * Gets and sets the username used by dataservices in its request headers
     */
    public static string Username
    {
      get { return USER_NAME; }
      set { USER_NAME = value; }
    }

    /**
     * Gets and sets the AppPath used by dataservices in it's request headers
     */
    public static string AppPath
    {
      get { return APP_PATH; }
      set { APP_PATH = value; }
    }

    /**
     * returns environment variable ds.useDSServices value
     */
    public static bool UseDSServices
    {
      get { return DS_USEDSSERVICES; }
    }
  }
}
