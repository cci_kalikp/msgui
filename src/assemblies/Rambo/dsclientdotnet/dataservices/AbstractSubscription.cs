//---------------------------------------------------------------------------
// ($Id $)
//---------------------------------------------------------------------------
//
// Copyright (c) 2003 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
//---------------------------------------------------------------------------

using System;
using System.Collections;
using MorganStanley.MSDotNet.MSNet;
using MorganStanley.MSDesktop.Rambo.DataServices.Util;

namespace MorganStanley.MSDesktop.Rambo.DataServices
{
  /// <summary>
  /// Abstract implementation of the ISubscription interface.
  /// </summary>
  public abstract class AbstractSubscription : ISubscription 
  {
    protected IDataService m_dataService;	
    protected SubType m_type;
    protected IList m_keys;
    protected IDictionary m_attributes;
    protected string m_userId;

    #region Constructors
    /// <summary>
    /// Constructor for global subscription
    /// </summary>
    /// <exception cref="System.ArgumentNullException">
    ///   thrown when dataservice_ is null
    /// </exception>
    protected AbstractSubscription( IDataService dataService_, string userId_ )
    {
      Utils.ArgumentNullException( "dataservice", dataService_ );
      m_dataService = dataService_; 
      m_userId = userId_;
      m_type = SubType.GLOBAL;
    }

    /// <summary>
    /// Constructor for collection subscription
    /// </summary>
    /// <exception cref="System.ArgumentNullException">
    ///   thrown when dataservice_ is null
    /// </exception>
    protected AbstractSubscription( IDataService dataService_, IList keys_, string userId_ )
    {
      Utils.ArgumentNullException( "dataservice", dataService_ );
      m_dataService = dataService_; 
      m_userId = userId_;
      m_keys = keys_;
      m_type = SubType.COLLECTION;
    }

    /// <summary>
    /// Constructor for attribute subscription
    /// </summary>
    /// <exception cref="System.ArgumentNullException">
    ///   thrown when dataservice_ is null
    /// </exception>
    protected AbstractSubscription( IDataService dataService_, IDictionary attributes_, string userId_, bool strictAttributeNames)
    {
      Utils.ArgumentNullException( "dataservice", dataService_ );
      m_dataService = dataService_; 
      m_userId = userId_;
      m_attributes = attributes_;
      m_type = strictAttributeNames ? SubType.STRICTATTRIBUTE : SubType.ATTRIBUTE;
    }
    #endregion

    #region Implementation of ISubscription interface
    public abstract event DataEventHandler OnDataUpdate;

    public virtual IDataService DataService
    {
      get { return m_dataService; }
    }

    public virtual SubType Type
    {
      get { return m_type; }
    }

    public virtual IList DataKeys
    {
      get
      {
        if ( m_type == SubType.COLLECTION )
        {
          return m_keys;
        }
        else
        {
          throw new NotImplementedException( "Not a collection subscription " ) ;
        }
      }
    }

    public virtual IDictionary Attributes
    {
      get
      {
          if (m_type == SubType.ATTRIBUTE || m_type == SubType.STRICTATTRIBUTE)
        {
          return m_attributes;
        }
        else
        {
          throw new NotImplementedException( "Not an attribute subscription " ) ;
        }
      }
    }

    public virtual string UserId
    {
      get { return m_userId; }
    }


    public virtual IDictionary Snapshot()
    {        
      return m_dataService.GetSnapshot( this );
    }		
    
    public virtual IAsyncResult BeginSnapshot( AsyncCallback callback_, Object state_ )
    {
      return m_dataService.BeginSnapshot( this, callback_, state_ );
    }

    public virtual IDictionary EndSnapshot( IAsyncResult result_ )
    {
      IDictionary dict = m_dataService.EndSnapshot( result_ );
      if ( dict != null )
      { 
        foreach ( DataEvent e in dict.Values )
        {				
          FireDataUpdate( e );          
        }
      }      
      return dict;
    }

    #endregion

    protected abstract void FireDataUpdate( DataEvent evnt_ );
  }
}
