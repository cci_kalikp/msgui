//---------------------------------------------------------------------------
// ($Id $)
//---------------------------------------------------------------------------
//
// Copyright (c) 2003 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
//---------------------------------------------------------------------------

using System;
using MorganStanley.MSDesktop.Rambo.DataServices.Util;

namespace MorganStanley.MSDesktop.Rambo.DataServices
{
	/// <summary>
	/// Encapsulates a string object that uniquely identifies a data-object.
	/// </summary>
	/// <author><a href="mailto:Kunle.Adetona@morganstanley.com">Kunle Adetona</a></author>
	public class StringDataKey : IDataKey, IComparable
	{
		private String m_key;

		/// <summary>
		/// public constructor
		/// </summary>
		/// <param name="key_"></param>
		public StringDataKey( String key_ )
		{
      Utils.ArgumentNullException( "key", key_ );
			m_key = key_;
		}

    public virtual string DSKey
    { 
      get { return m_key; } 
    }		

		/// <summary>
		/// overrides System.GetHashCode()
		/// </summary>ftdftse
		/// <returns></returns>
		public override int GetHashCode()
		{
			return KeyString.GetHashCode();
		}

		/// <summary>
		/// overrides Object.Equals().
		/// </summary>
		/// <param name="obj_"></param>
		/// <returns></returns>
    public override bool Equals( Object obj_ )
    {
      return (obj_ is StringDataKey) &&
             ((object)obj_ != null) &&
             KeyString == ((StringDataKey)obj_).KeyString;
    }

		public int CompareTo( object anotherKey )
		{
			StringDataKey otherObject = (StringDataKey)anotherKey ;

			return ( m_key.CompareTo( otherObject.m_key ) ) ;
		}

		/// <summary>
		/// return string representation of key
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			return KeyString;
		}

    /// <summary>
    /// return string representation of key
    /// </summary>
    /// <returns></returns>
    public virtual string KeyString
    {
      get { return m_key; }
    }
	}
}
