using System;
using System.Collections;
using System.ComponentModel;

namespace MorganStanley.MSDesktop.Rambo.DataServices
{
	/// <summary>
	/// Summary description for VersionDataKeyCollection.
	/// </summary>
  public class VersionDataKeyCollection: CollectionBase, ITypedList, IListSource, IBindingList
  {  
    #region constructors
    /// <summary>
    /// Initializes a new empty instance of the VersionDataKeyCollection class.
    /// </summary>
    public VersionDataKeyCollection()
    {
    }
  
    /// <summary>
    /// Initializes a new instance of the VersionDataKeyCollection class, containing elements
    /// copied from an array.
    /// </summary>
    /// <param name="items">
    /// The array whose elements are to be added to the new VersionDataKeyCollection.
    /// </param>
    public VersionDataKeyCollection(IVersionDataKey[] items)
    {
      this.AddRange(items);
    }

    /// <summary>
    /// Initializes a new instance of the VersionDataKeyCollection class, containing elements
    /// copied from another instance of VersionDataKeyCollection
    /// </summary>
    /// <param name="items">
    /// The VersionDataKeyCollection whose elements are to be added to the new VersionDataKeyCollection.
    /// </param>
    public VersionDataKeyCollection(VersionDataKeyCollection items)
    {
      this.AddRange(items);
    }
    #endregion 
       
    #region CollectionBase members

    /// <summary>
    /// Adds the elements of an array to the end of this VersionDataKeyCollection.
    /// </summary>
    /// <param name="items">
    /// The array whose elements are to be added to the end of this VersionDataKeyCollection.
    /// </param>
    public virtual void AddRange(IVersionDataKey[] items)
    {
      foreach (IVersionDataKey item in items)
      {
        this.List.Add(item);
      }
    }

    /// <summary>
    /// Adds the elements of another VersionDataKeyCollection to the end of this VersionDataKeyCollection.
    /// </summary>
    /// <param name="items">
    /// The VersionDataKeyCollection whose elements are to be added to the end of this VersionDataKeyCollection.
    /// </param>
    public virtual void AddRange(VersionDataKeyCollection items)
    {
      foreach (IVersionDataKey item in items)
      {
        this.List.Add(item);
      }
    }

    /// <summary>
    /// Adds an instance of type IVersionDataKey to the end of this VersionDataKeyCollection.
    /// </summary>
    /// <param name="value">
    /// The IVersionDataKey to be added to the end of this VersionDataKeyCollection.
    /// </param>
    public virtual void Add(IVersionDataKey value)
    {
      this.List.Add(value);         
    }

    /// <summary>
    /// Determines whether a specfic IVersionDataKey value is in this VersionDataKeyCollection.
    /// </summary>
    /// <param name="value">
    /// The IVersionDataKey value to locate in this VersionDataKeyCollection.
    /// </param>
    /// <returns>
    /// true if value is found in this VersionDataKeyCollection;
    /// false otherwise.
    /// </returns>
    public virtual bool Contains(IVersionDataKey value)
    {
      return this.List.Contains(value);
    }

    /// <summary>
    /// Return the zero-based index of the first occurrence of a specific value
    /// in this VersionDataKeyCollection
    /// </summary>
    /// <param name="value">
    /// The IVersionDataKey value to locate in the VersionDataKeyCollection.
    /// </param>
    /// <returns>
    /// The zero-based index of the first occurrence of the _ELEMENT value if found;
    /// -1 otherwise.
    /// </returns>
    public virtual int IndexOf(IVersionDataKey value)
    {
      return this.List.IndexOf(value);
    }

    /// <summary>
    /// Inserts an element into the VersionDataKeyCollection at the specified index
    /// </summary>
    /// <param name="index">
    /// The index at which the IVersionDataKey is to be inserted.
    /// </param>
    /// <param name="value">
    /// The IVersionDataKey to insert.
    /// </param>
    public virtual void Insert(int index, IVersionDataKey value)
    {
      this.List.Insert(index, value);
    }

    /// <summary>
    /// Gets or sets the IVersionDataKey at the given index in this VersionDataKeyCollection.
    /// </summary>
    public virtual IVersionDataKey this[int index]
    {
      get
      {
        return (IVersionDataKey) this.List[index];
      }
      set
      {
        this.List[index] = value;
      }
    }

    /// <summary>
    /// Removes the first occurrence of a specific IVersionDataKey from this VersionDataKeyCollection.
    /// </summary>
    /// <param name="value">
    /// The IVersionDataKey value to remove from this VersionDataKeyCollection.
    /// </param>
    public virtual void Remove(IVersionDataKey value)
    {
      this.List.Remove(value);    
    }

    /// <summary>
    /// Type-specific enumeration class, used by VersionDataKeyCollection.GetEnumerator.
    /// </summary>
    public class Enumerator: System.Collections.IEnumerator
    {
      private System.Collections.IEnumerator wrapped;

      public Enumerator(VersionDataKeyCollection collection)
      {
        this.wrapped = ((System.Collections.CollectionBase)collection).GetEnumerator();
      }

      public IVersionDataKey Current
      {
        get
        {
          return (IVersionDataKey) (this.wrapped.Current);
        }
      }

      object System.Collections.IEnumerator.Current
      {
        get
        {
          return (IVersionDataKey) (this.wrapped.Current);
        }
      }

      public bool MoveNext()
      {
        return this.wrapped.MoveNext();
      }

      public void Reset()
      {
        this.wrapped.Reset();
      }
    }

    /// <summary>
    /// Returns an enumerator that can iteIVersionDataKey through the elements of this VersionDataKeyCollection.
    /// </summary>
    /// <returns>
    /// An object that implements System.Collections.IEnumerator.
    /// </returns>        
    public new virtual VersionDataKeyCollection.Enumerator GetEnumerator()
    {
      return new VersionDataKeyCollection.Enumerator(this);
    }
    #endregion

    #region CollectionBase Notification Overrides

    protected override void OnClearComplete()
    {    
      if (ListChanged != null)
      {
        ListChangedEventArgs args = new ListChangedEventArgs (ListChangedType.Reset, -1, -1);
        ListChanged (this, args);
      }      
    }


    protected override void OnInsertComplete(int index_, object value_ )
    {
      if (ListChanged != null)
      {
        ListChangedEventArgs args = new ListChangedEventArgs (ListChangedType.ItemAdded, index_, -1);
        ListChanged (this, args);
      }     
    }

    protected override void OnRemoveComplete( int index_, object value_ )
    {       
      if (ListChanged != null)
      {
        ListChangedEventArgs args = new ListChangedEventArgs (ListChangedType.ItemDeleted, index_, -1);
        ListChanged (this, args);
      }           
    }


    protected override void OnSetComplete( int index_, object oldValue_, object newValue_ )
    {
      if ( ListChanged != null )
      {     
        ListChangedEventArgs args = new ListChangedEventArgs( ListChangedType.ItemChanged, index_, -1 );
        ListChanged( this, args );           
      }
    }

    #endregion

    #region ITypedList Members
    public PropertyDescriptorCollection GetItemProperties( PropertyDescriptor[] pd_ )
    {     
      return TypeDescriptor.GetProperties( typeof(IVersionDataKey) );
    }

    public string GetListName( PropertyDescriptor[] pd_ )
    {
      return "VersionDataKeyCollection";
    }
    #endregion

    #region IListSource Members
    public IList GetList()
    {
      return this;
    }

    public bool ContainsListCollection
    {
      get
      {
        return false;
      }
    }
    #endregion
    
    #region IBindList Members

    public event System.ComponentModel.ListChangedEventHandler ListChanged;

    public object AddNew()
    {
      throw new NotSupportedException();
    }
   
    public bool AllowEdit
    {
      get
      {
        return true;
      }
    }

    public bool AllowNew
    {
      get
      {
        return false;
      }
    }

    public bool AllowRemove
    {
      get
      {
        return true;
      }
    }

    public bool SupportsChangeNotification
    {
      get
      {
        return true;
      }
    }

    public bool SupportsSearching
    {
      get
      {
        return false;
      }
    }

    public bool SupportsSorting
    {
      get
      {
        return false;
      }
    }

    public bool IsSorted
    {
      get
      {
        return false;
      }
    }

    public void ApplySort(PropertyDescriptor property, System.ComponentModel.ListSortDirection direction)
    {      
      throw new NotSupportedException();
    }
    
    public PropertyDescriptor SortProperty
    {
      get
      {
        return null;
      }
    }
    
    public int Find(PropertyDescriptor property, object key)
    {      
      throw new NotSupportedException();   
    }

    public void AddIndex(PropertyDescriptor property)
    {      
      throw new NotSupportedException();
    }
    
    public ListSortDirection SortDirection
    {
      get
      {        
        return new ListSortDirection ();
      }
    }

    public void RemoveSort()
    { 
      throw new NotSupportedException();
    }

    public void RemoveIndex(PropertyDescriptor property)
    {      
      throw new NotSupportedException();
    }
    #endregion
  }
}
