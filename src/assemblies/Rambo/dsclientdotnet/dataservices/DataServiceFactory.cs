//---------------------------------------------------------------------------
// ($Id $)
//---------------------------------------------------------------------------
//
// Copyright (c) 2003 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
//---------------------------------------------------------------------------

using System;
using System.Configuration;
using System.Collections;
using System.Xml;
using MorganStanley.MSDotNet.MSNet;
using MorganStanley.MSDotNet.MSSoap.Common;

namespace MorganStanley.MSDesktop.Rambo.DataServices
{
  [Obsolete("This class is redundant - use DataServiceManager to get an instance of a DataService",false)]  
  public class DataServiceFactory 
  {
    protected static string[] m_serverArr;

    public const string TRANSPORT_HTTP = "http";
    public const string TRANSPORT_TCP = "tcp";

    public const string ENVIRONMENT_PROD = "prod";
    public const string ENVIRONMENT_QA = "qa";
    public const string ENVIRONMENT_TEST = "test";

    public const string LOCATION_LN = "ln";
    public const string LOCATION_NY	= "ny";
    public const string LOCATION_TK	= "tk";
   
    public static readonly char[] DOT = new Char[] { '.' };

    private DataServiceFactory()
    {
    }

    public static void Init( string environment_ )
    {
      string location = "ln";
      string sysloc = System.Environment.GetEnvironmentVariable("SYS_LOC");
      if ( sysloc != null )
      {
        string[] fields = sysloc.Split(DOT);
        if ( fields.Length >= 3 )
        {
          location = fields[fields.Length-2];
        }
      }
      Init( location, environment_, TRANSPORT_TCP );
    }

		public static void Init()
		{
      Init( ENVIRONMENT_PROD );
		}

		public static void Init( string filename_, string transport_ )
		{
			XmlDocument doc = new XmlDocument();
			doc.Load( filename_ );
			XmlNodeList list = doc.DocumentElement.SelectNodes("Server");
			m_serverArr = new String[list.Count];
			int i = 0;
			foreach ( XmlNode node in list )
			{
				if ( node is XmlElement && node.Attributes[transport_] != null )
				{					
					string server = transport_ + "://" + node.InnerText + ":" + node.Attributes[transport_].Value;		
					m_serverArr[i++] = server;			
				}
			}
		}


		public static void Init( string location_, string environment_, string transport_ )
		{		
			string filename = "P:\\dist\\ecdev\\dataservicehosts\\"+ environment_ + "\\" + location_ + "_" + environment_ + "_hosts.xml";
			Init( filename, transport_ );
		}

    public static void Init( String[] serverArr_ )
    { 
      m_serverArr = serverArr_;
    }

    public static String[] Servers
    {
      get
      {
        return m_serverArr;
      }
    }

    public static IDataService GetDataService(String key_, IMSNetLoop loop_, bool setupSub_)
    {   
      return DataServiceManager.GetDataService(key_,true,setupSub_);
    }    

    public static IDataService GetDataService(String key_, IMSNetLoop loop_) {      
      return GetDataService(key_, loop_, true);
    }
	}
}
