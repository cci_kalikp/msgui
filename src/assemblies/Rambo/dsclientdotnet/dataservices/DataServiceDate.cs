using System;
using System.Globalization;

namespace MorganStanley.MSDesktop.Rambo.DataServices
{
  /// <summary>
  ///This class wraps up dates within dataservices to format them and manage them
  ///</summary>
  public class DataServiceDate
  {
    private static readonly DateTimeFormatInfo _dateTimeFormat;

    static DataServiceDate()
    {
      _dateTimeFormat = new DateTimeFormatInfo();
      _dateTimeFormat.FullDateTimePattern = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff";
    }

    //This is the internal representation of the data that we use
    private DateTime _date;
    
    /// <summary>
    /// Void constructor is restricted - another constructor must be used to populate the date info for this object
    /// </summary>
    private DataServiceDate()
    {
    }

    /// <summary>
    /// Create a DataServiceDate with this complete date provided
    /// </summary>
    /// <param name="dt_">The date for this date</param>
    public DataServiceDate(DateTime dt_)
    {
      _date = dt_.ToUniversalTime();
      Normalise();
    }

    /// <summary>
    /// Create a DataServiceDate on this date at 23:59:59.990 GMT
    /// </summary>
    /// <param name="day_">The date</param>
    /// <param name="month_">The month - 0 based like Java calendars</param>
    /// <param name="year_">The year</param>
    public DataServiceDate(int day_, int month_, int year_)
    {
      CreateDate(day_, month_, year_);    
    }
    
    /// <summary>
    /// Create a DataServiceDate with this date string (in GMT)
    /// </summary>
    /// <param name="s_">The string in the correct format</param>
    public DataServiceDate(String s_)
    { 
      //If this has a time portion, use the normal parsing of the date        
      if (s_.IndexOf('T') != -1)
      {      
        _date = Convert.ToDateTime(s_, _dateTimeFormat);
        Normalise();
      }
      else //Otherwise, create it with the day, month, year
      { 
        int year = Convert.ToInt32(s_.Substring(0,4));
        int month = Convert.ToInt32(s_.Substring(5,2));
        int day = Convert.ToInt32(s_.Substring(8,2));
        CreateDate(day, month, year);
      }
    }
  
    /// <summary>
    /// Set the date with a time of 23:59:59.990 GMT
    /// </summary>
    /// <param name="day_">The date</param>
    /// <param name="month_">The month</param>
    /// <param name="year_">The year</param>
    private void CreateDate(int day_, int month_, int year_)
    {
      CultureInfo info = new System.Globalization.CultureInfo("en-GB", false);
      Calendar calendar = info.Calendar;

      _date = new DateTime(year_,month_,day_,23,59,59,990,calendar);

      Normalise();
    }
  
    /// <summary>
    /// Override the toString to format this into yyyy-MM-dd'T'HH:mm:ss.SSS
    /// </summary>
    /// <returns>The formatted string</returns>
    public override string ToString()
    {
      return _date.ToString("F", _dateTimeFormat);
    }  
  
    public bool Equals(DataServiceDate dsd_)
    {
      return _date.Equals(dsd_.Date);
    }

    public override bool Equals(Object obj)
    {
      if(obj.GetType() == this.GetType())
        return this.Equals((DataServiceDate)obj);
      else
        return false;
    }

    public override int GetHashCode()
    {
      return _date.GetHashCode();
    }

    /// <summary>
    /// An accessor to get the date back DataServiceDate represents
    /// </summary>
    public DateTime Date
    {
      get {return _date; }
    }
 
    private void Normalise()
    {
      _date = new DateTime(_date.Ticks - _date.Ticks%(TimeSpan.TicksPerMillisecond*10));
    }
  }
}