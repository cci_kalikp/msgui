﻿using MorganStanley.MSDesktop.Rambo.DataServices.SoapCps;

namespace MorganStanley.MSDesktop.Rambo.DataServices
{
  public interface IDataServiceFactory
  {
    IDataService GetDataServiceManager(string id_);
    IDataService GetDataServiceManager(string id_, ServerList serverList_);
    IDataService GetDataServiceManager(string id_, bool createIfNotYetRegistered_, bool setupStub_);
    IDataService GetDataServiceManager(string id_, bool createIfNotYetRegistered_, bool setupStub_, ServerList serverList_);
  }
}
