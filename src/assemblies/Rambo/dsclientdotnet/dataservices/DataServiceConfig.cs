﻿using System.Collections.Generic;
using MorganStanley.MSDotNet.MSNet;

namespace MorganStanley.MSDesktop.Rambo.DataServices
{
  /// <summary>
  /// The configuration object used to Init DataServiceManager
  /// </summary>
  public class DataServiceConfig
  {

    public DataServiceConfig(IMSNetLoop loop_)
    {
      Loop = loop_;
      Environment = DSEnvironment.Environment;
      Location = DSEnvironment.Location;
      Transport = DSEnvironment.PreferredTransport;
    }

    public IMSNetLoop Loop { get; set; }

    public DSEnvironment.DSEnv Environment { get; set; }

    public DSEnvironment.DSLoc Location { get; set; }

    public DSEnvironment.DSTransport Transport { get; set; }

    public IDictionary<string, string> DSMapping { get; set; }

    public string[] ServerArray { get; set; }
  }
}
