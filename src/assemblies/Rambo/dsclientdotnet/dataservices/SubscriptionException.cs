//---------------------------------------------------------------------------
// ($Id $)
//---------------------------------------------------------------------------
//
// Copyright (c) 2003 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
//---------------------------------------------------------------------------

using System;

namespace MorganStanley.MSDesktop.Rambo.DataServices
{
	/// <summary>
	/// An exception that occurs when attempting to create a dataservice subscription
	/// </summary>
	/// <author><a href="mailto:Kunle.Adetona@morganstanley.com">Kunle Adetona</a></author>
  public class SubscriptionException : DataServiceException
	{

		/// <summary>
		/// public constructor
		/// </summary>
		/// <param name="message_"></param>
		public SubscriptionException(String message_):base(message_)
		{
		}
    
		/// <summary>
		/// public constructor
		/// </summary>
		/// <param name="message_"></param>
		/// <param name="inner_"></param>
    public SubscriptionException(String message_,Exception inner_):base(message_,inner_)
    {
    }

	}
}
