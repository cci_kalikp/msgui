using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;

namespace MorganStanley.Ecdev.DataServices
{
	/// <summary>
	/// Summary description for VersionCollection.
	/// </summary>
  public class VersionCollection: CollectionBase, ITypedList, IListSource, IBindingList
  {  
    #region constructors
    /// <summary>
    /// Initializes a new empty instance of the VersionCollection class.
    /// </summary>
    public VersionCollection()
    {
    }

    /// <summary>
    /// Initializes a new instance of the VersionCollection class, containing elements
    /// copied from an array.
    /// </summary>
    /// <param name="items">
    /// The array whose elements are to be added to the new VersionCollection.
    /// </param>
    public VersionCollection(Version[] items)
    {
      this.AddRange(items);
    }

    /// <summary>
    /// Initializes a new instance of the VersionCollection class, containing elements
    /// copied from another instance of VersionCollection
    /// </summary>
    /// <param name="items">
    /// The VersionCollection whose elements are to be added to the new VersionCollection.
    /// </param>
    public VersionCollection(VersionCollection items)
    {
      this.AddRange(items);
    }
    #endregion 

    #region Specific methods

    public virtual VersionCollection GetUpdateTimes(DateTime dataTime_)
    {
      VersionCollection updates = new VersionCollection();
      foreach (Version item in this.List)
      {
        if (item.DataTime.Equals(dataTime_))
          updates.Add(item);        
      }
      return updates;
    }

    #endregion
   
    #region CollectionBase members

    /// <summary>
    /// Adds the elements of an array to the end of this VersionCollection.
    /// </summary>
    /// <param name="items">
    /// The array whose elements are to be added to the end of this VersionCollection.
    /// </param>
    public virtual void AddRange(Version[] items)
    {
      foreach (Version item in items)
      {
        this.List.Add(item);
      }
    }

    /// <summary>
    /// Adds the elements of another VersionCollection to the end of this VersionCollection.
    /// </summary>
    /// <param name="items">
    /// The VersionCollection whose elements are to be added to the end of this VersionCollection.
    /// </param>
    public virtual void AddRange(VersionCollection items)
    {
      foreach (Version item in items)
      {
        this.List.Add(item);
      }
    }

    /// <summary>
    /// Adds an instance of type Version to the end of this VersionCollection.
    /// </summary>
    /// <param name="value">
    /// The Version to be added to the end of this VersionCollection.
    /// </param>
    public virtual void Add(Version value)
    {
      this.List.Add(value);         
    }

    /// <summary>
    /// Determines whether a specfic Version value is in this VersionCollection.
    /// </summary>
    /// <param name="value">
    /// The Version value to locate in this VersionCollection.
    /// </param>
    /// <returns>
    /// true if value is found in this VersionCollection;
    /// false otherwise.
    /// </returns>
    public virtual bool Contains(Version value)
    {
      return this.List.Contains(value);
    }

    /// <summary>
    /// Return the zero-based index of the first occurrence of a specific value
    /// in this VersionCollection
    /// </summary>
    /// <param name="value">
    /// The Version value to locate in the VersionCollection.
    /// </param>
    /// <returns>
    /// The zero-based index of the first occurrence of the _ELEMENT value if found;
    /// -1 otherwise.
    /// </returns>
    public virtual int IndexOf(Version value)
    {
      return this.List.IndexOf(value);
    }

    /// <summary>
    /// Inserts an element into the VersionCollection at the specified index
    /// </summary>
    /// <param name="index">
    /// The index at which the Version is to be inserted.
    /// </param>
    /// <param name="value">
    /// The Version to insert.
    /// </param>
    public virtual void Insert(int index, Version value)
    {
      this.List.Insert(index, value);
    }

    /// <summary>
    /// Gets or sets the Version at the given index in this VersionCollection.
    /// </summary>
    public virtual Version this[int index]
    {
      get
      {
        return (Version) this.List[index];
      }
      set
      {
        this.List[index] = value;
      }
    }

    /// <summary>
    /// Removes the first occurrence of a specific Version from this VersionCollection.
    /// </summary>
    /// <param name="value">
    /// The Version value to remove from this VersionCollection.
    /// </param>
    public virtual void Remove(Version value)
    {
      this.List.Remove(value);    
    }

    /// <summary>
    /// Type-specific enumeration class, used by VersionCollection.GetEnumerator.
    /// </summary>
    public class Enumerator: System.Collections.IEnumerator
    {
      private System.Collections.IEnumerator wrapped;

      public Enumerator(VersionCollection collection)
      {
        this.wrapped = ((System.Collections.CollectionBase)collection).GetEnumerator();
      }

      public Version Current
      {
        get
        {
          return (Version) (this.wrapped.Current);
        }
      }

      object System.Collections.IEnumerator.Current
      {
        get
        {
          return (Version) (this.wrapped.Current);
        }
      }

      public bool MoveNext()
      {
        return this.wrapped.MoveNext();
      }

      public void Reset()
      {
        this.wrapped.Reset();
      }
    }

    /// <summary>
    /// Returns an enumerator that can iteVersion through the elements of this VersionCollection.
    /// </summary>
    /// <returns>
    /// An object that implements System.Collections.IEnumerator.
    /// </returns>        
    public new virtual VersionCollection.Enumerator GetEnumerator()
    {
      return new VersionCollection.Enumerator(this);
    }
    #endregion

    #region CollectionBase Notification Overrides

    protected override void OnClearComplete()
    {    
      if (ListChanged != null)
      {
        ListChangedEventArgs args = new ListChangedEventArgs (ListChangedType.Reset, -1, -1);
        ListChanged (this, args);
      }      
    }


    protected override void OnInsertComplete(int index_, object value_ )
    {
      if (ListChanged != null)
      {
        ListChangedEventArgs args = new ListChangedEventArgs (ListChangedType.ItemAdded, index_, -1);
        ListChanged (this, args);
      }     
    }

    protected override void OnRemoveComplete( int index_, object value_ )
    {       
      if (ListChanged != null)
      {
        ListChangedEventArgs args = new ListChangedEventArgs (ListChangedType.ItemDeleted, index_, -1);
        ListChanged (this, args);
      }           
    }


    protected override void OnSetComplete( int index_, object oldValue_, object newValue_ )
    {
      if ( ListChanged != null )
      {     
        ListChangedEventArgs args = new ListChangedEventArgs( ListChangedType.ItemChanged, index_, -1 );
        ListChanged( this, args );           
      }
    }

    #endregion

    #region ITypedList Members
    public PropertyDescriptorCollection GetItemProperties( PropertyDescriptor[] pd_ )
    {     
      return TypeDescriptor.GetProperties( typeof(Version) );
    }

    public string GetListName( PropertyDescriptor[] pd_ )
    {
      return "VersionCollection";
    }
    #endregion

    #region IListSource Members
    public IList GetList()
    {
      return this;
    }

    public bool ContainsListCollection
    {
      get
      {
        return false;
      }
    }
    #endregion
    
    #region IBindList Members

    public event System.ComponentModel.ListChangedEventHandler ListChanged;

    public object AddNew()
    {
      throw new NotSupportedException();
    }
   
    public bool AllowEdit
    {
      get
      {
        return true;
      }
    }

    public bool AllowNew
    {
      get
      {
        return false;
      }
    }

    public bool AllowRemove
    {
      get
      {
        return true;
      }
    }

    public bool SupportsChangeNotification
    {
      get
      {
        return true;
      }
    }

    public bool SupportsSearching
    {
      get
      {
        return false;
      }
    }

    public bool SupportsSorting
    {
      get
      {
        return false;
      }
    }

    public bool IsSorted
    {
      get
      {
        return false;
      }
    }

    public void ApplySort(PropertyDescriptor property, System.ComponentModel.ListSortDirection direction)
    {      
      throw new NotSupportedException();
    }
    
    public PropertyDescriptor SortProperty
    {
      get
      {
        return null;
      }
    }
    
    public int Find(PropertyDescriptor property, object key)
    {      
      throw new NotSupportedException();   
    }

    public void AddIndex(PropertyDescriptor property)
    {      
      throw new NotSupportedException();
    }
    
    public ListSortDirection SortDirection
    {
      get
      {        
        return new ListSortDirection ();
      }
    }

    public void RemoveSort()
    { 
      throw new NotSupportedException();
    }

    public void RemoveIndex(PropertyDescriptor property)
    {      
      throw new NotSupportedException();
    }
    #endregion
  }
}
