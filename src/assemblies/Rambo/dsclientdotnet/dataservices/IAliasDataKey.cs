using System;

namespace MorganStanley.MSDesktop.Rambo.DataServices
{
	/// <summary>
	/// Summary description for IAliasDataKey.
	/// </summary>
  public interface IAliasDataKey : IDataKey
  {
    string Alias {	get; 	}
  }
}
