//---------------------------------------------------------------------------
// ($Id $)
//---------------------------------------------------------------------------
//
// Copyright (c) 2003 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
//---------------------------------------------------------------------------

using System;
using System.Xml;
using System.Globalization;

using MorganStanley.MSDesktop.Rambo.DataServices.Util;

namespace MorganStanley.MSDesktop.Rambo.DataServices
{
	/// <summary>
	/// Summary description for XMLMetaDataObject.
	/// </summary>
	public class XMLMetaDataObject : IMetaDataObject
	{
    /// <summary>
    /// the xml content
    /// </summary>
    protected XmlElement m_value;
    
    public XMLMetaDataObject(XmlElement value_ )       
    {
      Utils.ArgumentNullException( "value", value_ );

      m_value = value_;
    }

    object IMetaDataObject.Value
    {
      get { return m_value; }
    }

    /// <summary>
    /// the Xml content
    /// </summary>
    new virtual public XmlElement Value
    {
      get { return m_value; }
    }

    /// <summary>
    /// A deep copy
    /// </summary>
    /// <returns></returns>
    public object Clone()
    {
      XMLMetaDataObject clone = new XMLMetaDataObject((XmlElement)Value.Clone());
      return clone;
    }

    /// <summary>
    /// returns the XML string
    /// </summary>
    /// <returns></returns>
    public override String ToString()
    {
      return Utils.NodeToString(m_value);
    }
  }
}




    
