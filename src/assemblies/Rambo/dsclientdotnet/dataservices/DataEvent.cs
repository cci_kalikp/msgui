//---------------------------------------------------------------------------
// ($Id $)
//---------------------------------------------------------------------------
//
// Copyright (c) 2003 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
//---------------------------------------------------------------------------

using System;
using System.Xml;
using System.Xml.Linq;
using JetBrains.Annotations;
using MorganStanley.MSDesktop.Rambo.DataServices.Util;

namespace MorganStanley.MSDesktop.Rambo.DataServices
{ 
  /// <summary>
  /// Encapsulates an event raised by a DataService
  /// </summary>
  public sealed class DataEvent : EventArgs, ICloneable 
  {
    private readonly IDataObject m_object;
    private readonly DateTime m_timestamp;
    private readonly string m_userId;

    public DataEvent( IDataObject object_ ) : this( object_, DSEnvironment.USER_NAME )
    {      
    }

    public DataEvent( IDataObject object_, string userId_ ) : this( object_, DateTime.Now, userId_ )
    {      
    }

    public DataEvent( IDataObject object_, DateTime timestamp_, string userId_ )
    {      
      Utils.ArgumentNullException( "object", object_ );

      m_object = object_;
      m_timestamp = timestamp_;
      m_userId = userId_;
    }

    #region Obsolete constructors
    [Obsolete("DataEventType is redundant",false)]  		
    public DataEvent( IDataObject object_, DataEventType eventType_) : this( object_,eventType_, DateTime.Now )
    {      
    }

    [Obsolete("DataEventType is redundant",false)]  		
    public DataEvent( IDataObject object_, DataEventType eventType_, DateTime timestamp_ ) : this( object_,eventType_, timestamp_, DSEnvironment.USER_NAME )
    {      
    }

    [Obsolete("DataEventType is redundant",false)]  		
    public DataEvent ( IDataObject object_, DataEventType eventType_, DateTime timestamp_, string userId_ ) : this( object_, timestamp_, userId_ )
    {
    }
    #endregion

    [NotNull]
    public IDataObject DataObject 
    { 
      get { return m_object; }
    }

    [NotNull]
    public XElement DataXml
    {
      get
      {
        var xml = (XmlElement) m_object.Value;
        return xml.ToXElement();
      }
    }

    public IDataKey DataKey
    {
      get { return m_object.DataKey; }
    }
		
    [Obsolete("DataEventType is redundant",false)]  		
    public DataEventType Type 
    { 				
      get { return DataEventType.UPDATE; }		
    }

    public String UserId
    {
      get { return m_userId; }
    }

    /// <summary>
    /// Returns the timestamp the event occured at
    /// </summary>
    public DateTime Timestamp
    { 
      get
      {
        return m_object.Timestamp;
      }
    }

    public object Clone()
    {
      DataEvent clone = new DataEvent( (IDataObject)m_object.Clone(), m_timestamp, m_userId );
      return clone;
    }
  }
}
