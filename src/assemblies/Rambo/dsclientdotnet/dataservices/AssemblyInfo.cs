//---------------------------------------------------------------------------
// ($Id $)
//---------------------------------------------------------------------------
//
// Copyright (c) 2003 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
//---------------------------------------------------------------------------

using System;
using System.Reflection;
using System.Runtime.CompilerServices;

//Cannot make the following declaration because one of our references, 
// AppMW.CPSDotNet, is NOT CLS complaint
// [ assembly: CLSCompliant(true) ]

[ assembly: AssemblyProduct("Ecdev.DataServices") ]
[ assembly: AssemblyVersion("2011.08.1") ]
[ assembly: AssemblyCulture("") ]
[ assembly: AssemblyTitle("Ecdev Dataservices API") ]
[ assembly: AssemblyDescription("Ecdev Dataservices API") ]
[ assembly: AssemblyCompany("Morgan Stanley") ]
[ assembly: AssemblyCopyright("Copyright (c) 2005 Morgan Stanley, Inc., All Rights Reserved") ]
[ assembly: AssemblyTrademark("") ]

// In order to sign your assembly you must specify a key to use. Refer to the 
// Microsoft .NET Framework documentation for more information on assembly signing.
//
// Use the attributes below to control which key is used for signing. 
//
// Notes: 
//   (*) If no key is specified, the assembly is not signed.
//   (*) KeyName refers to a key that has been installed in the Crypto Service
//       Provider (CSP) on your machine. KeyFile refers to a file which contains
//       a key.
//   (*) If the KeyFile and the KeyName values are both specified, the 
//       following processing occurs:
//       (1) If the KeyName can be found in the CSP, that key is used.
//       (2) If the KeyName does not exist and the KeyFile does exist, the key 
//           in the KeyFile is installed into the CSP and used.
//   (*) In order to create a KeyFile, you can use the sn.exe (Strong Name) utility.
//       When specifying the KeyFile, the location of the KeyFile should be
//       relative to the project output directory which is
//       %Project Directory%\obj\<configuration>. For example, if your KeyFile is
//       located in the project directory, you would specify the AssemblyKeyFile 
//       attribute as [assembly: AssemblyKeyFile("..\\..\\mykey.snk")]
//   (*) Delay Signing is an advanced option - see the Microsoft .NET Framework
//       documentation for more information on this.
//