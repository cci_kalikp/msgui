//---------------------------------------------------------------------------
// ($Id $)
//---------------------------------------------------------------------------
//
// Copyright (c) 2003 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
//---------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MorganStanley.MSDesktop.Rambo.AsyncUtils;
using MorganStanley.MSDesktop.Rambo.DataServices.SoapCps;

namespace MorganStanley.MSDesktop.Rambo.DataServices
{
  /// <summary>
  /// Provides default implementations for some IDataService's methods.
  /// </summary>
  public abstract class AbstractDataService : IDataService
  {
    protected string m_id;

    protected bool m_subFilterSet = false;
    
    protected bool m_subFiltering;
		
    #region Delegate definitions

    protected delegate IDictionary GetSnapshotDelegate( ISubscription subscription_ );

    protected delegate void SinglePublishDelegate( IDataObject obj_ );

    protected delegate IDataKey[] MultiplePublishDelegate( IList objects_ );

    protected delegate IDataKey[] GetDataKeysDelegate( );

    protected delegate IDataKey[] GetDataKeysDateRangeDelegate( DataServiceDate startDataTime_, DataServiceDate endDataTime_ );

    protected delegate IDataKey[] GetDataKeysKeyAttributesDelegate( IDictionary attributes_ );

    protected delegate IDataKey[] GetDataKeysKeyAttributesDateRangeDelegate( IDictionary attributes_, DataServiceDate startDataTime_, DataServiceDate endDataTime_ );

    protected delegate IDataKey[] GetDataKeysKeyPatternDelegate( IDataKey key_ );

    protected delegate IDataKey[] GetDataKeysKeyPatternDateRangeDelegate( IDataKey key_, DataServiceDate startDataTime_, DataServiceDate endDataTime_ );

    protected delegate VersionDataKeyCollection GetVersionsListDelegate( IDataKey key_, bool sort_ );

    protected delegate VersionDataKeyCollection GetVersionsListDateRangeDelegate( IDataKey key_, DataServiceDate startDataTime_, DataServiceDate endDataTime_ );

    protected delegate DataEvent GetVersionDelegate( IVersionDataKey key_ );

    protected delegate IMetaDataObject GetMetaDataDelegate( );

    #endregion

    protected AbstractDataService( string id_)
    {
      m_id = id_ ;
    }

    public string Id
    {
      get { return m_id; }
    }

		public abstract ServerList Servers 
		{
			get;
		}

    public bool ServerSubFiltering
    {
      get {
        if (m_subFilterSet == false) {
          if (DSEnvironment.FilterLocation == DSEnvironment.DSFilterLocation.Server) {
            return true;
          } else {
            return false;
          }
        } else {
          return m_subFiltering;
        }
      }
        
      set {
        if (!m_subFilterSet) {
          m_subFilterSet = true;
          m_subFiltering = value ;
        }
      }
    }

    public abstract void refreshServerList();

    #region CreateSubscription operations

    public virtual ISubscription CreateSubscription( IDataKey key_ )
    {
      return CreateSubscription( key_, DSEnvironment.USER_NAME );
    }


    public virtual ISubscription CreateSubscription( IDataKey key_, string userId_ )
    {
      ArrayList al = new ArrayList(1);
      al.Add( key_ );
      return CreateSubscription( al, userId_ );
    }
		

    public virtual ISubscription CreateSubscription( IList keys_ )
    {
      return CreateSubscription( keys_, DSEnvironment.USER_NAME );
    }

    public ISubscription CreateBatchSubscription(IEnumerable<IDataKey> keys_)
    {
      return CreateSubscription(keys_.ToList(), DSEnvironment.USER_NAME);
    }

    public abstract ISubscription CreateSubscription( IList keys_, string userId_ );


    public virtual ISubscription CreateSubscription( IDictionary dict_ )
    {
      return CreateSubscription( dict_, DSEnvironment.USER_NAME );
    }

    public virtual ISubscription CreateSubscription(IDictionary dict_, bool strictAttributeNames_)
    {
        return CreateSubscription(dict_, DSEnvironment.USER_NAME, strictAttributeNames_);
    }

    public virtual ISubscription CreateSubscription(IDictionary dict_, string userId_, bool strictAttributeNames_)
    {
        throw new NotImplementedException();
    }

    public abstract ISubscription CreateSubscription(IDictionary dict_, string userId_);


    public virtual ISubscription GlobalSubscription()
    {
      return GlobalSubscription( DSEnvironment.USER_NAME );
    }

    public abstract ISubscription GlobalSubscription( string userId_ );

    #endregion
		
    public virtual IDataKey CreateDataKey( Object key_ )
    {
      return DataKeyFactory.BuildDataKey(key_);
    }
  
    #region Publish operations

    public abstract void Publish( IDataObject obj_ ); 

    /// <summary>
    /// Basic implementation which iterates through the list provided publishing each IDataObject individually.
    /// Subclasses can override this method to provide a more efficient implementation.
    /// </summary>
    /// <param name="dataObjects_"></param>
    /// <returns>IDataKey[] containing keys of any failed publishes or empty array if all successful</returns> 
    public virtual IDataKey[] Publish( IList dataObjects_ )
    {
      return PublishPrivate(dataObjects_);
    }

    public IEnumerable<IDataKey> PublishBatch(IEnumerable<IDataObject> dataObjects_)
    {
      return PublishPrivate(dataObjects_);
    }

    private IDataKey[] PublishPrivate(IEnumerable dataObjects_)
    {
      SoapCps.CPSSubscriberCache.ForceHeartbeat();
      IList failedPublishKeys = new ArrayList();
      if (dataObjects_ != null)
      {
        foreach (IDataObject obj in dataObjects_)
        {
          try
          {
            Publish(obj);
          }
          catch
          {
            failedPublishKeys.Add(obj.DataKey);
          }
        }
      }

      IDataKey[] failedPublishKeysArr = new IDataKey[failedPublishKeys.Count];
      if (failedPublishKeysArr.Length > 0)
        failedPublishKeys.CopyTo(failedPublishKeysArr, 0);

      return failedPublishKeysArr;
    }

    public virtual IAsyncResult BeginPublish( IDataObject obj_, AsyncCallback callback_, Object state_ )
    {
      Asynchronizer ssi = new Asynchronizer( callback_, state_, DSEnvironment.DS_THREAD_POOL); 
      return ssi.BeginInvoke( new SinglePublishDelegate(this.Publish), obj_ );
    }

    public virtual IAsyncResult BeginPublish( IList dataObjects_, AsyncCallback callback_, Object state_ )		
    {
      Asynchronizer ssi = new Asynchronizer( callback_, state_, DSEnvironment.DS_THREAD_POOL); 
      return ssi.BeginInvoke( new MultiplePublishDelegate(this.Publish), new object [] {dataObjects_} );
    }
   
    public virtual IDataKey[] EndPublish( IAsyncResult result_ )
    {
      AsynchronizerResult asr = (AsynchronizerResult)result_;
      return (IDataKey[])asr.SynchronizeInvoke.EndInvoke(result_);
    }

    #endregion

    #region Snapshot operations

    public virtual DataEvent GetSnapshot( IDataKey key_ )
    {
      ISubscription sub = CreateSubscription( key_ );
      IDictionary dict = GetSnapshot( sub );
      if ( dict != null )
      {
        DataEvent returnVal = dict[ key_ ] as DataEvent ;

        return ( returnVal ) ;
      }

      return null;
    }


    public abstract IDictionary GetSnapshot( ISubscription subscription_ );
    public abstract IDictionary<IDataKey, DataEvent> GetSnapshots(ISubscription subscription_);

    public virtual IAsyncResult BeginSnapshot( ISubscription subscription_, AsyncCallback callback_, Object state_)
    {
      Asynchronizer ssi = new Asynchronizer( callback_, state_, DSEnvironment.DS_THREAD_POOL);	
      return ssi.BeginInvoke( new GetSnapshotDelegate(this.GetSnapshot), new object[]{ subscription_});
    }


    public virtual IDictionary EndSnapshot( IAsyncResult result_ )
    {
      AsynchronizerResult asr = (AsynchronizerResult)result_;      
      IDictionary dict = (IDictionary)asr.SynchronizeInvoke.EndInvoke(result_);
      return dict;
    }

    #endregion


    public virtual DataEvent GetSnapshot( IDataKey key_, DateTime version_ )
    {
      throw new NotSupportedException();
    }

    public virtual DateTime[] GetVersions ( IDataKey key_ )
    {
      throw new NotSupportedException();
    }

    #region GetDataKeys operations

    public virtual IDataKey[] GetDataKeys()
    {
      throw new NotSupportedException();
    }

    public virtual IDataKey[] GetDataKeys( DataServiceDate startDataTime_, DataServiceDate endDataTime_ )
    {
      throw new NotSupportedException();
    }

    public virtual IDataKey[] GetDataKeys( IDictionary attributes_ )
    {
      throw new NotSupportedException();
    }

    public virtual IDataKey[] GetDataKeys( IDictionary attributes_, DataServiceDate startDataTime_, DataServiceDate endDataTime_ )
    {
      throw new NotSupportedException();
    }

    public virtual IDataKey[] GetDataKeys( IDataKey key_ )
    {
      throw new NotSupportedException();
    }

    public virtual IDataKey[] GetDataKeys( IDataKey key_, DataServiceDate startDataTime_, DataServiceDate endDataTime_ )
    {
      throw new NotSupportedException();
    }

    public virtual IAsyncResult BeginGetDataKeys( AsyncCallback callback_, Object state_)
    {
      Asynchronizer ssi = new Asynchronizer( callback_, state_, DSEnvironment.DS_THREAD_POOL); 
      return ssi.BeginInvoke( new GetDataKeysDelegate(this.GetDataKeys), null );
    }

    public virtual IAsyncResult BeginGetDataKeys( DataServiceDate startDataTime_, DataServiceDate endDataTime_, AsyncCallback callback_, Object state_ )
    {
      Asynchronizer ssi = new Asynchronizer( callback_, state_, DSEnvironment.DS_THREAD_POOL); 
      return ssi.BeginInvoke( new GetDataKeysDateRangeDelegate(this.GetDataKeys), new object [] {startDataTime_, endDataTime_} );
    }

    public virtual IAsyncResult BeginGetDataKeys( IDictionary attributes_, AsyncCallback callback_, Object state_ )
    {
      Asynchronizer ssi = new Asynchronizer( callback_, state_, DSEnvironment.DS_THREAD_POOL); 
      return ssi.BeginInvoke( new GetDataKeysKeyAttributesDelegate(this.GetDataKeys), new object [] {attributes_} );
    }

    public virtual IAsyncResult BeginGetDataKeys( IDictionary attributes_, DataServiceDate startDataTime_, DataServiceDate endDataTime_, AsyncCallback callback_, Object state_ )
    {
      Asynchronizer ssi = new Asynchronizer( callback_, state_, DSEnvironment.DS_THREAD_POOL); 
      return ssi.BeginInvoke( new GetDataKeysKeyAttributesDateRangeDelegate(this.GetDataKeys), new object [] {attributes_, startDataTime_, endDataTime_} );
    }

    public virtual IAsyncResult BeginGetDataKeys(IDataKey key_, AsyncCallback callback_, Object state_ )
    {
      Asynchronizer ssi = new Asynchronizer( callback_, state_, DSEnvironment.DS_THREAD_POOL); 
      return ssi.BeginInvoke( new GetDataKeysKeyPatternDelegate(this.GetDataKeys), new object [] {key_} );
    }

    public virtual IAsyncResult BeginGetDataKeys(IDataKey key_, DataServiceDate startDataTime_, DataServiceDate endDataTime_, AsyncCallback callback_, Object state_ )
    {
      Asynchronizer ssi = new Asynchronizer( callback_, state_, DSEnvironment.DS_THREAD_POOL); 
      return ssi.BeginInvoke( new GetDataKeysKeyPatternDateRangeDelegate(this.GetDataKeys), new object [] {key_, startDataTime_, endDataTime_} );
    }    

    public virtual IDataKey[] EndGetDataKeys( IAsyncResult result_ )
    {
      AsynchronizerResult asr = (AsynchronizerResult)result_;
      return (IDataKey[])asr.SynchronizeInvoke.EndInvoke(result_);
    }

    #endregion

    #region Version/Alias API

    public virtual VersionDataKeyCollection GetVersionsList ( IDataKey key_, bool sort_ )
    {
      throw new NotSupportedException();
    }

		public virtual VersionDataKeyCollection GetVersionsList(IDataKey key_, DataServiceDate startDataTime_, DataServiceDate endDataTime_)
		{
			throw new NotSupportedException();
		}

    public virtual IAsyncResult BeginGetVersionsList( IDataKey key_, bool sort_, AsyncCallback callback_, Object state_ )
    {
      Asynchronizer ssi = new Asynchronizer( callback_, state_, DSEnvironment.DS_THREAD_POOL); 
      return ssi.BeginInvoke( new GetVersionsListDelegate(this.GetVersionsList), new object [] {key_, sort_} );
    }      

    public virtual IAsyncResult BeginGetVersionsList( IDataKey key_, DataServiceDate startDataTime_, DataServiceDate endDataTime_, AsyncCallback callback_, Object state_ )
    {
      Asynchronizer ssi = new Asynchronizer( callback_, state_, DSEnvironment.DS_THREAD_POOL);
      return ssi.BeginInvoke( new GetVersionsListDateRangeDelegate(this.GetVersionsList), new object [] {key_, startDataTime_, endDataTime_} );
    }  

    public virtual VersionDataKeyCollection EndGetVersionsList( IAsyncResult result_ )
    {
      AsynchronizerResult asr = (AsynchronizerResult)result_;
      return (VersionDataKeyCollection)asr.SynchronizeInvoke.EndInvoke(result_);
    }

		public virtual DataEvent GetVersion ( IVersionDataKey key_ )
    {
      throw new NotSupportedException();
    }

    public virtual IAsyncResult BeginGetVersion( IVersionDataKey key_, AsyncCallback callback_, Object state_ )
    {
      Asynchronizer ssi = new Asynchronizer( callback_, state_, DSEnvironment.DS_THREAD_POOL); 
      return ssi.BeginInvoke( new GetVersionDelegate(this.GetVersion), new object [] {key_} );
    } 

    public virtual DataEvent EndGetVersion( IAsyncResult result_ )
    {
      AsynchronizerResult asr = (AsynchronizerResult)result_;
      return (DataEvent)asr.SynchronizeInvoke.EndInvoke(result_);
    }

    public virtual IVersionDataKey GetVersionDataKey( IAliasDataKey aliasDataKey_ )
    {
      throw new NotSupportedException();
    }

    public virtual void TurnoverAlias(IAliasDataKey aliasDataKey_, IVersionDataKey newVersionKey_)
    {
      throw new NotSupportedException();
    }

    #endregion

	  public virtual IMetaDataObject GetMetaData()
	  {
      DSMetaDataRequest metaDataRequest = new DSMetaDataRequest(this);
      DSMetaDataResponse metaDataResponse = metaDataRequest.SendRequest();
      return metaDataResponse.MetaDataObject;
    }

    public virtual IAsyncResult BeginMetaData( AsyncCallback callback_, Object state_)
    {
      Asynchronizer ssi = new Asynchronizer( callback_, state_, DSEnvironment.DS_THREAD_POOL);	
      return ssi.BeginInvoke( new GetMetaDataDelegate(this.GetMetaData), null);
    }

    public virtual IMetaDataObject EndMetaData( IAsyncResult result_ )
    {
      AsynchronizerResult asr = (AsynchronizerResult)result_;      
      IMetaDataObject metaData = (IMetaDataObject)asr.SynchronizeInvoke.EndInvoke(result_);
      return metaData;
    }

  }
}
