//---------------------------------------------------------------------------
// ($Id $)
//---------------------------------------------------------------------------
//
// Copyright (c) 2003 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
//---------------------------------------------------------------------------

using System;

namespace MorganStanley.MSDesktop.Rambo.DataServices
{
	/// <summary>
	/// An interface for data-objects.
	/// Every implementation must override ToString(), GetHashCode(), Equals() of the base Object
	/// </summary>
	/// <author><a href="mailto:Kunle.Adetona@morganstanley.com">Kunle Adetona</a></author>
	public interface IDataObject : ICloneable 
	{
    /// <summary>
    /// Accessor to this object's unique identifier
    /// </summary>
		IDataKey DataKey { get; } 
 
    object Value { get; }

    /// <summary>
    /// Last update time. The time when oject last updated
    /// </summary>
    DateTime Timestamp{ get; }

    /// <summary>
    /// Current update time. The time when object currently updated(Usually it is DateTime.Now)
    /// </summary>
    DataServiceDate UpdateTime{ get; }

    /// <summary>
    /// Creation time. The Time when object is created.
    /// </summary>
    DataServiceDate DataTime{ get; }
	}
}
