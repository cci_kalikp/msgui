//---------------------------------------------------------------------------
// ($Id $)
//---------------------------------------------------------------------------
//
// Copyright (c) 2003 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
//---------------------------------------------------------------------------

using System;
using System.Collections;
using MorganStanley.Ecdev.DataServices.Util;

namespace MorganStanley.Ecdev.DataServices
{
	/// <summary>
	/// Caches updates from another dataservice including the snapshot of all data.
	/// Satisfies snapshot requests from an internal cache.
	/// Enforces that events are fired in order (using timestamps on events).
	/// Delegates some actions to the encapsulate dataservice e.g. publish, 
	/// subscription creation, datakey creation.
	/// 
	/// TODO: Work in progress... Currently unusable.
	/// </summary>
	/// <author><a href="mailto:Kunle.Adetona@morganstanley.com">Kunle Adetona</a></author>
  public class DataServiceCache : AbstractDataService
  {
    protected delegate void GlobalUpdateDelegate( DataServiceCache source_, DataEvent event_ );
    protected event GlobalUpdateDelegate OnGlobalUpdate;

    protected IDataService m_service;
    protected IDictionary m_data;  //caches datakey --> dataobject
    protected Subscription m_global;
    
    //todo: should it has its own id?
    /// <summary>
    /// caches a global snapshot of the given dataservice
    /// </summary>
    /// <param name="service_"></param>
    public DataServiceCache( IDataService service_ ):base(service_.Id)
    {
      Utils.ArgumentNullException( "dataservice", service_ );
      
      m_service = service_;
      m_data = new Hashtable();
      m_global = m_service.GlobalSubscription;
      m_global.OnDataUpdate += new DataEventHandler( this.GlobalUpdate );
      //will block here until end of global snapshot
      m_global.EndSnapshot( m_global.BeginSnapshot(null,null) );
    }

    protected void GlobalUpdate( object source_, DataEvent event_ )
    {
      lock( m_data )
      {
        if ( m_data.Contains(event_.DataKey) )
        {
          DataEvent current = (DataEvent)m_data[event_.DataKey];           
          if ( event_.Timestamp > current.Timestamp )
          { //new event, update cache and fire it
            m_data.Add( event_.DataKey, event_ ); 
            fireGlobalUpdate( event_ );
          }
        }
        else
        {
          //new event, update cache and fire it
          m_data.Add( event_.DataKey, event_ ); 
          fireGlobalUpdate( event_ );
        }
      }      
    }

    protected void fireGlobalUpdate( DataEvent event_ )
    {
      if ( OnGlobalUpdate != null )
      {
        OnGlobalUpdate( this, event_ );
      }
    }

    /// <summary>
    /// Gets snapshot from its internal cache.
    /// </summary>
    /// <param name="subscription_"></param>
    /// <returns></returns>
    public override IDictionary GetSnapshot( Subscription subscription_ ) //DataServiceException
    {      
      if ( !(subscription_ is MySubscription) )
      {
        throw new DataServiceException("Wrong subscription type");
      }

      if ( !Object.ReferenceEquals( this, subscription_.DataService ) )
      {
        throw new DataServiceException("I did not create this subscription.");
      }			

      lock( m_data )
      {
//        if ( subscription_.Type == Subscription.SubscriptionType.GLOBAL  )
//        {
//          return new Hashtable( m_data );
//        }
        
        IDictionary snapshot = new Hashtable();
//TODO: rethink:
//        foreach ( DataEvent e in m_data.Values )
//        {
//          if ( ((MySubscription)subscription_).Matches(e.DataObject) )
//          {
//            snapshot.Add( e.DataKey, e );
//          }
//        }
        return snapshot;
      }
    }

    public override Subscription CreateSubscription( IDictionary dict_ )
    { 
      Subscription sub = m_service.CreateSubscription( dict_ );
      return new MySubscription( this, sub );      
    }

    public override Subscription GlobalSubscription
    {
      get
      {
        return new MySubscription( this, m_global );
      }
    }

    public override Subscription CreateSubscription( IList keys_ )
    {
      Subscription sub = m_service.CreateSubscription( keys_ );
      return new MySubscription( this, sub );      
    }

    /// <summary>
    /// delegates publish action to enclosed dataservice
    /// </summary>
    /// <param name="obj_"></param>
    public override void Publish( IDataObject obj_ ) //throws PublishException
    {
      m_service.Publish( obj_ );
    }

    /// <summary>
    /// delegates key creation to enclosed dataservice
    /// </summary>
    /// <param name="key_"></param>
    /// <returns></returns>
    public override IDataKey CreateDataKey( Object key_ )
    {
      return m_service.CreateDataKey( key_ );
    }

    //-------------------------------------------------------------------------------------------------

    protected class MySubscription : Subscription 
    {
      Subscription m_delegate;

      public MySubscription ( DataServiceCache dataService_, Subscription delegate_ )
         :base( dataService_ )
      {       
        m_delegate = delegate_;
        dataService_.OnGlobalUpdate += new GlobalUpdateDelegate( this.DataServiceUpdate );
      }

      protected void DataServiceUpdate( DataServiceCache source_, DataEvent event_ )
      {
        if ( IsActive && Includes(event_.DataObject) )
        {
          FireOnSubscriptionUpdate( event_ );
        }
      }      

      public override bool IsActive
      {
        get
        {
          return m_delegate.IsActive;
        }
        set
        {
          m_delegate.IsActive = value;
        }
      }

      protected bool Includes( IDataObject obj_ )
      {
        //todo: reth
        return false; //return m_delegate.Includes( obj_ );
      }
    }
	}
}
