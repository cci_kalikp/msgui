using System;

namespace MorganStanley.MSDesktop.Rambo.DataServices
{
	/// <summary>
	/// Summary description for DSErrorCode.
	/// </summary>
	public enum DSStatusType
	{
    CPSError,
    CPSReconnect,
    AsyncConnectionFailed
	}
}
