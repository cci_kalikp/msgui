using System;

namespace MorganStanley.MSDesktop.Rambo.DataServices
{
	/// <summary>
	/// VersionSubscription for subscriptions on IVersionDataKeys.  Updates are not supported for VersionSubscriptions.
	/// </summary>
	public class VersionSubscription : AbstractSubscription
	{
    internal VersionSubscription(IDataService dataService_, IVersionDataKey versionKey_, string userId_) : this(dataService_, new VersionDataKeyCollection(new IVersionDataKey[] { versionKey_ }), userId_)
    {
    }

    internal VersionSubscription(IDataService dataService_, VersionDataKeyCollection versionKeys_, string userId_) : base(dataService_, versionKeys_, userId_)
		{
		}

    public new virtual VersionDataKeyCollection DataKeys
    {
      get
      {
        return (VersionDataKeyCollection)base.DataKeys;
      }
    }

    public override event DataEventHandler OnDataUpdate
    {
      add
      {
        // no op
      }
      remove
      {
        // no op
      }
    }

    protected override void FireDataUpdate( DataEvent evnt_ )
    {
      // no op
    }
	}
}
