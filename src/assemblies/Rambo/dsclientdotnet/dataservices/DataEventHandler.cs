//---------------------------------------------------------------------------
// ($Id $)
//---------------------------------------------------------------------------
//
// Copyright (c) 2003 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
//---------------------------------------------------------------------------

using System;

namespace MorganStanley.MSDesktop.Rambo.DataServices
{
	/// <summary>
	/// Summary description for DataEventHandler.
	/// </summary>
	/// <author><a href="mailto:Kunle.Adetona@morganstanley.com">Kunle Adetona</a></author>
	public delegate void DataEventHandler ( object source_, DataEvent event_ ) ;  
}
