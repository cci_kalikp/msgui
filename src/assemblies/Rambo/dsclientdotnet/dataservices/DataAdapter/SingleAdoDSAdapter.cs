using System;
using System.Collections;
using System.IO;
using System.Xml;
using System.Data;
using MorganStanley.MSDotNet.MSLog;
using MorganStanley.MSDesktop.Rambo.DataServices.Util;

namespace MorganStanley.MSDesktop.Rambo.DataServices
{
	/// <summary>
	/// links a DataSet to a single DataService
	/// </summary>
	public class SingleAdoDSAdapter
	{

    protected XmlDataDocument m_xmlDoc;
    protected DataSet m_dataSet;
    protected IXMLDataService m_dataService;
    protected Logger m_log = new Logger( typeof(AdoDataAdapter) );
    
    protected string m_userId;
    protected string _keyElementName;
   
    public SingleAdoDSAdapter(string dsName):this((IXMLDataService)DataServiceManager.GetDataService(dsName),null)
    {
      
    }

    public SingleAdoDSAdapter(IXMLDataService dataService):this(dataService,null)
    {
      
    }


    public SingleAdoDSAdapter(IXMLDataService dataService, DataSet dataSet)
    {
      m_dataService = dataService;
      String schemaString = dataService.Schema;
      XmlDocument schemaDoc = new XmlDocument();
      schemaDoc.LoadXml( schemaString );
      _keyElementName = GetKeyField( schemaDoc );//keyElementName_;
      if (dataSet==null)
      {
        m_dataSet = new DataSet();
        m_dataSet.ReadXmlSchema( new XmlNodeReader(schemaDoc.DocumentElement) );
        m_dataSet.DataSetName = dataService.Id+"Set";
      }
      else
      {
        m_dataSet = dataSet;
      }
      m_xmlDoc = new XmlDataDocument(m_dataSet);
      if (m_xmlDoc.DocumentElement==null)
      {
        String dataSetName = m_dataSet.DataSetName;
        String xmlStr = "<" + dataSetName + "> </"+dataSetName+">";
        m_xmlDoc.LoadXml(xmlStr);
      }
    }

  

    public XmlDataDocument XmlDataDocument
    {
      get
      {
        return m_xmlDoc;
      }
    }

    public DataSet DataSet
    {
      get
      {
        return m_dataSet;
      }
    }

    public IXMLDataService DataService
    {
      get
      {
        return m_dataService;
      }
    }

    // clears the underlying dataset
    public void Clear()
    {
      this.DataSet.EnforceConstraints = false;
      this.XmlDataDocument.RemoveAll();
      this.DataSet.EnforceConstraints = true;
    }
    
    /// <summary>
    /// UserId of updater.
    /// </summary>
    public string UserId
    {
      get
      {
        if ( m_userId != null )
        {
          return m_userId;
        }
        return DSEnvironment.USER_NAME;
      }
      set
      {
        m_userId = value;
      }
    }
 
    

    public void Attach( ISubscription sub_ )
    {
      if (this.XmlDataDocument!=null && this.XmlDataDocument.DocumentElement.HasChildNodes)
      {
        this.DataSet.EnforceConstraints = false;
        this.XmlDataDocument.DocumentElement.RemoveAll();
      }
      Attach( sub_, true, true, true );
    }

   

    public void Attach( ISubscription sub_, bool snapshot_, bool updates_, bool _newValues )
    {  
      if (sub_ ==null)
      {
        return;
      }
      if ( snapshot_ )
      {
        IDictionary dict = sub_.Snapshot();
        if (dict!=null)
        {
          if (_newValues)
          {
            m_dataSet.EnforceConstraints=false;
            foreach ( DataEvent de in dict.Values )
            {
              XMLDataObject xdo = (XMLDataObject)de.DataObject;
              XmlElement el = xdo.Value;
              XmlNode clone = m_xmlDoc.ImportNode(el,true);
              m_xmlDoc.DocumentElement.AppendChild(clone);
            }
            m_dataSet.EnforceConstraints=true;
            m_dataSet.AcceptChanges();
          }
          else
          {
            foreach ( DataEvent de in dict.Values )
            {
              DataUpdate( sub_, de );
            }
          }
        }
      }                   
      if ( updates_ )
      {
          sub_.OnDataUpdate += new DataEventHandler( this.DataUpdate ); 
      }
    }

    private  string ToString(XmlElement el)
    {
      if (el == null) return null;
      StringWriter strWriter = new StringWriter();
      XmlTextWriter writer = new XmlTextWriter(strWriter);
      writer.Formatting = Formatting.Indented;
      el.WriteTo(writer);
      return strWriter.ToString();
    }

    protected void DoneSnapShot( IAsyncResult ar_ )
    {
      ISubscription s = (ISubscription)ar_.AsyncState;
      try
      {
        s.EndSnapshot( ar_ );
      }
      catch ( DataServiceException ex )
      {
        m_log.Log( MSLog.Error(), "DoneSnapShot", ex.Message, ex );     
      }
    }

    /// <summary>
    /// throws publish exception
    /// </summary>
    public void Publish()
    {
      if ( m_dataSet.HasChanges() )
      {
        foreach( DataTable t in m_dataSet.Tables )
        {         
          Publish( t.TableName );
        }        
      }
    }
    /// <summary>
    ///   throws publish exception	
    /// </summary>
    /// <param name="tableName_"></param>
    public void Publish ( String tableName_ )
    {
      DataTable table = m_dataSet.Tables[tableName_];
      IDataService service =  m_dataService;

			
      if ( table != null && service != null )
      {
        foreach ( DataRow row in table.Rows )
        {
          if ( row.RowState == DataRowState.Modified || row.RowState == DataRowState.Added )
          {
            XmlElement element = m_xmlDoc.GetElementFromRow( row ); 
            
            //navigate up dom-tree to obtain parent-table
            while ( element.ParentNode != m_xmlDoc.DocumentElement )
            {
              element = (XmlElement)element.ParentNode;
            }
                                                
            m_dataSet.EnforceConstraints = false;
            Utils.SetValue( element, "UpdateUserId", UserId );
            m_dataSet.EnforceConstraints = true;

            IDataKey dataKey = DataKeyFactory.BuildDataKey(Utils.GetValue(element,_keyElementName));
            XMLDataObject xdo = new XMLDataObject( dataKey, element);
                    
            try
            {                                    
              service.Publish( xdo );   
              AcceptChanges( element );           
            }
            catch ( PublishException ex )
            {
              RejectChanges( element );
              throw ex;              
            }
          }
        }
      }
    }

    public void DataUpdate( object source_, DataEvent event_ )
    {                   
      ISubscription sub = (ISubscription)source_;
      XMLDataObject xdo = (XMLDataObject)event_.DataObject;            

      string tableName = xdo.Value.Name; 			

      if ( m_dataSet.Tables.Contains(tableName) )
      {        
				//string keyElementName = (string)m_keyElementByDSId[sub.DataService.Id];

        String xpath = "" + tableName + "[" + _keyElementName + "='" + event_.DataKey + "']";
        XmlElement updateElement = m_xmlDoc.DocumentElement.SelectSingleNode(xpath) as XmlElement; 
        
        m_dataSet.EnforceConstraints = false;
        
        if ( updateElement == null )
        {            
          updateElement = m_xmlDoc.CreateElement( tableName );  
          m_xmlDoc.DocumentElement.AppendChild( updateElement );

        }
        updateElement.InnerXml = xdo.Value.InnerXml;            
       
        try
        {
					m_dataSet.EnforceConstraints = true;      
          AcceptChanges( updateElement ); 
        }
        catch ( ConstraintException ex )           
        {
          m_log.Log( MSLog.Error(), "DataUpdate", "Updated violated constraints: " + 
                                            Utils.NodeToString(xdo.Value), ex );  
          RejectChanges( updateElement ); 
        }
      }       
      else
      {
        m_log.Log(MSLog.Warning(), "DataUpdate", "Unknown table: " + tableName );
      }
    }

    /// <summary>
    /// accept changes made to the dataset by the given element
    /// </summary>
    /// <param name="element_"></param>
    protected void AcceptChanges( XmlElement element_ )
    { 
      DataRow row = m_xmlDoc.GetRowFromElement( element_ );
      if ( row != null )
      {
        row.AcceptChanges();
        foreach( XmlNode child in element_.ChildNodes )
        {
					if ( child is XmlElement )
					{
						AcceptChanges( (XmlElement)child );
					}
        }
      }
    }
    /// <summary>
    /// reject changes made to the dataset by the given element
    /// </summary>
    /// <param name="element_"></param>
    protected void RejectChanges( XmlElement element_ )
    {
      DataRow row = m_xmlDoc.GetRowFromElement( element_ );
      if ( row != null )
      {
        row.RejectChanges();        
        foreach( XmlNode child in element_.ChildNodes )
        {      
					if ( child is XmlElement )
					{
						RejectChanges( (XmlElement)child );     
					}
        }
      }
    }

    protected string GetKeyField( XmlDocument schemaDoc )
    {
      //identify key element
      string keyElementName = null;
      XmlNamespaceManager xsmgr = new XmlNamespaceManager(new NameTable());
      xsmgr.AddNamespace("xs","http://www.w3.org/2001/XMLSchema");
      xsmgr.AddNamespace("ds","http://www-server-eu/ecdev/dataservices/XMLSchema");

      XmlNodeList nodeList = schemaDoc.SelectNodes( "//xs:element[@ds:IsKey='true']", xsmgr );
      foreach ( XmlNode node in nodeList )
      {
        if ( node is XmlElement )
        {
          keyElementName =  ((XmlElement)node).GetAttribute("name");					
          break; //only want the first one.
        }
      }

      if ( keyElementName != null && keyElementName.Length > 0 )
      {
        return keyElementName;
      }
      else
      {
        throw new DataServiceException("Could not determine key field from schema of dataservice ");
      }
    }


	}
}
