//---------------------------------------------------------------------------
// ($Id $)
//---------------------------------------------------------------------------
//
// Copyright (c) 2003 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
//---------------------------------------------------------------------------

using System;
using System.Collections;
using System.IO;
using System.Xml;
using System.Data;
using MorganStanley.MSDotNet.MSLog;
using MorganStanley.MSDesktop.Rambo.DataServices.Util;

namespace MorganStanley.MSDesktop.Rambo.DataServices
{
  /// <summary>
  /// 
  /// </summary>
  /// <author><a href="mailto:Kunle.Adetona@morganstanley.com">Kunle Adetona</a></author>
  public class AdoDataAdapter
  {
    protected XmlDataDocument m_xmlDoc;
    protected DataSet m_dataSet;
    protected Logger m_log = new Logger( typeof( AdoDataAdapter));
    protected IDictionary m_dataServiceById = new Hashtable();
    protected string m_userId;
		protected IDictionary m_keyElementByDSId = new Hashtable();
   
    public AdoDataAdapter( IXMLDataService ds_ ): this( new IXMLDataService[] {ds_} ) 
    {   
    }

		/// <summary>
		/// public constructor
		/// </summary>
		/// <param name="dsArray_"></param>
    public AdoDataAdapter( IXMLDataService[] dsArray_ )
    {
			
      MorganStanley.MSDesktop.Rambo.DataServices.Util.Utils.ArgumentNullException( dsArray_ );
      if ( dsArray_.Length <= 0 )
      {
        throw new ArgumentException("Empty array - Cant load DataSet schema. ");
      }
      
      m_dataSet = new DataSet();
      for ( int i = 0; i < dsArray_.Length; i++ )
      {       
        DataSet newDataSet = new DataSet();       
        string schemaString = dsArray_[i].Schema;        
        newDataSet.ReadXmlSchema( new StringReader(schemaString) );        
        AddDataService( dsArray_[i], newDataSet, schemaString );			

				//merge the schemas together..        
        m_dataSet.Merge( newDataSet, false, MissingSchemaAction.Add );
      }      
      m_xmlDoc = new XmlDataDocument( m_dataSet );   
      m_xmlDoc.LoadXml("<NewDataSet></NewDataSet>");             
    }	

    public XmlDataDocument XmlDataDocument
    {
      get
      {
        return m_xmlDoc;
      }
    }

    public DataSet DataSet
    {
      get
      {
        return m_dataSet;
      }
    }
    
    /// <summary>
    /// UserId of updater.
    /// </summary>
    public string UserId
    {
      get
      {
        if ( m_userId != null )
        {
          return m_userId;
        }
        return DSEnvironment.USER_NAME;
      }
      set
      {
        m_userId = value;
      }
    }
 
    /// <summary>
    /// remember which dataservice "owns" which tables
    /// </summary>
    /// <param name="service_"></param>
    /// <param name="dataSet_"></param>
    protected void AddDataService( IXMLDataService service_, DataSet dataSet_, String schemaString_ )
    {
      foreach ( DataTable table in dataSet_.Tables )
      {
        m_dataServiceById.Add( table.TableName, service_ );
      }

			//identify key element
			XmlDocument schemaDoc = new XmlDocument();
			schemaDoc.LoadXml( schemaString_ );
			string keyElementName = null;
			XmlNamespaceManager xsmgr = new XmlNamespaceManager(new NameTable());
			xsmgr.AddNamespace("xs","http://www.w3.org/2001/XMLSchema");
			xsmgr.AddNamespace("ds","http://www-server-eu/ecdev/dataservices/XMLSchema");

			XmlNodeList nodeList = schemaDoc.SelectNodes( "//xs:element[@ds:IsKey='true']", xsmgr );
			foreach ( XmlNode node in nodeList )
			{
				if ( node is XmlElement )
				{
					keyElementName =  ((XmlElement)node).GetAttribute("name");					
					break; //only want the first one.
				}
			}
      
			if ( keyElementName != null && keyElementName.Length > 0 )
			{
				m_keyElementByDSId.Add( service_.Id, keyElementName );
			}
			else
			{
				throw new DataServiceException("Could not determine key field from schema of dataservice '" 
					+ service_.Id +"'");
			}
    }

    public void Attach( ISubscription sub_ )
    {  
      Attach( sub_, true, true );
    }

    public void Attach( ISubscription sub_, bool snapshot_ )
    {  
      Attach( sub_, snapshot_, true );
    }    

    public void Attach( ISubscription sub_, bool snapshot_, bool updates_ )
    {  
      if ( sub_ != null )
      {        
        if ( updates_ )
        {
          sub_.OnDataUpdate += new DataEventHandler( this.DataUpdate ); 
          if ( snapshot_ )
          {
            sub_.EndSnapshot( sub_.BeginSnapshot(null,null) );            
          }
        }
        else if ( snapshot_ )
        {
          IDictionary dict = sub_.Snapshot();
          foreach ( DataEvent de in dict.Values )
          {
            DataUpdate( sub_, de );
          }
        }                   
      }
    }

    protected void DoneSnapShot( IAsyncResult ar_ )
    {
      ISubscription s = (ISubscription)ar_.AsyncState;
      try
      {
        s.EndSnapshot( ar_ );
      }
      catch ( DataServiceException ex )
      {
        m_log.Log(MSLog.Error(), "DoneSnapShot", ex.Message, ex );     
      }
    }

    /// <summary>
    /// throws publish exception
    /// </summary>
    public void Publish()
    {
      if ( m_dataSet.HasChanges() )
      {
        foreach( DataTable t in m_dataSet.Tables )
        {         
          Publish( t.TableName );
        }        
      }
    }
    /// <summary>
    ///   throws publish exception	
    /// </summary>
    /// <param name="tableName_"></param>
    public void Publish ( String tableName_ )
    {
      DataTable table = m_dataSet.Tables[tableName_];
      IDataService service =  m_dataServiceById[tableName_] as IDataService;

			string keyElementName = m_keyElementByDSId[service.Id] as string;		
			
      if ( table != null && service != null )
      {
        foreach ( DataRow row in table.Rows )
        {
          if ( row.RowState == DataRowState.Modified || row.RowState == DataRowState.Added )
          {
            XmlElement element = m_xmlDoc.GetElementFromRow( row ); 
            
            //navigate up dom-tree to obtain parent-table
            while ( element.ParentNode != m_xmlDoc.DocumentElement )
            {
              element = (XmlElement)element.ParentNode;
            }
                                                
            m_dataSet.EnforceConstraints = false;
            MorganStanley.MSDesktop.Rambo.DataServices.Util.Utils.SetValue( element, "UpdateUserId", UserId );
            m_dataSet.EnforceConstraints = true;

            IDataKey dataKey = DataKeyFactory.BuildDataKey(MorganStanley.MSDesktop.Rambo.DataServices.Util.Utils.GetValue(element,keyElementName));
            XMLDataObject xdo = new XMLDataObject( dataKey, element);
                    
            try
            {                                    
              service.Publish( xdo );   
              AcceptChanges( element );           
            }
            catch ( PublishException ex )
            {
              RejectChanges( element );
              throw ex;              
            }
          }
        }
      }
    }

    public void DataUpdate( object source_, DataEvent event_ )
    {                   
      ISubscription sub = (ISubscription)source_;
      XMLDataObject xdo = (XMLDataObject)event_.DataObject;            

      string tableName = xdo.Value.Name; 			

      if ( m_dataSet.Tables.Contains(tableName) )
      {        
				string keyElementName = (string)m_keyElementByDSId[sub.DataService.Id];

        String xpath = "" + tableName + "[" + keyElementName + "='" + event_.DataKey + "']";
        XmlElement updateElement = m_xmlDoc.DocumentElement.SelectSingleNode(xpath) as XmlElement; 
        
        m_dataSet.EnforceConstraints = false;

        if ( updateElement == null )
        {            
          updateElement = m_xmlDoc.CreateElement( tableName );  
          m_xmlDoc.DocumentElement.AppendChild( updateElement );

        }
        updateElement.InnerXml = xdo.Value.InnerXml;                  

        try
        {
//					m_dataSet.EnforceConstraints = true;      
          AcceptChanges( updateElement ); 
        }
        catch ( ConstraintException ex )           
        {
          m_log.Log(MSLog.Error(), "DataUpdate", "Updated violated constraints: " + 
                                            MorganStanley.MSDesktop.Rambo.DataServices.Util.Utils.NodeToString(xdo.Value), ex );  
          RejectChanges( updateElement ); 
        }
      }       
      else
      {
        m_log.Log(MSLog.Warning(), "DataUpdate", "Unknown table: " + tableName );
      }
    }

    /// <summary>
    /// accept changes made to the dataset by the given element
    /// </summary>
    /// <param name="element_"></param>
    protected void AcceptChanges( XmlElement element_ )
    { 
      DataRow row = m_xmlDoc.GetRowFromElement( element_ );
      if ( row != null )
      {
        row.AcceptChanges();
        foreach( XmlNode child in element_.ChildNodes )
        {
					if ( child is XmlElement )
					{
						AcceptChanges( (XmlElement)child );
					}
        }
      }
    }
    /// <summary>
    /// reject changes made to the dataset by the given element
    /// </summary>
    /// <param name="element_"></param>
    protected void RejectChanges( XmlElement element_ )
    {
      DataRow row = m_xmlDoc.GetRowFromElement( element_ );
      if ( row != null )
      {
        row.RejectChanges();        
        foreach( XmlNode child in element_.ChildNodes )
        {      
					if ( child is XmlElement )
					{
						RejectChanges( (XmlElement)child );     
					}
        }
      }
    }
  } 
}
