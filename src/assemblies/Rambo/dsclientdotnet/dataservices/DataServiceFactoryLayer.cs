﻿using MorganStanley.MSDesktop.Rambo.DataServices.SoapCps;

namespace MorganStanley.MSDesktop.Rambo.DataServices
{
  public class DataServiceFactoryLayer : IDataServiceFactory
  {
    private static IDataServiceFactory _dataServiceFactory;

    static DataServiceFactoryLayer()
    {
      _dataServiceFactory = new DataServiceFactoryLayer();
    }

    #region IDataServiceFactory Members

    public IDataService GetDataServiceManager(string id_)
    {
      return DataServiceManager.GetDataService(id_);
    }

    public IDataService GetDataServiceManager(string id_, ServerList serverList_)
    {
      return DataServiceManager.GetDataService(id_, serverList_);
    }

    public IDataService GetDataServiceManager(string id_, bool createIfNotYetRegistered_, bool setupStub_)
    {
      return DataServiceManager.GetDataService(id_, createIfNotYetRegistered_, setupStub_);
    }

    public IDataService GetDataServiceManager(string id_, bool createIfNotYetRegistered_, bool setupStub_, ServerList serverList_)
    {
      return DataServiceManager.GetDataService(id_, createIfNotYetRegistered_, setupStub_, serverList_);
    }

    public static IDataService GetDataService(string id_)
    {
      return _dataServiceFactory.GetDataServiceManager(id_);
    }

    public static IDataService GetDataService(string id_, ServerList serverList_)
    {
      return _dataServiceFactory.GetDataServiceManager(id_, serverList_);
    }

    public static IDataService GetDataService(string id_, bool createIfNotYetRegistered_, bool setupStub_)
    {
      return _dataServiceFactory.GetDataServiceManager(id_, createIfNotYetRegistered_, setupStub_);
    }

    public static IDataService GetDataService(string id_, bool createIfNotYetRegistered_, bool setupStub_, ServerList serverList_)
    {
      return _dataServiceFactory.GetDataServiceManager(id_, createIfNotYetRegistered_, setupStub_, serverList_);
    }

    #endregion
  }
}
