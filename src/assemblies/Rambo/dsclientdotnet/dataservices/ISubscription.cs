using System;
using System.Collections;

namespace MorganStanley.MSDesktop.Rambo.DataServices
{
  /// <summary>
  /// subscription type enum
  /// </summary>
    public enum SubType : byte
    {
        /// <summary>
        ///Query for all data
        /// </summary>
        GLOBAL, 
        /// <summary>
        /// Query by one or more keys
        /// </summary>
        COLLECTION, 
        /// <summary>
        /// Query by attributes but enforce that the first charatcter of the attribute names are upper case
        /// </summary>
        ATTRIBUTE, 
        /// <summary>
        /// Query by attribute and leave the name of the attribute exactly as presented
        /// </summary>
        STRICTATTRIBUTE
    }

  /// <summary>
  /// Describes the interface to a Subscription object which
  /// encapsulate the type of subscription a set of callbacks to
  /// be notified when a subscription eventoccurs and a flag indicating
  /// if the subscription is valid
  /// </summary>
  public interface ISubscription
  {

    /// <summary>
    /// Occurs when a dataservice update matches this subscription.
    /// </summary>
    event DataEventHandler OnDataUpdate;

    /// <summary>
    /// Provide a handle to this subscriptions dataservice
    /// </summary>
    IDataService DataService
    {
      get;
    }

    /// <summary>
    /// Get the subscriptions sub-type
    /// </summary>
    SubType Type
    {
      get;
    }

    /// <summary>
    /// Get the set of keys that make up this subscription (If a COLLECTION subscription)
    /// </summary>
    IList DataKeys
    {
      get;
    }

    /// <summary>
    /// Get the map of attributes making up our attribute subscription (If an ATTRIBUTE subscription)
    /// </summary>
    IDictionary Attributes
    {
      get; 
    }


    /// <summary>
    /// Get the user id
    /// </summary>
    string UserId 
    {
      get;
    }

    /// <summary>
    /// Get a snapshot of the data matching this subscription
    /// </summary>
    IDictionary Snapshot();

    /// <summary>
    /// Start subscribing for a snapshot asynchronusly, following the .NET async programming pattern
    /// </summary>
    IAsyncResult BeginSnapshot( AsyncCallback callback, Object state);

    /// <summary>
    /// Ends an asynchronous snapshot, following the .NET async programming pattern
    /// </summary>
    IDictionary EndSnapshot( IAsyncResult result );

  }
}
