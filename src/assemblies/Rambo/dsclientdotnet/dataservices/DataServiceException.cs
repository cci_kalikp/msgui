//---------------------------------------------------------------------------
// ($Id $)
//---------------------------------------------------------------------------
//
// Copyright (c) 2003 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
//---------------------------------------------------------------------------

using System;

namespace MorganStanley.MSDesktop.Rambo.DataServices
{
	/// <summary>
	/// The base of exceptions throwable by a DataService.
	/// </summary>
	/// <author><a href="mailto:Kunle.Adetona@morganstanley.com">Kunle Adetona</a></author>
	public class DataServiceException : ApplicationException
	{
		public DataServiceException(String message_):base(message_)
		{
		}

    public DataServiceException(String message_,Exception inner_):base(message_,inner_)
    {
    }
	}
}
