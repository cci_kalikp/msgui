using System;
using System.Collections;

using MorganStanley.MSDesktop.Rambo.DataServices.Util;

namespace MorganStanley.MSDesktop.Rambo.DataServices
{
	/// <summary>
	/// Summary description for DataKeyFactory.
	/// </summary>
  public sealed class DataKeyFactory
  {
    private static readonly Type stringType = typeof(string);

    private DataKeyFactory(){}

    public static IDataKey BuildDataKey(object obj_)
    {
      if (obj_.GetType() == stringType)
      {
        return BuildDataKey((obj_ as string));
      }
      else
      {
        throw new ArgumentException("Argument must be of type 'string'","obj_");
      }
    }

    /// <summary>
    /// Attempts to locate the passed character in the passed String and return the index of it.
    /// It will not recognise the character if it has a single \ before it.
    /// </summary>
    /// <param name="str_">The String to look for that char in</param>
    /// <param name="c_">The character to find</param>
    /// <returns>-1 if not found else the index of the unescaped char</returns>
    private static int UnescapedIndexOf(string str_, char c_)
    {
      return UnescapedIndexOf(str_, c_, 0);
    }
      
    /// <summary>
    /// Attempts to locate the passed character in the passed String and return the first index of it.
    /// It will not recognise the character if it has a single \ before it.
    /// </summary>
    /// <param name="str_">The String to look for that char in</param>
    /// <param name="c_">The character to find</param>
    /// <param name="startPos_">An offset to start at in the string</param>
    /// <returns>-1 if not found else the index of the unescaped char</returns>
    private static int UnescapedIndexOf(string str_, char c_, int startPos_)
    {
      if (null == str_ || startPos_ >= str_.Length)
      {
        return -1;
      }
      int slashCount = 0;
      for (int i=startPos_; i<str_.Length; ++i)
      {
        char current = str_[i];
        if ('\\' == current)
        {
          ++slashCount;
        }
        else if (c_ == current && slashCount % 2 == 0)
        {
          return i;
        }
        else
        {
          slashCount = 0;
        }
      }
      return -1;
    }

    /// <summary>
    /// Attempts to locate the passed character in the passed String and return the last index of it.
    /// It will not recognise the character if it has a single \ before it.
    /// </summary>
    /// <param name="str_">The String to look for that char in</param>
    /// <param name="c_">The character to find</param>
    /// <returns>-1 if not found else the index of the unescaped char</returns>
    private static int UnescapedLastIndexOf(string str_, char c_)
    {
      if (null == str_)
      {
        return -1;
      }
      int pos = -1;
      int slashCount = 0;
      for (int i=str_.Length-1; i>=0; --i)
      {
        char current = str_[i];
        if (current == c_)
        {
          if (pos == i+1)
          {
            return pos;
          }
          pos = i;
          slashCount = 0;
        }
        else if (current == '\\')
        {
          ++slashCount;
        }
        else if (pos != -1 && slashCount % 2 == 0)
        {
          return pos;
        }
        else
        {
          slashCount = 0;
          pos = -1;
        }
      }
      return -1;
    }
    
    public static IDataKey BuildDataKey(string keyString_)
    {
      // this operation contains logic to convert string representation of an Alias DataKey or a Version DataKey into its
      // corresponding object representation.
      // NBYRNE - The first implementation of this operation does not support version aliases as this need more thought

      // See if version information can be extracted from key string e.g.
      // 'USD/LIBOR/DEFAULT@2008-12-12[T12:00:00.012][U2008-12-12[T12:00:00.012][E]]'
      // @ and : can be escaped via \@ and \: and the \ can be escaped via \\ so \\@ is a version key etc...
      int atIndex = UnescapedIndexOf(keyString_, '@', 1); // some Dividend Keys contain '@' as first character - eg '@mser.shd/default'
        //keyString_.IndexOf('@',1); 
      if (atIndex != -1) // then attempt to build IVersionDataKey
      {
        bool exactMatch_ = keyString_.EndsWith("E");
        string dsKey = keyString_.Substring(0,atIndex);

        keyString_ = keyString_.Remove(0, dsKey.Length + 1);
        string[] arr = keyString_.Split('U','E');

        DataServiceDate dataTime, updateTime = null;

        dataTime = new DataServiceDate(arr[0]);
        if (arr.Length>1 && arr[1].Trim().Length>0)
          updateTime = new DataServiceDate(arr[1]);

        if ( updateTime == null )
          return BuildDataKey( dsKey, dataTime );
        else
          return BuildDataKey( dsKey, dataTime, updateTime, exactMatch_ );
      }      

      // attempt to extract Alias info from the keyString_
      int colonIndex = UnescapedLastIndexOf(keyString_, ':');
      if ( colonIndex != -1 ) // then has Alias
      {
        string keyStr = keyString_.Substring(0, colonIndex);
        string alias = keyString_.Substring(colonIndex +1);

        if (alias.IndexOf('#') != -1)
          throw new ArgumentException("Alias cannot contain the character '#'");

        return BuildDataKey( keyStr , alias );
      }

      // otherwise 
      return new StringDataKey( keyString_ );
    }

    public static IAliasDataKey BuildDataKey(string dskey_ , string alias_)
    {
      return new AliasDataKey(dskey_,alias_);
    }

    public static IVersionDataKey BuildDataKey(string dskey_,DataServiceDate dataTime_, DataServiceDate updateTime_, bool exactMatch_)
    {
      return new VersionDataKey(dskey_, dataTime_, updateTime_, exactMatch_);
    }

    public static IVersionDataKey BuildDataKey(string dskey_,DataServiceDate dataTime_, DataServiceDate updateTime_, bool exactMatch_, string userId_)
    {
      return new VersionDataKey(dskey_, dataTime_, updateTime_, exactMatch_, userId_);
    }

    public static IVersionDataKey BuildDataKey(string dskey_,DataServiceDate dataTime_)
    {
      return new VersionDataKey(dskey_,dataTime_);
    }
  }

  #region AliasDataKey
  /// <summary>
  /// Summary description for AliasDataKey.
  /// </summary>
  internal class AliasDataKey : StringDataKey , IAliasDataKey
  {
    protected string _alias;

    public AliasDataKey(string dsKey_,string alias_) : base(dsKey_)
    {
      _alias = alias_;
    }

    public virtual string Alias 
    {	
      get { return _alias; } 	
    }

    public override string KeyString
    {
      get { return (DSKey + ":" + Alias); }
    }

    public override bool Equals(Object obj_)
    {
      return (obj_ is AliasDataKey) &&
        base.Equals(obj_) &&
        _alias == ((AliasDataKey) obj_)._alias;
    }

    public override int GetHashCode()
    {
      return base.GetHashCode();
    }
  }
  #endregion

  #region VersionDataKey
    internal class VersionDataKey : StringDataKey , IVersionDataKey
    {
      private DataServiceDate _dataTime;
      private DataServiceDate _updateTime;
      private bool _isExactMatch;
      private string _userId;

      public VersionDataKey(string dsKey_, DataServiceDate dataTime_, DataServiceDate updateTime_, bool isExachMatch_, string userId_) : base(dsKey_)
      {
        _dataTime = dataTime_;
        _updateTime = updateTime_;
        _isExactMatch = isExachMatch_;
        _userId = userId_;
      }

      public VersionDataKey(string dsKey_, DataServiceDate dataTime_, DataServiceDate updateTime_, bool isExachMatch_) : this(dsKey_,dataTime_,updateTime_,isExachMatch_,null)
      {
      }

      public VersionDataKey(string dsKey_,DataServiceDate dataTime_) : this(dsKey_,dataTime_,new DataServiceDate(DateTime.Now),false, null)
      {
      }   

      public virtual DataServiceDate DataTime 
      {
        get { return _dataTime; }
      }

      public virtual DataServiceDate UpdateTime 
      { 
        get { return _updateTime; }
      }

      public virtual bool IsExactMatch 
      { 
        get { return _isExactMatch; }
      }

      public virtual string UserId
      {
        get { return _userId; }
      }

      public override string KeyString
      {
        get
        {
          string tmp = String.Format("{0}@{1}U{2}", DSKey, DataTime, UpdateTime);
          if (IsExactMatch)
            tmp += 'E';

          return tmp ;
        }
      }

      public override bool Equals(Object obj_)
      {
        if (obj_ is VersionDataKey)
        {
          VersionDataKey key = obj_ as VersionDataKey;
          return base.Equals(obj_) &&
            ((_dataTime != null && _dataTime.Equals(key._dataTime)) ||
            (_dataTime == null && key._dataTime == null)) &&
            ((_updateTime != null && _updateTime.Equals(key._updateTime)) ||
            (_updateTime == null && key._updateTime == null)) &&
            _isExactMatch == key._isExactMatch;
        }
        else 
        {
          return false;
        }
      }

      public override int GetHashCode()
      {
        return base.GetHashCode();
      }
    }
  #endregion
}
