//---------------------------------------------------------------------------
// ($Id $)
//---------------------------------------------------------------------------
//
// Copyright (c) 2003 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
//---------------------------------------------------------------------------

using System;
using System.Xml;
using System.Globalization;
using System.Xml.Linq;
using MorganStanley.MSDesktop.Rambo.DataServices.Util;
using MorganStanley.MSDesktop.Rambo.Desktop.Utils.Extensions;

namespace MorganStanley.MSDesktop.Rambo.DataServices
{
  /// <summary>
  /// Summary description for XMLDataObject.
  /// </summary>
  /// <author><a href="mailto:Kunle.Adetona@morganstanley.com">Kunle Adetona</a></author>
  public class XMLDataObject : IDataObject
  {
    /// <summary>
    /// the data key
    /// </summary>
    protected IDataKey m_key;
    /// <summary>
    /// the xml content
    /// </summary>
    protected XmlElement m_value;

    protected DataServiceDate m_creationTime = new DataServiceDate( DateTime.Now );

    protected DateTime m_timestamp = DateTime.MaxValue;

    /// <summary>
    /// public constructor
    /// </summary>
    /// <param name="key_"></param>
    /// <param name="value_"></param>
    /// <param name="timestamp_"></param>
    [Obsolete("Use the method that takes an IDataKey")]
    public XMLDataObject( StringDataKey key_, XmlElement value_, DateTime timestamp_ ) 
      : this ( key_, value_ )
    {      
    }

    public XMLDataObject( IDataKey key_, XmlElement value_ )       
    {
      Utils.ArgumentNullException( "key", key_ );
      Utils.ArgumentNullException( "value", value_ );

      m_key = key_;
      m_value = value_;
    }

    public XMLDataObject(IDataKey key_, XElement value_)
      : this(key_, value_.ToXmlElement())
    {
    }

    /// <summary>
    /// public constructor
    /// </summary>
    /// <param name="key_"></param>
    /// <param name="value_"></param>
    /// <param name="timestamp_"></param>
    [Obsolete("Use the method that takes an IDataKey")]
    public XMLDataObject( String key_, XmlElement value_, DateTime timestamp_ ) 
      : this ( new StringDataKey(key_), value_ )
    {    
    }

    /// <summary>
    /// DataKey property
    /// </summary>
    public IDataKey DataKey
    {
      get { return m_key; }
    }

    /// <summary>
    /// Timestamp property, shows the last update time
    /// </summary>
    public DateTime Timestamp
    {
      get
      {
        if ( m_timestamp == DateTime.MaxValue ) // only doing this logic once
        {
          // attempt to pull the correct timestamp from the underlying data
          DateTime utcDT = this.UpdateTime.Date;;
          switch ( m_value.Name )
          {
            case "DivStream":
            case "GrowthSpread":
            case "YieldCurve":
              XmlNode tsNode = m_value.SelectSingleNode("Timestamp");
              if ( null != tsNode )
                utcDT = ParseDateTime( tsNode.InnerText );
              break;
            case "Surface":
              if ( m_value.HasAttribute("timestamp") )
                utcDT = ParseDateTime( m_value.GetAttribute("timestamp") );
              break;
          }

          m_timestamp = utcDT.ToLocalTime();
        }

        return m_timestamp;
      }
    }

    private DateTime ParseDateTime( string dt_ )
    {
      DateTime result = DateTime.MaxValue;

      try
      {
        result = DateTime.Parse( dt_ );
      }
      catch { /* do nothing */ }      

      return result;
    }

    /// <summary>
    /// DataTime property in UTC
    /// </summary>    
    public DataServiceDate DataTime
    {
      get
      {
        IVersionDataKey versionKey = DataKey as IVersionDataKey;
        if (versionKey != null)
          return versionKey.DataTime;
        else
          return m_creationTime;
      }
    }

    /// <summary>
    /// Update property in UTC
    /// </summary>
    public DataServiceDate UpdateTime
    {
      get
      {
        IVersionDataKey versionKey = DataKey as IVersionDataKey;
        if (versionKey != null)
          return versionKey.UpdateTime;
        else
          return m_creationTime;
      }
    }
		
    object IDataObject.Value
    {
      get { return m_value; }
    }

    /// <summary>
    /// the Xml content
    /// </summary>
    new virtual public XmlElement Value
    {
      get { return m_value; }
    }
    
    /// <summary>
    /// A deep copy
    /// </summary>
    /// <returns></returns>
    public object Clone()
    {
      XMLDataObject clone = new XMLDataObject( m_key ,(XmlElement)Value.Clone() );
      return clone;
    }

    /// <summary>
    /// returns the XML string
    /// </summary>
    /// <returns></returns>
    public override String ToString()
    {
      return Utils.NodeToString(m_value);
    }
  }
}
