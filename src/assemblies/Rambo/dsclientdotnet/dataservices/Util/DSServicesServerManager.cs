using System;

using System.Collections;

using System.Xml;

using MorganStanley.MSDotNet.MSLog;

using MorganStanley.MSDesktop.Rambo.DataServices.SoapCps;

using MorganStanley.MSDesktop.Rambo.DataServices;



namespace MorganStanley.MSDesktop.Rambo.DataServices.Util
{

  /// <summary>

  /// Manager class used when the user has not specified a serverlist explicitly.

  /// The manager attempts to connect to a DSServices data service (using the servers

  /// listed in the old style files) and uses it to get a list of servers for all

  /// services requested via the GetServers() method.

  /// If it cannot connect to a DSServices data service the manager will fall back to

  /// returning the list of servers loaded from the files.

  /// </summary>

  internal class DSServicesServerManager : ServerManager
  {

    private static Logger _log = new Logger(typeof(DSServicesServerManager));



    /// <summary>

    /// The cache of all the server lists indexed on service Id

    /// </summary>

    protected IDictionary _services;



    /// <summary>

    /// Hash of all the servers (DSServer objects) in the managers cache keyed on host:admin 

    /// </summary>

    private Hashtable _dsservers;



    /// <summary>

    /// The DSServices data services keyed on env:loc. If it is null then the manager could not

    /// connect to the service and is using the backup server list instead

    /// </summary>

    private IDictionary _routingDataServices;



    /// <summary>

    /// List of servers loaded from the old style file for use if cannot connect to

    /// a DSServices service

    /// </summary>

    private ServerList _backupServers;

    /// <summary>
    /// Builds a new DSServicesServerManager object which will load its backup serverlist
    /// from the old style files and attempt to connect to a DSServices service using the
    /// servers in the file or using the ds.dsservices parameter in DSEnvironment
    /// </summary>
    public DSServicesServerManager(XmlNodeList overrides_ = null) : base(overrides_)
    {
      Initialize();
    }

    private void Initialize()
    {
      _log.Log(MSLog.Info(), "Initialize", "Using DSServices to get hosts");
      _services = new Hashtable();
      _dsservers = new Hashtable();
      _routingDataServices = new Hashtable();

      _backupServers = GetHostFromFile(DSEnvironment.DS_ENVIRONMENT.ToString().ToLower(),
                                       DSEnvironment.DS_LOCATION.ToString().ToLower());

      _backupServers.IsBackup = true;
    }



    private void GetDSServicesServers(string env_, string loc_, ServerList servers_)
    {

      servers_.IsFixed = false;

      servers_.IsBackup = false;

      servers_.Clear();

      if (_overrides.Contains("DSServices"))
      {

        Override o = _overrides["DSServices"] as Override;

        if (o.IsServerList)
        {

          foreach (DSServer server in o.Servers)
          {

            servers_.AddServer(server);

          }

          return;

        }

        else
        {

          env_ = o.Environment;

          loc_ = o.Location;

        }

      }

      ServerList tmplist;

      if (DSEnvironment.HasDSServicesServerList)
      {

        tmplist = new ServerList(DSEnvironment.DSServicesServerList, false);

      }

      else
      {

        tmplist = GetHostFromFile(env_, loc_);

      }

      if (tmplist != null)
      {

        foreach (DSServer server in tmplist)
        {

          servers_.AddServer(server);

        }

      }

    }



    private IDataService GetDSServicesDS(string env_, string loc_)
    {

      string key = env_ + ":" + loc_;

      IDataService ds;

      lock (_routingDataServices)
      {

        ds = _routingDataServices[key] as IDataService;

        if (ds == null)
        {

          try
          {

            _log.Log(MSLog.Info(), "GetDSServicesDS", "Creating DSServices ds for " + key + " with servers:");

            ServerList servers = new ServerList();

            GetDSServicesServers(env_, loc_, servers);

            if (servers == null || servers.Count == 0)
            {

              throw new DataServiceException("No DSServices servers found for " + key);

            }

            if (_log.WillLog(MSLogPriority.Info))
            {

              _log.Log(MSLog.Info(), "GetDSServicesDS", "DSServices Hosts: " + servers.ToStringArray().ToString());

            }

            ds = new SOAPCPSDataService("DSServices", servers);

            ((SOAPCPSDataService)ds).Timeout = 30000; //30 seconds timeout for DSServices

            _routingDataServices[key] = ds;

            IDictionary attribs = new Hashtable(2);

            attribs["Environment"] = env_;

            attribs["Location"] = loc_;

            ISubscription sub = ds.CreateSubscription(attribs);

            sub.OnDataUpdate += new DataEventHandler(HandleRoutingUpdate);

          }

          catch (DataServiceException ex_)
          {

            _log.Log(MSLog.Error(), "GetDSServicesDS", "Error creating DSServices data service: ", ex_);

            throw ex_;

          }

        }

      }

      return ds;

    }



    /// <summary>

    /// Builds a subscription to the DSServices data service for the current environment

    /// settings and the passed service name.

    /// </summary>

    /// <param name="service_">the service Id to use</param>

    /// <returns>A Data Service subscription to the DSServices data service</returns>

    private ISubscription GetSubscription(IDataService ds_, string service_, string env_, string loc_)
    {

      IDictionary attributes = new Hashtable(3);

      attributes["environment"] = env_;

      attributes["location"] = loc_;

      attributes["service"] = service_;

      ISubscription sub = ds_.CreateSubscription(attributes);

      return sub;

    }



    private ServerList GetBackupServers(String service_)
    {

      Override o = _overrides[service_] as Override;

      if (o != null)
      {

        try
        {

          ServerList servers = GetHostFromFile(o.Environment, o.Location);

          servers.IsBackup = true;

          return servers;

        }

        catch (Exception ex_)
        {

          _log.Log(MSLog.Error(), "GetBackupServers", "Couldn't read backup server list from file: ", ex_);

          return _backupServers;

        }

      }

      else
      {

        return _backupServers;

      }

    }



    /// <summary>

    /// Attempts to get the a list of all the services available for the passed

    /// service name and builds a ServerList object to store them in.

    /// This object is then stored in the manager's cache.

    /// </summary>

    /// <param name="service_">the service Id to lookup</param>

    /// <param name="servers_">the server list to set to the servers returned by DSServices</param>

    /// <exception cref="DataServiceException">thrown if cannot retrieve list</exception>

    private void FetchServerList(string service_, ServerList servers_)
    {

      try
      {

        String env = DSEnvironment.DS_ENVIRONMENT.ToString().ToLower();

        String loc = DSEnvironment.DS_LOCATION.ToString().ToLower();

        if (_overrides.Contains(service_))
        {

          Override o = _overrides[service_] as Override;

          if (o.IsServerList)
          {

            lock (_services)
            {

              servers_.IsFixed = false;

              servers_.IsBackup = false;

              servers_.Clear();

              for (IEnumerator i = o.Servers.GetEnumerator(); i.MoveNext(); )
              {

                servers_.AddServer((DSServer)i.Current);

              }

            }

            return;

          }

          else
          {

            env = o.Environment;

            loc = o.Location;

          }

        }

        IDataService ds = GetDSServicesDS(env, loc);

        ISubscription subscription = GetSubscription(ds, service_, env, loc);

        IDictionary snapshot = ds.GetSnapshot(subscription);

        lock (_services)
        {

          servers_.IsFixed = false;

          servers_.IsBackup = false;

          servers_.Clear();

          if (snapshot != null)
          {

            for (IEnumerator i = snapshot.Values.GetEnumerator(); i.MoveNext(); )
            {

              DataEvent data = (DataEvent)i.Current;

              XmlElement dsservice = data.DataObject.Value as XmlElement;

              if ("up" == dsservice.SelectSingleNode("Status").InnerText)
              {

                string host = dsservice.SelectSingleNode("Host").InnerText;

                string port = dsservice.SelectSingleNode("Admin").InnerText;

                string id = host + ":" + port;

                DSServer server = _dsservers[id] as DSServer;

                if (server == null)
                {

                  server = new DSServer(dsservice);

                  _dsservers[id] = server;

                }

                else
                {

                  server.AddService(dsservice.SelectSingleNode("Service").InnerText);

                }

                servers_.AddServer(server);

              }

            }

          }

        }

      }

      catch (DataServiceException ex_)
      {

        _log.Log(MSLog.Warning(), "FetchServerList", "Couldn't connect to DSServices service ", ex_);

        throw new DataServiceException("Couldn't connect to DSServices service", ex_);

      }

      catch (Exception ex_)
      {

        _log.Log(MSLog.Warning(), "FetchServerList", "Couldn't connect to DSServices service", ex_);

        throw new DataServiceException("Couldn't connect to DSServices service", ex_);

      }

    }



    /// <summary>

    /// Gets the ServerList for the specified service. It will first look for the server list

    /// in the managers cache and if it is not found there then it attempts to get the

    /// list from the DSServices data service.

    /// No DSServices can be found then the method returns the backup serverlist loaded

    /// from the files.

    /// </summary>

    /// <param name="service_">the Id of the service to get the list for</param>

    public override ServerList GetServers(string service_)
    {

      if (DSEnvironment.DS_USEDSSERVICES)
      {

        ServerList servers;

        lock (_services)
        {

          servers = _services[service_] as ServerList;

          if (servers == null)
          {

            servers = new ServerList();

            _services[service_] = servers;

          }

        }

        if (servers.IsBackup || servers.Count == 0)
        {

          try
          {

            FetchServerList(service_, servers);

            if (servers.Count == 0)
            {

              _log.Log(MSLog.Warning(), "GetServers", "Couldn't locate any servers for " + service_ + " using DSServices service, defaulting to file");

              lock (_services)
              {

                servers.IsBackup = true;

                foreach (DSServer server in GetBackupServers(service_))
                {

                  servers.AddServer(server);

                }

              }

            }

          }

          catch (Exception ex_)
          {

            _log.Log(MSLog.Warning(), "GetServers", "Error connecting to DSServices service, defaulting to file list", ex_);

            lock (_services)
            {

              servers.IsBackup = true;

              foreach (DSServer server in GetBackupServers(service_))
              {

                servers.AddServer(server);

              }

            }

          }

        }

        return servers;

      }

      else
      {

        return GetBackupServers(service_);

      }

    }



    private void RemoveDSServicesServer(DSServer server_)
    {

      lock (_routingDataServices)
      {

        lock (_services)
        {

          foreach (string id in _routingDataServices.Keys)
          {

            string[] parts = id.Split(':');

            IDataService ds = _routingDataServices[id] as IDataService;

            ServerList servers = ds.Servers;

            servers.RemoveServer(server_);

            if (servers.Count == 0)
            {

              GetDSServicesServers(parts[0], parts[1], servers);

            }

          }

        }

      }

    }



    /// <summary>

    /// Removes the DSServer object from all the server lists it is on

    /// </summary>

    /// <param name="server_">The server object (from a serverlist) to remove</param>

    public override void RemoveServer(DSServer server_)
    {

      RemoveDSServicesServer(server_);

      string id = server_.Host + ":" + server_.AdminPort;

      IList refetch = new ArrayList();

      lock (_services)
      {

        _dsservers.Remove(id);

        foreach (string service in server_.Services)
        {

          if (_services.Contains(service))
          {

            ServerList list = (ServerList)_services[service];

            list.RemoveServer(server_);

            if (list.Count <= 0)
            {

              refetch.Add(service);

            }

          }

        }

      }

      for (IEnumerator i = refetch.GetEnumerator(); i.MoveNext(); )
      {

        string service = (string)i.Current;

        _log.Log(MSLog.Debug(), "RemoveServer", "Refetching serverlist for " + service);

        GetServers(service);

      }

    }



    private void RefreshDSServicesServers()
    {

      lock (_routingDataServices)
      {

        lock (_services)
        {

          foreach (string id in _routingDataServices.Keys)
          {

            string[] parts = id.Split(':');

            IDataService ds = _routingDataServices[id] as IDataService;

            GetDSServicesServers(parts[0], parts[1], ds.Servers);

          }

        }

      }

    }



    /// <summary>

    /// Refreshes the serverlists to take into account any changes

    /// </summary>

    public override void RefreshServers()
    {

      Logger log = new Logger(typeof(DSServicesServerManager));

      Hashtable clonedservices;

      lock (_services)
      {

        log.Log(MSLog.Debug(), "RefreshServers", "Starting server refresh for " + _services.Count + " services");

        clonedservices = new Hashtable(_services);

      }

      foreach (string key in clonedservices.Keys)
      {

        ServerList servers = clonedservices[key] as ServerList;

        FetchServerList(key, servers);

      }

      log.Log(MSLog.Debug(), "RefreshServers", "Refresh complete");

    }



    /// <summary>

    /// Gets the ServerList for the specified service

    /// </summary>

    public override ServerList this[string service_]
    {

      get { return GetServers(service_); }

    }



    /// <summary>

    /// Gets a hashtable keyed on Service Id with the Serverlist's for each service as the

    /// values

    /// </summary>

    public override IDictionary Servers
    {

      get
      {

        IDictionary services = new Hashtable(_services.Count + 1);

        Hashtable clonedservices;

        lock (_services)
        {

          clonedservices = new Hashtable(_services);

        }

        foreach (string key in clonedservices.Keys)
        {

          ServerList servers = clonedservices[key] as ServerList;

          services.Add(key, new ServerList(servers));

        }

        return services;

      }

    }



    /// <summary>

    /// returns true if the manager has managed to connect to a DSServices service

    /// and is using it to get the server locations

    /// </summary>

    public bool UsingDSServices
    {

      get { return DSEnvironment.DS_USEDSSERVICES; }

    }



    private void HandleRoutingUpdate(object sender_, DataEvent event_)
    {

      XmlElement update = event_.DataObject.Value as XmlElement;

      if (update != null)
      {

        string service = update.SelectSingleNode("Service").InnerText;

        try
        {

          lock (_services)
          {





            if (_overrides.Contains(service))
            {

              Override o = _overrides[service] as Override;

              if (update.SelectSingleNode("Location").InnerText != o.Location ||

                update.SelectSingleNode("Environment").InnerText != o.Environment)
              {

                _log.Log(MSLog.Debug(), "HandleRoutingUpdate",

                                        "Ignoring DSServices update on " +

                                        event_.DataKey.DSKey + " due to " +

                                        service + "/" + o.Environment + "/" +

                                        o.Location + " Override !");

                return;

              }

            }

            else
            {

              if (DSEnvironment.DS_ENVIRONMENT.ToString().ToLower() != update.SelectSingleNode("Environment").InnerText ||

                    DSEnvironment.DS_LOCATION.ToString().ToLower() != update.SelectSingleNode("Location").InnerText)

                _log.Log(MSLog.Debug(), "HandleRoutingUpdate", "Ignoring DSServices update on " +

                                             event_.DataKey.DSKey + " due to " +

                                             service + " env / loc mismatch !");

              return;

            }



            ServerList list = (ServerList)_services[service];

            if (list != null)
            {

              _log.Log(MSLog.Debug(), "HandleRoutingUpdate",

                                      "Recieved DSServices update on " +

                                      event_.DataKey.DSKey + " - " +

                                      update.SelectSingleNode("Status").InnerText);

              string host = update.SelectSingleNode("Host").InnerText;

              string port = update.SelectSingleNode("Admin").InnerText;

              string id = host + ":" + port;

              DSServer server = (DSServer)_dsservers[id];



              if ("up" == update.SelectSingleNode("Status").InnerText)
              {

                if (list.IsBackup)
                {

                  list.IsBackup = false;

                  list.IsFixed = false;

                  list.Clear();

                }

                if (server == null)
                {

                  server = new DSServer(update);

                  _dsservers[id] = server;

                }

                else
                {

                  if (!server.HasService(service))
                  {

                    server.AddService(service);

                  }

                }

                list.AddServer(server);

              }

              else
              {

                if (server != null)
                {

                  list.RemoveServer(server);

                  server.RemoveService(service);

                  if (server.Services.Count == 0)
                  {

                    _dsservers.Remove(id);

                  }

                }

              }

            }

          }

        }

        catch (Exception ex_)
        {

          _log.Log(MSLog.Error(), "HandleRoutingUpdate",

                    "Error processing Routing table update: " + update.OuterXml, ex_);

        }

      }

    }

  }

}

