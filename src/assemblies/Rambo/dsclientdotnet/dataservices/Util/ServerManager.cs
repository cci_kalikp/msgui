//---------------------------------------------------------------------------
// ($Id $)
//---------------------------------------------------------------------------
//
// Copyright (c) 2005 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
//---------------------------------------------------------------------------

using System;
using System.Collections;
using System.Xml;
using MorganStanley.IED.Concord.Configuration;
using MorganStanley.MSDotNet.MSLog;
using MorganStanley.MSDesktop.Rambo.DataServices.SoapCps;
using MorganStanley.MSDesktop.Rambo.AsyncUtils;

namespace MorganStanley.MSDesktop.Rambo.DataServices.Util
{

  /// <summary>
  /// Interface for the Class responsible for handling server lookups for a given service Id
  /// </summary>
  abstract class ServerManager
  {
    private static Logger _log = new Logger(typeof(ServerManager));

    protected IDictionary _overrides;

    private delegate ServerList GetServersHandle(string service_);
    private readonly Hashtable _asyncRequests = Hashtable.Synchronized(new Hashtable());

    protected ServerManager(XmlNodeList overrides_ = null)
    {
      _overrides = new Hashtable();

      if (overrides_ == null)
      {
        ProcessOverrides();
      }
      else
      {
        ProcessConfigOverridesXml(overrides_);
      }
    }        

    protected void ProcessOverrides()
    {
      if (DSEnvironment.DS_SERVER_OVERRIDES != null)
      {
        ProcessEnvOverrides(DSEnvironment.DS_SERVER_OVERRIDES);
      }
      if (DSEnvironment.DS_SERVER_OVERRIDES_CONFIG != null)
      {
        ProcessConfigOverrides(DSEnvironment.DS_SERVER_OVERRIDES_CONFIG);
      }
      else if (DSEnvironment.DS_SERVER_OVERRIDES_CONFIG_NAME != null)
      {
        ProcessConfigOverridesName(DSEnvironment.DS_SERVER_OVERRIDES_CONFIG_NAME);
      }
    }

    protected void ProcessEnvOverrides(string overrides_)
    {
      string[] overrides = overrides_.Split(',');
      for (int i = 0; i < overrides.Length; ++i)
      {
        string singleOverride = overrides[i];
        try
        {
          string[] parts = singleOverride.Split('+');
          if (parts.Length < 3)
          {
            _log.Log(MSLog.Error(), "processEnvOverrides", "Invalid override: " + singleOverride);
            continue;
          }
          string dsname = parts[0];
          _log.Log(MSLog.Debug(), "processEnvOverrides", "Processing DS override on " + dsname);
          string type = parts[1];
          string ovalue = parts[2];
          if ("env" == type)
          {
            _overrides[dsname] = new Override(ovalue, DSEnvironment.DS_LOCATION.ToString().ToLower());
          }
          else if ("loc" == type)
          {
            _overrides[dsname] = new Override(DSEnvironment.DS_ENVIRONMENT.ToString().ToLower(), ovalue);
          }
          else if ("env_loc" == type)
          {
            string[] envloc = ovalue.Split('_');
            if (envloc.Length < 2)
            {
              _log.Log(MSLog.Error(), "processEnvOverrides", "Invalid override: " + singleOverride);
              continue;
            }
            _overrides[dsname] = new Override(envloc[0], envloc[1]);
          }
          else if ("server" == type)
          {
            IList servers = new ArrayList();
            servers.Add(ovalue);
            _overrides[dsname] = new Override(new ServerList(servers, true ));
          }
          else
          {
            _log.Log(MSLog.Error(), "processEnvOverrides", "Invalid override: " + singleOverride);
          }
        }
        catch (Exception)
        {
          _log.Log(MSLog.Error(), "processEnvOverrides", "Invalid override: " + singleOverride);
        }
      }
    }

    protected void ProcessConfigOverrides(string filename_)
    {
      try
      {
        XmlDocument doc = new XmlDocument();
        doc.Load(filename_);
        XmlNodeList overrideNodeList = doc.DocumentElement.SelectNodes("Override");

        ProcessConfigOverridesXml(overrideNodeList);
      }
      catch (Exception ex_)
      {
        _log.Log(MSLog.Error(), "processConfigOverrides", "Error building DS overrides from file: " + filename_, ex_);
      }
    }

    protected void ProcessConfigOverridesName(string overrideName_)
    {
      try
      {
        Configurator config = (Configurator)ConfigurationManager.GetConfig(overrideName_);
        XmlNode node = config.GetNode(overrideName_, null);
        XmlNodeList overrides = node.SelectNodes("Override");

        ProcessConfigOverridesXml(overrides);
      }
      catch (Exception ex_)
      {
        _log.Log(MSLog.Error(), "processConfigOverrides", "Error building DS overrides from config: " + overrideName_, ex_);
      }
    }

    protected void ProcessConfigOverridesXml(XmlNodeList overrideNodeList_)
    {
      try
      {
        if (overrideNodeList_ != null)
        {
          foreach (XmlElement overrideEl in overrideNodeList_)
          {
            string dsName = overrideEl.GetAttribute("dataservice");
            _log.Log(MSLog.Debug(), "processConfigOverrides", "Processing DS override on " + dsName);
            XmlElement serverEl = overrideEl.SelectSingleNode("Servers") as XmlElement;
            if (serverEl == null)
            {
              XmlNode env = overrideEl.SelectSingleNode("Environment");
              XmlNode loc = overrideEl.SelectSingleNode("Location");
              _overrides[dsName] = new Override(
                      env == null ? DSEnvironment.DS_ENVIRONMENT.ToString().ToLower() : env.InnerText,
                      loc == null ? DSEnvironment.DS_LOCATION.ToString().ToLower() : loc.InnerText);
            }
            else
            {
              XmlNodeList serverEls = serverEl.SelectNodes("Server");
              if (serverEls != null && serverEls.Count > 0)
              {
                _log.Log(MSLog.Debug(), "processConfigOverrides", "Found " + serverEls.Count + " servers");
                IList servers = new ArrayList();
                foreach (XmlElement server in serverEls)
                {
                  _log.Log(MSLog.Debug(), "processConfigOverrides", "Adding server: " + server.InnerText);
                  servers.Add(server.InnerText);
                }
                _log.Log(MSLog.Debug(), "processConfigOverrides", "Setting _services[" + dsName + "] to " + servers.Count + " servers");
                _overrides[dsName] = new Override(new ServerList(servers, true));
              }
              else
              {
                _log.Log(MSLog.Error(), "processConfigOverrides", "Invalid override: " + overrideEl.OuterXml);
              }
            }
          }          
        }
      }
      catch (Exception ex_)
      {
        _log.Log(MSLog.Error(), "processConfigOverrides", "Error building DS overrides from xml", ex_);
      }
    }

    protected ServerList GetHostFromFile(String env_, String loc_)
    {
      Logger log = new Logger(typeof(DSServicesServerManager));
      string version = env_ == "prod" ? loc_ + env_ : env_;
      string fileName = DSEnvironment.DS_SERVERS_CONFIG != null ?
                        DSEnvironment.DS_SERVERS_CONFIG :
                        @"\\san01b\DevAppsGML\dist\ecdev\PROJ\dataservicehosts\" + version + @"\etc\" + loc_ + "_" + env_ + "_hosts.xml";
      try
      {
        log.Log(MSLog.Debug(), "getHostFromFile", "Loading DSServices servers from: " + fileName);
        XmlDocument doc = new XmlDocument();
        doc.Load(fileName);
        XmlNodeList serverEleList = doc.DocumentElement.SelectNodes("Server");

        if (serverEleList.Count == 0)
        {
          log.Log(MSLog.Critical(), "getHostFromFile", "No entries in " + fileName + " for DSServices servers");
          throw new DataServiceException("No DSServices Servers found, Cannot connect to Data Services");
        }

        ServerList servers = new ServerList();
        foreach (XmlElement serverEle in serverEleList)
        {
          string host = serverEle.InnerText;
          string ktcp = null;
          if (serverEle.HasAttribute("ktcp"))
          {
            ktcp = serverEle.GetAttribute("ktcp");
          }
          string http = serverEle.GetAttribute("http");
          string tcp = serverEle.GetAttribute("tcp");
          DSServer server = new DSServer(host, null, ktcp, tcp, http, "up", loc_, env_, null); 
          servers.AddServer(server);
        }
        return servers;
      }
      catch (Exception ex_)
      {
        log.Log(MSLog.Critical(), "getHostFromFile", "Error reading " + fileName + " for DSServices servers", ex_);
        throw new DataServiceException("Couldn't Read DSServices Servers list, Cannot connect to Data Services", ex_);
      }
    }

    /// <summary>
    /// Gets the ServerList for the specified service asynchronously
    /// </summary>
    /// <param name="service_">the service you want the ServerList for</param>
    /// <param name="callback_">callback delegate</param>
    /// <param name="state_">async state object</param>
    /// <returns>IAsyncResult to be used with EndGetServers</returns>
    public virtual IAsyncResult BeginGetServers(string service_, AsyncCallback callback_, object state_)
    {
      Asynchronizer async = new Asynchronizer(callback_, state_, "ds-serverlist");
      lock (_asyncRequests.SyncRoot)
      {
        IList requests = _asyncRequests[service_] as IList;
        if (requests == null)
        {
          requests = new ArrayList();
          _asyncRequests[service_] = requests;
        }
        else
        {
          ManualAsyncResult result = new ManualAsyncResult(callback_, state_, async);
          requests.Add(result);
          return result;
        }
      }
      return async.BeginInvoke(new GetServersHandle(GetServersAndCallback), new object[] { service_ });
    }

    /// <summary>
    /// Gets a ServerList requested by the past async request result
    /// </summary>
    /// <param name="ar_">result from BeginGetServers</param>
    /// <returns>ServerList for the service originally requested</returns>
    public virtual ServerList EndGetServers(IAsyncResult ar_)
    {
      return (ServerList)((AsynchronizerResult)ar_).SynchronizeInvoke.EndInvoke(ar_);
    }

    private ServerList GetServersAndCallback(string service_)
    {
      try
      {
        ServerList list = GetServers(service_);
        IList requests;
        lock (_asyncRequests.SyncRoot)
        {
          requests = _asyncRequests[service_] as IList;
          if (requests != null)
          {
            _asyncRequests.Remove(service_);
          }
        }
        if (requests != null)
        {
          foreach (ManualAsyncResult result in requests)
          {
            result.SetMethodReturnedValue(list);
          }
        }
        return list;
      }
      catch (Exception ex_)
      {
        IList requests;
        lock (_asyncRequests.SyncRoot)
        {
          requests = _asyncRequests[service_] as IList;
          if (requests != null)
          {
            _asyncRequests.Remove(service_);
          }
        }
        if (requests != null)
        {
          foreach (ManualAsyncResult result in requests)
          {
            result.SetInvokeException(ex_);
          }
        }
        throw ex_;
      }
    }

    /// <summary>
    /// Gets the ServerList for the specified service
    /// </summary>
    /// <param name="service_">the Id of the service to get the list for</param>
    public abstract ServerList GetServers(string service_);

    /// <summary>
    /// Gets the ServerList for the specified service
    /// </summary>
    public abstract ServerList this[string service_]
    {
      get;
    }

    /// <summary>
    /// Removes the DSServer object from the serverlist
    /// </summary>
    /// <param name="server_">The server object (from a serverlist) to remove</param>
    public abstract void RemoveServer(DSServer server_);

    /// <summary>
    /// Refreshes the serverlists to take into account any changes
    /// </summary>
    public abstract void RefreshServers();

    /// <summary>
    /// Gets a hashtable keyed on Service Id with the Serverlist's for each service as the
    /// values
    /// </summary>
    public abstract IDictionary Servers
    {
      get;
    }

    protected class Override
    {
      private string _environment;
      private string _location;
      private ServerList _servers;

      public Override(string environment_, string location_)
      {
        _environment = environment_;
        _location = location_;
      }

      public Override(ServerList servers_)
      {
        _servers = servers_;
      }

      public string Environment
      {
        get
        {
          return _environment;
        }
      }

      public string Location
      {
        get
        {
          return _location;
        }
      }

      public ServerList Servers
      {
        get
        {
          return _servers;
        }
      }

      public bool IsServerList
      {
        get
        {
          return _servers != null;
        }
      }
    }
  }
}
