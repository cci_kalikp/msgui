using System;
using System.Collections;
using System.Xml;
using MorganStanley.MSDotNet.MSLog;
using MorganStanley.MSDesktop.Rambo.DataServices.SoapCps;

namespace MorganStanley.MSDesktop.Rambo.DataServices.Util
{
  /// <summary>
  /// Server manager for the case when the user specifies his own server list via the
  /// ds.servers parameter. The manager will always return the specified server list and
  /// will never lookup a server via the DSServices service or from the old style file
  /// </summary>
  internal class ListServerManager : ServerManager
  {
    /// <summary>
    /// The specified serverlist
    /// </summary>
    private ServerList _defaultServers;

    /// <summary>
    /// The cache of all the server lists indexed on service Id
    /// </summary>
    protected IDictionary _services;

    /// <summary>
    /// Creates a manager from the passed list of servers (as strings of the form
    /// transport://host:port
    /// </summary>
    /// <param name="servers_">the list of servers</param>
    public ListServerManager(IList servers_ , XmlNodeList overrides_ = null) : base(overrides_)
    {
      Initailize(servers_);
    }

    private void Initailize(IList servers_)
    {
      _services = new Hashtable();
      _defaultServers = new ServerList(servers_, true);
      Logger log = new Logger(typeof(ListServerManager));
      if (log.WillLog(MSLogPriority.Info))
      {
        log.Log(MSLog.Info(), "Initailize", "Using List to get hosts");
        string[] servers = _defaultServers.ToStringArray();
        for (int i = 0; i < servers.Length; ++i)
        {
          log.Log(MSLog.Info(), "Initailize", "Host: " + servers[i]);
        }
      }
    }

    /// <summary>
    /// Gets the ServerList for the specified service
    /// </summary>
    /// <param name="service_">the Id of the service to get the list for</param>
    public override ServerList GetServers(string service_) 
    {
      lock (_services)
      {
        ServerList servers = _services[service_] as ServerList;
        if (servers == null)
        {
          Override oride = _overrides[service_] as Override;
          servers = oride != null && oride.IsServerList ? new ServerList(oride.Servers) :
                    _defaultServers;
          _services[service_] = servers;
        }
        return servers;
      }
    }

    /// <summary>
    /// Removes the DSServer object from the serverlist
    /// </summary>
    /// <param name="server_">The server object (from a serverlist) to remove</param>
    public override void RemoveServer(DSServer server_) 
    {
      _defaultServers.RemoveServer(server_);
    }

    /// <summary>
    /// Refreshes the serverlists to take into account any changes
    /// </summary>
    public override void RefreshServers()
    {
    }

    /// <summary>
    /// Gets the ServerList for the specified service
    /// </summary>
    public override ServerList this [string service_]
    {
      get { return GetServers(service_); }
    }

    /// <summary>
    /// Gets a hashtable keyed on Service Id with the Serverlist's for each service as the
    /// values
    /// </summary>
    public override IDictionary Servers 
    {
      get 
      {
        IDictionary services = new Hashtable(_services.Count + 1);
        IDictionary clonedservices;
        lock (_services)
        {
          clonedservices = new Hashtable(_services);
        }
        foreach (string key in clonedservices.Keys)
        {
          ServerList servers = clonedservices[key] as ServerList;
          services[key] = new ServerList(servers);
        }
        return services;
      }
    }
  }
}
