using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using System.Collections;
using MorganStanley.MSDotNet.MSLog;

namespace MorganStanley.MSDesktop.Rambo.DataServices.Util
{

  /// <summary>
  /// Represents the state of a Data Service Server represented by a DSServer object
  /// </summary>
	public enum ServerState { up, down, error };

	/// <summary>
	/// Representation for a Data Service server used by the ServerLists
	/// </summary>
	public class DSServer
	{
    /// <summary>
    /// The server's host name
    /// </summary>
		private string _host;

    /// <summary>
    /// The server's admin port
    /// </summary>
		private string _admin;

    /// <summary>
    /// The kerberized tcp connection
    /// </summary>
	  private string _ktcp;

    /// <summary>
    /// the server's tcp port (null if none)
    /// </summary>
		private string _tcp;

    /// <summary>
    /// the server's http port (null if none)
    /// </summary>
		private string _http;

    /// <summary>
    /// the current status of the server
    /// </summary>
		private ServerState _status;

    /// <summary>
    /// The location of the server
    /// </summary>
		private DSEnvironment.DSLoc _location;

    /// <summary>
    /// The environment the server is for
    /// </summary>
		private DSEnvironment.DSEnv _environment;

    /// <summary>
    /// The services supported by this server (or the ones known at the moment
    /// as this list is built when data services are requested not all at once.
    /// </summary>
    private IList _services;

    private static Logger _log = new Logger(typeof(DSServer));

    /// <summary>
    /// Creates a new DSServer object from the passed values
    /// </summary>
    /// <param name="host_">the host name</param>
    /// <param name="admin_">the admin port</param>
    /// <param name="ktcp_">the ktcp port</param>
    /// <param name="tcp_">the tcp port</param>
    /// <param name="http_">the http port</param>
    /// <param name="status_">the server's current status</param>
    /// <param name="location_">the location</param>
    /// <param name="environment_">the environment</param>
    /// <param name="services_">list of service names supported</param>
		public DSServer(string host_, string admin_, string ktcp_ , string tcp_, string http_, string status_,
									string location_, string environment_, ICollection services_)
		{
            if (ktcp_ == null && tcp_ == null && http_ == null) throw new ArgumentNullException("ports_");
 
            _host = host_;
			_admin = admin_;
            _ktcp = ktcp_;
			_tcp = tcp_;
			_http = http_;
			_status = (ServerState) Enum.Parse(typeof(ServerState), status_, true);
			_location = (DSEnvironment.DSLoc) Enum.Parse(typeof(DSEnvironment.DSLoc), location_, true);
			_environment = (DSEnvironment.DSEnv) Enum.Parse(typeof(DSEnvironment.DSEnv), environment_, true);
			if (services_ != null) 
			{
				_services = new ArrayList(services_);
			}
			else 
			{
				_services = new ArrayList();
			}
		}

    /// <summary>
    /// Creates a new DSServer object from the passed Xml Element (of the form returned by
    /// the DSServices data service)
    /// </summary>
    /// <param name="dsservice_">The XmlElement (value) return by the data serivce</param>
		public DSServer(XmlElement dsservice_) 
		{
			_services = new ArrayList();
          
			_host = dsservice_.SelectSingleNode("Host").InnerText;
			_admin = dsservice_.SelectSingleNode("Admin").InnerText;
			
            if (dsservice_.SelectSingleNode("Ktcp") != null)
            {
                _ktcp = dsservice_.SelectSingleNode("Ktcp").InnerText;
            }

            if (dsservice_.SelectSingleNode("Tcp") != null)
            {
                _tcp = dsservice_.SelectSingleNode("Tcp").InnerText;
            }

            if (dsservice_.SelectSingleNode("Http") != null)
            {
               _http = dsservice_.SelectSingleNode("Http").InnerText;
            }

            _environment = (DSEnvironment.DSEnv) Enum.Parse(typeof(DSEnvironment.DSEnv), 
																		dsservice_.SelectSingleNode("Environment").InnerText, true);
			_location = (DSEnvironment.DSLoc) Enum.Parse(typeof(DSEnvironment.DSLoc), 
																		dsservice_.SelectSingleNode("Location").InnerText, true);
			_status = (ServerState) Enum.Parse(typeof(ServerState), 
																		dsservice_.SelectSingleNode("Status").InnerText, true);
			_services.Add(dsservice_.SelectSingleNode("Service").InnerText);
		}


    /// <summary>
    /// Whether or not the server has the passed service in it's services list
    /// </summary>
    /// <param name="service_">the service Id</param>
    /// <returns>true if contained</returns>
		public bool HasService(string service_) 
		{
			return _services.Contains(service_);
		}

    /// <summary>
    /// Adds the Service Id to the DSServer object's list of services
    /// </summary>
    /// <param name="service_"></param>
		public void AddService(string service_)
		{
			if (!_services.Contains(service_)) 
			{
				_services.Add(service_);
			}
		}

    /// <summary>
    /// Removes the passed service from the DSServer's service list
    /// </summary>
    /// <param name="service_"></param>
		public void RemoveService(string service_) 
		{
			_services.Remove(service_);
		}

    /// <summary>
    /// The server's hostname
    /// </summary>
    public string Host
		{
			get { return _host; }
		}

    /// <summary>
    /// The server's admin port
    /// </summary>
    public string AdminPort
		{
			get { return _admin; }
		}

    /// <summary>
    /// the server's ktcp port (null if none)
    /// </summary>
    public string KTCPPort
    {
      get { return _ktcp; }
    }

    /// <summary>
    /// the server's tcp port (null if none)
    /// </summary>
    public string TCPPort
		{
			get { return _tcp; }
		}

    /// <summary>
    /// the server's http port (null if none)
    /// </summary>
    public string HTTPPort 
		{
			get { return _http; }
		}

    /// <summary>
    /// the current status of the server
    /// </summary>
    public ServerState Status 
		{
			get { return _status; }
		}

    /// <summary>
    /// The location of the server
    /// </summary>
    public DSEnvironment.DSLoc Location 
		{
			get { return _location; }
		}

    /// <summary>
    /// The environment the server is for
    /// </summary>
    public DSEnvironment.DSEnv Environment
		{
			get { return _environment; }
		}

    /// <summary>
    /// The services supported by this server (or the ones known at the moment
    /// as this list is built when data services are requested not all at once.
    /// </summary>
    public IList Services 
		{
			get { return _services; }
		}

    /// <summary>
    /// The server's address using the current environment settings
    /// Default logic: it will pick the first available transport in this order: ktcp, tcp, http.
    /// If DSEnvironment.PreferredTransport is set, it will try to pick that if it's supported by the server,
    /// otherwise it falls back to the default logic.
    /// </summary>
    public string Address 
    {
      get
      {
        DSEnvironment.DSTransport[] transportPreferenceOrder = { 
          DSEnvironment.PreferredTransport, 
          DSEnvironment.DSTransport.Ktcp, 
          DSEnvironment.DSTransport.Tcp, 
          DSEnvironment.DSTransport.Http 
        };

        string address = (from transport in transportPreferenceOrder
                          where TransportSupported(transport)
                          select transport.ToString().ToLower() + "://" + _host + ":" + GetPortFor(transport)).
            FirstOrDefault();

        if (_log.WillLog(MSLogPriority.Info))
            _log.Log(MSLog.Info(), "Address", string.Format("Preferred transport{0} , Address: {1}", DSEnvironment.PreferredTransport.ToString(), address));

        return address;
      }
    }

    public override bool Equals(object o_)
    {
      if (o_ == null || !(o_ is DSServer))
      {
        return false;
      }
      DSServer s = o_ as DSServer;
      return _host == s._host && _ktcp == s._ktcp && _tcp == s._tcp && _http == s._http && _admin == s._admin;
    }

    public override int GetHashCode()
    {
      return base.GetHashCode();
    }

    private string GetPortFor(DSEnvironment.DSTransport transport_)
    {
      switch (transport_)
      {
        case DSEnvironment.DSTransport.Ktcp:
          return _ktcp;
        case DSEnvironment.DSTransport.Tcp:
          return _tcp;
        case DSEnvironment.DSTransport.Http:
          return _http;
      }
      return null;
    }

    private bool TransportSupported(DSEnvironment.DSTransport transport_)
    {
      return GetPortFor(transport_) != null;
    }
  }
}
