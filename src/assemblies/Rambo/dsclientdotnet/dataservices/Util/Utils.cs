//---------------------------------------------------------------------------
// ($Id $)
//---------------------------------------------------------------------------
//
// Copyright (c) 2003 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
//---------------------------------------------------------------------------

using System;
using System.Xml;
using System.IO;

namespace MorganStanley.MSDesktop.Rambo.DataServices.Util
{
	/// <summary>
	/// Some static utility methods
	/// </summary>
	/// <author><a href="mailto:Kunle.Adetona@morganstanley.com">Kunle Adetona</a></author>
  public sealed class Utils
  {
		private Utils()
		{
		}

		/// <summary>
		/// throws System.ArgumentNullException is arg_ is null
		/// </summary>
		/// <param name="arg_"></param>
    public static void ArgumentNullException( Object arg_ )
    {
      if ( arg_ == null )
      {
        throw new ArgumentNullException(); 
      }
    }

		/// <summary>
		/// throws System.ArgumentNullException is arg_ is null
		/// </summary>
		/// <param name="argName_"></param>
		/// <param name="arg_"></param>
    public static void ArgumentNullException( String argName_, Object arg_ )
    {
      if ( arg_ == null )
      {
        throw new ArgumentNullException(argName_, "Argument '" + argName_ + "' cannot be null" );
      }
    }

		/// <summary>
		/// returns the first element child of given node
		/// </summary>
		/// <param name="node_"></param>
		/// <returns></returns>
    public static XmlElement GetFirstElementChild( XmlNode node_ )
    {
      if ( node_ != null )
      {
        foreach( XmlNode n in node_.ChildNodes )
        {
          if ( n.NodeType == XmlNodeType.Element )
          {
            return (XmlElement)n;
          }       
        }
      }
       return null;
    }

		/// <summary>
		/// returns the values of xpath.
		/// </summary>
		/// <param name="node_"></param>
		/// <param name="xpath_"></param>
		/// <returns></returns>
    public static String GetValue( XmlNode node_, String xpath_ )
    {
      if ( node_ != null )
      {
        XmlNode childNode = node_.SelectSingleNode(xpath_);
        if ( childNode != null )
        {
          return childNode.InnerText;
        }
      }
      return null;
    }
    
		/// <summary>
		/// Capitalises the first character of the given camel-cased string
		/// </summary>
		/// <param name="camelCasedString_"></param>
		/// <returns>A pascal-cased string</returns>
		public static string PascalCase( string camelCasedString_ )
		{
			if ( camelCasedString_ != null && camelCasedString_.Length > 0 )
			{
				char[] arr = camelCasedString_.ToCharArray();
				arr[0] = Char.ToUpper( arr[0] );
				return new String( arr );
			}
			return null;
		}

		/// <summary>
		/// sets the value of xpath, if it exists
		/// </summary>
		/// <param name="node_"></param>
		/// <param name="xpath_"></param>
		/// <param name="value_"></param>
    public static void SetValue( XmlNode node_, String xpath_, String value_ )
    {
      if ( node_ != null )
      {
        XmlNode childNode = node_.SelectSingleNode(xpath_);
        if ( childNode != null )
        {
          childNode.InnerText = value_;
        }
      }
    }

		/// <summary>
		/// returns a formatted xml string
		/// </summary>
		/// <param name="node_"></param>
		/// <returns></returns>
    public static string NodeToString( XmlNode node_ )
    {
      if ( node_ != null )
      {
        StringWriter sw = new StringWriter();       
        XmlTextWriter xtw = new XmlTextWriter( sw );
        xtw.Formatting = Formatting.Indented;
        xtw.Indentation = 4;
        node_.WriteTo( xtw  );
        return sw.ToString();
      }
      return null;
    }

		/// <summary>
		/// Extracts the user Id from an update Value XML Node
		/// </summary>
		/// <param name="node_">The Value xml Node for an update</param>
		/// <returns>The user Id extracted from the node</returns>
		public static string GetUserID(XmlElement valueElement_) 
		{
			string userID  = null;
			XmlNode updateUserIdNode = valueElement_.SelectSingleNode("UpdateUserId");
			if (updateUserIdNode != null)
				userID = updateUserIdNode.InnerText;

			// K.W 10/05/2004. Yes this is nasty but we need to go live and we need to
			// be able to detect vol surfaces saved by the current user.
			if ( userID == null || userID.Length == 0 )
			{
				XmlElement surfaceCreatorNode = valueElement_.SelectSingleNode( "Surface/Creator" ) as XmlElement;
				if ( surfaceCreatorNode != null )
					userID = surfaceCreatorNode.InnerText;
				else 
				{
					XmlElement lastUpdater = valueElement_.SelectSingleNode("GrowthSpread/LastUpdater") as XmlElement;
					if (lastUpdater != null) 
					{
						userID = lastUpdater.InnerText;
					}
				}
			}
      return userID;			
		}
	}
}
