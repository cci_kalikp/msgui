using System;

using MorganStanley.MSDotNet.MSLog;

namespace MorganStanley.MSDesktop.Rambo.DataServices.Util
{
  /// <summary>
  /// Convenience wrapper containing an MSLogLayer for a particular class.
  /// </summary>
  public class Logger
  {
    private string _class;
    private MSLogLayer _layer;

    /// <summary>
    /// Ctr. Creates a log layer by parsing the class' name and assembly version.
    /// </summary>
    public Logger(Type claz_)
    {
      String meta, proj, rel;
      rel = GetVersionString(claz_);
      GetMetaProject(claz_, out meta, out proj);
      _layer = new MSLogLayer(meta, proj, rel);
      _class = claz_.Name;
    }

    /// <summary>
    /// Enrich and send the given log message.
    /// </summary>
    /// <param name="msg_">Log message to send</param>
    /// <param name="method_">originating method name</param>
    /// <param name="text_">message text.</param>
    public void Log(MSLogMessage msg_, String method_, String text_)
    {
      msg_.Location(_class, method_)
        .SetLayer(_layer)
        .Append(text_)
        .SendQuietly();
    }

    /// <summary>
    /// Enrich and send the given log message.
    /// </summary>
    /// <param name="msg_">Log message to send</param>
    /// <param name="method_">originating method name</param>
    /// <param name="text_">message text.</param>
    /// <param name="ex_">exception details to output</param>
    public void Log(MSLogMessage msg_, String method_, String text_, Exception ex_)
    {
      String exStr = RecursiveErrorSearch(ex_);
      Log(msg_, method_, String.Format("{0}\nException Dump Follows...\n{1}", text_, exStr));
    }

    /// <summary>
    /// Returns true if a message at the pass priority will be logged
    /// </summary>
    /// <param name="priority_">The priority as a MSLogPriority enum</param>
    /// <returns>true if will log</returns>
    public bool WillLog(MSLogPriority priority_)
    {
      return MSLog.IsLoggable(_layer, priority_);
    }

    /// <summary>
    /// Returns true if a message at the pass priority will be logged.
    /// The passed string must be on of the values in MSLogPriority as
    /// it is parsed into one.
    /// </summary>
    /// <param name="priority_">The priority as a string</param>
    /// <returns>true if will log</returns>
    public bool WillLog(string priority_)
    {
      return MSLog.IsLoggable(_layer, (MSLogPriority) Enum.Parse(typeof(MSLogPriority), priority_, true));
    }

    /// <summary>
    /// Gets version information from the given class' assembly. Remember to set
    /// the correct version in your AssemblyInfo class!
    /// </summary>
    /// <param name="claz_">class for which the assembly is to be interrogated</param>
    /// <returns>Assembly version string</returns>
    public static String GetVersionString(System.Type claz_)
    {
      Version version = claz_.Assembly.GetName().Version;
      return (version.Build > 20)
        ? String.Format("{0}.{1}", version.Major, version.Minor)
        : String.Format("{0}.{1}.{2}", version.Major, version.Minor, version.Build);
    }

    /// <summary>
    /// Attempt to guess the MSDE meta and project from the class's namespace. Assumes
    /// the namespace is of the form "MorganStanley.&lt;meta&gt;.&lt;proj&gt;
    /// </summary>
    /// <param name="claz_">class whose namespace is to be used</param>
    /// <param name="meta_">output meta string</param>
    /// <param name="proj_">output proj string</param>
    public static void GetMetaProject(System.Type claz_, out string meta_, out string proj_)
    {
      String[] elements = claz_.Namespace.Split(new char[]{'.'}, 10);
      System.Diagnostics.Debug.Assert(elements.Length > 2 && elements[1] != null && elements[2] != null);
      meta_ = elements[1].ToLower();
      proj_ = elements[2].ToLower();
    }

    private static string RecursiveErrorSearch(Exception ex)
    {
      String msg = String.Format(
        "Exception type: {0}\n" +
        "Message:        {1}\n" + 
        "Stack Dump:     \n{2}",
        ex.GetType().FullName, ex.Message, ex.StackTrace);
      Exception inner = ex.InnerException;
      if (inner != null)
        msg += "Inner Exception Dump Follows...\n" + RecursiveErrorSearch(inner);
      return msg;
    }
  }
}
