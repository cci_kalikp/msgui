using System;
using System.Collections;

namespace MorganStanley.MSDesktop.Rambo.DataServices.Util
{
  /// <summary>
  /// A dictionary with keys of type Object and values of type ArrayList
  /// </summary>
  public class MultiMap: DictionaryBase
  {
    /// <summary>
    /// Initializes a new empty instance of the MultiMap class
    /// </summary>
    public MultiMap()
    {
      // empty
    }

    /// <summary>
    /// Gets or sets the ArrayList associated with the given Object
    /// </summary>
    /// <param name="key">
    /// The Object whose value to get or set.
    /// </param>
    public virtual ArrayList this[Object key]
    {
      get
      {
        return (ArrayList) this.Dictionary[key];
      }
      set
      {
        this.Dictionary[key] = value;
      }
    }

    /// <summary>
    /// Adds an element with the specified key and value to this MultiMap.
    /// </summary>
    /// <param name="key">
    /// The Object key of the element to add.
    /// </param>
    /// <param name="value">
    /// The ArrayList value of the element to add.
    /// </param>
    public virtual void Add(Object key, ArrayList value)
    {
      this.Dictionary.Add(key, value);
    }

    /// <summary>
    /// Determines whether this MultiMap contains a specific key.
    /// </summary>
    /// <param name="key">
    /// The Object key to locate in this MultiMap.
    /// </param>
    /// <returns>
    /// true if this MultiMap contains an element with the specified key;
    /// otherwise, false.
    /// </returns>
    public virtual bool ContainsKey(Object key)
    {
      return this.Dictionary.Contains(key);
    }

    /// <summary>
    /// Removes the element with the specified key from this MultiMap.
    /// </summary>
    /// <param name="key">
    /// The Object key of the element to remove.
    /// </param>
    public virtual void Remove(Object key)
    {
      this.Dictionary.Remove(key);
    }

    /// <summary>
    /// Gets a collection containing the keys in this MultiMap.
    /// </summary>
    public virtual System.Collections.ICollection Keys
    {
      get
      {
        return this.Dictionary.Keys;
      }
    }
  }
}
