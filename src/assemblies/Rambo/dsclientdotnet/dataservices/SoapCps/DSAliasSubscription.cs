using System;
using System.Xml;
using MorganStanley.MSDotNet.MSLog;
using MorganStanley.MSDesktop.Rambo.DataServices.Util;

namespace MorganStanley.MSDesktop.Rambo.DataServices.SoapCps
{
	/// <summary>
	/// Summary description for DSAliasSubscription.
	/// </summary>
  public class DSAliasSubscription : AbstractSubscription
  {
    /// <summary>
    /// Ths ISubscription on the underlying Alias dataservice
    /// </summary>
    private ISubscription _ulAliasSubscription;
    private DataEventHandler _onDataUpdate;

    private static Logger _logger = new Logger( typeof(DSAliasSubscription) );
    
    public DSAliasSubscription( SOAPCPSDataService dataService_, IAliasDataKey aliasDataKey_, string userId_) : base( dataService_, new IAliasDataKey[] { aliasDataKey_ }, userId_)
    {
      IDataService aliasDS = DataServiceManager.GetDataService("Aliases");
      // get the datakey on the Alias dataservice ( eg DataServiceName#DSKey#Alias
      IDataKey ulAliasDataKey = DataKeyFactory.BuildDataKey( SOAPCPSDataService.GetAliasDSKeyString( dataService_, aliasDataKey_ ) );
      // get the subscription for the underlying alias ds
      _ulAliasSubscription = aliasDS.CreateSubscription( ulAliasDataKey, userId_ );
    }

    public DSAliasSubscription( SOAPCPSDataService dataService_, IAliasDataKey aliasDataKey_) : this (dataService_, aliasDataKey_, DSEnvironment.USER_NAME)
    {
    }

    public override event DataEventHandler OnDataUpdate
    {
      add
      {
        if (_onDataUpdate == null)
        {
           _ulAliasSubscription.OnDataUpdate += new DataEventHandler( this.OnAliasDSDataUpdate );
        }

        _onDataUpdate += value ;
      }
      remove
      {
        _onDataUpdate -= value ;

        if (_onDataUpdate == null)
        {
          _ulAliasSubscription.OnDataUpdate -= new DataEventHandler( this.OnAliasDSDataUpdate );
        }
      }
    }

    protected override void FireDataUpdate( DataEvent event_ )
    {
      if ( _onDataUpdate != null && event_ != null )
      { 
        //give delegates a clone of the event, for thread-safety ??
        foreach ( DataEventHandler callback in _onDataUpdate.GetInvocationList() )
        {
          try
          {
            callback(this,(DataEvent)event_.Clone());
          }
          catch ( Exception ex )
          {
            _logger.Log(MSLog.Error(),"FireDataUpdate", "Error firing DataUpdate event to registered listener", ex );
          }
        }
      }
    }

    private void OnAliasDSDataUpdate( object dataService_, DataEvent evnt_ )
    {
      _logger.Log( MSLog.Debug(), "OnAliasDSDataUpdate", "Got update on AliasDataKey: " + this.m_keys[0].ToString() );
      try
      {
        // now get the  version data that corresponds to the alias update
        XMLDataObject xdo = evnt_.DataObject as XMLDataObject;
        DataServiceDate dt = new DataServiceDate(xdo.Value.SelectSingleNode("//VersionDataTime").InnerText);
        DataServiceDate ut = new DataServiceDate(xdo.Value.SelectSingleNode("//VersionUpdateTime").InnerText);
        string rawDsKey = xdo.Value.SelectSingleNode("//DsKey").InnerText;
        IVersionDataKey verDsKey = DataKeyFactory.BuildDataKey(rawDsKey, dt, ut, true);
        DataEvent evt = m_dataService.GetVersion( verDsKey );
        if (evt == null)
        {
          evt = new DataEvent(new XMLDataObject(verDsKey, (XmlElement) null));
        }

        FireDataUpdate( evt );
      }
      catch (Exception ex)
      {
        _logger.Log( MSLog.Error(), "OnAliasDSDataUpdate", ex.Message, ex);
      }
    }
  }
}
