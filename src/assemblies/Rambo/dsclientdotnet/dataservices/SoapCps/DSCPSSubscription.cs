using System;
using System.Xml;
using System.Text;
using System.Collections;
using System.Threading;
using MorganStanley.AppMW.CPS;
using MorganStanley.MSDotNet.MSLog;
using MorganStanley.MSDotNet.MSNet;
using MorganStanley.MSDesktop.Rambo.DataServices.Util;

namespace MorganStanley.MSDesktop.Rambo.DataServices.SoapCps
{
	/// <summary>
	/// Summary description for DSCPSSubscription.
	/// </summary>
	public class DSCPSSubscription : AbstractSubscription
	{
		protected string _cpsAddress;
    	protected string[] _cpsClientAddresses;
		protected string _cpsTopic;
		protected string _cpsFilter;

		internal CPSSubscriptionHandler _cpsSubscription;

		protected DataEventHandler _onDataUpdate;

		private static Logger _logger = new Logger( typeof(DSCPSSubscription) );

    #region Constructors

		/// <summary>
		/// Constructor for global subscription
		/// </summary>
		internal DSCPSSubscription( SOAPCPSDataService dataService_, string userId_ ) : base ( dataService_, userId_ )
		{
		}

		/// <summary>
		/// Constructor for collection subscription
		/// </summary>
		internal DSCPSSubscription( SOAPCPSDataService dataService_, IList keys_, string userId_ ) :base( dataService_, keys_, userId_ )
		{
		}

		/// <summary>
		/// Constructor for attribute subscription
		/// </summary>
		internal DSCPSSubscription( SOAPCPSDataService dataService_, IDictionary attributes_, string userId_, bool strictAttributeNames) :base( dataService_, attributes_, userId_, strictAttributeNames)
		{
		}

    #endregion

		protected virtual void Subscribe() 
		{
      if (!m_dataService.ServerSubFiltering)
			{
				_cpsSubscription = CPSSubscriberCache.Subscribe(_cpsAddress, _cpsClientAddresses, _cpsTopic, "",  m_dataService);
			}
			else 
			{
				_cpsSubscription = CPSSubscriberCache.Subscribe(_cpsAddress, _cpsClientAddresses, _cpsTopic, _cpsFilter, m_dataService);
			}
			_cpsSubscription.AddListener(new CPSMessageDelegate(this.OnCPSMessageCallback));
		}

		private void CPSInfoResponseCallback(IAsyncResult ar_) 
		{
      DSDataRequest dataRequest = ar_.AsyncState as DSDataRequest;
      try 
			{
				DSDataResponse dataResponse = dataRequest.EndSendRequest(ar_);
        if (dataResponse != null) 
        {
          if ( !dataResponse.HasCPSInfo )
            throw new DataServiceException("Error requesting CPS Config");

          _cpsAddress = dataResponse.CPSMasterAddress;
          _cpsClientAddresses = dataResponse.CPSClientAddresses;
          _cpsTopic = dataResponse.CPSTopic;
          _cpsFilter = dataResponse.CPSFilter;
          Subscribe();
        }
        else 
        {
          throw new DataServiceException("No response from async data request");
        }
			}
      catch (DSCPSException ex_) 
      {
        DataServiceManager.ReportStatus(new string[] {m_dataService.Id}, DSStatusType.CPSError, ex_.Message);
        _logger.Log(MSLog.Error(), "CPSInfoResponseCallback", "Couldn't connect to CPS as: ", ex_);
      }
			catch (Exception ex_) 
      {
        DataServiceManager.ReportStatus(new string[] {m_dataService.Id}, DSStatusType.AsyncConnectionFailed, "Couldn't connect to Data Services as: "+ex_.Message);
        _logger.Log(MSLog.Error(), "CPSInfoResponseCallback", "Couldn't connect to Data Services as: ", ex_);
			}
		}

    /// <summary>
    /// This is a synchronous version of event registration 
    /// When you register the OnDataUpdate event , it will first send out data service message to retrieve the cps info
    /// And in the callback , it will start to subscribe ds cps information. 
    /// This actually have a race condiction and break the standard .net event registration expectation because it might be possible
    /// that you lost some initial ds cps messages
    /// </summary>
    public event DataEventHandler OnSyncDataUpdate
    {
      add
      {
        if (_onDataUpdate == null)
        {
          if (!HasCPSInfo)
          {
            _logger.Log(MSLog.Info(), "SetupCPSInfo", "Attempting to retrieve CPS Config");

            DSDataRequest dataRequest;
            dataRequest = new DSDataRequest(m_dataService, this, DSDataRequest.RequestType.updates);
            var iAsyncResult = dataRequest.BeginSendRequest(new System.AsyncCallback(CPSInfoResponseCallback), dataRequest);
            dataRequest.EndSendRequest(iAsyncResult);
          }
          else
          {
            Subscribe();
          }
        }

        _onDataUpdate += value;
      }
      remove
      {
        _onDataUpdate -= value;

        if (_onDataUpdate == null && _cpsSubscription != null)
        {
          _cpsSubscription.RemoveListener(new CPSMessageDelegate(this.OnCPSMessageCallback));
          CPSSubscriberCache.UnSubscribe(_cpsSubscription, m_dataService);
          _cpsSubscription = null;
        }
      }
    }

		public override event DataEventHandler OnDataUpdate
		{
			add
			{
				if (_onDataUpdate == null)
				{
					if ( ! HasCPSInfo ) 
					{
						_logger.Log( MSLog.Info(), "SetupCPSInfo", "Attempting to retrieve CPS Config");

						DSDataRequest dataRequest;
						dataRequest = new DSDataRequest( m_dataService, this, DSDataRequest.RequestType.updates );
						dataRequest.BeginSendRequest(new System.AsyncCallback(CPSInfoResponseCallback), dataRequest);
					}
					else 
					{
						Subscribe();
					}
				}

				_onDataUpdate += value ;
			}
			remove
			{
				_onDataUpdate -= value ;

				if (_onDataUpdate == null && _cpsSubscription != null)
				{
					_cpsSubscription.RemoveListener(new CPSMessageDelegate(this.OnCPSMessageCallback));
					CPSSubscriberCache.UnSubscribe(_cpsSubscription, m_dataService);
					_cpsSubscription = null;
				}
			}
		}

		internal string CPSAddress
		{
			get { return _cpsAddress; }
			set { _cpsAddress = value; }
		}

    internal string[] CPSClientAddresses
    {
      get { return _cpsClientAddresses; }
      set { _cpsClientAddresses = value; }
    }

		internal string CPSTopic
		{
			get { return _cpsTopic; }
			set { _cpsTopic = value; }
		}

		internal string CPSFilter
		{
			get { return _cpsFilter; }
			set { _cpsFilter = value; }
		}

		internal bool HasCPSInfo
		{
			get { return (_cpsAddress != null && _cpsTopic != null && _cpsFilter != null); }
		}

		private void FireDataUpdateCallback(object state_) 
		{
			FireDataUpdate((DataEvent) state_);
		}

		protected override void FireDataUpdate( DataEvent event_ )
		{
			if ( _onDataUpdate != null && event_ != null )
			{ 
				//give delegates a clone of the event, for thread-safety ??
				foreach( DataEventHandler callback in _onDataUpdate.GetInvocationList() )
				{
					try
					{
						callback(this,(DataEvent)event_.Clone());
					}
					catch ( Exception ex )
					{
						_logger.Log(MSLog.Error(),"FireDataUpdate", "Error firing DataUpdate event to registered listener", ex );
					}
				}
			}
		}

		protected void OnCPSMessageCallback(object sender_, CPSMessage msg_ )
		{
      CPSSubscription subscription_ = (CPSSubscription) sender_;
			if (_cpsSubscription.Subscription == subscription_) // this should always be true
			{
				if ( _onDataUpdate == null) // then we should be unsubscribed from CPS
					return;

				string command = msg_.Header.GetField( CPSHeaderConstants.FIELD_COMMAND );	
				if ( command != CPSHeaderConstants.COMMAND_PUBLISH )
				{
					_logger.Log( MSLog.Info(), "CPSMessageCallback", "Ignoring command type: " + command );
					return;
				}

				string msgBody = msg_.GetBodyText(Encoding.Default);
                
				try
				{
					XmlDocument doc = new XmlDocument();			
					doc.LoadXml( msgBody );
					DataEvent dataEvent = BuildDataEvent( doc.DocumentElement );

          if (m_dataService.ServerSubFiltering)
					{
            if (_logger.WillLog(MSLogPriority.Info))
            {
              _logger.Log( MSLog.Info(), "CPSMessageCallback", "CPS[" + _cpsAddress + "]->Update: "+ msgBody );			
            }
						DataServiceManager.NetLoop.InvokeLaterWithState(new MSNetVoidObjectDelegate(FireDataUpdateCallback), dataEvent);
					}
					else 
					{
            ClientFilterCallback(dataEvent);
					}
				}
				catch( Exception ex )
				{
					_logger.Log( MSLog.Error(), "CPSMessageCallback", ex.Message, ex );
				}        
			}
			else
				_logger.Log( MSLog.Warning(), "OnCPSMessageEvent", "CPSSubscription on the callback does not match our CPSSubscription" );
		}

		/// <summary>
		/// Returns whether or not the object matches this subscription
		/// </summary>
		/// <param name="event_"></param>
		/// <returns></returns>
		private bool Accept( DataEvent event_ )
		{
			IDataObject obj_ = event_.DataObject ;
			if (m_type == SubType.GLOBAL)
			{
				return true;
			}
			else if (m_type == SubType.COLLECTION)
			{
				return (m_keys != null) ? m_keys.Contains(obj_.DataKey) : false;
			}
            else if ((m_type == SubType.ATTRIBUTE || m_type == SubType.STRICTATTRIBUTE) && m_attributes != null && m_attributes.Count > 0)
			{                   
				XMLDataObject xdo = (XMLDataObject)obj_;
				bool include = false;
				foreach ( Object key in m_attributes.Keys )
				{
					string xpath = key.ToString();
					XmlElement e = xdo.Value.SelectSingleNode(xpath) as XmlElement;
					include = ( e != null && String.Equals(m_attributes[key],e.InnerText) );						
					if ( !include )
					{
						break;
					}
				}         
				return include;
			}
			return false;
		}

		private void ClientFilterCallback(object state_) 
		{
			try 
			{
				DataEvent dataEvent = (DataEvent) state_;
				if (Accept(dataEvent))
				{
          if (_logger.WillLog(MSLogPriority.Info))
          {
            _logger.Log( MSLog.Info(), "ClientFilterCallback", "CPS[" + _cpsAddress + "]->Update: "+ ((XMLDataObject)dataEvent.DataObject).Value.InnerXml);
          }
					DataServiceManager.NetLoop.InvokeLaterWithState(new MSNetVoidObjectDelegate(FireDataUpdateCallback), dataEvent);
				}
				else 
				{
          if (_logger.WillLog(MSLogPriority.Debug))
          {
            _logger.Log( MSLog.Debug(), "ClientFilterCallback", "No Match CPS[" + _cpsAddress + "]->Update: "+ ((XMLDataObject)dataEvent.DataObject).Value.InnerXml);
          }
					dataEvent.ToString();
				}
			}
			catch (Exception ex_) 
			{
				_logger.Log(MSLog.Error(), "ClientFilterCallback", "Exception: "+ex_.ToString());
			}
		}

		/// <summary>
		/// Builds an XMLDataObject from the give xml element
		/// </summary>
		private DataEvent BuildDataEvent( XmlElement element_ )
		{
			XmlElement keyElement = element_.SelectSingleNode("Key") as XmlElement;
			XmlElement valueElement =  Utils.GetFirstElementChild ( element_.SelectSingleNode("Value") );

			if ( keyElement != null && valueElement != null )
			{
				string dsKeyStr = keyElement.InnerText;
				IDataKey dataKey = DataKeyFactory.BuildDataKey(dsKeyStr);

				/* //NBYRNE when gateway is rewritten then messages from persistor dataservices will need to be fired with IVersionDataKey
											XmlElement dataTimeElement = element_.SelectSingleNode("DataTime") as XmlElement;
											XmlElement updateTimeElement = element_.SelectSingleNode("UpdateTime") as XmlElement;

											if ( dataTimeElement == null && updateTimeElement == null)
											{
												dataKey = DataKeyFactory.BuildDataKey(dsKeyStr);

												// NBYRNE may want to keep this code
												/*
												if ( valueElement.HasAttribute("timestamp") )
												{
													try
													{            
														updateTime = Convert.ToDateTime( valueElement.GetAttribute("timestamp"), _dateTimeFormat );                        
													}
													catch( InvalidCastException ex )
													{
														m_log.Log(MSLog.Warning() "BuildDataEvent", ex.Message );
													}
												} 
												* /       
											}
											else
											{
												// NBYRNE could catch execeptions here
												DataServiceDate dataTime = new DataServiceDate(dataTimeElement.InnerText);
												DataServiceDate updateTime = new DataServiceDate(updateTimeElement.InnerText);

												dataKey = DataKeyFactory.BuildDataKey(dsKeyStr,dataTime,updateTime,true);
											}
							*/

				XMLDataObject xdo = new XMLDataObject( dataKey, valueElement );      

        string updateUserId = Util.Utils.GetUserID(element_.SelectSingleNode("Value") as XmlElement);
			
				// NBYRNE the timestamp of the Event could be set to the updatetime for version info
				return new DataEvent( xdo, updateUserId );
			}

      if (_logger.WillLog(MSLogPriority.Warning))
      {
        _logger.Log(MSLog.Warning(),"BuildDataEvent", "Could not build XMLDataObject from: " + Utils.NodeToString(element_) );
      }
      
			return null;
		}
	}
}
