using System;
using System.Collections;
using MorganStanley.MSDesktop.Rambo.DataServices.Util;
using MorganStanley.AppMW.CPS;
using MorganStanley.MSDotNet.MSLog;

namespace MorganStanley.MSDesktop.Rambo.DataServices.SoapCps
{
	/// <summary>
	/// Summary description for BufferedCPSKeySubscription.
	/// </summary>
	public class BufferedCPSKeySubscription : DSCPSSubscription
	{
    
    private static Logger _logger = new Logger( typeof(BufferedCPSKeySubscription) );

    /// <summary>
    /// Constructor for collection subscription
    /// </summary>
    internal BufferedCPSKeySubscription(BufferedSOAPCPSDataService dataService_, IList keys_, string userId_) :base(dataService_, keys_, userId_)
    {    
    }

    //CPSInfoResponseCallback
    public override event DataEventHandler OnDataUpdate
    {
      add
      {
        if (_onDataUpdate == null)
        {
          if ( ! HasCPSInfo ) 
          {
            _logger.Log( MSLog.Info(), "SetupCPSInfo", "Attempting to retrieve CPS Config");
            ((BufferedSOAPCPSDataService) m_dataService).BeginGetCPSInfo(this, new System.AsyncCallback(CPSInfoResponseCallback), null);
          }
          else 
          {
            Subscribe();
          }
        }

        _onDataUpdate += value ;
      }
      remove
      {
        _onDataUpdate -= value ;

        if (_onDataUpdate == null)
        {
          _cpsSubscription.RemoveListener(new CPSMessageDelegate(this.OnCPSMessageCallback));
          CPSSubscriberCache.UnSubscribe(_cpsSubscription, m_dataService);
          _cpsSubscription = null;
        }
      }
    }

    protected virtual void CPSInfoResponseCallback(IAsyncResult ar_)
    {
      try 
      {
        DSDataResponse dataResponse = ((BufferedSOAPCPSDataService) m_dataService).EndGetCPSInfo(ar_);
        if (dataResponse != null) 
        {
          if ( !dataResponse.HasCPSInfo )
            throw new DataServiceException("Error requesting CPS Config");

          _cpsAddress = dataResponse.CPSMasterAddress;
          _cpsClientAddresses = dataResponse.CPSClientAddresses;
          _cpsTopic = dataResponse.CPSTopic;
          _cpsFilter = dataResponse.CPSFilter;
          Subscribe();
        }
        else 
        {
          throw new DataServiceException("No response from async data request");
        }
      }
      catch (DSCPSException ex_) 
      {
        DataServiceManager.ReportStatus(new string[] {m_dataService.Id}, DSStatusType.CPSError, ex_.Message);
        _logger.Log(MSLog.Error(), "CPSInfoResponseCallback", "Couldn't connect to CPS as: ", ex_);
      }
      catch (Exception ex_) 
      {
        DataServiceManager.ReportStatus(new string[] {m_dataService.Id}, DSStatusType.AsyncConnectionFailed, "Couldn't connect to Data Services as: "+ex_.Message);
        _logger.Log(MSLog.Error(), "CPSInfoResponseCallback", "Couldn't connect to Data Services as: ", ex_);
      }
    }
    
	}
}
