using System;
using System.IO;
using System.Xml;
using MorganStanley.MSDotNet.MSSoap.Common;

namespace MorganStanley.MSDesktop.Rambo.DataServices.SoapCps
{
	/// <summary>
	/// Summary description for DSVersionDatesRequest.
	/// </summary>
	public class DSVersionDatesRequest : DSSoapRequest
	{
    public DSVersionDatesRequest(IDataService dataService_, IDataKey key_, bool sort_ ) : base(dataService_)
    {
      CreateVersionDatesRequestEnvelope(key_, sort_);
    }

    public DSVersionDatesResponse SendRequest()
    {
      ISoapEnvelope responseEnvelope = base.InternalSendRequest();
      DSVersionDatesResponse response = new DSVersionDatesResponse(responseEnvelope);
      return response;
    }

    private void CreateVersionDatesRequestEnvelope(IDataKey key_, bool sort_ )
    {
      StringWriter sw = new StringWriter();
      XmlWriter xmlWriter = new XmlTextWriter(sw);
      xmlWriter.WriteStartDocument();
      xmlWriter.WriteStartElement("DSVersionDatesRequest");			
      xmlWriter.WriteElementString( "DataService", _dataService.Id );
      xmlWriter.WriteElementString( "Key", key_.ToString() );
      xmlWriter.WriteElementString( "Sort", sort_.ToString() );            	
      xmlWriter.WriteEndDocument();
      xmlWriter.Close();			
      xmlWriter.Close();			
      XmlDocument doc = new XmlDocument();
      doc.LoadXml( sw.ToString() );
      
      base.ModifySoapAction(doc.DocumentElement);
    }

	}
}
