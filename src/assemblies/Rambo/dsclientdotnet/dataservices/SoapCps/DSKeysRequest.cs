using System;
using System.IO;
using System.Xml;
using System.Collections;
using MorganStanley.MSDotNet.MSSoap.Common;

namespace MorganStanley.MSDesktop.Rambo.DataServices.SoapCps
{
	/// <summary>
	/// Summary description for DSVersionKeysRequest.
	/// </summary>
	public class DSKeysRequest : DSSoapRequest
	{
		public DSKeysRequest(IDataService dataService_) : this(dataService_,null,null)
		{
    }

    public DSKeysRequest(IDataService dataService_,DataServiceDate startDataTime_, DataServiceDate endDataTime_) : base(dataService_)
    {
      CreateKeysSoapEnvelope(null, null, startDataTime_, endDataTime_);
    }

    public DSKeysRequest(IDataService dataService_, IDictionary attributes_ ) : this(dataService_,attributes_,null,null)
    {
    }

    public DSKeysRequest(IDataService dataService_, IDictionary attributes_,DataServiceDate startDataTime_, DataServiceDate endDataTime_) : base(dataService_)
    {
      CreateKeysSoapEnvelope(attributes_, null,startDataTime_,endDataTime_);
    }

    public DSKeysRequest(IDataService dataService_, IDataKey key_) : this(dataService_,key_,null,null)
		{
		}

    public DSKeysRequest(IDataService dataService_, IDataKey key_,DataServiceDate startDataTime_, DataServiceDate endDataTime_)  : base(dataService_)
    {
      CreateKeysSoapEnvelope(null, key_,startDataTime_,endDataTime_);
    }
    
    public DSKeysResponse SendRequest()
    {
      ISoapEnvelope responseEnvelope = base.InternalSendRequest();
      DSKeysResponse response = new DSKeysResponse(responseEnvelope);
      return response;
    }

    public void CreateKeysSoapEnvelope(IDictionary attributes_, IDataKey key_, DataServiceDate startDataTime_, DataServiceDate endDataTime_)
    {
      StringWriter sw = new StringWriter();
      XmlWriter xmlWriter = new XmlTextWriter(sw);
      xmlWriter.WriteStartDocument();
      xmlWriter.WriteStartElement("DSKeyRequest");			
      xmlWriter.WriteElementString("DataService", _dataService.Id);

			if ( attributes_ != null )
			{
				foreach ( object key in attributes_.Keys )
				{
					xmlWriter.WriteStartElement("Attribute");        
					xmlWriter.WriteAttributeString("name", key.ToString());
					xmlWriter.WriteAttributeString("value", attributes_[key].ToString());
					xmlWriter.WriteEndElement();
				}
			}
			else if (key_ != null) 
			{
				xmlWriter.WriteElementString("Key", key_.DSKey);
			}

      if (startDataTime_ != null)
        xmlWriter.WriteElementString("StartDataTime", startDataTime_.ToString());

      if (startDataTime_ != null)
        xmlWriter.WriteElementString("EndDataTime", endDataTime_.ToString());

      xmlWriter.WriteEndDocument();
      xmlWriter.Close();			
      XmlDocument doc = new XmlDocument();
      doc.LoadXml(sw.ToString());
			
      base.ModifySoapAction(doc.DocumentElement);
    }
	}
}
