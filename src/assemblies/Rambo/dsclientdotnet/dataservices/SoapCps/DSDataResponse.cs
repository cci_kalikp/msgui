using System;
using System.Xml;
using System.Collections;
using MorganStanley.MSDotNet.MSSoap.Common;

namespace MorganStanley.MSDesktop.Rambo.DataServices.SoapCps
{
  /// <summary>
  /// Summary description for DSDataResponse.
  /// </summary>
  public class DSDataResponse : DSSoapResponse
  {
    private string _cpsMasterAddress;
    private string _cpsTopic;
    private string _cpsFilter;
    private string[] _cpsClientAddresses;
    private ArrayList _xmlDataObjects = new ArrayList();

    public DSDataResponse(ISoapEnvelope responseEnv_): this(((SoapBodyDomImpl) responseEnv_.Body).XmlElement)
    {
    }

    internal DSDataResponse(XmlElement responseBodyXml_) : base(responseBodyXml_)
    {
      XmlNode cpsInfoNode = ResponseBodyXml.SelectSingleNode("//UpdateInstructions/CPS");
      if (cpsInfoNode != null)
      {
        string host = cpsInfoNode.SelectSingleNode("Host").InnerText;
        string port = cpsInfoNode.SelectSingleNode("Port").InnerText;				
        _cpsMasterAddress = host + ":" + port;
        _cpsTopic = cpsInfoNode.SelectSingleNode("Topic").InnerText;
        _cpsFilter = cpsInfoNode.SelectSingleNode("Subscription").InnerText;
        if (cpsInfoNode.SelectNodes("ClientCPSs/CPS") != null)
        {
          ArrayList clientCPSs = new ArrayList();
          foreach (XmlElement cps in cpsInfoNode.SelectNodes("ClientCPSs/CPS"))
          {
            clientCPSs.Add(cps.InnerText);            
          }
          _cpsClientAddresses = clientCPSs.ToArray(typeof(string)) as string[];
        }
      }

      XmlNodeList itemNodes = ResponseBodyXml.SelectNodes("//Snapshot/Item");
      if (itemNodes != null)
      {
        foreach( XmlElement itemElement in itemNodes )
        {       
          XmlElement keyElement = itemElement.SelectSingleNode("Key") as XmlElement;
          XmlElement valueElement =  itemElement.SelectSingleNode("Value").FirstChild as XmlElement;
          if ( keyElement != null && valueElement != null )
          {
            IDataKey dataKey = new StringDataKey( keyElement.InnerText );
            _xmlDataObjects.Add( new XMLDataObject( dataKey, valueElement ) );
          }
        }
      }	
    }

    internal DSDataResponse(DSDataResponse response_) : this(response_.ResponseBodyXml)
    {
    }

    public bool HasCPSInfo
    {
      get { return _cpsMasterAddress != null; }
    }

    public string CPSMasterAddress
    {
      get { return _cpsMasterAddress; }
    }

    public string[] CPSClientAddresses
    {
      get { return _cpsClientAddresses; }
    }

    public string CPSTopic
    {
      get { return _cpsTopic; }
    }

    public string CPSFilter
    {
      get { return _cpsFilter; }
      set { _cpsFilter = value; }
    }

    public XMLDataObject[] DataObjects
    {
      get
      {
        XMLDataObject[] xmlDataObjArr = new XMLDataObject[ _xmlDataObjects.Count ];
        _xmlDataObjects.CopyTo(xmlDataObjArr);
        return xmlDataObjArr;
      }
    }
  }
}
