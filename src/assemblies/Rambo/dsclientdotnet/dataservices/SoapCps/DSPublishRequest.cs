using System;
using System.IO;
using System.Xml;
using System.Collections;

using MorganStanley.MSDotNet.MSSoap.Common;

namespace MorganStanley.MSDesktop.Rambo.DataServices.SoapCps
{
	/// <summary>
	/// Summary description for DSPublishRequest.
	/// </summary>
	public class DSPublishRequest : DSSoapRequest
	{

		public DSPublishRequest(IDataService dataService_, XMLDataObject xdo_) : base(dataService_)
		{
      // could do single as multipublish with one element
      CreateSinglePublishSoapEnvelope(xdo_);
		}

    public DSPublishRequest(IDataService dataService_, IList xdos_) : base(dataService_)
    {
      CreateMultiplePublishSoapEnvelope(xdos_);
    }

    public DSPublishResponse SendRequest()
    {
      ISoapEnvelope responseEnvelope = base.InternalSendRequest();
      DSPublishResponse response = new DSPublishResponse(responseEnvelope);
      return response;
    }

    private void CreateSinglePublishSoapEnvelope(XMLDataObject xdo_)
    {
      StringWriter sw = new StringWriter();
      XmlWriter xmlWriter = new XmlTextWriter(sw);
      xmlWriter.WriteStartDocument();
      xmlWriter.WriteStartElement("DSPublishRequest");			
      xmlWriter.WriteElementString("DataService", _dataService.Id);
      xmlWriter.WriteElementString("Key", xdo_.DataKey.DSKey);

      if (xdo_.DataKey is IVersionDataKey)
      {
        IVersionDataKey versionKey = xdo_.DataKey as IVersionDataKey;
        xmlWriter.WriteElementString( "DataTime", versionKey.DataTime.ToString() );
        xmlWriter.WriteElementString( "UpdateTime", versionKey.UpdateTime.ToString() );
      }

      xmlWriter.WriteElementString( "GenNewUpdateId", "true" );      
      xmlWriter.WriteStartElement("Value");   

/* NBYRNE invesigate
      XmlElement userId = element_.OwnerDocument.CreateElement("UpdateUserId");
      userId.InnerText = Constants.USER_NAME;
      element_.AppendChild( userId );
*/      
      xdo_.Value.WriteTo(xmlWriter);     		
      
      xmlWriter.WriteEndDocument();
      xmlWriter.Close();			
      XmlDocument doc = new XmlDocument();
      doc.LoadXml(sw.ToString());
			
      base.ModifySoapAction(doc.DocumentElement);      
    }

    private void CreateMultiplePublishSoapEnvelope(IList xdos_)
    {
      StringWriter sw = new StringWriter();
      XmlWriter xmlWriter = new XmlTextWriter(sw);
      xmlWriter.WriteStartDocument();
      xmlWriter.WriteStartElement("DSPublishRequest");			
      xmlWriter.WriteElementString( "DataService", _dataService.Id );      		
      xmlWriter.WriteElementString( "GenNewUpdateId", "true" );

      foreach( XMLDataObject xdo in xdos_ )
      {
        xmlWriter.WriteStartElement("Item");
        xmlWriter.WriteElementString( "Key", xdo.DataKey.ToString() );	

        if (xdo.DataKey is IVersionDataKey)
        {
          IVersionDataKey versionKey = xdo.DataKey as IVersionDataKey;
          xmlWriter.WriteElementString( "DataTime", versionKey.DataTime.ToString() );
          xmlWriter.WriteElementString( "UpdateTime", versionKey.UpdateTime.ToString() );
        }
        
        xmlWriter.WriteStartElement("Value");
        /* NBYRNE invesigate
              XmlElement userId = element_.OwnerDocument.CreateElement("UpdateUserId");
              userId.InnerText = Constants.USER_NAME;
              element_.AppendChild( userId );
        */  
        xdo.Value.WriteTo(xmlWriter);
        xmlWriter.WriteEndElement();
        xmlWriter.WriteEndElement();
      }
      	
      xmlWriter.WriteEndDocument();
      xmlWriter.Close();			
      XmlDocument doc = new XmlDocument();
      doc.LoadXml( sw.ToString() );

      base.ModifySoapAction(doc.DocumentElement);      
    }
	}
}
