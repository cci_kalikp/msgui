using System;
using System.Xml;
using System.Collections;
using MorganStanley.MSDesktop.Rambo.DataServices.Util;
using MorganStanley.MSDotNet.MSSoap.Common;
using MorganStanley.MSDotNet.MSLog;


namespace MorganStanley.MSDesktop.Rambo.DataServices.SoapCps
{
	/// <summary>
	/// Summary description for DSVersionDatesResponse.
	/// </summary>
	public class DSVersionDatesResponse : DSSoapResponse
	{
    private static Logger m_log = new Logger( typeof(DSVersionDatesResponse) );

    private VersionDataKeyCollection _versionDataKeys = new VersionDataKeyCollection();

    public DSVersionDatesResponse(ISoapEnvelope responseEnv_) : base(responseEnv_)
    {
      XmlNodeList xmlVersionKeys = ResponseBodyXml.SelectNodes("//Versions/Version");

      if (xmlVersionKeys != null)
      {
        string keyStr = ResponseBodyXml.SelectSingleNode("//Key").InnerText;

        for ( int i = 0; i < xmlVersionKeys.Count; ++i )
        {
          try
          {
            XmlElement element = xmlVersionKeys.Item(i) as XmlElement;
            if ( element.HasAttribute("dataTime") && element.HasAttribute("updateTime")) // required attributes
            {
              DataServiceDate dataTime = new DataServiceDate(element.GetAttribute("dataTime"));
              DataServiceDate updateTime = new DataServiceDate(element.GetAttribute("updateTime"));
              string userName = element.GetAttribute("userName");
              _versionDataKeys.Add( DataKeyFactory.BuildDataKey(keyStr, dataTime, updateTime,true, userName));
            }
            else
              m_log.Log(MSLog.Warning(),"DSVersionDatesResponse","Required attribute 'dataTime' or 'updateTime' missing from version info");
          }
          catch( Exception ex )
          {
            m_log.Log(MSLog.Warning(), "DSVersionDatesResponse", ex.Message );
          }
        }
      }
    }

    public VersionDataKeyCollection VersionDataKeys
    {
      get { return (_versionDataKeys.Count!=0) ? _versionDataKeys : null; }
    }
	}
}
