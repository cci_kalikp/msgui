//---------------------------------------------------------------------------
// ($Id $)
//---------------------------------------------------------------------------
//
// Copyright (c) 2003 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
//---------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Text;
using System.Collections;
using System.Runtime.CompilerServices;
using System.Globalization;
using System.Threading;
using MorganStanley.MSDotNet.MSNet;
using MorganStanley.MSDotNet.MSLog;
using MorganStanley.MSDesktop.Rambo.DataServices.Util;
using MorganStanley.MSDesktop.Rambo.DataServices.SoapCps;

namespace MorganStanley.MSDesktop.Rambo.DataServices
{
  /// <summary>
  /// SOAPCPSDataService gets snapshots and publishes updates via SOAP.  It listens for dynamic updates on CPS.
  /// SOAPCPSDataService is stateless.
  /// </summary>
  public class SOAPCPSDataService : AbstractDataService, IXMLDataService 
  {
    private Logger _logger = new Logger( typeof(SOAPCPSDataService) );
    private string _schema;    

    private long _timeout = -2;

    protected ManualResetEvent _waitingEvnt;
    protected ServerList _servers;

    /// <summary>
    /// Public constructor
    /// </summary>
    /// <exception cref="System.ArgumentException">If the id_ parameters is null</exception>
    /// <param name="id_">DataService Name</param>
    public SOAPCPSDataService( string id_, ServerList servers_ ) : base(id_) 
    {						     
      DataServices.Util.Utils.ArgumentNullException( "DataService Id", id_ );
			_servers = servers_;

      _logger.Log(MSLog.Info(), "SOAPCPSDataService.ctr()", "SOAPCPSDataService[" + id_ + "]");
    }		

    [Obsolete("This constructor is redundant",false)]  
    public SOAPCPSDataService( string id_,  String[] serverArr_, IMSNetLoop loop_, ServerList servers_ ) : this(id_, servers_) 
    {						     
			_servers = servers_;
		}				

    [Obsolete("This constructor is redundant",false)]  
    public SOAPCPSDataService( string id_,  String[] serverArr_, IMSNetLoop loop_, bool initSub_, ServerList servers_) : this(id_, servers_)
    {						      
			_servers = servers_;
		}

    ~SOAPCPSDataService()
    {
      _logger.Log(MSLog.Info(), "~SOAPCPSDataService()", "SOAPCPSDataService[" + Id + "]");
    }

    internal void GetServers(ServerManager serverManager_)
    {
      lock (this)
      {
        if (_waitingEvnt == null)
        {
          _waitingEvnt = new ManualResetEvent(false);
          serverManager_.BeginGetServers(this.Id, new AsyncCallback(GetServersCallback), serverManager_);
        }
      }
    }

    private void GetServersCallback(IAsyncResult ar_)
    {
      ServerManager sm = ar_.AsyncState as ServerManager;
      try
      {
        _servers = sm.EndGetServers(ar_);
      }
      catch (Exception ex_)
      {
        _logger.Log(MSLog.Error(), "GetServersCallback", "Error retrieving serverlist from ServerManager: ", ex_);
      }
      lock (this)
      {
        if (_waitingEvnt != null)
        {
          _waitingEvnt.Set();
          _waitingEvnt = null;
        }
      }
    }

		public override SoapCps.ServerList Servers 
		{
			get 
      {
        ManualResetEvent evnt = _waitingEvnt;
        if (evnt != null)
        {
          evnt.WaitOne();
        }
        return _servers;
      }
		}

    public override void refreshServerList()
    {
      _servers = DataServiceManager.ServerManager.GetServers(m_id);
    }

    private Dictionary<IDataKey, DataEvent> GetSnapshot( DSAliasSubscription subscription_ )
    {
      IAliasDataKey aliasKey = (IAliasDataKey) subscription_.DataKeys[0];
      IVersionDataKey aliasVersionKey = GetVersionDataKey(aliasKey);

      DataEvent dataEvt = this.GetVersion( aliasVersionKey );
      Dictionary<IDataKey, DataEvent> dict = new Dictionary<IDataKey, DataEvent>();
      dict[ aliasKey ] = dataEvt;

      return dict;
    }

    private Dictionary<IDataKey, DataEvent> GetSnapshot( VersionSubscription subscription_ )
    {
      var dict = new Dictionary<IDataKey, DataEvent>(subscription_.DataKeys.Count);
      foreach (IVersionDataKey verionKey in subscription_.DataKeys)
      {
        DataEvent evt = this.GetVersion(verionKey);
        dict[ verionKey ] = evt;
      }

      return dict;
    }

    [Obsolete("Use GetSnapshots() instead")]
    public override IDictionary GetSnapshot(ISubscription subscription_)
    {
      return GetSnapshotsPrivate(subscription_);
    }

    public override IDictionary<IDataKey, DataEvent> GetSnapshots(ISubscription subscription_)
    {
      return GetSnapshotsPrivate(subscription_);
    }

    private Dictionary<IDataKey, DataEvent> GetSnapshotsPrivate(ISubscription subscription_)
    {
      if (subscription_ is DSAliasSubscription)
      {
        return GetSnapshot((DSAliasSubscription)subscription_);
      }
      else if (subscription_ is VersionSubscription)
      {
        return GetSnapshot((VersionSubscription)subscription_);
      }
      else
      {
        DSCPSSubscription sub = subscription_ as DSCPSSubscription;
        if (null != sub)
        {
          if (!Object.ReferenceEquals(this, subscription_.DataService))
            throw new SubscriptionException("I did not create this subscription.");

          DSDataRequest dataRequest = new DSDataRequest(this, sub, sub.HasCPSInfo ? DSDataRequest.RequestType.snapshot : DSDataRequest.RequestType.all);
          DSDataResponse dataResponse = dataRequest.SendRequest(Timeout);

          if (dataResponse.HasCPSInfo)
          {
            sub.CPSAddress = dataResponse.CPSMasterAddress;
            sub.CPSClientAddresses = dataResponse.CPSClientAddresses;
            sub.CPSTopic = dataResponse.CPSTopic;
            sub.CPSFilter = dataResponse.CPSFilter;
          }

          XMLDataObject[] xdoArr = dataResponse.DataObjects;

          var events = new Dictionary<IDataKey, DataEvent>(xdoArr.Length);
          foreach (XMLDataObject xdo in xdoArr)
          {
            string userId = Util.Utils.GetUserID(xdo.Value);
            DataEvent dataEvent = new DataEvent(xdo, userId);
            events.Add(dataEvent.DataKey, dataEvent);
          }
          return events;
        }
        else
          throw new SubscriptionException("Wrong subscription type");
      }
    }

    public override IDataKey[] Publish( IList dataObjects_ )
    {
      DSPublishRequest publishRequest = new DSPublishRequest(this, dataObjects_);
      DSPublishResponse publishResponse = publishRequest.SendRequest();
      return publishResponse.FailedPublishKeys;
    }

    /// <summary>
    /// for publishing XMLDataObjects
    /// </summary>
    /// <param name="obj_"></param>
    public override void Publish( IDataObject obj_ )
    {      
      XMLDataObject dataObject = obj_ as XMLDataObject;

      if ( dataObject == null ) 
        throw new PublishException("Message should be an XMLDataObject: " + obj_ );

      DSPublishRequest publishRequest = new DSPublishRequest(this, dataObject);
      publishRequest.SendRequest();
    }

    public override ISubscription CreateSubscription( IDictionary attributes_, string userId_ )
    {
      return new DSCPSSubscription( this, attributes_, userId_, false);
    }

    public override ISubscription CreateSubscription(IDictionary attributes_, string userId_, bool strictAttributeNames)
    {
        return new DSCPSSubscription(this, attributes_, userId_, strictAttributeNames);
    }

    public override ISubscription GlobalSubscription( string userId_ )
    {  
      return new DSCPSSubscription( this, userId_ );
    }

    public override ISubscription CreateSubscription( IList keys_, string userId_ )
    {
      if (keys_ is VersionDataKeyCollection)
        return new VersionSubscription(this, (VersionDataKeyCollection)keys_, userId_);
      else
        return new DSCPSSubscription( this, keys_, userId_ );
    }
   
    public override ISubscription CreateSubscription( IDataKey key_, string userId_ )
    {
      if ( key_ is IAliasDataKey)
        return new DSAliasSubscription( this, (IAliasDataKey)key_, userId_ );
      else if ( key_ is IVersionDataKey)
        return new VersionSubscription( this, (IVersionDataKey)key_, userId_ );
      else
        return base.CreateSubscription( key_, userId_ );
    }
		
    public virtual string Schema
    {
      get
      {
        if (_schema==null)
        {
          DSSchemaRequest schemaRequest = new DSSchemaRequest(this);
          DSSchemaResponse schemaResponse = schemaRequest.SendRequest();
          _schema = schemaResponse.Schema;
        }
        return _schema;
      }
    }

    #region GetDataKey operations

    public override IDataKey[] GetDataKeys()
    {
      DSKeysRequest keysRequest = new DSKeysRequest(this);
      DSKeysResponse keysResponse = keysRequest.SendRequest();
      return keysResponse.DataKeys;
    }

    public override IDataKey[] GetDataKeys( DataServiceDate startDataTime_, DataServiceDate endDataTime_ )
    {
      DSKeysRequest keysRequest = new DSKeysRequest(this,startDataTime_,endDataTime_);
      DSKeysResponse keysResponse = keysRequest.SendRequest();
      return keysResponse.DataKeys;
    }

    public override IDataKey[] GetDataKeys( IDictionary attributes_ )
    {
      DSKeysRequest keysRequest = new DSKeysRequest(this, attributes_ );
      DSKeysResponse keysResponse = keysRequest.SendRequest();
      return keysResponse.DataKeys;
    }

    public override IDataKey[] GetDataKeys( IDictionary attributes_, DataServiceDate startDataTime_, DataServiceDate endDataTime_ )
    {
      DSKeysRequest keysRequest = new DSKeysRequest(this, attributes_, startDataTime_, endDataTime_ );
      DSKeysResponse keysResponse = keysRequest.SendRequest();
      return keysResponse.DataKeys;
    }

    public override IDataKey[] GetDataKeys( IDataKey key_ )
    {
      DSKeysRequest keysRequest = new DSKeysRequest(this, key_ );
      DSKeysResponse keysResponse = keysRequest.SendRequest();
      return keysResponse.DataKeys;
    }

    public override IDataKey[] GetDataKeys( IDataKey key_, DataServiceDate startDataTime_, DataServiceDate endDataTime_ )
    {
      DSKeysRequest keysRequest = new DSKeysRequest(this, key_, startDataTime_, endDataTime_ );
      DSKeysResponse keysResponse = keysRequest.SendRequest();
      return keysResponse.DataKeys;
    }

    #endregion

		/// <summary>
		/// Returns null if there are no versions in the passed collection
		/// (this is to mirror the java and C++ behaviour)
		/// </summary>
		/// <param name="aliasDS_"></param>
		/// <param name="key_"></param>
		/// <param name="versionAliasDataKeys_"></param>
		/// <returns>list of IVersionDataKey object or null if none</returns>
		private VersionDataKeyCollection BuildVersionsList(IDataService aliasDS_, IAliasDataKey key_, VersionDataKeyCollection versionAliasDataKeys_)
		{
			VersionDataKeyCollection versions = new VersionDataKeyCollection();
			foreach (IVersionDataKey versionedAliasDataKey in versionAliasDataKeys_)
			{
				DataEvent dataEvt = aliasDS_.GetVersion(versionedAliasDataKey);
				if (dataEvt != null)
				{
					XMLDataObject xdo = dataEvt.DataObject as XMLDataObject;
					DataServiceDate dt = new DataServiceDate(xdo.Value.SelectSingleNode("//VersionDataTime").InnerText);
					DataServiceDate ut = new DataServiceDate(xdo.Value.SelectSingleNode("//VersionUpdateTime").InnerText);
					IVersionDataKey verDsKey = DataKeyFactory.BuildDataKey(key_.DSKey, dt, ut, true);
					versions.Add( verDsKey );
				}
				else
				{
					_logger.Log(MSLog.Warning(), "GetVersionList", "AliasDS.GetVersionList( " + " ) returned a VerKey but AliasDS.GetVersion( " + versionedAliasDataKey.ToString() + " ) returned no data - Ignoring" );
				}
			}
			return (versions.Count != 0) ? versions : null;
		}

    // NBYRNE - Investigating if this could just be done with attribute subscription on Alias dataservice
    private VersionDataKeyCollection GetVersionList( IAliasDataKey key_, bool sort_ )
    {
      IDataService aliasDS = DataServiceManager.GetDataService("Aliases");
      
      string dsKeyStr = SOAPCPSDataService.GetAliasDSKeyString( this, key_ );

      VersionDataKeyCollection versionedAliasDataKeys = aliasDS.GetVersionsList( new StringDataKey( dsKeyStr ), sort_);
      
			return BuildVersionsList(aliasDS, key_, versionedAliasDataKeys);
    }

    public override VersionDataKeyCollection GetVersionsList ( IDataKey key_, bool sort_ )
    {
      if ( key_ is IAliasDataKey)
        return GetVersionList( (IAliasDataKey)key_, sort_ );

      DSVersionDatesRequest versionDatesRequest = new DSVersionDatesRequest(this, key_,sort_);
      DSVersionDatesResponse versionDatesResponse = versionDatesRequest.SendRequest();
      return versionDatesResponse.VersionDataKeys;
    }

		private VersionDataKeyCollection GetVersionsList(AliasDataKey key_, DataServiceDate startDataTime_, DataServiceDate endDataTime_) 
		{
      if (_logger.WillLog(MSLogPriority.Info))
      {
        _logger.Log(MSLog.Info(),"GetVersionsList", Id + ": Trying to get the version list for the alias key " + key_.KeyString);
        if (startDataTime_ != null)
        {
          _logger.Log(MSLog.Info(),"GetVersionsList", Id + ": Getting versions after and including the date: " + startDataTime_.ToString());
        }
        if (endDataTime_ != null) 
        {
          _logger.Log(MSLog.Info(),"GetVersionsList", Id + ": Getting versions before and including the date: " + endDataTime_.ToString());
        }
      }
			if (startDataTime_!= null && endDataTime_ != null && startDataTime_.Date.Ticks >= endDataTime_.Date.Ticks)
			{
				throw new SubscriptionException("The start date must be before the end date");
			}
			IDataService aliasDS = DataServiceManager.GetDataService("Aliases");
			String dsKeyStr = Id + "#" + key_.DSKey + "#" + key_.Alias;
			VersionDataKeyCollection versionAliasKeys = aliasDS.GetVersionsList(DataKeyFactory.BuildDataKey(dsKeyStr), startDataTime_, endDataTime_);
			VersionDataKeyCollection versions = BuildVersionsList(aliasDS, key_, versionAliasKeys); 
      if (_logger.WillLog(MSLogPriority.Info))
      {
        if (versions == null)
        {
          _logger.Log(MSLog.Info(),"GetVersionsList", Id + ": No versions returned");
        }
        else
        {
          _logger.Log(MSLog.Info(),"GetVersionsList", Id + ": Found " + versions.Count + " distinct versions");
        }
      }
			return versions;
		}

		public override VersionDataKeyCollection GetVersionsList(IDataKey key_, DataServiceDate startDataTime_, DataServiceDate endDataTime_) 
		{
			//If both the times are null, call the normal getVersionsList
			if (startDataTime_ == null && endDataTime_ == null)
			{
				_logger.Log(MSLog.Info(),"GetVersionsList", Id + ": Trying to get a versions list range with no start or end date - called getVersionsList with no dates");
				return GetVersionsList(key_, true);
			}
			//If we have an AliasDataKey call the private getVersionsList
			if (key_.GetType() ==  typeof(AliasDataKey)) 
			{
				return GetVersionsList((AliasDataKey) key_, startDataTime_, endDataTime_);
			}
      if (_logger.WillLog(MSLogPriority.Info))
      {
        _logger.Log(MSLog.Info(), "GetVersionsList", Id + ": Trying to get the version list for the key " + key_.DSKey);
        if (startDataTime_ != null) 
        {
          _logger.Log(MSLog.Info(), "GetVersionsList", Id + ": Getting versions after and including the date: " + startDataTime_.ToString());
        }
        if (endDataTime_ != null) 
        {
          _logger.Log(MSLog.Info(), "GetVersionsList", Id + ": Getting versions before and including the date: " + endDataTime_.ToString());
        }
      }
			//Throw an error if the start time is after the end time as that makes no sense
			if (startDataTime_!= null && endDataTime_ != null && startDataTime_.Date.Ticks >= endDataTime_.Date.Ticks) 
			{
				throw new SubscriptionException("The start date must be before the end date");
			}
			//Create the version dates range request and get the response
			DSVersionDatesRangeRequest request = new DSVersionDatesRangeRequest(this, key_, startDataTime_, endDataTime_);
			DSVersionDatesRangeResponse response = request.SendRequest();
			VersionDataKeyCollection versions = response.VersionDataKeys;
			//Return the list of version data keys found
      if (_logger.WillLog(MSLogPriority.Info))
      {
        if (versions == null)
        {
          _logger.Log(MSLog.Info(), "GetVersionsList", Id + ": No versions returned");
        }
        else
        {
          _logger.Log(MSLog.Info(), "GetVersionsList", Id + ": Found " + versions.Count + " distinct versions");
        }
      }
			return versions;
		}

    public override DataEvent GetVersion ( IVersionDataKey key_ )
    {
      DSVersionRequest versionRequest = new DSVersionRequest(this, key_);
      DSVersionResponse versionResponse = versionRequest.SendRequest();

      if (versionResponse.HasVersion)
      {
        // NBYRNE the timestamp of the Event could be set to the updatetime for version info
        return new DataEvent( versionResponse.DataObject, versionResponse.UpdateUserId );
      }
      else
        return null;
    }

    public override IVersionDataKey GetVersionDataKey( IAliasDataKey aliasDataKey_ )
    {
      string dsKeyStr = SOAPCPSDataService.GetAliasDSKeyString( this, aliasDataKey_ );
      IDataService aliasDS = DataServiceManager.GetDataService("Aliases");

      IVersionDataKey verDsKey = null;
      DataEvent dataEvt = aliasDS.GetSnapshot(new StringDataKey(dsKeyStr));
      if (dataEvt != null)
      {
        XMLDataObject xdo = dataEvt.DataObject as XMLDataObject;
        DataServiceDate dt = new DataServiceDate(xdo.Value.SelectSingleNode("//VersionDataTime").InnerText);
        DataServiceDate ut = new DataServiceDate(xdo.Value.SelectSingleNode("//VersionUpdateTime").InnerText);
        verDsKey = DataKeyFactory.BuildDataKey(aliasDataKey_.DSKey, dt, ut, true);
      }

      return verDsKey;
    }

    public override void TurnoverAlias(IAliasDataKey aliasDataKey_, IVersionDataKey newVersionKey_)
    {
      if (aliasDataKey_.DSKey != newVersionKey_.DSKey)
        throw new DataServiceException("AliasDataKey and VersionDataKey are not for the same DataService Key");

      IDataService aliasDS = DataServiceManager.GetDataService("Aliases");

      StringWriter sw = new StringWriter();
      XmlWriter xmlWriter = new XmlTextWriter(sw);
      xmlWriter.WriteStartDocument();
      xmlWriter.WriteStartElement("Alias");
      xmlWriter.WriteElementString("DataService", this.Id);
      xmlWriter.WriteElementString("DsKey", aliasDataKey_.DSKey);
      xmlWriter.WriteElementString("AliasName", aliasDataKey_.Alias);
      xmlWriter.WriteElementString("UserId", DSEnvironment.USER_NAME);
      xmlWriter.WriteElementString("VersionDataTime", newVersionKey_.DataTime.ToString());
      xmlWriter.WriteElementString("VersionUpdateTime", newVersionKey_.UpdateTime.ToString());

      DataServiceDate now = new DataServiceDate(DateTime.Now);
      string nowString = now.ToString();

      xmlWriter.WriteElementString("DataTime", nowString);
      xmlWriter.WriteElementString("UpdateTime", nowString);
      xmlWriter.WriteEndDocument();
      xmlWriter.Close();
      XmlDocument doc = new XmlDocument();
      doc.LoadXml(sw.ToString());
      
      string dsKeyStr = SOAPCPSDataService.GetAliasDSKeyString( this, aliasDataKey_ );
      IDataObject dataObj = new XMLDataObject( new StringDataKey( dsKeyStr ), doc.DocumentElement );

      aliasDS.Publish(dataObj);
    }

    internal static string GetAliasDSKeyString( IDataService ds_, IAliasDataKey key_ )
    {
      return String.Format("{0}#{1}#{2}", ds_.Id, key_.DSKey, key_.Alias);
    }

    public long Timeout
    {
      get
      {
        return _timeout == -2 ? DSEnvironment.SoapRequestTimeout : _timeout;
      }
      set
      {
        _timeout = value;
      }
    }
  }
}