using System;
using System.Threading;
using MorganStanley.MSDotNet.MSNet;

namespace MorganStanley.MSDesktop.Rambo.DataServices.SoapCps
{
	public class CPSNetLoop
	{
		private static IMSNetLoop _loop;
		private static Thread _loopThread;
		private static ManualResetEvent _latch = new ManualResetEvent(false);

		public static IMSNetLoop Loop
		{
			get
			{
				lock(typeof(CPSNetLoop))
				{
					if(_loop == null)
					{
						_loopThread = new Thread(new ThreadStart(CPSNetLoop.StartLooping));
						_loopThread.Name = "CPSNetLoop";
						_loopThread.IsBackground = true;
						_loopThread.Start();
						_latch.WaitOne();
					}

					return _loop;
				}
			}
		}

		private static void StartLooping()
		{
			_loop = new MSNetLoopDefaultImpl();
			_latch.Set();
			_loop.Loop();
		}
	}
}

