using System;
using System.Xml;
using MorganStanley.MSDotNet.MSSoap.Common;

namespace MorganStanley.MSDesktop.Rambo.DataServices.SoapCps
{
	/// <summary>
	/// Summary description for DSVersionKeysResponse.
	/// </summary>
	public class DSKeysResponse : DSSoapResponse
	{
    private IDataKey[] _dataKeys;

		public DSKeysResponse(ISoapEnvelope responseEnv_) : base(responseEnv_)
		{
      XmlNodeList keyEleList = ResponseBodyXml.SelectNodes("//Key");
      _dataKeys = new IDataKey[ keyEleList.Count ];
      for ( int i = 0; i < keyEleList.Count; i++)
      {
        _dataKeys[i] =  new StringDataKey(keyEleList[i].InnerText);
      }
    }

    public IDataKey[] DataKeys
    {
      get { return _dataKeys; }
    }
	}
}
