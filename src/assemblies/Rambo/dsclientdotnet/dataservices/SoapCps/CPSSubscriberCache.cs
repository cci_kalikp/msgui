using System;
using System.Collections;
using System.Threading;
using System.Xml;
using System.Text;
using MorganStanley.MSDotNet.MSNet;
using MorganStanley.MSDotNet.MSLog;
using MorganStanley.AppMW.CPS;
using MorganStanley.MSDesktop.Rambo.DataServices.Util;

namespace MorganStanley.MSDesktop.Rambo.DataServices.SoapCps
{
  /// <summary>
  /// Summary description for CPSSubscriberCache.
  /// </summary>
  internal sealed class CPSSubscriberCache
  {
    private static Logger _logger = new Logger(typeof(CPSSubscriberCache));

    private static object _subscriptionMapLock = new object();

    /// <summary>
    /// Map with entries keyed by 'host:port' storing all the subscriptions made on the CPS at that location
    /// </summary>
    private static MultiMap _subscriptionMap = new MultiMap();

    /// <summary>
    /// Map with entries keyed by 'host:port' storing all the subscriptions still to be made on the CPS at that location
    /// </summary>
    private static MultiMap _toBeSubscribedMap = new MultiMap();

    /// <summary>
    /// Lock for the Filter Subsciptions map
    /// </summary>
    private static object _clientFilterSubscriptionsLock = new object();

    /// <summary>
    /// Map of CPSSubscriptionHandler objects keyed on the address and topic of the subscriptions
    /// </summary>
    private static IDictionary _clientFilterSubscriptions = new Hashtable();

    private static CPSSubsciberManager _subscriberMgr;

    #region Heartbeat members

    private static CPSDefaultClientBuilder _cpsClientBuilder = new CPSDefaultClientBuilder();

    private static IMSNetLoop _cpsLoop;

    private static int _heartbeatInterval = 600000;

    private static int _heartbeatTimeout = 2000;

    private static MSNetTimerDelegate _heartbeatCallback = new MSNetTimerDelegate(HeartbeatCallback);

    private const string HEARTBEAT_MSG_TOPIC = "DSHeartbeat";

    /// <summary>
    /// Marker class to tell when hearbeat has been forced in HeartbeatCallback
    /// </summary>
    private class ForceHeartbeatMarker { }

    #endregion

    static CPSSubscriberCache()
    {
      _cpsLoop = CPSNetLoop.Loop;
      _subscriberMgr = new CPSSubsciberManager(_cpsLoop);
      _subscriberMgr.OnConnected += new CPSSubsciberManager.CPSConnectDelegate(ConnectCallback);
      _subscriberMgr.OnDisconnect += new CPSSubsciberManager.CPSDisconnectDelegate(DisconnectCallback);
      _cpsLoop.CallbackRepeatedly(_heartbeatInterval, _heartbeatCallback);
    }

    private CPSSubscriberCache()
    {
    }

    internal static IMSNetLoop CPSLoop
    {
      get { return _cpsLoop; }
    }

    public static int HeartbeatInterval
    {
      get
      {
        return _heartbeatInterval;
      }
      set
      {
        _heartbeatInterval = value;
        _cpsLoop.CancelCallback(_heartbeatCallback);
        _cpsLoop.CallbackRepeatedly(_heartbeatInterval, _heartbeatCallback);
      }
    }

    public static void ForceHeartbeat()
    {
      _cpsLoop.InvokeLater(new MSNetVoidVoidDelegate(HeartbeatCallback));
    }

    private static void HeartbeatCallback()
    {
      HeartbeatCallback(new ForceHeartbeatMarker(), null);
    }

    /// <summary>
    /// Called by the DataServiceManager's MSNetLoop every _heartbeatInterval to send heartbeat messages
    /// to the CPS servers subscribed to. If a heartbeat message is not acknowledged by the CPS the
    /// CPSDisconnectCallback method is invoked.
    /// </summary>
    /// <param name="sender_">dummy</param>
    /// <param name="args_">dummy</param>
    private static void HeartbeatCallback(object sender_, MSNetTimerEventArgs args_)
    {
      XmlDocument doc = new XmlDocument();
      XmlElement message = doc.CreateElement("DSHeartbeat");
      XmlElement hostportNode = doc.CreateElement("HostPort");
      message.AppendChild(hostportNode);
      XmlElement env = doc.CreateElement("Environment");
      env.AppendChild(doc.CreateTextNode(Enum.GetName(typeof(DSEnvironment.DSEnv), DSEnvironment.Environment)));
      message.AppendChild(env);
      XmlElement loc = doc.CreateElement("Location");
      loc.AppendChild(doc.CreateTextNode(Enum.GetName(typeof(DSEnvironment.DSLoc), DSEnvironment.Location)));
      message.AppendChild(loc);
      XmlElement timestamp = doc.CreateElement("Timestamp");
      message.AppendChild(timestamp);
      XmlElement appPath = doc.CreateElement("AppPath");
      appPath.AppendChild(doc.CreateTextNode(DSEnvironment.APP_PATH));
      message.AppendChild(appPath);
      XmlElement username = doc.CreateElement("Username");
      username.AppendChild(doc.CreateTextNode(DSEnvironment.USER_NAME));
      message.AppendChild(username);


      //Clone the subscriberMap for better concurrency
      ICollection plants = _subscriberMgr.Plants;

      foreach (CPSPlant plant in plants)
      {
        CPSSubscriber sub;
        lock (plant.SyncRoot)
        {
          sub = plant.Subscriber;
        }
        if (sub != null && sub.IsConnected)
        {
          CPSPublisher pub = _cpsClientBuilder.BuildPublisher(_cpsLoop, sub.SubscriberID, "#DSHeartbeatPublisher");
          CPSPublication publication = pub.CreatePublication();
          publication.Topic = HEARTBEAT_MSG_TOPIC;
          publication.AckDesired = CPSPublication.RECEIVED_ACK_DESIRED;
          timestamp.RemoveAll();
          timestamp.AppendChild(doc.CreateTextNode(DateTime.Now.ToString()));
          hostportNode.AppendChild(doc.CreateTextNode(sub.SubscriberID));
          publication.MessageBody = Encoding.Default.GetBytes(message.OuterXml);

          try
          {
            pub.SyncConnect();
            pub.SyncPublish(publication, _heartbeatTimeout);
            pub.Close();
          }
          catch (Exception) //The MSNet documentation doesn't specify what is thrown by SyncConnect
          {
            _logger.Log(MSLog.Debug(), "HeartbeatCallback", "Connection problem for: " + sub.SubscriberID);
            sub.Close();
          }
        }
      }
    }

    private static string BuildSubscriptionsMapKey(string address_, string topic_)
    {
      return address_ + "#" + topic_;
    }

    private static CPSSubscriptionHandler CreateSubscription(CPSPlant plant_, string topic_, string filter_, string dsname_)
    {
      CPSSubscriptionHandler subscription;
      subscription = new CPSSubscriptionHandler(new CPSSubscription(_cpsLoop, plant_.Subscriber, topic_, filter_, false), plant_);
      plant_.AddDataService(dsname_);
      return subscription;
    }

    public static CPSSubscriptionHandler Subscribe(string masterAddress_, string[] clientCPSAddresses_,
                                                   string topic_, string filter_, IDataService ds_)
    {
      CPSPlant plant = _subscriberMgr.GetCPSPlant(masterAddress_, clientCPSAddresses_);
      CPSSubscriptionHandler subscription;
      if (ds_.ServerSubFiltering)
      {
        subscription = CreateSubscription(plant, topic_, filter_, ds_.Id);
        lock (_subscriptionMapLock)
        {
          // all subscriptions are placed in this map
          AddEntry(_subscriptionMap, plant.MasterAddress, subscription);
          _Subscribe(subscription, false);
        }
      }
      else
      {
        lock (_clientFilterSubscriptionsLock)
        {
          string key = BuildSubscriptionsMapKey(plant.MasterAddress, topic_);
          subscription = (CPSSubscriptionHandler)_clientFilterSubscriptions[key];
          if (subscription == null)
          {
            subscription = CreateSubscription(plant, topic_, filter_, ds_.Id);
            _clientFilterSubscriptions.Add(key, subscription);
            lock (_subscriptionMapLock)
            {
              // all subscriptions are placed in this map
              AddEntry(_subscriptionMap, plant.MasterAddress, subscription);
              _Subscribe(subscription, false);
            }
          }
          else
          {
            plant.AddDataService(ds_.Id);
            ++subscription;
          }
        }
      }
      return subscription;
    }

    public static void UnSubscribe(CPSSubscriptionHandler subscription_, IDataService ds_)
    {
      subscription_.Plant.RemoveDataService(ds_.Id);
      if (!ds_.ServerSubFiltering)
      {
        lock (_clientFilterSubscriptionsLock)
        {
          --subscription_;
          if (subscription_.SubscriptionCount == 0)
          {
            string key = BuildSubscriptionsMapKey(subscription_.Plant.MasterAddress, subscription_.Subscription.Topic);
            _clientFilterSubscriptions.Remove(key);
          }
          else
          {	//Exit as still in use
            return;
          }
        }
      }
      lock (_subscriptionMapLock)
      {
        RemoveEntry(_subscriptionMap, subscription_.Plant.MasterAddress, subscription_);
        RemoveEntry(_toBeSubscribedMap, subscription_.Plant.MasterAddress, subscription_);
        _UnSubscribe(subscription_);
      }
    }

    private static void _UnSubscribe(CPSSubscriptionHandler subscription_)
    {
      ICPSSubscriber subscriber = subscription_.Subscription.Subscriber;
      string logMsg = String.Format("{0} - Topic[{1}] - Filter[{2}]", GetCPSInfo(subscriber), subscription_.Subscription.Topic, subscription_.Subscription.Filter);
      _logger.Log(MSLog.Info(), "_UnSubscribe", String.Format("{0} - Unsubscribing - subId='{1}'", logMsg, subscription_.Subscription.SubscriptionID));

      if (subscriber != null)
      {
        try
        {
          subscriber.BeginUnsubscribe(subscription_.Subscription, new AsyncCallback(_UnSubscribeCallback), subscription_.Subscription);
        }
        catch (Exception ex_) // the only one i'm aware of 'CPSNotConnectedException' and we just checked if we were
        {
          _logger.Log(MSLog.Warning(), "_UnSubscribe", logMsg + " - Error: ", ex_);
        }
      }
      subscription_.Subscription.SubscriptionID = null;
    }

    private static void _UnSubscribeCallback(IAsyncResult ar_)
    {
      CPSSubscription subscription = ar_.AsyncState as CPSSubscription;
      string logMsg = "Unknown subscription: ";
      if (subscription.Subscriber != null)
      {
        try
        {
          logMsg = String.Format("{0} - Topic[{1}] - Filter[{2}]", GetCPSInfo(subscription.Subscriber), subscription.Topic, subscription.Filter);
          subscription.Subscriber.EndUnsubscribe(ar_);
        }
        catch (Exception ex_)
        {
          _logger.Log(MSLog.Warning(), "_UnSubscribeCallback", logMsg + " - Error: ", ex_);
        }
      }
      if (subscription != null)
      {
        subscription.SubscriptionID = null;
      }
    }

    private static void EndSubscribeCallback(IAsyncResult ar_)
    {
      string logMsg = "";
      CPSSubscription sub = ar_.AsyncState as CPSSubscription;
      logMsg = String.Format("{0} - Topic[{1}] - Filter[{2}]", GetCPSInfo(sub.Subscriber), sub.Topic, sub.Filter);
      if (sub.Subscriber != null)
      {
        try
        {
          sub.SubscriptionID = sub.Subscriber.EndSubscribe(ar_);
          MSNetAsyncOperation res = (MSNetAsyncOperation)ar_;
          if (res.Exception != null)
          {
            throw res.Exception;
          }
          if (_logger.WillLog(MSLogPriority.Info))
          {
            _logger.Log(MSLog.Info(), "EndSubscribeCallback", String.Format("{0} - successfully subscribed - subId='{1}'", logMsg, sub.SubscriptionID));
          }
        }
        catch (Exception ex_)
        {
          _logger.Log(MSLog.Error(), "EndSubscribeCallback", logMsg + " - Error: ", ex_);
          string msg = "Couldn't subscribe to : " + sub.Topic + " - " + sub.Filter;
          DataServiceManager.ReportStatus(new string[] { }, DSStatusType.CPSError, msg);
        }
      }
    }

    // expected to be called by the Thread holding the lock on '_subscriptionMapLock'
    private static bool _Subscribe(CPSSubscriptionHandler subscription_, bool reconnect_)
    {
      ICPSSubscriber subscriber = subscription_.Subscription.Subscriber;
      string logMsg = String.Format("{0} - Topic[{1}] - Filter[{2}]", GetCPSInfo(subscriber), subscription_.Subscription.Topic, subscription_.Subscription.Filter);
      _logger.Log(MSLog.Info(), "_Subscribe", logMsg);

      if (subscriber != null)
      {
        try
        {
          if (_logger.WillLog(MSLogPriority.Info))
          {
            _logger.Log(MSLog.Info(), "_Subscribe", String.Format("{0} - already connected so attempting to make the new subscription", logMsg));
          }
          subscriber.BeginSubscribe(subscription_.Subscription, new AsyncCallback(EndSubscribeCallback), subscription_.Subscription);
          return true;
        }
        catch (Exception ex_) // the only one i'm aware of 'CPSNotConnectedException' and we just checked if we were
        {
          _logger.Log(MSLog.Warning(), "_Subscribe", logMsg + " - Error: ", ex_);
        }
      }
      else
      {
        _logger.Log(MSLog.Info(), "_Subscribe", String.Format("{0} - not connected yet, so will make subscription on the ConnectCallback", logMsg));
      }
      if (!reconnect_)
      {
        AddEntry(_toBeSubscribedMap, subscription_.Plant.MasterAddress, subscription_);
      }
      return false;
    }


    private static void ConnectCallback(object sender_, CPSPlant plant_)
    {
      try
      {
        _logger.Log(MSLog.Debug(), "ConnectCallback", plant_.MasterAddress + " -> " + plant_.Subscriber.SubscriberID);

        lock (_subscriptionMapLock)
        {
          ArrayList pendingSubs = _toBeSubscribedMap[plant_.MasterAddress];

          while (pendingSubs != null && pendingSubs.Count > 0) // could drain this in a different thread & release the NetLoop
          {
            CPSSubscriptionHandler sub = (CPSSubscriptionHandler)pendingSubs[0];
            CPSSubscription oldSub = sub.Subscription;
            sub.Subscription = new CPSSubscription(_cpsLoop, sub.Plant.Subscriber, oldSub.Topic, oldSub.Filter, false);
            if (_Subscribe(sub, true))
            {
              RemoveEntry(_toBeSubscribedMap, plant_.MasterAddress, sub);
            }
            else
            {
              _logger.Log(MSLog.Error(), "ConnectCallback", "Failed to subscribe - breaking");
              break; // assuming this was due to connection problem & try again on re-connect
            }
          }
        }
      }
      catch (Exception ex_)
      {
        _logger.Log(MSLog.Error(), "ConnectCallback", "Error: " + ex_.Message, ex_);
      }
    }

    private static void DisconnectCallback(object sender_, CPSPlant plant_)
    {
      LogCPSInfo(plant_.Subscriber, "CPSDisconnectCallback");
      lock (_subscriptionMapLock)
      {
        if (_toBeSubscribedMap.ContainsKey(plant_.MasterAddress))
          _toBeSubscribedMap[plant_.MasterAddress].Clear();
        _toBeSubscribedMap[plant_.MasterAddress] = (ArrayList)_subscriptionMap[plant_.MasterAddress].Clone();
      }
    }


    // assumes the calling Thread has the relevant lock on the 'map_' provided
    private static void AddEntry(MultiMap map_, string key_, CPSSubscriptionHandler value_)
    {
      if (map_.ContainsKey(key_))
      {
        map_[key_].Add(value_);
      }
      else
      {
        ArrayList list = new ArrayList();
        list.Add(value_);
        map_[key_] = list;
      }
    }

    // assumes the calling Thread has the relevant lock on the 'map_' provided
    private static void RemoveEntry(MultiMap map_, string key_, CPSSubscriptionHandler value_)
    {
      if (map_.ContainsKey(key_))
      {
        ArrayList list = map_[key_];
        if (list.Contains(value_))
        {
          list.Remove(value_);
          if (list.Count == 0)
          {
            if (_logger.WillLog(MSLogPriority.Debug))
            {
              _logger.Log(MSLog.Debug(), "RemoveEntry", GetCPSInfo(value_.Subscription.Subscriber) + " Removed last subscription");
            }
            map_.Remove(key_);
          }
        }
        else
          _logger.Log(MSLog.Debug(), "RemoveEntry", "The CPSSubscription was not found in the map provided");
      }
      else
        _logger.Log(MSLog.Debug(), "RemoveEntry", "There are no subscriptions for that CPS in the map provided");
    }

    private static void LogCPSInfo(CPSSubscriber subscriber_, string callback_)
    {
      string subscriberId = subscriber_ == null ? null : subscriber_.SubscriberID;
      string cpsInfo;
      bool isSubscriberIdInvalid = GenerateCPSInfoText(subscriberId, out cpsInfo);

      MSLogMessage message = isSubscriberIdInvalid ? MSLog.Warning() : MSLog.Info();
      _logger.Log(message, callback_, cpsInfo);
    }

    private static string GetCPSInfo(ICPSSubscriber subscriber_)
    {
      string subscriberId = subscriber_ == null ? null : subscriber_.SubscriberID;
      string messageText;
      GenerateCPSInfoText(subscriberId, out messageText);

      return messageText;
    }

    private static bool GenerateCPSInfoText(string subscriberId_, out string messageText_)
    {
      if (String.IsNullOrEmpty(subscriberId_))
      {
        messageText_ = "<null subscriber>";
        return true;
      }

      try
      {
        MSNetInetAddress address = new MSNetInetAddress(subscriberId_);
        messageText_ = String.Format("CPS[{0}:{1}]", address.HostName, address.Port);
      }
      catch
      {
        messageText_ = "CPS[SubscriberID was not in the expected format]";
        return false;
      }
      return true;
    }
  }
}