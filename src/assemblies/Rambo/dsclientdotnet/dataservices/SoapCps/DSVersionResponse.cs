using System;
using System.Xml;
using MorganStanley.MSDotNet.MSSoap.Common;

namespace MorganStanley.MSDesktop.Rambo.DataServices.SoapCps
{
  /// <summary>
  /// Summary description for DSVersionResponse.
  /// </summary>
  public class DSVersionResponse : DSSoapResponse
  {
    private IVersionDataKey _versionDataKey;
    private XMLDataObject _versionDataObject;
    private string _updateUserId;

    public DSVersionResponse(ISoapEnvelope responseEnv_) : base(responseEnv_)
    {
      XmlElement valueElement = ResponseBodyXml.SelectSingleNode("//Value").FirstChild as XmlElement;     
      if ( valueElement != null )
      {
          string keyStr = ResponseBodyXml.SelectSingleNode("//Key").InnerText;
          string dataTimeStr = ResponseBodyXml.SelectSingleNode("//DataTime").InnerText;
          string updateTimeStr = ResponseBodyXml.SelectSingleNode("//UpdateTime").InnerText;

          DataServiceDate dataTime = new DataServiceDate(dataTimeStr);
          DataServiceDate updateTime = new DataServiceDate(updateTimeStr);

          _versionDataKey = DataKeyFactory.BuildDataKey(keyStr,dataTime,updateTime,true);
          _versionDataObject = new XMLDataObject( _versionDataKey, valueElement );      

          _updateUserId = GetUpdateUserId(valueElement);
      }
    }

    /// <summary>
    /// Returns bool indicating if the response contained version data
    /// </summary>
    public bool HasVersion
    {
      get { return ( _versionDataKey != null && _versionDataObject != null ); }
    }

    /// <summary>
    /// This will be null if no version was found matching the request information
    /// </summary>
    public IVersionDataKey DataKey
    {
      get { return _versionDataKey; }
    }

    /// <summary>
    /// This will be null if no version was found matching the request information
    /// </summary>
    public XMLDataObject DataObject
    {
      get { return _versionDataObject; }
    }

    /// <summary>
    /// This will be null if no version was found matching the request information
    /// </summary>
    public string UpdateUserId
    {
      get { return _updateUserId; }
    }
  }
}
