using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using MorganStanley.MSDotNet.MSSoap.Common;

namespace MorganStanley.MSDesktop.Rambo.DataServices.SoapCps
{
	/// <summary>
	/// Summary description for DSDataRequest.
	/// </summary>
	public class DSDataRequest : DSSoapRequest
	{
    /// <summary>
    /// Enumeration defining the request type required for a DSDataRequest
    /// </summary>
    public enum RequestType { snapshot, updates, all }

    /// <summary>
    /// The parameter 'requestType_' defines which DSDataRequest will be send to the DataService Server
    /// </summary>
    /// <param name="dataService_">The dataservice</param>
    /// <param name="subscription_">The definition of the subscription</param>
    /// <param name="reqestType_">The type of DSDataRequest</param>
    public DSDataRequest(IDataService dataService_, ISubscription subscription_, RequestType reqestType_) : base(dataService_)
    {
      CreateDataRequestSoapEnvelope(subscription_,  subscription_.Type, reqestType_);
    }

		/// <summary>
		/// Overrides the subscription's type to force a subscription of a different type (e.g. Global for client side filtering)
		/// to be created.
		/// The parameter 'requestType_' defines which DSDataRequest will be send to the DataService Server
		/// </summary>
    /// <param name="dataService_">The dataservice</param>
		/// <param name="subscription_">The definition of the subscription</param>
		/// <param name="subType_">The Subscription type to override the subscription.Type property with</param>
		/// <param name="reqestType_">The type of DSDataRequest</param>
		public DSDataRequest(IDataService dataService_, ISubscription subscription_,  SubType subType_, RequestType reqestType_) : base(dataService_)
		{
			CreateDataRequestSoapEnvelope(subscription_, subType_, reqestType_);
		}

    public DSDataResponse SendRequest(long timeout_)
    {
      ISoapEnvelope responseEnvelope = InternalSendRequest(timeout_);
      DSDataResponse response = new DSDataResponse(responseEnvelope);
      return response;
    }

		public new virtual DSDataResponse EndSendRequest(IAsyncResult ar_) 
		{
			return new DSDataResponse(InternalEndSendRequest(ar_));
		}

    private void CreateDataRequestSoapEnvelope(ISubscription subscription_,  SubType subType_, RequestType reqestType_)
    {
      StringWriter sw = new StringWriter();
      XmlWriter xmlWriter = new XmlTextWriter(sw);
      xmlWriter.WriteStartDocument();
      xmlWriter.WriteStartElement("DSDataRequest");
      xmlWriter.WriteElementString( "DataService", _dataService.Id);

      xmlWriter.WriteElementString( "RequestType", reqestType_.ToString() );

      // NBYRNE - this info is in the header already?
      //      xmlWriter.WriteElementString( "UserId", subscription_.UserId );
      xmlWriter.WriteStartElement( "Subscription" );
      string subDesc = string.Empty;
      if ( subType_ == SubType.GLOBAL )
      {
        xmlWriter.WriteAttributeString( "type", "global" );
        subDesc = "global";
      }
      else if ( subType_ == SubType.COLLECTION && subscription_.DataKeys != null && subscription_.DataKeys.Count > 0 )
      {
        IList<string> keys = new List<string>();
        foreach( IDataKey dataKey in subscription_.DataKeys )
        {
          xmlWriter.WriteStartElement("Key");
          xmlWriter.WriteString(dataKey.ToString());
          xmlWriter.WriteEndElement();				
			    keys.Add(dataKey.ToString());
        }
        subDesc = string.Join(";", keys);
        subDesc = "Key=" + subDesc;
      }
      else if ((subType_ == SubType.ATTRIBUTE || subType_ == SubType.STRICTATTRIBUTE) && subscription_.Attributes != null && subscription_.Attributes.Count > 0)
      {
        xmlWriter.WriteAttributeString( "type", "attribute" );
        
        IList<string> attributes = new List<string>(); 
        foreach ( string key in subscription_.Attributes.Keys )
        {
          if (string.IsNullOrEmpty(key))
          {
             throw new InvalidOperationException("Attribute name cannot be blank");
          }

          xmlWriter.WriteStartElement("Attribute");
          string attName = subType_ == SubType.ATTRIBUTE ? key.Substring(0, 1).ToUpper() + key.Substring(1) : key;
          object attributeValue = subscription_.Attributes[key];
          if (attributeValue == null)
          {
            throw new InvalidOperationException(String.Format("Attribute value for {0} cannot be null", key));
          }
          string value = attributeValue.ToString();
          xmlWriter.WriteAttributeString("name", attName);
          xmlWriter.WriteAttributeString( "value", value );
          xmlWriter.WriteEndElement();
          attributes.Add(string.Format("{0}:{1}",attName, value));
        }
        subDesc = string.Join("; ", attributes);
        subDesc = "Attributes=" + subDesc;        
      }

      xmlWriter.WriteEndDocument();
      xmlWriter.Close();			
      XmlDocument doc = new XmlDocument();
      doc.LoadXml( sw.ToString() );

      SubDesc = subDesc;
      ModifySoapAction(doc.DocumentElement);
    }
	}
}
