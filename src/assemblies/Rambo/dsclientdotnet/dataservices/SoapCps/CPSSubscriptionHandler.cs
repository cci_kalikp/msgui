using System;
using MorganStanley.AppMW.CPS;

namespace MorganStanley.MSDesktop.Rambo.DataServices.SoapCps
{
	/// <summary>
	/// Summary description for CPSSubscriptionHandler.
	/// </summary>
	internal class CPSSubscriptionHandler
	{
    private event CPSMessageDelegate OnMessage;

		private CPSSubscription _subscription;
    private CPSPlant _plant;
		private int _subscriptionCount;

		public CPSSubscriptionHandler(CPSSubscription subscription_, CPSPlant plant_)
		{
			_subscriptionCount = 1;
			_subscription = subscription_;
      _subscription.OnMessage += new CPSMessageDelegate(OnCPSMessage);
      _plant = plant_;
		}

    public CPSPlant Plant
    {
      get { return _plant; }
    }

    public CPSSubscription Subscription 
    {
      get { return _subscription; }
      set
      {
        if (_subscription != null)
        {
          _subscription.OnMessage -= new CPSMessageDelegate(OnCPSMessage);
        }
        _subscription = value;
        if (_subscription != null)
        {
          _subscription.OnMessage += new CPSMessageDelegate(OnCPSMessage);
        }
      }
    }

		public int SubscriptionCount 
		{
			get { return _subscriptionCount; } 
		}

		public static CPSSubscriptionHandler operator ++(CPSSubscriptionHandler x_) 
		{
			lock (x_) 
			{
				++x_._subscriptionCount;
			}
			return x_;
		}

		public static CPSSubscriptionHandler operator --(CPSSubscriptionHandler x_)
		{
			lock (x_) 
			{
				--x_._subscriptionCount;
			}
			return x_;
		}

		public void AddListener(CPSMessageDelegate listener_)
		{
			OnMessage += listener_;
		}

		public void RemoveListener(CPSMessageDelegate listener_) 
		{
			OnMessage -= listener_;
		}

    private void OnCPSMessage(object sender_, CPSMessage message_)
    {
      if (OnMessage != null)
      {
        OnMessage(sender_, message_);
      }
    }
	}
}
