using System;
using System.Xml;
using MorganStanley.MSDotNet.MSSoap.Common;


namespace MorganStanley.MSDesktop.Rambo.DataServices.SoapCps
{
	/// <summary>
	/// Summary description for DSSoapResponse.
	/// </summary>
	public abstract class DSSoapResponse
	{
    protected string _dataServiceName;
    protected XmlElement _responseBodyXml;

    protected DSSoapResponse(XmlElement responseBodyXml_)
    {
      _responseBodyXml = responseBodyXml_;
      XmlElement dataServiceEle = _responseBodyXml.SelectSingleNode("//DataService") as XmlElement;
      if (dataServiceEle != null)
        _dataServiceName = dataServiceEle.InnerText;
    }

    protected DSSoapResponse(ISoapEnvelope responseEnv_) : this(((SoapBodyDomImpl) responseEnv_.Body).XmlElement)
    {
    }

    protected virtual string DataServiceName
    {
      get { return _dataServiceName; }
    }

    protected virtual XmlElement ResponseBodyXml
    {
      get { return _responseBodyXml; }
    }

    protected string GetUpdateUserId(XmlElement valueElement_)
    {
			return Util.Utils.GetUserID(valueElement_);
    }
	}
}
