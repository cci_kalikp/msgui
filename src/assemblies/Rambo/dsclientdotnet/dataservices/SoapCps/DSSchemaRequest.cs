using System;
using System.IO;
using System.Xml;
using MorganStanley.MSDotNet.MSSoap.Common;

namespace MorganStanley.MSDesktop.Rambo.DataServices.SoapCps
{
	/// <summary>
	/// Summary description for DSSchemaRequest.
	/// </summary>
	public class DSSchemaRequest : DSSoapRequest
	{
		public DSSchemaRequest(IDataService dataService_) : base(dataService_)
		{
      CreateSchemaSoapEnvelope();
		}

    public DSSchemaResponse SendRequest()
    {
      ISoapEnvelope responseEnvelope = base.InternalSendRequest();
      DSSchemaResponse response = new DSSchemaResponse(responseEnvelope);
      return response;
    }

    private void CreateSchemaSoapEnvelope()
    {
      StringWriter sw = new StringWriter();
      XmlWriter xmlWriter = new XmlTextWriter( sw );
      xmlWriter.WriteStartDocument();
      xmlWriter.WriteStartElement("DSSchemaRequest"); 
      xmlWriter.WriteElementString("DataService",_dataService.Id);
      xmlWriter.WriteEndDocument();
      xmlWriter.Close();
      XmlDocument doc = new XmlDocument();
      doc.LoadXml( sw.ToString() );

      base.ModifySoapAction(doc.DocumentElement);
    }
	}
}
