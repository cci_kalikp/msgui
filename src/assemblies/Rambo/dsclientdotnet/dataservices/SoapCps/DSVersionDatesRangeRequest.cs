using System;
using System.Xml;
using System.Xml.XPath;
using MorganStanley.MSDotNet.MSSoap.Common;
using MorganStanley.MSDesktop.Rambo.DataServices;

namespace MorganStanley.MSDesktop.Rambo.DataServices.SoapCps
{
	/// <summary>
	/// This class extends the basic DSSOAPRequest for a version dates range request to the data service
	/// </summary>
	public class DSVersionDatesRangeRequest : DSSoapRequest
	{
		internal DSVersionDatesRangeRequest(IDataService dataService_,  IDataKey key_, DataServiceDate startDataTime_, DataServiceDate endDataTime_)
			: base(dataService_)
		{
			createVersionDatesRangeRequest(key_, startDataTime_, endDataTime_);
		}

		public DSVersionDatesRangeResponse SendRequest() 
		{
			return new DSVersionDatesRangeResponse(InternalSendRequest());
		}

		private void createVersionDatesRangeRequest(IDataKey key_, DataServiceDate startDataTime_, DataServiceDate endDataTime_)
		{
			XmlDocument xmlDoc = new XmlDocument();
      XmlElement actionEl = xmlDoc.CreateElement("DSVersionDatesRangeRequest");
			XmlElement child = xmlDoc.CreateElement("DataService");
			child.AppendChild(xmlDoc.CreateTextNode(_dataService.Id));
			actionEl.AppendChild(child);
			child = xmlDoc.CreateElement("Key");
			child.AppendChild(xmlDoc.CreateTextNode(key_.DSKey));
			actionEl.AppendChild(child);
			if (startDataTime_ != null)
			{
				child = xmlDoc.CreateElement("StartDataTime");
				child.AppendChild(xmlDoc.CreateTextNode(startDataTime_.ToString()));
				actionEl.AppendChild(child);
			}
			if (endDataTime_ != null)
			{
				child = xmlDoc.CreateElement("EndDataTime");
				child.AppendChild(xmlDoc.CreateTextNode(endDataTime_.ToString()));
				actionEl.AppendChild(child);
			}
			ModifySoapAction(actionEl);
		}
	}
}
