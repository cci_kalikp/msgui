//---------------------------------------------------------------------------
// ($Id $)
//---------------------------------------------------------------------------
//
// Copyright (c) 2005 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
//---------------------------------------------------------------------------

using System;
using System.Collections;
using MorganStanley.MSDesktop.Rambo.DataServices.Util;
using MorganStanley.MSDotNet.MSLog;
   
namespace MorganStanley.MSDesktop.Rambo.DataServices.SoapCps
{
	/// <summary>
	/// Storage class used to store a list of DSServer objects which are used by the
	/// SoapServer class to connect to a data service.
	//  This class is thread safe.
	/// </summary>
	public class ServerList : IEnumerable
	{
    /// <summary>
    /// The DSServer objects
    /// </summary>
		private IList _servers;

    /// <summary>
    /// Whether or not the server list can be altered
    /// </summary>
    private bool _fixed;

    /// <summary>
    /// Whether or not the serverlist is a backup
    /// </summary>
    private bool _backup;

    /// <summary>
    /// The random number generator used by the iterators to return random servers
    /// each time
    /// </summary>
		private Random _random = new Random(DateTime.Now.Millisecond);

    /// <summary>
    /// Creates an empty server list (can add servers using Add() or AddServer() methods)
    /// </summary>
		public ServerList(): this((IList)null, false)
    {
		}

    /// <summary>
    /// Creates a ServerList from a list of either strings of the form\n
    /// transport://host:port\n
    /// or of DSServer objects.
    /// </summary>
    /// <param name="servers_">list of either strings or DSServer objects</param>
    /// <param name="fixed_">Whether or not the list can be altered</param>
    public ServerList(IList servers_, bool fixed_)
		{
      _fixed = fixed_;
      _backup = false;
			if (servers_ == null) 
			{
				_servers = new ArrayList();
			}
			else 
			{
        BuildFromStringArray(servers_);
			}
		}

    /// <summary>
    /// Copy constructor which provides a threadsafe clone
    /// </summary>
    /// <param name="serverList_">the ServerList object to clone from</param>
		public ServerList(ServerList serverList_)
		{
      _backup = false;
      lock (serverList_) 
      {
        _fixed = serverList_._fixed;
        _servers = new ArrayList();
        ((ArrayList) _servers).AddRange(serverList_._servers);
        _random = serverList_._random;
      }
		}

    /// <summary>
    /// Builds a list of DSServer objects from a list of address strings, (see ctor for format)
    /// </summary>
    /// <param name="servers_">List of address strings</param>
		public void BuildFromStringArray(IList servers_) 
		{
			try 
			{
				_servers = new ArrayList();
				for (int i=0; i<servers_.Count; ++i) 
				{
					string[] sections = ((string) servers_[i]).Split( new char[] {':'} );
					string host = sections[1].Substring(2);
					string port = sections[2];
          if (sections[0] == "ktcp")
          {
            //ktcp
            _servers.Add(new DSServer(host, null, port, null, null, "up", DSEnvironment.Location.ToString(),
                                      DSEnvironment.Environment.ToString(), null));
          }
					else if (sections[0] == "tcp") 
					{
            //tcp
					  _servers.Add(new DSServer(host, null, null, port, null, "up", DSEnvironment.Location.ToString(),
					                            DSEnvironment.Environment.ToString(), null));
					}
					else 
					{ //http
					  _servers.Add(new DSServer(host, null, null, null, port, "up", DSEnvironment.Location.ToString(),
					                            DSEnvironment.Environment.ToString(), null));
					}
				}
			}
			catch (Exception ex_) 
			{
				Logger log = new Logger( typeof(ListServerManager) );
				log.Log(MSLog.Critical(), "ctor(string[])", "Invalid server list syntax :"+servers_.ToString());
				throw new DataServiceException("Invalid server list syntax", ex_);
			}
		}

    /// <summary>
    /// Returns the addresses of all the servers in the list as a string[]
    /// </summary>
    /// <returns>the server addresses</returns>
		public string[] ToStringArray()
		{
      lock (this) 
      {
        string[] resultArr = new string[_servers.Count];
        int i = 0;
        foreach (DSServer server in _servers) 
        {
          resultArr[i++] = server.Address;
        }
        return resultArr;
      }
		}

    /// <summary>
    /// Gets or sets whether or not the list can be altered
    /// </summary>
    public bool IsFixed
    {
      get { return _fixed; }
      set { _fixed = value; }
    }

    /// <summary>
    /// Gets or sets whether or not the list is a backup server list
    /// </summary>
    public bool IsBackup
    {
      get { return _backup; }
      set { _backup = value; }
    }

    /// <summary>
    /// Returns the number of servers in the list
    /// </summary>
		public int Count 
		{
			get { return _servers.Count; }
		}

    /// <summary>
    /// Adds a DSServer object to the list
    /// </summary>
    /// <param name="server_">the server to add</param>
		public void AddServer(DSServer server_) 
		{
      if (!_fixed) 
      {
        lock (this) 
        {
          if (!_servers.Contains(server_))
          {
            _servers.Add(server_);
          }
        }
      }
		}

    /// <summary>
    /// Adds a server to the list by it's address string
    /// </summary>
    /// <param name="server_">The address of the server</param>
		public void Add(string server_) 
		{
      if (!_fixed) 
      {
        try 
        {
          string[] sections = server_.Split(new char[] {':'});
          string host = sections[1].Substring(2);
          string port = sections[2];
          lock (this) 
          {
            if (sections[0] == "ktcp")
            {
              //ktcp
              _servers.Add(new DSServer(host, null, port, null, null, "up", DSEnvironment.Location.ToString(),
                                        DSEnvironment.Environment.ToString(), null));
            }
            else if (sections[0] == "tcp") 
            {
              //tcp
              _servers.Add(new DSServer(host, null, null, port, null, "up", DSEnvironment.Location.ToString(),
                                        DSEnvironment.Environment.ToString(), null));
            }
            else 
            { //http
              _servers.Add(new DSServer(host, null, null, null, port, "up", DSEnvironment.Location.ToString(),
                                        DSEnvironment.Environment.ToString(), null));
            }
          }
        }
        catch (Exception ex_) 
        {
          Logger log = new Logger( typeof(ServerList) );
          log.Log(MSLog.Error(), "Add", "Invalid server syntax :"+server_.ToString());
          throw new DataServiceException("Invalid server syntax", ex_);
        }	
      }
		}

    /// <summary>
    /// Removes the passed server from the list
    /// </summary>
    /// <param name="server_"></param>
		public void RemoveServer(DSServer server_) 
		{
      if (!_fixed) 
      {
        lock (this) 
        {
          _servers.Remove(server_);
        }
      }
		}

    /// <summary>
    /// Removes all the servers from the list
    /// </summary>
    public void Clear()
    {
      if (!_fixed) 
      {
        lock (this) 
        {
          _servers.Clear();
        }
      }
    }

    /// <summary>
    /// Returns a ServerList.ServerListIterator enumerator for the list
    /// </summary>
    /// <returns>an enumerator for the list</returns>
    public virtual IEnumerator GetEnumerator()
		{
			return new ServerListIterator(this);
		}
   
    /// <summary>
    /// Starts from a random index in the ServerList and traverses the whole list
    /// </summary>
		private sealed class ServerListIterator : IEnumerator
		{
			private int _index;
			private int _count;
			private int _size;
			private ServerList _serverList;
   
      /// <summary>
      /// Creates an enumerator for the passed list
      /// </summary>
      /// <param name="serverList_"></param>
			public ServerListIterator(ServerList serverList_)
			{
				_serverList = serverList_;
				_size = _serverList._servers.Count;
				Reset();
			}
 
      /// <summary>
      /// Returns the current DSServer object
      /// </summary>
			public object Current 
			{
				get
				{
					if (_index == -1)
						throw new InvalidOperationException("The enumerator is positioned before the first element of the collection");
					else if (_count > _size)
						throw new InvalidOperationException("The enumerator is positioned after the last element");
   
					return _serverList._servers[_index];
				}
			}
 
      /// <summary>
      /// If first call starts at random location else moves index to next position
      /// </summary>
      /// <returns>if not end of list</returns>
			public bool MoveNext()
			{
				if (_size == 0) 
					return false; // no elements to iterate over
   
				if (_index == -1) // then first call to MoveNext or after call to Reset
				{
					_index = _serverList._random.Next( _size );
				}
				else
				{
					_index = (_index + 1) % _size;
				}
   
				bool successfulAdvance = (_count < _size);
				++_count;
				return successfulAdvance;
			}
   
      /// <summary>
      /// Resets the enumerator
      /// </summary>
			public void Reset()
			{
				_index = -1;
				_count = 0;
			}
  	}
	}
}
