using System;
using System.Xml;
using System.Text;
using System.Threading;
using System.Collections;
using MorganStanley.MSDotNet.MSNet;
using MorganStanley.MSDotNet.MSLog;
using MorganStanley.MSDesktop.Rambo.DataServices.Util;
using MorganStanley.MSDotNet.MSSoap.Common;

namespace MorganStanley.MSDesktop.Rambo.DataServices.SoapCps
{
	/// <summary>
	/// Summary description for BufferedSOAPCPSDataService.
	/// </summary>
  public class BufferedSOAPCPSDataService : SOAPCPSDataService
  {
    protected class BufferedSnapshotRequest
    {
      private ISubscription _subscription;
      private IAsyncResult _result;

      public BufferedSnapshotRequest(ISubscription subscription_, IAsyncResult result_)
      {
        _subscription = subscription_;
        _result = result_;
      }

      public ISubscription Subscription
      {
        get { return _subscription; }
      }

      public IAsyncResult Result
      {
        get { return _result; }
      }
    }


    protected class BufferedPublishRequest
    {
      private IList _dataObjects;
      private IAsyncResult _result;

      public BufferedPublishRequest(IList dataObjects_, IAsyncResult result_)
      {
        _dataObjects = dataObjects_;
        _result = result_;
      }

      public BufferedPublishRequest(IDataObject dataObject_, IAsyncResult result_)
      {
        _dataObjects = new ArrayList();
        _dataObjects.Add(dataObject_);
        _result = result_;
      }

      public IList DataObjects
      {
        get { return _dataObjects; }
      }

      public IAsyncResult Result
      {
        get { return _result; }
      }
    }


    protected class BufferedCPSInfoRequest
    {
      private IAsyncResult _result;
      private ISubscription _subscription;

      public BufferedCPSInfoRequest(ISubscription subscription_, IAsyncResult result_)
      {
        _result = result_;
        _subscription = subscription_;
      }
      
      public IAsyncResult Result
      {
        get 
        {
          return _result;
        }
      }

      public ISubscription Subscription
      {
        get 
        {
          return _subscription;
        }
      }
    }


    protected class DSDataRequestWrapper
    {
      private DSDataRequest _dsDataRequest;
      private IList _requests;

      public DSDataRequestWrapper(DSDataRequest dsDataRequest_, IList requests_)
      {
        _dsDataRequest = dsDataRequest_;
        _requests = requests_;
      }

      public DSDataRequest DSDataRequest
      {
        get 
        {
          return _dsDataRequest;
        }
      }

      public IList Requests
      {
        get 
        {
          return _requests;
        }
      }
    }


    public class BufferedDSAsyncResult : IAsyncResult
    {
      protected ManualResetEvent _evnt;
      protected object _state;
      protected AsyncCallback _callback;
      protected Exception _exception = null;
      protected object _resultValue;
      protected bool _completed;

      public BufferedDSAsyncResult(AsyncCallback callback_, object state_)
      {
        _callback = callback_;
        _state = state_;
        _evnt = new ManualResetEvent(false);
        _resultValue = null;
        _completed = false;
      }

      public object AsyncState
      {
        get
        {
          return _state;
        }
      }

      public WaitHandle AsyncWaitHandle
      {
        get
        {
          return _evnt;
        }
      }

      public bool CompletedSynchronously
      {
        get
        {
          return false;
        }
      }

      public bool IsCompleted
      {
        get
        {
          return _completed;
        }
      }

      public Exception InvokeException
      {
        get
        {
          return _exception;
        }
        set
        {
          _exception = value;
        }
      }

      public AsyncCallback Callback
      {
        get 
        {
          return _callback;
        }
      }

      public object ResultValue
      {
        get
        {
          if (_exception != null)
          {
            throw _exception;
          }
          return _resultValue;
        }
      }

      public void CompleteAndCallback(object resultValue_)
      {
        _resultValue = resultValue_;
        _completed = true;
        _evnt.Set();
        _callback(this);
      }

      public void ErrorAndCallback(Exception ex_)
      {
        _exception = ex_;
        _completed = true;
        _evnt.Set();
        _callback(this);
      }
    }


    private Logger _logger = new Logger( typeof(BufferedSOAPCPSDataService) );

    protected object _pendingLock = new object();
    protected IList _pendingSnapshots;
    protected IList _pendingPublishes;
    protected IList _pendingCPSInfos;
    
    private bool _awaitingCallback;
    private int _interval = 1000;
    private int _bufferSize = 100;

    /// <summary>
    /// Public constructor
    /// </summary>
    /// <exception cref="System.ArgumentException">If the id_ parameters is null</exception>
    /// <param name="id_">DataService Name</param>
    /// <param name="servers_">List of servers for the data service to use</param>
    public BufferedSOAPCPSDataService( string id_, ServerList servers_ ) : base(id_, servers_) 
    {						     
      _logger.Log(MSLog.Info(), "BufferedSOAPCPSDataService.ctr()", "BufferedSOAPCPSDataService[" + id_ + "]");
      _pendingSnapshots = new ArrayList();
      _pendingPublishes = new ArrayList();
      _pendingCPSInfos = new ArrayList();
      _awaitingCallback = false;
    }		

    ~BufferedSOAPCPSDataService()
    {
      _logger.Log(MSLog.Info(), "~BufferedSOAPCPSDataService()", "BufferedSOAPCPSDataService[" + Id + "]");
    }

    public int Interval
    {
      get
      {
        return _interval;
      }
      set 
      {
        _interval = value;
      }
    }

    public int BufferSize
    {
      get
      {
        lock (_pendingLock)
        {
          return _bufferSize;
        }
      }
      set
      {
        lock (_pendingLock)
        {
          _bufferSize = value;
        }
      }
    }

    private void SetUpCallback()
    {
      if (!_awaitingCallback)
      {
        DataServiceManager.NetLoop.CallbackAfterDelay(_interval, new MSNetTimerDelegate(this.TriggerRequestsCallback));
        _awaitingCallback = true;
      }
    }
 
    private void TriggerRequestsCallback(object sender_, MSNetTimerEventArgs args_)
    {
      lock (_pendingLock)
      {
          if (_pendingSnapshots.Count > 0)
          {
            IList batchRequests = new ArrayList(_bufferSize);
            IDictionary keys = new Hashtable();
            int c = 0;
            for (IEnumerator i=_pendingSnapshots.GetEnumerator(); i.MoveNext(); )
            {
              BufferedSnapshotRequest req = i.Current as BufferedSnapshotRequest;
              foreach (IDataKey key in req.Subscription.DataKeys)
              {
                keys[key] = new object();
              }
              batchRequests.Add(req);
              if (++c == _bufferSize)
              {
                ISubscription subscription = CreateSubscription(new ArrayList(keys.Keys));
                base.BeginSnapshot(subscription, new AsyncCallback(this.SnapshotCallback), batchRequests);
                c = 0;
                keys = new Hashtable();
                batchRequests = new ArrayList(_bufferSize);
              }
            }
            if (batchRequests.Count > 0)
            {
              ISubscription subscription = CreateSubscription(new ArrayList(keys.Keys));
              base.BeginSnapshot(subscription, new AsyncCallback(this.SnapshotCallback), batchRequests);
            }
            _pendingSnapshots = new ArrayList();
          }
          if (_pendingPublishes.Count > 0)
          {
            IList batchRequests = new ArrayList(_bufferSize);
            ArrayList dataObjects = new ArrayList();
            int c = 0;
            for (IEnumerator i=_pendingPublishes.GetEnumerator(); i.MoveNext() && c<_bufferSize; ++c)
            {
              BufferedPublishRequest req = i.Current as BufferedPublishRequest;
              dataObjects.AddRange(req.DataObjects);
              batchRequests.Add(req);
              if (++c == _bufferSize)
              {
                base.BeginPublish(dataObjects, new AsyncCallback(this.PublishCallback), batchRequests);
                c = 0;
                dataObjects = new ArrayList();
                batchRequests = new ArrayList(_bufferSize);
              }
            }
            if (batchRequests.Count > 0)
            {
              base.BeginPublish(dataObjects, new AsyncCallback(this.PublishCallback), batchRequests);
            }
            _pendingPublishes = new ArrayList();
          }
        
          if (_pendingCPSInfos.Count > 0)
          {
            IList cpsInfoRequests = new ArrayList(_pendingCPSInfos);
            IList batchRequests = new ArrayList(_bufferSize);
            //Create Batch subscription
            IDictionary keys = new Hashtable();
            int c = 0;
            for (IEnumerator i=cpsInfoRequests.GetEnumerator(); i.MoveNext() && c<_bufferSize; ++c)
            {
              BufferedCPSInfoRequest req = i.Current as BufferedCPSInfoRequest;
              foreach (IDataKey key in req.Subscription.DataKeys)
              {
                keys[key] = new object();
              }
              batchRequests.Add(req);
              if (++c == _bufferSize)
              {
                ISubscription sub = base.CreateSubscription(new ArrayList(keys.Keys));
                DSDataRequest dataRequest;
                dataRequest = new DSDataRequest(this, sub, DSDataRequest.RequestType.updates);
                dataRequest.BeginSendRequest(new System.AsyncCallback(this.CPSInfoCallback), new DSDataRequestWrapper(dataRequest, batchRequests));
                c = 0;
                keys = new Hashtable();
                batchRequests = new ArrayList(_bufferSize);
              }
            }
            if (batchRequests.Count > 0)
            {
              ISubscription sub = base.CreateSubscription(new ArrayList(keys.Keys));
              DSDataRequest dataRequest;
              dataRequest = new DSDataRequest(this, sub, DSDataRequest.RequestType.updates);
              dataRequest.BeginSendRequest(new System.AsyncCallback(this.CPSInfoCallback), new DSDataRequestWrapper(dataRequest, batchRequests));
            }
            _pendingCPSInfos = new ArrayList();
          }
        _awaitingCallback = false;
      }
    }

    private void SnapshotCallback(IAsyncResult result_)
    {
      IList requests = (IList) result_.AsyncState;
      try 
      {
        IDictionary snapshot = base.EndSnapshot(result_);
        for (IEnumerator i=requests.GetEnumerator(); i.MoveNext(); )
        {
          BufferedSnapshotRequest req = i.Current as BufferedSnapshotRequest;
          IDictionary subSnapshot = new Hashtable();
          for (IEnumerator j=req.Subscription.DataKeys.GetEnumerator(); j.MoveNext(); )
          {
            IDataKey dataKey = j.Current as IDataKey;
            DataEvent e = snapshot[dataKey] as DataEvent;
            if (e != null)
            {
              subSnapshot[dataKey] = e;
            }
          }
          BufferedDSAsyncResult result = req.Result as BufferedDSAsyncResult;
          result.CompleteAndCallback(subSnapshot);
        }
      }
      catch (Exception ex_)
      {
        for (IEnumerator i=requests.GetEnumerator(); i.MoveNext(); )
        {
          BufferedSnapshotRequest req = i.Current as BufferedSnapshotRequest;
          BufferedDSAsyncResult result = req.Result as BufferedDSAsyncResult;
          result.ErrorAndCallback(ex_);
        }
      }
    }

    private void PublishCallback(IAsyncResult result_)
    {
      IList requests = (IList) result_.AsyncState;
      try 
      {
        IDataKey[] failedKeys = base.EndPublish(result_);
        //Build hash table for speed
        IDictionary keys = new Hashtable(failedKeys.Length);
        foreach (IDataKey key in failedKeys)
        {
          keys[key] = new object();
        }

        for (IEnumerator i=requests.GetEnumerator(); i.MoveNext(); )
        {
          BufferedPublishRequest req = i.Current as BufferedPublishRequest;
          ArrayList datakeys = new ArrayList();
          for (IEnumerator j=req.DataObjects.GetEnumerator(); j.MoveNext(); )
          {
            IDataObject dataObj = j.Current as IDataObject;
            if (keys[dataObj.DataKey] != null)
            {
              datakeys.Add(dataObj.DataKey);
            }
          }
          IDataKey[] resultKeys = (IDataKey[]) datakeys.ToArray(typeof(IDataKey));
          BufferedDSAsyncResult result = req.Result as BufferedDSAsyncResult;
          result.CompleteAndCallback(resultKeys);
        }
      }
      catch (Exception ex_)
      {
        for (IEnumerator i=requests.GetEnumerator(); i.MoveNext(); )
        {
          BufferedPublishRequest req = i.Current as BufferedPublishRequest;
          BufferedDSAsyncResult result = req.Result as BufferedDSAsyncResult;
          result.ErrorAndCallback(ex_);
        }
      }
    }

    private IDictionary BuildFilterMap(string filterString_)
    {
      //split on " OR "
      // extract key from /DSDataUpdate/Key = '' using index of ''
      Hashtable map = new Hashtable();
      StringBuilder currentLine = new StringBuilder();
      StringBuilder currentKey = new StringBuilder();
      bool atKey = false;
      foreach (char c in filterString_)
      {
        if (c == '\'')
        {
          atKey = !atKey;
        }
        else if (atKey)
        {
          currentKey.Append(c);
        }

        if (c == ' ' && currentLine.Length > 3 && currentLine.ToString(currentLine.Length-3,3) == " OR")
        {
          map[currentKey.ToString()] = currentLine.ToString(0, currentLine.Length - 4);
          currentLine.Remove(0, currentLine.Length);
          currentKey.Remove(0, currentKey.Length);
        }
        else 
        {
          currentLine.Append(c);
        }
      }
      if (currentKey.Length > 0)
      {
        map[currentKey.ToString()] = currentLine.ToString();
      }
      return map;
    }
    
    private void CPSInfoCallback(IAsyncResult result_)
    {
      DSDataRequestWrapper wrapper = (DSDataRequestWrapper) result_.AsyncState;
      try 
      {
        DSDataResponse response = wrapper.DSDataRequest.EndSendRequest(result_);
//        ISoapEnvelope env = new SoapEnvelopeDomImpl();
//        ISoapBody body = env.CreateBody();
//        XmlDocument doc = new XmlDocument();
//        XmlElement action = doc.CreateElement("DSDataResponse");
//        XmlElement instructions = doc.CreateElement("DSDataResponse");
//        XmlElement cps = doc.CreateElement("CPS");
//        action.AppendChild(instructions);
//        instructions.AppendChild(cps);
//        XmlElement host = doc.CreateElement("Host");
//        host.InnerText = ;
//        XmlElement port
//        XmlElement topic;
//        XmlElement subscription;
        IDictionary filterMap = BuildFilterMap(response.CPSFilter);
        for (IEnumerator i=wrapper.Requests.GetEnumerator(); i.MoveNext(); )
        {
          BufferedCPSInfoRequest req = i.Current as BufferedCPSInfoRequest;
          //Calculate filter
          StringBuilder filterBuilder = new StringBuilder();
          int counter = 0;
          foreach (IDataKey key in req.Subscription.DataKeys)
          {
            filterBuilder.Append(filterMap[key.KeyString]);
            if (++counter < req.Subscription.DataKeys.Count)
            {
              filterBuilder.Append(" OR ");
            }
          }
          //Build dsdata response
          DSDataResponse resp = new DSDataResponse(response);
          resp.CPSFilter = filterBuilder.ToString();
          
          BufferedDSAsyncResult result = req.Result as BufferedDSAsyncResult;
          result.CompleteAndCallback(resp);
        }
      }
      catch (Exception ex_)
      {
        for (IEnumerator i=wrapper.Requests.GetEnumerator(); i.MoveNext(); )
        {
          BufferedCPSInfoRequest req = i.Current as BufferedCPSInfoRequest;
          BufferedDSAsyncResult result = req.Result as BufferedDSAsyncResult;
          result.ErrorAndCallback(ex_);
        }
      }    
    }

    //Batching publish requests    
    public override IAsyncResult BeginPublish(IDataObject obj_, AsyncCallback callback_, object state_)
    {
      lock (_pendingLock)
      {
        BufferedDSAsyncResult result = new BufferedDSAsyncResult(callback_, state_);
        BufferedPublishRequest req = new BufferedPublishRequest(obj_, result);
        _pendingPublishes.Add(req);
        SetUpCallback();
        return result;
      }
    }

    public override IAsyncResult BeginPublish(IList dataObjects_, AsyncCallback callback_, object state_)
    {
      lock (_pendingLock)
      {
        BufferedDSAsyncResult result = new BufferedDSAsyncResult(callback_, state_);
        BufferedPublishRequest req = new BufferedPublishRequest(dataObjects_, result);
        _pendingPublishes.Add(req);
        SetUpCallback();
        return result;
      }
    }

    public override IAsyncResult BeginSnapshot(ISubscription subscription_, AsyncCallback callback_, object state_)
    {
      if (subscription_.Type == SubType.COLLECTION)
      {
        lock (_pendingLock)
        {
          BufferedDSAsyncResult result = new BufferedDSAsyncResult(callback_, state_);
          BufferedSnapshotRequest req = new BufferedSnapshotRequest(subscription_, result);
          _pendingSnapshots.Add(req);
          SetUpCallback();
          return result;
        }
      }
      else 
      {
        return base.BeginSnapshot(subscription_, callback_, state_);
      }
    }

    public override IDataKey[] EndPublish(IAsyncResult result_)
    {
      result_.AsyncWaitHandle.WaitOne();
      return (IDataKey[]) ((BufferedDSAsyncResult) result_).ResultValue;
    }

    public override IDictionary EndSnapshot(IAsyncResult result_)
    {
      if (result_.GetType() == typeof(BufferedDSAsyncResult))
      {
        result_.AsyncWaitHandle.WaitOne();
        return (IDictionary) ((BufferedDSAsyncResult) result_).ResultValue;
      }
      else
      {
        return base.EndSnapshot(result_);
      }
    }

    //Subscription methods also need to batch there DSDataRequests
    public virtual IAsyncResult BeginGetCPSInfo(ISubscription subscription_, AsyncCallback callback_, object state_)
    {
      lock (_pendingLock)
      {
        BufferedDSAsyncResult result = new BufferedDSAsyncResult(callback_, state_);
        _pendingCPSInfos.Add(new BufferedCPSInfoRequest(subscription_, result));
        SetUpCallback();
        return result;
      }
    }

    public virtual DSDataResponse EndGetCPSInfo(IAsyncResult ar_)
    {
      ar_.AsyncWaitHandle.WaitOne();
      return (DSDataResponse) ((BufferedDSAsyncResult) ar_).ResultValue;
    }

    public override ISubscription CreateSubscription( IList keys_, string userId_ )
    {
      return new BufferedCPSKeySubscription( this, keys_, userId_ );
    }
   
    public override ISubscription CreateSubscription( IDataKey key_, string userId_ )
    {
      if ( key_ is IAliasDataKey)
      {
        return new DSAliasSubscription( this, (IAliasDataKey)key_, userId_ );
      }
      else
      {
        IList keys = new IDataKey[] {key_};
        return new BufferedCPSKeySubscription(this, keys, userId_ );
      }
    }

	}
}
