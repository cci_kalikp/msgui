using System;
using System.IO;
using System.Xml;
using MorganStanley.MSDotNet.MSSoap.Common;

namespace MorganStanley.MSDesktop.Rambo.DataServices.SoapCps
{
	/// <summary>
	/// Summary description for DSVersionRequest.
	/// </summary>
	public class DSVersionRequest : DSSoapRequest
	{
		public DSVersionRequest(IDataService dataService_,IVersionDataKey versionDataKey_)
								: base(dataService_)
		{
      CreateVersionRequestEnvelope(versionDataKey_);
		}

    public DSVersionResponse SendRequest()
    {
      ISoapEnvelope responseEnvelope = base.InternalSendRequest();
      DSVersionResponse response = new DSVersionResponse(responseEnvelope);
      return response;
    }

    private void CreateVersionRequestEnvelope(IVersionDataKey versionDataKey_)
    {
      StringWriter sw = new StringWriter();
      XmlWriter xmlWriter = new XmlTextWriter(sw);
      xmlWriter.WriteStartDocument();
      xmlWriter.WriteStartElement("DSVersionRequest");			
      xmlWriter.WriteElementString( "DataService", _dataService.Id);      		
      xmlWriter.WriteElementString( "Key", versionDataKey_.DSKey );
      xmlWriter.WriteElementString( "DataTime", versionDataKey_.DataTime.ToString() );      
      xmlWriter.WriteElementString( "UpdateTime", versionDataKey_.UpdateTime.ToString() );
      xmlWriter.WriteElementString( "Exact", versionDataKey_.IsExactMatch.ToString() );            	
      xmlWriter.WriteEndDocument();
      xmlWriter.Close();			
      XmlDocument doc = new XmlDocument();
      doc.LoadXml( sw.ToString() );
      
      base.ModifySoapAction(doc.DocumentElement);
    }

	}
}
