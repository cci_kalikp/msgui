using System;
using MorganStanley.MSDotNet.MSNet;
using MorganStanley.MSDotNet.MSLog;
using MorganStanley.MSDotNet.MSSoap.Common;
using MorganStanley.MSDotNet.MSSoap.Client;
using MorganStanley.MSDesktop.Rambo.DataServices.Util;

namespace MorganStanley.MSDesktop.Rambo.DataServices.SoapCps
{
  /// <summary>
  /// This class encapulates communication to configured DataService SOAP servers.
  /// </summary>
  public sealed class DSSoapServer
  {

    private DSSoapServer() { }

    private static Logger m_log = new Logger(typeof(DSSoapServer));

    public static SoapMessage SendRequest(SOAPCPSDataService dataService_, SoapMessage request_)
    {
      return SendRequest(dataService_, request_, DSEnvironment.SoapRequestTimeout);
    }

    public static SoapMessage SendRequest(SOAPCPSDataService dataService_, SoapMessage request_, long timeout_)
    {
      return SendRequest(dataService_, request_, string.Empty, timeout_);
    }

    public static SoapMessage SendRequest(SOAPCPSDataService dataService_, SoapMessage request_, string subDesc_, long timeout_)
    {
      if (dataService_.Servers.IsBackup || dataService_.Servers.Count == 0)
      {
        dataService_.refreshServerList();
      }
      ServerList serverList = new ServerList(dataService_.Servers); //clone
      foreach (DSServer server in serverList)
      {
        ISoapOutputTransport transport = null;
        try
        {
          transport = GetTransport(server.Address);
          if (m_log.WillLog(MSLogPriority.Info))
          {
            m_log.Log(MSLog.Info(), "SendRequest", String.Format("DSServer[{0}]->SyncSendRequest[{1}]: {2}", server.Address, request_.Header.RequestID, request_.Envelope.ToString()));
          }
          SoapMessage response = transport.SyncSendRequest(request_, timeout_);

          if (m_log.WillLog(MSLogPriority.Info))
          {
            m_log.Log(MSLog.Info(), "SendRequest", String.Format("DSServer[{0}]->Response[{1}]: {2}", server.Address, request_.Header.RequestID, response.Envelope.ToString()));
          }
          return response;
        }
        catch (InvalidOperationException ex)
        {
          m_log.Log(MSLog.Error(), "SendRequest", String.Format("DSServer[{0}]->Error[{1}]->DataService[{2}]->RequestDesc[{3}]: ", server.Address, request_.Header.RequestID, dataService_.Id, subDesc_), ex);
          throw new DataServiceException(ex.Message, ex); // There were not enough free threads in the ThreadPool object to complete the operation
        }
        catch (SoapTimeoutException ex_)
        {
          m_log.Log(MSLog.Error(), "SendRequest", String.Format("DSServer[{0}]->Error[{1}]->DataService[{2}]->RequestDesc[{3}]: ", server.Address, request_.Header.RequestID, dataService_.Id, subDesc_), ex_);
          DataServiceManager.ServerManager.RemoveServer(server);
        }
        catch (System.Net.Sockets.SocketException ex_)
        {
          m_log.Log(MSLog.Error(), "SendRequest", String.Format("DSServer[{0}]->Error[{1}]->DataService[{2}]->RequestDesc[{3}]: ", server.Address, request_.Header.RequestID, dataService_.Id, subDesc_), ex_);
          DataServiceManager.ServerManager.RemoveServer(server);
        }
        catch (Exception ex)
        {
           m_log.Log(MSLog.Error(), "SendRequest", String.Format("DSServer[{0}]->Error[{1}]->DataService[{2}]->RequestDesc[{3}]: ", server.Address, request_.Header.RequestID, dataService_.Id, subDesc_), ex);
        }
        finally
        {
          if ( transport != null )
          {
          try
          {
              transport.Stop();
              transport.Destroy();
            }
            catch (Exception ex)
            {
              string tmp = ex.ToString();
            }
          }
            }
            }
      throw new DataServiceException( "Request was not handled by any server" );
          }

    private static ISoapOutputTransport GetTransport(string address_)
    {
      if ( address_ != null ) 
      {
        if ( address_.StartsWith("http://") ) 
          {
          SoapHttpClient httpClient = new SoapHttpClient( null, address_ );
          httpClient.KeepAlive = false;
          return httpClient;
          }
        else if( address_.StartsWith("ktcp://") || address_.StartsWith("tcp://")) 
        {
          string address = address_.StartsWith("ktcp://") ? address_.Substring(7) : address_.Substring(6);
          SoapTCPConnection conn = new SoapTCPConnection(MSNetLoopPoolImpl.Instance(), new MSNetInetAddress(address),
                                                         new MSNetID(Guid.NewGuid().ToString()), false);
          //setup kerberized connection 
          if (address_.StartsWith("ktcp://"))
          {
            conn.Authenticator = new MSNetAuthLayer();
            IMSNetAuthMechanism mech = new MSNetKerberosAuthMechanism(new MSNetKerberosAuthProperties());
            conn.Authenticator.AddAuthMechanism(mech);
          }

          conn.RetryFlag = false;
          conn.SyncConnect();
          return conn;
        }
      }
    
      throw new ArgumentException( "Unsupported protocol in address: " + address_ );
    }
  }
}
