using System;
using System.IO;
using System.Xml;
using MorganStanley.MSDotNet.MSSoap.Common;

namespace MorganStanley.MSDesktop.Rambo.DataServices.SoapCps
{
	/// <summary>
	/// Summary description for DSDataRequest.
	/// </summary>
	public class DSMetaDataRequest : DSSoapRequest
	{
		/// <summary>
		/// Creates a DSMetaDataRequest object will be sent to the DataService Server to get DS metadata information
		/// </summary>
		/// <param name="dataService_">The dataservice</param>
		public DSMetaDataRequest(IDataService dateService_) : base(dateService_)
		{
			CreateMetaDataRequestSoapEnvelope();
		}

		public DSMetaDataResponse SendRequest()
		{
			ISoapEnvelope responseEnvelope = base.InternalSendRequest();
			DSMetaDataResponse response = new DSMetaDataResponse(responseEnvelope);
			return response;
		}

		public new virtual DSDataResponse EndSendRequest(IAsyncResult ar_) 
		{
			return new DSDataResponse(InternalEndSendRequest(ar_));
		}

		private void CreateMetaDataRequestSoapEnvelope()
		{
			StringWriter sw = new StringWriter();
			XmlWriter xmlWriter = new XmlTextWriter(sw);
			xmlWriter.WriteStartDocument();
			xmlWriter.WriteStartElement("DSMetaDataRequest");
			xmlWriter.WriteElementString( "DataService", _dataService.Id);
			xmlWriter.WriteEndDocument();
			xmlWriter.Close();			
			XmlDocument doc = new XmlDocument();
			doc.LoadXml( sw.ToString() );

			base.ModifySoapAction(doc.DocumentElement);
		}
	}
}
