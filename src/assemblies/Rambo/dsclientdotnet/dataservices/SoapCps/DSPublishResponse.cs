using System;
using System.Xml;
using System.Collections;
using MorganStanley.MSDotNet.MSSoap.Common;

namespace MorganStanley.MSDesktop.Rambo.DataServices.SoapCps
{
	/// <summary>
	/// Summary description for DSPublishResponse.
	/// </summary>
	public class DSPublishResponse : DSSoapResponse
	{
    private IList _failedPublishKeys = new ArrayList();

    public DSPublishResponse(ISoapEnvelope responseEnv_) : base(responseEnv_)
    {
      XmlNodeList items = ResponseBodyXml.SelectNodes("//Item");
      if (items != null && items.Count >0)
      {
        foreach(XmlElement itemXml in items)
        {
          string status = itemXml.SelectSingleNode("Status").InnerText;
          if (status == "Error")
          {
            string keyStr = itemXml.SelectSingleNode("Key").InnerText;
            IDataKey dsKey = DataKeyFactory.BuildDataKey(keyStr);
            _failedPublishKeys.Add(dsKey);
          }
        }
      }
    }

    public IDataKey[] FailedPublishKeys
    {
      get
      {
        IDataKey[] failedPublishKeysArr = new IDataKey[_failedPublishKeys.Count];
        if (failedPublishKeysArr.Length > 0)
          _failedPublishKeys.CopyTo(failedPublishKeysArr,0);

        return failedPublishKeysArr;
      }
    }
	}
}
