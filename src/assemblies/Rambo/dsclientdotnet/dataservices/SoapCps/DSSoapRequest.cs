
using System;
using System.Xml;
using System.Text;
using MorganStanley.MSDotNet.MSSoap.Common;
using MorganStanley.MSDesktop.Rambo.AsyncUtils;

namespace MorganStanley.MSDesktop.Rambo.DataServices.SoapCps
{
	/// <summary>
	/// Summary description for DSSoapRequest.
	/// </summary>
  public abstract class DSSoapRequest
  {

    protected delegate ISoapEnvelope SendRequestDelegate();

    protected string SubDesc { private get; set; }
    
    protected ISoapEnvelope _requestEnvelope;

		protected IDataService _dataService;

		protected DSSoapRequest(IDataService dataService_)
    {
			_dataService = dataService_;
      _requestEnvelope = CreateSOAPEnvelope();
    }


    protected SoapMessage SoapRequestMessage
    {
      get { return new SoapMessage(new SoapIOBuffer(_requestEnvelope.GetXmlBytes(Encoding.UTF8))); }
    }

    protected ISoapEnvelope InternalSendRequest()
    {
      return InternalSendRequest(DSEnvironment.SoapRequestTimeout);
    }

    protected ISoapEnvelope InternalSendRequest(long timeout_)
    {
      SoapMessage response = DSSoapServer.SendRequest((SOAPCPSDataService)_dataService, this.SoapRequestMessage, SubDesc, timeout_);

      // NBYRNE - need to decide who should look after Faults
      if ( response.Body.HasFault() )
      {
        throw new DataServiceException( "SOAPFault from server: " +  response.Body.Fault.FaultString  );
      }	
      else
      {
        return response.Envelope;
      }
    }

    public virtual IAsyncResult BeginSendRequest(AsyncCallback callback_, Object state_ )		
    {
      Asynchronizer ssi = new Asynchronizer( callback_, state_, DSEnvironment.DS_THREAD_POOL); 
      return ssi.BeginInvoke( new SendRequestDelegate(this.InternalSendRequest));
    }
   
    public virtual ISoapEnvelope InternalEndSendRequest( IAsyncResult result_ )
    {
      AsynchronizerResult asr = (AsynchronizerResult)result_;
      return asr.SynchronizeInvoke.EndInvoke(result_) as ISoapEnvelope;
    }

    protected void ModifySoapAction(XmlElement action_)
    {
      //Create empty SOAPEnvelope
      SoapEnvelopeDomImpl SoapEnvelope = (SoapEnvelopeDomImpl) _requestEnvelope;
      XmlDocument xmlDOM = SoapEnvelope.XmlDocument;
	
      //Get SOAP body
      ISoapBody body = SoapEnvelope.Body;

      //Add SOAP Action
      XmlElement action =(XmlElement) xmlDOM.ImportNode(action_, true);
      ISoapAction SoapAction = new SoapActionDomImpl(action, SoapEnvelope);
      _requestEnvelope.Body.Action = SoapAction;
    }

    protected ISoapEnvelope CreateSOAPEnvelope()
    {
      ISoapEnvelope SoapEnvelope = new SoapEnvelopeDomImpl("http://schemas.xmlsoap.org/soap/envelope/"); 

      //Setup Header
      SoapHeaderDomImpl header = new SoapHeaderDomImpl((SoapEnvelopeDomImpl) SoapEnvelope);

      header.RequestID = Guid.NewGuid().ToString();
      header.Timestamp = DateTime.Now.Ticks;
      header.AddEntry( this.GetSoapHeaderEntry(header, "UserId", DSEnvironment.USER_NAME ));
      header.AddEntry( this.GetSoapHeaderEntry(header, "MachineName", DSEnvironment.HOST));
      header.AddEntry( this.GetSoapHeaderEntry(header, "Host", DSEnvironment.HOST ));
      header.AddEntry( this.GetSoapHeaderEntry(header, "AppPath", DSEnvironment.APP_PATH));
      header.AddEntry( this.GetSoapHeaderEntry(header, "LibPath", DSEnvironment.LIB_PATH));

      SoapEnvelope.Header = (ISoapHeader) header;

      //Add Body
      SoapEnvelope.CreateBody();

      return SoapEnvelope;
    }

    protected ISoapHeaderEntry GetSoapHeaderEntry(SoapHeaderDomImpl headerFactory_, string name_, string value_)
    {
      ISoapHeaderEntry entry = headerFactory_.CreateEntry(name_);
      entry.Value = value_;

      return entry;
    }
  }
}
