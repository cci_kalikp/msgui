using System;
using System.Xml;
using MorganStanley.MSDotNet.MSSoap.Common;

namespace MorganStanley.MSDesktop.Rambo.DataServices.SoapCps
{
	/// <summary>
	/// Summary description for DSSchemaResponse.
	/// </summary>
	public class DSSchemaResponse : DSSoapResponse
	{
    private string _schema;

		public DSSchemaResponse(ISoapEnvelope responseEnv_) : base(responseEnv_)
		{
      XmlNode schemaNode = ResponseBodyXml.SelectSingleNode("//Schema" ); 

      if ( schemaNode != null )
      {
        _schema = schemaNode.InnerXml;
      }
		}

    public string Schema
    {
      get { return _schema; }
    }
	}
}
