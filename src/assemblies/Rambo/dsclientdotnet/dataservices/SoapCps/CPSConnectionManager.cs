using System;
using System.Xml;
using System.Collections;
using MorganStanley.MSDotNet.MSNet;
using MorganStanley.MSDotNet.MSLog;
using MorganStanley.MSDesktop.Rambo.DataServices.Util;
using MorganStanley.AppMW.CPS;

namespace MorganStanley.MSDesktop.Rambo.DataServices.SoapCps
{
	/// <summary>
	/// Manager class which keeps a cache of all the active CPSPlants and which subscribers
	/// they are currently connected to.
	/// The OnConnect and OnDisconnect event handlers can be used to handle re-subscribing logic
	/// </summary>
	public class CPSSubsciberManager
	{
    public delegate void CPSConnectDelegate(object sender_, CPSPlant plant_);
    public delegate void CPSDisconnectDelegate(object sender_, CPSPlant plant_);

    /// <summary>
    /// Triggered when a CPSPlant has reconnected to a CPS
    /// </summary>
    public event CPSConnectDelegate OnConnected;

    /// <summary>
    /// Triggered by a disconnect from a CPS instance
    /// </summary>
    public event CPSDisconnectDelegate OnDisconnect;

    private static CPSDefaultClientBuilder _cpsClientBuilder = new CPSDefaultClientBuilder();
    private static Logger _logger = new Logger( typeof(CPSSubsciberManager) );

    private object _syncRoot = new object();
    private IDictionary _cpsPlants; //master -> CPSPlant
    private IDictionary _cpsActiveSubscribers; //cps address -> [CPSPlant,...]
    private IDictionary _cpsSubscribers;  //CPSSubscriber by cps address
    private IMSNetLoop _loop;

    public CPSSubsciberManager(IMSNetLoop loop_)
    {
      _loop = loop_;
      _cpsSubscribers = new Hashtable();
      _cpsPlants = new Hashtable();
      _cpsSubscribers = new Hashtable();
      _cpsActiveSubscribers = new Hashtable();
		}

    /// <summary>
    /// Either builds a CPSPlant from the passed addresses or returns the cached
    /// plant for the masterAddress_.
    /// CPSPlant.Subscriber will return the currently active subscriber
    /// </summary>
    /// <param name="masterAddress_">The addres of the plant's master CPS</param>
    /// <param name="clientCPSAddresses_">client CPS addresses if they exist</param>
    /// <returns></returns>
    public CPSPlant GetCPSPlant(string masterAddress_, string[] clientCPSAddresses_)
    {
      lock (_syncRoot)
      {
        CPSPlant plant = _cpsPlants[masterAddress_] as CPSPlant;
        if (plant == null)
        {
          plant = new CPSPlant(this, masterAddress_, clientCPSAddresses_);
          _cpsPlants[plant.MasterAddress] = plant;
          if (plant.Subscriber != null)
          {
            IList list = _cpsActiveSubscribers[plant.ActiveAddress] as IList;
            bool found = false;
            if (list != null)
            {
              foreach (CPSPlant p in list)
              {
                if (p.MasterAddress != plant.MasterAddress)
                {
                  found = true;
                }
              }
            }
            else
            {
              list = new ArrayList();
              _cpsActiveSubscribers[plant.ActiveAddress] = list;
            }
            if (!found)
            {
              list.Add(plant);
            }
          }
        }
        return plant;
      }
    }

    private void LogCPSInfo(ICPSSubscriber subscriber_, string callback_)
    {
      if (_logger.WillLog(MSLogPriority.Info))
      {
        MSNetInetAddress address = new MSNetInetAddress(subscriber_.SubscriberID);
        if (null != address)
          _logger.Log(MSLog.Info(), callback_, GetCPSInfo( subscriber_ ));
        else
          _logger.Log(MSLog.Warning(), callback_, GetCPSInfo( subscriber_ ));
      }
    }

    private string GetCPSInfo(ICPSSubscriber subscriber_)
    {
      MSNetInetAddress address = new MSNetInetAddress(subscriber_.SubscriberID);
      if (null != address)
        return String.Format("CPS[{0}:{1}]", address.HostName, address.Port );
      else
        return String.Format("CPS[CPSSubscriber.Address was not the expected type: {0}]", subscriber_.GetType().ToString());
    }

    private string GetCPSSubscriberName()
    {
      return String.Format("{0}-{1}-{2}", DSEnvironment.HOST, DSEnvironment.PID, Guid.NewGuid().ToString());
    }

    /// <summary>
    /// Retrives a cached subscriber or builds one for the passed hostport_ if one doesn't already exist
    /// </summary>
    /// <param name="hostport_">the CPS address</param>
    /// <returns>a connected CPSSubscriber or null if it couldn't connect to any</returns>
    public CPSSubscriber GetCPSSubscriber(string hostport_)
    {
      lock (_syncRoot)
      {
        CPSSubscriber subscriber = _cpsSubscribers[hostport_] as CPSSubscriber;
        if (subscriber == null)
        {
          //Attempt to connect if failed log and return null
          try
          {
            subscriber = _cpsClientBuilder.BuildSubscriber(_loop, hostport_, GetCPSSubscriberName());
            subscriber.RetryFlag = false;
            subscriber.SubscriberID = hostport_;
            subscriber.SyncConnect();
            // if connect successfully then setup delegates
            subscriber.OnDisconnect += new MSNetDisconnectDelegate(CPSDisconnectCallback);
            subscriber.OnError += new MSNetErrorDelegate(CPSErrorCallback);
            subscriber.OnRetry += new MSNetRetryDelegate(CPSRetryCallback);
            subscriber.OnTransientDisconnect += new MSNetTransientDisconnectDelegate(CPSTransientDisconnectCallback);
            subscriber.OnTransientReconnect += new MSNetTransientReconnectDelegate(CPSTransientReconnectCallback);
            subscriber.OnTransientRetry += new MSNetTransientRetryDelegate(CPSTransientRetryCallback);
            string logMsg = String.Format("CPSSubscriber[{0}] Connected to CPS", hostport_);
            _logger.Log(MSLog.Info(), "GetCPSSubscriber", logMsg);
            _cpsSubscribers[hostport_] = subscriber;
          }
          catch (Exception ex_) 
          {
            string logMsg = String.Format("CPSSubscriber[{0}] Failed to connect to CPS as: {1}", hostport_, ex_.ToString());
            _logger.Log(MSLog.Error(), "GetCPSSubscriber", logMsg);
            return null;
            //throw new DSCPSException("Couldn't connect to CPS ("+hostPort_+") ", ex_);
          }
        }
        return subscriber;
      }
    }

    #region Handlers
    
    /// <summary>
    /// Called by a CPSPlant after it has successfully failed over to a CPS instance
    /// </summary>
    /// <param name="plant_">The plant which has failedover</param>
    public void CPSConnected(CPSPlant plant_)
    {
      _logger.Log(MSLog.Info(), "CPSConnected", "CPS["+plant_.ActiveAddress+"] Connected: for plant "+plant_.MasterAddress);
      lock (_syncRoot)
      {
        IList list = _cpsActiveSubscribers[plant_.ActiveAddress] as IList;
        if (list == null)
        {
          list = new ArrayList();
          _cpsActiveSubscribers[plant_.ActiveAddress] = list;
        }
        list.Add(plant_);
      }
      if (OnConnected != null)
      {
        OnConnected(this, plant_);
      }
    }

    /// <summary>
    /// CPS disconnect handler
    /// </summary>
    private void CPSDisconnectCallback(object sender_, MSNetDisconnectEventArgs args_)
    {
      string address = GetAddressFromSender(sender_);
      if (address != null)
      {
        _logger.Log(MSLog.Warning(), "CPSDisconnectCallback", "CPS["+address+"] Disconnected: "+args_.ID.ToString());
        lock (_syncRoot)
        {
          IList plants = _cpsActiveSubscribers[address] as IList;
          if (plants != null && OnConnected != null)
          {
            foreach (CPSPlant plant in plants)
            {
              OnDisconnect(this, plant);
            }
          }
          CPSSubscriber sub = _cpsSubscribers[address] as CPSSubscriber;
          sub.Close();
          _cpsActiveSubscribers.Remove(address);
          _cpsSubscribers.Remove(address);
          if (plants != null)
          {
            foreach (CPSPlant plant in plants)
            {
              plant.Failover();
            }
          }
        }
      }
      else
      {
        _logger.Log(MSLog.Warning(), "CPSDisconnectCallback", "Disconnect callback on a non-CPSSubscriber object");
      }
    }

    /// <summary>
    /// Logs a CPS error
    /// </summary>
    private void CPSErrorCallback(object sender_, MSNetErrorEventArgs args_)
    {
      string address = GetAddressFromSender(sender_);
      if (address != null)
      {
        _logger.Log(MSLog.Warning(),"CPSErrorCallback", "CPS["+address+"] Error on "+args_.ID.ToString());
        if (_logger.WillLog(MSLogPriority.Warning))
        {
          _logger.Log(MSLog.Warning(), "CPSErrorCallback", args_.Error.ToString(), args_.Exception);
        }
      }
      else
      {
        _logger.Log(MSLog.Warning(), "CPSErrorCallback", "Error callback on a non-CPSSubscriber object");
      }
    }

    private string GetAddressFromSender(object sender_)
    {
      CPSSubscriber subscriber = sender_ as CPSSubscriber;
      if (null == subscriber)
        return null;
      else
        return subscriber.Address.ToString();
    }

    /////////////////////////////////////////Not valid with new logic/////////////////////////

    private void CPSRetryCallback(object sender_, MSNetRetryEventArgs args_)
    {
      string address = GetAddressFromSender(sender_);
      _logger.Log(MSLog.Debug(),"CPSRetryCallback", "CPS["+address+"] Retry on "+args_.ID.ToString());
    }

    private void CPSTransientDisconnectCallback(object sender_, MSNetTransientDisconnectEventArgs args_)
    {
      _logger.Log(MSLog.Debug(),"CPSTransientDisconnectCallback",args_.ID.ToString());
    }

    private void CPSTransientReconnectCallback(object sender_, MSNetTransientReconnectEventArgs args_)
    {
      _logger.Log(MSLog.Debug(),"CPSTransientReconnectCallback",args_.ID.ToString());
    }

    private void CPSTransientRetryCallback(object sender_, MSNetTransientRetryEventArgs args_)
    {
      _logger.Log(MSLog.Debug(),"CPSTransientRetryCallback",args_.ID.ToString());
    }
    #endregion

    public IMSNetLoop Loop
    {
      get { return _loop; }
    }

    /// <summary>
    /// Returns all the plants in the manager's cache
    /// </summary>
    public ICollection Plants
    {
      get
      {
        lock (_syncRoot)
        {
          IList plants;
          plants = new ArrayList(_cpsPlants.Values);
          return plants;
        }
      }
    }
	}
}
