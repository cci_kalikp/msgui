using System;
using System.Xml;
using System.Collections;
using MorganStanley.MSDotNet.MSSoap.Common;

namespace MorganStanley.MSDesktop.Rambo.DataServices.SoapCps
{
	/// <summary>
	/// Summary description for DSMetaDataResponse.
	/// </summary>
	public class DSMetaDataResponse : DSSoapResponse
	{
    private XMLMetaDataObject _metaDataObject = null;

		public DSMetaDataResponse(ISoapEnvelope responseEnv_): this(((SoapBodyDomImpl) responseEnv_.Body).XmlElement)
		{
		}

		internal DSMetaDataResponse(XmlElement responseBodyXml_) : base(responseBodyXml_)
		{
      XmlElement itemElement = responseBodyXml_.SelectSingleNode("//MetaData") as XmlElement;
      if (itemElement != null)
      {
        _metaDataObject = new XMLMetaDataObject(itemElement);
      }
		}

    public IMetaDataObject MetaDataObject
    {
      get
      {
        return _metaDataObject;
      }
    }
	}
}
