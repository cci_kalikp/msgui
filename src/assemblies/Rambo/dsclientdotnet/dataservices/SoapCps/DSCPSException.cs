using System;

namespace MorganStanley.MSDesktop.Rambo.DataServices.SoapCps
{
  /// <summary>
  /// Summary description for DSCPSException.
  /// </summary>
  public class DSCPSException: DataServiceException
  {
    public DSCPSException(String message_):base(message_)
    {
    }

    public DSCPSException(String message_,Exception inner_):base(message_,inner_)
    {
    }		
  }
}
