using System;
using MorganStanley.MSDotNet.MSSoap.Common;

namespace MorganStanley.MSDesktop.Rambo.DataServices.SoapCps
{
	/// <summary>
	/// Summary description for DSVersionDatesRangeResponse.
	/// </summary>
	public class DSVersionDatesRangeResponse : DSVersionDatesResponse
	{
		public DSVersionDatesRangeResponse(ISoapEnvelope envelope_)
			:base(envelope_)
		{
		}
	}
}
