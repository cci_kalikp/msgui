using System;
using System.Collections;
using MorganStanley.AppMW.CPS;
using MorganStanley.MSDotNet.MSLog;
using MorganStanley.MSDotNet.MSNet;
using MorganStanley.MSDesktop.Rambo.DataServices.Util;

namespace MorganStanley.MSDesktop.Rambo.DataServices.SoapCps
{

  /// <summary>
  /// A CPSPlant represents a master->clients CPS plant where clients only connect to the client
  /// cps instances (unless there is only a master cps in which case it will connect to that).
  /// The class manages which CPSSubscriber is currently in use and performs Failover and Backout
  /// logic.
  /// </summary>
  public class CPSPlant 
  {
    private static int[] BACKOUT_TIMES = new int[]{1000,1000,5000,10000,20000,30000,60000};
    public const int REPORT_LIMIT = 3;
    private static Random _random = new Random(DateTime.Now.Millisecond);
    private string _masterAddress;
    private string[] _clientAddresses;
    private int _activeAddressIndex;
    private CPSSubscriber _subscriber;
    private int _retryCount;
    private IList _dataServices;
    private object _syncRoot = new object();
    private CPSSubsciberManager _cpsManager;
    private static Logger _log = new Logger(typeof(CPSPlant));

    public CPSPlant(CPSSubsciberManager cpsManager_, string masterAddress_, string[] clientAddresses_)
    {
      _cpsManager = cpsManager_;
      _masterAddress = masterAddress_;
      if (clientAddresses_ != null)
      {
        _clientAddresses = GenerateRandomList(clientAddresses_);
      }
      _retryCount = 0;
      _dataServices = new ArrayList();
      _activeAddressIndex = 0;
      //Connect to the Subscriber
      if (IsPlant)
      {
        int c = 0;
        while (_subscriber == null && c<_clientAddresses.Length)
        {
          _subscriber = _cpsManager.GetCPSSubscriber(_clientAddresses[_activeAddressIndex]);
          c++;
          if (_subscriber == null)
          {
            _activeAddressIndex = (_activeAddressIndex + 1) % _clientAddresses.Length;
          }
        }
      }
      else
      {
        _subscriber = _cpsManager.GetCPSSubscriber(_masterAddress);
      }
      if (_subscriber == null)
      {
        //If failed to connect to any CPSs backoff
        _log.Log(MSLog.Error(), "CPSPlant","Couldn't connect to any of the CPS servers for plant: "+_masterAddress);
        Backoff();
      }
    }

    /// <summary>
    /// returns a copy of the passed array randomly shuffled
    /// </summary>
    private string[] GenerateRandomList(string[] list_)
    {
      string[] list = new string[list_.Length];
      for (int i=0; i<list_.Length; ++i)
      {
        int r = _random.Next(list_.Length);
        int tries = 0; //Ensure it can't get stuck here forever
        while (list[r] != null && tries < list_.Length*5)
        {
          r = _random.Next(list_.Length);
          ++tries;
        }
        if (tries == list_.Length*5)
        {
          r = 0;
          while (list[r] != null) ++r;
        }
        list[r] = list_[i];
      }

      return list;
    }

    /// <summary>
    /// The address of the Master CPS instance
    /// </summary>
    public string MasterAddress
    {
      get { return _masterAddress; }
    }

    /// <summary>
    /// The addresses of the client CPSs
    /// </summary>
    public string[] ClientAddresses
    {
      get { return _clientAddresses; }
    }

    /// <summary>
    /// Whether or not the current configuration represents a plant or a single CPS
    /// </summary>
    public bool IsPlant
    {
      get { return _clientAddresses != null && _clientAddresses.Length > 0; }
    }

    /// <summary>
    /// The address of the CPS instance currently in use
    /// </summary>
    public string ActiveAddress
    {
      get
      {
        lock (_syncRoot)
        {
          if (_subscriber != null)
          {
            return _subscriber.Address.ToString();
          }
          else
          {
            return null;
          }
        }
      }
    }

    /// <summary>
    /// Attempts to reconnect to another CPS instance in the plant. If it cannot connect
    /// to any of the client CPSs it backsoff for a period.
    /// </summary>
    public void Failover()
    {
      _log.Log(MSLog.Info(), "Failover","Failing over CPS plant: "+_masterAddress);
      lock (_syncRoot)
      {
        _subscriber = null;
        if (IsPlant)
        {
          _activeAddressIndex = (_activeAddressIndex + 1) % _clientAddresses.Length;
          int c = 0;
          while (_subscriber == null && c<_clientAddresses.Length)
          {
            _subscriber = _cpsManager.GetCPSSubscriber(_clientAddresses[_activeAddressIndex]);
            c++;
            if (_subscriber == null)
            {
              _activeAddressIndex = (_activeAddressIndex + 1) % _clientAddresses.Length;
            }
          }
        }
        else
        {
          _subscriber = _cpsManager.GetCPSSubscriber(_masterAddress);
        }
        if (_subscriber == null)
        {
          ++_retryCount;
          if (_retryCount == REPORT_LIMIT)
          {
            string msg = "Connection lost to CPS Plant: "+MasterAddress;
            DataServiceManager.ReportStatus(DataServices, DSStatusType.CPSError, msg);
          }
          Backoff();
        }
        else
        {
          if (_retryCount >= REPORT_LIMIT)
          {
            string msg = "Connection Re-established to CPS Plant "+MasterAddress;
            DataServiceManager.ReportStatus(DataServices, DSStatusType.CPSReconnect, msg);
          }
          _retryCount = 0;
          //Alert Subscription Manager of successful re-connection
          _cpsManager.CPSConnected(this);
        }
      }
    }

    /// <summary>
    /// Adds a callback to the net loop to run the Failover method after a delay determined by
    /// the number of attempts which have been made.
    /// </summary>
    private void Backoff()
    {
      long delay;
      if (_retryCount < BACKOUT_TIMES.Length)
      {
        delay = BACKOUT_TIMES[_retryCount];
      }
      else
      {
        delay = BACKOUT_TIMES[BACKOUT_TIMES.Length-1];
      }
      _log.Log(MSLog.Info(),"Backoff","sleeping for "+(delay/1000)+"s before retrying CPS plant "+_masterAddress);
      _cpsManager.Loop.CallbackAfterDelay(delay, new MSNetTimerDelegate(FailoverCallback));
    }

    private void FailoverCallback(object sender_, MSNetTimerEventArgs args_)
    {
      lock (_syncRoot)
      {
        Failover();
      }
    }

    /// <summary>
    /// Records a CPSSubscription for the passed dataservice is active on the CPSPlant
    /// </summary>
    public void AddDataService(string dataService_) 
    {
      lock (_syncRoot) 
      { 
        for (IEnumerator i=_dataServices.GetEnumerator(); i.MoveNext(); ) 
        {
          DSItem item = i.Current as DSItem;
          if (item.Name == dataService_) 
          {
            ++item;
            return;
          }
        }
        _dataServices.Add(new DSItem(dataService_));
      }
    }

    public void RemoveDataService(string dataService_) 
    {
      lock (_syncRoot) 
      {
        for (IEnumerator i=_dataServices.GetEnumerator(); i.MoveNext(); ) 
        {
          DSItem item = i.Current as DSItem;
          if (item.Name == dataService_) 
          {
            --item;
            if (item.Count == 0) 
            {
              _dataServices.Remove(item);
              return;
            }
          }
        }
      }
    }

    public CPSSubscriber Subscriber 
    {
      get
      {
        lock (_syncRoot)
        {
          return _subscriber;
        }
      }
    }

    public object SyncRoot
    {
      get { return _syncRoot; }
    }

    public string[] DataServices 
    {
      get 
      {
        lock (_syncRoot) 
        {
          string[] array = new string[_dataServices.Count];
          int i=0;
          foreach (DSItem item in _dataServices) 
          {
            array[i++] = item.Name;
          }
          return array;
        }
      }
    }

    private class DSItem 
    {
      private string _dsname;
      private int _count;

      public DSItem(string dsname_)
      {
        _dsname = dsname_;
        _count = 1;
      }

      public static DSItem operator++(DSItem x_) 
      {
        ++x_._count;
        return x_;
      }

      public static DSItem operator--(DSItem x_)
      {
        --x_._count;
        return x_;
      }

      public string Name 
      {
        get { return _dsname; }
      }

      public int Count 
      {
        get { return _count; }
      }
    }
  }
}
