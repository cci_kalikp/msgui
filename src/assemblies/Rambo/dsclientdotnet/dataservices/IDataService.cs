//---------------------------------------------------------------------------
// ($Id $)
//---------------------------------------------------------------------------
//
// Copyright (c) 2003 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright.  This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
//---------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;

namespace MorganStanley.MSDesktop.Rambo.DataServices
{
  /// <summary>
  /// The typical responsibilities of a DataService are too create subscriptions, provide snapshots ( given a subscription created 
  /// by the same dataservice ) and to support publishing of updates on a DataService key. The subscription class encapsulates 
  /// the actual details of individual callbacks.
  /// 
  /// Some DataServices support the concept of versioning data.  This requires the data to be versioned in its persistent store
  /// and as a result is not available by default.
  /// </summary>
  public interface IDataService
  {
    /// <summary>
    /// DataService identifier
    /// </summary>
    /// <returns>a unique identifer for this DataService</returns>	 
    string Id { get; }

    bool ServerSubFiltering { get; set; }

    /// <summary>
    /// The server list attached to this data service.
    /// </summary>
		SoapCps.ServerList Servers 
		{
			get;
		}

    void refreshServerList();

    #region CreateSubscription operations
    /// <summary>
    /// For creating key subscriptions.
    /// </summary>
    /// <exception cref="MorganStanley.MSDesktop.Rambo.DataServices.SubscriptionException">
    /// if subscription could not be created for any reason
    /// </exception>
    /// <returns>an identity subscription</returns>
    ISubscription CreateSubscription( IDataKey key_ );

    /// <summary>
    /// For creating key subscriptions.
    /// </summary>
    /// <exception cref="MorganStanley.MSDesktop.Rambo.DataServices.SubscriptionException">
    /// if subscription could not be created for any reason
    /// </exception>
    /// <returns>an identity subscription</returns>
    ISubscription CreateSubscription( IDataKey key_, string userId_ );

    /// <summary>
    /// For creating key-set subscriptions
    /// </summary>
    /// <exception cref="MorganStanley.MSDesktop.Rambo.DataServices.SubscriptionException">
    /// if subscription could not be created for any reason
    /// </exception>
    /// <returns>a key-set subscription</returns>		 
    [Obsolete("Use generic CreateSubscription() instead")]
    ISubscription CreateSubscription( IList keys_ );

    /// <summary>
    /// For creating key-set subscriptions
    /// </summary>
    /// <exception cref="MorganStanley.MSDesktop.Rambo.DataServices.SubscriptionException">
    /// if subscription could not be created for any reason
    /// </exception>
    /// <returns>a key-set subscription</returns>		 
    ISubscription CreateBatchSubscription(IEnumerable<IDataKey> keys_);

    /// <summary>
    /// For creating key-set subscriptions
    /// </summary>
    /// <exception cref="MorganStanley.MSDesktop.Rambo.DataServices.SubscriptionException">
    /// if subscription could not be created for any reason
    /// </exception>
    /// <returns>a key-set subscription</returns>		 
    ISubscription CreateSubscription( IList keys_, string userId_ );


    /// <summary>
    /// For creating content or attribute subscriptions
    /// </summary>
    /// <exception cref="MorganStanley.MSDesktop.Rambo.DataServices.SubscriptionException">
    /// if subscription could not be created for any reason
    /// </exception>
    /// <returns>a content subscription</returns>	
    ISubscription CreateSubscription( IDictionary dict_ );

    /// <summary>
    /// For creating content or attribute subscriptions
    /// </summary>
    /// <exception cref="MorganStanley.MSDesktop.Rambo.DataServices.SubscriptionException">
    /// if subscription could not be created for any reason
    /// </exception>
    /// <returns>a content subscription</returns>	
    ISubscription CreateSubscription(IDictionary dict_, bool strictAttributeNames); 

    /// <summary>
    /// For creating content subscriptions
    /// </summary>
    /// <exception cref="MorganStanley.MSDesktop.Rambo.DataServices.SubscriptionException">
    /// if subscription could not be created for any reason
    /// </exception>
    /// <returns>a content subscription</returns>	
    ISubscription CreateSubscription( IDictionary dict_, string userId_ );

    /// <summary>
    /// For creating content subscriptions
    /// </summary>
    /// <exception cref="MorganStanley.MSDesktop.Rambo.DataServices.SubscriptionException">
    /// if subscription could not be created for any reason
    /// </exception>
    /// <returns>a content subscription</returns>	
    ISubscription CreateSubscription(IDictionary dict_, string userId_, bool strictAttributeNames); 

    /// <summary>
    /// A global subscription.
    /// </summary>
    /// <exception cref="MorganStanley.MSDesktop.Rambo.DataServices.SubscriptionException">
    /// if subscription could not be created for any reason
    /// </exception>
    /// <returns>a global subscription</returns>		 
    ISubscription GlobalSubscription();

    /// <summary>
    /// A global subscription.
    /// </summary>
    /// <exception cref="MorganStanley.MSDesktop.Rambo.DataServices.SubscriptionException">
    /// if subscription could not be created for any reason
    /// </exception>
    /// <returns>a global subscription</returns>		 
    ISubscription GlobalSubscription( string userId_ );

    #endregion
		
    /// <summary>
    /// For Creating keys
    /// </summary>
    /// <exception cref="MorganStanley.MSDesktop.Rambo.DataServices.DataServiceException">
    /// if key could not be created 
    /// </exception>
    /// <returns>a valid key with which subscriptions can be made</returns>
    [Obsolete("Use the operations on the class DataKeyFactory to generate IDataKeys",false)]
    IDataKey CreateDataKey( Object key_ );

    
    #region Publish operations

    /// <summary>
    /// Publishes the IDataObject synchronously
    /// </summary>    
    /// <param name="obj_">object to publish</param>
    /// <exception cref="MorganStanley.MSDesktop.Rambo.DataServices.PublishException">
    /// thrown if any problems.
    /// </exception>
    void Publish( IDataObject obj_ ); 
		
    /// <summary>
    /// Publishes the list of DataObjects synchronously (Multi publish)
    /// </summary>
    /// <param name="dataObjects_">list of dataObjects</param>
    /// <returns>IDataKey[] containing keys of any failed publishes or empty array if all successful</returns>
    [Obsolete("Use PublishBatch() instead")]
    IDataKey[] Publish( IList dataObjects_ );

    /// <summary>
    /// Publishes the list of DataObjects synchronously (Multi publish)
    /// </summary>
    /// <param name="dataObjects_">list of dataObjects</param>
    /// <returns>IDataKey[] containing keys of any failed publishes or empty array if all successful</returns>
    IEnumerable<IDataKey> PublishBatch(IEnumerable<IDataObject> dataObjects_);

    /// <summary>
    /// Starts publishing the given object asynchronously, following the .NET async programming pattern.
    /// </summary>
    IAsyncResult BeginPublish( IDataObject obj_, AsyncCallback acb_, Object state_ );		
	
    /// <summary>
    /// Starts publishing the given objects asynchronously, following the .NET async programming pattern.
    /// </summary>
    IAsyncResult BeginPublish( IList dataObjects_, AsyncCallback acb_, Object state_ );		

    /// <summary>
    /// Ends an asynchronously publish, following the .NET async programming pattern.
    /// </summary>
    /// <param name="result_"></param>
    /// <returns>
    /// If EndPublish being called for a batch publish then returns an IDataKey[] containing keys of any failed publishes or empty array if all successful.
    /// </returns>
    /// <exception cref="MorganStanley.MSDesktop.Rambo.DataServices.PublishException">
    /// thrown if any problems.
    /// </exception>
    IDataKey[] EndPublish( IAsyncResult result_ );

    #endregion

    #region Snapshot operations

    /// <summary>
    /// returns the most current version of the given key
    /// </summary>
    DataEvent GetSnapshot( IDataKey key_ );

    /// <summary>
    /// Retrieves a snapshot of the IDataObjects that match the given subscription.
    /// </summary>
    /// <exception cref="MorganStanley.MSDesktop.Rambo.DataServices.DataServiceException">
    /// if any problems.
    /// </exception>   
    /// <returns>Dictionary of DataKey --> DataEvent</returns>	 
    [Obsolete("Use GetSnapshots() instead")]
    IDictionary GetSnapshot( ISubscription subscription_ );

    /// <summary>
    /// Retrieves a snapshot of the IDataObjects that match the given subscription.
    /// </summary>
    /// <exception cref="MorganStanley.MSDesktop.Rambo.DataServices.DataServiceException">
    /// if any problems.
    /// </exception>   
    /// <returns>Dictionary of DataKey --> DataEvent</returns>	 
    IDictionary<IDataKey, DataEvent> GetSnapshots(ISubscription subscription_);

    /// <summary>
    /// Start subscribing for a snapshot asynchronusly, following the .NET async programming pattern
    /// </summary>
    IAsyncResult BeginSnapshot( ISubscription subscription_, AsyncCallback callback_, Object state_);

    /// <summary>
    /// Ends an asynchronous snapshot, following the .NET async programming pattern
    /// </summary>
    /// <returns>A dictionary containing the snapshot of data matching the subscription</returns>
    /// <exception cref="MorganStanley.MSDesktop.Rambo.DataServices.SubscriptionException">thrown if any problems</exception>
    IDictionary EndSnapshot( IAsyncResult result );

    #endregion

    /// <summary>
    /// Returns a specific version of the given key or null if not found
    /// </summary> 
    /// <exception cref="MorganStanley.MSDesktop.Rambo.DataServices.DataServiceException">
    /// if any problems
    /// </exception>
    [Obsolete("Use 'GetVersionList' and 'GetVersion' operations instead",false)]
    DataEvent GetSnapshot( IDataKey key_, DateTime version_ );
    
    /// <summary>
    /// returns all known versions for the given key
    /// </summary> 
    /// <exception cref="MorganStanley.MSDesktop.Rambo.DataServices.DataServiceException">
    /// if any problems
    /// </exception>
    [Obsolete("Use the operation 'GetVersionList' instead",false)]
    DateTime[] GetVersions ( IDataKey key_ );

    #region GetDataKeys operations
		/// <summary>
		/// If versioning is support by the underlying DataService then:
		/// Returns an array of all the IDataKeys that have versioned data
		/// </summary>
		IDataKey[] GetDataKeys();

    /// <summary>
    /// If versioning is support by the underlying DataService then:
    /// Returns an array of all the IDataKeys that have versioned data
    /// </summary>
    /// <param name="startDataTime_">The dataTime to start the search from</param>
    /// <param name="endDataTime_">The dataTime to end the search at</param>
    IDataKey[] GetDataKeys( DataServiceDate startDataTime_, DataServiceDate endDataTime_ );

    /// <summary>
    /// If versioning is support by the underlying DataService then:
    /// Returns an array of all the IDataKeys that have versioned data and the key components match
    /// those of the attribute_ dictionary provided
    /// </summary>
    /// <param name="attributes_">The keys of this dictionary must match the key components of this DataService</param>
    IDataKey[] GetDataKeys( IDictionary attributes_ );

    /// <summary>
    /// If versioning is support by the underlying DataService then:
    /// Returns an array of all the IDataKeys that have versioned data and the key components match
    /// those of the attribute_ dictionary provided
    /// </summary>
    /// <param name="attributes_">The keys of this dictionary must match the key components of this DataService</param>
    /// <param name="startDataTime_">The dataTime to start the search from</param>
    /// <param name="endDataTime_">The dataTime to end the search at</param>
    IDataKey[] GetDataKeys( IDictionary attributes_, DataServiceDate startDataTime_, DataServiceDate endDataTime_ );

		/// <summary>
    /// If versioning is support by the underlying DataService then:
    /// Returns an array of all the IDataKeys for the key pattern e.g. %/value/%
		/// </summary>
		/// <param name="attributes_">key pattern to lookup</param>
		IDataKey[] GetDataKeys(IDataKey key_);

    /// <summary>
    /// If versioning is support by the underlying DataService then:
    /// Returns an array of all the IDataKeys for the key pattern e.g. %/value/%
    /// </summary>
    /// <param name="attributes_">key pattern to lookup</param>
    /// <param name="startDataTime_">The dataTime to start the search from</param>
    /// <param name="endDataTime_">The dataTime to end the search at</param>
    IDataKey[] GetDataKeys(IDataKey key_, DataServiceDate startDataTime_, DataServiceDate endDataTime_ );

    /// <summary>
    /// Start DataKeys request asynchronusly, following the .NET async programming pattern
    /// </summary>
    IAsyncResult BeginGetDataKeys( AsyncCallback callback_, Object state_);

    /// <summary>
    /// Start DataKeys request asynchronusly, following the .NET async programming pattern
    /// </summary>
    IAsyncResult BeginGetDataKeys( DataServiceDate startDataTime_, DataServiceDate endDataTime_, AsyncCallback callback_, Object state_ );

    /// <summary>
    /// Start DataKeys request asynchronusly, following the .NET async programming pattern
    /// </summary>
    IAsyncResult BeginGetDataKeys( IDictionary attributes_, AsyncCallback callback_, Object state_ );

    /// <summary>
    /// Start DataKeys request asynchronusly, following the .NET async programming pattern
    /// </summary>
    IAsyncResult BeginGetDataKeys( IDictionary attributes_, DataServiceDate startDataTime_, DataServiceDate endDataTime_, AsyncCallback callback_, Object state_ );

    /// <summary>
    /// Start DataKeys request asynchronusly, following the .NET async programming pattern
    /// </summary>
    IAsyncResult BeginGetDataKeys(IDataKey key_, AsyncCallback callback_, Object state_ );

    /// <summary>
    /// Start DataKeys request asynchronusly, following the .NET async programming pattern
    /// </summary>
    IAsyncResult BeginGetDataKeys(IDataKey key_, DataServiceDate startDataTime_, DataServiceDate endDataTime_, AsyncCallback callback_, Object state_ );    

    /// <summary>
    /// Ends an asynchronous DataKeys reqeust, following the .NET async programming pattern
    /// </summary>
    /// <returns>An array of DataKey objects matching of request or null of none exist</returns>
    /// <exception cref="MorganStanley.MSDesktop.Rambo.DataServices.SubscriptionException">thrown if any problems</exception>
    IDataKey[] EndGetDataKeys( IAsyncResult result );

    #endregion

    #region Version/Alias API
		
		/// <summary>
    /// If versioning is support by the underlying DataService then:
    /// Retrieves all the IVersionDataKeys for the IDataKey provided
    /// If no versions are found null is returned
    /// </summary>
    /// <param name="key_">The key to find the versions for</param>
    /// <param name="sort_">Whether you want the results sorted</param>
    /// <returns>A collection of IVersionDataKeys</returns>
    VersionDataKeyCollection GetVersionsList ( IDataKey key_, bool sort_ );

		/// <summary>
		/// If versioning is support by the underlying DataService then:
		/// Retrieves all the IVersionDataKeys for the IDataKey provided in the passed date range
		/// If no versions are found null is returned
		/// </summary>
		/// <param name="key_">The key to find the versions for</param>
		/// <param name="startDataTime_">The starting date of the range</param>
		/// <param name="endDataTime_">The ending date of the range</param>
		/// <returns>A collection of IVersionDataKeys</returns>
		VersionDataKeyCollection GetVersionsList( IDataKey key_, DataServiceDate startDataTime_, DataServiceDate endDataTime_ );

    /// <summary>
    /// Start VersionList request asynchronusly, following the .NET async programming pattern
    /// </summary>
    IAsyncResult BeginGetVersionsList( IDataKey key_, bool sort_, AsyncCallback callback_, Object state_ );    

    /// <summary>
    /// Start VersionList request asynchronusly, following the .NET async programming pattern
    /// </summary>
    IAsyncResult BeginGetVersionsList( IDataKey key_, DataServiceDate startDataTime_, DataServiceDate endDataTime_, AsyncCallback callback_, Object state_ );

    /// <summary>
    /// Ends an asynchronous VersionList request, following the .NET async programming pattern
    /// </summary>
    /// <returns>VersionDataKeyCollection of IVersionDataKeys matching the request or null if none exist</returns>
    /// <exception cref="MorganStanley.MSDesktop.Rambo.DataServices.SubscriptionException">thrown if any problems</exception>
    VersionDataKeyCollection EndGetVersionsList( IAsyncResult result );

    /// <summary>
    /// If versioning is support by the underlying DataService then:
    /// Returns a DataEvent with the versioned data matching that of the IVersionKey provided or
    /// null if no matching data was found
    /// </summary>
    /// <param name="key_">The key of the data you are looking for</param>
    DataEvent GetVersion ( IVersionDataKey key_ );

    /// <summary>
    /// Start Version request asynchronusly, following the .NET async programming pattern
    /// </summary>
    IAsyncResult BeginGetVersion( IVersionDataKey key_, AsyncCallback callback_, Object state_ );

    /// <summary>
    /// Ends an asynchronous Version request, following the .NET async programming pattern
    /// </summary>
    /// <returns>
    /// Returns a DataEvent with the versioned data matching that of the IVersionKey provided or
    /// null if no matching data was found<
    /// /returns>
    /// <exception cref="MorganStanley.MSDesktop.Rambo.DataServices.SubscriptionException">thrown if any problems</exception>
    DataEvent EndGetVersion( IAsyncResult result );

    /// <summary>
    /// To retrieve the IVersionDataKey that the IAliasDataKey is currently pointing to
    /// </summary>
    IVersionDataKey GetVersionDataKey( IAliasDataKey aliasDataKey_ );

    /// <summary>
    /// If versioning is support by the underlying DataService then:
    /// Moves the specified alias to the new version of the data
    /// </summary>
    /// <exception cref="MorganStanley.MSDesktop.Rambo.DataServices.DataServiceException">
    /// If there is any problem with this operation
    /// </exception>
    /// <param name="aliasDataKey_"></param>
    /// <param name="newVersionKey_"></param>
    void TurnoverAlias(IAliasDataKey aliasDataKey_, IVersionDataKey newVersionKey_);

    #endregion

	  /// <summary>
	  /// Retrieves metadata information.
	  /// </summary>
	  /// </exception>
	  /// <returns>MetaData object</returns>	 
	  IMetaDataObject GetMetaData();

    /// <summary>
    /// Start subscribing for a metadata request asynchronusly, following the .NET async programming pattern
    /// </summary>
    IAsyncResult BeginMetaData( AsyncCallback callback_, Object state_);

    /// <summary>
    /// Ends an asynchronous metadata request, following the .NET async programming pattern
    /// </summary>
    /// <returns>A metadata object containing the data service's metadata</returns>

    /// <exception cref="MorganStanley.MSDesktop.Rambo.DataServices.SubscriptionException">thrown if any problems</exception>
    IMetaDataObject EndMetaData( IAsyncResult result );

	}
}
