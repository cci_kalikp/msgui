using System;
using System.Collections.Generic;
using System.Collections;
using System.Xml;
using MorganStanley.MSDotNet.MSNet;
using MorganStanley.MSDotNet.MSLog;
using MorganStanley.MSDesktop.Rambo.DataServices.Util;
using MorganStanley.MSDesktop.Rambo.DataServices.SoapCps;
using MorganStanley.MSDesktop.Rambo.AsyncUtils;

namespace MorganStanley.MSDesktop.Rambo.DataServices
{

  public delegate void DSStatusHandler(IDataService[] source_, DSStatusType type_, string msg_);


  /// <summary>
  /// Maintains a thread-safe cache of dataservices
  /// </summary>
  public sealed class DataServiceManager
  {
    private static IDictionary m_cache = new Hashtable();
    private static IDictionary m_bufferedCache = new Hashtable();
    private static ServerManager _serverManager;
    private static IMSNetLoop m_loop;
    private static IDictionary<string, string> m_dsmappingCollection;
    public static event DSStatusHandler Status;

    /// <summary>
    /// Gets the name of the data service
    /// If there is any override, using the override ds name
    /// If not, use the default name.
    /// </summary>
    /// <param name="id_">The id_.</param>
    /// <returns></returns>
    public static string GetDSName(string id_)
    {
      //don't include RamboResource related ds to get from DSMapping.config
      //we can only get Rambo Related ds(we need to check whether there is any override) after we get DSMapping.config,
      //but we can only get DSMapping.config after we get Rambo Related ds.
      //It's a loop! StackOverflow exception will be thrown
      //Note: can remove for now. But we MUST add DSMapping.config to Rambo.config -> ReadOnlyConfig list to make it's read only and won't go to Rambo
      //Note: to prevent the loop!
      //if(id_.Contains("Rambo"))
      //{
      //  return id_;
      //}

      if (m_dsmappingCollection == null || m_dsmappingCollection.Count == 0)
      {
        return id_;
      }

      string dsName;
      m_dsmappingCollection.TryGetValue(id_, out dsName);
      return dsName ?? id_;
    }

    internal static void ReportStatus(string[] source_, DSStatusType type_, string msg_)
    {
      if (Status != null)
      {
        IDataService[] src = new IDataService[source_.Length];
        for (int i = 0; i < source_.Length; ++i)
        {
          src[i] = GetDataService(source_[i]);
        }
        ReportStatus(src, type_, msg_);
      }
    }

    internal static void ReportStatus(IDataService[] source_, DSStatusType type_, string msg_)
    {
      if (Status != null)
      {
        object[] statusParams = new Object[] { source_, type_, msg_ };
        m_loop.InvokeLaterWithState(new MSNetVoidObjectDelegate(ReportStatusCallback), statusParams);
      }
    }

    private static void ReportStatusCallback(object _state)
    {
      object[] statusParams = _state as object[];
      Status((IDataService[])statusParams[0], (DSStatusType)statusParams[1], (string)statusParams[2]);
    }

    public static IMSNetLoop NetLoop
    {
      get { return m_loop; }
    }

    public static bool IsInitialised()
    {
      return (m_loop != null);
    }

    #region Init operations

    public static void Init(DataServiceConfig dsconfig_)
    {
      Init(dsconfig_ , null);
    }

    public static void Init(DataServiceConfig dsconfig_ , XmlNodeList overrides_)
    {
      if (dsconfig_ == null) return;
      m_dsmappingCollection = dsconfig_.DSMapping;

      if (dsconfig_.ServerArray != null && dsconfig_.ServerArray.Length > 0)
      {
        Init(dsconfig_.Loop, dsconfig_.ServerArray);
        return;
      }

      Init(dsconfig_.Loop, dsconfig_.Location, dsconfig_.Environment, dsconfig_.Transport , overrides_);
    }

    public static void Init(IMSNetLoop loop_)
    {
      Init(loop_, DSEnvironment.Environment);
    }

    public static void Init(IMSNetLoop loop_, string environment_)
    {
      // let exceptions be thrown if the string provided does not match our enum
      DSEnvironment.DSEnv env = (DSEnvironment.DSEnv)Enum.Parse(typeof(DSEnvironment.DSEnv), environment_, true);
      Init(loop_, DSEnvironment.Location, env);
    }

    public static void Init(IMSNetLoop loop_, string location_, string environment_)
    {
      // let exceptions be thrown if the string provided does not match our enum
      DSEnvironment.DSEnv env = (DSEnvironment.DSEnv)Enum.Parse(typeof(DSEnvironment.DSEnv), environment_, true);
      DSEnvironment.DSLoc loc = (DSEnvironment.DSLoc)Enum.Parse(typeof(DSEnvironment.DSLoc), location_, true);

      Init(loop_, loc, env, DSEnvironment.PreferredTransport , null);
    }

    public static void Init(IMSNetLoop loop_, string location_, string environment_, string transport_)
    {
      // let exceptions be thrown if the string provided does not match our enum
      DSEnvironment.DSEnv env = (DSEnvironment.DSEnv)Enum.Parse(typeof(DSEnvironment.DSEnv), environment_, true);
      DSEnvironment.DSLoc loc = (DSEnvironment.DSLoc)Enum.Parse(typeof(DSEnvironment.DSLoc), location_, true);
      DSEnvironment.DSTransport trans = (DSEnvironment.DSTransport)Enum.Parse(typeof(DSEnvironment.DSTransport), transport_, true);

      Init(loop_, loc, env, trans , null);
    }

    private static void Init(IMSNetLoop loop_, DSEnvironment.DSEnv environment_)
    {
      Init(loop_, DSEnvironment.Location, environment_);
    }

    private static void Init(IMSNetLoop loop_, DSEnvironment.DSLoc location_, DSEnvironment.DSEnv environment_)
    {
      Init(loop_, location_, environment_, DSEnvironment.PreferredTransport , null);
    }

    private static void Init(IMSNetLoop loop_, DSEnvironment.DSLoc location_, DSEnvironment.DSEnv environment_, DSEnvironment.DSTransport transport_,XmlNodeList overrides_)
    {
      if (IsInitialised()) // Check if alreadly initialised
        throw new InvalidOperationException("DataServiceManager has already been initialised");

      if (loop_ == null)
        throw new NullReferenceException("Parameter 'loop_' of type IMSNetLoop cannot be null");

      DSEnvironment.DS_LOCATION = location_;
      DSEnvironment.DS_ENVIRONMENT = environment_;
      DSEnvironment.DS_PREFERRED_TRANSPORT = transport_;

      m_loop = loop_;

      if (DSEnvironment.HasServerList)
      {
        _serverManager = new ListServerManager(DSEnvironment.ServerList , overrides_);
      }
      else
      {
        _serverManager = new DSServicesServerManager(overrides_);
      }
    }


    public static void Init(IMSNetLoop loop_, string[] serverArray_)
    {
      Init(loop_, (IList)serverArray_);
    }

    public static void Init(IMSNetLoop loop_, IList servers_)
    {
      if (IsInitialised()) // Check if alreadly initialised
        throw new InvalidOperationException("DataServiceManager has already been initialised");

      if (loop_ == null)
        throw new NullReferenceException("Parameter 'loop_' of type IMSNetLoop cannot be null");

      if (servers_ == null)
        throw new NullReferenceException("Parameter 'servers_' of type IList cannot be null");

      if (servers_.Count == 0)
        throw new ArgumentException("No servers specified");

      m_loop = loop_;

      _serverManager = new ListServerManager(servers_);

      Logger log = new Logger(typeof(DataServiceManager));
      if (log.WillLog(MSLogPriority.Info))
      {
        log.Log(MSLog.Info(), "Init", "Dataservices will use the following " + servers_.Count + " servers:");
        foreach (string server in servers_)
          log.Log(MSLog.Info(), "Init", "Server: " + server);
      }
    }

    [Obsolete("Environment or server overrides can be set as command line arguments")]
    public static void InitByCommandLineArgs(IMSNetLoop loop_)
    {
      string[] cmdLineArgs = Environment.GetCommandLineArgs();
      if (cmdLineArgs.Length >= 3)
      {
        string loc = cmdLineArgs[1];
        string env = cmdLineArgs[2];
        Init(loop_, loc, env);
      }
      else
      {
        Init(loop_);
      }
    }
    #endregion

    /// <summary>
    /// This operation allows DataServiceManager to be reset to an un-initialised state.  A subsequent call
    /// to one of the Init(...) operations will not result in InvalidOperationException stating that the 
    /// DataServiceManager is already initialised. 
    /// </summary>
    public static void Reset()
    {
      _serverManager = null;
      m_loop = null;
      lock (m_cache)
      {
        m_cache.Clear();
      }
    }
    //m_bufferedCache
    public static void Register(IDataService service_)
    {
      if (service_ != null)
      {
        lock (m_cache)
        {
          if (!m_cache.Contains(service_.Id))
          {
            m_cache[service_.Id] = service_;
          }
          else
          {
            throw new DataServiceException("DataService already registered with that name");
          }
        }
      }
    }

    public static void RegisterBufferedDataService(IDataService service_)
    {
      if (service_ != null)
      {
        lock (m_bufferedCache)
        {
          if (!m_bufferedCache.Contains(service_.Id))
          {
            m_bufferedCache[service_.Id] = service_;
          }
          else
          {
            throw new DataServiceException("DataService already registered with that name");
          }
        }
      }
    }

    public static void DeRegister(IDataService service_)
    {
      if (service_ != null)
      {
        lock (m_cache)
        {
          if (m_cache.Contains(service_.Id))
          {
            m_cache.Remove(service_.Id);
          }
        }
      }
    }

    public static void DeRegisterBufferedDataService(IDataService service_)
    {
      if (service_ != null)
      {
        lock (m_bufferedCache)
        {
          if (m_bufferedCache.Contains(service_.Id))
          {
            m_bufferedCache.Remove(service_.Id);
          }
        }
      }
    }

    /// <summary>
    /// Defaults setupSub_ to true 
    /// </summary>
    /// <param name="id_"></param>
    /// <param name="createIfNotYetRegistered_"></param>
    /// <returns></returns>
    [Obsolete("No point to this operation")]
    public static IDataService GetDataService(string id_, bool createIfNotYetRegistered_)
    {
      return GetDataService(id_, true, true);
    }

    /// <summary>
    /// Defaults createIfNotYetRegistered_ to true    
    /// </summary>
    /// <param name="id_"></param>
    /// <returns></returns>

    public static IDataService GetDataService(string id_)
    {
      return GetDataService(id_, true, true);
    }

    public static IDataService GetDataService(string id_, ServerList servers_)
    {
      return GetDataService(id_, true, true, servers_);
    }

    public static IDataService GetDataService(string id_, bool createIfNotYetRegistered_, bool setupSub_)
    {
      return GetDataService(id_, createIfNotYetRegistered_, setupSub_, null);
    }

    /// <summary>
    /// Gets the dataservice with this id_ and whether to create it and set up the subscription
    /// </summary>
    public static IDataService GetDataService(string id_, bool createIfNotYetRegistered_,
                                              bool setupSub_, ServerList servers_)
    {
      if (string.IsNullOrEmpty(id_))
        return null;

      if (!IsInitialised())
        throw new InvalidOperationException("DataServiceManager is un-initialised.");

      //find if there is any override for the ds name. If not, just use the default
      id_ = GetDSName(id_);

      lock (m_cache)
      {
        IDataService dataService;
        if (servers_ == null)
        {
          dataService = m_cache[id_] as IDataService;
          if (dataService == null && createIfNotYetRegistered_)
          {
            try
            {
              dataService = new SOAPCPSDataService(id_, null);
              ((SOAPCPSDataService)dataService).GetServers(_serverManager);
              Register(dataService);
            }
            catch (Exception ex)
            {
              throw new DataServiceException(ex.Message, ex);
            }
          }
        }
        else
        {
          try
          {
            dataService = new SOAPCPSDataService(id_, servers_);
          }
          catch (Exception ex)
          {
            throw new DataServiceException(ex.Message, ex);
          }
        }
        return dataService;
      }
    }

    public static IDataService GetBufferedDataService(string id_)
    {
      return GetBufferedDataService(id_, null);
    }

    public static IDataService GetBufferedDataService(string id_, ServerList servers_)
    {
      if (null == id_ || id_ == "")
        return null;

      if (!IsInitialised())
        throw new InvalidOperationException("DataServiceManager is un-initialised.");

      lock (m_bufferedCache)
      {
        IDataService dataService;
        if (servers_ == null)
        {
          dataService = m_bufferedCache[id_] as IDataService;
          if (dataService == null)
          {
            try
            {
              dataService = new BufferedSOAPCPSDataService(id_, null);
              ((SOAPCPSDataService)dataService).GetServers(_serverManager);
              RegisterBufferedDataService(dataService);
            }
            catch (Exception ex)
            {
              throw new DataServiceException(ex.Message, ex);
            }
          }
        }
        else
        {
          try
          {
            dataService = new BufferedSOAPCPSDataService(id_, servers_);
          }
          catch (Exception ex)
          {
            throw new DataServiceException(ex.Message, ex);
          }
        }
        return dataService;
      }
    }

    /// <summary>
    /// Returns true if DataServices is using the DSServices data service to get servers
    /// for other data services
    /// </summary>
    public static bool UsingDSServices
    {
      get
      {
        if (_serverManager.GetType() == typeof(DSServicesServerManager))
        {
          return ((DSServicesServerManager)_serverManager).UsingDSServices;
        }
        else
        {
          return false;
        }
      }
      set
      {
        Logger log = new Logger(typeof(DataServiceManager));
        log.Log(MSLog.Info(), "UsingDSServices set", "DS_USEDSSERVICES set to " + value);
        DSEnvironment.DS_USEDSSERVICES = value;
      }
    }

    /// <summary>
    /// Gets a hashtable keyed on Service Id with the Serverlist's for each service as the
    /// values
    /// </summary>
    public static IDictionary ServerList
    {
      get
      {
        return _serverManager.Servers;
      }
    }

    /// <summary>
    /// Returns the current ServerManager
    /// </summary>
    internal static ServerManager ServerManager
    {
      get { return _serverManager; }
    }

    /// <summary>
    /// This boolean indicates whether the DataServiceManager was configured by a server list.
    /// </summary>
    public static bool ConfiguredByServerList
    {
      get { return _serverManager.GetType() == typeof(ListServerManager); }
    }

    public static IAsyncResult BeginRefreshServers(AsyncCallback callback_, Object state_)
    {
      Asynchronizer ssi = new Asynchronizer(callback_, state_, DSEnvironment.DS_THREAD_POOL);
      return ssi.BeginInvoke(new MSNetVoidVoidDelegate(_serverManager.RefreshServers));// new object [] {dataObjects_} );
    }

    public static void EndRefreshServers(IAsyncResult ar_)
    {
      AsynchronizerResult asr = (AsynchronizerResult)ar_;
      asr.SynchronizeInvoke.EndInvoke(ar_);
    }

    /// <summary>
    /// Sets the DS mapping dictionary
    /// </summary>
    public static void SetDSMapping(IDictionary<string, string> dsMapping_)
    {
      m_dsmappingCollection = dsMapping_;
    }
  }
}
