namespace MorganStanley.MSDesktop.Rambo.CafGuiAdmin
{
  /// <summary>
  /// Provides methods that are used by Event Horizon to inform admin GUI
  /// </summary>
  public interface ICafGuiAdminProxy
  {
    /// <summary>
    /// Prepares a GUIAdmin object and registers plugins and providers.
    /// </summary>
    void InitializeAdminService();
    /// <summary>
    /// Starts GUI Admin services.
    /// </summary>
    void Start();
    /// <summary>
    /// Stops GUI Adming services.
    /// </summary>
    void Stop();
    /// <summary>
    /// Notifies GUI Admin
    /// </summary>
    /// <param name="domain_">High level abstraction</param>
    /// <param name="eventName_">Meaning inside domain</param>
    /// <param name="parameters_">Relevant information to show</param>
    void ReportEvent(string domain_, string eventName_, string parameters_);
    ///// <summary>
    ///// Notifies GUI Admin
    ///// </summary>
    ///// <param name="domain_">High level abstraction</param>
    ///// <param name="eventName_">Meaning inside domain</param>
    ///// <param name="parameters_">Relevant information to show</param>
    void ReportEvent(EventDomain domain_, string eventName_, string parameters_);
  }
}