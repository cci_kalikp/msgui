using System;
using System.Xml.Serialization;

namespace MorganStanley.MSDesktop.Rambo.CafGuiAdmin.Configuration
{
  /// <summary>
  /// Contains information regading connecting to CafGUI.Admin service
  /// </summary>
  [XmlRoot("GuiAdminCPSService")]
  public class CafGuiAdminCpsServiceInfo : IServiceInfo
  {
    /// <summary>
    /// Gets and sets the server host name
    /// </summary>
    public string Server { get; set; }
    /// <summary>
    /// Gets and sets the connection port
    /// </summary>
    public int Port { get; set; }
    /// <summary>
    /// Indicates whether the connection is using Kerberos authentication
    /// </summary>
    public bool Kerberized { get; set; }
    /// <summary>
    /// Gets or sets the amount of time, in seconds the poller wait before it queries for metrics.
    /// </summary>
    public double PublishIntervalInSeconds { get; set; }
    /// <summary>
    /// Gets or sets a value indicating whether restarting the application is disabled.
    /// </summary>
    public bool DisableRestartPlugin { get; set; }
    /// <summary>
    /// Gets the period of time between polls.
    /// </summary>
    [XmlIgnore]
    public TimeSpan PublishInterval
    {
      get { return TimeSpan.FromSeconds(PublishIntervalInSeconds); }
    }
  }
}