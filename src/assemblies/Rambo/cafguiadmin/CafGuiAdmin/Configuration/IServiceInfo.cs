using System;
using MorganStanley.MSDotNet.CafGUI.Admin.Core.Plugins;

namespace MorganStanley.MSDesktop.Rambo.CafGuiAdmin.Configuration
{
  /// <summary>
  /// Provides an interface to CafGUI.Admin CPS service
  /// </summary>
  public interface IServiceInfo
  {
    /// <summary>
    /// Gets the host value of the CPS server
    /// </summary>
    string Server { get; }
    /// <summary>
    /// Gets the network port on which CPS server is listening.
    /// </summary>
    int Port { get; }
    /// <summary>
    /// Gets a value indicating the <see cref="RestartClientPlugin"/> should be disabled
    /// </summary>
    bool DisableRestartPlugin { get; }
    /// <summary>
    /// Gets the period of time between publishing metrics.
    /// </summary>
    TimeSpan PublishInterval { get; }
  }
}