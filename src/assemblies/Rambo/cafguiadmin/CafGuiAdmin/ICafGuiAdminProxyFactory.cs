﻿using MorganStanley.MSDesktop.Rambo.CafGuiAdmin.Configuration;

namespace MorganStanley.MSDesktop.Rambo.CafGuiAdmin
{
  public interface ICafGuiAdminProxyFactory
  {
    IServiceInfo GerServiceInfo();
  }
}