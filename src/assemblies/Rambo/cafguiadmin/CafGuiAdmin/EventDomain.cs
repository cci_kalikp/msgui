﻿namespace MorganStanley.MSDesktop.Rambo.CafGuiAdmin
{
  /// <summary>
  /// Specifies the type of domain to which event belongs
  /// </summary>
  public enum EventDomain
  {
    /// <summary>
    /// End of day
    /// </summary>
    EOD,
    /// <summary>
    /// Intraday
    /// </summary>
    Intraday,
    /// <summary>
    /// Application related events
    /// </summary>
    Application,
    /// <summary>
    /// Click on menu related events
    /// </summary>
    MenuAction,
    /// <summary>
    /// Click on Toolbar related events
    /// </summary>
    ToolAction,
    /// <summary>
    /// Health check related events
    /// </summary>
    HealthCheck,
    /// <summary>
    /// Snap related events
    /// </summary>
    Snaps,
    /// <summary>
    /// Events that were triggered by scenario analysis
    /// </summary>
    ScenarioAnalysis,
    /// <summary>
    /// Events (for logging) that goes to CafGUI database
    /// </summary>
    LogEvent,
    /// <summary>
    /// Events related to Rambo resource usage
    /// </summary>
    ResourceUsage,
    /// <summary>
    /// Events that were triggered by Signoff
    /// </summary>
    Signoff,
  }
}