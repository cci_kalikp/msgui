﻿using System;
using MorganStanley.MSDesktop.Rambo.CafGuiAdmin.Configuration;
using MorganStanley.MSDesktop.Rambo.Desktop.Utils;
using MorganStanley.MSDotNet.CafAdmin.Metrics;
using MorganStanley.MSDotNet.CafAdmin.Metrics.Providers;
using MorganStanley.MSDotNet.CafGUI.Admin.Core;
using MorganStanley.MSDotNet.CafGUI.Admin.Core.API;
using MorganStanley.MSDotNet.CafGUI.Admin.Core.Plugins;
using MorganStanley.MSDotNet.CafGUI.Admin.Core.Providers;

namespace MorganStanley.MSDesktop.Rambo.CafGuiAdmin
{
  public class CafGuiAdminProxy : ICafGuiAdminProxy
  {
    private static readonly ILogger _log = LoggerManager.GetLogger(typeof(CafGuiAdminProxy));
    private readonly IServiceInfo _serviceInfo;
    private ClientAdminService _adminService;

    private bool Initialised { get; set; }

    public CafGuiAdminProxy(IServiceInfo serviceInfo_)
    {
      if (serviceInfo_ == null)
        throw new ArgumentNullException("serviceInfo_");

      _serviceInfo = serviceInfo_;
    }

    #region ICafGuiAdminProxy Members

    public virtual void InitializeAdminService()
    {
      if (Initialised) return;
      _log.Info("InitializeAdminService", "Starting Event Horizon admin service.");

      _adminService = new ClientAdminService(_serviceInfo.Server, _serviceInfo.Port,
                                             _serviceInfo.PublishInterval, _serviceInfo.PublishInterval,
                                             AuthenticatorMechanism.WCFSSPI);

      // reduce Event Horizon impact to not maintain logs at runtime.
      _adminService.AddLimitedProvidersAndPlugins();
      _adminService.AddTickingMetricsToLimitedSet(_serviceInfo.PublishInterval, MetricProviderLevel.Normal);
      _adminService.AddProvider(new EnvironmentProvider(_serviceInfo.PublishInterval, MetricProviderLevel.Normal));
      _adminService.AddProvider(new ProcessInfoProvider(_serviceInfo.PublishInterval, MetricProviderLevel.Normal));   
      _adminService.AddProvider(new EventProvider());

      LogProvider logProvider;
      if (_adminService.TryGetProvider(out logProvider))
      {
        logProvider.Level = MetricProviderLevel.None;
      }

      RestartClientPlugin restartPlugin;
      if (_adminService.TryGetPlugin(out restartPlugin))
      {
        if (_serviceInfo.DisableRestartPlugin)
        {
          _log.Debug("InitializeAdminService", "Disabling RestartClientPlugin");
          _adminService.Plugins.Remove(restartPlugin);
        }
        else
        {
          ConfigRestartClient(restartPlugin);
        }
      }

      var monitorAgentPlugin = new MonitorAgentPlugin();
      _adminService.AddPlugin(monitorAgentPlugin);

      Initialised = true;
    }

    public void Start()
    {
      if (!Initialised) return;
      _log.Info("Start", "Starting Event Horizon admin service.");

      _adminService.Start();
    }

    public void Stop()
    {
      if (!Initialised) return;
      _log.Info("Stop", "Stopping Event Horizon admin service.");

      _adminService.Stop();

    }

    public void ReportEvent(string domain_, string eventName_, string parameters_)
    {
      if (!Initialised) return;

      _log.Info("ReportEvent", String.Format("Adding: Domain='{0}', EventName='{1}', Parameters='{2}'", domain_, eventName_, parameters_));

      EventProvider ep;
      if (_adminService.TryGetProvider(out ep))
      {
        ep.Event(domain_, eventName_, parameters_, this);
      }
    }

    public void ReportEvent(EventDomain domain_, string eventName_, string parameters_)
    {
      ReportEvent(domain_.ToString(), eventName_, parameters_);
    }

    public virtual void ConfigRestartClient(RestartClientPlugin restartClientPlugin_)
    {
    }
    #endregion

  }
}
