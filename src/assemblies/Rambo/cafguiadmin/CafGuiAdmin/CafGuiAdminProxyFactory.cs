﻿using MorganStanley.MSDesktop.Rambo.CafGuiAdmin.Configuration;

namespace MorganStanley.MSDesktop.Rambo.CafGuiAdmin
{
  public class CafGuiAdminProxyFactory : ICafGuiAdminProxyFactory
  {
    public virtual IServiceInfo GerServiceInfo()
    {
      return new CafGuiAdminCpsServiceInfo();
    }

    public ICafGuiAdminProxy CreateCafGuiAdminProxy()
    {
      IServiceInfo serviceInfo = GerServiceInfo();
      var cafGuiAdminProxy = new CafGuiAdminProxy(serviceInfo);
      cafGuiAdminProxy.InitializeAdminService();
      return cafGuiAdminProxy;
    }
  }
}