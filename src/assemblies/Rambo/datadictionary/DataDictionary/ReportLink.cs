﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MorganStanley.MSDesktop.Rambo.DataDictionary
{
  public class ReportLink:Link, ICloneable
  {
    public string SecondaryViewTypeId { get; set; }


    public ReportLink Clone()
    {
      var newLink = new ReportLink
                      {
                        SecondaryViewTypeId = SecondaryViewTypeId,
                        ReferenceColumn = ReferenceColumn,
                        ReferenceSource = ReferenceSource,
                        SecondaryColumn = SecondaryColumn,
                        SecondarySourceRefParam = SecondarySourceRefParam,
                        ViewType = ViewType,
                        SecondarySource = SecondarySource
                      };
      return newLink;
    }

    #region ICloneable Members

    object ICloneable.Clone()
    {
      return Clone();
    }

    #endregion
  }
}
