using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace MorganStanley.MSDesktop.Rambo.DataDictionary
{
  public class Source
  {
    #region Protected Data
    protected string _sourceId;
    protected bool _isPrimary;
    protected string _description;
    protected string _type;
    protected IDictionary<string, string[]> _hostPorts;
    protected string _keyString;
    protected string _routingField;
    protected string _rowSetIdField;
    protected bool _isListeningForUpdates;
    private bool _cache;
    protected ViewTypeInfo _viewTypeInfo;
    #endregion

    #region Constructor
    public Source( )
    {
      _isListeningForUpdates = true;
      InitialSubscribe = true;
      _hostPorts = new Dictionary<string, string[]>( );
    }
    #endregion

    #region Properties
    public string SourceId
    {
      get { return ( _sourceId ); }
      set { _sourceId = value; }
    }

    public bool IsPrimary
    {
      get { return ( _isPrimary ); }
      set { _isPrimary = value; }
    }

    public string Description
    {
      get { return ( _description ); }
      set { _description = value; }
    }

    public string Type
    {
      get { return ( _type ); }
      set { _type = value; }
    }

    public string KeyString
    {
      get { return ( _keyString ); }
      set { _keyString = value; }
    }

    public bool AllowNonSpecifiedColumns { get; set; }

    public bool InitialSubscribe { get; set; }

    /// <summary>
    /// Interval( miliseconds ) to snap data from KDB source.
    /// Valid for 'kdb' sources only.
    /// </summary>
    public int? KdbPollingInterval { get; set; }

    /// <summary>
    /// Proc ID required to connect to a kerberized KDB instance.
    /// Valid for 'kdb' sources only.
    /// </summary>
    public string KdbPeerId { get; set; }

    public string RoutingField
    {
      get { return _routingField; }
      set { _routingField = value; }
    }

    public string RowSetIdField
    {
      get { return _rowSetIdField; }
      set { _rowSetIdField = value; }
    }

    public bool IsListeningForUpdates
    {
      get { return _isListeningForUpdates; }
      set { _isListeningForUpdates = value; }
    }

    public bool Cache
    {
      get { return _cache; }
      set { _cache = value; }
    }

    [XmlIgnore]
    public ViewTypeInfo ViewType
    {
      get { return _viewTypeInfo; }
      set { _viewTypeInfo = value; }
    }

    [XmlIgnore]
    public IList<SourceField> SourceFields
    {
      get { return _viewTypeInfo == null ? null : _viewTypeInfo.GetSourceFields(this.SourceId); }
    }

    [XmlIgnore]
    public IList<SourceField> KeySourceFields
    {
      get { return _viewTypeInfo == null ? null : _viewTypeInfo.GetKeySourceFields(this.SourceId); }
    }

    [XmlIgnore]
    public IDictionary<string, string[]> HostPortsByExchange
    {
      get { return _hostPorts; }
    }
    #endregion

    #region Equality Methods
    public override bool Equals(object obj)
    {
      Source source = obj as Source;

      if (source == null || source.SourceId == null)
      {
        return false;
      }

      return source.SourceId.Equals(this.SourceId);
    }

    public override int GetHashCode( )
    {
      return this.SourceId.GetHashCode( );
    }
    #endregion

    #region ToString
    public override string ToString( )
    {
      return this.SourceId;
    }
    #endregion

    #region Public Methods
    public string[] GetHostPorts(string exchange_)
    {
      string[] hostports;
      if (exchange_ != null && _hostPorts.TryGetValue(exchange_, out hostports))
      {
        return hostports;
      }

      return null;
    }

    public string GetShortDescription(string attribute_)
    {
      return _viewTypeInfo.GetShortDescritpion(SourceId, attribute_);
    }

    public void SetHostPorts(string exchange_, string[] hostPorts_)
    {
      if (!String.IsNullOrEmpty(exchange_) && hostPorts_ != null)
      {
        _hostPorts.Add(exchange_, hostPorts_);
      }
    }
    #endregion
  }
}