﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace MorganStanley.MSDesktop.Rambo.DataDictionary
{
  public class Category
  {
    protected string _categoryName;
    protected string _description;
    protected int _categoryOrder;
    protected ViewTypeInfo _viewTypeInfo;

    public string Name
    {
      get { return (_categoryName); }
      set { _categoryName = value; }
    }

    public string Description
    {
      get { return (_description); }
      set { _description = value; }
    }

    public int Order
    {
      get { return (_categoryOrder); }
      set { _categoryOrder = value;}
    }

    [XmlIgnore]
    public ViewTypeInfo ViewType
    {
      get { return _viewTypeInfo; }
      set { _viewTypeInfo = value; }
    }
  }
}
