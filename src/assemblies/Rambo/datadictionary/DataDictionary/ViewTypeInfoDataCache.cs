using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Linq;
using MorganStanley.MSDesktop.Rambo.Desktop.Utils;

namespace MorganStanley.MSDesktop.Rambo.DataDictionary
{
  public class ViewTypeInfoDataCache
  {
    public delegate void BeforeDeserializeViewTypeHandler(string viewTypeId_, ref XmlNode node_);

    public event BeforeDeserializeViewTypeHandler BeforeDeserializeViewType;
    private readonly object _cacheLock = new object();
    protected static readonly XmlSerializer SERIALIZER = new XmlSerializer(typeof(ViewTypeInfo));

    protected static ViewTypeInfoDataCache _instance;
    protected string _viewTypeDirectory = DEFAULT_VIEWTYE_DIR;
    protected static ILogger _logger = LoggerManager.GetLogger(typeof(ViewTypeInfoDataCache));
    private readonly bool _hasCommandLineArgs;
    protected Dictionary<string, ViewTypeInfo> _cache = new Dictionary<string, ViewTypeInfo>();

    protected const string DEFAULT_VIEWTYE_DIR = @"\\san01b\DevAppsGML\dist\ecdev\PROJ\riskviewerconfig\prod\Configuration\datadictionary";

    private ViewTypeInfoDataCache( )
    {
      string[] cmdLineArgs = Environment.GetCommandLineArgs( );

      for (int i = 1; i < cmdLineArgs.Length; i++)
      {
        string[] nameValue = cmdLineArgs[i].Split('=');
        if (nameValue.Length == 2)
        {
          switch (nameValue[0])
          {
            case "/DataDictionary":
              ViewTypeDirectory = nameValue[1];
              _hasCommandLineArgs = true;
              break;
          }
        }
      }
    }

    public static ViewTypeInfoDataCache Instance
    {
      get
      {
        lock (typeof(ViewTypeInfoDataCache))
        {
          return _instance ?? (_instance = new ViewTypeInfoDataCache());
        }
      }
    }

    /// <summary>
    /// Function is used to create a deep copy of existing view type 
    /// </summary>
    /// <param name="existingId_"></param>
    /// <param name="newIdPostfix_"></param>
    /// <returns></returns>
    public ViewTypeInfo CloneWithNewId(string existingId_,string newIdPostfix_)
    {
      lock (_cacheLock)
      {
        newIdPostfix_=':' + newIdPostfix_;
        //Lazy man's clone 

        XmlElement xml = GetViewTypeXml(existingId_);
        ViewTypeInfo newViewType = DeserializeViewType(xml);
       
        //copying hostports from existing source

        foreach (var field in newViewType.FieldIterator)
        {

          field.SourceId += newIdPostfix_;
          field.Id = DDUtils.GetSourceFieldId(field.SourceId, field.FieldId);
        }

        foreach (var source in newViewType.Sources)
        {
          source.SourceId += newIdPostfix_;
        }

        //Basically we need to reset all the caches and maps that store source fields
        //so we set the value to itself
        newViewType.SourceFields = newViewType.SourceFields.ToArray();
        newViewType.Sources = newViewType.Sources.ToArray();

  
        foreach (var link in newViewType.Links)
        {

          link.SecondarySource += newIdPostfix_;
          link.ReferenceSource += newIdPostfix_;
        }
        newViewType.Links = newViewType.Links.ToArray();
        

        //Collect all view types that use this as a report
        var linkedViewTypes = _cache.Where(
          a_ => a_.Value.Reports.Where(b_ => b_.Equals(existingId_)).Any());

        //now lets add Report links to a newly created clone
        foreach (var pair in linkedViewTypes)
        {
          var allLinkedReports = pair.Value.ReportLinks.Where(a_ => a_.SecondaryViewTypeId == existingId_);
          

          allLinkedReports = allLinkedReports.Aggregate(new List<ReportLink>(pair.Value.ReportLinks),
            (list_, reportLink_) =>
              {
                var reportLink = reportLink_.Clone();
                reportLink.SecondaryViewTypeId += newIdPostfix_;
                reportLink.SecondarySource += reportLink.SecondarySource;
                list_.Add(reportLink);
                return list_;
              });

          pair.Value.ReportLinks = allLinkedReports.ToArray();
         
        }

        newViewType.ViewTypeId += newIdPostfix_;
        _cache.Add(newViewType.ViewTypeId, newViewType);

        return newViewType;
      }
    }

    /// <summary>
    /// Gets or sets the directory to load a view type from.
    /// </summary>
    public string ViewTypeDirectory
    {
      get { return _viewTypeDirectory; }
      set
      {
        if (Directory.Exists(value))
        {
          _viewTypeDirectory = value;
          _logger.Info("ViewTypeDirectory", "Seting DataDictionary directory to: " + value);
        }
        else
        {
          _logger.Info("ViewTypeDirectory",
                       "Invalid setting for DataDictionary directory = " + value +
                       ". Defaulting to: " + _viewTypeDirectory);
        }
      }
    }

    public bool HasCommandLineOverride
    {
      get { return _hasCommandLineArgs; }
    }

    /// <summary>
    /// XML reperesentation of ViewType.
    /// </summary>
    /// <param name="viewTypeId_"></param>
    /// <returns>Top XML definition element</returns>
    public XmlElement GetViewTypeXml(string viewTypeId_)
    {
        string fileName = Path.Combine(Instance.ViewTypeDirectory,
          string.Format("{0}.xml", viewTypeId_));
        XmlDocument doc = new XmlDocument();
        doc.Load(fileName);
        
        return doc.DocumentElement;
    }

    /// <summary>
    /// List of all defined ViewTypes.
    /// </summary>
    /// <returns>List of viewTypes ids.</returns>
    public string[] GetViewTypesList()
    {
        string[] names = Directory.GetFiles(Instance.ViewTypeDirectory, "*.xml");
        for (int i = 0; i < names.Length; i++)
        {
            names[i] = Path.GetFileNameWithoutExtension(names[i]);
        }
        return names;
    }


    public ViewTypeInfo this[string viewTypeId_]
    {
      get
      {
        lock (_cacheLock)
        {
          if (!_cache.ContainsKey(viewTypeId_))
          {
            string filePath = _viewTypeDirectory + "\\" + viewTypeId_ + ".xml";

            if (!File.Exists(filePath))
            {
              _logger.Error("ViewTypeInfo indexer", "Unable to load view type from file: " + filePath);
              return null;
            }

            XmlDocument viewTypeDocument = new XmlDocument();
            viewTypeDocument.Load(filePath);

            // enrich view type definition if necessary
            XmlNode node = viewTypeDocument.DocumentElement;
            if (BeforeDeserializeViewType != null)
            {
              BeforeDeserializeViewType(viewTypeId_, ref node);
            }
            if (node == null) throw new NullReferenceException("node");
            _cache[viewTypeId_] = DeserializeViewType(node);
          }

          return _cache[viewTypeId_];
        }
      }
    }

    private static ViewTypeInfo DeserializeViewType(XmlNode node_)
    {
      var result = (ViewTypeInfo) SERIALIZER.Deserialize(new XmlNodeReader(node_));
      foreach (SourceField sourceField in result.SourceFields)
      {
        sourceField.IsCoreField = true;
      }
      return result;
    }
  }
}