using System;
using System.Xml.Serialization;

namespace MorganStanley.MSDesktop.Rambo.DataDictionary
{
  /// <summary>
  /// Summary description for Link.
  /// </summary>
  public class Link
  {
    private string _referenceSource;
    private string _referenceColumn;
    private string _secondarySource;
    private string _secondaryColumn;
    private string _secondarySourceRefParam;
   
    private ViewTypeInfo _viewType;

    public String ReferenceSource
    {
      get { return _referenceSource; }
      set { _referenceSource = value; }
    }

    public String ReferenceColumn
    {
      get { return _referenceColumn; }
      set { _referenceColumn = value; }
    }

    public String SecondarySource
    {
      get { return _secondarySource; }
      set { _secondarySource = value; }
    }

    public String SecondaryColumn
    {
      get { return _secondaryColumn; }
      set { _secondaryColumn = value; }
    }

    public String SecondarySourceRefParam
    {
      get { return _secondarySourceRefParam; }
      set { _secondarySourceRefParam = value; }
    }

    public override string ToString( )
    {
      return ( _referenceSource + "." + _referenceColumn + " -> " + _secondarySource + "." + _secondaryColumn );
    }

    [XmlIgnore]
    public ViewTypeInfo ViewType
    {
      set { _viewType = value; }
      get { return _viewType; }
    }

    [XmlIgnore]
    public SourceField ReferenceField
    {
      get { return _viewType.GetSourceField(DDUtils.GetSourceFieldId(_referenceSource, _referenceColumn)); }
    }

    [XmlIgnore]
    public SourceField SecondaryField
    {
      get { return _viewType.GetSourceField(DDUtils.GetSourceFieldId(_secondarySource, _secondaryColumn)); }
    }
  }
}