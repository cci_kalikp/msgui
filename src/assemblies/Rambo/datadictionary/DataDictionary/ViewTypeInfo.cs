using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace MorganStanley.MSDesktop.Rambo.DataDictionary
{

  /// <summary>
  /// Some viewTypes (report type) can work only on certain record type as an input
  /// None means has no input
  /// All  --- all record types
  /// Record --- Asset or Position
  /// Asset --- only on Asset level
  /// Position --- only on Position level
  /// </summary>
  public enum RecordInputType { None, All, Record, Asset, Position }
  /// <summary>
  /// Summary description for ViewTypeInfo.
  /// </summary>
  public class ViewTypeInfo
  {
    #region Readonly & Static Fields

    protected readonly object _syncRoot = new object();

    #endregion

    #region Fields

    private IList<SourceField> _aggregateSouceFields;
    private IList<SourceField> _consistencySourceFields;
    private string _description;
    private IList<SourceField> _errorSourceFields;
    private IList<SourceField> _fxToDisplaySourceFields;
    private IList<SourceField> _groupingSourceFields;
    private IList<SourceField> _invisibleSourceFields;

    private IDictionary<string, List<SourceField>> _keySourceFieldsBySourceId;
    private Link[] _links;
    private IDictionary<string, Link> _linksByReferenceCol;
    private IDictionary<string, Link> _linksBySecondaryCol;

    private IList<SourceField> _overrideSourceFields;

    private Source _primarySource;
    private IList<SourceField> _requiredSourceFields;

    private IDictionary<string, Source> _sourceCache;
    private IDictionary<string, Category> _categoryCache;
    private IDictionary<string, SourceField> _sourceFieldCache;
    private List<SourceField> _sourceFields;
    private List<Category> _categories;
    private IDictionary<string, List<SourceField>> _sourceFieldsByBusinessType;
    private IDictionary<string, List<SourceField>> _sourceFieldsByDataType;
    private IDictionary<string, List<SourceField>> _sourceFieldsBySourceId;
    private IDictionary<string, List<SourceField>> _sourceFieldByCategory;
    private List<Source> _sources;
    private IDictionary<string, List<Source>> _sourcesByType;
    private string _viewTypeId;
    private readonly List<SourceField> _visibleArrayTypeFields;
    private IList<SourceField> _visibleSourceFields;
    private IDictionary<string, List<ReportLink>> _reportToReportLinks;
    private Dictionary<string, Dictionary<string, string>> _shortDescriptionByAttribute;

    ReportLink[] _reportLinks;


    #endregion

    #region Constructors

    public ViewTypeInfo()
    {

      _reportToReportLinks =  new Dictionary<string, List<ReportLink>>();
      _sourceFields = new List<SourceField>();
      _sources = new List<Source>();
      _categories = new List<Category>();
      _sourceFieldCache = new Dictionary<string, SourceField>();
      _sourceCache = new Dictionary<string, Source>();
      _sourcesByType = new Dictionary<string, List<Source>>();

      _sourceFieldsBySourceId = new Dictionary<string, List<SourceField>>();
      _sourceFieldsByBusinessType = new Dictionary<string, List<SourceField>>();
      _sourceFieldsByDataType = new Dictionary<string, List<SourceField>>();
      _sourceFieldByCategory = new Dictionary<string, List<SourceField>>();
      _keySourceFieldsBySourceId = new Dictionary<string, List<SourceField>>();
      
      _linksByReferenceCol = new Dictionary<string, Link>();
      _linksBySecondaryCol = new Dictionary<string, Link>();

      _consistencySourceFields = new List<SourceField>();
      _overrideSourceFields = new List<SourceField>();
      _errorSourceFields = new List<SourceField>();
      _aggregateSouceFields = new List<SourceField>();
      _groupingSourceFields = new List<SourceField>();
      _fxToDisplaySourceFields = new List<SourceField>();
      _visibleArrayTypeFields = new List<SourceField>();
      _visibleSourceFields = new List<SourceField>();
      _invisibleSourceFields = new List<SourceField>();
      _requiredSourceFields = new List<SourceField>();
      _categoryCache = new Dictionary<string, Category>();
      _shortDescriptionByAttribute = new Dictionary<string, Dictionary<string,string>>();

    }

    #endregion

    #region Instance Properties

    [XmlIgnore]
    public IList<SourceField> AggregateSourceFields
    {
      get { return _aggregateSouceFields; }
    }

    [XmlIgnore]
    public IList<SourceField> ConsistencySourceFields
    {
      get { return _consistencySourceFields; }
    }

    [XmlIgnore]
    public IList<string> Reports
    {
      get { return new List<string>(_reportToReportLinks.Keys); }
    }

    private string _reportId;

    [XmlIgnore]
    public string ReportId
    {
      get
      {
        return _reportId;
      }
    }

    public string Description
    {
      get { return _description; }
      set { _description = value; }
    }

    /// <summary>
    /// Some viewTypes (report type) can work only on certain record type as an input
    /// None means has no input
    /// All  --- all record types
    /// Record --- Asset or Position
    /// Asset --- only on Asset level
    /// Position --- only on Position level
    /// </summary>
    public RecordInputType DetailsInputType
    {
      get;
      set;
    }

    /// <summary>
    /// Maximum number of positions allowed for the report input
    /// </summary>
    public int DetailsInputMaxItems
    {
      get;
      set;
    }


    public string Group
    {
      get;set;
    }

    public bool Visible
    {
      get; set;
    }

    [XmlIgnore]
    public IList<SourceField> ErrorSourceFields
    {
      get { return _errorSourceFields; }
    }

    [XmlIgnore]
    public int FieldCount
    {
      get { return _sourceFields.Count; }
    }

    [XmlIgnore]
    public IList<SourceField> FXSourceFields
    {
      get { return _fxToDisplaySourceFields; }
    }

    [XmlIgnore]
    public IList<SourceField> GroupingSourceFields
    {
      get { return _groupingSourceFields; }
    }

    [XmlIgnore]
    public IList<SourceField> InVisibleSourceFields
    {
      get { return _invisibleSourceFields; }
    }

    public ReportLink[] ReportLinks
    {
      get
      {
        return _reportLinks;
      }
      set
      {
        SetReportLinks(value);
      }
    }

    public Link[] Links
    {
      get { return _links; }
      set { SetLinks(value); }
    }

    [XmlIgnore]
    public IList<SourceField> OverrideSourceFields
    {
      get { return _overrideSourceFields; }
    }

    [XmlIgnore]
    public Source PrimarySource
    {
      get { return _primarySource; }
    }

    [XmlIgnore]
    public IEnumerable<SourceField> FieldIterator
    {
      get
      {  
        for (int i = 0; i < _sourceFields.Count; i++)
        {
          yield return _sourceFields[i];
        }
      }
    }

    public SourceField[] SourceFields
    {
      get { return _sourceFields.ToArray(); }
      set { SetSourceFields(value); }
    }

    public Source[] Sources
    {
      get { return _sources.ToArray(); }
      set { SetSources(value); }
    }

    public Category[] Categories
    {
      get { return _categories.ToArray(); }
      set { SetCategories(value); }
    }

    public object SyncRoot
    {
      get { return _syncRoot; }
    }

    public string ViewTypeId
    {
      get { return _viewTypeId; }
      set
      {
        _viewTypeId = value;
        var idParts = _viewTypeId.Split(':');
        _layoutGroupingId = idParts[0];
        _reportId = idParts.Length>1?idParts[1]:string.Empty;
      }
    }

    private string _layoutGroupingId;

    [XmlIgnore]
    public string LayoutGroupingId
    {
      get { return _layoutGroupingId; }
    }

    //this identifies layout and gerouping id
    //on what key values are stored in DS
    //in most cases it is equal to viewtype id
    //however in reports it is something else
    [XmlIgnore]
    public IList<SourceField> VisibleSourceFields
    {
      get { return _visibleSourceFields; }
    }

    [XmlIgnore]
    public List<SourceField> VisibleArrayTypeFields
    {
      get { return _visibleArrayTypeFields; }
    }

    #endregion

    #region Instance Methods

    public void AddSource(Source source_)
    {
      if (!_sourceCache.ContainsKey(source_.SourceId))
      {
        _sourceCache[source_.SourceId] = source_;
        _sources.Add(source_);
        DDUtils.AddToListDictionary(_sourcesByType, source_.Type, source_);
        source_.ViewType = this;
      }
    }

    public void AddCategory(Category category_)
    {
      if(!_categoryCache.ContainsKey(category_.Name))
      {
        _categoryCache[category_.Name] = category_;
        _categories.Add(category_);
        category_.ViewType = this;
      }
    }

    public void AddSourceField(SourceField sf_)
    {
      lock (_syncRoot)
      {
        if (sf_ == null || _sourceFieldCache.ContainsKey(sf_.Id))
        {
          return;
        }

        _sourceFields.Add(sf_);
        _sourceFieldCache.Add(sf_.Id, sf_);
        //_shortDescriptionByAttribute.Add(sf_.SourceId,sf_.ShortDescription);
        sf_.ViewType = this;

        DDUtils.AddToDictionaryDictionary(_shortDescriptionByAttribute,sf_.SourceId,sf_.Attribute,sf_.ShortDescription);
        DDUtils.AddToListDictionary(_sourceFieldsBySourceId, sf_.SourceId, sf_);

        if (!String.IsNullOrEmpty(sf_.BusinessType))
        {
          DDUtils.AddToListDictionary(_sourceFieldsByBusinessType, sf_.BusinessType, sf_);
        }

        if (!String.IsNullOrEmpty(sf_.DataType))
        {
          DDUtils.AddToListDictionary(_sourceFieldsByDataType, sf_.DataType, sf_);
        }

        if(!String.IsNullOrEmpty(sf_.CategoryName))
        {
          DDUtils.AddToListDictionary(_sourceFieldByCategory, sf_.CategoryName, sf_);
        }

        if (sf_.IsKeyField)
        {
          DDUtils.AddToListDictionary(_keySourceFieldsBySourceId, sf_.SourceId, sf_);
        }

        if (sf_.IsConsistencyField)
        {
          _consistencySourceFields.Add(sf_);
        }

        if (sf_.IsOverrideField)
        {
          _overrideSourceFields.Add(sf_);
        }

        if (sf_.IsErrorField)
        {
          _errorSourceFields.Add(sf_);
        }

        if (sf_.AllowAggregation)
        {
          _aggregateSouceFields.Add(sf_);
        }

        if (sf_.AllowGrouping)
        {
          _groupingSourceFields.Add(sf_);
        }

        if (sf_.FxToDisplay)
        {
          _fxToDisplaySourceFields.Add(sf_);
        }

        if (sf_.IsVisible)
        {
          _visibleSourceFields.Add(sf_);
          if (sf_.DataTypeType == typeof(Array))
          {
            _visibleArrayTypeFields.Add(sf_);
          }
        }
        else
        {
          _invisibleSourceFields.Add(sf_);
        }

        if (sf_.IsRequired)
        {
          _requiredSourceFields.Add(sf_);
        }
      }

      OnSourceFieldAdded(new SourceFieldEventArgs(sf_));
    }

    public string GetShortDescritpion(string sourceId_, string attribute_)
    {
      return DDUtils.GetDictionaryDictionaryValue(_shortDescriptionByAttribute, sourceId_, attribute_);
    }

    public IList<SourceField> GetKeySourceFields(string sourceId_)
    {
      return DDUtils.GetListDictionaryValue(_keySourceFieldsBySourceId, sourceId_);
    }

    public Link GetLinkByReferenceSourceField(SourceField refSourceField_)
    {
      string sourceFieldId = refSourceField_.Id;
      return DDUtils.GetDictionaryValue(_linksByReferenceCol, sourceFieldId);
    }

    public Link GetLinkBySecondarySourceField(SourceField secondarySourceField_)
    {
      string sourceFieldId = secondarySourceField_.Id;
      return DDUtils.GetDictionaryValue(_linksBySecondaryCol, sourceFieldId);
    }

    public Source GetSource(string sourceId_)
    {
      return DDUtils.GetDictionaryValue(_sourceCache, sourceId_);
    }

    public Category GetCategory(string categoryName_)
    {
      return DDUtils.GetDictionaryValue(_categoryCache, categoryName_);
    }

    public SourceField GetSourceField(string sourceFieldId_)
    {
      return DDUtils.GetDictionaryValue(_sourceFieldCache, sourceFieldId_);
    }

    public SourceField GetSourceField(string sourceId_, string fieldId_)
    {
      string sourceFieldId = DDUtils.GetSourceFieldId(sourceId_, fieldId_);
      return GetSourceField(sourceFieldId);
    }

    public IList<SourceField> GetSourceFields(string sourceId_)
    {
      return DDUtils.GetListDictionaryValue(_sourceFieldsBySourceId, sourceId_);
    }

    public IList<ReportLink> GetReportLinks(string viewTypeId_)
    {
      return new List<ReportLink>(_reportToReportLinks[viewTypeId_]);
    }

    public IList<SourceField> GetSourceFieldsByBusinessType(string businessType_)
    {
      return DDUtils.GetListDictionaryValue(_sourceFieldsByBusinessType, businessType_);
    }

    public IList<SourceField> GetSourceFieldsByCategory(string categoryId_)
    {
      return DDUtils.GetListDictionaryValue(_sourceFieldByCategory, categoryId_);
    }

    public IList<SourceField> GetSourceFieldsByDataType(string dataType_)
    {
      return DDUtils.GetListDictionaryValue(_sourceFieldsByBusinessType, dataType_);
    }

    public IList<Source> GetSourcesByType(string type_)
    {
      return DDUtils.GetListDictionaryValue(_sourcesByType, type_);
    }

    public bool HasLink(Source source_)
    {
      if (_links == null)
      {
        return false;
      }

      foreach (Link link in _links)
      {
        if (link.SecondarySource == source_.SourceId)
        {
          return true;
        }
      }

      return false;
    }

    public bool IsSecondaryFieldInJoin(SourceField sf_)
    {
      string sourceFieldId = sf_.Id;
      return _linksBySecondaryCol.ContainsKey(sourceFieldId);
    }

    public void RemoveSource(Source source_)
    {
      if (_sourceCache.Remove(source_.SourceId))
      {
        _sources.Remove(source_);
        DDUtils.RemoveFromListDictionary(_sourcesByType, source_.Type, source_);
      }
      source_.ViewType = null;
    }

    public void RemoveCategory(Category category_)
    {
      if(_categoryCache.Remove(category_.Name))
      {
        _categories.Remove(category_);
      }
    }


    public void RemoveSourceField(SourceField sf_)
    {
      if (sf_ == null)
      {
        return;
      }

      lock (_syncRoot)
      {
        if (!_sourceFieldCache.Remove(sf_.Id))
        {
          return;
        }

        _sourceFields.Remove(sf_);
        sf_.ViewType = this;

        DDUtils.RemoveFromListDictionary(_sourceFieldsBySourceId, sf_.SourceId, sf_);

        if (!String.IsNullOrEmpty(sf_.BusinessType))
        {
          DDUtils.RemoveFromListDictionary(_sourceFieldsByBusinessType, sf_.BusinessType, sf_);
        }

        if (!String.IsNullOrEmpty(sf_.DataType))
        {
          DDUtils.RemoveFromListDictionary(_sourceFieldsByDataType, sf_.DataType, sf_);
        }

        if(!String.IsNullOrEmpty(sf_.CategoryName))
        {
          DDUtils.RemoveFromListDictionary(_sourceFieldByCategory, sf_.CategoryName, sf_);
        }

        if (sf_.IsKeyField)
        {
          DDUtils.RemoveFromListDictionary(_keySourceFieldsBySourceId, sf_.SourceId, sf_);
        }

        if (sf_.IsConsistencyField)
        {
          _consistencySourceFields.Remove(sf_);
        }

        if (sf_.IsOverrideField)
        {
          _overrideSourceFields.Remove(sf_);
        }

        if (sf_.IsErrorField)
        {
          _errorSourceFields.Remove(sf_);
        }

        if (sf_.AllowAggregation)
        {
          _aggregateSouceFields.Remove(sf_);
        }

        if (sf_.AllowGrouping)
        {
          _groupingSourceFields.Remove(sf_);
        }

        if (sf_.FxToDisplay)
        {
          _fxToDisplaySourceFields.Remove(sf_);
        }

        if (sf_.IsVisible)
        {
          _visibleSourceFields.Remove(sf_);
          if (sf_.DataTypeType == typeof(Array))
          {
            _visibleArrayTypeFields.Remove(sf_);
          }
        }
        else
        {
          _invisibleSourceFields.Remove(sf_);
        }

        if (sf_.IsRequired)
        {
          _requiredSourceFields.Remove(sf_);
        }
      }

      OnSourceFieldRemoved(new SourceFieldEventArgs(sf_));
    }

    protected void OnSourceFieldAdded(SourceFieldEventArgs e)
    {
      EventHandler<SourceFieldEventArgs> sfadded = SourceFieldAdded;
      if (sfadded != null)
      {
        sfadded(this, e);
      }
    }

    protected void OnSourceFieldRemoved(SourceFieldEventArgs e)
    {
      EventHandler<SourceFieldEventArgs> sfremoved = SourceFieldRemoved;
      if (sfremoved != null)
      {
        sfremoved(this, e);
      }
    }


    protected void SetReportLinks(ReportLink[] reportLinks_)
    {
      _reportLinks = reportLinks_;
      if (_reportLinks == null)
      {
        return;
      }
      
      _reportToReportLinks.Clear();
      foreach (ReportLink reportLink in _reportLinks)
      {
        
        reportLink.ViewType = this;
        List<ReportLink> links;
        if(!_reportToReportLinks.TryGetValue(reportLink.SecondaryViewTypeId,out links))
        {
          links=new List<ReportLink>();
          _reportToReportLinks.Add(reportLink.SecondaryViewTypeId,links);
        }
        links.Add(reportLink);
      }
    }

    protected void SetLinks(Link[] links_)
    {
      _links = links_;
      _linksByReferenceCol.Clear();
      _linksBySecondaryCol.Clear();

      if (_links == null)
      {
        return;
      }

      foreach (Link link in _links)
      {
        link.ViewType = this;
        string referenceSourceField = DDUtils.GetSourceFieldId(link.ReferenceSource, link.ReferenceColumn);
        string secondarySourceField = DDUtils.GetSourceFieldId(link.SecondarySource, link.SecondaryColumn);
        _linksByReferenceCol[referenceSourceField] = link;
        _linksBySecondaryCol[secondarySourceField] = link;
      }
    }

    protected void SetSourceFields(SourceField[] sourceFields_)
    {
      _sourceFields.Clear();
      _sourceFieldCache.Clear();

      _consistencySourceFields.Clear();
      _overrideSourceFields.Clear();
      _errorSourceFields.Clear();
      _aggregateSouceFields.Clear();
      _groupingSourceFields.Clear();
      _fxToDisplaySourceFields.Clear();
      _visibleArrayTypeFields.Clear();
      _visibleSourceFields.Clear();
      _invisibleSourceFields.Clear();

      _sourceFieldsByDataType.Clear();
      _sourceFieldsBySourceId.Clear();
      _sourceFieldsByBusinessType.Clear();
      _sourceFieldByCategory.Clear();
      _keySourceFieldsBySourceId.Clear();

      if (sourceFields_ != null && sourceFields_.Length > 0)
      {
        foreach (SourceField sf in sourceFields_)
        {
          AddSourceField(sf);
        }
      }
    }

    protected void SetCategories(Category[] categories_)
    {
      _categoryCache.Clear();

      foreach(Category category in categories_)
      {
        AddCategory(category);
      }
    }

    protected void SetSources(Source[] sources_)
    {
      _sourcesByType.Clear();
      _sourceCache.Clear();
      _sources.Clear();

      //cache primary source
      Source primarySource = null;
      if (sources_ != null && sources_.Length > 0)
      {
        foreach (Source source in sources_)
        {
          AddSource(source);

          if (source.IsPrimary)
          {
            primarySource = source;
          }
        }
      }
      _primarySource = primarySource;
    }

    #endregion

    #region Event Declarations

    public event EventHandler<SourceFieldEventArgs> SourceFieldAdded;
    public event EventHandler<SourceFieldEventArgs> SourceFieldRemoved;

    #endregion
  }
}
