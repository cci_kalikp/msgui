using System;

namespace MorganStanley.MSDesktop.Rambo.DataDictionary
{
  public class SourceFieldEventArgs : EventArgs
  {
    public SourceField SourceField;

    public SourceFieldEventArgs(SourceField sourceField_)
    {
      SourceField = sourceField_;
    }
  }
}