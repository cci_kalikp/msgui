using System;
using System.Xml.Serialization;

namespace MorganStanley.MSDesktop.Rambo.DataDictionary
{
  public class SourceField : ICloneable
  {
    private string _dataType;
    private Type _dataTypeType;

    /// <summary>
    /// Used by XML serialization
    /// </summary>
    public SourceField()
    {
      
    }

    /// <summary>
    /// This constructor is for unit testing purposes where we want to set the (otherwise internal) core field flag.
    /// </summary>
    /// <param name="isCoreField_">True means the field is specified in the viewtype XML</param>
    public SourceField(bool isCoreField_)
    {
      IsCoreField = isCoreField_;
    }

    public string CategoryName { get; set; }

    public string BusinessType {get; set; }

    public string SourceId { get; set; }

    public string FieldId { get; set; }

    public string Attribute { get; set; }

    public string DomainField { get; set; }

    public string DomainSource { get; set; }

    public string OverrideField { get; set; }

    public string OverrideSource { get; set; }

    public string ShortDescription { get; set; }

    public string LongDescription { get; set; }

    public String DataType
    {
      get { return _dataType; }
      set
      {
        _dataType = value;
        _dataTypeType = Type.GetType(value);
      }
    }

    public string DefaultValue { get; set; }

    public string Format { get; set; }

    public bool AllowAggregation { get; set; }

    public bool AllowGrouping { get; set; }

    public bool FxToDisplay { get; set; }

    public bool IsKeyField { get; set; }

    public bool IsVisible { get; set; }

    public bool IsErrorField { get; set; }

    public bool IsRequired { get; set; }

    public string Id { get; set; }

    public string AggregationType { get; set; }

    public bool IsEditable { get; set; }

    public bool IsQueryable { get; set; }

    public bool IsOverrideField { get; set; }

    /// <summary>
    /// Means that field value can be entered and represents not a direct override but an adjustment (as modify the overriden field by x amount)
    /// </summary>
    public bool IsAdjustmentField { get; set; }


    public bool IsConsistencyField { get; set; }

    public string TurboRiskId { get; set; }

    public bool IsActiveGrouping { get; set; }

    public object Clone( )
    {
      return MemberwiseClone( );
    }

    public override string ToString( )
    {
      return ShortDescription;
    }

    [XmlIgnore]
    public ViewTypeInfo ViewType { get; set; }

    [XmlIgnore]
    public Source Source
    {
      get { return ( ViewType != null ) ? ViewType.GetSource(SourceId) : null; }
    }

    [XmlIgnore]
    public Category Category
    {
      get { return (ViewType != null) ? ViewType.GetCategory(CategoryName) : null; }
    }

    [XmlIgnore]
    public object Tag { get; set; }

    [XmlIgnore]
    public Type DataTypeType
    {
      get { return _dataTypeType; }
    }

    /// <summary>
    /// If true the field was automaticly generated from the 
    /// columns suplied by the backend
    /// </summary>
    [XmlIgnore]
    public bool NonSpecifiedColumn {get;  set;}

    /// <summary>
    /// If true, the field was created on creation of the view type, not added on later.
    /// </summary>
    [XmlIgnore]
    public bool IsCoreField { get; internal set; }

    public override bool Equals(object obj_)
    {
      var that = obj_ as SourceField;

      if (that == null)
      {
        return false;
      }

      return Id.Equals(that.Id);
    }

    public override int GetHashCode( )
    {
      return Id.GetHashCode( );
    }
  }
}