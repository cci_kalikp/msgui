﻿namespace MorganStanley.Ecdev.DataDictionary.HostConfiguration
{
  /// <summary>
  /// Provides functionality for registering host/ports configurations for a <see cref="Source"/>.
  /// </summary>
  public interface IHostPortConfiguration
  {
    /// <summary>
    /// Determines whether the Source contains a configuration with the specified key.
    /// </summary>
    /// <param name="key_">The key to locate.</param>
    /// <returns>true if the Source contains an element with the specified key; otherwise, false.</returns>
    bool ContainsKey(HostPortKey key_);
    /// <summary>
    /// Returns the first configuration entry in the Source.
    /// </summary>
    /// <returns> List of host/ports.</returns>
    string[] GetFirstHostPortConfiguration();
    /// <summary>
    /// Gets the list of the host/ports that are associated with the specified key.
    /// </summary>
    /// <param name="key_">The key that contains id/exchange/region</param>
    /// <returns>
    /// If the key is registered it will return the list of host/port settings. Otherwise null.
    /// </returns>
    string[] GetHostPorts(HostPortKey key_);
    /// <summary>
    /// Registers the list of host/ports with the given key object.
    /// </summary>
    /// <param name="key_">The key of the element to add.</param>
    /// <param name="hostPorts_">The list of host/ports that are associated with the key object.</param>
    void SetHostPorts(HostPortKey key_, string[] hostPorts_);
  }
}