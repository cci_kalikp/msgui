﻿using System;

namespace MorganStanley.Ecdev.DataDictionary.HostConfiguration
{
  /// <summary>
  /// Defines the caching type of the key.
  /// </summary>
  public enum PubSubType
  {
    Publisher,
    Subscriber,
  }

  /// <summary>
  /// This class represents a key for a specific HostPorts list.
  /// </summary>
  public class HostPortKey
  {
    /// <summary>
    /// Fail-over string in key definition for exchange and source in case of 
    /// they are missing. 
    /// </summary>
    public const string KEYPART_DEFAULT = "default";

    #region Properties
    /// <summary>
    /// Gets and sets the exchange part of the key.
    /// </summary>
    public string Exchange { get; protected set; }
    /// <summary>
    /// Gets the source part of the key.
    /// </summary>
    public string Source { get; protected set; }
    /// <summary>
    /// Gets the region part of the key.
    /// </summary>
    public string Region { get; protected set; }
    /// <summary>
    /// Gets the key type.
    /// </summary>
    public PubSubType HostPortType { get; protected set; }
    /// <summary>
    /// Indicates whether this key is overriden.
    /// </summary>
    public bool IsOverride { get; protected set; }
    #endregion

    #region Constructors
    /// <summary>
    /// Creates a new key using exchange, source, region and mode.
    /// </summary>
    /// <param name="ex_">
    /// The exchange part.
    /// </param>
    /// <param name="sr_">
    /// The source part.
    /// </param>
    /// <param name="rg_">
    /// The region part.
    /// </param>
    /// <param name="ty_">
    /// The mode of subscription.
    /// </param>
    /// <exception cref="ArgumentNullException">
    /// It is thrown if one of <paramref name="ex_"/>, <paramref name="sr_"/> or 
    /// <paramref name="rg_"/> is a null reference or an <see cref="String.Empty">empty</see> string.
    /// </exception>
    public HostPortKey(string ex_, string sr_, string rg_, PubSubType ty_)
    {
      //if (String.IsNullOrEmpty(ex_)) throw new ArgumentNullException("ex_");
      //if (String.IsNullOrEmpty(sr_)) throw new ArgumentNullException("sr_");
      //if (String.IsNullOrEmpty(rg_)) throw new ArgumentNullException("rg_");

      Exchange = ex_;
      Source = sr_;
      Region = rg_;
      HostPortType = ty_;
    }
    /// <summary>
    /// Creates a new key using a concatenated key and mode.
    /// </summary>
    /// <param name="key_">
    /// The concatenated key. <example>default/genCalcAndPas/LN</example>
    /// </param>
    /// <param name="ty_">
    /// The mode of subscription.
    /// </param>
    /// <exception cref="ArgumentNullException">
    /// It is thrown if one of <paramref name="key_"/> is a null reference or 
    /// an <see cref="string.Empty">empty</see> string.
    /// </exception>
    /// <exception cref="FormatException">
    /// It is thrown, if <paramref name="key_"/> cannot be split to [exchange]/[source]/[region].
    /// </exception>
    public HostPortKey(string key_, PubSubType ty_) : this(key_, ty_, false)
    {
    }
    /// <summary>
    /// Creates a new key using a concatenated key and mode indicating if it is an override.
    /// </summary>
    /// <param name="key_">
    /// The concatenated key. <example>default/genCalcAndPas/LN</example>
    /// </param>
    /// <param name="ty_">
    /// The mode of subscription.
    /// </param>
    /// <param name="ovr_">
    /// Set true if it is an override.
    /// </param>
    /// <exception cref="ArgumentNullException">
    /// It is thrown if one of <paramref name="key_"/> is a null reference or 
    /// an <see cref="string.Empty">empty</see> string.
    /// </exception>
    /// <exception cref="FormatException">
    /// It is thrown, if <paramref name="key_"/> cannot be splitted to [exchange]/[source]/[region].
    /// </exception>
    public HostPortKey(string key_, PubSubType ty_, bool ovr_)
    {
      if (String.IsNullOrEmpty(key_)) throw new ArgumentNullException("key_");

      var keyParts = key_.Split('/');
      if (keyParts.Length != 3)
      {
        throw new FormatException(String.Format("Key {0} cannot be split to <exhange>/<source>/<region>.", key_));
      }

      Exchange = keyParts[0];
      Source = keyParts[1];
      Region = keyParts[2];
      HostPortType = ty_;
      IsOverride = ovr_;
    }

    #endregion

    #region Overrides
    public override bool Equals(object obj)
    {
      var result = false;

      var that = obj as HostPortKey;
      if (that != null)
      {
        result = (HostPortType == that.HostPortType) && (Exchange == that.Exchange) &&
                 (Source == that.Source) && (Region == that.Region);
      }

      return result;
    }
    public override int GetHashCode()
    {
      int result = (Exchange == null) ? 0 : Exchange.GetHashCode();
      result ^= (Source == null) ? 0 : Source.GetHashCode();
      result ^= Region.GetHashCode() ^ HostPortType.GetHashCode();

      return result;
    }
    public override string ToString()
    {
      return String.Format("{0}/{1}/{2}", Exchange, Source, Region);
    }
    #endregion
  }
}