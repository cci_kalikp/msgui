using System;
using System.Collections.Generic;

namespace MorganStanley.MSDesktop.Rambo.DataDictionary
{
  public static class DDUtils
  {
    public static string GetSourceFieldId(string sourceId_, string fieldId_)
    {
      if (sourceId_ == null || sourceId_ == string.Empty || fieldId_ == null || fieldId_ == string.Empty)
      {
        return null;
      }

      // optimized replacement for: string.Format("{0}.{1}", sourceId_, fieldId_)
      int len = sourceId_.Length;
      char[] buf = new Char[len + fieldId_.Length + 1];
      sourceId_.CopyTo(0, buf, 0, len);
      buf[len] = '.';
      fieldId_.CopyTo(0, buf, len + 1, fieldId_.Length);
      string sourceFieldId = new string(buf);

      return sourceFieldId;
    }

    internal static void AddToListDictionary<K, V>(IDictionary<K, List<V>> dictionary_, K key_, V value_)
    {
      if (Equals(key_, default( K )))
      {
        return;
      }

      List<V> list;
      if (!dictionary_.TryGetValue(key_, out list))
      {
        list = new List<V>( );
        dictionary_.Add(key_, list);
      }
      list.Add(value_);
    }

    internal static void AddToDictionaryDictionary<K, V, M>(Dictionary<K, Dictionary<V,M>> dictionary_, K key_, V subKey_, M value_)
    {
      if (Equals(key_, default(K))|| Equals(subKey_, default(V)))
      {
        return;
      }

      Dictionary<V,M> dictionary;
      if (!dictionary_.TryGetValue(key_, out dictionary))
      {
        dictionary = new Dictionary<V,M>();
        dictionary_.Add(key_, dictionary);
      }

      if(!dictionary.ContainsKey(subKey_))
         dictionary.Add(subKey_,value_);
    }

    internal static M GetDictionaryDictionaryValue<K, V, M>(Dictionary<K, Dictionary<V,M>> dictionary_, K key_, V subKey_)
    {
      Dictionary<V,M> dictionary;
      if (!Equals(key_, default(K)) && dictionary_.TryGetValue(key_, out dictionary))
      {
        M value;
        if(!Equals(subKey_,default(V)) && dictionary != null && dictionary.TryGetValue(subKey_,out value))
        {
          return value;
        }
      }

      return default(M);
    }

    internal static void RemoveFromListDictionary<K, V>(IDictionary<K, List<V>> dictionary_, K key_, V value_)
    {
      if (Equals(key_, default( K )))
      {
        return;
      }

      List<V> list;
      if (dictionary_.TryGetValue(key_, out list))
      {
        list.Remove(value_);
      }
    }

    internal static IList<V> GetListDictionaryValue<K, V>(IDictionary<K, List<V>> dictionary_, K key_)
    {
      List<V> list;
      if (!Equals(key_, default( K )) && dictionary_.TryGetValue(key_, out list))
      {
        return list;
      }

      return null;
    }

    internal static V GetDictionaryValue<K, V>(IDictionary<K, V> dictionary_, K key_)
    {
      V value;
      if (!Equals(key_, default( K )) && dictionary_.TryGetValue(key_, out value))
      {
        return value;
      }

      return default( V );
    }
  }
}