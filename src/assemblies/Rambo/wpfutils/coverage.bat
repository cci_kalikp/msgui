@call \\ms\dist\winaurora\PROJ\logon\incr\bin\gettokens.cmd &
@call \\ms\dist\winaurora\PROJ\compat\incr\bin\useafs.cmd &
call module load msdotnet/nunit/2.5
call module load msdotnet/ncover/1.5.8-b

NCover.Console.exe nunit-console.exe Tests\bin\Debug\Ecdev.WpfUtils.Tests.dll /noShadow //a Ecdev.WpfUtils.v9.2 //reg //x coverage.xml
\\ms\dist\msde\PROJ\eval\ncoverexplorer-1.4.0.7\bin\NCoverExplorer.console.exe coverage.xml /h:coverage.html /r:ModuleClassFunctionSummary