﻿using System;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Interop;

namespace MorganStanley.MSDesktop.Rambo.WpfUtils.Core.Helpers
{
  public static class IconHelper
  {
    [DllImport("user32.dll")]         
    static extern int GetWindowLong(IntPtr hwnd_, int index_);   
       
    [DllImport("user32.dll")]         
    static extern int SetWindowLong(IntPtr hwnd_, int index_, int newStyle_);          
    
    [DllImport("user32.dll")]         
    static extern bool SetWindowPos(IntPtr hwnd_, IntPtr hwndInsertAfter_, int x_, int y_, int width_, int height_, uint flags_); 
         
    [DllImport("user32.dll")]         
    static extern IntPtr SendMessage(IntPtr hwnd_, uint msg_, IntPtr wParam_, IntPtr lParam_);          

    const int GWL_EXSTYLE = -20;         
    const int WS_EX_DLGMODALFRAME = 0x0001;         
    const int SWP_NOSIZE = 0x0001;         
    const int SWP_NOMOVE = 0x0002;         
    const int SWP_NOZORDER = 0x0004;         
    const int SWP_FRAMECHANGED = 0x0020;         
    const uint WM_SETICON = 0x0080;
    private const int ICON_SMALL = 0;
    private const int ICON_BIG = 1;
    
    public static void RemoveIcon(Window window_)         
    {             
      // Get this window's handle             
      IntPtr hwnd = new WindowInteropHelper(window_).Handle;              
      // Change the extended window style to not show a window icon             
      int extendedStyle = GetWindowLong(hwnd, GWL_EXSTYLE);             
      SetWindowLong(hwnd, GWL_EXSTYLE, extendedStyle | WS_EX_DLGMODALFRAME);
      SendMessage(hwnd, WM_SETICON, (IntPtr)ICON_SMALL, IntPtr.Zero); SendMessage(hwnd, WM_SETICON, (IntPtr)ICON_BIG, IntPtr.Zero);      
      // Update the window's non-client area to reflect the changes            
      SetWindowPos(hwnd, IntPtr.Zero, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_FRAMECHANGED);        
    } 
  }
}
