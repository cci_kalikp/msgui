﻿using System;

namespace MorganStanley.MSDesktop.Rambo.WpfUtils.Core.Helpers
{
  public static class EnumHelper
  {
    /// <summary>
    /// Converts the string representation of an enum its enum representation. 
    /// A return value indicates whether the conversion succeeded or failed.
    /// </summary>
    /// <typeparam name="T">The type of the enum</typeparam>
    /// <param name="valueToParse_">A string representing the value to convert.</param>
    /// <param name="returnValue_">
    /// The <typeparamref name="T"/> equivalent of <paramref name="valueToParse_"/> if the conversion 
    /// succeeded, or the default value of that type.
    /// </param>
    /// <returns>true if s was converted successfully; otherwise, false.</returns>
    public static bool TryParse<T>(string valueToParse_, out T returnValue_)
    {
      returnValue_ = default(T);

      if (!String.IsNullOrEmpty(valueToParse_))
      {
        string strTypeFixed = valueToParse_.Replace(' ', '_');
        if (Enum.IsDefined(typeof(T), strTypeFixed))
        {
          returnValue_ = (T)Enum.Parse(typeof(T), strTypeFixed, true);
          return true;
        }

        foreach (string item in Enum.GetNames(typeof(T)))
        {
          if (item.Equals(strTypeFixed, StringComparison.OrdinalIgnoreCase))
          {
            returnValue_ = (T)Enum.Parse(typeof(T), item);
            return true;
          }
        } 
      }

      return false;
    }
  }
}