﻿using System.Windows;

namespace MorganStanley.MSDesktop.Rambo.WpfUtils.Core.AttachedProperties
{
  public static class FocusAttachedProperty
  {
    public static bool GetIsFocused(DependencyObject obj_)
    {
      return (bool)obj_.GetValue(IsFocusedProperty);
    }

    public static void SetIsFocused(DependencyObject obj_, bool value_)
    {
      obj_.SetValue(IsFocusedProperty, value_);
    }

    public static readonly DependencyProperty IsFocusedProperty =
            DependencyProperty.RegisterAttached(
             "IsFocused"
             , typeof(bool)
             , typeof(FocusAttachedProperty)
             , new UIPropertyMetadata(false, OnIsFocusedPropertyChanged));

    private static void OnIsFocusedPropertyChanged(DependencyObject d_, DependencyPropertyChangedEventArgs e_)
    {
      var uie = (UIElement)d_;
      if ((bool)e_.NewValue)
      {
        uie.Focus();
      }
    }
  }
}
