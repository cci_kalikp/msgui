﻿using System.Windows;

using MorganStanley.MSDesktop.Rambo.WpfUtils.Core.Extensions;

namespace MorganStanley.MSDesktop.Rambo.WpfUtils.Core.AttachedProperties
{

  /// <summary>
  /// Attached property that is used to close a window using MVVM pattern 
  /// This is especially useful for model dialogs that have some sort of apply, confirm button
  /// Note that dialogResult property of the window that was closed this way is true
  /// </summary>
  /// 
  public static class WindowConfirmed
  {

    public static readonly DependencyProperty WindowConfirmedProperty = DependencyProperty.RegisterAttached("WindowConfirmed", typeof(bool),
                                                                                          typeof(WindowConfirmed), new FrameworkPropertyMetadata(OnWindowConfirmedPropertyPropertyChanged));

    private static void OnWindowConfirmedPropertyPropertyChanged(DependencyObject d_, DependencyPropertyChangedEventArgs e_)
    {

      var window = d_ as Window ?? d_.FindParent<Window>();

      if (window == null) return;
      if ((bool)e_.NewValue)
      {
        window.DialogResult = true;
        window.Hide();
      }
      else
      {
        window.ShowDialog();
      }
    }

    public static void SetWindowConfirmed(Window element_, bool value_)
    {
      element_.SetValue(WindowConfirmedProperty, value_);

    }

    public static bool GetWindowConfirmed(Window element_)
    {
      return (bool)element_.GetValue(WindowConfirmedProperty);
    }

  }
}
