namespace MorganStanley.MSDesktop.Rambo.WpfUtils.Core.ViewModel
{
  /// <summary>
  /// Contains view specific information.
  /// </summary>
  public interface IView
  {
    /// <summary>
    /// Sets the Model of the view.
    /// </summary>
    object Model { get; set; }
  }
}