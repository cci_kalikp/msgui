using System;
using System.ComponentModel;
using System.Linq.Expressions;
using MorganStanley.MSDesktop.Rambo.Desktop.Utils.Extensions;

namespace MorganStanley.MSDesktop.Rambo.WpfUtils.Core.ViewModel
{
  [Obsolete("Use ObservableObject instead")]
  public abstract class ViewModelBase : INotifyPropertyChanged
  {
    public event PropertyChangedEventHandler PropertyChanged;

    /// <summary>
    /// Notify based on passed <paramref name="propertyName_"/>
    /// </summary>
    /// <param name="propertyName_">Property to bubble the change event for</param>
    /// <remarks>Use the InvokePropertyChanged method that takes a lambda expression instead of a string, unless there will be a big performance hit.
    /// The benefit of using the other method is simplicity of change in the future (avoiding arbitrary string-passing).
    /// Although there is a noted reflection hit with it, for non-performance-critical areas, this often has an acceptable/negligible effect on the visual result.</remarks>   
    protected void Notify(string propertyName_)
    {
      PropertyChangedEventHandler tmp = PropertyChanged;
      if (tmp == null) return;

      tmp(this, new PropertyChangedEventArgs(propertyName_));
    }

    protected void InvokePropertyChanged(Expression<Func<object>> property_)
    {
      this.InvokePropertyChangedUsingLambda(PropertyChanged, property_);
    }
  }
}