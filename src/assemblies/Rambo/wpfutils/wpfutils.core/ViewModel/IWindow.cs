using System;

namespace MorganStanley.MSDesktop.Rambo.WpfUtils.Core.ViewModel
{
  /// <summary>
  /// Interface for Window
  /// </summary>
  [Obsolete("Use View.IWindow instead")]
  public interface IWindow : IView
  {
    /// <summary>
    /// Opens the window and return
    /// </summary>
    void Show();
    /// <summary>
    /// Opens the window and returns only when the newly created window is closed.
    /// </summary>
    /// <returns>True if DialogResult was set to true, otherwise false.</returns>
    bool ShowModal();
    /// <summary>
    /// Close the window
    /// </summary>
    void CloseView();
  }
}