﻿using System;
using System.Windows;
using System.Windows.Input;

namespace MorganStanley.MSDesktop.Rambo.WpfUtils.Core.Behaviours
{
  public static class EventBehaviourFactory
  {
    public static DependencyProperty CreateCommandExecutionEventBehaviour(RoutedEvent routedEvent_, string propertyName_, Type ownerType_, bool handledEventsToo_)
    {
      return CreateCommandExecutionEventBehaviour(routedEvent_, propertyName_, ownerType_, false, handledEventsToo_);
    }

    public static DependencyProperty CreateCommandExecutionEventBehaviour(RoutedEvent routedEvent_, string propertyName_, Type ownerType_, bool handleEvent_, bool handledEventsToo_)
    {
      DependencyProperty property =
        DependencyProperty.RegisterAttached(propertyName_, typeof(ICommand), ownerType_,
                                            new PropertyMetadata(null, new ExecuteCommandOnRoutedEventBehaviour(routedEvent_,handleEvent_,  handledEventsToo_).PropertyChangedHandler));

      return property;
    }

    /// <summary>  
    /// An internal class to handle listening for an event and executing a command,  
    /// when a Command is assigned to a particular DependencyProperty  
    /// </summary>  
    private class ExecuteCommandOnRoutedEventBehaviour : ExecuteCommandBehaviour
    {
      private readonly RoutedEvent _routedEvent;
      private readonly bool _handleEvent;
      private readonly bool _handleEventsToo;

      public ExecuteCommandOnRoutedEventBehaviour(RoutedEvent routedEvent_, bool handleEvent_, bool handledEventsToo_)
      {
        _routedEvent = routedEvent_;
        _handleEvent = handleEvent_;
        _handleEventsToo = handledEventsToo_;
      }

      /// <summary>  
      /// Handles attaching or Detaching Event handlers when a Command is assigned or unassigned  
      /// </summary>  
      /// <param name="sender_"></param>  
      /// <param name="oldValue_"></param>  
      /// <param name="newValue_"></param>  
      protected override void AdjustEventHandlers(DependencyObject sender_, object oldValue_, object newValue_)
      {
        UIElement element = sender_ as UIElement;
        if (element == null) { return; }

        if (oldValue_ != null)
        {
          element.RemoveHandler(_routedEvent, new RoutedEventHandler(EventHandler));
        }

        if (newValue_ != null)
        {
          element.AddHandler(_routedEvent, new RoutedEventHandler(EventHandler), _handleEventsToo);
        }
      }

      protected void EventHandler(object sender, RoutedEventArgs e)
      {
        HandleEvent(sender, e);
        if (_handleEvent) 
        {
          e.Handled = true;
        }
      }
    }

    internal abstract class ExecuteCommandBehaviour
    {
      protected DependencyProperty _property;
      protected abstract void AdjustEventHandlers(DependencyObject sender_, object oldValue_, object newValue_);

      protected void HandleEvent(object sender, EventArgs e)
      {
        DependencyObject dp = sender as DependencyObject;
        if (dp == null)
        {
          return;
        }

        ICommand command = dp.GetValue(_property) as ICommand;

        if (command == null)
        {
          return;
        }

        if (command.CanExecute(e))
        {
          command.Execute(e);
        }
      }

      /// <summary>  
      /// Listens for a change in the DependencyProperty that we are assigned to, and  
      /// adjusts the EventHandlers accordingly  
      /// </summary>  
      /// <param name="sender_"></param>  
      /// <param name="e_"></param>  
      public void PropertyChangedHandler(DependencyObject sender_, DependencyPropertyChangedEventArgs e_)
      {
        // the first time the property changes,  
        // make a note of which property we are supposed  
        // to be watching  
        if (_property == null)
        {
          _property = e_.Property;
        }

        object oldValue = e_.OldValue;
        object newValue = e_.NewValue;

        AdjustEventHandlers(sender_, oldValue, newValue);
      }
    }
  }
}