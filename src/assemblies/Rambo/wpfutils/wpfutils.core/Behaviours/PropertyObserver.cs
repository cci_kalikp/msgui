﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Windows;

namespace MorganStanley.MSDesktop.Rambo.WpfUtils.Core.Behaviours
{
  /// <summary>
  /// Monitors the PropertyChanged event of an object that implements INotifyPropertyChanged,
  /// and executes callback methods (i.e. handlers) registered for properties of that object.
  /// </summary>
  /// <typeparam name="TPropertySource">The type of object to monitor for property changes.</typeparam>
  public class PropertyObserver<TPropertySource> : IWeakEventListener
    where TPropertySource : INotifyPropertyChanged
  {
    #region Constructor

    /// <summary>
    /// Initializes a new instance of PropertyObserver, which
    /// observes the 'propertySource' object for property changes.
    /// </summary>
    /// <param name="propertySource_">The object to monitor for property changes.</param>
    public PropertyObserver(TPropertySource propertySource_)
    {
      if (propertySource_ == null)
        throw new ArgumentNullException("propertySource_");

      _propertySourceRef = new WeakReference(propertySource_);
      _propertyNameToHandlerMap = new Dictionary<string, Action<TPropertySource>>();
    }

    #endregion // Constructor

    #region Public Methods

    #region RegisterHandler
    
    /// <summary>
    /// Registers a callback to be invoked when the PropertyChanged event has been raised for the specified property.
    /// </summary>
    /// <param name="expression_">A lambda expression like 'n => n.PropertyName'.</param>
    /// <param name="handler_">The callback to invoke when the property has changed.</param>
    /// <returns>The object on which this method was invoked, to allow for multiple invocations chained together.</returns>
    public PropertyObserver<TPropertySource> RegisterHandler(
      Expression<Func<TPropertySource, object>> expression_,
      Action<TPropertySource> handler_)
    {
      if (expression_ == null)
        throw new ArgumentNullException("expression_");

      string propertyName = GetPropertyName(expression_);
      if (String.IsNullOrEmpty(propertyName))
        throw new ArgumentException("'expression_' did not provide a property name.");

      if (handler_ == null)
        throw new ArgumentNullException("handler_");

      TPropertySource propertySource = GetPropertySource();
      if (propertySource != null)
      {
        Debug.Assert(!_propertyNameToHandlerMap.ContainsKey(propertyName),
                     "Why is the '" + propertyName + "' property being registered again?");

        _propertyNameToHandlerMap[propertyName] = handler_;
        PropertyChangedEventManager.AddListener(propertySource, this, propertyName);
      }

      return this;
    }

    #endregion // RegisterHandler

    #region UnregisterHandler

    /// <summary>
    /// Removes the callback associated with the specified property.
    /// </summary>
    /// <param name="expression_">A lambda expression_ like 'n => n.PropertyName'.</param>
    /// <returns>The object on which this method was invoked, to allow for multiple invocations chained together.</returns>
    public PropertyObserver<TPropertySource> UnregisterHandler(Expression<Func<TPropertySource, object>> expression_)
    {
      if (expression_ == null)
        throw new ArgumentNullException("expression_");

      string propertyName = GetPropertyName(expression_);
      if (String.IsNullOrEmpty(propertyName))
        throw new ArgumentException("'expression_' did not provide a property name.");

      TPropertySource propertySource = GetPropertySource();
      if (propertySource != null)
      {
        if (_propertyNameToHandlerMap.ContainsKey(propertyName))
        {
          _propertyNameToHandlerMap.Remove(propertyName);
          PropertyChangedEventManager.RemoveListener(propertySource, this, propertyName);
        }
      }

      return this;
    }

    #endregion // UnregisterHandler

    #endregion // Public Methods

    #region IWeakEventListener Members

    bool IWeakEventListener.ReceiveWeakEvent(Type managerType_, object sender_, EventArgs e_)
    {
      bool handled = false;

      if (managerType_ == typeof(PropertyChangedEventManager))
      {
        PropertyChangedEventArgs args = e_ as PropertyChangedEventArgs;
        if (args != null && sender_ is TPropertySource)
        {
          string propertyName = args.PropertyName;
          TPropertySource propertySource = (TPropertySource)sender_;

          if (String.IsNullOrEmpty(propertyName))
          {
            // When the property name is empty, all properties are considered to be invalidated.
            // Iterate over a copy of the list of handlers, in case a handler is registered by a callback.
            foreach (Action<TPropertySource> handler in _propertyNameToHandlerMap.Values.ToArray())
              handler(propertySource);

            handled = true;
          }
          else
          {
            Action<TPropertySource> handler;
            if (_propertyNameToHandlerMap.TryGetValue(propertyName, out handler))
            {
              handler(propertySource);

              handled = true;
            }
          }
        }
      }

      return handled;
    }

    #endregion // IWeakEventListener Members

    #region Private Helpers

    #region GetPropertyName

    static string GetPropertyName(Expression<Func<TPropertySource, object>> expression_)
    {
      var lambda = expression_ as LambdaExpression;
      MemberExpression memberExpression;
      if (lambda.Body is UnaryExpression)
      {
        var unaryExpression = lambda.Body as UnaryExpression;
        memberExpression = unaryExpression.Operand as MemberExpression;
      }
      else
      {
        memberExpression = lambda.Body as MemberExpression;
      }

      Debug.Assert(memberExpression != null, "Please provide a lambda expression_ like 'n => n.PropertyName'");

      if (memberExpression != null)
      {
        var propertyInfo = memberExpression.Member as PropertyInfo;

        return propertyInfo.Name;
      }

      return null;
    }

    #endregion // GetPropertyName

    #region GetPropertySource

    TPropertySource GetPropertySource()
    {
      try
      {
        return (TPropertySource)_propertySourceRef.Target;
      }
      catch
      {
        return default(TPropertySource);
      }
    }

    #endregion // GetPropertySource

    #endregion // Private Helpers

    #region Fields

    readonly Dictionary<string, Action<TPropertySource>> _propertyNameToHandlerMap;
    readonly WeakReference _propertySourceRef;

    #endregion // Fields
  }
}