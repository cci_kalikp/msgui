﻿using System;
using System.Windows;
using System.Windows.Input;

namespace MorganStanley.MSDesktop.Rambo.WpfUtils.Core.Commands
{
  public class VisibilityToggleCommand : ICommand
  {
    public void Execute(object parameter_)
    {
      var x = parameter_ as UIElement;
      if (x == null) return;

      bool isVisible = x.Visibility == Visibility.Visible;
      x.Visibility = isVisible ? Visibility.Collapsed : Visibility.Visible;
    }

    public bool CanExecute(object parameter_)
    {
      return parameter_ is UIElement;
    }

    public event EventHandler CanExecuteChanged;
  }
}
