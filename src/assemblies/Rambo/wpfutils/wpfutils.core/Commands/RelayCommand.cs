﻿using System;
using System.Diagnostics;
using System.Windows.Input;

namespace MorganStanley.MSDesktop.Rambo.WpfUtils.Core.Commands
{
  /// <summary>
  /// This class incorporates two methods of the given class in a command 
  /// to be used as CanExecute and Execute.
  /// </summary>
  public class RelayCommand : ICommand
  {
    #region Readonly & Static Fields

    private Predicate<object> _canExecute;
    private Action<object> _execute;

    #endregion

    #region Constructors

    /// <summary>
    /// Creates a new command instance using the execute method only.
    /// </summary>
    /// <param name="execute_">Execute delegate</param>
    public RelayCommand(Action<object> execute_) : this(execute_, null)
    {
    }

    /// <summary>
    /// Creates a new instance of this class using CanExecute and Execute delegates.
    /// </summary>
    /// <param name="execute_">Execute delegate</param>
    /// <param name="canExecute_">CanExecute delegate</param>
    /// <exception cref="ArgumentNullException">
    /// This is thrown, if <paramref name="execute_"/> is a null reference.
    /// </exception>
    public RelayCommand(Action<object> execute_, Predicate<object> canExecute_)
    {
      if (execute_ == null)
        throw new ArgumentNullException("execute_");

      _execute = execute_;
      _canExecute = canExecute_;
    }

    #endregion

    #region ICommand Members

    /// <summary>
    /// Checks, if command can be performed.
    /// </summary>
    /// <param name="parameter_">Command parameter</param>
    /// <returns><b>false</b>, if action cannot be performed. Otherwise, <b>true</b>.</returns>
    [DebuggerStepThrough]
    public bool CanExecute(object parameter_)
    {
      return _canExecute == null ? true : _canExecute(parameter_);
    }

    /// <summary>
    /// Adds or removes event handlers on changing CanExecute
    /// </summary>
    public event EventHandler CanExecuteChanged
    {
      add { CommandManager.RequerySuggested += value; }
      remove { CommandManager.RequerySuggested -= value; }
    }

    /// <summary>
    /// Performs action using command's Execute delegate
    /// </summary>
    /// <param name="parameter_">Command parameter</param>
    public void Execute(object parameter_)
    {
      _execute(parameter_);
    }

    public void RemoveCommand()
    {
      _canExecute = null;
      _execute = null;
    }

    #endregion
  }
}