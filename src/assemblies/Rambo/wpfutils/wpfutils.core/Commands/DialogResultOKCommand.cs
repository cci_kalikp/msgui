﻿using System;
using System.Windows;
using System.Windows.Input;

namespace MorganStanley.MSDesktop.Rambo.WpfUtils.Core.Commands
{
  public class DialogResultOKCommand : ICommand
  {
    public void Execute(object parameter_)
    {
      var window = parameter_ as Window;
      if (window == null) return;      
      window.DialogResult = true;
      window.Close();
    }

    public bool CanExecute(object parameter_)
    {
      return true;
    }

    public event EventHandler CanExecuteChanged;
  }
}
