﻿using System.Windows;

namespace MorganStanley.MSDesktop.Rambo.WpfUtils
{
  public static class Templates
  {
    private static ComponentResourceKey _linkButtonTemplateKey;

    public static ComponentResourceKey LinkButtonTemplateKey
    {
      get { return _linkButtonTemplateKey ?? (_linkButtonTemplateKey = new ComponentResourceKey(typeof(Templates), "LinkButtonTemplate")); }
    }
  }
}
