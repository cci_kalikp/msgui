﻿using System.Windows;

namespace MorganStanley.MSDesktop.Rambo.WpfUtils.Core.Styles
{
  public sealed class AnimationStyles
  {
    private static ComponentResourceKey _busyAnimationKey;

    private static ComponentResourceKey _smallCircularAnimationKey;

    public static ComponentResourceKey BusyAnimationKey
    {
      get
      {
        return _busyAnimationKey ?? (_busyAnimationKey = new ComponentResourceKey(typeof(AnimationStyles), "BusyAnimation"));
      }
    }

    public static ComponentResourceKey SmallCircularAnimationKey
    {
      get { return _smallCircularAnimationKey ?? (_smallCircularAnimationKey = new ComponentResourceKey(typeof(AnimationStyles), "SmallCircularAnimation")); }
    }
  }
}
