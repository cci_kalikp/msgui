﻿using System.Windows;

namespace MorganStanley.MSDesktop.Rambo.WpfUtils.Core.Styles
{
  public sealed class Brushes
  {
    private static ComponentResourceKey _textBrushKey;

    private static ComponentResourceKey _itemHeaderBackgroundBrushKey;

    private static ComponentResourceKey _footerBrushKey;

    private static ComponentResourceKey _backgroundBrushKey;

    private static ComponentResourceKey _headerBrushKey;

    private static ComponentResourceKey _toolBarBrushKey;

    private static ComponentResourceKey _itemBackgroundBrushKey;

    private static ComponentResourceKey _borderBrushKey;

    private static ComponentResourceKey _popupBackgroundBrushKey;

    public static ComponentResourceKey TextBrushKey
    {
      get { return _textBrushKey ?? (_textBrushKey = new ComponentResourceKey(typeof(Brushes), "TextBrush")); }
    }

    public static ComponentResourceKey ToolBarBrushKey
    {
      get { return _toolBarBrushKey ?? (_toolBarBrushKey = new ComponentResourceKey(typeof(Brushes), "ToolBarBrush")); }
    }

    public static ComponentResourceKey ItemBackgroundBrushKey
    {
      get { return _itemBackgroundBrushKey ?? (_itemBackgroundBrushKey = new ComponentResourceKey(typeof(Brushes), "ItemBackgroundBrush")); }
    }

    public static ComponentResourceKey ItemHeaderBackgroundBrushKey
    {
      get
      {
        return _itemHeaderBackgroundBrushKey
               ?? (_itemHeaderBackgroundBrushKey = new ComponentResourceKey(typeof(Brushes), "ItemHeaderBackgroundBrush"));
      }
    }

    public static ComponentResourceKey FooterBrushKey
    {
      get
      {
        return _footerBrushKey ?? (_footerBrushKey = new ComponentResourceKey(typeof(Brushes), "FooterBrush"));
      }
    }

    public static ComponentResourceKey BackgroundBrushKey
    {
      get
      {
        return _backgroundBrushKey ?? (_backgroundBrushKey = new ComponentResourceKey(typeof(Brushes), "BackgroundBrush"));
      }
    }

    public static ComponentResourceKey HeaderBrushKey
    {
      get
      {
        return _headerBrushKey ?? (_headerBrushKey = new ComponentResourceKey(typeof(Brushes), "HeaderBrush"));
      }
    }

    public static ComponentResourceKey BorderBrushKey
    {
      get
      {
        return _borderBrushKey ?? (_borderBrushKey = new ComponentResourceKey(typeof(Brushes), "BorderBrush"));
      }
    }

    public static ComponentResourceKey PopupBackgroundBrushKey
    {
      get
      {
        return _popupBackgroundBrushKey ?? (_popupBackgroundBrushKey = new ComponentResourceKey(typeof(Brushes), "PopupBackgroundBrush"));
      }
    }

  }
}
