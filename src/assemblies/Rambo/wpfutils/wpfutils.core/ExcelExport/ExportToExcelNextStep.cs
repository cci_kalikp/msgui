﻿using System.Diagnostics;
using MorganStanley.MSDesktop.Rambo.Desktop.Utils.FileSenders;

namespace MorganStanley.MSDesktop.Rambo.WpfUtils.Core.ExcelExport
{
  public class ExportToExcelNextStep : IExportToExcelNextStep
  {
    public void ShowNextSteps(string fileName_, int totalRows_)
    {
      IExportToExcelNextStepView view = new ExportToExcelNextStepView();
      var vm = new ExportToExcelNextStepViewModel(view);
      ExportToExcelNextStepResult nextStep = vm.ShowNextStep(totalRows_);
      switch (nextStep)
      {
        case ExportToExcelNextStepResult.Open:
          var proc = new Process { StartInfo = { FileName = fileName_ } };
          proc.Start();
          break;
        case ExportToExcelNextStepResult.Send:
          var sender = new OutlookFileSender();
          sender.SendFile(fileName_);
          break;
      }
    }
  }
}
