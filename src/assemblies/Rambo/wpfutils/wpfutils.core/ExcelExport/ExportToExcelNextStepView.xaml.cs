﻿using System.Windows;

namespace MorganStanley.MSDesktop.Rambo.WpfUtils.Core.ExcelExport
{
  public partial class ExportToExcelNextStepView : IExportToExcelNextStepView
  {
    public ExportToExcelNextStepView()
    {
      InitializeComponent();
      Owner = Application.Current.MainWindow;
    }

    public object Model
    {
      get { return DataContext; }
      set { DataContext = value; }
    }

    public bool ShowModal()
    {
      bool? result = ShowDialog();
      return result.GetValueOrDefault();
    }

    public void CloseView()
    {
      Close();
    }
  }
}
