﻿using System.Windows.Input;
using MorganStanley.MSDesktop.Rambo.WpfUtils.Core.Commands;

namespace MorganStanley.MSDesktop.Rambo.WpfUtils.Core.ExcelExport
{
  class ExportToExcelNextStepViewModel
  {
    private readonly IExportToExcelNextStepView _view;

    public int TotalNumberRows { get; private set; }

    private ExportToExcelNextStepResult NextStepResult { get; set; }

    public ICommand SendFileAsAttachment { get; private set; }

    public ICommand OpenExportedFile { get; private set; }

    public ExportToExcelNextStepViewModel(IExportToExcelNextStepView view_)
    {
      _view = view_;

      OpenExportedFile = new RelayCommand(ExecuteOpenExportedFile);
      SendFileAsAttachment = new RelayCommand(ExecuteSendFileAsAttachment);
    }

    private void ExecuteOpenExportedFile(object obj_)
    {
      NextStepResult = ExportToExcelNextStepResult.Open;
      _view.CloseView();
    }

    private void ExecuteSendFileAsAttachment(object obj_)
    {
      NextStepResult = ExportToExcelNextStepResult.Send;
      _view.CloseView();
    }

    public ExportToExcelNextStepResult ShowNextStep(int totalRows_)
    {
      TotalNumberRows = totalRows_;

      _view.Model = this;
      _view.ShowModal();

      return NextStepResult;
    }
  }
}
