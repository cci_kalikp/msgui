﻿namespace MorganStanley.MSDesktop.Rambo.WpfUtils.Core.ExcelExport
{
  public interface IExportToExcelNextStep
  {
    void ShowNextSteps(string fileName_, int totalRows_);
  }
}