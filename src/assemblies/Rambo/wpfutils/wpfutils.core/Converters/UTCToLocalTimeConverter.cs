﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace MorganStanley.MSDesktop.Rambo.WpfUtils.Core.Converters
{
  /// <summary>
  /// Converts <see cref="DateTime"/> in UTC to local time.
  /// </summary>
  public class UTCToLocalTimeConverter : IValueConverter
  {
    #region IValueConverter Members

    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value == null) return value;

      var d = (DateTime) value;
      return d.ToLocalTime();
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value == null) return value;

      var d = (DateTime) value;
      return d.ToUniversalTime();
    }

    #endregion
  }
}