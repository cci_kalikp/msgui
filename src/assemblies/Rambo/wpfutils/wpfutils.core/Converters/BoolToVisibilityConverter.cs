using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace MorganStanley.MSDesktop.Rambo.WpfUtils.Core.Converters
{
  public class BoolToVisibilityConverter : IValueConverter
  {
    /// <summary>
    /// Sets the visibility to this if the value is not true.
    /// </summary>
    public Visibility VisibilityIfValueIsFalse { get; set; }

    public BoolToVisibilityConverter()
    {
      VisibilityIfValueIsFalse = Visibility.Collapsed;
    }

    #region IValueConverter Members

    public object Convert(object value_, Type targetType_, object parameter_, CultureInfo culture_)
    {
      bool invert = System.Convert.ToBoolean(parameter_);
      if (value_ is bool)
      {
        var input = (bool) value_;
        if (input)
        {
          return invert ? VisibilityIfValueIsFalse : Visibility.Visible;
        }

        return invert ? Visibility.Visible : VisibilityIfValueIsFalse;
      }

      return value_;
    }

    public object ConvertBack(object value_, Type targetType_, object parameter, CultureInfo culture_)
    {
      throw new NotSupportedException();
    }

    #endregion
  }
}