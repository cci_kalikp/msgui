﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace MorganStanley.MSDesktop.Rambo.WpfUtils.Core.Converters
{
  [ValueConversion(typeof(String), typeof(Visibility))]
  public class HasStringToVisibilityConverter : IValueConverter
  {
    public object Convert(object value_, Type targetType_, object parameter_, CultureInfo culture_)
    {
      if (value_ == null || value_.GetType() != typeof(String))
      {
        return Visibility.Collapsed;
      }

      if (String.IsNullOrEmpty((string)value_))
      {
        return Visibility.Collapsed;
      }

      return Visibility.Visible;
    }

    public object ConvertBack(object value_, Type targetType_, object parameter_, CultureInfo culture_)
    {
      throw new NotSupportedException();
    }
  }
}
