using System;
using System.Globalization;
using System.Windows.Data;

namespace MorganStanley.MSDesktop.Rambo.WpfUtils.Core.Converters
{
  /// <summary>
  /// This class inverts boolean values.
  /// </summary>
  public class InvertBooleanConverter : IValueConverter
  {
    #region IValueConverter Members
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value == null)
      {
        return value;
      }

      if (value is bool && (targetType == typeof(bool) || targetType == typeof(bool?)))
      {
        return !(bool) value;
      }

      throw new NotSupportedException("Target type mismatch. Boolean targets are expected.");
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value == null)
      {
        return value;
      }

      if (value is bool && targetType == typeof(bool) || targetType == typeof(bool?))
      {
        return !(bool)value;
      }

      throw new NotSupportedException("Target type mismatch. Boolean targets are expected.");
    } 
    #endregion
  }
}