﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace MorganStanley.MSDesktop.Rambo.WpfUtils.Core.Converters
{
  public class ParameterValueToBoolConverter : IValueConverter
  {
    public object Convert(object value_, Type targetType_, object parameter_, CultureInfo culture_)
    {
      return Equals(value_, parameter_);
    }

    public object ConvertBack(object value_, Type targetType_, object parameter_, CultureInfo culture_)
    {
      var boolValue = (bool)value_;
      return boolValue ? parameter_ : Binding.DoNothing;
    }
  }
}
