﻿using System;
using System.Drawing;
using System.Globalization;
using System.Reflection;
using System.Windows;
using System.Windows.Data;
using System.Windows.Interop;
using System.Windows.Media.Imaging;

namespace MorganStanley.MSDesktop.Rambo.WpfUtils.Core.Converters
{
  [ValueConversion(typeof(String), typeof(BitmapSource))]
  public class SystemIconConverter : IValueConverter
  {
    public object Convert(object value_, Type targetType_, object parameter_, CultureInfo culture_)
    {
      var icon = (Icon)typeof(SystemIcons).GetProperty(parameter_.ToString(), BindingFlags.Public | BindingFlags.Static).GetValue(null, null); 
      BitmapSource bs = Imaging.CreateBitmapSourceFromHIcon(icon.Handle, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions()); 
      return bs; 
    }

    public object ConvertBack(object value_, Type targetType_, object parameter_, CultureInfo culture_)
    {
      throw new NotSupportedException(); 
    }
  }
}
