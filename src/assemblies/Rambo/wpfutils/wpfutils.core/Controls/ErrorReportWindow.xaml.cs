﻿using MorganStanley.MSDesktop.Rambo.WpfUtils.Core.ViewModel;

namespace MorganStanley.MSDesktop.Rambo.WpfUtils.Core.Controls
{
  /// <summary>
  /// Interaction logic for ErrorReportWindow.xaml
  /// </summary>
  public partial class ErrorReportWindow : IErrorReportWindowView
  {
    public ErrorReportWindow()
    {
      InitializeComponent();
    }

    #region IErrorReportWindowView Members

    public object Model
    {
      set { DataContext = value; }
    }

    public bool ShowModal()
    {
      bool? result = ShowDialog();
      return result.HasValue && result.Value;
    }

    #endregion
  }
}