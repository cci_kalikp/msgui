﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;

namespace MorganStanley.MSDesktop.Rambo.WpfUtils.Core.Controls
{
  public enum SearchMode
  {
    /// <summary>
    /// Live filtering or searching of data
    /// </summary>
    Instant,
    /// <summary>
    /// Search icon or ENTER key need to be pressed in order for the search to take place
    /// </summary>
    Delayed,
  }

  [TemplatePart(Name = TEMPLATEPART_ICONBORDER, Type = typeof(Border))]
  [TemplatePart(Name = TEMPLATEPART_CONTENTHOST, Type = typeof(ScrollViewer))]
  public class SearchTextBox : TextBox
  {
    private const string TEMPLATEPART_ICONBORDER = "PART_SearchIconBorder";
    private const string TEMPLATEPART_CONTENTHOST = "PART_ContentHost";

    public static readonly ResourceKey BackgroundKey = 
      new ComponentResourceKey(typeof (SearchTextBox), "BackgroundKey");
    public static readonly ResourceKey ForegroundKey =
      new ComponentResourceKey(typeof(SearchTextBox), "ForegroundKey");
    public static readonly ResourceKey BorderKey =
      new ComponentResourceKey(typeof (SearchTextBox), "BorderKey");
    public static readonly ResourceKey Border_MouseOverKey =
  new ComponentResourceKey(typeof(SearchTextBox), "Border_MouseOverKey");
    public static readonly ResourceKey SearchIconBorderKey =
      new ComponentResourceKey(typeof(SearchTextBox), "SearchIconBorderKey");
    public static readonly ResourceKey SearchIconBorder_MouseOverKey =
      new ComponentResourceKey(typeof(SearchTextBox), "SearchIconBorder_MouseOverKey");
    public static readonly ResourceKey SearchIconBackgroundKey =
      new ComponentResourceKey(typeof(SearchTextBox), "SearchIconBackgroundKey");
    public static readonly ResourceKey SearchIconBackground_MouseOverKey =
      new ComponentResourceKey(typeof(SearchTextBox), "SearchIconBackground_MouseOverKey");    
    public static readonly ResourceKey SearchIconBorder_MouseDownKey =
      new ComponentResourceKey(typeof(SearchTextBox), "SearchIconBorder_MouseDownKey");
    public static readonly ResourceKey SearchIconBackground_MouseDownKey =
      new ComponentResourceKey(typeof(SearchTextBox), "SearchIconBackground_MouseDownKey");    
    public static readonly ResourceKey PlaceholderTextColorKey =
      new ComponentResourceKey(typeof(SearchTextBox), "PlaceholderTextColorKey");


    private static readonly DependencyPropertyKey HasTextPropertyKey =
      DependencyProperty.RegisterReadOnly(
        "HasText",
        typeof (bool),
        typeof (SearchTextBox),
        new PropertyMetadata());

    public static DependencyProperty HasTextProperty = HasTextPropertyKey.DependencyProperty;

    private static readonly DependencyPropertyKey IsMouseLeftButtonDownPropertyKey =
      DependencyProperty.RegisterReadOnly(
        "IsMouseLeftButtonDown",
        typeof (bool),
        typeof (SearchTextBox),
        new PropertyMetadata());

    public static DependencyProperty IsMouseLeftButtonDownProperty = IsMouseLeftButtonDownPropertyKey.DependencyProperty;

    public static readonly DependencyProperty PlaceholderTextColorProperty =
      DependencyProperty.Register(
        "PlaceholderTextColor",
        typeof (Brush),
        typeof (SearchTextBox));

    public static readonly DependencyProperty PlaceholderTextProperty =
      DependencyProperty.Register(
        "PlaceholderText",
        typeof (string),
        typeof (SearchTextBox));

    public static readonly RoutedEvent SearchEvent =
      EventManager.RegisterRoutedEvent(
        "Search",
        RoutingStrategy.Bubble,
        typeof (RoutedEventHandler),
        typeof (SearchTextBox));


    public static DependencyProperty SearchEventTimeDelayProperty =
      DependencyProperty.Register(
        "SearchEventTimeDelay",
        typeof (Duration),
        typeof (SearchTextBox),
        new FrameworkPropertyMetadata(
          new Duration(new TimeSpan(0, 0, 0, 0, 100)),
          new PropertyChangedCallback(OnSearchEventTimeDelayChanged)));

    /// <summary>
    /// The type of search that will be taking place: instant or delayed. The default will be instant.
    /// </summary>
    public static DependencyProperty SearchModeProperty =
      DependencyProperty.Register(
        "SearchMode",
        typeof (SearchMode),
        typeof (SearchTextBox),
        new PropertyMetadata(SearchMode.Instant));

    static SearchTextBox()
    {
      DefaultStyleKeyProperty.OverrideMetadata(
        typeof (SearchTextBox),
        new FrameworkPropertyMetadata(typeof (SearchTextBox)));
    }

    public SearchTextBox()
    {
      SearchEventDelayTimer = new DispatcherTimer {Interval = SearchEventTimeDelay.TimeSpan};
      SearchEventDelayTimer.Tick += OnSeachEventDelayTimerTick;
    }

    public string PlaceholderText
    {
      get { return (string) GetValue(PlaceholderTextProperty); }
      set { SetValue(PlaceholderTextProperty, value); }
    }

    public Brush PlaceholderTextColor
    {
      get { return (Brush) GetValue(PlaceholderTextColorProperty); }
      set { SetValue(PlaceholderTextColorProperty, value); }
    }

    public SearchMode SearchMode
    {
      get { return (SearchMode) GetValue(SearchModeProperty); }
      set { SetValue(SearchModeProperty, value); }
    }

    public bool HasText
    {
      get { return (bool) GetValue(HasTextProperty); }
      private set { SetValue(HasTextPropertyKey, value); }
    }

    public bool IsMouseLeftButtonDown
    {
      get { return (bool) GetValue(IsMouseLeftButtonDownProperty); }
      private set { SetValue(IsMouseLeftButtonDownPropertyKey, value); }
    }

    public Duration SearchEventTimeDelay
    {
      get { return (Duration) GetValue(SearchEventTimeDelayProperty); }
      set { SetValue(SearchEventTimeDelayProperty, value); }
    }

    private DispatcherTimer SearchEventDelayTimer { get; set; }

    public event RoutedEventHandler Search
    {
      add { AddHandler(SearchEvent, value); }
      remove { RemoveHandler(SearchEvent, value); }
    }

    protected override void OnTextChanged(TextChangedEventArgs e_)
    {
      base.OnTextChanged(e_);

      HasText = (Text.Length != 0);

      if (SearchMode != SearchMode.Instant) return;

      SearchEventDelayTimer.Stop();
      SearchEventDelayTimer.Start();
    }

    public override void OnApplyTemplate()
    {
      base.OnApplyTemplate();

      var iconBorder = GetTemplateChild(TEMPLATEPART_ICONBORDER) as Border;
      if (iconBorder != null)
      {
        iconBorder.MouseLeftButtonDown += IconBorder_MouseLeftButtonDown;
        iconBorder.MouseLeftButtonUp += IconBorder_MouseLeftButtonUp;
        iconBorder.MouseLeave += IconBorder_MouseLeave;
      }
    }

    private void OnSeachEventDelayTimerTick(object o_, EventArgs e_)
    {
      SearchEventDelayTimer.Stop();
      RaiseSearchEvent();
    }

    private void RaiseSearchEvent()
    {
      var args = new RoutedEventArgs(SearchEvent);
      RaiseEvent(args);
    }

    private void IconBorder_MouseLeftButtonDown(object obj_, MouseButtonEventArgs e_)
    {
      IsMouseLeftButtonDown = true;
    }

    private void IconBorder_MouseLeftButtonUp(object obj_, MouseButtonEventArgs e_)
    {
      if (!IsMouseLeftButtonDown) return;

      if (HasText && SearchMode == SearchMode.Instant)
      {
        Text = String.Empty;
      }

      if (HasText && SearchMode == SearchMode.Delayed)
      {
        RaiseSearchEvent();
      }

      IsMouseLeftButtonDown = false;
    }

    private void IconBorder_MouseLeave(object obj_, MouseEventArgs e_)
    {
      IsMouseLeftButtonDown = false;
    }

    private static void OnSearchEventTimeDelayChanged(DependencyObject d_, DependencyPropertyChangedEventArgs e_)
    {
      var stb = d_ as SearchTextBox;
      if (stb == null) return;

      stb.SearchEventDelayTimer.Interval = ((Duration) e_.NewValue).TimeSpan;
      stb.SearchEventDelayTimer.Stop();
    }
  }
}