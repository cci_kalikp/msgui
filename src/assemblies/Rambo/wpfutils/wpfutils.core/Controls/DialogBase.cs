﻿using System;
using System.Windows;

namespace MorganStanley.MSDesktop.Rambo.WpfUtils.Core.Controls
{
  /// <summary>
  /// Base class to create WPF dialog window 
  /// without any button or icon in the title section of the window
  /// </summary>
  public abstract class DialogBase : Window
  {
    protected DialogBase()
    {
      ResizeMode = ResizeMode.NoResize;
      ShowInTaskbar = false;
      Topmost = true;
      AllowsTransparency = true;
      WindowStartupLocation = WindowStartupLocation.CenterOwner;
    }

    protected override void OnSourceInitialized(EventArgs e)
    {
      WindowExtensions.HideWindowHeaderButtons(this);
    }
  }

  internal static class WindowExtensions
  {
    internal static void HideWindowHeaderButtons(Window window)
    {
      // Get window handler
      IntPtr hwnd = new System.Windows.Interop.WindowInteropHelper(window).Handle;

      // Get current window style value
      long value = NativeMethods.GetWindowLong(hwnd, NativeMethods.GWL_STYLE);

      // Change the window style with the new value
      NativeMethods.SetWindowLong(hwnd, NativeMethods.GWL_STYLE, value & NativeMethods.WS_CAPTION);
    }
  }
}