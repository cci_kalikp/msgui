﻿using System;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace MorganStanley.MSDesktop.Rambo.WpfUtils.Core.Controls
{
  public enum WpfMessageBoxButton
  {
    OK,
    OKCancel,
    YesNo,
    YesNoCancel
  }

  public enum WpfMessageBoxResult
  {
    None,
    OK,
    Cancel,
    Yes,
    No,
  }

  public enum WpfMessageBoxImage
  {
    None,
    Error,
    Information,
    Question,
    Warning,
  }

  /// <summary>
  /// Interaction logic for WpfMessageBox.xaml
  /// </summary>
  public partial class WpfMessageBox
  {
    private WpfMessageBoxResult MessageBoxResult { get; set; }
    private WpfMessageBoxButton MessageBoxButton { get; set; }
    private DispatcherTimer CloseDelayTimer { get; set; }

    public static ResourceKey ControlBackgroundKey = new ComponentResourceKey(typeof (WpfMessageBox),
                                                                                   "ControlBackgroundKey");


    public WpfMessageBox()
    {
      InitializeComponent();

      Closing += WpfMessageBox_Closing;
      _btnOk.Click += _btnOk_Click;
      _btnCancel.Click += _btnCancel_Click;
      _btnYes.Click += _btnYes_Click;
      _btnNo.Click += _btnNo_Click;

      CloseDelayTimer = new DispatcherTimer();
      CloseDelayTimer.Tick += CloseDelayTimer_Tick;
    }

    /// <summary>
    /// Displays a message box that has a message and that returns a result.
    /// </summary>
    /// <param name="messageBoxText_">
    /// <see cref="string"/> that specifies the text to display.
    /// </param>
    /// <returns>
    /// A <see cref="WpfMessageBoxResult"/>value that specifies which message box button is clicked by the user.
    /// </returns>
    public static WpfMessageBoxResult Show(string messageBoxText_)
    {
      return ShowCore(messageBoxText_, String.Empty, default(WpfMessageBoxButton), default(WpfMessageBoxImage));
    }
    /// <summary>
    /// Displays a message box that has a message and title bar caption; and that returns a result.
    /// </summary>
    /// <param name="messageBoxText_">
    /// <see cref="string"/> that specifies the text to display.
    /// </param>
    /// <param name="title_">
    /// <see cref="string"/> that specifies the title bar caption to display.
    /// </param>
    /// <returns>
    /// A <see cref="WpfMessageBoxResult"/>value that specifies which message box button is clicked by the user.
    /// </returns>
    public static WpfMessageBoxResult Show(string messageBoxText_, string title_)
    {
      return ShowCore(messageBoxText_, title_, default(WpfMessageBoxButton), default(WpfMessageBoxImage));
    }
    /// <summary>
    /// Displays a message box that has a message and title bar caption; and that returns a result.
    /// </summary>
    /// <param name="messageBoxText_">
    /// <see cref="string"/> that specifies the text to display.
    /// </param>
    /// <param name="title_">
    /// <see cref="string"/> that specifies the title bar caption to display.
    /// </param>
    /// <param name="button_">
    /// A <see cref="WpfMessageBoxButton"/> value that specifies which button or buttons to display.
    /// </param>
    /// <returns>
    /// A <see cref="WpfMessageBoxResult"/>value that specifies which message box button is clicked by the user.
    /// </returns>
    public static WpfMessageBoxResult Show(string messageBoxText_, string title_, WpfMessageBoxButton button_)
    {
      return ShowCore(messageBoxText_, title_, button_, default(WpfMessageBoxImage));
    }
    /// <summary>
    /// Displays a message box that has a message and title bar caption; and that returns a result.
    /// </summary>
    /// <param name="messageBoxText_">
    /// <see cref="string"/> that specifies the text to display.
    /// </param>
    /// <param name="title_">
    /// <see cref="string"/> that specifies the title bar caption to display.
    /// </param>
    /// <param name="button_">
    /// A <see cref="WpfMessageBoxButton"/> value that specifies which button or buttons to display.
    /// </param>
    /// <param name="image_">
    /// A <see cref="WpfMessageBoxImage"/> value that specifies the icon to display.
    /// </param>
    /// <returns>
    /// A <see cref="WpfMessageBoxResult"/>value that specifies which message box button is clicked by the user.
    /// </returns>
    public static WpfMessageBoxResult Show(string messageBoxText_, string title_, WpfMessageBoxButton button_, WpfMessageBoxImage image_)
    {
      return ShowCore(messageBoxText_, title_, button_, image_);
    }

    public static void ShowVanishing(string messageBoxText_, string title_, WpfMessageBoxButton button_, WpfMessageBoxImage image_, TimeSpan interval_)
    {
      var dialog = new WpfMessageBox {MessageBoxButton = button_, Title = title_};
      dialog._messageBox.Text = messageBoxText_;
      dialog.SetImage(image_);
      dialog.SetVisibility(button_);
      dialog.CloseDelayTimer.Interval = interval_;

      dialog.Show();
      dialog.CloseDelayTimer.Start();
    }

    private static WpfMessageBoxResult ShowCore(string messageBoxText_, string title_, WpfMessageBoxButton button_, WpfMessageBoxImage image_)
    {
      var dialog = new WpfMessageBox {MessageBoxButton = button_, Title = title_};
      dialog._messageBox.Text = messageBoxText_;
      dialog.SetImage(image_);
      dialog.SetVisibility(button_);
      dialog.ShowDialog();

      return dialog.MessageBoxResult;
    }

    private void WpfMessageBox_Closing(object sender, System.ComponentModel.CancelEventArgs e)
    {
      if (MessageBoxButton == WpfMessageBoxButton.YesNo &&
          MessageBoxResult != WpfMessageBoxResult.Yes && MessageBoxResult != WpfMessageBoxResult.No)
          e.Cancel = true;

      CloseDelayTimer.Stop();
    }

    private void _btnNo_Click(object sender, RoutedEventArgs e)
    {
      MessageBoxResult = WpfMessageBoxResult.No;
      Close();
    }

    private void _btnYes_Click(object sender, RoutedEventArgs e)
    {
      MessageBoxResult = WpfMessageBoxResult.Yes;
      Close();
    }

    private void _btnCancel_Click(object sender, RoutedEventArgs e)
    {
      MessageBoxResult = WpfMessageBoxResult.Cancel;
      Close();
    }

    private void _btnOk_Click(object sender, RoutedEventArgs e)
    {
      MessageBoxResult = WpfMessageBoxResult.OK;
      Close();
    }

    private void CloseDelayTimer_Tick(object sender, EventArgs e)
    {
      VerifyAccess();

      Close();
    }

    private void SetImage(WpfMessageBoxImage image_)
    {
      switch (image_)
      {
        case WpfMessageBoxImage.None:
          break;
        case WpfMessageBoxImage.Error:
          _image.Source = new BitmapImage(new Uri(@"..\Images\close_b_48.png", UriKind.Relative));
          break;
        case WpfMessageBoxImage.Information:
          _image.Source = new BitmapImage(new Uri(@"..\Images\info_48.png", UriKind.Relative));
          break;
        case WpfMessageBoxImage.Question:
          _image.Source = new BitmapImage(new Uri(@"..\Images\help_48.png", UriKind.Relative));
          break;
        case WpfMessageBoxImage.Warning:
          _image.Source = new BitmapImage(new Uri(@"..\Images\warning_48.png", UriKind.Relative));
          break;
        default:
          throw new ArgumentOutOfRangeException("image_");
      }
    }

    private void SetVisibility(WpfMessageBoxButton button_)
    {
      switch (button_)
      {
        case WpfMessageBoxButton.OK:
          _btnOk.IsDefault = true;
          _btnOk.Visibility = Visibility.Visible;
          _btnCancel.Visibility = Visibility.Collapsed;
          _btnYes.Visibility = Visibility.Collapsed;
          _btnNo.Visibility = Visibility.Collapsed;
          break;

        case WpfMessageBoxButton.OKCancel:
          _btnCancel.IsDefault = true;
          _btnOk.Visibility = Visibility.Visible;
          _btnCancel.Visibility = Visibility.Visible;
          _btnYes.Visibility = Visibility.Collapsed;
          _btnNo.Visibility = Visibility.Collapsed;
          break;

        case WpfMessageBoxButton.YesNo:
          _btnNo.IsDefault = true;
          _btnOk.Visibility = Visibility.Collapsed;
          _btnCancel.Visibility = Visibility.Collapsed;
          _btnYes.Visibility = Visibility.Visible;
          _btnNo.Visibility = Visibility.Visible;
          break;
        case WpfMessageBoxButton.YesNoCancel:
          _btnCancel.IsDefault = true;
          _btnOk.Visibility = Visibility.Collapsed;
          _btnCancel.Visibility = Visibility.Visible;
          _btnYes.Visibility = Visibility.Visible;
          _btnNo.Visibility = Visibility.Visible;
          break;
        default:
          throw new ArgumentOutOfRangeException("button_");
      }
    }

  }
}