﻿using System.Windows;
using System.Windows.Forms;



namespace MorganStanley.MSDesktop.Rambo.WpfUtils.Core.Extensions
{
  public static class  WPFShowWindowLocationExtensiions
  {
    public static void ShowWPFWindowInCenter(this Window windowToShow_, Control parent_)
    {
      SetCoordinates(windowToShow_, parent_);
      windowToShow_.Show();
    }


    public static bool? ShowDialogWPFWindowInCenter(this Window windowToShow_, Control parent_)
    {
      SetCoordinates(windowToShow_, parent_);
      return windowToShow_.ShowDialog();
    }

    public static void SetCoordinates(this Window windowToShow_, Control parent_)
    {
      windowToShow_.WindowStartupLocation = WindowStartupLocation.Manual;
      var rvControl = parent_;
      var searchX = (rvControl.Width / 2.0) - windowToShow_.Width / 2;
      var searchY = (rvControl.Height / 2.0) - windowToShow_.Height / 2;

      var point = rvControl.PointToScreen(new System.Drawing.Point((int)searchX, (int)searchY));

      windowToShow_.Left = point.X;
      windowToShow_.Top = point.Y;
    }
  }
}
