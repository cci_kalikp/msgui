﻿
using System.Windows;
using System.Windows.Media;

namespace MorganStanley.MSDesktop.Rambo.WpfUtils.Core.Extensions
{
  public static class WPFTreeExtensions
  {

    /// <summary>
    /// Looks for a parent element of specific type in the visual/logical tree and once it is found returns it
    /// </summary>
    /// <typeparam name="T">type of the visual element to find</typeparam>
    /// <param name="child_">child element to start the search from</param>
    /// <returns>null if nothing was found or the element of the type T</returns>
    
    public static T FindParent<T>(this DependencyObject child_)
   where T : DependencyObject
    {
      //get parent item
      DependencyObject parentObject = child_.GetParentObject();

      //we've reached the end of the tree
      if (parentObject == null) return null;

      //check if the parent matches the type we're looking for
      var parent = parentObject as T;
      return parent ?? FindParent<T>(parentObject);

      //use recursion to proceed with next level
    }

    /// <summary>
    /// Gets the parent of the dependency object 
    /// </summary>
    /// <param name="child_"> child element</param>
    /// <returns>Parent of the child element</returns>
    public static DependencyObject GetParentObject(this DependencyObject child_)
    {
      if (child_ == null) return null;

      //handle content elements separately
      var contentElement = child_ as ContentElement;
      if (contentElement != null)
      {
        var parent = ContentOperations.GetParent(contentElement);
        if (parent != null) return parent;

        var fce = contentElement as FrameworkContentElement;
        return fce != null ? fce.Parent : null;
      }

      //also try searching for parent in framework elements (such as DockPanel, etc)
      var frameworkElement = child_ as FrameworkElement;
      if (frameworkElement != null)
      {
        DependencyObject parent = frameworkElement.Parent;
        if (parent != null) return parent;
      }

      //if it's not a ContentElement/FrameworkElement, rely on VisualTreeHelper
      return VisualTreeHelper.GetParent(child_);
    }
  }
}
