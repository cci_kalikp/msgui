﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Markup;

[assembly: XmlnsPrefix("http://wpfutils.ecdev.morganstanley.com/Commands", "wpfutilsCoreCommands")]
[assembly: XmlnsDefinition("http://wpfutils.ecdev.morganstanley.com/Commands", "MorganStanley.MSDesktop.Rambo.WpfUtils.Core.Commands")]
[assembly: XmlnsPrefix("http://wpfutils.ecdev.morganstanley.com/Controls", "wpfutilsCoreControls")]
[assembly: XmlnsDefinition("http://wpfutils.ecdev.morganstanley.com/Controls", "MorganStanley.MSDesktop.Rambo.WpfUtils.Core.Controls")]
[assembly: XmlnsPrefix("http://wpfutils.ecdev.morganstanley.com/Converters", "wpfutilsCoreConverters")]
[assembly: XmlnsDefinition("http://wpfutils.ecdev.morganstanley.com/Converters", "MorganStanley.MSDesktop.Rambo.WpfUtils.Core.Converters")]
[assembly: XmlnsPrefix("http://wpfutils.ecdev.morganstanley.com/Helpers", "wpfutilsCoreHelpers")]
[assembly: XmlnsDefinition("http://wpfutils.ecdev.morganstanley.com/Helpers", "MorganStanley.MSDesktop.Rambo.WpfUtils.Core.Helpers")]
[assembly: XmlnsPrefix("http://wpfutils.ecdev.morganstanley.com/Behaviours", "wpfutilsCoreBehaviours")]
[assembly: XmlnsDefinition("http://wpfutils.ecdev.morganstanley.com/Behaviours", "MorganStanley.MSDesktop.Rambo.WpfUtils.Core.Behaviours")]
[assembly: XmlnsPrefix("http://wpfutils.ecdev.morganstanley.com/ViewModel", "wpfutilsCoreViewModel")]
[assembly: XmlnsDefinition("http://wpfutils.ecdev.morganstanley.com/ViewModel", "MorganStanley.MSDesktop.Rambo.WpfUtils.Core.ViewModel")]
[assembly: XmlnsPrefix("http://wpfutils.ecdev.morganstanley.com/Styles", "wpfutilsCoreStyles")]
[assembly: XmlnsDefinition("http://wpfutils.ecdev.morganstanley.com/Styles", "MorganStanley.MSDesktop.Rambo.WpfUtils.Core.Styles")]
[assembly: XmlnsPrefix("http://wpfutils.ecdev.morganstanley.com/Templates", "wpfutilsCoreTemplates")]
[assembly: XmlnsDefinition("http://wpfutils.ecdev.morganstanley.com/Templates", "MorganStanley.MSDesktop.Rambo.WpfUtils")]

namespace MorganStanley.MSDesktop.Rambo.WpfUtils.Core
{
  internal static class NativeMethods
  {
    internal static int GWL_STYLE = -16;
    internal static uint WS_CAPTION = 0x00C00000; // WS_BORDER | WS_DLGFRAME

    [DllImport("user32.dll")]
    internal extern static int SetWindowLong(IntPtr hwnd, int index, long value);

    [DllImport("user32.dll")]
    internal extern static long GetWindowLong(IntPtr hwnd, int index);
  }
}
