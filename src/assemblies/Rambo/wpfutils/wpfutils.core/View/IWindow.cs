﻿namespace MorganStanley.MSDesktop.Rambo.WpfUtils.Core.View
{
  public interface IWindow
  {
    object DataContext { set; }
    void Show();
    bool? ShowDialog();
    void Close();
  }
}
