﻿using System;
using System.Collections;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using Infragistics.Documents.Excel;
using Infragistics.Windows.DataPresenter;
using Infragistics.Windows.DataPresenter.ExcelExporter;
using JetBrains.Annotations;
using Microsoft.Win32;
using MorganStanley.MSDesktop.Rambo.WpfUtils.Core.ExcelExport;

namespace MorganStanley.MSDesktop.Rambo.WpfUtils.Commands
{
  public class ExportDataPresenterBaseToExcelCommand : ICommand
  {
    private const int INVALID_NUMBER_OF_VISIBLE_FIELDS = -1;

    private int _numberOfVisibleFields = INVALID_NUMBER_OF_VISIBLE_FIELDS;

    public ExportDataPresenterBaseToExcelCommand()
    {
      MessageBoxTitle = "Export to Excel";
      DefaultFileName = "GridExport";
      HeaderBackgroundColor = Colors.LightGray;
      HeaderFontWeight = FontWeights.Bold;
      FormatString = @"#,##0,;[Red](#,##0,)";
    }

    public string MessageBoxTitle { get; set; }

    public string DefaultFileName { get; set; }

    public Color HeaderBackgroundColor { get; set; }

    public FontWeight HeaderFontWeight { get; set; }

    public string FormatString { get; set; }

    public void Execute(object parameter_)
    {
      var dataPresenter = (DataPresenterBase) parameter_;
      try
      {
        var saveFileDialog = new SaveFileDialog
                               {
                                 FileName = String.Format("{0}-{1}", DefaultFileName, DateTime.Now.ToString("yyyyMMdd-HHmmss")),
                                 DefaultExt = "*.xslx",
                                 Filter = "Excel Workbook (*.xlsx)|*.xlsx|Excel 97-2003 Workbook (*.xls)|*.xls"
                               };
        var result = saveFileDialog.ShowDialog();
        if (result.GetValueOrDefault())
        {
          ExportToExcel(dataPresenter, saveFileDialog.FileName);
        }
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message + Environment.NewLine + ex.StackTrace, MessageBoxTitle, MessageBoxButton.OK, MessageBoxImage.None);
      }
    }

    public bool CanExecute(object parameter_)
    {
      return true;
    }

    public int NumberOfVisibleFields
    {
      get
      {
        if (_numberOfVisibleFields == INVALID_NUMBER_OF_VISIBLE_FIELDS) throw new InvalidOperationException("Number of visible fields not assigned");
        return _numberOfVisibleFields;
      }
      private set { _numberOfVisibleFields = value; }
    }

    public event EventHandler CanExecuteChanged;

    protected virtual void HandleHeaderAreaExported(object sender_, HeaderAreaExportedEventArgs args_)
    {      
    }

    protected virtual void HandleRecordExported(object sender_, RecordExportedEventArgs args_)
    {      
    }

    protected virtual void HandleCellExporting(object sender_, CellExportingEventArgs e_)
    {
      Type fieldType = e_.Field.DataType;
      if (fieldType == typeof(double) || fieldType == typeof(double?))
      {
        e_.FormatSettings.FormatString = FormatString;
      }

      if (e_.CurrentColumnIndex == 0)
      {
        e_.FormatSettings.Indent = e_.CurrentOutlineLevel*2;
      }
    }

    private void ExportToExcel(DataPresenterBase grid_, [NotNull] string fileName_)
    {
      if (fileName_ == null) throw new ArgumentNullException("fileName_");

      int totalRow = 0;
      var exporter = new DataPresenterExcelExporter();
      exporter.HeaderAreaExporting += (s, e) =>
                                        {
                                          var enumerableType = typeof(IEnumerable);
                                          NumberOfVisibleFields = e
                                            .FieldLayout
                                            .Fields
                                            .Where(f => f.VisibilityResolved == Visibility.Visible)
                                            .Where(f => f.DataType == typeof(string) || !enumerableType.IsAssignableFrom(f.DataType))
                                            .ToArray()
                                            .Count();

                                          FormatSettings settings = e.FormatSettings;
                                          settings.VerticalAlignment = VerticalCellAlignment.Center;
                                          settings.HorizontalAlignment = HorizontalCellAlignment.Center;
                                          settings.WrapText = ExcelDefaultableBoolean.True;
                                          settings.FontWeight = HeaderFontWeight;
                                          settings.FillPattern = FillPatternStyle.Solid;
                                          settings.FillPatternForegroundColor = HeaderBackgroundColor;
                                          settings.FillPatternBackgroundColor = HeaderBackgroundColor;
                                        };
      exporter.HeaderLabelExporting += (s, e) => e.Label = e.Label.ToString().Replace('\n', ' ');
      exporter.HeaderAreaExported += HandleHeaderAreaExported;
      exporter.RecordExporting += (s, e) =>
                                    {
                                      e.CurrentColumnIndex = 0;
                                    };
      exporter.RecordExported += HandleRecordExported;
      exporter.CellExporting += HandleCellExporting;
      exporter.ExportEnded += (s, e) => totalRow = e.CurrentRowIndex;

      var options = new ExportOptions
      {
        ChildRecordCollectionSpacing = ChildRecordCollectionSpacing.None,
      };

      WorkbookFormat format = fileName_.EndsWith("xlsx") ? WorkbookFormat.Excel2007 : WorkbookFormat.Excel97To2003;
      exporter.Export(grid_, fileName_, format, options);

      var nextStep = new ExportToExcelNextStep();
      nextStep.ShowNextSteps(fileName_, totalRow);
    }
  }
}
