﻿using System.Windows;

namespace MorganStanley.MSDesktop.Rambo.WpfUtils.Styles
{
  public sealed class ToolTipEditorStyles
  {

    static ToolTipEditorStyles()
    {
      _xamTextEditorToolTipStyle = new ComponentResourceKey(typeof (ToolTipEditorStyles), "XamTextEditorToolTipStyle");
    }

    private static readonly ComponentResourceKey _xamTextEditorToolTipStyle;
    public static ComponentResourceKey XamTextEditorToolTipStyleKey
    {
      get { return _xamTextEditorToolTipStyle; }
    }
  }
}