﻿using System.Windows;

namespace MorganStanley.MSDesktop.Rambo.WpfUtils.Styles
{
  public sealed class XamComboStyle
  {
    private static ComponentResourceKey _synchronizedKey;

    public static ComponentResourceKey SynchronizedKey
    {
      get { return _synchronizedKey ?? (_synchronizedKey = new ComponentResourceKey(typeof(XamComboStyle), "Synchronized")); }
    }
  }
}
