﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

[assembly: XmlnsPrefix("http://wpfutils.ecdev.ms.com/Helpers", "wpfUtilsHelpers")]
[assembly: XmlnsDefinition("http://wpfutils.ecdev.ms.com/Helpers", "MorganStanley.MSDesktop.Rambo.WpfUtils.Helpers")]
[assembly: XmlnsPrefix("http://wpfutils.ecdev.ms.com/Styles", "wpfUtilsStyles")]
[assembly: XmlnsDefinition("http://wpfutils.ecdev.ms.com/Styles", "MorganStanley.MSDesktop.Rambo.WpfUtils.Styles")]
[assembly: XmlnsPrefix("http://wpfutils.ecdev.ms.com/Commands", "wpfUtilsCommands")]
[assembly: XmlnsDefinition("http://wpfutils.ecdev.ms.com/Commands", "MorganStanley.MSDesktop.Rambo.WpfUtils.Commands")]

[assembly: System.Windows.ThemeInfo(System.Windows.ResourceDictionaryLocation.None, System.Windows.ResourceDictionaryLocation.SourceAssembly)]
namespace MorganStanley.MSDesktop.Rambo.WpfUtils.AttachedProperties
{
  public class AutoTooltipBehavior
  {
    /// <summary>
    /// Gets the value of the AutoTooltipProperty dependency property
    /// </summary>
    public static bool GetAutoTooltip(DependencyObject obj_)
    {
      return (bool)obj_.GetValue(AutoTooltipProperty);
    }

    /// <summary>
    /// Sets the value of the AutoTooltipProperty dependency property
    /// </summary>
    public static void SetAutoTooltip(DependencyObject obj_, bool value_)
    {
      obj_.SetValue(AutoTooltipProperty, value_);
    }

    /// <summary>
    /// Identified the attached AutoTooltip property. When true, this will set the TextBlock TextTrimming
    /// property to WordEllipsis, and display a tooltip with the full text whenever the text is trimmed.
    /// </summary>
    public static readonly DependencyProperty AutoTooltipProperty = DependencyProperty.RegisterAttached("AutoTooltip",
            typeof(bool), typeof(AutoTooltipBehavior), new PropertyMetadata(false, OnAutoTooltipPropertyChanged));

    private static void OnAutoTooltipPropertyChanged(DependencyObject dependencyObject_, DependencyPropertyChangedEventArgs e_)
    {
      var textBlock = dependencyObject_ as TextBlock;
      if (textBlock == null)
        return;

      if (e_.NewValue.Equals(true))
      {
        textBlock.TextTrimming = TextTrimming.CharacterEllipsis;
        ApplyTooltipForTrimmedTextBlocks(textBlock);
        textBlock.DataContextChanged += textBlock_DataContextChanged;
        textBlock.SizeChanged += textBlock_SizeChanged;
      }
      else
      {
        textBlock.DataContextChanged -= textBlock_DataContextChanged;
        textBlock.SizeChanged -= textBlock_SizeChanged;
      }
    }

    static void textBlock_DataContextChanged(object sender_, DependencyPropertyChangedEventArgs e_)
    {
      var textBlock = sender_ as TextBlock;
      ApplyTooltipForTrimmedTextBlocks(textBlock);
    }

    static void textBlock_SizeChanged(object sender_, SizeChangedEventArgs e_)
    {
      var textBlock = sender_ as TextBlock;
      ApplyTooltipForTrimmedTextBlocks(textBlock);
    }

    /// <summary>
    /// Assigns the ToolTip for the given TextBlock based on whether the text is trimmed
    /// </summary>
    private static void ApplyTooltipForTrimmedTextBlocks(TextBlock textBlock_)
    {
      textBlock_.Measure(new Size(Double.PositiveInfinity, Double.PositiveInfinity));
      double textBlockMarginWidth = textBlock_.Margin.Left + textBlock_.Margin.Right;
      double desiredTextBlockWidth = textBlock_.DesiredSize.Width - textBlockMarginWidth;

      ToolTipService.SetShowDuration(textBlock_, 100000);
      ToolTipService.SetToolTip(textBlock_, textBlock_.ActualWidth < desiredTextBlockWidth ? textBlock_.Text : null);
    }


  }
}
