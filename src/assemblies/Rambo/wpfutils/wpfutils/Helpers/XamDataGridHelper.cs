﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml;
using Infragistics.Windows.DataPresenter;
using Infragistics.Windows.Editors;
using MorganStanley.MSDesktop.Rambo.DataDictionary;
using MorganStanley.MSDesktop.Rambo.WpfUtils.Core.Helpers;

namespace MorganStanley.MSDesktop.Rambo.WpfUtils.Helpers
{
  /// <summary>
  /// This class provides helper functions to <see cref="XamDataGrid"/>.
  /// </summary>
  public static class XamDataGridHelper
  {
    /// <summary>
    /// Returns the record that was the target of the mouse click event in the grid.
    /// </summary>
    /// <param name="sender">XamDataGrid</param>
    /// <param name="e">MouseButtonEventArgs</param>
    /// <returns>The record that was clicked on or null.</returns>
    public static Record TryGetClickedRecord(object sender, MouseButtonEventArgs e)
    {
      var grid = sender as XamDataGrid;

      if (grid == null || e == null) return null;

      //get the cursor position and the item under the cursor
      var p = e.GetPosition(grid);
      var obj = grid.InputHitTest(p) as DependencyObject;

      //if there is nothing under the cursor, return
      if (obj == null) return null;

      //check if we're dealing with the presenter itself
      var presenter = obj as CellValuePresenter ?? FindParent<CellValuePresenter>(obj);

      //return the presenter's record, if possible
      return (presenter == null) ? null : presenter.Record;
    }

    /// <summary>
    /// Finds a parent of a given item on the visual tree.
    /// </summary>
    /// <typeparam name="T">The type of the queried item.</typeparam>
    /// <param name="child_">A direct or indirect child of the
    /// queried item.</param>
    /// <returns>The first parent item that matches the submitted
    /// type parameter.</returns>
    public static T FindParent<T>(DependencyObject child_) where T : DependencyObject
    {
      //get parent item
      DependencyObject parentObject = VisualTreeHelper.GetParent(child_);

      //we've reached the end of the tree
      if (parentObject == null) return null;

      //check if the parent matches the type we're looking for
      var parent = parentObject as T;
      return parent ?? FindParent<T>(parentObject);
    }

    /// <summary>
    /// Creates a field layout for <see cref="XamDataGrid"/>.
    /// </summary>
    /// <param name="layoutDef_">The list of fields in XML version</param>
    /// <param name="viewType_">The corresponding ViewType in DataDictionary</param>
    /// <param name="resources_">Current window resources</param>
    /// <returns></returns>
    public static FieldLayout CreateLayout(XmlNode layoutDef_, ViewTypeInfo viewType_, ResourceDictionary resources_)
    {
      var unorderedFields = new List<Field>();
      var orderedFields = new SortedDictionary<int, Field>();

      XmlAttribute fixingAttr;
      XmlNode node;

      foreach (var sf in viewType_.FieldIterator)
      {
        if (!sf.IsVisible) continue;

        node = layoutDef_.SelectSingleNode(String.Format(LayoutConstants.XPATH_ID, sf.Id));
        if (node == null) continue;

        var field = new Field {Name = sf.Id, Label = sf.ShortDescription, DataType = sf.DataTypeType};

        fixingAttr = node.ParentNode.Attributes[LayoutConstants.XPATH_ATTR_ALLOWFIXING];
        if (fixingAttr != null)
        {
          AllowFieldFixing allowFixing;
          if (EnumHelper.TryParse(fixingAttr.InnerText, out allowFixing))
          {
            field.Settings.AllowFixing = allowFixing;
          }
        }

        node = layoutDef_.SelectSingleNode(String.Format(LayoutConstants.XPATH_FIXEDLOCATION, sf.Id));
        if (node != null)
        {
          FixedFieldLocation fixedLocation;
          if (EnumHelper.TryParse(node.InnerText, out fixedLocation))
          {
            field.FixedLocation = fixedLocation;
          }
        } 

        node = layoutDef_.SelectSingleNode(String.Format(LayoutConstants.XPATH_WIDTH, sf.Id));
        int iwidth;
        if (node != null && int.TryParse(node.InnerText, out iwidth))
        {
          field.Settings.LabelWidth = iwidth;
          field.Settings.CellWidth = iwidth;
        }

        node = layoutDef_.SelectSingleNode(String.Format(LayoutConstants.XPATH_MINWIDTH, sf.Id));
        if (node != null && int.TryParse(node.InnerText, out iwidth))
        {
          field.Settings.LabelMinWidth = iwidth;
          field.Settings.CellMaxWidth = iwidth;
        }

        node = layoutDef_.SelectSingleNode(String.Format(LayoutConstants.XPATH_MAXWIDTH, sf.Id));
        if (node != null && int.TryParse(node.InnerText, out iwidth))
        {
          field.Settings.LabelMaxWidth = iwidth;
          field.Settings.CellMaxWidth = iwidth;
        }
        else
        {
          //HACK: otherwise cell value will be clipped
          field.Settings.LabelMaxWidth = double.MaxValue;
          field.Settings.CellMaxWidth = double.MaxValue;
        }

        node = layoutDef_.SelectSingleNode(String.Format(LayoutConstants.XPATH_LABEL, sf.Id));
        if (node != null)
        {
          var label = node.InnerText;
          if (label != null)
          {
            field.Label = label;
          }
        }

        node = layoutDef_.SelectSingleNode(String.Format(LayoutConstants.XPATH_ISPRIMARY, sf.Id));
        bool isPrimary;
        if (node != null && Boolean.TryParse(node.InnerText, out isPrimary) && isPrimary)
        {
          field.IsPrimary = true;
        }

        node = layoutDef_.SelectSingleNode(String.Format(LayoutConstants.XPATH_STYLE, sf.Id));
        if (node != null)
        {
          var style = resources_[node.InnerText] as Style;
          if (style != null)
          {
            field.Settings.CellValuePresenterStyle = style;
          }
        }

        node = layoutDef_.SelectSingleNode(String.Format(LayoutConstants.XPATH_LABELSTYLE, sf.Id));
        if (node != null)
        {
          var style = resources_[node.InnerText] as Style;
          if (style != null)
          {
            field.Settings.LabelPresenterStyle = new Style(typeof(LabelPresenter), style);
          }
        }
        if (field.Settings.LabelPresenterStyle == null)
        {
          field.Settings.LabelPresenterStyle = new Style(typeof(LabelPresenter));
        }
        field.Settings.LabelPresenterStyle.Setters.Add(new Setter(FrameworkElement.ToolTipProperty, sf.LongDescription));

        //set format for editors
        node = layoutDef_.SelectSingleNode(String.Format(LayoutConstants.XPATH_FORMAT, sf.Id));
        if (node != null)
        {
          var formatSetter = new Setter(ValueEditor.FormatProperty, node.InnerText);
          if (field.Settings.EditorStyle == null)
          {
            field.Settings.EditorStyle = new Style(typeof(ValueEditor));
          }
          field.Settings.EditorStyle.Setters.Add(formatSetter);
        }
        //set converter
        node = layoutDef_.SelectSingleNode(String.Format(LayoutConstants.XPATH_CONVERTER, sf.Id));
        if (node != null)
        {
          var converter = resources_[node.InnerText] as IValueConverter;
          if (converter != null)
          {
            field.Settings.AllowEdit = false;

            field.Converter = converter;
          }
        }
        //set group-by style
        node = layoutDef_.SelectSingleNode(String.Format(LayoutConstants.XPATH_GROUPBYSTYLE, sf.Id));
        if (node != null)
        {
          var style = resources_[node.InnerText] as Style;
          if (style != null)
          {
            field.Settings.GroupByRecordPresenterStyle = style;
          }
        }
        //set comparer
        node = layoutDef_.SelectSingleNode(String.Format(LayoutConstants.XPATH_COMPARER, sf.Id));
        if (node != null)
        {
          var comparer = resources_[node.InnerText] as IComparer;
          if (comparer != null)
          {
            field.Settings.SortComparer = comparer;
          }
        }
        //set visibility
        node = layoutDef_.SelectSingleNode(String.Format(LayoutConstants.XPATH_VISIBILITY, sf.Id));
        if (node != null)
        {
          Visibility isVisible;
          if (EnumHelper.TryParse(node.InnerText, out isVisible))
          {
            field.Visibility = isVisible;
          }
        }
        //process column order
        node = layoutDef_.SelectSingleNode(String.Format(LayoutConstants.XPATH_COLUMNORDER, sf.Id));
        int columnOrder;
        if (node != null && int.TryParse(node.InnerText, out columnOrder))
        {
          orderedFields.Add(columnOrder, field);
        }
        else
        {
          unorderedFields.Add(field);
        }
      }

      var fieldLayout = new FieldLayout();
      var cnt = orderedFields.Count + unorderedFields.Count;
      var enumerator = unorderedFields.GetEnumerator();
      enumerator.MoveNext();
      for (var i = 0; i < cnt; i++)
      {
        if (orderedFields.ContainsKey(i))
        {
          fieldLayout.Fields.Add(orderedFields[i]);
        }
        else
        {
          fieldLayout.Fields.Add(enumerator.Current);
          enumerator.MoveNext();
        }
      }

      node = layoutDef_.SelectSingleNode(LayoutConstants.XPATH_ROOTNODE);
      //set AllowFixing for the layout
      fixingAttr = node.Attributes[LayoutConstants.XPATH_ATTR_ALLOWFIXING];
      if (fixingAttr != null)
      {
        AllowFieldFixing allowFixing;
        EnumHelper.TryParse(fixingAttr.InnerText, out allowFixing);
        fieldLayout.FieldSettings.AllowFixing = allowFixing;
      }

      //set FixedFieldUIType for the layout
      fixingAttr = node.Attributes[LayoutConstants.XPATH_ATTR_FIXEDFIELD_UITYPE];
      if (fixingAttr != null)
      {
        FixedFieldUIType ffUiType;
        EnumHelper.TryParse(fixingAttr.InnerText, out ffUiType);
        fieldLayout.Settings.FixedFieldUIType = ffUiType;
      }

      return fieldLayout;
    }

    /// <summary>
    /// Creates a new field layout for <see cref="XamDataGrid"/> and apply sorting on it
    /// </summary>
    /// <param name="layoutDef_">The list of fields in XML version</param>
    /// <param name="viewType_">The corresponding ViewType in DataDictionary</param>
    /// <param name="resources_">Current window resources</param>
    /// <returns></returns>
    /// <remarks>It sorts fields not according to the field's comparer, but the built-in text comparer</remarks>
    public static FieldLayout CreateSortedLayout(XmlNode layoutDef_, ViewTypeInfo viewType_, ResourceDictionary resources_)
    {
      var fieldLayout = CreateLayout(layoutDef_, viewType_, resources_);
      if (fieldLayout != null)
      {
        var sortedFieldNode = layoutDef_.SelectSingleNode(LayoutConstants.XPATH_SORTEDFIELDS);
        if (sortedFieldNode != null && sortedFieldNode.HasChildNodes)
        {
          foreach (XmlNode node in sortedFieldNode)
          {
            var fieldName = node.Attributes[LayoutConstants.XPATH_ATTR_FIELDNAME].InnerText;
            var direction = (ListSortDirection) Enum.Parse(typeof (ListSortDirection),
                                                           node.Attributes[LayoutConstants.XPATH_ATTR_DIRECTION].InnerText);
            var fsd = new FieldSortDescription(fieldName, direction, false);
            fieldLayout.SortedFields.Add(fsd);
          }
        }
      }

      return fieldLayout;
    }
  }
}