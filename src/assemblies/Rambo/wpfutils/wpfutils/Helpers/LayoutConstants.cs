﻿namespace MorganStanley.MSDesktop.Rambo.WpfUtils.Helpers
{
  /// <summary>
  /// Placeholder of constants that are used to create field layout for XamDataGrid 
  /// </summary>
  internal static class LayoutConstants
  {
    public const string XPATH_ATTR_DIRECTION = "Direction";
    public const string XPATH_ATTR_FIELDNAME = "FieldName";
    public const string XPATH_ATTR_ALLOWFIXING = "AllowFixing";
    public const string XPATH_ATTR_FIXEDFIELD_UITYPE = "FixedFieldUIType";

    public const string XPATH_ROOTNODE = "/FieldLayout";
    public const string XPATH_COLUMNORDER = "/FieldLayout/Fields/Field[Id='{0}']/ColumnOrder";
    public const string XPATH_CONVERTER = "/FieldLayout/Fields/Field[Id='{0}']/Converter";
    public const string XPATH_GROUPBYSTYLE = "/FieldLayout/Fields/Field[Id='{0}']/GroupbyStyle";
    public const string XPATH_ISPRIMARY = "/FieldLayout/Fields/Field[Id='{0}']/IsPrimary";
    public const string XPATH_SORT_FIELDS = "/FieldLayout/SortedFields/FieldSortDescription";
    public const string XPATH_STYLE = "/FieldLayout/Fields/Field[Id='{0}']/Style";
    public const string XPATH_WIDTH = "/FieldLayout/Fields/Field[Id='{0}']/Width";
    public const string XPATH_LABEL = "/FieldLayout/Fields/Field[Id='{0}']/Label";
    public const string XPATH_ID = "/FieldLayout/Fields/Field[Id='{0}']";
    public const string XPATH_MINWIDTH = "/FieldLayout/Fields/Field[Id='{0}']/MinWidth";
    public const string XPATH_MAXWIDTH = "/FieldLayout/Fields/Field[Id='{0}']/MaxWidth";
    public const string XPATH_TEXTALIGN = "/FieldLayout/Fields/Field[Id='{0}']/TextAlign";
    public const string XPATH_LABELSTYLE = "/FieldLayout/Fields/Field[Id='{0}']/LabelStyle";
    public const string XPATH_COMPARER = "/FieldLayout/Fields/Field[Id='{0}']/Comparer";
    public const string XPATH_FORMAT = "/FieldLayout/Fields/Field[Id='{0}']/Format";
    public const string XPATH_VISIBILITY = "/FieldLayout/Fields/Field[Id='{0}']/Visibility";
    public const string XPATH_FIXEDLOCATION = "/FieldLayout/Fields/Field[Id='{0}']/FixedLocation";
    public const string XPATH_SORTEDFIELDS = "/FieldLayout/SortedFields";
  }
}