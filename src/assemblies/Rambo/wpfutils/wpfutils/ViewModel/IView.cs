using System;

namespace MorganStanley.Ecdev.WpfUtils.ViewModel
{
  /// <summary>
  /// Contains view specific information.
  /// </summary>
  [Obsolete("Use classes in core library")]
  public interface IView
  {
    /// <summary>
    /// Sets the Model of the view.
    /// </summary>
    object Model { get; set; }
  }
}