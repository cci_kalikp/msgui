﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Mail;
using System.Windows.Input;
using JetBrains.Annotations;
using MorganStanley.Ecdev.Desktop.Utils;
using MorganStanley.Ecdev.WpfUtils.Commands;

namespace MorganStanley.Ecdev.WpfUtils.ViewModel
{
  [Obsolete("Use classes in core library")]
  public class ErrorReportWindowViewModel
  {
    private static ILogger _log = new Logger(typeof(ErrorReportWindowViewModel));

    private const string SMTP_DEFAULT = "mta-hub.ms.com";
    private const string REPORT_SUBJECT_DEFAULT = "Application Error";
    private const string APPSETTING_SMTPHOST = "SMTPHost";
    private const string APPSETTING_ERRORREPORTADDRESS = "ErrorReportAddress";
    private const string APPSETTING_ERRORREPORTSUBJECT = "ErrorReportSubject";

    private bool _errorReportSent;
    private IErrorReportWindowView View { get; set;}

    public string ErrorDetails { get; private set; }
    public string ErrorSummary { get; private set; }
    [UsedImplicitly]
    public bool ShowDetails { get; set; }
    public ICommand SendReport { get; private set; }

    public ErrorReportWindowViewModel([NotNull]IErrorReportWindowView view_)
    {
      if (view_ == null)
        throw new ArgumentNullException("view_");

      SendReport = new RelayCommand(ExecuteSendReport, o_ => CanSendReport);

      View = view_;
      View.Model = this;
    }

    public void ShowErrorReport(string errorSummary_)
    {
      _log.Debug("ShowErrorReport", 
                 String.Format("Error report form is initialized with summary: {0}", errorSummary_));
      ErrorSummary = errorSummary_;

      View.ShowModal();
    }

    public void ShowErrorReport(string errorSummary_, string errorDetails_)
    {
      _log.Debug("ShowModal",
           String.Format("Error report form is initialized with the followings:\nSUMMARY:{0}\nDETAILS:{1}", errorSummary_, errorDetails_));
      ErrorSummary = errorSummary_;
      ErrorDetails = errorDetails_;

      View.ShowModal();
    }

    public void ShowErrorReport(Exception e_)
    {
      ShowErrorReport(e_.Message, e_.ToString());
    }

    public static string SmtpHost
    {
      get
      {
        string smtpHost = ConfigurationManager.AppSettings[APPSETTING_SMTPHOST];
        if (String.IsNullOrEmpty(smtpHost))
          return SMTP_DEFAULT;

        return smtpHost;
      }
    }

    public static string ReportSubject
    {
      get
      {
        string subject = ConfigurationManager.AppSettings[APPSETTING_ERRORREPORTSUBJECT];
        if (String.IsNullOrEmpty(subject))
          return REPORT_SUBJECT_DEFAULT;

        return subject;
      }
    }

    public static string ReportAddress
    {
      get
      {
        string to = ConfigurationManager.AppSettings[APPSETTING_ERRORREPORTADDRESS];
        if (!String.IsNullOrEmpty(to))
          _log.Notice("ReportAddress.get", "No addressee was found");

        return to;
      }
    }

    private bool CanSendReport
    {
      get
      {
        return !(_errorReportSent || String.IsNullOrEmpty(ReportAddress));
      }
    }

    private void ExecuteSendReport(object obj_)
    {
      if (!CanSendReport) return;

      string from = String.Format("{0}@ms.com", Environment.UserName);
      try
      {
        var msg = new MailMessage
                {
                  From = new MailAddress(from),
                  Subject = ReportSubject,
                  Body = ErrorSummary + "\n" + ErrorDetails
                };
        var smtpClient = new SmtpClient(SmtpHost)
                           {
                             Credentials = CredentialCache.DefaultNetworkCredentials
                           };
        msg.To.Add(ReportAddress);

        smtpClient.Send(msg);
        _errorReportSent = true;
      }
      catch (Exception e)
      {
        _log.Error("ExecuteSendReport", "Cannot send email with error report.", e);
      }
    }
  }
}