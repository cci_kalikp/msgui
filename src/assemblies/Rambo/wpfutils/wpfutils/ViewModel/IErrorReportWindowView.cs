using System;

namespace MorganStanley.Ecdev.WpfUtils.ViewModel
{
  [Obsolete("Use classes in core library")]
  public interface IErrorReportWindowView
  {
    /// <summary>
    /// Sets the Model of the view.
    /// </summary>
    object Model { set; }
    /// <summary>
    /// Opens the window and return
    /// </summary>
    void Show();
    /// <summary>
    /// Opens the window and returns only when the newly created window is closed.
    /// </summary>
    /// <returns>True if DialogResult was set to true, otherwise false.</returns>
    bool ShowModal();
  }
}