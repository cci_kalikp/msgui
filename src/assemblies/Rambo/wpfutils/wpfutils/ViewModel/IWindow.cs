using System;

namespace MorganStanley.Ecdev.WpfUtils.ViewModel
{
  /// <summary>
  /// Interface for Window
  /// </summary>
  [Obsolete("Use classes in core library")]
  public interface IWindow : IView
  {
    /// <summary>
    /// Opens the window and return
    /// </summary>
    void Show();
    /// <summary>
    /// Opens the window and returns only when the newly created window is closed.
    /// </summary>
    /// <returns>True if DialogResult was set to true, otherwise false.</returns>
    bool ShowModal();
    /// <summary>
    /// Close the window
    /// </summary>
    void CloseView();
  }
}