﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using JetBrains.Annotations;

namespace MorganStanley.Ecdev.WpfUtils.Extensions
{
  [Obsolete("Use classes in core library")]
  public static class ICollectionViewHelper
  {
    /// <summary>
    /// Moves the current item of the view to the first element starting from the beginning where the predicate is true for the element.
    /// If there is no element satisfying the predicate, the current item pointer is moved back to the original value. 
    /// During this operation, the CurrentItem is moved so expect event handlers getting called.
    /// </summary>
    /// <typeparam name="T">The type of the elements in the source collection</typeparam>
    /// <param name="view_">The view on top of the source collection</param>
    /// <param name="pred_"></param>
    [Obsolete("Use classes in core library")]
    public static void MoveCurrentTo<T>([NotNull] this ICollectionView view_, [NotNull] Func<T, bool> pred_)
    {
      if (view_ == null) throw new ArgumentNullException("view_");
      if (pred_ == null) throw new ArgumentNullException("pred_");

      object oldCurrent = view_.CurrentItem;

      ICollection<T> collection = (ICollection<T>) view_.SourceCollection;
      T newItem = collection.FirstOrDefault(pred_);
      bool success = view_.MoveCurrentTo(newItem);
      if (!success)
      {
        view_.MoveCurrentTo(oldCurrent);
      }
    }
  }
}
