﻿using System;
using System.Collections.Generic;
using Infragistics.Windows.DataPresenter;
using JetBrains.Annotations;

namespace MorganStanley.MSDesktop.Rambo.WpfUtils.Extensions
{
  public static class XamDataGridHelper
  {
    /// <summary>
    /// Gets the items selected on the grid as the type of each data item
    /// </summary>
    /// <typeparam name="T">The type in the collection that is bound to the grid</typeparam>
    /// <param name="grid_"></param>
    /// <returns></returns>
    [NotNull]
    public static List<T> GetSelectedItems<T>(this XamDataGrid grid_)
    {
      var result = new List<T>();
      foreach (Record record in grid_.SelectedItems.Records)
      {
        GetSelectedItems(record, result);
      }

      return result;
    }

    private static void GetSelectedItems<T>([NotNull] Record record_, [NotNull] ICollection<T> accu_)
    {
      if (record_ == null) throw new ArgumentNullException("record_");
      if (accu_ == null) throw new ArgumentNullException("accu_");

      if (record_.IsSpecialRecord)
      {
        return;
      }

      var dataRecord = record_ as DataRecord;
      if (dataRecord != null)
      {
        var dataItem = (T)dataRecord.DataItem;
        accu_.Add(dataItem);
        return;
      }

      var groupByRecord = record_ as GroupByRecord;
      if (groupByRecord == null)
      {
        return;
      }

      foreach (Record record in groupByRecord.ChildRecords)
      {
        GetSelectedItems(record, accu_);
      }
    }
  }
}
