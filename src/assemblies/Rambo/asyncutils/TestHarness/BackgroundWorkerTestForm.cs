using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;

using MorganStanley.MSDotNet.MSNet;

namespace MorganStanley.MSDesktop.Rambo.AsyncUtils.TestHarness
{
  public partial class BackgroundWorkerTestForm : Form
  {
    public BackgroundWorkerTestForm()
    {
      InitializeComponent();
    }

    private void BackgroundWorkerTestForm_Load(object sender, EventArgs e)
    {
      iBackgroundWorkerComponentBindingSource.DataSource = asynchronizerBackgroundWorker1;
    }

    private void numArgument_ValueChanged(object sender, EventArgs e)
    {
      progress.Argument = (int)numArgument.Value;
    }

    private void asynchronizerBackgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
    {
      ThreadingUtils.ThreadSafeInvoke(new MSNetVoidVoidDelegate(DisableParamsGroup));
      DoWorkAgorithm.DoWork(sender as IBackgroundWorker, e);
    }

    private void DisableParamsGroup( )
    {
      gbBackgroundWorker.Enabled = false;
    }

    private void asynchronizerBackgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
    {
      gbBackgroundWorker.Enabled = true;
    }
  }
}