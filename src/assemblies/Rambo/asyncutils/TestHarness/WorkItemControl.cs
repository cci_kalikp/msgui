using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace MorganStanley.MSDesktop.Rambo.AsyncUtils.TestHarness
{
  public partial class WorkItemControl : UserControl
  {
    public WorkItemControl()
    {
      InitializeComponent();
    }

    public string ItemName
    {
      get
      {
        return lblName.Text;
      }

      set
      {
        lblName.Text = value;
      }
    }

    public int SleepTime
    {
      get
      {
        return (int)ultraNumericEditor1.Value;
      }
      set
      {
        if(value < (int)ultraNumericEditor1.MinValue)
        {
          ultraNumericEditor1.Value = ultraNumericEditor1.MinValue;
        }
        else if( value > (int)ultraNumericEditor1.MaxValue)
        {
          ultraNumericEditor1.Value = ultraNumericEditor1.MaxValue;
        }
        else
        {
          ultraNumericEditor1.Value = value;
        }
      }
    }
  }
}
