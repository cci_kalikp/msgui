﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace MorganStanley.MSDesktop.Rambo.AsyncUtils.TestHarness
{
  public class StaticAsyncTestSimpleClass
  {
    private delegate string WorkItemDelegate(string name, int sleepTime, bool showMessageBox);
    private TextBox _textbox;
    public static void StartTestOnSimpleClass(TextBox testbox)
    {
      var testerClass = new StaticAsyncTestSimpleClass();
      testerClass._textbox = testbox;
      string threadName = Thread.CurrentThread.Name;
      testerClass.StartTest(false);
      testerClass.StartTest(true);
    }

    private void StartTest(bool throwException)
    {
      string threadName = Thread.CurrentThread.Name;
      _textbox.Text += string.Format("Starting Non-Form Two-way BeginOperation on Thread '{0}'", threadName);
      int seconds = 1;
      if (throwException)
      {
        _textbox.Text += "and throw exception";
        seconds = 2;
      }
      _textbox.Text += Environment.NewLine;
      _textbox.Text += Environment.NewLine;
      Asynchronizer.BeginOperation(CompleteBackgroundWork, "This is the state",
                                   new WorkItemDelegate(PerformBackgroundWork), "SimpleClass", seconds, true);
    }

    private string PerformBackgroundWork(string name, int sleepTime, bool showMessageBox)
    {
      Thread.Sleep(sleepTime * 1000);
      string threadName = Thread.CurrentThread.Name;
      if ((sleepTime % 2) == 0)
      {
        Exception ex = new InvalidOperationException("Test Exception for sleep time '" + sleepTime + "'");
        if (showMessageBox)
        {
          MessageBox.Show(string.Format("Throwing Exception '{0}'", ex.Message));
        }
        throw ex;
      }
      if (showMessageBox)
      {
        MessageBox.Show(string.Format("Process name is '{0}', ThreadName: '{1}', SleepTime: {2}'", name, threadName,
                                      sleepTime));
      }
      return string.Format("Process name is '{0}', ThreadName: '{1}', SleepTime: {2}'", name, threadName,
                           sleepTime);
    }

    private void CompleteBackgroundWork(IAsyncResult ar)
    {
      string threadName = Thread.CurrentThread.Name;
      string result;
      if (Asynchronizer.TryEndOperation(ar, out result))
      {
        _textbox.Text += string.Format("Completed (SimpleClass) on Thread '{1}' with Result '{0}'", result, threadName);
        _textbox.Text += Environment.NewLine;
        _textbox.Text += Environment.NewLine;
      }
      else
      {
        _textbox.Text += string.Format("Thrown exception! Result = '{0}'", Asynchronizer.GetException(ar).Message);
        _textbox.Text += Environment.NewLine;
        _textbox.Text += Environment.NewLine;        
      }
    }
  }
}
