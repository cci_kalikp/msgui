namespace MorganStanley.MSDesktop.Rambo.AsyncUtils.TestHarness
{
  partial class WorkItemControl
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
      this.lblName = new Infragistics.Win.Misc.UltraLabel();
      this.ultraLabel3 = new Infragistics.Win.Misc.UltraLabel();
      this.ultraNumericEditor1 = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
      ((System.ComponentModel.ISupportInitialize)(this.ultraNumericEditor1)).BeginInit();
      this.SuspendLayout();
      // 
      // ultraLabel1
      // 
      this.ultraLabel1.Location = new System.Drawing.Point(3, 7);
      this.ultraLabel1.Name = "ultraLabel1";
      this.ultraLabel1.Size = new System.Drawing.Size(42, 23);
      this.ultraLabel1.TabIndex = 0;
      this.ultraLabel1.Text = "Name:";
      // 
      // lblName
      // 
      this.lblName.Location = new System.Drawing.Point(51, 7);
      this.lblName.Name = "lblName";
      this.lblName.Size = new System.Drawing.Size(66, 19);
      this.lblName.TabIndex = 0;
      // 
      // ultraLabel3
      // 
      this.ultraLabel3.Location = new System.Drawing.Point(123, 7);
      this.ultraLabel3.Name = "ultraLabel3";
      this.ultraLabel3.Size = new System.Drawing.Size(66, 19);
      this.ultraLabel3.TabIndex = 0;
      this.ultraLabel3.Text = "Sleep (sec):";
      // 
      // ultraNumericEditor1
      // 
      this.ultraNumericEditor1.Location = new System.Drawing.Point(195, 3);
      this.ultraNumericEditor1.MaskClipMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.Raw;
      this.ultraNumericEditor1.MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.Raw;
      this.ultraNumericEditor1.MaxValue = 10;
      this.ultraNumericEditor1.MinValue = 1;
      this.ultraNumericEditor1.Name = "ultraNumericEditor1";
      this.ultraNumericEditor1.PromptChar = ' ';
      this.ultraNumericEditor1.Size = new System.Drawing.Size(55, 21);
      this.ultraNumericEditor1.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Always;
      this.ultraNumericEditor1.SpinWrap = true;
      this.ultraNumericEditor1.TabIndex = 1;
      this.ultraNumericEditor1.Value = 1;
      // 
      // WorkItemControl
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.Controls.Add(this.ultraNumericEditor1);
      this.Controls.Add(this.lblName);
      this.Controls.Add(this.ultraLabel3);
      this.Controls.Add(this.ultraLabel1);
      this.MaximumSize = new System.Drawing.Size(1200, 30);
      this.MinimumSize = new System.Drawing.Size(307, 30);
      this.Name = "WorkItemControl";
      this.Size = new System.Drawing.Size(305, 26);
      ((System.ComponentModel.ISupportInitialize)(this.ultraNumericEditor1)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private Infragistics.Win.Misc.UltraLabel ultraLabel1;
    private Infragistics.Win.Misc.UltraLabel lblName;
    private Infragistics.Win.Misc.UltraLabel ultraLabel3;
    private Infragistics.Win.UltraWinEditors.UltraNumericEditor ultraNumericEditor1;
  }
}
