namespace MorganStanley.MSDesktop.Rambo.AsyncUtils.TestHarness
{
  partial class AsyncTestForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.btnTestBackgroundWorkerFactory = new Infragistics.Win.Misc.UltraButton();
      this.btnTestBackGroundWorker = new Infragistics.Win.Misc.UltraButton();
      this.btnMulitplex = new Infragistics.Win.Misc.UltraButton();
      this.ultraButton2 = new Infragistics.Win.Misc.UltraButton();
      this._btnStaticTester = new Infragistics.Win.Misc.UltraButton();
      this.ultraButton1 = new Infragistics.Win.Misc.UltraButton();
      this.ultraButton3 = new Infragistics.Win.Misc.UltraButton();
      this.SuspendLayout();
      // 
      // btnTestBackgroundWorkerFactory
      // 
      this.btnTestBackgroundWorkerFactory.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button;
      this.btnTestBackgroundWorkerFactory.Dock = System.Windows.Forms.DockStyle.Top;
      this.btnTestBackgroundWorkerFactory.Location = new System.Drawing.Point(0, 0);
      this.btnTestBackgroundWorkerFactory.Margin = new System.Windows.Forms.Padding(10);
      this.btnTestBackgroundWorkerFactory.Name = "btnTestBackgroundWorkerFactory";
      this.btnTestBackgroundWorkerFactory.Size = new System.Drawing.Size(240, 35);
      this.btnTestBackgroundWorkerFactory.TabIndex = 0;
      this.btnTestBackgroundWorkerFactory.Text = "Test BackgroundWorker Factory";
      this.btnTestBackgroundWorkerFactory.Click += new System.EventHandler(this.btnTestBackgroundWorkerFactory_Click);
      // 
      // btnTestBackGroundWorker
      // 
      this.btnTestBackGroundWorker.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button;
      this.btnTestBackGroundWorker.Dock = System.Windows.Forms.DockStyle.Top;
      this.btnTestBackGroundWorker.Location = new System.Drawing.Point(0, 35);
      this.btnTestBackGroundWorker.Margin = new System.Windows.Forms.Padding(10);
      this.btnTestBackGroundWorker.Name = "btnTestBackGroundWorker";
      this.btnTestBackGroundWorker.Size = new System.Drawing.Size(240, 35);
      this.btnTestBackGroundWorker.TabIndex = 0;
      this.btnTestBackGroundWorker.Text = "Test BackgroundWorker";
      this.btnTestBackGroundWorker.Click += new System.EventHandler(this.btnTestBackGroundWorker_Click);
      // 
      // btnMulitplex
      // 
      this.btnMulitplex.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button;
      this.btnMulitplex.Dock = System.Windows.Forms.DockStyle.Top;
      this.btnMulitplex.Location = new System.Drawing.Point(0, 70);
      this.btnMulitplex.Margin = new System.Windows.Forms.Padding(10);
      this.btnMulitplex.Name = "btnMulitplex";
      this.btnMulitplex.Size = new System.Drawing.Size(240, 35);
      this.btnMulitplex.TabIndex = 0;
      this.btnMulitplex.Text = "Test Multiplexer";
      this.btnMulitplex.Click += new System.EventHandler(this.btnMulitplex_Click);
      // 
      // ultraButton2
      // 
      this.ultraButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.Button;
      this.ultraButton2.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.ultraButton2.Location = new System.Drawing.Point(0, 211);
      this.ultraButton2.Margin = new System.Windows.Forms.Padding(10);
      this.ultraButton2.Name = "ultraButton2";
      this.ultraButton2.ShowFocusRect = false;
      this.ultraButton2.Size = new System.Drawing.Size(240, 27);
      this.ultraButton2.TabIndex = 0;
      this.ultraButton2.Text = "Close";
      this.ultraButton2.Click += new System.EventHandler(this.ultraButton2_Click);
      // 
      // _btnStaticTester
      // 
      this._btnStaticTester.Dock = System.Windows.Forms.DockStyle.Top;
      this._btnStaticTester.Location = new System.Drawing.Point(0, 105);
      this._btnStaticTester.Name = "_btnStaticTester";
      this._btnStaticTester.Size = new System.Drawing.Size(240, 35);
      this._btnStaticTester.TabIndex = 1;
      this._btnStaticTester.Text = "Test Static Asynchronizer";
      this._btnStaticTester.Click += new System.EventHandler(this._btnStaticTester_Click);
      // 
      // ultraButton1
      // 
      this.ultraButton1.Dock = System.Windows.Forms.DockStyle.Top;
      this.ultraButton1.Location = new System.Drawing.Point(0, 140);
      this.ultraButton1.Name = "ultraButton1";
      this.ultraButton1.Size = new System.Drawing.Size(240, 35);
      this.ultraButton1.TabIndex = 2;
      this.ultraButton1.Text = "Call Worker From UI Thread";
      this.ultraButton1.Click += new System.EventHandler(this.ultraButton1_Click);
      // 
      // ultraButton3
      // 
      this.ultraButton3.Dock = System.Windows.Forms.DockStyle.Top;
      this.ultraButton3.Location = new System.Drawing.Point(0, 175);
      this.ultraButton3.Name = "ultraButton3";
      this.ultraButton3.Size = new System.Drawing.Size(240, 35);
      this.ultraButton3.TabIndex = 3;
      this.ultraButton3.Text = "Call Worker from Thread Pool Thread";
      this.ultraButton3.Click += new System.EventHandler(this.ultraButton3_Click);
      // 
      // AsyncTestForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(240, 238);
      this.Controls.Add(this.ultraButton3);
      this.Controls.Add(this.ultraButton1);
      this.Controls.Add(this._btnStaticTester);
      this.Controls.Add(this.ultraButton2);
      this.Controls.Add(this.btnMulitplex);
      this.Controls.Add(this.btnTestBackGroundWorker);
      this.Controls.Add(this.btnTestBackgroundWorkerFactory);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
      this.MaximizeBox = false;
      this.MaximumSize = new System.Drawing.Size(250, 274);
      this.MinimumSize = new System.Drawing.Size(250, 254);
      this.Name = "AsyncTestForm";
      this.ShowIcon = false;
      this.Text = "AsyncTestForm";
      this.ResumeLayout(false);

    }

    #endregion

    private Infragistics.Win.Misc.UltraButton btnTestBackgroundWorkerFactory;
    private Infragistics.Win.Misc.UltraButton btnTestBackGroundWorker;
    private Infragistics.Win.Misc.UltraButton btnMulitplex;
    private Infragistics.Win.Misc.UltraButton ultraButton2;
    private Infragistics.Win.Misc.UltraButton _btnStaticTester;
    private Infragistics.Win.Misc.UltraButton ultraButton1;
    private Infragistics.Win.Misc.UltraButton ultraButton3;
  }
}