using System;
using MorganStanley.MSDotNet.Runtime;
using MorganStanley.MSDotNet.MSNet;
using System.Threading;

namespace MorganStanley.MSDesktop.Rambo.AsyncUtils.TestHarness
{
  static class Program
  {
    private static IMSNetLoop _UILoop;
    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    [STAThread]
    static void Main(string[] args)
    {
      AssemblyResolver.RunMain(args);
    }

    [MSDEMain]
    static void MSDEMain(string[] args)
    {
      _UILoop = new MSNetLoopUIImpl();
      Thread.CurrentThread.Name = "GUI Thread";
      ((MSNetLoopUIImpl)_UILoop).Loop(new AsyncTestForm( ));
    }

    static public IMSNetLoop UILoop
    {
      get { return _UILoop; }
    }
  }
}