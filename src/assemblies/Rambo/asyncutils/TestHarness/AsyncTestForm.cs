using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MorganStanley.MSDesktop.Rambo.AsyncUtils.TestHarness
{
  public partial class AsyncTestForm : Form
  {
    public AsyncTestForm()
    {
      InitializeComponent();
    }

    private void btnTestBackgroundWorkerFactory_Click(object sender, EventArgs e)
    {
      using (Form frm = new BackgroundWorkerFactoryTestForm())
      {
        frm.ShowDialog();
      }
    }

    private void btnTestBackGroundWorker_Click(object sender, EventArgs e)
    {
      using (Form frm = new BackgroundWorkerTestForm())
      {
        frm.ShowDialog();
      }
    }

    private void btnMulitplex_Click(object sender, EventArgs e)
    {
      using (Form frm = new MultiplexTestForm())
      {
        frm.ShowDialog();
      }
    }

    private void ultraButton2_Click(object sender, EventArgs e)
    {
      Close();
    }

    private void _btnStaticTester_Click(object sender, EventArgs e)
    {
      using (var frm = new StaticAsyncTestForm())
      {
        frm.ShowDialog();
      }
    }

    private void ultraButton1_Click(object sender, EventArgs e)
    {
      TestAsyncCaller.StartTestAsyncCaller();
    }

    private void ultraButton3_Click(object sender, EventArgs e)
    {
      var del = new MethodInvoker(TestAsyncCaller.StartTestAsyncCaller);
      del.BeginInvoke(del.EndInvoke, null);
    }
  }
}