﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace MorganStanley.MSDesktop.Rambo.AsyncUtils.TestHarness
{
  public partial class StaticAsyncTestForm : Form
  {
    private delegate string WorkItemDelegate(string name, int sleepTime, bool showMessageBox);

    public StaticAsyncTestForm()
    {
      InitializeComponent();
    }

    private void _btnSimple_Click(object sender, EventArgs e)
    {
      textBox1.Clear();
      textBox1.Text = string.Format("Starting One-way BeginOperation with text value '{0}' sleeping for '{1}' second(s)\r\n", _txtName.Text, _secs.Value);
      Asynchronizer.BeginOperation(new WorkItemDelegate(PerformBackgroundWork), _txtName.Text, _secs.Value, true);
    }

    private string PerformBackgroundWork(string name, int sleepTime, bool showMessageBox)
    {
      Thread.Sleep(sleepTime * 1000);
      string threadName = Thread.CurrentThread.Name;
      if ((sleepTime % 2) == 0)
      {
        Exception ex = new InvalidOperationException("Test Exception");
        if (showMessageBox)
        {
          MessageBox.Show("Throwing Exception '" + ex.Message + "' for even sleep number");
        }
        throw new InvalidOperationException("Test Exception");
      }
      if (showMessageBox)
      {
        MessageBox.Show(string.Format("Process name is '{0}', ThreadName: '{1}', SleepTime: {2}'", name, threadName,
                                      sleepTime));
      }
      return string.Format("Process name is '{0}', ThreadName: '{1}', SleepTime: {2}'", name, threadName,
                           sleepTime);
    }

    private void _btnTwoWay_Click(object sender, EventArgs e)
    {
      string threadName = Thread.CurrentThread.Name;
      textBox1.Clear();
      textBox1.Text = string.Format("Starting Two-way BeginOperation on Thread '{0}' with text value '{1}' sleeping for '{2}' second(s)\r\n", threadName, _txtName.Text, _secs.Value);
      Asynchronizer.BeginOperation(CompleteBackgroundWork, "This is the state", new WorkItemDelegate(PerformBackgroundWork), _txtName.Text, _secs.Value, true);
    }

    private void CompleteBackgroundWork(IAsyncResult ar)
    {
      string threadName = Thread.CurrentThread.Name;
      string result = Asynchronizer.EndOperation(ar) as string;
      textBox1.Text += string.Format("Completed on Thread '{1}' with Result '{0}'\r\n", result, threadName);

      StaticAsyncTestSimpleClass.StartTestOnSimpleClass(textBox1);
    }
  }
}
