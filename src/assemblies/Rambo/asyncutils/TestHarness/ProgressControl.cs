using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace MorganStanley.MSDesktop.Rambo.AsyncUtils.TestHarness
{
  public partial class ProgressControl : UserControl
  {
    private int _argument;
    private IBackgroundWorker _backgroundWorker;

    private static ColorMapping _colorMapping = new ColorMapping( );

    public event EventHandler Close;
    public ProgressControl()
    {
      InitializeComponent();
    }

    public IBackgroundWorker BackgroundWorker
    {
      get { return _backgroundWorker; }
      set
      {
        if(_backgroundWorker != null)
        {
          throw new InvalidOperationException("BackGround worker should only be assigned once.");
        }

        _backgroundWorker = value;
        _backgroundWorker.ProgressChanged += backgroundWorker_ProgressChanged;
        _backgroundWorker.RunWorkerCompleted += backgroundWorker_RunWorkerCompleted;

        IThreadPoolConfigurable threadPoolConfig = _backgroundWorker as IThreadPoolConfigurable;
        
        if(threadPoolConfig != null && !string.IsNullOrEmpty(threadPoolConfig.ThreadPool))
        {
          BackColor = _colorMapping[threadPoolConfig.ThreadPool];
        }

      }
    }

    public int Argument
    {
      get { return _argument; }
      set { _argument = value; }
    }

    public bool ShowCloseButton
    {
      get { return btnClose.Visible; }
      set { btnClose.Visible = value; }
    }
    
    private void ProgressControl_Load(object sender, EventArgs e)
    {
      ultraLabel1.Text = Name;
    }

    private void btnStart_Click(object sender, EventArgs e)
    {
      if (!_backgroundWorker.IsBusy)
      {
        _backgroundWorker.RunWorkerAsync(Argument);
        ultraLabel1.Text = Name;
        btnClose.Enabled = false;
        panel1.Enabled = true;

        ultraProgressBar1.Value = 0;
        ultraProgressBar1.Visible = _backgroundWorker.WorkerReportsProgress;

        btnAbort.Enabled = _backgroundWorker.WorkerSupportsAbort;
        btnCancel.Enabled = _backgroundWorker.WorkerSupportsCancellation;
        btnStart.Enabled = _backgroundWorker.WorkerSupportsPause;
        btnStart.Text = "Pause";
      }
      else if (_backgroundWorker.IsPaused)
      {
        _backgroundWorker.RestartAsync();
        btnStart.Text = "Pause";
      }
      else
      {
        _backgroundWorker.PauseAsync();
        btnStart.Text = "Resume";
      }
    }

    private void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
    {
      if(_backgroundWorker.IsPaused)
      {
        ultraProgressBar1.FillAppearance.BackColor = Color.Gold;
      }
      else
      {
        ultraProgressBar1.FillAppearance.BackColor = Color.LimeGreen;
      }

      ultraProgressBar1.Value = e.ProgressPercentage;
      ultraProgressBar1.Visible = e.ProgressPercentage > 0 && e.ProgressPercentage < 100;

      ProgressState progress = (ProgressState)e.UserState;
      ultraLabel1.Text =
        string.Format("{0} last updated: {1} on {2}", Name, 
        progress.Time.ToString("HH:mm:ss"), 
        progress.ThreadId);
    }

    void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
    {
      AbortableRunWorkerCompletedEventArgs args =
        e as AbortableRunWorkerCompletedEventArgs;

      if(args != null && args.Aborted)
      {
        ultraLabel1.Text = string.Format("{0} (Aborted)", Name);
      }
      else if(e.Cancelled)
      {
        ultraLabel1.Text = string.Format("{0} (Cancelled)", Name);
      }
      else if(e.Error != null)
      {
        ultraLabel1.Text = string.Format("{0} (Error) {1}", Name, e.Error.Message);

      }
      else
      {
        TimeSpan span = (TimeSpan)e.Result;

        ultraLabel1.Text = string.Format("{0} (Completed (on {1}) in {2} seconds)", Name, Thread.CurrentThread.Name, Math.Round(span.TotalSeconds));
      }

      if(e.Error == null)
      {
        ultraProgressBar1.FillAppearance.BackColor = Color.Gray; 
      }
      else
      {
        ultraProgressBar1.FillAppearance.BackColor = Color.Red;
      }

      btnClose.Enabled = true;
      btnStart.Enabled = true;
      btnStart.Text = "Start";
      panel1.Enabled = false;
    }

    private void btnAbort_Click(object sender, EventArgs e)
    {
      if(_backgroundWorker != null)
      {
        _backgroundWorker.AbortAsync( );
      }
    }

    private void btnCancel_Click(object sender, EventArgs e)
    {
      if (_backgroundWorker != null)
      {
        _backgroundWorker.CancelAsync( );
      }
    }

    private void btnClose_Click(object sender, EventArgs e)
    {
      this.OnClose(EventArgs.Empty);
    }

    protected void OnClose(EventArgs e)
    {
      if(Close != null)
      {
        Close(this, e);
      }
    }


    private class ColorMapping
    {
      private int _counter = 0;
      private Dictionary<string, Color> _colorMap = new Dictionary<string, Color>();

      private Color[] _colorList =
        new Color[]
          {
            Color.LightCyan,
            Color.LightBlue, 
            Color.LightSteelBlue,
            Color.LightSkyBlue, 
            Color.MediumTurquoise, 
            Color.MediumAquamarine
          };

      public ColorMapping()
      {
        // this just initialized the first two colors to the 
        // "static" thread pools.
        Color dummy = this[ManagedThreadPool.NAME];
        dummy = this[WindowsThreadPool.NAME];
      }

      public Color this[string name_]
      {
        get
        {
          Color color;
          if(!_colorMap.TryGetValue(name_, out color))
          {
            int index = _counter++ % _colorList.Length;
            color = _colorList[index];
            _colorMap.Add(name_, color);
          }

          return color;
        }
      }
    }
  }
}
