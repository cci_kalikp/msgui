namespace MorganStanley.MSDesktop.Rambo.AsyncUtils.TestHarness
{
  partial class BackgroundWorkerFactoryTestForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      Infragistics.Win.UltraWinStatusBar.UltraStatusPanel ultraStatusPanel4 = new Infragistics.Win.UltraWinStatusBar.UltraStatusPanel();
      Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
      Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
      Infragistics.Win.UltraWinStatusBar.UltraStatusPanel ultraStatusPanel5 = new Infragistics.Win.UltraWinStatusBar.UltraStatusPanel();
      Infragistics.Win.UltraWinStatusBar.UltraStatusPanel ultraStatusPanel6 = new Infragistics.Win.UltraWinStatusBar.UltraStatusPanel();
      Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
      Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton1 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
      this.ultraStatusBar1 = new Infragistics.Win.UltraWinStatusBar.UltraStatusBar();
      this.gbParams = new Infragistics.Win.Misc.UltraGroupBox();
      this.gbDynamicThreadPool = new Infragistics.Win.Misc.UltraGroupBox();
      this.lblDTPName = new Infragistics.Win.Misc.UltraLabel();
      this.txtDTPName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
      this.lblKeepAlive = new Infragistics.Win.Misc.UltraLabel();
      this.btnCreateThreadPool = new Infragistics.Win.Misc.UltraButton();
      this.numKeepAlive = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
      this.lblMaxThreads = new Infragistics.Win.Misc.UltraLabel();
      this.numMaxThreads = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
      this.lblMinThreads = new Infragistics.Win.Misc.UltraLabel();
      this.numMinThreads = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
      this.gbThreadPool = new Infragistics.Win.Misc.UltraGroupBox();
      this.lblThreadPoolSize = new Infragistics.Win.Misc.UltraLabel();
      this.numThreadPool = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
      this.gbBackgroundWorker = new Infragistics.Win.Misc.UltraGroupBox();
      this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
      this.txtWorkerName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
      this.cmbThreadPool = new System.Windows.Forms.ComboBox();
      this.lblThreadPool = new Infragistics.Win.Misc.UltraLabel();
      this.lblArgument = new Infragistics.Win.Misc.UltraLabel();
      this.btnCreate = new Infragistics.Win.Misc.UltraButton();
      this.numArgument = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
      this.chkSupportProgress = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
      this.chkSupportPause = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
      this.chkSupportAbort = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
      this.chkSupportCancel = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
      this.gbProgress = new Infragistics.Win.Misc.UltraGroupBox();
      this.panel2 = new System.Windows.Forms.Panel();
      this.btnResetManagedPool = new Infragistics.Win.Misc.UltraButton();
      this.backgroundWorkerComponentBindingSource = new System.Windows.Forms.BindingSource(this.components);
      this.backgroundWorkerFactory = new MorganStanley.MSDesktop.Rambo.AsyncUtils.AsynchronizerBackgroundWorkerFactory();
      ((System.ComponentModel.ISupportInitialize)(this.gbParams)).BeginInit();
      this.gbParams.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.gbDynamicThreadPool)).BeginInit();
      this.gbDynamicThreadPool.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.txtDTPName)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.numKeepAlive)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.numMaxThreads)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.numMinThreads)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.gbThreadPool)).BeginInit();
      this.gbThreadPool.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.numThreadPool)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.gbBackgroundWorker)).BeginInit();
      this.gbBackgroundWorker.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.txtWorkerName)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.numArgument)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.gbProgress)).BeginInit();
      this.gbProgress.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.backgroundWorkerComponentBindingSource)).BeginInit();
      this.SuspendLayout();
      // 
      // ultraStatusBar1
      // 
      this.ultraStatusBar1.Location = new System.Drawing.Point(0, 360);
      this.ultraStatusBar1.Name = "ultraStatusBar1";
      ultraStatusPanel4.BorderStyle = Infragistics.Win.UIElementBorderStyle.Etched;
      appearance4.BackColor2 = System.Drawing.Color.White;
      appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.GlassTop50;
      ultraStatusPanel4.ProgressBarInfo.Appearance = appearance4;
      appearance5.BackColor = System.Drawing.Color.Lime;
      appearance5.BackColor2 = System.Drawing.Color.White;
      appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.GlassTop50;
      appearance5.ForeColor = System.Drawing.Color.Gray;
      ultraStatusPanel4.ProgressBarInfo.FillAppearance = appearance5;
      ultraStatusPanel4.ProgressBarInfo.Style = Infragistics.Win.UltraWinProgressBar.ProgressBarStyle.Continuous;
      ultraStatusPanel4.Style = Infragistics.Win.UltraWinStatusBar.PanelStyle.Progress;
      ultraStatusPanel4.Width = 200;
      ultraStatusPanel5.SizingMode = Infragistics.Win.UltraWinStatusBar.PanelSizingMode.Spring;
      appearance6.TextHAlignAsString = "Center";
      ultraStatusPanel6.Appearance = appearance6;
      ultraStatusPanel6.Padding = new System.Drawing.Size(2, 1);
      ultraStatusPanel6.Style = Infragistics.Win.UltraWinStatusBar.PanelStyle.Button;
      ultraStatusPanel6.Text = "Close";
      this.ultraStatusBar1.Panels.AddRange(new Infragistics.Win.UltraWinStatusBar.UltraStatusPanel[] {
            ultraStatusPanel4,
            ultraStatusPanel5,
            ultraStatusPanel6});
      this.ultraStatusBar1.Size = new System.Drawing.Size(912, 28);
      this.ultraStatusBar1.TabIndex = 0;
      this.ultraStatusBar1.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
      this.ultraStatusBar1.ButtonClick += new Infragistics.Win.UltraWinStatusBar.PanelEventHandler(this.ultraStatusBar1_ButtonClick);
      // 
      // gbParams
      // 
      this.gbParams.Controls.Add(this.gbDynamicThreadPool);
      this.gbParams.Controls.Add(this.gbThreadPool);
      this.gbParams.Dock = System.Windows.Forms.DockStyle.Right;
      this.gbParams.Location = new System.Drawing.Point(712, 0);
      this.gbParams.Name = "gbParams";
      this.gbParams.Size = new System.Drawing.Size(200, 360);
      this.gbParams.TabIndex = 1;
      this.gbParams.Text = "Parameters";
      // 
      // gbDynamicThreadPool
      // 
      this.gbDynamicThreadPool.Controls.Add(this.lblDTPName);
      this.gbDynamicThreadPool.Controls.Add(this.txtDTPName);
      this.gbDynamicThreadPool.Controls.Add(this.lblKeepAlive);
      this.gbDynamicThreadPool.Controls.Add(this.btnCreateThreadPool);
      this.gbDynamicThreadPool.Controls.Add(this.numKeepAlive);
      this.gbDynamicThreadPool.Controls.Add(this.lblMaxThreads);
      this.gbDynamicThreadPool.Controls.Add(this.numMaxThreads);
      this.gbDynamicThreadPool.Controls.Add(this.lblMinThreads);
      this.gbDynamicThreadPool.Controls.Add(this.numMinThreads);
      this.gbDynamicThreadPool.Location = new System.Drawing.Point(7, 131);
      this.gbDynamicThreadPool.Name = "gbDynamicThreadPool";
      this.gbDynamicThreadPool.Size = new System.Drawing.Size(187, 180);
      this.gbDynamicThreadPool.TabIndex = 8;
      this.gbDynamicThreadPool.Text = "Dynamic Thread Pool";
      // 
      // lblDTPName
      // 
      this.lblDTPName.Location = new System.Drawing.Point(6, 21);
      this.lblDTPName.Name = "lblDTPName";
      this.lblDTPName.Size = new System.Drawing.Size(61, 23);
      this.lblDTPName.TabIndex = 5;
      this.lblDTPName.Text = "Name:";
      // 
      // txtDTPName
      // 
      this.txtDTPName.Location = new System.Drawing.Point(72, 19);
      this.txtDTPName.Name = "txtDTPName";
      this.txtDTPName.Size = new System.Drawing.Size(108, 21);
      this.txtDTPName.TabIndex = 4;
      // 
      // lblKeepAlive
      // 
      this.lblKeepAlive.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
      this.lblKeepAlive.Location = new System.Drawing.Point(6, 108);
      this.lblKeepAlive.Name = "lblKeepAlive";
      this.lblKeepAlive.Size = new System.Drawing.Size(76, 23);
      this.lblKeepAlive.TabIndex = 1;
      this.lblKeepAlive.Text = "Keep Alive:";
      // 
      // btnCreateThreadPool
      // 
      this.btnCreateThreadPool.Location = new System.Drawing.Point(6, 131);
      this.btnCreateThreadPool.Name = "btnCreateThreadPool";
      this.btnCreateThreadPool.Size = new System.Drawing.Size(176, 43);
      this.btnCreateThreadPool.TabIndex = 0;
      this.btnCreateThreadPool.Text = "Create Thread Pool";
      this.btnCreateThreadPool.Click += new System.EventHandler(this.btnCreateThreadPool_Click);
      // 
      // numKeepAlive
      // 
      this.numKeepAlive.ButtonsRight.Add(spinEditorButton1);
      this.numKeepAlive.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.numKeepAlive.Location = new System.Drawing.Point(82, 104);
      this.numKeepAlive.MaxValue = 60000;
      this.numKeepAlive.MinValue = -1;
      this.numKeepAlive.Name = "numKeepAlive";
      this.numKeepAlive.PromptChar = ' ';
      this.numKeepAlive.Size = new System.Drawing.Size(98, 21);
      this.numKeepAlive.TabIndex = 0;
      this.numKeepAlive.Value = 30000;
      this.numKeepAlive.EditorSpinButtonClick += new Infragistics.Win.UltraWinEditors.SpinButtonClickEventHandler(this.numKeepAlive_EditorSpinButtonClick);
      // 
      // lblMaxThreads
      // 
      this.lblMaxThreads.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
      this.lblMaxThreads.Location = new System.Drawing.Point(6, 79);
      this.lblMaxThreads.Name = "lblMaxThreads";
      this.lblMaxThreads.Size = new System.Drawing.Size(76, 23);
      this.lblMaxThreads.TabIndex = 1;
      this.lblMaxThreads.Text = "Max Threads:";
      // 
      // numMaxThreads
      // 
      this.numMaxThreads.Location = new System.Drawing.Point(82, 75);
      this.numMaxThreads.MaxValue = 100;
      this.numMaxThreads.MinValue = 0;
      this.numMaxThreads.Name = "numMaxThreads";
      this.numMaxThreads.PromptChar = ' ';
      this.numMaxThreads.Size = new System.Drawing.Size(98, 21);
      this.numMaxThreads.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Always;
      this.numMaxThreads.TabIndex = 0;
      this.numMaxThreads.Value = 5;
      // 
      // lblMinThreads
      // 
      this.lblMinThreads.Location = new System.Drawing.Point(6, 50);
      this.lblMinThreads.Name = "lblMinThreads";
      this.lblMinThreads.Size = new System.Drawing.Size(70, 23);
      this.lblMinThreads.TabIndex = 1;
      this.lblMinThreads.Text = "Min Threads:";
      // 
      // numMinThreads
      // 
      this.numMinThreads.Location = new System.Drawing.Point(82, 46);
      this.numMinThreads.MaxValue = 100;
      this.numMinThreads.MinValue = 0;
      this.numMinThreads.Name = "numMinThreads";
      this.numMinThreads.PromptChar = ' ';
      this.numMinThreads.Size = new System.Drawing.Size(98, 21);
      this.numMinThreads.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Always;
      this.numMinThreads.TabIndex = 0;
      // 
      // gbThreadPool
      // 
      this.gbThreadPool.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.gbThreadPool.Controls.Add(this.btnResetManagedPool);
      this.gbThreadPool.Controls.Add(this.lblThreadPoolSize);
      this.gbThreadPool.Controls.Add(this.numThreadPool);
      this.gbThreadPool.Location = new System.Drawing.Point(6, 17);
      this.gbThreadPool.Name = "gbThreadPool";
      this.gbThreadPool.Size = new System.Drawing.Size(188, 108);
      this.gbThreadPool.TabIndex = 7;
      this.gbThreadPool.Text = "Managed Thread Pool";
      // 
      // lblThreadPoolSize
      // 
      this.lblThreadPoolSize.Location = new System.Drawing.Point(6, 19);
      this.lblThreadPoolSize.Name = "lblThreadPoolSize";
      this.lblThreadPoolSize.Size = new System.Drawing.Size(77, 23);
      this.lblThreadPoolSize.TabIndex = 1;
      this.lblThreadPoolSize.Text = "Max Threads:";
      // 
      // numThreadPool
      // 
      this.numThreadPool.Location = new System.Drawing.Point(83, 19);
      this.numThreadPool.MaxValue = 12;
      this.numThreadPool.MinValue = 1;
      this.numThreadPool.Name = "numThreadPool";
      this.numThreadPool.PromptChar = ' ';
      this.numThreadPool.Size = new System.Drawing.Size(98, 21);
      this.numThreadPool.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Always;
      this.numThreadPool.TabIndex = 0;
      this.numThreadPool.Value = 5;
      this.numThreadPool.ValueChanged += new System.EventHandler(this.numThreadPool_ValueChanged);
      // 
      // gbBackgroundWorker
      // 
      this.gbBackgroundWorker.Controls.Add(this.ultraLabel2);
      this.gbBackgroundWorker.Controls.Add(this.txtWorkerName);
      this.gbBackgroundWorker.Controls.Add(this.cmbThreadPool);
      this.gbBackgroundWorker.Controls.Add(this.lblThreadPool);
      this.gbBackgroundWorker.Controls.Add(this.lblArgument);
      this.gbBackgroundWorker.Controls.Add(this.btnCreate);
      this.gbBackgroundWorker.Controls.Add(this.numArgument);
      this.gbBackgroundWorker.Controls.Add(this.chkSupportProgress);
      this.gbBackgroundWorker.Controls.Add(this.chkSupportPause);
      this.gbBackgroundWorker.Controls.Add(this.chkSupportAbort);
      this.gbBackgroundWorker.Controls.Add(this.chkSupportCancel);
      this.gbBackgroundWorker.Dock = System.Windows.Forms.DockStyle.Left;
      this.gbBackgroundWorker.Location = new System.Drawing.Point(0, 0);
      this.gbBackgroundWorker.Name = "gbBackgroundWorker";
      this.gbBackgroundWorker.Size = new System.Drawing.Size(187, 360);
      this.gbBackgroundWorker.TabIndex = 6;
      this.gbBackgroundWorker.Text = "Background Worker";
      // 
      // ultraLabel2
      // 
      this.ultraLabel2.Location = new System.Drawing.Point(6, 19);
      this.ultraLabel2.Name = "ultraLabel2";
      this.ultraLabel2.Size = new System.Drawing.Size(61, 23);
      this.ultraLabel2.TabIndex = 5;
      this.ultraLabel2.Text = "Name:";
      // 
      // txtWorkerName
      // 
      this.txtWorkerName.Enabled = false;
      this.txtWorkerName.Location = new System.Drawing.Point(73, 19);
      this.txtWorkerName.Name = "txtWorkerName";
      this.txtWorkerName.Size = new System.Drawing.Size(108, 21);
      this.txtWorkerName.TabIndex = 4;
      // 
      // cmbThreadPool
      // 
      this.cmbThreadPool.FormattingEnabled = true;
      this.cmbThreadPool.Location = new System.Drawing.Point(83, 45);
      this.cmbThreadPool.Name = "cmbThreadPool";
      this.cmbThreadPool.Size = new System.Drawing.Size(98, 21);
      this.cmbThreadPool.TabIndex = 6;
      this.cmbThreadPool.SelectedIndexChanged += new System.EventHandler(this.cmbThreadPool_SelectedIndexChanged);
      // 
      // lblThreadPool
      // 
      this.lblThreadPool.Location = new System.Drawing.Point(5, 48);
      this.lblThreadPool.Name = "lblThreadPool";
      this.lblThreadPool.Size = new System.Drawing.Size(70, 23);
      this.lblThreadPool.TabIndex = 1;
      this.lblThreadPool.Text = "ThreadPool:";
      // 
      // lblArgument
      // 
      this.lblArgument.Location = new System.Drawing.Point(6, 77);
      this.lblArgument.Name = "lblArgument";
      this.lblArgument.Size = new System.Drawing.Size(61, 23);
      this.lblArgument.TabIndex = 1;
      this.lblArgument.Text = "Argument:";
      // 
      // btnCreate
      // 
      this.btnCreate.Location = new System.Drawing.Point(4, 208);
      this.btnCreate.Name = "btnCreate";
      this.btnCreate.Size = new System.Drawing.Size(176, 43);
      this.btnCreate.TabIndex = 0;
      this.btnCreate.Text = "Create Worker";
      this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
      // 
      // numArgument
      // 
      this.numArgument.Location = new System.Drawing.Point(82, 72);
      this.numArgument.MaxValue = 10;
      this.numArgument.MinValue = 1;
      this.numArgument.Name = "numArgument";
      this.numArgument.PromptChar = ' ';
      this.numArgument.Size = new System.Drawing.Size(98, 21);
      this.numArgument.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Always;
      this.numArgument.TabIndex = 0;
      this.numArgument.Value = 2;
      // 
      // chkSupportProgress
      // 
      this.chkSupportProgress.DataBindings.Add(new System.Windows.Forms.Binding("CheckedValue", this.backgroundWorkerComponentBindingSource, "WorkerReportsProgress", true));
      this.chkSupportProgress.Location = new System.Drawing.Point(5, 106);
      this.chkSupportProgress.Name = "chkSupportProgress";
      this.chkSupportProgress.Size = new System.Drawing.Size(120, 19);
      this.chkSupportProgress.TabIndex = 3;
      this.chkSupportProgress.Text = "Support Progress";
      // 
      // chkSupportPause
      // 
      this.chkSupportPause.DataBindings.Add(new System.Windows.Forms.Binding("CheckedValue", this.backgroundWorkerComponentBindingSource, "WorkerSupportsPause", true));
      this.chkSupportPause.Location = new System.Drawing.Point(5, 181);
      this.chkSupportPause.Name = "chkSupportPause";
      this.chkSupportPause.Size = new System.Drawing.Size(120, 19);
      this.chkSupportPause.TabIndex = 3;
      this.chkSupportPause.Text = "Support Pause";
      // 
      // chkSupportAbort
      // 
      this.chkSupportAbort.DataBindings.Add(new System.Windows.Forms.Binding("CheckedValue", this.backgroundWorkerComponentBindingSource, "WorkerSupportsAbort", true));
      this.chkSupportAbort.Location = new System.Drawing.Point(5, 156);
      this.chkSupportAbort.Name = "chkSupportAbort";
      this.chkSupportAbort.Size = new System.Drawing.Size(120, 19);
      this.chkSupportAbort.TabIndex = 3;
      this.chkSupportAbort.Text = "Support Abort";
      // 
      // chkSupportCancel
      // 
      this.chkSupportCancel.DataBindings.Add(new System.Windows.Forms.Binding("CheckedValue", this.backgroundWorkerComponentBindingSource, "WorkerSupportsCancellation", true));
      this.chkSupportCancel.Location = new System.Drawing.Point(5, 131);
      this.chkSupportCancel.Name = "chkSupportCancel";
      this.chkSupportCancel.Size = new System.Drawing.Size(120, 19);
      this.chkSupportCancel.TabIndex = 3;
      this.chkSupportCancel.Text = "Support Cancel";
      // 
      // gbProgress
      // 
      this.gbProgress.Controls.Add(this.panel2);
      this.gbProgress.Dock = System.Windows.Forms.DockStyle.Fill;
      this.gbProgress.Location = new System.Drawing.Point(187, 0);
      this.gbProgress.Name = "gbProgress";
      this.gbProgress.Size = new System.Drawing.Size(525, 360);
      this.gbProgress.TabIndex = 3;
      this.gbProgress.Text = "Progress";
      // 
      // panel2
      // 
      this.panel2.AutoScroll = true;
      this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel2.Location = new System.Drawing.Point(3, 16);
      this.panel2.Name = "panel2";
      this.panel2.Size = new System.Drawing.Size(519, 341);
      this.panel2.TabIndex = 0;
      // 
      // btnResetManagedPool
      // 
      this.btnResetManagedPool.Location = new System.Drawing.Point(7, 55);
      this.btnResetManagedPool.Name = "btnResetManagedPool";
      this.btnResetManagedPool.Size = new System.Drawing.Size(176, 47);
      this.btnResetManagedPool.TabIndex = 2;
      this.btnResetManagedPool.Text = "Reset";
      this.btnResetManagedPool.Click += new System.EventHandler(this.btnResetManagedPool_Click);
      // 
      // backgroundWorkerComponentBindingSource
      // 
      this.backgroundWorkerComponentBindingSource.DataSource = typeof(MorganStanley.MSDesktop.Rambo.AsyncUtils.IBackgroundWorkerComponent);
      // 
      // backgroundWorkerFactory
      // 
      this.backgroundWorkerFactory.ThreadPool = null;
      this.backgroundWorkerFactory.WorkerReportsProgress = true;
      this.backgroundWorkerFactory.WorkerSupportsAbort = true;
      this.backgroundWorkerFactory.WorkerSupportsCancellation = true;
      this.backgroundWorkerFactory.WorkerSupportsPause = true;
      this.backgroundWorkerFactory.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerFactory_DoWork);
      this.backgroundWorkerFactory.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerFactory_RunWorkerCompleted);
      this.backgroundWorkerFactory.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorkerFactory_ProgressChanged);
      // 
      // BackgroundWorkerFactoryTestForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(912, 388);
      this.Controls.Add(this.gbProgress);
      this.Controls.Add(this.gbBackgroundWorker);
      this.Controls.Add(this.gbParams);
      this.Controls.Add(this.ultraStatusBar1);
      this.MinimizeBox = false;
      this.MinimumSize = new System.Drawing.Size(580, 362);
      this.Name = "BackgroundWorkerFactoryTestForm";
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.Text = "BackgroundWorker Factory";
      this.Load += new System.EventHandler(this.AsyncTestForm_Load);
      ((System.ComponentModel.ISupportInitialize)(this.gbParams)).EndInit();
      this.gbParams.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.gbDynamicThreadPool)).EndInit();
      this.gbDynamicThreadPool.ResumeLayout(false);
      this.gbDynamicThreadPool.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.txtDTPName)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.numKeepAlive)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.numMaxThreads)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.numMinThreads)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.gbThreadPool)).EndInit();
      this.gbThreadPool.ResumeLayout(false);
      this.gbThreadPool.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.numThreadPool)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.gbBackgroundWorker)).EndInit();
      this.gbBackgroundWorker.ResumeLayout(false);
      this.gbBackgroundWorker.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.txtWorkerName)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.numArgument)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.gbProgress)).EndInit();
      this.gbProgress.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.backgroundWorkerComponentBindingSource)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private AsynchronizerBackgroundWorkerFactory backgroundWorkerFactory;
    private Infragistics.Win.UltraWinStatusBar.UltraStatusBar ultraStatusBar1;
    private Infragistics.Win.Misc.UltraGroupBox gbParams;
    private Infragistics.Win.Misc.UltraLabel lblArgument;
    private Infragistics.Win.UltraWinEditors.UltraNumericEditor numArgument;
    private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkSupportCancel;
    private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkSupportAbort;
    private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkSupportProgress;
    private Infragistics.Win.UltraWinEditors.UltraTextEditor txtWorkerName;
    private Infragistics.Win.Misc.UltraButton btnCreate;
    private Infragistics.Win.Misc.UltraLabel ultraLabel2;
    private Infragistics.Win.Misc.UltraGroupBox gbProgress;
    private System.Windows.Forms.Panel panel2;
    private System.Windows.Forms.BindingSource backgroundWorkerComponentBindingSource;
    private Infragistics.Win.Misc.UltraGroupBox gbThreadPool;
    private Infragistics.Win.Misc.UltraGroupBox gbBackgroundWorker;
    private Infragistics.Win.Misc.UltraLabel lblThreadPoolSize;
    private Infragistics.Win.UltraWinEditors.UltraNumericEditor numThreadPool;
    private Infragistics.Win.Misc.UltraLabel lblThreadPool;
    private System.Windows.Forms.ComboBox cmbThreadPool;
    private Infragistics.Win.Misc.UltraGroupBox gbDynamicThreadPool;
    private Infragistics.Win.Misc.UltraLabel lblDTPName;
    private Infragistics.Win.UltraWinEditors.UltraTextEditor txtDTPName;
    private Infragistics.Win.Misc.UltraLabel lblMaxThreads;
    private Infragistics.Win.UltraWinEditors.UltraNumericEditor numMaxThreads;
    private Infragistics.Win.Misc.UltraLabel lblMinThreads;
    private Infragistics.Win.UltraWinEditors.UltraNumericEditor numMinThreads;
    private Infragistics.Win.Misc.UltraLabel lblKeepAlive;
    private Infragistics.Win.Misc.UltraButton btnCreateThreadPool;
    private Infragistics.Win.UltraWinEditors.UltraNumericEditor numKeepAlive;
    private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkSupportPause;
    private Infragistics.Win.Misc.UltraButton btnResetManagedPool;
  }
}