using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace MorganStanley.MSDesktop.Rambo.AsyncUtils.TestHarness
{
  public partial class MultiplexTestForm : Form
  {
    #region Readonly & Static Fields

    private readonly IThreadPool _threadPool;
    private readonly ThreadPoolManager _threadPoolManager;

    #endregion

    #region Fields

    private int _completed;
    private int _counter = 1;
    private int _keyCount;
    private StringBuilder _log;
    private DateTime _startTime;
    private int _total;

    #endregion

    #region Constructors

    public MultiplexTestForm()
    {
      _threadPoolManager = ThreadPoolManager.Instance;
      if (!_threadPoolManager.ThreadPoolExists("My dynamic pool"))
      {
        _threadPool = _threadPoolManager.CreateDynamicThreadPool("My dynamic pool", 1, 10);
      }
      else
      {
        _threadPoolManager.GetThreadPool("My dynamic pool");
      }
      InitializeComponent();
    }

    #endregion

    #region Instance Methods

    private void DoneMultiPlexOperation(IAsyncResult ar)
    {
      ultraGroupBox1.Enabled = true;
      _log.AppendLine("Done.");
      _log.AppendLine("----------------------------------------");
      ultraTextEditor1.Text = _log.ToString();

      MultiplexAsynchronizerResult result = (MultiplexAsynchronizerResult) ar;

      _log.AppendLine(string.Format("Results (on {0})", Thread.CurrentThread.Name));

      foreach (KeyValuePair<object, object> pair in result.Results)
      {
        _log.AppendLine(string.Format("Result of process '{0}' is '{1}'", pair.Key, pair.Value));
        ultraTextEditor1.Text = _log.ToString();
      }

      foreach (KeyValuePair<object, Exception> pair in result.ResultExceptions)
      {
        _log.AppendLine(string.Format("Exception process '{0}' is '{1}'", pair.Key, pair.Value.StackTrace));
        ultraTextEditor1.Text = _log.ToString();
      }

      Asynchronizer parentAsynchronizer = result.AsynchronizerParent;
      if (parentAsynchronizer != null)
      {
        object returnValue;
        if (!parentAsynchronizer.TryEndInvoke(result, out returnValue))
        {
          _log.AppendLine(string.Format("OVERALL EXCEPTION: {0}", result.InvokeException));
          ultraTextEditor1.Text = _log.ToString();          
        }
      }
    }

    private string PerformChildItemWork(string name, int sleepTime)
    {
      Thread.Sleep(sleepTime*1000);
      string threadName = Thread.CurrentThread.Name;
      ThreadingUtils.ThreadSafeInvoke(new UpdateStatisticsDelegate(UpdateStatistics), name, threadName);
      if ((sleepTime%2) == 0)
      {
        throw new InvalidOperationException("Test Exception");
      }
      return string.Format("Process name is '{0}', ThreadName: '{1}', SleepTime: {2}'", name, threadName, sleepTime);
    }

    private void UpdateStatistics(string name, string thread)
    {
      _log.AppendLine(
        string.Format("{0} - invoked on {1} ({2} seconds)", name, thread, (DateTime.Now - _startTime).Seconds));
      ultraTextEditor1.Text = _log.ToString();

      ultraStatusBar1.Panels[0].ProgressBarInfo.Value = ++_completed;
    }

    #endregion

    #region Event Handling

    private void ultraButton1_Click(object sender, EventArgs e)
    {
      ultraGroupBox1.Enabled = false;

      _log = new StringBuilder();

      _total = panel1.Controls.Count;
      _completed = 0;

      ultraStatusBar1.Panels[0].ProgressBarInfo.Maximum = _total;
      ultraStatusBar1.Panels[0].ProgressBarInfo.Value = _completed;
      MultiplexAsynchronizer multiPlex = new MultiplexAsynchronizer(DoneMultiPlexOperation, null, _threadPool);

      for (int i = panel1.Controls.Count - 1; i >= 0; i--)
      {
        WorkItemControl item = (WorkItemControl) panel1.Controls[i];
        if (_chkAssignKeys.Checked)
        {
          string key = string.Format("Operation #{0}", _keyCount++);
          multiPlex.AddKeyedOperation(key, new WorkItemDelegate(PerformChildItemWork), item.ItemName, item.SleepTime);
        }
        else
        {
          multiPlex.AddOperation(new WorkItemDelegate(PerformChildItemWork), item.ItemName, item.SleepTime);
        }
      }
      _keyCount = 0;

      _startTime = DateTime.Now;
      multiPlex.BeginInvokeAll();
    }

    private void ultraButton2_Click(object sender, EventArgs e)
    {
      panel1.SuspendLayout();
      WorkItemControl ctrl = new WorkItemControl();
      ctrl.ItemName = string.Format("Item{0}", _counter++);
      panel1.Controls.Add(ctrl);
      ctrl.BringToFront();
      ctrl.Dock = DockStyle.Top;
      panel1.ResumeLayout();
    }

    private void ultraStatusBar1_Click(object sender, EventArgs e)
    {
      Close();
    }

    #endregion

    #region Nested type: UpdateStatisticsDelegate

    private delegate void UpdateStatisticsDelegate(string itemName, string threadName);

    #endregion

    #region Nested type: WorkItemDelegate

    private delegate string WorkItemDelegate(string name, int sleepTime);

    #endregion
  }
}