using System;

namespace MorganStanley.MSDesktop.Rambo.AsyncUtils.TestHarness
{
  public class ProgressState
  {
    private DateTime _time;
    private string _threadId;

    public ProgressState(DateTime time_, string threadId_)
    {
      _time = time_;
      _threadId = threadId_;
    }

    public DateTime Time
    {
      get { return _time; }
    }

    public string ThreadId
    {
      get { return _threadId; }
    }
  }
}