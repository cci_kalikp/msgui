namespace MorganStanley.MSDesktop.Rambo.AsyncUtils.TestHarness
{
  partial class MultiplexTestForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      Infragistics.Win.UltraWinStatusBar.UltraStatusPanel ultraStatusPanel1 = new Infragistics.Win.UltraWinStatusBar.UltraStatusPanel();
      Infragistics.Win.UltraWinStatusBar.UltraStatusPanel ultraStatusPanel2 = new Infragistics.Win.UltraWinStatusBar.UltraStatusPanel();
      Infragistics.Win.UltraWinStatusBar.UltraStatusPanel ultraStatusPanel3 = new Infragistics.Win.UltraWinStatusBar.UltraStatusPanel();
      Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
      this.ultraStatusBar1 = new Infragistics.Win.UltraWinStatusBar.UltraStatusBar();
      this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
      this._chkAssignKeys = new System.Windows.Forms.CheckBox();
      this.ultraButton2 = new Infragistics.Win.Misc.UltraButton();
      this.ultraButton1 = new Infragistics.Win.Misc.UltraButton();
      this.panel1 = new System.Windows.Forms.Panel();
      this.splitter1 = new System.Windows.Forms.Splitter();
      this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
      this.ultraTextEditor1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
      ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
      this.ultraGroupBox1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
      this.ultraGroupBox2.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor1)).BeginInit();
      this.SuspendLayout();
      // 
      // ultraStatusBar1
      // 
      this.ultraStatusBar1.Location = new System.Drawing.Point(0, 319);
      this.ultraStatusBar1.Name = "ultraStatusBar1";
      ultraStatusPanel1.Style = Infragistics.Win.UltraWinStatusBar.PanelStyle.Progress;
      ultraStatusPanel2.SizingMode = Infragistics.Win.UltraWinStatusBar.PanelSizingMode.Spring;
      appearance1.TextHAlignAsString = "Center";
      ultraStatusPanel3.Appearance = appearance1;
      ultraStatusPanel3.Style = Infragistics.Win.UltraWinStatusBar.PanelStyle.Button;
      ultraStatusPanel3.Text = "Close";
      this.ultraStatusBar1.Panels.AddRange(new Infragistics.Win.UltraWinStatusBar.UltraStatusPanel[] {
            ultraStatusPanel1,
            ultraStatusPanel2,
            ultraStatusPanel3});
      this.ultraStatusBar1.Size = new System.Drawing.Size(649, 23);
      this.ultraStatusBar1.TabIndex = 0;
      this.ultraStatusBar1.Text = "ultraStatusBar1";
      this.ultraStatusBar1.Click += new System.EventHandler(this.ultraStatusBar1_Click);
      // 
      // ultraGroupBox1
      // 
      this.ultraGroupBox1.Controls.Add(this._chkAssignKeys);
      this.ultraGroupBox1.Controls.Add(this.ultraButton2);
      this.ultraGroupBox1.Controls.Add(this.ultraButton1);
      this.ultraGroupBox1.Controls.Add(this.panel1);
      this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Left;
      this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
      this.ultraGroupBox1.MinimumSize = new System.Drawing.Size(319, 319);
      this.ultraGroupBox1.Name = "ultraGroupBox1";
      this.ultraGroupBox1.Size = new System.Drawing.Size(319, 319);
      this.ultraGroupBox1.TabIndex = 1;
      this.ultraGroupBox1.Text = "Work Items";
      // 
      // _chkAssignKeys
      // 
      this._chkAssignKeys.AutoSize = true;
      this._chkAssignKeys.Location = new System.Drawing.Point(6, 260);
      this._chkAssignKeys.Name = "_chkAssignKeys";
      this._chkAssignKeys.Size = new System.Drawing.Size(149, 17);
      this._chkAssignKeys.TabIndex = 2;
      this._chkAssignKeys.Text = "Assign Keys to Operations";
      this._chkAssignKeys.UseVisualStyleBackColor = true;
      // 
      // ultraButton2
      // 
      this.ultraButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.ultraButton2.Location = new System.Drawing.Point(211, 260);
      this.ultraButton2.Name = "ultraButton2";
      this.ultraButton2.Size = new System.Drawing.Size(49, 53);
      this.ultraButton2.TabIndex = 1;
      this.ultraButton2.Text = "&Add";
      this.ultraButton2.Click += new System.EventHandler(this.ultraButton2_Click);
      // 
      // ultraButton1
      // 
      this.ultraButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.ultraButton1.Location = new System.Drawing.Point(266, 260);
      this.ultraButton1.Name = "ultraButton1";
      this.ultraButton1.Size = new System.Drawing.Size(47, 53);
      this.ultraButton1.TabIndex = 1;
      this.ultraButton1.Text = "&Start";
      this.ultraButton1.Click += new System.EventHandler(this.ultraButton1_Click);
      // 
      // panel1
      // 
      this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.panel1.AutoScroll = true;
      this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.panel1.Location = new System.Drawing.Point(6, 19);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(307, 235);
      this.panel1.TabIndex = 0;
      // 
      // splitter1
      // 
      this.splitter1.Location = new System.Drawing.Point(319, 0);
      this.splitter1.Name = "splitter1";
      this.splitter1.Size = new System.Drawing.Size(10, 319);
      this.splitter1.TabIndex = 2;
      this.splitter1.TabStop = false;
      // 
      // ultraGroupBox2
      // 
      this.ultraGroupBox2.Controls.Add(this.ultraTextEditor1);
      this.ultraGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.ultraGroupBox2.Location = new System.Drawing.Point(329, 0);
      this.ultraGroupBox2.Name = "ultraGroupBox2";
      this.ultraGroupBox2.Size = new System.Drawing.Size(320, 319);
      this.ultraGroupBox2.TabIndex = 3;
      this.ultraGroupBox2.Text = "Status";
      // 
      // ultraTextEditor1
      // 
      this.ultraTextEditor1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2003;
      this.ultraTextEditor1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.ultraTextEditor1.Location = new System.Drawing.Point(3, 16);
      this.ultraTextEditor1.Multiline = true;
      this.ultraTextEditor1.Name = "ultraTextEditor1";
      this.ultraTextEditor1.ReadOnly = true;
      this.ultraTextEditor1.Scrollbars = System.Windows.Forms.ScrollBars.Vertical;
      this.ultraTextEditor1.Size = new System.Drawing.Size(314, 300);
      this.ultraTextEditor1.TabIndex = 0;
      // 
      // MultiplexTestForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(649, 342);
      this.Controls.Add(this.ultraGroupBox2);
      this.Controls.Add(this.splitter1);
      this.Controls.Add(this.ultraGroupBox1);
      this.Controls.Add(this.ultraStatusBar1);
      this.MinimizeBox = false;
      this.MinimumSize = new System.Drawing.Size(657, 376);
      this.Name = "MultiplexTestForm";
      this.ShowInTaskbar = false;
      this.Text = "MultiplexTestForm";
      ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
      this.ultraGroupBox1.ResumeLayout(false);
      this.ultraGroupBox1.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
      this.ultraGroupBox2.ResumeLayout(false);
      this.ultraGroupBox2.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor1)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private Infragistics.Win.UltraWinStatusBar.UltraStatusBar ultraStatusBar1;
    private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
    private System.Windows.Forms.Splitter splitter1;
    private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
    private System.Windows.Forms.Panel panel1;
    private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor1;
    private Infragistics.Win.Misc.UltraButton ultraButton2;
    private Infragistics.Win.Misc.UltraButton ultraButton1;
    private System.Windows.Forms.CheckBox _chkAssignKeys;
  }
}