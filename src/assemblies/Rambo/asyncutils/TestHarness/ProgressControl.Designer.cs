namespace MorganStanley.MSDesktop.Rambo.AsyncUtils.TestHarness
{
  partial class ProgressControl
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing)
      {
        if(_backgroundWorker != null)
        {
          _backgroundWorker.Dispose( );
        }

        if (components != null)
        {
          components.Dispose( );
        }
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
      Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
      Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
      this.ultraProgressBar1 = new Infragistics.Win.UltraWinProgressBar.UltraProgressBar();
      this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
      this.btnStart = new Infragistics.Win.Misc.UltraButton();
      this.btnAbort = new Infragistics.Win.Misc.UltraButton();
      this.btnCancel = new Infragistics.Win.Misc.UltraButton();
      this.btnClose = new Infragistics.Win.Misc.UltraButton();
      this.panel1 = new System.Windows.Forms.Panel();
      this.panel1.SuspendLayout();
      this.SuspendLayout();
      // 
      // ultraProgressBar1
      // 
      this.ultraProgressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
      appearance1.BackColor2 = System.Drawing.Color.White;
      appearance1.BackGradientStyle = Infragistics.Win.GradientStyle.GlassTop50;
      this.ultraProgressBar1.Appearance = appearance1;
      this.ultraProgressBar1.BorderStyle = Infragistics.Win.UIElementBorderStyle.RaisedSoft;
      appearance2.BackColor = System.Drawing.Color.LimeGreen;
      appearance2.BackColor2 = System.Drawing.Color.WhiteSmoke;
      appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.GlassTop50;
      appearance2.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
      this.ultraProgressBar1.FillAppearance = appearance2;
      this.ultraProgressBar1.Location = new System.Drawing.Point(3, 24);
      this.ultraProgressBar1.Name = "ultraProgressBar1";
      this.ultraProgressBar1.Size = new System.Drawing.Size(401, 16);
      this.ultraProgressBar1.TabIndex = 0;
      this.ultraProgressBar1.Text = "[Formatted]";
      this.ultraProgressBar1.UseFlatMode = Infragistics.Win.DefaultableBoolean.False;
      this.ultraProgressBar1.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
      this.ultraProgressBar1.Value = 50;
      this.ultraProgressBar1.Visible = false;
      // 
      // ultraLabel1
      // 
      this.ultraLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.ultraLabel1.Location = new System.Drawing.Point(3, 3);
      this.ultraLabel1.Name = "ultraLabel1";
      this.ultraLabel1.Size = new System.Drawing.Size(380, 13);
      this.ultraLabel1.TabIndex = 1;
      // 
      // btnStart
      // 
      this.btnStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.btnStart.Location = new System.Drawing.Point(3, 44);
      this.btnStart.Name = "btnStart";
      this.btnStart.Size = new System.Drawing.Size(75, 20);
      this.btnStart.TabIndex = 2;
      this.btnStart.Text = "Start";
      this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
      // 
      // btnAbort
      // 
      this.btnAbort.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btnAbort.Location = new System.Drawing.Point(84, 1);
      this.btnAbort.Name = "btnAbort";
      this.btnAbort.Size = new System.Drawing.Size(75, 20);
      this.btnAbort.TabIndex = 2;
      this.btnAbort.Text = "Abort";
      this.btnAbort.Click += new System.EventHandler(this.btnAbort_Click);
      // 
      // btnCancel
      // 
      this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btnCancel.Location = new System.Drawing.Point(3, 1);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(75, 20);
      this.btnCancel.TabIndex = 2;
      this.btnCancel.Text = "Cancel";
      this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
      // 
      // btnClose
      // 
      this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      appearance3.FontData.BoldAsString = "True";
      appearance3.FontData.SizeInPoints = 6F;
      appearance3.ForeColor = System.Drawing.Color.Red;
      appearance3.TextHAlignAsString = "Center";
      appearance3.TextVAlignAsString = "Middle";
      this.btnClose.Appearance = appearance3;
      this.btnClose.Location = new System.Drawing.Point(389, 1);
      this.btnClose.Name = "btnClose";
      this.btnClose.Size = new System.Drawing.Size(16, 18);
      this.btnClose.TabIndex = 3;
      this.btnClose.Text = "X";
      this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
      // 
      // panel1
      // 
      this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.panel1.Controls.Add(this.btnAbort);
      this.panel1.Controls.Add(this.btnCancel);
      this.panel1.Enabled = false;
      this.panel1.Location = new System.Drawing.Point(245, 43);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(162, 25);
      this.panel1.TabIndex = 4;
      // 
      // ProgressControl
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.Controls.Add(this.panel1);
      this.Controls.Add(this.btnClose);
      this.Controls.Add(this.btnStart);
      this.Controls.Add(this.ultraLabel1);
      this.Controls.Add(this.ultraProgressBar1);
      this.Name = "ProgressControl";
      this.Size = new System.Drawing.Size(409, 67);
      this.Load += new System.EventHandler(this.ProgressControl_Load);
      this.panel1.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private Infragistics.Win.UltraWinProgressBar.UltraProgressBar ultraProgressBar1;
    private Infragistics.Win.Misc.UltraLabel ultraLabel1;
    private Infragistics.Win.Misc.UltraButton btnStart;
    private Infragistics.Win.Misc.UltraButton btnAbort;
    private Infragistics.Win.Misc.UltraButton btnCancel;
    private Infragistics.Win.Misc.UltraButton btnClose;
    private System.Windows.Forms.Panel panel1;
  }
}
