using System;
using System.ComponentModel;
using System.Threading;

namespace MorganStanley.MSDesktop.Rambo.AsyncUtils.TestHarness
{
  public static class DoWorkAgorithm
  {
    public static void DoWork(IBackgroundWorker worker_, DoWorkEventArgs e_)
    {

      int milliseconds = (int)e_.Argument * 1000;
      int totalSteps = 10;
      DateTime start = DateTime.Now;

      if(milliseconds < 0)
      {
        throw new IndexOutOfRangeException(string.Format("The argment {0} must be non-negative", e_.Argument));
      }

      for (int i = 0; i < totalSteps; i++)
      {
        if (worker_.IsPaused)
        {
          PauseOperation(worker_, i, totalSteps);
        }

        if (worker_.CancellationPending)
        {
          CancelOperation(worker_, e_, start);
          return;
        }

        // my worker sleeps on the job
        // insert real work here..
        Thread.Sleep(milliseconds);

        if (worker_.WorkerReportsProgress)
        {
          ReportProgress(worker_, i + 1, totalSteps);
        }
      }

      e_.Result = DateTime.Now - start;
    }

    private static void PauseOperation(IBackgroundWorker worker_, int stepsCompleted_, int totalSteps_)
    {
      if (worker_.WorkerReportsProgress)
      {
        ReportProgress(worker_, stepsCompleted_, totalSteps_);
      }

      while (worker_.IsPaused)
      {
        // loop until IsPaused is changed or until there is a cancellation request.
        if (worker_.CancellationPending)
        {
          break;
        }
      }

      if (worker_.WorkerReportsProgress)
      {
        ReportProgress(worker_, stepsCompleted_, totalSteps_);
      }
    }

    private static void ReportProgress(IBackgroundWorker worker_, int stepsCompleted_, int total_)
    {
      ProgressState progressState = new ProgressState(DateTime.Now, Thread.CurrentThread.Name);
      int progressAmount = (int)Math.Round(((double)stepsCompleted_*100)/total_);
      worker_.ReportProgress(progressAmount, progressState);
    }

    private static void CancelOperation(IBackgroundWorker worker_, DoWorkEventArgs e_, DateTime startTime_)
    {
      e_.Cancel = true;
      e_.Result = DateTime.Now - startTime_;
    }
  }
}