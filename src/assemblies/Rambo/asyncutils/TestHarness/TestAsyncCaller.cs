﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using MorganStanley.MSDotNet.MSNet;

namespace MorganStanley.MSDesktop.Rambo.AsyncUtils.TestHarness
{
  class TestAsyncCaller
  {
    public static void StartTestAsyncCaller()
    {
      TestAsyncCaller tester = new TestAsyncCaller();
      StartBW();
    }

    static void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
    {
      //TODO: If you're using this class, test here for expected thread handling.  In the future, NUnit tests should confirm the various conditions
      ((IDisposable) sender).Dispose();
    }

    static void bw_DoWork(object sender, DoWorkEventArgs e)
    {
      ((IBackgroundWorker)sender).ReportProgress(42, "Hello, World!");
      e.Result = "test";
    }

    private static void StartBW()
    {
      AsynchronizerBackgroundWorker bw = new AsynchronizerBackgroundWorker();
      bw.WorkerReportsProgress = true;
      bw.DoWork += new DoWorkEventHandler(bw_DoWork);
      bw.ProgressChanged += new ProgressChangedEventHandler(bw_ProgressChanged);
      bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bw_RunWorkerCompleted);
      bw.RunWorkerAsync();
    }

    static void bw_ProgressChanged(object sender, ProgressChangedEventArgs e)
    {
      // This should happen on UI thread.
      MessageBox.Show(string.Format("Percent: {0} UserState: {1} Thread: {2}", e.ProgressPercentage, e.UserState,
                                    Thread.CurrentThread.Name));
    }
  }
}
