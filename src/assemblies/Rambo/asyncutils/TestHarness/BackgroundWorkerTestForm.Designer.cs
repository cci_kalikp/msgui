namespace MorganStanley.MSDesktop.Rambo.AsyncUtils.TestHarness
{
  partial class BackgroundWorkerTestForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.gbBackgroundWorker = new Infragistics.Win.Misc.UltraGroupBox();
      this.lblArgument = new Infragistics.Win.Misc.UltraLabel();
      this.numArgument = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
      this.chkSupportProgress = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
      this.iBackgroundWorkerComponentBindingSource = new System.Windows.Forms.BindingSource(this.components);
      this.chkSupportAbort = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
      this.chkSupportCancel = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
      this.chkSupportPause = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
      this.progress = new MorganStanley.MSDesktop.Rambo.AsyncUtils.TestHarness.ProgressControl();
      this.asynchronizerBackgroundWorker1 = new MorganStanley.MSDesktop.Rambo.AsyncUtils.AsynchronizerBackgroundWorker();
      ((System.ComponentModel.ISupportInitialize)(this.gbBackgroundWorker)).BeginInit();
      this.gbBackgroundWorker.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.numArgument)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.iBackgroundWorkerComponentBindingSource)).BeginInit();
      this.SuspendLayout();
      // 
      // gbBackgroundWorker
      // 
      this.gbBackgroundWorker.Controls.Add(this.lblArgument);
      this.gbBackgroundWorker.Controls.Add(this.numArgument);
      this.gbBackgroundWorker.Controls.Add(this.chkSupportProgress);
      this.gbBackgroundWorker.Controls.Add(this.chkSupportPause);
      this.gbBackgroundWorker.Controls.Add(this.chkSupportAbort);
      this.gbBackgroundWorker.Controls.Add(this.chkSupportCancel);
      this.gbBackgroundWorker.Dock = System.Windows.Forms.DockStyle.Right;
      this.gbBackgroundWorker.Location = new System.Drawing.Point(383, 0);
      this.gbBackgroundWorker.Name = "gbBackgroundWorker";
      this.gbBackgroundWorker.Size = new System.Drawing.Size(187, 140);
      this.gbBackgroundWorker.TabIndex = 7;
      this.gbBackgroundWorker.Text = "Background Worker";
      // 
      // lblArgument
      // 
      this.lblArgument.Location = new System.Drawing.Point(6, 19);
      this.lblArgument.Name = "lblArgument";
      this.lblArgument.Size = new System.Drawing.Size(61, 23);
      this.lblArgument.TabIndex = 1;
      this.lblArgument.Text = "Argument:";
      // 
      // numArgument
      // 
      this.numArgument.Location = new System.Drawing.Point(83, 15);
      this.numArgument.MaxValue = 10;
      this.numArgument.MinValue = -1;
      this.numArgument.Name = "numArgument";
      this.numArgument.PromptChar = ' ';
      this.numArgument.Size = new System.Drawing.Size(98, 21);
      this.numArgument.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Always;
      this.numArgument.TabIndex = 0;
      this.numArgument.Value = 2;
      this.numArgument.ValueChanged += new System.EventHandler(this.numArgument_ValueChanged);
      // 
      // chkSupportProgress
      // 
      this.chkSupportProgress.Checked = true;
      this.chkSupportProgress.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chkSupportProgress.DataBindings.Add(new System.Windows.Forms.Binding("CheckedValue", this.iBackgroundWorkerComponentBindingSource, "WorkerReportsProgress", true));
      this.chkSupportProgress.Location = new System.Drawing.Point(5, 39);
      this.chkSupportProgress.Name = "chkSupportProgress";
      this.chkSupportProgress.Size = new System.Drawing.Size(120, 20);
      this.chkSupportProgress.TabIndex = 3;
      this.chkSupportProgress.Text = "Support Progress";
      // 
      // iBackgroundWorkerComponentBindingSource
      // 
      this.iBackgroundWorkerComponentBindingSource.DataSource = typeof(MorganStanley.MSDesktop.Rambo.AsyncUtils.IBackgroundWorkerComponent);
      // 
      // chkSupportAbort
      // 
      this.chkSupportAbort.Checked = true;
      this.chkSupportAbort.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chkSupportAbort.DataBindings.Add(new System.Windows.Forms.Binding("CheckedValue", this.iBackgroundWorkerComponentBindingSource, "WorkerSupportsAbort", true));
      this.chkSupportAbort.Location = new System.Drawing.Point(5, 88);
      this.chkSupportAbort.Name = "chkSupportAbort";
      this.chkSupportAbort.Size = new System.Drawing.Size(120, 20);
      this.chkSupportAbort.TabIndex = 3;
      this.chkSupportAbort.Text = "Support Abort";
      // 
      // chkSupportCancel
      // 
      this.chkSupportCancel.Checked = true;
      this.chkSupportCancel.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chkSupportCancel.DataBindings.Add(new System.Windows.Forms.Binding("CheckedValue", this.iBackgroundWorkerComponentBindingSource, "WorkerSupportsCancellation", true));
      this.chkSupportCancel.Location = new System.Drawing.Point(5, 62);
      this.chkSupportCancel.Name = "chkSupportCancel";
      this.chkSupportCancel.Size = new System.Drawing.Size(120, 20);
      this.chkSupportCancel.TabIndex = 3;
      this.chkSupportCancel.Text = "Support Cancel";
      // 
      // chkSupportPause
      // 
      this.chkSupportPause.Checked = true;
      this.chkSupportPause.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chkSupportPause.DataBindings.Add(new System.Windows.Forms.Binding("CheckedValue", this.iBackgroundWorkerComponentBindingSource, "WorkerSupportsPause", true));
      this.chkSupportPause.Location = new System.Drawing.Point(5, 114);
      this.chkSupportPause.Name = "chkSupportPause";
      this.chkSupportPause.Size = new System.Drawing.Size(120, 20);
      this.chkSupportPause.TabIndex = 3;
      this.chkSupportPause.Text = "Support Pause";
      // 
      // progress
      // 
      this.progress.Argument = 2;
      this.progress.BackgroundWorker = this.asynchronizerBackgroundWorker1;
      this.progress.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.progress.Dock = System.Windows.Forms.DockStyle.Fill;
      this.progress.Location = new System.Drawing.Point(0, 0);
      this.progress.Name = "progress";
      this.progress.ShowCloseButton = false;
      this.progress.Size = new System.Drawing.Size(383, 140);
      this.progress.TabIndex = 0;
      // 
      // asynchronizerBackgroundWorker1
      // 
      this.asynchronizerBackgroundWorker1.ThreadPool = null;
      this.asynchronizerBackgroundWorker1.WorkerReportsProgress = true;
      this.asynchronizerBackgroundWorker1.WorkerSupportsAbort = true;
      this.asynchronizerBackgroundWorker1.WorkerSupportsCancellation = true;
      this.asynchronizerBackgroundWorker1.WorkerSupportsPause = true;
      this.asynchronizerBackgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.asynchronizerBackgroundWorker1_DoWork);
      this.asynchronizerBackgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.asynchronizerBackgroundWorker1_RunWorkerCompleted);
      // 
      // BackgroundWorkerTestForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(570, 140);
      this.Controls.Add(this.progress);
      this.Controls.Add(this.gbBackgroundWorker);
      this.MinimizeBox = false;
      this.Name = "BackgroundWorkerTestForm";
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.Text = "Background Worker Test";
      this.Load += new System.EventHandler(this.BackgroundWorkerTestForm_Load);
      ((System.ComponentModel.ISupportInitialize)(this.gbBackgroundWorker)).EndInit();
      this.gbBackgroundWorker.ResumeLayout(false);
      this.gbBackgroundWorker.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.numArgument)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.iBackgroundWorkerComponentBindingSource)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private AsynchronizerBackgroundWorker asynchronizerBackgroundWorker1;
    private ProgressControl progress;
    private Infragistics.Win.Misc.UltraGroupBox gbBackgroundWorker;
    private Infragistics.Win.Misc.UltraLabel lblArgument;
    private Infragistics.Win.UltraWinEditors.UltraNumericEditor numArgument;
    private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkSupportProgress;
    private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkSupportAbort;
    private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkSupportCancel;
    private System.Windows.Forms.BindingSource iBackgroundWorkerComponentBindingSource;
    private Infragistics.Win.UltraWinEditors.UltraCheckEditor chkSupportPause;
  }
}