﻿namespace MorganStanley.MSDesktop.Rambo.AsyncUtils.TestHarness
{
  partial class StaticAsyncTestForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.textBox1 = new System.Windows.Forms.TextBox();
      this._btnSimple = new System.Windows.Forms.Button();
      this._btnTwoWay = new System.Windows.Forms.Button();
      this.label1 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this._secs = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
      this._txtName = new System.Windows.Forms.TextBox();
      ((System.ComponentModel.ISupportInitialize)(this._secs)).BeginInit();
      this.SuspendLayout();
      // 
      // textBox1
      // 
      this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.textBox1.Location = new System.Drawing.Point(109, 43);
      this.textBox1.Multiline = true;
      this.textBox1.Name = "textBox1";
      this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.textBox1.Size = new System.Drawing.Size(271, 140);
      this.textBox1.TabIndex = 0;
      // 
      // _btnSimple
      // 
      this._btnSimple.Location = new System.Drawing.Point(13, 13);
      this._btnSimple.Name = "_btnSimple";
      this._btnSimple.Size = new System.Drawing.Size(75, 23);
      this._btnSimple.TabIndex = 1;
      this._btnSimple.Text = "One-Way";
      this._btnSimple.UseVisualStyleBackColor = true;
      this._btnSimple.Click += new System.EventHandler(this._btnSimple_Click);
      // 
      // _btnTwoWay
      // 
      this._btnTwoWay.Location = new System.Drawing.Point(13, 43);
      this._btnTwoWay.Name = "_btnTwoWay";
      this._btnTwoWay.Size = new System.Drawing.Size(75, 23);
      this._btnTwoWay.TabIndex = 2;
      this._btnTwoWay.Text = "Two-Way";
      this._btnTwoWay.UseVisualStyleBackColor = true;
      this._btnTwoWay.Click += new System.EventHandler(this._btnTwoWay_Click);
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(109, 13);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(38, 13);
      this.label1.TabIndex = 3;
      this.label1.Text = "Name:";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(271, 13);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(37, 13);
      this.label2.TabIndex = 4;
      this.label2.Text = "Sleep:";
      // 
      // _secs
      // 
      this._secs.Location = new System.Drawing.Point(314, 9);
      this._secs.MaskClipMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.Raw;
      this._secs.MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.Raw;
      this._secs.MaxValue = 10;
      this._secs.MinValue = 1;
      this._secs.Name = "_secs";
      this._secs.PromptChar = ' ';
      this._secs.Size = new System.Drawing.Size(55, 21);
      this._secs.SpinButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Always;
      this._secs.SpinWrap = true;
      this._secs.TabIndex = 5;
      this._secs.Value = 1;
      // 
      // _txtName
      // 
      this._txtName.Location = new System.Drawing.Point(150, 10);
      this._txtName.Name = "_txtName";
      this._txtName.Size = new System.Drawing.Size(103, 20);
      this._txtName.TabIndex = 6;
      // 
      // StaticAsyncTestForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(392, 195);
      this.Controls.Add(this._txtName);
      this.Controls.Add(this._secs);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.label1);
      this.Controls.Add(this._btnTwoWay);
      this.Controls.Add(this._btnSimple);
      this.Controls.Add(this.textBox1);
      this.Name = "StaticAsyncTestForm";
      this.Text = "StaticAsyncTestForm";
      ((System.ComponentModel.ISupportInitialize)(this._secs)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.TextBox textBox1;
    private System.Windows.Forms.Button _btnSimple;
    private System.Windows.Forms.Button _btnTwoWay;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label2;
    private Infragistics.Win.UltraWinEditors.UltraNumericEditor _secs;
    private System.Windows.Forms.TextBox _txtName;
  }
}