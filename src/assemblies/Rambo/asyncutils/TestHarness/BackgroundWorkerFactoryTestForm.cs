using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms;

using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinStatusBar;

using MorganStanley.MSDotNet.MSNet;

namespace MorganStanley.MSDesktop.Rambo.AsyncUtils.TestHarness
{
  public partial class BackgroundWorkerFactoryTestForm : Form
  {
    private static int _workerCounter = 0;
    private static int _threadPoolCounter = 0;
    private static bool _maxThreadsConfigurable = true;

    private Dictionary<string, ProgressControl> _controls = new Dictionary<string, ProgressControl>();
    private BindingList<string> _threadPoolList = new BindingList<string>();

    public BackgroundWorkerFactoryTestForm()
    {
      InitializeComponent();
    }

    private void AsyncTestForm_Load(object sender, EventArgs e)
    {
      backgroundWorkerComponentBindingSource.DataSource = backgroundWorkerFactory;

      gbThreadPool.Enabled = _maxThreadsConfigurable;

      txtWorkerName.Text = string.Format("Worker{0}", ++_workerCounter);

      ultraStatusBar1.Panels[0].Visible = false;

      txtDTPName.Text = string.Format("ThreadPool{0}", ++_threadPoolCounter);

      _threadPoolList.Add(ManagedThreadPool.NAME);
      _threadPoolList.Add(WindowsThreadPool.NAME);
      cmbThreadPool.DataSource = _threadPoolList;
      cmbThreadPool.SelectedItem = ManagedThreadPool.NAME;
    }

    private void btnCreate_Click(object sender, EventArgs e)
    {
      CreateProgressControl( );

      txtWorkerName.Text = string.Format("Worker{0}", ++_workerCounter);
    }

    private void CreateProgressControl( )
    {
      string id = txtWorkerName.Text;

      if(_controls.ContainsKey(id))
      {
        MessageBox.Show("A control with name " + id + "already exists, choose another.",
                        "Invalid Name",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Exclamation);
        return;
      }


      ProgressControl ctrl = new ProgressControl( );

      ctrl.BackgroundWorker = backgroundWorkerFactory.CreateBackgroundWorker( );
      ctrl.Argument = (int)numArgument.Value;
      ctrl.Name = id;

      panel2.SuspendLayout( );
      
      panel2.Controls.Add(ctrl);
      ctrl.Dock = DockStyle.Top;
      ctrl.BringToFront( );

      panel2.ResumeLayout();

      _controls.Add(id, ctrl);

      ctrl.Close += ctrl_Close;
    }

    void ctrl_Close(object sender, EventArgs e)
    {
      ProgressControl ctrl = (ProgressControl) sender;
      
      _controls.Remove(ctrl.Name);
      panel2.Controls.Remove(ctrl);

      ctrl.Close -= ctrl_Close;
      ctrl.Dispose();
    }

    private void backgroundWorkerFactory_DoWork(object sender, DoWorkEventArgs e)
    {
      DoWorkAgorithm.DoWork(sender as IBackgroundWorker, e);
    }

    private void DisableThreadPoolParams( )
    {
      if(InvokeRequired)
      {
        BeginInvoke(new MSNetVoidVoidDelegate(DisableThreadPoolParams));
        return;
      }

      gbThreadPool.Enabled = false;
    }

    private void backgroundWorkerFactory_ProgressChanged(object sender, ProgressChangedEventArgs e)
    {
      UpdateProgress( );
    }

    private void UpdateProgress( )
    {
      double goal = 0.0;
      double completed = 0.0;
 
      foreach (ProgressControl ctrl in _controls.Values)
      {
        if(ctrl.BackgroundWorker.IsBusy || ctrl.BackgroundWorker.PercentCompleted == 100)
        {
          goal += 1.0;
          completed += ctrl.BackgroundWorker.PercentCompleted;
        }
      }

      int value = (int)(goal > 0.0 ? Math.Round(completed / goal) : 0);
      ultraStatusBar1.Panels[0].ProgressBarInfo.Value = value;
      ultraStatusBar1.Panels[0].Visible = value > 0 && value < 100;      
    }

    private void ultraStatusBar1_ButtonClick(object sender, PanelEventArgs e)
    {
      Close( );
    }

    private void backgroundWorkerFactory_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
    {
      UpdateProgress( );
    }

    private void numThreadPool_ValueChanged(object sender, EventArgs e)
    {
      ManagedThreadPoolConfig.MaxWorkerThreads = (int)numThreadPool.Value;
    }

    private void btnCreateThreadPool_Click(object sender, EventArgs e)
    {
      string id = txtDTPName.Text;
      if(_threadPoolList.Contains(id))
      {
        MessageBox.Show("A thread pool with name " + id + "already exists, choose another.",
                        "Invalid Name",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Exclamation);
        return;
      }

      ThreadPoolManager.Instance.CreateDynamicThreadPool(id,
                                                         (int) numMinThreads.Value,
                                                         (int) numMaxThreads.Value,
                                                         (int) numKeepAlive.Value);

      _threadPoolList.Add(id);

      txtDTPName.Text = string.Format("ThreadPool{0}", ++_threadPoolCounter);
    }

    private void cmbThreadPool_SelectedIndexChanged(object sender, EventArgs e)
    {
      backgroundWorkerFactory.ThreadPool = (string)cmbThreadPool.SelectedItem;
    }

    private void numKeepAlive_EditorSpinButtonClick(object sender, SpinButtonClickEventArgs e)
    {
      int value = (int)numKeepAlive.Value;

      switch (e.ButtonType)
      {
        case SpinButtonItem.First:
          numKeepAlive.Value = numKeepAlive.MinValue;
          break;

        case SpinButtonItem.Last:
          numKeepAlive.Value = numKeepAlive.MaxValue;
          break;

        case SpinButtonItem.NextItem:
          numKeepAlive.Value = (value + 1000) > (int)numKeepAlive.MaxValue ?
            numKeepAlive.MaxValue : value + 1000;
          break;

        case SpinButtonItem.PreviousItem:
          numKeepAlive.Value = (value - 1000) < (int)numKeepAlive.MinValue ?
            numKeepAlive.MinValue : value - 1000;
          break;

        case SpinButtonItem.NextPage:
          numKeepAlive.Value = (value + 10000) > (int)numKeepAlive.MaxValue ?
            numKeepAlive.MaxValue : value + 10000;
          break;

        case SpinButtonItem.PreviousPage:
          numKeepAlive.Value = (value - 10000) < (int)numKeepAlive.MinValue ?
            numKeepAlive.MinValue : value - 10000;
          break;
      }
    }

    private void btnResetManagedPool_Click(object sender, EventArgs e)
    {
      ManagedThreadPool.Reset( );
    }
  }
}