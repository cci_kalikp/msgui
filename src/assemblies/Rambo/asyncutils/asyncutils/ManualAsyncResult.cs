using System;
using System.ComponentModel; 

namespace MorganStanley.MSDesktop.Rambo.AsyncUtils
{
	/// <summary>
	/// An IAsyncResult implementation which the Result and Exception attributes are set
	/// manually.
	/// </summary>
  public class ManualAsyncResult : AsynchronizerResult
  {
    public ManualAsyncResult(AsyncCallback callBack_, object asyncState_, ISynchronizeInvoke async_)
      :base(null, null, callBack_, asyncState_, async_)
    {
    }

    /// <summary>
    /// Sets the result's InvokeException attribute and callsback
    /// </summary>
    public void SetInvokeException(Exception ex_)
    {
      InvokeException = ex_;
      OnComplete();
    }

    /// <summary>
    /// Sets the result's MethodReturnValue attribute and callsback
    /// </summary>
    public void SetMethodReturnedValue(object val_)
    {
      MethodReturnedValue = val_;
      OnComplete();
    }
  }
}
