using System;

using MorganStanley.MSDotNet.MSNet;

namespace MorganStanley.MSDesktop.Rambo.AsyncUtils
{

  /// <summary>
  /// An implementation of <see cref="IMSNetLoop"/> used to put background work on a specified <see cref="IThreadPool"/> instance.
  /// </summary>
  internal class AsynchronizerNetLoop : MSNetLoopPoolImpl,
    IMSNetLoop
  {
    private IThreadPool _threadPool;

    /// <summary>
    /// Initializes a new instance of the <see cref="AsynchronizerNetLoop"/> class using a specified <see cref="IThreadPool"/> instance for background processes.
    /// </summary>
    /// <param name="threadPool_">The thread pool_.</param>
    public AsynchronizerNetLoop(IThreadPool threadPool_)
    {
      _threadPool = threadPool_;
    }

    #region MSNetLoopPoolImpl Overrides

    /// <summary>
    /// 	<see cref="T:MorganStanley.MSDotNet.MSNet.IMSNetLoop">IMSNetLoop</see>
    /// implementation
    /// </summary>
    /// <param name="cb_"></param>
    public override void InvokeLater(MSNetVoidVoidDelegate cb_)
    {
      _threadPool.QueueUserWorkItem(m_wakeup, new MSNetInvokeVoid(cb_));
    }

    /// <summary>
    /// 	<see cref="T:MorganStanley.MSDotNet.MSNet.IMSNetLoop">IMSNetLoop</see>
    /// implementation
    /// </summary>
    /// <param name="method_"></param>
    /// <param name="params_"></param>
    public override void InvokeLater(Delegate method_, params object[] params_)
    {
      _threadPool.QueueUserWorkItem(m_wakeup, new MSNetInvokeAny(method_, params_));
    }

    /// <summary>
    /// 	<see cref="T:MorganStanley.MSDotNet.MSNet.IMSNetLoop">IMSNetLoop</see>
    /// implementation
    /// </summary>
    /// <param name="cb_"></param>
    /// <param name="o_"></param>
    public override void InvokeLaterWithState(MSNetVoidObjectDelegate cb_, object o_)
    {
      _threadPool.QueueUserWorkItem(m_wakeup, new MSNetInvokeObject(cb_, o_));
    }
    #endregion

  }
}