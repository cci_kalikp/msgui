namespace MorganStanley.MSDesktop.Rambo.AsyncUtils
{
  /// <summary>
  /// Interface for getting or setting a specific thread pool by name.
  /// </summary>
  public interface IThreadPoolConfigurable
  {
    /// <summary>
    /// Gets or sets the thread pool by name.
    /// </summary>
    /// <value>The thread pool name.</value>
    string ThreadPool { get; set; }
  }
}