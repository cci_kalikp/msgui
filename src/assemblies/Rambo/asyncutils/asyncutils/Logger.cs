﻿using System;
using System.Diagnostics;
using MorganStanley.MSDotNet.MSLog;

namespace MorganStanley.MSDesktop.Rambo.AsyncUtils
{
  /// <summary>
  /// Simplified Logging implementation to prevent recursive reference to Ecdev.Desktop Logging
  /// </summary>
  internal class Logger
  {
    #region Fields

    private string _class;
    private MSLogLayer _layer;

    #endregion

    #region Constructors

    public Logger(Type classType)
    {
      string meta, proj;
      string rel = GetVersionString(classType);
      GetMetaProject(classType, out meta, out proj);
      Init(classType, meta, proj, rel);
    }

    #endregion

    #region Instance Methods

    internal bool CanLogPriority(MSLogPriority priority)
    {
      return MSLog.IsLoggable(_layer, priority);
    }

    private void DoLog(string methodName_, string message_, MSLogMessage logmessage_)
    {
      logmessage_.SetLayer(_layer).Location(_class, methodName_);
      logmessage_.Append(message_);
      logmessage_.Send();
    }

    /// <summary>
    /// Performs logging at the debug level
    /// </summary>
    /// <param name="methodName_">The method name where the issue happened.</param>
    /// <param name="message_">The message to be logged.</param>
    internal void LogDebug(string methodName_, string message_)
    {
      DoLog(methodName_, message_, MSLog.Debug());
    }

    private void Init(Type class_, string meta_, string proj_, string rel_)
    {
      Init(class_, new MSLogLayer(meta_, proj_, rel_));
    }

    private void Init(Type class_, MSLogLayer layer_)
    {
      _class = class_.Name;
      _layer = layer_;
    }

    #endregion

    #region Static Methods

    private static void GetMetaProject(Type class_, out string meta_, out string proj_)
    {
      String[] elements = class_.Namespace.Split(new[] { '.' }, 10);
      Debug.Assert(elements.Length > 2 && elements[1] != null && elements[2] != null);
      meta_ = elements[1].ToLower();
      proj_ = elements[2].ToLower();
    }

    private static string GetVersionString(Type claz_)
    {
      Version version = claz_.Assembly.GetName().Version;
      return string.Format("{0}.{1}.{2}.{3}", version.Major, version.Minor, version.Build, version.Revision);
    }

    #endregion
  }
}