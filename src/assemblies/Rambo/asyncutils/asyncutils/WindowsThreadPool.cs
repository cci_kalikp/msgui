using System;
using System.Collections.Generic;
using System.Xml;
using System.Threading;

namespace MorganStanley.MSDesktop.Rambo.AsyncUtils
{
	/// <summary>
	/// Wrapper class for the Windows ThreadPool class that allows it to be used interchangably with
	/// other IThreadPool implementions and as a result for Asynchronizers.
	/// </summary>
	public class WindowsThreadPool : IThreadPool
	{
	  #region Fields
	  private static readonly WindowsThreadPool _instance = new WindowsThreadPool();

	  private UnhandledExceptionEventHandler _unhandledException;

	  public const string NAME = "WINDOWS";
	  #endregion

	  #region Constructors
	  private WindowsThreadPool()
	  {
	  }
	  #endregion

    #region ISignalUnhandledException Members
    public event UnhandledExceptionEventHandler OnUnhandledException
    {
      add
      {
        _unhandledException += value;
      }
      remove
      {
        _unhandledException -= value;
      }
    }
    #endregion

	  #region IThreadPool Members
	  /// <summary>
	  /// Queues callback delegate on the ThreadPool.
	  /// </summary>
	  /// <param name="callBack_">Delegate to enqueue</param>
	  /// <returns>true if successfully enqueued</returns>
	  public bool QueueUserWorkItem(WaitCallback callBack_)
	  {
	    return QueueUserWorkItem(callBack_, null);
	  }

	  /// <summary>
	  /// Queues callback delegate on the ThreadPool.
	  /// </summary>
	  /// <param name="callBack_">Delegate to enqueue</param>
	  /// <param name="state_">state to pass to callback</param>
	  /// <returns>true if successfully enqueued</returns>
	  public bool QueueUserWorkItem(WaitCallback callBack_, object state_)
	  {
	    UserWorkItem userWorkItem = new UserWorkItem(callBack_, state_);
	    WaitCallback callBack = userWorkItem.InvokeWorkItem;
	    return ThreadPool.QueueUserWorkItem(callBack);
	  }

	  public IList<ThreadInfo> GetAllThreadsInfo()
	  {
	    return new List<ThreadInfo>();
	  }

	  /// <summary>
	  /// Enqueue method on the ThreadPool which takes that passed parameters.
	  /// </summary>
	  /// <param name="method_">Method to enqueue</param>
	  /// <param name="params_">Method Params</param>
	  public void Enqueue(Delegate method_, params object[] params_)
	  {
	    UserWorkItem userWorkItem = new UserWorkItem(method_, params_);
	    userWorkItem.OnUnhandledException += UserWorkItemOnUnhandledException;

	    WaitCallback callBack = userWorkItem.InvokeWorkItem;
	    ThreadPool.QueueUserWorkItem(callBack);
	  }


	  /// <summary>
	  /// Gets number of WorkerThreads currently in the pool
	  /// </summary>
	  public int ThreadCount
	  {
	    get
	    {
	      int workerThreads;
	      int completionPortThreads;
	      ThreadPool.GetAvailableThreads(out workerThreads, out completionPortThreads);
	      return workerThreads;
	    }
	  }

	  /// <summary>
	  /// Gets number of items waiting to be processes by the ThreadPool
	  /// </summary>
	  public int QueueSize
	  {
	    get
	    {
	      return 0; //Not Supported by windows ThreadPool
	    }
	  }

	  /// <summary>
	  /// Gets the name of the ThreadPool (Will always return WINDOWS)
	  /// </summary>
	  public string Name
	  {
	    get
	    {
	      return NAME;
	    }
    }
    #endregion

	  #region Public Methods
	  /// <summary>
	  /// Returns the WindowsThreadPool instance
	  /// </summary>
	  public static WindowsThreadPool Instance
	  {
	    get
	    {
	      return _instance;
	    }
	  }

	  /// <summary>
	  /// Configures the Minimum worker Threads and the minimum IO Completion Threads
	  /// for ThreadPool.
	  /// expects:
	  /// <code>
	  ///   <WindowsThreadPool>
	  ///     <MinThreads>2</MinThreads>
	  ///     <MinIOCompletionThreads>2</MinIOCompletionThreads>
	  ///   </WindowsThreadPool>
	  /// </code>
	  /// with the 2 params optional.
	  /// </summary>
	  /// <param name="config_">Config XML</param>
	  public static void Configure(XmlElement config_)
	  {
	    int minIOC;
	    int minWorker;
	    ThreadPool.GetMinThreads(out minWorker, out minIOC);
	    XmlNode node = config_.SelectSingleNode("MinThreads");
	    if (node != null)
	    {
	      int num = int.Parse(node.InnerText);
	      if (num >= 0)
	      {
	        minWorker = num;
	      }
	      else
	      {
	        throw new ArgumentException(num+" is not a valid minimum number of threads");
	      }
	    }
	    node = config_.SelectSingleNode("MinIOCompletionThreads");
	    if (node != null)
	    {
	      int num = int.Parse(node.InnerText);
	      if (num >= 0)
	      {
	        minIOC = num;
	      }
	      else
	      {
	        throw new ArgumentException(num+" is not a valid minimum number of IOC threads");
	      }
	    }
	    ThreadPool.SetMinThreads(minWorker, minIOC);
	  }
	  #endregion

	  #region Protected Methods
    /// <summary>
    /// Fires the unhandled exception event.
    /// </summary>
    /// <param name="e_">The <see cref="System.UnhandledExceptionEventArgs"/> instance containing the event data.</param>
	  protected void FireOnUnhandledException(UnhandledExceptionEventArgs e_)
	  {
	    if(_unhandledException != null)
	    {
	      ThreadingUtils.ThreadSafeBeginInvoke(_unhandledException, this, e_);
	    }
	  }
	  #endregion

	  #region Private Methods
	  private void UserWorkItemOnUnhandledException(object sender, UnhandledExceptionEventArgs e)
	  {
	    FireOnUnhandledException(e);
	  }
	  #endregion
	}
}
