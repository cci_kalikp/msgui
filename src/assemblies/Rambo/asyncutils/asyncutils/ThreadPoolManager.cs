using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Xml;

using MorganStanley.MSDotNet.MSNet;

namespace MorganStanley.MSDesktop.Rambo.AsyncUtils
{
  /// <summary>
  /// Central point for all ThreadPools. Thread pools can either be created with the
  /// CreateThreadPool() methods or configured at startup from a config file via the
  /// ConfigureThreadPools() method.
  /// This class can also be used to access the Windows ThreadPool and the old style
  /// ManagedThread pool by calling
  ///      ThreadPoolManager.Instance.GetThreadPool("WINDOWS");
  /// and
  ///      ThreadPoolManager.Instance.GetThreadPool("MANAGED");
  /// The default ThreadPool type created by CreateThreadPool if you do not pass it a
  /// Type will be a DynamicThreadPool using the default min, max and keep alive
  /// variables for that type.
  /// </summary>
  public class ThreadPoolManager : ISignalUnhandledException
  {
    #region Fields
    private readonly static ThreadPoolManager _instance = new ThreadPoolManager();
    
    private readonly object _syncRoot = new object();
    private readonly Dictionary<string, IThreadPool> _threadPools = new Dictionary<string, IThreadPool>();
    private readonly Dictionary<string, IMSNetLoop> _netLoops = new Dictionary<string, IMSNetLoop>();

    private UnhandledExceptionEventHandler _unhandledException;
    #endregion

    #region Constructor
    private ThreadPoolManager()
    {
      ManagedThreadPool.Instance.OnUnhandledException += IThreadPoolUnhandledException;
      WindowsThreadPool.Instance.OnUnhandledException += IThreadPoolUnhandledException;

      AddThreadPool(ManagedThreadPool.NAME, ManagedThreadPool.Instance);
      AddThreadPool(WindowsThreadPool.NAME, WindowsThreadPool.Instance);

    }
    #endregion

    #region Public Methods
    /// <summary>
    /// Configures a selection of ThreadPools including the MANAGED and WINDOWS pools
    /// if they have config elements in the passed XML.
    /// The format for the config_ is
    /// <code>
    /// <ThreadPoolConfig>
    ///   <!-- Optional config for ManagedThreadPool -->
    ///   <ManagedThreadPool>
    ///     <NumberOfThreads>5</NumberOfThreads>
    ///   </ManagedThreadPool>
    ///   <WindowsThreadPool>
    ///     <MinThreads>2</MinThreads>
    ///     <MinIOCompletionThreads>2</MinIOCompletionThreads>
    ///   </WindowsThreadPool>
    ///   <ThreadPool name="" type="">
    ///     <!-- Thread pool implementation specific settings -->
    ///   </ThreadPool>
    ///   <!-- e.g. for a DynamicThreadPool -->
    ///   <ThreadPool name="ds" type="MorganStanley.MSDesktop.Rambo.AsyncUtils.DynamicThreadPool">
    ///     <MinThreads>1</MinThreads>
    ///     <MaxThreads>5</MaxThreads>
    ///     <KeepAlive>10000</KeepAlive>
    ///   </ThreadPool>
    /// </ThreadPoolConfig>
    /// </code>
    /// </summary>
    /// <param name="config_"></param>
    public void ConfigureThreadPools(XmlElement config_)
    {
      try
      {
        foreach (XmlNode node in config_.ChildNodes)
        {
          if (node is XmlElement)
          {
            XmlElement poolElement = node as XmlElement;
            if (poolElement.Name == "ManagedThreadPool")
            {
              ManagedThreadPool.Configure(poolElement);
            }
            else if (poolElement.Name == "WindowsThreadPool")
            {
              WindowsThreadPool.Configure(poolElement);
            }
            else if (poolElement.Name == "ThreadPool")
            {
              string name = poolElement.GetAttribute("name");
              string type = poolElement.GetAttribute("type");
              CreateThreadPool(name, type, poolElement);
            }
          }
        }
      }
      catch (Exception ex_)
      {
        throw new XmlException("Invalid ThreadPools config: ", ex_);
      }
    }

    /// <summary>
    /// Returns  all thread pools managed by the manager keyed by a name
    /// </summary>
    /// <returns></returns>
    public IDictionary<string, IThreadPool> GetAllThreadPools()
    {
      IDictionary<string, IThreadPool> threadPoolDictionary;
      lock (_syncRoot)
      {
        threadPoolDictionary = new Dictionary<string, IThreadPool>(_threadPools);
      }

      return threadPoolDictionary;
    }

    /// <summary>
    /// Creates a DynamicThreadPool with the name passed using the default
    /// parameters for a DynamicThreadPool.
    /// </summary>
    /// <param name="name_">Name of the ThreadPool</param>
    /// <returns>the created pool</returns>
    public IThreadPool CreateThreadPool(string name_)
    {
      return CreateThreadPool(name_, typeof(DynamicThreadPool));
    }

    /// <summary>
    /// Creates a ThreadPool of type type_ using it's default constructor.
    /// If no implementation of type_ is found the an ArgumentException is
    /// thrown
    /// </summary>
    /// <param name="name_">Name of the ThreadPool</param>
    /// <returns>the created pool</returns>
    public IThreadPool CreateThreadPool(string name_, string type_)
    {
      Type t = Type.GetType(type_);
      if (t == null)
      {
        throw new ArgumentException("ThreadPool type " + type_ + " was not found");
      }
      return CreateThreadPool(name_, t);
    }

    /// <summary>
    /// Creates a ThreadPool of type type_ using it's default constructor.
    /// </summary>
    /// <param name="name_">Name of the ThreadPool</param>
    /// <param name="type_">The type of the pool to create</param>
    /// <returns>the created pool</returns>
    public IThreadPool CreateThreadPool(string name_, Type type_)
    {
      lock (_syncRoot)
      {
        CheckExists(name_);
        IThreadPool pool = Activator.CreateInstance(type_, BindingFlags.NonPublic | BindingFlags.Instance, null, new object[] { name_ }, null) as IThreadPool;
        AddThreadPool(name_, pool);
        return pool;
      }
    }

    /// <summary>
    /// Creates a ThreadPool of type type_ using the passed XML config.
    /// If no implementation of type_ is found the an ArgumentException is
    /// thrown
    /// </summary>
    /// <param name="name_">Name of the ThreadPool</param>
    /// <param name="type_">The Type name of the pool to create</param>
    /// <param name="config_">the config to use</param>
    /// <returns>the created pool</returns>
    public IThreadPool CreateThreadPool(string name_, string type_, XmlElement config_)
    {
      Type t = Type.GetType(type_);
      if (t == null)
      {
        throw new ArgumentException("ThreadPool type " + type_ + " was not found");
      }
      return CreateThreadPool(name_, t, config_);
    }

    /// <summary>
    /// Creates a ThreadPool of type type_ using the passed XML config.
    /// </summary>
    /// <param name="name_">Name of the ThreadPool</param>
    /// <param name="type_">The Type of the pool to create</param>
    /// <param name="config_">the config to use</param>
    /// <returns>the created pool</returns>
    public IThreadPool CreateThreadPool(string name_, Type type_, XmlElement config_)
    {
      lock (_syncRoot)
      {
        CheckExists(name_);
        IThreadPool pool = Activator.CreateInstance(type_, BindingFlags.NonPublic | BindingFlags.Instance, null, new object[] { name_, config_ }, null) as IThreadPool;
        AddThreadPool(name_, pool);
        return pool;
      }
    }

    /// <summary>
    /// Creates a ThreadPool of type type_ using the passed parameters which must match the
    /// parameters for the ThreadPool's constructor minus the ThreadPool name.
    /// If no implementation of type_ is found the an ArgumentException is
    /// thrown
    /// </summary>
    /// <param name="name_">Name of the ThreadPool</param>
    /// <param name="type_">The Type name of the pool to create</param>
    /// <param name="params_">object array of the constructor params minus the name</param>
    /// <returns>the created pool</returns>
    public IThreadPool CreateThreadPool(string name_, string type_, object[] params_)
    {
      Type t = Type.GetType(type_);
      if (t == null)
      {
        throw new ArgumentException("ThreadPool type " + type_ + " was not found");
      }
      return CreateThreadPool(name_, t);
    }
    /// <summary>
    /// Creates a ThreadPool of type type_ using the passed parameters which must match the
    /// parameters for the ThreadPool's constructor minus the ThreadPool name.
    /// </summary>
    /// <param name="name_">Name of the ThreadPool</param>
    /// <param name="type_">The Type of the pool to create</param>
    /// <param name="params_">object array of the constructor params minus the name</param>
    /// <returns>the created pool</returns>
    public IThreadPool CreateThreadPool(string name_, Type type_, object[] params_)
    {
      lock (_syncRoot)
      {
        CheckExists(name_);
        object[] prms = new Object[params_.Length + 1];
        prms[0] = name_;
        params_.CopyTo(prms, 1);
        IThreadPool pool = Activator.CreateInstance(type_, BindingFlags.NonPublic | BindingFlags.Instance, null, prms, null) as IThreadPool;
        AddThreadPool(name_, pool);
        return pool;
      }
    }

    /// Creates a DynamicThreadPool with the name passed min and max thread parameters.
    /// </summary>
    /// <param name="name_">Name of the ThreadPool</param>
    /// <param name="min_">minimum no of threads</param>
    /// <param name="max_">maximum no of threads</param>
    /// <returns>the created pool</returns>
    public IThreadPool CreateDynamicThreadPool(string name_, int min_, int max_)
    {
      lock (_syncRoot)
      {
        CheckExists(name_);
        IThreadPool pool = new DynamicThreadPool(name_, min_, max_);
        AddThreadPool(name_, pool);
        return pool;
      }
    }

    /// Creates a DynamicThreadPool with the name passed min threads, max threads and keep alive parameters.
    /// </summary>
    /// <param name="name_">Name of the ThreadPool</param>
    /// <param name="min_">minimum no of threads</param>
    /// <param name="max_">maximum no of threads</param>
    /// <param name="keepAlive_">number of milliseconds idle threads will stay alive for before dying</param>
    /// <returns>the created pool</returns>
    public IThreadPool CreateDynamicThreadPool(string name_, int min_, int max_, int keepAlive_)
    {
      lock (_syncRoot)
      {
        CheckExists(name_);
        IThreadPool pool = new DynamicThreadPool(name_, min_, max_, keepAlive_);
        AddThreadPool(name_, pool);
        return pool;
      }
    }

    /// <summary>
    /// Returns the ThreadPool with Name=name_ if it exists and throws an ArguementException if it doesn't.
    /// The existence of a pool can be checked first with ThreadPoolExists() if needed.
    /// </summary>
    /// <param name="name_">Name of the thread pool</param>
    /// <returns>The requested ThreadPool</returns>
    /// <exception cref="ArgumentException"> Thrown if a IThreadPool does not  exist with the specified name</exception>
    public IThreadPool GetThreadPool(string name_)
    {
      lock (_syncRoot)
      {
        if (!_threadPools.ContainsKey(name_))
        {
          throw new ArgumentException(string.Format("Thread pool '{0}' has not yet been created", name_));
        }
        else
        {
          return _threadPools[name_];
        }
      }
    }

    /// <summary>
    /// Returns the <see cref="IMSNetLoop"/> that uses the <see cref="IThreadPool"/> with the specified name
    /// for its asynchronous calls. 
    /// </summary>
    /// <remarks>
    /// The existence of a pool can be checked first with ThreadPoolExists() if needed.
    /// </remarks>
    /// <param name="name_">Name of the thread pool</param>
    /// <returns>The requested IMSNetLoop</returns>
    /// <exception cref="ArgumentException"> Thrown if a IThreadPool does not  exist with the specified name</exception>
    public IMSNetLoop GetNetLoop(string name_)
    {
      lock (_syncRoot)
      {
        if (!_netLoops.ContainsKey(name_))
        {
          throw new ArgumentException(string.Format("Thread pool '{0}' has not yet been created", name_));
        }
        else
        {
          return _netLoops[name_];
        }
      }
    }

    /// <summary>
    /// Returns the ThreadPool with Name=name_ if it exists and throws an ArguementException if it doesn't.
    /// The existence of a pool can be checked first with ThreadPoolExists() if needed.
    /// </summary>
    /// <param name="name_">Name of the thread pool</param>
    /// <returns>The requested ThreadPool</returns>
    public IThreadPool this[string name_]
    {
      get
      {
        return GetThreadPool(name_);
      }
    }

    /// <summary>
    /// Returns true if there exists a ThreadPool with Name=name_.
    /// </summary>
    /// <param name="name_">The name of the ThreadPool</param>
    /// <returns>true if exists</returns>
    public bool ThreadPoolExists(string name_)
    {
      lock (_syncRoot)
      {
        return _threadPools.ContainsKey(name_);
      }
    }

    /// <summary>
    /// Returns the singleton instance of the ThreadPoolManager
    /// </summary>
    public static ThreadPoolManager Instance
    {
      get
      {
        return _instance;
      }
    }
    #endregion

    #region Private Methods

    private void CheckExists(string name_)
    {
      if(ThreadPoolExists(name_))
      {
        throw new ArgumentException(string.Format("A thread pool by this name {0} already exists", name_), "name_");
      }
    }

    private void AddThreadPool(string name_, IThreadPool pool_)
    {
      lock(_syncRoot)
      {
        _threadPools.Add(name_, pool_);
        _netLoops.Add(name_, new AsynchronizerNetLoop(pool_));
        pool_.OnUnhandledException += IThreadPoolUnhandledException;
      }
    }
    #endregion

    #region ISignalUnhandledException Members
    public event UnhandledExceptionEventHandler OnUnhandledException
    {
      add
      {
        _unhandledException += value;
      }
      remove
      {
        _unhandledException -= value;
      }
    }

    protected void FireOnUnhandledException(UnhandledExceptionEventArgs e_)
    {
      if(_unhandledException !=null)
      {
        ThreadingUtils.ThreadSafeInvoke(_unhandledException, this, e_);
      }
    }

    

    private void IThreadPoolUnhandledException(object sender_, UnhandledExceptionEventArgs e_)
    {
      FireOnUnhandledException(e_);
    }

    /// <summary>
    /// Composes a string with the information about all threads  and thread pools
    /// managed by this manager such as call stacks, thread details, pool
    /// note that this operation suspends all threads managed by this manager and then resumes them
    ///  in order to obtain call stack information 
    /// </summary>
    /// <returns></returns>
    public string GetAllThreadsStatsXml()
    {
      var strBuilder=new StringBuilder();
      var writer = new XmlTextWriter(new StringWriter(strBuilder));
      IDictionary<string, IThreadPool> threadPools = GetAllThreadPools();

      writer.Formatting = Formatting.Indented;

      writer.WriteStartElement("threadPoolList");
      foreach (KeyValuePair<string, IThreadPool> pair in threadPools)
      {
        writer.WriteStartElement("threadPool");
        IList<ThreadInfo> infos = pair.Value.GetAllThreadsInfo();
        writer.WriteAttributeString("name",pair.Value.Name);
        writer.WriteAttributeString("numberOfThreads",pair.Value.ThreadCount.ToString());
        writer.WriteAttributeString("workSizeQueue",pair.Value.QueueSize.ToString());

        foreach(ThreadInfo info in infos)
        {
          writer.WriteStartElement("thread");
          writer.WriteAttributeString("name", info.Name);
          writer.WriteAttributeString("isAlive", info.IsAlive.ToString());
          writer.WriteAttributeString("isBackground", info.IsBackground.ToString());
          writer.WriteAttributeString("isIdle", info.IsIdle.ToString());
          writer.WriteAttributeString("isThreadPoolThread", info.IsThreadPoolThread.ToString());
          writer.WriteAttributeString("priority", info.Priority.ToString());
          writer.WriteAttributeString("threadState", info.ThreadState.ToString());
          writer.WriteValue(info.StackTrace.ToString());
          writer.WriteEndElement();
        }
        writer.WriteEndElement();
      }
      writer.WriteEndElement();
      writer.Flush();
      return strBuilder.ToString();
    }

    #endregion
  }
}
