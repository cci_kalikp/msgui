using System.Diagnostics;
using System.Text;
using System.Threading;
using ThreadState=System.Threading.ThreadState;

namespace MorganStanley.MSDesktop.Rambo.AsyncUtils
{
  /// <summary>
  /// This is a wrapper class that is used to conway basic thread information
  /// </summary>
  public class ThreadInfo
  {
    #region Readonly & Static Fields

    private readonly bool _isAlive;
    private readonly bool _isBackground;
    private readonly bool _isIdle;
    private readonly bool _isThreadPoolThread;

    private readonly string _name;

    private readonly ThreadPriority _priority;
    private readonly StackTrace _stackTrace;
    private readonly ThreadState _threadState;

    #endregion

    #region Constructors

    /// <summary>
    /// This is the constructor Note that this will suspend and 
    /// then resume a thread to get stack information
    /// </summary>
    /// <param name="thread_"></param>
    public ThreadInfo(Thread thread_)
      : this(thread_, false)
    {
    }

    /// <summary>
    /// This is the constructor Note that this will suspend and 
    /// then resume a thread to get stack information
    /// </summary>
    /// <param name="thread_"> Thread to collect information about</param>
    /// <param name="isIdle_"></param>
    public ThreadInfo(Thread thread_, bool isIdle_)
    {
      thread_.Suspend();

      try
      {
        _stackTrace = new StackTrace(thread_, true);
      }
      finally
      {
        thread_.Resume();
      }

      _name = thread_.Name;
      _threadState = thread_.ThreadState;
      _priority = thread_.Priority;
      _isThreadPoolThread = thread_.IsThreadPoolThread;
      _isAlive = thread_.IsAlive;
      _isBackground = thread_.IsBackground;
      _isIdle = isIdle_;
    }

    #endregion

    #region Instance Properties

    public bool IsAlive
    {
      get { return _isAlive; }
    }

    public bool IsBackground
    {
      get { return _isBackground; }
    }

    public bool IsIdle
    {
      get { return _isIdle; }
    }

    public bool IsThreadPoolThread
    {
      get { return _isThreadPoolThread; }
    }

    public string Name
    {
      get { return _name; }
    }

    public ThreadPriority Priority
    {
      get { return _priority; }
    }

    public StackTrace StackTrace
    {
      get { return _stackTrace; }
    }

    public ThreadState ThreadState
    {
      get { return _threadState; }
    }

    #endregion

    #region Instance Methods

    public override string ToString()
    {
      var stringBuilder = new StringBuilder();

      stringBuilder.AppendLine("\n**********************");
      stringBuilder.AppendFormat("\nName: {0}", Name);
      stringBuilder.AppendFormat("\nIsIdle: {0}", IsIdle);
      stringBuilder.AppendFormat("\nThreadState: {0}", ThreadState);
      stringBuilder.AppendFormat("\nPriority: {0}", Priority);
      stringBuilder.AppendFormat("\nIsThreadPoolThread: {0}", IsThreadPoolThread);
      stringBuilder.AppendFormat("\nIsAlive: {0}", IsAlive);
      stringBuilder.AppendFormat("\nIsBackground: {0}", IsBackground);
      stringBuilder.AppendFormat("\nStackTrace: {0}", StackTrace);
      stringBuilder.AppendLine("\n**********************\n");

      return stringBuilder.ToString();
    }

    #endregion
  }
}