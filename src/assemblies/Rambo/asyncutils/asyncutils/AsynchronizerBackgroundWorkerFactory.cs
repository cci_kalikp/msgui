using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace MorganStanley.MSDesktop.Rambo.AsyncUtils
{
  /// <summary>
  /// This factory will create an object of type <see cref="IBackgroundWorker"/>.
  /// </summary>
  /// <remarks>
  /// You can create the <b>AsynchronizerBackgroundWorkerFactory</b> programmatically or you can drag it onto your form from the Components tab of the Toolbox.  The values of the <see cref="IBackgroundWorkerComponent"/> and  <see cref="IThreadPoolConfigurable"/> properties and events will be copied to the new <see cref="IBackgroundWorker"/> instance created by the factory. 
  /// </remarks>
  public class AsynchronizerBackgroundWorkerFactory : Component,
    IBackgroundWorkerFactory,
    IThreadPoolConfigurable
  {
    #region Data
    private ProgressChangedEventHandler _ProgressChanged;
    private RunWorkerCompletedEventHandler _RunWorkerCompleted;
    private DoWorkEventHandler _DoWork;

    private bool _workerReportsProgress;
    private bool _workerSupportsCancellation;
    private bool _workerSupportsAbort;
    private bool _workerSupportsPause;

    private string _threadPool;
    
    private readonly List<IBackgroundWorker> _workers = new List<IBackgroundWorker>();
    #endregion

    #region Constructor

    /// <summary>
    /// Initializes a new instance of the <see cref="AsynchronizerBackgroundWorkerFactory"/> class.
    /// </summary>
    public AsynchronizerBackgroundWorkerFactory()
    {
      
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="AsynchronizerBackgroundWorkerFactory"/> class using
    /// a specified <see cref="IThreadPool"/>.
    /// </summary>
    /// <param name="threadPool_">The thread pool.</param>
    public AsynchronizerBackgroundWorkerFactory(string threadPool_)
    {
      _threadPool = threadPool_;
    }
    #endregion

    #region IThreadPoolConfigurable Members
    /// <summary>
    /// Gets or sets the thread pool by name.
    /// </summary>
    /// <value>The thread pool name.</value>
    [Browsable(true)]
    public string ThreadPool
    {
      get { return _threadPool; }
      set { _threadPool = value; }
    }
    #endregion

    #region IBackgroundWorkerComponent Members
    /// <summary>
    /// Occurs when ReportProgress is called.
    /// </summary>
    public event ProgressChangedEventHandler ProgressChanged
    {
      add
      {
        _ProgressChanged += value;
      }
      remove
      {
        _ProgressChanged -= value;
      }
    }

    /// <summary>
    /// Occurs when the background operation has completed, has been canceled, or has raised an exception.
    /// </summary>
    public event RunWorkerCompletedEventHandler RunWorkerCompleted
    {
      add
      {
        _RunWorkerCompleted += value;
      }
      remove
      {
        _RunWorkerCompleted -= value;
      }
    }

    /// <summary>
    /// Occurs when RunWorkerAsync is called.
    /// </summary>
    public event DoWorkEventHandler DoWork
    {
      add
      {
        _DoWork += value;
      }
      remove
      {
        _DoWork -= value;
      }
    }

    /// <summary>
    /// Gets or sets a value indicating whether the BackgroundWorker can report progress updates.
    /// </summary>
    [Browsable(true)]
    public bool WorkerReportsProgress
    {
      get { return _workerReportsProgress; }
      set { _workerReportsProgress = value; }
    }

    /// <summary>
    /// Gets or sets a value indicating whether the BackgroundWorker supports asynchronous cancellation.
    /// </summary>
    [Browsable(true)]
    public bool WorkerSupportsCancellation
    {
      get { return _workerSupportsCancellation; }
      set { _workerSupportsCancellation = value; }
    }

    /// <summary>
    /// Gets or sets a value indicating whether the BackgroundWorker supports aborting the thread.
    /// </summary>
    [Browsable(true)]
    public bool WorkerSupportsAbort
    {
      get { return _workerSupportsAbort; }
      set { _workerSupportsAbort = value; }
    }

    /// <summary>
    /// Gets or sets a value indicating whether the BackgroundWorker supports pausing an asynchronous operation.
    /// </summary>
    [Browsable(true)]
    public bool WorkerSupportsPause
    {
      get { return _workerSupportsPause; }
      set { _workerSupportsPause = value; }
    }


    #endregion

    #region IBackgroundWorkerFactory Members
    /// <summary>
    /// Creates an IBackgroundWorker
    /// </summary>
    /// <returns> IBackgroundWorker </returns>
    public IBackgroundWorker CreateBackgroundWorker()
    {
      IBackgroundWorker backgroundWorker = new AsynchronizerBackgroundWorker(_threadPool);

      Delegate[] doWorkList = _DoWork == null ? null:  _DoWork.GetInvocationList( );
      Delegate[] progressList = _ProgressChanged == null ? null :  _ProgressChanged.GetInvocationList( );
      Delegate[] completedList = _RunWorkerCompleted == null ? null : _RunWorkerCompleted.GetInvocationList( );

      if (doWorkList != null)
      {
        foreach (DoWorkEventHandler doWork in doWorkList)
        {
          backgroundWorker.DoWork += doWork;
        }
      }

      if(progressList != null)
      {
        foreach (ProgressChangedEventHandler progress in progressList)
        {
          backgroundWorker.ProgressChanged += progress;
        }
      }

      if(completedList != null)
      {
        foreach (RunWorkerCompletedEventHandler completed in completedList)
        {
          backgroundWorker.RunWorkerCompleted += completed;
        }
      }

      backgroundWorker.WorkerReportsProgress = WorkerReportsProgress;
      backgroundWorker.WorkerSupportsCancellation = WorkerSupportsCancellation;
      backgroundWorker.WorkerSupportsAbort = WorkerSupportsAbort;
      backgroundWorker.WorkerSupportsPause = WorkerSupportsPause;

      _workers.Add(backgroundWorker);

      return backgroundWorker;
    }

    #endregion

    #region protected methods
    /// <summary>
    /// Releases the unmanaged resources used by the <see cref="T:System.ComponentModel.Component"></see> and optionally releases the managed resources.
    /// </summary>
    /// <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing)
      {
        foreach (IBackgroundWorker worker in _workers)
        {
          worker.Dispose( );
        }
      }

      base.Dispose(disposing);
    }
    #endregion

  }
}
