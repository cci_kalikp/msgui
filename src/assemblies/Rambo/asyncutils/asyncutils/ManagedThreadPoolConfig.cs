namespace MorganStanley.MSDesktop.Rambo.AsyncUtils
{
  /// <summary>
  /// Configuration class for the managed thread pool.
  /// Set the static configuration variables in this class before referencing the thread
  /// pool.
  /// It's fields are used by the ManagedThreadPool's static constructor
  /// </summary>
  public static class ManagedThreadPoolConfig
  {
    /// <summary>
    /// Number of worker threads The managed thread pool will use when it is created
    /// defaults to 5
    /// </summary>
    private static int _maxWorkerThreads = 5;

    /// <summary>
    /// The number of worker thread the Managed thread pool will use when it is started.
    /// This must be set before the thread pool class is referenced defaults to 5
    /// </summary>
    public static int MaxWorkerThreads
    {
      get { return _maxWorkerThreads; }
      set { _maxWorkerThreads = value; }
    }
  }
}