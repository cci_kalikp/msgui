using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using Threading = System.Threading;

namespace MorganStanley.MSDesktop.Rambo.AsyncUtils
{
  /// <summary>
  /// A simple Queue to be uses in a typical producer/consumer synchronization pattern, 
  /// uses a semaphore to block on dequeues when the queue is empty.
  /// </summary>
  /// <typeparam name="T">The type of item to be contained</typeparam>
  /// <remarks>
  /// http://blogs.msdn.com/toub/archive/2006/04/12/blocking-queues.aspx
  /// </remarks>
  internal class BlockingQueue<T> :
    IEnumerable<T>,
    IDisposable
  {
    #region Readonly & Static Fields

    private readonly Queue<T> _queue;
    private readonly object _syncRoot;

    #endregion

    #region Fields

    private Threading.Semaphore _semaphore;

    #endregion

    #region Constructors

    /// <summary>
    /// Initializes a new instance of the <see cref="BlockingQueue&lt;T&gt;"/> class.
    /// </summary>
    public BlockingQueue()
      : this(new object())
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="BlockingQueue&lt;T&gt;"/> class using the specified
    /// object for synchronization.
    /// </summary>
    /// <param name="syncRoot_">The sync root.</param>
    public BlockingQueue(object syncRoot_)
    {
      _syncRoot = syncRoot_;
      _queue = new Queue<T>();
      _semaphore = new Threading.Semaphore(0, int.MaxValue);
    }

    #endregion

    #region Instance Properties

    public int Count
    {
      get
      {
        lock (_syncRoot)
        {
          return _queue.Count;
        }
      }
    }

    #endregion

    #region Instance Methods

    /// <summary>
    /// Clears all items from the queue.
    /// </summary>
    public void Clear()
    {
      lock (_syncRoot)
      {
        T item;
        do
        {
          _semaphore.Release();
        } while (TryDequeue(0, out item));
      }
    }

    /// <summary>
    /// Removes and returns the object at the beginning of the Queue.  if the queue is empty, the 
    /// current thread blocks until there is an item to dequeue.
    /// </summary>
    /// <returns>The dequeued item</returns>
    public T Dequeue()
    {
      T item;
      TryDequeue(Timeout.Infinite, out item);
      return item;
    }

    /// <summary>
    /// Enqueues the specified item.
    /// </summary>
    /// <param name="item_">The item.</param>
    /// <exception cref="ArgumentNullException">When item is reference type and null</exception>
    public void Enqueue(T item_)
    {
      if (!typeof (T).IsValueType && item_ == null)
      {
        throw new ArgumentNullException("item_");
      }

      lock (_syncRoot)
      {
        _queue.Enqueue(item_);
      }

      _semaphore.Release();
    }

    /// <summary>
    /// Removes and returns the object at the beginning of the Queue if there is something to remove
    /// within the timeout specified.
    /// </summary>
    /// <param name="millisecondsTimeout_">The timeout period in milliseconds.</param>
    /// <param name="item_">The item dequeued.</param>
    /// <returns>true if an item was sucessfully dequeued within the timeout period.</returns>
    public bool TryDequeue(int millisecondsTimeout_, out T item_)
    {
      if (_semaphore.WaitOne(millisecondsTimeout_, false))
      {
        lock (_syncRoot)
        {
          if (_queue.Count > 0)
          {
            item_ = _queue.Dequeue();
            return true;
          }
        }
      }

      item_ = default(T);
      return false;
    }

    protected virtual void Dispose(bool disposing_)
    {
      if (disposing_)
      {
        if (_semaphore != null)
        {
          _semaphore.Close();
        }
      }

      _semaphore = null;
    }

    #endregion

    #region IDisposable Members

    void IDisposable.Dispose()
    {
      Dispose(true);
    }

    #endregion

    #region IEnumerable<T> Members

    ///<summary>
    ///Returns an enumerator that iterates through the collection.
    ///</summary>
    ///
    ///<returns>
    ///A <see cref="T:System.Collections.Generic.IEnumerator`1"></see> that can be used to iterate through the collection.
    ///</returns>
    ///<filterpriority>1</filterpriority>
    IEnumerator<T> IEnumerable<T>.GetEnumerator()
    {
      return _queue.GetEnumerator();
    }

    ///<summary>
    ///Returns an enumerator that iterates through a collection.
    ///</summary>
    ///
    ///<returns>
    ///An <see cref="T:System.Collections.IEnumerator"></see> object that can be used to iterate through the collection.
    ///</returns>
    ///<filterpriority>2</filterpriority>
    IEnumerator IEnumerable.GetEnumerator()
    {
      return ((IEnumerable<T>) this).GetEnumerator();
    }

    #endregion
  }
}