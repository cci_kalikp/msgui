namespace MorganStanley.MSDesktop.Rambo.AsyncUtils
{
  /// <summary>
  /// An interface for executing an operation on a background thread.
  /// </summary>
  /// <remarks>
  /// Extended from <see cref="System.ComponentModel.BackgroundWorker"/>.
  /// </remarks>
  public interface IBackgroundWorker : IBackgroundWorkerComponent
  {
    /// <summary>
    /// Starts execution of a background operation with argument
    /// </summary>
    void RunWorkerAsync(object argument_);

    /// <summary>
    /// Starts execution of a background operation.
    /// </summary>
    void RunWorkerAsync();

    /// <summary>
    /// Raises the ProgressChanged event.
    /// </summary>
    /// <param name="percent_"></param>
    void ReportProgress(int percent_);

    /// <summary>
    /// Raises the ProgressChanged event.
    /// </summary>
    /// <param name="percent_">The percent to be passed.</param>
    /// <param name="userState">Current state of the background worker.</param>
    void ReportProgress(int percent_, object userState);

    /// <summary>
    /// Requests cancellation of a pending background operation.
    /// </summary>
    void CancelAsync();

    /// <summary>
    /// Requests a pause of a pending background operation.
    /// </summary>
    void PauseAsync( );

    /// <summary>
    /// Requests a restart of paused background operation.
    /// </summary>
    void RestartAsync( );

    /// <summary>
    /// Forcefully aborts BackgroundWorker by calling Thread.Abort -- can be unsafe
    /// </summary>
    void AbortAsync();

    /// <summary>
    /// Gets a value indicating whether the application has requested cancellation of a background operation.
    /// </summary>
    bool CancellationPending { get;}

    /// <summary>
    /// Gets a value indicating whether the application has requested an abort of a background operation.
    /// </summary>
    bool AbortPending { get; }

    /// <summary>
    /// Gets a value indicating whether the BackgroundWorker is running an asynchronous operation.
    /// </summary>
    bool IsBusy { get;}

    /// <summary>
    /// Gets a value indicating whether the BackgroundWorker is paused.
    /// </summary>
    bool IsPaused { get; }

    /// <summary>
    /// Gets a value of percent of operation that was completed
    /// </summary>
    int PercentCompleted { get;}
  }
}
