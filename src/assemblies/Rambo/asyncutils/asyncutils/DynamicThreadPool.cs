using System;
using System.Collections.Generic;
using System.Xml;
using System.Text;
using System.Threading;

namespace MorganStanley.MSDesktop.Rambo.AsyncUtils
{
  /// <summary>
  /// <see cref="IThreadPool"/> implementation which will dynamically grow and shrink depending on it's
  /// usage.
  /// The three parameters which govern this behavior are:
  /// MinThreads - The minimum number of Threads the pool can shrink to
  /// MaxThreads - The maximum number of Threads the pool can grow to
  /// KeepAlive - The number of milliseconds after a Thread has become idle until it
  /// dies (unless it is used).
  /// 
  /// The ThreadPool will grow if there are:
  /// a) No idle threads
  /// b) The current threads &lt; Max
  /// </summary>
  public class DynamicThreadPool :
    IThreadPool
  {
    #region Constants

    private const int DEFAULT_KEEPALIVE = 30000;
    private const int DEFAULT_MAXTHREADS = 5;
    private const int DEFAULT_MINTHREADS = 0;

    #endregion

    #region Readonly & Static Fields

    private readonly Dictionary<string, Processor> _idleThreads;

    private readonly int _keepAlive;
    private readonly string _name;

    private readonly BlockingQueue<UserWorkItem> _queue;
    private readonly object _syncRoot;
    private readonly Dictionary<string, Processor> _threads;

    #endregion

    #region Fields

    private int _maxThreads;
    private int _minThreads;
    private int _threadsCreated;

    private UnhandledExceptionEventHandler _unhandledExceptionHandler;

    #endregion

    #region Constructors

    /// <summary>
    /// Constructs a DynamicThreadPool using the defaults
    /// MinThreads = 0
    /// MaxThreads = 5
    /// KeepAlive = 30,000 (30 seconds)
    /// </summary>
    /// <param name="name_">The name of the ThreadPool (used to name threads)</param>
    protected internal DynamicThreadPool(string name_)
      : this(name_, DEFAULT_MINTHREADS, DEFAULT_MAXTHREADS, DEFAULT_KEEPALIVE)
    {
    }

    /// <summary>
    /// Constructs a DynamicThreadPool using the passed config which should be of the form:
    /// <code>
    ///   <ThreadPool name="ds" type="MorganStanley.MSDesktop.Rambo.AsyncUtils.DynamicThreadPool">
    ///     <MinThreads>1</MinThreads>
    ///     <MaxThreads>5</MaxThreads>
    ///     <KeepAlive>10000</KeepAlive>
    ///   </ThreadPool>
    /// </code>
    /// if a parameters is not in the config the default is used.
    /// </summary>
    /// <param name="name_">The name of the ThreadPool (used to name threads)</param>
    /// <param name="config_">The XML config</param>
    protected internal DynamicThreadPool(string name_, XmlElement config_)
    {
      _syncRoot = new object();

      _name = name_;

      MinThreads = DEFAULT_MINTHREADS;
      MaxThreads = DEFAULT_MAXTHREADS;
      _keepAlive = DEFAULT_KEEPALIVE;

      _threadsCreated = 0;

      _queue = new BlockingQueue<UserWorkItem>(SyncRoot);
      _threads = new Dictionary<string, Processor>();
      _idleThreads = new Dictionary<string, Processor>();

      XmlNode node = config_.SelectSingleNode("MinThreads");
      if (node != null)
      {
        int num = int.Parse(node.InnerText);
        if (num >= 0 && num <= 20)
        {
          MinThreads = num;
        }
        else
        {
          throw new ArgumentException(num + " is not a valid min number of threads (0 =< min <= 20)");
        }
      }
      node = config_.SelectSingleNode("MaxThreads");
      if (node != null)
      {
        int num = int.Parse(node.InnerText);
        if (num > 0 && num < 20 && MinThreads <= num)
        {
          MaxThreads = num;
        }
        else
        {
          throw new ArgumentException(num + " is not a valid max number of threads (0 < min <= max <= 20)");
        }
      }
      node = config_.SelectSingleNode("KeepAlive");
      if (node != null)
      {
        _keepAlive = int.Parse(node.InnerText);
      }

      InitializeThreadPool();
    }

    /// <summary>
    /// Constructs a DynamicThreadPool using the passed parameter and the default
    /// KeepAlive = 30,000 (30 seconds)
    /// </summary>
    /// <param name="name_">The name of the ThreadPool (used to name threads)</param>
    /// <param name="min_">smallest the thread pool can shrink to</param>
    /// <param name="max_">largest the thread pool can grow to</param>
    protected internal DynamicThreadPool(string name_, int min_, int max_)
      : this(name_, min_, max_, DEFAULT_KEEPALIVE)
    {
    }

    /// <summary>
    /// Constructs a DynamicThreadPool using the passed behaviour parameters
    /// </summary>
    /// <param name="name_">The name of the ThreadPool (used to name threads)</param>
    /// <param name="min_">smallest the thread pool can shrink to</param>
    /// <param name="max_">largest the thread pool can grow to</param>
    /// <param name="keepAlive_">number of milliseconds to keep idle threads alive for</param>
    protected internal DynamicThreadPool(string name_, int min_, int max_, int keepAlive_)
    {
      _syncRoot = new object();

      _name = name_;

      MinThreads = min_;
      MaxThreads = max_;
      _keepAlive = keepAlive_;

      _threadsCreated = 0;

      _queue = new BlockingQueue<UserWorkItem>(SyncRoot);
      _threads = new Dictionary<string, Processor>();
      _idleThreads = new Dictionary<string, Processor>();

      InitializeThreadPool();
    }

    #endregion

    #region Instance Properties

    /// <summary>
    /// Number of threads currently processing requests in the pool.
    /// </summary>
    public int ActiveThreads
    {
      get
      {
        lock (SyncRoot)
        {
          return Threads.Count - IdleThreads.Count;
        }
      }
    }

    protected Dictionary<string, Processor> IdleThreads
    {
      get { return _idleThreads; }
    }

    protected int KeepAlive
    {
      get { return _keepAlive; }
    }

    protected int MaxThreads
    {
      get
      {
        lock (SyncRoot)
        {
          return _maxThreads;
        }
      }
      set
      {
        lock (SyncRoot)
        {
          _maxThreads = value;
        }
      }
    }

    protected int MinThreads
    {
      get
      {
        lock (SyncRoot)
        {
          return _minThreads;
        }
      }
      set
      {
        lock (SyncRoot)
        {
          _minThreads = value;
        }
      }
    }

    protected object SyncRoot
    {
      get { return _syncRoot; }
    }

    protected Dictionary<string, Processor> Threads
    {
      get { return _threads; }
    }

    #endregion

    #region Instance Methods

    /// <summary>
    /// Returns the current status of the ThreadPool including:
    /// Name, MinThreads, MaxThreads, KeepAlive, Number of threads created,
    /// QueueSize, Threads currently in pool, Active Threads, Idle Threads
    /// and a list of all the threads in the pool.
    /// </summary>
    public override string ToString()
    {
      lock (SyncRoot)
      {
        StringBuilder sb = new StringBuilder("Thread Pool: " + Name);
        sb.Append(base.ToString()).Append("\n");
        sb.Append("Min Threads: ").Append(MinThreads).Append("\n");
        sb.Append("Max Threads: ").Append(MaxThreads).Append("\n");
        sb.Append("Keep Alive: ").Append(KeepAlive).Append("\n");
        sb.Append("Threads Created: ").Append(_threadsCreated).Append("\n");
        sb.Append("Queue Size: ").Append(_queue.Count).Append("\n");
        sb.Append("---------------------------------------\n");
        sb.Append("Threads in pool: ").Append(Threads.Count).Append("\n");
        sb.Append("Active Threads: ").Append(ActiveThreads).Append("\n");
        sb.Append("Idle Threads: ").Append(IdleThreads).Append("\n");
        sb.Append("---------------------------------------\n");
        foreach (string name in Threads.Keys)
        {
          sb.Append(name).Append("\n");
        }
        return sb.ToString();
      }
    }

    /// <summary>
    /// Clears the thread pool.
    /// </summary>
    protected void ClearThreadPool()
    {
      lock (SyncRoot)
      {
        foreach (Processor processor in Threads.Values)
        {
          processor.StopThread();
        }
        Threads.Clear();
        IdleThreads.Clear();
      }
    }

    /// <summary>
    /// Creates the thread and adds it to the thread pool.
    /// </summary>
    protected void CreateThread()
    {
      Processor proc = new Processor(this);
      Thread t = new Thread(proc.RunThread);
      proc.Thread = t;
      t.IsBackground = true;
      t.Name = string.Format("{0} #{1}", Name, _threadsCreated++);

      lock (SyncRoot)
      {
        Threads.Add(t.Name, proc);
      }

      t.Start();
    }

    /// <summary>
    /// Fires the <see cref="OnUnhandledException" event/>.
    /// </summary>
    /// <param name="e_">The <see cref="System.UnhandledExceptionEventArgs"/> instance containing the event data.</param>
    protected void FireOnUnhandledException(UnhandledExceptionEventArgs e_)
    {
      if (_unhandledExceptionHandler != null)
      {
        ThreadingUtils.ThreadSafeInvoke(_unhandledExceptionHandler, this, e_);
      }
    }

    /// <summary>
    /// Initializes the thread pool.
    /// </summary>
    protected void InitializeThreadPool()
    {
      lock (SyncRoot)
      {
        while (Threads.Count < MinThreads)
        {
          CreateThread();
        }
      }
    }

    /// <summary>
    /// Removes the thread with the specified name from the pool.
    /// </summary>
    /// <param name="name_">The thread's name.</param>
    protected void RemoveThread(string name_)
    {
      lock (SyncRoot)
      {
        Processor processor;
        if (Threads.TryGetValue(name_, out processor))
        {
          Threads.Remove(name_);
        }
      }
    }

    #endregion

    #region Event Handling

    /// <summary>
    /// Fires the UnhandledException event for a UserWorkItem that had an exception during invocation.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void UserWorkItemOnUnhandledException(object sender, UnhandledExceptionEventArgs e)
    {
      FireOnUnhandledException(e);
    }

    #endregion

    #region IThreadPool Members

    /// <summary>
    /// Called when an exception is caught in the ThreadPool (i.e. not handled in method).
    /// </summary>
    public event UnhandledExceptionEventHandler OnUnhandledException
    {
      add { _unhandledExceptionHandler += value; }
      remove { _unhandledExceptionHandler -= value; }
    }

    /// <summary>
    /// Returns the list of thread infos for the pool note that this operation is dangerous
    /// as it will suspend each thread in the pool
    /// </summary>
    /// <returns></returns>
    public IList<ThreadInfo> GetAllThreadsInfo()
    {
      IList<ThreadInfo> threadInfoList= new List<ThreadInfo>();
      lock(SyncRoot)
      {
        foreach(KeyValuePair<string,Processor> pair in Threads)
        {
          if (pair.Value.Thread.Name != Thread.CurrentThread.Name)
          {
            ThreadInfo threadInfo = new ThreadInfo(pair.Value.Thread, IdleThreads.ContainsKey(pair.Key));
            threadInfoList.Add(threadInfo);
          }
        }
      }
      return threadInfoList;
    }

    /// <summary>
    /// Enqueue method on the ThreadPool which takes that passed parameters.
    /// </summary>
    /// <param name="method_">Method to enqueue</param>
    /// <param name="params_">Method Params</param>
    public void Enqueue(Delegate method_, params object[] params_)
    {
      UserWorkItem userWorkItem = new UserWorkItem(method_, params_);
      userWorkItem.OnUnhandledException += UserWorkItemOnUnhandledException;
      lock (SyncRoot)
      {
        //enqueue
        _queue.Enqueue(userWorkItem);

        if (IdleThreads.Count == 0 && Threads.Count < MaxThreads)
        {
          CreateThread();
        }
      }
    }


    /// <summary>
    /// Queues callback delegate on the ThreadPool.
    /// </summary>
    /// <param name="callBack_">Delegate to enqueue</param>
    /// <returns>true if successfully enqueued</returns>
    public bool QueueUserWorkItem(WaitCallback callBack_)
    {
      return QueueUserWorkItem(callBack_, null);
    }

    /// <summary>
    /// Queues callback delegate on the ThreadPool.
    /// </summary>
    /// <param name="callBack_">Delegate to enqueue</param>
    /// <param name="state_">state to pass to callback</param>
    /// <returns>true if successfully enqueued</returns>
    public bool QueueUserWorkItem(WaitCallback callBack_, object state_)
    {
      Enqueue(callBack_, state_);
      return true; //Always successfully queued as no max queue size
    }

    /// <summary>
    /// Gets number of items waiting to be processes by the ThreadPool
    /// </summary>
    public int QueueSize
    {
      get { return _queue.Count; }
    }

    /// <summary>
    /// Gets number of Threads currently in the pool
    /// </summary>
    public int ThreadCount
    {
      get
      {
        lock (SyncRoot)
        {
          return Threads.Count;
        }
      }
    }

    /// <summary>
    /// Gets the name of the ThreadPool
    /// </summary>
    public string Name
    {
      get { return _name; }
    }

    #endregion

    #region Nested type: Processor

    /// <summary>
    /// Takes work off the queue and invokes it until StopThread is called.
    /// </summary>
    protected class Processor
    {
      #region Readonly & Static Fields

      private readonly DynamicThreadPool _pool;

      #endregion

      #region Fields

      private bool _run;
      private Thread _thread;

      #endregion

      #region Constructors

      public Processor(DynamicThreadPool pool_)
      {
        _pool = pool_;
      }

      #endregion

      #region Instance Properties

      public Thread Thread
      {
        get { return _thread; }
        set { _thread = value; }
      }

      #endregion

      #region Instance Methods

      public void RunThread()
      {
        _run = true;
        bool isIdle = false;

        
        try
        {          
          while (_run)
          {
            UserWorkItem userWorkItem;
            int timeout = isIdle ? Timeout.Infinite : _pool.KeepAlive;

            // try to get a unit of work out of the queue, 
            // if empty, blocks for the specified timeout period.
            if (_pool._queue.TryDequeue(timeout, out userWorkItem))
            {
              if (userWorkItem == null)
              {
                break;
              }
              
              isIdle = false;

              lock (_pool.SyncRoot)
              {
                _pool.IdleThreads.Remove(Thread.Name);
              }

              userWorkItem.InvokeWorkItem(null);

              // Do not remove: Clean up the userWorkItem reference now that its been run
              userWorkItem = null;
            }
            else // the "keep alive" time has expired
            {
              lock (_pool.SyncRoot)
              {
                // End this thread.
                if (_pool.Threads.Count > _pool.MinThreads)
                {
                  break;
                }

                // otherwise, the thread was kept alive but is idle.
                _pool.IdleThreads.Add(_thread.Name, this);
                isIdle = true;
              }
            }
          }
        }
        catch (Exception ex_)
        {
          _pool.FireOnUnhandledException(new UnhandledExceptionEventArgs(ex_, true));
        }
        finally
        {
          lock (_pool.SyncRoot)
          {
            if (_pool.Threads.ContainsKey(_thread.Name))
            {
              _pool.Threads.Remove(_thread.Name);
            }
          }
        }
      }

      public void StopThread()
      {
        _run = false;
        _thread.Abort();
      }

      #endregion
    }

    #endregion
  }
}