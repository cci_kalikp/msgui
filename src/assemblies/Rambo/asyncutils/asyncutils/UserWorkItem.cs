using System;
using System.Reflection;

namespace MorganStanley.MSDesktop.Rambo.AsyncUtils
{
  internal class UserWorkItem : ISignalUnhandledException
  {
    #region Readonly & Static Fields

    private readonly Delegate _method;
    private readonly object[] _params;

    #endregion

    #region Fields

    private object _key = null;

    #endregion

    #region Constructors

    /// <summary>
    /// Initializes a new instance of the <see cref="UserWorkItem"/> class with a unique key.
    /// </summary>
    /// <param name="key_">The unique key to later identify the operation.</param>
    /// <param name="method_">The method to be called.</param>
    /// <param name="params_">The argument to be used for the call.</param>
    public UserWorkItem(object key_, Delegate method_, params object[] params_) : this(method_, params_)
    {
      _key = key_;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="UserWorkItem"/> class.
    /// </summary>
    /// <param name="method_">The method to be called.</param>
    /// <param name="params_">The argument to be used for the call.</param>
    public UserWorkItem(Delegate method_, params object[] params_)
    {
      _method = method_;
      _params = params_;
    }

    #endregion

    #region Instance Properties

    /// <summary>
    /// Gets or sets the unique key for the user work item.  If not key is pre-defined, the Key is given as "[MethodName]:[HashCode]" for uniqueness.
    /// </summary>
    /// <value>The key.</value>
    public object Key
    {
      get
      {
        if (_key == null)
        {
          _key = ToString();
        }
        return _key;
      }
    }

    /// <summary>
    /// Gets the method that is to be called for this work item.
    /// </summary>
    /// <value>The method to be called for this work item.</value>
    public Delegate Method
    {
      get { return _method; }
    }

    /// <summary>
    /// Gets the argument array to be used for this work item.
    /// </summary>
    /// <value>The argument array to be used for this work item.</value>
    public object[] Params
    {
      get { return _params; }
    }

    #endregion

    #region Instance Methods

    /// <summary>
    /// Returns a <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.  
    /// </summary>
    /// <returns>
    /// The value "[MethodName]:[HashCode]" giving a unique mechanism to read the method name, but keep the result unique from all other work items
    /// </returns>
    public override string ToString()
    {
      return string.Format("{0}:{1}", _method.Method.Name, GetHashCode());
    }

    /// <summary>
    /// Invokes the work item and signals any unhandled exceptions.
    /// </summary>
    /// <param name="state_">The state (ignored).</param>
    public void InvokeWorkItem(object state_)
    {
      Exception exception = null;
      try
      {
        try
        {
          Method.DynamicInvoke(Params);
        }
        catch (TargetInvocationException invEx)
        {
          exception = invEx.InnerException;
        }
        catch (Exception ex)
        {
          exception = ex;
        }

        if (exception != null)
        {
          FireOnUnhandledException(new UnhandledExceptionEventArgs(exception, false));
        }
      }

      finally
      {
        // clear any references to OnUnhandledException, so that UserWorkItem can be cleaned up.
        OnUnhandledException = null;
      }
    }

    /// <summary>
    /// Fires the unhandled exception event.
    /// </summary>
    /// <param name="e_">The <see cref="System.UnhandledExceptionEventArgs"/> instance containing the event data.</param>
    protected void FireOnUnhandledException(UnhandledExceptionEventArgs e_)
    {
      if (OnUnhandledException != null)
      {
        OnUnhandledException(this, e_);
      }
    }

    #endregion

    #region ISignalUnhandledException Members

    /// <summary>
    /// Occurs when an Unhandled Exception occurs during the workitem's processing.
    /// </summary>
    public event UnhandledExceptionEventHandler OnUnhandledException;

    #endregion
  }
}