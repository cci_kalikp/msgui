using System;
using System.ComponentModel;
using System.Threading;
using MorganStanley.MSDotNet.MSLog;

namespace MorganStanley.MSDesktop.Rambo.AsyncUtils
{
  /// <summary>
  /// Executes an operation on a separate thread from a user-specified thread pool.
  /// </summary>
  /// <remarks>
  /// <para>
  /// The <b>AsynchronizerBackgroundWorker</b> class allows you to run an operation on a separate, dedicated thread. Time-consuming operations like downloads and database transactions can cause your user interface (UI) to seem as though  it has stopped responding while they are running. When you want a responsive UI and you are faced with  long delays associated with such operations, the <b>AsynchronizerBackgroundWorker</b> class provides a convenient solution.
  /// </para>
  /// <para>
  /// To execute a time-consuming operation in the background, create a <b>AsynchronizerBackgroundWorker</b> and listen for events that report the progress of your operation and signal when your operation is finished. You can create the <b>AsynchronizerBackgroundWorker</b> programmatically or you can drag it onto your form from the Components tab of the Toolbox. If you create the <b>AsynchronizerBackgroundWorker</b> in the Windows Forms Designer, it will appear in the Component Tray, and its properties will be displayed in the Properties window.
  /// </para>
  /// <para>
  /// To set up for a background operation, add an event handler for the <see cref="DoWork"/> event. Call your time-consuming operation in this event handler. To start the operation, call <see cref="IBackgroundWorker.RunWorkerAsync()"/>. To receive notifications of progress updates, handle the <see cref="ProgressChanged"/> event. To receive a notification when the operation is completed, handle the <see cref="RunWorkerCompleted"/> event.
  /// </para>
  /// <para>
  /// If your background operation requires a parameter, call <see cref="IBackgroundWorker.RunWorkerAsync(System.Object)"/> with your parameter. Inside the <see cref="DoWork"/> event handler, you can extract the parameter from the <see cref="DoWorkEventArgs.Argument"/> property.
  /// </para>
  /// <para>
  /// To cancel an operation you must first specify that the <b>AsynchronizerBackgroundWorker</b> supports cancellation through setting the <see cref="IBackgroundWorkerComponent.WorkerSupportsCancellation"/> property to <c>true</c>.  You may then call <see cref="IBackgroundWorker.CancelAsync"/> to request the cancellation. Note that it is responsibility of the implementer of the <see cref="DoWorkEventHandler"/> to end this operation after the cancellation has been requested - this may be determined by inspecting  the <see cref="IBackgroundWorker.CancellationPending"/> flag, then the implementer must set the <see cref="DoWorkEventArgs.Cancel"/> to <c>true</c>. 
  /// </para>
  /// <para>
  /// To end an operation explicitly you could abort the thread entirely by calling the <see cref="IBackgroundWorker.AbortAsync"/>.  Note that you must first specify that the <b>AsynchronizerBackgroundWorker</b> supports the abort operation through setting the <see cref="IBackgroundWorkerComponent.WorkerSupportsAbort"/> property to <c>true</c>.  To determine that an abort operation has occured you may (safely) cast the <see cref="RunWorkerCompletedEventArgs"/> in the <see cref="RunWorkerCompletedEventHandler"/> to a <see cref="AbortableRunWorkerCompletedEventArgs"/> and inspect the <see cref="AbortableRunWorkerCompletedEventArgs.Aborted"/> flag.
  /// </para>
  /// <para>
  /// An operation may also be paused if the <b>AsynchronizerBackgroundWorker</b> specifies it supports pause through setting the <see cref="IBackgroundWorkerComponent.WorkerSupportsPause"/> property to <c>true</c>. You may then call <see cref="IBackgroundWorker.PauseAsync"/> and <see cref="IBackgroundWorker.PauseAsync"/> to pause or restart the work on the thread. Note that it is responsibility of the implementer of the <see cref="DoWorkEventHandler"/> to pause or restart work after they have been requested - this may be determined by inspecting  the <see cref="IBackgroundWorker.IsPaused"/> flag.
  /// </para>
  /// </remarks>
  public class AsynchronizerBackgroundWorker : Component, 
    IBackgroundWorker,
    IThreadPoolConfigurable
  {
    #region Data
    private DoWorkEventHandler _doWork;
    private ProgressChangedEventHandler _progressChanged;
    private RunWorkerCompletedEventHandler _runWorkerCompleted;

    private Thread _workerThread;
    private AsynchronizerResult _result;
    private readonly object _threadSyncRoot;

    private bool _isBusy;
    private bool _isPaused;
    private bool _cancellationPending;
    private bool _abortPending;
    private int _percentCompleted;

    private bool _workerReportsProgress;
    private bool _workerSupportsAbort;
    private bool _workerSupportsCancellation;
    private bool _workerSupportsPause;

    private static readonly Logger _logger = new Logger(typeof(AsynchronizerBackgroundWorker));
    private string _threadPool;
    #endregion

    #region Constructors
    /// <summary>
    /// Initializes a new instance of the <see cref="AsynchronizerBackgroundWorker"/> class.
    /// </summary>
    public AsynchronizerBackgroundWorker() 
    {
      _threadSyncRoot = new object( );
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="AsynchronizerBackgroundWorker"/> class which uses the specified thread pool.
    /// </summary>
    /// <param name="threadPool_">The thread pool.</param>
    public AsynchronizerBackgroundWorker(string threadPool_) 
      : this()
    {
      _threadPool = threadPool_;
    }
    #endregion

    #region IThreadPoolConfigurable Members
    /// <summary>
    /// Gets or sets the id of the thread pool to use for the asynchronous <see cref="DoWork"/> operation.
    /// </summary>
    [Browsable(true)]
    public string ThreadPool
    {
      get { return _threadPool; }
      set { _threadPool = value; }
    }
    #endregion

    #region IBackgroundWorkerComponent Members
    /// <summary>
    /// Occurs when RunWorkerAsync is called.
    /// </summary>
    public event DoWorkEventHandler DoWork
    {
      add
      {
        _doWork += value;
      }
      remove
      {
        _doWork -= value;
      }
    }

    /// <summary>
    /// Occurs when ReportProgress is called.
    /// </summary>
    public event ProgressChangedEventHandler ProgressChanged
    {
      add
      {
        _progressChanged += value;
      }
      remove
      {
        _progressChanged -= value;
      }
    }

    /// <summary>
    /// Occurs when the background operation has completed, has been canceled, or has raised an exception.
    /// </summary>
    public event RunWorkerCompletedEventHandler RunWorkerCompleted
    {
      add
      {
        _runWorkerCompleted += value;
      }
      remove
      {
        _runWorkerCompleted -= value;
      }
    }

    /// <summary>
    /// Gets or sets a value indicating whether the BackgroundWorker can report progress updates.
    /// </summary>
    [Browsable(true)]
    public bool WorkerReportsProgress
    {
      get { return _workerReportsProgress; }
      set { _workerReportsProgress = value; }
    }

    /// <summary>
    /// Gets or sets a value indicating whether the BackgroundWorker supports asynchronous cancellation.
    /// </summary>
    [Browsable(true)]
    public bool WorkerSupportsCancellation
    {
      get { return _workerSupportsCancellation; }
      set { _workerSupportsCancellation = value; }
    }

    /// <summary>
    /// Gets or sets a value indicating whether the BackgroundWorker supports aborting the thread.
    /// </summary>
    [Browsable(true)]
    public bool WorkerSupportsAbort
    {
      get { return _workerSupportsAbort; }
      set { _workerSupportsAbort = value; }
    }

    /// <summary>
    /// Gets or sets a value indicating whether the BackgroundWorker supports pausing an asynchronous operation.
    /// </summary>
    [Browsable(true)]
    public bool WorkerSupportsPause
    {
      get { return _workerSupportsPause; }
      set { _workerSupportsPause = value; }
    }
    #endregion

    #region IBackgroundWorker Members
    /// <summary>
    /// Starts execution of a background operation with argument
    /// </summary>
    public void RunWorkerAsync(object argument_)
    {
      if (_isBusy)
      {
        throw new InvalidOperationException(
          "This BackgroundWorker is currently busy and cannot run multiple tasks concurrently.");
      }
      
      _isBusy = true;
 
      _result = (AsynchronizerResult)BeginRunWorkerAsync(EndRunWorkerAsync, new DoWorkEventArgs(argument_));
    }


    /// <summary>
    /// Starts execution of a background operation.
    /// </summary>
    public void RunWorkerAsync( )
    {
      RunWorkerAsync(null);
    }

    /// <summary>
    /// Raises the ProgressChanged event.
    /// </summary>
    /// <param name="percent_"></param>
    public void ReportProgress(int percent_)
    {
      ReportProgress(percent_, null);
    }

    /// <summary>
    /// Raises the ProgressChanged event.
    /// </summary>
    /// <param name="percent_">The percent to be passed.</param>
    /// <param name="userState_">Current state of the background worker.</param>
    public void ReportProgress(int percent_, object userState_)
    {
      if(!_workerReportsProgress)
      {
        throw new InvalidOperationException(
          "This BackgroundWorker states that it doesn't report progress. Modify WorkerReportsProgress to state that it does report progress.");
      }

      Interlocked.Exchange(ref _percentCompleted, percent_);

      OnProgressChanged(new ProgressChangedEventArgs(percent_, userState_));
    }

    /// <summary>
    /// Requests cancellation of a pending background operation.
    /// </summary>
    public void CancelAsync( )
    {
      if(!_workerSupportsCancellation)
      {
        throw new InvalidOperationException("This BackgroundWorker states that it doesn't support cancellation. Modify WorkerSupportsCancellation to state that it does support cancellation.");
      }

      _cancellationPending = true;
    }

    /// <summary>
    /// Requests a pause of a pending background operation.
    /// </summary>
    public void PauseAsync( )
    {
      if(!_workerSupportsPause)
      {
        throw new InvalidOperationException("This BackgroundWorker states that it doesn't support pause. Modify WorkerSupportsPause to state that it does support pause.");
      }

      _isPaused = true;
    }

    /// <summary>
    /// Requests a restart of paused background operation.
    /// </summary>
    public void RestartAsync( )
    {
      if (!_workerSupportsPause)
      {
        throw new InvalidOperationException("This BackgroundWorker states that it doesn't support pause. Modify WorkerSupportsPause to state that it does support pause.");
      }

      _isPaused = false;

    }

    /// <summary>
    /// Forcefully aborts BackgroundWorker by calling Thread.Abort -- can be unsafe
    /// </summary>
    public void AbortAsync( )
    {
      if(!_workerSupportsAbort)
      {
        throw new InvalidOperationException(
          "This BackgroundWorker states that it doesn't support the abort operation. Modify WorkerSupportsAbort to state that it does support an abort.");
      }

      if(!_isBusy)
      {
        throw new InvalidOperationException(
          "AbortAsync has been called before RunWorkAsync.  Check IsBusy state before calling AbortAsync.");
      }

      lock (_threadSyncRoot)
      {
        Thread workerThread = WorkerThread;
        if(workerThread != null)
        {
          _abortPending = false;
          workerThread.Abort();
        }
        else
        {
          _abortPending = true;
        }
      }
    }

    /// <summary>
    /// Gets a value indicating whether the application has requested cancellation of a background operation
    /// </summary>
    [Browsable(false)]
    public bool CancellationPending
    {
      get { return _cancellationPending; }
    }

    /// <summary>
    /// Gets a value indicating whether the application has requested an abort of a background operation.
    /// </summary>
    [Browsable(false)]
    public bool AbortPending
    {
      get { return _abortPending; }
    }

    /// <summary>
    /// Gets a value indicating whether the BackgroundWorker is running an asynchronous operation.
    /// </summary>
    [Browsable(false)]
    public bool IsBusy
    {
      get { return _isBusy; }
    }

    /// <summary>
    /// Gets a value indicating whether the BackgroundWorker is paused.
    /// </summary>
    public bool IsPaused
    {
      get { return _isPaused; }
    }

    /// <summary>
    /// Gets a value of percent of operation that was completed
    /// </summary>
    [Browsable(false)]
    public int PercentCompleted
    {
      get { return _percentCompleted; }
    }

    #endregion

    #region Protected Properties
    /// <summary>
    /// Provides synchronized access to the BackgroundWorker's thread 
    /// </summary>
    protected Thread WorkerThread
    {
      get
      {
        lock (_threadSyncRoot)
        {
          return _workerThread;
        }
      }
      set
      {
        lock (_threadSyncRoot)
        {
          _workerThread = value;
        } 
      }
    }
    #endregion

    #region Protected methods
    /// <summary>
    /// Releases the unmanaged resources used by the <see cref="T:System.ComponentModel.Component"></see> and optionally releases the managed resources.
    /// </summary>
    /// <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
    protected override void Dispose(bool disposing)
    {
      if(disposing)
      {
        _doWork = null;
        _progressChanged = null;
        _runWorkerCompleted = null;
      }

      base.Dispose(disposing);
    }

    /// <summary>
    /// Raises the <see cref="DoWork"/> event.
    /// </summary>
    /// <param name="e_">An <see cref="EventArgs"/> that contains the event data.</param>
    protected void OnDoWork(DoWorkEventArgs e_)
    {
      try 
      {
        lock (_threadSyncRoot)
        {
          WorkerThread = Thread.CurrentThread;

          if(_abortPending)
          {
            AbortAsync( );
          }
        }


        if (_doWork != null)
        {
          if (_logger.CanLogPriority(MSLogPriority.Debug))
          {
            _logger.LogDebug("OnDoWork",
                 string.Format("ASYNC OPERATION (BW): Starting DoWork process on '{0}.{1}' on a background thread",
                               _doWork.Method.DeclaringType, _doWork.Method.Name));
          }
          _doWork(this, e_);
        }
      }
      finally
      {
        WorkerThread = null;
      }
    }

    /// <summary>
    /// Raises the <see cref="RunWorkerCompleted"/> event.
    /// </summary>
    /// <param name="e_">An <see cref="EventArgs"/> that contains the event data.</param>
    protected void OnRunWorkComplete(RunWorkerCompletedEventArgs e_)
    {
      _isBusy = false;
      _cancellationPending = false;
      _isPaused = false;

      if (_runWorkerCompleted != null)
      {
        if (_logger.CanLogPriority(MSLogPriority.Debug))
        {
          _logger.LogDebug("OnRunWorkComplete",
               string.Format("ASYNC OPERATION (BW): Starting RunWorkCompleted process on '{0}.{1}' ",
                             _runWorkerCompleted.Method.DeclaringType, _runWorkerCompleted.Method.Name));
        }
        ThreadingUtils.ThreadSafeContextInvoke(_result.SynchronizationContext, _runWorkerCompleted, this, e_);
      }
    }

    /// <summary>
    /// Raises the <see cref="ProgressChanged"/> event.
    /// </summary>
    /// <param name="e_">An <see cref="EventArgs"/> that contains the event data.</param>
    protected void OnProgressChanged(ProgressChangedEventArgs e_)
    {
      if (_progressChanged != null)
      {
        ThreadingUtils.ThreadSafeContextInvoke(_result.SynchronizationContext, _progressChanged, this, e_);
      }
    }
    #endregion

    #region Private methods
    /// <summary>
    /// Begins the asynchronous invocation of the <see cref="DoWork"/> event.
    /// </summary>
    /// <param name="callback_">The callback delegate that is invoked upon completion of <see cref="DoWork"/>.</param>
    /// <param name="e_">The <see cref="DoWorkEventArgs"/> instance containing the event data.</param>
    /// <returns>The <see cref="IAsyncResult"/> containing the status of the asynchronous operation.</returns>
    private IAsyncResult BeginRunWorkerAsync(AsyncCallback callback_, DoWorkEventArgs e_)
    {
      AsyncSettings settings = new AsyncSettings(true);
      Asynchronizer async = _threadPool == null
                              ? new Asynchronizer(callback_, e_, settings)
                              : new Asynchronizer(callback_, e_, _threadPool, settings);

      return async.BeginInvoke(new Action<DoWorkEventArgs>(OnDoWork), e_);
    }

    /// <summary>
    /// Completes the asynchronous invocation of the <see cref="DoWork"/> event and invokes the 
    /// <see cref="RunWorkerCompleted"/> event.
    /// </summary>
    /// <param name="result_">The <see cref="IAsyncResult"/> containing the status of the asynchronous operation.</param>
    private void EndRunWorkerAsync(IAsyncResult result_)
    {
      AsynchronizerResult asyncResult = (AsynchronizerResult) result_;
      DoWorkEventArgs doWorkArgs = (DoWorkEventArgs) result_.AsyncState;

      asyncResult.AsyncWaitHandle.WaitOne();       

      if (asyncResult.InvokeException is ThreadAbortException)
      {
        OnRunWorkComplete(
            new AbortableRunWorkerCompletedEventArgs(null, null, false, true));
      }
      else
      {
        OnRunWorkComplete(
          new AbortableRunWorkerCompletedEventArgs(doWorkArgs.Result,
                                                   asyncResult.InvokeException,
                                                   doWorkArgs.Cancel,
                                                   false));
      }
    }
    #endregion
  }
}
