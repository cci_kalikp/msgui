using System;
using System.Collections.Generic;
using System.Threading;

namespace MorganStanley.MSDesktop.Rambo.AsyncUtils
{
  /// <summary>
  /// Put multiple work items on background threads simultaneously and block until all operations are complete.  
  /// <see cref="MultiplexAsynchronizer"/> assumes the callback will be returned on a GUI thread if it was started on one, and 
  /// a background thread if it was started on a background thread.
  /// </summary>
  public class MultiplexAsynchronizer : Asynchronizer
  {
    #region Readonly & Static Fields

    private readonly List<UserWorkItem> _operations = new List<UserWorkItem>();

    #endregion

    #region Constructors

    /// <summary>
    /// Initializes a new instance of the <see cref="MultiplexAsynchronizer"/> class.
    /// </summary>
    /// <param name="callback_">The callback.</param>
    /// <param name="state_">The state.</param>
    public MultiplexAsynchronizer(AsyncCallback callback_, object state_)
      : base(callback_, state_, new AsyncSettings(true))
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="MultiplexAsynchronizer"/> class.
    /// </summary>
    /// <param name="callback_">The callback.</param>
    /// <param name="state_">The state.</param>
    /// <param name="threadPool_">The thread pool.</param>
    public MultiplexAsynchronizer(AsyncCallback callback_, object state_, string threadPool_)
      : base(callback_, state_, threadPool_, new AsyncSettings(true))
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="MultiplexAsynchronizer"/> class.
    /// </summary>
    /// <param name="callback_">The callback.</param>
    /// <param name="state_">The state.</param>
    /// <param name="threadPool_">The thread pool.</param>
    public MultiplexAsynchronizer(AsyncCallback callback_, object state_, IThreadPool threadPool_)
      : base(callback_, state_, threadPool_, new AsyncSettings(true))
    {
    }

    #endregion

    #region Instance Methods

    /// <summary>
    /// Adds an operation to the list of operations to be invoked, with an object key.
    /// </summary>
    /// <param name="key">The key for the operation.</param>
    /// <param name="method_">The method to be called.</param>
    /// <param name="args_">The arguments to be passed to the operation.</param>
    /// <seealso cref="AddOperation"/>
    public void AddKeyedOperation(object key, Delegate method_, params object[] args_)
    {
      AddOperation(new UserWorkItem(key, method_, args_));
    }

    /// <summary>
    /// Adds an operation to the list of operations to be invoked.
    /// </summary>
    /// <param name="method_">The method to be called.</param>
    /// <param name="args_">The arguments to be passed to the operation.</param>
    /// <seealso cref="AddKeyedOperation"/>
    public void AddOperation(Delegate method_, params object[] args_)
    {
      AddOperation(new UserWorkItem(method_, args_));
    }

    /// <summary>
    /// Invokes all operations asynchronously, calling the callback defined with the constructor when all processes have completed.
    /// </summary>
    ///<returns>
    ///An <see cref="IAsyncResult"/> interface that represents the asynchronous operation started by calling this method.
    ///</returns>
    public IAsyncResult BeginInvokeAll()
    {
      return BeginInvoke(null);
    }

    /// <summary>
    /// Invokes all operations synchronously, i.e. doesn't multiplex.
    /// </summary>
    public void InvokeAll()
    {
      Invoke(null);
    }

    /// <summary>
    /// Adds the provided method to the list of operations and asynchronously invokes all operations using threads from this instance's <see cref="IThreadPool"/>.
    /// </summary>
    /// <param name="method_">A <see cref="T:System.Delegate"></see> to a method that takes parameters of the same number and type that are contained in args.</param>
    /// <param name="args_">An array of type <see cref="T:System.Object"></see> to pass as arguments to the given method. This can be null if no arguments are needed.</param>
    /// <returns>
    /// An <see cref="T:System.IAsyncResult"></see> interface that represents the asynchronous operation started by calling this method.
    /// </returns>
    protected override IAsyncResult BeginInvokeStrategy(Delegate method_, object[] args_)
    {
      InitSynchronizationContext();
      MultiplexAsynchronizerResult result = new MultiplexAsynchronizerResult(_operations, AsyncCallBack, State, this, _context);
      ThreadPool.Enqueue(new WaitCallback(result.DoInvoke), new object[] {null});
      return result;
    }

    /// <summary>
    /// Adds the provided method to the list of operations and synchronously invokes all operations on the current thread.
    /// </summary>System.Threading;
    /// <param name="method_">A <see cref="T:System.Delegate"></see> that contains a method to call, in the context of the thread for the control.</param>
    /// <param name="args_">An array of type <see cref="T:System.Object"></see> that represents the arguments to pass to the given method. This can be null if no arguments are needed.</param>
    /// <returns>
    /// An <see cref="T:System.Object"></see> that represents the return value from the delegate being invoked, or null if the delegate has no return value.
    /// </returns>
    protected override object InvokeStrategy(Delegate method_, params object[] args_)
    {
      foreach (UserWorkItem operation in _operations)
      {
        base.InvokeStrategy(operation.Method, operation.Params);
      }

      return base.InvokeStrategy(method_, args_);
    }

    private void AddOperation(UserWorkItem item)
    {
      _operations.Add(item);
    }

    #endregion
  }
}