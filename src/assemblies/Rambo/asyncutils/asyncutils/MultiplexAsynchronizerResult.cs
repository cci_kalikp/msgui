using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using MorganStanley.MSDotNet.MSNet;

namespace MorganStanley.MSDesktop.Rambo.AsyncUtils
{
  /// <summary>
  /// Mechanism to run multiple asynchronous processes, collect their results, and 
  /// run a call back at the end to mark the completion of all processes (failed or not).
  /// </summary>
  public class MultiplexAsynchronizerResult : AsynchronizerResult
  {
    #region Fields

    private readonly IList<UserWorkItem> _operations;
    private readonly IDictionary<object, object> _results;
    private IDictionary<object, Exception> _resultExceptions;

    #endregion

    #region Constructors

    internal MultiplexAsynchronizerResult(IList<UserWorkItem> operations, AsyncCallback callBack, object asyncState,
                                          ISynchronizeInvoke async, SynchronizationContext context) : base(null, null, callBack, asyncState, async, context)
    {
      _operations = operations;
      _results = new Dictionary<object, object>(operations.Count);
      _resultExceptions = new Dictionary<object, Exception>();
    }

    /// <summary>
    /// Gets the results from the multiplexed operations, keyed based on the values passed in 
    /// with the <see cref="MultiplexAsynchronizer.AddKeyedOperation"/> method or based on "[MethodName]:[HashCode]".
    /// </summary>
    /// <value>The results from the multiplexed operations.</value>
    public IDictionary<object, object> Results
    {
      get { return _results; }
    }

    public IDictionary<object, Exception> ResultExceptions
    {
      get { return _resultExceptions; }
    }
    
    #endregion

    #region Instance Methods

    /// <summary>
    /// Loops thru the <see cref="_operations"/> list to performs the actual method invocation, each 
    /// on its own <see cref="Asynchronizer"/> (with each calling <see cref="EndChildOperation(IAsyncResult)"/> on completion).
    /// 
    /// At the end of the multiplex operation, <see cref="EndMultiplex"/> is called.
    /// </summary>
    protected override void DoInvokeStrategy()
    {
      if (_operations.Count == 0)
      {
        return;
      }

      
      IMSNetLoop loop = ThreadPoolManager.Instance.GetNetLoop(AsynchronizerParent.ThreadPool.Name);

      MSNetMultiplexedAsyncOperation multiPlex =
        new MSNetMultiplexedAsyncOperation(loop, EndMultiplex, null, _operations.Count);
      AsyncSettings settings = new AsyncSettings(true);
      foreach (UserWorkItem operation in _operations)
      {
        MSNetMultiplexedAsyncOperation.ChildOperation childOp = multiPlex.CreateChild(null);
        Asynchronizer childAsync =
          new Asynchronizer(EndChildOperation, new OperationState(operation.Key, childOp),
                            AsynchronizerParent.ThreadPool);
        childAsync.BeginInvoke(operation.Method, operation.Params);
      }
    }

    /// <summary>
    /// Signals to the <see cref="MSNetMultiplexedAsyncOperation"/> that a child operation is complete.
    /// </summary>
    /// <param name="result_">The result_.</param>
    private void EndChildOperation(IAsyncResult result_)
    {
      AsynchronizerResult result = (AsynchronizerResult) result_;
      OperationState state = (OperationState) result.AsyncState;
      object key = state.Key;
      MSNetMultiplexedAsyncOperation.ChildOperation childOp = state.ChildOp;

      if (result.InvokeException != null)
      {
        _resultExceptions.Add(key, result.InvokeException);
        result.AsyncWaitHandle.WaitOne();
        childOp.SignalDone(result.InvokeException);
      }
      else
      {
        _results.Add(key, result.MethodReturnedValue);
        result.AsynchronizerParent.EndInvoke(result);
        childOp.SignalDone();
      }
    }

    /// <summary>
    /// Called back when the last child operation completes.  Releases the <see cref="MSNetMultiplexedAsyncOperation.AsyncWaitHandle"/>, 
    /// so that the <see cref="MultiplexAsynchronizer"/> operation can complete.
    /// </summary>
    /// <param name="result_">The result_.</param>
    private void EndMultiplex(IAsyncResult result_)
    {
      try
      {
        // any exceptions occuring during child operations are rethrown here in a single exception.
        if (result_ is MSNetMultiplexedAsyncOperation)
        {
          MSNetMultiplexedAsyncOperation result = (MSNetMultiplexedAsyncOperation) result_;
          result.EndOperation();
        }
        else if (result_ is MSNetAsyncOperation)
        {
          MSNetAsyncOperation result = (MSNetAsyncOperation) result_;
          result.EndOperation();
        }
      }
      catch (Exception ex)
      {
        InvokeException = ex;
      }

      OnComplete();
    }

    #endregion

    #region Nested type: OperationState

    private class OperationState
    {
      #region Readonly & Static Fields

      private readonly MSNetMultiplexedAsyncOperation.ChildOperation _childOp;

      private readonly object _key;

      #endregion

      #region Constructors

      public OperationState(object key_, MSNetMultiplexedAsyncOperation.ChildOperation childOp_)
      {
        _key = key_;
        _childOp = childOp_;
      }

      #endregion

      #region Instance Properties

      public MSNetMultiplexedAsyncOperation.ChildOperation ChildOp
      {
        get { return _childOp; }
      }

      public object Key
      {
        get { return _key; }
      }

      #endregion
    }

    #endregion
  }
}