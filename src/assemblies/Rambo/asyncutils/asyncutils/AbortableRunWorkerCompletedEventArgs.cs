using System;
using System.ComponentModel;

namespace MorganStanley.MSDesktop.Rambo.AsyncUtils
{
  /// <summary>
  /// An extension of <see cref="RunWorkerCompletedEventArgs"/> that provides the <see cref="Aborted"/>
  /// property, indicating whether the <see cref="DoWorkEventHandler"/> was aborted.
  /// </summary>
  public class AbortableRunWorkerCompletedEventArgs : RunWorkerCompletedEventArgs
  {
    private readonly bool _aborted;

    /// <summary>
    /// Initializes a new instance of the <see cref="AbortableRunWorkerCompletedEventArgs"/> class.
    /// </summary>
    /// <param name="result_">The result of the operation.</param>
    /// <param name="error_">The error that occured during the operation.</param>
    /// <param name="cancelled_">if set to <c>true</c> the operation was cancelled.</param>
    /// <param name="aborted_">if set to <c>true</c> the operation was aborted.</param>
    public AbortableRunWorkerCompletedEventArgs(object result_, Exception error_, bool cancelled_, bool aborted_)
      : base(result_, error_, cancelled_)
    {
      _aborted = aborted_;
    }

    /// <summary>
    /// Gets a value indicating whether the <see cref="DoWorkEventHandler"/> was aborted.
    /// </summary>
    /// <value><c>true</c> if aborted; otherwise, <c>false</c>.</value>
    public bool Aborted
    {
      get { return _aborted; }
    }
  }
}