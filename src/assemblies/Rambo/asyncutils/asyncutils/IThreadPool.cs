using System;
using System.Collections.Generic;
using System.Threading;

namespace MorganStanley.MSDesktop.Rambo.AsyncUtils
{
  /// <summary>
  /// Interface for ThreadPool implementations to be used in the <see cref="ThreadPoolManager"/> class.
  /// </summary>
  public interface IThreadPool : ISignalUnhandledException
  {

    /// <summary>
    /// Returns a summary for list of active threads 
    /// Please use with care as it returns a stack trace and can be
    /// expensive
    /// </summary>
    /// <returns></returns>
    IList<ThreadInfo> GetAllThreadsInfo();

    /// <summary>
    /// Enqueue method on the ThreadPool which takes that passed parameters.
    /// </summary>
    /// <param name="method_">Method to enqueue</param>
    /// <param name="params_">Method Params</param>
    void Enqueue(Delegate method_, params object[] params_);

    /// <summary>
    /// Queues callback delegate on the ThreadPool.
    /// </summary>
    /// <param name="callBack_">Delegate to enqueue</param>
    /// <returns>true if successfully enqueued</returns>
    bool QueueUserWorkItem(WaitCallback callBack_);

    /// <summary>
    /// Queues callback delegate on the ThreadPool.
    /// </summary>
    /// <param name="callBack_">Delegate to enqueue</param>
    /// <param name="state_">state to pass to callback</param>
    /// <returns>true if successfully enqueued</returns>
    bool QueueUserWorkItem(WaitCallback callBack_, object state_);

    /// <summary>
    /// Gets number of Threads currently in the pool
    /// </summary>
    int ThreadCount
    {
      get;
    }

    /// <summary>
    /// Gets number of items waiting to be processes by the ThreadPool
    /// </summary>
    int QueueSize
    {
      get;
    }

    /// <summary>
    /// Gets the name of the ThreadPool
    /// </summary>
    string Name
    {
      get;
    }
  }
}
