namespace MorganStanley.MSDesktop.Rambo.AsyncUtils
{
  /// <summary>
  /// Interface for creating instances of <see cref="IBackgroundWorker"/> using the same
  /// values as set on the factory's <see cref="IBackgroundWorkerComponent"/> values.
  /// </summary>
  public interface IBackgroundWorkerFactory : IBackgroundWorkerComponent
  {
    /// <summary>
    /// Creates a new background worker.
    /// </summary>
    IBackgroundWorker CreateBackgroundWorker( );
  }
}