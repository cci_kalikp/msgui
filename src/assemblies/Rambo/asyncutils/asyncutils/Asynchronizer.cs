using System;
using System.ComponentModel;
using System.Threading;

namespace MorganStanley.MSDesktop.Rambo.AsyncUtils
{
  /// <summary>
  /// An implementation of <see cref="ISynchronizeInvoke"/> that invokes delegates on a background
  /// thread using a specified <see cref="IThreadPool"/> or the default <see cref="ManagedThreadPool"/>.
  /// </summary>
  /// <remarks>
  /// Lifted from http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dnwinforms/html/asyncui.asp
  /// Slightly modified for handling exceptions and null AsyncCallback delegates
  /// </remarks>
  public class Asynchronizer : ISynchronizeInvoke
  {
    #region Readonly & Static Fields

    private readonly AsyncSettings _settings;
    private readonly IThreadPool _threadPool;

    #endregion

    #region Fields

    protected SynchronizationContext _context;

    #endregion

    #region Constructors

    /// <summary>
    /// Initializes a new instance of the <see cref="Asynchronizer"/> class.
    /// </summary>
    /// <param name="callback_">The <see cref="AsyncCallback"/>  for the asynchronous process started by the <see cref="BeginInvoke"/> call.</param>
    /// <param name="asyncState_">The state to be passed across the thread boundary.</param>
    /// <param name="settings_">The <see cref="AsyncSettings"/> for this instance.</param>
    public Asynchronizer(AsyncCallback callback_, object asyncState_, AsyncSettings settings_)
      : this(callback_, asyncState_)
    {
      _settings = settings_;
    }


    /// <summary>
    /// Initializes a new instance of the <see cref="Asynchronizer"/> class.
    /// </summary>
    /// <param name="callback_">The <see cref="AsyncCallback"/>  for the asynchronous process started by the <see cref="BeginInvoke"/> call.</param>
    /// <param name="asyncState_">The state to be passed across the thread boundary.</param>
    public Asynchronizer(AsyncCallback callback_, object asyncState_)
    {
      AsyncCallBack = callback_;
      State = asyncState_;
      _threadPool = ThreadPoolManager.Instance[ManagedThreadPool.NAME];
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="Asynchronizer"/> class.
    /// </summary>
    /// <param name="callback_">The <see cref="AsyncCallback"/>  for the asynchronous process started by the <see cref="BeginInvoke"/> call.</param>
    /// <param name="asyncState_">The state to be passed across the thread boundary.</param>
    /// <param name="threadPool_">The thread pool name that the <see cref="Asynchronizer"/> should be managed on.</param>   
    /// <param name="settings_">The <see cref="AsyncSettings"/> for this instance.</param>
    public Asynchronizer(AsyncCallback callback_, object asyncState_, string threadPool_, AsyncSettings settings_)
      : this(settings_)
    {
      AsyncCallBack = callback_;
      State = asyncState_;
      _threadPool = !ThreadPoolManager.Instance.ThreadPoolExists(threadPool_)
                      ? ThreadPoolManager.Instance.CreateThreadPool(threadPool_)
                      : ThreadPoolManager.Instance[threadPool_];
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="Asynchronizer"/> class.
    /// </summary>
    /// <param name="callback_">The <see cref="AsyncCallback"/>  for the asynchronous process started by the <see cref="BeginInvoke"/> call.</param>
    /// <param name="asyncState_">The state to be passed across the thread boundary.</param>
    /// <param name="threadPool_">The thread pool name that the <see cref="Asynchronizer"/> should be managed on.</param>   
    public Asynchronizer(AsyncCallback callback_, object asyncState_, string threadPool_)
      : this(callback_, asyncState_, threadPool_, AsyncSettings.Empty)
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="Asynchronizer"/> class.
    /// </summary>
    /// <param name="callback_">The <see cref="AsyncCallback"/>  for the asynchronous process started by the <see cref="BeginInvoke"/> call.</param>
    /// <param name="asyncState_">The state to be passed across the thread boundary.</param>
    /// <param name="threadPool_">The <see cref="IThreadPool"/> interface object that the <see cref="Asynchronizer"/> should be managed on.</param>
    /// <param name="settings_">The <see cref="AsyncSettings"/> for this instance.</param>
    public Asynchronizer(AsyncCallback callback_, object asyncState_, IThreadPool threadPool_, AsyncSettings settings_)
      : this(settings_)
    {
      AsyncCallBack = callback_;
      State = asyncState_;
      _threadPool = threadPool_;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="Asynchronizer"/> class.
    /// </summary>
    /// <param name="callback_">The <see cref="AsyncCallback"/>  for the asynchronous process started by the <see cref="BeginInvoke"/> call.</param>
    /// <param name="asyncState_">The state to be passed across the thread boundary.</param>
    /// <param name="threadPool_">The <see cref="IThreadPool"/> interface object that the <see cref="Asynchronizer"/> should be managed on.</param>
    public Asynchronizer(AsyncCallback callback_, object asyncState_, IThreadPool threadPool_)
      : this(callback_, asyncState_, threadPool_, AsyncSettings.Empty)
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="Asynchronizer"/> class.
    /// </summary>
    /// <param name="settings_">The <see cref="AsyncSettings"/> for this instance.</param>
    private Asynchronizer(AsyncSettings settings_)
    {
      _settings = settings_;
    }

    #endregion

    #region Instance Properties

    /// <summary>
    /// The delegate to be invoked when the background operation is complete.
    /// </summary>
    protected AsyncCallback AsyncCallBack { get; set; }

    /// <summary>
    /// The user state object to be used in the <see cref="IAsyncResult"/> when the operation is complete
    /// </summary>
    protected object State { get; set; }

    /// <summary>
    /// The thread pool in which the Asynchronizer invokes background operations. 
    /// </summary>
    internal IThreadPool ThreadPool
    {
      get { return _threadPool; }
    }

    #endregion

    #region Instance Methods

    /// <summary>
    /// Returns the return value of the Asynchronizer run; if a <see cref="MultiplexAsynchronizer"/> is used, the return value
    /// is meaningless, but this should still be executed to complete the process.
    /// </summary>
    /// <param name="result">The <see cref="IAsyncResult"/> from the completion method.</param>
    /// <param name="returnValue">The return value of the complete Asynchonizer run.</param>
    /// <returns></returns>
    public bool TryEndInvoke(IAsyncResult result, out object returnValue)
    {
      returnValue = null;

      var arResult = result as AsynchronizerResult;
      if (arResult != null)
      {
        if (arResult.InvokeException == null)
        {
          returnValue = EndInvokeStrategy(result);
          return true;
        }
      }
      result.AsyncWaitHandle.WaitOne();
      return false;
    }

    ///<summary>
    /// Asynchronously executes the delegate on a thread from this instance's <see cref="IThreadPool"/>.
    ///</summary>
    ///
    ///<returns>
    ///An <see cref="T:System.IAsyncResult"></see> interface that represents the asynchronous operation started by calling this method.
    ///</returns>
    ///
    ///<param name="args_">An array of type <see cref="T:System.Object"></see> to pass as arguments to the given method. This can be null if no arguments are needed. </param>
    ///<param name="method_">A <see cref="T:System.Delegate"></see> to a method that takes parameters of the same number and type that are contained in args. </param>
    protected virtual IAsyncResult BeginInvokeStrategy(Delegate method_, params object[] args_)
    {
      InitSynchronizationContext();
      var result = new AsynchronizerResult(method_, args_, AsyncCallBack, State, this, _context);
      _threadPool.Enqueue(new WaitCallback(result.DoInvoke), new object[] {null});
      return result;
    }

    ///<summary>
    ///Waits until the process started by calling <see cref="M:System.ComponentModel.ISynchronizeInvoke.BeginInvoke(System.Delegate,System.Object[])"></see> completes, and then returns the value generated by the process.
    ///</summary>
    ///
    ///<returns>
    ///An <see cref="T:System.Object"></see> that represents the return value generated by the asynchronous operation.
    ///</returns>
    ///
    ///<param name="result_">An <see cref="T:System.IAsyncResult"></see> interface that represents the asynchronous operation started by calling <see cref="M:System.ComponentModel.ISynchronizeInvoke.BeginInvoke(System.Delegate,System.Object[])"></see>. </param>
    /// <exception cref="System.Exception">throws any exception that occurred during invocation</exception>
    protected virtual object EndInvokeStrategy(IAsyncResult result_)
    {
      var asynchResult = (AsynchronizerResult) result_;
      asynchResult.AsyncWaitHandle.WaitOne();

      return asynchResult.MethodReturnedValue;
    }

    ///<summary>
    /// Synchronously executes the delegate on the current thread.
    ///</summary>
    ///
    ///<returns>
    ///An <see cref="T:System.Object"></see> that represents the return value from the delegate being invoked, or null if the delegate has no return value.
    ///</returns>
    ///
    ///<param name="method_">A <see cref="T:System.Delegate"></see> that contains a method to call, in the context of the thread for the control. </param>
    ///<param name="args_">An array of type <see cref="T:System.Object"></see> that represents the arguments to pass to the given method. This can be null if no arguments are needed. </param>    
    protected virtual object InvokeStrategy(Delegate method_, params object[] args_)
    {
      if (method_ == null)
      {
        return null;
      }

      return method_.DynamicInvoke(args_);
    }

    protected void InitSynchronizationContext()
    {
      if (_settings.UseSyncContext)
      {
        _context = SynchronizationContext.Current;
      }
    }

    #endregion

    #region ISynchronizeInvoke Members

    /// <summary>
    /// Gets a value indicating whether the caller must call <see cref="M:System.ComponentModel.ISynchronizeInvoke.Invoke(System.Delegate,System.Object[])"></see> when calling an object that implements this interface.
    /// </summary>
    /// <returns>false</returns>
    public bool InvokeRequired
    {
      get { return false; }
    }

    ///<summary>
    /// Executes this instance's <see cref="InvokeStrategy"/>.
    ///</summary>
    ///
    ///<returns>
    ///An <see cref="T:System.Object"></see> that represents the return value from the delegate being invoked, or null if the delegate has no return value.
    ///</returns>
    ///
    ///<param name="args_">An array of type <see cref="T:System.Object"></see> that represents the arguments to pass to the given method. This can be null if no arguments are needed. </param>
    ///<param name="method_">A <see cref="T:System.Delegate"></see> that contains a method to call, in the context of the thread for the control. </param>
    public object Invoke(Delegate method_, params object[] args_)
    {
      return InvokeStrategy(method_, args_);
    }

    ///<summary>
    /// Executes this instance's <see cref="BeginInvokeStrategy"/>.
    ///</summary>
    ///
    ///<returns>
    ///An <see cref="T:System.IAsyncResult"></see> interface that represents the asynchronous operation started by calling this method.
    ///</returns>
    ///
    ///<param name="args_">An array of type <see cref="T:System.Object"></see> to pass as arguments to the given method. This can be null if no arguments are needed. </param>
    ///<param name="method_">A <see cref="T:System.Delegate"></see> to a method that takes parameters of the same number and type that are contained in args. </param>
    public IAsyncResult BeginInvoke(Delegate method_, params object[] args_)
    {
      return BeginInvokeStrategy(method_, args_);
    }


    ///<summary>
    /// Executes this instance's <see cref="EndInvokeStrategy"/>.
    ///</summary>
    ///
    ///<returns>
    ///An <see cref="T:System.Object"></see> that represents the return value generated by the asynchronous operation.
    ///</returns>
    ///
    ///<param name="result_">An <see cref="T:System.IAsyncResult"></see> interface that represents the asynchronous operation started by calling <see cref="M:System.ComponentModel.ISynchronizeInvoke.BeginInvoke(System.Delegate,System.Object[])"></see>. </param>
    /// <exception cref="System.Exception">throws any exception that occurred during invocation</exception>
    public object EndInvoke(IAsyncResult result_)
    {
      return EndInvokeStrategy(result_);
    }

    #endregion

    #region Static Methods

    /// <summary>
    /// Begins a two-way async operation running the specified delegate with the specified arguments, and returning the callback with the associated state.
    /// </summary>
    /// <param name="callback_">The <see cref="AsyncCallback"/>  for the asynchronous process started by the <see cref="BeginInvoke"/> call.</param>
    /// <param name="asyncState_">The state to be passed across the thread boundary.</param>
    /// <param name="method_">A <see cref="T:System.Delegate"></see> to a method that takes parameters of the same number and type that are contained in args. </param>
    /// <param name="args_">An array of type <see cref="T:System.Object"></see> to pass as arguments to the given method. This can be null if no arguments are needed. </param>
    /// <remarks>This process uses the Synchronization Context model by default - where it calls back on the GUI thread if it started on one.  
    /// To use the non-Synchronization Context model, use the standalone asynchronizer.  It was kept this way for backward compatibility, but it is not an ideal use case.</remarks>
    public static void BeginOperation(AsyncCallback callback_, object asyncState_, Delegate method_,
                                      params object[] args_)
    {
      new Asynchronizer(callback_, asyncState_, new AsyncSettings(true)).BeginInvoke(method_, args_);
    }

    /// <summary>
    /// Begins a one-way async operation running the specified delegate with the specified arguments.
    /// </summary>
    /// <param name="method_">A <see cref="T:System.Delegate"></see> to a method that takes parameters of the same number and type that are contained in args. </param>
    /// <param name="args_">An array of type <see cref="T:System.Object"></see> to pass as arguments to the given method. This can be null if no arguments are needed. </param>
    /// <remarks>This process uses the Synchronization Context model by default - where it calls back on the GUI thread if it started on one.  
    /// To use the non-Synchronization Context model, use the standalone asynchronizer.  It was kept this way for backward compatibility, but it is not an ideal use case.</remarks>
    public static void BeginOperation(Delegate method_, params object[] args_)
    {
      new Asynchronizer(InternalEndOperation, null, new AsyncSettings(true)).BeginInvoke(method_, args_);
    }

    private static void InternalEndOperation(IAsyncResult ar)
    {
      EndOperation(ar);
    }

    public static object EndOperation(IAsyncResult ar)
    {
      return ((AsynchronizerResult) ar).AsynchronizerParent.EndInvoke(ar);
    }

    public static bool TryEndOperation<T>(IAsyncResult ar, out T result)
    {
      result = default(T);
      if (((AsynchronizerResult)ar).InvokeException != null)
      {
        return false;
      }
      result = (T)((AsynchronizerResult)ar).AsynchronizerParent.EndInvoke(ar);
      return true;
    }
    public static Exception GetException(IAsyncResult ar)
    {
      return ((AsynchronizerResult)ar).InvokeException;
    }

    #endregion
  }

}