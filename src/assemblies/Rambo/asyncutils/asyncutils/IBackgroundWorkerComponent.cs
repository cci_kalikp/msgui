using System.ComponentModel;

namespace MorganStanley.MSDesktop.Rambo.AsyncUtils
{
  /// <summary>
  /// The properties and events of <see cref="IBackgroundWorker"/> that are modifiable
  /// through the designer.
  /// </summary>
  public interface IBackgroundWorkerComponent : IComponent
  {
    /// <summary>
    /// Occurs when ReportProgress is called.
    /// </summary>
    event ProgressChangedEventHandler ProgressChanged;

    /// <summary>
    /// Occurs when the background operation has completed, has been canceled, or has raised an exception.
    /// </summary>
    event RunWorkerCompletedEventHandler RunWorkerCompleted;

    /// <summary>
    /// Occurs when RunWorkerAsync is called.
    /// </summary>
    event DoWorkEventHandler DoWork;

    /// <summary>
    /// Gets or sets a value indicating whether the BackgroundWorker can report progress updates.
    /// </summary>
    bool WorkerReportsProgress { get; set;}

    /// <summary>
    /// Gets or sets a value indicating whether the BackgroundWorker supports asynchronous cancellation.
    /// </summary>
    bool WorkerSupportsCancellation { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether the BackgroundWorker supports aborting the thread.
    /// </summary>
    bool WorkerSupportsAbort { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether the BackgroundWorker supports pausing an asynchronous operation.
    /// </summary>
    bool WorkerSupportsPause { get; set; }

  }
}