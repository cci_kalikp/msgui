using System;
using System.ComponentModel;
using System.Threading;

namespace MorganStanley.MSDesktop.Rambo.AsyncUtils
{
  /// <summary>
  /// A set of static threading specific utility functions. 
  /// </summary>
  public static class ThreadingUtils
  {
    #region Static Methods

    /// <summary>
    /// Invokes each <see cref="Delegate"/>s in the provided delegate's invocation list, such that if the  delegate's <see cref="Delegate.Target"/> implements <see cref="ISynchronizeInvoke"/>, the delegate is asynchronously invoked against the ISynchronizeInvoke instance; otherwise, the delegate is invoked dynamically.
    /// </summary>
    /// <param name="delegate_">The multicast delegate.</param>
    /// <param name="args_">The arguments.</param>
    public static void ThreadSafeBeginInvoke(Delegate delegate_, params object[] args_)
    {
      ThreadSafeInvocation(delegate_, false, args_);
    }

    /// <summary>
    /// Performs the <see cref="SynchronizationContext"/>-based Thread Safe Invocation of the specified <see cref="Delegate"/>
    /// </summary>
    /// <param name="delegate_">The <see cref="Delegate"/> to be executed.</param>
    /// <param name="context">The <see cref="SynchronizationContext"/> maintaining the thread state.</param>
    /// <param name="args_">The arguments to be passed to the specified <see cref="Delegate"/>.</param>
    public static void ThreadSafeContextInvoke(SynchronizationContext context, Delegate delegate_, params object[] args_)
    {
      if(context == null)
      {
        ThreadSafeInvocation(delegate_, true, args_);
      }
      else
      {
        ThreadSafeContextInvocation(delegate_, true, context, args_);
      }
    }

    /// <summary>
    /// Invokes each <see cref="Delegate"/>s in the provided delegate's invocation list, such that if the  delegate's <see cref="Delegate.Target"/> implements <see cref="ISynchronizeInvoke"/>, the delegate is synchronously invoked against the ISynchronizeInvoke instance; otherwise, the delegate is invoked dynamically.
    /// </summary>
    /// <param name="delegate_">The multicast delegate.</param>
    /// <param name="args_">The arguments.</param>
    public static void ThreadSafeInvoke(Delegate delegate_, params object[] args_)
    {
      ThreadSafeInvocation(delegate_, true, args_);
    }

    private static void CallbackProcessor(object state)
    {
      var userWorkItem = state as UserWorkItem;
      if (userWorkItem != null)
      {
        ThreadSafeInvocation(userWorkItem.Method, true, userWorkItem.Params);
      }
    }

    /// <summary>
    /// Performs the <see cref="SynchronizationContext"/>-based Thread Safe Invocation of the specified <see cref="Delegate"/>
    /// </summary>
    /// <param name="delegate_">The <see cref="Delegate"/> to be executed.</param>
    /// <param name="synchronous_">if set to <c>true</c> process is executed synchronously; otherwise, it is executed asynchronously.</param>
    /// <param name="context">The <see cref="SynchronizationContext"/> maintaining the thread state.</param>
    /// <param name="args_">The arguments to be passed to the specified <see cref="Delegate"/>.</param>
    private static void ThreadSafeContextInvocation(Delegate delegate_, bool synchronous_,
                                                    SynchronizationContext context, params object[] args_)
    {
      if (delegate_ == null)
      {
        return;
      }

      if (synchronous_)
      {
        context.Send(CallbackProcessor, new UserWorkItem(delegate_, args_));
      }
      else
      {
        context.Post(CallbackProcessor, new UserWorkItem(delegate_, args_));
      }
    }

    private static void ThreadSafeInvocation(Delegate delegate_, bool synchronous_, params object[] args_)
    {
      if (delegate_ == null)
      {
        return;
      }

      Delegate[] invocationList = delegate_.GetInvocationList();

      foreach (Delegate del in invocationList)
      {
        var synchronizeInvoke = del.Target as ISynchronizeInvoke;

        if (synchronizeInvoke != null && synchronizeInvoke.InvokeRequired)
        {
          if (synchronous_)
          {
            synchronizeInvoke.Invoke(del, args_);
          }
          else
          {
            synchronizeInvoke.BeginInvoke(del, args_);
          }
        }
        else
        {
          del.DynamicInvoke(args_);
        }
      }
    }

    #endregion
  }
}