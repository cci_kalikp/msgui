using System;
using System.Threading;
using System.Xml;

namespace MorganStanley.MSDesktop.Rambo.AsyncUtils
{
	/// <summary>A fixed size thread pool, configured by setting a maximum number of threads.</summary>
	/// <remarks>
  /// http://scottwater.com/blog/archive/2004/01/08/ManagedThreadPool
  /// http://www.gotdotnet.com/Community/UserSamples/Details.aspx?SampleGuid=bf59c98e-d708-4f8e-9795-8bae1825c3b6
	/// </remarks>
	public class ManagedThreadPool
	{
		#region Member Variables
    public const string NAME = "MANAGED";

    /// <summary>
    /// Singleton instance.
    /// </summary>
    private static readonly FixedThreadPool _instance = new FixedThreadPool(5);
		
		#endregion

    #region Singleton
    /// <summary>
    /// Gets the ManagedThreadPool's IThreadPool implementation interface
    /// </summary>
    public static IThreadPool Instance
    {
      get
      {
        return _instance;
      }
    }
	  #endregion

	

		#region Public Methods
		/// <summary>
		/// Queues a user work item to the thread pool.
		/// </summary>
		/// <param name="callback">
		/// A WaitCallback representing the delegate to invoke when the thread in the 
		/// thread pool picks up the work item.
		/// </param>
		public static void QueueUserWorkItem(WaitCallback callback)
		{
			// Queue the delegate with no state
			_instance.QueueUserWorkItem(callback, null);
		}

		/// <summary>
		/// Queues a user work item to the thread pool.
		/// </summary>
		/// <param name="callback">
		/// A WaitCallback representing the delegate to invoke when the thread in the 
		/// thread pool picks up the work item.
		/// </param>
		/// <param name="state">
		/// The object that is passed to the delegate when serviced from the thread pool.
		/// </param>
		public static void QueueUserWorkItem(WaitCallback callback, object state)
		{
		  _instance.QueueUserWorkItem(callback, state);
		}

    /// <summary>
    /// Configures the ManagedThreadPool from the passed config element and reset's the
    /// ThreadPool.
    /// Warning this will abort all threads currently in the pool.
    /// The config should be of the form:
    /// <code>    
    ///   <ManagedThreadPool>
    ///     <NumberOfThreads>5</NumberOfThreads>
    ///   </ManagedThreadPool>
    /// </code>
    /// </summary>
    /// <param name="config_">The XML config</param>
    public static void Configure(XmlElement config_)
    {
      XmlNode node = config_.SelectSingleNode("NumberOfThreads");
      if (node != null)
      {
        int num = int.Parse(node.InnerText);
        if (num > 0 && num < 20)
        {
          ManagedThreadPoolConfig.MaxWorkerThreads = num;
        }
        else
        {
          throw new ArgumentException(num+" is not a valid number of threads (0 < n <= 20)");
        }
      }
    }

		/// <summary>
		/// Empties the work queue of any queued work items.  Resets all threads in the pool.
		/// </summary>
		public static void Reset()
		{
      _instance.Reset( );
    }
    #endregion

    private class FixedThreadPool : DynamicThreadPool
    {
      public FixedThreadPool(int maxThreads_)
        : base(NAME, maxThreads_, maxThreads_, Timeout.Infinite)
      {
      }

      public void Reset()
      {
        lock (SyncRoot)
        {
           ClearThreadPool();
          MinThreads = MaxThreads = ManagedThreadPoolConfig.MaxWorkerThreads;
          InitializeThreadPool();
        }
      }

    }
  }
}