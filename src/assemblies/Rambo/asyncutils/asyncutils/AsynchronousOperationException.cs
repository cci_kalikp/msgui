using System;

namespace MorganStanley.MSDesktop.Rambo.AsyncUtils
{
  /// <summary>
  /// Indicates that the exception originally occured during an asynchronous operation.
  /// </summary>
  public class AsynchronousOperationException : Exception
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="AsynchronousOperationException"/> class.
    /// </summary>
    /// <param name="innerException_">The inner exception_.</param>
    public AsynchronousOperationException(Exception innerException_)
      : base("An exception was thrown during an asynchronous operation.", innerException_)
    {

    }
  }
}