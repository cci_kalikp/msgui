﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Threading;

namespace MorganStanley.MSDesktop.Rambo.AsyncUtils
{
  /// <summary>
  /// Settings for <see cref="Asynchronizer"/> and its child classes (e.g. <see cref="AsynchronizerBackgroundWorker"/> and <see cref="MultiplexAsynchronizer"/>)
  /// </summary>
  /// <remarks>
  /// By default, the Asynchronizer performs all callbacks on a background thread, unless the Asynchronizer was initialized on an <see cref="ISynchronizeInvoke"/>-inheriting object.
  /// 
  /// By setting <see name="UseSyncContext"/> to true, the originating context is utilized in order to call back on the GUI thread if it originated there, regardless of what type of object began it.
  /// Also, if the originating class inherits <see cref="ISynchronizeInvoke"/>, the GUI thread will always be called back on, regardless of where it came from.
  /// Lastly, if the call originated on a background thread, it will be returned on a background thread (not necessarily the same one it started on).
  /// </remarks>

  public struct AsyncSettings
  {
    public static readonly AsyncSettings Empty = new AsyncSettings();

    public bool UseSyncContext { get; private set; }

    /// <summary>
    /// Initializes a new instance of the <see cref="AsyncSettings"/> class.
    /// </summary>
    /// <param name="useSyncContext_">If set to <c>true</c> UI-invoked processes will automatically return on the UI thread. For backward compatibility, the default is false. See remarks.</param>
    public AsyncSettings(bool useSyncContext_) : this()
    {
      UseSyncContext = useSyncContext_;
    }
  }
}
