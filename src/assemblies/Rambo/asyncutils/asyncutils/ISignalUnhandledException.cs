using System;

namespace MorganStanley.MSDesktop.Rambo.AsyncUtils
{
  /// <summary>
  /// An interface for signaling an unhandled excption has occured.
  /// </summary>
  public interface ISignalUnhandledException
  {
    event UnhandledExceptionEventHandler OnUnhandledException;
  }
}