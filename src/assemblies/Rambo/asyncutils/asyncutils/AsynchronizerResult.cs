using System;
using System.ComponentModel;
using System.Reflection;
using System.Threading;
using MorganStanley.MSDotNet.MSLog;

namespace MorganStanley.MSDesktop.Rambo.AsyncUtils
{
  /// <summary>
  /// An implementation of <see cref="IAsyncResult"/> to be used in the <see cref="AsyncCallback"/> in 
  /// asynchronous operations invoked by <see cref="Asynchronizer.BeginInvoke"/> calls. 
  /// </summary>
  /// <remarks>
  /// Lifted from http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dnwinforms/html/asyncui.asp
  /// Slightly modified for handling exceptions and null AsyncCallback delegates
  /// </remarks>
  public class AsynchronizerResult : IAsyncResult
  {
    #region Readonly & Static Fields

    private readonly AsyncCallback _asyncCallback;
    private readonly ISynchronizeInvoke _asynchronizerParent;

    private readonly ManualResetEvent _handle;
    private static readonly Logger _logger = new Logger(typeof (AsynchronizerResult));
    private readonly object[] _resultArgs;
    private readonly Delegate _resultMethod;
    private readonly object _state;

    #endregion

    #region Fields

    private bool _completed;
    private Exception _exception;
    private object _returnValue;

    #endregion

    #region Constructors

    internal AsynchronizerResult(Delegate method, object[] args,
                                 AsyncCallback callBack, object asyncState, ISynchronizeInvoke async)
    {
      _state = asyncState;
      _resultMethod = method;
      _resultArgs = args;
      _handle = new ManualResetEvent(false);
      _completed = false;
      _asyncCallback = callBack;
      _asynchronizerParent = async;
    }

    internal AsynchronizerResult(Delegate method, object[] args,
                                 AsyncCallback callBack, object asyncState, ISynchronizeInvoke async,
                                 SynchronizationContext context) :
                                   this(method, args, callBack, asyncState, async)
    {
      SynchronizationContext = context;
    }

    #endregion

    #region Instance Properties

    /// <summary>
    /// Gets the <see cref="Asynchronizer"/> that is used to produce this result.  
    /// </summary>
    /// <value>The <see cref="Asynchronizer"/> that is used to produce this result.</value>
    public Asynchronizer AsynchronizerParent
    {
      get { return _asynchronizerParent as Asynchronizer; }
    }

    /// <summary>
    /// Gets exception that occurred while invoking the asynchronous operation.
    /// </summary>
    /// <value>The invoke exception.</value>
    public Exception InvokeException
    {
      get { return _exception; }

      protected set { _exception = value; }
    }

    /// <summary>
    /// Gets the value returned by the asynchronous operation.
    /// </summary>
    /// <value>The method returned value.</value>
    /// <exception cref="AsynchronousOperationException">Rethrows any exception that ocurred while invoking the asynchronous operation.</exception>
    public object MethodReturnedValue
    {
      get
      {
        if (_exception != null)
        {
          throw new AsynchronousOperationException(_exception);
        }
        return _returnValue;
      }
      protected set { _returnValue = value; }
    }

    /// <summary>
    /// The <see cref="ISynchronizeInvoke"/> instance that is used to produce this result. Consider using <see cref="AsynchronizerParent"/> instead, to remove the need to cast.
    /// </summary>
    /// <value>The <see cref="ISynchronizeInvoke"/> instance that is used to produce this result.</value>
    /// <seealso cref="AsynchronizerParent"/>
    [Obsolete("SynchronizeInvoke will be eventually retired in favor of AsynchronizerParent")]
    public ISynchronizeInvoke SynchronizeInvoke
    {
      get { return _asynchronizerParent; }
    }

    internal SynchronizationContext SynchronizationContext
    {
      get; private set;
    }

    #endregion

    #region Instance Methods

    /// <summary>
    /// Performs the singular method invocation, captures any exceptions thrown by the call in the 
    /// local exception field, and calls <see cref="OnComplete()"/> to process the result
    /// </summary>
    protected virtual void DoInvokeStrategy()
    {
      try
      {
        if (_logger.CanLogPriority(MSLogPriority.Debug))
        {
          _logger.LogDebug("DoInvokeStrategy",
                           string.Format("ASYNC OPERATION: Invoking '{0}.{1}' on a background thread",
                                         _resultMethod.Method.DeclaringType, _resultMethod.Method.Name));
        }
        MethodReturnedValue = _resultMethod.DynamicInvoke(_resultArgs);
      }
      catch (TargetInvocationException invEx)
      {
        _exception = invEx.InnerException;

        if (_exception is ThreadAbortException)
        {
          Thread.ResetAbort();
        }
      }
      catch (ThreadAbortException thEx)
      {
        _exception = thEx;
        Thread.ResetAbort();
      }
      catch (Exception ex)
      {
        _exception = ex;
      }

      OnComplete();
    }

    protected void OnComplete()
    {
      _handle.Set();
      _completed = true;
      if (_asyncCallback == null) return;
      if (_logger.CanLogPriority(MSLogPriority.Debug))
      {
        string resultMethod = _resultMethod == null
                                ? "Multiplex operation"
                                : string.Format("'{0}.{1}'", _resultMethod.Method.DeclaringType,
                                                _resultMethod.Method.Name);
        string asyncCallbackMethod = string.Format("'{0}.{1}'", _asyncCallback.Method.DeclaringType,
                                                   _asyncCallback.Method.Name);
        _logger.LogDebug("OnComplete",
                         string.Format("ASYNC OPERATION: Completed {0}, Calling back {1} ", resultMethod,
                                       asyncCallbackMethod));
      }
      if (SynchronizationContext != null && !(this is ManualAsyncResult))
      {
        ThreadingUtils.ThreadSafeContextInvoke(SynchronizationContext, _asyncCallback, this);
      }
      else
      {
        ThreadingUtils.ThreadSafeInvoke(_asyncCallback, this);
      }
    }

    /// <summary>
    /// Called on a thread from the thread pool to execute the method
    /// </summary>
    /// <param name="state">Unused state parameter (here to match .NET's WaitCallback mechanism).  Any state that is part of the processing are already contained in this class's <see cref="_state"/> member.</param>
    internal void DoInvoke(object state)
    {
      DoInvokeStrategy();
    }

    #endregion

    #region IAsyncResult Members

    ///<summary>
    ///Gets an indication whether the asynchronous operation has completed.
    ///</summary>
    ///
    ///<returns>
    ///true if the operation is complete; otherwise, false.
    ///</returns>
    ///<filterpriority>2</filterpriority>
    public bool IsCompleted
    {
      get { return _completed; }
    }

    ///<summary>
    ///Gets a <see cref="T:System.Threading.WaitHandle"></see> that is used to wait for an asynchronous operation to complete.
    ///</summary>
    ///
    ///<returns>
    ///A <see cref="T:System.Threading.WaitHandle"></see> that is used to wait for an asynchronous operation to complete.
    ///</returns>
    ///<filterpriority>2</filterpriority>
    public WaitHandle AsyncWaitHandle
    {
      get { return _handle; }
    }

    ///<summary>
    ///Gets a user-defined object that qualifies or contains information about an asynchronous operation.
    ///</summary>
    ///
    ///<returns>
    ///A user-defined object that qualifies or contains information about an asynchronous operation.
    ///</returns>
    ///<filterpriority>2</filterpriority>
    public object AsyncState
    {
      get { return _state; }
    }

    ///<summary>
    ///Gets an indication of whether the asynchronous operation completed synchronously.
    ///</summary>
    ///
    ///<returns>
    ///true if the asynchronous operation completed synchronously; otherwise, false.
    ///</returns>
    ///<filterpriority>2</filterpriority>
    public bool CompletedSynchronously
    {
      get { return false; }
    }

    #endregion
  }
}