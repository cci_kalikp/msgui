#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/ConcordConfiguration/ConfigurationSection.cs#3 $
  $DateTime: 2013/01/18 15:24:18 $
    $Change: 813214 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion

using System;
using System.Xml;

namespace MorganStanley.IED.Concord.Configuration
{
  /// <include file='xmldocs/ConfigurationSection.cs.xml' path='doc/doc[@for="ConfigurationSection"]/*'/>
	public class ConfigurationSection : ConfigurationIdentity
	{
    #region Declarations
    private XmlNode[] _readOnlyNodes;
    private XmlNode _readWriteNode;
    #endregion Declarations

    #region Constructors
    /// <include file='xmldocs/ConfigurationSection.cs.xml' path='doc/doc[@for="ConfigurationSection.ConfigurationSection"]/*'/>
    public ConfigurationSection(string name_, string version_, XmlNode[] readOnlySections_, XmlNode readWriteSection_) : base(name_, version_)
		{
      _readOnlyNodes = readOnlySections_;
      _readWriteNode = readWriteSection_;
		}
    #endregion Constructors

    #region Public Properties
    /// <include file='xmldocs/ConfigurationSection.cs.xml' path='doc/doc[@for="ConfigurationSection.ReadOnlyNodes"]/*'/>
    public XmlNode[] ReadOnlyNodes
    {
      get { return _readOnlyNodes; }
    }

    /// <include file='xmldocs/ConfigurationSection.cs.xml' path='doc/doc[@for="ConfigurationSection.ReadWriteNode"]/*'/>
    public XmlNode ReadWriteNode
    {
      get { return _readWriteNode; }
    }
    #endregion Public Properties
	}
}
