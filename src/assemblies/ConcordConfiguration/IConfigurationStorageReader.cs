#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/ConcordConfiguration/IConfigurationStorageReader.cs#3 $
  $DateTime: 2013/01/18 15:24:18 $
    $Change: 813214 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion

using System;
using System.Collections;
using System.Collections.Specialized;
using System.Xml;

namespace MorganStanley.IED.Concord.Configuration
{
  /// <include file='xmldocs/IConfigurationStorageReader.cs.xml' path='doc/doc[@for="IConfigurationStorageReader"]/*'/>
  public interface IConfigurationStorageReader
  {
    #region Methods
    /// <include file='xmldocs/IConfigurationStorageReader.cs.xml' path='doc/doc[@for="IConfigurationStorageReader.Initialize"]/*'/>
    void Initialize(string settings_);

    void Initialize(string settings_, XmlNode config_);

    /// <include file='xmldocs/IConfigurationStorageReader.cs.xml' path='doc/doc[@for="IConfigurationStorageReader.ReadConfiguration"]/*'/>
    ConfigurationSection ReadConfiguration(string name_, string version_);

    /// <include file='xmldocs/IConfigurationStorageReader.cs.xml' path='doc/doc[@for="IConfigurationStorageReader.ReadConfigurations"]/*'/>
    ConfigurationSection[] ReadConfigurations(ConfigurationIdentityCollection configEntries_);
    #endregion Methods
	}
}
