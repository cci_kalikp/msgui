#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002-2011 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/ConcordConfiguration/ConfigurationManager.cs#7 $
  $DateTime: 2014/03/28 12:45:34 $
    $Change: 873853 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion

using System;
using System.Collections;
using System.Configuration;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Xml;

using MorganStanley.IED.Concord.Logging;

namespace MorganStanley.IED.Concord.Configuration
{
  /// <include file='xmldocs/ConfigurationManager.cs.xml' path='doc/doc[@for="ConfigurationManager"]/*'/>
  public class ConfigurationManager
  {
    #region Declarations
    private static int _initComplete = 0;
    internal Hashtable _configs;
    private static ConfigurationManager _configMgrImpl;
    private static object _defaultProvider;
    private static Type _defaultProviderType;
    private static object _lock = new object();
    private static string _profileString;
    private static Hashtable _providers;
    private static XmlNode _configurationNode;
    #endregion Declarations

    #region Constructors
    static ConfigurationManager()
    {
      _configMgrImpl = new ConfigurationManager();
      _providers = new Hashtable();
    }

    private ConfigurationManager()
    {
      _configs = new Hashtable();
    }
    #endregion Constructors

    #region Public Methods

    /// <include file='xmldocs/ConfigurationManager.cs.xml' path='doc/doc[@for="ConfigurationManager.Reset"]/*'/>
    public static void Reset()
    {
      #region Logging
      if (Log.IsLogLevelEnabled(LogLevel.Info))
      {
        LogLayer.Info.SetLocation("ConfigurationManager", "Reset").Write(Res.GetString(Res.LOG_CONFIGMGR_RESET));
      }
      #endregion Logging

      lock (_lock)
      {
        _profileString = null;
        _defaultProviderType = null;
        _configurationNode = null;
        _defaultProvider = null;
        _configMgrImpl = new ConfigurationManager();
        _providers = new Hashtable();
        Interlocked.Exchange(ref _initComplete, 0);
      }
    }

    /// <summary>
    /// Clears the Config cache.
    /// </summary>
    public static void ClearConfigCache()
    {
      #region Logging
      if (Log.IsLogLevelEnabled(LogLevel.Info))
      {
        LogLayer.Info.SetLocation("ConfigurationManager", "ClearConfigCache").WriteFormat(Res.GetString(Res.LOG_CLEAR_CONFIG_CACHE));
      }
      #endregion Logging

      lock (_lock)
      {
        _configMgrImpl._configs.Clear();
      }
    }

    /// <include file='xmldocs/ConfigurationManager.cs.xml' path='doc/doc[@for="ConfigurationManager.Initialize"]/*'/>
    public static void Initialize(string profileString_, Type defaultProvider_)
    {
      #region Validate Arguments
      if (defaultProvider_ == null) throw new ArgumentNullException("_defaultProviderType");
      #endregion Validate Arguments

      #region Logging
      if (Log.IsLogLevelEnabled(LogLevel.Info))
      {
        LogLayer.Info.SetLocation("ConfigurationManager", "Initialize").WriteFormat(Res.GetString(Res.LOG_CONFIGMGR_INITIALIZE), profileString_, defaultProvider_);
      }
      #endregion Logging

      lock (_lock)
      {
        _profileString = profileString_;

        _defaultProviderType = defaultProvider_;

        _configurationNode = XmlConfigurationHandler.GetConfigNode("concordConfiguration");

        _defaultProvider = _configMgrImpl.GetProvider(_defaultProviderType);

        Interlocked.Exchange(ref _initComplete, 1);
      }
    }
    /// <include file='xmldocs/ConfigurationManager.cs.xml' path='doc/doc[@for="ConfigurationManager.Initialize2"]/*'/>
    public static void Initialize(string profileString_)
    {
      #region Logging
      if (Log.IsLogLevelEnabled(LogLevel.Info))
      {
        LogLayer.Info.SetLocation("ConfigurationManager", "Initialize(string)").WriteFormat(Res.GetString(Res.LOG_CONFIGMGR_INITIALIZE), profileString_, "");
      }
      #endregion Logging

      lock (_lock)
      {
        _profileString = profileString_;

        //get provider configs from app.config
        _defaultProviderType = _configMgrImpl.GetDefaultStorageProvider();

        _configurationNode = XmlConfigurationHandler.GetConfigNode("concordConfiguration");

        _defaultProvider = _configMgrImpl.GetProvider(_defaultProviderType);

        Interlocked.Exchange(ref _initComplete, 1);
      }
    }
    /// <include file='xmldocs/ConfigurationManager.cs.xml' path='doc/doc[@for="ConfigurationManager.Initialize3"]/*'/>
    public static void Initialize(string profileString_, XmlNode config_)
    {
      #region Logging
      if (Log.IsLogLevelEnabled(LogLevel.Info))
      {
        LogLayer.Info.SetLocation("ConfigurationManager", "Initialize(string, XmlNode)").WriteFormat(Res.GetString(Res.LOG_CONFIGMGR_INITIALIZE), profileString_, config_.OuterXml);
      }
      #endregion Logging

      lock (_lock)
      {
        _profileString = profileString_;

        //get provider configs from app.config
        _defaultProviderType = _configMgrImpl.GetDefaultStorageProvider();

        //used passed-in node as config node
        _configurationNode = config_;

        _defaultProvider = _configMgrImpl.GetProvider(_defaultProviderType);

        Interlocked.Exchange(ref _initComplete, 1);
      }


    }
    /// <include file='xmldocs/ConfigurationManager.cs.xml' path='doc/doc[@for="ConfigurationManager.Initialize4"]/*'/>
    public static void Initialize(string profileString_, Type
      defaultProvider_, XmlNode config_)
    {
      #region Logging
      if (Log.IsLogLevelEnabled(LogLevel.Info))
      {
        LogLayer.Info.SetLocation("ConfigurationManager", "Initialize(string,Type,XmlNode)").WriteFormat(Res.GetString(Res.LOG_CONFIGMGR_INITIALIZE), profileString_, defaultProvider_);
      }
      #endregion Logging

      lock (_lock)
      {
        _profileString = profileString_;

        _defaultProviderType = defaultProvider_;

        //used passed-in node as config node
        _configurationNode = config_;

        _defaultProvider = _configMgrImpl.GetProvider(_defaultProviderType);

        Interlocked.Exchange(ref _initComplete, 1);
      }
    }

    /// <include file='xmldocs/ConfigurationManager.cs.xml' path='doc/doc[@for="ConfigurationManager.LoadConfigurations"]/*'/>
    public static void LoadConfigurations(ConfigurationIdentityCollection configEntries_)
    {
      #region Validate Arguments
      if (configEntries_ == null) throw new ArgumentNullException("configEntries_");
      #endregion Validate Arguments

      if (_initComplete == 0)
      {
        #region Logging
        if (Log.IsLogLevelEnabled(LogLevel.Error))
        {
          LogLayer.Error.SetLocation("ConfigurationManager", "LoadConfigurations").Write(Res.GetString(Res.EXCEPTION_MUST_INITIALIZE));
        }
        #endregion Logging

        throw new ConfigurationException(Res.GetString(Res.EXCEPTION_MUST_INITIALIZE));
      }

      lock (_lock)
      {
        // providerConfigs contains a collection of ConfigurationIdentities for
        // keyed by provider Type.  This allows for the batching of config
        // requests to each provider.
        Hashtable providerConfigs = new Hashtable();
        providerConfigs.Add(_defaultProviderType, new ConfigurationIdentityCollection());

        // Cycle through configuration requests - create and initialize
        // any non-default config storage providers
        foreach (ConfigurationIdentity configIdentity in configEntries_)
        {
          Type providerType = (configIdentity.ProviderType != null) ? configIdentity.ProviderType : _defaultProviderType;

          //check to see if this section is in our app.config list indicating a custom provider
          if (_configurationNode != null)
          {
            XmlNode nodeConfig = _configurationNode.SelectSingleNode("configSections/configSection[@name='" + configIdentity.Name + "']", null);
            if (nodeConfig != null)
            {
              if (nodeConfig.Attributes["provider"] != null && nodeConfig.Attributes["provider"].Value != null)
              {
                providerType = Type.GetType(nodeConfig.Attributes["provider"].Value);
                if (providerType == null)
                  providerType = _defaultProviderType;
                else
                {
                  #region Logging
                  if (Log.IsLogLevelEnabled(LogLevel.Debug))
                  {
                    LogLayer.Debug.SetLocation("ConfigurationManager", "LoadConfigurations").WriteFormat(Res.GetString(Res.LOG_CONFIG_SPECIFIC_PROVIDER), providerType.ToString(), configIdentity.Name);
                  }
                  #endregion Logging

                }
              }

            }
          }

          _configMgrImpl.GetProvider(configIdentity.ProviderType);

          if (providerConfigs[providerType] == null)
          {
            // Maintain a collection of config identities for each provider so we can perform
            // batch "ReadConfigurations" requests.
            providerConfigs.Add(providerType, new ConfigurationIdentityCollection());
          }

          // Add the configuration identity to the proper provider/config collection
          ((ConfigurationIdentityCollection)providerConfigs[providerType]).Add(configIdentity);
        }

        // Enumerate through all providers and call ReadConfigurations to perform
        // a batch request
        foreach (Type providerType in providerConfigs.Keys)
        {
          #region Logging
          if (Log.IsLogLevelEnabled(LogLevel.Info))
          {
            StringBuilder configNames = new StringBuilder();
            foreach (ConfigurationIdentity identity in (ConfigurationIdentityCollection)providerConfigs[providerType])
            {
              configNames.AppendFormat(" {0}:{1}", identity.Name, identity.Version);
            }
            LogLayer.Info.SetLocation("ConfigurationManager", "LoadConfigurations").WriteFormat(Res.GetString(Res.LOG_PROVIDER_RETRIEVE_CONFIGS_BATCH), configNames.ToString().Trim(), providerType);
          }
          #endregion Logging

          if (_providers[providerType] == null)
          {
            _configMgrImpl.GetProvider(providerType);
          }


          ConfigurationSection[] configs = ((IConfigurationStorageReader)_providers[providerType]).ReadConfigurations((ConfigurationIdentityCollection)providerConfigs[providerType]);

          // For each config section returned from the batch query, create
          // the corresponding section handler and add the item to the config cache.
          foreach (ConfigurationSection configSection in configs)
          {
            try
            {
              object config = null, handler = null;

              // Check to see if a custom handler is requested
              Type handlerType = null;

              ConfigurationIdentity configEntry = configEntries_[configSection.Name, configSection.Version] as ConfigurationIdentity;

              if (configEntry != null)
              {
                handlerType = configEntry.HandlerType;
              }

              if (handlerType != null) // Create the custom handler
              {
                #region Logging
                if (Log.IsLogLevelEnabled(LogLevel.Debug))
                {
                  LogLayer.Debug.SetLocation("ConfigurationManager", "LoadConfigurations").WriteFormat(Res.GetString(Res.LOG_CONFIG_HANDLER_TYPE), handlerType, configSection.Name);
                }
                #endregion Logging

                handler = _configMgrImpl.GetInstanceFromType(handlerType);
              }
              else // Use the default configuator section handler
              {
                handlerType = typeof(Configurator);

                #region Logging
                if (Log.IsLogLevelEnabled(LogLevel.Debug))
                {
                  LogLayer.Debug.SetLocation("ConfigurationManager", "LoadConfigurations").WriteFormat(Res.GetString(Res.LOG_CONFIG_HANDLER_TYPE), handlerType, configSection.Name);
                }
                #endregion Logging

                handler = new Configurator();
              }

              // Enumerate through the hierachy of configs from top down, and call
              // IConfigurationSectionHandler.Create() one at a time.  The return
              // value is passed in as the parent on each subsequent call.  This 
              // "chains" the hierachy together and the custom config section
              // can merge the levels how it sees fit.
              if (configSection.ReadOnlyNodes != null)
              {
                foreach (XmlNode node in configSection.ReadOnlyNodes)
                {
                  if (node != null)
                  {
                    config = ((IConfigurationSectionHandler)handler).Create(config, null, node);
                  }
                }
              }

              // If the provider returned a read-write section, pass it to the handler
              // through IConfigurationSectionHandlerWrite.Create().  If the handler
              // is read-only (ie: only implements IConfigurationSectionHandler), pass
              // it in through IConfigurationSectionHandler.Create().
              if (handler is IConfigurationSectionHandlerWriter)
              {
                config = ((IConfigurationSectionHandlerWriter)handler).Create(config, null, configSection.ReadWriteNode);
              }
              else if (configSection.ReadWriteNode != null)
              {
                config = ((IConfigurationSectionHandler)handler).Create(config, null, configSection.ReadWriteNode);
              }

              // Add the configuration to the cache.  Encapsulate the configuration with
              // a ConfigurationEntry instance, so we can maintain items about the config section
              // such as it's provider, handler, name, value, etc.
              _configMgrImpl._configs[configSection.Name + configSection.Version] = new ConfigurationEntry(configSection.Name, configSection.Version, handlerType, providerType, (IConfigurationSectionHandler)handler, config);

              #region Logging
              if (Log.IsLogLevelEnabled(LogLevel.Debug))
              {
                LogLayer.Debug.SetLocation("ConfigurationManager", "LoadConfigurations").WriteFormat(Res.GetString(Res.LOG_ADD_CONFIG_TO_CACHE), configSection.Name);
              }
              #endregion Logging
            }
            catch (Exception ex_)
            {
              #region Logging
              if (Log.IsLogLevelEnabled(LogLevel.Error))
              {
                LogLayer.Error.SetLocation("ConfigurationManager", "LoadConfigurations").WriteFormat(Res.GetString(Res.EXCEPTION_UNABLE_TO_LOAD_CONFIG), ex_, configSection.Name, configSection.Version);
              }
              #endregion Logging

              throw new ConfiguratorException(String.Format(Res.GetString(Res.EXCEPTION_UNABLE_TO_LOAD_CONFIG), configSection.Name, configSection.Version), ex_);
            }
          } // foreach (ConfigurationSection configSection in configs) 
        } // foreach (Type providerType in providerConfigs.Keys) 
      } // lock
    }

    /// <include file='xmldocs/ConfigurationManager.cs.xml' path='doc/doc[@for="ConfigurationManager.GetConfig2"]/*'/>
    public static object GetConfig(ConfigurationIdentity configIdentity_)
    {
      #region Logging
      if (Log.IsLogLevelEnabled(LogLevel.Debug))
      {
        LogLayer.Debug.SetLocation("ConfigurationManager", "GetConfig").WriteFormat(Res.GetString(Res.LOG_GETCONFIG), configIdentity_.Name, configIdentity_.Version, configIdentity_.HandlerType, configIdentity_.ProviderType);
      }
      #endregion Logging

      if (_initComplete == 0)
      {
        #region Logging
        if (Log.IsLogLevelEnabled(LogLevel.Error))
        {
          LogLayer.Error.SetLocation("ConfigurationManager", "GetConfig").Write(Res.GetString(Res.EXCEPTION_MUST_INITIALIZE));
        }
        #endregion Logging

        throw new InvalidOperationException(Res.GetString(Res.EXCEPTION_MUST_INITIALIZE));
      }

      string name = configIdentity_.Name;
      string version = configIdentity_.Version;

      lock (_lock)
      {
        ConfigurationEntry entry = _configMgrImpl._configs[name + version] as ConfigurationEntry;

        if (entry == null)
        {
          #region Logging
          if (Log.IsLogLevelEnabled(LogLevel.Debug))
          {
            LogLayer.Debug.SetLocation("ConfigurationManager", "GetConfig").WriteFormat(Res.GetString(Res.LOG_GETCONFIG_CACHE_MISS), name, version);
          }
          #endregion Logging

          Type providerType = _configMgrImpl.GetProviderType(configIdentity_);

          Type handlerType = configIdentity_.HandlerType;
          object handler = null;

          if (handlerType != null) // Create the custom handler
          {
            #region Logging
            if (Log.IsLogLevelEnabled(LogLevel.Debug))
            {
              LogLayer.Debug.SetLocation("ConfigurationManager", "LoadConfigurations").WriteFormat(Res.GetString(Res.LOG_CONFIG_HANDLER_TYPE), handlerType, name);
            }
            #endregion Logging

            handler = _configMgrImpl.GetInstanceFromType(handlerType);
          }
          else // Use the default configuator section handler
          {
            handlerType = typeof(Configurator);

            #region Logging
            if (Log.IsLogLevelEnabled(LogLevel.Debug))
            {
              LogLayer.Debug.SetLocation("ConfigurationManager", "LoadConfigurations").WriteFormat(Res.GetString(Res.LOG_CONFIG_HANDLER_TYPE), handlerType, name);
            }
            #endregion Logging

            handler = new Configurator();
          }

          IConfigurationStorageReader provider = (IConfigurationStorageReader)_configMgrImpl.GetProvider(providerType);

          #region Logging
          if (Log.IsLogLevelEnabled(LogLevel.Debug))
          {
            LogLayer.Debug.SetLocation("ConfigurationManager", "GetConfig").WriteFormat(Res.GetString(Res.LOG_PROVIDER_RETRIEVE_CONFIG), name, version, providerType);
          }
          #endregion Logging

          // Provider will never be null.  Exceptions will be thrown from GetProvider()
          // if the provider can't be loaded

          ConfigurationSection configSection = provider.ReadConfiguration(name, version);

          if (configSection != null)
          {
            object config = null;

            if (configSection.ReadOnlyNodes != null)
            {
              foreach (XmlNode node in configSection.ReadOnlyNodes)
              {
                config = ((IConfigurationSectionHandler)handler).Create(config, null, node);
              }
            }

            if (handler is IConfigurationSectionHandlerWriter)
            {
              config = ((IConfigurationSectionHandlerWriter)handler).Create(config, null, configSection.ReadWriteNode);
            }
            else
            {
              config = ((IConfigurationSectionHandler)handler).Create(config, null, configSection.ReadWriteNode);
            }

            // Add the config to the config cache
            _configMgrImpl._configs[configIdentity_.Name + configIdentity_.Version] = new ConfigurationEntry(name, version, handlerType, providerType, (IConfigurationSectionHandler)handler, config);

            #region Logging
            if (Log.IsLogLevelEnabled(LogLevel.Debug))
            {
              LogLayer.Debug.SetLocation("ConfigurationManager", "LoadConfigurations").WriteFormat(Res.GetString(Res.LOG_ADD_CONFIG_TO_CACHE), name);
            }
            #endregion Logging

            return config;
          }
          else
          {
            #region Logging
            if (Log.IsLogLevelEnabled(LogLevel.Debug))
            {
              LogLayer.Debug.SetLocation("ConfigurationManager", "GetConfig").WriteFormat(Res.GetString(Res.LOG_CONFIG_NOT_FOUND), name, version, providerType);
            }
            #endregion Logging

            return null;
          }
        }
        else
        {
          #region Logging
          if (Log.IsLogLevelEnabled(LogLevel.Debug))
          {
            LogLayer.Debug.SetLocation("ConfigurationManager", "GetConfig").WriteFormat(Res.GetString(Res.LOG_GETCONFIG_CACHE_HIT), name, version);
          }
          #endregion Logging

          return entry.Configuration;
        }
      }
    }

    /// <include file='xmldocs/ConfigurationManager.cs.xml' path='doc/doc[@for="ConfigurationManager.GetConfig"]/*'/>
    public static object GetConfig(string name_)
    {
      return GetConfig(name_, null);
    }

    /// <include file='xmldocs/ConfigurationManager.cs.xml' path='doc/doc[@for="ConfigurationManager.GetConfig1"]/*'/>
    public static object GetConfig(string name_, string version_)
    {
      return GetConfig(new ConfigurationIdentity(name_, version_));
    }

    internal static void WriteConfig(IConfigurationSectionHandlerWriter handler_)
    {
      if (_initComplete == 0)
      {
        #region Logging
        if (Log.IsLogLevelEnabled(LogLevel.Error))
        {
          LogLayer.Error.SetLocation("ConfigurationManager", "WriteConfig").Write(Res.GetString(Res.EXCEPTION_MUST_INITIALIZE));
        }
        #endregion Logging

        throw new InvalidOperationException(Res.GetString(Res.EXCEPTION_MUST_INITIALIZE));
      }

      lock (_lock)
      {
        foreach (string key in _configMgrImpl._configs.Keys)
        {
          ConfigurationEntry entry = (ConfigurationEntry)_configMgrImpl._configs[key];
          if (Object.ReferenceEquals(handler_, entry.SectionHandler))
          {
            XmlNode node = handler_.Serialize(handler_);
            ((IConfigurationStorageWriter)_providers[entry.ProviderType]).WriteConfiguration(entry.Name, entry.Version, node);
          }
        }
      }
    }

    /// <include file='xmldocs/ConfigurationManager.cs.xml' path='doc/doc[@for="ConfigurationManager.WriteConfig"]/*'/>
    public static void WriteConfig(string name_, object value_)
    {
      WriteConfig(name_, null, value_);
    }

    /// <include file='xmldocs/ConfigurationManager.cs.xml' path='doc/doc[@for="ConfigurationManager.WriteConfig1"]/*'/>
    public static void WriteConfig(string name_, string version_, object value_)
    {
      #region Logging
      if (Log.IsLogLevelEnabled(LogLevel.Info))
      {
        LogLayer.Info.SetLocation("ConfigurationManager", "WriteConfig").WriteFormat(Res.GetString(Res.LOG_WRITE_CONFIG), name_, version_);
      }
      #endregion Logging

      if (_initComplete == 0)
      {
        #region Logging
        if (Log.IsLogLevelEnabled(LogLevel.Error))
        {
          LogLayer.Error.SetLocation("ConfigurationManager", "WriteConfig").Write(Res.GetString(Res.EXCEPTION_MUST_INITIALIZE));
        }
        #endregion Logging

        throw new InvalidOperationException(Res.GetString(Res.EXCEPTION_MUST_INITIALIZE));
      }

      if (IsReadOnly(name_, version_))
      {
        #region Logging
        if (Log.IsLogLevelEnabled(LogLevel.Error))
        {
          LogLayer.Error.SetLocation("ConfigurationManager", "WriteConfig").Write(Res.GetString(Res.EXCEPTION_CONFIG_IS_READONLY));
        }
        #endregion Logging

        throw new InvalidOperationException(Res.GetString(Res.EXCEPTION_CONFIG_IS_READONLY));
      }

      lock (_lock)
      {
        ConfigurationEntry entry = (ConfigurationEntry)_configMgrImpl._configs[name_ + version_];
        if (entry != null)
        {
          XmlNode node = ((IConfigurationSectionHandlerWriter)entry.SectionHandler).Serialize(value_);
          ((IConfigurationStorageWriter)_providers[entry.ProviderType]).WriteConfiguration(name_, version_, node);
        }
        else
        {
          #region Logging
          if (Log.IsLogLevelEnabled(LogLevel.Error))
          {
            LogLayer.Error.SetLocation("ConfigurationManager", "WriteConfig").WriteFormat(Res.GetString(Res.LOG_CONFIG_DOES_NOT_EXIST), name_, version_);
          }
          #endregion Logging

          throw new ConfiguratorException(String.Format(Res.GetString(Res.EXCEPTION_CONFIG_DOES_NOT_EXIST), name_, version_));
        }
      }
    }

    /// <include file='xmldocs/ConfigurationManager.cs.xml' path='doc/doc[@for="ConfigurationManager.DeleteConfig"]/*'/>
    public static void DeleteConfig(string name_)
    {
      ConfigurationManager.DeleteConfig(name_, null);
    }

    /// <include file='xmldocs/ConfigurationManager.cs.xml' path='doc/doc[@for="ConfigurationManager.DeleteConfig1"]/*'/>
    public static void DeleteConfig(string name_, string version_)
    {
      ConfigurationManager.DeleteConfig(new ConfigurationIdentity(name_, version_));
    }

    /// <include file='xmldocs/ConfigurationManager.cs.xml' path='doc/doc[@for="ConfigurationManager.DeleteConfig2"]/*'/>
    public static void DeleteConfig(ConfigurationIdentity configIdentity_)
    {
      if (Log.IsLogLevelEnabled(LogLevel.Debug))
      {
        LogLayer.Debug.SetLocation("ConfigurationManager", "DeleteConfig").WriteFormat(
          "DeleteConfig Config '{0}', Version='{1}', Handler='{2}', Provider='{3}'", configIdentity_.Name, configIdentity_.Version,
          configIdentity_.HandlerType, configIdentity_.ProviderType);
      }

      if (_initComplete == 0)
      {
        if (Log.IsLogLevelEnabled(LogLevel.Error))
        {
          LogLayer.Error.SetLocation("ConfigurationManager", "DeleteConfig").Write(
            Res.GetString(Res.EXCEPTION_MUST_INITIALIZE));
        }

        throw new InvalidOperationException(Res.GetString(Res.EXCEPTION_MUST_INITIALIZE));
      }

      string name = configIdentity_.Name;
      string version = configIdentity_.Version;

      lock (_lock)
      {
        ConfigurationEntry entry = _configMgrImpl._configs[name + version] as ConfigurationEntry;
        if (entry != null)
        {
          Type providerType = _configMgrImpl.GetProviderType(configIdentity_);
          IConfigurationStorageDelete configDeleter = _configMgrImpl.GetProvider(providerType) as IConfigurationStorageDelete;

          if (configDeleter != null)
          {
            configDeleter.DeleteConfiguration(name, version);
            _configMgrImpl._configs.Remove(name + version);

            if (Log.IsLogLevelEnabled(LogLevel.Info))
            {
              LogLayer.Info.SetLocation("ConfigurationManager", "DeleteConfig").WriteFormat(
                "Config Delete succeeded for '{0}', Version='{1}'", name, version);
            }
          }
          else
          {
            if (Log.IsLogLevelEnabled(LogLevel.Info))
            {
              LogLayer.Info.SetLocation("ConfigurationManager", "DeleteConfig").WriteFormat(
                "Provider does not support delete for '{0}', Version='{1}'", name, version);
            }
          }
        }
        else
        {
          if (Log.IsLogLevelEnabled(LogLevel.Info))
          {
            LogLayer.Info.SetLocation("ConfigurationManager", "DeleteConfig").WriteFormat(
              "Config not found in cache. DeleteConfig action not taken for '{0}', Version='{1}'", name, version);
          }
        }
      }
    }    
    
    /// <include file='xmldocs/ConfigurationManager.cs.xml' path='doc/doc[@for="ConfigurationManager.RefreshConfig"]/*'/>
    public static void RefreshConfig(string name_)
    {
      ConfigurationManager.RefreshConfig(name_, null);
    }

    /// <include file='xmldocs/ConfigurationManager.cs.xml' path='doc/doc[@for="ConfigurationManager.RefreshConfig1"]/*'/>
    public static void RefreshConfig(string name_, string version_)
    {
      ConfigurationManager.RefreshConfig(new ConfigurationIdentity(name_, version_));
    }

    /// <include file='xmldocs/ConfigurationManager.cs.xml' path='doc/doc[@for="ConfigurationManager.RefreshConfig2"]/*'/>
    public static void RefreshConfig(ConfigurationIdentity configIdentity_)
    {
      if (Log.IsLogLevelEnabled(LogLevel.Debug))
      {
        LogLayer.Debug.SetLocation("ConfigurationManager", "RefreshConfig").WriteFormat(
          "Refresh Config '{0}', Version='{1}', Handler='{2}', Provider='{3}'", configIdentity_.Name, configIdentity_.Version,
          configIdentity_.HandlerType, configIdentity_.ProviderType);
      }

      if (_initComplete == 0)
      {
        if (Log.IsLogLevelEnabled(LogLevel.Error))
        {
          LogLayer.Error.SetLocation("ConfigurationManager", "RefreshConfig").Write(
            Res.GetString(Res.EXCEPTION_MUST_INITIALIZE));
        }

        throw new InvalidOperationException(Res.GetString(Res.EXCEPTION_MUST_INITIALIZE));
      }

      string name = configIdentity_.Name;
      string version = configIdentity_.Version;

      lock (_lock)
      {
        ConfigurationEntry entry = _configMgrImpl._configs[name + version] as ConfigurationEntry;
        if (entry != null)
        {
          Type providerType = _configMgrImpl.GetProviderType(configIdentity_);
          IConfigurationStorageRefresh configRefresher = _configMgrImpl.GetProvider(providerType) as IConfigurationStorageRefresh;

          if (configRefresher != null)
          {
            configRefresher.RefreshConfiguration(name, version);
            _configMgrImpl._configs.Remove(name + version);

            if (Log.IsLogLevelEnabled(LogLevel.Info))
            {
              LogLayer.Info.SetLocation("ConfigurationManager", "RefreshConfig").WriteFormat(
                "Config Refresh succeeded for '{0}', Version='{1}'", name, version);
            }
          }
          else
          {
            if (Log.IsLogLevelEnabled(LogLevel.Info))
            {
              LogLayer.Info.SetLocation("ConfigurationManager", "RefreshConfig").WriteFormat(
                "Provider does not support refresh for '{0}', Version='{1}'", name, version);
            }
          }
        }
        else
        {
          if (Log.IsLogLevelEnabled(LogLevel.Info))
          {
            LogLayer.Info.SetLocation("ConfigurationManager", "RefreshConfig").WriteFormat(
              "Config not found in cache. Refresh action not taken for '{0}', Version='{1}'", name, version);
          }
        }
      }
    }

    /// <include file='xmldocs/ConfigurationManager.cs.xml' path='doc/doc[@for="ConfigurationManager.IsReadOnly"]/*'/>
    public static bool IsReadOnly(string name_)
    {
      return IsReadOnly(name_, null);
    }

    /// <include file='xmldocs/ConfigurationManager.cs.xml' path='doc/doc[@for="ConfigurationManager.IsReadOnly1"]/*'/>
    public static bool IsReadOnly(string name_, string version_)
    {
      if (_initComplete == 0)
      {
        #region Logging
        if (Log.IsLogLevelEnabled(LogLevel.Error))
        {
          LogLayer.Error.SetLocation("ConfigurationManager", "IsReadOnly").Write(Res.GetString(Res.EXCEPTION_MUST_INITIALIZE));
        }
        #endregion Logging

        throw new InvalidOperationException(Res.GetString(Res.EXCEPTION_MUST_INITIALIZE));
      }

      lock (_lock)
      {
        ConfigurationEntry entry = (ConfigurationEntry)_configMgrImpl._configs[name_ + version_];

        if (entry == null)
        {
          #region Logging
          if (Log.IsLogLevelEnabled(LogLevel.Error))
          {
            LogLayer.Error.SetLocation("ConfigurationManager", "IsReadOnly").WriteFormat(Res.GetString(Res.LOG_CONFIG_DOES_NOT_EXIST), name_, version_);
          }
          #endregion Logging

          throw new ConfiguratorException(String.Format(Res.GetString(Res.EXCEPTION_CONFIG_DOES_NOT_EXIST), name_, version_));
        }

        if (entry.SectionHandler != null && (entry.SectionHandler is IConfigurationSectionHandlerWriter))
        {
          return false;
        }

        return true;
      }
    }

    /// <include file='xmldocs/ConfigurationManager.cs.xml' path='doc/doc[@for="ConfigurationManager.EnumerateConfigs"]/*'/>
    public static ArrayList EnumerateConfigs(string pattern_)
    {
      ArrayList retVal = null;

      #region Logging
      if (Log.IsLogLevelEnabled(LogLevel.Debug))
      {
        LogLayer.Debug.SetLocation("ConfigurationManager", "EnumerateConfigs").Write(Res.GetString(Res.LOG_ENUMERATE_CONFIGS));
      }
      #endregion Logging

      foreach (object provider in _providers)
      {
        IConfigurationEnumeratesConfigs enumerates = ((System.Collections.DictionaryEntry)provider).Value as IConfigurationEnumeratesConfigs;
        if (enumerates != null)
        {
          retVal = enumerates.EnumerateAvailableConfigs(pattern_);
          return retVal;
        }
      }

      return retVal;
    }
    #endregion Public Methods

    #region Private Methods
    private Type GetProviderType(ConfigurationIdentity configIdentity_)
    {
      Type providerType = configIdentity_.ProviderType ?? _defaultProviderType;

      //check to see if this section is in our app.config list indicating a custom provider
      if (_configurationNode != null)
      {
        XmlNode nodeConfig = _configurationNode.SelectSingleNode("configSections/configSection[@name='" + configIdentity_.Name + "']", null);
        if (nodeConfig != null)
        {
          if (nodeConfig.Attributes["provider"] != null && nodeConfig.Attributes["provider"].Value != null)
          {
            providerType = Type.GetType(nodeConfig.Attributes["provider"].Value);
            if (providerType == null)
            {
              providerType = _defaultProviderType;
            }
            else
            {
              #region Logging
              if (Log.IsLogLevelEnabled(LogLevel.Debug))
              {
                LogLayer.Debug.SetLocation("ConfigurationManager", "GetConfig").WriteFormat(Res.GetString(Res.LOG_CONFIG_SPECIFIC_PROVIDER), providerType.ToString(), configIdentity_.Name);
              }
              #endregion Logging
            }
          }
        }
      }

      return providerType;
    }

    private object GetProvider(Type providerType_)
    {
      #region Logging
      if (Log.IsLogLevelEnabled(LogLevel.Debug))
      {
        LogLayer.Debug.SetLocation("ConfigurationManager", "GetProvider").WriteFormat(Res.GetString(Res.LOG_PROVIDER_REQUEST), providerType_);
      }
      #endregion Logging

      if (providerType_ == null)
      {
        return _defaultProvider;
      }
      else if (_providers[providerType_] != null)
      {
        return _providers[providerType_];
      }

      object providerInstance = null;

      try
      {
        // Validate that the default provider implements the proper interface
        if (providerType_.GetInterface("IConfigurationStorageReader") == null)
        {
          #region Logging
          if (Log.IsLogLevelEnabled(LogLevel.Error))
          {
            LogLayer.Error.SetLocation("ConfigurationManager", "GetProvider").WriteFormat(Res.GetString(Res.EXCEPTION_MUST_IMPLEMENT_INTERFACE), providerType_, "IConfigurationStorageReader");
          }
          #endregion Logging

          throw new ConfiguratorException(String.Format(Res.GetString(Res.EXCEPTION_MUST_IMPLEMENT_INTERFACE), providerType_, "IConfigurationStorageReader"));
        }

        #region Logging
        if (Log.IsLogLevelEnabled(LogLevel.Debug))
        {
          LogLayer.Debug.SetLocation("ConfigurationManager", "GetProvider").WriteFormat(Res.GetString(Res.LOG_PROVIDER_CREATEINSTANCE), providerType_);
        }
        #endregion Logging

        providerInstance = GetInstanceFromType(providerType_);
        _providers.Add(providerType_, providerInstance);
      }
      catch (TargetInvocationException ex_)
      {
        #region Logging
        if (Log.IsLogLevelEnabled(LogLevel.Error))
        {
          LogLayer.Error.SetLocation("ConfigurationManager", "GetProvider").WriteFormat(Res.GetString(Res.EXCEPTION_MUST_IMPLEMENT_INTERFACE), ex_, providerType_, "IConfigurationStorageReader");
        }
        #endregion Logging

        throw new ConfiguratorException(String.Format(Res.GetString(Res.EXCEPTION_UNABLE_TO_CREATE_INSTANCE), providerType_), ex_);
      }

      try
      {
        #region Logging
        if (Log.IsLogLevelEnabled(LogLevel.Debug))
        {
          LogLayer.Debug.SetLocation("ConfigurationManager", "GetProvider").WriteFormat(Res.GetString(Res.LOG_PROVIDER_INITIALIZE), providerType_);
        }
        #endregion Logging

        if (_configurationNode != null)
        {
          //this isn't so good but only alternative is to instantiate all the types contained
          //as strings in app.config section to compare types directly
          string typeName = providerType_.ToString();
          string assemblyName = providerType_.Assembly.FullName;
          int comma = assemblyName.IndexOf(',', 0);
          if (comma > 0)
          {
            assemblyName = assemblyName.Substring(0, comma);
          }

          string fullName = typeName + ", " + assemblyName;


          XmlNode nodeConfig = _configurationNode.SelectSingleNode("storageProviders/storageProvider[@type='" + fullName + "']", null);
          if (nodeConfig == null)
          {
            //try without space
            fullName = typeName + "," + assemblyName;
            nodeConfig = _configurationNode.SelectSingleNode("storageProviders/storageProvider[@type='" + fullName + "']", null);
          }
          ((IConfigurationStorageReader)providerInstance).Initialize(_profileString, nodeConfig);

        }
        else
          ((IConfigurationStorageReader)providerInstance).Initialize(_profileString);

      }
      catch (Exception ex_)
      {
        #region Logging
        if (Log.IsLogLevelEnabled(LogLevel.Error))
        {
          LogLayer.Error.SetLocation("ConfigurationManager", "GetProvider").WriteFormat(Res.GetString(Res.EXCEPTION_ERROR_INITIALIZING_STORAGE_READER), ex_, providerType_);
        }
        #endregion Logging

        throw new ConfiguratorException(String.Format(Res.GetString(Res.EXCEPTION_ERROR_INITIALIZING_STORAGE_READER), providerType_), ex_);
      }


      return providerInstance;
    }

    private object GetInstanceFromType(Type type_)
    {
      try
      {
        #region Logging
        if (Log.IsLogLevelEnabled(LogLevel.Debug))
        {
          LogLayer.Debug.SetLocation("ConfigurationManager", "GetInstanceFromType").WriteFormat(Res.GetString(Res.LOG_CREATE_INSTANCE), type_);
        }
        #endregion Logging

        return Activator.CreateInstance(type_);
      }
      catch (TargetInvocationException ex_)
      {
        #region Logging
        if (Log.IsLogLevelEnabled(LogLevel.Error))
        {
          LogLayer.Error.SetLocation("ConfigurationManager", "GetProvider").WriteFormat(Res.GetString(Res.EXCEPTION_UNABLE_TO_CREATE_INSTANCE), ex_, type_);
        }
        #endregion Logging

        throw new ConfiguratorException(String.Format(Res.GetString(Res.EXCEPTION_UNABLE_TO_CREATE_INSTANCE), type_), ex_);
      }
    }

    private Type GetDefaultStorageProvider()
    {

      //get the default storage provider from app.config
      XmlNode configNode = XmlConfigurationHandler.GetConfigNode("concordConfiguration");

      Type defaultStore = null;

      string defaultConfigStore;
      if (configNode != null)
      {
        if (configNode.Attributes["defaultProvider"] != null && configNode.Attributes["defaultProvider"].Value != null)
        {
          defaultConfigStore = configNode.Attributes["defaultProvider"].Value;
          if (defaultConfigStore != null && defaultConfigStore.Length > 0)
          {
            Type type = Type.GetType(defaultConfigStore);

            if (type == null)
            {
              if (Log.IsLogLevelEnabled(LogLevel.Error))
              {
                LogLayer.Error.SetLocation("ConfigurationManager", "GetDefaultStorageProvider").WriteFormat(Res.GetString(Res.EXCEPTION_UNABLE_TO_CREATE_INSTANCE), defaultConfigStore);
              }

              throw new Exception(String.Format(Res.GetString(Res.EXCEPTION_UNABLE_TO_CREATE_INSTANCE), defaultConfigStore));
            }

            defaultStore = type;
          }
        }
        else
        {
          defaultStore = typeof(FileConfigurationStore);
        }
      }
      else
      {
        defaultStore = typeof(FileConfigurationStore);
      }

      if (defaultStore == null)
      {
        if (Log.IsLogLevelEnabled(LogLevel.Error))
        {
          LogLayer.Error.SetLocation("ConfigurationManager", "GetDefaultStorageProvider").WriteFormat(Res.GetString(Res.EXCEPTION_DEFAULT_PROVIDER));
        }

        throw new Exception(String.Format(Res.GetString(Res.EXCEPTION_DEFAULT_PROVIDER)));
      }


      return defaultStore;

    }
    #endregion Private Methods
  }
}
