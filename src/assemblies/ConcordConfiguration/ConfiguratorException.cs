#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/ConcordConfiguration/ConfiguratorException.cs#3 $
  $DateTime: 2013/01/18 15:24:18 $
    $Change: 813214 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion

using System;

namespace MorganStanley.IED.Concord.Configuration
{
  /// <include file='xmldocs/ConfiguratorException.cs.xml' path='doc/doc[@for="ConfiguratorException"]/*'/>
  public class ConfiguratorException : ApplicationException
  {
    /// <include file='xmldocs/ConfiguratorException.cs.xml' path='doc/doc[@for="ConfiguratorException.ConfiguratorException"]/*'/>
    public ConfiguratorException(string message_) : base(message_) 
    {
    }

    /// <include file='xmldocs/ConfiguratorException.cs.xml' path='doc/doc[@for="ConfiguratorException.ConfiguratorException1"]/*'/>
    public ConfiguratorException(string message_, Exception innerException_) : base(message_, innerException_) 
    {
    }
  }
}
