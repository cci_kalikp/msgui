#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/ConcordConfiguration/IConfigurationSectionHandlerWriter.cs#3 $
  $DateTime: 2013/01/18 15:24:18 $
    $Change: 813214 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion

using System;
using System.Configuration;
using System.Xml;

namespace MorganStanley.IED.Concord.Configuration
{
  /// <include file='xmldocs/IConfigurationSectionHandlerWriter.cs.xml' path='doc/doc[@for="IConfigurationSectionHandlerWriter"]/*'/>
	public interface IConfigurationSectionHandlerWriter : IConfigurationSectionHandler
	{
    #region Methods
    /// <include file='xmldocs/IConfigurationSectionHandlerWriter.cs.xml' path='doc/doc[@for="IConfigurationSectionHandlerWriter.Create"]/*'/>
    object Create(object parent_, object configContext_, XmlNode section_);

    /// <include file='xmldocs/IConfigurationSectionHandlerWriter.cs.xml' path='doc/doc[@for="IConfigurationSectionHandlerWriter.Serialize"]/*'/>
    XmlNode Serialize(object value_);
    #endregion Methods
	}
}
