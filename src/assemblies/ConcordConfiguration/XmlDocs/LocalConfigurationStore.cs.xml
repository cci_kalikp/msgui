﻿<?xml version="1.0" encoding="utf-8" ?> 

<!--
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/ConcordConfiguration/XmlDocs/LocalConfigurationStore.cs.xml#3 $
  $DateTime: 2013/01/18 15:24:18 $
    $Change: 813214 $
    $Author: smulovic $
_____________________________________________________________________________*/
//-->

<doc>
  <doc for="LocalConfigurationStore">
    <summary>
      Represents a local configuration file based storage provider.
    </summary>
    <remarks>
      <para>
        Read-only sections are read from the applications main configuration file.  Customized settings
        are written to <i>appname.exe</i>.local.config.
      </para>
      <para>
        A section entry must be added to the application configuration for each configuration section that
        is to be used.
        <code>
&lt;configSections&gt;
  &lt;section name="sectionname" type="MorganStanley.IED.Concord.Configuration.XmlConfigurationHandler, IED.ConcordConfiguration" /&gt;
&lt;/configSections&gt;
        </code>
      </para>
    </remarks>
    <threadsafety>
      Any public static members of this type are safe for multithreaded operations.  Any instance
      members are not guaranteed to be thread safe.
    </threadsafety>
  </doc>
  
  <doc for="LocalConfigurationStore.LocalConfigurationStore">
    <summary>
      Initializes a new instance of the <see cref="LocalConfigurationStore">LocalConfigurationStore</see> class.
    </summary>
  </doc>
  
  <doc for="LocalConfigurationStore.Initialize">
    <summary>
      Initializes the <see cref="LocalConfigurationStore">LocalConfigurationStore</see> with the profile
      string.
    </summary>
    <param name="settings_">The profile settings.</param>
  </doc>

  <doc for="LocalConfigurationStore.ReadConfiguration">
    <summary>
      Reads the configuration section from the local application configuration file.
    </summary>
    <param name="name_">The name of the configuration to write.</param>
    <param name="version_">The version of the configuration to write.</param>
    <returns>
      A <see cref="ConfigurationSection">ConfigurationSection</see> containing the configuration content.
    </returns>
    <remarks>
      All profile settings are ignored.
    </remarks>
  </doc>
  
  <doc for="LocalConfigurationStore.ReadConfigurations">
    <summary>
      Reads the configuration sections from the local application configuration file.
    </summary>
    <param name="configs_">The list of configurations to retrieve.</param>
    <returns>
      An array of <see cref="ConfigurationSection">ConfigurationSection</see>.
    </returns>
    <remarks>
      All profiles settings are ignored.
    </remarks>
  </doc>
  
  <doc for="LocalConfigurationStore.WriteConfiguration">
    <summary>
      Writes the configuration section to the local application configuration file.
    </summary>
    <param name="name_">The name of the configuration to write.</param>
    <param name="version_">The version of the configuration to write.</param>
    <param name="node_">
      An <see cref="System.Xml.XmlNode">XmlNode</see> containing the configuration to write.
    </param>
    <remarks>
      All configurations sections are written to a single file in the executable directory named
      <i>appname.exe</i>.local.config.
    </remarks>
  </doc>
  
  <doc for="XmlConfigurationHandler">
    <summary>
      Provides a means of reading an XmlNode in a configuration section.
    </summary>
  </doc>

  <doc for="XmlConfigurationHandler.Create">
    <summary>
      Returns an <see cref="System.Xml.XmlNode">XmlNode</see> containing the entire configuration section.
    </summary>
    <param name="parent_">The configuration settings in a corresponding parent configuration section.</param>
    <param name="configContext_">This paramter is reserved and is a null referece.</param>
    <param name="section_">
      An <see cref="System.Xml.XmlNode">XmlNode</see> that contains configuration information from the
      configuration file.
    </param>
    <returns>
      An <see cref="System.Xml.XmlNode">XmlNode</see> containing the configuration section.
    </returns>
  </doc>

  <doc for="XmlConfigurationHandler.GetConfigNode">
    <summary>
      Use instead of <see cref="ConfigurationSettings.GetConfig"/>.
      Workaround for .net 4 security exception caused by using the obsolete <see cref="ConfigurationSettings.GetConfig"/> while running from a network share.
      See: http://social.msdn.microsoft.com/Forums/en-US/clr/thread/1e14f665-10a3-426b-a75d-4e66354c5522
      Returns an <see cref="System.Xml.XmlNode">XmlNode</see> containing the entire configuration section
    </summary>
    <param name="sectionName">The section name to retrieve from the app.config</param>
    <returns>
      An <see cref="System.Xml.XmlNode">XmlNode</see> containing the configuration section.
    </returns>
  </doc>
</doc>