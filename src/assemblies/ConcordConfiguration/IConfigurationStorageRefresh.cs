#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2011 Morgan Stanley, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/ConcordConfiguration/IConfigurationStorageRefresh.cs#3 $
  $DateTime: 2013/01/18 15:24:18 $
    $Change: 813214 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion File Info Header

using System;

namespace MorganStanley.IED.Concord.Configuration
{
  /// <include file='xmldocs/IConfigurationStorageRefresh.cs.xml' path='doc/doc[@for="IConfigurationStorageRefresh"]/*'/>
  public interface IConfigurationStorageRefresh
  {
    /// <include file='xmldocs/IConfigurationStorageRefresh.cs.xml' path='doc/doc[@for="IConfigurationStorageReader.RefreshConfiguration"]/*'/>
    void RefreshConfiguration(string name_, string version_);
  }
}