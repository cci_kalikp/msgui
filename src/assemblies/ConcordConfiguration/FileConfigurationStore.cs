#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/ConcordConfiguration/FileConfigurationStore.cs#4 $
  $DateTime: 2014/03/30 06:28:27 $
    $Change: 873945 $
    $Author: caijin $
_____________________________________________________________________________*/
#endregion

using System;
using System.Collections;
using System.Collections.Specialized;
using System.IO;
using System.Xml;

using MorganStanley.IED.Concord.Logging;

using MorganStanley.MSDotNet.MSXml;

namespace MorganStanley.IED.Concord.Configuration
{
  /// <include file='xmldocs/FileConfigurationStore.cs.xml' path='doc/doc[@for="FileConfigurationStore"]/*'/>
  public class FileConfigurationStore : IConfigurationStorageWriter
  {
    #region Declarations
    private NameValueCollection _settings;
    private string _systemBaseDir;
    private string _userBaseDir;
    private MSXmlProcessorFactoryCollection _xmlProcessorCollection = new MSXmlProcessorFactoryCollection();
    private MSXmlHashtableParameterResolver _envVarParamResolver= new MSXmlHashtableParameterResolver();
    #endregion Declarations

    #region Constructors
    /// <include file='xmldocs/FileConfigurationStore.cs.xml' path='doc/doc[@for="FileConfigurationStore.FileConfigurationStore"]/*'/>
    public FileConfigurationStore()
    {
      ConcordXIncludeProcessorFactory includeProcessorFactory = new ConcordXIncludeProcessorFactory();
      _xmlProcessorCollection.Add(includeProcessorFactory);
      _envVarParamResolver.Hashtable=new Hashtable(System.Environment.GetEnvironmentVariables());
    }
    #endregion Constructors

    #region Implementation of IConfigurationStorageWriter
    /// <include file='xmldocs/FileConfigurationStore.cs.xml' path='doc/doc[@for="FileConfigurationStore.WriteConfiguration"]/*'/>
    public virtual void WriteConfiguration(string name_, string version_, System.Xml.XmlNode node_)
    {
      #region Logging
      if (Log.IsLogLevelEnabled(LogLevel.Debug)) 
      {
        LogLayer.Debug.SetLocation("FileConfigurationStore", "WriteConfig").WriteFormat(Res.GetString(Res.LOG_WRITE_CONFIG), name_, version_, node_.OuterXml);
      }
      #endregion Logging

      string file = GetUserFilePath(_settings, name_, version_);
      
      using (FileStream fs = new FileStream(file, FileMode.Create, FileAccess.Write, FileShare.Read))
      {
        try 
        {
          node_.OwnerDocument.Save(fs);
        } 
        catch (Exception ex_) 
        {
          #region Logging
          if (Log.IsLogLevelEnabled(LogLevel.Error)) 
          {
            LogLayer.Error.SetLocation("FileConfigurationStore", "WriteConfig").WriteFormat(Res.GetString(Res.LOG_ERROR_SAVING_CONFIG), ex_, name_, version_, node_.OuterXml);
          }
          #endregion Logging

          throw new ConfiguratorException("Error writing configuration", ex_);
        }
      }
    }
    #endregion Implementation of IConfigurationStorageWriter

    #region Implementation of IConfigurationStorageReader
    /// <include file='xmldocs/FileConfigurationStore.cs.xml' path='doc/doc[@for="FileConfigurationStore.Initialize"]/*'/>
    public virtual void Initialize(string settings_) 
    {
      #region Logging
      if (Log.IsLogLevelEnabled(LogLevel.Debug)) 
      {
        LogLayer.Debug.SetLocation("FileConfigurationStore", "Initialize").WriteFormat(Res.GetString(Res.LOG_FILESTORE_INITIALIZE), settings_);
      }
      #endregion Logging

      _settings = this.ParseProfileString(settings_);

      if (_userBaseDir == null || _userBaseDir.Length == 0)
      {
        _userBaseDir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
      }

      if (_systemBaseDir == null || _systemBaseDir.Length == 0)
      {
        _systemBaseDir = AppDomain.CurrentDomain.BaseDirectory;
      }

      _userBaseDir = _userBaseDir.TrimEnd('\\');
      _systemBaseDir = _systemBaseDir.TrimEnd('\\');
    }

    public virtual void Initialize(string settings_, XmlNode config_) 
    {
      _settings = this.ParseProfileString(settings_);

      //read the node pertaining to this provider from app.config
      if (config_ != null)
      {
        XmlNode node = config_.SelectSingleNode("SystemBaseDir",null);
        if (node != null)
        {
          string sData = ParseDirectoryString(node.InnerXml);
          if (sData != null && sData.Length > 0)
          {
            _systemBaseDir = sData;
          }
        }

        node = config_.SelectSingleNode("UserBaseDir",null);
        if (node != null)
        {
          string sData = ParseDirectoryString(node.InnerXml);
          if (sData != null && sData.Length > 0)
          {
            _userBaseDir = sData; 
          }
        }
      }

      if (_userBaseDir == null || _userBaseDir.Length == 0)
      {
        _userBaseDir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
      }

      if (_systemBaseDir == null || _systemBaseDir.Length == 0)
      {
        _systemBaseDir = AppDomain.CurrentDomain.BaseDirectory;
      }

      _userBaseDir = _userBaseDir.TrimEnd('\\');
      _systemBaseDir = _systemBaseDir.TrimEnd('\\');
    }

    /// <include file='xmldocs/FileConfigurationStore.cs.xml' path='doc/doc[@for="FileConfigurationStore.ReadConfiguration"]/*'/>
    public virtual ConfigurationSection ReadConfiguration(string name_, string version_)
    {
      #region Logging
      if (Log.IsLogLevelEnabled(LogLevel.Debug)) 
      {
        LogLayer.Debug.SetLocation("FileConfigurationStore", "ReadConfiguration").WriteFormat(Res.GetString(Res.LOG_STORE_READCONFIG), name_, version_);
      }
      #endregion Logging

      XmlNode userFile = GetUserFile(_settings, name_, version_);
      XmlNode[] systemFiles = GetFiles(_settings, name_, version_);

      return new ConfigurationSection(name_, version_, systemFiles, userFile);
    }

    /// <include file='xmldocs/FileConfigurationStore.cs.xml' path='doc/doc[@for="FileConfigurationStore.ReadConfigurations"]/*'/>
    public virtual ConfigurationSection[] ReadConfigurations(ConfigurationIdentityCollection configs_)
    {
      ConfigurationSection[] configSections = new ConfigurationSection[configs_.Count];

      int index = 0;
      foreach (ConfigurationIdentity config in configs_) 
      {
        configSections[index++] = this.ReadConfiguration(config.Name, config.Version);
      }

      return configSections;
    }
    #endregion Implementation of IConfigurationStorageReader

    #region Protected Methods
    protected virtual XmlElement GetDocumentFromFile(string file_)
    {
      XmlDocument doc = new XmlDocument();

      try
      {
        StreamReader sr = File.OpenText(file_);

        System.Xml.XmlTextReader textReader = new XmlTextReader(sr);
        MorganStanley.MSDotNet.MSXml.MSXmlProcessingReader reader = new MSXmlProcessingReader(textReader, _xmlProcessorCollection, _envVarParamResolver);

        doc.Load(reader);
        sr.Close();

        return doc.DocumentElement;
      }
      catch(Exception e_)
      {
        if (Log.IsLogLevelEnabled(LogLevel.Warning))
        {
          LogLayer.Warning.SetLocation("FileConfigurationStore", "GetDocumentFromFile").Write("Error reading file: " + file_, e_);
        }
      }

      // null implies that there was an error while getting the document.
      return null;
    }

    protected virtual XmlNode GetUserFile(NameValueCollection settings_, string name_, string version_)
    {
      string file = this.GetUserFilePath(settings_, name_, version_);

      #region Logging
      string logMessage = null;
      if (Log.IsLogLevelEnabled(LogLevel.Debug)) 
      {
        logMessage = String.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file);
      }
      #endregion Logging

      if (File.Exists(file)) 
      {
        #region Logging
        if (Log.IsLogLevelEnabled(LogLevel.Debug)) 
        {
          LogLayer.Debug.SetLocation("FileConfigurationStore", "GetUserFile").WriteFormat("{0}, Exists='True'", logMessage);
        }
        #endregion Logging

        return GetDocumentFromFile(file) ?? CreateEmptyConfig(name_);
      } 
      else 
      {
        #region Logging
        if (Log.IsLogLevelEnabled(LogLevel.Debug)) 
        {
          LogLayer.Debug.SetLocation("FileConfigurationStore", "GetUserFile").WriteFormat("{0}, Exists='False', Configuration was created", logMessage);
        }
        #endregion Logging

        return CreateEmptyConfig(name_);
      }
    }

    protected virtual string GetUserFilePath(NameValueCollection settings_, string name_, string version_)
    {
      string app = settings_[Configurator.CONFIG_SETTING_APP_KEY];
      string user = settings_[Configurator.CONFIG_SETTING_USER_KEY];
      string reg = settings_[Configurator.CONFIG_SETTING_REGION_KEY];
      string env = settings_[Configurator.CONFIG_SETTING_ENV_KEY];
      string role = settings_[Configurator.CONFIG_SETTING_ROLE_KEY];      
      
      string dir = GetUserDirectory(user, app, reg, env, role);

      if (version_ != null) 
      {
        return String.Format(@"{0}\{1}.{2}.config", dir, name_, version_);
      } 
      else 
      {
        return String.Format(@"{0}\{1}.config", dir, name_);
      }
    }

    protected virtual XmlNode[] GetFiles(NameValueCollection settings_, string name_, string version_)
    {
      ArrayList _configs = new ArrayList();

      string app = settings_[Configurator.CONFIG_SETTING_APP_KEY];
      string reg = settings_[Configurator.CONFIG_SETTING_REGION_KEY];
      string env = settings_[Configurator.CONFIG_SETTING_ENV_KEY];
      string role = settings_[Configurator.CONFIG_SETTING_ROLE_KEY];      
      string dir = null, file = null;
      
      if (version_ != null) 
      {
        dir = GetTemplateDirectory(null, null, null);
        file = String.Format(@"{0}\{1}.{2}.config", dir, name_, version_);
        if (File.Exists(file)) 
        {
          #region Logging
          if (Log.IsLogLevelEnabled(LogLevel.Debug)) 
          {
            LogLayer.Debug.SetLocation("FileConfigurationStore", "GetFiles").WriteFormat("{0}, Exists='True'", String.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file));
          }
          #endregion Logging

          _configs.Add(GetDocumentFromFile(file));
        } 
        else 
        {
          #region Logging
          if (Log.IsLogLevelEnabled(LogLevel.Debug)) 
          {
            LogLayer.Debug.SetLocation("FileConfigurationStore", "GetFiles").WriteFormat("{0}, Exists='False'", String.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file));
          }
          #endregion Logging
        }

        dir = GetTemplateDirectory(reg, null, null);
        file = String.Format(@"{0}\{1}.{2}.config", dir, name_, version_);
        if (File.Exists(file)) 
        {
          #region Logging
          if (Log.IsLogLevelEnabled(LogLevel.Debug)) 
          {
            LogLayer.Debug.SetLocation("FileConfigurationStore", "GetFiles").WriteFormat("{0}, Exists='True'", String.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file));
          }
          #endregion Logging

          _configs.Add(GetDocumentFromFile(file));
        }
        else 
        {
          #region Logging
          if (Log.IsLogLevelEnabled(LogLevel.Debug)) 
          {
            LogLayer.Debug.SetLocation("FileConfigurationStore", "GetFiles").WriteFormat("{0}, Exists='False'", String.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file));
          }
          #endregion Logging
        }

        dir = GetTemplateDirectory(reg, env, null);
        file = String.Format(@"{0}\{1}.{2}.config", dir, name_, version_);
        if (File.Exists(file)) 
        {
          #region Logging
          if (Log.IsLogLevelEnabled(LogLevel.Debug)) 
          {
            LogLayer.Debug.SetLocation("FileConfigurationStore", "GetFiles").WriteFormat("{0}, Exists='True'", String.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file));
          }
          #endregion Logging

          _configs.Add(GetDocumentFromFile(file));
        }                
        else 
        {
          #region Logging
          if (Log.IsLogLevelEnabled(LogLevel.Debug)) 
          {
            LogLayer.Debug.SetLocation("FileConfigurationStore", "GetFiles").WriteFormat("{0}, Exists='False'", String.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file));
          }
          #endregion Logging
        }
        
        if (role != null)
        {
          dir = GetTemplateDirectory(reg, env, role);
          file = String.Format(@"{0}\{1}.{2}.config", dir, name_, version_);
          if (File.Exists(file)) 
          {
            #region Logging
            if (Log.IsLogLevelEnabled(LogLevel.Debug)) 
            {
              LogLayer.Debug.SetLocation("FileConfigurationStore", "GetFiles").WriteFormat("{0}, Exists='True'", String.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file));
            }
            #endregion Logging

            _configs.Add(GetDocumentFromFile(file));
          }                
          else 
          {
            #region Logging
            if (Log.IsLogLevelEnabled(LogLevel.Debug)) 
            {
              LogLayer.Debug.SetLocation("FileConfigurationStore", "GetFiles").WriteFormat("{0}, Exists='False'", String.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file));
            }
            #endregion Logging
          }        
        }
      } 
      else 
      {
        dir = GetTemplateDirectory(null, null, null);
        file = String.Format(@"{0}\{1}.config", dir, name_);
        if (File.Exists(file)) 
        {
          #region Logging
          if (Log.IsLogLevelEnabled(LogLevel.Debug)) 
          {
            LogLayer.Debug.SetLocation("FileConfigurationStore", "GetFiles").WriteFormat("{0}, Exists='True'", String.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file));
          }
          #endregion Logging

          _configs.Add(GetDocumentFromFile(file));
        }
        else 
        {
          #region Logging
          if (Log.IsLogLevelEnabled(LogLevel.Debug)) 
          {
            LogLayer.Debug.SetLocation("FileConfigurationStore", "GetFiles").WriteFormat("{0}, Exists='False'", String.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file));
          }
          #endregion Logging
        }

        dir = GetTemplateDirectory(reg, null, null);
        file = String.Format(@"{0}\{1}.config", dir, name_);
        if (File.Exists(file)) 
        {
          #region Logging
          if (Log.IsLogLevelEnabled(LogLevel.Debug)) 
          {
            LogLayer.Debug.SetLocation("FileConfigurationStore", "GetFiles").WriteFormat("{0}, Exists='True'", String.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file));
          }
          #endregion Logging

          _configs.Add(GetDocumentFromFile(file));
        }
        else 
        {
          #region Logging
          if (Log.IsLogLevelEnabled(LogLevel.Debug)) 
          {
            LogLayer.Debug.SetLocation("FileConfigurationStore", "GetFiles").WriteFormat("{0}, Exists='False'", String.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file));
          }
          #endregion Logging
        }

        dir = GetTemplateDirectory(reg, env, null);
        file = String.Format(@"{0}\{1}.config", dir, name_);
        if (File.Exists(file)) 
        {
          #region Logging
          if (Log.IsLogLevelEnabled(LogLevel.Debug)) 
          {
            LogLayer.Debug.SetLocation("FileConfigurationStore", "GetFiles").WriteFormat("{0}, Exists='True'", String.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file));
          }
          #endregion Logging

          _configs.Add(GetDocumentFromFile(file));
        }
        else 
        {
          #region Logging
          if (Log.IsLogLevelEnabled(LogLevel.Debug)) 
          {
            LogLayer.Debug.SetLocation("FileConfigurationStore", "GetFiles").WriteFormat("{0}, Exists='False'", String.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file));
          }
          #endregion Logging
        }
        
        if (role != null)
        {
          dir = GetTemplateDirectory(reg, env, role);
          file = String.Format(@"{0}\{1}.config", dir, name_);
          if (File.Exists(file)) 
          {
            #region Logging
            if (Log.IsLogLevelEnabled(LogLevel.Debug)) 
            {
              LogLayer.Debug.SetLocation("FileConfigurationStore", "GetFiles").WriteFormat("{0}, Exists='True'", String.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file));
            }
            #endregion Logging

            _configs.Add(GetDocumentFromFile(file));
          }
          else 
          {
            #region Logging
            if (Log.IsLogLevelEnabled(LogLevel.Debug)) 
            {
              LogLayer.Debug.SetLocation("FileConfigurationStore", "GetFiles").WriteFormat("{0}, Exists='False'", String.Format(Res.GetString(Res.LOG_FILESTORE_GETFILE), file));
            }
            #endregion Logging
          }        
        }
      }

      if (_configs.Count > 0) 
      {
        return (XmlNode[]) _configs.ToArray(typeof(XmlNode));
      } 
      else 
      {
        #region Logging
        if (Log.IsLogLevelEnabled(LogLevel.Debug)) 
        {
          LogLayer.Debug.SetLocation("FileConfigurationStore", "GetFiles").Write(Res.GetString(Res.LOG_FILESTORE_NOFILES_FOUND));
        }
        #endregion Logging

        return null;
      }
    }

    protected virtual string GetUserDirectory(string username_, string app_, string reg_, string env_, string role_)
    {
      string dir = String.Empty;
    
      if (role_ != null)
      {
        dir = String.Format(@"{0}\Morgan Stanley\{1}\Configuration\{2}\{3}", _userBaseDir, app_, reg_, env_);      
      }
      else
      {
        dir = String.Format(@"{0}\Morgan Stanley\{1}\Configuration\{2}\{3}\{4}", _userBaseDir, app_, reg_, env_, role_);
      }

      if (!System.IO.Directory.Exists(dir))
      {
        System.IO.Directory.CreateDirectory(dir);
      }

      return dir;
    }

    protected virtual string GetTemplateDirectory(string reg_, string env_, string role_)
    {
      string dir = null;

      if (reg_ == null && env_ == null && role_ == null) 
      {
        dir = String.Format(@"{0}\Configuration", _systemBaseDir);
      }
      else if (reg_ != null && env_ == null && role_ == null) 
      {
        dir = String.Format(@"{0}\Configuration\{1}", _systemBaseDir, reg_);
      }
      else if (reg_ != null && env_ != null && role_ == null)
      {
        dir = String.Format(@"{0}\Configuration\{1}\{2}", _systemBaseDir, reg_, env_);
      }
      else
      {
        dir = String.Format(@"{0}\Configuration\{1}\{2}\{3}", _systemBaseDir, reg_, env_, role_);
      }      

      return dir;
    }

    protected virtual NameValueCollection ParseProfileString(string profileString_) 
    {
      NameValueCollection values = new NameValueCollection();

      string[] settings = profileString_.Split(';');

      for (int index = 0; index < settings.Length; index++) 
      {
        string[] namevalue = settings[index].Split('=');
        if (namevalue.Length == 2) 
        {
          values.Add(namevalue[0].Trim(), namevalue[1].Trim());
        } 
        else 
        {
          #region Logging
          if (Log.IsLogLevelEnabled(LogLevel.Error)) 
          {
            LogLayer.Error.SetLocation("FileConfigurationStore", "ParseProfileString").WriteFormat(Res.GetString(Res.EXCEPTION_INVALID_PROFILESTRING), profileString_);
          }
          #endregion Logging

          throw new ConfiguratorException(String.Format(Res.GetString(Res.EXCEPTION_INVALID_PROFILESTRING), profileString_));
        }
      }

      if (values.Count == 0) 
      {
        #region Logging
        if (Log.IsLogLevelEnabled(LogLevel.Error)) 
        {
          LogLayer.Error.SetLocation("FileConfigurationStore", "ParseProfileString").WriteFormat(Res.GetString(Res.EXCEPTION_INVALID_PROFILESTRING), profileString_);
        }
        #endregion Logging

        throw new ConfiguratorException(Res.GetString(Res.EXCEPTION_INVALID_PROFILESTRING));
      }
      
      return values;      
    }

    protected virtual string ParseDirectoryString(string data_)
    {
      //code allows for exactly 1 environment variable to be specified in the string
      //of form %ETSHOME%
      if (data_.IndexOf('%') > -1)
      {
        int nFirst = data_.IndexOf('%');
        int nLast = data_.LastIndexOf('%');
        if (nLast > nFirst)
        {
          string sLeft = "";
          if (nFirst > 0)
          {
            sLeft = data_.Substring(0,nFirst);
          }
          string sRight = "";
          if (nLast != data_.Length -1)
          {
            sRight = data_.Substring(nLast + 1);
          }

          string sMid = data_.Substring(nFirst +1, nLast - nFirst -1);
          //now swap it for environment variable
          string sEnv = "";
          try 
          {
            sEnv = System.Environment.GetEnvironmentVariable(sMid);
          }
          catch (Exception e_)
          {
            //user probably didn't have permission to read this variable
            #region Logging
            if (Log.IsLogLevelEnabled(LogLevel.Error)) 
            {
              LogLayer.Error.SetLocation("FileConfigurationStore", "ParseDirectoryString").WriteFormat("Error reading environment variable: {0}", e_, data_);
            }
            #endregion Logging 
            return null;
          }
          if (sEnv != null && sEnv.Length > 0)
          {
            return (sLeft + sEnv + sRight);
          }
        }
        #region Logging
        if (Log.IsLogLevelEnabled(LogLevel.Error)) 
        {
          LogLayer.Error.SetLocation("FileConfigurationStore", "ParseDirectoryString").WriteFormat("Directory string was of invalid format: {0}", data_);
        }
        #endregion Logging
        return null;
      }
      else
      {
        return data_;
      }
    }
    #endregion Protected Methods

    #region Private Methods

    private XmlNode CreateEmptyConfig(string name_)
    {
      XmlDocument doc = new XmlDocument();
      doc.LoadXml(String.Format("<{0} />", name_));
      return doc.DocumentElement;
    }

    #endregion
  }
}
