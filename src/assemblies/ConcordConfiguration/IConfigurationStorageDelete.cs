﻿#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) Morgan Stanley, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/ConcordConfiguration/IConfigurationStorageDelete.cs#1 $
  $DateTime: 2014/03/28 12:45:34 $
    $Change: 873853 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion File Info Header

using System;

namespace MorganStanley.IED.Concord.Configuration
{
  /// <include file='xmldocs/IConfigurationStorageDelete.cs.xml' path='doc/doc[@for="IConfigurationStorageDelete"]/*'/>
  public interface IConfigurationStorageDelete
  {
    /// <include file='xmldocs/IConfigurationStorageDelete.cs.xml' path='doc/doc[@for="IConfigurationStorageDelete.DeleteConfiguration"]/*'/>
    void DeleteConfiguration(string name_, string version_);
  }
}