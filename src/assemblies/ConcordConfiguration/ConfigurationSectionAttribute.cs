#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/ConcordConfiguration/ConfigurationSectionAttribute.cs#3 $
  $DateTime: 2013/01/18 15:24:18 $
    $Change: 813214 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion

using System;

namespace MorganStanley.IED.Concord.Configuration
{
  /// <include file='xmldocs/ConfigurationSectionAttribute.cs.xml' path='doc/doc[@for="ConfigurationSectionAttribute"]/*'/>
  [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
  public class ConfigurationSectionAttribute : Attribute
  {
    #region Declarations
    private string _name;
    private string _version;
    private Type _handlerType;
    private Type _providerType;
    #endregion Declarations

    #region Constructors
    /// <include file='xmldocs/ConfigurationSectionAttribute.cs.xml' path='doc/doc[@for="ConfigurationSectionAttribute.ConfigurationSectionAttribute"]/*'/>
    public ConfigurationSectionAttribute(string name_)
    {
      _name = name_;
    }

    /// <include file='xmldocs/ConfigurationSectionAttribute.cs.xml' path='doc/doc[@for="ConfigurationSectionAttribute.ConfigurationSectionAttribute1"]/*'/>
    public ConfigurationSectionAttribute(string name_, string version_) : this (name_)
    {
      _version = version_;
    }

    /// <include file='xmldocs/ConfigurationSectionAttribute.cs.xml' path='doc/doc[@for="ConfigurationSectionAttribute.ConfigurationSectionAttribute2"]/*'/>
    public ConfigurationSectionAttribute(string name_, string version_, Type handlerType_) : this(name_, version_) 
    {
      _handlerType = handlerType_;
    }

    /// <include file='xmldocs/ConfigurationSectionAttribute.cs.xml' path='doc/doc[@for="ConfigurationSectionAttribute.ConfigurationSectionAttribute3"]/*'/>
    public ConfigurationSectionAttribute(string name_, string version_, Type handlerType_, Type providerType_) : this(name_, version_, handlerType_) 
    {
      _providerType = providerType_;
    }
    #endregion Constructors

    #region Public Properties
    /// <include file='xmldocs/ConfigurationSectionAttribute.cs.xml' path='doc/doc[@for="ConfigurationSectionAttribute.HandlerType"]/*'/>
    public Type HandlerType
    {
      get { return _handlerType; }
      set { _handlerType = value; }
    }

    /// <include file='xmldocs/ConfigurationSectionAttribute.cs.xml' path='doc/doc[@for="ConfigurationSectionAttribute.ProviderType"]/*'/>
    public Type ProviderType
    {
      get { return _providerType; }
      set { _providerType = value; }
    }

    /// <include file='xmldocs/ConfigurationSectionAttribute.cs.xml' path='doc/doc[@for="ConfigurationSectionAttribute.Name"]/*'/>
    public string Name 
    {
      get { return _name; }
    }

    /// <include file='xmldocs/ConfigurationSectionAttribute.cs.xml' path='doc/doc[@for="ConfigurationSectionAttribute.Version"]/*'/>
    public string Version
    {
      get { return _version; }
    }
    #endregion Public Properties
  }
}
