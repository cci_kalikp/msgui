//===============================================================================
// Microsoft patterns & practices
// CompositeUI Application Block
//===============================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//===============================================================================
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Themes;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using System.Collections.Generic;
using System.Xml.Linq;
using System;
using Microsoft.Practices.CompositeUI.MSDesktopIntegration;
using System.Configuration;
using System.Xml.Serialization;
using Microsoft.Practices.CompositeUI.MSDesktopIntegration.Configuration;
using System.IO;


namespace Microsoft.Practices.CompositeUI
{
	/// <summary>
	/// Extends <see cref="CabApplication{TWorkItem}"/> to support applications with a shell.
	/// </summary>
	/// <typeparam name="TWorkItem">The type of the root application work item.</typeparam>
	/// <typeparam name="TShell">The type of the shell the application uses.</typeparam>
	public abstract class CabShellApplication<TWorkItem, TShell> : CabApplication<TWorkItem>
		where TWorkItem : WorkItem, new()
	{
		private TShell shell;
		protected Framework msDesktop;


        protected virtual void SetupTheme()
        {
            msDesktop.AddSimpleTheme();
        }

		/// <summary>
		/// Creates the shell.
		/// </summary>
		protected sealed override void OnRootWorkItemInitialized()
		{
			BeforeShellCreated();

			shell = RootWorkItem.Items.AddNew<TShell>();
			
			msDesktop = RootWorkItem.Items.AddNew<Framework>();
		    SetupTheme();
			msDesktop.SetDefaultWindowParameters(new InitialWindowParameters() { ShowFlashBorder = false, InitialLocation = MorganStanley.MSDotNet.MSGui.Core.InitialLocation.DockInNewTab });

			var exeConfiguration = System.Configuration.ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
			var bootModulesSection = exeConfiguration.GetSection("bootModules");
			if (bootModulesSection != null)
			{
				var xconfig = bootModulesSection.SectionInformation.GetRawXml();
				XmlSerializer ser = new XmlSerializer(typeof(BootModules));
				var bootModules = (BootModules)ser.Deserialize(new StringReader(xconfig));

				foreach (var module in bootModules)
				{
					var assembly = System.Reflection.Assembly.Load(module.ModuleAssembly);
					var moduleType = assembly.GetType(module.ModuleFQCN);
					var bootModule = Activator.CreateInstance(moduleType, msDesktop) as Microsoft.Practices.Composite.Modularity.IModule;
					bootModule.Initialize();
				}
			}
			msDesktop.AddModule<CabIntegrationModule>();
			
			AfterShellCreated();
		}

		/// <summary>
		/// May be overridden in derived classes to perform activities just before the shell
		/// is created.
		/// </summary>
		protected virtual void BeforeShellCreated()
		{
		}

		/// <summary>
		/// May be overridden in derived classes to perform activities just after the shell
		/// has been created.
		/// </summary>
		protected virtual void AfterShellCreated()
		{
		}

		/// <summary>
		/// Returns the shell that was created. Will not be valid until <see cref="AfterShellCreated"/>
		/// has been called.
		/// </summary>
		protected TShell Shell
		{
			get { return shell; }
		}
	}
}