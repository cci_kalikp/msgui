﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using System.Xml.Linq;

namespace Microsoft.Practices.CompositeUI.MSDesktopIntegration
{
	internal static class WidgetFactories
	{
		private static IChromeRegistry m_chromeRegistry;
		private static IChromeManager m_chromeManager;


		static void Initialize(IChromeRegistry chromeRegistry_, IChromeManager chromeManager_)
		{
			m_chromeRegistry = chromeRegistry_;
			m_chromeManager = chromeManager_;
		}

		static bool CABCommandFactory(IWidgetViewContainer vc_, XDocument state)
		{
			var parameters = vc_.Parameters as InitialCABCommandWidgetParameters;

			return true;
		}
	}
}
