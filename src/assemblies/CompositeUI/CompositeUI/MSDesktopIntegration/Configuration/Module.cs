﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Microsoft.Practices.CompositeUI.MSDesktopIntegration.Configuration
{
	public class Module
	{
		[XmlAttribute("FQCN")]
		public string ModuleFQCN { get; set; }
		[XmlAttribute("Assembly")]
		public string ModuleAssembly { get; set; }
	}
}
