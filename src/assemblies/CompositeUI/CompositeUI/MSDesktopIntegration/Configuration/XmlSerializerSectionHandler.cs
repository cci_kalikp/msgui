﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Xml.XPath;
using System.Xml.Serialization;
using System.Xml;
using System.Windows.Shapes;

namespace Microsoft.Practices.CompositeUI.MSDesktopIntegration.Configuration
{
	public class XmlSerializerSectionHandler : IConfigurationSectionHandler
	{
		public object Create(object parent, object configContext, System.Xml.XmlNode section)
		{
			XPathNavigator nav = section.CreateNavigator();
			string typename = (string)nav.Evaluate("string(@type)");
			Type t = Type.GetType(typename);
			XmlSerializer ser = new XmlSerializer(t);
			var nr = new XmlNodeReader(section);
			var rs = new XmlReaderSettings();
			//var vr = new XmlValidatingReader(section.OuterXml, XmlNodeType.Element, null);

			//rs.Schemas.Add(null, @"file://" + Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "MSDesktopConfigSchema.xsd"));
			//rs.ValidationType = ValidationType.Schema;
			//rs.ValidationEventHandler += new System.Xml.Schema.ValidationEventHandler(rs_ValidationEventHandler);

			var vr = XmlReader.Create(nr, rs);//section.OuterXml, XmlNodeType.Element, null);
			return ser.Deserialize(vr);
		}

		void rs_ValidationEventHandler(object sender, System.Xml.Schema.ValidationEventArgs e)
		{
			//throw new NotImplementedException();
		}
	}
}
