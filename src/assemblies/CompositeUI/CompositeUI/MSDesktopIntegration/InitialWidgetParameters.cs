﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace Microsoft.Practices.CompositeUI.MSDesktopIntegration
{
	internal class InitialCABCommandWidgetParameters : InitialWidgetParameters
	{
		public object UIEelement { get; set; }
	}
}
