﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace Microsoft.Practices.CompositeUI.MSDesktopIntegration
{
	public static class Extensions
	{
		public static IWindowViewContainer GetViewContainer(this System.Windows.Forms.Control smartPart)
		{
            return CabIntegrationModule.WindowsForSmartParts.ContainsKey(smartPart)
                ? CabIntegrationModule.WindowsForSmartParts[smartPart]
                : null;			
		}

        public static void ShowRelativeToShell(this Form form, Point pt)
        {
            if (form == null) return;

            var mainWindow = System.Windows.Application.Current.MainWindow;

            if (mainWindow == null) return;

            int x = (int)mainWindow.Left + pt.X ;
            int y = (int)mainWindow.Top + pt.Y;

            form.Location = new Point(x, y);
        }

	}
}
