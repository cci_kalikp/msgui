﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using System.Windows.Media;
using System.Windows.Threading;
using System.Xml.Linq;
using Microsoft.Practices.CompositeUI.Commands;
using Microsoft.Practices.CompositeUI.SmartParts;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using Application = System.Windows.Application;
using Brushes = System.Windows.Media.Brushes;
using Button = System.Windows.Controls.Button;
using Control = System.Windows.Forms.Control;
using FontFamily = System.Windows.Media.FontFamily;
using Image = System.Drawing.Image;
using Point = System.Drawing.Point;
using UserControl = System.Windows.Forms.UserControl;

//using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent;


namespace Microsoft.Practices.CompositeUI.MSDesktopIntegration
{

    public class CabIntegrationModule : Microsoft.Practices.Composite.Modularity.IModule
    {
        private static readonly FontFamily FontFamilyToSet = new FontFamily("Microsoft Sans Serif");
        internal static IChromeRegistry chromeRegistry;
        internal static IChromeManager chromeManager;

        internal static Dictionary<Control, string> smartPartToWindowFactory = new Dictionary<Control, string>();
        internal static List<InitialWidgetParameters> pendingWidgets = new List<InitialWidgetParameters>();
        internal static List<InitialWidgetParameters> pendingMenueItems = new List<InitialWidgetParameters>();
        internal static object statusbarControl;

        internal static Dictionary<object, IntegratedWorkspace> WorkspaceForSmartPart =
            new Dictionary<object, IntegratedWorkspace>();

        internal static List<object> smartpartsToSkip = new List<object>();

        internal static Dictionary<object, Microsoft.Practices.CompositeUI.Commands.Command> CommandsForWidgets =
            new Dictionary<object, Commands.Command>();

        internal static Dictionary<object, IWindowViewContainer> WindowsForSmartParts =
            new Dictionary<object, IWindowViewContainer>();

        internal static List<IntegratedWorkspace> GroupWorkSpaces = new List<IntegratedWorkspace>();

        public CabIntegrationModule(IChromeManager chromeManager_, IChromeRegistry chromeRegistry_)
        {
            chromeManager = chromeManager_;
            chromeRegistry = chromeRegistry_;
        }

        #region IModule Members

        public void Initialize()
        {
            /*
			(CabIntegrationModule.chromeManager as ChromeManagerBase).Deactivated += (s, e) =>
			{
				var x = from k in WindowsForSmartParts.Keys
						where WindowsForSmartParts[k] == e.ViewContainer
						select k;

				foreach (var k in x)
				{
					if (workspaceForSmartPart.ContainsKey(k))
					{
						dynamic d = workspaceForSmartPart[k];
						d.setActiveSmartPart(null);
					}
				}
			};
			*/



            (CabIntegrationModule.chromeManager as ChromeManagerBase).Activated += (s, e) =>
                {
                    Debug.WriteLine("Activated CallBack = " + e.ViewContainer.Title);

                    var x = from k in WindowsForSmartParts.Keys
                            where WindowsForSmartParts[k] == e.ViewContainer
                            select k;

                    foreach (var k in x)
                    {

                        if (WorkspaceForSmartPart.ContainsKey(k))
                        {
                            var ctrl = k as Control;

                            if (ctrl != null && (ctrl.IsDisposed || ctrl.Disposing)) continue;

                            IntegratedWorkspace d = WorkspaceForSmartPart[k];
                            d.SetActiveSmartPart(k);

                            GroupWorkSpaces.ForEach(grpWs =>
                                {
                                    if (grpWs.GroupName == d.GroupName)
                                        grpWs.RaiseWorkSpaceActivated(d.GroupName);
                                });
                        }
                    }

                };


            foreach (var id in windowFactories.Keys)
            {
                CabIntegrationModule.chromeRegistry.RegisterWindowFactory(id, windowFactories[id]);
            }

            foreach (var id in dialogFactories.Keys)
            {
                CabIntegrationModule.chromeRegistry.RegisterDialogFactory(id, dialogFactories[id]);
            }

            //foreach (var w in windowsToShow)
            foreach (var w in smartPartToWindowFactory.Keys)
            {
                try
                {
                    DoShow(w, smartPartsToShow[w]);
                }
                catch
                {
                    DoShow(w, null, true);
                }
            }


            foreach (var iwp in pendingWidgets)
            {
                if (iwp is InitialMenuToolbarWidget)
                {
                    var site = (iwp as InitialMenuToolbarWidget).ToolStripButton.Site;
                    var id = site == null || string.IsNullOrEmpty(site.Name) ? "CAB_QAT_" : "QAT_" + site.Name;
                    id = id + iwp.Text;
                    var widget = CabIntegrationModule.chromeManager.Ribbon["Main"]["Tools"].AddWidget(id, iwp);
                    widget.AddToQAT();
                }
                else
                {
                    var widget = CabIntegrationModule.chromeManager.Ribbon["Main"]["Tools"].AddWidget(iwp);
                    widget.AddToQAT();
                }

            }


            foreach (var iwp in pendingMenueItems)
            {
                var mi = ((InitialMenuRibbonWidget) iwp).MenuItem;

                var groupName = "Tools";
                var tabName = "Main";

                if (mi.OwnerItem != null)
                {
                    groupName = ExcludeAmpersand(mi.OwnerItem.Text);
                    if (mi.OwnerItem.OwnerItem != null)
                    {
                        tabName = ExcludeAmpersand(mi.OwnerItem.OwnerItem.Text);
                    }
                }

                var id = mi.Site == null || string.IsNullOrEmpty(mi.Site.Name) ? "CAB_" : mi.Site.Name + "_";
                id = id + mi.Text;
                if (mi.HasDropDownItems)
                {
                    var dropdown = new InitialDropdownButtonParameters()
                        {
                            Size =ButtonSize.Small,   
                            Image = ConvertImage(mi.Image),
                            Enabled = true,
                            Text = ExcludeAmpersand(mi.Text),
                            ToolTip = mi.ToolTipText,
                            Opening = (s, e) =>
                                {
                                    if (CommandsForWidgets.ContainsKey(mi))
                                    {
                                        var cmd = CommandsForWidgets[mi];
                                        Dispatcher.CurrentDispatcher.BeginInvoke((Action)(cmd.Execute));
                                    }
                                }
                        };

                    var rootMenu = chromeManager.Ribbon[tabName][groupName].AddWidget(id, dropdown);
                    foreach (var item in mi.DropDownItems)
                    {
                        var m = item as ToolStripMenuItem;
                        var button = new InitialButtonParameters
                            {
                                Size = ButtonSize.Small,
                                Text = ExcludeAmpersand(m.Text),
                                Image = ConvertImage(m.Image),
                                ToolTip = m.ToolTipText,
                                Click = (s, e) =>
                                    {
                                        Command cmd;
                                        if (CommandsForWidgets.TryGetValue(m, out cmd))
                                        {
                                            Dispatcher.CurrentDispatcher.BeginInvoke((Action)(cmd.Execute));
                                            
                                        }
                                    }
                            };

                        rootMenu.AddWidget(button);
                    }
                }
                else
                {
                    var button = new InitialButtonParameters
                        {
                            Size = ButtonSize.Small,
                            Text = ExcludeAmpersand(mi.Text),
                            ToolTip = mi.ToolTipText,
                            Image = ConvertImage(mi.Image),
                            Click = (s, e) =>
                                {
                                    Command cmd;
                                    if (CommandsForWidgets.TryGetValue(mi, out cmd))
                                    {
                                        Dispatcher.CurrentDispatcher.BeginInvoke((Action)(cmd.Execute));
                                    }
                                    else
                                    {
                                        Dispatcher.CurrentDispatcher.BeginInvoke((Action)(mi.PerformClick));
                                    }
                                }
                        };
                    chromeManager.Ribbon[tabName][groupName].AddWidget(id, button);

                }
            }



            if (statusbarControl as Control != null)
            {
                var wfh = new WindowsFormsHost()
                    {
                        Margin = new Thickness(0),
                        FontSize = 11.33,
                        FontFamily = new FontFamily("Microsoft Sans Serif"),
                        Child = statusbarControl as Control
                    };
                var statusbarItem = new StatusbarItem() {Control = wfh};
                chromeManager.StatusBar.AddItem(statusbarItem, StatusBarItemAlignment.Right);
            }

            //Register for Blank View
            chromeRegistry.RegisterWindowFactory(BlankId, (vc, state) =>
                {
                    vc.Content = null;
                    vc.Title = vc.GetHashCode().ToString();
                    vc.Background = Brushes.Transparent;
                    return true;
                });
        }

        private static ImageSource ConvertImage(Image img)
        {
            if (img == null) return null;

            var bmp = new System.Windows.Media.Imaging.BitmapImage();

            var stream = new MemoryStream();
            bmp.BeginInit();
            img.Save(stream, ImageFormat.Png);
            stream.Seek(0, SeekOrigin.Begin);
            bmp.StreamSource = stream;
            bmp.EndInit();

            return bmp;
        }

        private const string BlankId = "BLANK_VIEW_ID";

        private static Dictionary<string, InitializeWindowHandler> windowFactories =
            new Dictionary<string, InitializeWindowHandler>();

        private static Dictionary<string, InitializeDialogHandler> dialogFactories =
            new Dictionary<string, InitializeDialogHandler>();

        internal static void Register(IntegratedWorkspace workspace, string ID, string title, object smartPart,
                                      string description)
        {
            // This function registers both window and dialog factories in CM, as later on we may need to display the smartpart as modal.

            InitializeWindowHandler wh = (vc, state) =>
                {
                    var smartPartControl = (Control) smartPart;
                    var wfh = new WindowsFormsHost();

                    SetDefaultFont(wfh);

                    wfh.Loaded += (s, e) =>
                        {
                            var formsContainer = new MinimalContentControl();

                            if (smartPartControl.Width > 0 && smartPartControl.Height > 0)
                            {
                                formsContainer.Width = smartPartControl.Width;
                                formsContainer.Height = smartPartControl.Height;
                            }

                            // We can also have UserControl with ParentForm. In which case, we are interested in title and sizing of the parent form.
                            var containerControl = smartPartControl as ContainerControl;
                            if (containerControl != null)
                            {
                                var parentForm = containerControl.ParentForm;
                                if (parentForm != null)
                                {
                                    vc.Title = parentForm.Text;
                                    vc.Icon = parentForm.Icon;
                                }
                            }

                            formsContainer.Controls.Add(smartPartControl);
                            Control child = formsContainer;
                            wfh.Child = child;
                        };

                    wfh.Unloaded += (s, e) =>
                        {
                            var formsContainer = wfh.Child;

                            // We are deparented
                            formsContainer.Controls.Remove(smartPartControl);
                            formsContainer.Dispose();
                        };

                    vc.Content = wfh;
                    vc.Title = title; // smartPart.GetType().FullName; //title;
                    vc.Tooltip = description;
                    return true;
                };

            InitializeDialogHandler dh = (IDialogWindow dialog) =>
                {
                    var wfh = new System.Windows.Forms.Integration.WindowsFormsHost();
                    SetDefaultFont(wfh);
                    wfh.Child = smartPart as System.Windows.Forms.Control;
                    dialog.Content = wfh;
                    dialog.Title = title;
                    return true;
                };

            //if (windowFactories.ContainsKey(ID) || dialogFactories.ContainsKey(ID))
            //    return;

            if (!windowFactories.ContainsKey(ID))
            {

                windowFactories.Add(ID, wh);
                if (chromeManager != null)
                {
                    CabIntegrationModule.chromeRegistry.RegisterWindowFactory(ID, wh);
                }
            }


            if (!dialogFactories.ContainsKey(ID))
            {
                dialogFactories.Add(ID, dh);
                if (chromeManager != null)
                {
                    CabIntegrationModule.chromeRegistry.RegisterDialogFactory(ID, dh);
                }
            }


            if (title == string.Empty)
            {
                //smartpartsToSkip.Add(smartPart); //TODO: Why skip based on title?

                if (String.IsNullOrEmpty(workspace.GroupName) == false)
                {
                    if (GroupWorkSpaces.Contains(workspace) == false)
                    {
                        GroupWorkSpaces.Add(workspace);
                    }
                }
            }

            if (!smartPartToWindowFactory.ContainsKey((Control)smartPart))
                smartPartToWindowFactory.Add((Control)smartPart, ID);

            if (!WorkspaceForSmartPart.ContainsKey(smartPart))
                WorkspaceForSmartPart.Add(smartPart, workspace);


        }

        private class MinimalContentControl : ContainerControl
        {
            public override System.Drawing.Size GetPreferredSize(System.Drawing.Size proposedSize)
            {
                var proposedBaseSize = base.GetPreferredSize(proposedSize);

                if (Controls.Count > 0 && (proposedBaseSize.Height == 0 || proposedBaseSize.Width == 0))
                {
                    // Return the underlying control actual size
                    var controlSize = Controls[0].Size;
                    return new System.Drawing.Size
                        {
                            Width = Math.Max(1, controlSize.Width),
                            Height = Math.Max(1, controlSize.Height),
                        };
                }

                return proposedBaseSize;
            }
        }

        private string ExcludeAmpersand(string text)
        {
            var index = text.IndexOf('&');
            if (index == -1)
            {
                return text;
            }

            var b = new StringBuilder();
            if (index > 0)
            {
                b.Append(text, 0, index);
            }

            if (index < text.Length - 1)
            {
                b.Append(text, index + 1, text.Length - (index + 1));
            }

            return b.ToString();
        }

        internal static void SetRelativeToShell(Form form)
        {
            if (form == null) return;

            var mainWindow = System.Windows.Application.Current.MainWindow;

            if (mainWindow == null) return;

            int x = (int) ((mainWindow.Width/2) - (form.Width/2)) + (int) mainWindow.Left;
            int y = (int) ((mainWindow.Height/2) - (form.Height/2)) + (int) mainWindow.Top;

            form.Location = new Point(x, y);
        }

        private static Dictionary<object, ISmartPartInfo> smartPartsToShow = new Dictionary<object, ISmartPartInfo>();

        internal static void Show(object smartPart, ISmartPartInfo info)
        {
            if (chromeManager == null)
            {
                smartPartsToShow.Add(smartPart, info);
            }
            else
            {
                DoShow(smartPart, info, false, (info != null) ? info.Title : "");
            }
        }

        internal static bool CloseTabForSmartPart(object smartPart)
        {
            if (smartPart == null) return false;

            IWindowViewContainer wvc;

            if (WindowsForSmartParts.TryGetValue(smartPart, out wvc))
            {
                wvc.Close();
                return true;
            }

            return false;
        }

        internal static void ActivateParentTab(string name)
        {
            if (name == null) return;

            if (chromeManager.Tabwell.Tabs.Count == 0) return;

            var tab = chromeManager.Tabwell.Tabs.FirstOrDefault(dock => dock.Title == name);
            if (tab != null)
            {
                tab.Activate();
            }
            else
            {
                EnsureBlankShown(name);
            }
        }

        private static void RemoveBlankView(string groupName)
        {
            if (groupName == null) return;

            if (chromeManager != null && beforeCreateChrome == false)
            {
                var tab = chromeManager.Tabwell.Tabs.FirstOrDefault(dock => dock.Title == groupName);
                if (tab != null && tab.Windows.Count > 0)
                {
                    var blank = tab.Windows.FirstOrDefault(wvc => wvc.Content == null);
                    if (blank != null)
                    {
                        blank.Close();
                    }
                }
            }
        }

        private static void EnsureBlankShown(string groupName)
        {
            if (groupName == null) return;

            if (chromeManager != null && beforeCreateChrome == false)
            {
                var tab = chromeManager.Tabwell.Tabs.FirstOrDefault(dock => dock.Title == groupName);
                if (tab == null)
                {
                    //Add a dummy blank view to Tab
                    var parameters = new InitialWindowParameters()
                        {
                            InitialLocation = InitialLocation.DockInNewTab,
                            IsHeaderVisible = false,
                            Transient = true
                        };
                    var wvc = CabIntegrationModule.chromeManager.CreateWindow(BlankId, parameters);
                    tab = chromeManager.Tabwell.Tabs.First(t => t.Windows.Contains(wvc));
                    tab.Title = groupName;
                }
            }
        }

        private static void SetDefaultFont(System.Windows.Forms.Integration.WindowsFormsHost windowsFormsHost)
        {
            windowsFormsHost.Margin = new Thickness(0);
            windowsFormsHost.FontSize = 11.33;
            windowsFormsHost.FontFamily = FontFamilyToSet;
        }

        private static bool beforeCreateChrome = true;

        private static void DoShow(object smartPart, ISmartPartInfo info, bool fakeOnly = false, string title = null)
        {
            var partControl = (Control) smartPart;
            if (smartPartToWindowFactory.ContainsKey(partControl))
            {
                if (smartPart.GetType() != ChromeManagerBase.CABNavigationControl &&
                    (fakeOnly || smartpartsToSkip.Contains(smartPart)))
                {
                    //var w = new System.Windows.Window();
                    //var wfh = new System.Windows.Forms.Integration.WindowsFormsHost();
                    //SetDefaultFont(wfh);
                    //wfh.Child = smartPart as System.Windows.Forms.Control;
                    //w.Content = wfh;

                    //w.WindowStartupLocation = System.Windows.WindowStartupLocation.Manual;
                    //w.Left = -5000;
                    //w.ShowInTaskbar = false;

                    //w.Loaded += (s, e) =>
                    //{
                    //    w.Dispatcher.Invoke((Action)(() => { w.Content = null; w.Close(); }));
                    //};

                    //w.Show();

                    IntegratedWorkspace workSpace;
                    if (WorkspaceForSmartPart.TryGetValue(smartPart, out workSpace))
                    {
                        var groupName = workSpace.GroupName;
                        ActivateParentTab(groupName);
                    }
                }
                else
                {
                    if (beforeCreateChrome)
                    {
                        EventHandler hack = null;
                        hack = (s, e) =>
                            {
                                if (smartPart.GetType() == ChromeManagerBase.CABNavigationControl)
                                    //if (smartPart.GetType().FullName == "MorganStanley.Fast.Magnet.UI.Navigation.NavigationView")
                                { 
                                    if ( ((ChromeManagerBase) chromeManager).RootWidgets.ContainsKey(
                                            ChromeArea.NavigationBar.ToLower()))
                                    {
  
                                        string guiId = Guid.NewGuid().ToString();
                                        chromeRegistry.RegisterWidgetFactory(guiId, (container_, state_) =>
                                            {
                                                var wfh = new System.Windows.Forms.Integration.WindowsFormsHost();
                                                SetDefaultFont(wfh);

                                                EventHandler visibilityHook = (s_, e_) =>
                                                                                  {
                                                                                      wfh.Visibility
                                                                                          = wfh.Child.Visible
                                                                                                ? Visibility.
                                                                                                      Visible
                                                                                                : Visibility.
                                                                                                      Collapsed;
                                                                                  };

                                                wfh.ChildChanged += (sender_, args_) =>
                                                                        {
                                                                            var prev =
                                                                                args_.PreviousChild as
                                                                                System.Windows.Forms.Control;
                                                                            if (prev != null)
                                                                                prev.VisibleChanged -= visibilityHook;

                                                                            wfh.Child.VisibleChanged += visibilityHook;
                                                                        };


                                                wfh.Child = smartPart as System.Windows.Forms.Control;
                                                container_.Content = wfh;
                                               

                                                return true;

                                            });
                                        chromeManager[ChromeArea.NavigationBar].AddWidget(guiId,
                                                                                          new InitialWidgetParameters()); 
                                    }
     
                                }
                                else
                                {
                                    if (IsModal(info))
                                    {
                                        var dvc =
                                            chromeManager.CreateDialog(
                                                smartPartToWindowFactory[(Control)smartPart]);
                                        SetupCloseHack(dvc);

                                        dvc.ShowDialog();
                                    }
                                    else
                                    {
                                        var wvc =
                                            chromeManager.CreateWindow(
                                                smartPartToWindowFactory[(Control)smartPart],
                                                new InitialWindowParameters());
                                        SetupCloseHack(wvc);
                                        WindowsForSmartParts.Add(smartPart, wvc);
                                    }
                                    (chromeManager as ChromeManagerBase).AfterCreateChrome -= hack;
                                }

                                beforeCreateChrome = false;
                            };

                        (chromeManager as ChromeManagerBase).AfterCreateChrome += hack;
                    }
                    else
                    {
                        System.Windows.Application.Current.Dispatcher.BeginInvoke((Action) (() =>
                            {
                                if (IsModal(info))
                                {
                                    var dvc = chromeManager.CreateDialog(smartPartToWindowFactory[partControl]);
                                    SetupCloseHack(dvc);

                                    dvc.ShowDialog();
                                }
                                else
                                {
                                    IntegratedWorkspace wsp;
                                    if (WorkspaceForSmartPart.TryGetValue(smartPart, out wsp))
                                    {
                                        var name = wsp.GroupName;
                                        RemoveBlankView(name);
                                    }

                                    InitialWindowParameters parameters;
                                    var newTab = false;
                                    IDockTab tab;

                                    var ribbonChromeManager = chromeManager as RibbonChromeManager;
                                    if (ribbonChromeManager != null &&
                                        ribbonChromeManager.ShellMode != ShellMode.RibbonAndFloatingWindows)
                                    {

                                        tab = chromeManager.Tabwell.Tabs.FirstOrDefault(dock => dock.Title == wsp.GroupName);
                                        if (tab == null || tab.Windows.Count == 0)
                                        {
                                            newTab = true;
                                            //tab = chromeManager.Tabwell.AddTab(name);
                                            parameters = new InitialWindowParameters
                                                {
                                                    InitialLocation = InitialLocation.DockInNewTab,
                                                    Transient = true
                                                };
                                        }
                                        else
                                        {
                                            parameters = new InitialWindowParameters
                                                {
                                                    InitialLocation = InitialLocation.DockTabbed,
                                                    InitialLocationTarget = tab.Windows.First(),
                                                    Transient = true
                                                };
                                        }
                                    }
                                    else
                                    {
                                        parameters = new InitialWindowParameters
                                            {
                                                InitialLocation = InitialLocation.Floating,
                                                Width = partControl.Width,
                                                Height = partControl.Height,
                                            };
                                    }

                                    if (!partControl.Visible)
                                    {
                                        partControl.Show();
                                    }

                                    //if (chromeManager.Windows.Count == 0)
                                    //{
                                    //    parameters = new InitialWindowParameters();
                                    //}
                                    //else
                                    //{
                                    //    parameters = new InitialWindowParameters()
                                    //                     {
                                    //                         InitialLocation = InitialLocation.DockTabbed,
                                    //                         InitialLocationTarget = chromeManager.Windows.First()
                                    //                     };
                                    //}
                                    var factoryId = smartPartToWindowFactory[partControl];
                                    var wvc = chromeManager.CreateWindow(factoryId, parameters);
                                    if (newTab)
                                    {
                                        tab = chromeManager.Tabwell.Tabs.First(t => t.Windows.Contains(wvc));
                                        tab.Title = wsp.GroupName;
                                    }
                                    /*else
                                    {
                                        wvc.Title = partControl.Text;
                                    }*/

                                    SetupCloseHack(wvc);
                                    WindowsForSmartParts.Add(smartPart, wvc);
                                    wvc.Activate();

                                }
                                //var wvc = CabIntegrationModule.chromeManager.CreateWindow(smartPartToWindowFactory[smartPart as System.Windows.Forms.Control], new InitialWindowParameters());
                                //SetupCloseHack(wvc);
                                //WindowsForSmartParts.Add(smartPart, wvc);
                            }), System.Windows.Threading.DispatcherPriority.Normal);
                    }
                }
            }
        }

        private static bool IsModal(ISmartPartInfo info)
        {
            var modal = false;
            try
            {
                modal = (bool) ReflHelper.PropertyGet(info, "Modal");
            }
            catch (Exception)
            {
                // do nothing, info was not a WindowSmartPartInfo, that's okay
            }

            return modal;
        }

        private static void FormHostClosing(WindowEventArgs ea)
        {
            var wfh = (WindowsFormsHost) ea.ViewContainer.Content;
            var sp = wfh.Child;

            var containerControl = sp as MinimalContentControl;
            if (containerControl != null && containerControl.Controls.Count > 0 )
            {
                sp = containerControl.Controls[0];
                containerControl.Controls.RemoveAt(0);
            }

            // sp.Hide();
            if (WorkspaceForSmartPart.ContainsKey(sp))
            {
                var d = WorkspaceForSmartPart[sp];
                d.SetActiveSmartPart(null);
                EnsureBlankShown(d.GroupName);
                WorkspaceForSmartPart.Remove(sp);   
            }
            
            ea.ViewContainer.Content = null;

            WindowsForSmartParts.Remove(sp);
            

            
        }

        private static void SetupCloseHack(IWindowViewContainer wvc)
        {
            EventHandler<WindowEventArgs> closehack = null;
            closehack = (sender, ea) =>
                {
                    FormHostClosing(ea);
                    wvc.Closed -= closehack;
                    wvc.ClosedQuietly -= closehack;
                };

            wvc.Closed += closehack;
            wvc.ClosedQuietly += closehack;

            wvc.Closing += ContainerClosing;
        }

        private static void ContainerClosing(object sender, CancelEventArgs eventArgs)
        {
            var wvc = sender as IWindowViewContainer;
            if (wvc != null)
            {
                var sp = ((WindowsFormsHost) wvc.Content).Child;

                IntegratedWorkspace wksp;
                if (WorkspaceForSmartPart.TryGetValue(sp, out wksp))
                {
                    //wksp.WindowViewContainerClosing(sp, eventArgs);
                    var closeInfo = wksp.GetType().GetMethod("Close", new[] {typeof (object)});
                    if (closeInfo != null)
                    {
                        closeInfo.Invoke(wksp, new object[] {sp});
                    }
                    else
                    {
                        wksp.WindowViewContainerClosing(sp, eventArgs);
                    }

                    if (eventArgs.Cancel == false)
                    {
                        wvc.Closing -= ContainerClosing;
                    }
                }
            }
        }

        private static void SetupCloseHack(IDialogWindow dvc)
        {
            EventHandler closehack = null;
            closehack = (sender, ea) =>
                {
                    var sp = ((WindowsFormsHost)dvc.Content).Child;
                    IntegratedWorkspace d = WorkspaceForSmartPart[sp];
                    d.SetActiveSmartPart(null);
                    dvc.Content = null;
                    //WindowsForSmartParts.Remove(sp);
                    dvc.Closed -= closehack;
                };

            dvc.Closed += closehack;
        }


        private class StatusbarItem : IStatusBarItem
        {
            #region Implementation of IStatusBarItem

            /// <summary>
            /// Update the status of the status bar item.
            /// </summary>
            /// <param name="level_">Status level of this update.</param>
            /// <param name="statusText_">Text (or value) of the status item.</param>
            public void UpdateStatus(StatusLevel level_, string statusText_)
            {
                throw new NotImplementedException();
            }

            /// <summary>
            /// Clear the current status text.
            /// </summary>
            public void ClearStatus()
            {
                throw new NotImplementedException();
            }

            /// <summary>
            /// Get the control associated with the status bar item.
            /// </summary>
            public object Control { get; set; }

            public event StatusUpdatedEventHandler StatusUpdated;

            #endregion
        }

        #endregion

    }
}
