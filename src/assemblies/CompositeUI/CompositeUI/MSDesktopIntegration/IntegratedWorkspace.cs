﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Microsoft.Practices.CompositeUI.MSDesktopIntegration
{
    internal interface IntegratedWorkspace
    {
        string GroupName { get; }

        void WindowViewContainerClosing(object sender, CancelEventArgs cancelEventArgs);
        void SetActiveSmartPart(object value);

        void RaiseWorkSpaceActivated(string groupName);
    }
}
