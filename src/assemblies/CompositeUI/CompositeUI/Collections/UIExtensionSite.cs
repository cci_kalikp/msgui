//===============================================================================
// Microsoft patterns & practices
// CompositeUI Application Block
//===============================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//===============================================================================

using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;
using System.Windows.Interop;
using System.Windows.Media;
using Microsoft.Practices.CompositeUI.Commands;
using Microsoft.Practices.CompositeUI.UIElements;
using Microsoft.Practices.CompositeUI.MSDesktopIntegration;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace Microsoft.Practices.CompositeUI
{
    /// <summary>
    /// Represents an extension site for UI elements.
    /// </summary>
    public class UIExtensionSite : IEnumerable<object>
    {
        IUIElementAdapter adapter;
        List<object> items = new List<object>();

      

        /// <summary>
        /// Initializes a new instance of the <see cref="UIExtensionSite"/> class with the provided
        /// adapter.
        /// </summary>
        public UIExtensionSite(IUIElementAdapter adapter)
        {
            this.adapter = adapter;
        }

        /// <summary>
        /// Returns the number of items added to the site.
        /// </summary>
        public int Count
        {
            get { return items.Count; }
        }

        /// <summary>
        /// Adds an element to the site.
        /// </summary>
        /// <typeparam name="TElement">The type of the element to be added.</typeparam>
        /// <param name="uiElement">The element to be added.</param>
        /// <returns>The added element. The adapter may return a different element than was
        /// passed; in this case, the returned element is the new element provided by the
        /// adapter.</returns>
        public TElement Add<TElement>(TElement uiElement)
        {
            TElement element = (TElement)adapter.Add(uiElement);
            items.Add(element);

            #region MSDesktop integration

            Func<Image, ImageSource> convert = (img) =>
            {
                if (img == null) return null;

                var bmp = new System.Windows.Media.Imaging.BitmapImage();

                var stream = new MemoryStream();
                bmp.BeginInit();
                img.Save(stream, ImageFormat.Bmp);
                stream.Seek(0, SeekOrigin.Begin);
                bmp.StreamSource = stream;
                bmp.EndInit();

                return bmp;
            };

            
            // Yes, that condition is ugly but this doesn't need a reference to the IG assembly.
            if (uiElement.GetType().FullName == "Infragistics.Win.UltraWinToolbars.ButtonTool")
            {
                dynamic buttonTool = uiElement;

                CabIntegrationModule.pendingWidgets.Add(new InitialButtonParameters()
                {
                    Text = buttonTool.CaptionResolved,
                    ToolTip = buttonTool.ToolTipTextResolved,
                    Click = (s, e) =>
                    {
                        Microsoft.Practices.CompositeUI.Commands.Command cmd = null;

                        if (CabIntegrationModule.CommandsForWidgets.ContainsKey(element))
                        {
                            cmd = CabIntegrationModule.CommandsForWidgets[element];
                            cmd.Execute();
                        }
                    }
                });
            }
            if (uiElement.GetType() == typeof(System.Windows.Forms.ToolStripButton))
            {
                var stripButton = uiElement as ToolStripButton;

                CabIntegrationModule.pendingWidgets.Add(
                    new InitialMenuToolbarWidget()
                        {
                            ToolStripButton = stripButton,
                            Text = stripButton.Text,
                            ToolTip = stripButton.ToolTipText,
                            Image = convert(stripButton.Image),
                            Click = (s, e) =>
                                        {
                                            Microsoft.Practices.CompositeUI.Commands.Command cmd = null;

                                            if (CabIntegrationModule.CommandsForWidgets.ContainsKey(element))
                                            {
                                                cmd = CabIntegrationModule.CommandsForWidgets[element];
                                                cmd.Execute();
                                            }
                                        }
                        });
            }

            if (uiElement.GetType() == typeof(ToolStripMenuItem))
            {
                var menuItem = uiElement as ToolStripMenuItem;
                

             /*   CabIntegrationModule.pendingMenueItems.Add(new InitialButtonParameters()
                {
                    Text = menuItem.Text,
                    ToolTip = menuItem.ToolTipText,
                    Image = convert(menuItem.Image),
                    Size = ButtonSize.Small,
                    Click = (s, e) =>
                    {
                        Microsoft.Practices.CompositeUI.Commands.Command cmd = null;

                        if (CabIntegrationModule.CommandsForWidgets.ContainsKey(element))
                        {
                            cmd = CabIntegrationModule.CommandsForWidgets[element];
                            cmd.Execute();
                        }
                    }
                });*/

                CabIntegrationModule.pendingMenueItems.Add(new InitialMenuRibbonWidget()
                                                               {
                                                                   MenuItem = menuItem
                                                               });
            }

            var status = uiElement as ToolStripLabel;
            if (status != null && status.Owner is StatusStrip)
            {
                var strip = status.Owner as StatusStrip;
                strip.SizingGrip = false;
                strip.AutoSize = true;
                CabIntegrationModule.statusbarControl = status.Owner;
            }

            /*if (uiElement.GetType() == typeof(StatusStrip))
            {
                CabIntegrationModule.statusbarControl = uiElement;
            }
            */
            #endregion

            return element;
        }

        

        /// <summary>
        /// Removes all items from the site, and removes them from the UI.
        /// </summary>
        public void Clear()
        {
            foreach (object obj in items)
                adapter.Remove(obj);

            items.Clear();
        }

        /// <summary>
        /// Determines if the site contains a UI element.
        /// </summary>
        /// <param name="uiElement">The element to find.</param>
        /// <returns>true if the element is present; false otherwise.</returns>
        public bool Contains(object uiElement)
        {
            return items.Contains(uiElement);
        }

        /// <summary>
        /// Removes an item from the site, and removes it from the UI.
        /// </summary>
        /// <param name="uiElement">The element to be removed.</param>
        public void Remove(object uiElement)
        {
            if (items.Contains(uiElement))
            {
                adapter.Remove(uiElement);
                items.Remove(uiElement);
            }
        }

        internal UIExtensionSite Duplicate()
        {
            return new UIExtensionSite(this.adapter);
        }

        IEnumerator<object> IEnumerable<object>.GetEnumerator()
        {
            return items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return items.GetEnumerator();
        }
    }

    //menu will add to Ribbon
    internal class InitialMenuRibbonWidget: InitialWidgetParameters
    {
        public ToolStripMenuItem MenuItem { get; set; }
    }

    //toolbar will add to QAT
    internal class InitialMenuToolbarWidget : InitialButtonParameters
    {
        public ToolStripButton ToolStripButton { get; set; }
    }
}
