﻿using System;

namespace MSDesktop.ViewModelAPI
{
    /// <summary>
    /// Represents the base view model for a module.
    /// Will force you to implement everything required for 
    /// window management and persistance
    /// </summary>
    /// <typeparam name="TParams">The type of settings to be used for this window</typeparam>
    public interface IFrameworkViewModel<TParams> : IDisposable
        where TParams : IFrameworkViewParameters, new()
    {
        /// <summary>
        /// Called by the framework to set the state of the view model
        /// </summary>
        /// <param name="state_">The state</param>
        void SetState(TParams state_);

        /// <summary>
        /// Called by the framework to get the current state of the view
        /// model. Can be called on save or app close.
        /// </summary>
        /// <returns></returns>
        TParams GetState();

        /// <summary>
        /// A way to flag whether this window can be closed. Window closing
        /// will be blocked until this returns true
        /// </summary>
        bool CanClose { get; }

        /// <summary>
        /// Called by the framework to pass this instance an Action that 
        /// can be called to close the window.
        /// </summary>
        /// <param name="close_"></param>
        void SetCloseAction(Action close_);

        /// <summary>
        /// Called when the window has been closed. This is not called if the 
        /// application is closed. Dispose will be called after this. Dispose
        /// will also be called when the application shuts down.
        /// </summary>
        void Closed();
    }
}