﻿using System.Xml.Linq;

namespace MSDesktop.ViewModelAPI
{
    public interface IFrameworkViewParameters
    {
        /// <summary>
        /// The version of this file. When you make a change
        /// you must increment the version. Then upgrade can
        /// perform the correct upgrade of the xml.
        /// </summary>
        int Version { get; set; }

        /// <summary>
        /// Returns the default values for this parameters
        /// </summary>
        /// <returns>The default</returns>
        IFrameworkViewParameters GetDefault();

        /// <summary>
        /// Upgrades the xml.
        /// </summary>
        /// <param name="version_">The version of the xDocument</param>
        /// <param name="xDocument_">The xml that is about to be deserialized</param>
        /// <returns>The upgraded xDoc</returns>
        XDocument Upgrade(int version_, XDocument xDocument_);

    }

    public interface IPersistableFrameworkViewParameters:IFrameworkViewParameters
    {
        
    }
}
