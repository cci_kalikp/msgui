﻿using System;
using System.Windows.Media;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MSDesktop.ViewModelAPI
{
    public class ButtonDefinition
    {
        public ButtonDefinition()
        {
            InitialViewLocation = InitialWindowLocation.FloatingAnywhere;
        }
        public string Id { get; set; }
        public string Text { get; set; }
        public string Tab { get; set; }
        public string Group { get; set; }
        public ImageSource Image { get; set; }
        public bool Singleton { get; set; }
        public InitialWindowLocation InitialViewLocation { get; set; }
        public string InitialViewTab { get; set; }
        public ButtonSize? Size { get; set; }
        public Action<IUnityContainer> OnViewCreated { get; set; }
    }
}