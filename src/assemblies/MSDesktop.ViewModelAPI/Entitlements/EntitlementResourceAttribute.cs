﻿using System;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MSDesktop.ViewModelAPI.Entitlements
{
    /// <summary>
    /// Enables to specify the entitlement resource that will be tested for a Load
    /// action based on the current environemnt.
    /// 
    /// For this attribute to be honored, you need to use the EnableViewModelApiEntitlements
    /// framework extention. Additionally, modules have to be added using AddViewModelBasedModule
    /// framework extension.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class EntitlementResourceAttribute : Attribute
    {
        public const string NoEclipsePermsRequired = "*";
        public const string NotAllowed = "-";

        public string Dev { get; set; }
        public string Qa { get; set; }
        public string Uat { get; set; }
        public string Prod { get; set; }

        public string GetEclipseResource(Environ env)
        {
            switch (env)
            {
                case Environ.Dev:
                    return Dev;
                case Environ.QA:
                    return Qa;
                case Environ.UAT:
                    return Uat;
                case Environ.Prod:
                    return Prod;
                default:
                    throw new InvalidOperationException();
            }
        }
    }
}
