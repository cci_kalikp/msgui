﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl;
using MSDesktop.Environment;
using MSDesktop.ModuleEntitlements;
using MorganStanley.MSDotNet.MSGui.Impl.Application;

namespace MSDesktop.ViewModelAPI.Entitlements
{
    public static class ViewModelAPIExtensions
    {
        private static readonly List<Type> ModulesToLoad = new List<Type>();

        public static void EnableViewModelApiEntitlements(this Framework framework)
        {
            EnvironmentExtensions.EnvironmentRegistered +=
                (sender, args) =>
                {
                    var environment = framework.Container.Resolve<IMSDesktopEnvironment>();
                    IModuleLoadInfos moduleLoadInfos = framework.Container.Resolve<IModuleLoadInfos>();
                    foreach (var module in ModulesToLoad)
                    {
                        var attribute = module.GetCustomAttributes(typeof(EntitlementResourceAttribute), false)
                            .OfType<EntitlementResourceAttribute>()
                            .Single();
                        var resource = attribute.GetEclipseResource(environment.Environment);
                        if (string.IsNullOrWhiteSpace(resource) || resource == EntitlementResourceAttribute.NotAllowed)
                        {
                            var moduleType = ModuleInfoHelper.GetModuleType(module);
                            var moduleLoadInfo = moduleLoadInfos.AddModuleLoadInfo(moduleType.AssemblyQualifiedName,
                                                              ModuleStatus.BlockedByNoEntitlementsInfo);
                            moduleLoadInfo.RejectReason = "Always rejected under " + environment.Environment.ToString() +
                                                          " environment";
                            continue;
                        }
                        if (resource == EntitlementResourceAttribute.NoEclipsePermsRequired)
                        {
                            framework.AddModule(module);
                            continue;
                        }

                        framework.AddModuleWithEntitlementCheck(module, resource);
                    }
                };
        }

        public static void AddViewModelBasedModule<TModule>(this Framework framework)
            where TModule : ViewModelBasedModule
        {
            ModulesToLoad.Add(typeof(TModule));
        }
    }
}
