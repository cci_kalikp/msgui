﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing; 
using System.Windows; 
using System.Xml.Linq;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions; 
using MorganStanley.MSDotNet.My;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;

namespace MSDesktop.ViewModelAPI
{
    public abstract class ViewModelBasedModule : IModule
    {
        private static readonly IMSLogger Logger = MSLoggerFactory.CreateLogger();

        private readonly IApplication _application;
        private readonly IChromeManager _chromeManager;
        private readonly IChromeRegistry _chromeRegistry;
        private readonly IUnityContainer _unityContainer;
        private readonly bool isRibbon;

        protected ViewModelBasedModule(IUnityContainer unityContainer)
        {
            _chromeRegistry = unityContainer.Resolve<IChromeRegistry>();
            _chromeManager = unityContainer.Resolve<IChromeManager>();
            _application = unityContainer.Resolve<IApplication>();
            _unityContainer = unityContainer;
            var initialSettings = _unityContainer.Resolve<InitialSettings>();
            isRibbon = initialSettings.ShellMode == ShellMode.RibbonAndFloatingWindows ||
                initialSettings.ShellMode == ShellMode.RibbonMDI ||
                initialSettings.ShellMode == ShellMode.LauncherBarAndWindow;
        }

        #region API

 
        /// <summary>
        /// Adds a button to the launcher bar that will open a view.
        /// The view and viewmodel are initialized via IOC and disposed
        /// when the view is disposed. The close events and persistence
        /// is all wired up for you.
        /// </summary>
        /// <typeparam name="TView">The view, must implement FrameworkElement</typeparam>
        /// <typeparam name="TViewModel">The view model to be hooked up to the view</typeparam>
        /// <typeparam name="TParams">The parameters that get persisted with this view</typeparam>
        /// <param name="viewId">The unique id for this view, used for persistence</param>
        /// <param name="buttonText">The text for the button or the tooltip if there is an image</param>
        /// <param name="group">group for the button to be placed</param>
        /// <param name="buttonImage">The image for the button</param>
        /// <param name="onViewCreated">Called when a view is created, use this to reg other things into IOC</param>
        protected void AddLauncherButton<TView, TViewModel, TParams>(
            string viewId,
            string buttonText,
            Bitmap buttonImage, 
            string group = null,
            Action<IUnityContainer> onViewCreated = null)
            where TParams : IFrameworkViewParameters, new()
            where TView : FrameworkElement
            where TViewModel : IFrameworkViewModel<TParams>
        {
            AddButton<TView, TViewModel, TParams>(new ButtonDefinition()
                {
                    Id = viewId,
                    Group = group,
                    Text = buttonText,
                    Image = buttonImage.ToImageSource(),
                    OnViewCreated = onViewCreated
                });
        }

       
        /// <summary>
        /// Adds a button that creates a singleton view. So if the view is already open it just 
        /// shows that view rather than create a new one.
        /// </summary>
        /// <typeparam name="TView"></typeparam>
        /// <typeparam name="TViewModel"></typeparam>
        /// <typeparam name="TParams"></typeparam>
        /// <param name="viewId"></param>
        /// <param name="buttonText"></param> 
        /// <param name="buttonImage"></param>
        /// <param name="onViewCreated"></param>
        protected void AddSingletonLauncherButton<TView, TViewModel, TParams>(
            string viewId,
            string buttonText,
            Bitmap buttonImage,
            string group = null,
            Action<IUnityContainer> onViewCreated = null)
            where TParams : IFrameworkViewParameters, new()
            where TView : FrameworkElement
            where TViewModel : IFrameworkViewModel<TParams>
        {
            AddButton<TView, TViewModel, TParams>(new ButtonDefinition()
            {
                Id = viewId,
                Singleton = true,
                Group = group,
                Text = buttonText,
                Image = buttonImage.ToImageSource(),
                OnViewCreated = onViewCreated
            });
        }

  
        /// <summary>
        /// Adds a button to the ribbon bar that will open a view.
        /// The view and viewmodel are initialized via IOC and disposed
        /// when the view is disposed. The close events and persistence
        /// is all wired up for you.
        /// </summary>
        /// <typeparam name="TView">The view, must implement FrameworkElement</typeparam>
        /// <typeparam name="TViewModel">The view model to be hooked up to the view</typeparam>
        /// <typeparam name="TParams">The parameters that get persisted with this view</typeparam>
        /// <param name="viewId">The unique id for this view, used for persistence</param>
        /// <param name="buttonText">The text for the button or the tooltip if there is an image</param>
        /// <param name="buttonImage">The image for the button</param>
        /// <param name="onViewCreated">Called when a view is created, use this to reg other things into IOC</param>
        protected void AddRibbonButton<TView, TViewModel, TParams>(
            string viewId,
            string buttonText,
            string ribbonTab,
            string ribbonGroup,
            Bitmap buttonImage,
            Action<IUnityContainer> onViewCreated = null)
            where TParams : IFrameworkViewParameters, new()
            where TView : FrameworkElement
            where TViewModel : IFrameworkViewModel<TParams>
        {
            AddButton<TView, TViewModel, TParams>(new ButtonDefinition()
            {
                Id = viewId,
                Tab = ribbonTab,
                Group = ribbonGroup,
                Text = buttonText,
                Image = buttonImage.ToImageSource(),
                OnViewCreated = onViewCreated
            });
        }


          

        /// <summary>
        /// Adds a button that creates a singleton view. So if the view is already open it just 
        /// shows that view rather than create a new one.
        /// </summary>
        /// <typeparam name="TView"></typeparam>
        /// <typeparam name="TViewModel"></typeparam>
        /// <typeparam name="TParams"></typeparam>
        /// <param name="viewId"></param>
        /// <param name="buttonText"></param>
        /// <param name="buttonImage"></param>
        /// <param name="onViewCreated"></param>
        protected void AddSingletonRibbonButton<TView, TViewModel, TParams>(
            string viewId,
            string buttonText,
            string ribbonTab,
            string ribbonGroup,
            Bitmap buttonImage,
            Action<IUnityContainer> onViewCreated = null)
            where TParams : IFrameworkViewParameters, new()
            where TView : FrameworkElement
            where TViewModel : IFrameworkViewModel<TParams>
        {
            AddButton<TView, TViewModel, TParams>(new ButtonDefinition()
            {
                Id = viewId,
                Singleton = true,
                Tab = ribbonTab,
                Group = ribbonGroup,
                Text = buttonText,
                Image = buttonImage.ToImageSource(),
                OnViewCreated = onViewCreated
            });
        }


        protected void AddButton<TView, TViewModel, TParams>(ButtonDefinition definition_)
            where TParams : IFrameworkViewParameters, new()
            where TView : FrameworkElement
            where TViewModel : IFrameworkViewModel<TParams>
        {
            var openViews = new ConcurrentDictionary<string, IFrameworkViewModel<TParams>>();
            //If the app is closing, we don't want to call close on the view model
            //This will mean just Dispose is called.
            _application.ApplicationClosed += (sender, args) => openViews.Clear();
            _chromeRegistry.RegisterWindowFactory(
               definition_.Id,
               (container, state) =>
                   CreateView<TView, TViewModel, TParams>(state, container, openViews, _unityContainer.CreateChildContainer(), 
                   definition_.OnViewCreated ?? (_ => { })),
               container => 
                   SaveView(container, openViews));

            var buttonParameter = new InitialButtonParameters
                {
                    Text = definition_.Text,
                    ToolTip = definition_.Text,
                    Image = definition_.Image,
                    Click =
                        (sender, args) =>
                        HandleButtonClick(definition_.Text, definition_.Id,
                                          definition_.Singleton ? definition_.Id : null,
                                          _chromeManager.ConvertToInitialLocation(definition_.InitialViewLocation, definition_.InitialViewTab),
                                          typeof (IPersistableFrameworkViewParameters).IsAssignableFrom(typeof (TParams)))
                };
            if (definition_.Size != null)
            {
                buttonParameter.Size = definition_.Size.Value;
            }
            _chromeManager.AddWidget(definition_.Id, buttonParameter, isRibbon ? 
                _chromeManager.GetParentContainer(definition_.Tab, definition_.Group):
                _chromeManager.GetParentContainer(definition_.Group, null)); 
        } 
        
        #endregion
          
        private void HandleButtonClick(string buttonText, string id, string singletonKey, InitialLocation initialLocation_, bool persistableView)
        {
            var initialWindowParameters = new InitialWindowParameters
                                              {
                                                  ResizeMode = ResizeMode.NoResize,
                                                  SizingMethod = SizingMethod.SizeToContent,
                                                  Singleton = !string.IsNullOrWhiteSpace(singletonKey),
                                                  SingletonKey = singletonKey ?? "" ,
                                                  InitialLocation = initialLocation_,
                                                  Persistable = persistableView
                                              };
            IWindowViewContainer window = _chromeManager.CreateWindow(id, initialWindowParameters);
            if (window == null)
                return;
            window.Title = buttonText;
        }

 

        private static XDocument SaveView<T>(
            IWindowViewContainer windowViewContainer,
            IDictionary<string, IFrameworkViewModel<T>> openViews)
            where T : IFrameworkViewParameters, new()
        {
            IFrameworkViewModel<T> viewModel;
            T viewParameters = openViews.TryGetValue(windowViewContainer.ID, out viewModel)
                                              ? viewModel.GetState()
                                              : (T) new T().GetDefault();
            return PersistenceExtensions.Store(viewParameters);
        }

        private static bool CreateView<TView, TViewModel, TParams>(
            XDocument state,
            IWindowViewContainer container,
            ConcurrentDictionary<string, IFrameworkViewModel<TParams>> openViews,
            IUnityContainer unityContainer,
            Action<IUnityContainer> onViewCreated)
            where TParams : IFrameworkViewParameters, new()
            where TView : FrameworkElement
            where TViewModel : IFrameworkViewModel<TParams>
        {
            try
            {
                onViewCreated(unityContainer);
                unityContainer.RegisterType<TView>(new ContainerControlledLifetimeManager());
                unityContainer.RegisterType<TViewModel>(new ContainerControlledLifetimeManager());
                var stateDerserialized = StateDeserialized<TParams>(state);
                var viewModel = unityContainer.Resolve<TViewModel>();
                viewModel.SetState(stateDerserialized);
                var view = unityContainer.Resolve<TView>();
                view.DataContext = viewModel;
                openViews[container.ID] = viewModel;
                viewModel.SetCloseAction(container.Close);

                EventHandler<CancelEventArgs> closingHandler = null;
                container.Closing += closingHandler = (sender_, args_) => HandleViewClosing<TViewModel, TParams>(viewModel, args_); 

                EventHandler<WindowEventArgs> closeHandler = null;
                closeHandler = (sender_, args_) =>
                    {
                        HandleViewClosed(container, openViews, unityContainer);
                        container.Closed -= closeHandler;
                        container.ClosedQuietly -= closeHandler;
                        container.Closing -= closingHandler;
                    };
                container.Closed += closeHandler;
                container.ClosedQuietly += closeHandler; 

                container.Content = view;
                return true;
            }
            catch
            {
                Logger.Error("View of type '{0}' could not be created", typeof(TView).ToString());
                return false;
            }
        }

        private static void HandleViewClosing<TViewModel, TParams>(
            TViewModel viewModel,
            CancelEventArgs args)
            where TParams : IFrameworkViewParameters, new()
            where TViewModel : IFrameworkViewModel<TParams>
        {
            if (!viewModel.CanClose)
                args.Cancel = true;
        }

        private static void HandleViewClosed<TParams>(
            IWindowViewContainer container,
            ConcurrentDictionary<string, IFrameworkViewModel<TParams>> openViews,
            IUnityContainer unityContainer)
            where TParams : IFrameworkViewParameters, new()
        {
            IFrameworkViewModel<TParams> viewModel;
            if (openViews.TryRemove(container.ID, out viewModel))
                viewModel.Closed();
            unityContainer.Dispose();
        }

        private static TParams StateDeserialized<TParams>(XDocument state)
            where TParams : IFrameworkViewParameters, new()
        {
            TParams stateDeserialized;
            var defaultParams = (TParams) new TParams().GetDefault();
            try
            {
                if (state == null || state.Root == null)
                {
                    stateDeserialized = defaultParams;
                }
                else
                {
                    XElement xElement = state.Root.Element("Version");
                    int version;
                    if (xElement != null && int.TryParse(xElement.Value, out version))
                    {
                        stateDeserialized =
                            PersistenceExtensions.Restore<TParams>(defaultParams.Upgrade(version, state));
                    }
                    else
                    {
                        stateDeserialized = defaultParams;
                    }
                }
            }
            catch
            {
                stateDeserialized = defaultParams;
            }
            return stateDeserialized;
        }

        public abstract void Initialize();
    }
}