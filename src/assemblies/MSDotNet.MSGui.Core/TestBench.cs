﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/TestBench.cs#43 $
// $Change: 901195 $
// $DateTime: 2014/10/16 05:40:06 $
// $Author: caijin $

#pragma warning disable 612,618,67
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Xml.Linq;
using Microsoft.Practices.Composite;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Composite.UnityExtensions;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core.Ribbon;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using Brush = System.Windows.Media.Brush;
using Size = System.Windows.Size;
using System.Collections.ObjectModel;

namespace MorganStanley.MSDotNet.MSGui.Core
{
	public class TestBench : IDisposable
	{
		private readonly IUnityContainer _container = new UnityContainer();
		private readonly ILoggerFacade _loggerFacade = new TextLogger();
		private TestBenchApplication _application;
		private TestBenchModuleCatalog _moduleCatalog;
		private IList<Type> _moduleTypes;
		private TestBenchApplicationOptions _options;
		private TestBenchPersistence _persistenceService;
		private TestBenchSplashScreen _splashScreen;
		private TestBenchStatusBar _statusBar;
		private TestBenchUnityContainerExtension _unityContainerExtension;
		private TestBenchViewManager _viewManager;
		//private TestBenchPersistenceProfileService _persistenceProfileService;

		#region IDisposable Members

		public void Dispose()
		{
		}

		#endregion

		public static TestBench For<TModule>() where TModule : IModule
		{
			var testbench = new TestBench { _moduleTypes = new List<Type> { typeof(TModule) } };
			testbench.EnsureMSGuiInitialized();
			return testbench;
		}

		public TModule InitializeModule<TModule>() where TModule : IModule
		{
			_container.Resolve<IModuleManager>().LoadModule(typeof(TModule).AssemblyQualifiedName);
			return
			  (TModule)
			  ((TestBenchUnityServiceLocatorAdapter)_container.Resolve<IServiceLocator>()).CreatedInstances[
				typeof(TModule)];
		}

		public void DestroyModule<TModule>() where TModule : IModule
		{
			IDictionary<Type, object> createdInstances =
			  ((TestBenchUnityServiceLocatorAdapter)_container.Resolve<IServiceLocator>()).CreatedInstances;
			var module = (TModule)createdInstances[typeof(TModule)];
			_container.Teardown(module);

			_unityContainerExtension.extensionContext.Lifetime.Remove(module);

			_moduleCatalog.ModuleInfos[typeof(TModule)].State = ModuleState.NotStarted;

			createdInstances.Remove(typeof(TModule));
		}

		public void CreateView<TModule>() where TModule : IModule
		{
		}

		private void EnsureMSGuiInitialized()
		{
			ExceptionExtensions.RegisterFrameworkExceptionType(
			  typeof(ActivationException));

			ExceptionExtensions.RegisterFrameworkExceptionType(
			  typeof(ResolutionFailedException));
			_unityContainerExtension = new TestBenchUnityContainerExtension();
			_container.AddExtension(_unityContainerExtension);
			_container.RegisterInstance(_loggerFacade);
			_moduleCatalog = new TestBenchModuleCatalog(_moduleTypes);
			_container.RegisterInstance((IModuleCatalog)_moduleCatalog);

			_container.RegisterType(typeof(IServiceLocator), typeof(TestBenchUnityServiceLocatorAdapter),
									new ContainerControlledLifetimeManager());
			_container.RegisterType(typeof(IModuleInitializer), typeof(ModuleInitializer),
									new ContainerControlledLifetimeManager());
			_container.RegisterType(typeof(IModuleManager), typeof(ModuleManager),
									new ContainerControlledLifetimeManager());
			_container.RegisterType(typeof(IEventAggregator), typeof(EventAggregator),
									new ContainerControlledLifetimeManager());

			ServiceLocator.SetLocatorProvider(() => _container.Resolve<IServiceLocator>());

			_application = new TestBenchApplication();
			_viewManager = new TestBenchViewManager( /*_persistenceProfileService*/);
			_statusBar = new TestBenchStatusBar();
			_splashScreen = new TestBenchSplashScreen();
			_persistenceService = new TestBenchPersistence();
			_options = new TestBenchApplicationOptions();
			_container.RegisterInstance<IApplication>(_application);
			_container.RegisterInstance<IViewManager>(_viewManager);
			_container.RegisterInstance<IStatusBar>(_statusBar);
			_container.RegisterInstance<IPersistenceService>(_persistenceService);
			_container.RegisterInstance<ISplashScreen>(_splashScreen);
			_container.RegisterInstance<IApplicationOptions>(_options);

			_moduleCatalog.Initialize();
		}

		public IList<string> GetSplashScreenTexts()
		{
			return _splashScreen.texts;
		}

		public void ClearSplashScreenTexts()
		{
			_splashScreen.texts.Clear();
		}

		public int ViewCreatorsCount()
		{
			return _viewManager._creators.Count;
		}

		public bool ContainsViewCreator(string key)
		{
			return _viewManager._creators.ContainsKey(key);
		}

		public void RemoveViewCreator(string key)
		{
			_viewManager._creators.Remove(key);
		}

		public void PushRibbonButton(RibbonItemData ribbonItemData)
		{
			if (ribbonItemData.ribbonItem is WrapperPannelRibbonItem)
			{
				PushRibbonButton(((WrapperPannelRibbonItem)ribbonItemData.ribbonItem).Items.First());
			}
			else if (ribbonItemData.ribbonItem is RibbonButton)
			{
				PushRibbonButton((RibbonButton)ribbonItemData.ribbonItem);
			}
			else if (ribbonItemData.ribbonItem is RibbonButtonGroup)
			{
				PushRibbonButton(((RibbonButtonGroup)ribbonItemData.ribbonItem).Buttons.First());
			}
		}

		public void PushRibbonButton(RibbonItem ribbonItem)
		{
			if (ribbonItem is RibbonButton)
				PushRibbonButton((RibbonButton)ribbonItem);
		}

		public void PushRibbonButton(RibbonButton ribbonButton)
		{
			if (ribbonButton.Clicked != null)
				ribbonButton.Clicked(this, EventArgs.Empty);
		}

		public IViewContainer GetActiveViewContainer()
		{
			return _viewManager._activeViewContainer;
		}

		public void SetActiveViewContainer(IViewContainer container)
		{
			_viewManager._activeViewContainer = container;
		}

		public ObservableCollection<IViewContainer> GetViewContainers()
		{
			return _viewManager.Views;
		}

		public void RemoveViewContainer(IViewContainer container)
		{
			_viewManager.Views.Remove(container);
		}

		public void AddViewContainer(IViewContainer container)
		{
			_viewManager.Views.Add(container);
		}

		//#region Nested type: IProfileMonitor

		//internal interface IProfileMonitor
		//{
		//  string CurrentProfile { get; }
		//  ReadOnlyProfilesCollection AvailableProfiles { get; }
		//  event EventHandler<CurrentProfileChangedEventArgs> CurrentProfileChanged;
		//}

		//#endregion

		#region Nested type: RibbonItemData

		public struct RibbonItemData
		{
			public string group;
#pragma warning disable 612,618
			public RibbonItem ribbonItem;
#pragma warning restore 612,618
			public string tab;
		}

		#endregion

		#region Nested type: TestBenchApplication

		private class TestBenchApplication : IApplication
		{
			#region IApplication Members

			public string Name
			{
				get { return ""; }
			}

			public string Version
			{
				get { return new Version(1, 2, 3, 4).ToString(); }
			}

			public string[] CommandLine
			{
				get { return new[] { "" }; }
			}

			public event EventHandler ApplicationLoaded;

			public void Quit()
			{
				return;
			}

			public event EventHandler ApplicationClosed;

			#endregion
		}

		#endregion

		#region Nested type: TestBenchApplicationOptions

		private class TestBenchApplicationOptions : IApplicationOptions
		{
			#region IApplicationOptions Members

			public void AddOptionPage(string path, string title, IOptionView view)
			{
				throw new NotImplementedException();
			}

		    public void AddViewModelBasedOptionPage<TViewModel>(string path, string title, bool global) where TViewModel : IOptionViewViewModel
		    {
		        throw new NotImplementedException();
		    }

		    public void RemoveOptionPage(string path, string title)
		    {
		        throw new NotImplementedException();
		    }

            public void AddOptionPage(string path, string title, IOptionView view, int priority)
            {
                throw new NotImplementedException();
            }

            public void AddViewModelBasedOptionPage<TViewModel>(string path, string title, bool global, int priority) where TViewModel : IOptionViewViewModel
            {
                throw new NotImplementedException();
            }
		    #endregion


        }

		#endregion

		#region Nested type: TestBenchModuleCatalog

		private class TestBenchModuleCatalog : ModuleCatalog
		{
			public readonly IDictionary<Type, ModuleInfo> ModuleInfos = new Dictionary<Type, ModuleInfo>();
			private readonly IList<Type> _moduleTypes;

			public TestBenchModuleCatalog(IList<Type> _moduleTypes)
			{
				this._moduleTypes = _moduleTypes;
			}

			protected override void InnerLoad()
			{
				foreach (Type moduleType in _moduleTypes)
				{
					var moduleInfo = new ModuleInfo
									   {
										   ModuleName = moduleType.AssemblyQualifiedName,
										   ModuleType = moduleType.AssemblyQualifiedName,
										   InitializationMode = InitializationMode.OnDemand
									   };

					ModuleInfos.Add(moduleType, moduleInfo);
					AddModule(moduleInfo);
				}
			}
		}

		#endregion

		#region Nested type: TestBenchPersistence

		private class TestBenchPersistence : IPersistenceService
		{

            public void AddPersistor(string regeneratorId, RestoreStateHandler restoreStateHandler, SaveStateHandler saveStateHandler)
            {
                
            } 
            public void AddGlobalPersistor(string persistorId, RestoreStateHandler restoreStateHandler, SaveStateHandler saveStateHandler)
            {
                
            } 
            public void LoadProfile(string name)
            {
                 
            }

            public void SaveProfileAs()
            {
                 
            }

            public void SaveProfileAs(string name)
            {
                 
            }

            public void SaveCurrentProfile()
            {
                 
            }

            public ObservableCollection<string> GetProfileNames()
            {
                return null;
            }

            public void DeleteCurrentProfile()
            {
                 
            }

            public string GetCurrentProfileName()
            {
                return null;
            }

            public void MakeProfileDefault(string name)
            {
                 
            }

            public void PersistGlobalItems(IEnumerable<string> keys = null)
            {
                 
            }



            public void RemovePersistor(string persistorId)
            {
                 
            }

            public void RemoveGlobalPersistor(string persistorId)
            {
                 
            }


            public void PersistCurrentProfileItems(IEnumerable<string> keys)
            {

            }

            public void PersistProfileItems(string profileName, IEnumerable<string> keys)
            {

            }

            public void LoadGlobalItems(IEnumerable<string> keys = null)
            {

            }

            public void LoadCurrentProfileItems(IEnumerable<string> keys)
            {

            }

            public void LoadProfileItems(string profileName, IEnumerable<string> keys)
            {

            }


            public event EventHandler<ProfileSavedEventArgs> ProfileSaved;
        }

		#endregion

		//#region Nested type: TestBenchPersistenceProfileService

		//private class TestBenchPersistenceProfileService : IProfileMonitor
		//{
		//  private static readonly ILog m_logger = LogManager.GetLogger(typeof (PersistenceProfileService));
		//  private readonly Dispatcher m_currentDispatcher;

		//  private readonly IItemsDictionaryService m_itemsDictionaryService;
		//  private readonly IStatusBar m_statusBar;
		//  private readonly IPersistenceStorage m_storage;
		//  private readonly StorageAccessorRunner m_storageAccessorRunner;
		//  private string m_currentProfile;

		//  private Dictionary<string, XElement> m_lastSavedStateOfCurrentProfile = new Dictionary<string, XElement>();

		//  public PersistenceProfileService(
		//    IPersistenceStorage storage_,
		//    IItemsDictionaryService itemsDictionaryService_,
		//    IStatusBar statusBar_)
		//  {
		//    m_storage = storage_;
		//    m_itemsDictionaryService = itemsDictionaryService_;
		//    m_statusBar = statusBar_;
		//    m_currentDispatcher = Dispatcher.CurrentDispatcher;
		//    m_storageAccessorRunner = new StorageAccessorRunner();
		//    m_storageAccessorRunner.ExecutionExceptionHappened += OnExecutionExceptionHappened;
		//  }

		//  internal StorageAccessorRunner StorageAccessorRunner
		//  {
		//    get { return m_storageAccessorRunner; }
		//  }

		//  #region IProfileMonitor Members

		//  public string CurrentProfile
		//  {
		//    get { return m_currentProfile; }
		//    private set
		//    {
		//      string oldvalue = m_currentProfile;
		//      m_currentProfile = value;
		//      var copy = CurrentProfileChanged;
		//      if (copy != null)
		//      {
		//        DoDispatchedAction(() => copy(this, new CurrentProfileChangedEventArgs(value, oldvalue)));
		//      }
		//    }
		//  }

		//  public ReadOnlyProfilesCollection AvailableProfiles
		//  {
		//    get { return m_storage.AvailableProfiles; }
		//  }

		//  public event EventHandler<CurrentProfileChangedEventArgs> CurrentProfileChanged;

		//  #endregion

		//  private bool CheckProfileValidity(string profile_)
		//  {
		//    if (m_storage == null)
		//    {
		//      m_statusBar.MainItem.UpdateStatus(StatusLevel.Critical, "ProfileStorage is not set!");
		//      return false;
		//    }
		//    if (string.IsNullOrEmpty(profile_))
		//    {
		//      m_statusBar.MainItem.UpdateStatus(StatusLevel.Critical, "Empty profile!");
		//      return false;
		//    }
		//    return true;
		//  }

		//  public bool CheckIsUnchanged(out Dictionary<string, XElement> dictionary_)
		//  {
		//    var stateDict = m_itemsDictionaryService.SaveItemsToDict();
		//    dictionary_ = stateDict;
		//    if (m_lastSavedStateOfCurrentProfile == null)
		//    {
		//      return false;
		//    }
		//    if (stateDict.Keys.Any(key_ => !m_lastSavedStateOfCurrentProfile.ContainsKey(key_)))
		//    {
		//      return false;
		//    }

		//    if (m_lastSavedStateOfCurrentProfile.Keys.Any(key_ => !stateDict.ContainsKey(key_)))
		//    {
		//      return false;
		//    }

		//    return
		//      stateDict.Keys.All(key_ => m_lastSavedStateOfCurrentProfile[key_].ToString() == stateDict[key_].ToString());
		//  }

		//  public void DeleteProfile(string profile_)
		//  {
		//    if (m_storage == null)
		//    {
		//      m_statusBar.MainItem.UpdateStatus(StatusLevel.Critical, "ProfileStorage is not set!");
		//      return;
		//    }
		//    m_storageAccessorRunner.Run(
		//      delegate
		//        {
		//          try
		//          {
		//            m_statusBar.MainItem.UpdateStatus(StatusLevel.Information, "Deleting profile " + profile_);
		//            m_storage.DeleteState(profile_);
		//            if (profile_ == CurrentProfile)
		//            {
		//              //???????????? TODO 
		//              CurrentProfile = null;
		//              m_lastSavedStateOfCurrentProfile.Clear();
		//            }
		//          }
		//          catch (Exception exception)
		//          {
		//            m_logger.ErrorFormat("Failed to delete state " + profile_ + " " + exception.Message);
		//            m_statusBar.MainItem.UpdateStatus(
		//              StatusLevel.Critical,
		//              "Failed to delete state " + profile_ + " " + exception.Message);
		//            return;
		//          }
		//          m_statusBar.MainItem.UpdateStatus(StatusLevel.Information,
		//                                            string.Format("Profile '{0}' deleted.", profile_));
		//        });
		//  }

		//  public void RenameProfile(string profile_, string newName_)
		//  {
		//    if (m_storage == null)
		//    {
		//      m_statusBar.MainItem.UpdateStatus(StatusLevel.Critical, "ProfileStorage is not set!");
		//      return;
		//    }
		//    m_storageAccessorRunner.Run(
		//      delegate
		//        {
		//          try
		//          {
		//            m_storage.RenameState(profile_, newName_);
		//            if (profile_ == CurrentProfile)
		//            {
		//              CurrentProfile = newName_;
		//            }
		//          }
		//          catch (Exception exception)
		//          {
		//            m_logger.ErrorFormat("Failed to rename state " + profile_ + " " + exception.Message);
		//            m_statusBar.MainItem.UpdateStatus(
		//              StatusLevel.Critical,
		//              "Failed to rename state " + profile_ + " " + exception.Message);
		//            return;
		//          }
		//        });
		//  }

		//  private void DoDispatchedAction(Action action)
		//  {
		//    if (m_currentDispatcher.CheckAccess())
		//    {
		//      action.Invoke();
		//    }
		//    else
		//    {
		//      m_currentDispatcher.BeginInvoke(DispatcherPriority.Normal, action).Wait();
		//    }
		//  }

		//  private void OnExecutionExceptionHappened(object sender, ExecutionExceptionEventArgs e)
		//  {
		//    m_logger.ErrorFormat(e.Exception.Message);
		//    m_statusBar.MainItem.UpdateStatus(StatusLevel.Critical,
		//                                      string.Format(
		//                                        "Error: {0}",
		//                                        e.Exception.Message));
		//  }

		//  #region static

		//  //Calls: 
		//  //1) XDocumentToDict --> UnpackItem
		//  //2) DictToXDocument --> PackItem -> BuildXPath --> BuildParentPath --> FindOrder

		//  internal static XElement PackItem(string itemKey_, XElement element_, bool removeSubitems_)
		//  {
		//    var elementToAdd = new XElement(element_);
		//    var itemElement = new XElement("Item",
		//                                   new XAttribute("Id", itemKey_));
		//    itemElement.Add(elementToAdd);
		//    if (element_ is XElementWithSubitems)
		//    {
		//      Dictionary<string, XElement> subitems = ((XElementWithSubitems) element_).Subitems;
		//      var subitemsElement = new XElement("Subitems");
		//      foreach (string key in subitems.Keys)
		//      {
		//        string xpath = BuildXPath(element_, subitems[key]);
		//        subitemsElement.Add(
		//          new XElement("Subitem",
		//                       new XAttribute("Id", key),
		//                       new XAttribute("XPath", xpath)));
		//        if (removeSubitems_)
		//        {
		//          XElement subitemElement = elementToAdd.XPathSelectElement(xpath);
		//          subitemElement.ReplaceWith(new XElement(subitemElement.Name.LocalName));
		//        }
		//      }
		//      itemElement.Add(subitemsElement);
		//    }
		//    return itemElement;
		//  }

		//  internal static XDocument DictToXDocument(Dictionary<string, XElement> dict_)
		//  {
		//    var rootElement = new XElement("State");
		//    foreach (string itemKey in dict_.Keys)
		//    {
		//      try
		//      {
		//        rootElement.Add(PackItem(itemKey, dict_[itemKey], false));
		//      }
		//      catch (Exception exception)
		//      {
		//        m_logger.ErrorFormat("Failed to process item {0}. Error: {1}", itemKey, exception.Message);
		//      }
		//    }
		//    return new XDocument(rootElement);
		//  }

		//  internal static KeyValuePair<string, XElement> UnpackItem(XElement itemXElement_)
		//  {
		//    string id = itemXElement_.Attribute("Id").Value;
		//    var elements = new List<XElement>(itemXElement_.Elements());
		//    switch (elements.Count)
		//    {
		//      case 1:
		//        return new KeyValuePair<string, XElement>(id, elements[0]);
		//      case 2:
		//        var result = new XElementWithSubitems(elements[0]);
		//        foreach (XElement subelementNode in elements[1].Elements("Subitem"))
		//        {
		//          result.Subitems[subelementNode.Attribute("Id").Value] =
		//            result.XPathSelectElement(subelementNode.Attribute("XPath").Value);
		//        }
		//        return new KeyValuePair<string, XElement>(id, result);
		//    }
		//    throw new InvalidOperationException("Unxepected Item xml format");
		//  }

		//  internal static Dictionary<string, XElement> XDocumentToDict(XDocument document_)
		//  {
		//    if (document_.Root == null)
		//    {
		//      return new Dictionary<string, XElement>();
		//    }

		//    //return document_.Root.Elements("Item").
		//    //  Where(node_ => (string) node_.Attribute("Id") != null).
		//    //  Select(UnpackItem).
		//    //  ToDictionary(pair_ => pair_.Key, pair_ => pair_.Value);
		//    var result = new Dictionary<string, XElement>();
		//    foreach (XElement element in document_.Root.Elements("Item").
		//      Where(node_ => (string) node_.Attribute("Id") != null))
		//    {
		//      KeyValuePair<string, XElement> unpacked;
		//      try
		//      {
		//        unpacked = UnpackItem(element);
		//      }
		//      catch (Exception exception)
		//      {
		//        m_logger.ErrorFormat("Failed to unpack state item {0}. Error: {1}", element.ToString(), exception);
		//        continue;
		//      }
		//      result[unpacked.Key] = unpacked.Value;
		//    }
		//    return result;
		//  }

		//  private static string BuildXPath(XElement parentNode_, XElement childNode_)
		//  {
		//    if (parentNode_ == childNode_)
		//    {
		//      return ".";
		//    }
		//    if (parentNode_.Descendants().Contains(childNode_))
		//    {
		//      return BuildParentPath(parentNode_, childNode_);
		//    }
		//    throw new Exception("BuildXPath error");
		//  }

		//  private static string BuildParentPath(XElement parentNode_, XElement childNode_)
		//  {
		//    if (childNode_ == parentNode_)
		//    {
		//      return string.Empty;
		//    }
		//    string prefix = BuildParentPath(parentNode_, childNode_.Parent);
		//    if (!string.IsNullOrEmpty(prefix))
		//    {
		//      prefix = prefix + "/";
		//    }

		//    return string.Format("{0}{1}[{2}]", prefix, childNode_.Name.LocalName, FindOrder(childNode_));
		//  }

		//  private static int FindOrder(XElement childNode_)
		//  {
		//    return childNode_.ElementsBeforeSelf(childNode_.Name.LocalName).Count() + 1;
		//  }

		//  #endregion

		//  #region Saving

		//  public void SaveCurrentProfile()
		//  {
		//    if (string.IsNullOrEmpty(CurrentProfile))
		//    {
		//      //we don't throw any exceptions here, presuming that emptiness of the current profile is checked before the call        
		//      //we don't want to throw any unhandled exceptions from this class
		//      m_statusBar.MainItem.UpdateStatus(StatusLevel.Critical, "Current profile's name is not specified");
		//    }
		//    SaveCurrentProfileAs(CurrentProfile);
		//  }

		//  public void SaveCurrentProfileAs(string profile_)
		//  {
		//    if (!CheckProfileValidity(profile_))
		//      return;

		//    Dictionary<string, XElement> savedStateDict = m_itemsDictionaryService.SaveItemsToDict();
		//    m_statusBar.MainItem.UpdateStatus(StatusLevel.Information, "Saving profile " + profile_);

		//    if (m_storage is IPartialPersistenceStorage)
		//    {
		//      SaveDictionaryForPartialStorage(savedStateDict, profile_, true);
		//    }
		//    else
		//    {
		//      SaveDictionaryForNonPartialStorage(savedStateDict, profile_);
		//    }
		//    CurrentProfile = profile_;
		//    //left for historical reasons
		//    if (profile_ == CurrentProfile)
		//    {
		//      m_lastSavedStateOfCurrentProfile = savedStateDict;
		//    }
		//    m_statusBar.MainItem.UpdateStatus(StatusLevel.Information, string.Format("Profile '{0}' saved.", profile_));
		//  }

		//  public void SaveCurrentProfileForKeys(IEnumerable<string> keys_)
		//  {
		//    SaveCurrentProfileAsForKeys(CurrentProfile, keys_);
		//  }

		//  public void SaveCurrentProfileAsForKeys(string profile_, IEnumerable<string> keys_)
		//  {
		//    //TODO i'm not sure what is better here. Maybe we should check if there
		//    //exists a profile with such name and use it's last saved state instead of the current      
		//    SaveItemsDictionaryAndRestItemsFromPrevious(m_itemsDictionaryService.SaveItemsToDict(keys_), profile_);
		//    //Should we change the current profile here?
		//  }

		//  private void ResaveLastSaved(string profile_)
		//  {
		//    SaveItemsDictionaryAs(m_lastSavedStateOfCurrentProfile, profile_);
		//  }

		//  public void SaveSubitem(
		//    string itemId_,
		//    string subitemId_,
		//    XElement subitemXml_,
		//    Action<XElementWithSubitems, XElement> addNewSubItem_,
		//    XElement cleanItemXml_)
		//  {
		//    SaveSubitemAs(itemId_, subitemId_, subitemXml_, addNewSubItem_, cleanItemXml_, CurrentProfile);
		//  }

		//  public void SaveSubitemAs(
		//    string itemId_,
		//    string subitemId_,
		//    XElement subitemXml_,
		//    Action<XElementWithSubitems, XElement> addNewSubItem_,
		//    XElement cleanItemXml_,
		//    string profile_)
		//  {
		//    if (!m_lastSavedStateOfCurrentProfile.ContainsKey(itemId_))
		//    {
		//      var cleanWithSubitemsXml = new XElementWithSubitems(cleanItemXml_);
		//      addNewSubItem_(cleanWithSubitemsXml, subitemXml_);
		//      cleanWithSubitemsXml.Subitems[subitemId_] = subitemXml_;
		//      m_lastSavedStateOfCurrentProfile.Add(itemId_, cleanWithSubitemsXml);
		//    }
		//    else
		//    {
		//      if (!(m_lastSavedStateOfCurrentProfile[itemId_] is XElementWithSubitems))
		//      {
		//        //TODO we are in trouble. throw an exception?
		//        return;
		//      }
		//      var itemElement = m_lastSavedStateOfCurrentProfile[itemId_] as XElementWithSubitems;
		//      if (itemElement.Subitems.ContainsKey(subitemId_))
		//      {
		//        ReplaceSubitem(itemElement, subitemId_, subitemXml_);
		//      }
		//      else
		//      {
		//        addNewSubItem_(itemElement, subitemXml_);
		//        itemElement.Subitems[subitemId_] = subitemXml_;
		//      }
		//    }
		//    ResaveLastSaved(profile_);
		//  }

		//  private static void ReplaceSubitem(XElementWithSubitems xelement_, string subitemId_, XElement replacement_)
		//  {
		//    replacement_ = new XElement(replacement_);
		//    xelement_.Subitems[subitemId_].ReplaceWith(replacement_);
		//    xelement_.Subitems[subitemId_] = replacement_;
		//  }

		//  private void SaveDictionaryForPartialStorage(Dictionary<string, XElement> dict_, string profile_, bool clean_)
		//  {
		//    m_storageAccessorRunner.Run(
		//      delegate
		//        {
		//          try
		//          {
		//            var partStorage = (IPartialPersistenceStorage) m_storage;
		//            var itemsToSave = new Dictionary<string, XElement>();
		//            var subitemsToSave = new Dictionary<string, XElement>();
		//            foreach (string key in dict_.Keys)
		//            {
		//              XElement itemXelement;
		//              try
		//              {
		//                itemXelement = PackItem(key, dict_[key], true);
		//              }
		//              catch (Exception exception)
		//              {
		//                m_logger.ErrorFormat("Failed to save item {0} for profile {1}. Error: {2}", key, profile_,
		//                                     exception.Message);
		//                m_statusBar.MainItem.UpdateStatus(StatusLevel.Critical,
		//                                                  string.Format(
		//                                                    "Failed to save item {0} for profile {1}. Error: {2}",
		//                                                    key, profile_, exception.Message));
		//                continue;
		//              }
		//              if (dict_[key] is XElementWithSubitems)
		//              {
		//                var withSub = (XElementWithSubitems) dict_[key];
		//                foreach (string subitemKey in withSub.Subitems.Keys)
		//                {
		//                  subitemsToSave[subitemKey] = withSub.Subitems[subitemKey];
		//                }
		//              }
		//              itemsToSave[key] = itemXelement;
		//            }
		//            partStorage.SaveState(profile_, itemsToSave, subitemsToSave, clean_);
		//          }
		//          catch (Exception exception)
		//          {
		//            m_logger.ErrorFormat("Failed to save state " + profile_ + " " + exception.Message);
		//            m_statusBar.MainItem.UpdateStatus(
		//              StatusLevel.Critical,
		//              "Failed to save state " + profile_ + " " + exception.Message);
		//            return;
		//          }
		//        });
		//  }

		//  private void SaveDictionaryForNonPartialStorage(Dictionary<string, XElement> dict_, string profile_)
		//  {
		//    XDocument stateDoc = DictToXDocument(dict_);
		//    m_storageAccessorRunner.Run(
		//      delegate
		//        {
		//          try
		//          {
		//            m_storage.SaveState(stateDoc, profile_);
		//          }
		//          catch (Exception exception)
		//          {
		//            m_logger.ErrorFormat("Failed to save state " + profile_ + " " +
		//                                 exception.Message);
		//            m_statusBar.MainItem.UpdateStatus(
		//              StatusLevel.Critical,
		//              "Failed to save state " + profile_ + " " + exception.Message);
		//            return;
		//          }
		//        });
		//  }

		//  private void SaveItemsDictionaryAndRestItemsFromPrevious(Dictionary<string, XElement> savedStateDict_,
		//                                                           string profile_)
		//  {
		//    if (!CheckProfileValidity(profile_))
		//      return;

		//    var combinedDict = new Dictionary<string, XElement>();
		//    foreach (string key in m_lastSavedStateOfCurrentProfile.Keys)
		//    {
		//      combinedDict[key] = m_lastSavedStateOfCurrentProfile[key];
		//    }
		//    foreach (string key in savedStateDict_.Keys)
		//    {
		//      combinedDict[key] = savedStateDict_[key];
		//    }

		//    if (m_storage is IPartialPersistenceStorage)
		//    {
		//      SaveDictionaryForPartialStorage(savedStateDict_, profile_, false);
		//    }
		//    else
		//    {
		//      SaveDictionaryForNonPartialStorage(combinedDict, profile_);
		//    }
		//    if (profile_ == CurrentProfile)
		//    {
		//      m_lastSavedStateOfCurrentProfile = combinedDict;
		//    }
		//  }

		//  internal void SaveItemsDictionaryAs(Dictionary<string, XElement> dictionary_, string profile_)
		//  {
		//    //!!!we don't change the current profile here
		//    if (m_storage is IPartialPersistenceStorage)
		//    {
		//      SaveDictionaryForPartialStorage(dictionary_, profile_, true);
		//    }
		//    else
		//    {
		//      SaveDictionaryForNonPartialStorage(dictionary_, profile_);
		//    }
		//  }

		//  internal void SaveItemsDictionary(Dictionary<string, XElement> dictionary_)
		//  {
		//    SaveItemsDictionaryAs(dictionary_, CurrentProfile);
		//  }

		//  #endregion

		//  #region Loading

		//  public void LoadProfile(string profile_)
		//  {
		//    if (!CheckProfileValidity(profile_))
		//      return;

		//    m_storageAccessorRunner.Run(
		//      delegate
		//        {
		//          XDocument state;
		//          try
		//          {
		//            m_statusBar.MainItem.UpdateStatus(StatusLevel.Information, "Loading profile " + profile_);

		//            if (m_storage is IPartialPersistenceStorage)
		//            {
		//              state = LoadPartial(profile_);
		//            }
		//            else
		//            {
		//              state = m_storage.LoadState(profile_);
		//            }
		//          }
		//          catch (Exception exception)
		//          {
		//            m_logger.ErrorFormat("Failed to load state " + profile_ + " " + exception.Message);
		//            m_statusBar.MainItem.UpdateStatus(
		//              StatusLevel.Critical,
		//              "Failed to load state " + profile_ + " " + exception.Message);
		//            return;
		//          }

		//          if (state != null)
		//          {
		//            Dictionary<string, XElement> dict = XDocumentToDict(state);
		//            DoDispatchedAction(
		//              () =>
		//                {
		//                  try
		//                  {
		//                    m_itemsDictionaryService.RestoreItemsFromDict(dict);
		//                  }
		//                  catch (Exception exception)
		//                  {
		//                    m_storageAccessorRunner.InvokeExceptionEvent(exception);
		//                  }
		//                });
		//            m_lastSavedStateOfCurrentProfile = dict;
		//          }

		//          CurrentProfile = profile_;

		//          m_statusBar.MainItem.UpdateStatus(StatusLevel.Information,
		//                                            string.Format("Profile '{0}' loaded.", profile_));
		//        });
		//  }

		//  private XDocument LoadPartial(string profile_)
		//  {
		//    var partStorage = (IPartialPersistenceStorage) m_storage;
		//    Dictionary<string, XElement> items;
		//    Dictionary<string, XElement> subitems;

		//    //called from LoadProfile from a thread pool thread
		//    partStorage.LoadState(profile_, out items, out subitems);

		//    var unpackeditems = new Dictionary<string, XElement>();
		//    if (items != null)
		//    {
		//      foreach (string itemKey in items.Keys)
		//      {
		//        XElement item;
		//        try
		//        {
		//          item = UnpackItem(items[itemKey]).Value;
		//        }
		//        catch (Exception exception)
		//        {
		//          m_logger.ErrorFormat("Failed to load state item {0} for profile {1}. Error: {2}", itemKey, profile_,
		//                               exception.Message);
		//          m_statusBar.MainItem.UpdateStatus(StatusLevel.Critical,
		//                                            string.Format(
		//                                              "Failed to load state item {0} for profile {1}. Error: {2}", itemKey,
		//                                              profile_, exception.Message));
		//          continue;
		//        }

		//        if (item is XElementWithSubitems)
		//        {
		//          var xews = ((XElementWithSubitems) item);
		//          foreach (string key in new List<string>(xews.Subitems.Keys))
		//          {
		//            if (subitems != null && subitems.ContainsKey(key))
		//            {
		//              ReplaceSubitem(xews, key, subitems[key]);
		//            }
		//            else
		//            {
		//              //TODO we've lost one subitem state. what to do?
		//            }
		//            subitems.Remove(key);
		//          }
		//          //item = new XElement(item);
		//        }
		//        unpackeditems.Add(itemKey, item);
		//        //TODO load the rest of subitems
		//      }
		//    }
		//    return DictToXDocument(unpackeditems);
		//  }

		//  #endregion
		//}

		//#endregion

		#region Nested type: TestBenchSplashScreen

		private class TestBenchSplashScreen : ISplashScreen
		{
			public readonly IList<string> texts = new List<string>();

			#region ISplashScreen Members

			public void SetText(string text_)
			{
				texts.Add(text_);
			}

		    public bool IsVisible
		    {
		        get
		        {
		            throw new NotImplementedException();
		        }
		        set
		        {
		            throw new NotImplementedException();
		        }
		    }

		    public void ShowProgressBar(int maxSize)
		    {
		    }

		    public void SetProgress(int value)
		    {
		    }

		    #endregion


            public void Close()
            {
                throw new NotImplementedException();
            }


            public void BringToFront()
            {
                throw new NotImplementedException();
            }
        }

		#endregion

		#region Nested type: TestBenchStatusBar

		private class TestBenchStatusBar : IStatusBar
		{
			#region IStatusBar Members

			public ICollection<string> Keys
			{
				get { throw new NotImplementedException(); }
			}

			public IStatusBarItem this[string statusBarItemKey]
			{
				get { throw new NotImplementedException(); }
			}

			public IStatusBarItem MainItem
			{
				get { throw new NotImplementedException(); }
			}

			public void AddItem(string key, IStatusBarItem item, StatusBarItemAlignment alignment)
			{
				throw new NotImplementedException();
			}

			public string AddItem(IStatusBarItem item, StatusBarItemAlignment alignment)
			{
				throw new NotImplementedException();
			}

			public void AddTextStatusItem(string key, StatusBarItemAlignment alignment)
			{
				throw new NotImplementedException();
			}

			public string AddTextStatusItem(StatusBarItemAlignment alignment)
			{
				throw new NotImplementedException();
			}

			public void AddProgressBarStatusItem(string key, double @from, double to,
												 StatusBarItemAlignment alignment)
			{
				throw new NotImplementedException();
			}

			public string AddProgressBarStatusItem(double @from, double to, StatusBarItemAlignment alignment)
			{
				throw new NotImplementedException();
			}

			public void AddSeparator(string key, StatusBarItemAlignment alignment)
			{
				throw new NotImplementedException();
			}

			public string AddSeparator(StatusBarItemAlignment alignment)
			{
				throw new NotImplementedException();
			}

			public void RemoveItem(string key)
			{
				throw new NotImplementedException();
			}

			public bool ContainsItem(string key)
			{
				throw new NotImplementedException();
			}

			#endregion
		}

		#endregion

		#region Nested type: TestBenchUnityContainerExtension

		private class TestBenchUnityContainerExtension : UnityContainerExtension
		{
			public ExtensionContext extensionContext;

			protected override void Initialize()
			{
				extensionContext = Context;
			}
		}

		#endregion

		#region Nested type: TestBenchUnityServiceLocatorAdapter

		public class TestBenchUnityServiceLocatorAdapter : UnityServiceLocatorAdapter
		{
			public IDictionary<Type, object> CreatedInstances = new Dictionary<Type, object>();

			public TestBenchUnityServiceLocatorAdapter(IUnityContainer unityContainer)
				: base(unityContainer)
			{
			}

			protected override object DoGetInstance(Type serviceType, string key)
			{
				object o = base.DoGetInstance(serviceType, key);
				CreatedInstances.Add(serviceType, o);
				return o;
			}
		}

		#endregion

		#region Nested type: TestBenchViewContainer


		private class TestBenchViewContainer : IViewContainer
		{
			public bool m_isActive;
			public bool m_isVisible;

			public TestBenchViewContainer()
			{
				HeaderItems = new List<object>();
			}

			#region IViewContainer Members

			public string Title { get; set; }

			public Size DefaultSize { get; set; }

			public double Width
			{
				get { throw new NotImplementedException(); }
				set { throw new NotImplementedException(); }
			}

			public double Height
			{
				get { throw new NotImplementedException(); }
				set { throw new NotImplementedException(); }
			}

            public double Top
            {
                get { throw new NotImplementedException(); }
                set { throw new NotImplementedException(); }
            }

            public double Left
            {
                get { throw new NotImplementedException(); }
                set { throw new NotImplementedException(); }
            }

			public object Content { get; set; }

			public IList<object> HeaderItems { get; private set; }

			public bool IsVisible
			{
				get { return m_isVisible; }
			}

			public bool IsActive
			{
				get { return m_isActive; }
			}

			public Icon Icon { get; set; }

			public Brush Background { get; set; }
			public InitialViewSettings InitialViewSettings { get; set; }

			public string ID
			{
				get { throw new NotImplementedException(); }
			}

			public void Close()
			{
				throw new NotImplementedException();
			}

			public void Activate()
			{
				throw new NotImplementedException();
			}

			public event EventHandler<CancelEventArgs> Closing;

			public event EventHandler<ViewEventArgs> Closed;

			#endregion

			#region IViewContainer Members


			public void StartFlash()
			{
				throw new NotImplementedException();
			}

			public void StopFlash()
			{
				throw new NotImplementedException();
			}

			public void Flash()
			{
				throw new NotImplementedException();
			}

			#endregion


			public void Flash(System.Windows.Media.Color color)
			{
				throw new NotImplementedException();
			}
		}

		#endregion

		#region Nested type: TestBenchViewManager

		private class TestBenchViewManager : IViewManager
		{
			public readonly Dictionary<string, ViewCreator> _creators = new Dictionary<string, ViewCreator>();
			public readonly ObservableCollection<IViewContainer> _viewCollection = new ObservableCollection<IViewContainer>();
			public readonly IList<IViewContainer> _viewContainers = new List<IViewContainer>();
			public IViewContainer _activeViewContainer;

			private readonly Dictionary<string, IViewContainer> _singletons = new Dictionary<string, IViewContainer>();


			#region IViewManager Members

			public IDialogWindow CreateDialog()
			{
				throw new NotImplementedException();
			}

			public IViewContainer ActiveViewContainer
			{
				get { return _activeViewContainer; }
			}

			public ObservableCollection<IViewContainer> Views
			{
				get { return _viewCollection; }
			}

			public void AddViewCreator(string id_, InitialiseViewHandler initialiseViewHandler_)
			{
				if (!_creators.ContainsKey(id_))
					_creators.Add(id_, new ViewCreator { InitHandler = initialiseViewHandler_ });
			}

			public void AddViewCreator(string id_, RestoreViewHandler stateToViewHandler_, SaveViewHandler viewtoState_)
			{
				if (!_creators.ContainsKey(id_))
					_creators.Add(id_, new ViewCreator { RestoreHandler = stateToViewHandler_, SaveHadler = viewtoState_ });
			}

			public IViewContainer CreateView(string creatorId_)
			{
				return CreateView(creatorId_, null);
			}

			public IViewContainer CreateView(string creatorId_, XDocument viewState_)
			{
				ViewCreator creator = _creators[creatorId_];
				var viewContainer = new TestBenchViewContainer();
				if (creator.InitHandler != null)
				{
					creator.InitHandler(viewContainer);
				}
				else if (creator.RestoreHandler != null)
				{
					creator.RestoreHandler(viewState_, viewContainer);
				}
				else
				{
					throw new InvalidOperationException("No valid Creator has been registered.");
				}
				_viewContainers.Add(viewContainer);
				_viewCollection.Add(viewContainer);
				if (ViewAdded != null)
					ViewAdded(this, new ViewEventArgs(viewContainer));

				return viewContainer;
			}

			public IViewContainer CreateSingletonView(string creatorId_)
			{
				return CreateSingletonView(creatorId_, null);
			}

			public IViewContainer CreateSingletonView(string creatorId_, XDocument viewState_)
			{
				IViewContainer view = null;
				if (_singletons.ContainsKey(creatorId_))
				{
					view = _singletons[creatorId_];
					if (view != null)
					{
						view.Activate();
					}
				}

				if (view == null)
				{
					view = CreateView(creatorId_, viewState_);
					view.Closed += OnSingleWindowClosed;
					_singletons[creatorId_] = view;
				}
				return view;
			}

			private void OnSingleWindowClosed(object sender_, ViewEventArgs e_)
			{
				var view = e_.ViewContainer;
				bool internalError = true;
				if (view != null)  // view == NULL shall never happen
				{
					if (_singletons.ContainsValue(view))
					{

						var key = _singletons.Keys.First(k_ => _singletons[k_] == view);
						view.Closed -= OnSingleWindowClosed;
						_singletons.Remove(key);
						internalError = false;
					}
				}

			}

			public IViewContainer CreateView(string creatorId_, InitialLocation initialLocation_)
			{
				//TODO actually implement initialLocation support
				return CreateView(creatorId_, null);
			}

			public IViewContainer CreateView(string creatorId_, XDocument viewState_, InitialLocation initialLocation_)
			{
				//TODO actually implement initialLocation support
				return CreateView(creatorId_, viewState_);
			}

			public void SaveViewContainer(IViewContainer container_)
			{
				//_persistenceProfileService.SaveSubitem(
				//  ViewManager.PersistorId, container_.ID, BuildIViewXml((ViewContainer) container_), AddNewView, m_cleanLayout);
			}

			public event EventHandler<ViewEventArgs> Activated;

			public event EventHandler<ViewEventArgs> Deactivated;

			public event EventHandler<ViewEventArgs> ViewAdded;

			#endregion


			#region IViewManager Members

			public void RegisterFactory(string ID,
				InitializeWindowHandler initWindowHandler = null, DehydrateWindowHandler dehydrateWindowHandler = null,
				InitializeWidgetHandler initWidgetHandler = null, DehydrateWidgetHandler dehydrateWidgetHandler = null)
			{
				throw new NotImplementedException();
			}


			public void RegisterWindowFactory(string ID, InitializeWindowHandler initHandler, DehydrateWindowHandler dehydrateHandler = null)
			{
				throw new NotImplementedException();
			}

			public void RegisterWidgetFactory(string ID, InitializeWidgetHandler initHandler, DehydrateWidgetHandler dehydrateHandler = null)
			{
				throw new NotImplementedException();
			}

			public IWindowViewContainer CreateWindow(string factoryID, InitialWindowParameters parameters = null)
			{
				throw new NotImplementedException();
			}

			public IWidgetViewContainer CreateWidget(string factoryID, InitialWidgetParameters parameters = null)
			{
				throw new NotImplementedException();
			}

			#endregion
		}

		#endregion

		#region Nested type: ViewCreator

		public struct ViewCreator
		{
			public InitialiseViewHandler InitHandler;
			public RestoreViewHandler RestoreHandler;
			public SaveViewHandler SaveHadler;
		}

		#endregion
	}
}
#pragma warning restore 612,618,67