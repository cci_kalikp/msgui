﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/IApplicationOptions.cs#10 $
// $Change: 864360 $
// $DateTime: 2014/01/29 00:36:55 $
// $Author: caijin $

namespace MorganStanley.MSDotNet.MSGui.Core
{
    /// <summary>
    /// Add a view into the options pane.
    /// </summary> 
    public interface IApplicationOptions
    {
        /// <summary>
        /// Add an options view. You should only be using this method if your use case is not handled
        /// by AddViewModelBasedOptionPage.
        /// </summary>
        /// <param name="path">Path to add view to.</param>
        /// <param name="title">Title of view</param>
        /// <param name="view">View to add.</param>
        void AddOptionPage(string path, string title, IOptionView view); 

        /// <summary>
        /// Add an options page based on TViewModel. Please provide the relevant DataTemplate
        /// in the application resources as well.
        /// </summary>
        /// <typeparam name="TViewModel">
        /// The TViewModel to use. The type can but does not have to be registered in the container.
        /// </typeparam>
        /// <param name="path">Options path to add the view to.</param>
        /// <param name="title">The title of the view.</param>
        /// <param name="global">
        /// Indicates whether the settings should be per-layout or global to all layouts.
        /// </param>
        void AddViewModelBasedOptionPage<TViewModel>(string path, string title, bool global)
            where TViewModel : IOptionViewViewModel;
 

        void RemoveOptionPage(string path, string title);
        
    }
}
