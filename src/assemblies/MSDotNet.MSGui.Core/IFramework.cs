﻿using System;
using Microsoft.Practices.Composite.Modularity;

namespace MorganStanley.MSDotNet.MSGui.Core
{
    public interface IFramework
    {
        void Start();

        void AddPreInitializer<T>() where T : IPreInitializer, new();
        void AddPreInitializer<T>(T preInitializer) where T : IPreInitializer;

        void AddModule<T>();
        void AddModule(Type moduleType, string moduleName);
        void AddModule(Type moduleType);
        //void AddModule(ModuleInfo moduleInfo); //TODO add this back
        //void AddModule(Microsoft.Practices.Prism.Modularity.ModuleInfo moduleInfo);
    }
}
