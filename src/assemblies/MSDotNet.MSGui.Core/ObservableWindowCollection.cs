﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Collections;

namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager
{
	public sealed class ObservableWindowCollection : ICollection<IWindowViewContainer>, INotifyCollectionChanged
	{
		internal ObservableCollection<IWindowViewContainer> m_windows;

		internal ObservableWindowCollection(ObservableCollection<IWindowViewContainer> source_)
		{
			this.m_windows = source_;
		}

		#region ICollection<IWindowViewContainer> Members

		public void Add(IWindowViewContainer item)
		{
			throw new InvalidOperationException();
		}

		public void Clear()
		{
			throw new InvalidOperationException();
		}

		public bool Contains(IWindowViewContainer item)
		{
			return this.m_windows.Contains(item);
		}

		public void CopyTo(IWindowViewContainer[] array, int arrayIndex)
		{
			this.m_windows.CopyTo(array, arrayIndex);
		}

		public int Count
		{
			get 
			{
				return this.m_windows.Count;
			}
		}

		public bool IsReadOnly
		{
			get 
			{
				return true;
			}
		}

		public bool Remove(IWindowViewContainer item)
		{
			throw new InvalidOperationException();
		}

		#endregion

		#region IEnumerable<IWindowViewContainer> Members

		public IEnumerator<IWindowViewContainer> GetEnumerator()
		{
			return this.m_windows.GetEnumerator();
		}

		#endregion

		#region IEnumerable Members

		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.m_windows.GetEnumerator();
		}

		#endregion

		#region INotifyCollectionChanged Members

		public event NotifyCollectionChangedEventHandler CollectionChanged
		{
			add
			{
				this.m_windows.CollectionChanged += value;
			}
			remove
			{
				this.m_windows.CollectionChanged -= value;
			}
		}

		#endregion
	}
}
