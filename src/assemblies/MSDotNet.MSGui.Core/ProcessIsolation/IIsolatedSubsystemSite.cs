﻿using System;
using System.ServiceModel;

namespace MorganStanley.MSDotNet.MSGui.Core.ProcessIsolation
{
    /// <summary>
    /// Site for subsystem isolation. Not intended to be used or implemented by your code.
    /// </summary>
    [ServiceContract(Name = "IsolatedSubsystemSite", Namespace = "net-pipe://IsolatedSubsystemSettings")]
    public interface IIsolatedSubsystemSite
    {
        [OperationContract]
        string GetPayloadType();

        [OperationContract]
        IsolatedSubsystemSettings GetIsolatedSubsystemSettings();

        [OperationContract]
        string AddWidget(string addinUri, string widgetFactoryId, IntPtr hwnd, string parentFactoryId, string widgetXaml);

        [OperationContract]
        string PlaceWidget(string addInUri, string widgetFactoryId, IntPtr hwnd, string root, string location, string widgetXaml);

        [OperationContract]
        string AddWindow(string addInUri, IntPtr hwnd, InitialWindowParametersProxy initialParametersProxy, string windowTitle);

        [OperationContract]
        void SetModuleStatus(string moduleTypeName, ModuleStatus status, Exception exception = null);

        [OperationContract]
        void ViewLoadedCompletely(string connectionInfo);

        [OperationContract]
        void SubscribeM2M(string messageType, string communicationAddInUrl);

        [OperationContract]
        void PublishM2M(byte[] objectBytestream);

        bool IsControl
        {
            [OperationContract]
            get;
        }
    }
}
