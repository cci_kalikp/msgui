﻿using System.ServiceModel;

namespace MorganStanley.MSDotNet.MSGui.Core.ProcessIsolation
{
    [ServiceContract(Name = "XProcUiAddIn")]
    public interface IXProcCommunicationAddIn
    {
        [OperationContract]
        void Receive(byte[] payloadBytes);
    }
}
