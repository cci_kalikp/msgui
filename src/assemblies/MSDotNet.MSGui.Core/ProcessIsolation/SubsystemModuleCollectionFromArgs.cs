﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Core.ProcessIsolation
{
    public class SubsystemModuleCollectionFromArgs : ISubsystemModuleCollection
    {
        public const string ModuleArgs = "/modules:";
        public IEnumerable<Type> GetModules()
        {
            string args =
                Environment.GetCommandLineArgs()
                           .FirstOrDefault(
                               arg_ => arg_.StartsWith(ModuleArgs, StringComparison.CurrentCultureIgnoreCase));
            if (args != null)
            {
                string[] modules = args.Substring(ModuleArgs.Length).Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                List<Type> moduleTypes = new List<Type>();
                foreach (var module in modules)
                {
                    Type moduleType = Type.GetType(module.Trim());
                    moduleTypes.Add(moduleType);
                }
                return moduleTypes.ToArray();
            }
            return new Type[0];
        }
    }
}
