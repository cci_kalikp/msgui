﻿using System.ServiceModel;
using System.Windows.Input;

namespace MorganStanley.MSDotNet.MSGui.Core.ProcessIsolation
{
    [ServiceContract(Name = "XProcAddIn")]
	public interface IXProcAddIn
	{
        [OperationContract]
        bool TabInto(TraversalRequest request);

        [OperationContract]
        void ShutDown();

        [OperationContract]
        void Load(string doc);

        [OperationContract]
        void Paint(byte[] background);

        [OperationContract]
        string Save();

        [OperationContract]
        void Receive(byte[] payload);
	}
}
