﻿using System.ServiceModel;
using System.Windows.Input;
using System.Windows.Interop;

namespace MorganStanley.MSDotNet.MSGui.Core.ProcessIsolation
{
    /// <summary>
    /// Site for subsystem isolation. Not intended to be used by your code.
    /// </summary>
    [ServiceContract(Name = "XProcWindowSite")]
    public interface IXProcWindowSite
    {
        [OperationContract]
        bool TabOut(TraversalRequest request);

        [OperationContract]
        bool TranslateAccelerator(MSG msg);

        [OperationContract]
        void Send(byte[] payload);
    }
}
