﻿using System;
using System.Runtime.Serialization;

namespace MorganStanley.MSDotNet.MSGui.Core.ProcessIsolation
{
    /// <summary>
    /// This class is used with serialization of isolated subsystem metadata and is not 
    /// intended to be used from your code.
    /// </summary>
    [Serializable, DataContract(Name = "IsolatedSubsystemSettings", Namespace = "net-pipe://IsolatedSubsystemSettings")]
    public sealed class IsolatedSubsystemSettings
    {
        [DataMember]
        public string AdditionalCommandLine { get; set; }

        [DataMember]
        public string IsolatedSubsystemLocation { get; set; }

        [DataMember]
        public string IsolatedSubsystemConfigFile { get; set; }

        [DataMember]
        public string IsolatedType { get; set; }

        [DataMember]
        public bool IsCpsRequired { get; set; }

        [DataMember]
        public bool UsePrism { get; set; }

        [DataMember]
        public bool IsAdditionalArRequired { get; set; }

        [DataMember]
        public string SubsystemRibbonPath { get; set; }

        [DataMember]
        public string ProfileName { get; set; }

        [DataMember]
        public string Layout { get; set; }

        [DataMember]
        public string CpsConfig { get; set; }

        [DataMember]
        public int HostProcessId { get; set; }

        [DataMember]
        public string ApplicationName { get; set; }

        [DataMember]
        public string[] AssemblyResolutionFiles { get; set; }
    }
}

