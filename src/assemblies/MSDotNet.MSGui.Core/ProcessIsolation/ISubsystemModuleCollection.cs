﻿using System;
using System.Collections.Generic;

namespace MorganStanley.MSDotNet.MSGui.Core.ProcessIsolation
{
    /// <summary>
    /// Represents a module collection that will be run in an isolated process.
    /// </summary>
    public interface ISubsystemModuleCollection
    {
        /// <summary>
        /// Retrieves the collection of types of modules to instantiate in an isolated subsystem.
        /// </summary>
        /// <returns>Collection of types of the modules implementing IModule interfaces to be used with isolated subsystem.</returns>
        IEnumerable<Type> GetModules();
    }
}
