﻿using System;
using System.Runtime.Serialization;
using System.Windows;

namespace MorganStanley.MSDotNet.MSGui.Core.ProcessIsolation
{
    [Serializable, DataContract(Name = "InitialWindowParameters")]
    public sealed class InitialWindowParametersProxy
    {
        private InitialLocation _initialLocation = InitialLocation.Floating;
	    private bool _isModal;
        private string _singletonKey = string.Empty;
        private bool _showFlashBorder = true;
        private bool _transient;
        private bool _useDockManager = true;
        private bool _enforceSizeRestrictions;
        private SizingMethod _sizingMethod;
        private ResizeMode _resizeMode;
        private string _factoryId;
        private string _viewId;
        private bool _topmost;

        [DataMember]
        public InitialLocation InitialLocation
        {
            get { return _initialLocation; }
            set { _initialLocation = value; }
        }

        [DataMember]
        public bool IsModal
        {
            get { return _isModal; }
            set { _isModal = value; }
        }

        [DataMember]
        public string SingletonKey
        {
            get { return _singletonKey; }
            set { _singletonKey = value; }
        }

        [DataMember]
        public bool ShowFlashBorder
        {
            get { return _showFlashBorder; }
            set { _showFlashBorder = value; }
        }

        [DataMember]
        public bool Transient
        {
            get { return _transient; }
            set { _transient = value; }
        }

        [DataMember]
        public bool UseDockManager
        {
            get { return _useDockManager; }
            set { _useDockManager = value; }
        }

        [DataMember]
        public bool EnforceSizeRestrictions
        {
            get { return _enforceSizeRestrictions; }
            set { _enforceSizeRestrictions = value; }
        }

        [DataMember]
        public string FactoryId
        {
            get { return _factoryId; }
            set { _factoryId = value; }
        }

        [DataMember]
        public SizingMethod SizingMethod
        {
            get { return _sizingMethod; }
            set { _sizingMethod = value; }
        }

        [DataMember]
        public bool Topmost
        {
            get { return _topmost; }
            set { _topmost = value; }
        }

        [DataMember]
        public int ResizeModeInt
        {
            get { return (int) _resizeMode; }
            set { _resizeMode = (ResizeMode)Enum.ToObject(typeof(ResizeMode), value); }
        }

        public ResizeMode ResizeMode
        {
            get { return _resizeMode; }
            set { _resizeMode = value; }
        }

        [DataMember]
        public string ViewId
        {
            get { return _viewId; }
            set { _viewId = value; }
        }
    }
}
