﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/ISplashScreen.cs#6 $
// $Change: 882731 $
// $DateTime: 2014/05/29 02:54:26 $
// $Author: caijin $

namespace MorganStanley.MSDotNet.MSGui.Core
{
    public interface ISplashScreen
    {
        void SetText(string text_);

        bool IsVisible { get; set; }

        void Close();

        void BringToFront();
    }
}
