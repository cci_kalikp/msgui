﻿using Microsoft.Practices.Unity;

namespace MorganStanley.MSDotNet.MSGui.Core
{
    /// <summary>
    /// Marks a pre-initializer module. Implementing this interface allows you to perform additional set-up or configuration tasks
    /// before any of your modules get instantiated and initialized.
    /// </summary>
    public interface IPreInitializer
    {
        /// <summary>
        /// Performs actual pre-initialization tasks.
        /// </summary>
        void PreInitialize(IUnityContainer container);
    }
}
