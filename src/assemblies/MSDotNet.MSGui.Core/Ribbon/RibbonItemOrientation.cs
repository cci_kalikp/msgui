﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/Ribbon/RibbonItemOrientation.cs#6 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;

namespace MorganStanley.MSDotNet.MSGui.Core.Ribbon
{
  /// <summary>
  /// Orientation of items in <see cref="WrapperPannelRibbonItem"/> 
  /// </summary>
  [Obsolete("Use the ControlParadigm approach instead of Ribbon approach")]
  public enum RibbonItemOrientation 
  {
    /// <summary>
    /// Elements left to right.
    /// </summary>
    Horizontal,

    /// <summary>
    /// Elements top to bottom.
    /// </summary>
    Vertical
  }
}
