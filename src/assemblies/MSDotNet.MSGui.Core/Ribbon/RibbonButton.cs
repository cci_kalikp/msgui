﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/Ribbon/RibbonButton.cs#8 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.ComponentModel;
using System.Drawing;

namespace MorganStanley.MSDotNet.MSGui.Core.Ribbon
{
  /// <summary>
  /// Represents a button in the ribbon shell.
  /// </summary>
  /// <remarks>
  /// Currently this is an immutable object.
  /// </remarks>
  [Obsolete("Use the ControlParadigm approach instead of Ribbon approach")]
  public class RibbonButton : RibbonItem, INotifyPropertyChanged
  {
    private readonly Icon m_icon;
    private readonly RibbonButtonSize m_size;
    private readonly string m_text;
    private readonly object m_toolTip;
    private readonly EventHandler m_clicked;
    private bool m_isEnabled = true;    

    /// <summary>
    /// Initializes a new instance of the <see cref="RibbonButton"/> class
    /// </summary>
    /// <param name="text_">Button Text.</param>
    /// <param name="onClick_">Event handler</param>
    public RibbonButton(string text_, EventHandler onClick_)
      : this(text_, text_, onClick_)
    {
    }


    /// <summary>
    /// Initializes a new instance of the <see cref="RibbonButton"/> class
    /// </summary>
    /// <param name="text_">Button Text.</param>
    /// <param name="toolTip_">Tooltip.</param>
    /// <param name="onClick_">Event handler</param>
    public RibbonButton(string text_, object toolTip_, EventHandler onClick_)
      : this(null, text_, RibbonButtonSize.Small, toolTip_, onClick_)
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="RibbonButton"/> class
    /// </summary>
    /// <param name="icon_">Icon.</param>
    /// <param name="text_">Button Text.</param>
    /// <param name="toolTip_">Tooltip.</param>
    /// <param name="onClick_">Event handler</param>
    public RibbonButton(Icon icon_, string text_, object toolTip_, EventHandler onClick_)
      : this(icon_, text_, RibbonButtonSize.Large, toolTip_, onClick_, true)
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="RibbonButton"/> class
    /// </summary>
    /// <param name="icon_">Icon.</param>
    /// <param name="text_">Button Text.</param>
    /// <param name="size_">Button size.</param>
    /// <param name="toolTip_">Tooltip.</param>
    /// <param name="onClick_">Event handler</param>
    public RibbonButton(Icon icon_, string text_, RibbonButtonSize size_, object toolTip_, EventHandler onClick_)
      : this(icon_, text_, size_, toolTip_, onClick_, true)
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="RibbonButton"/> class
    /// </summary>
    /// <param name="icon_">Icon.</param>
    /// <param name="text_">Button Text.</param>
    /// <param name="size_">Button size.</param>
    /// <param name="toolTip_">Tooltip.</param>
    /// <param name="onClick_">Event handler</param>
    /// <param name="isEnabled_">Is the button enabled.</param>
    public RibbonButton(Icon icon_, string text_, RibbonButtonSize size_, object toolTip_, EventHandler onClick_, bool isEnabled_)
    {
      m_icon = icon_;
      m_text = text_;
      m_toolTip = toolTip_;
      IsEnabled = isEnabled_; 
      
      if (onClick_ != null)
      {
        m_clicked = (o, e) => onClick_(o, e);
      }
      m_size = size_;
    }

    /// <summary>
    /// Gets a value for the button <see cref="Icon"/>.
    /// </summary>
    public Icon Icon
    {
      get
      {
        return m_icon;
      }
    }

    /// <summary>
    /// Gets a value for the button text.
    /// </summary>
    public string Text
    {
      get
      {
        return m_text;
      }
    }

    /// <summary>
    /// Gets a object representing the buttons tooltip.
    /// </summary>
    public object ToolTip
    {
      get
      {
        return m_toolTip;
      }
    }

    /// <summary>
    /// Event raised when button is clicked.
    /// </summary>
    /// <remarks>
    /// iansau: Do we really need this as a "Get" 
    /// property. I don't see the benefit.
    /// </remarks>
    public EventHandler Clicked
    {
      get
      {
        return m_clicked;
      }
    }

    /// <summary>
    /// Gets or sets a value indicating 
    /// if the button is enabled.
    /// </summary>    
    public bool IsEnabled
    {
      get
      {
        return m_isEnabled;
      }
      set
      {
        m_isEnabled = value;
        InvokePropertyChanged("IsEnabled");
      }
    }

    /// <summary>
    /// Gets a value representing the 
    /// <see cref="RibbonButtonSize"/> of the button.
    /// </summary>
    public RibbonButtonSize Size
    {
      get
      {
        return m_size;
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;
    private void InvokePropertyChanged(string name_)
    {
      PropertyChangedEventHandler copy = PropertyChanged;
      if (copy != null)
      {
        copy(this, new PropertyChangedEventArgs(name_));
      }
    }
  }
}
