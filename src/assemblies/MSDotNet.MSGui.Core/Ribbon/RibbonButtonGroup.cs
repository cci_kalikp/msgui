﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/Ribbon/RibbonButtonGroup.cs#6 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Core.Ribbon
{
  /// <summary>
  /// Represents a grouping of <see cref="RibbonButton"/> in the ribbon shell.
  /// </summary>
  /// <remarks>
  /// Currently this is an immutable object.
  /// </remarks>
  [Obsolete("Use the ControlParadigm approach instead of Ribbon approach")]
  public class RibbonButtonGroup : RibbonItem
  {
    private IEnumerable<RibbonButton> m_items;

    /// <summary>
    /// Gets an enumeration of <see cref="RibbonButton"/>.
    /// </summary>
    public IEnumerable<RibbonButton> Buttons
    {
      get
      {
        return m_items;
      }
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="RibbonButtonGroup"/> class
    /// </summary>
    /// <param name="items">Buttons in group.</param>
    public RibbonButtonGroup(params RibbonButton[] items)
    {
      m_items = items;
    }
  }
}
