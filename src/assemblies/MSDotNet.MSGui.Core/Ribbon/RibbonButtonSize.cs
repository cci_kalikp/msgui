﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/Ribbon/RibbonButtonSize.cs#6 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Core.Ribbon
{
  /// <summary>
  /// Size for <see cref="RibbonButton"/>.
  /// </summary>
  [Obsolete("Use the ControlParadigm approach instead of Ribbon approach")]
  public enum RibbonButtonSize
  {
    /// <summary>
    /// Small button.
    /// </summary>
    Small,

    /// <summary>
    /// Large button.
    /// </summary>
    Large
  }
}
