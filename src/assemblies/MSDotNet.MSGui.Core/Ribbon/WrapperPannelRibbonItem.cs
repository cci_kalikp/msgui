﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/Ribbon/WrapperPannelRibbonItem.cs#6 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.Collections.Generic;

namespace MorganStanley.MSDotNet.MSGui.Core.Ribbon
{
  /// <summary>
  /// Represents a wrapper panel of elements in the
  /// ribbon.
  /// </summary>
  [Obsolete("Use the ControlParadigm approach instead of Ribbon approach")]
  public class WrapperPannelRibbonItem : RibbonItem
  {
    private RibbonItemOrientation m_orientation;
    private IEnumerable<RibbonItem> m_items;
    private int m_minRows = 2;
    private int m_maxRows = 3;

    /// <summary>
    /// Gets a value indicating the orientation of the 
    /// ribbon items it contains.
    /// </summary>
    public RibbonItemOrientation Orientation
    {
      get
      {
        return m_orientation;
      }
    }

    /// <summary>
    /// Gets the items contained by this panel.
    /// </summary>
    public IEnumerable<RibbonItem> Items
    {
      get
      {
        return m_items;
      }
    }

    /// <summary>
    /// Minimum rows in this panel.
    /// </summary>
    public int MinRows
    {
      get
      {
        return m_minRows;
      }
    }

    /// <summary>
    /// Maximum rows in this panel.
    /// </summary>
    public int MaxRows
    {
      get
      {
        return m_maxRows;
      }
    }
    
    /// <summary>
    /// Initializes a new instance of the <see cref="WrapperPannelRibbonItem"/> class   
    /// </summary>
    /// <param name="orientation_">Orientation of items in panel.</param>
    /// <param name="items">Items to add to control.</param>
    public WrapperPannelRibbonItem(RibbonItemOrientation orientation_, params RibbonItem[] items)
      : this(orientation_, 2, 3, items)
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="WrapperPannelRibbonItem"/> class   
    /// </summary>
    /// <param name="orientation_">Orientation of items in panel.</param>
    /// <param name="minRows_">Mimimum rows (default 2)</param>
    /// <param name="maxRows_">Maximum rows (default 3)</param>
    /// <param name="items">Items to add to control.</param>
    public WrapperPannelRibbonItem(RibbonItemOrientation orientation_, int minRows_, int maxRows_, params RibbonItem[] items)
    {
      m_orientation = orientation_;
      m_items = items;
      m_minRows = minRows_;
      m_maxRows = maxRows_;
    }
  }
}
