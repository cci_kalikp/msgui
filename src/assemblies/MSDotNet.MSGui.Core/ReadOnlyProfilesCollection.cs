﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/ReadOnlyProfilesCollection.cs#4 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;

namespace MorganStanley.MSDotNet.MSGui.Core
{
  
  [Serializable]
  public class ReadOnlyProfilesCollection : ReadOnlyCollection<string>, INotifyCollectionChanged, INotifyPropertyChanged
  {
    [field: NonSerialized]
    public event NotifyCollectionChangedEventHandler CollectionChanged;

    [field: NonSerialized]
    public event PropertyChangedEventHandler PropertyChanged;

    public ReadOnlyProfilesCollection(ProfilesCollection list)
      : base(list)
    {
      ((INotifyCollectionChanged)base.Items).CollectionChanged += this.HandleCollectionChanged;
      ((INotifyPropertyChanged)base.Items).PropertyChanged += this.HandlePropertyChanged;
    }

    private void HandleCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
    {
      this.OnCollectionChanged(e);
    }

    private void HandlePropertyChanged(object sender, PropertyChangedEventArgs e)
    {
      this.OnPropertyChanged(e);
    }

    protected virtual void OnCollectionChanged(NotifyCollectionChangedEventArgs args)
    {
      if (this.CollectionChanged != null)
      {
        this.CollectionChanged(this, args);
      }
    }

    protected virtual void OnPropertyChanged(PropertyChangedEventArgs args)
    {
      if (this.PropertyChanged != null)
      {
        this.PropertyChanged(this, args);
      }
    }
  }
}




