﻿using System;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Core
{
    public class WindowDockStateChangedEventArgs:EventArgs
    {
        private readonly IWindowViewContainerHostRoot oldRoot;
        private readonly IWindowViewContainerHostRoot newRoot;
        private readonly DockState state;
        public WindowDockStateChangedEventArgs(IWindowViewContainerHostRoot oldRoot_, IWindowViewContainerHostRoot newRoot_, DockState state_)
        {
            state = state_;
            oldRoot = oldRoot_;
            newRoot = newRoot_;
        }

        public IWindowViewContainerHostRoot OldRoot
        {
            get { return oldRoot; }
        }

        public IWindowViewContainerHostRoot NewRoot
        {
            get { return newRoot; }
        }


        public DockState State
        {
            get { return state; }
        }
    }

    public enum DockState
    {
        Docked,
        Undocked
    }
}