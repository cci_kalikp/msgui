﻿using System;
using System.Collections.Generic;
using System.Windows.Media;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Core.ScreenGroup
{
    public interface  IScreenGroupManager
    {
        IEnumerable<IScreenGroup> ScreenGroups { get; }

        IEnumerable<IScreenGroup> GetScreenGroupsByName(string name);

        IScreenGroup CreateGroup(string name, Brush color = null);

        void RemoveGroup(IScreenGroup group);

        IScreenGroup GetScreenGroup(IWindowViewContainer view);

        Brush GetScreenGroupBrush(IScreenGroup screenGroup);


        /// <summary>
        /// When work with V2V Auto-Hook, this will enable the subcribers registered with the view 
        /// will automatically receive the messages from its publisher when add in a group as both Publisher and Subscriber
        /// 
        /// By default, self auto hook is disabled
        /// </summary>
        /// <param name="windowViewContainer"></param>
        void EnableSelfAutoHook(IWindowViewContainer windowViewContainer);

        /// <summary>
        /// When work with V2V Auto-Hook, this will enable the subcribers registered with the view 
        /// will NOT automatically receive the messages from its publisher when add in a group as both Publisher and Subscriber
        /// 
        /// By default, self auto hook is disabled
        /// </summary>
        /// <param name="windowViewContainer"></param>
        void DisableSelfAutoHook(IWindowViewContainer windowViewContainer);

        /// <summary>
        /// To check if a view will auto-hook to itself
        /// </summary>
        /// <param name="windowViewContainer"></param>
        /// <returns></returns>
        bool IsViewSelfAutoHook(IWindowViewContainer windowViewContainer);

        event EventHandler<GroupChangedEventArgs> GroupMemberChanged;
        event EventHandler<GroupCollectionChagnedArgs> GroupCollectionChanged;
        
    }

    public class GroupCollectionChagnedArgs : EventArgs
    {
        public IScreenGroup AddedGroup { get; set; }
        public IScreenGroup RemovedGroup { get; set; }
    }

    public class GroupChangedEventArgs : EventArgs
    {
        public IWindowViewContainer Window { get; set; }
        public IScreenGroup OldGroup { get; set; }
        public IScreenGroup NewGroup { get; set; }
        public V2VRole OldRole { get; set; }
        public V2VRole NewRole { get; set; }
    }
}