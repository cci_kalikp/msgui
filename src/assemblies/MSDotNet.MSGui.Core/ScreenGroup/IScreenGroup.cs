﻿using System;
using System.Collections.Generic;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Core.ScreenGroup
{
    public interface IScreenGroup
    {
        void AddWindow(IWindowViewContainer windowViewContainer);

        /// <summary>
        /// role == None takes no effect, i.e. it won't be added to the group 
        /// </summary>
        /// <param name="windowViewContainer"></param>
        /// <param name="role"></param>
        void AddWindow(IWindowViewContainer windowViewContainer, V2VRole role);

       
        void RemoveWindow(IWindowViewContainer windowViewContainer);

        /// <summary>
        /// role == None takes no effect, i.e. won't be removed from the group 
        /// </summary>
        /// <param name="windowViewContainer"></param>
        /// <param name="role"></param>
        void RemoveWindow(IWindowViewContainer windowViewContainer, V2VRole role);

        IEnumerable<IWindowViewContainer> Windows { get; }
        IEnumerable<IWindowViewContainer> PublishingWindows { get; }
        IEnumerable<IWindowViewContainer> SubscribingWindows { get; }

        String Name { get; set; }

    }




    /// <summary>
    /// used when a window join a screen group.  The value indicate how will the window connect to others in V2V auto-hook Screegroup mode. 
    /// </summary>
    [Flags]
    public enum V2VRole
    {
        None = 0x0,
        /// <summary>
        /// window can receive messages
        /// </summary>
        AsSubscriber = 0x01, 
        /// <summary>
        /// window can publish messages
        /// </summary>
        AsPublisher = 0x10, 

        /// <summary>
        /// window can receive and publish messages
        /// </summary>
        Both = 0x11
    }

}
