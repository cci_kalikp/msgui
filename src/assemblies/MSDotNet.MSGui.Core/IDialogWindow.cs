﻿////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/IDialogWindow.cs#8 $
// $Change: 826740 $
// $DateTime: 2013/05/01 09:13:54 $
// $Author: milosp $

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Media;

namespace MorganStanley.MSDotNet.MSGui.Core
{
    [Obsolete("Use IChromeManager.CreateWindow with relevant parameters instead")]
	public interface IDialogWindow
	{
	    Size WindowSize { get; }

		bool? ShowDialog();

		void Show();
		void Hide();

		bool? DialogResult
		{
			get;
			set;
		}

		bool Activate();

		void Close();

		object Content
		{
			get;
			set;
		}

		string Title
		{
			get;
			set;
		}

		ImageSource Icon
		{
			get;
			set;
		}

		double Width { get; set; }
		double MinWidth { get; set; }
		double MaxWidth { get; set; }
		double Height { get; set; }
		double MinHeight { get; set; }
		double MaxHeight { get; set; }
		double Left { get; set; }
		double Top { get; set; }
		ResizeMode ResizeMode { get; set; }
		SizeToContent SizeToContent { get; set; }

		event CancelEventHandler Closing;
		event EventHandler Closed;
		WindowStartupLocation WindowStartupLocation { get; set; }
		Window Owner { get; set; }
		bool Topmost { get; set; }

		/// <summary>
		/// Gets or sets the collection thet contains the elements in the header of the window.
		/// </summary>
		IList<object> HeaderItems { get; }

		bool MinimizeVisible { get; set; }
		bool MaximizeVisible { get; set; }
		bool CloseVisible { get; set; }
	}
}
