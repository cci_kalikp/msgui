﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Core
{
    public class EventArgs<TValue1> : EventArgs
    {
        public TValue1 Value1 { get; set; }
        public EventArgs(TValue1 value1)
        {
            Value1 = value1;
        }
    }
}
