﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows;

namespace MorganStanley.MSDotNet.MSGui.Core
{
    public static class PathUtilities
    {
        private const string EnvVariablePattern = @"%(.+?)%";
        private const string DevExamplePathPatten = @"msgui\\trunk\\examples\\.+?\\bin\\.+?";
        private const string DevAssemblyPathPatten = @"msgui\\trunk\\.+?\\.+?\\bin\\.+?";
        public static string GetAbsolutePath(string path_)
        {
            return GetAbsolutePath(path_, AppDomain.CurrentDomain.BaseDirectory);
        }

        public static bool PathEquals(string path1_, string path2_, bool parseEnvironmentVariable_ = false)
        {
            return string.Equals(
                TrimPath(path1_, parseEnvironmentVariable_),
                TrimPath(path2_, parseEnvironmentVariable_),
                StringComparison.OrdinalIgnoreCase);
        }

        public static string TrimPath(string path_, bool parseEnvironmentVariable_=false)
        {
            if (string.IsNullOrEmpty(path_)) return path_;
            if (parseEnvironmentVariable_)
            {
                path_ = ReplaceEnvironmentVariables(path_);
            }
            path_ = Path.GetFullPath(path_).Trim();
            if (path_[path_.Length - 1] == '\\')
            {
                path_ = path_.Substring(0, path_.Length - 1);
            }

            return path_;
        }
        public static string ReplaceEnvironmentVariables(string text_)
        {
            if (string.IsNullOrEmpty(text_)) return text_;
           return Regex.Replace(text_, EnvVariablePattern, match_ =>
            {
                string envName = match_.Groups[1].ToString();
                return System.Environment.GetEnvironmentVariable(envName);
            }, RegexOptions.Compiled); 
        }

        public static string GetAbsolutePath(string path_, string currentDirectory_)
        {
            if (string.IsNullOrEmpty(path_)) return path_;
            path_ = ReplaceEnvironmentVariables(path_);

            if (Path.IsPathRooted(path_)) return path_;
            path_ = path_.Replace('/', '\\');
            if (path_.StartsWith(".\\"))
            {
                path_ = path_.Substring(2);
            }
            return Path.Combine(currentDirectory_, path_);
        }


        public static string ConvertToResourcePath(string path_)
        {
            if (string.IsNullOrEmpty(path_)) return path_;
            if (Path.IsPathRooted(path_)) return path_;
            path_ = path_.Replace('\\', '/');
            if (path_.StartsWith("."))
            {
                path_ = path_.Substring(1);
            }
            if (!path_.StartsWith("/"))
            {
                path_ = "/" + path_;
            }
            return @"pack://application:,,," + path_;
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public static string GetExampleRootPath()
        {
            if (Environment.UserName == "pjenkbld") //running from build server
            {
                string workspace = Environment.GetEnvironmentVariable("WORKSPACE");
                 
                if (!string.IsNullOrEmpty(Environment.GetEnvironmentVariable("TRAIN_RELEASE_NAME")))
                {
                    return Path.Combine(workspace, @"msdotnet\msgui\trunk\install\examples_release"); 
                }
                return Path.Combine(workspace, @"msdotnet\msgui\trunk\install\common\examples");
            }
            string exampleRootPath = Environment.GetEnvironmentVariable("ExampleRootPath");
            if (!string.IsNullOrEmpty(exampleRootPath))
            {
                return exampleRootPath;
            }
            string currentFolder = AppDomain.CurrentDomain.BaseDirectory;
            // we suppose we are running an example from development env with full msgui source code
            if (Regex.IsMatch(currentFolder, DevExamplePathPatten, RegexOptions.IgnoreCase | RegexOptions.Compiled))
            {          
                //examplepath\examplename\bin\Debug
                return Directory.GetParent(currentFolder).Parent.Parent.Parent.FullName;
            }
            
            string loc = Environment.GetEnvironmentVariable("LOC");
            if (string.IsNullOrEmpty(loc))
            {
                string env = Environment.GetEnvironmentVariable("ENV");
                if (string.IsNullOrEmpty(env)) env = "PROD";
                string reg = Environment.GetEnvironmentVariable("SYS_LOC").Split('.')[2];

                if (reg == "eu") loc = @"\\msad\root\EU\LN\lib\MSDotNet\Codewiki\MSDesktop\" + env;
                else if (reg == "ap" || reg == "as") loc = @"\\msad\root\AP\APBO\WO\lib\MSDotNet\Codewiki\MSDesktop\" + env;
                else loc = @"\\msad\root\NA\NY\LIB\MSDotNet\Codewiki\MSDesktop\" + env;
            }
            return Path.Combine(loc.Trim('"'), "examples");
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public static string GetExecutableRelativePath()
        {
            if (Environment.UserName == "pjenkbld" || !string.IsNullOrEmpty(Environment.GetEnvironmentVariable("LOC")) ||
               !Regex.IsMatch(AppDomain.CurrentDomain.BaseDirectory, DevAssemblyPathPatten, RegexOptions.IgnoreCase | RegexOptions.Compiled))
            {
#if DEBUG 
                return @"\.debug";
#endif
                return string.Empty;
            }
            else
            {
#if DEBUG
                return @"\bin\Debug";
#endif
                return @"\bin\Release";
            }
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public static string GetExamplePath(string exampleName_)
        {
            return Path.Combine(GetExampleRootPath(), exampleName_) + GetExecutableRelativePath();
        }

        public const string ExampleRootVariable = "ExampleRoot";
        public const string ExeRelativeVariable = "ExeRelative";

        [EditorBrowsable(EditorBrowsableState.Never)]
        public static void PrepareExampleReference(this Application application_)
        {
            Environment.SetEnvironmentVariable(ExampleRootVariable, GetExampleRootPath());
            Environment.SetEnvironmentVariable(ExeRelativeVariable, GetExecutableRelativePath()); 
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public static void PrepareExampleReference()
        {
            PrepareExampleReference(null);
        }
    }
}
