﻿namespace MorganStanley.MSDotNet.MSGui.Core
{
    /***
     * For msdesktop provided modules
     */
    internal interface IMSDesktopModule
    {
        void Initialize();
    }
}