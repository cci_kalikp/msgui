﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2011 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/IShellWindowMenuFacade.cs#4 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System.Windows;

namespace MorganStanley.MSDotNet.MSGui.Core
{
  public interface IShellWindowMenuFacade
  {
    UIElement LoadLayoutMenu { get; }
    UIElement SaveLayoutButton { get; }
    UIElement SaveLayoutAsMenu { get; }
    UIElement DeleteCurrentLayoutButton { get; }
    UIElement ThemesMenu { get; }
    UIElement ExitButton { get; }
  	UIElement HeaderVisibilityToggle { get; }
    UIElement PrintButton { get; }
    UIElement PrintPreviewButton { get; }
  }
}
