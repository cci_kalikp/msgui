﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers.SystemTray
{
	internal interface ISystemTrayFactories
	{
		InitializeWidgetHandler IconFactory { get; }
	}
}
