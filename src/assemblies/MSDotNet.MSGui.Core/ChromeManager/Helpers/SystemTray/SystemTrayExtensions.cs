﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.ViewContainers;
using Microsoft.Practices.Unity;

namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers.SystemTray
{
	public static class SystemTrayExtensions
	{
		#region Factories

		private static IUnityContainer m_container;
		private static ISystemTrayFactories m_systemTrayHelper;

		internal static void InitHelpers(IUnityContainer container_)
		{
			m_container = container_;
			m_systemTrayHelper = m_container.Resolve<ISystemTrayFactories>();
		}

		public static InitializeWidgetHandler IconFactory(this IChromeManager chromeManager_)
		{
			return m_systemTrayHelper.IconFactory;
		}
		
		#endregion
	
		#region Add* Helpers
	
		public static ITrayIconViewContainer AddTrayIcon(this IChromeManager chromeManager_, string ID, InitialTrayIconParameters parameters)
		{
			return chromeManager_.PlaceWidget(ID, ChromeArea.SystemTray, "", parameters) as ITrayIconViewContainer;
		}
 
		#endregion	
	}
}
