﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers
{
	internal interface IStatusbarFactories
	{
		InitializeWidgetHandler LabelFactory { get; }
		InitializeWidgetHandler ProgressBarFactory { get; }
		InitializeWidgetHandler SeparatorFactory { get; }
	}
}
