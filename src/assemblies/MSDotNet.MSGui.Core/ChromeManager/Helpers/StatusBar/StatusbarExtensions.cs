﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.ViewContainers;

namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers.StatusBar
{
	public static class StatusbarExtensions
	{
		#region Factories

		private static IUnityContainer m_container;
		private static IStatusbarFactories m_statusbarHelper;

		internal static void InitHelpers(IUnityContainer container_)
		{
			m_container = container_;
			m_statusbarHelper = m_container.Resolve<IStatusbarFactories>();
		}

		public static InitializeWidgetHandler LabelFactory(this IChromeManager chromeManager_)
		{
			return m_statusbarHelper.LabelFactory;
		}

		public static InitializeWidgetHandler ProgressBarFactory(this IChromeManager chromeManager_)
		{
			return m_statusbarHelper.ProgressBarFactory;
		}

		public static InitializeWidgetHandler SeparatorFactory(this IChromeManager chromeManager_)
		{
			return m_statusbarHelper.SeparatorFactory;
		}

		#endregion

		#region Add* Helpers

		/// <summary>
		/// Adds a progressbar to the left side of the statusbar.
		/// </summary>
		/// <param name="chromeManager_">The Chromemanager instance.</param>
		/// <param name="ID">The desired ID of the new widget.</param>
		/// <param name="parameters">Parameters describing the new widget.</param>
		/// <returns>a container representing the newly created widget.</returns>
		public static IProgressBarViewContainer AddStatusProgressbar(this IChromeManager chromeManager_, string ID, InitialProgressBarParameters parameters)
		{
			return chromeManager_.PlaceWidget(ID, ChromeArea.StatusBar, "left", parameters) as IProgressBarViewContainer;
		}

		/// <summary>
		/// Adds a label to the left side of the statusbar.
		/// </summary>
		/// <param name="chromeManager_">The Chromemanager instance.</param>
		/// <param name="ID">The desired ID of the new widget.</param>
		/// <param name="parameters">Parameters describing the new widget.</param>
		/// <returns>a container representing the newly created widget.</returns>
		public static IStatusLabelViewContainer AddStatusLabel(this IChromeManager chromeManager_, string ID, InitialStatusLabelParameters parameters)
		{
#pragma warning disable 612,618
		    //var statusbar = chromeManager_.Statusbar().LeftSide as IWidgetViewContainer;
		    //return statusbar.AddWidget(ID, parameters) as IStatusLabelViewContainer;
			return chromeManager_.PlaceWidget(ID, ChromeArea.StatusBar, "left", parameters) as IStatusLabelViewContainer;
#pragma warning restore 612,618
		}

		/// <summary>
		/// Adds a progressbar to the left side of the statusbar.
		/// </summary>
		/// <param name="chromeManager_">The Chromemanager instance.</param>
		/// <param name="ID">The desired ID of the new widget.</param>
		/// <param name="location">The location of the widget. use "left" or "right" to choose a side.</param>
		/// <param name="parameters">Parameters describing the new widget.</param>
		/// <returns>a container representing the newly created widget.</returns>
		public static IProgressBarViewContainer AddStatusProgressbar(this IChromeManager chromeManager_, string ID, string location, InitialProgressBarParameters parameters)
		{
			return chromeManager_.PlaceWidget(ID, ChromeArea.StatusBar, location, parameters) as IProgressBarViewContainer;
		}

		/// <summary>
		/// Adds a label to the left side of the statusbar.
		/// </summary>
		/// <param name="chromeManager_">The Chromemanager instance.</param>
		/// <param name="ID">The desired ID of the new widget.</param>
		/// <param name="location">The location of the widget. use "left" or "right" to choose a side.</param>
		/// <param name="parameters">Parameters describing the new widget.</param>
		/// <returns>a container representing the newly created widget.</returns>
		public static IStatusLabelViewContainer AddStatusLabel(this IChromeManager chromeManager_, string ID, string location, InitialStatusLabelParameters parameters)
		{
			return chromeManager_.PlaceWidget(ID, ChromeArea.StatusBar, location, parameters) as IStatusLabelViewContainer;
		}

		#endregion	
	}
}
