﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows;
using System.IO;

namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers
{
	public static class Extensions
	{
		public static ImageSource ToImageSource(this System.Drawing.Icon icon)
		{
			if (icon == null)
				return null;
		    return ToImageSource(icon.ToBitmap(), true);
			 
		}

        public static ImageSource ToImageSource(this System.Drawing.Bitmap image, bool disposeOriginalImage_=false)
        {
            if (image == null)
                return null;
            else
            {
                var bmpHandle = image.GetHbitmap();
                try
                {
                    return System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(bmpHandle, IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                }
                finally
                {
                    NativeMethods.DeleteObject(bmpHandle);
                    if (disposeOriginalImage_)
                    {
                        image.Dispose();
                    }
                }
            }
        }
    public static System.Drawing.Icon ToIcon(this Uri uri, System.Windows.Media.Imaging.BitmapEncoder encoder)
    {
      return ToIcon(BitmapFrame.Create(uri), encoder);
    }

    public static System.Drawing.Icon ToIcon(this Stream bitmapStream, System.Windows.Media.Imaging.BitmapEncoder encoder)
    {
      return ToIcon(BitmapFrame.Create(bitmapStream), encoder);
    }

    public static System.Drawing.Icon ToIcon(this System.Windows.Media.Imaging.BitmapFrame imageSource, System.Windows.Media.Imaging.BitmapEncoder encoder)
    {
      if (imageSource == null) throw new ArgumentNullException("imageSource");
      if (encoder == null) throw new ArgumentNullException("encoder");
      var handle = IntPtr.Zero;
      try
      {
        using (var ms = new MemoryStream())
        {
          handle = GetHicon(imageSource, encoder);
          var icon = System.Drawing.Icon.FromHandle(handle);
          return icon;
        }
      }
      finally
      {
        if (handle != IntPtr.Zero)
        {
          NativeMethods.DeleteObject(handle);
        }
      }
    }

    internal static IntPtr GetHicon(System.Windows.Media.Imaging.BitmapFrame imageSource, System.Windows.Media.Imaging.BitmapEncoder encoder)
    {
      using (var ms = new MemoryStream())
      {
        encoder.Frames.Add(imageSource);
        encoder.Save(ms);
        ms.Seek(0, SeekOrigin.Begin);
        System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(ms);
        return bitmap.GetHicon();
      }
    }

 


		public static Window GetNativeWindow(this IWindowViewContainer vc)
		{
			if (!vc.Parameters.UseDockManager)
			{
				return Window.GetWindow(vc.Content as DependencyObject);
			}
			else
			{
				return null;
			}
		}
	}
}
