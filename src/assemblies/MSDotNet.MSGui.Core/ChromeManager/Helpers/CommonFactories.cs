﻿using System.ComponentModel;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.ViewContainers;

namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers
{
    public static class CommonHelpers
    {
        private static IUnityContainer _container;
        private static ICommonFactories _commonHelper;
        private static IChromeManager _chromeManager;

        internal static void InitHelpers(IUnityContainer container)
        {
            _container = container;
            _commonHelper = _container.Resolve<ICommonFactories>();
            _chromeManager = _container.Resolve<IChromeManager>();
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public static InitializeWidgetHandler ButtonFactory(this IChromeManager chromeManager)
        {
            return _commonHelper.ButtonFactory;
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public static InitializeWidgetHandler ShowWindowButtonFactory(this IChromeManager chromeManager)
        {
            return _commonHelper.ShowWindowButtonFactory;
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public static InitializeWidgetHandler DropdownButtonFactory(this IChromeManager chromeManager)
        {
            return _commonHelper.DropdownButtonFactory;
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public static InitializeWidgetHandler ComboBoxFactory(this IChromeManager chromeManager)
        {
            return _commonHelper.ComboBoxFactory;
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public static InitializeWidgetHandler PopupFactory(this IChromeManager chromeManager)
        {
            return _commonHelper.PopupFactory;
        }

        #region Add* Helpers

        public static IWidgetViewContainer AddToQAT(this IWidgetViewContainer widget)
        {
            return _chromeManager.PlaceWidget(widget.FactoryID, ChromeArea.QAT, "", null);
        }

        public static IWidgetViewContainer AddToQAT(this IWidgetViewContainer widget, int index)
        {
            return _chromeManager.PlaceWidget(widget.FactoryID, ChromeArea.QAT, "", null);
        }

        public static IButtonViewContainer AddButton(this IChromeManager chromeManager, string ID, string location,
                                                     InitialButtonParameters parameters)
        {
            return chromeManager.PlaceWidget(ID, ChromeArea.Default, location, parameters) as IButtonViewContainer;
        }

        public static IButtonViewContainer AddButton(this IChromeManager chromeManager, string ID, string root,
                                                     string location, InitialButtonParameters parameters)
        {
            return chromeManager.PlaceWidget(ID, root, location, parameters) as IButtonViewContainer;
        }

        public static IButtonViewContainer AddButton(this IChromeManager chromeManager, string ID, string location,
                                                     InitialCommandButtonParameters parameters)
        {
            return chromeManager.PlaceWidget(ID, ChromeArea.Default, location, parameters) as IButtonViewContainer;
        }

        public static IButtonViewContainer AddButton(this IChromeManager chromeManager, string ID, string root,
                                                     string location, InitialCommandButtonParameters parameters)
        {
            return chromeManager.PlaceWidget(ID, root, location, parameters) as IButtonViewContainer;
        }

        public static IButtonViewContainer AddShowWindowButton(this IChromeManager chromeManager, string ID,
                                                               string location,
                                                               InitialShowWindowButtonParameters parameters)
        {
            return chromeManager.PlaceWidget(ID, ChromeArea.Default, location, parameters) as IButtonViewContainer;
        }

        public static IButtonViewContainer AddShowWindowButton(this IChromeManager chromeManager, string ID, string root,
                                                               string location,
                                                               InitialShowWindowButtonParameters parameters)
        {
            return chromeManager.PlaceWidget(ID, root, location, parameters) as IButtonViewContainer;
        }

        #endregion
    }
}

