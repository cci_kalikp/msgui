﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows;

namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers.LauncherBar
{
	public class VisibleWhenExpandedConverter : IValueConverter
	{
		#region IValueConverter Members

		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			return (LauncherBarState)value == LauncherBarState.Expanded ? Visibility.Visible : Visibility.Collapsed;
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			return ((Visibility)value == Visibility.Visible) ? LauncherBarState.Expanded : LauncherBarState.Shrinked;
		}

		#endregion
	}
}
