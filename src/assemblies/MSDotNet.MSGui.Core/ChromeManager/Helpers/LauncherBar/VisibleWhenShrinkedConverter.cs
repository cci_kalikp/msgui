﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;

namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers.LauncherBar
{
	public class VisibleWhenShrinkedConverter : IValueConverter
	{
		#region IValueConverter Members

		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			return (LauncherBarState)value == LauncherBarState.Expanded ? Visibility.Collapsed : Visibility.Visible;
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			return ((Visibility)value == Visibility.Collapsed) ? LauncherBarState.Expanded : LauncherBarState.Shrinked;
		}

		#endregion
	}
}
