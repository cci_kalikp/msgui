﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.ViewContainers;

namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers.Locker
{
	public static class LockerExtensions
	{
		#region Add* Helpers

		public static IWidgetViewContainer AddWidget(this IChromeManager chromeManager_, string ID, InitialWidgetParameters parameters)
		{
			return chromeManager_.PlaceWidget(ID, ChromeArea.Locker, "", parameters);
		}

		public static IButtonViewContainer AddButton(this IChromeManager chromeManager_, string ID, InitialButtonParameters parameters)
		{
			return chromeManager_.AddButton(ID, ChromeArea.Locker, "", parameters);
		}

		public static IButtonViewContainer AddShowWindowButton(this IChromeManager chromeManager_, string ID, InitialShowWindowButtonParameters parameters)
		{
			return chromeManager_.PlaceWidget(ID, ChromeArea.Locker, "", parameters) as IButtonViewContainer;
		}

		#endregion	

	}
}
