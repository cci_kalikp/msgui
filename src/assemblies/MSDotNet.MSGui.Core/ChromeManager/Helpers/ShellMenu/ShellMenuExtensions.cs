﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Unity;

namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers.ShellMenu
{
	public static class ShellMenuExtensions
	{
		private static IChromeRegistry m_chromeRegistry;
		private static IShellWindowMenuFacade m_menuFacade;

		private static readonly string LoadLayoutMenuFactoryID = "ShellMenu_LoadLayoutMenu";
		private static readonly string SaveLayoutButtonFactoryID = "ShellMenu_SaveLayoutButton";
		private static readonly string SaveLayoutAsMenuFactoryID = "ShellMenu_SaveLayoutAsMenu";
		private static readonly string DeleteCurrentLayoutButtonFactoryID = "ShellMenu_DeleteCurrentLayoutButton";
		private static readonly string ThemesMenuFactoryID = "ShellMenu_ThemesMenu";
		private static readonly string ExitButtonFactoryID = "ShellMenu_ExitButton";
		private static readonly string HeaderVisibilityToggleFactoryID = "ShellMenu_HeaderVisibilityToggle";
    private static readonly string PrintButtonFactoryID = "ShellMenu_PrintButton";
    private static readonly string PrintPreviewButtonFactoryID = "ShellMenu_PrintPreviewButton";

		private static bool LoadLayoutMenuFactoryRegistered = false;
		private static bool SaveLayoutButtonFactoryRegistered = false;
		private static bool SaveLayoutAsMenuFactoryRegistered = false;
		private static bool DeleteCurrentLayoutButtonFactoryRegistered = false;
		private static bool ThemesMenuFactoryRegistered = false;
		private static bool ExitButtonFactoryRegistered = false;
		private static bool HeaderVisibilityToggleFactoryRegistered = false;
    private static bool PrintButtonFactoryRegistered = false;
    private static bool PrintPreviewButtonRegistered = false;

        public static class ApplicationMenuFactories
        {
            public const string TopmostButton = "{9A3D91C0-F1A8-4DB4-9875-D195E85844A7}.topmostMenu";
            public const string DockingMenu = "{5E61336E-7036-470F-B535-7225BC13C8B9}.dockingMenu";
            public const string LoadLayoutMenu = "{17C2529A-2193-494E-A09E-8C97894D19D2}.menuLoadLayout";
            public const string SaveLayoutButton = "{26763E54-1614-4338-8890-4E0D1EB73412}.saveLayoutButton";
            public const string SaveLayoutMenu = "{2537BCC3-DD75-4229-9405-03706296FEA3}.saveLayoutMenu";
            public const string DeleteLayoutButton = "{64BBEDDA-9BE6-4B2D-A7C5-E3834F80021D}.deleteLayoutButton";
            public const string ThemesButton = "{DF0E926B-97F9-4246-8838-207D6F0886CF}.themesButton";
            public const string OptionsButton = "MSGuiOptionsButton";
            public const string ExitButtonWithSeparator = "{B0F01122-621C-4177-AD9E-BF7AF0B74B46}.exitButton";
            public const string AfterLayoutManagementSeparator = "{CD4DBB84-A51D-4EE2-B2ED-098AED097D52}.separator";
            public const string MinimizeButton = "{2151CB0A-4B6D-49A9-A9B2-B135859E7BC9}.minimizeButton";
            public const string ExitButtonSeparator = "{858F1A52-59D4-41A1-8FE3-10B9BE3981FC}.exitButtonSeperator";

        }

		internal static void InitHelpers(IUnityContainer container_)
		{
			m_chromeRegistry = container_.Resolve<IChromeRegistry>();
			m_menuFacade = container_.Resolve<IShellWindowMenuFacade>();
		}

		/// <summary>
		/// Registers the ID of a factory that can place the "Load layout" menu from the shell menu on the chrome.
		/// </summary>
		/// <param name="chromeManager_"></param>
		/// <returns>The factory name.</returns>
		public static string LoadLayoutMenuFactory(this IChromeManager chromeManager_)
		{
			if (!LoadLayoutMenuFactoryRegistered)
			{
				m_chromeRegistry.RegisterWidgetFactory(LoadLayoutMenuFactoryID, (vc, s) => { vc.Content = m_menuFacade.LoadLayoutMenu; return true; });
			}
			LoadLayoutMenuFactoryRegistered = true;
			return LoadLayoutMenuFactoryID;
		}

		/// <summary>
		/// Registers the ID of a factory that can place the "Save layout" button from the shell menu on the chrome.
		/// </summary>
		/// <param name="chromeManager_"></param>
		/// <returns>The factory name.</returns>
		public static string SaveLayoutButtonFactory(this IChromeManager chromeManager_)
		{
			if (!SaveLayoutButtonFactoryRegistered)
			{
				m_chromeRegistry.RegisterWidgetFactory(SaveLayoutButtonFactoryID, (vc, s) => { vc.Content = m_menuFacade.SaveLayoutButton; return true; });
			}
			SaveLayoutButtonFactoryRegistered = true;
			return SaveLayoutButtonFactoryID;
		}

		/// <summary>
		/// Registers the ID of a factory that can place the "Save layout as" menu from the shell menu on the chrome.
		/// </summary>
		/// <param name="chromeManager_"></param>
		/// <returns>The factory name.</returns>
		public static string SaveLayoutAsMenuFactory(this IChromeManager chromeManager_)
		{
			if (!SaveLayoutAsMenuFactoryRegistered)
			{
				m_chromeRegistry.RegisterWidgetFactory(SaveLayoutAsMenuFactoryID, (vc, s) => { vc.Content = m_menuFacade.SaveLayoutAsMenu; return true; });
			}
			SaveLayoutAsMenuFactoryRegistered = true;
			return SaveLayoutAsMenuFactoryID;
		}

		/// <summary>
		/// Registers the ID of a factory that can place the "Delete current layout" button from the shell menu on the chrome.
		/// </summary>
		/// <param name="chromeManager_"></param>
		/// <returns>The factory name.</returns>
		public static string DeleteCurrentLayoutButtonFactory(this IChromeManager chromeManager_)
		{
			if (!DeleteCurrentLayoutButtonFactoryRegistered)
			{
				m_chromeRegistry.RegisterWidgetFactory(DeleteCurrentLayoutButtonFactoryID, (vc, s) => { vc.Content = m_menuFacade.DeleteCurrentLayoutButton; return true; });
			}
			DeleteCurrentLayoutButtonFactoryRegistered = true;
			return DeleteCurrentLayoutButtonFactoryID;
		}

		/// <summary>
		/// Registers the ID of a factory that can place the "themes" menu from the shell menu on the chrome.
		/// </summary>
		/// <param name="chromeManager_"></param>
		/// <returns>The factory name.</returns>
		public static string ThemesMenuFactory(this IChromeManager chromeManager_)
		{
			if (!ThemesMenuFactoryRegistered)
			{
				m_chromeRegistry.RegisterWidgetFactory(ThemesMenuFactoryID, (vc, s) => { vc.Content = m_menuFacade.ThemesMenu; return true; });
			}
			ThemesMenuFactoryRegistered = true;
			return ThemesMenuFactoryID;
		}

		/// <summary>
		/// Returns the ID of a factory that can place the "Exit" button from the shell menu on the chrome.
		/// </summary>
		/// <param name="chromeManager_"></param>
		/// <returns>The factory name.</returns>
		public static string ExitButtonFactory(this IChromeManager chromeManager_)
		{
			if (!ExitButtonFactoryRegistered)
			{
				m_chromeRegistry.RegisterWidgetFactory(ExitButtonFactoryID, (vc, s) => { vc.Content = m_menuFacade.ExitButton; return true; });
			}
			ExitButtonFactoryRegistered = true;
			return ExitButtonFactoryID;
		}

		/// <summary>
		/// Returns the ID of a factory that can place the "Exit" button from the shell menu on the chrome.
		/// </summary>
		/// <param name="chromeManager_"></param>
		/// <returns>The factory name.</returns>
		public static string HeaderVisibilityToggleFactory(this IChromeManager chromeManager_)
		{
			if (!HeaderVisibilityToggleFactoryRegistered)
			{
				m_chromeRegistry.RegisterWidgetFactory(HeaderVisibilityToggleFactoryID, (vc, s) => { vc.Content = m_menuFacade.HeaderVisibilityToggle; return true; });
			}
			HeaderVisibilityToggleFactoryRegistered = true;
			return HeaderVisibilityToggleFactoryID;
		}

    public static string PrintButtonFactory(this IChromeManager chromeManager_)
    {
      if (!PrintButtonFactoryRegistered)
      {
        m_chromeRegistry.RegisterWidgetFactory(PrintButtonFactoryID, (vc, s) => { vc.Content = m_menuFacade.PrintButton; return true; });
      }
      PrintButtonFactoryRegistered = true;
      return PrintButtonFactoryID;

    }

    public static string PrintPreviewFactory(this IChromeManager chromeManager_)
    {
      if (!PrintPreviewButtonRegistered)
      {
        m_chromeRegistry.RegisterWidgetFactory(PrintPreviewButtonFactoryID, (vc, s) => { vc.Content = m_menuFacade.PrintPreviewButton; return true; });
      }
      PrintPreviewButtonRegistered = true;
      return PrintPreviewButtonFactoryID;
    }

	}
}
