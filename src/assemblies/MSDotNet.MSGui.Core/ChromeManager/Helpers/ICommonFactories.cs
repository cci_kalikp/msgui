﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media.Imaging;

namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager
{
	/// <summary>
	/// Provides factories for frequent use cases with the ChromeManager approach.
	/// </summary>
	internal interface ICommonFactories
	{
		InitializeWidgetHandler ButtonFactory { get; }
		InitializeWidgetHandler SplitButtonFactory { get; }
		InitializeWidgetHandler ShowWindowButtonFactory { get; }
		InitializeWidgetHandler DropdownButtonFactory { get; }
		InitializeWidgetHandler ComboBoxFactory { get; }
		InitializeWidgetHandler PopupFactory { get; }
	}
}
