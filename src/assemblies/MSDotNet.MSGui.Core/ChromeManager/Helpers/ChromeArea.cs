﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers
{
	/// <summary>
	/// Stores the magic strings used in widget placement.
	/// </summary>
	public static class ChromeArea
	{
		/// <summary>
		/// The default chrome area - ribbon for the ribbon shell mode, launcher bar for the launcher shell mode.
		/// </summary>
		public static readonly string Default = null;

		/// <summary>
		/// Warning: Consider using ChromeArea.Default instead to ensure shell mode invariance.
		/// 
		/// Launcher bar root area for the launcher shell. Location string is ignored.
		/// </summary>
		//[Obsolete("Use ChromeArea.Default instead to ensure shell mode invariance.")]
		public static readonly string LauncherBar = "LauncherBar";

		/// <summary>
		/// Warning: Consider using ChromeArea.Default instead to ensure shell mode invariance.
		/// 
		/// Ribbon root area for the ribbon shell. Location string format: {tab}/{group}
		/// </summary>
		//[Obsolete("Use ChromeArea.Default instead to ensure shell mode invariance.")]
		public static readonly string Ribbon = "Ribbon";

	    public static readonly string NavigationBar = "NavigationBar";

	    public static readonly string TopContent = "TopContent";

		/// <summary>
		/// QAT (Quick Access Toolbar) area for the ribbon shell mode. Controls can only be placed here if they have already been
		/// placed on the ribbon.
		/// </summary>
		public static readonly string QAT = "QAT";

		/// <summary>
		/// Statusbar root area.
		/// </summary>
		public static readonly string StatusBar = "StatusBar";

        /// <summary>
        /// TitleBar root area.
        /// </summary>
	    public static readonly string TitleBar = "TitleBar";

		/// <summary>
		/// System tray root area.
		/// </summary>
		public static readonly string SystemTray = "SystemTray";

		/// <summary>
		/// Dummy area to hold widgets until they are placed on the chrome.
		/// </summary>
		public static readonly string Locker = "Locker";

		/// <summary>
		/// Tabwell area for Ribbon shell mode. Items will apear to the right of the main tabwell just below the ribbon.
		/// </summary>
		public static readonly string TabWell = "tabwell";

        /// <summary>
        /// Application menu area. New items will appear just above the 'Exit' button.
        /// </summary>
	    public static readonly string ApplicationMenu = "menu";

	    public static readonly string WorkspaceRootArea = "workspaceRoot";
	}
}
