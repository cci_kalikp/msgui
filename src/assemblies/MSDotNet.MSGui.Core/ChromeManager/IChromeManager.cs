﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Printing;
using System.Windows;
using System.Windows.Media;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent;
using MorganStanley.MSDotNet.MSGui.Core.Printing;
using Size = System.Drawing.Size;

namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager
{
	public interface IChromeManager
	{
		/// <summary>
		/// Create a window using the specified factory and initial parameters.
		/// </summary>
		/// <param name="factoryId">ID of the factory registered with IChromeRegistry.RegisterFactory or IChromeRegistry.RegisterWindowFactory.</param>
		/// <param name="initialParameters">Initial parameters for the new instance.</param>
		/// <returns></returns>
		IWindowViewContainer CreateWindow(string factoryId, InitialWindowParameters initialParameters); 
 

		/// <summary>
		/// Create a dialog window using the specified factory and initial parameters.
		/// </summary>
		/// <param name="factoryId">ID of the factory registered with IChromeRegistry.RegisterFactory or IChromeRegistry.RegisterWindowFactory.</param>
		/// <returns></returns>
		[Obsolete("Consider using a window with Transient set to true and InitialLocation set to FloatingOnly instead.")]
		IDialogWindow CreateDialog(string factoryId);

		/// <summary>
		/// Place a widget on the default control interface.
		/// </summary>
		/// <param name="factoryId">ID of the factory registered with IChromeRegistry.RegisterFactory or IChromeRegistry.RegisterWidgetFactory.</param>
		/// <param name="location">Location within the control interface.</param>
		/// <param name="initialParameters">Initial parameters for the new instance.</param>
		/// <returns></returns>
        [Obsolete("Use IChromeManager.AddWidget and use centralized placement instead.")]
        IWidgetViewContainer PlaceWidget(string factoryId, string location, InitialWidgetParameters initialParameters);

		/// <summary>
		/// Place a widget on the specified control interface.
		/// </summary>
		/// <param name="factoryId">ID of the factory registered with IChromeRegistry.RegisterFactory or IChromeRegistry.RegisterWidgetFactory.</param>
		/// <param name="root">The name of the control interface.</param>
		/// <param name="location">Location within the control interface.</param>
		/// <param name="initialParameters">Initial parameters for the new instance.</param>
		/// <returns></returns>
		[Obsolete("Use IChromeManager.AddWidget and use centralized placement instead.")]
		IWidgetViewContainer PlaceWidget(string factoryId, string root, string location, InitialWidgetParameters initialParameters);

		/// <summary>
		/// Create the user interface.
		/// </summary>
		/// <param name="placeUnassigned">If true, all controls will be placed even if no PlaceWidget calls have been made for some.</param>
	    [EditorBrowsable(EditorBrowsableState.Never)]
        [Obsolete("You no longer need to make this call.")]
		void CreateChrome(bool placeUnassigned);

		/// <summary>
		/// Returns a widget by it's factory ID.
		/// </summary>
		/// <param name="factoryId">The factory ID to look for.</param>
		/// <returns></returns>
		IWidgetViewContainer GetWidget(string factoryId = null);

		/// <summary>
		/// Returns a widget by it's factory ID.
		/// </summary>
		/// <param name="factoryId">The factory ID to look for.</param>
		/// <returns></returns>
		IEnumerable<IWidgetViewContainer> GetWidgets(string factoryId);

		/// <summary>
		/// Remove a widget from the chrome.
		/// </summary>
		/// <param name="placedWidget">The widget to remove</param>
		void RemoveWidget(IWidgetViewContainer placedWidget);

		/// <summary>
		/// Remove a widget from the chrome.
		/// </summary>
		/// <param name="placedWidget">The factory ID for the widget to remove</param>
		void RemoveWidget(string factoryId);

        /// <summary>
        /// Remove all children of a widget from the chrome.
        /// </summary>
        /// <param name="placedWidget">The widget to remove</param>
        void ClearWidget(IWidgetViewContainer placedWidget);

		/// <summary>
		/// Returns an observable collection of the open windows.
		/// </summary>
		ObservableWindowCollection Windows { get; }

        /// <summary>
        /// Returns normal windows and embedded tile items
        /// </summary>
        /// <returns></returns>
	    IEnumerable<IWindowViewContainer> GetViews();

		/// <summary>
		/// Fired right after a layout has been loaded.
		/// </summary>
		event EventHandler LayoutLoaded;

        /// <summary>
        /// Fired when the persistence service completes the handling of the profile.
        /// </summary>
        event EventHandler ProfileProcessingComplete;

        /// <summary>
        /// Fired when the persistence service begins the handling of the profile.
        /// </summary>
        event EventHandler BeginProfileProcessing;

		/// <summary>
		/// Fired right after a window has been activated.
		/// </summary>
		event EventHandler<WindowEventArgs> WindowActivated;

		/// <summary>
		/// Fired right after a window has been deactivated.
		/// </summary>
		event EventHandler<WindowEventArgs> WindowDeactivated;

		/// <summary>
		/// Fired before a window has been deactivated.
		/// </summary>
		event EventHandler<CancellableWindowEventArgs> WindowDeactivating;

		/// <summary>
		/// Fired right after the application closed.
		/// </summary>
		event EventHandler ApplicationClosed;

		#region CM20
		IWidgetViewContainer this[string index] { get; }

		IWidgetViewContainer AddWidget(string factoryId, InitialWidgetParameters parameters, IWidgetViewContainer parent = null);

        /// <summary>
        /// Gets the ribbon if available, null otherwise.
        /// </summary>
        IRibbon Ribbon { get; }
        
        /// <summary>
        /// Gets the tabwell if available, null otherwise.
        /// </summary>
        ITabbedDock Tabwell { get; }
        /// <summary>
        /// Gets the workspaces if available, null otherwise.
        /// </summary>
        IWorkspaces Workspaces { get; }
        /// <summary>
        /// Gets the statusbar if available, null otherwise.
        /// </summary>
        IStatusBar StatusBar { get; }
        

		#endregion
        /// <summary>
        /// 
        /// </summary>
	    ITitleBar TitleBar { get; }

        /// <summary>
        /// Drop event on elements of the shell.
        /// </summary>
	    event EventHandler<DragOrDropOnWindowEventArgs> DropOnShell;
        /// <summary>
        /// PreviewDrop event on elements of the shell.
        /// </summary>
	    event EventHandler<DragOrDropOnWindowEventArgs> PreviewDropOnShell;
        /// <summary>
        /// DragEnter event on elements of the shell.
        /// </summary>
	    event EventHandler<DragOrDropOnWindowEventArgs> DragEnterOnShell;
        /// <summary>
        /// DragOver event on elements of the shell.
        /// </summary>
	    event EventHandler<DragOrDropOnWindowEventArgs> DragOverOnShell;
        /// <summary>
        /// DragLeave event on elements of the shell.
        /// </summary>
	    event EventHandler<DragOrDropOnWindowEventArgs> DragLeaveOnShell;
        /// <summary>
        /// PreviewDragEnter event on elements of the shell.
        /// </summary>
        event EventHandler<DragOrDropOnWindowEventArgs> PreviewDragEnterOnShell;
        /// <summary>
        /// PreviewDragOver event on elements of the shell.
        /// </summary>
        event EventHandler<DragOrDropOnWindowEventArgs> PreviewDragOverOnShell;
        /// <summary>
        /// PreviewDragLeave event on elements of the shell.
        /// </summary>
        event EventHandler<DragOrDropOnWindowEventArgs> PreviewDragLeaveOnShell;
	}


  //@TODO: Move this to its separate files.
  #region Print delegates

  public delegate IPrintImage PrintWindowHandler(IWindowViewContainer emptyViewContainer, InitialPrintParameters printParamters);

  public delegate IPrintImage PreviewPrintWindowHandler(IWindowViewContainer emptyViewContainer, InitialPrintParameters printParamters); 

  #endregion Print delegates


  #region Print Classes
  /// <summary>
  /// Print Window Parameters
  /// </summary>
  public class PrintWindowParameters
  {
    /// <summary>
    /// Whether or not to use Paginator. It is advised to the <see cref="PrintWindowHandler"/>.
    /// </summary>
    public bool UsePaginator { get; set; }

    /// <summary>
    /// Print to XpsDocumentWriter, used for test.
    /// </summary>
    public bool ToXpsWriter { get; set; }

    /// <summary>
    /// Advised Size to <see cref="PrintWindowHandler"/>. 
    /// </summary>
    public Size Size { get; set; }
  }

  public class PrintImage : IPrintImage
  {

    private DrawingVisual m_visual;

    public DrawingVisual DrawingVisual
    {
      get {
        return m_visual;
      }
      internal set {
        if (ImageSource == null)
        {
          m_visual = value;
          IsVisualOrImageSource = true;
        }
        else throw new InvalidOperationException("Image or Visual, only one is allowed to set");
      } 
    }

    private ImageSource m_image;
    public ImageSource ImageSource
    {
      get { return m_image; }
      internal set 
      {
        if (DrawingVisual == null)
        {
          m_image = value;
          IsVisualOrImageSource = false;
        }
        else throw new InvalidOperationException("Image or Visual, only one is allowed to set");
      }
    }

    internal System.Windows.Forms.Control ControlElement { get; set; }

    public PrintImage(Visual visual)
    {
      this.VisualElement = visual;
    }

    internal Visual VisualElement { get; set; } 

    public PrintImage(System.Windows.Forms.Control control)
    {
      this.ControlElement = control;
    }

    internal bool IsVisualOrImageSource
    {
      get;
      set;
    }

  }


  public interface IPrintImage
  {
    /// <summary>
    /// The Visual to print
    /// </summary>
    DrawingVisual DrawingVisual { get;  }

    /// <summary>
    /// Abstract pix-level Image 
    /// </summary>
    ImageSource ImageSource { get; }
  }


  /// <summary>
  /// Initial Print Paramters
  /// </summary>
  public class InitialPrintParameters : PrintWindowParameters
  {
    public InitialPrintParameters()
    {
      //PageMediaSize = new PageMediaSize(PageMediaSizeName.ISOA4);
      //PrintQueue = null;
      //PrintTicket = null;
      PageMediaSizeName = System.Printing.PageMediaSizeName.Unknown;
      PrinterName = null;
      ColorType = InkType.BlackAndWhite;
      Copies = 1;
      Layout = ChromeManager.Layout.Portrait; 
      Options = null;
      Scale = 0.0;
      PageOrientation = System.Printing.PageOrientation.Unknown;
      this.Options = new DefaultPrintOptions();
      UsePaginator = true;
      ToXpsWriter = false;
      Size = new Size();
    }

    public InitialPrintParameters(InitialPrintParameters printParameter)
    {
      if (printParameter == null) throw new ArgumentNullException("printParameter");

      //this.PageMediaSize = printParameter.PageMediaSize;
      //this.PrintQueue = printParameter.PrintQueue;
      //this.PrintTicket = printParameter.PrintTicket;
      this.PageMediaSizeName = System.Printing.PageMediaSizeName.ISOA4;
      this.PrinterName = printParameter.PrinterName;
      this.ColorType = printParameter.ColorType;
      this.Copies = printParameter.Copies;
      this.Layout = printParameter.Layout;
      this.Options = printParameter.Options;
      this.Scale = printParameter.Scale;
      this.PageOrientation = printParameter.PageOrientation;
      this.UsePaginator = printParameter.UsePaginator;
      this.ToXpsWriter = printParameter.ToXpsWriter;
      this.Size = printParameter.Size;
    }

    public PageMediaSizeName? PageMediaSizeName { get; set; }

    public string PrinterName { get; set; }

    ///// <summary>
    ///// Describes the page size for paper or other media. http://msdn.microsoft.com/en-us/library/system.printing.pagemediasize.aspx
    ///// </summary>
    //public PageMediaSize PageMediaSize { get; set; }



    ///// <summary>
    ///// Manages printers and print jobs. http://msdn.microsoft.com/en-us/library/system.printing.printqueue.aspx
    ///// </summary>
    //public PrintQueue PrintQueue { get; set; }

    ///// <summary>
    ///// Settings of a Print job. http://msdn.microsoft.com/en-us/library/system.printing.printticket.aspx
    ///// </summary>
    //public PrintTicket PrintTicket { get; set; }


    /// <summary>
    /// Advise whether to print with Black
    /// </summary>
    public InkType ColorType
    {
      get;
      set;
    }

    /// <summary>
    /// How many copies to print
    /// </summary>
    public int Copies
    {
      get;
      set;
    }


    /// <summary>
    /// Whether it is <see cref="Layout.Portrait"/> or <see cref="Layout.Landscape"/>
    /// </summary>
    [Obsolete("Replaced by 'PageOrientation'")]
    public Layout Layout
    {
      get;
      set;
    }

    /// <summary>
    /// Additional Options to print
    /// </summary>
    public PrintOptions Options
    {
      get;
      set;
    }

    /// <summary>
    /// The scale of the image
    /// </summary>
    public double Scale
    {
      get;
      set;
    }

    /// <summary>
    /// The <see cref="PageOrientation"/>
    /// </summary>
    public PageOrientation? PageOrientation
    {
      get;
      set;
    }

  }

  public enum InkType
  {
    BlackAndWhite = 0,
    Color,
  }

  [Obsolete("use 'PageOrientation' instead'")]
  public enum Layout
  {
    Unknown = 0,
    Portrait = 1,
    Landscape,
  }



  #endregion Print Classes

	public class CancellableWindowEventArgs : WindowEventArgs
	{
		public CancellableWindowEventArgs(IWindowViewContainer view) : base(view) { }
		public bool Cancel { get; set; }
	}

    /// <summary>
    /// A wrapper around DragEventArgs which additionally knows about which view container
    /// the event is associated with.
    /// </summary>
    public class DragOrDropOnWindowEventArgs : EventArgs
    {
        /// <summary>
        /// Gets the view container associated with the event. Will return null if the event
        /// is associated with empty dock manager space (only possible with Tabbed Dock shell
        /// mode).
        /// </summary>
        public IWindowViewContainer Window { get; set; }
        /// <summary>
        /// Original DragEventArgs associated with the event. All modifications will have effect.
        /// </summary>
        public DragEventArgs DragEventArgs { get; set; }
    }
}
