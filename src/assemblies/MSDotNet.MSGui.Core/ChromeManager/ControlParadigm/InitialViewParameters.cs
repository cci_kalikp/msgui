﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager.ControlParadigm
{
	public class InitialConcordParameters : InitialWidgetParameters
	{
		public InitialViewSettings InitialViewSettings { get; internal set; }
	}
}
