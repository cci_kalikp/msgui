﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager
{
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
	public class ViewContainerAttribute : Attribute
	{
		/// <summary>
		/// The type used as ViewModel. The class must implement the IChromeViewModel interface.
		/// </summary>
		internal Type ViewContainerType { get; set; }

		public ViewContainerAttribute(Type viewContainerType_)
			:base()
		{
			this.ViewContainerType = viewContainerType_;
		}
	}
}
