﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager
{
	public interface IViewContainerBase
	{
		string FactoryID { get; }
		InitialViewParameters Parameters { get; }
	}
}
