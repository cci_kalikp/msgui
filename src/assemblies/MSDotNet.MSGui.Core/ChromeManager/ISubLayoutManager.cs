﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager
{
    public interface ISubLayoutManager
    {
        void SaveSubLayout(IWindowViewContainer rootView_, bool skipLocation_, string subLayoutName_, bool saveAs_);
        void LoadSubLayout(string subLayoutName_, SubLayoutLocation location_);
        void RemoveSubLayout(string subLayoutName_);
        void CreateSubLayout(IWindowViewContainer rootView_);
        void UpdateSubLayout(IWindowViewContainer oldRootView_, IWindowViewContainer newRootView_);
        void RenameSubLayout(string oldSubLayoutName_, string newSubLayoutName_);
        void RenameSubLayouts(IDictionary<string, string> namingChanges_);
        IList<SubLayoutDefinition> GetSubLayouts();
        SubLayoutDefinition GetSubLayout(string subLayoutName_);
        bool IsSaved(IWindowViewContainer rootView_);
        bool IsDeleted(IWindowViewContainer rootView_);
        bool IsSubLayoutContainer(IWindowViewContainer rootView_);
        bool IsChanged(IWindowViewContainer rootView_, bool skipLocation_);
        void WriteSubLayoutConfig(string configName_, string value_);
        string ReadSubLayoutConfig(string configName_);
        event EventHandler<SubLayoutChangedEventArgs> SubLayoutChanged;
        ObservableWindowCollection OpenedWorkspaces { get; }
    }

    public enum SubLayoutLocation
    {
        SavedLocation,
        Floating,
        DockedInNewTab
    }
 
    public class SubLayoutChangedEventArgs:EventArgs
    {
        public SubLayoutChangedEventArgs(SubLayoutDefinition oldDefintion_, SubLayoutDefinition newDefinition_)
        {
            OldLayout = oldDefintion_;
            NewLayout = newDefinition_;
        }

        public SubLayoutDefinition OldLayout { get; private set; }
        public SubLayoutDefinition NewLayout { get; private set; }
    }
     
    public class SubLayoutDefinition
    {
        public string Category;
        public string LayoutName;
        public string RootViewId;
    }
}