﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent;

namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager
{
    /// <summary>
    /// Holds a window during its lifetime.
    /// </summary>
    public interface IWindowViewContainer : IViewContainerBase
    {
        /// <summary>
        /// The initial parameters used for creating the window.
        /// </summary>
        new InitialWindowParameters Parameters { get; }

        /// <summary>
        /// Gets or sets the title of the window.
        /// </summary>
        string Title { get; set; }

        /// <summary>
        /// Gets or sets the width of the window (excluding the chrome).
        /// </summary>
        double Width { get; set; }

        /// <summary>
        /// Gets or sets the height of the window (excluding the chrome).
        /// </summary>
        double Height { get; set; }

        double Top { get; }

        double Left { get; }

        double? FloatingWindowMaximumWidth { get; set; }

        double? FloatingWindowMaximumHeight { get; set; }

        /// <summary>
        /// default is 100
        /// </summary>
        double FloatingWindowMinimumWidth { get; set; }

        /// <summary>
        /// default is 100
        /// </summary>
        double FloatingWindowMinimumHeight { get; set; }


        /// <summary>
        /// Gets or sets the content of the window.
        /// </summary>
        object Content { get; set; }

        /// <summary>
        /// Gets the collection that contains the elements in the header of the window.
        /// </summary>
        [Obsolete("Use Header instead.")]
        IList<object> HeaderItems { get; }

        /// <summary>
        /// Gets the root widget of the header.
        /// </summary>
        IWidgetViewContainer Header { get; }

        /// <summary>
        /// Gets if the window is visible.
        /// </summary>
        bool IsVisible { get; }

        /// <summary>
        /// Gets fired when visibility of the window changes.
        /// </summary>
        event EventHandler<VisibilityEventArgs> IsVisibleChanged;

        /// <summary>
        /// Gets fired when a content of the WindowViewContainer changes
        /// </summary>
        event EventHandler<ContentChangedEventArgs> ContentChanged;

        /// <summary>
        /// Gets if the window is active.
        /// </summary>
        bool IsActive { get; }

        bool IsHidden { get; set; }

        /// <summary>
        /// Gets or sets the icon of the window.
        /// </summary>
        Icon Icon { get; set; }

        /// <summary>
        /// Gets or sets the background brush of the window.
        /// </summary>
        System.Windows.Media.Brush Background { get; set; }

        /// <summary>
        /// Gets or sets the unique ID of the window. (Not it's factory ID!)
        /// </summary>
        string ID { get; }

        /// <summary>
        /// Gets or sets the accent colour of the window.
        /// </summary>
        System.Windows.Media.Color AccentColour { get; set; }

        /// <summary>
        /// Gets or sets the brush of the tab when the window is docked in a tab group.
        /// The default brush is used when set to null.
        /// </summary>
        System.Windows.Media.Brush TabColour { get; set; }

        /// <summary>
        /// Gets or sets the brush of the tab when the window is docked in a tab group and
        /// the window is currently active.
        /// The default brush is used when set to null.
        /// </summary>
        System.Windows.Media.Brush SelectedTabColour { get; set; }

        /// <summary>
        /// Gets or sets the transient/persistent behaviour of the window. If true, the window will be excluded from 
        /// the saved layout.
        /// </summary>
        bool Transient { get; set; }

        /// <summary>
        /// Gets or sets if the window is always on top.
        /// </summary>
        bool Topmost { get; set; }

        /// <summary>
        /// Gets or sets if the window is shown on the taskbar.
        /// </summary>
        bool ShowInTaskbar { get; set; }

        /// <summary>
        /// Determines whether the window is owned by the main window when in floating state.
        /// </summary>
        bool OwnedByContainer { get; set; }

        /// <summary>
        /// Gets or sets whether the window can be maximized.
        /// </summary>
        bool CanMaximize { get; set; }

        /// <summary>
        /// If true, double clicking on a pane header will unpin it.
        /// </summary>
        bool UnpinOnHeaderDoubleClick { get; set; }

        /// <summary>
        /// If true, clicking on an un-pinned pane header will restore the pane to pinned state.
        /// </summary>
        bool StayPinnedWhenRevealed { get; set; }

        /// <summary>
        /// Closes the window.
        /// </summary>
        void Close();

        /// <summary>
        /// Activates the window.
        /// </summary>
        void Activate();

        /// <summary>
        /// Flashes the window.
        /// </summary>
        void Flash();

        /// <summary>
        /// Flashes the window.
        /// </summary>
        void Flash(System.Windows.Media.Color color);

        /// <summary>
        /// Called before the window is closed.
        /// </summary>
        event EventHandler<CancelEventArgs> Closing;

        /// <summary>
        /// Called when the window has been closed.
        /// </summary>
        event EventHandler<WindowEventArgs> Closed;

        /// <summary>
        /// Called when the window has been closed due to the current layout being disposed (layout reload, application close).
        /// </summary>
        event EventHandler<WindowEventArgs> ClosedQuietly;

        /// <summary>
        /// Called when the window has been created.
        /// </summary>
        event EventHandler<WindowEventArgs> Created;

        /// <summary>
        /// Called when the topmost property changes.
        /// </summary>
        event EventHandler TopmostChanged;


        /// <summary>
        /// Called when the window is request to print .
        /// </summary>
        PrintWindowHandler PrintHandler { get; set; }

        /// <summary>
        /// Called when the window is request to Preview print.
        /// </summary>
        PreviewPrintWindowHandler PreviewPrintHandler { get; set; }

        /// <summary>
        /// Gets or sets the object used for tooltip contents for the view header.
        /// The same tooltip may be also used while the view is presented in a tab group
        /// pane and user hovers mouse over the tab header. The latter feature needs to be enabled 
        /// with EnableSubTabHeaderTooltips framework extension.
        /// </summary>
        object Tooltip { get; set; }

        /// <summary>
        /// Gets the result of the ShowDialog call. Only available for modal windows.
        /// </summary>
        bool? DialogResult { get; }

        HeaderStructure HeaderStructure { get; set; }

        IContextMenuExtrasHolder ContextMenuExtras { get; }

        event EventHandler<WindowDockStateChangedEventArgs> DockStateChanged; 
    }

    internal interface IWindowViewContainerInternal : IWindowViewContainer
    {
        XDocument ViewState { get; set; }
    }
}
