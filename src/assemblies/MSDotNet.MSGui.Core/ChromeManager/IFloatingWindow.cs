﻿using System;
using System.Collections.Generic;
using System.ComponentModel; 
using System.Windows;

namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager
{
    public interface IFloatingWindow : IWindowViewContainerHostRoot
    {
        Guid GuidID { get; set; }
        Window HostWindow { get; } 
        WindowState WindowState { get; set; }
        double Top { get; set; }
        double Left { get; set; }
        event EventHandler Closed;
        event CancelEventHandler Closing;
        string Title { get; set; }
        string SubLayoutTitle { get; set; }
    }

    public interface IWindowViewContainerHostRoot
    { 
        IEnumerable<IWindowViewContainer> Windows { get; }
    }
}
