﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager.ViewContainers
{
	public interface ITrayIconViewContainer : IWidgetViewContainer
	{
		System.Drawing.Icon Icon { get; set; }
		//bool Visible { get; set; }
		System.Windows.Visibility Visibility { get; set; }
		string Text { get; set; }
	}
}
