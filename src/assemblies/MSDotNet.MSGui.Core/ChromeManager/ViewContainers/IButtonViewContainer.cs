﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows.Media;

namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager.ViewContainers
{
	public interface IButtonViewContainer : IWidgetViewContainer
	{
		/// <summary>
		/// Gets or sets the text displayed on the button.
		/// </summary>
		string Text { get; set; }
		
		/// <summary>
		/// Gets or sets the delegate that will be called when the button is clicked.
		/// </summary>
		EventHandler Click { get; set; }

		/// <summary>
		/// Gets or sets whether the button is enabled or not.
		/// </summary>
		bool Enabled { get; set; }

		/// <summary>
		/// Gets or sets the hint of the button.
		/// </summary>
		string ToolTip { get; set; }

		/// <summary>
		/// Gets or sets the image displayed on the button.
		/// </summary>
		ImageSource Image { get; set; }

		/// <summary>
		/// Gets or sets the size of the button.
		/// </summary>
		ButtonSize Size { get; set; }

        KeyGesture Gesture { get; set; }
	}
}
