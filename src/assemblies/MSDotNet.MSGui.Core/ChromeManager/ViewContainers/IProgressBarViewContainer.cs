﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager.ViewContainers
{
	public interface IProgressBarViewContainer : IWidgetViewContainer
	{
		/// <summary>
		/// Gets or sets the minimum value of the progressbar.
		/// </summary>
		double MinValue { get; set; }

		/// <summary>
		/// Gets or sets the maximum value of the progressbar.
		/// </summary>
		double MaxValue { get; set; }

		/// <summary>
		/// Gets or sets the current position of the progressbar.
		/// </summary>
		double Position { get; set; }
	}
}
