﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager.ViewContainers
{
    public interface IDropdownButtonViewContainer : IWidgetViewContainer
    {
        /// <summary>
        /// Gets or sets the text displayed on the button.
        /// </summary>
        string Text { get; set; }

        bool IsDropDownEnabled { get; set; } 
    }
}
