﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager
{
    public enum LayoutEngine
    {
        Dock = 0,
        Tile = 1,
        Default
    }
}
