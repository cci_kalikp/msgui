﻿using System.Windows;

namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager
{
    /// <summary>
    /// Enables attachment of an initial framework element into a widget.
    /// </summary>
    public class InitialExistingElementWidgetParameters : InitialWidgetParameters
    {
        private readonly FrameworkElement _element;

        internal InitialExistingElementWidgetParameters(FrameworkElement element)
        {
            _element = element;
        }

        internal FrameworkElement Element
        {
            get { return _element; }
        }
    }
}
