﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Xml.Linq;

namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager
{
    public interface IChromeRegistry
    {
        /// <summary>
        /// Register a multipurpose factory.
        /// </summary>
        /// <param name="ID">The ID of the new factory. This must be unique on the application level.</param>
        /// <param name="initWindowHandler">Callback to create windows.</param>
        /// <param name="rehydrateWindowHandler">Callback to restore windows from persistent storage.</param>
        /// <param name="dehydrateWindowHandler">Callback to serialize windows for persistent storage.</param>
        /// <param name="initWidgetHandler">Callback to create widgets.</param>
        /// <param name="rehydrateWidgetHandler">Callback to restore widgets from persistent storage.</param>
        /// <param name="dehydrateWidgetHandler">Callback to serialize widgets for persistent storage.</param>
        //[EditorBrowsable(EditorBrowsableState.Never)]
        void RegisterFactory(string ID,
            InitializeWindowHandler initWindowHandler = null, DehydrateWindowHandler dehydrateWindowHandler = null,
            InitializeWidgetHandler initWidgetHandler = null, DehydrateWidgetHandler dehydrateWidgetHandler = null);

        /// <summary>
        /// Registers a window factory. 
        /// </summary>
        /// <param name="ID">The ID of the factory that will be used to create the window.</param>
        /// <param name="initHandler">The factory delegate.</param>
        /// <param name="dehydrateHandler">The dehydration delegate.</param>
        void RegisterWindowFactory(string ID, InitializeWindowHandler initHandler, DehydrateWindowHandler dehydrateHandler = null);

        /// <summary>
        /// Reuse the window factory mapping based on the base parameter type in the inherited parameter type
        /// </summary>
        /// <typeparam name="F">base parameter type</typeparam>
        /// <typeparam name="T">inherited parameter type</typeparam> 
        void RegisterWindowFactoryMapping<F, T>()  where F : InitialWindowParameters where T : F;

        void ChainWindowFactoryMapping<T>(InitializeWindowHandlerStep extraHandler_) where T : InitialWindowParameters;
        /// <summary>
        /// Registers a window factory. 
        /// </summary>
        /// <param name="ID">The ID of the factory that will be used to create the window.</param>
        IWindowFactoryHolder RegisterWindowFactory(string ID);

        /// <summary>
        /// Registers a dialog factory.
        /// </summary>
        /// <param name="ID">The ID of the factory that will be used to create the window.</param>
        /// <param name="initHandler">The factory delegate.</param>
        [Obsolete("Use RegisterWindowFactory instead")]
        void RegisterDialogFactory(string ID, InitializeDialogHandler initHandler);

        /// <summary>
        /// Registers a widget factory. Usually the only necessary parameter is the <paramref name="ID"/>, the
        /// ChromeManager will figure out the correct factory from the InitialWidgetParameters class that is passed to
        /// its PlaceWidget call unless custom factories are used. 
        /// 
        /// Ability to specify the factory for a given InitialWidgetParameters class will be introduced in a later 
        /// release.
        /// </summary>
        /// <param name="ID">The ID of the factory that will be used to create the widget.</param>
        /// <param name="initHandler">The factory delegate.</param>
        /// <param name="dehydrateHandler">The dehydration delegate.</param>
        void RegisterWidgetFactory(string ID, InitializeWidgetHandler initHandler = null, DehydrateWidgetHandler dehydrateHandler = null);

        /// <summary>
        /// Registers a widget factory. 
        /// </summary>
        /// <param name="ID">The ID of the factory that will be used to create the widget.</param>
        IWidgetFactoryHolder RegisterWidgetFactory(string ID);

        /// <summary>
        /// Registers a widget factory. Usually the only necessary parameter is the <paramref name="ID"/>, the
        /// ChromeManager will figure out the correct factory from the InitialWidgetParameters class that is passed to
        /// its PlaceWidget call unless custom factories are used. 
        /// 
        /// Ability to specify the factory for a given InitialWidgetParameters class will be introduced in a later 
        /// release.
        /// </summary>
        /// <param name="ID">The ID of the factory that will be used to create the widget.</param>
        /// <param name="initHandler">The factory delegate.</param>
        /// <param name="dehydrateHandler">The dehydration delegate.</param>
        void RegisterGlobalStoredWidgetFactory(string ID, InitializeWidgetHandler initHandler = null, DehydrateWidgetHandler dehydrateHandler = null);

        /// <summary>
        /// Registers a widget factory mapping.
        /// </summary>
        /// <typeparam name="T">Type of initial parameters desired widget.</typeparam>
        /// <param name="factory">Factory responsible for creating it.</param>
        void RegisterWidgetFactoryMapping<T>(InitializeWidgetHandler factory)
            where T : InitialWidgetParameters;

        /// <summary>
        /// Registers a widget factory mapping.
        /// </summary>
        /// <typeparam name="T">Type of initial parameters desired widget.</typeparam>
        IWidgetFactoryHolder RegisterWidgetFactoryMapping<T>()
            where T : InitialWidgetParameters;

        /// <summary>
        /// Registers a widget dehydrate handler mapping.
        /// </summary>
        /// <typeparam name="T">Type of initial parameters for the desired widget.</typeparam>
        /// <param name="callback">Method that will handle dehydration.</param>
        void RegisterWidgetDehydrateCallbackMapping<T>(DehydrateWidgetHandler callback)
            where T : InitialWidgetParameters;

        ReadOnlyObservableCollection<string> WindowFactoryIDs { get; }

        ReadOnlyObservableCollection<string> WidgetFactoryIDs { get; }

        

    }
    #region Initialization delegates

    /// <summary>
    /// Initialize an empty window view from scratch or stored XML if state is not null.
    /// </summary>
    /// <param name="emptyViewContainer">The empty container to fill.</param>
    /// <param name="state">The state retrieved from storage if any.</param>
    /// <returns>True if successful.</returns>
    public delegate bool InitializeWindowHandler(IWindowViewContainer emptyViewContainer, XDocument state);

    public delegate bool InitializeWindowHandlerStep(IWindowViewContainer viewContainer, XDocument state, Stack<InitializeWindowHandlerStep> chain);
    /// <summary>
    /// Initialize an empty widget view from scratch or stored XML if state is not null.
    /// </summary>
    /// <param name="emptyViewContainer">The empty container to fill.</param>
    /// <param name="state">The state retrieved from storage if any.</param>
    /// <returns>True if successful.</returns>
    public delegate bool InitializeWidgetHandler(IWidgetViewContainer emptyViewContainer, XDocument state);

    public delegate bool InitializeWidgetHandlerStep(IWidgetViewContainer viewContainer, XDocument state, Stack<InitializeWidgetHandlerStep> chain);

    public delegate bool InitializeDialogHandler(IDialogWindow emptyDialog);


    #endregion

    #region Dehydration delegates

    /// <summary>
    /// Attempt to save a window to storage.
    /// </summary>
    /// <param name="windowViewContainer">The container to serialize.</param>
    /// <returns>An XDocument instance containing data to be persisted, or null if failed.</returns>
    public delegate XDocument DehydrateWindowHandler(IWindowViewContainer windowViewContainer);

    /// <summary>
    /// Attempt to save a widget to storage.
    /// </summary>
    /// <param name="widgetViewContainer">The container to serialize.</param>
    /// <returns>An XDocument instance containing data to be persisted, or null if failed.</returns>
    public delegate XDocument DehydrateWidgetHandler(IWidgetViewContainer widgetViewContainer);


    #endregion

    #region factories
    public interface IWindowFactoryHolder
    {
        string Name { get; }
        IWindowFactoryHolder SetInitHandler(InitializeWindowHandler value);
        IWindowFactoryHolder SetDehydrateHandler(DehydrateWindowHandler value);
    }

    internal interface IWindowFactoryHolderInternal : IWindowFactoryHolder
    {
        void AddStep(InitializeWindowHandlerStep step);
        InitializeWindowHandler BuildUp();
    }

    public interface IWidgetFactoryHolder
    {
        string Name { get; }
        IWidgetFactoryHolder SetInitHandler(InitializeWidgetHandler value);
        IWidgetFactoryHolder SetDehydrateHandler(DehydrateWidgetHandler value, bool global=false);

    }
    #endregion



}
