﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager
{
	public static class WidgetViewContainerExtensions
	{
		/// <summary>
		/// Determines if the content property is of the specified type and not null.
		/// </summary>
		/// <typeparam name="T">the desired type</typeparam>
		/// <param name="vc_">The container object to test</param>
		/// <returns>True if not null and of type.</returns>
		public static bool ContentOfType<T>(this IWidgetViewContainer vc_)
			where T : class
		{
			if (vc_ == null || vc_.Content == null)
			{
				return false;
			}
			else
			{
				return (vc_.Content as T) != null;
			}
		}

		internal static IChromeManager chromeManager = null;

        /// <summary>
        /// Adds and returns a new widget using an auto-generated ID.
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static IWidgetViewContainer AddWidget(this IWidgetViewContainer parent, InitialWidgetParameters parameters)
        {
            return chromeManager.AddWidget(Guid.NewGuid().ToString(), parameters, parent);
        }

        /// <summary>
        /// Adds and returns a new widget.
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="ID"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static IWidgetViewContainer AddWidget(this IWidgetViewContainer parent, string ID, InitialWidgetParameters parameters)
        {
            return chromeManager.AddWidget(ID, parameters, parent);
        }

		public static void RemoveWidget(this IWidgetViewContainer parent, string childID)
		{
			if (parent.Contains(childID))
			{
				chromeManager.RemoveWidget(parent[childID]);
			}
		}

		public static void RemoveWidget(this IWidgetViewContainer parent, IWidgetViewContainer child)
		{
			if (parent.Contains(child.FactoryID))
			{
				chromeManager.RemoveWidget(child);
			}
		}

        public static void Clear(this IWidgetViewContainer widget)
        {
            chromeManager.ClearWidget(widget);
        }
	}
}
