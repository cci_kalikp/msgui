﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager
{
	/// <summary>
	/// Holds a widget during its lifetime.
	/// </summary>
	public interface IWidgetViewContainer : IViewContainerBase
	{
		/// <summary>
		/// The initial parameters used for creating the widget.
		/// </summary>
		new InitialWidgetParameters Parameters { get; }

		/// <summary>
		/// The parent widget, if any.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Never)]
		IWidgetViewContainer Parent { get; }

		/// <summary>
		/// Children of the widget.
		/// </summary>
		/// <param name="index">Name of the child</param>
		/// <returns>the child widget.</returns>
		IWidgetViewContainer this[string index] { get; }

		/// <summary>
		/// Returns if a child with the specified ID exists.
		/// </summary>
		/// <param name="index">The ID to look for.</param>
		/// <returns>True if a child exists with the ID</returns>
		bool Contains(string index);

		/// <summary>
		/// The user interface element for the widget.
		/// </summary>
		object Content { get; set; }

		/// <summary>
		/// Executed when the factory method has successfully been executed and the content is created.
		/// </summary>
		event EventHandler ContentReady;
	}
}
