﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager
{
	/// <summary>
	/// Specifies the ViewModel class that belongs to the factory.
	/// </summary>
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
	public class FactoryOptionsAttribute : Attribute
	{
		internal Type ViewContainerType { get; set; }
		
		public FactoryOptionsAttribute(Type viewContainerType_)
			: base()
		{
			this.ViewContainerType = viewContainerType_;
		}
	}
}
