﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager
{
    internal interface IWindowViewContainerHost
    {
        IWindowViewContainer View { get; }
        IFloatingWindow FloatingWindow { get; }
    }
}
