﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager
{
    public interface ITileItemViewContainer : IWindowViewContainer, IChildViewContainer
    {
        /// <summary>
        /// The initial parameters used for creating the tile item.
        /// </summary>
        new InitialTileItemParameters Parameters { get; }


    }
}
