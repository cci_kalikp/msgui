﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace MorganStanley.MSDotNet.MSGui.Core
{
	//public interface IViewManager
	//{
	//    void RegisterFactory(string ID,
	//        InitializeWindowHandler initWindowHandler = null, RehydrateWindowHandler rehydrateWindowHandler = null, DehydrateWindowHandler dehydrateWindowHandler = null,
	//        InitializeWidgetHandler initWidgetHandler = null, RehydrateWidgetHandler rehydrateWidgetHandler = null, DehydrateWidgetHandler dehydrateWidgetHandler = null);


	//    void RegisterWindowFactory(string ID, InitializeWindowHandler initHandler, RehydrateWindowHandler rehydrateHandler = null, DehydrateWindowHandler dehydrateHandler = null);
	//    void RegisterWidgetFactory(string ID, InitializeWidgetHandler initHandler, RehydrateWidgetHandler rehydrateHandler = null, DehydrateWidgetHandler dehydrateHandler = null);

	//    IWindowViewContainer CreateWindow(string factoryID, InitialWindowParameters parameters = null);
	//    IWidgetViewContainer CreateWidget(string factoryID, InitialWidgetParameters parameters = null);
	//}



	//#region Intitialization delegates

	///// <summary>
	///// Initialize an empty view.
	///// </summary>
	///// <param name="emptyViewContainer">The empty container to fill.</param>
	///// <returns>True if successful.</returns>
	//public delegate bool InitializeViewHandler(IViewContainer emptyViewContainer);

	///// <summary>
	///// Initialize an empty window view.
	///// </summary>
	///// <param name="emptyViewContainer">The empty container to fill.</param>
	///// <returns>True if successful.</returns>
	//public delegate bool InitializeWindowHandler(IWindowViewContainer emptyViewContainer);

	///// <summary>
	///// Initialize an empty widget view.
	///// </summary>
	///// <param name="emptyViewContainer">The empty container to fill.</param>
	///// <returns>True if successful.</returns>
	//public delegate bool InitializeWidgetHandler(IWidgetViewContainer emptyViewContainer);

	//#endregion

	//#region Rehydration delegates

	///// <summary>
	///// Attempt to rehydrate a view from storage.
	///// </summary>
	///// <param name="emptyViewContainer">The empty container to fill.</param>
	///// <param name="savedState">The state retrieved from storage.</param>
	///// <returns>True if successful.</returns>
	//public delegate bool RehydrateViewHandler(IViewContainer emptyViewContainer, XDocument savedState);

	///// <summary>
	///// Attempt to rehydrate a window from storage.
	///// </summary>
	///// <param name="emptyViewContainer">The empty container to fill.</param>
	///// <param name="savedState">The state retrieved from storage.</param>
	///// <returns>True if successful.</returns>
	//public delegate bool RehydrateWindowHandler(IWindowViewContainer emptyWindowViewContainer, XDocument savedState);

	///// <summary>
	///// Attempt to rehydrate a widget from storage.
	///// </summary>
	///// <param name="emptyViewContainer">The empty container to fill.</param>
	///// <param name="savedState">The state retrieved from storage.</param>
	///// <returns>True if successful.</returns>
	//public delegate bool RehydrateWidgetHandler(IWidgetViewContainer emptyWidgetViewContainer, XDocument savedState);

	//#endregion


	//#region Dehydration delegates

	///// <summary>
	///// Attempt to save a view to storage.
	///// </summary>
	///// <param name="viewContainer">The container to serialize.</param>
	///// <returns>An XDocument instance containing data to be persisted, or null if failed.</returns>
	//public delegate XDocument DehydrateViewHandler(IViewContainer viewContainer);

	///// <summary>
	///// Attempt to save a window to storage.
	///// </summary>
	///// <param name="emptyViewContainer">The container to serialize.</param>
	///// <returns>An XDocument instance containing data to be persisted, or null if failed.</returns>
	//public delegate XDocument DehydrateWindowHandler(IWindowViewContainer windowViewContainer);

	///// <summary>
	///// Attempt to save a widget to storage.
	///// </summary>
	///// <param name="emptyViewContainer">The container to serialize.</param>
	///// <returns>An XDocument instance containing data to be persisted, or null if failed.</returns>
	//public delegate XDocument DehydrateWidgetHandler(IWidgetViewContainer widgetViewContainer);

	//#endregion
}
