﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent;
using MorganStanley.MSDotNet.MSGui.Core.SmartApi;

namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager
{
	[EditorBrowsable(EditorBrowsableState.Never)]
    [Serializable]
	public class InitialViewParameters : IHideObjectMembers
	{
		public InitialViewParameters()
		{
		}
	}

	/// <summary>
	/// Holds settings that define how a window is created.
	/// </summary>
    [Serializable]
	public class InitialWindowParameters : InitialViewParameters
	{
        public static InitialWindowParameters Defaults = null;
        
        private InitialLocation _initialLocation = InitialLocation.Floating;
	    private bool _isModal;
        private string _singletonKey = string.Empty;
        private bool _showFlashBorder = true;
        private bool _transient;
        private bool _useDockManager = true; 

        [NonSerialized]
        private CustomInitialLocationDelegate _initialLocationCallback;
        [NonSerialized]
        private IWindowViewContainer _initialLocationTarget;
        [NonSerialized]
        private ResourceDictionary _resources;

        /// <summary>
        /// Gets or sets resize mode of the window. Defaults to CanResize.
        /// </summary>
	    public virtual ResizeMode ResizeMode { get; set; }



	    private bool _injectRootRegionManger;

        /// <summary>
        /// to inject the root region manager into the view content. 
        /// </summary>
	    public bool InjectRootRegionManager
	    {
	        get { return _injectRootRegionManger; } 
            set { _injectRootRegionManger = value; }
	    }

        /// <summary>
        /// Gets or sets whether the window is modal. There are several restrictions on
        /// modal windows: 
        /// * cannot be docked
        /// * cannot be persisted
        /// Additionally, if set to true, SizingMethod is set to Custom for convenience.
        /// </summary>
	    public virtual bool IsModal
	    {
	        get
	        {
	            return _isModal;
	        }
	        set
	        {
	            _isModal = value;
                if (value)
                {
                    if (!InitialLocation.HasFlag(InitialLocation.FloatingOnly))
                    {
                        InitialLocation = InitialLocation.FloatingOnly;
                    }
                    Transient = true;
                    UseDockManager = false;
                    SizingMethod = SizingMethod.Custom;
                }
	        }
	    }

	    /// <summary>
		/// Gets or sets whether the window is singleton or not.
		/// </summary>
		public virtual bool Singleton { get; set; }

		/// <summary>
		/// Gets or sets whether the window is singleton or not.
		/// </summary>
		public virtual string SingletonKey 
		{ 
			get
			{
				return this._singletonKey;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("SingletonKey", "The singleton key can not be null. Use string.Empty to disable keying.");
				}

				_singletonKey = value;
			}
		}

        public bool ShowFlashBorder { get { return _showFlashBorder; } set { _showFlashBorder = value; } }

		/// <summary>
		/// Gets or sets the transient/persistent behaviour of the window. If true, the window will be excluded from 
		/// the saved layout.
		/// </summary>
		public virtual bool Transient 
		{ 
			get
			{
				return _transient;
			}
			set
			{
                if (IsModal && !value)
                {
                    throw new ArgumentOutOfRangeException("Transient", "Modal windows have to be transient");
                }
				
                _transient = value;
				if (value)
				{
					if ((_initialLocation & InitialLocation.Floating) == InitialLocation.Floating)
					{
                        _initialLocation -= InitialLocation.Floating;
                        _initialLocation |= InitialLocation.FloatingOnly;
					}
				}
			}
		}

		/// <summary>
		/// Gets or sets where the window will be placed when shown.
		/// </summary>
		public virtual InitialLocation InitialLocation
		{
			get
			{
				return _initialLocation;
			}
			set
			{
				//if ((int)value >= (int)InitialLocation.PlaceAtCursor && 
				//    !((value & InitialLocation.Floating) == InitialLocation.Floating || (value & InitialLocation.FloatingOnly) == InitialLocation.FloatingOnly))
				//{
				//    throw new ArgumentOutOfRangeException("InitialLocation", "values other than Floating, FloatingOnly and DockInNewTab can only be used in conjunction with Floating or FloatingOnly.");
				//}
				//else
                if (IsModal && !value.HasFlag(InitialLocation.FloatingOnly))
                {
                    throw new ArgumentOutOfRangeException("InitialLocation", "Modal windows can be floating only");
                }
				if (value == (InitialLocation.Floating) ||
				    value == (InitialLocation.FloatingOnly) ||
				    value == (InitialLocation.Floating | InitialLocation.PlaceAtCursor) ||
				    value == (InitialLocation.FloatingOnly | InitialLocation.PlaceAtCursor) ||
				    value == (InitialLocation.Floating | InitialLocation.Custom) ||
				    value == (InitialLocation.FloatingOnly| InitialLocation.Custom) ||
				    value == (InitialLocation.DockTabbed) ||
				    value == (InitialLocation.DockLeft) ||
				    value == (InitialLocation.DockTop) ||
				    value == (InitialLocation.DockRight) ||
				    value == (InitialLocation.DockBottom) ||
					value == (InitialLocation.DockInNewTab) ||
                    value == (InitialLocation.DockTabbed | InitialLocation.DockInActiveTab) ||
                    value == (InitialLocation.DockLeft | InitialLocation.DockInActiveTab) ||
				    value == (InitialLocation.DockTop | InitialLocation.DockInActiveTab) ||
				    value == (InitialLocation.DockRight | InitialLocation.DockInActiveTab) ||
				    value == (InitialLocation.DockBottom | InitialLocation.DockInActiveTab)
					)
				{
					if ((value & Core.InitialLocation.PlaceAtCursor) == Core.InitialLocation.PlaceAtCursor)
						/* Compatibility hack: originally PlaceAtCursor was the only funky initial placement, now we 
						 * allow anything via a callback so PlaceAtCursor was updated to use the callback.
						 */
					{
						_initialLocation = (InitialLocation)((int)value - (int)InitialLocation.PlaceAtCursor + (int)InitialLocation.Custom);
						this.InitialLocationCallback = InitialLocationHelper.PlaceAtCursor;
					}
					else
					{
						_initialLocation = value;
					}
				}
			}
		}

		/// <summary>
		/// Gets or sets the custom logic used to determine the initial location of a window when InitialLocation is 
		/// set to InitialLocation.Custom .
		/// </summary>
		public virtual  CustomInitialLocationDelegate InitialLocationCallback
		{
		    get
		    {
		        return _initialLocationCallback;
		    }
		    set
		    {
		        _initialLocationCallback = value;
		    }
		}

	    /// <summary>
		/// Gets or sets the view used for specifying docking target when using InitialLocation.Dock* values.
		/// </summary>
		public virtual IWindowViewContainer InitialLocationTarget
	    {
	        get
	        {
	            return _initialLocationTarget;
	        }
	        set
	        {
	            _initialLocationTarget = value;
	        }
	    }

        /// <summary>
        /// Gets or sets the tab used for specifying docking target when using InitialLocation.DockInNewTab value.
        /// </summary>
        public virtual IDockTab InitialLocationTargetTab { get; set; }

	    /// <summary>
		/// Gets or sets if the window can be resized by the user or code.
		/// </summary>
		public virtual SizingMethod SizingMethod { get; set; }

		/// <summary>
		/// Gets or sets the width of the window when it is first shown.
		/// </summary>
		public double Width { get; set; }

		/// <summary>
		/// Gets or sets the height of the window when it is first shown.
		/// </summary>
		public double Height { get; set; }

		/// <summary>
		/// Gets or sets if the window is allowed to be larger than a single display.
		/// </summary>
		public bool EnforceSizeRestrictions { get; set; }

		/// <summary>
        /// Gets or sets if the window is always on top. would only take effect when UseDockManager is false
		/// </summary>
		public bool Topmost { get; set; }

		/// <summary>
		/// Gets or sets if the window is shown on the taskbar.
		/// </summary>
		public bool ShowInTaskbar { get; set; }

		/// <summary>
		/// Gets or sets if the window can be moved partially offscreen.
		/// </summary>
		public bool AllowPartiallyOffscreen { get; set; }

        /// <summary>
        /// Gets or sets whether the window's header is initially visible.
        /// </summary>
        public bool IsHeaderVisible { get; set; }
         

        [Obsolete("Please set IWindowViewContainer.Title directly"), EditorBrowsable(EditorBrowsableState.Never)]
        public string Title { get; set; }

        #region View Persistence Related
        public bool Persistable { get; set; }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public Type PersistorType { get; set; }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool DynamicComponentPersistors { get; set; }
        #endregion

        /// <summary>
		/// Gets or sets the resources applied to the window.
		/// </summary>
		public ResourceDictionary Resources
		{
		    get
		    {
		        return _resources;
		    }
		    set
		    {
		        _resources = value;
		    }
		}

	    [EditorBrowsable(EditorBrowsableState.Never)]
	    public virtual bool UseDockManager
	    {
	        get { return _useDockManager; } 
            set
            {
                if (IsModal && value)
                {
                    throw new ArgumentOutOfRangeException("UseDockManager", "Modal windows cannot use dock manager");
                }
                _useDockManager = value;
            }
	    }


        #region Workspace Related
        public string WorkspaceCategory { get; set; }

        #endregion


        [Obsolete("To support setting a centralized default value (via Framework.SetDefaultWindowParameters) this method has been obsoleted. Please use object initializer syntax instead.", true)]
		public InitialWindowParameters(
			bool singleton = false,
			InitialLocation initialLocation = InitialLocation.Floating,
			SizingMethod sizingMethod = SizingMethod.SizeToContent,
			double width = double.NaN,
			double height = double.NaN,
			bool enforceSizeRestrictions = true
			)
		{
			this.Singleton = singleton;
			this.InitialLocation = initialLocation;
			this.SizingMethod = sizingMethod;
			this.Width = width;
			this.Height = height;
			this.EnforceSizeRestrictions = enforceSizeRestrictions;
		}

		public InitialWindowParameters()
		{
			this.Singleton = false;
			this._singletonKey = "";
			this.InitialLocation = InitialLocation.Floating;
			this.SizingMethod = SizingMethod.SizeToContent;
			this.Width = double.NaN;
			this.Height = double.NaN;
			this.EnforceSizeRestrictions = true;
			this.Transient = false;
			this.Topmost = false;
			this.ShowInTaskbar = true;
			this.AllowPartiallyOffscreen = true;
		    this.IsHeaderVisible = true;
		    ResizeMode = ResizeMode.CanResize;

			if (InitialWindowParameters.Defaults != null)
			{
				this.EnforceSizeRestrictions = InitialWindowParameters.Defaults.EnforceSizeRestrictions;
				this.InitialLocation = InitialWindowParameters.Defaults.InitialLocation;
				this.ShowFlashBorder = InitialWindowParameters.Defaults.ShowFlashBorder; 
			    this.Persistable = InitialWindowParameters.Defaults.Persistable;
			    this.PersistorType = InitialWindowParameters.Defaults.PersistorType;
                #region Workspace Related
                this.WorkspaceCategory = InitialWindowParameters.Defaults.WorkspaceCategory;
                #endregion
            }
		}
	}

    public sealed class InitialCafGuiWindowParameters : InitialWindowParameters
    {
        private readonly string _cafGuiViewId;
        private readonly UIElement _cafGuiUiElement;

        public InitialCafGuiWindowParameters(string cafGuiViewId, UIElement cafGuiUiElement)
        {
            _cafGuiUiElement = cafGuiUiElement;
            _cafGuiViewId = cafGuiViewId;
        }

        public string CafGuiViewId
        {
            get { return _cafGuiViewId; }
        }

        public UIElement CafGuiUiElement
        {
            get { return _cafGuiUiElement; }
        }
    }

    internal sealed class InitialIsolatedViewParameters : InitialPlaceableWindowParameters
    {
        private readonly UIElement _isolatedControlHost;
        private readonly XDocument _state;

        internal InitialIsolatedViewParameters(string viewId, UIElement isolatedControlHost, XDocument state)
            : base(viewId)
        {
            _isolatedControlHost = isolatedControlHost;
            _state = state;
        }

        internal InitialIsolatedViewParameters(string viewId, UIElement isolatedControlHost, XDocument state,
                                               InitialWindowParameters initialParameters)
            : this(viewId, isolatedControlHost, state)
        {
            Transient = initialParameters.Transient;
            InitialLocation = (InitialLocation) (int) initialParameters.InitialLocation;
            UseDockManager = initialParameters.UseDockManager;
            ShowFlashBorder = initialParameters.ShowFlashBorder;
            IsModal = initialParameters.IsModal;
            SingletonKey = initialParameters.SingletonKey;
            SizingMethod = (SizingMethod) (int) initialParameters.SizingMethod;
            Topmost = initialParameters.Topmost;
            ResizeMode = (ResizeMode) (int) initialParameters.ResizeMode;
            EnforceSizeRestrictions = initialParameters.EnforceSizeRestrictions;
        }

        public UIElement IsolatedControlHost
        {
            get { return _isolatedControlHost; }
        }

        public XDocument State
        {
            get { return _state; }
        }
    }

    public class InitialPlaceableWindowParameters : InitialWindowParameters
    {
        private readonly string _viewId;

        public InitialPlaceableWindowParameters(string viewId)
        {
            _viewId = viewId;
        }

        public string ViewId
        {
            get { return _viewId; }
        }
    }

    public class InitialTileParameters:InitialWindowParameters
    {
        public new static InitialTileParameters Defaults;

        public bool IsFloating { get; set;}
        [Browsable(false)]
        public override InitialLocation InitialLocation
        {
            get
            {
                if (IsFloating)
                {
                    InitialLocation location = InitialLocation.Floating;
                    if (!double.IsNaN(Left) && !double.IsNaN(Top))
                    {
                        location |= InitialLocation.Custom;
                    }
                    return location;
                }
                return InitialLocation.DockInNewTab;
            }
            set
            {
                 
            }
        }

        #region none used parameters
        [Browsable(false)]
        public override bool IsModal
        {
            get { return false; }
            set
            { 
            }
        }

        [Browsable(false)]
        public override bool Singleton
        {
            get { return false; }
            set
            {
                 
            }
        }

        [Browsable(false)]
        public override string SingletonKey
        {
            get { return null; }
            set { }
        }

        [Browsable(false)]
        public override IWindowViewContainer InitialLocationTarget
        {
            get { return null; }
            set
            {
                 
            }
        }

        [Browsable(false)]
        public override bool Transient
        {
            get { return false; }
            set
            {
                 
            }
        }

        [Browsable(false)]
        public override SizingMethod SizingMethod
        {
            get
            {
                return SizingMethod.Custom;
            }
            set
            {
                
            }
        }
        #endregion

        private double left;
        public double Left
        {
            get { return left; }
            set
            {
                left = value;
                if (!double.IsNaN(left) && !double.IsNaN(top))
                this.InitialLocationCallback = (w_, h_) => new Rect(left, top, w_,h_); 
            }
        }

        private double top;
        public double Top
        {
            get { return top; }
            set
            {
                top = value;
                if (!double.IsNaN(left) && !double.IsNaN(top))
                    this.InitialLocationCallback = (w_, h_) => new Rect(left, top, w_, h_);
            }
        }

        public Icon Icon { get; set; }
        /// <summary>
        /// Append available items to "Edit" menu, if AppStoreButtonParameters is specified, this would be automatically set to false
        /// </summary>
        public bool GenerateAvailableItems { get; set; }

        public string EditModeSuffix { get; set; }
        public InitialDropdownButtonParameters AppStoreButtonParameters { get; set; }
        public bool AllowZoom { get; set; }
        
        public double UnitSize { get; set; }
        public double MarginSize { get; set; }  
        public InitialTileParameters()
        {
            this.IsFloating = true;
			this.Width = double.NaN;
			this.Height = double.NaN; 
			this.EnforceSizeRestrictions = true; 
			this.Topmost = false;
			this.ShowInTaskbar = true;
			this.AllowPartiallyOffscreen = true;
		    this.IsHeaderVisible = true;
            this.UnitSize = 4d;
            this.MarginSize = 2d; 
            EditModeSuffix = " - Editing";
		    ResizeMode = ResizeMode.CanResize;
            GenerateAvailableItems = true;
            if (InitialTileParameters.Defaults != null)
			{
                this.EnforceSizeRestrictions = InitialTileParameters.Defaults.EnforceSizeRestrictions;
                this.ShowFlashBorder = InitialTileParameters.Defaults.ShowFlashBorder;
                this.Width = InitialTileParameters.Defaults.Width;
			    this.Height = InitialTileParameters.Defaults.Height;
			    this.Left = InitialTileParameters.Defaults.Left;
			    this.Top = InitialTileParameters.Defaults.Top;
			    this.UnitSize = InitialTileParameters.Defaults.UnitSize;
			    this.MarginSize = InitialTileParameters.Defaults.MarginSize; 
			    this.Persistable = InitialTileParameters.Defaults.Persistable; 
                this.PersistorType = InitialTileParameters.Defaults.PersistorType;
                this.Icon = InitialTileParameters.Defaults.Icon;
                this.WorkspaceCategory = InitialTileParameters.Defaults.WorkspaceCategory;
                this.AppStoreButtonParameters = InitialTileParameters.Defaults.AppStoreButtonParameters;
			    this.AllowZoom = Defaults.AllowZoom;
			    this.GenerateAvailableItems = Defaults.GenerateAvailableItems;
                this.IsFloating = Defaults.IsFloating;
                this.IsHeaderVisible = Defaults.IsHeaderVisible;
			    this.EditModeSuffix = Defaults.EditModeSuffix;
			}
		}
    }
     

    [Serializable]
    public class InitialTileItemParameters : InitialWindowParameters
    {
        public new static InitialTileItemParameters Defaults = null;

        #region none used parameters
        [Browsable(false)]
        public override bool IsModal
        {
            get { return false; }
            set
            { 
            }
        }

        [Browsable(false)]
        public override bool Transient
        {
            get { return false; }
            set
            {
            }
        }

        [Browsable(false)]
        public override bool UseDockManager
        {
            get { return false; }
            set
            {
            }
        }

        [Browsable(false)]
        public override InitialLocation InitialLocation
        {
            get
            {
                return base.InitialLocation;
            }
            set
            {
                base.InitialLocation = value;
            }
        }

 
        #endregion

        public override ResizeMode ResizeMode
        {
            get { return AllowMaximize ? ResizeMode.CanResize : ResizeMode.NoResize; }
            set { AllowMaximize = (value != ResizeMode.NoResize); }
        }

        public CustomInitialLocationDelegate2 InitialLocationCallback { get; set; }
        public bool AllowMaximize { get; set; }

        public Uri MaximizeIconSource { get; set; }

        public Uri CloseIconSource { get; set; }

        [NonSerialized]
        private ImageSource maximizeIcon;
        public ImageSource MaximizeIcon
        {
            get
            {
                if (maximizeIcon == null && MaximizeIconSource != null)
                {
                    maximizeIcon = new BitmapImage(MaximizeIconSource);
                }
                return maximizeIcon;
            }
        }

        [NonSerialized]
        private ImageSource closeIcon;
        public ImageSource CloseIcon
        {
            get
            {
                if (closeIcon == null && CloseIconSource != null)
                {
                    closeIcon = new BitmapImage(CloseIconSource);
                }
                return closeIcon;
            }
        }
        /// <summary>
        /// Gets or sets the left point of the tile item when it is first added to the tile view.
        /// </summary>
        public double Left { get; set; }
         
        /// <summary>
        /// Gets or sets the top point of the tile item when it is first added to the tile view.
        /// </summary>
        public double Top { get; set; } 

		public InitialTileItemParameters()
		{
			this.Singleton = false;
			this.SingletonKey = ""; 
			this.SizingMethod = SizingMethod.SizeToContent;
			this.Width = double.NaN;
			this.Height = double.NaN;  
		    this.AllowMaximize = true;
		    this.IsHeaderVisible = true;

			if (InitialTileItemParameters.Defaults != null)
			{ 
				this.ShowFlashBorder = InitialTileItemParameters.Defaults.ShowFlashBorder;
			    this.MaximizeIconSource = InitialTileItemParameters.Defaults.MaximizeIconSource;
                this.CloseIconSource = InitialTileItemParameters.Defaults.CloseIconSource;
			    this.Persistable = InitialTileItemParameters.Defaults.Persistable; 
                this.PersistorType = InitialTileItemParameters.Defaults.PersistorType;
			}
		}
	}

    [Serializable]
    public class TileItemDragInfo
    {
        public InitialTileItemParameters Parameters { get; set; }
        public string FactoryId { get; set; }
    }

    public class InitialDialogWindowParameters : InitialWindowParameters
    {
        /// <summary>
        /// Gets or sets the owner window. This only applies when IsModal is set to true.
        /// </summary>
        private Window _modalViewOwner;
        public Window ModalViewOwner
        {
            get
            {
                return _modalViewOwner;
            }
            set
            {
                if (!IsModal)
                {
                    throw new ArgumentOutOfRangeException("ModalViewOwner", "The view needs to be modal");
                }
                _modalViewOwner = value;
            }
        }
    }

	/// <summary>
	/// Base class for initial widget parameters. You most likely do not need to use this.
	/// </summary>
	[EditorBrowsable(EditorBrowsableState.Never)]
    [Serializable]
	public class InitialWidgetParameters : InitialViewParameters
	{
		public string Text { get; set; }

        internal virtual bool Merge(InitialWidgetParameters parameters)
        {
            return false;
        }
         
	}

    /// <summary>
    /// Holds parameters for a button factory.
    /// </summary>
    public class InitialButtonParametersBase : InitialWidgetParameters, ISupportsGesture, ISupportsImage
    {
       
        /// <summary>
        /// Gets or sets whether the button is enabled or not.
        /// </summary>
        public bool Enabled { get; set; }

        /// <summary>
        /// Gets or sets the hint of the button.
        /// </summary>
        public string ToolTip { get; set; }

        /// <summary>
        /// Gets or sets the image displayed on the button.
        /// </summary>
        public virtual ImageSource Image { get; set; }

        /// <summary>
        /// Gets or sets the image displayed on the button when it is small.
        /// </summary>
        /// Only used on standard and toggle buttons.
        public virtual ImageSource SmallImage { get; set; }

        /// <summary>
        /// Sets Image property from a path used to create a BitmapFrame object.
        /// </summary>
        public string ImagePath
        {
            set
            {
                this.SetImageFromPath(value);
            }
        }
 

        /// <summary>
        /// Gets or sets the size of the button.
        /// </summary>
        public ButtonSize Size { get; set; }

        /// <summary>
        /// Gets or sets the Gesture
        /// </summary>
        public virtual KeyGesture Gesture { get; set; }


        public InitialButtonParametersBase()
        {
            this.Enabled = true;
            this.Size = ButtonSize.Large;
        }

        internal override bool Merge(InitialWidgetParameters parameters)
        {
            var candidate = parameters as InitialButtonParametersBase;
            if (candidate == null)
                return false;
            this.ToolTip = candidate.ToolTip ?? this.ToolTip;
            this.Image = candidate.Image ?? this.Image;
            this.Gesture = candidate.Gesture ?? this.Gesture;
            this.Size = candidate.Size;
            return true;
        }
    } 

	/// <summary>
	/// Holds parameters for a button factory.
	/// </summary>
    public class InitialButtonParameters : InitialButtonParametersBase
	{
	    /// <summary>
		/// Gets or sets the delegate that will be called when the button is clicked.
		/// </summary>
		public EventHandler Click { get; set; }

        public bool IsCheckable { get; set; }

        public bool IsChecked { get; set; }

        public bool ShowInputGesture { get; set; } 

        internal override bool Merge(InitialWidgetParameters parameters)
        {
            if (!base.Merge(parameters)) return false;
            var candidate = parameters as InitialButtonParameters;
            if (candidate != null)
            {
                this.ShowInputGesture = candidate.Gesture == null ? this.ShowInputGesture : candidate.ShowInputGesture;              
            } 
            return true;
        }
	} 

  /// <summary>
  /// Holds parameters for a button factory that expose the Command Properties
  /// </summary>
  public class InitialCommandButtonParameters : InitialButtonParametersBase
  {
   
    /// <summary>
    /// Gets or sets the ICommand 
    /// </summary>
    public ICommand Command { get; set; } 

    /// <summary>
    /// Gets or sets the ICommand 
    /// </summary>
    public CommandBinding CommandBinding { get; set; }
 
      /// <summary>
      /// Gets or sets the CommandParameter
      /// </summary>
      /// <remarks>Cannot be used together with KeyGestures due to how InputBindings work</remarks>
    public object CommandParameter { get; set; }

  }



	/// <summary>
	/// Holds parameters for a button factory that automatically wires up a window creation to the Click event.
	/// </summary>
	[Serializable]
	public class InitialShowWindowButtonParameters : InitialButtonParametersBase
	{
        [NonSerialized]
        private ImageSource image;
        [NonSerialized]
        private KeyGesture gesture;
        private LayoutEngine windowLayout = LayoutEngine.Default;

        public override KeyGesture Gesture
        {
            get
            {
                return gesture;
            }
            set
            {
                gesture = value;
            }
        }

        public override ImageSource Image
        {
            get { return image; }
            set
            {
                image = value;
            }
        }
	    /// <summary>
		/// Gets or sets the factory ID for the window to be created.
		/// </summary>
		public string WindowFactoryID { get; set; }
		/// <summary>
		/// Gets or sets the initial parameters of the window.
		/// </summary>
		public InitialWindowParameters InitialParameters { get; set; }

	    public LayoutEngine WindowLayout
	    {
            get { return windowLayout; }
            set
            {
                if (windowLayout != value)
                {
                    windowLayout = value;
                    switch (value)
                    {

                        case LayoutEngine.Tile:
                            this.InitialParameters = new InitialTileItemParameters();
                            break;
                        default: 
                            this.InitialParameters = new InitialWindowParameters();
                            break; 
                    }
                }
            }
	    }
	    public InitialShowWindowButtonParameters()
		{  
            this.WindowLayout = LayoutEngine.Dock; 
		}

    }

 
	/// <summary>
	/// Holds parameters for a factory that creates dropdown buttons.
	/// </summary>
	public class InitialDropdownButtonParameters : InitialWidgetParameters, ISupportsImage
	{
	    /// <summary>
		/// Gets or sets whether the button is enabled or not.
		/// </summary>
		public bool Enabled { get; set; }

		/// <summary>
		/// Gets or sets the hint of the button.
		/// </summary>
		public string ToolTip { get; set; }

		/// <summary>
		/// Gets or sets the image displayed on the button.
		/// </summary>
        public ImageSource Image { get; set; }

        /// <summary>
        /// Gets or sets the small image displayed on the button.
        /// </summary>
        public ImageSource SmallImage { get; set; }

        /// <summary>
        /// Sets Image property from a path used to create a BitmapFrame object.
        /// </summary>
        public string ImagePath
        {
            set
            {
                this.SetImageFromPath(value);
            }
        }

	    public InitialDropdownButtonParameters() { this.Enabled = true; }

        public EventHandler Opening { get; set; }

        public EventHandler Opened { get; set; }

        public ButtonSize Size { get; set; }
    }
    public class InitialDropdownSeparatorParameters : InitialWidgetParameters
    {
        public InitialDropdownSeparatorParameters()
        {
            
        }
    }

    /// <summary>
    /// Holds parameters for a button factory.
    /// </summary>
    public class InitialSplitButtonParameters : InitialDropdownButtonParameters
    {
        /// <summary>
        /// Gets or sets the delegate that will be called when the button is clicked.
        /// </summary>
        public EventHandler Click { get; set; }

        /// <summary>
        /// Gets or sets whether the dropdown button is enabled
        /// </summary>
        public bool IsDropDownEnabled { get; set; }

        public InitialSplitButtonParameters() : base()
        {
            IsDropDownEnabled = true;
        }
    }

    /// <summary>
    /// Holds parameters for a wrap panel factory.
    /// </summary>
    public class InitialPanelParameters : InitialWidgetParameters
    {
        public enum Layout
        {
            Vertical,
            Horizontal
        }

        public Layout Orientation { get; set; }
        public bool Wrap { get; set; }
    }

    public class InitialButtonGroupParameters : InitialWidgetParameters
    {
        // Empty
    }

	/// <summary>
	/// Holds parameters for a factory that creates combo boxes.
	/// </summary>
	public class InitialComboBoxParameters : InitialWidgetParameters
	{
		/// <summary>
		/// Gets or sets whether the button is enabled or not.
		/// </summary>
		public bool Enabled { get; set; }

		/// <summary>
		/// Gets or sets the hint of the button.
		/// </summary>
		public string ToolTip { get; set; }

		/// <summary>
		/// Gets or sets the image displayed on the button.
		/// </summary>
		public ImageSource Image { get; set; }

        /// <summary>
        /// Gets or sets whether the text can be edited in the text box portion of the control.
        /// </summary>
        public bool Editable { get; set; }

		/// <summary>
		/// Gets or sets the ItemsSource of the combo box.
		/// </summary>
		public System.Collections.IEnumerable ItemsSource { get; set; }

		/// <summary>
		/// Gets or sets the delegate that is called when the dropdown opens. You may modify the ItemsSource collection here.
		/// </summary>
		public EventHandler<System.Windows.RoutedEventArgs> DropDownOpened { get; set; }

		public InitialComboBoxParameters() { this.Enabled = true; }
	}

	/// <summary>
	/// Holds parameters for a factory that creates popups.
	/// </summary>
	public class InitialPopupParameters : InitialWidgetParameters, ISupportsImage
	{
	    /// <summary>
		/// Gets or sets the ItemsSource of the popup.
		/// </summary>
		public System.Collections.IEnumerable ItemsSource { get; set; }

		/// <summary>
		/// Gets or sets whether the button is enabled or not.
		/// </summary>
		public bool Enabled { get; set; }

		/// <summary>
		/// Gets or sets the hint of the button.
		/// </summary>
		public string ToolTip { get; set; }

		/// <summary>
		/// Gets or sets the image displayed on the button.
		/// </summary>
		public ImageSource Image { get; set; }

        /// <summary>
        /// Sets Image property from a path used to create a BitmapFrame object.
        /// </summary>
        public string ImagePath
        {
            set
            {
                this.SetImageFromPath(value);
            }
        }

	    public InitialPopupParameters() { this.Enabled = true; }
	}

	/// <summary>
	/// Holds parameters for a factory that creates progress bars.
	/// </summary>
	public class InitialProgressBarParameters : InitialWidgetParameters
	{
		[EditorBrowsable(EditorBrowsableState.Never)]
		public new string Text { get; set; }

		/// <summary>
		/// Gets or sets the minimum value of the progressbar.
		/// </summary>
		public double MinValue { get; set; }

		/// <summary>
		/// Gets or sets the maximum value of the progressbar.
		/// </summary>
		public double MaxValue { get; set; }
	}

	/// <summary>
	/// Holds parameters for a factory that creates labels.
	/// </summary>
	public class InitialLabelParameters : InitialWidgetParameters
	{

	}

	/// <summary>
	/// Holds parameters for a factory that creates labels.
	/// </summary>
	public class InitialStatusLabelParameters : InitialWidgetParameters
	{
		public StatusLevel Level { get; set; }

	    public bool IsVisible { get; set; }

        public InitialStatusLabelParameters() { IsVisible = true; }
	}

	/// <summary>
	/// Holds parameters for a factory that creates separators.
	/// </summary>
	public class InitialSeparatorParameters : InitialWidgetParameters
	{

	}

    public class InitialLauncherBarSeparatorParameters : InitialWidgetParameters
    {
        public InitialLauncherBarSeparatorParameters()
        {
            Margin = new Thickness(3, 0, 3, 0);
        }
        public Thickness Margin { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    //TODO: add meaningful parameters
    public class InitialTitleBarWidgetParameters: InitialWidgetParameters
    {
        
    }

    /// <summary>
    /// 
    /// </summary>
    /// TODO: add meaningful parameters
    public class InitialTitleBarRootParameters: InitialWidgetParameters
    { 
        public Visibility AppIconVisibility { get; set; }
        
    }

	/// <summary>
	/// Holds parameters for a factory that creates tray icons.
	/// </summary>
	public class InitialTrayIconParameters : InitialWidgetParameters
	{
		public Icon Icon { get; set; }
		//public bool Visible { get; set; }
		public System.Windows.Visibility Visibility { get; set; }
		public new string Text { get; set; }
	}

    /// <summary>
    /// Holds parameters for a factory that creates tray icons.
    /// </summary>
    public class InitialTrayMenuItemParameters : InitialSeparatorParameters
    {
        //		public Icon Icon { get; set; }
        //		public System.Windows.Visibility Visibility { get; set; }

        public RoutedEventHandler Click { get; set; }

        public new string Text { get; set; }

        public ImageSource Image { get; set; }
    } 

    public class InitialTrayMenuSeparatorParameters : InitialWidgetParameters
    {
    }


    public class InitialWidgetSearchParameters : InitialWidgetParameters
    {
        static InitialWidgetSearchParameters()
        {
            ExtensionsUtils.RequireAssembly(ExternalAssembly.Infragistics, ExternalAssembly.InfragisticsDataPresenter); 
        }
        public double Width { get; set; }

        public IWidgetViewContainer FavoritesContainer { get; set; }

        public bool HeadersVisible { get; set; }
    }
    
    public interface ISearchableItem
    {
        string DisplayName { get; }
        string Key { get; }
        EventHandler Click { get; }
    }

    public class InitialSearchableItemParameters : InitialWidgetParameters
    {
        public ISearchableItem ItemViewModel { get; set; }
    }


    public class InitialOutlookBarParameters : InitialWidgetParameters, ILockerPassThrough
    {
        static InitialOutlookBarParameters()
        {
            ExtensionsUtils.RequireAssembly(ExternalAssembly.Infragistics, ExternalAssembly.InfragisticsXamOutlookBar);
        }

        public InitialOutlookBarParameters()
        {
            MinWidth = Width = double.NaN;

        }

        public double MinWidth { get; set; }

        public double Width { get; set; }
         
    }

    public class InitialOutlookBarGroupParameters : InitialWidgetParameters, ILockerPassThrough
    {

        public ImageSource LargeImage { get; set; }

        public ImageSource SmallImage { get; set; }
    }

    public interface ISupportsGesture
    {
        KeyGesture Gesture { get; set; }
    }

    public interface ISupportsImage
    {
        ImageSource Image { get; set; }
        string ImagePath { set; }
    }

    public static class SupportsImageExtensions
    {
        public static void SetImageFromPath(this ISupportsImage parameters_, string path)
        {
            if (string.IsNullOrEmpty(path)) return;
            parameters_.Image = BitmapFrame.Create(new Uri(path, UriKind.RelativeOrAbsolute));
        }
    }
}
