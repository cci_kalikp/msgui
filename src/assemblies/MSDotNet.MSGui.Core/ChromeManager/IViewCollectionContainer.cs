﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager
{
    public interface IViewCollectionContainer : IWindowViewContainer
    {
        ObservableCollection<IWindowViewContainer> Children { get; } 
    }
}
