﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent
{
    public interface IWorkspace
    {
        string Name { get; set; }
        ReadOnlyObservableCollection<IWindowViewContainer> Views { get; }
        void Save();
        event EventHandler Closed;
    }

    /// <summary>
    /// Used for saved workspace layout
    /// </summary>
    public interface IAvailableWorkspace
    {
        string Name { get; }
        IEnumerable<string> ViewFactoryIds { get; }
        void Load();
    }
}
