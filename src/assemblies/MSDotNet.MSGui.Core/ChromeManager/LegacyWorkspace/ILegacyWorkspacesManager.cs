﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent;

namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager
{
    public interface ILegacyWorkspacesManager
    {
        /// <summary>
        /// Gets the workspaces if available, null otherwise.
        /// </summary>
        IWorkspaces Workspaces { get; }

        IWorkspace CreateWorkspace(IWindowViewContainer seedView, string name);

        IEnumerable<string> GetViewFactoryIds(string workspace);

        void SaveViews(IEnumerable<IWindowViewContainer> views, string name);

        IEnumerable<string> GetSubLayoutNames();

        void LoadViews(string subLayoutName);
    }
}
