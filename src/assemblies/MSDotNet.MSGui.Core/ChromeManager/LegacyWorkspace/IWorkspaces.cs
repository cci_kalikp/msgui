﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent
{
    public interface IWorkspaces : ICollection<IWorkspace>
    {
        IWorkspace this[int index] { get; }
        event NotifyCollectionChangedEventHandler CollectionChanged;
        IWorkspace CreateWorkspace(IWindowViewContainer seedView_, string name_);
        IEnumerable<IAvailableWorkspace> AvailableWorkspaces(); 
    }
}
