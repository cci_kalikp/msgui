﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Drawing;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager
{
	/// <summary>
	/// Holds a window during its lifetime.
	/// </summary>
    public interface ITileViewContainer : IViewCollectionContainer
	{
        new InitialTileParameters Parameters { get; }  

        double Zoom { get; set; }
	}
}
