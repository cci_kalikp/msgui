﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager
{
	/// <summary>
	/// Specifies the ViewModel class that belongs to the factory.
	/// </summary>
	[Obsolete("Use FactoryOptionsAttribute instead.")]
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
	public class ViewModelAttribute : Attribute
	{
		/// <summary>
		/// The type used as ViewModel. The class must implement the IChromeViewModel interface.
		/// </summary>
		internal Type ViewModelType { get; set; }

		public ViewModelAttribute(Type viewModelType_)
			:base()
		{
			this.ViewModelType = viewModelType_;
		}
	}
}
