﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent
{
	public interface IRibbonGroup : IWidgetViewContainer
	{
	}

	public static class RibbonGroupExtensions
	{
		public static IWidgetViewContainer AddButton(this IRibbonGroup group, string ID, InitialButtonParameters parameters)
		{
			var g = group as IWidgetViewContainer;
			return g.AddWidget(ID, parameters);
		}

    public static IWidgetViewContainer AddButton(this IRibbonGroup group, string ID, InitialCommandButtonParameters parameters)
    {
      var g = group as IWidgetViewContainer;
      return g.AddWidget(ID, parameters);
    }

	}
}
