﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent
{
	public static class Extensions
	{
		public static IRibbon Ribbon(this IChromeManager cm_)
		{
			return cm_ == null ? null : cm_[MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers.ChromeArea.Ribbon] as IRibbon;
		}

		public static ITabbedDock Tabs(this IChromeManager cm_)
		{
			return cm_ == null ? null : cm_[MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers.ChromeArea.TabWell] as ITabbedDock;
		}

		public static IDockTab Tabs(this IChromeManager cm_, string tabName)
		{
			return cm_ == null ? null : cm_[MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers.ChromeArea.TabWell][tabName] as IDockTab;
		}

		public static IStatusBar Statusbar(this IChromeManager cm_)
		{
			return cm_ == null ? null : cm_[MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers.ChromeArea.StatusBar] as IStatusBar;
		}

        public static ITitleBar TitleBar(this IChromeManager cm_)
        {
            return cm_ == null
                       ? null
                       : cm_[MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers.ChromeArea.TitleBar] as ITitleBar;
        }

		public static IWidgetViewContainer SystemTray(this IChromeManager cm_)
		{
			return cm_ == null ? null : cm_[MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers.ChromeArea.SystemTray];
		}

		public static IWidgetViewContainer Launcherbar(this IChromeManager cm_)
		{
			return cm_ == null ? null : cm_[MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers.ChromeArea.LauncherBar];
		}

	    public static IWindowFactoryHolder Singleton(this IWindowFactoryHolder factoryHolder)
	    {
	        var factory = factoryHolder as IWindowFactoryHolderInternal;
	        factory.AddStep((container, state, chain) =>
	            {
	                var p = container.Parameters;
	                if (p != null)
	                {
	                    // Maybe the user code reads the value, who knows.
	                    p.Singleton = true;
	                }

	                var chainResult = chain.Pop()(container, state, chain);

	                if (chainResult && p != null)
	                {
	                    // And maybe they change it to false.
	                    p.Singleton = true;
	                }

	                return chainResult;
	            });

	        return factoryHolder;
	    }
	}
}
