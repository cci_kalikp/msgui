﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent
{
    public interface ITitleBar
    {
        IWidgetViewContainer Left { get; set; }
        IWidgetViewContainer Middle { get; set; }
        IWidgetViewContainer Right { get; set; }
        double Height { get; set; }
    }
}
