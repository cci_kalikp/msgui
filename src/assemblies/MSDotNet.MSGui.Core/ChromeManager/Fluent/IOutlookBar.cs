﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent
{
    public interface IOutlookBar
    {
        IOutlookBarGroup this[string groupName_] { get; }
        IOutlookBarGroup AddGroup(string groupName_, ImageSource largeImage_=null, ImageSource smallImage_=null);
        IOutlookBarGroup ActiveGroup { get; }
        bool IsMinimized { get; set; }
        event EventHandler<ActiveGroupChangedEventArgs> ActiveGroupChanged;


    }

    public class ActiveGroupChangedEventArgs : EventArgs
    {
        public string Old { get; set; }
        public string New { get; set; }
    }
}
