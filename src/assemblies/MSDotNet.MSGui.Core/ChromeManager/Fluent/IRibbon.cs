﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent
{
	public interface IRibbon
	{
		IRibbonTab this[string tabName] { get; }
		IRibbonTab AddTab(string tabName);

        ImageSource Icon { get; set; }

        IRibbonContexts Contexts { get; }
        IRibbonContext AddContext(string id, string caption, Color accentColour);
        void HideAllContexts();

	    event EventHandler<ActiveTabChangedEventArgs> ActiveTabChanged;

	    IQuickAccessToolbar QAT { get; }
	}

    public interface IQuickAccessToolbar
    {
        void AddWidget(IWidgetViewContainer widget, int index);
    }

    public interface IContextMenuExtrasHolder : IWidgetViewContainer
    {
        void AddWidget(InitialWidgetParameters widget, int index);
        int ElementCount { get; }
    }

    public class ActiveTabChangedEventArgs : EventArgs
    {
        public string Old { get; set; }
        public string New { get; set; }
    }
}
