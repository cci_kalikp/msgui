﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent
{
    public interface IRibbonContexts
    {
        IRibbonContext this[string index] { get; }
    }
}
