﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Windows.Media;


namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent
{
	public interface IDockTab// : IWidgetViewContainer
	{
		ReadOnlyObservableCollection<IWindowViewContainer> Windows { get; }

        IWidgetViewContainer HeaderPreItems { get; }
        IWidgetViewContainer HeaderPostItems { get; }

        IWidgetViewContainer Toolbar { get; set; }

        string Title { get; set; }

        object TitleData { get; set; }

        object Tooltip { get; set; }

        Brush AccentBrush { get; set; }

		void Activate();

        void Close();
    }
}
