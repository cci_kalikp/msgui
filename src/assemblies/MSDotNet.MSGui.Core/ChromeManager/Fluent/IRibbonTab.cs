﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent
{
	public interface IRibbonTab// : IWidgetViewContainer
	{
		IRibbonGroup this[string groupName] { get; }
		IRibbonGroup AddGroup(string groupName);
	    void Activate();
	}
}
