﻿
namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent
{
    public interface IRibbonContext
    {
        string Key { get; }
        IContextRibbonTab this[string tabName] { get; }
        IContextRibbonTab AddTab(string tabName);
        void Show(bool hideOthers = true);
        void Hide();
    }
}
