﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent
{
	public interface ITabbedDock
	{
		IDockTab this[string tabName] { get; }

        ReadOnlyObservableCollection<IDockTab> Tabs { get; }
        IDockTab ActiveTab { get; }
		IWidgetViewContainer LeftEdge { get; }
		IWidgetViewContainer RightEdge { get; }

		IDockTab AddTab(string tabName);

        IDockTab AddTab(string tabName, object titleData);

	    event EventHandler ActiveTabChanged;
	}
}
