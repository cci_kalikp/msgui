﻿
namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent
{
    public interface IContextRibbonTab : IRibbonTab
    {
        string Name { get; }
        bool IsVisible { get; set; }
    }
}
