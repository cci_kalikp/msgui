﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Core.ChromeManager
{
    public enum HeaderStructure
    {
        /// <summary>
        /// Icon and caption on the left, extra header items and system buttons aligned to the right
        /// </summary>
        WindowsClassic,

        /// <summary>
        /// Icon on the left, extra header items and system buttons aligned to the right, caption centered across 
        /// the entire window's width, pushed to the left if it would overlap with the header items.
        /// </summary>
        WindowsModern,

        /// <summary>
        /// Icon and extra header items aligned to the left, system buttons to the right, caption centered across 
        /// the entire window's width, pushed to the right if it would overlap with the header items
        /// </summary>
        Office2007
    }
}
