﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/IApplication.cs#7 $
// $Change: 876482 $
// $DateTime: 2014/04/13 22:46:26 $
// $Author: caijin $

using System;

namespace MorganStanley.MSDotNet.MSGui.Core
{
    /// <summary>
    /// Provides access to application specific functions.
    /// </summary>
    public interface IApplication
    {
        /// <summary>
        /// Gets a value indicating the current user
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets a value indicating the version number of this application.
        /// </summary>
        string Version { get; }

        /// <summary>
        /// Gets the command line arguments
        /// </summary>
        /// <remarks>
        /// Maybe this is redundant as they can get them via
        /// Enviroment.CommandLine
        /// </remarks>
        string[] CommandLine { get; }

        /// <summary>
        /// Application startup is complete.
        /// </summary>
        /// <remarks>
        /// The definition of this could be a little hazy. To
        /// me this is that the application has started, the modules
        /// have been loaded and initialized and the shell
        /// has been shown.
        /// </remarks>
        event EventHandler ApplicationLoaded;

        /// <summary>
        /// Quite the current running process.
        /// </summary>
        void Quit();

        /// <summary>
        /// Application has been just closed
        /// </summary>
        /// <remarks>
        /// Can be used from modules and views
        /// </remarks>
        event EventHandler ApplicationClosed;
         
    }
}
