﻿using System;

namespace MorganStanley.MSDotNet.MSGui.Core.Messaging
{

    public interface IMessageWithHttpResponse
    {
        string NewSequence(string publisherId_);

        /// <summary>
        /// ReponseType must implement IMessageResponse
        /// </summary>
        Type ResponseType { get; }

    }

    public interface IMessageResponse
    {
        string Sequence { get; set; }
        string Content { get; set; } 
    }

 

}
