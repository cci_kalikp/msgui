﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Core.Messaging
{
    public enum AutoHookMode
    {
        /// <summary>
        /// 1  will auto-hook all views with each other within the same tab 
        /// 2  will auto-hook up floating views with all other views.
        /// 3  Explicit connections/disconnections will overwrite the auto-hooks
        /// </summary>
        TabAndFloatingMode, 

        /// <summary>
        /// 1 will auto-hook all views with each other within the same 'tab' or 'floating island'
        /// 2 Explicit connections/disconnections will overwrite the auto-hooks
        /// </summary>
        IslandMode,

        /// <summary>
        /// Each view could be attached to a group, and can auto-connect to others within the same group. 
        /// </summary>
        ScreenGroupMode
    }
}
