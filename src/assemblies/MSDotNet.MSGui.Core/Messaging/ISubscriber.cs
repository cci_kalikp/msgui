﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2011 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/Messaging/ISubscriber.cs#6 $
// $Change: 842602 $
// $DateTime: 2013/08/20 14:36:37 $
// $Author: hrechkin $

using System;

namespace MorganStanley.MSDotNet.MSGui.Core.Messaging
{
    /// <summary>
    /// Subscriber for subscribing to messages in View-to-View messaging.
    /// </summary>
    /// <typeparam name="TMessage">The type of the message.</typeparam>
    public interface ISubscriber<out TMessage>
    {
        /// <summary>
        /// Subscribes the specified message handler.
        /// </summary>
        /// <param name="messageHandler">The message handler.
        /// WARNING: will be called from background thread, user code must take care of marshalling back to UI thread as needed.</param>
        /// <exception cref="InvalidOperationException">Thrown when trying to subscribe an unregistered subscriber.</exception>
        [Obsolete]
        /// Use GetObservable().Subscribe instead
        void Subscribe(Action<TMessage> messageHandler);

        IObservable<TMessage> GetObservable();
    }
}
