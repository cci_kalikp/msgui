﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reactive.Linq;
using System.Linq;
using System.Text;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Presentation.Events;
using Microsoft.Practices.Unity;

namespace MorganStanley.MSDotNet.MSGui.Core.Messaging
{
    //TODO: prism ---- what is this for? 
	public class IPCCompositePresentationEvent<TPayload> : CompositePresentationEvent<IPCPayloadWrapper<TPayload>>
	{
		private static IDictionary<string, string> _globalTargeting = IIPCEventAggregatorTargetingExtension.globalTargeting;

		public void Publish(TPayload payload, IDictionary<string, string> targeting)
		{
			IModulePublisher<IPCPayloadWrapper<TPayload>> publisher = null;
			lock (IPCCompositePresentationEventContainerHolder.Publishers)
			{
				if (IPCCompositePresentationEventContainerHolder.Publishers.ContainsKey(typeof(TPayload)))
				{
					publisher =
						IPCCompositePresentationEventContainerHolder.Publishers[typeof(TPayload)].Target as
						IModulePublisher<IPCPayloadWrapper<TPayload>>;
				}
				if (publisher == null)
				{
					publisher =
						IPCCompositePresentationEventContainerHolder.Container.Resolve<ICommunicator>().GetPublisher
							<IPCPayloadWrapper<TPayload>>();
					IPCCompositePresentationEventContainerHolder.Publishers[typeof (TPayload)] = new WeakReference(publisher);
				}
			}
			foreach (KeyValuePair<string, string> keyValuePair in GlobalTargeting)
			{
				if (!targeting.ContainsKey(keyValuePair.Key))
					targeting.Add(keyValuePair);
			}
			publisher.Publish(new IPCPayloadWrapper<TPayload> { Payload = payload, Targeting = targeting });
		}

		internal protected static IDictionary<string, string> GlobalTargeting
		{
			get {
				return _globalTargeting;
			}
			set {
				_globalTargeting = value;
			}
		}

		public SubscriptionToken Subscribe(Action<TPayload> action, Predicate<IPCPayloadWrapper<TPayload>> filter)
		{
			IModuleSubscriber<IPCPayloadWrapper<TPayload>> subscriber = null;
			lock (IPCCompositePresentationEventContainerHolder.Subscribers)
			{
				if (IPCCompositePresentationEventContainerHolder.Subscribers.ContainsKey(typeof(TPayload)))
				{
					subscriber =
						IPCCompositePresentationEventContainerHolder.Subscribers[typeof(TPayload)].Target as
						IModuleSubscriber<IPCPayloadWrapper<TPayload>>;
				}
				if (subscriber == null)
				{
					subscriber =
						IPCCompositePresentationEventContainerHolder.Container.Resolve<ICommunicator>().GetSubscriber
							<IPCPayloadWrapper<TPayload>>();
					IPCCompositePresentationEventContainerHolder.Subscribers[typeof (TPayload)] = new WeakReference(subscriber);
				}
			}

			var observable = from message in subscriber.GetObservable() where filter(message) select message;
		    observable.Subscribe(m =>
		        {
		            action(m.Payload);
		        });

			var token = new SubscriptionToken();
			lock (IPCCompositePresentationEventContainerHolder.SubscriptionTokens)
			{
				IPCCompositePresentationEventContainerHolder.SubscriptionTokens.Add(token, new WeakReference(observable));
			}
			lock (IPCCompositePresentationEventContainerHolder.Actions)
			{
				IPCCompositePresentationEventContainerHolder.Actions.Add(action, new WeakReference(observable));
			}
			return token;
		}

		public override void Unsubscribe(SubscriptionToken token)
		{
			lock (IPCCompositePresentationEventContainerHolder.SubscriptionTokens)
			{
				if (IPCCompositePresentationEventContainerHolder.SubscriptionTokens.ContainsKey(token))
				{
					var d = IPCCompositePresentationEventContainerHolder.SubscriptionTokens[token].Target as IDisposable;
					if (d != null)
					{
						d.Dispose();
						IPCCompositePresentationEventContainerHolder.SubscriptionTokens.Remove(token);
					}
				}
			}
		}

		public override bool Contains(SubscriptionToken token)
		{
			lock (IPCCompositePresentationEventContainerHolder.SubscriptionTokens)
			{
				if (IPCCompositePresentationEventContainerHolder.SubscriptionTokens.ContainsKey(token))
				{
					var d = IPCCompositePresentationEventContainerHolder.SubscriptionTokens[token].Target as IDisposable;
					if (d != null)
					{
						return true;
					}
				}
			}
			return false;
		}

		public bool Contains(Action<TPayload> subscriber)
		{
			lock (IPCCompositePresentationEventContainerHolder.Actions)
			{
				if (IPCCompositePresentationEventContainerHolder.Actions.ContainsKey(subscriber))
				{
					var d = IPCCompositePresentationEventContainerHolder.Actions[subscriber].Target as IDisposable;
					if (d != null)
						return true;
				}
			}
			return false;
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public override bool Contains(Action<IPCPayloadWrapper<TPayload>> subscriber)
		{
			throw new InvalidOperationException();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public override void Publish(IPCPayloadWrapper<TPayload> payload)
		{
			throw new InvalidOperationException();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public override SubscriptionToken Subscribe(Action<IPCPayloadWrapper<TPayload>> action, ThreadOption threadOption, bool keepSubscriberReferenceAlive, Predicate<IPCPayloadWrapper<TPayload>> filter)
		{
			throw new InvalidOperationException();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public override void Unsubscribe(Action<IPCPayloadWrapper<TPayload>> subscriber)
		{
			throw new InvalidOperationException();
		}
	}

	internal static class IPCCompositePresentationEventContainerHolder
	{
		internal static IUnityContainer Container { get; set; }
		internal static IDictionary<Type, WeakReference> Publishers = new Dictionary<Type, WeakReference>();
		internal static IDictionary<Type, WeakReference> Subscribers = new Dictionary<Type, WeakReference>();
		internal static IDictionary<SubscriptionToken, WeakReference> SubscriptionTokens = new Dictionary<SubscriptionToken, WeakReference>();
		internal static IDictionary<object, WeakReference> Actions = new Dictionary<object, WeakReference>();
	}

	public class IPCPayloadWrapper<TPayload>
	{
		public TPayload Payload { get; set; }
		public IDictionary<string, string> Targeting { get; set; }
	}
}
