﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2011 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/Messaging/IModulePublisher.cs#6 $
// $Change: 859739 $
// $DateTime: 2013/12/20 12:05:19 $
// $Author: anbuy $

using System;

namespace MorganStanley.MSDotNet.MSGui.Core.Messaging
{
    public interface IModulePublisher<in TMessage>
    {
        /**
         * Will publish to applications within the same environment (i.e. QA, Dev, Prod...)
         */
        void Publish(TMessage message);

        
        void Publish(TMessage message, CommunicationTargetFilter targetFilter = CommunicationTargetFilter.All, params string[] location);
        
        void PublishRelativeToCurrent(TMessage message, CommunicationTargetFilter targetFilter); 
       
    }
}
