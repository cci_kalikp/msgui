﻿using System;
using System.ComponentModel;

namespace MorganStanley.MSDotNet.MSGui.Core.Messaging
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct)]
    public class MessageAttribute: System.Attribute
    {
        private readonly string _id;
        public string Id { get { return _id; } }

        public string TypeName { get; set; }

        [DefaultValue("0.0")]
        public string Version { get;  set; }
        
        public string Grn { get;  set; }

        private string _description;
        public string Description
        {
            get { return _description; }
            set { _description = value; }

        }

        public MessageAttribute(string id)
        {
            _id = id;
        }

        [DefaultValue(false)]
        public bool StrongTyped { get; set; }
    }
}
