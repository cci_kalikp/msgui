﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2011 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/Messaging/IAdapterService.cs#6 $
// $Change: 877117 $
// $DateTime: 2014/04/16 11:25:13 $
// $Author: hrechkin $

using System;

namespace MorganStanley.MSDotNet.MSGui.Core.Messaging
{
    /// <summary>
    /// Adapter service
    /// </summary>
    public interface IAdapterService
    {
        /// <summary>
        /// Adds the adapter.
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <typeparam name="TDestination">The type of the destination.</typeparam>
        /// <param name="adapter">The adapter.</param>
        void AddAdapter<TSource, TDestination>(Func<TSource, TDestination> adapter);

        /// <summary>
        /// Removes the adapter.
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <typeparam name="TDestination">The type of the destination.</typeparam>
        /// <param name="adapter">The adapter_.</param>
        /// <returns><c>true</c> if the adapter was removed.</returns>
        bool RemoveAdapter<TSource, TDestination>(Func<TSource, TDestination> adapter);

        /// <summary>
        /// Applies the adapter chain.
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <typeparam name="TDestination">The type of the destination.</typeparam>
        /// <param name="sourceMessage">The source message.</param>
        /// <returns>The adapted message.</returns>
        TDestination ApplyAdapterChain<TSource, TDestination>(TSource sourceMessage);

        /// <summary>
        /// Checks to see if message of sourceType can be adapted to destinationType.
        /// </summary>
        /// <param name="sourceType"></param>
        /// <param name="destinationType"></param>
        /// <returns>True if message of sourceType can be adapted to destinationType</returns>
        bool IsAdaptationPossible(Type sourceType, Type destinationType);
    }
}
