﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2011 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/Messaging/IPublisher.cs#6 $
// $Change: 842641 $
// $DateTime: 2013/08/20 19:49:01 $
// $Author: hrechkin $

using System;

namespace MorganStanley.MSDotNet.MSGui.Core.Messaging
{
    /// <summary>
    /// Publisher for publishing View-to-View messages
    /// </summary>
    /// <typeparam name="TMessage">The type of the message.</typeparam>
    public interface IPublisher<in TMessage>
    {

        /// <summary>
        /// Publishes the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// 
        /// <param name="locationFilter"></param>
        /// <param name="location"></param>
        /// <exception cref="InvalidOperationException">Thrown when trying to publish on an unregistered publisher.</exception>
        void Publish(TMessage message, CommunicationTargetFilter locationFilter = CommunicationTargetFilter.All,
                     params string[] location);
    }
}
