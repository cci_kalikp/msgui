﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Core.Messaging
{
    public enum CommunicationTargetFilter
    {
        User, UserHost, UserApp, UserHostApp, 
        All, Machine, Application, Tab, MachineApplication, MachineInstance
    }

    public enum AppEnvironment
    {
        QA, UAT, Prod, Dev, All, Current
    }
}
