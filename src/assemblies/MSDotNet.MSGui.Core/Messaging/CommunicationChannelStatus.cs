﻿using System;

namespace MorganStanley.MSDotNet.MSGui.Core.Messaging
{
    /// <summary>
    /// Indicates the initialization status of Kraken communication channel (IPC or IMC).
    /// </summary>
    [Serializable]
    public enum CommunicationChannelStatus
    {
        /// <summary>
        /// Indicates that the communication channel is disabled and not used.
        /// </summary>
        Disabled,

        /// <summary>
        /// Indicates that the communication channel is optional. MSDesktop platform will attempt to initialize it,
        /// however the application will start and run if the initialization of the channel fails.
        /// This is the default behavior for isolated subsystem hosting process.
        /// </summary>
        Optional,

        /// <summary>
        /// Indicates that the communication channel is required. MSDesktop platform will attempt to initialize it,
        /// and the application will fail start if the initialization of the channel fails. 
        /// This is the default behavior for the host applications.
        /// </summary>
        Required
    }
}
