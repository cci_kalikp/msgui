﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2011 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/Messaging/IEventRegistrator.cs#15 $
// $Change: 879056 $
// $DateTime: 2014/04/29 21:11:56 $
// $Author: hrechkin $

using System;
using System.Collections.Generic;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Core.Messaging
{
    /// <summary>
    /// Register publishers and subscribers to event message types
    /// </summary>
    public interface IEventRegistrator
    {
        /// <summary>
        /// Registers the subscriber.
        /// </summary>
        /// <typeparam name="TMessage">The type of the message.</typeparam>
        /// <param name="viewContainer">The view container ID.</param>
        /// <returns>The message type specific subscriber</returns>
        ISubscriber<TMessage> RegisterSubscriber<TMessage>(IViewContainerBase viewContainer);

        /// <summary>
        /// Registers the subscriber.
        /// </summary>
        /// <typeparam name="TMessage">The type of the message.</typeparam>
        /// <param name="viewContainer">The view container ID.</param>
        /// <param name="receiveLastMessage">If <c>true</c> the subscriber will receive the last sent message of the publisher once connected to it.</param>
        /// <returns>The message type specific subscriber</returns>
        ISubscriber<TMessage> RegisterSubscriber<TMessage>(IViewContainerBase viewContainer, bool receiveLastMessage);

        /// <summary>
        /// Unregisters the subscriber.
        /// </summary>
        /// <typeparam name="TMessage">The type of the message.</typeparam>
        /// <param name="subscriber">The subscriber.</param>
        /// <returns><c>true</c> if view container was unregistered</returns>
        [Obsolete]
        bool UnregisterSubscriber<TMessage>(ISubscriber<TMessage> subscriber);

        /// <summary>
        /// Unregisters the subscriber.
        /// </summary>
        /// <typeparam name="TMessage">The type of the message.</typeparam>
        /// <param name="viewContainer">The view container</param>
        /// <returns><c>true</c> if view container was unregistered</returns>
        bool UnregisterSubscriber<TMessage>(IViewContainerBase viewContainer);
        //bool UnregisterSubscriber<TMessage>(string viweId_);
        /// <summary>
        /// Registers the publisher.
        /// </summary>
        /// <typeparam name="TMessage">The type of the message.</typeparam>
        /// <param name="viewContainer">The view container ID.</param>
        /// <returns>The message type specific publisher</returns>
        IPublisher<TMessage> RegisterPublisher<TMessage>(IViewContainerBase viewContainer);

        /// <summary>
        /// Unregisters the publisher.
        /// </summary>
        /// <typeparam name="TMessage">The type of the message.</typeparam>
        /// <param name="publisher">The publisher.</param>
        /// <returns><c>true</c> if view container was unregistered</returns>
        [Obsolete]
        bool UnregisterPublisher<TMessage>(IPublisher<TMessage> publisher);

        /// <summary>
        /// Unregisters the publisher.
        /// </summary>
        /// <typeparam name="TMessage">The type of the message.</typeparam>
        /// <param name="viewContainer">The publisher.</param>
        /// <returns><c>true</c> if view container was unregistered</returns>
        bool UnregisterPublisher<TMessage>(IViewContainerBase viewContainer);

        /// <summary>
        /// Gets the compatible publishers.
        /// </summary>
        /// <typeparam name="TMessage">The type of the message.</typeparam>
        /// <returns>IDs of all Publishers compatibel with the message type</returns>
        IEnumerable<string> GetCompatiblePublishers<TMessage>();

        /// <summary>
        /// Gets the compatible subscribers.
        /// </summary>
        /// <typeparam name="TMessage">The type of the message.</typeparam>
        /// <returns>IDs of All subscribers compatibel with the message type</returns>
        IEnumerable<string> GetCompatibleSubscribers<TMessage>();

        /// <summary>
        /// Gets the subscribed types.
        /// </summary>
        /// <returns>All types that were subscribed to</returns>
        ICollection<Type> GetSubscribedTypes();

        /// <summary>
        /// Gets the published types.
        /// </summary>
        /// <returns>All types that were subscribed to</returns>
        ICollection<Type> GetPublishedTypes();

        /// <summary>
        /// Gets the registered publisher.
        /// </summary>
        /// <typeparam name="TMessage">The type of the message.</typeparam>
        /// <param name="viewContainer">The view container.</param>
        /// <returns>The publisher previously registered for the view container and message type or <c>null</c> if nothing was registered.</returns>
        IPublisher<TMessage> GetRegisteredPublisher<TMessage>(IViewContainerBase viewContainer);

        /// <summary>
        /// Gets the registered subscriber.
        /// </summary>
        /// <typeparam name="TMessage">The type of the message.</typeparam>
        /// <param name="viewContainer">The view container.</param>
        /// <returns>The subscriber previously registered for the view container and message type or <c>null</c> if nothing was registered.</returns>
        ISubscriber<TMessage> GetRegisteredSubscriber<TMessage>(IViewContainerBase viewContainer);

        /// <summary>
        /// Connect a publisher and a subscriber
        /// </summary>
        /// <typeparam name="TMessage1">Published message type</typeparam>
        /// <typeparam name="TMessage2">Subscribed message type</typeparam>
        /// <param name="publishContainer"></param>
        /// <param name="subscribeContainer"></param>
        void Connect<TMessage1, TMessage2>(IViewContainerBase publishContainer, IViewContainerBase subscribeContainer);
        void Connect<TMessage1, TMessage2>(string publishContainer, string subscribeContainer);

        /// <summary>
        /// Disconnect a publisher and subscriber
        /// </summary>
        /// <typeparam name="TMessage1"></typeparam>
        /// <typeparam name="TMessage2"></typeparam>
        /// <param name="publishContainer"></param>
        /// <param name="subscribeContainer"></param>
        void Disconnect<TMessage1, TMessage2>(IViewContainerBase publishContainer, IViewContainerBase subscribeContainer);
        void Disconnect<TMessage1, TMessage2>(string publishViewId, string subscribeViewId);

        /// <summary>
        /// Registers a cross-process marshalign subscriber for the given view.
        /// This method is temporary on this interface and should nto be used by your application.
        /// </summary>
        /// <typeparam name="TMessage"></typeparam>
        /// <param name="viewContainer"></param>
        /// <param name="addinHost"></param>
        /// <returns></returns>
        // TODO (hrechkin): addinHost here is of type IXProcAddInHost. Properly slice the interface between Core and Impl.
        ISubscriber<TMessage> RegisterMarshalingSubscriber<TMessage>(IViewContainerBase viewContainer, object addinHost);

        /// <summary>
        /// Registers a cross-process marshaling publisher for the given view.
        /// This method is temporary on this interface and should nto be used by your application.
        /// </summary>
        /// <typeparam name="TMessage"></typeparam>
        /// <param name="viewContainer"></param>
        /// <param name="addinHost"></param>
        /// <returns></returns>
        // TODO (hrechkin): addinHost here is of type IXProcAddInHost. Properly slice the interface between Core and Impl.
        IPublisher<TMessage> RegisterMarshalingPublisher<TMessage>(IViewContainerBase viewContainer, object addinHost);

        IAdapterService AdapterService { get; }
    }
}
