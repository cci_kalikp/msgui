﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2011 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/Messaging/IEventRouter.cs#5 $
// $Change: 842602 $
// $DateTime: 2013/08/20 14:36:37 $
// $Author: hrechkin $

using System;

namespace MorganStanley.MSDotNet.MSGui.Core.Messaging
{
  /// <summary>
  /// Event router interface
  /// </summary>
  public interface IEventRouter
  {
    /// <summary>
    /// Connects the specified publisher to the subscriber.
    /// </summary>
    /// <typeparam name="TMessage1">The type of the published message.</typeparam>
    /// <typeparam name="TMessage2">The type of the subscribed message.</typeparam>
    /// <param name="publisher">The publisher.</param>
    /// <param name="subscriber">The subscriber.</param>
    void Connect<TMessage1, TMessage2>(IPublisher<TMessage1> publisher, ISubscriber<TMessage2> subscriber);
    
    /// <summary>
    /// Disconnects the specified publisher and subscriber.
    /// </summary>
    /// <typeparam name="TMessage1">The type of the published message.</typeparam>
    /// <typeparam name="TMessage2">The type of the subscribed message.</typeparam>
    /// <param name="publisher">The publisher.</param>
    /// <param name="subscriber">The subscriber.</param>
    void Disconnect<TMessage1, TMessage2>(IPublisher<TMessage1> publisher, ISubscriber<TMessage2> subscriber);
  }
}
