﻿
namespace MorganStanley.MSDotNet.MSGui.Core.Messaging
{
    public interface ICommunicatorSettings
    {
        CommunicationChannelStatus ImcStatus { get; }
        CommunicationChannelStatus IpcStatus { get; }
        int HostProcessId { get; }
    }
}
