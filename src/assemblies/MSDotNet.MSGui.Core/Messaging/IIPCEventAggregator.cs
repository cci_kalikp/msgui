﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Composite.Events;

namespace MorganStanley.MSDotNet.MSGui.Core.Messaging
{
    public interface IMSDesktopEventAggregator
    {
        TEvent GetEvent<TEvent>();
    }

    //TODO: why this is public?
	public interface IIPCEventAggregator : IMSDesktopEventAggregator
	{
	    new TEventType GetEvent<TEventType>();
	}

	public static class IIPCEventAggregatorTargetingExtension
	{
		internal static IDictionary<string, string> globalTargeting;

		public static void AddIPCTargeting(this IIPCEventAggregator framework, IDictionary<string, string> targeting)
		{
			globalTargeting = targeting;
		}
	}
}
