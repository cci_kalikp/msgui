﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2011 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/Messaging/IModuleSubscribe.cs#3 $
// $Change: 766181 $
// $DateTime: 2011/12/05 04:59:27 $
// $Author: smulovic $

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Core.Messaging
{
    public interface IModuleSubscribe<T>
    {
        IObservable<T> GetObservable();
    }
}
