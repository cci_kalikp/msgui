﻿using System;
using System.Collections.Generic;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Core.Messaging
{
    public interface IV2VConnectionManager
    {
        void SignUpAutoHook(IWindowViewContainer viewContainer, Action<IEnumerable<IWindowViewContainer>> hookupCallback,
                            Action<IEnumerable<IWindowViewContainer>> unhookedCallback);

        void SignOffAutoHook(IWindowViewContainer viewContainer);

        AutoHookMode AutoHookMode { set; }
    }
}
