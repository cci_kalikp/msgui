﻿using System;
using System.Collections.Generic;

namespace MorganStanley.MSDotNet.MSGui.Core.Messaging
{
    public interface ICommunicator
    {
        IAdapterService AdapterService { get; }
        IEnumerable<Type> GetChannels();
        IModulePublisher<TMessage> GetPublisher<TMessage>();
        IModuleSubscriber<TMessage> GetSubscriber<TMessage>();
        void EnableIpcImc(AppEnvironment env);
        void DisableIpcImc();
    }
}
