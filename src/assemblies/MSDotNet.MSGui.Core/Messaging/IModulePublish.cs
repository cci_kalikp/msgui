﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2011 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/Messaging/IModulePublish.cs#1 $
// $Change: 766053 $
// $DateTime: 2011/12/02 08:33:44 $
// $Author: smulovic $

namespace MorganStanley.MSDotNet.MSGui.Core.Messaging
{
    public interface IModulePublish<T>
    {
        void Publish(T value);
    }
}
