﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
// using MorganStanley.MSDotNet.CafGUI.Toolkit.Messaging;

namespace MorganStanley.MSDotNet.MSGui.Core.Messaging
{
    public interface ILocationHolder
    {
        CafGuiApplicationInfoProxy Application { get; set; }
        string Tab { get; set; }
       
    }

    /// <summary>
    /// Use this class to decouple the dependency of CalGui.Toolkit and ILocationHolder
    /// </summary>
    public sealed class CafGuiApplicationInfoProxy
    {
        public string Name { get; set; }
        public string Host { get; set; }
        public string Instance { get; set; }
        public string UserName { get; set; }

        public CafGuiApplicationInfoProxy()
        {
        }

        internal CafGuiApplicationInfoProxy(ApplicationInfo other)
        {
            Host = other.Host;
            Instance = other.Instance;
            Name = other.Name;
            UserName = other.User;
        }
    }

    public sealed class ApplicationInfo
    {
        private readonly string _host;
        private readonly string _user;
        private readonly string _desktop;
        private readonly string _name;
        private readonly string _instance;

        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationInfo"/> class.
        /// </summary>
        /// <param name="host">The host name.</param>
        /// <param name="user">The user name.</param>
        /// <param name="desktop">The desktop id.</param>
        /// <param name="name">The application name in the desktop.</param>
        /// <param name="instance">The application instance id.</param>
        public ApplicationInfo(string host, string user, string desktop, string name, string instance)
        {
            Guard.ArgumentNotNullOrEmptyString(host, "host");
            Guard.ArgumentNotNullOrEmptyString(user, "user");
            Guard.ArgumentNotNullOrEmptyString(desktop, "desktop");
            Guard.ArgumentNotNullOrEmptyString(name, "name");
            Guard.ArgumentNotNull(instance, "instance");

            _host = host;
            _user = user;
            _desktop = desktop;
            _name = name;
            _instance = instance;
        }

        /// <summary>
        /// Gets the host name.
        /// </summary>
        public string Host
        {
            get
            {
                return _host;
            }
        }

        /// <summary>
        /// Gets the user name.
        /// </summary>
        public string User
        {
            get
            {
                return _user;
            }
        }

        /// <summary>
        /// Gets the desktop id.
        /// </summary>
        public string Desktop
        {
            get
            {
                return _desktop;
            }
        }

        /// <summary>
        /// Gets the application name.
        /// </summary>
        public string Name
        {
            get
            {
                return _name;
            }
        }

        /// <summary>
        /// Gets the application instance id.
        /// </summary>
        public string Instance
        {
            get
            {
                /*
                if (_instance.Contains("_")) 
                {
                    return _instance.Split('_')[1];
                }*/

                return _instance;
            }
        }

        /// <summary>
        /// Serves as a hash function for a particular type. 
        /// </summary>
        /// <returns>
        /// A hash code for the current <see cref="T:System.Object" />.
        /// </returns>
        public override int GetHashCode()
        {
            return (_host + _user + _desktop + _name).GetHashCode();
        }

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object" /> is equal to the current <see cref="T:System.Object" />.
        /// </summary>
        /// <returns>
        /// true if the specified <see cref="T:System.Object" /> is equal to the current <see cref="T:System.Object" />; otherwise, false.
        /// </returns>
        /// <param name="rhs">The <see cref="T:System.Object" /> to compare with the current <see cref="T:System.Object" />. </param>
        /// <exception cref="T:System.NullReferenceException">The <paramref name="rhs" /> parameter is null.</exception>
        public override bool Equals(object rhs)
        {
            if (rhs == null)
            {
                return false;
            }
            if (rhs == this)
            {
                return true;
            }

            var providedApplicationInfo = rhs as ApplicationInfo;
            if (providedApplicationInfo != null)
            {
                return
                    Host.Equals(providedApplicationInfo.Host) &&
                    User.Equals(providedApplicationInfo.User) &&
                    Desktop.Equals(providedApplicationInfo.Desktop) &&
                    Name.Equals(providedApplicationInfo.Name) &&
                    Instance.Equals(providedApplicationInfo.Instance);
            }

            return false;
        }

        /// <summary>
        /// Returns a <see cref="T:System.String" /> that represents the current <see cref="T:System.Object" />.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String" /> that represents the current <see cref="T:System.Object" />.
        /// </returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("/");
            sb.Append(_host).Append('/');
            sb.Append(_user).Append('/');
            sb.Append(_desktop).Append('/');
            sb.Append(_name).Append('/');
            sb.Append(_instance);
            return sb.ToString();
        }
    }

    internal static class LocationHolderExtension
    {
        /// <summary>
        /// to check if the tow process instance belongs to the same applicaiont instance.  For isolation usage.
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="otherInstance"></param>
        /// <returns></returns>
        internal static bool OfSameApplicationInstance(string instance, string otherInstance)
        {
            var host = instance.Split('_')[0];
            var otherHost = otherInstance.Split('_')[0];
            return host == otherHost;
        }


        /// <summary>
        /// sender and receiver are subsystems belongs to the same host or subsystem and host system;
        /// </summary>
        /// <param name="senderInstance"></param>
        /// <param name="receiverInstance"></param>
        /// <returns></returns>
        internal static bool OfSameHostSystemButNotSameInstance(string senderInstance, string receiverInstance)
        {
            if (senderInstance == receiverInstance)
            {
                // Local message - not handled through remote means
                return false;
            }
             
         
            var messageParts = senderInstance.Split('_');
            if (messageParts.Length < 2)
            {
                messageParts = new string[2]{messageParts[0], string.Empty};
            }
            var applicationParts = receiverInstance.Split('_');
            if (applicationParts.Length < 2)
            {
                applicationParts = new string[2]{applicationParts[0], string.Empty};
            }
            
            // Compare the parts. The first parts have to be equal (host process ID), but second ones don't (isolated process ID for isolated process, and empty for host process, or host process for legacy system)
            if (messageParts[0] == applicationParts[0] && messageParts[1] != applicationParts[1])
            {
                return true;
            }
             
            return false;
        }
         
        public static bool BelongsToLocation(this ILocationHolder locationHolder, CommunicationTargetFilter targetFilterLocation, params string[] location)
        {
            
            switch (targetFilterLocation)
            {
                case CommunicationTargetFilter.All:
                    return true;
                case CommunicationTargetFilter.Machine:
                    return locationHolder.Application.Host == location[0];
                case CommunicationTargetFilter.Application:
                    return  locationHolder.Application.Name == location[0];
                case CommunicationTargetFilter.Tab:
                    return locationHolder.Tab == null || locationHolder.Tab == location[0];

                case CommunicationTargetFilter.MachineApplication:
                    return 
                           (locationHolder.Application.Host == location[0] &&
                            locationHolder.Application.Name == location[1]);

                case CommunicationTargetFilter.MachineInstance:
                    return
                        (locationHolder.Application.Host == location[0] &&
                         OfSameApplicationInstance(location[1], locationHolder.Application.Instance));


                case CommunicationTargetFilter.User:
                    return  locationHolder.Application.UserName == location[0];
                case CommunicationTargetFilter.UserHost:
                    return 
                           (locationHolder.Application.UserName == location[0] &&
                            locationHolder.Application.Host == location[1]);
                
                case CommunicationTargetFilter.UserApp:
                    return 
                           (locationHolder.Application.UserName == location[0] &&
                            locationHolder.Application.Name == location[1]);

                case CommunicationTargetFilter.UserHostApp:
                    return 
                           (locationHolder.Application.UserName == location[0] &&
                            locationHolder.Application.Host == location[1] &&
                            locationHolder.Application.Name == location[2]);



                default:
                    throw new ApplicationException("Unknow target locaion");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="locationHolder"></param>
        /// <param name="targetFilterLocation"></param>
        /// <param name="location">null to match all</param>
        /// <returns></returns>

        public static bool BelongsToLocation(this ILocationHolder locationHolder, CommunicationTargetFilter targetFilterLocation, ILocationHolder location)
        {

            if (location == null)
                return true;

            switch (targetFilterLocation)
            {
                case CommunicationTargetFilter.All:
                    return true;

                case CommunicationTargetFilter.Machine:
                    return locationHolder.Application == null || locationHolder.Application.Host == location.Application.Host;
                case CommunicationTargetFilter.Application:
                    return locationHolder.Application == null || locationHolder.Application.Name == location.Application.Name;
                case CommunicationTargetFilter.Tab:
                    return locationHolder.Tab == null || locationHolder.Tab == location.Tab;

                case CommunicationTargetFilter.MachineApplication:
                    return locationHolder.Application == null || (locationHolder.Application.Host == location.Application.Host && locationHolder.Application.Name == location.Application.Name);

                case CommunicationTargetFilter.MachineInstance:
                    return locationHolder.Application == null || (locationHolder.Application.Host == location.Application.Host && locationHolder.Application.Instance == location.Application.Instance);


                default:
                    throw new ApplicationException("Unknow target locaion");
            }
        }
    }
}
