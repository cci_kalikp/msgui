﻿
namespace MorganStanley.MSDotNet.MSGui.Core
{
    public enum ModuleStatus
    {
        NotLoaded,
        Loaded,
        Initialized,
        BlockedByNoEntitlementsInfo,
        BlockedByEntitlements,
        DependentModuleBlocked,
        TypeResolutionFailed,
        Exceptioned,
        ProcessExited
    }
}
