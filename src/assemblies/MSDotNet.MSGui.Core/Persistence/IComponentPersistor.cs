﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace MorganStanley.MSDotNet.MSGui.Core.Persistence
{
    public interface IComponentPersistor:IPersistable
    {
        LoadStage AutoLoadStage { get; set; }
        LoadStage AutoLoadStageResolved { get; }
        AutoSaveMode AutoSave { get; set; }
        AutoSaveMode AutoSaveResolved { get; }
        PersistenceExceptionPolicy ExceptionPolicy { get; set; }
        PersistenceExceptionPolicy ExceptionPolicyResolved { get; }
        IComponentPersistor WrapComponent(object component_, IEnumerable<string> namespaces_, Configuration configuration_);
        XDocument LoadedState { get; }
        XDocument SavedState { get; }
    }

    public interface IRootComponentPersistor<out TEnumerator> : IComponentPersistor where TEnumerator: IComponentPersistorEnumerator, new()
    {
        TEnumerator PersistorEnumerator { get; }
        bool CachePersistors { get; set; }
        string RootElementName { get; }
    }

    public enum AutoSaveMode
    {
        Default,
        True,
        False
    }
    public enum LoadStage
    {
        Default = 0,
        /// <summary>
        /// This context is active when the Initialized event is raised on the window.
        /// </summary>
        AfterInitialized = 1, 

        /// <summary>
        /// This context is active when the Loaded event is raised, so the window is loaded.
        /// </summary>
        AfterLoaded = 2,

        /// <summary>
        /// This context is active when the IsVisibleChanged event is raised for the first time on the window.
        /// </summary>
        AfterShown = 3,

        /// <summary>
        ///  Load manually using code
        /// </summary> 
        Later = 4
    }
}
