﻿using System;

namespace MorganStanley.MSDotNet.MSGui.Core.Persistence
{
    internal interface IDelayInitializationModule
    {
        event EventHandler<EventArgs> ModuleInitialized;
    }
}
