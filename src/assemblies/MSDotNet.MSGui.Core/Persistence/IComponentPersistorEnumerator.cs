﻿using System.Collections.Generic;

namespace MorganStanley.MSDotNet.MSGui.Core.Persistence
{
    public interface IComponentPersistorEnumerator
    {
        /// <summary>
        /// Visits the specified object.
        /// </summary>
        /// <param name="rootObject_">The object to visit.</param>
        IEnumerable<IComponentPersistor> GetPersistors(object rootObject_);

        IEnumerable<IComponentPersistor> GetComponentPersistors(object rootObject_, object componentObject_);
    }
}