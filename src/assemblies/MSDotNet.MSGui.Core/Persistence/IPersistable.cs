﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/Persistence/IPersistable.cs#4 $
// $Change: 859007 $
// $DateTime: 2013/12/16 05:38:47 $
// $Author: caijin $

using System.Xml.Linq;

namespace MorganStanley.MSDotNet.MSGui.Core.Persistence
{
    public interface IPersistable
    {
        void LoadState(XDocument state_);
        XDocument SaveState();
        string PersistorId { get; }
    }
}
