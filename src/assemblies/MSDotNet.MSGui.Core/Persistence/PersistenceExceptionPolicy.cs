﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Core.Persistence
{
    /// <summary>
    /// Enumeration for the possible exception handling policies for the <see cref="IPersistable.LoadState" /> and <see cref="IPersistable.SaveState" /> method-calls.
    /// </summary>
    public enum PersistenceExceptionPolicy
    {
        
        /// <summary>
        /// Inherit from parent or Ingore if no parent configuration
        /// </summary>
        Default = 0,

        /// <summary>
        /// Exceptions thrown when the <see cref="IPersistable.LoadState" /> or <see cref="IPersistable.SaveState" /> is called will not be caught at all.
        /// </summary>
        Rethrow = 1,

        /// <summary>
        /// Exceptions thrown when the <see cref="IPersistable.LoadState" /> or <see cref="IPersistable.SaveState" /> is called will be caught and traced.
        /// </summary>
        Ignore = 2,

    }
}
