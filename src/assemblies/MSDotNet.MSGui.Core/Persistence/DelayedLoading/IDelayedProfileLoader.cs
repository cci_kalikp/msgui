﻿using System.Threading;
using System.Xml.Linq;

namespace MorganStanley.MSDotNet.MSGui.Core.Persistence.DelayedLoading
{
    public interface IDelayedProfileLoader
    {
        void ReloadCurrentProfile();
        void LoadConnections(XDocument element);
        ManualResetEvent PersistanceInitialized { get; }
    }
}
