﻿using System;
using System.Collections.Specialized;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;

namespace MorganStanley.MSDotNet.MSGui.Core.Persistence.DelayedLoading
{
    internal static class DelayedProfileLoadingExtensionPoints
    {
        private static bool delayLoadProfile;
        public static bool DelayLoadProfile
        {
            get { return delayLoadProfile; }
            set
            {
                if (delayLoadProfile == true && value == false)
                {
                    InvokeAfterDelayLoadedProfile();
                }
                delayLoadProfile = value; 
            }
        }

        public static event EventHandler AfterDelayLoadedProfile;
        public static event EventHandler<BeforeModuleInitializedEventArgs> BeforeModuleInitialized;

        public static event EventHandler<BeforeLoadView2ViewConnectionEventArgs> BeforeLoadView2ViewConnection;

        public static event EventHandler<AllModulesLoadedEventArgs> AllModulesLoaded;

        public static event EventHandler<PeerApplicationJointEventArgs> PeerApplicationJoined;

        private static void InvokeAfterDelayLoadedProfile()
        {
            var handler = AfterDelayLoadedProfile;
            if (handler != null)
            {
                handler(null, EventArgs.Empty);
            }
        }

        public static void InvokeAllModulesLoaded(AllModulesLoadedEventArgs e_)
        {
            var handler = AllModulesLoaded;
            if (handler != null) handler(null, e_);
        }

        public static void InvokeBeforeLoadView2ViewConnection(BeforeLoadView2ViewConnectionEventArgs e_)
        {
            EventHandler<BeforeLoadView2ViewConnectionEventArgs> handler = BeforeLoadView2ViewConnection;
            if (handler != null) handler(null, e_);
        }

        public static void InvokeModuleInfoLoaded(BeforeModuleInitializedEventArgs e_)
        {
            EventHandler<BeforeModuleInitializedEventArgs> handler = BeforeModuleInitialized;
            if (handler != null) handler(null, e_);
        }

        public static void InvokePeerApplicationJoin(ApplicationInfo application_)
        {
            var handler = PeerApplicationJoined;
            if (handler != null)
            {
                handler(null, new PeerApplicationJointEventArgs(application_));
            }
        }


    }

    internal class AllModulesLoadedEventArgs : EventArgs
    {
        public IModuleLoadInfos Modules { get; set; }
    }

    internal class BeforeLoadView2ViewConnectionEventArgs : EventArgs
    {
        public bool Cancel { get; set; }
        public XDocument Connection { get; set; }
    }

    internal class BeforeModuleInitializedEventArgs : EventArgs
    {
        public string ModuleName { get; set; }
        public IMSDesktopModule Module { get; set; }
        public StringDictionary ExtraInformation { get; set; }
    }

    internal class PeerApplicationJointEventArgs:EventArgs
    {
        public PeerApplicationJointEventArgs(ApplicationInfo application_)
        {
            this.Application = application_;
        }
        public ApplicationInfo Application { get; private set; }
    }

}
