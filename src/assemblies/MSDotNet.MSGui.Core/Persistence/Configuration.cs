﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Markup;
using System.Xml.Linq;

namespace MorganStanley.MSDotNet.MSGui.Core.Persistence
{
    [ContentProperty("Persistors")]
    public class Configuration : DependencyObject, IPersistable
    {
        public Configuration()
        {
            Persistors = new List<IComponentPersistor>();
        }
        public List<IComponentPersistor> Persistors { get; private set; } 

        public PersistenceExceptionPolicy ExceptionPolicy
        {
            get { return (PersistenceExceptionPolicy)GetValue(ExceptionPolicyProperty); }
            set { SetValue(ExceptionPolicyProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ExceptionPolicy.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ExceptionPolicyProperty =
            DependencyProperty.Register("ExceptionPolicy", typeof(PersistenceExceptionPolicy), typeof(Configuration), new PropertyMetadata(PersistenceExceptionPolicy.Default));


        public string PersistenceName
        {
            get { return (string)GetValue(PersistenceNameProperty); }
            set { SetValue(PersistenceNameProperty, value); }
        }

        // Using a DependencyProperty as the backing store for PersistenceName.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PersistenceNameProperty =
            DependencyProperty.Register("PersistenceName", typeof(string), typeof(Configuration), new UIPropertyMetadata(null));


        public AutoSaveMode AutoSave
        {
            get { return (AutoSaveMode)GetValue(AutoSaveProperty); }
            set { SetValue(AutoSaveProperty, value); }
        }

        // Using a DependencyProperty as the backing store for AutoSave.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AutoSaveProperty =
            DependencyProperty.Register("AutoSave", typeof(AutoSaveMode), typeof(Configuration), new UIPropertyMetadata(AutoSaveMode.Default));



        public LoadStage AutoLoadStage
        {
            get { return (LoadStage)GetValue(AutoLoadStageProperty); }
            set { SetValue(AutoLoadStageProperty, value); }
        }

        // Using a DependencyProperty as the backing store for AutoLoadStage.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AutoLoadStageProperty =
            DependencyProperty.Register("AutoLoadStage", typeof(LoadStage), typeof(Configuration), new UIPropertyMetadata(LoadStage.Default));



        public bool SuppressChildrenPersistence
        {
            get { return (bool)GetValue(SuppressChildrenPersistenceProperty); }
            set { SetValue(SuppressChildrenPersistenceProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SuppressPersistence.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SuppressChildrenPersistenceProperty =
            DependencyProperty.Register("SuppressChildrenPersistence", typeof(bool), typeof(Configuration), new UIPropertyMetadata(false));

        

        public void LoadState(XDocument state_)
        {
            if (state_ == null || state_.Root == null) return;
            var persistenceNameAttr = state_.Root.Attribute(PersistenceNameProperty.Name);
            if (persistenceNameAttr != null && !string.IsNullOrEmpty(persistenceNameAttr.Value))
            {
                PersistenceName = persistenceNameAttr.Value;
            }
            var exceptionPolicyAttr = state_.Root.Attribute(ExceptionPolicyProperty.Name);
            if (exceptionPolicyAttr != null && !string.IsNullOrEmpty(exceptionPolicyAttr.Value))
            {
                PersistenceExceptionPolicy exceptionPolicy;
                 if (Enum.TryParse(exceptionPolicyAttr.Value, true, out exceptionPolicy))
                 {
                     ExceptionPolicy = exceptionPolicy;
                 }
            }

            var autoSaveAttr = state_.Root.Attribute(AutoSaveProperty.Name);
            if (autoSaveAttr != null && !string.IsNullOrEmpty(autoSaveAttr.Value))
            {
                AutoSaveMode autoSave;
                if (Enum.TryParse(autoSaveAttr.Value, true, out autoSave))
                {
                    AutoSave = autoSave;
                }
            }

            var autoLoadStageAttr = state_.Root.Attribute(AutoLoadStageProperty.Name);
            if (autoLoadStageAttr != null && !string.IsNullOrEmpty(autoLoadStageAttr.Value))
            {
                LoadStage autoLoadStage;
                if (Enum.TryParse(autoLoadStageAttr.Value, true, out autoLoadStage))
                {
                    AutoLoadStage = autoLoadStage;
                }
            }
            var suppressPersistenceAttr = state_.Root.Attribute(SuppressChildrenPersistenceProperty.Name);
            if (suppressPersistenceAttr != null && suppressPersistenceAttr.Value.ToLower() == "true")
            {
                this.SuppressChildrenPersistence = true;
            }
            foreach (var persistorEl in state_.Root.Descendants("Persistor"))
            {
                XElement persistorDefEl = persistorEl.Descendants().FirstOrDefault();
                if (persistorDefEl == null) continue; 
                XDocument doc = new XDocument(persistorDefEl);
                Type persistorType = Type.GetType(persistorEl.Attribute("Type").Value);
                var persistor = PersistenceExtensions.Restore(doc, persistorType) as IComponentPersistor;
                if (persistor != null)
                {
                   Persistors.Add(persistor);
                }
            }
        }

        public XDocument SaveState()
        {
            XElement root = new XElement(PersistorId);
            root.SetAttributeValue(PersistenceNameProperty.Name, this.PersistenceName);
            root.SetAttributeValue(ExceptionPolicyProperty.Name, this.ExceptionPolicy.ToString());
            root.SetAttributeValue(AutoSaveProperty.Name, this.AutoSave);
            root.SetAttributeValue(AutoLoadStageProperty.Name, this.AutoLoadStage);
            root.SetAttributeValue(SuppressChildrenPersistenceProperty.Name,  this.SuppressChildrenPersistence);
            if (this.Persistors.Count > 0)
            { 
                foreach (var persistor in Persistors)
                {
                    XDocument persistorState = PersistenceExtensions.StoreRealType(persistor);
                    XElement persistorStateEl = new XElement("Persistor");
                    persistorStateEl.SetAttributeValue("Type", persistor.GetType().AssemblyQualifiedName);
                    persistorStateEl.Add(persistorState.Root);
                    root.Add(persistorStateEl);
                }
            }
            return new XDocument(root);
        }

        public string PersistorId
        {
            get { return "PersistenceConfiguration"; }
        }
    }
}
