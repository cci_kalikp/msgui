﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/Persistence/PersistenceExtensions.cs#7 $
// $Change: 878794 $
// $DateTime: 2014/04/28 16:31:00 $
// $Author: smulovic $

using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Windows;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Core
{
    public static class PersistenceExtensions
    {
        /// <summary>
        /// Get the global persistence key for the object; to be used with m_persistenceService.PersistGlobalItems(new [] { PersistenceExtensions.ConvertGlobalKey(obj) }); 
        /// </summary>
        /// <param name="obj">Object to get the key for</param>
        /// <returns>Global persistence key</returns>
        public static string ConvertGlobalKey(object obj)
        {
            if (obj != null)
            {
                return "ViewModelBasedOptionPagePersistor." + obj.GetType().FullName;
            }
            return string.Empty;
        }

        /// <summary>
        /// Get the global persistence key for the object; to be used with m_persistenceService.PersistGlobalItems(new [] { PersistenceExtensions.ConvertGlobalKey &gt; T %lt;() }); 
        /// </summary>
        /// <typeparam name="T">Type to get the key for</typeparam>
        /// <returns>Global persistence key</returns>
        public static string ConvertGlobalKey<T>()
        {
            return "ViewModelBasedOptionPagePersistor." + typeof (T).FullName;
        }

        /// <summary>
        /// Deserialize an object from XDocument.
        /// </summary>
        /// <typeparam name="T">Object's type</typeparam>
        /// <param name="state_">Serialized object.</param>
        /// <returns>An object deserialized from XDocument</returns>
        /// <remarks>
        /// This function does not access the persistence storage. It is a deserialization helper.
        /// </remarks>
        public static T Restore<T>(XDocument state_) where T : new()
        {
            if (state_ == null)
                return new T();
            var s = new XmlSerializer(typeof (T));
            var ms = new MemoryStream();
            state_.Save(ms);
            ms.Position = 0;
            var reader = XmlReader.Create(ms, new XmlReaderSettings {IgnoreWhitespace = true});
            var result = (T) s.Deserialize(reader);
            reader.Close();
            return result;
        }

        public static object Restore(XDocument state_, Type objType_)
        {
            if (state_ == null)
                return null;
            var s = new XmlSerializer(objType_);
            var ms = new MemoryStream();
            state_.Save(ms);
            ms.Position = 0;
            var reader = XmlReader.Create(ms, new XmlReaderSettings {IgnoreWhitespace = true});
            var result = s.Deserialize(reader);
            reader.Close();
            return result;
        }

        /// <summary>
        /// Serialize an object of given type to XDocument.
        /// </summary>
        /// <typeparam name="T">Object's type</typeparam>
        /// <param name="source_">an object to serialize</param>
        /// <returns>XDocument containing serialized object</returns>
        /// <remarks>
        /// This function doesn't do anything with actual persistence storage. It only serializes an object to xml.
        /// </remarks>
        public static XDocument Store<T>(T source_)
        {
            XDocument target = new XDocument();
            XmlSerializer s = new XmlSerializer(typeof (T));
            XmlWriter writer = target.CreateWriter();
            s.Serialize(writer, source_);
            writer.Close();

            return target;
        }

        public static XDocument StoreRealType(object source_)
        {
            XDocument target = new XDocument();
            XmlSerializer s = new XmlSerializer(source_.GetType());
            XmlWriter writer = target.CreateWriter();
            s.Serialize(writer, source_);
            writer.Close();

            return target;
        }

        public static XDocument SerializeDataContext<T>(IViewContainer viewcontainer_)
        {
            if (viewcontainer_.Content as FrameworkElement != null &&
                ((FrameworkElement) viewcontainer_.Content).DataContext is T)
            {
                return Store((T) (((FrameworkElement) viewcontainer_.Content).DataContext));
            }
            return null;
        } 
    }


}
