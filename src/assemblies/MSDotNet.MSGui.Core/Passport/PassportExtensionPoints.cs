﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Unity;

namespace MorganStanley.MSDotNet.MSGui.Core.Passport
{
    internal static class PassportExtensionPoints
    {
        public static event EventHandler BeforeStart;

        public static void InvokeBeforeStart()
        {
            var handler = BeforeStart;
            if (handler != null) handler(null, null);
        }

        public static event EventHandler CreatingPassportApplication;

        public static void InvokeCreatingPassportApplication(IUnityContainer container)
        {
            var handler = CreatingPassportApplication;
            if (handler != null) handler(container, null);
        }
    }
}
