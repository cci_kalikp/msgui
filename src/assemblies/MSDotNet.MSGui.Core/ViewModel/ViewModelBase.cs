﻿using System.ComponentModel;

namespace MorganStanley.MSDotNet.MSGui.Core.ViewModel
{
    public abstract  class ViewModelBase:INotifyPropertyChanged
    {		
        /// <summary>
        /// Raised when a property on this object has a new value.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises this object's PropertyChanged event.
        /// </summary>
        /// <param name="propertyName_">The property that has a new value.</param>
        protected virtual void OnPropertyChanged(string propertyName_)
        { 
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                var e = new PropertyChangedEventArgs(propertyName_);
                handler(this, e);
            }
        }
    }
}
