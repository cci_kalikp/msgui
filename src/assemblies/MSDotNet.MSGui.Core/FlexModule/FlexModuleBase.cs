﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/FlexModule/FlexModuleBase.cs#7 $
// $Change: 861162 $
// $DateTime: 2014/01/08 12:20:01 $
// $Author: anbuy $

using System;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Core.FlexModule
{
    /// <summary>
    /// Base abstract class for Flex Module implementations
    /// </summary>
    public abstract class FlexModuleBase 
    {

        #region Protected Fields
		/// <summary>
		/// The chrome registry.
		/// </summary>
		protected readonly IChromeRegistry m_chromeRegistry;

		/// <summary>
		/// The chrome manager.
		/// </summary>
		protected readonly IChromeManager m_chromeManager;
        #endregion

        #region Properties

        private Uri m_swfPath;
        /// <summary>
        /// Gets or sets the SWF path.
        /// </summary>
        /// <value>
        /// The SWF path.
        /// </value>
        public virtual Uri SwfPath
        {
            get { return m_swfPath; }
            protected set { m_swfPath = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="FlexModuleBase"/> class.
        /// </summary>
		/// <param name="chromeRegistry_">The chrome registry.</param>
		/// <param name="chromeManager_">The chrome manager.</param>
        /// <param name="swfPath_">The SWF path.</param>
		protected FlexModuleBase(IChromeRegistry chromeRegistry_, IChromeManager chromeManager_, Uri swfPath_)
        {
			m_chromeRegistry = chromeRegistry_;
			m_chromeManager = chromeManager_;
            m_swfPath = swfPath_;
        }

                /// <summary>
        /// Initializes a new instance of the <see cref="FlexModuleBase"/> class.
        /// </summary>
		/// <param name="chromeRegistry_">The chrome registry.</param>
		/// <param name="chromeManager_">The chrome manager.</param>
		protected FlexModuleBase(IChromeRegistry chromeRegistry_, IChromeManager chromeManager_) :
			this(chromeRegistry_, chromeManager_, null)
        {
        }

        #endregion

        #region IModule Members

        public abstract void Initialize();

        #endregion

        #region Communication With Flex

        /// <summary>
        /// Invoked when Flex code calls a method on ExternalInterface
        /// </summary>
        /// <param name="functionName_">Name of the called function</param>
        /// <param name="arguments_">Array of parameters</param>
        /// <returns></returns>
        protected abstract object CallFromFlex(string functionName_, object[] arguments_);

        /// <summary>
        /// Call Flex functions from .Net
        /// </summary>
        /// <param name="functionName_">Name of the function to be called</param>
        /// <param name="arguments_">Array of parameters</param>
        /// <returns></returns>
        protected abstract object CallToFlex(string functionName_, params object[] arguments_);

        #endregion

    }
}
