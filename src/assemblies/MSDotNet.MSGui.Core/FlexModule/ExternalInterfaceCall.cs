/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/FlexModule/ExternalInterfaceCall.cs#7 $
// $Change: 872846 $
// $DateTime: 2014/03/24 21:33:08 $
// $Author: caijin $
// based on http://livedocs.adobe.com/flex/3/html/help.html?content=19_External_Interface_10.html

using System;
using System.Collections;
using System.Text;

namespace Flash.External
{
    /// <summary>
    /// Value object containing information about an ExternalInterface call
    /// sent between a .NET application and a Shockwave Flash object.
    /// </summary>
    public class ExternalInterfaceCall
    {
        #region Private Fields

        private string _functionName;
        private ArrayList _arguments;

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new ExternalInterfaceCall instance with the specified 
        /// function name.
        /// </summary>
        /// <param name="functionName">The name of the function as provided
        /// by Flash Player</param>
        public ExternalInterfaceCall(string functionName)
        {
            _functionName = functionName;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// The name of the function call provided by Flash Player
        /// </summary>
        public string FunctionName
        {
            get { return _functionName; }
        }

        /// <summary>
        /// The function parameters associated with this function call.
        /// </summary>
        public object[] Arguments
        {
            get { return _arguments == null ? new object[0] : (object[])_arguments.ToArray(typeof(object)); }
        }

        #endregion

        #region Public Methods

        public override string ToString()
        {
            StringBuilder result = new StringBuilder();
            result.AppendFormat("Function Name: {0}{1}", _functionName, Environment.NewLine);
            if (_arguments != null && _arguments.Count > 0)
            {
                result.AppendFormat("Arguments:{0}", Environment.NewLine);
                foreach (object arg in _arguments)
                {
                    result.AppendFormat("\t{0}{1}", arg, Environment.NewLine);
                }
            }
            return result.ToString();
        }

        #endregion

        #region Internal Methods

        internal void AddArgument(object argument)
        {
            if (_arguments == null)
            {
                _arguments = new ArrayList();
            }
            _arguments.Add(argument);
        }

        #endregion
    }
}