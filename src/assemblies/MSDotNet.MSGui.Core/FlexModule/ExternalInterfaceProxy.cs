/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/FlexModule/ExternalInterfaceProxy.cs#7 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $
// based on http://livedocs.adobe.com/flex/3/html/help.html?content=19_External_Interface_10.html

using System;
using System.Runtime.InteropServices;
using AxShockwaveFlashObjects;

namespace Flash.External
{
    /// <summary>
    /// Facilitates External Interface communication between a .NET application and a Shockwave
    /// Flash ActiveX control by providing an abstraction layer over the XML-serialized data format
    /// used by Flash Player for ExternalInterface communication.
    /// This class provides the Call method for calling ActionScript functions and raises 
    /// the ExternalInterfaceCall event when calls come from ActionScript.
    /// </summary>
    internal class ExternalInterfaceProxy : IDisposable
    {
        #region Private Fields

        private AxShockwaveFlash _flashControl;

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new ExternalInterfaceProxy for the specified Shockwave Flash ActiveX control.
        /// </summary>
        /// <param name="flashControl">The Shockwave Flash ActiveX control with whom communication
        /// is managed by this proxy.</param>
        public ExternalInterfaceProxy(AxShockwaveFlash flashControl)
        {
            _flashControl = flashControl;
            _flashControl.FlashCall += new _IShockwaveFlashEvents_FlashCallEventHandler(_flashControl_FlashCall);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Calls the ActionScript function which is registered as a callback method with the
        /// ActionScript ExternalInterface class.
        /// </summary>
        /// <param name="functionName">The function name registered with the ExternalInterface class
        /// corresponding to the ActionScript function that is to be called</param>
        /// <param name="arguments">Additional arguments, if any, to pass to the ActionScript function.</param>
        /// <returns>The result returned by the ActionScript function, or null if no result is returned.</returns>
        /// <exception cref="System.Runtime.InteropServices.COMException">Thrown when there is an error
        /// calling the method on Flash Player. For instance, this exception is raised if the
        /// specified function name is not registered as a callable function with the ExternalInterface
        /// class; it is also raised if the ActionScript method throws an Error.</exception>
        public object Call(string functionName, params object[] arguments)
        {
            try
            {
                string request = ExternalInterfaceSerializer.EncodeInvoke(functionName, arguments);
                string response = _flashControl.CallFunction(request);
                object result = ExternalInterfaceSerializer.DecodeResult(response);
                return result;
            }
            catch (COMException)
            {
                throw;
            }
        }

        #endregion

        #region Events

        /// <summary>
        /// Raised when an External Interface call is made from Flash Player.
        /// </summary>
        public event ExternalInterfaceCallEventHandler ExternalInterfaceCall;

        /// <summary>
        /// Raises the ExternalInterfaceCall event, indicating that a call has come from Flash Player.
        /// </summary>
        /// <param name="e">The event arguments related to the event being raised.</param>
        protected virtual object OnExternalInterfaceCall(ExternalInterfaceCallEventArgs e)
        {
            if (ExternalInterfaceCall != null)
            {
                return ExternalInterfaceCall(this, e);
            }
            return null;
        }

        #endregion

        #region Event Handling

        /// <summary>
        /// Called when Flash Player raises the FlashCallEvent (when an External Interface call
        /// is made by ActionScript)
        /// </summary>
        /// <param name="sender">The object raising the event</param>
        /// <param name="e">The arguments for the event</param>
        private void _flashControl_FlashCall(object sender, _IShockwaveFlashEvents_FlashCallEvent e)
        {
            ExternalInterfaceCall functionCall = ExternalInterfaceSerializer.DecodeInvoke(e.request);
            ExternalInterfaceCallEventArgs eventArgs = new ExternalInterfaceCallEventArgs(functionCall);
            object response = OnExternalInterfaceCall(eventArgs);
            _flashControl.SetReturnValue(ExternalInterfaceSerializer.EncodeResult(response));
        }

        #endregion

        public void Dispose()
        {
            _flashControl.FlashCall -= _flashControl_FlashCall;
        }
    }
}