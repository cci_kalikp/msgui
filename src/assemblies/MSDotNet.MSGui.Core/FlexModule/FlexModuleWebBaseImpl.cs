﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/FlexModule/FlexModuleWebBaseImpl.cs#9 $
// $Change: 874505 $
// $DateTime: 2014/04/02 03:13:42 $
// $Author: caijin $

using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Core.FlexModule
{
    /// <summary>
    /// Base implementation for Flex modules using the web browser
    /// </summary>
    public abstract class FlexModuleWebBaseImpl : FlexModuleBase
    {

        #region Private Fields

        private readonly ObjectForScripting m_objectForScripting;
        private WebBrowser m_browser;

        #endregion

        #region Properties

        public override Uri SwfPath
        {
            get
            {
                return base.SwfPath;
            }

            protected set
            {
                var newPath = ConvertAndNavigate(value);
                if (newPath != null)
                {
                    base.SwfPath = newPath;
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to use an external window.
        /// </summary>
        /// <value>
        ///   <c>true</c> if using external window; otherwise, <c>false</c>.
        /// </value>
        public bool UseExternalWindow { get; protected set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="FlexModuleWebBaseImpl"/> class.
        /// </summary>
		/// <param name="chromeRegistry_">The chrome registry.</param>
		/// <param name="chromeManager_">The chrome manager.</param>
		/// <param name="swfPath_">The SWF path.</param>
        /// <param name="useExternalWindow_">if set to <c>true</c> uses external window.</param>
		protected FlexModuleWebBaseImpl(IChromeRegistry chromeRegistry_, IChromeManager chromeManager_, Uri swfPath_, bool useExternalWindow_) :
            base(chromeRegistry_, chromeManager_, swfPath_)
        {
            UseExternalWindow = useExternalWindow_;
            m_objectForScripting = new ObjectForScripting(this);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FlexModuleWebBaseImpl"/> class.
        /// </summary>
		/// <param name="chromeRegistry_">The chrome registry.</param>
		/// <param name="chromeManager_">The chrome manager.</param>
		/// <param name="swfPath_">The SWF path.</param>
		protected FlexModuleWebBaseImpl(IChromeRegistry chromeRegistry_, IChromeManager chromeManager_, Uri swfPath_) :
            this(chromeRegistry_, chromeManager_, swfPath_, false)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FlexModuleWebBaseImpl"/> class.
        /// </summary>
		/// <param name="chromeRegistry_">The chrome registry.</param>
		/// <param name="chromeManager_">The chrome manager.</param>
		/// <param name="useExternalWindow_">if set to <c>true</c> uses external window.</param>
		protected FlexModuleWebBaseImpl(IChromeRegistry chromeRegistry_, IChromeManager chromeManager_, bool useExternalWindow_) :
            this(chromeRegistry_, chromeManager_, null, useExternalWindow_)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FlexModuleWebBaseImpl"/> class.
        /// </summary>
		/// <param name="chromeRegistry_">The chrome registry.</param>
		/// <param name="chromeManager_">The chrome manager.</param>
		protected FlexModuleWebBaseImpl(IChromeRegistry chromeRegistry_, IChromeManager chromeManager_) :
            this(chromeRegistry_, chromeManager_, null, false)
        {
        }

        #endregion

        #region View Creation

        /// <summary>
        /// Create the Module's content. Adds the Flash Player control to the module and initializes it.
        /// </summary>
        /// <param name="viewcontainer_">The view's container</param>
        protected virtual bool CreateView(IWindowViewContainer viewcontainer_, XDocument state)
        {
            if (SwfPath != null)
            {
                if (!UseExternalWindow)
                {
                    m_browser = new WebBrowser();
                    m_browser.Navigated += BrowserNavigated;
                    viewcontainer_.Parameters.Width = m_browser.Width + 30;
					viewcontainer_.Parameters.Height = m_browser.Height + 50;  // add extra space for decoration
                    viewcontainer_.Content = m_browser;
                }
                ConvertAndNavigate(SwfPath);
            }
			return true;
        }

        /// <summary>
        /// Converts the SWF path to use a web address instead of network drives and navigate to it.
        /// </summary>
        /// <param name="swfPath_">The SWF path.</param>
        /// <returns>The converted path</returns>
        private Uri ConvertAndNavigate(Uri swfPath_)
        {
            if (swfPath_ != null)
            {
                if (swfPath_.Host == "v")
                {
                    swfPath_ = new Uri("http://nfsweb/v" + swfPath_.PathAndQuery);
                }
                else if (swfPath_.Host == "ms")
                {
                    swfPath_ = new Uri("http://afsweb/ms" + swfPath_.PathAndQuery);
                } 
                var hostFile = GetHostingFile(swfPath_, UseExternalWindow);
                if (UseExternalWindow)
                {
                    if (!File.Exists(hostFile))
                    {
                        throw new FileNotFoundException(hostFile);
                    }
                    new Process
                        {
                            StartInfo = { Arguments = swfPath_.ToString(), FileName = hostFile },
                            EnableRaisingEvents = false
                        }.Start();
                }
                else
                {
                    m_browser.NavigateToString(hostFile);
                }
            }
            return swfPath_;
        }

        /// <summary>
        /// Handles the Navigated event of the Browser control.
        /// </summary>
        /// <param name="sender_">The source of the event.</param>
        /// <param name="e_">The <see cref="System.Windows.Navigation.NavigationEventArgs"/> instance containing the event data.</param>
        private void BrowserNavigated(object sender_, NavigationEventArgs e_)
        {
            m_browser.AllowDrop = false;
            m_browser.ContextMenu = null;
            m_browser.ObjectForScripting = m_objectForScripting;
        }

        /// <summary>
        /// Gets a hosting file
        /// </summary>
        /// <param name="swfPath_">Path to the SWF file to generate the host file for</param>
        /// <param name="getHta_"><code>true</code> if the file needs to be saved</param>
        /// <returns>The path of the saved file or its contents depending of the <code>getHTA_</code> argument</returns>
        private static string GetHostingFile(Uri swfPath_, bool getHta_ = false)
        {
            var assy = Assembly.GetExecutingAssembly();
            if (assy == null || assy.Location == null)
            {
                throw new ApplicationException("Could not get executing Assembly's path");
            }
            return getHta_
                // ReSharper disable AssignNullToNotNullAttribute
                ? Path.Combine(Path.Combine(Path.GetDirectoryName(assy.Location), "FlexModule"), "FlexModule.hta")
                // ReSharper restore AssignNullToNotNullAttribute
                : Properties.Resources.swf.Replace("${swf}", swfPath_.ToString());
        }

        #endregion

        #region Communication With Flex

        protected override object CallToFlex(string functionName_, params object[] arguments_)
        {
            return m_objectForScripting.CallToFlex(functionName_, arguments_);
        }

        #endregion

        #region Helper Classes

        /// <summary>
        /// Object used to interact with the embedded browser
        /// </summary>
        [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
        [ComVisible(true)]
        public class ObjectForScripting
        {

            #region Private Fields

            private readonly FlexModuleWebBaseImpl m_flexModuleWebBaseImpl;
            private bool m_appReady = false;
            private bool m_swfReady = false;
            private readonly JavaScriptSerializer m_serializer = new JavaScriptSerializer();

            #endregion

            #region Constructor

            /// <summary>
            /// Initializes a new instance of the <see cref="ObjectForScripting"/> class.
            /// </summary>
            /// <param name="flexModuleWebBaseImpl_">The flex module web base implementation.</param>
            public ObjectForScripting(FlexModuleWebBaseImpl flexModuleWebBaseImpl_)
            {
                m_flexModuleWebBaseImpl = flexModuleWebBaseImpl_;
            }

            #endregion

            /// <summary>
            /// Called from JavaScript/ActionScript
            /// </summary>
            /// <param name="args_">Function name and arguments as JSON text</param>
            /// <returns>The return value</returns>
            public string Callback(string args_)
            {
                if (args_ == null)
                {
                    return null;
                }
                var args = m_serializer.Deserialize<object[]>(args_);
                var func = args[0] as string;
                switch (func)
                {
                    case "MSGUI_isReady":
                        return m_serializer.Serialize(IsReady());
                    case "MSGUI_setSWFIsReady":
                        SetSwfIsReady();
                        return null;
                    case "MSGUI_useDirectCalls":
                        return m_serializer.Serialize(true);
                    default:
                        return m_serializer.Serialize(m_flexModuleWebBaseImpl.CallFromFlex(func, args[1] as object[]));
                }
            }

            #region Methods called by Flash Player

            /// <summary>
            /// Called to check if the page has initialized and JavaScript is available
            /// </summary>
            /// <returns></returns>
            private bool IsReady()
            {
                return m_appReady;
            }

            /// <summary>
            /// Called to notify the page that the SWF has set it's callbacks
            /// </summary>
            private void SetSwfIsReady()
            {
                // record that the SWF has registered it's functions (i.e. that C#
                // can safely call the ActionScript functions)
                m_swfReady = true;
            }

            #endregion


            /// <summary>
            /// Calls to flex.
            /// </summary>
            /// <param name="functionName_">The function name.</param>
            /// <param name="arguments_">The arguments.</param>
            /// <returns>the object returned by the AS3 call</returns>
            internal object CallToFlex(string functionName_, object[] arguments_)
            {
                var jsArgs = m_serializer.Serialize(new object[] { functionName_, arguments_ });
                var result = m_flexModuleWebBaseImpl.m_browser.InvokeScript("MSGUI_callToFlex", jsArgs) as string;
                return m_flexModuleWebBaseImpl.m_browser != null && m_swfReady && result != null
                    ? m_serializer.Deserialize<object>(result)
                    : null;
            }
        }

        #endregion

    }

}
