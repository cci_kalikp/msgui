/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/FlexModule/ExternalInterfaceCallEventArgs.cs#7 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $
// based on http://livedocs.adobe.com/flex/3/html/help.html?content=19_External_Interface_10.html

namespace Flash.External
{
    public delegate object ExternalInterfaceCallEventHandler(object sender, ExternalInterfaceCallEventArgs e);

    /// <summary>
    /// Event arguments for the ExternalInterfaceCallEventHandler.
    /// </summary>
    public class ExternalInterfaceCallEventArgs : System.EventArgs
    {
        private ExternalInterfaceCall _functionCall;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExternalInterfaceCallEventArgs"/> class.
        /// </summary>
        /// <param name="functionCall">The function call.</param>
        public ExternalInterfaceCallEventArgs(ExternalInterfaceCall functionCall)
            : base()
        {
            _functionCall = functionCall;
        }

        /// <summary>
        /// Gets the function call.
        /// </summary>
        public ExternalInterfaceCall FunctionCall
        {
            get { return _functionCall; }
        }
    }
}