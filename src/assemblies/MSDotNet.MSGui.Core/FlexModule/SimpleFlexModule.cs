﻿using System;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Core.Persistence.DelayedLoading;

namespace MorganStanley.MSDotNet.MSGui.Core.FlexModule
{
    public class SimpleFlexModule : FlexModuleFlashPlayerBaseImpl
    {
        private const String UrlAttribute = "url";
        private const String LocationStringAttribute = "showButtonLocation";
        private const String ButtonTextAttribute = "showButtonText";
        private const String ButtonIdAttribute = "buttonId";

        public String ButtonText { get; set; }
        public String LocationString { get; set; }
        public string ButtonId { get; set; }

        static SimpleFlexModule()
        {
            DelayedProfileLoadingExtensionPoints.BeforeModuleInitialized += OnBeforeModuleInitialized;
        }

        private static void OnBeforeModuleInitialized(object sender, BeforeModuleInitializedEventArgs beforeModuleInitializedEventArgs)
        {
            if (beforeModuleInitializedEventArgs.ExtraInformation != null && beforeModuleInitializedEventArgs.ExtraInformation.Count > 0 &&
                beforeModuleInitializedEventArgs.Module is SimpleFlexModule)
            {
                var module = beforeModuleInitializedEventArgs.Module as SimpleFlexModule;
                module.SwfPath = new Uri(PathUtilities.ReplaceEnvironmentVariables(beforeModuleInitializedEventArgs.ExtraInformation[UrlAttribute]));
                module.ButtonText = beforeModuleInitializedEventArgs.ExtraInformation[ButtonTextAttribute];
                module.LocationString = beforeModuleInitializedEventArgs.ExtraInformation[LocationStringAttribute];
                module.ButtonId = beforeModuleInitializedEventArgs.ExtraInformation[ButtonIdAttribute];
            }
        }

        public SimpleFlexModule(IChromeRegistry chromeRegistry, IChromeManager chromeManager) 
            : base(chromeRegistry, chromeManager)
        {
        }

        public override void Initialize()
        {
            var browserViewId = "SimpleFlexModule." + SwfPath + ".BrowserViewId";
            var browserViewButtonId = string.IsNullOrEmpty(ButtonId) ? 
                ("SimpleFlexModule." + SwfPath + ".BrowserViewButtonId") : ButtonId;

            m_chromeRegistry.RegisterWindowFactory(browserViewId, CreateView);
            m_chromeRegistry.RegisterWidgetFactory(browserViewButtonId, m_chromeManager.ShowWindowButtonFactory());

#pragma warning disable 612,618
            // this will not be necessary once the Flex API exists
            m_chromeManager.PlaceWidget(browserViewButtonId, LocationString, new InitialShowWindowButtonParameters
#pragma warning restore 612,618
            {
                Text = ButtonText,
                WindowFactoryID = browserViewId,
                InitialParameters =
                    new InitialWindowParameters()
            });
        }

        protected override bool CreateView(IWindowViewContainer viewcontainer, XDocument state)
        {
            if (base.CreateView(viewcontainer, state))
            {
                viewcontainer.Title = ButtonText;
                return true;
            }
            return false;
        }

        protected override object CallFromFlex(string functionName, object[] arguments)
        {
            return null;
        }
    }
}
