﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/FlexModule/FlexModuleFlashPlayerBaseImpl.cs#12 $
// $Change: 862682 $
// $DateTime: 2014/01/17 16:11:04 $
// $Author: hrechkin $

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Flash.External;
using Microsoft.Practices.Composite.Modularity;
using System.Diagnostics.CodeAnalysis;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Core.FlexModule
{
    /// <summary>
    /// Base implementation for Flex modules using the Flash ActiveX control
    /// </summary>
    public abstract class FlexModuleFlashPlayerBaseImpl : FlexModuleBase, IMSDesktopModule
    {
        #region Private Fields

        private readonly Dictionary<FlexModuleFlashPlayerView, FlashPlayerInstanceInfo> m_instanceInfos = 
            new Dictionary<FlexModuleFlashPlayerView, FlashPlayerInstanceInfo>();

        private class FlashPlayerInstanceInfo
        {
            public bool AppReady { get; set; }
            public bool SwfReady { get; set; }
            public ExternalInterfaceProxy FlashProxy { get; set; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="FlexModuleFlashPlayerBaseImpl"/> class.
        /// </summary>
		/// <param name="chromeRegistry_">The chrome registry.</param>
		/// <param name="chromeManager_">The chrome manager.</param>
		/// <param name="swfPath_">The SWF path.</param>
		protected FlexModuleFlashPlayerBaseImpl(IChromeRegistry chromeRegistry_, IChromeManager chromeManager_, Uri swfPath_) :
            base(chromeRegistry_, chromeManager_, swfPath_)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FlexModuleFlashPlayerBaseImpl"/> class.
        /// </summary>
		/// <param name="chromeRegistry_">The chrome registry.</param>
		/// <param name="chromeManager_">The chrome manager.</param>
        protected FlexModuleFlashPlayerBaseImpl(IChromeRegistry chromeRegistry_, IChromeManager chromeManager_) :
            base(chromeRegistry_, chromeManager_)
        {
        }

        #endregion

        #region View Creation

        /// <summary>
        /// Create the Module's content. Adds the Flash Player control to the module and initializes it.
        /// </summary>
        /// <param name="viewcontainer_">The view's container</param>
        protected virtual bool CreateView(IWindowViewContainer viewcontainer_, XDocument state)
        {
            var flashPlayer = new FlexModuleFlashPlayerView();
            m_instanceInfos.Add(flashPlayer, new FlashPlayerInstanceInfo());
            flashPlayer.Loaded += FlashPlayerLoaded;
            viewcontainer_.Parameters.Width = flashPlayer.Width + 30;
			viewcontainer_.Parameters.Height = flashPlayer.Height + 50;  // add extra space for decoration
            viewcontainer_.Content = flashPlayer;
            if (SwfPath != null)
            {
                flashPlayer.Source = SwfPath.ToString();
            }

            viewcontainer_.Closed += ViewClosed;
            viewcontainer_.ClosedQuietly += ViewClosed;
			return true;
        }

        void ViewClosed(object sender, WindowEventArgs e)
        {
            var flashPlayer = e.ViewContainer.Content as FlexModuleFlashPlayerView;
            e.ViewContainer.Closed -= ViewClosed;
            e.ViewContainer.ClosedQuietly -= ViewClosed;
            if (flashPlayer == null) return;
            var flashProxy = m_instanceInfos[flashPlayer].FlashProxy;
            flashProxy.ExternalInterfaceCall -= ExternalInterfaceCall;
            flashProxy.Dispose();
            m_instanceInfos.Remove(flashPlayer);
            flashPlayer.Dispose();

        }

        /// <summary>
        /// Called when the Flash control is initialised
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FlashPlayerLoaded(object sender, RoutedEventArgs e)
        {
            var flashPlayer = sender as FlexModuleFlashPlayerView;

            // Create the proxy and register this app to receive notification when the proxy receives
            // a call from ActionScript
            var flashProxy = new ExternalInterfaceProxy(flashPlayer.m_flashPlayer);
            flashProxy.ExternalInterfaceCall += ExternalInterfaceCall;

            m_instanceInfos[flashPlayer].AppReady = true;
            m_instanceInfos[flashPlayer].FlashProxy = flashProxy;

            flashPlayer.Loaded -= FlashPlayerLoaded;
        }

        #endregion

        #region SWF Call dispatch

        /// <summary>
        /// Called by the proxy when an ActionScript ExternalInterface call
        /// is made by the SWF
        /// </summary>
        /// <param name="sender">The object raising the event</param>
        /// <param name="e">The event arguments associated with the event</param>
        /// <returns>The response to the function call.</returns>
        private object ExternalInterfaceCall(object sender, ExternalInterfaceCallEventArgs e)
        {
            string functionName = e.FunctionCall.FunctionName;
            var player = m_instanceInfos.Keys.FirstOrDefault(p => ReferenceEquals(m_instanceInfos[p].FlashProxy, sender));
            switch (functionName)
            {
                case "MSGUI_isReady":
                    return IsReady(player);
                case "MSGUI_setSWFIsReady":
                    SetSwfIsReady(player);
                    return null;
                case "MSGUI_useDirectCalls":
                    return UseDirectCalls();
                default:
                    return CallFromFlex(functionName, e.FunctionCall.Arguments);
            }
        }

        /// <summary>
        /// Call Flex functions from .Net
        /// </summary>
        /// <param name="functionName_">Name of the function to be called</param>
        /// <param name="arguments_">Array of parameters</param>
        /// <returns></returns>
        [Obsolete("Use the other overload and specify the viewcontainer")]
        protected override object CallToFlex(string functionName_, params object[] arguments_)
        {
            var results = new List<object>();
            foreach (var info in m_instanceInfos.Values)
            {
                if (info.SwfReady)
                {
                    results.Add(info.FlashProxy.Call(functionName_, arguments_));
                }
            }
            return results.FirstOrDefault();
        }

        protected object CallToFlex(IWindowViewContainer viewContainer, string functionName, params object[] arguments)
        {
            var player = viewContainer.Content as FlexModuleFlashPlayerView;
            return player != null && m_instanceInfos.ContainsKey(player)
                       ? m_instanceInfos[player].FlashProxy.Call(functionName, arguments)
                       : null;
        }

        #endregion

        #region Methods called by Flash Player

        /// <summary>
        /// Called to check if the page has initialized and JavaScript is available
        /// </summary>
        /// <returns></returns>
        private bool IsReady(FlexModuleFlashPlayerView player)
        {
            FlashPlayerInstanceInfo info;
            return player != null && m_instanceInfos.TryGetValue(player, out info) && info.AppReady;
        }

        /// <summary>
        /// Called to notify the page that the SWF has set it's callbacks
        /// </summary>
        private void SetSwfIsReady(FlexModuleFlashPlayerView player)
        {
            // record that the SWF has registered it's functions (i.e. that C#
            // can safely call the ActionScript functions)
            m_instanceInfos[player].SwfReady = true;
        }

        /// <summary>
        /// Tells Flex that it's hosted in an ActiveX container
        /// </summary>
        /// <returns><code>true</code></returns>
        private static object UseDirectCalls()
        {
            return true;
        }

        #endregion

    }
}
