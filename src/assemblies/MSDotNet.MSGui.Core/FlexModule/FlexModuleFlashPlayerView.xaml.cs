﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/FlexModule/FlexModuleFlashPlayerView.xaml.cs#7 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using UserControl = System.Windows.Controls.UserControl;

namespace MorganStanley.MSDotNet.MSGui.Core.FlexModule
{
	/// <summary>
	/// Interaction logic for FlexModuleView.xaml
	/// </summary>
	public partial class FlexModuleFlashPlayerView : UserControl, IDisposable
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="FlexModuleFlashPlayerView"/> class.
		/// </summary>
		public FlexModuleFlashPlayerView()
		{
			InitializeComponent();
			m_flashPlayer.CreateControl();  // needed to avoid an InvalidActiveXStateException under .Net 3.5
		}

		/// <summary>
		/// Sets the source.
		/// </summary>
		/// <value>
		/// The source.
		/// </value>
		public string Source
		{
			set
			{
				m_flashPlayer.LoadMovie(0, value);
				m_flashPlayer.Play();
			}
		}

		public static void FieldSet(object object_, string fieldName_, object value_)
		{
			FieldInfo field = object_.GetType().GetField(fieldName_,
														 BindingFlags.FlattenHierarchy | BindingFlags.Public |
														 BindingFlags.NonPublic | BindingFlags.Static |
														 BindingFlags.Instance);

			if (field == null)
			{
				throw new InvalidOperationException(string.Format("Field \"{0}\" is not accessible.", fieldName_));
			}

			field.SetValue(object_, value_);
		}

		public void Dispose()
		{
			Dispatcher.BeginInvoke(DispatcherPriority.Render, (Action)(() =>
																			{
																				wfh.Child = null;
																				m_flashPlayer.Visible = false;
																				m_flashPlayer.Dispose();
																				m_flashPlayer = null;
																				// reflection hack to fix a leak in the .net fw itself
																				object winFormsAdapter =
																					typeof(WindowsFormsHost).GetProperty(
																						"HostContainerInternal",
																						BindingFlags.GetProperty |
																						BindingFlags.NonPublic |
																						BindingFlags.Instance).
																						GetValue(wfh, null);

																				FieldSet(winFormsAdapter, "_host", null);

																				//this.RemoveLogicalChild(wfh);

																				if (winFormsAdapter is IDisposable)
																				{
																					((IDisposable)winFormsAdapter).Dispose();
																				}
																			}
																	   ));
		}
	}
}
