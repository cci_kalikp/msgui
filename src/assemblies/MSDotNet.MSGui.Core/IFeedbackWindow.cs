﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Core
{
    public interface IFeedbackWindow
    {
        event EventHandler<EventArgs> FullyLoaded;

        bool IsLayoutLoadActive { get; set; }
    }
}
