﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/ControlParadigm/ControlParadigmPresentationMode.cs#6 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;

namespace MorganStanley.MSDotNet.MSGui.Core
{
  [Flags]
  public enum ControlParadigmPresentationMode
  {
    Default,
    Expanded,
    Client,
    All = Default | Expanded | Client
  }
}
