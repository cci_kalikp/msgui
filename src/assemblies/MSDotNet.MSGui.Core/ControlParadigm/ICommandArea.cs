﻿using System;

namespace MorganStanley.MSDotNet.MSGui.Core
{
	public interface ICommandArea
	{
		void AddItem(string itemId);
		void AddItem(string itemId, int index);
		void AddItem(string itemId, InitialViewSettings ivs);
		void AddItem(string itemId, int index, InitialViewSettings ivs);
		[Obsolete("Use the RemoveItem(string item_) call instead")]
		void RemoveItem(object item);
		void RemoveItem(string item);
		void Clear();
	}
}