﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/ControlParadigm/IEarlyInitializedModule.cs#6 $
// $Change: 863505 $
// $DateTime: 2014/01/23 04:26:52 $
// $Author: caijin $

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Composite.Modularity;


namespace MorganStanley.MSDotNet.MSGui.Core.ControlParadigm
{ 
  public interface IEarlyInitializedModule:IModule
  {
  }
}
