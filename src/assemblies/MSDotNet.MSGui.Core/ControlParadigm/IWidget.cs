﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/ControlParadigm/IWidget.cs#6 $
// $Change: 901409 $
// $DateTime: 2014/10/17 05:24:50 $
// $Author: caijin $

using System.Windows;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Core
{
  public interface IWidget
  {
    string Title
    {
      get; set;
    }

    UIElement MaximizedView
    {
      get; set;
    }

    UIElement MinimizedView
    {
      get;
      set;
    }

    WidgetState State
    {
      get;
    }

    IWidgetViewContainer Parent { get; }
  }

  public enum WidgetState
  {
    Maximized,
    Minimized
  }
}
