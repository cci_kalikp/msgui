﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/ControlParadigm/MSGuiButton.cs#8 $
// $Change: 894829 $
// $DateTime: 2014/08/29 04:54:09 $
// $Author: scull $

using System;
using System.ComponentModel;
using System.Windows.Media;
using System.Windows.Input;

namespace MorganStanley.MSDotNet.MSGui.Core
{
    public class MSGuiButton : INotifyPropertyChanged
    {
        private ImageSource m_image;
        public ImageSource Image
        {
            get
            {
                return m_image;
            }
            set
            {
                m_image = value;
                InvokePropertyChanged("Image");
            }
        }

        private string m_text;
        public string Text
        {
            get
            {
                return m_text;
            }
            set
            {
                m_text = value;
                InvokePropertyChanged("Text");
            }
        }

        private string m_toolTip;
        public string ToolTip
        {
            get
            {
                return m_toolTip;
            }
            set
            {
                m_toolTip = value;
                InvokePropertyChanged("ToolTip");
            }
        }

        private bool m_isEnabled = true;
        public bool IsEnabled
        {
            get
            {
                return m_isEnabled;
            }
            set
            {
                m_isEnabled = value;
                InvokePropertyChanged("IsEnabled");
            }
        }

        private ButtonSize m_buttonSize;

        public ButtonSize ButtonSize
        {
            get
            {
                return m_buttonSize;
            }
            set
            {
                m_buttonSize = value;
                InvokePropertyChanged("ButtonSize");
            }
        }

        public EventHandler OnClick;


        private ICommand m_command;
        public ICommand Command
        {
            get { return m_command; }
            set
            {
                m_command = value;
                InvokePropertyChanged("Command");
            }
        }

        private CommandBinding m_commandBinding;


        public CommandBinding CommandBinding
        {
            get { return m_commandBinding; }
            set
            {
                m_commandBinding = value;
                InvokePropertyChanged("CommandBinding");
            }
        }
        private object m_commandParameter;

        public object CommandParameter
        {
            get { return m_commandParameter; }
            set
            {
                m_commandParameter = value;
                InvokePropertyChanged("CommandParameter");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void InvokePropertyChanged(string name_)
        {
            PropertyChangedEventHandler copy = PropertyChanged;
            if (copy != null)
            {
                copy(this, new PropertyChangedEventArgs(name_));
            }
        }
    }
}

public enum ButtonSize
{
    Large,
    Small,
    ImageOnly
}
