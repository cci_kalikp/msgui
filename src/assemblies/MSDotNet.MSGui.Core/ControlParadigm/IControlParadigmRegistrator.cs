﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/ControlParadigm/IControlParadigmRegistrator.cs#11 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.Windows;
using MorganStanley.MSDotNet.MSGui.Core.Ribbon;

namespace MorganStanley.MSDotNet.MSGui.Core
{
  public interface IControlParadigmRegistrator
  {
    [Obsolete("Use the ControlParadigm approach instead of Ribbon approach")]
    void AddRibbonItem(string tab, string group, RibbonItem item);
    [Obsolete("Use the ControlParadigm approach instead of Ribbon approach")]
    void AddMenuItem(RibbonButton ribbonButton);

    void Register(string key_, CreateControlHandler createControlHandler_);
    void Register(string key_, MSGuiButton button_);

    void Register(string key_, UIElement control_); 

    /// <summary>
    /// Check that the registry already contains the key with such name
    /// </summary>
    /// <param name="key_"></param>
    /// <returns></returns>
    bool IsRegistered(string key_);
  }

  public delegate void CreateControlHandler(IWidget widget_); 
}
