﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/ControlParadigm/IControlParadigmImplementator.cs#11 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.Drawing;

namespace MorganStanley.MSDotNet.MSGui.Core
{
  public interface IControlParadigmImplementator
  {
    void SetOnClosingAction(Action OnClosing);    
    [Obsolete]
    void PlaceAllControls();
    [Obsolete]
    void SetRibbonIcon(Icon icon);

    ICommandArea this[string id_]
    {
      get;
    }
  }
}
