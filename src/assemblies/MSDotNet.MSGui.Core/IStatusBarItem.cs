﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/IStatusBarItem.cs#5 $
// $Change: 862682 $
// $DateTime: 2014/01/17 16:11:04 $
// $Author: hrechkin $

namespace MorganStanley.MSDotNet.MSGui.Core
{
  /// <summary>
  /// Represents an item in the status bar.
  /// </summary>
  public interface IStatusBarItem
  {
    /// <summary>
    /// Update the status of the status bar item.
    /// </summary>
    /// <param name="level_">Status level of this update.</param>
    /// <param name="statusText_">Text (or value) of the status item.</param>
    void UpdateStatus(StatusLevel level_, string statusText_);

    /// <summary>
    /// Clear the current status text.
    /// </summary>
    void ClearStatus();

    /// <summary>
    /// Get the control associated with the status bar item.
    /// </summary>
    object Control
    {
      get;
    }

    /// <summary>
    /// Noification that a status event occured.
    /// </summary>
    event StatusUpdatedEventHandler StatusUpdated;
  }
}
