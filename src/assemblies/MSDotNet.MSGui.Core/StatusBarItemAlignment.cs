﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/StatusBarItemAlignment.cs#5 $
// $Change: 862682 $
// $DateTime: 2014/01/17 16:11:04 $
// $Author: hrechkin $

using System.Runtime.Serialization;

namespace MorganStanley.MSDotNet.MSGui.Core
{
    /// <summary>
    /// Alignment of item in status bar.
    /// </summary>
    [DataContract(Name = "StatusBarItemAlignment")]
    public enum StatusBarItemAlignment
    {
        [EnumMember] Left,
        [EnumMember] Right
        //Center?
    }
}
