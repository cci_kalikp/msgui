﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/ReflHelper.cs#1 $
// $Change: 858188 $
// $DateTime: 2013/12/10 01:31:58 $
// $Author: caijin $

using System;
using System.Reflection;

namespace MorganStanley.MSDotNet.MSGui.Core
{
    //now when upgrade IG, check the reference of this type might find all the hacks
    public static class ReflHelper
    {
        private const BindingFlags CommonBindingFlags = BindingFlags.FlattenHierarchy | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance;

        public static object PropertyGet(object object_, string propertyName_, Type objectType_)
        {
            Guard.ArgumentNotNull(object_, "object_");
            Guard.ArgumentNotNull(objectType_, "objectType_");
            Guard.ArgumentNotNullOrEmptyString(propertyName_, "propertyName_");

            PropertyInfo property = objectType_.GetProperty(propertyName_, CommonBindingFlags);
            if (property == null)
            {
                throw new InvalidOperationException(string.Format("Property \"{0}\" is not accessible.", propertyName_));
            }

            return property.GetValue(object_, null);
        }

        public static bool PropertyExists(object object_, string propertyName_)
        {
            return null != object_.GetType().GetProperty(propertyName_, CommonBindingFlags);
        }

        public static object TryPropertyGet(object object_, string propertyName_)
        {
            if (object_ == null)
            {
                return null;
            }

            PropertyInfo property = object_.GetType().GetProperty(propertyName_, CommonBindingFlags);

            if (property == null)
            {
                return null;
            }

            return property.GetValue(object_, null);
        }

        public static object PropertyGet(object object_, string propertyName_)
        {
            Guard.ArgumentNotNull(object_, "object_");
            Guard.ArgumentNotNullOrEmptyString(propertyName_, "propertyName_");

            PropertyInfo property = object_.GetType().GetProperty(propertyName_, CommonBindingFlags);

            if (property == null)
            {
                throw new InvalidOperationException(string.Format("Property \"{0}\" is not accessible.", propertyName_));
            }

            return property.GetValue(object_, null);
        }

        public static void PropertySet(object object_, string propertyName_, object value_)
        {
            Guard.ArgumentNotNull(object_, "object_");
            Guard.ArgumentNotNullOrEmptyString(propertyName_, "propertyName_");

            PropertyInfo property = object_.GetType().GetProperty(propertyName_, CommonBindingFlags);

            if (property == null)
            {
                throw new InvalidOperationException(string.Format("Property \"{0}\" is not accessible.", propertyName_));
            }

            property.SetValue(object_, value_, null);
        }

        public static void TryPropertySet(object object_, string propertyName_, object value_)
        {
            if (object_ == null)
            {
                return;
            }

            PropertyInfo property = object_.GetType().GetProperty(propertyName_, CommonBindingFlags);

            if (property == null)
            {
                return;
            }

            property.SetValue(object_, value_, null);
        }

        public static void PropertySet(object object_, string propertyName_, object value_, Type objectType_)
        {
            Guard.ArgumentNotNull(object_, "object_");
            Guard.ArgumentNotNullOrEmptyString(propertyName_, "propertyName_");

            PropertyInfo property = objectType_.GetProperty(propertyName_, CommonBindingFlags);
            if (property == null)
            {
                throw new InvalidOperationException(string.Format("Property \"{0}\" is not accessible.", propertyName_));
            }

            property.SetValue(object_, value_, null);
        }

        public static object MethodInvoke(object object_, string methodName_, Type[] paramTypes_, object[] parameters_)
        {
            Guard.ArgumentNotNull(object_, "object_");
            Guard.ArgumentNotNullOrEmptyString(methodName_, "methodName_");

            MethodInfo method = object_.GetType().GetMethod(methodName_, CommonBindingFlags, null, paramTypes_, null);

            if (method == null)
            {
                throw new InvalidOperationException(string.Format("Method \"{0}\" is not accessible.", methodName_));
            }

            return method.Invoke(object_, parameters_);
        }

        public static object MethodInvoke(object object_, string methodName_, Type[] paramTypes_, object[] parameters_,
                                          Type objectType_)
        {
            Guard.ArgumentNotNull(object_, "object_");
            Guard.ArgumentNotNull(objectType_, "objectType_");
            Guard.ArgumentNotNullOrEmptyString(methodName_, "methodName_");

            MethodInfo method = objectType_.GetMethod(methodName_, CommonBindingFlags, null, paramTypes_, null);
            if (method == null)
            {
                throw new InvalidOperationException(string.Format("Method \"{0}\" is not accessible.", methodName_));
            }

            return method.Invoke(object_, parameters_);
        }

        public static object StaticMethodInvoke(Type classType_, string methodName_, object[] parameters_)
        {
            Guard.ArgumentNotNull(classType_, "classType_");
            Guard.ArgumentNotNullOrEmptyString(methodName_, "methodName_");

            MethodInfo method;
            BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static;
            try
            {
                method = classType_.GetMethod(methodName_, flags);
            }
            catch (AmbiguousMatchException exc)
            {
                throw new InvalidOperationException(
                    string.Format(
                        "Method \"{0}\" is ambigous. Please use another overloaded version of StaticMethodInvoke to provide paremeter types",
                        methodName_), exc);
            }
            if (method == null)
            {
                throw new InvalidOperationException(string.Format("Method \"{0}\" is not accessible.", methodName_));
            }

            return method.Invoke(null, parameters_);
        }

        public static object StaticMethodInvoke(Type classType_, string methodName_, Type[] paramTypes_,
                                                object[] parameters_)
        {
            Guard.ArgumentNotNull(classType_, "classType_");
            Guard.ArgumentNotNullOrEmptyString(methodName_, "methodName_");

            MethodInfo method = classType_.GetMethod(methodName_, CommonBindingFlags, null, paramTypes_, null);
            if (method == null)
            {
                throw new InvalidOperationException(string.Format("Method \"{0}\" is not accessible.", methodName_));
            }

            return method.Invoke(null, parameters_);
        }

        public static object MethodInvoke(object object_, string methodName_, object[] parameters_)
        {
            Guard.ArgumentNotNull(object_, "object_");
            Guard.ArgumentNotNullOrEmptyString(methodName_, "methodName_");

            MethodInfo method = object_.GetType().GetMethod(methodName_, CommonBindingFlags);
            if (method == null)
            {
                throw new InvalidOperationException(string.Format("Method \"{0}\" is not accessible.", methodName_));
            }

            return method.Invoke(object_, parameters_);
        }

        public static object MethodInvoke(object object_, string methodName_, object[] parameters_, Type objectType_)
        {
            Guard.ArgumentNotNull(object_, "object_");
            Guard.ArgumentNotNull(objectType_, "objectType_");
            Guard.ArgumentNotNullOrEmptyString(methodName_, "methodName_");

            MethodInfo method = objectType_.GetMethod(methodName_, CommonBindingFlags);
            if (method == null)
            {
                throw new InvalidOperationException(string.Format("Method \"{0}\" is not accessible.", methodName_));
            }

            return method.Invoke(object_, parameters_);
        }

        public static void AddStaticEventHandler(object object_, string eventName_, string handlerName_,
                                                 Type handlerClassType_)
        {
            Guard.ArgumentNotNull(object_, "object_");
            Guard.ArgumentNotNull(handlerClassType_, "handlerClassType_");
            Guard.ArgumentNotNullOrEmptyString(eventName_, "eventName_");
            Guard.ArgumentNotNullOrEmptyString(handlerName_, "handlerName_");

            EventInfo eventInfo = object_.GetType().GetEvent(
                eventName_, CommonBindingFlags);
            Type handlerType = eventInfo.EventHandlerType;
            if (handlerType == null)
            {
                throw new InvalidOperationException("Event type unavailable: " + eventName_);
            }

            BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static;
            MethodInfo handlerMethod = handlerClassType_.GetMethod(handlerName_, flags);
            Delegate d = Delegate.CreateDelegate(handlerType, handlerMethod);

            MethodInfo addHandler = eventInfo.GetAddMethod();
            object[] handlerArgs = {d};
            addHandler.Invoke(object_, handlerArgs);
        }

        public static void RemoveStaticEventHandler(object object_, string eventName_, string handlerName_,
                                                    Type handlerClassType_)
        {
            Guard.ArgumentNotNull(object_, "object_");
            Guard.ArgumentNotNull(handlerClassType_, "handlerClassType_");
            Guard.ArgumentNotNullOrEmptyString(eventName_, "eventName_");
            Guard.ArgumentNotNullOrEmptyString(handlerName_, "handlerName_");

            EventInfo eventInfo = object_.GetType().GetEvent(eventName_, CommonBindingFlags);
            Type eventType = eventInfo.EventHandlerType;
            if (eventType == null)
            {
                throw new InvalidOperationException("Event type unavailable: " + eventName_);
            }

            BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static;
            MethodInfo handlerMethod = handlerClassType_.GetMethod(handlerName_, flags);
            Delegate d = Delegate.CreateDelegate(eventType, object_, handlerMethod);

            MethodInfo removeHandler = eventInfo.GetRemoveMethod();
            object[] handlerArgs = {d};
            removeHandler.Invoke(object_, handlerArgs);
        }

        public static void AddInstanceEventHandler(object object_, string eventName_, string handlerName_,
                                                   object handlerClass_)
        {
            //specify Type explicitely if the handler is located in a base class
            AddInstanceEventHandler(object_, eventName_, handlerName_, handlerClass_, handlerClass_.GetType());
        }

        public static void AddInstanceEventHandler(object object_, string eventName_, string handlerName_,
                                                   object handlerClass_, Type handlerClassType_)
        {
            Guard.ArgumentNotNull(object_, "object_");
            Guard.ArgumentNotNull(handlerClassType_, "handlerClassType_");
            Guard.ArgumentNotNullOrEmptyString(eventName_, "eventName_");
            Guard.ArgumentNotNullOrEmptyString(handlerName_, "handlerName_");

            EventInfo eventInfo = object_.GetType().GetEvent(eventName_, CommonBindingFlags);
            Type handlerType = eventInfo.EventHandlerType;
            if (handlerType == null)
            {
                throw new InvalidOperationException("Event type unavailable: " + eventName_);
            }

            BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance;

            MethodInfo handlerMethod = handlerClassType_.GetMethod(handlerName_, flags);
            Delegate d = Delegate.CreateDelegate(
                handlerType,
                handlerClass_,
                handlerMethod,
                true);

            MethodInfo addHandler = eventInfo.GetAddMethod();
            object[] handlerArgs = {d};
            addHandler.Invoke(object_, handlerArgs);
        }

        public static void RemoveInstanceEventHandler(object object_, string eventName_, string handlerName_,
                                                      object handlerClass_)
        {
            //specify Type explicitely if the handler is located in a base class
            RemoveInstanceEventHandler(object_, eventName_, handlerName_, handlerClass_, handlerClass_.GetType());
        }

        public static void RemoveInstanceEventHandler(object object_, string eventName_, string handlerName_,
                                                      object handlerClass_, Type handlerClassType_)
        {
            Guard.ArgumentNotNull(object_, "object_");
            Guard.ArgumentNotNull(handlerClassType_, "handlerClassType_");
            Guard.ArgumentNotNullOrEmptyString(eventName_, "eventName_");
            Guard.ArgumentNotNullOrEmptyString(handlerName_, "handlerName_");

            EventInfo eventInfo = object_.GetType().GetEvent(eventName_, CommonBindingFlags);
            Type eventType = eventInfo.EventHandlerType;
            if (eventType == null)
            {
                throw new InvalidOperationException("Event type unavailable: " + eventName_);
            }

            BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance;
            MethodInfo handlerMethod = handlerClassType_.GetMethod(handlerName_, flags);
            Delegate d = Delegate.CreateDelegate(
                eventType,
                handlerClass_,
                handlerMethod,
                true);

            MethodInfo removeHandler = eventInfo.GetRemoveMethod();
            object[] handlerArgs = {d};
            removeHandler.Invoke(object_, handlerArgs);
        }

        public static object GetIndexed(object collection_, object index_, string itemTypeName_)
        {
            //we assume here that collection and Item types live in the same assembly
            Guard.ArgumentNotNull(collection_, "collection_");

            Type collType = collection_.GetType();
            PropertyInfo pi = collType.GetProperty(
                "Item",
                CommonBindingFlags,
                null,
                collType.GetNamedTypeFromTheSameAssemblyAsType(itemTypeName_),
                new[] {index_.GetType()},
                null);
            if (pi == null)
            {
                throw new InvalidOperationException(
                    string.Format("Indexed property Item is not accessible for this collection."));
            }

            return pi.GetValue(collection_, new[] {index_});
        }

        public static object GetIndexed(object collection_, object index_, Type itemType_)
        {
            //we assume here that Collection's and Item's types are in the same assembly
            Guard.ArgumentNotNull(collection_, "collection_");

            Type collType = collection_.GetType();
            PropertyInfo pi = collType.GetProperty(
                "Item",
                CommonBindingFlags,
                null,
                itemType_,
                new[] {index_.GetType()},
                null);
            if (pi == null)
            {
                throw new InvalidOperationException(
                    string.Format("Indexed property Item is not accessible for this collection."));
            }

            return pi.GetValue(collection_, new[] {index_});
        }

        // the better solution could be: to create an instance of the type and to use "is"
        public static bool IsOfType(object object_, string typename_)
        {
            if (object_ == null)
            {
                return false;
            }
            Type objectType = object_.GetType();

            while (objectType != typeof (object) && objectType.FullName != typename_)
            {
                objectType = objectType.BaseType;
            }
            return objectType.FullName == typename_;
        }

        public static bool IsType(Type type_, string typename_)
        {
            Guard.ArgumentNotNull(type_, "type_");
            while (type_ != typeof (object) && type_.FullName != typename_)
            {
                type_ = type_.BaseType;
            }
            return type_.FullName == typename_;
        }

        public static object StaticPropertyGet(Type type_, string propertyName_)
        {
            Guard.ArgumentNotNull(type_, "type_");
            Guard.ArgumentNotNullOrEmptyString(propertyName_, "propertyName_");

            PropertyInfo property = type_.GetProperty(
                propertyName_,
                BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
            if (property == null)
            {
                throw new InvalidOperationException(string.Format("Property \"{0}\" is not accessible.", propertyName_));
            }

            return property.GetValue(null, null);
        }

        public static void StaticPropertySet(Type type_, string propertyName_, object value_)
        {
            Guard.ArgumentNotNull(type_, "type_");
            Guard.ArgumentNotNullOrEmptyString(propertyName_, "propertyName_");

            PropertyInfo property = type_.GetProperty(
                propertyName_,
                BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
            if (property == null)
            {
                throw new InvalidOperationException(string.Format("Property \"{0}\" is not accessible.", propertyName_));
            }

            property.SetValue(null, value_, null);
        }

        public static object StaticFieldGet(Type type_, string fieldName_)
        {
            Guard.ArgumentNotNull(type_, "type_");
            Guard.ArgumentNotNullOrEmptyString(fieldName_, "fieldName_");

            return type_.GetField(fieldName_, CommonBindingFlags).GetValue(null);
        }

        public static void StaticFieldSet(Type type_, string fieldName_, object value_)
        {
            Guard.ArgumentNotNull(type_, "type_");
            Guard.ArgumentNotNullOrEmptyString(fieldName_, "fieldName_");

            type_.GetField(fieldName_, CommonBindingFlags).SetValue(null, value_);
        }

        public static Type GetNamedBaseType(object object_, string typename_)
        {
            Guard.ArgumentNotNull(object_, "object_");
            return GetNamedBaseType(object_.GetType(), typename_);
        }

        public static Type GetNamedBaseType(Type type_, string typename_)
        {
            Guard.ArgumentNotNull(type_, "type_");

            Type objectType = type_;
            while (objectType.FullName != typename_ && objectType != typeof (object))
            {
                objectType = objectType.BaseType;
            }
            if (objectType.FullName == typename_)
            {
                return objectType;
            }
            return null;
        }

        public static Type GetNamedTypeFromTheSameAssemblyAsObject(this object object_, string typename_)
        {
            Guard.ArgumentNotNull(object_, "object_");
            Guard.ArgumentNotNull(typename_, "typename_");

            return object_.GetType().GetNamedTypeFromTheSameAssemblyAsType(typename_);
        }

        public static Type GetNamedTypeFromTheSameAssemblyAsType(this Type type_, string typename_)
        {
            Guard.ArgumentNotNull(type_, "type_");
            Guard.ArgumentNotNull(typename_, "typename_");

            Type objectType = type_;
            while (objectType != typeof (object) && objectType.Assembly.GetType(typename_, false) == null)
            {
                objectType = objectType.BaseType;
            }

            return objectType.Assembly.GetType(typename_, false);
        }

        public static Assembly GetAssemblyWithNamedType(object object_, string typename_)
        {
            return GetNamedBaseType(object_, typename_).Assembly;
        }

        public static Assembly GetAssemblyWithNamedType(Type type_, string typename_)
        {
            return GetNamedBaseType(type_, typename_).Assembly;
        }

        public static object FieldGet(object object_, string fieldName_)
        {
            Guard.ArgumentNotNull(object_, "object_");
            Guard.ArgumentNotNullOrEmptyString(fieldName_, "fieldName_");

            FieldInfo field = object_.GetType().GetField(fieldName_, CommonBindingFlags);

            if (field == null)
            {
                throw new InvalidOperationException(string.Format("Field \"{0}\" is not accessible.", fieldName_));
            }

            return field.GetValue(object_);
        }

        public static void FieldSet(object object_, string fieldName_, object value_)
        {
            Guard.ArgumentNotNull(object_, "object_");
            Guard.ArgumentNotNullOrEmptyString(fieldName_, "propertyName_");

            FieldInfo field = object_.GetType().GetField(fieldName_, CommonBindingFlags);

            if (field == null)
            {
                throw new InvalidOperationException(string.Format("Field \"{0}\" is not accessible.", fieldName_));
            }

            field.SetValue(object_, value_);
        }


    }
}
