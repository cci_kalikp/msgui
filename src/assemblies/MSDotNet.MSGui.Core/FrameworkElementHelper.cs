﻿using System;
using System.Globalization;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Xml;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Core
{
    public static class FrameworkElementHelper
    {
         
        public static void AccumulateMargin<T>(this DependencyObject item_, ref double margin_, bool vertical_) where T:Control
        {
            AccumulateMargin(item_, ref margin_, vertical_, typeof(T));
        }

        public static void AccumulateMargin(this DependencyObject item_, ref double margin_, bool vertical_, Type topLevelControlType_)
        {
            AccumulateMargin(item_, ref margin_, vertical_, topLevelControlType_.IsInstanceOfType);
        }


        public static void AccumulateMargin(this DependencyObject item_, ref double margin_, bool vertical_, Predicate<DependencyObject> stopAt_)
       {
           var element = item_ as FrameworkElement;
           if (element != null && element.Visibility != Visibility.Collapsed) margin_ += vertical_ ? (element.Margin.Bottom + element.Margin.Top) : (element.Margin.Left + element.Margin.Right);
           DependencyObject parent = VisualTreeHelper.GetParent(item_);
           var c = parent as Control;
           if (c != null)
           {
               margin_ += vertical_ ? (c.Padding.Top + c.Padding.Bottom + c.BorderThickness.Top + c.BorderThickness.Bottom) :
                   (c.Padding.Left + c.Padding.Right + c.BorderThickness.Left + c.BorderThickness.Right);
           }
           else
           {
               var b = parent as Border;
               if (b != null) margin_ += vertical_ ? (b.Padding.Top + b.Padding.Bottom + b.BorderThickness.Top + b.BorderThickness.Bottom) :
                   (b.Padding.Left + b.Padding.Right + b.BorderThickness.Left + b.BorderThickness.Right);
           }

           if (parent == null || stopAt_(parent)) return;
           AccumulateMargin(parent, ref margin_, vertical_, stopAt_);
       }
         
        public static IWindowViewContainer GetContainer(this DependencyObject element_)
        {
            DependencyObject vchcParent = element_;
            while (vchcParent != null)
            {
                IWindowViewContainerHost host = vchcParent as IWindowViewContainerHost;
                if (host != null)
                {
                    return host.View;  
                }
                vchcParent = VisualTreeHelper.GetParent(vchcParent);
            }
            return null;
        }
        public static void HandleUnloaded(this FrameworkElement element_, Action<FrameworkElement> action_)
        {
            RoutedEventHandler loadedHandler = null;

            IWindowViewContainer w = element_.GetContainer();
            if (w == null || !element_.IsLoaded)
            {
                element_.Loaded += loadedHandler = (sender_, args_) =>
                {
                    w = element_.GetContainer();
                    if (w != null)
                    {
                        EventHandler<WindowEventArgs> closedHandler = null;
                        w.Closed +=  closedHandler = (o_, eventArgs_) =>
                            {
                                action_(element_);
                                w.Closed -= closedHandler;
                                w.ClosedQuietly -= closedHandler;
                            };
                        w.ClosedQuietly += closedHandler;
                    }
                     
                    element_.Loaded -= loadedHandler;
                };
            }
            else
            {
                EventHandler<WindowEventArgs> closedHandler = null;
                w.Closed += closedHandler = (o_, eventArgs_) =>
                {
                    action_(element_);
                    w.Closed -= closedHandler;
                    w.ClosedQuietly -= closedHandler;
                };
                w.ClosedQuietly += closedHandler;
            } 
        }

        public static bool IsTrimmed(TextBlock tb_)
        {
            Rect characterRect;
            Rect rect2;
            if (tb_ == null)
            {
                return false;
            }
            if (((VisualTreeHelper.GetChildrenCount(tb_) == 0) || (tb_.ContentStart == null)) || (tb_.ContentEnd == null))
            {
                characterRect = rect2 = Rect.Empty;
            }
            else
            {
                characterRect = tb_.ContentStart.DocumentStart.GetCharacterRect(tb_.ContentStart.LogicalDirection);
                rect2 = tb_.ContentEnd.DocumentEnd.GetCharacterRect(tb_.ContentEnd.LogicalDirection);
            }
            if (!characterRect.IsEmpty && !rect2.IsEmpty)
            {
                Rect rect3 = Rect.Union(characterRect, rect2);
                Size size = LayoutInformation.GetLayoutSlot(tb_).Size;
                Thickness margin = tb_.Margin;
                rect3.Width = Math.Max((double)0.0, (double)((rect3.Width + margin.Left) + margin.Right));
                rect3.Height = Math.Max((double)0.0, (double)((rect3.Height + margin.Top) + margin.Bottom));
                if ((CoreUtilities.AreClose(rect3.Width, size.Width) || (rect3.Width <= size.Width)) && (CoreUtilities.AreClose(rect3.Height, size.Height) || (rect3.Height <= size.Height)))
                {
                    return false;
                }
                return true;
            }
            tb_.UpdateLayout();
            if (LayoutInformation.GetLayoutClip(tb_) != null)
            {
                return true;
            }
            try
            {
                AccessText parent = VisualTreeHelper.GetParent(tb_) as AccessText;
                string textToFormat = (parent != null) ? parent.Text : tb_.Text;
                CultureInfo nonNeutralCulture = DependencyObjectHelper.GetNonNeutralCulture(parent == null ? tb_ : (FrameworkElement)parent);
                Typeface typeface = new Typeface(tb_.FontFamily, tb_.FontStyle, tb_.FontWeight, tb_.FontStretch);
                FormattedText text2 = new FormattedText(textToFormat, nonNeutralCulture, tb_.FlowDirection, typeface, tb_.FontSize, tb_.Foreground)
                {
                    MaxTextWidth = tb_.ActualWidth,
                    Trimming = TextTrimming.None
                };
                if (text2.Height > tb_.ActualHeight)
                {
                    return true;
                }
            }
            catch (Exception)
            {
            }
            return false;
        }

        public static object CloneElement(object elementToClone_)
        {
            string xaml = XamlWriter.Save(elementToClone_);
            return XamlReader.Load(new XmlTextReader(new StringReader(xaml)));
        }




        public static bool CanExecuteCommand(ICommandSource commandSource)
        {
            if (commandSource == null)
            {
                throw new ArgumentNullException("commandSource");
            }
            bool flag = false;
            ICommand command = commandSource.Command;
            if (command == null)
            {
                return flag;
            }
            RoutedCommand command2 = command as RoutedCommand;
            object commandParameter = commandSource.CommandParameter;
            if (command2 != null)
            {
                IInputElement target = commandSource.CommandTarget ?? (commandSource as IInputElement);
                return command2.CanExecute(commandParameter, target);
            }
            return command.CanExecute(commandParameter);
        }

 

 

    }
}
