﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Core.SmartApi
{
    public static class ExtensionsUtils
    {
        internal static ISmartApiResolver Resolver;

        public static void RequireAssembly(params ExternalAssembly[] assemblies_)
        {
            if (Resolver != null) Resolver.RequireAssembly(assemblies_);
        }
    }

    public enum ExternalAssembly
    {
        InfragisticsDragAndDrop,
        SharpZipLib,
        AttachLink,
        WindowsApiCodePackShell,
        MSDesktopIPC,
        MSDesktopEntitlement,
        MSDesktopServiceDiscovery,
        ReactiveThreading,
        ReactiveInterfaces,
        ReactiveCore,
        ReactiveLinq,
        NewtonSoftJson,
        XceedWpfToolkit,
        InfragisticsPersistence,
        InfragisticsXamColorPicker,
        Interactivity,
        Infragistics,
        InfragisticsRibbon,
        InfragisticsEditors,
        InfragisticsDockManager,
        InfragisticsDataPresenter,
        InfragisticsXamMenu,
        InfragisticsXamOutlookBar,
        MicrosoftComposite,
        MicrosoftCompositePresentation,
        MicrosoftCompositeUnityExtensions,
        MicrosoftCompositeObjectBuilder2,
        MicrosoftServiceLocation,
        MicrosoftUnity,
        DnpartsMSXml,
        DnpartsMSTools,
        DnpartsMSLog,
        DnpartsMSNetNative,
        DnpartsMSNet,
        DnpartsChannels,
        DnpartsChannelsCps,
        DnpartsProtocolBuffers,
        DnpartsCafDispatch, 
        MSDotNetRuntime,
        ConcordRuntime,
        ConcordLogger,
        ConcordApplication,
        ConcordTopicMediator,
        ConcordConfiguration,
        Log4net,
        GoogleProtocolBuffers,
        MSDesktopDebugging,
        MSDesktopViewModelAPI,
        MSDesktopWorkspaces,
        MSDesktopEnvironment,
        MSDesktopModuleEntitlements,
        MSDesktopMessageRouting,
        MSDesktopMessageRoutingInterfaces,
        MSDesktopMessageRoutingGUI,
        MSDesktopMessageRoutingHistory,
        MSDesktopMessageRoutingHistoryGUI,
        MSDesktopMessageRoutingMonitor,
        MSDesktopHTTPServer,
        MSDesktopMailService,
        MSDesktopWormhole,
        MSDesktopAuthenticationService,
        MSDesktopWebSupport,
        MSDesktopWebSupportCEF,
        MSDesktopCrossMachineApplication,
        MSDesktopIsolation,
        MSDesktopIsolationInterfaces,
        MSDesktopAspectProvider,
        PostSharp,
        CefSharp,
        CefSharpWpf
    }

    public enum MSDesktopApplication
    {
        IsolationHostProcess,
        IsolationHostProcessX86,
        Launcher,
        LoaderConfigEditor,
        LogViewer, 
    }

    internal struct ExternalAssemblyDescriptor
    {
        public AssemblyName Name { get; set; }
        public string AfsPath { get; set; }
    }
}
