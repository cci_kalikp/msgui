﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Core.SmartApi
{
    interface ISmartApiResolver
    {
        void RequireAssembly(params ExternalAssembly[] assemblies_);
    }
}
