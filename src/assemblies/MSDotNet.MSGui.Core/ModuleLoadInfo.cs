﻿using System;
using System.Collections.ObjectModel;

namespace MorganStanley.MSDotNet.MSGui.Core
{

    public enum ModuleType
    {
        Normal,
        ConcordApplet,
        BootModule,
    }

    public interface IModuleLoadInfo
    {
        string FullTypeName { get; set; }
        string ModuleName { get; }
        int LoadOrder { get; }

        long LoadingTime { get; set; }
        Collection<string> DependsOnModuleNames { get; set; }
        ModuleType Type { get; set; }
        ModuleStatus Status { get; set; }
        Exception Error { get; set; }
        string RejectReason { get; set; }
        string TypeNotFoundReason { get; set; }
        bool OutOfProcess { get; set; }
        bool IsEarlyModule { get; }
    }
}
