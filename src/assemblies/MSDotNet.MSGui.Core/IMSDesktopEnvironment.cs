﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Core
{
    public interface IMSDesktopEnvironment
    {
        /// <summary>
        /// The environment of the user will be one of Prod, Uat, Qa, Dev
        /// </summary>
        Environ Environment { get; }

        /// <summary>
        /// The region of the user will be one of HK, LN, TK, NY
        /// </summary>
        EnvironmentRegion Region { get; }

        /// <summary>
        /// The lowercase username of the user (after possible impersonation)
        /// </summary>
        string User { get; }

        string Subenvironment { get; set; }
    }

    public enum Environ
    {
        N_A = -1,
        Prod,
        UAT,
        QA,
        Dev        
    }

    public enum EnvironmentRegion
    {
        N_A = -1,
        LN,
        EU = LN,
        NY,
        HK,
        TK
    }
}
