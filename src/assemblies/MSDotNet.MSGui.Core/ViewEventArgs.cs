﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/ViewEventArgs.cs#5 $
// $Change: 825522 $
// $DateTime: 2013/04/22 14:13:45 $
// $Author: smulovic $

using System;

namespace MorganStanley.MSDotNet.MSGui.Core
{

    public class VisibilityEventArgs : EventArgs
    {
        public VisibilityEventArgs(bool visible)
        {
            Visible = visible;
        }
        public bool Visible { get; private set; }
    }
  /// <summary>
  /// <see cref="EventArgs"/> associated with a specific 
  /// <see cref="IViewContainer"/>.
  /// </summary>
  public class ViewEventArgs : EventArgs
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="ViewEventArgs"/> class.
    /// </summary>
    /// <param name="viewContainer_">viewContainer associated with this event.</param>
    public ViewEventArgs(IViewContainer viewContainer_)
    {
      ViewContainer = viewContainer_;
    }

    /// <summary>
    /// <see cref="IViewContainer"/> associated with this event.
    /// </summary>
    public IViewContainer ViewContainer
    {
      get;
      private set;
    }
  }
}