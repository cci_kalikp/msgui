﻿
using System;

namespace MorganStanley.MSDotNet.MSGui.Core
{
    public sealed class ContentChangedEventArgs : EventArgs
    {
        private readonly object _oldContent;
        private readonly object _newContent;

        internal ContentChangedEventArgs(object oldContent, object newContent)
        {
            _oldContent = oldContent;
            _newContent = newContent;
        }

        public object OldContent
        {
            get { return _oldContent; }
        }

        public object NewContent
        {
            get { return _newContent; }
        }
    }
}
