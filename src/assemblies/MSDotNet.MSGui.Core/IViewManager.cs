﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/IViewManager.cs#8 $
// $Change: 823929 $
// $DateTime: 2013/04/11 16:24:49 $
// $Author: smulovic $

using System;
using System.Xml.Linq;
using System.Collections.ObjectModel;

namespace MorganStanley.MSDotNet.MSGui.Core
{
	/// <summary>
	/// Manager for set of views
	/// </summary>
	[Obsolete("Use the IChromeRegistry/IChromeManager API instead.")]
	public interface IViewManager
	{
		/// <summary>
		/// Gets a value indicating the currently active <see cref="IViewContainer"/>.
		/// </summary>
		IViewContainer ActiveViewContainer
		{
			get;
		}

		/// <summary>
		/// Gets a collection of all the <see cref="IViewContainer"/>s.
		/// </summary>
		ObservableCollection<IViewContainer> Views
		{
			get;
		}

		/// <summary>
		/// Registers a way to create a non-persistent view.
		/// Id specified in the first argument must be unique for the whole application.
		/// It's recommended to use module name in its prefix to avoid name id collision with other modules.
		/// </summary>
		/// <param name="id_">Unique id for this way of creating views_</param>
		/// <param name="initialiseViewHandler_">Delegates that initializes new IView instance by setting its properties.</param>
		void AddViewCreator(string id_, InitialiseViewHandler initialiseViewHandler_);

		/// <summary>
		/// Registers a way to create and save a persistent view.
		/// Id specified in the first argument must be unique for the whole application.
		/// It's recommended to use module name in its prefix to avoid name id collision with other modules.
		/// </summary>
		/// <param name="id_">Unique id for this way of creating views_</param>
		/// <param name="stateToViewHandler_">Function that initializes IView using contents of xml</param>
		/// <param name="viewtoState_">Function that saves the current state of window to xml</param>
		void AddViewCreator(string id_, RestoreViewHandler stateToViewHandler_, SaveViewHandler viewtoState_);

		/// <summary>
		/// Show window, with registeren generator or regenerator with this ID.
		/// </summary>
		/// <param name="creatorId_">ID of generator or regenerator</param>
		/// <returns>IView for newly created window</returns>
		IViewContainer CreateView(string creatorId_);

		/// <summary>
		/// Show window, with registeren generator or regenerator with this ID.
		/// </summary>
		/// <param name="creatorId_">ID of generator or regenerator</param>
		/// /// <param name="viewState_">Xml representing state of a window. Ignored for non-persistent views</param>
		/// <returns>IView for newly created window</returns>
		IViewContainer CreateView(string creatorId_, XDocument viewState_);

    /// <summary>
    /// Show window in singleton mode, with registered generator or regenerator with this ID
    /// </summary>
    /// <param name="creatorId_">ID of generator or regenerator</param>
    /// <returns>IView for newly created window</returns>
    IViewContainer CreateSingletonView(string creatorId_);

    /// <summary>
    /// Show window in singleton mode, with registered generator or regenerator with this ID
    /// </summary>
    /// <param name="creatorId_">ID of generator or regenerator</param>
    /// <param name="viewState_">Xml representing state of a window. Ignored for non-persistent views</param>
    /// <returns>IViewContainer for newly created window</returns>
    IViewContainer CreateSingletonView(string creatorId_, XDocument viewState_);

		//IViewContainer CreateView(string creatordId_, IViewContainer parentTile_);


		/// <summary>
		/// Show window, with registeren generator or regenerator with this ID.
		/// </summary>
		/// <param name="creatordId_">ID of generator or regenerator</param>
		/// <param name="initialLocation_">Tells where the new view will be docked</param>
		/// <returns>IView for newly created window</returns>
		//IViewContainer CreateView(string creatorId_, InitialLocation initialLocation_);

		/// <summary>
		/// Show window, with registeren generator or regenerator with this ID.
		/// </summary>
		/// <param name="creatordId_">ID of generator or regenerator</param>
		/// <param name="initialLocation_">Tells where the new view will be docked</param>
		/// /// <param name="viewState_">Xml representing state of a window. Ignored for non-persistent views</param>
		/// <returns>IView for newly created window</returns>
		//IViewContainer CreateView(string creatorId_, XDocument viewState_, InitialLocation initialLocation_);

		/// <summary>
		/// returns a dialog that could be shown after Content property is assigned
		/// </summary>    
		/// <returns></returns>
		IDialogWindow CreateDialog();

		/// <summary>
		/// Saves state of a given viewcontainer only.
		/// </summary>
		/// <param name="container_">viewContainer</param>
		void SaveViewContainer(IViewContainer container_);

		/// <summary>
		/// Notification that a view has been activated.
		/// </summary>
		event EventHandler<ViewEventArgs> Activated;

		/// <summary>
		/// Notification that a view has been Deactivated.
		/// </summary>
		event EventHandler<ViewEventArgs> Deactivated;

		/// <summary>
		/// Notification that a view has been added.
		/// </summary>
		event EventHandler<ViewEventArgs> ViewAdded;
	}

#pragma warning disable 612,618
	public delegate void InitialiseViewHandler(IViewContainer emptyViewContainer_);
	public delegate void RestoreViewHandler(XDocument savedState_, IViewContainer emptyViewContainer_);
	public delegate XDocument SaveViewHandler(IViewContainer viewContainer_);
#pragma warning restore 612,618
}