﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/ViewCollection.cs#5 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace MorganStanley.MSDotNet.MSGui.Core
{
  /// <summary>
  /// Collection of <see cref="IViewContainer"/>'s.
  /// </summary>
  public class ViewCollection : ICollection<IViewContainer>
  {
		private readonly ObservableCollection<IViewContainer> m_views;

    public ViewCollection(ViewCollection collection_)
    {
			m_views = new ObservableCollection<IViewContainer>(collection_.m_views);
    }

    public ViewCollection()
    {
			m_views = new ObservableCollection<IViewContainer>();
		}


		#region ObservableCollection members passthrough
		public virtual event NotifyCollectionChangedEventHandler CollectionChanged
		{
			add
			{
				this.m_views.CollectionChanged += value;
			}
			remove
			{
				this.m_views.CollectionChanged -= value;
			}
		}

		public void Move(int oldIndex, int newIndex)
		{
			this.m_views.Move(oldIndex, newIndex);
    }
		#endregion


    #region ICollection<IViewContainer> Members

    public void Add(IViewContainer item_)
    {
      m_views.Add(item_);
    }

    public void Clear()
    {
      m_views.Clear();
    }

    public bool Contains(IViewContainer item_)
    {
      return m_views.Contains(item_);
    }

    public void CopyTo(IViewContainer[] array_, int arrayIndex_)
    {
      m_views.CopyTo(array_, arrayIndex_);
    }

    public int Count
    {
      get { return m_views.Count; }
    }

    public bool IsReadOnly
    {
      get { return false; }
    }

    public bool Remove(IViewContainer item_)
    {
      return m_views.Remove(item_);
    }

    #endregion

    #region IEnumerable<IViewContainer> Members

    public IEnumerator<IViewContainer> GetEnumerator()
    {
      return m_views.GetEnumerator();
    }

    #endregion

    #region IEnumerable Members

    System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
    {
      return m_views.GetEnumerator();
    }

    #endregion

    #region ICollection<IViewContainer> Members

    void ICollection<IViewContainer>.Add(IViewContainer item_)
    {
      m_views.Add(item_);
    }

    void ICollection<IViewContainer>.Clear()
    {
      m_views.Clear();
    }

    bool ICollection<IViewContainer>.Contains(IViewContainer item_)
    {
      return m_views.Contains(item_);
    }

    void ICollection<IViewContainer>.CopyTo(IViewContainer[] array_, int arrayIndex_)
    {
      m_views.CopyTo(array_, arrayIndex_);
    }

    int ICollection<IViewContainer>.Count
    {
      get { return m_views.Count; }
    }

    bool ICollection<IViewContainer>.IsReadOnly
    {
      get { return false; }
    }

    bool ICollection<IViewContainer>.Remove(IViewContainer item_)
    {
      return m_views.Remove(item_);
    }

    #endregion

    #region IEnumerable<IViewContainer> Members

    IEnumerator<IViewContainer> IEnumerable<IViewContainer>.GetEnumerator()
    {
      return m_views.GetEnumerator();
    }

    #endregion
  }
}
