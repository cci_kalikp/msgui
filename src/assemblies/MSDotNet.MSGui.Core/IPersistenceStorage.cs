/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/IPersistenceStorage.cs#9 $
// $Change: 868051 $
// $DateTime: 2014/02/24 01:36:05 $
// $Author: caijin $

using System;
using System.Xml.Linq;

namespace MorganStanley.MSDotNet.MSGui.Core
{
    /// <summary>
    /// Ability to store state in a location
    /// </summary>
    public interface IPersistenceStorage
    {
        /// <summary>
        /// Store the state.
        /// </summary>
        /// <param name="state">State to store.</param>
        /// <param name="name">Profile state is associated with.</param>
        void SaveState(XDocument state, string name);

        /// <summary>
        /// Load all the state associated with a profile.
        /// </summary>
        /// <param name="name">Profile to load.</param>
        /// <returns>Loaded state.</returns>
        XDocument LoadState(string name);

        /// <summary>
        /// Delete the state associated with a profile.
        /// </summary>
        /// <param name="name">Profile to remove.</param>
        void DeleteState(string name);

        /// <summary>
        /// Rename the profile.
        /// </summary>
        /// <param name="oldName">Profile to be renamed.</param>
        /// <param name="newName">New name.</param>
        void RenameState(string oldName, string newName);

        /// <summary>
        /// List of profiles stored in the storage.
        /// </summary>
        ReadOnlyProfilesCollection AvailableProfiles { get; }

        /// <summary>
        /// Initialize the persistence storage
        /// </summary>
        /// <remarks>
        /// InitializeStorage may depends on initialization of ConcordConfigurationManager.Container property
        /// </remarks>
        void InitializeStorage();

        /// <summary>
        /// Apply the Persistence Storage. when used in Concord enabled application, could be used to initialize layout
        /// </summary>
        /// <remarks>
        /// Apply method may depends on initialization of ConcordConfigurationManager.Container property
        /// </remarks>
        void Apply();

        /// <summary>
        /// Default
        /// </summary>
        string DefaultName { get; set; }
    }
}
