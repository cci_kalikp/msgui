﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using System.Windows;
using System.Windows.Input;

namespace MorganStanley.MSDotNet.MSGui.Core
{

  namespace Printing
  {
    // Conductor of the Window Print
    public interface IPrintManager
    {
      void Print(IWindowViewContainer windowContainer_, InitialPrintParameters printParameter_ = null);
      void PrintAll(InitialPrintParameters printParameter_ = null);

      void PreviewPrint(IWindowViewContainer windowContainer_, InitialPrintParameters printParameter_ = null);
      void PreviewPrintAll(InitialPrintParameters printParameter_ = null);
    }


    /// <summary>
    /// Print controller
    /// </summary>
    /// <remarks>
    /// A <see cref="IPrintManager"/> is the user interface to request print from the <see cref="IChromeManager"/>. This <see cref="IPrintController"/> internally 
    /// control things like Preview ...</remarks>
    internal interface IPrintController : IPrintPreview, IExecutePrint
    {
      InitialPrintParameters PrintParameters { get; set; }
    }

    internal interface IPrintPreview
    {
      bool ShowPreview(); 
    }

    internal interface IExecutePrint
    {
      void ExecutePrint(object parameter);
    }

    
    /// <summary>
    /// Marker interface to Additional Print Options
    /// </summary>
    public abstract class PrintOptions
    {
    }

    /// <summary>
    /// Default impl to the PrintOptions
    /// </summary>
    public class DefaultPrintOptions : PrintOptions
    {
    }
 }
}
