﻿using System;
using System.Windows;

namespace MorganStanley.MSDotNet.MSGui.Core
{
    public interface IHidingLockingManager
    {
        HideLockUIGranularity Granularity { get; set; }
        event EventHandler HideUIGranularityChanged;
        void ToggleFlag(HideLockUIGranularity flag);
        Visibility GetVisibilityForElement(HideLockUIGranularity element);
    }

    /// <summary>
    /// Allows to lock and hide different parts of the shell. Some of the settings do only
    /// take effect when used with the boot time extention method HideOrLockShellElements and
    /// some of them might be dynamically changed at runtime using IHidingLockingManager.
    /// 
    /// The list of items honored at runtime:
    /// * HideViewTearOffButton
    /// * HideViewPinButton
    /// * DisableViewFloatingOnly
    /// * HideViewCloseButton
    /// * DisableViewHeaderContextMenuClose
    /// * DisableViewTearOff
    /// * DisableViewDocking
    /// * DisableViewClose
    /// </summary>
    [Flags]
    public enum HideLockUIGranularity
    {
        None = 0,
        HideViewMinimizeButton = 1 << 0, //supported in harmonia
        HideViewMaximizeButton = 1 << 1,//supported in harmonia
        HideViewTearOffButton = 1 << 2,//supported in harmonia
        HideViewPinButton = 1 << 3,
        LockViewDragDrop = 1 << 4,//supported in harmonia
        HideRibbon = 1 << 5,
        HideLauncherBar = 1 << 6,
        HideStatusBar = 1 << 7,
        DontAppendProfileToWindowTitle = 1 << 8,
        DisableViewFloatingOnly = 1 << 9,//supported in harmonia
        HideNewTabButton = 1 << 10,
        DisableTabwellContextMenu = 1 << 11,
        HideViewCloseButton = 1 << 12,//supported in harmonia
        DisableViewHeaderContextMenuClose = 1 << 13,//supported in harmonia
        DisableSubtabContextMenuMove = 1 << 14,//supported in harmonia
        DisableViewHeaderContextMenuRename = 1 << 15,//supported in harmonia
        DisableViewTearOff = (1 << 16) | HideViewTearOffButton,//supported in harmonia
        DisableViewMaximize = (1 << 17) | HideViewMaximizeButton,//supported in harmonia
        DisableTabClose = 1 << 18,
        DisableTabMove = 1 << 19,
        DisableTabRename = 1 << 20,
        HideMainTabwell = 1 << 21,
        DisableViewDocking = 1 << 22,//this seems doesn't take effect for floating windows
        HideHeaderIcon = 1 << 23,//supported in harmonia
        HideEnvironmentLabelling = 1 << 24,//supported in harmonia
        LockRelativeWindowsPositions = 1 << 25,
        HideEnvironmentLabellingForDockedWindows = 1 << 26,         
        WorkspaceShellStyleDragDropLock = 1 << 27,
        DisableResizingViews = 1 << 28,//supported in harmonia
        /// <summary>
        /// hide the main status bar item on the left side of the main window
        /// </summary>
        HideStatusBarNotifications = 1 << 29,
        /// <summary>
        /// don't close the main window when double click the orb on main ribbon window
        /// </summary>
        DisableCloseWindowByOrbDoubleClick = 1 << 30,
        HideViewLockButton = 1 << 31, //supported only in harmonia
        DisableViewClose = DisableViewHeaderContextMenuClose | HideViewCloseButton,
        /// <summary>
        /// Reflects the obsolete SetLockedLayoutMode setting.
        /// </summary>
        LegacyLockedLayoutMode = HideViewMaximizeButton |
                            HideViewMinimizeButton |
                            HideViewPinButton |
                            HideViewTearOffButton |
                            LockViewDragDrop |
                            HideRibbon |
                            HideLauncherBar |
                            DontAppendProfileToWindowTitle |
                            DisableViewFloatingOnly |
                            HideNewTabButton |
                            DisableTabwellContextMenu |
                            HideViewCloseButton,
        /// <summary>
        /// Completely locked layout. A default layout file should be provided.
        /// </summary>
        LockedLayoutMode = ((1 << 23) - 1 - HideMainTabwell), // everything apart from HideMainTabwell
        /// <summary>
        /// Allows the user to create and close views. Initial location should be provided for views.
        /// </summary>
        LockedLayoutSoftMode = LockedLayoutMode
            - DisableViewClose
            - HideRibbon
            - HideLauncherBar
            - HideStatusBar,
    }

}
