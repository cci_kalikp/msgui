﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace MorganStanley.MSDotNet.MSGui.Core
{
	internal class NativeMethods
	{
		[return: MarshalAs(UnmanagedType.Bool)]
		[DllImport("gdi32", ExactSpelling = true, CharSet = System.Runtime.InteropServices.CharSet.Auto)]
		internal static extern bool DeleteObject(IntPtr hObject);
	}
}
