﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/IPersistenceService.cs#13 $
// $Change: 901195 $
// $DateTime: 2014/10/16 05:40:06 $
// $Author: caijin $

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Xml.Linq;

namespace MorganStanley.MSDotNet.MSGui.Core
{
    /// <summary>
    /// Persistence service.
    /// </summary>
    public interface IPersistenceService
    {
        /// <summary>
        /// This methods allows you to add a couple of functions for saving/restoring states of your objects
        /// </summary>
        /// <param name="regeneratorId">unique id for this persistor identified with saved block of xml in the profile specific saved state</param>
        /// <param name="restoreStateHandler">A function that generates xml to be saved</param>
        /// <param name="saveStateHandler">A function that restores state saved in xml</param>
        void AddPersistor(string regeneratorId, RestoreStateHandler restoreStateHandler,
                          SaveStateHandler saveStateHandler);

        /// <summary>
        /// Remove the profile specific persistor added
        /// </summary>
        /// <param name="persistorId">unique id for this persistor identified with saved block of xml in the profile specific saved state</param>
        void RemovePersistor(string persistorId);

        /// <summary>
        /// This methods allows you to add a couple of functions for saving/restoring states of your objects that are not layout specific
        /// </summary>
        /// <param name="persistorId">unique id for this persistor identified with saved block of xml in the global saved state</param>
        /// <param name="restoreStateHandler">A function that generates xml to be saved</param>
        /// <param name="saveStateHandler">A function that restores state saved in xml</param>
        void AddGlobalPersistor(string persistorId, RestoreStateHandler restoreStateHandler,
                                SaveStateHandler saveStateHandler);

        /// <summary>
        /// Remove the global persistor added
        /// </summary>
        /// <param name="persistorId">unique id for this persistor identified with saved block of xml in the global saved state</param>
        void RemoveGlobalPersistor(string persistorId);

        /// <summary>
        /// This method loads profile with the given name. The result is exactly the same as if the user
        /// used the GUI to perform this operation.
        /// </summary>
        /// <param name="name">The name of the profile to load</param>
        void LoadProfile(string name);

        /// <summary>
        /// This method prompts the user to enter the profile name and saves the profile under
        /// the given name. The result is exactly the same as if the user
        /// used the GUI to perform this operation.
        /// </summary>
        void SaveProfileAs();

        /// <summary>
        /// This method saves the profile under the given name. If a profile with the given name
        /// exists, it will be overwritten.
        /// </summary>
        void SaveProfileAs(string name);

        /// <summary>
        /// This saves changes to the current profile. The result is exactly the same as if the user
        /// used the GUI to perform this operation.
        /// </summary>
        void SaveCurrentProfile();

        /// <summary>
        /// This method returns a list containing names of available profiles.
        /// </summary>
        /// <returns></returns>
        ObservableCollection<string> GetProfileNames();

        /// <summary>
        /// This method deletes the current profile. The result is exactly the same as if the user
        /// used the GUI to perform this operation.
        /// </summary>
        void DeleteCurrentProfile();

        event EventHandler<ProfileSavedEventArgs> ProfileSaved;

        /// <summary>
        /// This method return the name of the currently loaded profile.
        /// </summary>
        /// <returns>Null if no profile is currently loaded.</returns>
        string GetCurrentProfileName();

        /// <summary>
        /// Sets the current profile as default.
        /// </summary>
        /// <param name="name"></param>
        void MakeProfileDefault(string name);

        void PersistGlobalItems(IEnumerable<string> keys = null);


        [EditorBrowsable(EditorBrowsableState.Never)]
        void PersistCurrentProfileItems(IEnumerable<string> keys);


        [EditorBrowsable(EditorBrowsableState.Never)]
        void PersistProfileItems( string profileName, IEnumerable<string> keys);
         

        [EditorBrowsable(EditorBrowsableState.Never)]
        void LoadGlobalItems(IEnumerable<string> keys = null);

        [EditorBrowsable(EditorBrowsableState.Never)]
        void LoadCurrentProfileItems(IEnumerable<string> keys);

        [EditorBrowsable(EditorBrowsableState.Never)]
        void LoadProfileItems(string profileName, IEnumerable<string> keys);
    }
    public class ProfileSavedEventArgs : EventArgs
    {
        public ProfileSavedEventArgs(string profileSaved_)
        {
            ProfileSaved = profileSaved_;
        }

        public string ProfileSaved { get; private set; }
    }
    public delegate XDocument SaveStateHandler();
  public delegate void RestoreStateHandler(XDocument xmlState_);
}
