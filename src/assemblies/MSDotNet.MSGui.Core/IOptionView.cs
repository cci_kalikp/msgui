﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/IOptionView.cs#13 $
// $Change: 879655 $
// $DateTime: 2014/05/05 06:52:42 $
// $Author: caijin $

using System.Windows;
using System.ComponentModel;

namespace MorganStanley.MSDotNet.MSGui.Core
{
    /// <summary>
    /// Define a control which can be displayed as 
    /// an options page.
    /// </summary>
    public interface IOptionView
    {


        /// <summary>
        /// Notifies the <see cref="IOptionView"/>
        /// that 'OK' was selected.
        /// </summary>
        void OnOK();

        /// <summary>
        /// Notifies the <see cref="IOptionView"/>
        /// that 'Cancel' was selected.
        /// </summary>
        void OnCancel();

        /// <summary>
        /// Notifies the <see cref="IOptionView"/>
        /// that it is about to be shown.
        /// </summary>
        void OnDisplay();

        /// <summary>
        /// Notifies the <see cref="IOptionView"/>
        /// that 'Help' was selected.
        /// </summary>
        void OnHelp();

        /// <summary>
        /// Gets the content of the <see cref="IOptionView"/>.
        /// </summary>
        UIElement Content { get; }
    }


    public interface IDisposableOptionView:IOptionView
    {
        void OnLeave();
    }

    public interface IValidatableOptionView : IOptionView
    {
        /// <summary>
        /// Before OK, validate if failed, don't proceed to OnOK
        /// </summary>
        /// <returns></returns>
        bool OnValidate();

        /// <summary>
        /// If validation failed, this can be implemented to activate error control
        /// </summary>
        void OnInvalid();
    }

    public interface IOptionViewWithModel:IOptionView
    {
        object ViewModel { get; } 
    }
     
    public enum LocateSubItemMethod
    {
        None,
        Default,
        Custom,
    }
    public interface IImplicitSaveOptionView : IOptionView
    {
        
    }
}
