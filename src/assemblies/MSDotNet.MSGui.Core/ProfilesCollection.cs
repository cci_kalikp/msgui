﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/ProfilesCollection.cs#5 $
// $Change: 844450 $
// $DateTime: 2013/09/03 07:09:11 $
// $Author: caijin $

using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Windows.Threading;

namespace MorganStanley.MSDotNet.MSGui.Core
{
  /// <summary>
  /// This class is an observablecollection which invokes automatically.
  /// This means that any change will be done in the right thread.
  /// </summary>  
  public class ProfilesCollection : ObservableCollection<string>
  {

    public ProfilesCollection()
    {
      _currentDispatcher = Dispatcher.CurrentDispatcher;
    }

    //since we use directory name to categorize different profile
    //and windows file system is case insensitive to folder names,
    //we need to compare in a case insensitive way
    public string FindProfile(string profile_)
    {
        if (string.IsNullOrEmpty(profile_)) return profile_;
        foreach (string profile in this)
        {
            if (string.Compare(profile, profile_, true) == 0)
            {
                return profile;
            }
        }
        return null;
    }

    //since we use directory name to categorize different profile
    //and windows file system is case insensitive to folder names,
    //we need to compare in a case insensitive way
    public new bool Contains(string profile_)
    {
        string profile = FindProfile(profile_);
        return profile != null;
    }

    private readonly Dispatcher _currentDispatcher;

    private void DoDispatchedAction(Action action)
    {
      if (_currentDispatcher.CheckAccess())
        action.Invoke();
      else
        _currentDispatcher.Invoke(DispatcherPriority.DataBind, action);
    }

    protected override void ClearItems()
    {
      DoDispatchedAction(BaseClearItems);
    }

    private void BaseClearItems()
    {
      base.ClearItems();
    }

    protected override void InsertItem(int index, string item)
    {
      DoDispatchedAction(() => BaseInsertItem(index, item));
    }

    private void BaseInsertItem(int index, string item)
    {
      base.InsertItem(index, item);
    }

    protected override void MoveItem(int oldIndex, int newIndex)
    {
      DoDispatchedAction(() => BaseMoveItem(oldIndex, newIndex));
    }

    private void BaseMoveItem(int oldIndex, int newIndex)
    {
      base.MoveItem(oldIndex, newIndex);
    }

    protected override void RemoveItem(int index)
    {
      DoDispatchedAction(() => BaseRemoveItem(index));
    }

    private void BaseRemoveItem(int index)
    {
      base.RemoveItem(index);
    }

    protected override void SetItem(int index, string item)
    {
      DoDispatchedAction(() => BaseSetItem(index, item));
    }

    private void BaseSetItem(int index, string item)
    {
      base.SetItem(index, item);
    }

    protected override void OnCollectionChanged(System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
    {
      DoDispatchedAction(() => BaseOnCollectionChanged(e));
    }

    private void BaseOnCollectionChanged(NotifyCollectionChangedEventArgs e)
    {
      base.OnCollectionChanged(e);
    }

    protected override void OnPropertyChanged(PropertyChangedEventArgs e)
    {
      DoDispatchedAction(() => BaseOnPropertyChanged(e));
    }

    private void BaseOnPropertyChanged(PropertyChangedEventArgs e)
    {
      base.OnPropertyChanged(e);
    }
  }

}
