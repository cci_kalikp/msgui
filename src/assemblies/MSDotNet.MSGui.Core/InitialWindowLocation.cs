﻿using System.ComponentModel;

namespace MorganStanley.MSDotNet.MSGui.Core
{   
 
    public enum InitialWindowLocation
    { 
        /// <summary>
        /// Start up as a floating window(can be docked in tab) in random location
        /// </summary>
        FloatingAnywhere = 1,
         
        /// <summary>
        /// Start up as a floating window(can be docked in tab) at the current cursor place
        /// </summary>
        FloatingOnlyAnywhere = 2,

        /// <summary>
        /// Start up as a floating only window(cannot be docked in tab) at the current cursor place
        /// </summary>
        FloatingAtCursor = FloatingAnywhere + 8,

        /// <summary>
        /// Start up as a floating only window(cannot be docked in tab) at the current cursor place
        /// </summary>
        FloatingOnlyAtCursor = FloatingOnlyAnywhere + 8,

        /// <summary>
        /// Start up in the new tabwell, only valid for ribbon shell
        /// </summary>
        DockInNewTab = 4, 
        
        /// <summary>
        /// Start up in the active tab or tab specified by "WindowTabName", only valid for ribbon shell
        /// </summary>
        DockLeft = 64,
 
        /// <summary>
        /// Start up in the top of the active tab or tab specified by "WindowTabName", only valid for ribbon shell
        /// </summary>
        DockTop = 128,
         
        /// <summary>
        /// Start up in the right of the active tab or tab specified by "WindowTabName", only valid for ribbon shell
        /// </summary>
        DockRight = 256,
         
        /// <summary>
        /// Start up in the bottom of the active tab or tab specified by "WindowTabName", only valid for ribbon shell
        /// </summary>
        DockBottom = 512,
 
    };
      
}
