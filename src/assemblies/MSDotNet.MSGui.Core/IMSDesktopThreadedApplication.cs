﻿using System.Windows.Threading;

namespace MorganStanley.MSDotNet.MSGui.Core
{
    internal interface IMSDesktopThreadedApplication : IApplication
    {
        Dispatcher MainWindowDispatcher { get; }
    }
}

