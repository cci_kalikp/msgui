﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace MorganStanley.MSDotNet.MSGui.Core
{
	public interface IConcordTypeToViewCreator
	{
		/// <summary>
		/// Maps a concord applet to a ChromeManager window factory.
		/// </summary>
		/// <param name="concordAppletFQN">The fully qualified name of the concord applet.</param>
		/// <param name="creatorID">The ChromeManager window factory ID.</param>
		void Map(string concordAppletFQN, string creatorID);

		/// <summary>
		/// Maps a concord applet to a ChromeManager window factory.
		/// </summary>
		/// <param name="concordAppletFQN">The fully qualified name of the concord applet.</param>
		/// <param name="creatorID">The ChromeManager window factory ID.</param>
		/// <param name="converter">The state converter function.</param>
		void Map(string concordAppletFQN, string creatorID, Func<XElement, XElement> converter);

		/// <summary>
		/// Maps a concord applet to a ChromeManager window factory using a callback.
		/// </summary>
		/// <param name="concordAppletFQN">The fully qualified name of the concord applet.</param>
		/// <param name="getCreatorId">The function that maps the applet FQN to a window factory ID.</param>
		void Map(string concordAppletFQN, Func<string, XElement, string> getCreatorId);

		/// <summary>
		/// Maps a singleton concord applet to a ChromeManager window factory.
		/// </summary>
		/// <param name="concordAppletFQN">The fully qualified name of the concord applet.</param>
		/// <param name="creatorID">The ChromeManager window factory ID.</param>
		void MapSingleton(string concordAppletFQN, string creatorID);

		/// <summary>
		/// Maps a singleton concord applet to a ChromeManager window factory.
		/// </summary>
		/// <param name="concordAppletFQN">The fully qualified name of the concord applet.</param>
		/// <param name="creatorID">The ChromeManager window factory ID.</param>
		/// <param name="converter">The state converter function.</param>
		void MapSingleton(string concordAppletFQN, string creatorID, Func<XElement, XElement> converter);

		/// <summary>
		/// Maps a singleton concord applet to a ChromeManager window factory using a callback.
		/// </summary>
		/// <param name="concordAppletFQN">The fully qualified name of the concord applet.</param>
		/// <param name="getCreatorId">The function that maps the applet FQN to a window factory ID.</param>
		void MapSingleton(string concordAppletFQN, Func<string, XElement, Tuple<string, bool>> getCreatorId);
	}
}
