﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/Guard.cs#1 $
// $Change: 858188 $
// $DateTime: 2013/12/10 01:31:58 $
// $Author: caijin $

using System;
using System.Text.RegularExpressions;

namespace MorganStanley.MSDotNet.MSGui.Core
{
  /// <summary>
  /// Static helper class for checking input parameters.
  /// </summary>
  internal static class Guard
  {
    /// <summary>
    /// Throws <see cref="ArgumentNullException"/> if the specified argument is null.
    /// </summary>
    /// <param name="argument_">The argument to check.</param>
    /// <param name="argumentName_">The argument name.</param>
    public static void ArgumentNotNull(object argument_, string argumentName_)
    {
      if (argument_ == null)
      {
        throw new ArgumentNullException(argumentName_);
      }
    }

    /// <summary>
    /// Throws <see cref="ArgumentNullException"/> if the specified argument is null. If it is
    /// an empty string it throws <see cref="ArgumentException"/>.
    /// </summary>
    /// <param name="argument_">The argument to check.</param>
    /// <param name="argumentName_">The argument name.</param>
    public static void ArgumentNotNullOrEmptyString(string argument_, string argumentName_)
    {
      if (argument_ == null)
      {
        throw new ArgumentNullException(argumentName_);
      }
      if (argument_.Length == 0)
      {
        throw new ArgumentException(String.Format("'{0}' argument must not be empty string.", argumentName_));
      }
    }

    /// <summary>
    /// Throws <see cref="ArgumentException"/> if the specified argument contains one of the
    /// given characters.
    /// </summary>
    /// <param name="argument_">The argument to check.</param>
    /// <param name="invalidCharacters_">The invalid characters.</param>
    /// <param name="argumentName_">The argument name.</param>
    public static void ArgumentMustNotContain(string argument_, string invalidCharacters_, string argumentName_)
    {
      if (String.IsNullOrEmpty(argument_))
      {
        // does not contain anything
        return;
      }

      if (String.IsNullOrEmpty(invalidCharacters_))
      {
        // does not contain these
        return;
      }

      if (Regex.IsMatch(argument_, String.Format(@".*[{0}].*", Regex.Escape(invalidCharacters_))))
      {
        throw new ArgumentException(String.Format("'{0}' = '{1}' argument must not contain any of the following characters: '{2}'", argumentName_, argument_, invalidCharacters_));
      }
    }
  }
}