﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/StatusUpdatedEventArgs.cs#4 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;

namespace MorganStanley.MSDotNet.MSGui.Core
{
  public class StatusUpdatedEventArgs : EventArgs
  {
    /// <summary>
    /// Construct an Information status message.
    /// </summary>
    /// <param name="text_">Message</param>
    public StatusUpdatedEventArgs(string text_)
      : this(text_, StatusLevel.Information)
    {

    }

    /// <summary>
    /// Construct a <param name="level_"/> status message.
    /// </summary>
    /// <param name="text_">Message</param>
    public StatusUpdatedEventArgs(string text_, StatusLevel level_)
    {
      Level = level_;
      Text = text_;
      Time = DateTime.Now;
    }


    /// <summary>
    /// Gets the <see cref="StatusLevel"/>.
    /// </summary>
    public StatusLevel Level
    {
      get;
      private set;
    }

    /// <summary>
    /// Gets the text of the message.
    /// </summary>
    public string Text
    {
      get;
      private set;
    }
    
    /// <summary>
    /// Gets the time of the status update.
    /// </summary>
    public DateTime Time
    {
      get;
      private set;
    }
  }
}