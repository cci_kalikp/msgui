﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Core
{
    public interface IModuleInfoProvider
    {
        Type GetModuleType(Type wrapperType_);
    }
}
