﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/IViewContainer.cs#7 $
// $Change: 895526 $
// $DateTime: 2014/09/03 09:01:24 $
// $Author: serfozo $

using System.Collections.Generic;
using System.Drawing;
using System.ComponentModel;
using System;

namespace MorganStanley.MSDotNet.MSGui.Core
{
  /// <summary>
  /// Expose interactions with the view
  /// and it's host.
  /// </summary>
  [Obsolete("Use the IChromeManager/IChromeRegistry system with IWindowViewContainer instead.")]
  public interface IViewContainer
  {
    /// <summary>
    /// Gets or sets title of the view.
    /// </summary>
    string Title
    {
      get;
      set;
    }

    /// <summary>
    /// Gets or sets the default size of the view.
    /// </summary>
	[Obsolete("Use InitialViewSettings instead.")]
    System.Windows.Size DefaultSize
    {
      get;
      set;
    }

      /// <summary>
      /// Gets width of the view.
      /// </summary>
    double Width
    {
        get;
        set;
    }

      /// <summary>
      /// Gets height of the view.
      /// </summary>
    double Height
    {
        get;
        set;
    }

      double Top
      {
          get; 
      }

      double Left 
      { 
          get; 
      }

    /// <summary>
    /// Gets the object associated with this
    /// <see cref="IViewContainer"/>.
    /// </summary>
    object Content
    {
      get;
      set;
    }

    /// <summary>
    /// Gets the object associated with this
    /// <see cref="IViewContainer"/>.
    /// </summary>
    IList<object> HeaderItems
    {
      get;
    }

    /// <summary>
    /// Gets or sets visibilty of the view.
    /// </summary>
    bool IsVisible
    {
      get;
    }

    /// <summary>
    /// Gets a value indicating if a view
    /// is currently the active view
    /// </summary>
    bool IsActive
    {
      get;
    }

    /// <summary>
    /// Gets or sets the icon used to identify the view.
    /// </summary>
    Icon Icon
    {
      get;
      set;    
    }

    /// <summary>
    /// Gets or sets the background brush
    /// </summary>
    System.Windows.Media.Brush Background
    {
      get;
      set;
    }

    /// <summary>
    /// Gets a unique string id for this view container.
    /// </summary>
    string ID
    {
      get;
    }

	InitialViewSettings InitialViewSettings
	{
		get;
	}


    /// <summary>
    /// Close the view and removes it from the list
    /// of current views.
    /// </summary>
    void Close();

    /// <summary>
    /// Activates the view and makes it visible.
    /// </summary>
    void Activate();

    [Obsolete("Use Flash instead")]
      void StartFlash();
    [Obsolete("Use Flash instead")]
      void StopFlash();

    void Flash();

    void Flash(System.Windows.Media.Color color);

    /// <summary>
    /// Notification that a view is closing.
    /// </summary>
    event EventHandler<CancelEventArgs> Closing;

    /// <summary>
    /// Notifciation that a view is closed.
    /// </summary>
    event EventHandler<ViewEventArgs> Closed;
  }
}
