﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Core
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ModuleInfoProviderAttribute:Attribute
    {
        public ModuleInfoProviderAttribute(Type providerType_)
        {
            Provider = providerType_;
        }

        public ModuleInfoProviderAttribute()
        {
        }

        public Type Provider { get; set; }
    }
}
