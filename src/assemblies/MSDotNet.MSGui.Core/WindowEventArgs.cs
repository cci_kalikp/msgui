﻿using System;
using System.Windows;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Core
{
	/// <summary>
	/// <see cref="EventArgs"/> associated with a specific 
	/// <see cref="IWindowViewContainer"/>.
	/// </summary>
	public class WindowEventArgs : EventArgs
	{
	    private readonly IWindowViewContainer _viewContainer;

	    /// <summary>
		/// Initializes a new instance of the <see cref="WindowEventArgs"/> class.
		/// </summary>
		/// <param name="viewContainer_">viewContainer associated with this event.</param>
		public WindowEventArgs(IWindowViewContainer viewContainer)
	    {
	        _viewContainer = viewContainer;
	    }

	    /// <summary>
		/// <see cref="IWindowViewContainer"/> associated with this event.
		/// </summary>
		public IWindowViewContainer ViewContainer
		{
            get { return _viewContainer; }
		}
	}
}
