﻿using System; 
using System.Globalization; 
using System.Security;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Core
{
    public static class CoreUtilities
    {
        public static bool AreClose(double value1_, double value2_)
        {
            return ((value1_ == value2_) || (Math.Abs((double)(value1_ - value2_)) < 1E-3));
        } 
        public static CultureInfo GetNonNeutralCulture(CultureInfo culture_)
        {
            if (culture_ == null)
            {
                throw new ArgumentNullException("culture_");
            }
            if (!culture_.IsNeutralCulture)
            {
                return culture_;
            }
            CultureInfo currentCulture = null;
            try
            {
                foreach (CultureInfo info2 in CultureInfo.GetCultures(CultureTypes.SpecificCultures))
                {
                    if (culture_.Equals(info2.Parent))
                    {
                        currentCulture = info2;
                        goto Label_0059;
                    }
                }
            }
            catch (SecurityException)
            {
            }
        Label_0059:
            if (currentCulture == null)
            {
                currentCulture = CultureInfo.CurrentCulture;
            }
            if (currentCulture.IsNeutralCulture)
            {
                currentCulture = CultureInfo.InvariantCulture;
            }
            culture_ = currentCulture;
            return culture_;
        }
         

        public static object GetWeakReferenceTargetSafe(WeakReference weakReference_)
        {
            if (weakReference_ != null)
            {
                try
                {
                    return weakReference_.Target;
                }
                catch (Exception)
                {
                }
            }
            return null;
        }

 
        public static bool IsSubclassOf(Type type_, string superClassFullName_)
        {
            while (true)
            { 
                type_ = type_.BaseType;
                if (type_ == null) return false;
                if (type_.FullName == superClassFullName_)
                {
                    return true;
                }
            } 
        }

        public static string StripMnemonics(string text_, bool escapeRemainingMnemonics_)
        {
            if ((text_ == null) || (text_.Length < 2))
            {
                return text_;
            }
            int index = text_.IndexOf('_');
            if (index < 0)
            {
                return text_;
            }
            StringBuilder builder = new StringBuilder();
            if (index > 0)
            {
                builder.Append(text_.Substring(0, index));
            }
            bool flag = false;
            int num2 = index;
            int num3 = text_.Length - 1;
            while (num2 < num3)
            {
                char ch2;
                char ch = text_[num2];
                if (ch == '_')
                {
                    ch2 = text_[num2 + 1];
                    if (ch2 == '_')
                    {
                        if (escapeRemainingMnemonics_)
                        {
                            builder.Append('_');
                        }
                    }
                    else if (flag)
                    {
                        if (escapeRemainingMnemonics_)
                        {
                            builder.Append('_');
                        }
                        builder.Append('_');
                    }
                    else
                    {
                        flag = true;
                    }
                    builder.Append(ch2);
                    num2++;
                }
                else
                {
                    builder.Append(ch);
                }
                if (num2 == (num3 - 1))
                {
                    ch2 = text_[num2 + 1];
                    builder.Append(ch2);
                }
                num2++;
            }
            return builder.ToString();
        }

 

 


    }
}
