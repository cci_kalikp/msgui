﻿using System;
using System.Xml.Linq;
using System.ComponentModel;
using System.Windows;

namespace MorganStanley.MSDotNet.MSGui.Core
{
    public interface IOptionViewViewModel : ICloneable, INotifyPropertyChanged
    {
        /// <summary>
        /// Propagates the changes made by the user to the model.
        /// </summary>
        /// <param name="newViewModel_">A copy of the viewmodel containing the changes made by the user.</param>
        void AcceptChanges(IOptionViewViewModel newViewModel_);
        /// <summary>
        /// Validates the viewmodel. Invalid viewmodel cannot be saved.
        /// </summary>
        /// <returns>True if the data is valid.</returns>
        bool Validate(out string errorString_, out string invalidControlName_, out string invalidPropertyName_);
        /// <summary>
        /// Persists the state of the viewmodel. PersistenceExtensions can be used in the implementation.
        /// </summary>
        /// <returns></returns>
        XDocument Persist();
        /// <summary>
        /// Load the viewmodel from the XDocument. PersistenceExtensions can be used in the implementation.
        /// </summary>
        /// <param name="state"></param>
        void LoadState(XDocument state);

        bool HasCustomTemplate { get; }
    }
}