﻿using System.Xml.Linq;

namespace MorganStanley.MSDotNet.MSGui.Core
{
    internal interface IDehydratableElement
    {
        XDocument Dehydrate();
    }
}
