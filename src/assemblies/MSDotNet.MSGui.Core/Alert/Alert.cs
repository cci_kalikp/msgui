﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/Alert/Alert.cs#7 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

namespace MorganStanley.MSDotNet.MSGui.Core
{
  public class Alert
  {
    public AlertLevel AlertLevel { get; set; }
    public CounterAction CounterAction { get; set; }
    public string Text { get; set; }
    public IViewContainer ViewContainer { get; set; }
  }
}
