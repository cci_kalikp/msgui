﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/Alert/IAlert.cs#5 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

namespace MorganStanley.MSDotNet.MSGui.Core
{
    /// <summary>
    /// Represents the alerting subsystem
    /// </summary>
    /// <remarks>
    /// Just to remind you. We need an API to highlight/alert a view weather it’s docked or floating. 
    /// So when something happened in the view (let’s say a new trade) we can highlight it for user. 
    /// I.e. make it flashing or with right border or something else. 
    /// Also there should be a way to add number of elements in the view to the tab. 
    /// Let say I have MS GUI tab called “Inbox” with a single view in it. And when I get more items, it should show “Inbox (125)”.
    /// </remarks>
    public interface IAlert
    {
        void SubmitAlert(Alert alert_);
    }
}