﻿using System.ComponentModel;
using System.Windows;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;

namespace MorganStanley.MSDotNet.MSGui.Core
{
    public abstract class HidableStatusBarItemBase : ViewModelBase, IStatusBarItem, INotifyPropertyChanged
    {
        private bool visible = true;

        protected abstract UIElement UIElement { get; }

        public abstract void UpdateStatus(StatusLevel level_, string statusText_);
        public abstract void ClearStatus();
        public abstract event StatusUpdatedEventHandler StatusUpdated;
        public abstract string Name { get; }

        public object Control { get { return UIElement; } }

        public bool Visible
        {
            get
            {
                return visible;
            }
            set
            {
                visible = value;
                if (UIElement != null)
                {
                    UIElement.Visibility = value ? Visibility.Visible : Visibility.Collapsed;
                }
                OnPropertyChanged("Visible");
            }
        }
         
    }
}