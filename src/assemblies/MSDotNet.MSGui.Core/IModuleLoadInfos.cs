﻿using System.Collections.Generic;

namespace MorganStanley.MSDotNet.MSGui.Core
{
    public interface IModuleLoadInfos
    {
        List<IModuleLoadInfo> Infos { get; }
        IModuleLoadInfo AddModuleLoadInfo(object moduleInfo_);  //TODO:
        IModuleLoadInfo AddModuleLoadInfo(string moduleTypeName_, ModuleStatus status_ = ModuleStatus.NotLoaded); 
        IModuleLoadInfo GetModuleLoadInfo(string moduleTypeName_);
        void StartLoading(IModuleLoadInfo moduleLoadInfo_);
        void EndLoading(IModuleLoadInfo moduleLoadInfo_);
        bool HasErrorOnInit { get; }

    }
}