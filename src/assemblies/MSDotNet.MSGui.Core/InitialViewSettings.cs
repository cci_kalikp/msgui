﻿using System;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Core
{
    [Obsolete("The ControlParadigm approach with the InitialViewSettings class is obsolete. Use the new ChromeManager API Instead.")]
    public class InitialViewSettings
    {
        /// <summary>
        /// This is the new method we are providing an abstraction layer around for backwards compatibility.
        /// </summary>
        internal readonly InitialWindowParameters wrappedInitialWindowParameters = null;

        public InitialLocation InitialLocation
        {
            get
            {
                return this.wrappedInitialWindowParameters.InitialLocation;
            }
            set
            {
                this.wrappedInitialWindowParameters.InitialLocation = value;
            }
        }

        public SizingMethod SizingMethod
        {
            get
            {
                return this.wrappedInitialWindowParameters.SizingMethod;
            }
            set
            {
                this.wrappedInitialWindowParameters.SizingMethod = value;
            }
        }

        public double Width
        {
            get
            {
                return this.wrappedInitialWindowParameters.Width;
            }
            set
            {
                this.wrappedInitialWindowParameters.Width = value;
            }
        }

        public double Height
        {
            get
            {
                return this.wrappedInitialWindowParameters.Height;
            }
            set
            {
                this.wrappedInitialWindowParameters.Height = value;
            }
        }

        public bool EnforceSizeRestrictions
        {
            get
            {
                return this.wrappedInitialWindowParameters.EnforceSizeRestrictions;
            }
            set
            {
                this.wrappedInitialWindowParameters.EnforceSizeRestrictions = value;
            }
        }

        public bool PlaceAtCursor
        {
            get
            {
                return (this.wrappedInitialWindowParameters.InitialLocation & Core.InitialLocation.PlaceAtCursor) == Core.InitialLocation.PlaceAtCursor;
            }
            set
            {
                if (value && ((this.wrappedInitialWindowParameters.InitialLocation & Core.InitialLocation.PlaceAtCursor) != Core.InitialLocation.PlaceAtCursor))
                    this.wrappedInitialWindowParameters.InitialLocation = wrappedInitialWindowParameters.InitialLocation | Core.InitialLocation.PlaceAtCursor;
                else if (!value && ((this.wrappedInitialWindowParameters.InitialLocation & Core.InitialLocation.PlaceAtCursor) == Core.InitialLocation.PlaceAtCursor))
                    this.wrappedInitialWindowParameters.InitialLocation = wrappedInitialWindowParameters.InitialLocation ^ Core.InitialLocation.PlaceAtCursor;
            }
        }

        public InitialViewSettings()
        {
            this.wrappedInitialWindowParameters = new InitialWindowParameters();
        }

        public InitialViewSettings(InitialWindowParameters iwp)
        {
            this.wrappedInitialWindowParameters = iwp;
        }
    }
}
