﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/IStatusBar.cs#5 $
// $Change: 862682 $
// $DateTime: 2014/01/17 16:11:04 $
// $Author: hrechkin $

using System.Collections.Generic;

namespace MorganStanley.MSDotNet.MSGui.Core
{
  /// <summary>
  /// Represents the status bar area.
  /// </summary>
  public interface IStatusBar
  {
    /// <summary>
    /// Gets all available status bar items' keys.
    /// </summary>
    ICollection<string> Keys 
    {
      get; 
    }

    /// <summary>
    /// Get <see cref="IStatusBarItem"/> with the provided key.
    /// </summary>
    /// <param name="statusBarItemKey">String key. Returned status bar item was created with this key.</param>
    /// <returns></returns>
    IStatusBarItem this[string statusBarItemKey]
    {
      get;
    }

    /// <summary>
    /// Gets the main text status bar item.
    /// </summary>
    IStatusBarItem MainItem
    {
      get;
    }

    /// <summary>
    /// Add a new item to StatusBar.
    /// </summary>
    /// <param name="key">Unique string constant representing this item</param>
    /// <param name="item">An object implementing IStatusBarItem interface</param>
    /// <param name="alignment">Alignment of status bar element</param>
    void AddItem(string key, IStatusBarItem item, StatusBarItemAlignment alignment);

    /// <summary>
    /// Add a new item to StatusBar.
    /// </summary>   
    /// <param name="item">An object implementing IStatusBarItem interface</param>
    /// <param name="alignment">Alignment of status bar element</param>
    /// <returns>Unique string constant representing this item</returns>
    string AddItem(IStatusBarItem item, StatusBarItemAlignment alignment);

    /// <summary>
    /// Add new status bar item showing text messages.
    /// </summary>
    /// <param name="key">Unique string constant representing this item</param>
    /// <param name="alignment"></param>
    void AddTextStatusItem(string key, StatusBarItemAlignment alignment);
    /// <summary>
    /// Add new status bar item showing text messages.
    /// </summary>
    /// <param name="alignment"></param>
    /// <returns>Unique string constant representing this item</returns>
    string AddTextStatusItem(StatusBarItemAlignment alignment);

    /// <summary>
    /// Add new status bar item with progress bar.
    /// </summary>
    /// <param name="key">Unique string constant representing this item</param>
    /// <param name="from">Minimum progress bar value</param>
    /// <param name="to">Maximum progress bar value</param>
    /// <param name="alignment"></param>
    void AddProgressBarStatusItem(string key, double @from, double to, StatusBarItemAlignment alignment);

    /// <summary>
    /// Add new status bar item with progress bar.
    /// </summary>
    /// <param name="from">Minimum progress bar value</param>
    /// <param name="to">Maximum progress bar value</param>
    /// <param name="alignment"></param>
    /// <returns></returns>
    string AddProgressBarStatusItem(double @from, double to, StatusBarItemAlignment alignment);

    /// <summary>
    ///  Add separator
    /// </summary>
    /// <param name="key"></param>
    /// <param name="alignment"></param>
    void AddSeparator(string key, StatusBarItemAlignment alignment);

    /// <summary>
    /// Add separator
    /// </summary>
    /// <param name="alignment"></param>
    /// <returns></returns>
    string AddSeparator(StatusBarItemAlignment alignment);

    /// <summary>
    /// Remove an item with the given key from the status bar.
    /// </summary>
    /// <param name="key">Key</param>
    void RemoveItem(string key);

    /// <summary>
    /// Checks if an item with the following exist on the status bar
    /// </summary>
    /// <param name="key"></param>
    /// <returns>true if it exists, false otherwise</returns>
    bool ContainsItem(string key);    
  }
}
