﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace MorganStanley.MSDotNet.MSGui.Core
{
    public static class DependencyObjectHelper
    {
        public static DependencyObject FindVisualParent(this DependencyObject control_, Predicate<DependencyObject> match_, bool includeChild_=false) 
        {
            if (control_ == null) return null;
            var parent = includeChild_ ? control_ : VisualTreeHelper.GetParent(control_);
            while ((parent != null) && !match_(parent))
            {
                parent = VisualTreeHelper.GetParent(parent);
            }
            return parent;
        }

        public static T FindVisualParent<T>(this DependencyObject control_) where T : DependencyObject
        {
            if (control_ == null) return null;
            var parent = VisualTreeHelper.GetParent(control_); 
            while ((parent != null) && !(parent is T))
            {
                parent = VisualTreeHelper.GetParent(parent);
            }
            return parent as T; 
        }
        public static T FindVisualParentIncludeChild<T>(this DependencyObject control_) where T : DependencyObject
        {
            if (control_ == null) return null;
            DependencyObject parent = control_;
            while ((parent != null) && !(parent is T))
            {
                parent = VisualTreeHelper.GetParent(parent);
            }
            return parent as T;
        }
 
        public static T FindLogicalParent<T>(this DependencyObject control_) where T : DependencyObject
        {
            if (control_ == null) return null;
            var parent = LogicalTreeHelper.GetParent(control_);
            while ((parent != null) && !(parent is T))
            {
                parent = LogicalTreeHelper.GetParent(parent);
            }
            return parent as T; 
        }
        public static T FindLogicalParentIncludeChild<T>(this DependencyObject control_) where T : DependencyObject
        {
            if (control_ == null) return null;
            DependencyObject parent = control_;
            while ((parent != null) && !(parent is T))
            {
                parent = LogicalTreeHelper.GetParent(parent);
            }
            return parent as T;
        }
        public static IEnumerable<T> FindVisualChildrenIncludeParent<T>(this DependencyObject parent_) where T : DependencyObject
        { 
            if (parent_ != null)
            {
                if (parent_ is T) yield return (T) parent_;
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(parent_); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(parent_, i);
                    if (child == null) continue;

                    if (child is T)
                    {
                        yield return (T)child;
                    }

                    foreach (T childOfChild in FindVisualChildren<T>(child))
                    {
                        yield return childOfChild;
                    }
                }
            }
        } 

        public static IEnumerable<T> FindVisualChildren<T>(this DependencyObject parent_) where T : DependencyObject
        {
            if (parent_ != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(parent_); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(parent_, i);
                    if (child == null) continue;
                    
                    if (child is T)
                    {
                        yield return (T)child;
                    }

                    foreach (T childOfChild in FindVisualChildren<T>(child))
                    {
                        yield return childOfChild;
                    }
                }
            }
        }

        public static IEnumerable<DependencyObject> FindVisualChildren(this DependencyObject element_,
                                                               Predicate<DependencyObject> predicate_)
        {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(element_); i++)
            {
                DependencyObject dependencyObject = VisualTreeHelper.GetChild(element_, i);

                if (predicate_(dependencyObject))
                    yield return dependencyObject;
                 
                foreach (DependencyObject child in FindVisualChildren(dependencyObject, predicate_))
                    yield return child;
            }
        }

        public static bool Any(this DependencyObject parent_, Func<DependencyObject, bool> action_)
        {
            if (parent_ != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(parent_); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(parent_, i);
                    if (child == null) continue;
                    if (action_(child)) return true;
                    if (child.Any(action_))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public static T FindParent<T>(this DependencyObject descendant_, DependencyObject stopAtVisual_,  
            Type stopAtType_, bool includeLogicalParent_=true) where T : DependencyObject
        {
            DependencyObject parent;
            for (DependencyObject obj2 = descendant_; obj2 != null; obj2 = parent)
            {
                parent = GetParent(obj2, includeLogicalParent_);
                if (parent != null)
                {
                    if (parent == stopAtVisual_)
                    {
                        return null;
                    }
                    if ((stopAtType_ != null) && (stopAtType_ == parent.GetType()))
                    {
                        return null;
                    }
                    if (parent is T) return (T) parent;
                }
            }
            return null;

        }


        public static DependencyObject FindParent(this DependencyObject descendant_, DependencyObject stopAtVisual_, Type parentType_,
            Type stopAtType_, bool includeLogicalParent_ = true)
        {
            DependencyObject parent;
            for (DependencyObject obj2 = descendant_; obj2 != null; obj2 = parent)
            {
                parent = GetParent(obj2, includeLogicalParent_);
                if (parent != null)
                {
                    if (parent == stopAtVisual_)
                    {
                        return null;
                    }
                    if ((stopAtType_ != null) && (stopAtType_ == parent.GetType()))
                    {
                        return null;
                    }
                    Type c = parent.GetType();
                    if (c == parentType_ || parentType_.IsAssignableFrom(c))
                    {
                        return parent;
                    } 
                }
            }
            return null;

        }
        
        public static DependencyObject GetParent(this DependencyObject child_, bool includeLogicalParent_)
        {
            if (child_ == null)
            {
                throw new ArgumentNullException("child_");
            }
            Visual reference = child_ as Visual;
            if (reference != null)
            {
                DependencyObject parent = VisualTreeHelper.GetParent(reference);
                if (parent != null)
                {
                    return parent;
                }
                if (includeLogicalParent_)
                {
                    FrameworkElement element = child_ as FrameworkElement;
                    if (element != null)
                    {
                        return element.Parent;
                    }
                }
                return null;
            }
            if (includeLogicalParent_)
            {
                FrameworkContentElement element2 = child_ as FrameworkContentElement;
                if (element2 != null)
                {
                    return element2.Parent;
                }
            }
            Visual3D visuald = child_ as Visual3D;
            if (visuald != null)
            {
                return VisualTreeHelper.GetParent(visuald);
            }
            return null;
        }

 

        public static IEnumerable<T> FindVisualChildren<T>(this DependencyObject parent_, bool expandContentPresenter_) where T : DependencyObject
        {
            if (parent_ != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(parent_); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(parent_, i);
                    if (child == null) continue;

                    if (child is T)
                    {
                        yield return (T)child;
                    }

                    foreach (T childOfChild in FindVisualChildren<T>(child))
                    {
                        yield return childOfChild;
                    } 
                }
            }
        }

        public static IEnumerable<T> FindLogicalChildren<T>(this DependencyObject parent_, bool includeCurrentControl_ = false) where T : DependencyObject
        {
            if (parent_ != null)
            {
                if (includeCurrentControl_ && parent_ is T)
                {
                    yield return (T)parent_;
                }
                foreach (object child in LogicalTreeHelper.GetChildren(parent_))
                {
                    if (child == null) continue;
                    if (child is T)
                    {
                        yield return (T)child;
                    }
                    DependencyObject d = child as DependencyObject;
                    if (d == null) continue;
                    foreach (T childOfChild in FindLogicalChildren<T>(d))
                    {
                        yield return childOfChild;
                    }

                }
            }
        }

        public static T FindVisualChildByName<T>(this DependencyObject parent_, string name_) where T : DependencyObject
        {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(parent_); i++)
            {
                var child = VisualTreeHelper.GetChild(parent_, i);
                string controlName = child.GetValue(FrameworkElement.NameProperty) as string;
                if (controlName == name_)
                {
                    return child as T;
                }
                else
                {
                    T result = FindVisualChildByName<T>(child, name_);
                    if (result != null)
                        return result;
                }
            }
            return null;
        }
        public static bool IsDescendantOf(DependencyObject ancestor_, DependencyObject descendant_, bool preferLogicalTree_, bool usePopupTarget_)
        {
            if (ancestor_ == null)
            {
                throw new ArgumentNullException("ancestor_");
            }
            while (descendant_ != null)
            {
                if ((usePopupTarget_ && (descendant_ is Popup)) && (((Popup)descendant_).PlacementTarget != null))
                {
                    descendant_ = ((Popup)descendant_).PlacementTarget;
                }
                else if (!preferLogicalTree_)
                {
                    descendant_ = GetParent(descendant_, true);
                }
                else
                {
                    descendant_ = LogicalTreeHelper.GetParent(descendant_) ?? GetParent(descendant_, false);
                }
                if (ancestor_ == descendant_)
                {
                    return true;
                }
            }
            return false;
        }


        public static bool IsLogicalAncestor(DependencyObject ancestor, DependencyObject descendant)
        {
            while (descendant != null)
            {
                if (descendant == ancestor)
                {
                    return true;
                }
                descendant = GetParent(descendant, true);
            }
            return false;
        }

 

 

        public static List<BindingBase> GetBindingObjects(Object element_)
        {
            List<BindingBase> bindings = new List<BindingBase>();
            List<FieldInfo> propertiesAll = new List<FieldInfo>();
            Type currentLevel = element_.GetType();
            while (currentLevel != typeof(object))
            {
                propertiesAll.AddRange(currentLevel.GetFields());
                currentLevel = currentLevel.BaseType;
            }
            var propertiesDp = propertiesAll.Where(x => x.FieldType == typeof(DependencyProperty));

            foreach (var property in propertiesDp)
            {
                BindingBase b = BindingOperations.GetBindingBase(element_ as DependencyObject, property.GetValue(element_) as DependencyProperty);
                if (b != null)
                {
                    bindings.Add(b);
                }
            }

            return bindings;
        }


        public static IEnumerable GetBindingSources(this DependencyObject parent_, string propertyName_)
        {

            List<BindingBase> bindings = GetBindingObjects(parent_);
            Predicate<Binding> condition =
                (b) =>
                {
                    return b != null &&
                        (b.Path is PropertyPath)
                        && ((PropertyPath)b.Path).Path == propertyName_;
                };

            foreach (BindingBase bindingBase in bindings)
            {
                if (bindingBase is Binding)
                {
                    if (condition(bindingBase as Binding))
                        yield return parent_;
                }
                else if (bindingBase is MultiBinding)
                {
                    MultiBinding mb = bindingBase as MultiBinding;
                    foreach (Binding b in mb.Bindings)
                    {
                        if (condition(b))
                            yield return parent_;
                    }
                }
                else if (bindingBase is PriorityBinding)
                {
                    PriorityBinding pb = bindingBase as PriorityBinding;
                    foreach (Binding b in pb.Bindings)
                    {
                        if (condition(b))
                            yield return parent_;
                    }
                }
            }

            int childrenCount = VisualTreeHelper.GetChildrenCount(parent_);
            if (childrenCount > 0)
            {
                for (int i = 0; i < childrenCount; i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(parent_, i);
                    foreach (object element in GetBindingSources(child, propertyName_))
                    {
                        yield return element;
                    }
                }
            }
        }


        public static CultureInfo GetNonNeutralCulture(IFrameworkInputElement element_)
        {
            if (element_ == null)
            {
                throw new ArgumentException("element");
            }
            XmlLanguage nonDefaultLanguage = null;
            if (element_ is FrameworkElement)
            {
                nonDefaultLanguage = GetNonDefaultLanguage((FrameworkElement)element_);
            }
            else if (element_ is FrameworkContentElement)
            {
                nonDefaultLanguage = GetNonDefaultLanguage((FrameworkContentElement)element_);
            }
            CultureInfo culture = null;
            if (nonDefaultLanguage != null)
            {
                try
                {
                    culture = nonDefaultLanguage.GetEquivalentCulture();
                }
                catch (InvalidOperationException)
                {
                }
            }
            if (culture == null)
            {
                culture = CultureInfo.CurrentCulture;
            }
            return CoreUtilities.GetNonNeutralCulture(culture);
        }


        internal static XmlLanguage GetNonDefaultLanguage(DependencyObject element_)
        {
            XmlLanguage language = element_.GetValue(FrameworkElement.LanguageProperty) as XmlLanguage;
            if (((language != null) && (DependencyPropertyHelper.GetValueSource(element_, FrameworkElement.LanguageProperty).BaseValueSource == BaseValueSource.Default)) && (language.GetEquivalentCulture().IetfLanguageTag == "en-US"))
            {
                return null;
            }
            return language;
        }

        /// <summary>
        /// This method is an alternative to WPF's
        /// <see cref="VisualTreeHelper.GetParent"/> method, which also
        /// supports content elements. Keep in mind that for content element,
        /// this method falls back to the logical tree of the element!
        /// </summary>
        /// <param name="child_">The item to be processed.</param>
        /// <returns>The submitted item's parent, if available. Otherwise
        /// null.</returns>
        public static DependencyObject GetParentObject(this DependencyObject child_)
        {
            if (child_ == null) return null;

            //handle content elements separately
            ContentElement contentElement = child_ as ContentElement;
            if (contentElement != null)
            {
                DependencyObject parent = ContentOperations.GetParent(contentElement);
                if (parent != null) return parent;

                FrameworkContentElement fce = contentElement as FrameworkContentElement;
                return fce != null ? fce.Parent : null;
            }

            //also try searching for parent in framework elements (such as DockPanel, etc)
            FrameworkElement frameworkElement = child_ as FrameworkElement;
            if (frameworkElement != null)
            {
                DependencyObject parent = frameworkElement.Parent;
                if (parent != null) return parent;
            }

            //if it's not a ContentElement/FrameworkElement, rely on VisualTreeHelper
            return VisualTreeHelper.GetParent(child_);
        }

        public static string ToDisplayString(this Brush brush_)
        {
            if (brush_ == null) return null;
            SolidColorBrush solidBrush = brush_ as SolidColorBrush;
            if (solidBrush != null)
            {
               return solidBrush.Color.ToString();
            }
            LinearGradientBrush linearGradientBrush = brush_ as LinearGradientBrush;
            if (linearGradientBrush != null)
            {
                string colorText = string.Empty;
                List<GradientStop> stops = new List<GradientStop>(linearGradientBrush.GradientStops);
                stops.Sort((x_, y_) => (x_.Offset - y_.Offset).CompareTo(0));
                foreach (GradientStop gradientStop in stops)
                {
                    colorText += gradientStop.Color.ToString() + " (Offset: " + gradientStop.Offset + ")" + Environment.NewLine;
                }
                return colorText.TrimEnd('\r', '\n');
            }
            return brush_.ToString();
        }


        public static void WalkDictionary(ResourceDictionary dictionary_, Action<DictionaryEntry> resourceHandler_)
        {
            var dictionaries = new Queue<ResourceDictionary>();
            dictionaries.Enqueue(dictionary_);

            while (dictionaries.Count > 0)
            {
                var current = dictionaries.Dequeue();
                 
                foreach (var rd in current.MergedDictionaries)
                    dictionaries.Enqueue(rd);
                 
                foreach (DictionaryEntry entry in current)
                {
                    resourceHandler_(entry);
                }
            }
        }

        public static Binding CreateBindingObject(DependencyProperty dp_, BindingMode bindingMode_, object source_)
        {
            return new Binding { Path = new PropertyPath(dp_), Mode = bindingMode_, Source = source_ };
        }


        public static Binding CreateBindingObject(string path, BindingMode bindingMode, object source)
        {
            return new Binding { Path = new PropertyPath(path, new object[0]), Mode = bindingMode, Source = source };
        }

 

 

 

    }
}
