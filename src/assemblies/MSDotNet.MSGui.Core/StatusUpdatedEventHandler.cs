﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Core/StatusUpdatedEventHandler.cs#4 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

namespace MorganStanley.MSDotNet.MSGui.Core
{
  /// <summary>
  /// Handler for a status update.
  /// </summary>
  /// <param name="sender_">Sender.</param>
  /// <param name="e_">Arguments.</param>
  public delegate void StatusUpdatedEventHandler(object sender_, StatusUpdatedEventArgs e_);
}