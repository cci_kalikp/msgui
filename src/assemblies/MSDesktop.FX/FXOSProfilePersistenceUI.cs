﻿using System;
using System.Collections;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;
using Infragistics.Windows.Ribbon;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Unity;
using MorganStanley.IED.Concord.Configuration;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Concord;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;

namespace MSDesktop.FX
{
    public class FXOSProfilePersistenceUI : IModule
    {
        public const string FXOSProfilePersistenceUIMenuTopLevel = "FXOSProfilePersistenceUI#MenuTopLevel";

        private readonly IUnityContainer _container;
        private readonly IChromeManager _chromeManager;
        private readonly IChromeRegistry _chromeRegistry;
        private IUserProfilesProvider _profilesProvider;
        private IWidgetViewContainer _topLevelMenuContainer;
        private IFxProfileLoader _loader;

        public FXOSProfilePersistenceUI(IUnityContainer container, 
            IChromeManager chromeManager, 
            IChromeRegistry chromeRegistry)
        {
            _container = container;
            _chromeManager = chromeManager;
            _chromeRegistry = chromeRegistry;
        }

        public void Initialize()
        {
            _container.RegisterType<IFxProfileLoader, FxProfileLoader>(new ContainerControlledLifetimeManager());
            _loader = _container.Resolve<IFxProfileLoader>();

            _profilesProvider = _container.Resolve<IUserProfilesProvider>();
            _chromeRegistry.RegisterWidgetFactory(FXOSProfilePersistenceUIMenuTopLevel, CreateTopLevelMenu);
            _topLevelMenuContainer = _chromeManager.PlaceWidget(FXOSProfilePersistenceUIMenuTopLevel,
                                       ChromeManagerBase.MENU_COMMANDAREA_NAME, new InitialWidgetParameters());
            _topLevelMenuContainer.ContentReady += CreateMenuHierarchy;
        }

        private void CreateMenuHierarchy(object sender, EventArgs eventArgs)
        {
            var topLevelMenu = _topLevelMenuContainer.Content as MenuTool;
            if (topLevelMenu != null)
            {
                var userProfiles = _profilesProvider.UserProfiles;
                foreach (string key in userProfiles.Keys.OrderBy(s => s))
                {
                    var userSubMenu = new MenuTool { Caption = key };
                    foreach (var profile in userProfiles[key])
                    {
                        var profileButton = new ButtonTool {Caption = profile};
                        string key1 = key;
                        string profile1 = profile;
                        profileButton.Click += (o, args) => ProfileButtonClick(key1, profile1);
                        userSubMenu.Items.Add(profileButton);
                    }
                    topLevelMenu.Items.Add(userSubMenu);
                }
            }
        }

        private void ProfileButtonClick(string user, string profile)
        {
            _loader.LoadProfile(user, profile);
        }

        private static bool CreateTopLevelMenu(IWidgetViewContainer emptyViewContainer, XDocument state)
        {
            emptyViewContainer.Content = new MenuTool()
                {
                    Caption = @"Import Other Layout"
                };
            return true;
        }
    }

    public interface IFxProfileLoader
    {
        void LoadProfile(string user, string profile, string version = null);
    }

    internal class FxProfileLoader : IFxProfileLoader
    {
        private readonly IUnityContainer _container;
        private PersistenceProfileService _persistenceProfileService;

        public FxProfileLoader(IUnityContainer container)
        {
            _container = container;
            _persistenceProfileService = _container.Resolve<PersistenceProfileService>();
        }

        public void LoadProfile(string user, string profile, string version=null)
        {
#pragma warning disable 612,618
            var storage = _container.Resolve<IPersistenceStorage>();
#pragma warning restore 612,618
            storage.Apply();

            var name = version == null
                           ? string.Format("{0}#{1}", user, profile)
                           : string.Format("{0}#{1}#{2}", user, profile, version);
            // save the remote profile locally; interceptor has to do the work
            _persistenceProfileService.SaveCurrentProfileAs(name);

            ForceConcordToRereadConfigHack();

            _persistenceProfileService.LoadProfile(name);
        }

        private static void ForceConcordToRereadConfigHack()
        {
            ConfigurationManagerHelper.DirtyConfiguration("MSDesktopLayouts"); 
        }
    }
}
