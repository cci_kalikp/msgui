﻿using System;
using System.Reflection;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Themes;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Themes.Fx
{
  public static class DarkBlueThemeCreator
  {
    public static void AddFXOnlyTheme(this Framework framework_)
    {
      framework_.AddTheme(CreateDarkBlueTheme());
        PaneHeaderControl.Structure = HeaderStructure.WindowsModern;

    }
    private static Theme CreateDarkBlueTheme()
    {
      Theme theme = new Theme
      {
        Source =
          new Uri("pack://application:,,,/MSDesktop.FX;component/FX/ApplicationStyle.xaml",
                  UriKind.RelativeOrAbsolute),
        Name = "FXOnly"
      };
      Color oldTextSelectionColor = Colors.Blue;
      Color oldWindowBackgroundColor = Colors.White;
      theme.BeforeLoad = delegate
      {
        oldTextSelectionColor = GetTextHightlight();
        SetTextHightlight(Color.FromArgb(0xFF, 0x00, 0x88, 0x88));

        //we set window background color to make default caret sign color white
        //if background color of a textbox is set to anything then this solution won't work
        oldWindowBackgroundColor = GetWindowBackground();
        SetWindowBackground(Colors.Black);        
      };

      theme.AfterUnload = delegate
      {
        SetTextHightlight(oldTextSelectionColor);
        SetWindowBackground(oldWindowBackgroundColor);        
      };
      return theme;
    }
   
    private static void SetTextHightlight(Color color_)
    {
      var colorCache = (Color[])typeof(SystemColors).GetField("_colorCache", BindingFlags.NonPublic | BindingFlags.Static).GetValue(null);
      colorCache[14] = color_;
    }

    private static void SetWindowBackground(Color color_)
    {
      var colorCache = (Color[])typeof(SystemColors).GetField("_colorCache", BindingFlags.NonPublic | BindingFlags.Static).GetValue(null);
      colorCache[27] = color_;
    }

    private static Color GetWindowBackground()
    {
      var colorCache = (Color[])typeof(SystemColors).GetField("_colorCache", BindingFlags.NonPublic | BindingFlags.Static).GetValue(null);
      return colorCache[27];
    }

    private static Color GetTextHightlight()
    {
      var colorCache = (Color[])typeof(SystemColors).GetField("_colorCache", BindingFlags.NonPublic | BindingFlags.Static).GetValue(null);
      return colorCache[14];
    }
  }
}
