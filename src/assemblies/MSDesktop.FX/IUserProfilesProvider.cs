﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSDesktop.FX
{
    public interface IUserProfilesProvider
    {
        IDictionary<string, IList<string>> UserProfiles { get; }
    }
}
