﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MSDesktop.ModuleEntitlements
{
    public interface IModuleEntitlementsStatusInfo
    {
        ObservableCollection<ModuleStatusInfo> BlockedModules { get; }
        ObservableCollection<ModuleStatusInfo> AllowedModules { get; }
    }

    public struct ModuleStatusInfo
    {
        public string ModuleName { get; set; }
        public string ModuleType { get; set; }
        public ModuleStatus Status { get; set; }
        public IList<string> BlockedDependencies { get; set; }
    } 
}
