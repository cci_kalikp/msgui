﻿namespace MSDesktop.ModuleEntitlements
{
    public static class ModuleEntitlementsStrings
    {
        public const string CheckEntitlementAttribute = "CheckEntitlement";
        public const string ResourceType = "MSDesktop.Module";

        public static class Actions
        {
            public const string Load = "Load";
            public const string Ping = "Ping";
        }

        public static class Properties
        {
            public const string Name = "Name";
        }
    }
}
