﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using MSDesktop.ModuleEntitlements.Module.Impl;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.Desktop.Entitlement;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl; 
using MorganStanley.MSDotNet.MSGui.Impl.Modularity;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.Prism;

namespace MSDesktop.ModuleEntitlements
{
    public static class ModuleEntitlementsExtensions
    {
        public static void EnableModuleEntitlements(this Framework framework, 
            IEntitlementService entitlementService, 
            bool modulesMustOptIn=true,
            EventHandler<BlockingModuleEventArgs> blockingModuleCallback = null)
        {
            framework.EnableModuleEntitlements(entitlementService, 
                null,
                ModuleEntitlementsStrings.Actions.Load,
                ModuleEntitlementsStrings.Actions.Ping,
                modulesMustOptIn,
                blockingModuleCallback);
        }

        public static void EnableModuleEntitlements(this Framework framework_,
            bool modulesMustOptIn = true,
            EventHandler<BlockingModuleEventArgs> blockingModuleCallback = null)
        {
            framework_.EnableModuleEntitlements((IEntitlementService)null, modulesMustOptIn, blockingModuleCallback);
        }

        public static void EnableModuleEntitlements(this Framework framework_,
            IEntitlementService entitlementService_,
            Func<string, object> resourceFactory_,
            String customLoadActionName_, 
            String customPingActionName_,
            bool modulesMustOptIn_ = true,
            EventHandler<BlockingModuleEventArgs> blockingModuleCallback_ = null)
        {
            framework_.EnableModuleEntitlements(_=> entitlementService_, resourceFactory_, customLoadActionName_, customPingActionName_, modulesMustOptIn_,
                 blockingModuleCallback_); 
        }
        public static void EnableModuleEntitlements(this Framework framework,
            Func<IUnityContainer, IEntitlementService> entitlementServiceFactory_,
            bool modulesMustOptIn = true,
            EventHandler<BlockingModuleEventArgs> blockingModuleCallback = null)
        {
            framework.EnableModuleEntitlements(entitlementServiceFactory_,
                null,
                ModuleEntitlementsStrings.Actions.Load,
                ModuleEntitlementsStrings.Actions.Ping,
                modulesMustOptIn,
                blockingModuleCallback);
        }

        public static void EnableModuleEntitlements(this Framework framework_,
            Func<IUnityContainer, IEntitlementService> entitlementServiceFactory_,
            Func<string, object> resourceFactory_,
            String customLoadActionName_, 
            String customPingActionName_,
            bool modulesMustOptIn_ = true,
            EventHandler<BlockingModuleEventArgs> blockingModuleCallback_ = null)
        {
            EntitlementModuleInfoHelper.LoadActionName = customLoadActionName_;
            EntitlementModuleInfoHelper.PingActionName = customPingActionName_;
            EntitlementModuleInfoHelper.ResourceFactory = resourceFactory_;
            framework_.Bootstrapper.AfterPersistenceStorageInitialized +=
                (sender, args) =>
                {
                    var container = args.Container;
                    if (entitlementServiceFactory_ != null && !container.IsRegistered<IEntitlementService>())
                    {
                        var entitlementService = entitlementServiceFactory_(container);
                        if (entitlementService != null)
                        {
                            container.RegisterInstance(entitlementService);
                        }
                    }
                    if (container.IsRegistered<IInterceptableModuleCatalog>())
                    {
                        EntitlementModuleInfoHelper.AddInterceptorToCatalog(container, blockingModuleCallback_);
                    }
                    else if (container.IsRegistered<IInterceptablePrismModuleCatalog>())
                    {
                        EntitlementModuleInfoHelper.AddPrismInterceptorToCatalog(container, blockingModuleCallback_);
                    }
                    EntitlementModuleInfoHelper.AddIsolatedModuleInterceptor(container, blockingModuleCallback_);
                };
            EntitlementModuleInfoHelper.ModulesMustOptIn = modulesMustOptIn_; 
        }

        public static void SetModuleEntitlementService(this Framework framework_, IEntitlementService entitlementService_)
        {
            if (entitlementService_ != null && !framework_.Container.IsRegistered<IEntitlementService>())
            {
                framework_.Container.RegisterInstance(entitlementService_);
            }
        }
         

        public static void AddModuleWithEntitlementCheck<T>(this Framework framework)
        {
            framework.AddModule<T>();
            EntitlementModuleInfoHelper.ModulesWithCheck.Add(typeof(T).AssemblyQualifiedName);
        }

        public static void AddModuleWithEntitlementCheck<T>(this Framework framework, string name)
        {
            framework.AddModule<T>(name);
            EntitlementModuleInfoHelper.ModulesWithCheck.Add(typeof(T).AssemblyQualifiedName);
        }

        public static void AddModuleWithEntitlementCheck(this Framework framework, Type type)
        {
            framework.AddModule(type);
            EntitlementModuleInfoHelper.ModulesWithCheck.Add(type.AssemblyQualifiedName);
        }

        public static void AddModuleWithEntitlementCheck(this Framework framework, Type type, string name)
        {
            framework.AddModule(type, name);
            EntitlementModuleInfoHelper.ModulesWithCheck.Add(type.AssemblyQualifiedName);
        }

        public static string GetModuleNameFromType(Type type)
        {
            return ResolverConfigurationModuleCatalog.DecodeGenericTypeName(type);
        }
    }

    public class BlockingModuleEventArgs : CancelEventArgs
    {
        public string ModuleName { get; set; }
        public ModuleStatus Cause { get; set; }
         
        public BlockingModuleEventArgs(string moduleName)
        {
            ModuleName = moduleName;
        }
    }

    
}
