﻿using System;
using MorganStanley.Desktop.Entitlement;

namespace MSDesktop.ModuleEntitlements
{
    [EntitlementResource(ResourceType=ModuleEntitlementsStrings.ResourceType)]
    public class ModuleResource
    {
        [EntitlementResourceProperty]
        public string Name { get; set; }

        private Type m_type;
        public Type Type
        {
            get
            {
                return m_type;
            }
            set
            {
                m_type = value;
                Name = ModuleEntitlementsExtensions.GetModuleNameFromType(value);
            }
        }
    }
}
