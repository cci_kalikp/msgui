﻿using System; 
using System.Collections.ObjectModel; 
using Microsoft.Practices.Unity;
using MorganStanley.Desktop.Entitlement;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MSDesktop.ModuleEntitlements.Module.Impl
{
    abstract class EntitlementModuleInterceptorBase : IModuleEntitlementsStatusInfo
    {
        protected readonly IUnityContainer container; 
        protected readonly IModuleLoadInfos moduleLoadInfos;
        private IEntitlementService entitlementService;
        protected EntitlementModuleInterceptorBase(IUnityContainer container_)
        {
            container = container_;
            moduleLoadInfos = container.Resolve<IModuleLoadInfos>(); 
            BlockedModules = new ObservableCollection<ModuleStatusInfo>();
            AllowedModules = new ObservableCollection<ModuleStatusInfo>();
        }
         
        public IEntitlementService EntitlementService
        {
            get
            {
                if (entitlementService == null)
                {
                    if (!container.IsRegistered<IEntitlementService>())
                    {
                        throw new InvalidOperationException("IEntitlementService instance has to be registered at this point. Either provide " +
                                                            "it at boot time or use ConcordPersistenceStorage to inject it into the container.");
                    }
                    entitlementService = container.Resolve<IEntitlementService>();
                }
                return entitlementService;
            }
        }
        public event EventHandler<BlockingModuleEventArgs> BlockingModule;
        public void InvokeBlockingModule(BlockingModuleEventArgs e)
        {
            EventHandler<BlockingModuleEventArgs> handler = BlockingModule;
            if (handler != null) handler(this, e);
        }
         

        public ObservableCollection<ModuleStatusInfo> BlockedModules { get; private set; }
        public ObservableCollection<ModuleStatusInfo> AllowedModules { get; private set; }
    }
}
