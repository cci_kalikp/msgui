﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.My;

namespace MSDesktop.ModuleEntitlements.Module.Impl
{
    class EntitlementIsolatedModuleInterceptor : EntitlementModuleInterceptorBase, IIsolatedModuleEntitlementsStatusInfo, IIsolatedModuleInterceptor
    {
        internal static IMSLogger Logger = MSLoggerFactory.CreateLogger<EntitlementIsolatedModuleInterceptor>();

        public EntitlementIsolatedModuleInterceptor(IUnityContainer container_)
            : base(container_)
        {
            container_.RegisterInstance(typeof(IIsolatedModuleEntitlementsStatusInfo), this);
            container_.RegisterInstance(typeof(IIsolatedModuleInterceptor), this);
        }

        public bool Accept(string moduleType_, string moduleName_, bool checkEntitlement_)
        {
            var status = EntitlementModuleInfoHelper.IsModuleAllowed(this, moduleName_, checkEntitlement_);
            if (status == ModuleStatus.Loaded)
            {
                AllowModule(moduleType_, moduleName_);
                return true;
            }
            LogBlockedModule(moduleType_, moduleName_, status);
            return false;
        }


        private void AllowModule(string moduleType_, string moduleName_)
        { 
            AllowedModules.Add(new ModuleStatusInfo
            {
                ModuleName = moduleName_,
                ModuleType = moduleType_,
                Status = ModuleStatus.Loaded
            });
        }

        private void LogBlockedModule(string moduleType_, string moduleName_, ModuleStatus moduleStatus)
        {
            const string logMessage = "Module {0} was blocked with status: {1}";
            Logger.WarningWithFormat(logMessage, moduleName_, moduleStatus);

            var loadInfo = moduleLoadInfos.AddModuleLoadInfo(moduleType_, moduleStatus);
            loadInfo.OutOfProcess = true;
            if (moduleStatus == ModuleStatus.BlockedByEntitlements ||
                moduleStatus == ModuleStatus.BlockedByNoEntitlementsInfo)
            {
                loadInfo.RejectReason = "Resource rejected: " + moduleName_;
            } 
            BlockedModules.Add(new ModuleStatusInfo
            {
                ModuleName = moduleName_,
                ModuleType = moduleType_,
                Status = moduleStatus 
            });
        } 
    }
}
