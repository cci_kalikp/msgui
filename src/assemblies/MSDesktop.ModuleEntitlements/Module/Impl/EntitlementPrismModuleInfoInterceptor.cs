﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Unity;
using MorganStanley.Desktop.Entitlement;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.Prism;
using MorganStanley.MSDotNet.My;

namespace MSDesktop.ModuleEntitlements.Module.Impl
{
    class EntitlementPrismModuleInfoInterceptor : EntitlementModuleInterceptorBase, IPrismModuleInfoInterceptor
    {
        internal static IMSLogger Logger = MSLoggerFactory.CreateLogger<EntitlementModuleInfoInterceptor>(); 

        public EntitlementPrismModuleInfoInterceptor(IUnityContainer container_):base(container_)
        { 
            container_.RegisterInstance(typeof(IModuleEntitlementsStatusInfo), this);
        }

        public void Intercept(IList<ModuleInfo> modules_, Stack<IPrismModuleInfoInterceptor> interceptors_)
        {
 
            if (interceptors_.Count > 0)
            {
                interceptors_.Pop().Intercept(modules_, interceptors_);
            } 
             
            List<ModuleInfo> entitledInfos = new List<ModuleInfo>();
            foreach (var moduleInfo in modules_)
            {
                bool checkEntitlement = false;
                ResolverPrismModuleInfo infoCasted = moduleInfo as ResolverPrismModuleInfo;
                if (infoCasted != null)
                {
                    if (infoCasted.ExtraValues == null)
                    {
                        checkEntitlement = EntitlementModuleInfoHelper.ModulesWithCheck.Contains(infoCasted.ModuleType);
                    }
                    else
                    {
                        if (infoCasted.ExtraValues.ContainsKey(ModuleEntitlementsStrings.CheckEntitlementAttribute))
                        {
                            checkEntitlement = bool.Parse(infoCasted.ExtraValues[ModuleEntitlementsStrings.CheckEntitlementAttribute]);
                        }
                    }
                }

                var hasBlockedDependency =
               moduleInfo.DependsOn.Intersect(BlockedModules.Select(m => m.ModuleName)).Any();
                if (hasBlockedDependency)
                {
                    var blockedDependencies =
                        moduleInfo.DependsOn.Where(m => BlockedModules.Select(m2 => m2.ModuleName).Contains(m));
                    var moduleEventArgs = new BlockingModuleEventArgs(moduleInfo.ModuleName)
                    {
                        Cause = ModuleStatus.DependentModuleBlocked
                    };
                    InvokeBlockingModule(moduleEventArgs);
                    if (!moduleEventArgs.Cancel)
                    {
                        LogBlockedModule(moduleInfo, ModuleStatus.DependentModuleBlocked, blockedDependencies.ToList());
                        continue;
                    }
                    foreach (var blockedDependency in blockedDependencies.ToList())
                    {
                        moduleInfo.DependsOn.Remove(blockedDependency);
                    }
                }

                var status = EntitlementModuleInfoHelper.IsModuleAllowed(this, moduleInfo.ModuleName, checkEntitlement);
                if (status == ModuleStatus.Loaded)
                {
                    AllowModule(moduleInfo, entitledInfos);
                }
                else
                {
                    LogBlockedModule(moduleInfo, status);
                } 
            }

            if (modules_.Count > 0)
            {
                modules_.Clear();
            }

            foreach (var entitledInfo in entitledInfos)
            {
                modules_.Add(entitledInfo);
            } 
        }
 
        private void AllowModule(ModuleInfo moduleInfo, List<ModuleInfo> modules)
        {
            modules.Add(moduleInfo);
            AllowedModules.Add(new ModuleStatusInfo
            {
                ModuleName = moduleInfo.ModuleName,
                ModuleType = moduleInfo.ModuleType,
                Status = ModuleStatus.Loaded
            });
        }

        private void LogBlockedModule(ModuleInfo moduleInfo, ModuleStatus moduleStatus, IList<string> dependencies = null)
        {
            if (dependencies == null || dependencies.Count == 0)
            {
                const string logMessage = "Module {0} was blocked with status: {1}";
                Logger.WarningWithFormat(logMessage, moduleInfo.ModuleName, moduleStatus);
            }
            else
            {
                const string logMessage = "Module {0} was blocked with status: {1}; the dependencies are: {2}";
                Logger.WarningWithFormat(logMessage, moduleInfo.ModuleName, moduleStatus, dependencies.ToString());
            }
            var loadInfo = moduleLoadInfos.AddModuleLoadInfo(moduleInfo.ModuleType, moduleStatus);
            if (moduleStatus == ModuleStatus.BlockedByEntitlements ||
                moduleStatus == ModuleStatus.BlockedByNoEntitlementsInfo)
            {
               loadInfo.RejectReason = "Resource rejected: " + moduleInfo.ModuleName;
            }
            else if (dependencies != null)
            {
                loadInfo.RejectReason = "Resource rejected: " + string.Join(", ", dependencies);
            }
            BlockedModules.Add(new ModuleStatusInfo
            {
                ModuleName = moduleInfo.ModuleName,
                ModuleType = moduleInfo.ModuleType,
                Status = moduleStatus,
                BlockedDependencies = dependencies
            });
        }
    }
}
