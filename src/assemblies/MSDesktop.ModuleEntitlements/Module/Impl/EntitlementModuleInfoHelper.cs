﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Unity;
using MorganStanley.Desktop.Entitlement;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.Prism;
using MorganStanley.MSDotNet.My;

namespace MSDesktop.ModuleEntitlements.Module.Impl
{
    class EntitlementModuleInfoHelper
    {
        internal static string LoadActionName;
        internal static string PingActionName;
        internal static Func<string, object> ResourceFactory;
        internal static IList<string> ModulesWithCheck = new List<string>();
        internal static bool ModulesMustOptIn;
        internal static IMSLogger Logger = MSLoggerFactory.CreateLogger<EntitlementModuleInfoHelper>();

        internal static void AddInterceptorToCatalog(IUnityContainer container_, EventHandler<BlockingModuleEventArgs> blockingModuleCallback_)
        {
            var catalog = container_.Resolve<IInterceptableModuleCatalog>();
            var interceptor = new EntitlementModuleInfoInterceptor(container_);
            if (blockingModuleCallback_ != null)
            {
                interceptor.BlockingModule += blockingModuleCallback_;
            }
            catalog.AddInterceptor(interceptor);
        }
        internal static void AddPrismInterceptorToCatalog(IUnityContainer container_, EventHandler<BlockingModuleEventArgs> blockingModuleCallback_)
        {
            var catalog = container_.Resolve<IInterceptablePrismModuleCatalog>();
            var interceptor = new EntitlementPrismModuleInfoInterceptor(container_);
            if (blockingModuleCallback_ != null)
            {
                interceptor.BlockingModule += blockingModuleCallback_;
            }
            catalog.AddInterceptor(interceptor);
        }

        
        internal static void AddIsolatedModuleInterceptor(IUnityContainer container_,
                                                          EventHandler<BlockingModuleEventArgs> blockingModuleCallback_)
        {
            var interceptor = new EntitlementIsolatedModuleInterceptor(container_);
            if (blockingModuleCallback_ != null)
            {
                interceptor.BlockingModule += blockingModuleCallback_;
            }
        }
 
        internal static object GetModuleResource(string moduleName)
        { 
            return (ResourceFactory == null)
                       ? new ModuleResource { Name = moduleName }
                       : ResourceFactory(moduleName) ?? new ModuleResource(){Name="UnentitledModule"};
        }
         
        internal static ModuleStatus IsModuleAllowed(EntitlementModuleInterceptorBase entitlementService_, string moduleName_, bool checkEntitlement_)
        {
            try
            {
                if (!ModulesMustOptIn || checkEntitlement_)
                {
                    object moduleResource = GetModuleResource(moduleName_);
                    if (moduleResource == null || entitlementService_.EntitlementService.IsUserAllowed(LoadActionName, moduleResource))
                    {
                        Logger.InfoWithFormat("User is allowed to load module {0}", moduleName_);
                        return ModuleStatus.Loaded;
                    }
                    var moduleEventArgs = new BlockingModuleEventArgs(moduleName_);
                    // this check is relevant if the entitlements provider doesn't distinguish
                    // between NO and DON'T KNOW
                    if (!entitlementService_.EntitlementService.IsUserAllowed(PingActionName, moduleResource))
                    {
                        Logger.InfoWithFormat("Module {0} is neither Pingable nor Loadable",
                                              moduleName_);
                        moduleEventArgs.Cause = ModuleStatus.BlockedByNoEntitlementsInfo;
                    }
                    else
                    {
                        Logger.InfoWithFormat("Module {0} is Pingable but not Loadable", moduleName_);
                        moduleEventArgs.Cause = ModuleStatus.BlockedByEntitlements;
                    }
                    entitlementService_.InvokeBlockingModule(moduleEventArgs);
                    if (moduleEventArgs.Cancel)
                    {
                        Logger.InfoWithFormat("Module {0} was added although the status was: {1}",
                                              moduleName_, moduleEventArgs.Cause);
                        return ModuleStatus.Loaded;
                    }
                    return moduleEventArgs.Cause;
                }

                Logger.InfoWithFormat("Entitlement check for module {0} is not needed", moduleName_);
                return ModuleStatus.Loaded;
            }
            catch (EntitlementException)
            {
                // this is a DON'T KNOW situation
                var moduleEventArgs = new BlockingModuleEventArgs(moduleName_)
                {
                    Cause = ModuleStatus.BlockedByNoEntitlementsInfo
                };
                entitlementService_.InvokeBlockingModule(moduleEventArgs);
                if (moduleEventArgs.Cancel)
                {
                    Logger.InfoWithFormat("Module {0} was added although the status was: {1}", moduleName_,
                                          moduleEventArgs.Cause);
                    return ModuleStatus.Loaded;
                }
                return moduleEventArgs.Cause; 
            }
        }
    }
}
