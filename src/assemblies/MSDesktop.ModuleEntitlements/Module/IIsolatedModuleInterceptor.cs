﻿

namespace MSDesktop.ModuleEntitlements
{
    public interface IIsolatedModuleInterceptor
    {
        bool Accept(string moduleType_, string moduleName_, bool checkEntitlement_); 
 
    }
}
