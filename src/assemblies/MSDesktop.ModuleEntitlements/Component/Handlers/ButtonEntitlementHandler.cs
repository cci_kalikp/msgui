﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input; 
using MorganStanley.MSDotNet.My;

namespace MSDesktop.ComponentEntitlements
{
    public class ButtonEntitlementHandler:CommandSourceEntitlementHandler
    {
        internal static IMSLogger Logger = MSLoggerFactory.CreateLogger<ButtonEntitlementHandler>();

        public override bool Handle(DependencyObject dependencyObject_, string resourceId_, IEnumerable<string> permittedActions_)
        {
            ButtonBase element = dependencyObject_ as ButtonBase;
            if (element == null) return false;
            return base.Handle(dependencyObject_, resourceId_, permittedActions_);
        }
    }
}
