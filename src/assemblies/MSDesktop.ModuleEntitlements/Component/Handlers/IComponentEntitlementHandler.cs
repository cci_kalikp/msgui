﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows; 

namespace MSDesktop.ComponentEntitlements
{
    public interface IComponentEntitlementHandler
    { 
        bool Handle(DependencyObject dependencyObject_, string resourceId_, IEnumerable<string> permittedActions_);
    }
}
