﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input; 
using MorganStanley.MSDotNet.My;

namespace MSDesktop.ComponentEntitlements
{
    public class EditorEntitlementHandler : IComponentEntitlementHandler
    {
        internal static IMSLogger Logger = MSLoggerFactory.CreateLogger<EditorEntitlementHandler>();

        public virtual bool Handle(DependencyObject dependencyObject_, string resourceId_, IEnumerable<string> permittedActions_)
        {
            FrameworkElement element = dependencyObject_ as FrameworkElement;
            if (element == null) return false;

            bool viewable = permittedActions_.Contains(ComponentEntitlementsExtensions.ViewActionName);
            if (!viewable)
            {
                EntitlementHelper.HideElementAlways(element);
                Logger.InfoWithFormat("Resource id: '{0}' has no '{1}' permission, hide the element of type: '{2}'", resourceId_, ComponentEntitlementsExtensions.ViewActionName, element);
            }
            else 
            {
                bool editable = permittedActions_.Contains(ComponentEntitlementsExtensions.ModifyActionName);
                if (!editable)
                {
                    EntitlementHelper.DisableElementAlways(element);
                    Logger.InfoWithFormat("Resource id: '{0}' has no '{1}' permission, disable the element of type: '{2}'", resourceId_, ComponentEntitlementsExtensions.ModifyActionName, element);
                }
            }
            return true;
        }
    }
}
