﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls; 
using MorganStanley.MSDotNet.My;

namespace MSDesktop.ComponentEntitlements
{
    public class CheckBoxEntitlementHandler:EditorEntitlementHandler
    {
        internal static IMSLogger Logger = MSLoggerFactory.CreateLogger<CheckBoxEntitlementHandler>();

        public override bool Handle(DependencyObject dependencyObject_, string resourceId_, IEnumerable<string> permittedActions_)
        {
            CheckBox element = dependencyObject_ as CheckBox;
            if (element == null) return false;

            return base.Handle(dependencyObject_, resourceId_, permittedActions_);
        }

    }
}
