﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input; 
using MorganStanley.MSDotNet.My;

namespace MSDesktop.ComponentEntitlements
{
    public class ContentControlEntitlementHandler : IComponentEntitlementHandler
    {
        internal static IMSLogger Logger = MSLoggerFactory.CreateLogger<ContentControlEntitlementHandler>();

        public bool Handle(DependencyObject dependencyObject_, string resourceId_, IEnumerable<string> permittedActions_)
        {
            ContentControl element = dependencyObject_ as ContentControl;
            if (element == null) return false; 

            bool viewable = permittedActions_.Contains(ComponentEntitlementsExtensions.ViewActionName);
            if (!viewable)
            {
                EntitlementHelper.HideElementAlways(element);
                Logger.InfoWithFormat("Resource id: '{0}' has no '{1}' permission, hide the element of type: '{2}'", resourceId_, ComponentEntitlementsExtensions.ViewActionName, element);
            } 
            return true;
        }
    }
}
