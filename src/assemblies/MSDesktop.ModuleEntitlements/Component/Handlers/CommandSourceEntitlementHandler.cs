﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input; 
using MorganStanley.MSDotNet.My;

namespace MSDesktop.ComponentEntitlements
{
    public class CommandSourceEntitlementHandler:IComponentEntitlementHandler
    {
        internal static IMSLogger Logger = MSLoggerFactory.CreateLogger<CommandSourceEntitlementHandler>();

        public virtual bool Handle(DependencyObject dependencyObject_, string resourceId_, IEnumerable<string> permittedActions_)
        { 
            FrameworkElement element = dependencyObject_ as FrameworkElement;
            if (element == null) return false; 
            ICommandSource commandSource = dependencyObject_ as ICommandSource;
            if (commandSource == null) return false;

            bool allowExecute = permittedActions_.Contains(ComponentEntitlementsExtensions.ExecuteActionName);
            if (!allowExecute)
            {
                EntitlementHelper.HideElementAlways(element);
                Logger.InfoWithFormat("Resource id: '{0}' has no '{1}' permission, hide the element of type: '{2}'", resourceId_, ComponentEntitlementsExtensions.ExecuteActionName, element);

            }
            return true;
        }
    }
}
