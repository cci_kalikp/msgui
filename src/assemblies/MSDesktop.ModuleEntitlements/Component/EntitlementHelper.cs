﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using MorganStanley.MSDotNet.MSGui.Core; 

namespace MSDesktop.ComponentEntitlements
{
    public static class EntitlementHelper
    {
        public static void HideElementAlways(FrameworkElement element_)
        {
            element_.Visibility = Visibility.Collapsed;
            DependencyPropertyChangedEventHandler onVisibleChanged = null;
            element_.IsVisibleChanged += onVisibleChanged = (o_, args_) =>
            {
                UIElement local = o_ as UIElement;
                if (local.Visibility == Visibility.Visible)
                {
                    local.Visibility = Visibility.Collapsed;
                }

            };

            element_.HandleUnloaded(frameworkElement_ => frameworkElement_.IsVisibleChanged -= onVisibleChanged); 

        }


        public static void DisableElementAlways(FrameworkElement element_)
        {
            element_.IsEnabled = false;
            DependencyPropertyChangedEventHandler onEnabledChanged = null;
            element_.IsEnabledChanged += onEnabledChanged = (o_, args_) =>
            {
                UIElement local = o_ as UIElement;
                if (local.IsEnabled)
                {
                    local.IsEnabled = false;
                }

            };
            element_.HandleUnloaded(frameworkElement_ => frameworkElement_.IsEnabledChanged -= onEnabledChanged);  
        } 

        public static void ForcePropertyValue(INotifyPropertyChanged notifier_, string propertyName_,
                                              object valueToForce_, bool setValueNow_=true)
        {
            ReflHelper.PropertySet(notifier_, propertyName_, valueToForce_); 
            notifier_.PropertyChanged += (sender_, args_) =>
            {
                if (args_.PropertyName == propertyName_)
                {
                    ReflHelper.PropertySet(notifier_, propertyName_, valueToForce_);
                }
            };
        }
         
    }
}
