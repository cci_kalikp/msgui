﻿namespace MSDesktop.ComponentEntitlements
{
    public static class ComponentEntitlementsStrings
    { 
        public const string ResourceType = "MSDesktop.Component";

        public static class Actions
        {
            public const string Execute = "Execute"; 
            public const string View = "View";
            public const string Modify = "Modify";
            public const string Load = "Load"; 
        }

        public static class Properties
        {
            public const string ID = "ID";
        }
    }
}
