﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using Microsoft.Practices.Unity;
using MorganStanley.Desktop.Entitlement;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.Factories;
using MorganStanley.MSDotNet.My;

namespace MSDesktop.ComponentEntitlements
{
    public static class ComponentEntitlementsExtensions
    {
        internal static IEntitlementService EntitlementService;
        private static List<IComponentEntitlementHandler> handlers;
        internal static string LoadActionName;
        internal static string ViewActionName;
        internal static string ModifyActionName;
        internal static string ExecuteActionName;
        internal static Func<string, object> ResourceFactory;

        internal static IMSLogger Logger = MSLoggerFactory.CreateLogger(typeof(ComponentEntitlementsExtensions).FullName);

        public static void EnableComponentEntitlements(this Framework framework_, IEntitlementService entitlementService_,
            params IComponentEntitlementHandler[] customHandlers_)
        {
            EnableComponentEntitlements(framework_, entitlementService_, null, ComponentEntitlementsStrings.Actions.Load, ComponentEntitlementsStrings.Actions.View, ComponentEntitlementsStrings.Actions.Modify, 
                ComponentEntitlementsStrings.Actions.Execute, customHandlers_);
        }

        public static void EnableComponentEntitlements(this Framework framework_, IEntitlementService entitlementService_, 
            Func<string, object> resourceFactory_,
            string customLoadActionName_,
            string customViewActionName_,
            string customModifyActionName_,
            string customExecuteActionName_,
            params IComponentEntitlementHandler[] customHandlers_)
        { 
            framework_.EnableComponentEntitlements(c_=> entitlementService_, resourceFactory_, customLoadActionName_, customViewActionName_, customModifyActionName_, customExecuteActionName_, customHandlers_);
        }

        public static void EnableComponentEntitlements(this Framework framework_,  
   Func<IUnityContainer, IEntitlementService> entitlementServiceFactory_,
    Func<string, object> resourceFactory_,
    string customLoadActionName_,
    string customViewActionName_,
    string customModifyActionName_,
    string customExecuteActionName_,
    params IComponentEntitlementHandler[] customHandlers_)
        {
            framework_.Bootstrapper.AfterPersistenceStorageInitialized +=
                            (sender, args) =>
                            {
                                var container = args.Container;
                                if (entitlementServiceFactory_ != null && !container.IsRegistered<IEntitlementService>())
                                {
                                    var entitlementService = entitlementServiceFactory_(container);
                                    if (entitlementService != null)
                                    {
                                        container.RegisterInstance(entitlementService);
                                    }
                                } 
                            }; 
            handlers = new List<IComponentEntitlementHandler>();
            if (customHandlers_ != null)
            {
                handlers.AddRange(customHandlers_);
            }
            handlers.Add(new CheckBoxEntitlementHandler());
            handlers.Add(new ButtonEntitlementHandler());
            handlers.Add(new ContentControlEntitlementHandler());
            handlers.Add(new CommandSourceEntitlementHandler());
            handlers.Add(new EditorEntitlementHandler());
            LoadActionName = customLoadActionName_;
            ViewActionName = customViewActionName_;
            ModifyActionName = customModifyActionName_;
            ExecuteActionName = customExecuteActionName_;
            ResourceFactory = resourceFactory_;
        }

        public static void SetComponentEntitlementService(this Framework framework_, IEntitlementService entitlementService_)
        {
            EntitlementService = entitlementService_;
        }

        public static void CheckEntitlement(this IWindowFactoryHolder f_, InitializeWindowHandler noEntitlementHandler_ = null, string name_ = null)
        {
            WindowFactoryHolder holder = f_ as WindowFactoryHolder;
            if (holder == null) return;
            
            holder.AddStep((container_, state_, chain_) =>
                {

                    if (string.IsNullOrEmpty(name_))
                    {
                        name_ = f_.Name;
                    }
                    if (string.IsNullOrWhiteSpace(name_)) return true;
                    bool entitled = EntitlementService == null;
                    if (!entitled)
                    {
                        try
                        {
                            entitled = EntitlementService.IsUserAllowed(LoadActionName, GetComponentResource(name_));

                        }
                        catch (EntitlementException) //no permission defined
                        {
                            Logger.InfoWithFormat("No entitlement found for resource id: '{0}'", name_);
                            entitled = false;
                        }
                    } 
                    if (!entitled && noEntitlementHandler_ != null)
                    {
                        return noEntitlementHandler_(container_, state_);
                    }
                    
                    if (entitled && chain_.Count > 0)
                    {
                        var initHandler = chain_.Pop();
                        var initResult = initHandler(container_, state_, chain_);
                        if (!initResult)
                            return false;
                    } 
                    return entitled;
                });
        }

        public static void CheckEntitlement(this IWidgetFactoryHolder f_, InitializeWidgetHandler noEntitlementHandler_ = null, string name_ = null) 
        {
            WidgetFactoryHolder holder = f_ as WidgetFactoryHolder;
            if (holder == null) return;

            holder.AddStep((container_, state_, chain_) =>
            {
 
                if (string.IsNullOrEmpty(name_))
                {
                    name_ = f_.Name;
                }
                if (string.IsNullOrWhiteSpace(name_)) return true;
                bool entitled = EntitlementService == null;
                if (!entitled)
                {
                    try
                    {
                        entitled = EntitlementService.IsUserAllowed(LoadActionName,
                            GetComponentResource(name_));

                    }
                    catch (EntitlementException) //no permission defined
                    {
                        Logger.InfoWithFormat("No entitlement found for resource id: '{0}'", name_);
                        entitled = false;
                    }
                }

                if (!entitled && noEntitlementHandler_ != null)
                {
                    return noEntitlementHandler_(container_, state_);
                }
                if (entitled && chain_.Count > 0)
                {
                    var initHandler = chain_.Pop();
                    var initResult = initHandler(container_, state_, chain_);
                    if (!initResult)
                        return false;
                } 
                return entitled;
            });
        }

        public static readonly DependencyProperty CheckEntitlementProperty =
            DependencyProperty.RegisterAttached("CheckEntitlement", typeof(string), typeof(ComponentEntitlementsExtensions), new PropertyMetadata(default(string), OnCheckEntitlementChanged));

        public static void SetCheckEntitlement(DependencyObject dependencyObject_, string value_)
        {
            dependencyObject_.SetValue(CheckEntitlementProperty, value_);
        }

        public static string GetCheckEntitlement(DependencyObject dependencyObject_)
        {
            return (string)dependencyObject_.GetValue(CheckEntitlementProperty);
        }

        private static void OnCheckEntitlementChanged(DependencyObject sender_, DependencyPropertyChangedEventArgs e_)
        {
            if (EntitlementService == null) return;
            string id = e_.NewValue as string;
            var element = sender_ as FrameworkElement;
            if (string.IsNullOrWhiteSpace(id) && element != null && element.Name != null)
            {
                id = element.Name;
            }
            if (string.IsNullOrWhiteSpace(id)) return;

            var componentResource = GetComponentResource(id);

            IEnumerable<string> permittedActions = null;
            try
            {
                permittedActions = EntitlementService.GetPermittedActions(componentResource); 

            }
            catch (EntitlementException) //no permission defined
            {
                Logger.InfoWithFormat("No entitlement found for resource id: '{0}'",id);
                permittedActions = new List<string>();
            }

            if (handlers.Any(handler_ => handler_.Handle(sender_, id, permittedActions)))
            {
                return;
            }
        }
         
        internal static object GetComponentResource(string componentId_)
        {
            return ResourceFactory == null
                       ? new ComponentResource() {ID = componentId_}
                       : ResourceFactory(componentId_) ?? new ComponentResource() {ID = "UnentitledComponent"};
        }
    }
}
