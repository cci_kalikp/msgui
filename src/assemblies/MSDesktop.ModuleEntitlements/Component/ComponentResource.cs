﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using MorganStanley.Desktop.Entitlement;

namespace MSDesktop.ComponentEntitlements
{
    [EntitlementResource(ResourceType = ComponentEntitlementsStrings.ResourceType)]
    public class ComponentResource
    {
        [EntitlementResourceProperty]
        public string ID { get; set; }

        private FrameworkElement element;
        public FrameworkElement Element
        {
            get { return element; }
            set
            {
                element = value;
                if (element != null && element.Name != null && string.IsNullOrWhiteSpace(ID))
                {
                    ID = element.Name;
                }
            }
        }
    }
}
