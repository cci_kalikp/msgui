﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input; 

namespace MSDesktop.ComponentEntitlements
{
    public class EntitledIsEnabledExtension : EntitledPropertyExtension
    {
       
        protected override object GetDefaultValue(DependencyObject dependencyObject_, DependencyProperty targetProperty_, bool missingEntitlement_)
        {
            return !missingEntitlement_;
        }

        protected override object GetEntitledValue(DependencyObject dependencyObject_, DependencyProperty targetProperty_, IEnumerable<string> permittedActions_)
        {
            string actionToCheck = null;
             if (dependencyObject_ is ICommandSource)
             {
                 actionToCheck = ComponentEntitlementsExtensions.ExecuteActionName; 
             }
             else
             {
                 actionToCheck = ComponentEntitlementsExtensions.ModifyActionName; 
             }
             bool entitled = permittedActions_.Contains(actionToCheck);
             if (!entitled)
             {
                 Logger.InfoWithFormat("Resource id: '{0}' has no '{1}' permission, disable the element of type: '{2}'", ID, actionToCheck, dependencyObject_);
             }
             return entitled;
        }
    }
}
