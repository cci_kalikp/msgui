﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input; 

namespace MSDesktop.ComponentEntitlements
{
    public class EntitledIsReadOnlyExtension : EntitledPropertyExtension
    {
        
        protected override object GetDefaultValue(DependencyObject dependencyObject_, DependencyProperty targetProperty_, bool missingEntitlement_)
        {
            return missingEntitlement_;
        }

        protected override object GetEntitledValue(DependencyObject dependencyObject_, DependencyProperty targetProperty_, IEnumerable<string> permittedActions_)
        {
            bool editable = permittedActions_.Contains(ComponentEntitlementsExtensions.ModifyActionName);
            if (!editable)
            {
                Logger.InfoWithFormat("Resource id: '{0}' has no '{1}' permission, make the element of type: '{2}' readonly", ID, ComponentEntitlementsExtensions.ModifyActionName, dependencyObject_);
            }
            return !editable;

        }
    }
}
