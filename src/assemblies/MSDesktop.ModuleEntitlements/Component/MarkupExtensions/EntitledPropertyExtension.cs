﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Markup; 
using MorganStanley.Desktop.Entitlement;
using MorganStanley.MSDotNet.MSGui.Core; 
using MorganStanley.MSDotNet.My;

namespace MSDesktop.ComponentEntitlements 
{
    public abstract class EntitledPropertyExtension : MarkupExtension
    {
        internal static IMSLogger Logger = MSLoggerFactory.CreateLogger<EntitledPropertyExtension>();

        public override object ProvideValue(IServiceProvider serviceProvider_)
        {

            var target = serviceProvider_.GetService(typeof (IProvideValueTarget))
                         as IProvideValueTarget;
            var host = target.TargetObject as DependencyObject;
            if (host != null)
            {
                if (string.IsNullOrEmpty(ID))
                {
                    FrameworkElement element = host as FrameworkElement;
                    if (element != null)
                    {
                        ID = element.Name;
                    }
                }
            }
            var dp = target.TargetProperty as DependencyProperty;
            if (string.IsNullOrEmpty(ID))
            {
                object defaultValue = GetDefaultValue(host, dp, false);
                Logger.InfoWithFormat(
                    "Resource Id not specified for the component of type: '{0}', use default value '{1}' for property '{2}'" +
                    host, defaultValue, dp.Name);
                return defaultValue;
            }
            if (ComponentEntitlementsExtensions.EntitlementService == null)
            {
                object defaultValue = GetDefaultValue(host, dp, true);
                Logger.InfoWithFormat(
                    "EntitlementService not enabled, use default value '{1}' for property '{2}' of component of type: '{0}'" +
                    host, defaultValue, dp.Name);
                return defaultValue;
            }

            IEnumerable<string> permittedActions = null;
            try
            {
                var componentResource = ComponentEntitlementsExtensions.GetComponentResource(ID);
                permittedActions =  
                    ComponentEntitlementsExtensions.EntitlementService.GetPermittedActions(componentResource);

            }
            catch (EntitlementException) //no permission defined
            {
                Logger.InfoWithFormat("No entitlement found for resource id: '{0}'", ID);
                permittedActions = new List<string>();
            }
            object value = GetEntitledValue(host, dp, permittedActions);
            if (ValueToForce != value && value != GetDefaultValue(host, dp, false))
            {
                ValueToForce = value;
                ForcePropertyValue(host, dp);
            }
            return value;
        }

        public virtual string ID { get; set; }

        internal object ValueToForce { get; private set; }

        protected abstract object GetDefaultValue(DependencyObject dependencyObject_, DependencyProperty targetProperty_,
                                                  bool missingEntitlement_);

        protected abstract object GetEntitledValue(DependencyObject dependencyObject_,
                                                   DependencyProperty targetProperty_,
                                                   IEnumerable<string> permittedActions_);

        private readonly Dictionary<DependencyProperty, EventHandler> handlers =
            new Dictionary<DependencyProperty, EventHandler>();

        protected void ForcePropertyValue(DependencyObject dependencyObject_, DependencyProperty targetProperty_)
        {
            INotifyPropertyChanged notifier = dependencyObject_ as INotifyPropertyChanged;
            if (notifier != null)
            {
                EntitlementHelper.ForcePropertyValue(notifier, targetProperty_.Name, ValueToForce, false);
                return;
            }
            DependencyPropertyDescriptor property = DependencyPropertyDescriptor.FromProperty(targetProperty_,
                                                                                              dependencyObject_.GetType());
            FrameworkElement e = dependencyObject_.FindLogicalParent<FrameworkElement>();
            if (e != null)
            {
                e.HandleUnloaded(e_ =>
                    {
                        EventHandler localHandler;
                        if (handlers.TryGetValue(targetProperty_, out localHandler))
                        {
                            DependencyPropertyDescriptor.FromProperty(targetProperty_, dependencyObject_.GetType())
                                                        .RemoveValueChanged(dependencyObject_, localHandler);
                        }
                    });

                EventHandler changedHandler = null;
                if (!handlers.TryGetValue(targetProperty_, out changedHandler))
                {
                    changedHandler = (sender_, args_) =>
                        {
                            (sender_ as DependencyObject).SetValue(targetProperty_, ValueToForce);
                        };
                    handlers[targetProperty_] = changedHandler;
                }
                property.RemoveValueChanged(dependencyObject_, changedHandler);
                property.AddValueChanged(dependencyObject_, changedHandler);

            }
        }
    }
}
