﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.ConcordLayoutMgrModule/CreateLinksOptions.cs#7 $
// $Change: 848584 $
// $DateTime: 2013/10/07 23:19:23 $
// $Author: caijin $

using System.Windows;
using MorganStanley.IED.Concord.Application;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MorganStanley.MSDotNet.MSGui.ConcordLayoutMgrModule
{
  class CreateLinksOptions : IOptionView
  {
    private readonly IConcordApplication m_concordApplication;
    private UIElement m_content;

    public CreateLinksOptions(IConcordApplication concordApplication_)
    {
      m_concordApplication = concordApplication_;
    } 

    public void OnOK()
    {
      
    }

    public void OnCancel()
    {
      
    }

    public void OnDisplay()
    {
      
    }

    public void OnHelp()
    {
     
    }

    public UIElement Content
    {
      get
      {
        if (m_content == null)
        {
          LinksManager linksManager = new LinksManager(m_concordApplication);
          m_content = linksManager.View;
        }
        return m_content;
      }
    }
  }
}
