﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.ConcordLayoutMgrModule/Exporter.cs#5 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.Collections;
using System.ComponentModel;
using System.IO;
using System.Security;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using System.Xml;
using MorganStanley.IED.Concord.Application;
using MorganStanley.IED.Concord.Configuration;
using MorganStanley.MSDotNet.MSGui.ConcordLayoutMgrModule.Controls;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;

namespace MorganStanley.MSDotNet.MSGui.ConcordLayoutMgrModule
{
  internal class Exporter : INotifyPropertyChanged
  {
    private readonly IConcordApplication m_application;

    public Exporter(IConcordApplication application_)
    {
      m_application = application_;
      m_view = new ExportView()
                 {
                   DataContext = this
                 };

      //TODO it would be also good to interrupt loading on Cancel
      CommandBinding cancelBinding = new CommandBinding(m_cancelCommand,
                                                      (sender_, args_) => InvokeCloseRequested(),
                                                      (o_, e_) => e_.CanExecute = true);
      CommandManager.RegisterClassCommandBinding(typeof (Exporter), cancelBinding);
      CommandBindings.Add(cancelBinding);

      CommandBinding exportBinding = new CommandBinding(m_exportCommand,
                                                        OnExport,
                                                        (o_, e_) => e_.CanExecute = true);
      CommandManager.RegisterClassCommandBinding(typeof (Exporter), exportBinding);
      CommandBindings.Add(exportBinding);      

      ThreadPool.QueueUserWorkItem(LoadFolders);
    }

    private void LoadFolders(object state_)
    {
      ArrayList folders = ConfigurationManager.EnumerateConfigs("RETURN_FOLDER_LIST");
      
      if (folders != null && folders.Count > 0)
      {
        View.Dispatcher.Invoke((Action) (() => DefaultDirectory = folders[0].ToString()));
        string msg = "Recommended folders: ";
        foreach (var folder in folders)
        {
          msg += folder + "; ";
        }
        Message = msg;
        //View.Dispatcher.Invoke((Action)(() => Message = msg));
      }      
    }

    private string m_layoutName = string.Empty;
    public string LayoutName
    {
      get
      {
        return m_layoutName;
      }
      set
      {
        m_layoutName = value;
        InvokePropertyChanged("LayoutName");
      }
    }

    private string m_fileName = string.Empty;
    public string FileName
    {
      get
      {
        return m_fileName;
      }
      set
      {
        m_fileName = value;
        InvokePropertyChanged("FileName");
      }
    }


    private string m_message = "No recommended folders.";
    public string Message
    {
      get
      {
        return m_message;
      }
      set
      {
        m_message = value;
        InvokePropertyChanged("Message");
      }
    }

    private string m_defaultDirectory = string.Empty;
    public string DefaultDirectory
    {
      get
      {
        return m_defaultDirectory;
      }
      set
      {
        m_defaultDirectory = value;
        InvokePropertyChanged("DefaultDirectory");
      }
    }

    private void OnExport(object sender_, ExecutedRoutedEventArgs e_)
    {
      if (string.IsNullOrEmpty(LayoutName))
      {
        TaskDialog.ShowMessage("Please name your layout first", "Invalid Arguments",
          TaskDialogCommonButtons.Close, VistaTaskDialogIcon.Error);
        //_layoutNameTextBox.Focus();
        return;        
      }
      if (string.IsNullOrEmpty(FileName))
      {
        TaskDialog.ShowMessage("Please select a file first", "Invalid Arguments",
          TaskDialogCommonButtons.Close, VistaTaskDialogIcon.Error);
        return;
      }
      if (File.Exists(FileName))
      {
        TaskDialogSimpleResult result = TaskDialog.ShowMessage(string.Format("The file {0} already exists. Overwrite?", FileName), "File Exists",
                                                  TaskDialogCommonButtons.YesNo, VistaTaskDialogIcon.Warning);
        if (result == TaskDialogSimpleResult.No)
        {
          TaskDialog.ShowMessage("Export was aborted at user request", "Aborted",
                          TaskDialogCommonButtons.YesNo, VistaTaskDialogIcon.Information);
          return;
        }
      }
      //#region Logging
      //if (Log.IsLogLevelEnabled(LogLevel.Debug))
      //{
      //  Logger.Debug.SetLocation(CLASS, "cmdExport_Click").
      //    WriteFormat("Exporting layout to file: {0}", FileName);
      //}
      //#endregion Logging

      if (this.ExportLayout(FileName, LayoutName))
      {
        TaskDialog.ShowMessage("Export was successful to file: " + FileName, "Success",
                        TaskDialogCommonButtons.Close, VistaTaskDialogIcon.Information);
      }
      else
      {
        TaskDialog.ShowMessage("Export failed to file: " + FileName, "Failed",
                        TaskDialogCommonButtons.Close, VistaTaskDialogIcon.Error);
      }
      InvokeCloseRequested();
    }

    private bool ExportLayout(string filename_, string layoutname_)
    {

      XmlDocument layoutDoc = new XmlDocument();
      XmlNode layouts = layoutDoc.CreateNode(XmlNodeType.Element, "Layouts", null);
      XmlNode layout = layoutDoc.CreateElement("Layout");
      layout.Attributes.Append(layoutDoc.CreateAttribute("name"));
      layout.Attributes["name"].Value = layoutname_;

      m_application.WindowManager.SaveLayout((XmlElement)layout);

      layouts.AppendChild(layout);
      layoutDoc.AppendChild(layouts);
      System.IO.FileStream output = null;
      try
      {
        output = System.IO.File.Create(filename_);
      }
      catch (SecurityException e_)
      {
        TaskDialog.ShowMessage("You don't have permission to save in that location. Exception details: " + e_.ToString(),
          "Permission Denied", TaskDialogCommonButtons.Close, VistaTaskDialogIcon.Error);

        return false;

      }
      catch (UnauthorizedAccessException e_)
      {
        TaskDialog.ShowMessage("The file you are attempting to overwrite is read-only. Exception details: " + e_.ToString(),
          "Read Only", TaskDialogCommonButtons.Close, VistaTaskDialogIcon.Error);

        return false;


      }
      catch (ArgumentException e_)
      {
        TaskDialog.ShowMessage("The filename specified has some invalid characters. Try removing any spaces, colons etc from name. Exception details: " + e_.ToString(),
          "Invalid Filename", TaskDialogCommonButtons.Close, VistaTaskDialogIcon.Error);

        return false;


      }
      catch (DirectoryNotFoundException e_)
      {
        TaskDialog.ShowMessage("The directory specified does not exist. Please choose another. Exception details: " + e_.ToString(),
          "Directory doesn't exist", TaskDialogCommonButtons.Close, VistaTaskDialogIcon.Error);

        return false;


      }
      catch (Exception e_)
      {
        TaskDialog.ShowMessage("An unexpected error has occurred trying to create the file. Exception details: " + e_.ToString(),
          "Unexpected Error", TaskDialogCommonButtons.Close, VistaTaskDialogIcon.Error);

        //#region Logging
        //if (Log.IsLogLevelEnabled(LogLevel.Error))
        //{
        //  Logger.Error.SetLocation(CLASS, "ExportLayout").
        //    WriteFormat("Unexpected error in ExportLayout " + e_.ToString());
        //}
        //#endregion Logging

        return false;


      }
      layoutDoc.AppendChild(layouts);


      StreamWriter streamWriter = new StreamWriter(output);
      streamWriter.WriteLine(layoutDoc.OuterXml);
      streamWriter.Flush();
      streamWriter.Close();

      return true;

    }

    private readonly FrameworkElement m_view;
    public FrameworkElement View
    {
      get
      {
        return m_view;
      }
    }

    private readonly RoutedCommand m_cancelCommand = new RoutedCommand();
    public ICommand CancelCommand
    {
      get
      {
        return m_cancelCommand;
      }
    }

    private readonly RoutedCommand m_exportCommand = new RoutedCommand();
    public ICommand ExportCommand
    {
      get
      {
        return m_exportCommand;
      }
    }
    
    public event EventHandler CloseRequested;

    public void InvokeCloseRequested()
    {
      EventHandler handler = CloseRequested;
      if (handler != null) handler(this, EventArgs.Empty);
    }

    private readonly CommandBindingCollection m_commandBindings = new CommandBindingCollection();
    public CommandBindingCollection CommandBindings
    {
      get
      {
        return m_commandBindings;
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    public void InvokePropertyChanged(string propertyName_)
    {
      PropertyChangedEventHandler handler = PropertyChanged;
      if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName_));
    }
  }
}
