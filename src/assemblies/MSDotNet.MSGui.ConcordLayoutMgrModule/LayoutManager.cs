﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.ConcordLayoutMgrModule/LayoutManager.cs#5 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System.Windows;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.IED.Concord.Application;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MorganStanley.MSDotNet.MSGui.ConcordLayoutMgrModule
{
  public class LayoutManager : IModule
  {    
    private readonly IConcordApplication m_concordApplication;
    private readonly IApplicationOptions m_options;


    public LayoutManager(     
      IConcordApplication concordApplication_,
      IApplicationOptions options_)
    {   
      m_concordApplication = concordApplication_;
      m_options = options_;
    }    

    public void Initialize()
    {      
      m_options.AddOptionPage("Concord Layouts", "Import Layouts", new ImportOptions(m_concordApplication));
      m_options.AddOptionPage("Concord Layouts", "Export Layouts", new ExportOptions(m_concordApplication));
      m_options.AddOptionPage("Concord Layouts", "Create Links", new CreateLinksOptions(m_concordApplication));
    }
  }
}
