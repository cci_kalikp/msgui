/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.ConcordLayoutMgrModule/ILinkageData.cs#4 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System.Collections;

namespace MorganStanley.MSDotNet.MSGui.ConcordLayoutMgrModule
{

  public interface ILinkageData
  {
    string Description { get;}    

    /// <summary>
    /// Stores layout linkage details (as in Layouts.config file) for later use in saving.
    /// </summary>
    /// <returns>An Hashtable containing Attribute keys and value pairs</returns>
    /// <example>
    /// <code>
    /// public Hashtable GetAttributePairs()
    /// {
    ///  Hashtable pairs = new Hashtable(); 
    ///  if (Path != null)
    ///    pairs.Add("path",Path);
    ///  if (LinkedTab != null)
    ///    pairs.Add("linkedTab",LinkedTab);
    ///  return pairs;
    /// }
    /// </code>
    /// </example>
    Hashtable GetAttributePairs();    
  }
}