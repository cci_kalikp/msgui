﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.ConcordLayoutMgrModule/Importer.cs#6 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using System.Xml;
using MorganStanley.IED.Concord.Application;
using MorganStanley.IED.Concord.Configuration;
using MorganStanley.MSDotNet.MSGui.ConcordLayoutMgrModule.Controls;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;

namespace MorganStanley.MSDotNet.MSGui.ConcordLayoutMgrModule
{
  internal class Importer : INotifyPropertyChanged
  {
    private readonly IConcordApplication m_application;

    public Importer(IConcordApplication application_)
    {
      m_application = application_;
      m_view = new ImportView()
                 {
                   DataContext = this
                 };      

      //TODO it would be also good to interrupt loading on Cancel
      CommandBinding cancelBinding = new CommandBinding(m_cancelCommand,
                                                      (sender_, args_) => InvokeCloseRequested(),
                                                      (o_, e_) => e_.CanExecute = true);      
      CommandManager.RegisterClassCommandBinding(typeof(Importer), cancelBinding);      
      CommandBindings.Add(cancelBinding);

      CommandBinding importBinding = new CommandBinding(m_importCommand,
                                                      OnImport,
                                                      (o_, e_) => e_.CanExecute = true);      
      CommandManager.RegisterClassCommandBinding(typeof(Importer), importBinding);
      CommandBindings.Add(importBinding);

      ThreadPool.QueueUserWorkItem(LoadTree);
    }

    private void OnImport(object sender_, ExecutedRoutedEventArgs e_)
    {
      ////the following logic was copied with modifications from MorganStanley.IED.Concord.ConcordLayoutMgr.Import.cmdOK_Click
      try
      {
        if (StaticHelper._isLinkedByLayout)
        {
          TaskDialog.ShowMessage(
            "Your layout is linked to a remote layout. Please unlink it first by using the Import/Export Menu - View Links option.",
            "Please Unlink First");
          return;
        }

        //import the selected tabs and layouts to the current workspace
        //combine all tabs into 1 layout and load it, all tabs saved as links
        if (Roots.Count == 0)
        {
          return;
        }

        foreach (TreeItem parent in Roots)
        {
          if (parent.IsChecked)
          {
            //Importing entire layout {0}: ", parent.Config);
            
            //just load entire layout
            Configurator config = (Configurator) ConfigurationManager.GetConfig(parent.Config, null);
            if (config != null)
            {
              XmlNode layout = config.GetNode("Layout", null);
              if (layout != null)
              {
                //mark this layout as linked
                layout.Attributes.Append(layout.OwnerDocument.CreateAttribute("linker"));
                layout.Attributes["linker"].Value = StaticHelper.GetLinkerTypeString();
                layout.Attributes.Append(layout.OwnerDocument.CreateAttribute("path"));
                layout.Attributes["path"].Value = parent.Config;
                //MessageBox.Show("Calling LoadLayout");
                m_application.WindowManager.LoadLayout((XmlElement) layout);
                TaskDialog.ShowMessage("You must save this new layout in order for it to be available next time you start this application.", "Save Reminder");
              }
            }
            InvokeCloseRequested();
            return;
          }

        }

        //no top-level layouts were checked so must create own merged layout from tabs and load it.
        XmlDocument mergedLayout = new XmlDocument();
        XmlNode mainNode = mergedLayout.CreateNode(XmlNodeType.Element, "Layout", null);
        mainNode.Attributes.Append(mergedLayout.CreateAttribute("name"));
        mainNode.Attributes["name"].Value = "Import Temp";
        mergedLayout.AppendChild(mainNode);

        //MessageBox.Show("Calling SaveLayout");
        m_application.WindowManager.SaveLayout((XmlElement) mainNode);

        /* The tab being imported may have forms and colleagues with guids that clash with guids in the
         * existing layout.  To prevent any potential clashes, the guids in the current layout are
         * re-generated so that all guids remain unique. */
        StaticHelper.RegenerateGuids(mainNode);

        XmlNode tabSplitter = mainNode.SelectSingleNode("TabSplitter", null);

        ArrayList captions = new ArrayList();

        XmlNodeList existingTabs = mainNode.SelectNodes("TabSplitter/Tab", null);
        if (existingTabs != null && existingTabs.Count > 0)
        {
          foreach (XmlNode existingTab in existingTabs)
          {
            if (existingTab.Attributes["caption"] != null && existingTab.Attributes["caption"].Value != null)
            {
              captions.Add(existingTab.Attributes["caption"].Value);
            }
          }
        }

        foreach (TreeItem parent in Roots)
        {

          if (parent.Children != null)
          {
            foreach (TreeItem child in parent.Children)
            {
              if (child.IsChecked)
              {
                Configurator config = (Configurator) ConfigurationManager.GetConfig(parent.Config, null);
                if (config != null)
                {
                  XmlNode layout = config.GetNode("Layout", null);
                  if (layout != null)
                  {
                    XmlNodeList list = layout.SelectNodes("TabSplitter/Tab", null);
                    foreach (XmlNode tab in list)
                    {
                      if (tab.Attributes["caption"] != null && tab.Attributes["caption"].Value != null &&
                          tab.Attributes["caption"].Value == child.Name)
                      {
                        //save tab as a link
                        XmlNode tabNode = mergedLayout.CreateNode(XmlNodeType.Element, "Tab", null);

                        tabNode.Attributes.Append(mergedLayout.CreateAttribute("linker"));
                        tabNode.Attributes["linker"].Value = StaticHelper.GetLinkerTypeString();

                        //ensure no duplicate captions
                        if (!captions.Contains(tab.Attributes["caption"].Value))
                        {
                          tabNode.Attributes.Append(mergedLayout.CreateAttribute("caption"));
                          tabNode.Attributes["caption"].Value = tab.Attributes["caption"].Value;
                        }
                        else
                        {
                          tabNode.Attributes.Append(mergedLayout.CreateAttribute("caption"));
                          int count = 2;
                          string captionMod = tab.Attributes["caption"].Value + " (" + Convert.ToString(count) +
                                              ")";
                          while (captions.Contains(captionMod))
                            captionMod = tab.Attributes["caption"].Value + " (" + Convert.ToString(++count) + ")";

                          tabNode.Attributes["caption"].Value = captionMod;
                        }
                        captions.Add(tabNode.Attributes["caption"].Value);

                        //#region Logging
                        //if (Log.IsLogLevelEnabled(LogLevel.Debug))
                        //{
                        //  Logger.Debug.SetLocation(CLASS, "cmdOK_Click").
                        //    WriteFormat("Importing tab {0} from layout {1} ", tabNode.Attributes["caption"].Value, (string)parent.Tag);
                        //}
                        //#endregion Logging

                        tabNode.Attributes.Append(mergedLayout.CreateAttribute("linkedTab"));
                        tabNode.Attributes["linkedTab"].Value = tab.Attributes["caption"].Value;
                        tabNode.Attributes.Append(mergedLayout.CreateAttribute("index"));
                        if (tabSplitter.ChildNodes == null)
                          tabNode.Attributes["index"].Value = "1";
                        else
                          tabNode.Attributes["index"].Value = Convert.ToString(tabSplitter.ChildNodes.Count + 1);
                        tabNode.Attributes.Append(mergedLayout.CreateAttribute("path"));
                        tabNode.Attributes["path"].Value = parent.Config;
                        tabSplitter.AppendChild(tabNode);
                      }
                    }
                  }
                }
              }
            }
          }
        }

        // PaneWindowManager requires all the layout info (not only linker info) is in the passed XML.
        XmlElement mergedLayoutXML = StaticHelper.ResolveTabLinks((XmlElement)mergedLayout.FirstChild);
        //MessageBox.Show("Calling LoadLayout");
        m_application.WindowManager.LoadLayout(mergedLayoutXML);
      }
      catch(Exception exception)
      {
        //TODO log
      }

      TaskDialog.ShowMessage(
        "You must save this new layout in order for it to be available next time you start this application.",
        "Save Reminder");

      InvokeCloseRequested();
    }


    private void LoadTree(object state_)
    {
      ArrayList layouts = null;
      try
      {
        layouts = ConfigurationManager.EnumerateConfigs("*.layout.config");        
      }
      catch (Exception exception)
      {
        View.Dispatcher.Invoke((Action) (() => IsLoadingMessage =
                                               string.Format("Failed to load available configurations: {0}",
                                                             exception.Message.Substring(0, Math.Min(500, exception.Message.Length)))));
        return;
      }
      if (layouts == null)
      {
        View.Dispatcher.Invoke((Action) (() => 
          IsLoadingMessage = string.Format("Configuration enumeration is not available for current providers")));
        return;
      }
      foreach (string configName in layouts)
      {
        try
        {
          Configurator config = (Configurator) ConfigurationManager.GetConfig(configName);
          if (config != null)
          {
            XmlNode node = config.GetNode("Layout", null);
            //don't include layout files that are drawn entirely from another
            if (node != null && node.Attributes["name"] != null && node.Attributes["name"].Value != null &&
                node.Attributes["linker"] == null)
            {
              TreeItem parent = new TreeItem(node.Attributes["name"].Value, configName, m_roots);
              XmlNodeList list = node.SelectNodes("TabSplitter/Tab");
              foreach (XmlNode tab in list)
              {
                //don't include tabs that are drawn from another layout file
                if (tab.Attributes["caption"] != null && tab.Attributes["caption"].Value != null &&
                    tab.Attributes["linker"] == null)
                {
                  TreeItem child = new TreeItem(tab.Attributes["caption"].Value, m_roots);
                  parent.AddChild(child);
                }
              }
              View.Dispatcher.Invoke((Action) (() => m_roots.Add(parent)));
            }
          }
        }
        catch (Exception exception_)
        {
          //TODO failed to load one of configurations. log it
        }
      }

      View.Dispatcher.Invoke((Action) (() => IsLoading = false));
    }

    private readonly FrameworkElement m_view;
    public FrameworkElement View
    {
      get
      {
        return m_view;
      }
    }

    private bool m_isLoading = true;
    public bool IsLoading
    {
      get
      {
        return m_isLoading;
      }
      set
      {
        m_isLoading = value;
        InvokePropertyChanged("IsLoading");
      }
    }

    private string m_isLoadingMessage = "Loading...";
    public string IsLoadingMessage
    {
      get
      {
        return m_isLoadingMessage;
      }
      set
      {
        m_isLoadingMessage = value;
        InvokePropertyChanged("IsLoadingMessage");
      }
    }

    private readonly ObservableCollection<TreeItem> m_roots = new ObservableCollection<TreeItem>();
    public ObservableCollection<TreeItem> Roots
    {
      get
      {
        return m_roots;
      }
    }

    private readonly RoutedCommand m_cancelCommand = new RoutedCommand();
    public ICommand CancelCommand
    {
      get
      {
        return m_cancelCommand;
      }
    }

    private readonly RoutedCommand m_importCommand = new RoutedCommand();
    public ICommand ImportCommand
    {
      get
      {
        return m_importCommand;
      }
    }

    public event EventHandler CloseRequested;

    public void InvokeCloseRequested()
    {
      EventHandler handler = CloseRequested;
      if (handler != null) handler(this, EventArgs.Empty);
    }

    public event PropertyChangedEventHandler PropertyChanged;

    public void InvokePropertyChanged(string propertyName_)
    {
      PropertyChangedEventHandler handler = PropertyChanged;
      if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName_));
    }

    private readonly CommandBindingCollection m_commandBindings = new CommandBindingCollection();
    public CommandBindingCollection CommandBindings
    {
      get
      {
        return m_commandBindings;
      }
    }
  }
}
