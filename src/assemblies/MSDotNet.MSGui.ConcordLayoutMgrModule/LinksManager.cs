﻿using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using MorganStanley.IED.Concord.Application;
using MorganStanley.MSDotNet.MSGui.ConcordLayoutMgrModule.Controls;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;

namespace MorganStanley.MSDotNet.MSGui.ConcordLayoutMgrModule
{
  internal class LinksManager : INotifyPropertyChanged
  {
    private readonly IConcordApplication m_application;

    public LinksManager(IConcordApplication application_)
    {
      m_application = application_;
      m_view = new LinksView()
                 {
                   DataContext = this
                 };      

      //TODO it would be also good to interrupt loading on Cancel
      CommandBinding cancelBinding = new CommandBinding(m_cancelCommand,
                                                      (sender_, args_) => InvokeCloseRequested(),
                                                      (o_, e_) => e_.CanExecute = true);
      CommandManager.RegisterClassCommandBinding(typeof(LinksManager), cancelBinding);      
      CommandBindings.Add(cancelBinding);

      CommandBinding okBinding = new CommandBinding(m_okCommand,
                                                      (sender_, args_) => InvokeCloseRequested(),
                                                      (o_, e_) => e_.CanExecute = true);
      CommandManager.RegisterClassCommandBinding(typeof(LinksManager), okBinding);
      CommandBindings.Add(okBinding);

      CommandBinding unlinkBinding = new CommandBinding(m_unlinkCommand,
                                                      (sender_, args_) => Unlink(),
                                                      (o_, e_) => e_.CanExecute = true);
      CommandManager.RegisterClassCommandBinding(typeof(LinksManager), unlinkBinding);
      CommandBindings.Add(unlinkBinding);

      ThreadPool.QueueUserWorkItem(LoadLinks);
    }

    private void Unlink()
    {
      var defView = (CollectionView) CollectionViewSource.GetDefaultView(LinksDescriptions);
      if (StaticHelper._isLinkedByTab)
      {
        //remove the selected link
        if (defView.CurrentItem != null && TaskDialog.ShowMessage("Are you sure you want to remove the link: " +
          defView.CurrentItem.ToString(), "Confirm Link Removal", TaskDialogCommonButtons.YesNo, VistaTaskDialogIcon.Warning) == TaskDialogSimpleResult.Yes)
        {
          IDictionaryEnumerator de = StaticHelper._tabLinks.GetEnumerator();
          object keyToBeRemoved = null;
          de.Reset();
          while (de.MoveNext())
          {
            //------------------------------------------------------------------- 
            //Unlink the tab that is selected and go back to showing 
            //the remaining tabs  
            string description = ((ILinkageData)de.Value).Description;
            if (description == defView.CurrentItem.ToString())
            {
              if (StaticHelper._tabLinks.Count <= 1)
              {
                StaticHelper._isLinkedByTab = false;
              }
              keyToBeRemoved = de.Key;
              TaskDialog.ShowMessage("Changes to this tab can now be saved, however changes to the original remote layout will no longer be reflected in this tab", "Save Enabled", TaskDialogCommonButtons.Close, VistaTaskDialogIcon.Warning);
              LinksDescriptions.RemoveAt(defView.CurrentPosition);
              break;
            }
            //-------------------------------------------------------------------  
          }
          //Removing the key for the tab that was selected.
          if (keyToBeRemoved != null)
          {
            StaticHelper._tabLinks.Remove(keyToBeRemoved);
          }
          if (LinksDescriptions.Count > 0)
          {
            //If there are still linked tabs
            //select the top most in the list.
            defView.MoveCurrentToFirst();
          }
          else if (StaticHelper._isLinkedByLayout)
          {
            //Else if there are no linked tabs
            //but the layout is still linked then
            //Change the label to display the layout
            //to be unlinked. 
            LoadLinks(null);
          }
          else
          {
            //Else just close the link viewer form
            //as there is nothing more to unlink.
            InvokeCloseRequested();
          }
        }
      }
      else if (StaticHelper._isLinkedByLayout)
      {
        if (TaskDialog.ShowMessage("Are you sure you want to remove the link to the remote layout?", "Confirm Link Removal", TaskDialogCommonButtons.YesNo, VistaTaskDialogIcon.Warning)
          == TaskDialogSimpleResult.Yes)
        {
          StaticHelper._isLinkedByLayout = false;
          TaskDialog.ShowMessage("Changes to this layout can now be saved, however changes to the original remote layout will no longer be reflected in this layout", "Save Enabled");
        }
        InvokeCloseRequested();
      }      
    }

    private void LoadLinks(object state_)
    {
      try
      {
        View.Dispatcher.Invoke((Action) (() => LinksDescriptions.Clear()));        
        
        if (StaticHelper._isLinkedByLayout)
        {
          View.Dispatcher.Invoke((Action) (() => StatusText = "Your entire layout is linked to another file: "));
          Hashtable pairs = ((ILinkageData) StaticHelper._layoutLink).GetAttributePairs();
          
          View.Dispatcher.Invoke((Action) (() => StatusText += pairs["path"] ?? "(layout name unavailable)"));
          View.Dispatcher.Invoke((Action) (() => LayoutEnabled = false));
        } 
        else if (StaticHelper._isLinkedByTab)
        {
          View.Dispatcher.Invoke((Action) (() => StatusText = "One or more tabs is linked to another file:"));
          IDictionaryEnumerator de = StaticHelper._tabLinks.GetEnumerator();
          de.Reset();
          while (de.MoveNext())
          {
            string caption = de.Key.ToString();
            string description = ((ILinkageData) de.Value).Description;
            View.Dispatcher.Invoke((Action) (() => LinksDescriptions.Add(description)));
          }

          View.Dispatcher.Invoke((Action) (() => LayoutEnabled = true));

          if (LinksDescriptions.Count > 0)
          {
            ((CollectionView)CollectionViewSource.GetDefaultView(LinksDescriptions)).MoveCurrentToFirst();
          }
          else
          {
            StaticHelper._isLinkedByLayout = false;
            StaticHelper._isLinkedByTab = false;
            View.Dispatcher.Invoke((Action)(() => StatusText = "Your layout is not linked to another file"));
            //MessageBox.Show("Your layout is local and doesn't have links to any remote files", "No Links Detected");
            View.Dispatcher.Invoke((Action)(InvokeCloseRequested));
          }
        } 
        else
        {
          View.Dispatcher.Invoke((Action) (() => StatusText = "Your layout is not linked to another file"));
          //MessageBox.Show("Your layout is local and doesn't have links to any remote files", "No Links Detected");
          InvokeCloseRequested();
        }
      }
      catch (Exception e)
      {
        //if (Log.IsLogLevelEnabled(LogLevel.Error))
        //{
        //  Logger.Error.SetLocation(CLASS, "FillTreeView").
        //    WriteFormat("FillTreeView failed: " + e);
        //}
        View.Dispatcher.Invoke((Action) (() => StatusText = "Dialog Initialization failed"));
      }

      View.Dispatcher.Invoke((Action) (() => IsLoading = false));
    }

    private readonly ObservableCollection<string> m_linksDescriptions =
      new ObservableCollection<string>();
    public ObservableCollection<string> LinksDescriptions
    {
      get
      {
        return m_linksDescriptions;
      }
    }

    private string m_statusText = "The following tabs are linked to another file:";
    public string StatusText
    {
      get
      {
        return m_statusText;
      }
      set
      {
        m_statusText = value;
        InvokePropertyChanged("StatusText");
      }
    }

    private bool m_layoutEnabled = false;
    public bool LayoutEnabled
    {
      get
      {
        return m_layoutEnabled;
      }
      set
      {
        m_layoutEnabled = value;
        InvokePropertyChanged("LayoutEnabled");
      }
    }

    private bool m_isLoading = true;
    public bool IsLoading
    {
      get
      {
        return m_isLoading;
      }
      set
      {
        m_isLoading = value;
        InvokePropertyChanged("IsLoading");
      }
    }

    private readonly FrameworkElement m_view;
    public FrameworkElement View
    {
      get
      {
        return m_view;
      }
    }

    private readonly RoutedCommand m_cancelCommand = new RoutedCommand();
    public ICommand CancelCommand
    {
      get
      {
        return m_cancelCommand;
      }
    }

    private readonly RoutedCommand m_okCommand = new RoutedCommand();
    public ICommand OkCommand
    {
      get
      {
        return m_okCommand;
      }
    }

    private readonly RoutedCommand m_unlinkCommand = new RoutedCommand();
    public ICommand UnlinkCommand
    {
      get
      {
        return m_unlinkCommand;
      }
    }

    public event EventHandler CloseRequested;

    public void InvokeCloseRequested()
    {
      EventHandler handler = CloseRequested;
      if (handler != null) handler(this, EventArgs.Empty);
    }

    public event PropertyChangedEventHandler PropertyChanged;

    public void InvokePropertyChanged(string propertyName_)
    {
      PropertyChangedEventHandler handler = PropertyChanged;
      if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName_));
    }

    private readonly CommandBindingCollection m_commandBindings = new CommandBindingCollection();
    public CommandBindingCollection CommandBindings
    {
      get
      {
        return m_commandBindings;
      }
    }
  }
}
