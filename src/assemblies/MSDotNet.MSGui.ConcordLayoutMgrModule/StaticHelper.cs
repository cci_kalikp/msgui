﻿using System;
using System.Collections;
using System.Reflection;
using System.Xml;
using MorganStanley.IED.Concord.Application;

namespace MorganStanley.MSDotNet.MSGui.ConcordLayoutMgrModule
{
  /// <summary>
  /// Combined from static functions and fields from MorganStanley.IED.Concord.ConcordLayoutMgr
  /// </summary>  
  internal static class StaticHelper
  {
    public static bool _isLinkedByLayout;
    public static bool _isLinkedByTab;
    public static Hashtable _tabLinks;
    public static object _layoutLink;

    private static IConcordLayoutLinkage _linker;
    private const string DEFAULT_LAYOUT_LINKER_TYPE = "MorganStanley.MSDotNet.MSGui.ConcordLayoutMgrModule.FileLinkMgr, MSDotNet.MSGui.ConcordLayoutMgrModule";

    public static void UnlinkLayout()
    {
      _isLinkedByLayout = false;
      _isLinkedByTab = false;
      _tabLinks = null;
      _layoutLink = null;
    }

    public static string GetLinkerTypeString()
    {
      if (StaticHelper._linker != null)
      {
        return  StaticHelper._linker.GetType().ToString();
      }
      else
      {
        return StaticHelper.DEFAULT_LAYOUT_LINKER_TYPE;
      }
    }

    public static bool CreateLinker(string linkerType_)
    {
      if (linkerType_ == null)
      {
        linkerType_ = DEFAULT_LAYOUT_LINKER_TYPE;
      }

      //create instance of linker 
      Type type = Type.GetType(linkerType_);

      if (type == null)
      {
        //#region Logging
        //if (Log.IsLogLevelEnabled(LogLevel.Error))
        //{
        //  Logger.Error.SetLocation("IED.ConcordLayoutMgr", "CreateLinker").Write(Res.GetString(Res.ERROR_INVALID_TYPE));
        //}
        //#endregion Logging

        throw new Exception(String.Format(Res.ERROR_INVALID_TYPE));
      }
      else if (type.GetInterface("IConcordLayoutLinkage", true) == null)
      {
        //#region Logging
        //if (Log.IsLogLevelEnabled(LogLevel.Error))
        //{
        //  Logger.Error.SetLocation("IED.ConcordLayoutMgr", "CreateLinker").Write(Res.GetString(Res.ERROR_MUST_IMPLEMENT_INTERFACE));
        //}
        //#endregion Logging

        throw new Exception(String.Format(Res.ERROR_MUST_IMPLEMENT_INTERFACE));
      }

      try
      {
        object instance = Activator.CreateInstance(type);

        if (instance != null)
        {
          _linker = (IConcordLayoutLinkage)instance;
        }
      }
      catch (TargetInvocationException ex_)
      {
        //#region Logging
        //if (Log.IsLogLevelEnabled(LogLevel.Error))
        //{
        //  Logger.Error.SetLocation("IED.ConcordLayoutMgr", "CreateLinker").WriteFormat(Res.GetString(Res.ERROR_CAN_NOT_CREATE_INSTANCE), ex_, linkerType);
        //}
        //#endregion Logging

        throw new Exception(String.Format(Res.ERROR_CAN_NOT_CREATE_INSTANCE, linkerType_), ex_);
      }
      return true;
    }

    public static void RegenerateGuids(XmlNode layout_)
    {
      ReplaceConcordFormGuids(layout_);
      ReplaceColleagueGuids(layout_);
    }

    public static XmlElement ResolveTabLinks(XmlElement layout_)
    {      
      #region Validate arguments
      if (layout_ == null) throw new ArgumentNullException("layout_");
      #endregion Validate arguments

      XmlElement merged = layout_;

      XmlNodeList tabs = merged.SelectNodes("TabSplitter/Tab");

      StaticHelper._isLinkedByTab = false;

      foreach (XmlNode tab in tabs)
      {
        string linkerType = "";
        if (tab.Attributes["linker"] != null)
          linkerType = tab.Attributes["linker"].Value;

        if (linkerType != "" && StaticHelper._linker == null)
        {
          StaticHelper.CreateLinker(linkerType);
        }

        if (linkerType != "" && StaticHelper._linker != null)
        {
          XmlNode modifiedTab = StaticHelper._linker.GetTabNode(tab);
          tab.InnerXml = modifiedTab.InnerXml;
          StaticHelper._isLinkedByTab = true;
        }
      }

      if (StaticHelper._isLinkedByTab == false)
      {
        StaticHelper._tabLinks = null;
      }

      if (StaticHelper._linker != null && StaticHelper._isLinkedByTab == true)
      {
        //store this link in collection so we know later on for saving
        StaticHelper._tabLinks = StaticHelper._linker.StoreTabLinkageDetails(layout_);
      }
      return merged;
    }

    private static void ReplaceConcordFormGuids(XmlNode layout_)
    {
      // Replace all concord form guid attribute values and with new guid values.
      XmlNodeList list = layout_.SelectNodes("TabSplitter/Tab/SplitPane/descendant::*[local-name()='Pane']/ConcordForm/@guid");
      foreach (XmlNode atr in list)
      {
        // Replace the guid attribute's value with a new guid.
        atr.Value = Guid.NewGuid().ToString();
      }
    }

    private static void ReplaceColleagueGuids(XmlNode layout_)
    {
      // Replace all colleague colleagueid attribute values with new guid values.
      Hashtable guids = new Hashtable();
      XmlNodeList list = layout_.SelectNodes("TabSplitter/Tab/SplitPane/descendant::*[local-name()='Pane']/ConcordForm/State/ColleagueListenerState/@colleagueid");
      foreach (XmlNode atr in list)
      {
        // Replace the colleagueid attribute's value with a new guid.
        string currentGuid = string.Copy(atr.Value);
        Guid newGuid = Guid.NewGuid();
        atr.Value = newGuid.ToString();

        // Remember what we've done by storing the new guid in a hashtable, using the old guid as the key.
        guids[currentGuid] = atr.Value;
      }

      // Update other attributes where colleague ids are specified so that old id values are replaced with
      // new guid values.
      list = layout_.SelectNodes("TabSplitter/Tab/SplitPane/descendant::*[local-name()='Pane']/ConcordForm/State/ColleagueListenerState/@colleagueid");
      UpdateGuidAttributes(list, guids);
      list = layout_.SelectNodes("TabSplitter/Tab/SplitPane/descendant::*[local-name()='Pane']/ConcordForm/State/LinkControlState/LinkedSet/LinkedSetSubscriber/@colleagueid");
      UpdateGuidAttributes(list, guids);
      list = layout_.SelectNodes("TabSplitter/Tab/SplitPane/descendant::*[local-name()='Pane']/ConcordForm/State/LinkControlState/LinkedSet/@subscribeorder");
      UpdateAtrsContainingMultipleGuids(list, guids);
    }

    private static void UpdateGuidAttributes(XmlNodeList list_, Hashtable replacementValues_)
    {
      foreach (XmlNode atr in list_)
      {
        // Get the replacement value.
        string replacement = (string)replacementValues_[atr.Value];
        if (replacement == null)
        {
          continue;
        }

        // Do the replacement.
        atr.Value = replacement;
      }
    }

    private static void UpdateAtrsContainingMultipleGuids(XmlNodeList list_, Hashtable replacementValues_)
    {
      foreach (XmlNode atr in list_)
      {
        /* The attribute's value is formatted as:
         *   "guid1;guid2;guid3;guid4;...;guidN"
         * Take each guid in turn and replace it with the corresponding replacement guid.
         */
        string guids = string.Copy(atr.Value);
        int index = 0;

        do
        {
          // Find next ; separator character.
          int endIndex = guids.IndexOf(";", index);
          if (endIndex == -1)
          {
            endIndex = guids.Length;
          }

          // Get the replacement value for the guid.
          string replacement = (string)replacementValues_[guids.Substring(index, endIndex - index)];
          if (replacement != null)
          {
            // Do the replacement.
            string temp = string.Empty;

            if (index > 0)
            {
              temp = guids.Substring(0, index + 1);
            }
            guids += replacement + guids.Substring(endIndex);
          }

          // Move index to start of next guid.
          index = endIndex + 1;
        }
        while (index < guids.Length);

        // Update attribute's value.
        atr.Value = guids;
      }
    }

    public static void SaveLayout(XmlElement layout_, IWindowManager windowManager_)
    {
      //if linker hasn't been created then we can't have loaded any linked layouts
      if (StaticHelper._linker != null)
      {
        //just need to detect whether last layout loaded was linked
        if (StaticHelper._isLinkedByLayout)
        {
          //since entire layout is linked only need to save link
          layout_.Attributes.Append(layout_.OwnerDocument.CreateAttribute("linker"));
          layout_.Attributes["linker"].Value = StaticHelper._linker.GetType().ToString();
          //enumerate attribute pairs
          Hashtable pairs = ((ILinkageData)StaticHelper._layoutLink).GetAttributePairs();
          IDictionaryEnumerator de = pairs.GetEnumerator();
          de.Reset();
          while (de.MoveNext())
          {
            string key = de.Key.ToString();
            string val = de.Value.ToString();
            layout_.Attributes.Append(layout_.OwnerDocument.CreateAttribute(key));
            layout_.Attributes[key].Value = val;
          }
          return;
        }

        if (StaticHelper._isLinkedByTab)
        {
          //call base first then alter tabs
          windowManager_.SaveLayout(layout_);

          if (StaticHelper._tabLinks != null)
          {
            IDictionaryEnumerator de = StaticHelper._tabLinks.GetEnumerator();
            de.Reset();
            while (de.MoveNext())
            {
              string caption = de.Key.ToString();
              Hashtable pairs = ((ILinkageData)de.Value).GetAttributePairs();
              if (pairs != null)
              {
                IDictionaryEnumerator pairsEnum = pairs.GetEnumerator();
                pairsEnum.Reset();
                while (pairsEnum.MoveNext())
                {
                  string key = pairsEnum.Key.ToString();
                  string val = pairsEnum.Value.ToString();
                  //now get correct tab and change it
                  XmlNodeList nodeList = layout_.SelectNodes("TabSplitter/Tab");
                  foreach (XmlNode node in nodeList)
                  {
                    if (node.Attributes["caption"] != null && node.Attributes["caption"].Value != null &&
                      node.Attributes["caption"].Value == caption)
                    {
                      if (node.Attributes[key] != null)
                      {
                        node.Attributes[key].Value = val;
                      }
                      else
                      {
                        node.Attributes.Append(layout_.OwnerDocument.CreateAttribute(key));
                        node.Attributes[key].Value = val;
                      }

                      if (node.Attributes["linker"] != null)
                      {
                        node.Attributes["linker"].Value = StaticHelper._linker.GetType().ToString();
                      }
                      else
                      {
                        node.Attributes.Append(layout_.OwnerDocument.CreateAttribute("linker"));
                        node.Attributes["linker"].Value = StaticHelper._linker.GetType().ToString();
                      }
                    }
                  }
                }
              }
            }
          }
          return;
        }
        windowManager_.SaveLayout(layout_);
      }
      else
        windowManager_.SaveLayout(layout_);
    }

    public static void LoadLayout(XmlElement layout_, IWindowManager windowManager_)
    {
      #region Validate arguments
      if (layout_ == null) throw new ArgumentNullException("layout_");
      #endregion Validate arguments

      //if (Log.IsLogLevelEnabled(LogLevel.Info)) 
      //{
      //  Logger.Info.SetLocation("PaneWindowManagerEx", "LoadLayout").WriteFormat("LoadLayout '{0}'", layout_.Attributes["name"].Value);
      //}

      string linkerType = "";
      if (layout_.Attributes["linker"] != null)
        linkerType = layout_.Attributes["linker"].Value;

      bool success = false;
      if (linkerType != "" && StaticHelper._linker == null)
      {
        success = StaticHelper.CreateLinker(linkerType);
      }

      if (StaticHelper._linker != null)
      {
        //store this link in collection so we know later on for saving
        StaticHelper._layoutLink = StaticHelper._linker.StoreLayoutLinkageDetails(layout_);
        if (StaticHelper._layoutLink == null)
        {
          StaticHelper._isLinkedByLayout = false;
        }
        else
        {
          StaticHelper._isLinkedByLayout = true;
        }
      }
      else
      {
        StaticHelper._isLinkedByLayout = false;
        StaticHelper._layoutLink = null;
      }

      if (linkerType == "")
      {
        //all linking of layouts has been done but now must link in
        //individual tabs if they are referenced from elsewhere
        XmlElement resolvedLayout = StaticHelper.ResolveTabLinks(layout_);
        windowManager_.LoadLayout(resolvedLayout);
      }
      else
      {
        XmlNode linkedNode = StaticHelper._linker.GetLayoutNode(layout_);
        LoadLinkedLayout((XmlElement)linkedNode, windowManager_);
      }
    }

    private static void LoadLinkedLayout(XmlElement layout_, IWindowManager windowManager_)
    {
      #region Validate arguments
      if (layout_ == null) throw new ArgumentNullException("layout_");
      #endregion Validate arguments

      //if (Log.IsLogLevelEnabled(LogLevel.Info)) 
      //{
      //  Logger.Info.SetLocation("PaneWindowManagerEx", "LoadLinkedLayout").WriteFormat("LoadLinkedLayout '{0}'", layout_.Attributes["name"].Value);
      //}

      string linkerType = "";
      if (layout_.Attributes["linker"] != null)
        linkerType = layout_.Attributes["linker"].Value;

      bool success = false;
      if (linkerType != "" && StaticHelper._linker == null)
      {
        success = StaticHelper.CreateLinker(linkerType);
      }

      if (linkerType == "")
      {
        //all linking of layouts has been done but now must link in
        //individual tabs if they are referenced from elsewhere

        XmlElement resolvedLayout = StaticHelper.ResolveTabLinks(layout_);
        windowManager_.LoadLayout(resolvedLayout);
      }
      else
      {
        XmlNode linkedNode = StaticHelper._linker.GetLayoutNode(layout_);
        //recurse until only linked at tab level or not linked
        LoadLinkedLayout((XmlElement)linkedNode, windowManager_);
      }
    }
  }
}
