/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.ConcordLayoutMgrModule/FileLinkMgr.cs#5 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.Xml;
using System.Collections;
using MorganStanley.IED.Concord.Configuration;

namespace MorganStanley.MSDotNet.MSGui.ConcordLayoutMgrModule
{
	/// <summary>
	/// Summary description for FileLinkMgr.
	/// </summary>
	public class FileLinkMgr : IConcordLayoutLinkage
	{    
		public static readonly string CLASS = "FileLinkMgr";
		private Configurator m_config;
    
    /// <summary>
    /// Default contructor.
    /// </summary>
		public FileLinkMgr()
		{
			try 
			{
				m_config = (Configurator) ConfigurationManager.GetConfig("Layouts");
      }
      catch (Exception e)
      {
        //#region Logging
        //if (Log.IsLogLevelEnabled(LogLevel.Error))
        //{
        //  Logger.Error.SetLocation(CLASS, "FileLinkMgr").
        //    WriteFormat("Error in constructor " + e.ToString());
        //}	
        //#endregion Logging

      }
    }
    
		public XmlNode GetLayoutNode(XmlNode node_)
		{
			string path = "";
			if (node_.Attributes["path"] != null)
				path = node_.Attributes["path"].Value;
			
			if (path == "")
			{

        //#region Logging
        //if (Log.IsLogLevelEnabled(LogLevel.Error))
        //{
        //  Logger.Error.SetLocation("IED.ConcordLayoutMgr.FileLinkMgr", "GetLayoutNode").Write(Res.GetString(Res.ERROR_PATH_NOT_SPECIFIED));
        //}
        //#endregion Logging

				throw new Exception(String.Format(Res.ERROR_PATH_NOT_SPECIFIED));

			}

			Configurator config = (Configurator) ConfigurationManager.GetConfig(path);
			XmlNode layoutNode = config.GetNode("Layout",null);
			return  layoutNode;
		}

		public XmlNode GetTabNode(XmlNode node_)
		{
			#region Validate Node
			
			string path = "";
			if (node_.Attributes["path"] != null)
				path = node_.Attributes["path"].Value;
			
			if (path == "")
			{
        //#region Logging
        //if (Log.IsLogLevelEnabled(LogLevel.Error))
        //{
        //  Logger.Error.SetLocation("IED.ConcordLayoutMgr.FileLinkMgr", "GetTabNode").Write(Res.GetString(Res.ERROR_PATH_NOT_SPECIFIED));
        //}
        //#endregion Logging

				throw new Exception(String.Format(Res.ERROR_PATH_NOT_SPECIFIED));

			}

			string linkedTab = "";
			if (node_.Attributes["linkedTab"] != null)
				linkedTab = node_.Attributes["linkedTab"].Value;
			
			if (linkedTab == "")
			{
        //#region Logging
        //if (Log.IsLogLevelEnabled(LogLevel.Error))
        //{
        //  Logger.Error.SetLocation("IED.ConcordLayoutMgr.FileLinkMgr", "GetTabNode").Write(Res.GetString(Res.ERROR_TAB_NOT_SPECIFIED));
        //}
        //#endregion Logging

				throw new Exception(String.Format(Res.ERROR_LINKED_TAB_NOT_SPECIFIED));

			}

			#endregion Validate Node

			Configurator config = (Configurator) ConfigurationManager.GetConfig(path);
			XmlNode layoutNode = config.GetNode("Layout",null);

			XmlNode target = layoutNode.SelectSingleNode("TabSplitter/Tab[@caption='" + linkedTab +  "']" );
			XmlNode copy = target.Clone();
			
			return copy;
		}
    
    public Hashtable StoreTabLinkageDetails(XmlNode layout_)
    {
      Hashtable links = new Hashtable();
      XmlNodeList nodeList = layout_.SelectNodes("TabSplitter/Tab");
      foreach (XmlNode node in nodeList)
      {     
        if (node.Attributes["linkedTab"] != null && node.Attributes["path"] != null && node.Attributes["caption"] != null)
        {
          LinkageData data = new LinkageData
                               {
                                 LinkedTab = node.Attributes["linkedTab"].Value,
                                 Name = node.Attributes["caption"].Value,
                                 Path = node.Attributes["path"].Value
                               };
          links.Add(node.Attributes["caption"].Value,data);
        }       
      }
      return links;
    }

    public object StoreLayoutLinkageDetails(XmlNode node_)
    {
      if (node_.Attributes["path"] == null || node_.Attributes["name"] == null)
      {
        return null;
      }
      LinkageData data = new LinkageData
                           {
                             Name = node_.Attributes["name"].Value,
                             Path = node_.Attributes["path"].Value
                           };
      return data;
    }
	}
}
