﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace MorganStanley.MSDotNet.MSGui.ConcordLayoutMgrModule.Controls
{
  /// <summary>
  /// Interaction logic for SaveFileBox.xaml
  /// </summary>
  public partial class SaveFileBox : UserControl
  {
    public SaveFileBox()
    {
      InitializeComponent();
    }

    public string Text
    {
      get { return (string)this.GetValue(TextProperty); }
      set { this.SetValue(TextProperty, value); }
    }    
    
    public static readonly DependencyProperty TextProperty = DependencyProperty.Register(
      "Text", typeof(string), typeof(SaveFileBox), new PropertyMetadata(string.Empty));

    public string DefaultDirectory
    {
      get { return (string)this.GetValue(DefaultDirectoryProperty); }
      set { this.SetValue(DefaultDirectoryProperty, value); }
    }

    public static readonly DependencyProperty DefaultDirectoryProperty = DependencyProperty.Register(
      "DefaultDirectory", typeof(string), typeof(SaveFileBox), new PropertyMetadata(string.Empty));

    public string DefaultFilename
    {
      get { return (string)this.GetValue(DefaultFilenameProperty); }
      set { this.SetValue(DefaultFilenameProperty, value); }
    }

    public static readonly DependencyProperty DefaultFilenameProperty = DependencyProperty.Register(
      "DefaultFilename", typeof(string), typeof(SaveFileBox), new PropertyMetadata(null));

    public string FileExtension
    {
      get { return (string)this.GetValue(FileExtensionProperty); }
      set { this.SetValue(FileExtensionProperty, value); }
    }

    public static readonly DependencyProperty FileExtensionProperty = DependencyProperty.Register(
      "FileExtension", typeof(string), typeof(SaveFileBox), new PropertyMetadata(string.Empty));

    private bool m_dialogOpened = false;


    private void OnTextBoxMouseDown(object sender_, MouseButtonEventArgs e_)
    {
      if (m_dialogOpened)
      {
        return;
      }

      Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
      if (!string.IsNullOrEmpty(DefaultDirectory) && string.IsNullOrEmpty(Text))
      {
        dlg.InitialDirectory = DefaultDirectory;        
      }
      dlg.FileName = string.IsNullOrEmpty(Text) ? (DefaultFilename ?? "Document") : Text; // Default file name

      if (!string.IsNullOrEmpty(FileExtension))
      {
        dlg.DefaultExt = FileExtension; // Default file extension
      }

      //dlg.Filter = "Text documents (.txt)|*.txt"; // Filter files by extension
      
      m_dialogOpened = true;
      bool? result = dlg.ShowDialog(Window.GetWindow(this));
      m_dialogOpened = false;
      
      if (result == true)
      {
        Text = dlg.FileName;
      }      
    }
  }
}
