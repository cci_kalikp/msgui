﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.ConcordLayoutMgrModule/Controls/ExportView.xaml.cs#3 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System.Windows.Controls;

namespace MorganStanley.MSDotNet.MSGui.ConcordLayoutMgrModule.Controls
{
  /// <summary>
  /// Interaction logic for ExportView.xaml
  /// </summary>
  public partial class ExportView : UserControl
  {
    public ExportView()
    {
      InitializeComponent();
      
    }

    public void ShowProgressBar()
    {
      
    }

    
  }
}
