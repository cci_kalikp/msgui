﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.ConcordLayoutMgrModule/Controls/LayoutManagerMenu.xaml.cs#3 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.Windows;
using System.Windows.Controls;

namespace MorganStanley.MSDotNet.MSGui.ConcordLayoutMgrModule.Controls
{
  /// <summary>
  /// Interaction logic for LayoutManagerMenu.xaml
  /// </summary>
  public partial class LayoutManagerMenu : UserControl
  {
    public LayoutManagerMenu()
    {
      InitializeComponent();
    }

    public event EventHandler ImportRequested;
    public event EventHandler ExportRequested;
    public event EventHandler LinksRequested;

    private void OnViewLinks(object sender_, RoutedEventArgs e_)
    {
      EventHandler handler = LinksRequested;
      if (handler != null) handler(this, e_);
    }

    private void OnExport(object sender_, RoutedEventArgs e_)
    {
      EventHandler handler = ExportRequested;
      if (handler != null) handler(this, e_);
    }

    private void OnImport(object sender_, RoutedEventArgs e_)
    {
      EventHandler handler = ImportRequested;
      if (handler != null) handler(this, e_);
    }
  }
}
