﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.ConcordLayoutMgrModule/Controls/TreeItem.cs#3 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace MorganStanley.MSDotNet.MSGui.ConcordLayoutMgrModule.Controls
{
  internal class TreeItem : INotifyPropertyChanged
  {
    private readonly IEnumerable<TreeItem> m_roots;

    public TreeItem(string name_, IEnumerable<TreeItem> roots_)
    {
      m_roots = roots_;
      Name = name_;
    }

    public TreeItem(string name_, string config_, IEnumerable<TreeItem> roots_)
    {
      m_roots = roots_;
      Name = name_;
      Config = config_;
    }

    public string Name
    {
      get; private set;
    }

    public string Config
    {
      get; private set;
    }

    private readonly ObservableCollection<TreeItem> m_children = new ObservableCollection<TreeItem>();
    public ObservableCollection<TreeItem> Children
    {
      get
      {
        return m_children;
      }
    }

    public void AddChild(TreeItem item_)
    {
      m_children.Add(item_);
      item_.Parent = this;
    }

    public TreeItem Parent
    {
      get; private set;
    }

    private bool m_isChecked;    

    public bool IsChecked
    {
      get
      {
        return m_isChecked;
      }
      set
      {
        m_isChecked = value;

        //the following logic was copied with slight modifications from MorganStanley.IED.Concord.ConcordLayoutMgr.Import.treeViewTabs_AfterCheck
        if (m_isChecked) //was checked
        {
          //if node has children they are all checked
          if (Parent == null)
          {
            foreach (TreeItem child in Children)
            {
              child.IsChecked = true;
            }

            //if any peers are checked they are unchecked
            foreach (TreeItem parent in m_roots)
            {
              if (parent != this)
              {
                parent.IsChecked = false;
                foreach (TreeItem peerChild in parent.Children)
                {
                  peerChild.IsChecked = false;
                }
              }
            }
          }
          else
          {
            //have checked child node - ensure that only the parent and peers can be checked
            //if any other parents are checked they are unchecked
            foreach (TreeItem parent in m_roots)
            {
              if (parent != Parent)
              {
                if (parent.IsChecked == true)
                {
                  parent.IsChecked = false;
                  foreach (TreeItem peerChild in parent.Children)
                  {
                    peerChild.IsChecked = false;
                  }
                }
              }
            }
          }
        }
        else //was unchecked
        {
          //uncheck parent if child
          if (Parent != null)
          {
            Parent.IsChecked = false;
          }
        }
        InvokePropertyChanged("IsChecked");
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    public void InvokePropertyChanged(string propertyName_)
    {
      PropertyChangedEventHandler handler = PropertyChanged;
      if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName_));
    }
  }
}
