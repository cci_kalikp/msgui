/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.ConcordLayoutMgrModule/LinkageData.cs#4 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System.Collections;

namespace MorganStanley.MSDotNet.MSGui.ConcordLayoutMgrModule
{
  /// <summary>
  ///  Implementation allows caller to determine xml attribute keys and values required for linking layouts.
  /// </summary>
  /// <threadsafety>
  ///  not tested
  /// </threadsafety>
  public class LinkageData : ILinkageData
  {
    /// <summary>
    /// Name of the layout or tab.
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Configuration name (eg. MyLayout.layout) to which we will link.
    /// </summary>
    public string Path { get; set; }

    /// <summary>
    /// Name of the tab which is the target of this link.
    /// </summary>
    public string LinkedTab { get; set; }
       
    public Hashtable GetAttributePairs()
    {
      Hashtable pairs = new Hashtable();
      if (Path != null)
      {
        pairs.Add("path", Path);
      }
      if (LinkedTab != null)
      {
        pairs.Add("linkedTab", LinkedTab);
      }
      return pairs; 
    }

    public string Description
    {
      get
      {
        return "Local Tab: " + Name +  " Remote Layout: " + Path + " Remote Tab: " + LinkedTab;
      }
    }
  }
}