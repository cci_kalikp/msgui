/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.ConcordLayoutMgrModule/IConcordLayoutLinkage.cs#4 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System.Xml;
using System.Collections;

namespace MorganStanley.MSDotNet.MSGui.ConcordLayoutMgrModule
{

	public interface IConcordLayoutLinkage
	{
    /// <summary>
    /// Returns entire layout node (as in Layouts.config file) which has been linked at the layout level.
    /// </summary>
    /// <param name="node_">The xml node containing the linking info.</param>
    /// <returns>An xml node with the expanded (linked) layout.</returns>

		XmlNode GetLayoutNode(XmlNode node_);

		XmlNode GetTabNode(XmlNode node_);
    Hashtable StoreTabLinkageDetails(XmlNode node_);
    object StoreLayoutLinkageDetails(XmlNode node_);
	}
}