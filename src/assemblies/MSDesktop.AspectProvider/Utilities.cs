﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;  
using MorganStanley.MSDotNet.MSLog;
using PostSharp.Aspects;

namespace MSDesktop.AspectProvider
{
    public static class Utilities
    {
        private static readonly Type LogUsageAttrType = typeof(LogUsageAttribute);
        private static readonly Type CompilerGeneratedAttrType = typeof (CompilerGeneratedAttribute);
        public static bool ShouldIgnore(this Type type_)
        {
            return (!type_.Assembly.Location.StartsWith(CollectMethodUsageAspectProvider.CurrentFolder.FullName) ||
                    type_.GetCustomAttributes(CompilerGeneratedAttrType, false).Length > 0);

        }
        public static string ToDisplayString(this MethodBase method_)
        {
            StringBuilder builder = new StringBuilder();
            Type declaringType = method_.DeclaringType;
            if (declaringType != null && !string.IsNullOrEmpty(declaringType.FullName))
            {
                builder.Append(declaringType.FullName.Replace('+', '.'));
                builder.Append(".");
            }
            builder.Append(method_.Name);
            if ((method_ is MethodInfo) && ((MethodInfo)method_).IsGenericMethod)
            {
                Type[] genericArguments = ((MethodInfo)method_).GetGenericArguments();
                builder.Append("[");
                int index = 0;
                bool flag3 = true;
                while (index < genericArguments.Length)
                {
                    if (!flag3)
                    {
                        builder.Append(",");
                    }
                    else
                    {
                        flag3 = false;
                    }
                    builder.Append(genericArguments[index].Name);
                    index++;
                }
                builder.Append("]");
            }
            builder.Append("(");
            ParameterInfo[] parameters = method_.GetParameters();
            bool flag4 = true;
            for (int j = 0; j < parameters.Length; j++)
            {
                if (!flag4)
                {
                    builder.Append(", ");
                }
                else
                {
                    flag4 = false;
                }
                string name = "<UnknownType>";
                if (parameters[j].ParameterType != null)
                {
                    name = parameters[j].ParameterType.Name;
                }
                builder.Append(name + " " + parameters[j].Name);
            }
            builder.Append(")");
            return builder.ToString();
        }

        public static IEnumerable<Type> GetLoadableTypes(this Assembly assembly_)
        {
            try
            {
                return assembly_.GetTypes();
            }
            catch (ReflectionTypeLoadException e)
            {
                foreach (var loaderException in e.LoaderExceptions)
                { 
                    Console.WriteLine(loaderException);
                } 
                return e.Types.Where(t => t != null);
            }
        }

        public static void LogUsage(MethodInterceptionArgs methodArgs_, MethodBase declaredMethod_ =null)
        { 
            var methodData = new MethodData()
            {
                DeclaredMethod = declaredMethod_ ?? methodArgs_.Method,
                ImplementedMethod = declaredMethod_ == null ? null : methodArgs_.Method,
                Arguments = methodArgs_.Arguments.ToArray(),
                CallingMethod = new StackTrace().GetFrame(3).GetMethod(),
            };

     
            if (methodData.ImplementedMethod == null && methodData.DeclaredMethod != null && methodArgs_.Instance != null && !methodData.DeclaredMethod.IsStatic)
            {
                var parameterTypes = Array.ConvertAll(methodData.DeclaredMethod.GetParameters(), input_ => input_.ParameterType); 
                var implementedMethod = methodArgs_.Instance.GetType()
                                                   .GetMethod(methodData.DeclaredMethod.Name,
                                                              BindingFlags.Public | BindingFlags.NonPublic |
                                                              BindingFlags.Instance,
                                                              null, parameterTypes, null);
                if (implementedMethod != null)
                {
                    methodData.ImplementedMethod = implementedMethod;
                }
            }
 
            MethodInfo method = typeof(MSLog).GetMethod("LogMethodUsage", BindingFlags.Public | BindingFlags.Static);
            if (method != null)
            { 
                method.Invoke(null, new object[] { methodData }); 
            } 
            //MSLog.LogMethodUsage(methodData); 
        }

        public static bool ShouldLogUsage(this Type type_)
        {
            var attr = type_.GetCustomAttributes(LogUsageAttrType, false);
            return attr.Length > 0; 
        }

        public static bool ShouldLogUsage(this MethodBase method_)
        {
            var attr = method_.GetCustomAttributes(LogUsageAttrType, false);
            return attr.Length > 0;
        }
    }


 
}
