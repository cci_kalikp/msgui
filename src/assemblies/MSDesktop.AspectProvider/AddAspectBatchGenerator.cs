﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AddAspectBatchGenerator
{
    class Program
    {
        public static void Main(string[] args_)
        {
            MSDesktop.AspectProvider.CompileTimeAssemblyResolver.GenerateExecuteBatch(args_[0]);
        }
    }
}
