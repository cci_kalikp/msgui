﻿using System.Reflection;
using System.Runtime.CompilerServices;
#if TEST
[assembly: AssemblyTitle("MSDesktop.AspectProvider")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyProduct("MSDesktop.AspectProvider")]
[assembly: AssemblyCopyright("Copyright © Morgan Stanley 2014")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyVersion("9999.99.99.9")]
[assembly: AssemblyFileVersion("9999.99.99.9")]

#endif