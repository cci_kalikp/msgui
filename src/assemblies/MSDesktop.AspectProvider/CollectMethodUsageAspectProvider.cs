﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection; 
using System.Xml; 
using PostSharp.Aspects; 

namespace MSDesktop.AspectProvider
{
     
    //todo: support explicit interface 
    public class CollectMethodUsageAspectProvider : IAspectProvider 
    { 
        private static readonly List<string> InterfacesToTrack = new List<string>(); 
        private static readonly List<string> ClassesToTrack = new List<string>(); 
        internal static Dictionary<string, StrongKeyType> Assemblies = new Dictionary<string, StrongKeyType>(); 
        private readonly static CollectMethodUsageAspect Aspect = new CollectMethodUsageAspect();
        public readonly static DirectoryInfo CurrentFolder = new DirectoryInfo(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
        static CollectMethodUsageAspectProvider()
        {
            if (!File.Exists("AspectConfiguration.xml"))
            {
                return;
            }
            XmlDocument doc = new XmlDocument();
            doc.Load("AspectConfiguration.xml");
            XmlNodeList interfaceNodes = doc.SelectNodes("//Interface");
            if (interfaceNodes != null)
            {
                foreach (XmlElement interfaceNode in interfaceNodes)
                {
                    string interfaceName = interfaceNode.GetAttribute("Name");
                    if (string.IsNullOrEmpty(interfaceName)) continue;
                    InterfacesToTrack.Add(interfaceName);  
                }
            }
            XmlNodeList classNodes = doc.SelectNodes("//Class");
            if (classNodes != null)
            {
                foreach (XmlElement classNode in classNodes)
                {
                    string className = classNode.GetAttribute("Name");
                    if (string.IsNullOrEmpty(className)) continue;
                    ClassesToTrack.Add(className);
                }
            }

            XmlNodeList assemblyNodes = doc.SelectNodes("//Assembly");
            if (assemblyNodes != null)
            {
                foreach (XmlElement assemblyNode in assemblyNodes)
                {
                    List<string> assembliesNames = new List<string>();
                    string assemblyName = assemblyNode.GetAttribute("Name");
                    if (string.IsNullOrEmpty(assemblyName))
                    {
                        string assemblyNamePattern = assemblyNode.GetAttribute("Pattern");
                        if (string.IsNullOrEmpty(assemblyNamePattern)) continue;
                        assembliesNames.AddRange(CurrentFolder.GetFiles(assemblyNamePattern + ".dll", SearchOption.TopDirectoryOnly).
                            Select(file_ => Path.GetFileNameWithoutExtension(file_.Name)));
                        if (assembliesNames.Count == 0) continue;
                    }
                    else
                    {
                        assembliesNames.Add(assemblyName);
                    }
                    
                    StrongKeyType keyType = StrongKeyType.None;
                    string keyTypeStr = assemblyNode.GetAttribute("StrongKeyType");
                    if (!string.IsNullOrEmpty(keyTypeStr))
                    {
                        if (!Enum.TryParse(keyTypeStr, out keyType))
                        {
                            keyType = StrongKeyType.None;
                        }
                    }
                    foreach (var assemblyName2 in assembliesNames)
                    {
                        Assemblies[assemblyName2] = keyType;
                    }
                }
            }

        }
        
 
        public IEnumerable<AspectInstance> ProvideAspects(object targetElement_)
        {
            if (Assemblies.Count == 0) return new List<AspectInstance>();

            try
            {
                Assembly assembly = (Assembly)targetElement_; 
                var instances = new List<AspectInstance>();

                foreach (Type type in assembly.GetLoadableTypes())
                {
                    ProcessType(type, instances);
                }
                return instances;
            }
            catch (ReflectionTypeLoadException ex)
            {
                Console.WriteLine(ex.ToString());
                if (ex.InnerException != null)
                {
                    Console.WriteLine(ex.InnerException.ToString());
           
                }
                Console.WriteLine(ex.StackTrace);
                throw;
            }

        }


        private void ProcessType(Type type_, List<AspectInstance> instances_)
        {
            if (!type_.IsInterface && !type_.ShouldIgnore())
            {
                var methodsToTrack = GetMethodsToTrack(type_);
                if (methodsToTrack.Count > 0)
                {
                    foreach (var methodBase in methodsToTrack)
                    {
                        if (methodBase.Key == methodBase.Value)
                        {
                            Console.WriteLine(@"Method Usage found for {0} ", methodBase.Key.ToDisplayString());
                            instances_.Add(new AspectInstance(methodBase.Value, Aspect));
                        }
                        else
                        {
                            Console.WriteLine(@"Method Usage found for {0} implemented by {1}", methodBase.Key.ToDisplayString(), methodBase.Value.ToDisplayString());

                            instances_.Add(new AspectInstance(methodBase.Value,
                                                        new CollectMethodUsageAspect(methodBase.Key)));
                        }
                    }
                }
            }
           
           
            foreach (Type nestedType in type_.GetNestedTypes())
            { 
                ProcessType(nestedType, instances_);
            }
        }


        private Dictionary<MethodBase, MethodBase> GetMethodsToTrack(Type type_)
        {
            var methods = new Dictionary<MethodBase, MethodBase>(); 
            foreach (Type interfaceType in type_.GetInterfaces())
            { 
                var interfaceMapping = type_.GetInterfaceMap(interfaceType);
                var targetMethods = interfaceMapping.TargetMethods;
                var interfaceMethods = interfaceMapping.InterfaceMethods;
                bool logAllMethods = InterfacesToTrack.Contains(interfaceType.FullName) ||
                                     interfaceType.ShouldLogUsage();
                for (int i = 0; i < targetMethods.Length; i++)
                { 
                    if (logAllMethods || interfaceMethods[i].ShouldLogUsage())
                    {
                        methods[interfaceMethods[i]] = targetMethods[i];         
                    }
                }
                  
            }
            Type definitionType = type_; 
            while (definitionType != null && !definitionType.ShouldIgnore())
            { 
                //Console.WriteLine(@"    Definition Type: " + definitionType.FullName);
                    var members = definitionType.GetMembers(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public |
                    BindingFlags.InvokeMethod | BindingFlags.DeclaredOnly | BindingFlags.GetProperty | BindingFlags.SetProperty |
                    BindingFlags.CreateInstance);
                bool logAllMethods = ClassesToTrack.Contains(definitionType.FullName) ||
                    definitionType.ShouldLogUsage();
                foreach (var memberInfo in members)
                {
                    MethodBase method = memberInfo as MethodBase; 
                    if (method != null &&
                        (logAllMethods || method.ShouldLogUsage()))
                    {
                        if (method.IsStatic || method.IsFinal ||
                            (!method.IsVirtual && !method.IsAbstract) ||
                            definitionType.IsSealed)
                        {
                            methods[method] = method;
                        }
                        else
                        {
                            var parameters = method.GetParameters();
                            var types = new Type[parameters.Length];
                            for (int i = 0; i < parameters.Length; i++)
                            {
                                types[i] = parameters[i].ParameterType;
                            }
                            var targetMethod = method.IsConstructor
                                                   ? type_.GetConstructor(types) as MethodBase
                                                   : type_.GetMethod(method.Name, types);
                            if (targetMethod != null && !targetMethod.IsAbstract)
                            {
                                methods[method] = targetMethod;
                            }
                        }
                    }

                }
                definitionType = definitionType.BaseType;
            } 

            return methods;

        }
    }
}
