﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml; 

namespace MSDesktop.AspectProvider
{
    public class CompileTimeAssemblyResolver
    {
        private static readonly Dictionary<AssemblyName, string> assemblyDescriptors = new Dictionary<AssemblyName, string>();
        private const string IgVersionShort = "v14.1";
        private const string IgVersion = "14.1.2062";
        private const string IgVersionFull = "14.1.20141.2062";
        private const string DnPartsVersion = "2014.5.23.1";
        private const string DnPartsVersionFull = "2014.05.23.1";
 
        private static void AddAssemblyResolver(string assemblyName_, string afsPath_)
        {
            assemblyDescriptors[new AssemblyName(assemblyName_)] = afsPath_;
        }

        static CompileTimeAssemblyResolver()
        { 
            AddAssemblyResolver("ICSharpCode.SharpZipLib, Version=0.86.0.518, Culture=neutral, PublicKeyToken=1b03e6acf1164f73", 
                "\\\\ms\\dist\\msdotnet\\PROJ\\sharpziplib\\0.86.0\\assemblies\\ICSharpCode.SharpZipLib.dll");
            AddAssemblyResolver("DeskApps.attachlink-api, Version=1.4.1.18236, Culture=neutral, PublicKeyToken=24b9654a6b7f384a",
                "\\\\ms\\dist\\deskapps\\PROJ\\attachlink-api\\1.4.1\\assemblies\\DeskApps.attachlink-api.dll");
            AddAssemblyResolver("Microsoft.WindowsAPICodePack.Shell, Version=1.1.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35",
                "\\\\ms\\dist\\dotnet3rd\\PROJ\\windowsAPICodePack\\1.1\\binaries\\Microsoft.WindowsAPICodePack.Shell.dll");
            AddAssemblyResolver("System.Reactive.Windows.Threading, Version=2.0.20823.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35",
                 "\\\\ms\\dist\\microsoft\\PROJ\\rx\\2.0.20823\\assemblies\\4.0\\System.Reactive.Windows.Threading.dll");
            AddAssemblyResolver("System.Reactive.Interfaces, Version=2.0.20823.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35",
                "\\\\ms\\dist\\microsoft\\PROJ\\rx\\2.0.20823\\assemblies\\4.0\\System.Reactive.Interfaces.dll");
            AddAssemblyResolver("System.Reactive.Core, Version=2.0.20823.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35",
                 "\\\\ms\\dist\\microsoft\\PROJ\\rx\\2.0.20823\\assemblies\\4.0\\System.Reactive.Core.dll");
            AddAssemblyResolver("System.Reactive.Linq, Version=2.0.20823.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35",
                 "\\\\ms\\dist\\microsoft\\PROJ\\rx\\2.0.20823\\assemblies\\4.0\\System.Reactive.Linq.dll");
            AddAssemblyResolver("Newtonsoft.Json, Version=4.5.0.0, Culture=neutral, PublicKeyToken=30ad4fe6b2a6aeed",
                @"\\san01b\DevAppsGML\dist\dotnet3rd\PROJ\jsondotnet\5.0.6\assemblies\Net40\Newtonsoft.Json.dll");
            AddAssemblyResolver("Xceed.Wpf.Toolkit, Version=2.0.0.0, Culture=neutral, PublicKeyToken=3e4669d2f30244f4",
                @"\\san01b\DevAppsGML\dist\dotnet3rd\PROJ\extendedWPFToolkit\2.0.0.0\assemblies\Xceed.Wpf.Toolkit.dll");
            AddAssemblyResolver("System.Windows.Interactivity, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35",
                @"\\san01b\DevAppsGML\dist\microsoft\PROJ\prism\4.0.0\assemblies\System.Windows.Interactivity.dll");
            AddAssemblyResolver("Microsoft.Practices.Composite, Version=2.0.1.0, Culture=neutral, PublicKeyToken=d0e341ccaeb444f7",
                 @"\\san01b\DevAppsGML\dist\microsoft\PROJ\composite\2.2.0\assemblies\Microsoft.Practices.Composite.dll");
            AddAssemblyResolver("Microsoft.Practices.Composite.Presentation, Version=2.0.1.0, Culture=neutral, PublicKeyToken=d0e341ccaeb444f7",
                @"\\san01b\DevAppsGML\dist\microsoft\PROJ\composite\2.2.0\assemblies\Microsoft.Practices.Composite.Presentation.dll");
            AddAssemblyResolver("Microsoft.Practices.Composite.UnityExtensions, Version=2.0.1.0, Culture=neutral, PublicKeyToken=d0e341ccaeb444f7",
                 @"\\san01b\DevAppsGML\dist\microsoft\PROJ\composite\2.2.0\assemblies\Microsoft.Practices.Composite.UnityExtensions.dll");
            AddAssemblyResolver("Microsoft.Practices.ServiceLocation, Version=1.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35",
                @"\\san01b\DevAppsGML\dist\microsoft\PROJ\composite\2.2.0\assemblies\Microsoft.Practices.ServiceLocation.dll");
            AddAssemblyResolver("Microsoft.Practices.ObjectBuilder2, Version=2.2.0.0, Culture=neutral, PublicKeyToken=d0e341ccaeb444f7",
                @"\\san01b\DevAppsGML\dist\microsoft\PROJ\unity\1.2.0\assemblies\Microsoft.Practices.ObjectBuilder2.dll");
            AddAssemblyResolver("Microsoft.Practices.Unity, Version=2.0.414.0, Culture=neutral, PublicKeyToken=d0e341ccaeb444f7",
                 @"\\san01b\DevAppsGML\dist\microsoft\PROJ\unity\2.0.0\assemblies\Microsoft.Practices.Unity.dll");
            AddAssemblyResolver("Concord.Runtime, Version=2.1.1.0, Culture=neutral, PublicKeyToken=354cc34ba0b81117",
                 @"\\san01b\DevAppsGML\dist\concord\PROJ\runtime\2.1.1\assemblies\Concord.Runtime.dll");
            AddAssemblyResolver("Concord.Logger, Version=2.0.0.29705, Culture=neutral, PublicKeyToken=354cc34ba0b81117",
                @"\\san01b\DevAppsGML\dist\concord\PROJ\logger\2.0.0\assemblies\Concord.Logger.2.0.dll");
            AddAssemblyResolver("Concord.Application, Version=2.3.0.52938, Culture=neutral, PublicKeyToken=354cc34ba0b81117",
                @"\\san01b\DevAppsGML\dist\concord\PROJ\application\2.3.0\assemblies\Concord.Application.2.3.dll");
            AddAssemblyResolver("Concord.TopicMediator, Version=2.1.2.29263, Culture=neutral, PublicKeyToken=354cc34ba0b81117",
                @"\\san01b\DevAppsGML\dist\concord\PROJ\topicmediator\2.1.2\assemblies\Concord.TopicMediator.2.1.dll");
            AddAssemblyResolver("log4net, Version=1.2.10.0, Culture=neutral, PublicKeyToken=1b44e1d426115821",
                @"\\san01b\DevAppsGML\dist\msdotnet\PROJ\log4net\1.2.11\assemblies\log4net.dll");
            AddAssemblyResolver("Google.ProtocolBuffers, Version=2.4.1.473, Culture=neutral, PublicKeyToken=55f7125234beb589",
                @"\\san01b\DevAppsGML\dist\msdotnet\PROJ\protobuf-csharp-port\2.4.1\assemblies\Google.ProtocolBuffers.dll");
            AddAssemblyResolver("MSDotNet.Runtime, Version=1.0.0.0, Culture=neutral, PublicKeyToken=d0e341ccaeb444f7",
                 @"\\san01b\DevAppsGML\dist\msdotnet\PROJ\runtime\1.0\assemblies\MSDotNet.Runtime.dll");
            AddAssemblyResolver("Microsoft.Practices.Prism, Version=4.0.0.0, Culture=neutral, PublicKeyToken=d0e341ccaeb444f7",
                @"\\san01b\DevAppsGML\dist\microsoft\PROJ\prism\4.0.0-ms1\assemblies\Microsoft.Practices.Prism.dll");
            AddAssemblyResolver("Microsoft.Practices.Prism.UnityExtensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=d0e341ccaeb444f7",
                @"\\san01b\DevAppsGML\dist\microsoft\PROJ\prism\4.0.0-ms1\assemblies\Microsoft.Practices.Prism.UnityExtensions.dll");

            AddInfragisticsResolver(null);
            AddInfragisticsResolver("Ribbon");
            AddInfragisticsResolver("Editors");
            AddInfragisticsResolver("DockManager");
            AddInfragisticsResolver("DataPresenter");
            AddInfragisticsResolver("Controls.Menus.XamMenu");
            AddInfragisticsResolver("Controls.Editors.XamColorPicker");
            AddInfragisticsResolver("Persistence");
            AddInfragisticsResolver("DragDrop");
            AddInfragisticsResolver("OutlookBar");

            AddDnpartsResolver2("AppMW.E3DotNet");
            AddDnpartsResolver2("MSDesktop.IPC");
            AddDnpartsResolver2("MSDesktop.Entitlement");
            AddDnpartsResolver2("MSDesktop.ServiceDiscovery");
            AddDnpartsResolver("MSNet");
            AddDnpartsResolver("MSNet.Native");
            AddDnpartsResolver("MSLog");

            AddDnpartsResolver("MSTools");
            AddDnpartsResolver("MSXml");
            AddDnpartsResolver("Channels");
            AddDnpartsResolver("Channels.Cps");
            AddDnpartsResolver("ProtocolBuffers");
            AddDnpartsResolver("CafDispatch");
            AddAssemblyResolver("MSDesktop.Isolation.Interfaces", @"%Root%\..\install\common\bin\MSDesktop.Isolation\MSDesktop.Isolation.Interfaces.dll");

        }

        private const string ConfigurationFileFormat =
@"<?xml version=""1.0""?>
<configuration>  
  <configSections>
  </configSections>

  <runtime>

{0}

    <loadFromRemoteSources enabled=""true""/>
    <NetFx40_LegacySecurityPolicy enabled=""true""/>
  </runtime>
</configuration>
";

        private const string BindingRedirectFormat =
            @"
    <assemblyBinding xmlns=""urn:schemas-microsoft-com:asm.v1"">
      <qualifyAssembly partialName=""{0}"" fullName=""{1}""/>
      <dependentAssembly>
        <assemblyIdentity name=""{0}"" culture=""{2}"" publicKeyToken=""{3}""/>
        <codeBase version=""{4}"" href=""file://{5}""/>
        <bindingRedirect oldVersion=""0.0.0.0-65535.65535.65535.65535"" newVersion=""{4}""/>
      </dependentAssembly>
    </assemblyBinding>";
        public static void GenerateConfigurationFile(string configFileName_)
        {
            StringBuilder bindingRedirects = new StringBuilder();
            foreach (var assemblyDescriptor in assemblyDescriptors)
            {
                string publicKey =string.Empty;
                var bytes = assemblyDescriptor.Key.GetPublicKeyToken();
                if (bytes != null && bytes.Length > 0)
                {
                    publicKey = bytes.Aggregate(publicKey, (current_, b_) =>
    current_ + string.Format("{0:x}", b_));
                }
                else
                {
                    publicKey = "null";
                }
                string culture = assemblyDescriptor.Key.CultureInfo.ToString();
                if (string.IsNullOrEmpty(culture))
                {
                    culture = "neutral";
                }
                bindingRedirects.AppendFormat(BindingRedirectFormat,
                                              assemblyDescriptor.Key.Name, assemblyDescriptor.Key.FullName,
                                              culture, publicKey,
                                              assemblyDescriptor.Key.Version, assemblyDescriptor.Value.Replace("\\", "/"));
            }
            File.WriteAllText(configFileName_, string.Format(ConfigurationFileFormat, bindingRedirects));
        }


#if DEBUG
        private const string PostSharpCommandConst = "/X:default /NoLogo /D:PS0131 /p:Configuration=Debug /p:Platform=AnyCPU";

#else 
        private const string PostSharpCommandConst = "/X:default /NoLogo /D:PS0131 /p:Configuration=Release /p:Platform=AnyCPU";
#endif
        public static void GenerateExecuteBatch(string batchFileName_)
        { 
            string copyCommand = @"call xcopy /R /Y ""{0}"" .
";
            string deleteCommand = @"DEL /Q {0}
";

#if DEBUG

#else 

#endif
            string buildCommand = @"postsharp.4.0-x86-cil.exe {0}.dll " + PostSharpCommandConst + @" {1} /p:AspectProviders=MSDesktop.AspectProvider.CollectMethodUsageAspectProvider,MSDesktop.AspectProvider /p:Output=PostSharpTemp\{0}.dll 
call xcopy /R /Y PostSharpTemp\{0}.dll {0}.dll
call xcopy /R /Y PostSharpTemp\{0}.pdb {0}.pdb
DEL /Q PostSharpTemp\{0}.dll
DEL /Q PostSharpTemp\{0}.pdb
";
            StringBuilder copyCommands = new StringBuilder(); 
            StringBuilder deleteCommands = new StringBuilder();
            foreach (var assemblyDescriptor in assemblyDescriptors)
            {
                copyCommands.AppendFormat(copyCommand, assemblyDescriptor.Value);
                deleteCommands.AppendFormat(deleteCommand, Path.GetFileName(assemblyDescriptor.Value));
            }
            copyCommands.AppendFormat(copyCommand, @"%POSTSHARP20%Release\postsharp.4.0-x86-cil.exe");
            deleteCommands.AppendFormat(deleteCommand, "postsharp.4.0-x86-cil.exe");
            //copyCommands.AppendFormat(copyCommand, @"%POSTSHARP20%Release\PostSharp.dll"); 
            copyCommands.AppendFormat(copyCommand, @"%POSTSHARP20%Release\PostSharp.Hosting.dll");
            deleteCommands.AppendFormat(deleteCommand, "PostSharp.Hosting.dll");
            copyCommands.AppendFormat(copyCommand, @"%POSTSHARP20%Release\PostSharp.Sdk.dll");
            deleteCommands.AppendFormat(deleteCommand, "PostSharp.Sdk.dll");
            copyCommands.AppendFormat(copyCommand, @"%POSTSHARP20%Release\PostSharp.Pipe.dll");
            deleteCommands.AppendFormat(deleteCommand, "PostSharp.Pipe.dll");
            copyCommands.AppendFormat(copyCommand, @"%POSTSHARP20%Release\PostSharp.Sdk.XmlSerializers.dll");
            deleteCommands.AppendFormat(deleteCommand, "PostSharp.Sdk.XmlSerializers.dll");
            copyCommands.AppendFormat(copyCommand, @"%POSTSHARP20%Release\PostSharp.Settings.dll"); 
            deleteCommands.AppendFormat(deleteCommand, "PostSharp.Settings.dll"); 
            deleteCommands.AppendFormat(deleteCommand, "MSDesktop.AspectProvider.dll.config");
            deleteCommands.AppendFormat(deleteCommand, "postsharp.4.0-x86-cil.exe.config");
            deleteCommands.AppendFormat(deleteCommand, "AspectConfiguration.xml"); 
            
            StringBuilder buildCommands = new StringBuilder();
            buildCommands.AppendLine("mkdir PostSharpTemp");
            foreach (var assembly in CollectMethodUsageAspectProvider.Assemblies)
            {
                string assemblyName = assembly.Key; 
                string signAssemblyParameter = string.Empty;
                switch (assembly.Value)
                { 
                    case StrongKeyType.MSDotNet:
                        signAssemblyParameter = @"/p:SignAssembly=true /p:PrivateKeyLocation=\\ms\dev\msdotnet\msdotnet\incr\src\Assemblies\MSDotNet\StrongName.snk";
                        break;
                    case StrongKeyType.Concord:
                        signAssemblyParameter = @"/p:SignAssembly=true /p:PrivateKeyLocation=\\san01b\DevAppsGML\dist\ied\PROJ\concordkey\prod\Concord_StrongNameKey.snk";
                        break;
                    case StrongKeyType.Ecdev:
                        signAssemblyParameter = @"/p:SignAssembly=true /p:PrivateKeyLocation=\\ms\dev\ecdev\publickey\2.0\ecdev.snk";
                        break; 
                }
                buildCommands.AppendFormat(buildCommand, assemblyName, signAssemblyParameter);
            }
            buildCommands.AppendLine("rd PostSharpTemp");
            string content = File.ReadAllText("postsharp.4.0-x86-cil.exe.config");
            content = string.Format(content, typeof(CompileTimeAssemblyResolver).Assembly.GetName().Version);
            File.WriteAllText("postsharp.4.0-x86-cil.exe.config", content);

            string batchContent =
                copyCommands.ToString() +
                buildCommands.ToString() +
                deleteCommands.ToString();

            File.WriteAllText(batchFileName_, batchContent);
        }

        public static void EnableResolver()
        {
            AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(CurrentDomain_AssemblyResolve);
        }

        static Assembly CurrentDomain_AssemblyResolve(object sender_, ResolveEventArgs args_)
        {
            if (args_.Name.Contains(".resources,"))
                return null;
            var name = new AssemblyName(args_.Name);
            var afsPath = assemblyDescriptors.FirstOrDefault(a_ => a_.Key.FullName == name.FullName); 
            if (string.IsNullOrEmpty(afsPath.Value))
            {
                // will ignore version
                afsPath = assemblyDescriptors.FirstOrDefault(a_ => a_.Key.Name == name.Name); 
            }
            if (string.IsNullOrEmpty(afsPath.Value))
            {
                return null;
            }
            try
            {
                return Assembly.LoadFrom(afsPath.Value);
            }
            catch (InvalidOperationException)
            {
                //Logger.CriticalWithFormat("No AFS path registered for the assembly {0}", args_.Name);
                return null;
            }
        }

        private static void AddInfragisticsResolver(string name)
        {
            AddAssemblyResolver( String.Format(
                            "InfragisticsWPF4{2}.{0}, Version={1}, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb",
                            IgVersionShort, IgVersionFull, string.IsNullOrEmpty(name) ? "" : "." + name),
                            String.Format(
                        @"\\san01b\DevAppsGML\dist\msdotnet\PROJ\infragisticswpf\{0}\assemblies\InfragisticsWPF4{2}.{1}.dll", IgVersion,
                        IgVersionShort, string.IsNullOrEmpty(name) ? "" : "." + name)); 
        }

        private static void AddDnpartsResolver(string name)
        {
            AddDnpartsResolver2("MSDotNet." + name);
        }

        private static void AddDnpartsResolver2(string fullName)
        {
            AddAssemblyResolver(String.Format(
                "{1}, Version={0}, Culture=neutral, PublicKeyToken=d0e341ccaeb444f7",
                DnPartsVersion, fullName), String.Format(@"\\san01b\DevAppsGML\dist\msdotnet\PROJ\dnparts\{0}\assemblies\{1}.dll",
                                                          DnPartsVersionFull, fullName)); 
        }

 
    }

}
