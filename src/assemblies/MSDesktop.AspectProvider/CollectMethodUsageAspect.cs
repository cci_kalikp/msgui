﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using PostSharp.Aspects;
using PostSharp.Aspects.Internals;

namespace MSDesktop.AspectProvider
{
 
    [Serializable]
    public class CollectMethodUsageAspect : IMethodInterceptionAspect
    {
        private readonly MethodBase declaredMethod;
        public CollectMethodUsageAspect(MethodBase declaredMethod_)
        {
            declaredMethod = declaredMethod_;
        }

        public CollectMethodUsageAspect()
        {
            
        }
        public void RuntimeInitialize(MethodBase method)
        {
        }


        [MethodInterceptionAdviceOptimization(MethodInterceptionAdviceOptimizations.None)]
        public void OnInvoke(MethodInterceptionArgs args)
        { 
             Utilities.LogUsage(args, declaredMethod);
             args.Proceed();
        } 
       
    }

}
