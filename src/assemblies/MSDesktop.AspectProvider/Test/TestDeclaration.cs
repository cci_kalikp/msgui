﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MSDesktop.AspectProvider; 

namespace MSDotNet.MSGui.TestUsageDeclaration
{
    //[LogUsage]
    public interface IUsageDeclaredInInterface
    {
        void MethodToTrackImpl();

        bool PropertyToTrackImpl { get; set; }
    }

    public interface IUsageDelcaredInInterfaceMethod
    {
        [LogUsage]
        void MethodToTrackExpl();

        [LogUsage]
        bool PropertyToTrackExpl { get; set; }

        bool PropertyToNotTrack { get; set; }
    }
        
    //[LogUsage]
    public abstract class UsageDeclaredInBaseClass
    {
        public void MethodToTrackImpl()
        {
            
        }

        public virtual void VirtualMethodToTrackImpl(string argument1, bool argument2, int argument3)
        {
            
        }

        public abstract void AbstractMethodToTrackImpl();

        protected virtual void ProtectedVirtualMethod() {}
    }

    public abstract class UsageDeclaredInBaseClassMethod
    {
        public void MethodNotToTrack()
        {

        }

        [LogUsage]
        public virtual void VirtualMethodToTrackExpl()
        {

        }

        [LogUsage]
        public abstract void AbstractMethodToTrackExpl();

        protected virtual void ProtectedVirtualMethodNotToTrack() { }

        [LogUsage]
        public void MethodToTrackExpl() { }
    }
}
