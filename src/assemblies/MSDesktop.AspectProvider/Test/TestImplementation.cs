﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MSDesktop.AspectProvider;
using MSDotNet.MSGui.TestUsageDeclaration; 

namespace MSDotNet.MSGui.TestUsageImplementation
{
    public class TestUsageImplementationForInterface:IUsageDeclaredInInterface
    {

        public void MethodToTrackImpl()
        {
            
        }

        public bool PropertyToTrackImpl { get; set; }
    }
    
    public class TestUsageImplementationForInterface2:IUsageDelcaredInInterfaceMethod
    {

        public void MethodToTrackExpl()
        {
             
        }

        public bool PropertyToTrackExpl { get; set; }

        public bool PropertyToNotTrack { get; set; }
    }

    public class TestUsageImplementationForClass:UsageDeclaredInBaseClass
    {
        public override void AbstractMethodToTrackImpl()
        {
            
        } 
        public override void VirtualMethodToTrackImpl(string argument1, bool argument2, int argument3)
        {

        }

        protected override void ProtectedVirtualMethod()
        {
             
        }
    }


    public class TestUsageImplementationForClassMethod:UsageDeclaredInBaseClassMethod
    {
        public override void VirtualMethodToTrackExpl()
        {
            
        }

        public override void AbstractMethodToTrackExpl()
        {
            
        }

        protected override void ProtectedVirtualMethodNotToTrack()
        {
             
        }

        [LogUsage]
        public static void StaticMethodToTrack(int count)
        {}

        [LogUsage]
        public void MethodToTrackExplInChild()
        {
            
        }
    }

     
}
