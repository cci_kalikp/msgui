﻿using System;
using System.Threading;
using MSDotNet.MSGui.TestUsageDeclaration;
using MSDotNet.MSGui.TestUsageImplementation;

namespace TestUsage
{
    class Program
    {
        public static void Main(string[] args_)
        {
            for (int i = 0; i < 5; i++)
            {
                IUsageDeclaredInInterface t1 = new TestUsageImplementationForInterface();
                t1.MethodToTrackImpl();
                t1.PropertyToTrackImpl = !t1.PropertyToTrackImpl;

                IUsageDelcaredInInterfaceMethod t2 = new TestUsageImplementationForInterface2();
                t2.PropertyToNotTrack = !t2.PropertyToNotTrack;
                t2.PropertyToTrackExpl = !t2.PropertyToTrackExpl;
                t2.MethodToTrackExpl();

                UsageDeclaredInBaseClass t3 = new TestUsageImplementationForClass();
                t3.MethodToTrackImpl();
                t3.VirtualMethodToTrackImpl("test" + i, i % 2 == 0, i);
                t3.AbstractMethodToTrackImpl();

                UsageDeclaredInBaseClassMethod t4 = new TestUsageImplementationForClassMethod();
                t4.MethodNotToTrack();
                t4.VirtualMethodToTrackExpl();
                t4.AbstractMethodToTrackExpl();
                t4.MethodToTrackExpl();

                var t5 = t4 as TestUsageImplementationForClassMethod;
                t5.MethodToTrackExplInChild();
                TestUsageImplementationForClassMethod.StaticMethodToTrack(i);
                Thread.Sleep(5000);
            }  
            
        }
    }
}
