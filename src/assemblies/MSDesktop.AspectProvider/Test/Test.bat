﻿rem copy this file to C:\Users\caijin\shadow\ada\ada2\msdotnet\msgui\trunk_buildtest\src

xcopy /S /C /H /R /Y C:\Users\caijin\shadow\ada\ada2\msdotnet\msgui\trunk\assemblies\MSDesktop.AspectProvider\*.* %cd%\assemblies\MSDesktop.AspectProvider\
call "%VS100COMNTOOLS%vsvars32.bat"
DEL /S /Q %cd%\assemblies\MSDesktop.AspectProvider\bin\*.*
DEL /S /Q %cd%\assemblies\MSDesktop.AspectProvider\obj\*.* 
msbuild %cd%\assemblies\MSDesktop.AspectProvider\MSDesktop.AspectProvider.csproj /property:Configuration=Test
 
xcopy /S /C /H /R /Y %cd%\assemblies\MSDesktop.AspectProvider\bin\Debug\*.* %cd%\..\install\common\assemblies\.debug\ 

set AddAspectBatchGeneratorCode=%cd%\assemblies\MSDesktop.AspectProvider\AddAspectBatchGenerator.cs 
set TestUsageDeclarationCode=%cd%\assemblies\MSDesktop.AspectProvider\Test\TestDeclaration.cs
set TestUsageImplementationCode=%cd%\assemblies\MSDesktop.AspectProvider\Test\TestImplementation.cs 
set TestUsageCode=%cd%\assemblies\MSDesktop.AspectProvider\Test\TestUsage.cs 
set TestAspectConfiguration=%cd%\assemblies\MSDesktop.AspectProvider\Test\AspectConfiguration.xml 
set MSDOTNET_USAGE_CONFIG=%cd%\assemblies\MSDesktop.AspectProvider\Test\usage.config
set TestUsageAppConfig=%cd%\assemblies\MSDesktop.AspectProvider\Test\app.config
set Root=%cd%

pushd "%cd%\..\install\common\assemblies\.debug\" 
xcopy /R /Y "%POSTSHARP20%Release\PostSharp.dll" .
xcopy /R /Y %TestAspectConfiguration% .
csc /define:TRACE /reference:MSDesktop.AspectProvider.dll /out:AddAspectBatchGenerator.exe %AddAspectBatchGeneratorCode% /debug+ /debug:full /optimize- 
csc /define:TRACE /out:MSDotNet.MSGui.TestUsageDeclaration.dll /target:library /reference:MSDesktop.AspectProvider.dll %TestUsageDeclarationCode% /debug+ /debug:full /optimize- 
csc /define:TRACE /out:MSDotNet.MSGui.TestUsageImplementation.dll /target:library /reference:MSDesktop.AspectProvider.dll /reference:MSDotNet.MSGui.TestUsageDeclaration.dll /out:MSDotNet.MSGui.TestUsageImplementation.dll %TestUsageImplementationCode% /debug+ /debug:full /optimize- 
csc /define:TRACE /reference:MSDotNet.MSGui.TestUsageDeclaration.dll /reference:MSDotNet.MSGui.TestUsageImplementation.dll /out:TestUsage.exe %TestUsageCode% /debug+ /debug:full /optimize- 
copy %TestUsageAppConfig% TestUsage.exe.config

AddAspectBatchGenerator.exe AddAspectCore.bat
call AddAspectCore.bat
DEL /S /Q AddAspectCore.bat 
DEL /S /Q AddAspectBatchGenerator.* 
xcopy \\san01b\DevAppsGML\dist\msdotnet\PROJ\dnparts\2014.05.23.1\assemblies\MSDotNet.MSLog.dll . 
xcopy \\san01b\DevAppsGML\dist\msdotnet\PROJ\dnparts\2014.05.23.1\assemblies\MSDotNet.MSTools.dll .
TestUsage.exe
DEL /S /Q PostSharp.dll 
DEL /S /Q MSDotNet.MSGui.TestUsageDeclaration.dll 
DEL /S /Q MSDotNet.MSGui.TestUsageImplementation.dll
DEL /S /Q TestUsage.exe
DEL /S /Q MSDotNet.MSLog.dll 
DEL /S /Q MSDotNet.MSTools.dll
popd
