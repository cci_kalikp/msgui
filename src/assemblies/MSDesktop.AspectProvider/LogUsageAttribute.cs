﻿using System;

namespace MSDesktop.AspectProvider
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class | AttributeTargets.Property | AttributeTargets.Event | AttributeTargets.Interface)]
    public class LogUsageAttribute:Attribute
    {
         
    }
}
