﻿using System;
using System.Runtime.Serialization;

namespace MSDesktop.Isolation.Enums
{
    [Flags, Serializable, DataContract(Name = "InitialLocationProxy", Namespace = "net-pipe://MSDesktop.Isolation/")]
    public enum InitialLocation
    {
        /// <summary>
        /// The new view will be created floating, and can be docked by the user. 
        /// </summary>
        [EnumMember]
        Floating = 1,

        /// <summary>
        /// The new view will be created floating, and can NOT be docked by the user. 
        /// </summary>
        [EnumMember]
        FloatingOnly = 2,

        /// <summary>
        /// The new view will be created docked to a new empty tab in the main docking area.
        /// </summary>
        [EnumMember]
        DockInNewTab = 4,

        /// <summary>
        /// The new view will be created floating centered under the mouse pointer. Can only be 
        /// used in conjunction with InitialLocation.Floating or InitialLocation.FloatingOnly.
        /// </summary>
        [EnumMember]
        PlaceAtCursor = 8,

        /// <summary>
        /// Custom placement logic via the InitialLocationCallback, look at <see cref="InitialLocationHelper"/> for 
        /// common scenarios. Can only be used in conjunction with InitialLocation.Floating or 
        /// InitialLocation.FloatingOnly.
        /// </summary>
        [EnumMember]
        Custom = 16,

        /// <summary>
        /// The new view will be created docked in a tab group with DockTarget in <see cref="InitialWindowParameters"/>.
        /// 
        /// If InitialLocationTarget is null and DocumentContentHost mode is enabled, pane will be docked
        /// inside the DocumentContentHost.
        /// </summary>
        [EnumMember]
        DockTabbed = 32,

        /// <summary>
        /// The new view will be created docked to the left of DockTarget in <see cref="InitialWindowParameters"/>.
        /// </summary>
        [EnumMember]
        DockLeft = 64,

        /// <summary>
        /// The new view will be created docked to the top of DockTarget in <see cref="InitialWindowParameters"/>.
        /// </summary>
        [EnumMember]
        DockTop = 128,

        /// <summary>
        /// The new view will be created docked to the right of DockTarget in <see cref="InitialWindowParameters"/>.
        /// </summary>
        [EnumMember]
        DockRight = 256,

        /// <summary>
        /// The new view will be created docked to the bottom of DockTarget in <see cref="InitialWindowParameters"/>.
        /// </summary>
        [EnumMember]
        DockBottom = 512,

        /// <summary>
        /// Thew new view will be docked in the currently active tab. Has to be used in conjuction with
        /// DockTabbed, DockLeft, DockTop, DockRight or DockBottom.
        /// 
        /// The option should be used with InitialLocationTarget == null.
        /// </summary>
        [EnumMember]
        DockInActiveTab = 1024
    };
}
