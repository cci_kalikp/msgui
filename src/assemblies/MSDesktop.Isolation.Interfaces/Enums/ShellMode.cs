﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace MSDesktop.Isolation.Enums
{
    [DataContract(Name = "ShellMode", Namespace = "net-pipe://MSDesktop.Isolation/")]
    public enum ShellMode
    { 
        [EnumMember]
        RibbonWindow = 0,
        [EnumMember]
        LauncherBarAndFloatingWindows = 1,
        [EnumMember]
        LauncherBarAndWindow = 2,
        [EnumMember]
        External = 3,
        [EnumMember]
        RibbonMDI = 4,
        [EnumMember]
        RibbonAndFloatingWindows = 5,  
    }
}
