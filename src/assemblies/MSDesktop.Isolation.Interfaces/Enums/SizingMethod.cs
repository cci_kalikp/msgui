﻿using System.Runtime.Serialization;

namespace MSDesktop.Isolation.Enums
{
    /// <summary>
    /// Used to specify the sizing behaviour of panes.
    /// </summary>
    [DataContract(Name = "SizingMethod", Namespace = "net-pipe://MSDesktop.Isolation/")]
    public enum SizingMethod
    {
        /// <summary>
        /// Pane will adjust its size to the contained objects
        /// </summary>
        [EnumMember]
        SizeToContent,

        /// <summary>
        /// Pane size can be set programmatically
        /// </summary>
        [EnumMember]
        Custom
    }
}
