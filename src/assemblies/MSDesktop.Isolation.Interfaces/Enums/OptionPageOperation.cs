﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSDesktop.Isolation.Enums
{
    internal enum OptionPageOperation
    {
        OK = 1,
        Cancel = 2,
        Display = 3,
        Help = 4,
    }
}
