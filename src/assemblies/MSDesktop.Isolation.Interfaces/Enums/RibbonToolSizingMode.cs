﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSDesktop.Isolation.Enums
{
    internal enum RibbonToolSizingMode
    {
        ImageAndTextLarge = 1,
        ImageAndTextNormal = 2,
        ImageOnly = 3
    }
}
