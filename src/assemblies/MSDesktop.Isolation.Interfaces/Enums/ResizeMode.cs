﻿using System.Runtime.Serialization;

namespace MSDesktop.Isolation.Enums
{
    [DataContract(Name = "ResizeMode", Namespace = "net-pipe://MSDesktop.Isolation/")]
    public enum ResizeMode
    {
        [EnumMember] NoResize,
        [EnumMember] CanMinimize,
        [EnumMember] CanResize,
        [EnumMember] CanResizeWithGrip
    }
}

