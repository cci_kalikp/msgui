﻿using System.Runtime.Serialization;

namespace MSDesktop.Isolation.Enums
{
    [DataContract(Name = "ModuleStatus", Namespace = "net-pipe://MSDesktop.Isolation/")]
    public enum ModuleStatus
    {
        [EnumMember] NotLoaded,
        [EnumMember] Loaded,
        [EnumMember] BlockedByNoEntitlementsInfo,
        [EnumMember] BlockedByEntitlements,
        [EnumMember] DependentModuleBlocked,
        [EnumMember] TypeResolutionFailed,
        [EnumMember] Exceptioned,
        [EnumMember] ProcessExited
    }
}
