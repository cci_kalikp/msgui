﻿
namespace MSDesktop.Isolation.Enums
{
    internal enum WindowMessageOperation
    {
        ControlBackgroundPaint = 1,
        SetButtonSizingMode = 2,
        WindowClosing = 3,
        WindowClosed = 4,
    }
}
