﻿using System;

namespace MSDesktop.Isolation.Interfaces
{
    public interface IXProcAddInHost
    {
        IXProcUiAddIn AddIn { get; }
        void RegisterMessageHandler(Type messageType, Action<object> messageHandler);
    }
}
