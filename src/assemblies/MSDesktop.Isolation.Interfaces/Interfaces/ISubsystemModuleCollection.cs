﻿using System;
using System.Collections.Generic;
using MSDesktop.Isolation.Proxies;

namespace MSDesktop.Isolation.Interfaces
{
    /// <summary>
    /// Represents a module collection that will be run in an isolated process.
    /// </summary>
    public interface ISubsystemModuleCollection
    {
        /// <summary>
        /// Retrieves the collection of types of modules to instantiate in an isolated subsystem.
        /// </summary>
        /// <returns>Collection of types of the modules implementing IModule interfaces to be used with isolated subsystem.</returns>
        IEnumerable<Type> GetModules();
    }

    public interface ISubsystemModuleCollectionEx
    {
        IEnumerable<Type> GetModules(IsolatedSubsystemSettings settings, string subsystemExecuterDirectory);
    }

    public interface IModuleWithExtraInformation
    {
        void SetExtraInformation(int moduleIndex, IsolatedSubsystemSettings settings);
    }
}
