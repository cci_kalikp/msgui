﻿using System.ServiceModel;
using System.Windows.Input;
using System.Windows.Interop;
using MSDesktop.Isolation.Proxies;

namespace MSDesktop.Isolation.Interfaces
{
    /// <summary>
    /// Site for subsystem isolation. Not intended to be used by your code.
    /// </summary>
    [ServiceContract(Name = "XProcWindowSite")]
    public interface IXProcWindowSite
    {
        [OperationContract]
        bool TabOut(TraversalRequest request);

        [OperationContract]
        bool TranslateAccelerator(MsgProxy msg);

        [OperationContract]
        void Send(byte[] payload);

        [OperationContract]
        void SubscribeV2V(string viewId, string typeName);

        [OperationContract]
        void RegisterAsPublisher(string viewId, string typeName);
    }
}
