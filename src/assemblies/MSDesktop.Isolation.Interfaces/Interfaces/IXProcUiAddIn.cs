﻿using System.ServiceModel;

namespace MSDesktop.Isolation.Interfaces
{
    [ServiceContract(Name = "XProcUiAddIn", Namespace = "net-pipe://XProcUiAddIn")]
    public interface IXProcUiAddIn
	{
        [OperationContract]
        void Load(string doc);

        [OperationContract]
        string Save();

        [OperationContract]
        void Receive(byte[] payload);

        [OperationContract]
        void InvokeClosed();

        [OperationContract]
        bool InvokeClosing();

        [OperationContract]
        void InvokeClosedQuietly();
	}
}
