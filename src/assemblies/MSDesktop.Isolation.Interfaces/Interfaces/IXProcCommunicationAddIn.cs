﻿using System.ServiceModel;

namespace MSDesktop.Isolation.Interfaces
{
    [ServiceContract(Name = "XProcCommunicationAddIn", Namespace = "net-pipe://MSDesktop.Isolation/")]
    public interface IXProcCommunicationAddIn
    {
        [OperationContract]
        void Receive(byte[] payloadBytes);
    }
}
