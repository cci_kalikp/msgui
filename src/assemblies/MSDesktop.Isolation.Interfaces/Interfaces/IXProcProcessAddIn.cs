﻿using System.ServiceModel;

namespace MSDesktop.Isolation.Interfaces
{
    [ServiceContract(Name = "IXProcProcessAddIn", Namespace = "net-pipe://MSDesktop.Isolation/")]
    public interface IXProcProcessAddIn
    {
        [OperationContract]
        void ShutDown();

        [OperationContract]
        void ProceedWithInitialization(long profileFileHandle, int fileLength, string profileName);

        [OperationContract]
        void ReloadProfile(string currentProfile, long layoutFileHandle, int layoutFileSize);

        [OperationContract]
        void CreateWindow(string factoryId, string viewId);
    }
}
