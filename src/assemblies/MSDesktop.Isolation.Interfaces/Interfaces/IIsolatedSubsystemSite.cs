﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using MSDesktop.Isolation.Enums;
using MSDesktop.Isolation.Proxies;

namespace MSDesktop.Isolation.Interfaces
{
    /// <summary>
    /// This class encapsulates communication from the isolated process to the host.
    /// Site for subsystem isolation. Not intended to be used or implemented by your code.
    /// </summary>
    [ServiceContract(Name = "IsolatedSubsystemSite", Namespace = "net-pipe://IsolatedSubsystemSettings")]
    public interface IMSDesktopSubsystemSite
    {
        [OperationContract]
        string GetPayloadType();

        [OperationContract]
        IsolatedSubsystemSettings GetIsolatedSubsystemSettings(int processId, string settingsUri, string processAddinUri);

        [OperationContract]
        string AddWidget(string addinUri, string widgetFactoryId, long hwnd, string parentFactoryId, string widgetXaml, string isolatedButtonSize);

        [OperationContract]
        string PlaceWidget(string addInUri, string widgetFactoryId, long hwnd, string root, string location, string widgetXaml, string isolatedButtonSize);

        [OperationContract]
        void AddRemoteShowWindowButton(string widgetFactoryId, string text, string windowFactoryId, string root, string location);

        [OperationContract]
        string AddWindow(string addInUri, long hwnd, InitialWindowParametersProxy initialParametersProxy, string windowTitle, string initialState);

        [OperationContract]
        void AddOptionsPage(string path, string title, string addInUri, long hwnd);

        [OperationContract]
        void SetModuleStatus(string moduleTypeName, ModuleStatus status, Exception exception = null);

        [OperationContract]
        void ViewLoadedCompletely(string connectionInfo);

        [OperationContract]
        void SubscribeM2M(string messageType, string communicationAddInUrl);

        [OperationContract]
        void PublishM2M(byte[] objectBytestream);

        [OperationContract]
        void SetApplicationStarted(int processId, IEnumerable<string> windowFactoryIds);
         
        [OperationContract]
        void SetLogFilePath(string logFilePath);

        bool IsControl
        {
            [OperationContract]
            get;
        }
    }
}
