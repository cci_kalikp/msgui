﻿using System;

namespace MSDesktop.Isolation.Proxies
{
    public sealed class MessageArrivedEventArgs : EventArgs
    {
        private readonly object _message;

        public MessageArrivedEventArgs(object message)
        {
            _message = message;
        }

        public object Message
        {
            get { return _message; }
        }
    }
}
