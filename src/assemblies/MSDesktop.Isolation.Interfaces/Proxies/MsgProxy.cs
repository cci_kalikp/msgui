﻿using System;
using System.Runtime.Serialization;
using System.Windows.Interop;

namespace MSDesktop.Isolation.Proxies
{
    [Serializable, DataContract(Name = "MsgProxy", Namespace = "net-pipe://MsgProxy")]
    public sealed class MsgProxy
    {
        public MsgProxy()
        {
        }

        public MsgProxy(MSG msg)
        {
            Hwnd = msg.hwnd.ToInt64();
            Message = msg.message;
            WParam = msg.wParam;
            LParam = msg.lParam;
            Time = msg.time;
            PtX = msg.pt_x;
            PtY = msg.pt_y;
        }

        [DataMember]
        public long Hwnd { get; set; }

        [DataMember]
        public int Message { get; set; }

        [DataMember]
        public IntPtr WParam { get; set; }

        [DataMember]
        public IntPtr LParam { get; set; }

        [DataMember]
        public int Time { get; set; }

        [DataMember]
        public int PtX { get; set; }

        [DataMember]
        public int PtY { get; set; }
    }
}
