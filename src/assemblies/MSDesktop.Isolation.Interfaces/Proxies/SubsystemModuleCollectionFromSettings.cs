﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;
using MSDesktop.Isolation.Interfaces;

namespace MSDesktop.Isolation.Proxies
{
    public class SubsystemModuleCollectionFromSettings: ISubsystemModuleCollectionEx
    { 
        public IEnumerable<Type> GetModules(IsolatedSubsystemSettings settings, string subsystemExecuterDirectory)
        {
            var isolatedSubsystemLocation = settings.IsolatedSubsystemLocation;
            var extraInformation = settings.ExtraInformation;

            if (!string.IsNullOrEmpty(extraInformation))
            {
                XDocument doc = XDocument.Parse(extraInformation);
                foreach (var moduleElement in doc.Descendants("Module"))
                {
                    var type = moduleElement.Attribute("Type");
                    if (type != null && !string.IsNullOrEmpty(type.Value))
                    {
                        var assemblyPath = moduleElement.Attribute("AssemblyLocation"); 
                        yield return Type.GetType(type.Value.Trim(), name =>
                            {
                                var filePathBase = string.Format(@"{0}\{1}", isolatedSubsystemLocation, name.Name); 
                                List<string> filesToTry = new List<string>();
                                if (assemblyPath != null && !string.IsNullOrEmpty(assemblyPath.Value))
                                {
                                    var filePathBase2 = string.Format(@"{0}\{1}", assemblyPath.Value, name.Name); 
                                    filesToTry.AddRange(new[] { filePathBase2 + ".exe", filePathBase2 + ".dll" });
                                }
                                else
                                { 
                                    filesToTry.AddRange(new[] { filePathBase + ".exe", filePathBase + ".dll" });
                                    filePathBase = string.Format(@"{0}\{1}", subsystemExecuterDirectory, name.Name);
                                    filesToTry.AddRange(new[] { filePathBase + ".exe", filePathBase + ".dll" }); 
                                }

                               return filesToTry.Where(File.Exists) .Select(Assembly.LoadFrom) .FirstOrDefault(); 
                            }, null, true);
                    }
                } 
            } 
        }

         
    }
}
