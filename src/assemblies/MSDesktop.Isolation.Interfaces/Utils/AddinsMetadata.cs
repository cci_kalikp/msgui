﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace MSDesktop.Isolation.Utils
{
    internal static class AddinsMetadata
    {
        public static IEnumerable<Type> GetAddinTypes(ICustomAttributeProvider provider)
        {
            yield return Type.GetType("MSDesktop.Isolation.UiAddIns.IsolatedViewUiAddIn, MSDesktop.Isolation.HostProcess");
            yield return Type.GetType("MSDesktop.Isolation.UiAddIns.UiAddinBase, MSDesktop.Isolation.HostProcess");
            yield return Type.GetType("MSDesktop.Isolation.UiAddIns.ViewUiAddinBase, MSDesktop.Isolation.HostProcess");
            yield return Type.GetType("MSDesktop.Isolation.UiAddIns.WindowUiAddIn, MSDesktop.Isolation.HostProcess");
            yield return Type.GetType("MSDesktop.Isolation.UiAddIns.ControlUiAddIn, MSDesktop.Isolation.HostProcess");
        }
    }
}
