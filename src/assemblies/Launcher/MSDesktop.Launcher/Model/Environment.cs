#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2006 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

              $Id: //eai/msdotnet/msgui/trunk/assemblies/Launcher/MSDesktop.Launcher/Model/Environment.cs#5 $
  $DateTime: 2014/07/18 02:48:55 $
    $Change: 888859 $
    $Author: caijin $
_____________________________________________________________________________*/
#endregion File Info Header

using System;

namespace MSDesktop.Launcher.Model
{
  using System.Xml.Serialization;

  /// <summary>
  /// Represents a unique environment (eg. Production, BCP, ProductionBeta etc)
  /// </summary>
  [System.Xml.Serialization.XmlRoot("Environment")]
  public class Environment : IComparable
  {
    #region Constructors

      /// <summary>
      /// Default constructor
      /// NOTE: This is required by XMLSerializer
      /// </summary>
      public Environment()
      {
          AutoStart = new AutoStart();
          Impersonation = new Impersonation();
          Favorite = new Favorite();
      }

      /// <summary>
    /// The standard constructor for general usage
    /// </summary>
    /// <param name="name_">The name of the environment</param>
    /// <param name="version_">The release version number</param>
    /// <param name="sysLoc_">The location in which the application is used</param>
    /// <param name="arguments_">The environment specific command line arguments</param>
    /// <param name="allowedTimeGroup_">The time group which specifies the times this environment is available</param>
    /// <param name="autoStart_">Will start the application at the time specified in allowed time group, or now if start time already passed.</param>
    /// <param name="mailgroup_">Mail group for permission</param>    
    public Environment(string name_, string version_, string sysLoc_, string arguments_, string allowedTimeGroup_, AutoStart autoStart_, string mailgroup_)
    {
      this.Name = name_;
      this.Version = version_;
      this.SysLoc = sysLoc_;
      this.Arguments = arguments_;
      this.AllowedTimeGroup = allowedTimeGroup_;
      this.AutoStart = autoStart_;
      MailgroupString = mailgroup_;
    }

    #endregion

    #region Public Properties

    /// <summary>
    /// The name of the environment
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// The release that is active in this environment
    /// </summary>
    public string Version { get; set; }

    /// <summary>
    /// Alternative version to run if you are in the associated mailgroup
    /// </summary>
    public AlternateVersion AlternateVersion { get; set; }

    /// <summary>
    /// The location in which this environment is used
    /// </summary>
    public string SysLoc { get; set; }

    /// <summary>
    /// The command line arguments for the executable
    /// </summary>
    public string Arguments { get; set; }

    /// <summary>
    /// The name of the time group which specifies the availability of the environment
    /// </summary>
    public string AllowedTimeGroup { get; set; }

    /// <summary>
    /// Flag indicating whether app should auto start according to Allowed Time Group
    /// </summary>
    public AutoStart AutoStart { get; set; }

    /// <summary>
    /// Flag indicating whether impersonation should be turned on.
    /// </summary>
    public Impersonation Impersonation { get; set; }
       
    /// <summary>
    /// Flag indicating whether favorite is set.
    /// </summary>
    public Favorite Favorite { get; set; }

    public bool IsHidden { get; set; }

    /// <summary>
    /// the UniqueKeys - Name <see cref="MSDesktop.Launcher.MOdel.Environment.Name"/>of the dependent Environment
    /// </summary>
    [XmlElement("DependsOn")]    
    public string[] DependsOn { get; set; } 


    [XmlElement("Mailgroup")]
    public string MailgroupString
    {
      get; set;
    }
    
    /// <summary>
    /// One or more mailgroups, ; delimited, that the user must be part of to see this group.
    /// </summary>
    public string[] Mailgroups
    {
      get
      {
        string[] returnValue = null;
        if (!String.IsNullOrEmpty(MailgroupString))
        {
          returnValue = MailgroupString.Split(';');
        }

        return returnValue;
      }
    }

    public string IconFile { get; set; }
    #endregion

    #region Public Methods

    /// <summary>
    /// Gets a XML string representing the object
    /// </summary>
    /// <returns></returns>
    public string ToXMLString()
    {
      return Utils.GetXMLString(this);
    }

    #endregion

    #region IComparable Members

    /// <summary>
    /// Compares two environment objects using their names
    /// </summary>
    /// <param name="obj">The object to compare with - must be of type Environment.</param>
    /// <returns></returns>
    public int CompareTo(object obj)
    {
      Environment env = obj as Environment;
      if (env != null)
      {
        return this.Name.CompareTo(env.Name);
      }
      else
      {
        throw new ArgumentException("Environment object required for a comparison");
      }
    }

    #endregion
  }
}
