#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2006 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

              $Id: //eai/msdotnet/msgui/trunk/assemblies/Launcher/MSDesktop.Launcher/Model/SysLoc.cs#1 $
  $DateTime: 2014/01/10 04:31:48 $
    $Change: 861516 $
    $Author: caijin $
_____________________________________________________________________________*/
#endregion File Info Header

using System;

namespace MSDesktop.Launcher.Model
{
  /// <summary>
  /// Provides abstraction of SYS_LOC functionality.
  /// </summary>
  public class SysLoc
  {
    #region Constants
    public static readonly string ANY="*";
    private const string SYS_LOC = "SYS_LOC";
    #endregion

    #region PrivateState
    string[] _sysLocParts;
    #endregion

    #region Ctor()
    public SysLoc(string sysLoc_)
    {
      string[] sysLocParts=sysLoc_.Split('.');

      if(sysLocParts.Length<3)
        throw new ArgumentException(string.Format("Invalid format for SYS_LOC variable, must confirm to [otherparts.]{building}.{city}.{region}, received {0}", sysLoc_), "sysLoc_");
     
      _sysLocParts=new String[3];
      Array.Copy(sysLocParts, sysLocParts.Length-3, _sysLocParts, 0, 3);
    }
    #endregion

    #region Overrides
    public override bool Equals(object valueToCompare)
    {
      string[] paramParts;

      //If we passed in SysLoc object, get its param values
      //otherwise, treat all passed in objects as string
      SysLoc syslocParam=valueToCompare as SysLoc;
      if(syslocParam!=null)
      {
        paramParts=(string[]) syslocParam._sysLocParts.Clone();
      }
      else
      {
        paramParts=valueToCompare.ToString().Split('.');
      }

      if(paramParts.Length<3)
      {
        return false;
      }
      else
      {
        for(int i=0;i<paramParts.Length;i++)
        {
          if(string.Compare(_sysLocParts[i], paramParts[i],true)!=0 && paramParts[i]!= ANY && _sysLocParts[i]!=ANY)
          {
            return false;
          }
        }
        return true;
      }
    }
    public override int GetHashCode()
    {
      return base.GetHashCode();
    }
    public override string ToString()
    {
      return string.Join(".", _sysLocParts);
    }

    /// <summary>
    /// Returns sysloc of this machine.
    /// </summary>
    public static SysLoc MySysLoc
    {
      get 
      {
         return new SysLoc(System.Environment.GetEnvironmentVariable(SYS_LOC));
      }
    }
    #endregion
  }
}

