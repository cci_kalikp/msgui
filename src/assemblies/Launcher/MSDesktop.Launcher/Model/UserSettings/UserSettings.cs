#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2006 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

              $Id: //eai/msdotnet/msgui/trunk/assemblies/Launcher/MSDesktop.Launcher/Model/UserSettings/UserSettings.cs#14 $
  $DateTime: 2014/09/16 11:37:16 $
    $Change: 897128 $
    $Author: caijin $
_____________________________________________________________________________*/
#endregion File Info Header

 
using System.Collections.Generic;
using MSDesktop.Launcher.UI;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;

namespace MSDesktop.Launcher.Model.UserSettings
{
    /// <summary>
    /// Represents an Auto Launcher UserSettings (independent of a specific release)
    /// </summary>
    [System.Xml.Serialization.XmlRoot("UserSettings")]
    public class UserSettings : ViewModelBase
    {
        #region Constants

        public const string CLASS_NAME = "UserSettings";

        #endregion Constants

        #region Private Member Variables

        private Dictionary<string, AutoStart> m_autoStartEnvironments = new Dictionary<string, AutoStart>();
        private Dictionary<string, Impersonation> m_impersonateEnvironments = new Dictionary<string, Impersonation>();
        private Dictionary<string, Favorite> m_favoriteEnvironments = new Dictionary<string, Favorite>();  

        #endregion

        #region Constructors

        /// <summary>
        /// Default constructor
        /// NOTE: This is required by XMLSerializer
        /// </summary>
        public UserSettings()
        {
        }

        #endregion

        #region Public Properties  

        private bool filterBySysLoc = true;

        /// <summary>
        /// Persisting the sys loc menu item state
        /// </summary>
        public bool FilterBySysLoc
        {
            get { return filterBySysLoc; }
            set
            {
                if (filterBySysLoc != value)
                {
                    filterBySysLoc = value;
                    OnPropertyChanged("FilterBySysLoc");
                }
            }
        }

        private bool filterByFavorite;
        public bool FilterByFavorite
        {
            get { return filterByFavorite; }
            set
            {
                if (filterByFavorite != value)
                {
                    filterByFavorite = value;
                    OnPropertyChanged("FilterByFavorite");
                }
            }
        }

        
        /// <summary>
        /// Gets / Sets the auto start list using an array
        /// NOTE: An array is required as the XMLSerializer does not support hashtables
        /// </summary>
        public AutoStart[] AutoStarts
        {
            get
            {
                if (m_autoStartEnvironments.Count == 0)
                {
                    return null;
                }
                else
                {
                    return new List<AutoStart>(m_autoStartEnvironments.Values).ToArray();
                }
            }
            set
            {
                foreach (Model.AutoStart autoStart in value)
                {
                    m_autoStartEnvironments.Add(autoStart.Key, autoStart);
                }
            }
        }

        /// <summary>
        /// Gets / Sets the impersonation list using an array
        /// NOTE: An array is required as the XMLSerializer does not support hashtables
        /// </summary>
        public Impersonation[] Impersonations
        {
            get
            {
                if (m_impersonateEnvironments.Count == 0)
                {
                    return null;
                }
                else
                {
                    return new List<Impersonation>(m_impersonateEnvironments.Values).ToArray();
                }
            }
            set
            {
                foreach (Model.Impersonation impersonation in value)
                {
                    m_impersonateEnvironments.Add(impersonation.Key, impersonation);
                }
            }
        }


        public Favorite[] Favorites
        {
            get
            {
                if (m_favoriteEnvironments.Count == 0)
                {
                    return null;
                }
                else
                {
                    return new List<Favorite>(m_favoriteEnvironments.Values).ToArray();
                }
            }
            set
            {
                foreach (Model.Favorite favorite in value)
                {
                    m_favoriteEnvironments.Add(favorite.Key, favorite);
                }
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Add a new auto start
        /// </summary>
        /// <param name="key_">the unique icon key</param>
        /// <param name="autoStart_">whether to auto start or not</param>
        public void UpdateAutoStartOverride(AutoStart autoStart_, string key_, bool autoStartValue_)
        {
            if (m_autoStartEnvironments.ContainsKey(key_))
            {
                AutoStart autoStart = m_autoStartEnvironments[key_];
                if (autoStartValue_ == autoStart_.OriginalValue)
                {
                    // remove it if it matches the original
                    m_autoStartEnvironments.Remove(key_);
                    LogLayer.Debug.Location(CLASS_NAME, "UpdateAutoStart()").Append(
                        string.Format("Removing autostart override for {0}.  Now back to original value of {1}", key_,
                                      autoStart.OriginalValue)).Send();
                }
                else
                {
                    // otherwise update it
                    m_autoStartEnvironments[key_].Value = autoStartValue_;
                    LogLayer.Debug.Location(CLASS_NAME, "UpdateAutoStart()").Append(
                        string.Format("Updating autostart override for {0} to {1} ", key_, autoStartValue_)).Send();
                }
            }
            else
            {
                AutoStart newAutoStart = new AutoStart(key_, autoStartValue_);
                    //create a copy so we don't write out the delay minutes to the file
                m_autoStartEnvironments.Add(key_, newAutoStart);
                LogLayer.Debug.Location(CLASS_NAME, "UpdateAutoStart()")
                        .Append(string.Format("Adding autostart override for {0} to {1} ", key_, autoStartValue_))
                        .Send();
            }
        }

        /// <summary>
        /// Add a new impersonation override
        /// </summary>
        /// <param name="key_">the unique icon key</param>
        /// <param name="impersonation_">whether to impersonate or not</param>
        public void UpdateImpersonationOverride(string key_, bool impersonation_)
        {
            if (m_impersonateEnvironments.ContainsKey(key_) && impersonation_ == false)
            {
                // remove it because the default is false.
                m_impersonateEnvironments.Remove(key_);
                LogLayer.Debug.Location(CLASS_NAME, "AddAutoStart()")
                        .Append(string.Format("Removing impersonation override for {0} to {1} ", key_, impersonation_))
                        .Send();
            }
            else
            {
                Impersonation impersonation = new Impersonation();
                impersonation.Key = key_;
                impersonation.Value = true;
                    // override is always true as its not possible to have a default value of true and override that to false
                m_impersonateEnvironments[key_] = impersonation;
                LogLayer.Debug.Location(CLASS_NAME, "AddAutoStart()")
                        .Append(string.Format("Adding impersonation override for {0} to {1} ", key_, impersonation_))
                        .Send();
            }
        }

        /// <summary>
        /// Add a new favorite
        /// </summary>
        /// <param name="key_">the unique icon key</param>
        /// <param name="favoriteValue_">whether to show as a favorite or not</param>
        public void UpdateFavoriteOverride(Favorite favorite_, string key_, bool favoriteValue_)
        {
            if (m_favoriteEnvironments.ContainsKey(key_))
            {
                Favorite favorite = m_favoriteEnvironments[key_];
                if (favoriteValue_ == favorite_.OriginalValue)
                {
                    // remove it if it matches the original
                    m_favoriteEnvironments.Remove(key_);
                    LogLayer.Debug.Location(CLASS_NAME, "UpdateFavoriteOverride()").Append(string.Format("Removing favorite override for {0}.  Now back to original value of {1}", key_, favorite.OriginalValue)).Send();
                }
                else
                {
                    // otherwise update it
                    m_favoriteEnvironments[key_].Value = favoriteValue_;
                    LogLayer.Debug.Location(CLASS_NAME, "UpdateFavoriteOverride()").Append(string.Format("Updating favorite override for {0} to {1} ", key_, favoriteValue_)).Send();
                }
            }
            else
            {
                Favorite newFavorite = new Favorite(key_, favoriteValue_);  //create a copy so we don't write out the delay minutes to the file
                m_favoriteEnvironments.Add(key_, newFavorite);
                LogLayer.Debug.Location(CLASS_NAME, "UpdateFavoriteOverride()").Append(string.Format("Adding favorite override for {0} to {1} ", key_, favoriteValue_)).Send();
            }
        }
        /// <summary>
        /// Retrieve a specific auto start from the collection
        /// </summary> 
        /// <returns></returns>
        public bool? GetAutoStartOverride(ViewInfo viewInfo_)
        {
            if (m_autoStartEnvironments.ContainsKey(viewInfo_.UniqueKey))
            {
                return m_autoStartEnvironments[viewInfo_.UniqueKey].Value;
            }
            if (viewInfo_.UniqueKey != viewInfo_.UniqueKeyCalculated)
            {
                AutoStart autoStart;
                if (m_autoStartEnvironments.TryGetValue(viewInfo_.UniqueKeyCalculated, out autoStart))
                {
                    m_autoStartEnvironments.Remove(viewInfo_.UniqueKeyCalculated);
                    m_autoStartEnvironments[viewInfo_.UniqueKey] = autoStart;
                    return autoStart.Value;
                }
            }
            return null;

        }


        /// <summary>
        /// Retrieve a specific impersonation from the collection
        /// </summary> 
        /// <returns></returns>
        public bool? GetImpersonationOverride(ViewInfo viewInfo_)
        {
            if (m_impersonateEnvironments.ContainsKey(viewInfo_.UniqueKey))
            {
                return m_impersonateEnvironments[viewInfo_.UniqueKey].Value;
            }
            if (viewInfo_.UniqueKey != viewInfo_.UniqueKeyCalculated)
            {
                Impersonation impersonation;
                if (m_impersonateEnvironments.TryGetValue(viewInfo_.UniqueKeyCalculated, out impersonation))
                {
                    m_impersonateEnvironments.Remove(viewInfo_.UniqueKeyCalculated);
                    m_impersonateEnvironments[viewInfo_.UniqueKey] = impersonation;
                    return impersonation.Value;
                }
            }
            return null;
        }


        /// <summary>
        /// Retrieve a specific favorite from the collection
        /// </summary>
        /// <param name="key_"></param>
        /// <returns></returns>
        public bool? GetFavoriteOverride(string key_)
        {
            if (m_favoriteEnvironments.ContainsKey(key_))
            {
                return m_favoriteEnvironments[key_].Value;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Clear all the overrides in the config
        /// </summary>
        public void ClearAutoStarts()
        {
            m_autoStartEnvironments.Clear();
        }

        /// <summary>
        /// Gets a XML string representing the object
        /// </summary>
        /// <returns></returns>
        public string ToXMLString()
        {
            return Utils.GetXMLString(this);
        }

        #endregion


    }
}
