﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;

namespace MSDesktop.Launcher.Model.UserSettings
{
    public class ExtraUserSettings : ViewModelBase
    {
        public ExtraUserSettings()
        {
            FavoriteAsFilter = true;
        }
        private bool favoriteAsFilter;
        public bool FavoriteAsFilter
        {
            get { return favoriteAsFilter; }
            set
            {
                if (favoriteAsFilter != value)
                {
                    favoriteAsFilter = value;
                    OnPropertyChanged("FavoriteAsFilter");
                }
            }
        }

        private string defaultEnvironment;
        public string DefaultEnvironment
        {
            get { return defaultEnvironment; }
            set
            {
                if (defaultEnvironment != value)
                {
                    defaultEnvironment = value;
                    OnPropertyChanged("DefaultEnvironment");
                }
            }
        }

        public bool UseModernUI { get; set; }
         
    }
}
