#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/Launcher/MSDesktop.Launcher/Model/Task.cs#1 $
  $DateTime: 2014/01/10 04:31:48 $
    $Change: 861516 $
    $Author: caijin $
_____________________________________________________________________________*/
#endregion

using System;
using System.Text;
using System.Threading;

namespace MSDesktop.Launcher.Model
{
  public class Task
  {
    public const string SHORT_TIME_FORMAT = "H:mm:ss";
    public const string TIME_FORMAT = "HH:mm:ss";
    public const string DATE_TIME_FORMAT = "yyyyMMdd-HH:mm:ss";

    private string _taskID = string.Empty;
    private string _processToKill = string.Empty;
    private string _processToCheck = string.Empty;
    private string _mutexToCheck = string.Empty;
    private string _fileToRun = string.Empty;
    private string _arguments = string.Empty;
    private bool _runsOnWeekends = false;
    private TimeSpan _startTime = TimeSpan.Zero;
    private TimeSpan _delay = TimeSpan.Zero;
    private TimeSpan _interval = TimeSpan.Zero;
    private Timer _timer = null;
    private bool _enabled = true;

    public Task (string taskID_) 
    {
      _taskID = taskID_;
    }

    public Task (Task task_) 
    {
      if (task_ != null)
      {
        this.TaskID = task_.TaskID;
        this.Arguments = task_.Arguments;
        this.Delay = task_.Delay;
        this.Enabled = task_.Enabled;
        this.FileToRun = task_.FileToRun;
        this.Interval = task_.Interval;
        this.MutexToCheck = task_.MutexToCheck;
        this.ProcessToCheck = task_.ProcessToCheck;
        this.ProcessToKill = task_.ProcessToKill;
        this.StartTime = task_.StartTime;
        this.RunsOnWeekends = task_.RunsOnWeekends;
      }
    }

    public string TaskID
    {
      get { return _taskID; }
      set { _taskID = value; }
    }

    public string ProcessToKill
    {
      get { return _processToKill; }
      set { _processToKill = value; }
    }

    public string ProcessToCheck
    {
      get { return _processToCheck; }
      set { _processToCheck = value; }
    }

    public string MutexToCheck
    {
      get { return _mutexToCheck; }
      set { _mutexToCheck = value; }
    }

    public string FileToRun
    {
      get { return _fileToRun; }
      set { _fileToRun = value; }
    }

    public string Arguments
    {
      get { return _arguments; }
      set { _arguments = value; }
    }

    public TimeSpan StartTime
    {
      get { return _startTime; }
      set { _startTime = value; }
    }

    public TimeSpan Delay
    {
      get { return _delay; }
      set { _delay = value; }
    }

    public TimeSpan Interval
    {
      get { return _interval; }
      set { _interval = value; }
    }

    public Timer Timer
    {
      get { return _timer; }
      set { _timer = value; }
    }

    public bool RunsOnWeekends
    {
      get { return _runsOnWeekends; }
      set { _runsOnWeekends = value; }
    }

    public bool Enabled
    {
      get { return _enabled; }
      set { _enabled = value; }
    }

    public override string ToString()
    {
      StringBuilder sb = new StringBuilder();
      sb.Append(" TaskID: ").Append(this.TaskID);
      sb.Append(" ProcessToKill: ").Append(this.ProcessToKill);
      sb.Append(" ProcessToCheck: ").Append(this.ProcessToCheck);
      sb.Append(" MutexToCheck: ").Append(this.MutexToCheck);
      sb.Append(" FileToRun: ").Append(this.FileToRun);
      sb.Append(" Arguments: ").Append(this.Arguments);
      sb.Append(" StartTime: ").Append(this.StartTime.ToString());
      sb.Append(" Interval: ").Append(this.Interval.Seconds.ToString());
      sb.Append(" RunsOnWeekends: ").Append(this.RunsOnWeekends.ToString());
      return sb.ToString();
    }
  }
}
