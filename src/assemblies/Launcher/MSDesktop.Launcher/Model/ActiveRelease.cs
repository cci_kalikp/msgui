#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2006 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

              $Id: //eai/msdotnet/msgui/trunk/assemblies/Launcher/MSDesktop.Launcher/Model/ActiveRelease.cs#6 $
  $DateTime: 2014/07/28 05:34:31 $
    $Change: 890260 $
    $Author: caijin $
_____________________________________________________________________________*/
#endregion File Info Header

namespace MSDesktop.Launcher.Model
{
  using System;

  //Use instead of MSCodeLocationEnum 
  public enum IEDCodeLocation
  {
    UNKNOWN=-1,
    AFS,
    AFSDEV,
    DIRECT,
    COPYLOCAL,
    COPYLOCALTEMP,
    COPYLOCALDIRECT
  }

  /// <summary>
  /// Represents a release of the application code currently in use
  /// </summary>
  [System.Xml.Serialization.XmlRoot("ActiveRelease")]
  [Serializable]
  public class ActiveRelease: IComparable
  {
    #region Private Member Variables

    private string m_version = String.Empty;
    private string m_executable = String.Empty;
    private string m_package = String.Empty;
    private string _imageName = String.Empty;
    private string _workingDirectory = String.Empty;
    private IEDCodeLocation m_codeLocation = IEDCodeLocation.UNKNOWN;
    private string m_iconFile = String.Empty;

    #endregion

    #region Constructors

    /// <summary>
    /// Default constructor
    /// NOTE: This is required by XMLSerializer
    /// </summary>
    public ActiveRelease()
    {
    }

    /// <summary>
    /// Standard constructor for general usage
    /// </summary>
    /// <param name="version_">The release version (e.g. '1.0.0')</param>
    /// <param name="executable_">The name of the executable to run</param>
    /// <param name="fileSystem_">The type of filesystem ('Windist' or 'afs')</param>
    /// <param name="iconFile_">The file to use for the icon</param>
    public ActiveRelease(string version_, string executable_, string fileSystem_, string iconFile_, string workingDirectory_, string package_)
    {
      Version = version_;
      Executable = executable_;
       SetFileSystem(fileSystem_);
      IconFile = iconFile_;
      WorkingDirectory = workingDirectory_;
      Package = package_;
    }


    #endregion

    #region Public Properties

    /// <summary>
    /// The release version number (e.g. '1.0.0')
    /// </summary>
    public string Version
    {
      get
      {
        return m_version;
      }
      set
      {
        m_version = value;
      }
    }

    /// <summary>
    /// The executable command to launch (e.g. 'start.cmd')
    /// </summary>
    public string Executable
    {
      get
      {
        return m_executable;
      }
      set
      {
        m_executable = value;
      }
    }

    /// <summary>
    /// The package to download locally and uncompress (e.g. 'Carbon.zip')
    /// </summary>
    public string Package
    {
      get
      {
        return m_package;
      }
      set
      {
        m_package = value;
      }
    }

    /// <summary>
    /// The type of file system to use as a string (e.g. 'WINDIST' or 'AFS')
    /// </summary>
    public string FileSystem
    {
      get
      {
        return m_codeLocation.ToString();
      }
      set
      {
        this.SetFileSystem(value);
      }
    }

    public int EstimatedStartupSeconds { get; set; }
    /// <summary>
    /// The enum value indicating the file system to use (e.g. WINDIST or AFS)
    /// </summary>
    public IEDCodeLocation CodeLocation
    {
      get
      {
        return m_codeLocation;
      }
    }

    /// <summary>
    /// The icon filename
    /// </summary>
    public string IconFile
    {
      get
      {
        return m_iconFile;
      }
      set
      {
        m_iconFile = value;
      }
    }

    /// <summary>
    /// Actual executable that runs, for example, Loader.exe
    /// </summary>
    public string ImageName
    {
      get
      {
        return _imageName;
      }
      set
      {
        _imageName = value;
      }
    }

    /// <summary>
    /// The directory where the executable should run.  Needed if local config expected etc.
    /// </summary>
    public string WorkingDirectory
    {
      get
      {
        return _workingDirectory;
      }
      set
      {
        _workingDirectory = value;
      }
    }



    #endregion

    #region Private Helper Methods

    /// <summary>
    /// Sets the filesystem value
    /// </summary>
    /// <param name="fileSystem_">The filesystem to use - must be 'AFS' or 'Windist'.</param>
    private void SetFileSystem (string fileSystem_)
    {
      string fileSystemUpper = fileSystem_.ToUpper();

      //Only support 
      if(Enum.IsDefined(typeof(IEDCodeLocation), fileSystemUpper))
      {
        m_codeLocation=(IEDCodeLocation) Enum.Parse(typeof(IEDCodeLocation), fileSystemUpper, false);
      }

      if(m_codeLocation==IEDCodeLocation.UNKNOWN)
      {
        throw new System.ArgumentException(String.Format("A valid Filesystem expected: {0}, {1}, {2}, {3}.", 
          IEDCodeLocation.AFS.ToString(), 
          IEDCodeLocation.AFSDEV.ToString(), 
          IEDCodeLocation.DIRECT.ToString(),
          IEDCodeLocation.COPYLOCAL.ToString()));
      }
    }

    #endregion

    #region IComparable Members

    /// <summary>
    /// Compares two active release objects using the release version string
    /// </summary>
    /// <param name="obj">The object to compare with - must be of type ActiveRelease.</param>
    /// <returns></returns>
    public int CompareTo(object obj)
    {
      ActiveRelease release = obj as ActiveRelease;
      if (release != null)
      {
        return this.Version.CompareTo(release.Version);
      }
      else
      {
        throw new ArgumentException("ActiveRelease object required for a comparison");
      }
    }

    #endregion
  }
}
