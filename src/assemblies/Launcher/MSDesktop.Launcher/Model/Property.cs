﻿using System.Xml.Serialization;

namespace MSDesktop.Launcher.Model
{ 
    public class Property
    {
        [XmlAttribute]
        public string Name { get; set; }

        [XmlAttribute] 
        public string Value { get; set; }
    }
}
