#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2006 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

              $Id: //eai/msdotnet/msgui/trunk/assemblies/Launcher/MSDesktop.Launcher/Model/Root.cs#3 $
  $DateTime: 2014/03/24 05:36:31 $
    $Change: 872658 $
    $Author: caijin $
_____________________________________________________________________________*/
#endregion File Info Header

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml.Serialization;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication;

namespace MSDesktop.Launcher.Model
{
    /// <summary>
    /// The root of the configuration model.
    /// [This simply contains a list of the applications]
    /// </summary>
    public class Root
    {
        #region Private Member Variables

        private Hashtable m_applications = CollectionsUtil.CreateCaseInsensitiveHashtable();
        private readonly object m_syncRoot = new object();

        #endregion

        #region Constructors

        /// <summary>
        /// Standard constructor
        /// NOTE: Required by XMLSerializer
        /// </summary>
        public Root()
        { 
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Object used for locking the object between threads
        /// </summary>
        public object SyncRoot
        {
            get { return m_syncRoot; }
        }


        /// <summary>
        /// Gets / Sets the applications list using an array
        /// NOTE: An array is required as the XMLSerializer does not support hashtables
        /// </summary>
        public Application[] Applications
        {
            get { return (Application[]) Utils.GetTypedArrayFromHashtable(m_applications, typeof (Model.Application)); }
            set
            {
                foreach (Model.Application app in value)
                {
                    m_applications[app.UniqueKey] = app;
                }
            }
        }

        public Application GetApplicationByKey(string key_)
        {
            lock (m_applications.SyncRoot)
            {
                if (m_applications.ContainsKey(key_))
                {
                    return m_applications[key_] as Application;
                }
            }
            return null;
        }

        private LauncherSettings settings;
        public LauncherSettings LauncherSettings
        {
            get { return settings ?? new LauncherSettings() { IsDefault = true }; }
            set { settings = value; }
        }
       
        #endregion
    }
}
