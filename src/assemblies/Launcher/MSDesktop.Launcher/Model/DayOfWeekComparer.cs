#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2006 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

              $Id: //eai/msdotnet/msgui/trunk/assemblies/Launcher/MSDesktop.Launcher/Model/DayOfWeekComparer.cs#2 $
  $DateTime: 2013/12/25 05:08:11 $
    $Change: 860054 $
    $Author: caijin $
_____________________________________________________________________________*/
#endregion File Info Header

using System;
using System.Collections;
using System.Collections.Specialized;

namespace MSDesktop.Launcher.Model
{
	/// <summary>
	/// A utility class used to check that a string represents a valid day of the week
	/// </summary>
	public class DayOfWeekChecker
	{
		#region Private Member Variables
		
		static readonly Hashtable m_daysOfTheWeek;

		#endregion

		#region Constructors

		/// <summary>
		/// A static constructor used to build up a list of all days in a week
		/// </summary>
		static DayOfWeekChecker()
		{
			//Ensure that a case insensitive check is performed
			m_daysOfTheWeek = CollectionsUtil.CreateCaseInsensitiveHashtable();
			AddDay(System.DayOfWeek.Monday);
			AddDay(System.DayOfWeek.Tuesday);
			AddDay(System.DayOfWeek.Wednesday);
			AddDay(System.DayOfWeek.Thursday);
			AddDay(System.DayOfWeek.Friday);
			AddDay(System.DayOfWeek.Saturday);
			AddDay(System.DayOfWeek.Sunday);
		}

		#endregion
	
		#region Public Static Methods

		/// <summary>
		/// Attempts to match a string for a day of the week.
		/// [Throws an ArgumentException if the string doesn't represent a valid day]
		/// </summary>
		/// <param name="day_">A string representing a day of the week (e.g. 'Monday')</param>
		/// <returns>The matching day of the week enumeration value</returns>
		public static System.DayOfWeek GetDayOfWeek(string day_)
		{
			if (m_daysOfTheWeek.ContainsKey(day_))
			{
				return (System.DayOfWeek) m_daysOfTheWeek[day_];
			}
			else
			{
				throw new ArgumentException(String.Format("'{0}' is not a valid day of the week.", day_));
			}
		}

		#endregion

		#region Private Helper Methods

		/// <summary>
		/// Internal method used to build up the hash of allowed days
		/// </summary>
		/// <param name="dow">The enumeration day value to add to the hash</param>
		private static void AddDay(System.DayOfWeek dow)
		{
			m_daysOfTheWeek.Add(dow.ToString(), dow);
		}

		#endregion
	}
}
