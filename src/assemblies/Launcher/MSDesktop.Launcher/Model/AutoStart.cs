#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2006 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

              $Id: //eai/msdotnet/msgui/trunk/assemblies/Launcher/MSDesktop.Launcher/Model/AutoStart.cs#4 $
  $DateTime: 2014/04/10 09:09:20 $
    $Change: 876092 $
    $Author: caijin $
_____________________________________________________________________________*/
#endregion File Info Header
 
using System;
using System.Xml.Serialization;

namespace MSDesktop.Launcher.Model
{
    using System.ComponentModel;

    /// <summary>
    /// Represents a auto start override key/boolean
    /// </summary>
    [System.Xml.Serialization.XmlRoot("AutoStart")]
    public class AutoStart : IComparable, INotifyPropertyChanged
    {
        #region Private Member Variables
        private const string CLASS_NAME = "AutoStart";
        private static readonly int DEFAULT_DELAY_MINUTES = 5;
        private bool m_autoStart = false;

        #endregion

        #region Constructors

        /// <summary>
        /// Default constructor
        /// NOTE: This is required by XMLSerializer
        /// </summary>
        public AutoStart()
        {
        }

        /// <summary>
        /// The standard constructor for general usage
        /// </summary>
        /// <param name="key_">Unique key of the icon</param>
        /// <param name="autoStart_">Flag to autostart or not</param>
        public AutoStart(string key_, bool autoStart_)
        {
            this.Key = key_;
            this.Value = autoStart_;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// The key of the environment
        /// </summary>
        [XmlAttribute("iconId")]
        public string Key { get; set; }

        /// <summary>
        /// The number of minutes to delay
        /// </summary>
        [XmlAttribute("delayMinutes")]
        public string DelayMinutesInternal { get; set; }

        public int DelayMinutes
        {
            get
            {
                int minutes = DEFAULT_DELAY_MINUTES;
                try
                {
                    if (DelayMinutesInternal != null && DelayMinutesInternal.Length > 0)
                    {
                        return Int32.Parse(DelayMinutesInternal);
                    }
                }
                catch (Exception)
                {
                    LogLayer.Error.Location(CLASS_NAME, "DelayMinutesInternal").Append(string.Format("Delay minutes specifed not an integer value. Value: {0}", DelayMinutesInternal)).Send();
                }
                return minutes;
            }
        }

        /// <summary>
        /// Flag indicating whether app should auto start according to Allowed Time Group
        /// </summary>
        [XmlText]
        public bool Value
        {
            get
            {
                return m_autoStart;
            }
            set
            {
                m_autoStart = value;
                OnPropertyChanged("Value");
            }
        }

        /// <summary>
        /// Flag indicating whether app should auto start according to Allowed Time Group
        /// </summary>
        [XmlIgnore]
        public bool? OriginalValue
        {
            get;
            set;
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Gets a XML string representing the object
        /// </summary>
        /// <returns></returns>
        public string ToXMLString()
        {
            return Utils.GetXMLString(this);
        }

        #endregion

        #region IComparable Members

        /// <summary>
        /// Compares two auto start objects using their keys
        /// </summary>
        /// <param name="obj">The object to compare with - must be of type AutoStart.</param>
        /// <returns></returns>
        public int CompareTo(object obj)
        {
            AutoStart autoStart = obj as AutoStart;
            if (autoStart != null)
            {
                return this.Key.CompareTo(autoStart.Key);
            }
            else
            {
                throw new ArgumentException("AutoStart object required for a comparison");
            }
        }
        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName_)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName_));
            }
        }
        #endregion
    }
}
