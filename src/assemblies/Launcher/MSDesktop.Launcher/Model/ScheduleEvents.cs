#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2006 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

              $Id: //eai/msdotnet/msgui/trunk/assemblies/Launcher/MSDesktop.Launcher/Model/ScheduleEvents.cs#4 $
  $DateTime: 2014/02/09 22:41:22 $
    $Change: 865813 $
    $Author: caijin $
_____________________________________________________________________________*/
#endregion File Info Header

using System;
using MSDesktop.Launcher.UI;

namespace MSDesktop.Launcher.Model
{
  /// <summary>
  /// Base class for Scheduled Events
  /// </summary>
  public abstract class ScheduleEvent
  {
    public static readonly string TIME_FORMAT = "HH:mm";

    #region protected state
    protected const int DAYS_IN_WEEK = 7;
    protected readonly TimeSpan NOT_SCHEDULED = TimeSpan.Zero;

    //Times to act for each of the 7 days in the week
    protected TimeSpan[] _eventTimes = new TimeSpan[DAYS_IN_WEEK];
    #endregion

    #region Protected Ctor()
    protected ScheduleEvent(ViewInfo vi_, ProcessLaunchManager processLaunchManager_)
    {
      if (vi_ == null)
        throw new ArgumentNullException("vi_", "ViewInfo parameter cannot be null.");

      ViewInfo = vi_;
      ProcessLaunchManager = processLaunchManager_;

      for (int i = 0; i < DAYS_IN_WEEK; i++)
      {
        _eventTimes[i] = NOT_SCHEDULED;
      }
    }
    #endregion

    #region Public methods
    public ViewInfo ViewInfo
    {
      get; private set;
    }

    public ProcessLaunchManager ProcessLaunchManager
    {
      get; private set;
    }

    public void SetTime(DayOfWeek weekDay_, TimeSpan timeToAct_)
    {
      _eventTimes[(int)weekDay_] = timeToAct_;
    }

    public void SetTime(DayOfWeek weekDay_, DateTime timeToAct_)
    {
      TimeSpan timeSpanTTA = timeToAct_.Subtract(DateTime.Today);
      SetTime(weekDay_, timeSpanTTA);
    }

    public abstract void SetTodaysSchedule(bool initialLoad_);
    #endregion
  }

  /// <summary>
  /// StartEvent implementation of the base
  /// </summary>
  public class StartEvent : ScheduleEvent
  {
    private const string CLASS_NAME = "StartEvent";

    #region Ctor override
    public StartEvent(ViewInfo vi_, ProcessLaunchManager processLaunchManager_)
      : base(vi_, processLaunchManager_)
    {
    }
    #endregion

    #region Override
    /// <summary>
    /// 
    /// </summary>
    /// <param name="initialLoad_">This is true the first time the app starts to cover apps autostarting earlier on same day launcher was started</param>
    public override void SetTodaysSchedule(bool initialLoad_)
    {
      TimeSpan now = DateTime.Now.Subtract(DateTime.Today);
      bool scheduledToStartToday = (_eventTimes[(int)DateTime.Today.DayOfWeek] != NOT_SCHEDULED);
      bool startTimeIsAfterNow = (_eventTimes[(int)DateTime.Today.DayOfWeek] >= now);

      if (scheduledToStartToday)
      {
        if (initialLoad_ && !startTimeIsAfterNow)
        {
          //On the initial start up of launcher we want to launch now any process that has missed todays auto start time.
          LogLayer.Info.Location(CLASS_NAME, "SetTodaysSchedule").Append(string.Format("Starting application {0} immediately on start up as today's start time has passed", ViewInfo.ShortcutDescription)).Send();
          ProcessLaunchManager.ScheduleStart(ViewInfo, now);
        }
        else if (startTimeIsAfterNow)
        {
          if (ViewInfo.AutoStart.DelayMinutes > 0)
          {
            //randomise the start time
            int delayMinutes = (new Random()).Next(1, ViewInfo.AutoStart.DelayMinutes);
            TimeSpan delayTimeSpan = new TimeSpan(0, delayMinutes, 0);

            //add on the random delay to the start time
            _eventTimes[(int)DateTime.Now.DayOfWeek] += delayTimeSpan;

            LogLayer.Info.Location(CLASS_NAME, "SetTodaysSchedule").Append(string.Format("Adding random delay of {0} minutes to application start time {1}.  New start time {2}", delayMinutes, ViewInfo.ShortcutDescription, ((TimeSpan)_eventTimes[(int)DateTime.Now.DayOfWeek])).ToString()).Send();
          }
          LogLayer.Info.Location(CLASS_NAME, "SetTodaysSchedule").Append(string.Format("Requesting auto start of {0}", ViewInfo.ShortcutDescription)).Send();
          ProcessLaunchManager.ScheduleStart(ViewInfo, _eventTimes[(int)DateTime.Now.DayOfWeek]);
        }
      }
    }

    public override string ToString()
    {
      return "Start event -- " + ViewInfo.ShortcutDescription + " :  " + ((TimeSpan)_eventTimes[(int)DateTime.Today.DayOfWeek]).ToString();
    }
    #endregion
  }
}
