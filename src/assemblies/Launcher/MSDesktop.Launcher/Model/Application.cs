#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2006 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

              $Id: //eai/msdotnet/msgui/trunk/assemblies/Launcher/MSDesktop.Launcher/Model/Application.cs#7 $
  $DateTime: 2014/08/06 01:46:50 $
    $Change: 891586 $
    $Author: caijin $
_____________________________________________________________________________*/
#endregion File Info Header

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Xml.Serialization;

namespace MSDesktop.Launcher.Model
{
    /// <summary>
    /// Represents an Auto Launcher Application (independent of a specific release)
    /// </summary>
    [System.Xml.Serialization.XmlRoot("Application")]
    public class Application : IComparable
    {
        #region Private Member Variables

        private string m_description = String.Empty;
        private string m_meta = String.Empty;
        private string m_project = String.Empty;
        private string m_iconfile = String.Empty;
        private string m_appID = String.Empty;
        private string m_usingLdap = String.Empty;

        private Hashtable m_activeReleases = CollectionsUtil.CreateCaseInsensitiveHashtable();
        private Hashtable m_allowedTimeGroups = CollectionsUtil.CreateCaseInsensitiveHashtable();
        private Hashtable m_environmentGroups = CollectionsUtil.CreateCaseInsensitiveHashtable();

        #endregion

        #region Constructors

        /// <summary>
        /// Default constructor
        /// NOTE: This is required by XMLSerializer
        /// </summary>
        public Application()
        {
        }

        /// <summary>
        /// The standard constructor for general usage
        /// </summary>
        /// <param name="description_">A friendly description for the application</param>
        /// <param name="meta_">The Meta tag for a team (e.g. 'ied')</param>
        /// <param name="project_">The Project name</param>
        public Application(string description_, string meta_, string project_, string iconfile)
        {
            m_description = description_;
            m_meta = meta_;
            m_project = project_;
            m_iconfile = iconfile;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// AppID to crosscheck entitlements in eclipse and an ID used in Root.xml file
        /// </summary>
        [XmlAttribute("appID")]
        public string ID
        {
            get { return m_appID; }
            set { m_appID = value; }
        }

        /// <summary>
        /// AppID to crosscheck entitlements in eclipse and an ID used in Root.xml file
        /// </summary>
        [XmlAttribute("usingLDAP")]
        public string UsingLDAP
        {
            get { return m_usingLdap; }
            set { m_usingLdap = value; }
        }

        /// <summary>
        /// A user friendly description for the application
        /// </summary>
        public string Description
        {
            get { return m_description; }
            set { m_description = value; }
        }

        /// <summary>
        /// The standard Meta tag for the application (e.g. ied)
        /// </summary>
        public string Meta
        {
            get { return m_meta; }
            set { m_meta = value; }
        }

        /// <summary>
        /// The standard project name for the application(e.g. owm_ln_deriv)
        /// </summary>
        public string Project
        {
            get { return m_project; }
            set { m_project = value; }
        }

        public bool IsHidden { get; set; }

        /// <summary>
        /// The Icon Path for the whole application (to display in the Outlook group bar)
        /// </summary>
        public string IconFile
        {
            get { return m_iconfile; }
            set { m_iconfile = value; }
        }

        public string MessageDispatchFailMessageOverride { get; set; }
        public string MessageDispatchSucceedMessageOverride { get; set; }
        public string MessageDispatchFailHtml { get; set; }
        public string MessageDispatchSucceedHtml { get; set; }

        public int EstimatedStartupSeconds { get; set; }

        /// <summary>
        ///  Gets / Sets the active release list using an array
        ///  NOTE: An array is required as the XMLSerializer does not support hashtables
        ///  </summary>
        public ActiveRelease[] ActiveReleases
        {
            get { return (ActiveRelease[]) Utils.GetTypedArrayFromHashtable(m_activeReleases, typeof (Model.ActiveRelease)); }
            set
            {
                foreach (Model.ActiveRelease release in value)
                {
                    m_activeReleases.Add(release.Version, release);
                }
            }
        }

        /// <summary>
        /// Gets / Sets the allowed time group list using an array
        /// NOTE: An array is required as the XMLSerializer does not support hashtables
        /// </summary>
        public AllowedTimeGroup[] AllowedTimeGroups
        {
            get
            {
                return
                    (AllowedTimeGroup[])
                    Utils.GetTypedArrayFromHashtable(m_allowedTimeGroups, typeof (Model.AllowedTimeGroup));
            }
            set
            {
                foreach (Model.AllowedTimeGroup group in value)
                {
                    m_allowedTimeGroups.Add(group.Name, group);
                }
            }
        }

        /// <summary>
        /// Gets / Sets the environment group list using an array
        /// NOTE: An array is required as the XMLSerializer does not support hashtables
        /// </summary>
        public EnvironmentGroup[] EnvironmentGroups
        {
            get
            {
                return
                    (EnvironmentGroup[])
                    Utils.GetTypedArrayFromHashtable(m_environmentGroups, typeof (Model.EnvironmentGroup));
            }
            set
            {
                foreach (Model.EnvironmentGroup group in value)
                {
                    m_environmentGroups.Add(group.Name, group);
                }
            }
        }

        /// <summary>
        /// Gets an existing time group.  
        /// returns null of not present
        /// </summary>
        /// <param name="allowedTimeGroupName_">The time group name</param>
        /// <returns></returns>
        public AllowedTimeGroup GetTimeGroup(string allowedTimeGroupName_)
        {
            return GetTimeGroup(allowedTimeGroupName_, false);
        }

        /// <summary>
        /// The unique key for the application {Meta.Project}
        /// </summary>
        public string UniqueKey
        {
            get { return m_meta + "." + m_project + "." + m_appID + "." + m_description; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Retrieves a release based on its version number
        /// </summary>
        /// <param name="version_"></param>
        /// <returns></returns>
        public Model.ActiveRelease GetRelease(string version_)
        {
            Model.ActiveRelease match = null;

            if (ReleaseExists(version_))
            {
                match = (Model.ActiveRelease) m_activeReleases[version_];
            }

            return match;
        }

        /// <summary>
        /// Gets a XML string representing the object
        /// </summary>
        /// <returns></returns>
        public string ToXMLString()
        {
            return Utils.GetXMLString(this);
        }

        #endregion

        #region Private Helper Methods

        /// <summary>
        /// Gets an existing environment group.  
        /// Adds a new one if it doesn't exist and the createNew parameter is true.
        /// </summary>
        /// <param name="environmentGroupName_">The environment group name</param>
        /// <param name="createNew">If true, a new group is created if it doesn't exist.</param>
        /// <returns></returns>
        private EnvironmentGroup GetEnvGroup(string environmentGroupName_, bool createNew)
        {
            EnvironmentGroup group = null;

            if (m_environmentGroups.ContainsKey(environmentGroupName_))
            {
                group = (EnvironmentGroup) m_environmentGroups[environmentGroupName_];
            }
            else if (createNew)
            {
                group = new EnvironmentGroup(environmentGroupName_);
                m_environmentGroups.Add(group.Name, group);
            }

            return group;
        }

        /// <summary>
        /// Gets an existing time group.  
        /// Adds a new one if it doesn't exist and the createNew parameter is true.
        /// </summary>
        /// <param name="allowedTimeGroupName_">The time group name</param>
        /// <param name="createNew">If true, a new group is created if it doesn't exist.</param>
        /// <returns></returns>
        private AllowedTimeGroup GetTimeGroup(string allowedTimeGroupName_, bool createNew)
        {
            AllowedTimeGroup group = null;

            if (TimeGroupExists(allowedTimeGroupName_))
            {
                group = (AllowedTimeGroup) m_allowedTimeGroups[allowedTimeGroupName_];
            }
            else if (createNew)
            {
                group = new AllowedTimeGroup(allowedTimeGroupName_);
                m_allowedTimeGroups.Add(group.Name, group);
            }

            return group;
        }

        /// <summary>
        /// Checks to see if a release with the specified version number exists
        /// </summary>
        /// <param name="release_">The release version string (e.g. '1.0.0') to look for</param>
        /// <returns>True if the release exists.</returns>
        private bool ReleaseExists(string release_)
        {
            return this.m_activeReleases.ContainsKey(release_);
        }

        /// <summary>
        /// Checks to see if a time group with the specified name exists
        /// </summary>
        /// <param name="allowedTimeGroupName_">The time group name to look for</param>
        /// <returns>True if the group exists.</returns>
        private bool TimeGroupExists(string allowedTimeGroupName_)
        {
            return allowedTimeGroupName_ != null && this.m_allowedTimeGroups.ContainsKey(allowedTimeGroupName_);
        }

        #endregion

        #region IComparable Members

        /// <summary>
        /// Compares two application objects using the uniquekey
        /// </summary>
        /// <param name="obj">The object to compare with - must be of type Application.</param>
        /// <returns></returns>
        public int CompareTo(object obj)
        {
            Application app = obj as Application;
            if (app != null)
            {
                return this.UniqueKey.CompareTo(app.UniqueKey);
            }
            else
            {
                throw new ArgumentException("Application object required for a comparison");
            }
        }

        #endregion

        //public override string ToString()
        //{
        //    return this.m_description;
        //}
    }

    public class ApplicationDescriptionComparer : IComparer<Model.Application>
    {

        public int Compare(Application x, Application y)
        {
            return System.String.Compare(x.Description, y.Description, System.StringComparison.Ordinal);

        }
    }
}
