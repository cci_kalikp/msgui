#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2006 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

              $Id: //eai/msdotnet/msgui/trunk/assemblies/Launcher/MSDesktop.Launcher/Model/AllowedTimeGroup.cs#2 $
  $DateTime: 2013/12/25 05:08:11 $
    $Change: 860054 $
    $Author: caijin $
_____________________________________________________________________________*/
#endregion File Info Header

using System;
using System.Collections;

namespace MSDesktop.Launcher.Model
{
	/// <summary>
	/// Represents a group time groups during which an environment is available for use
	/// </summary>
	[System.Xml.Serialization.XmlRoot("AllowedTimeGroup")]
	public class AllowedTimeGroup: IComparable
	{
		#region Private Member Variables

		private string m_name = String.Empty;
		private ArrayList m_allowedTimes = new ArrayList();

		#endregion

		#region Constructors

		/// <summary>
		/// Default constructor
		/// NOTE: This is required by XMLSerializer
		/// </summary>
		public AllowedTimeGroup()
		{
		}

		/// <summary>
		/// Standard constructor for general usage 		
		/// </summary>
		/// <param name="allowedTimeGroupName_">The name of the time group</param>
		public AllowedTimeGroup(string allowedTimeGroupName_)
		{
			this.Name = allowedTimeGroupName_;
		}

		#endregion

		#region Public Properties

		/// <summary>
		/// The name of the time group
		/// </summary>
		public string Name
		{
			get
			{
				return m_name;
			}
			set
			{
				m_name = value;
			}
		}

		/// <summary>
		/// Gets / Sets the allowed time list using an array
		/// NOTE: An array is required as the XMLSerializer does not support collections correctly
		/// </summary>
		public AllowedTime[] AllowedTimes
		{
			get
			{
				return (AllowedTime[]) m_allowedTimes.ToArray(typeof(Model.AllowedTime));
			}
			set
			{
				m_allowedTimes = new ArrayList(value);
			}
		}
		
		#endregion

		#region Public Methods

		/// <summary>
		/// Adds a new time range
		/// </summary>
		/// <param name="startDay_">The start day (e.g. 'Monday')</param>
		/// <param name="startTime_">The start time (e.g. '09:00')</param>
		/// <param name="endDay_">The end day (e.g. 'Monday')</param>
		/// <param name="endTime_">The end time (e.g. '18:00')</param>
		public void AddAllowedTime(string startDay_, string startTime_, string endDay_, string endTime_)
		{
			AllowedTime allowedTime = new AllowedTime(startDay_, startTime_, endDay_, endTime_);
			m_allowedTimes.Add(allowedTime);
		}

		#endregion

		#region IComparable Members

		/// <summary>
		/// Compares two time group objects using the name
		/// </summary>
		/// <param name="obj">The object to compare with - must be of type AllowedTimeGroup.</param>
		/// <returns></returns>
		public int CompareTo(object obj)
		{
			AllowedTimeGroup group = obj as AllowedTimeGroup;
			if (group != null)
			{
				return this.Name.CompareTo(group.Name);
			}
			else
			{
				throw new ArgumentException("AllowedTimeGroup object required for a comparison");
			}
		}

		#endregion
	}
}
