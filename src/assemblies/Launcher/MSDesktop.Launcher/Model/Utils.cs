#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2006 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

              $Id: //eai/msdotnet/msgui/trunk/assemblies/Launcher/MSDesktop.Launcher/Model/Utils.cs#2 $
  $DateTime: 2013/12/25 05:08:11 $
    $Change: 860054 $
    $Author: caijin $
_____________________________________________________________________________*/
#endregion File Info Header

using System;
using System.Collections;
using System.Text;

using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace MSDesktop.Launcher.Model
{
  /// <summary>
  /// General utility methods
  /// </summary>
  public class Utils
  {
    #region constants

    #endregion

    #region Public Static Methods

    /// <summary>
    /// Convert the obj into an XML string using the XmlSerializer
    /// </summary>
    /// <param name="obj">The object to represent as XML</param>
    /// <returns>A string containing the XML for the object</returns>
    public static string GetXMLString(object obj)
    {
      System.Xml.Serialization.XmlSerializer ser = new XmlSerializer(obj.GetType());
      System.IO.StringWriter sw = new StringWriter();
      ser.Serialize(sw, obj);
      return sw.ToString();
    }

    /// <summary>
    /// Extracts objects from a hashtable into a strongly typed array
    /// </summary>
    /// <param name="hash">The hashtable containing the objects</param>
    /// <param name="type">The type of the output array</param>
    /// <returns>An array </returns>
    public static System.Array GetTypedArrayFromHashtable(Hashtable hash, System.Type type)
    {
      ArrayList list;

      if (hash.Count > 0)
      {
        list = new ArrayList(hash.Values);
        list.Sort();
      }
      else
      {
        list = new ArrayList();
      }

      return list.ToArray(type);
    }

    #endregion
  }
}
