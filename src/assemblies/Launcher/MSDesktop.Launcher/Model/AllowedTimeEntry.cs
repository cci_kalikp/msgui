#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2006 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

              $Id: //eai/msdotnet/msgui/trunk/assemblies/Launcher/MSDesktop.Launcher/Model/AllowedTimeEntry.cs#2 $
  $DateTime: 2013/12/25 05:08:11 $
    $Change: 860054 $
    $Author: caijin $
_____________________________________________________________________________*/
#endregion File Info Header

using System;

namespace MSDesktop.Launcher.Model
{
	/// <summary>
	/// Represents a day/time combination 
	/// </summary>
	public class AllowedTimeEntry
	{
		#region Private Member Variables

		private System.DayOfWeek m_day;
		private System.DateTime m_time;

		#endregion

		#region Constructors

		/// <summary>
		/// Default constructor
		/// NOTE: This is required by XMLSerializer
		/// </summary>
		public AllowedTimeEntry()
		{
		}

		/// <summary>
		/// Standard constructor for general usage 
		/// </summary>
		/// <param name="day_">The day of the week as a string (e.g. 'Monday')</param>
		/// <param name="time_">The time of the day as a string (e.g. '09:00')</param>
		public AllowedTimeEntry(string day_, string time_)
		{
			SetDay(day_);
			SetTime(time_);
		}
		
		#endregion

		#region Public Properties

		/// <summary>
		/// The day as a string (e.g. 'Monday')
		/// </summary>
		public string Day
		{
			get
			{
				return m_day.ToString();
			}
			set
			{
				SetDay(value);
			}
		}

		/// <summary>
		/// The time as a string(e.g. '09:00')
		/// </summary>
		public string Time
		{
			get
			{
				return m_time.ToString("HH:mm");
			}
			set
			{
				SetTime(value);
			}
		}

		#endregion

		#region Private Helper Methods

		/// <summary>
		/// Internal method used to set the day of the week 
		/// </summary>
		/// <param name="day_">The day of the week (e.g. 'Monday')</param>
		private void SetDay(string day_)
		{
			this.m_day = DayOfWeekChecker.GetDayOfWeek(day_);
		}

		/// <summary>
		/// Internal method used to set the time of day
		/// </summary>
		/// <param name="time_"></param>
		private void SetTime(string time_)
		{
			//TODO - Improve the exception handling here?!
			if (time_.Length == 5)
			{
				m_time = System.DateTime.Parse(time_);
			}
			else
			{
				throw new System.FormatException("Time format expected is hh:nn");
			}
		}

		#endregion
	}
}
