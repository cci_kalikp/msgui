﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace MSDesktop.Launcher.Model
{
    /// <summary>
    /// Represents an Impersonation override key/boolean
    /// </summary>
    [System.Xml.Serialization.XmlRoot("Impersonation")]
    public class Impersonation : IComparable, INotifyPropertyChanged
    {
        #region Private Member Variables
        private const string CLASS_NAME = "Impersonation";
        private bool m_impersonate = false;
        #endregion

        #region Constructors

        /// <summary>
        /// Default constructor
        /// NOTE: This is required by XMLSerializer
        /// </summary>
        public Impersonation()
        {
        }

        /// <summary>
        /// The standard constructor for general usage
        /// </summary>
        /// <param name="key_">Unique key of the icon</param>
        /// <param name="commandLineArgument_">If impersonation is true, the command line argument to pass in.</param>
        /// <param name="ldapGroup_">The ldapGroup you need to a member of to be allowed to impersonate</param>
        public Impersonation(string key_, string commandLineArgument_, string ldapGroup_)
        {
            this.Key = key_;
            this.CommandLineArgument = commandLineArgument_;
            this.LdapGroup = ldapGroup_;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// The key of the environment
        /// </summary>
        [XmlAttribute("iconId")]
        public string Key
        {
            get;
            set;
        }

        /// <summary>
        /// The command line argument if impersonation is allowed
        /// </summary>
        [XmlAttribute("commandLineArgument")]
        public string CommandLineArgument
        {
            get;
            set;
        }

        /// <summary>
        /// LDAP group you should be in to be allowed to impersonate.
        /// </summary>
        [XmlAttribute("ldapGroup")]
        public string LdapGroup
        {
            get;
            set;
        }

        public string[] LdapGroupsSplit
        {
            get { return LdapGroup.Split(';'); }
        }

        /// <summary>
        /// Flag indicating whether app should auto start according to Allowed Time Group
        /// </summary>
        [XmlText]
        public bool Value
        {
            get
            {
                return m_impersonate;
            }
            set
            {
                m_impersonate = value;
                OnPropertyChanged("Value");
            }
        }

        [XmlIgnore]
        public bool Visible
        {
            get { return LdapGroup != null && CommandLineArgument != null; }
        }

        [XmlIgnore]
        public bool IsEnabled
        {
            get
            {
                if (Visible)
                {
                    foreach (string group in LdapGroupsSplit)
                    {
                        if (Groups.Contains(group))
                            return true;
                    }
                }
                return false;
            }
        }

        [XmlIgnore]
        public IList<string> Groups { get; set; }

        [XmlIgnore]
        public string Text
        {
            get
            {
                return "Support Access controlled by mailgroup(s): " + LdapGroup;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Gets a XML string representing the object
        /// </summary>
        /// <returns></returns>
        public string ToXMLString()
        {
            return Utils.GetXMLString(this);
        }

        #endregion

        #region IComparable Members

        /// <summary>
        /// Compares two auto start objects using their keys
        /// </summary>
        /// <param name="obj">The object to compare with - must be of type Impersonation.</param>
        /// <returns></returns>
        public int CompareTo(object obj)
        {
            Impersonation autoStart = obj as Impersonation;
            if (autoStart != null)
            {
                return this.Key.CompareTo(autoStart.Key);
            }
            else
            {
                throw new ArgumentException("Impersonation object required for a comparison");
            }
        }
        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName_)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName_));
            }
        }
        #endregion
    }
}