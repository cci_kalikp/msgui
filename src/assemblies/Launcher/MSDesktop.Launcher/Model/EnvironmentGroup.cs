#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2006 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

              $Id: //eai/msdotnet/msgui/trunk/assemblies/Launcher/MSDesktop.Launcher/Model/EnvironmentGroup.cs#12 $
  $DateTime: 2014/07/28 05:34:31 $
    $Change: 890260 $
    $Author: caijin $
_____________________________________________________________________________*/
#endregion File Info Header

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace MSDesktop.Launcher.Model
{
    /// <summary>
    /// Represents an environment group
    /// </summary>
    [System.Xml.Serialization.XmlRoot("EnvironmentGroup")] 
    public class EnvironmentGroup : IComparable
    {
        #region Private Member Variables

        private readonly string CLASS = "EnvironmentGroup";

        private string m_name = String.Empty;
        private Hashtable m_environments = new Hashtable();

        #endregion

        #region Constructors

        /// <summary>
        /// Default constructor
        /// NOTE: This is required by XMLSerializer
        /// </summary>
        public EnvironmentGroup()
        {
        }

        /// <summary>
        /// The standard constructor for general usage
        /// </summary>
        /// <param name="name_">The name of the environment group</param>
        public EnvironmentGroup(string name_)
        {
            m_name = name_;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets / Sets the name of the environment group
        /// </summary>
        public string Name
        {
            get { return m_name; }
            set
            {
                m_name = value; 
            }
        }

        public string IconPath { get; set; }

        public bool IsHidden { get; set; }
         
        /// <summary>
        /// Gets / Sets the environment list using an array
        /// An array is required as the XMLSerializer does not support hashtables
        /// </summary>
        public Environment[] Environments
        {
            get
            {
                return (Environment[]) Utils.GetTypedArrayFromHashtable(m_environments, typeof (Model.Environment));
            }

            set
            {
                foreach (Model.Environment env in value)
                {
                    try
                    {
                        if (env.Name != null)
                        {
                            if (m_environments.ContainsKey(env.Name))
                            {
                                LogLayer.Info.Location(CLASS, "set_Environments").Append(
                                    String.Format("There is already an environment with name {0} in this group.",
                                                  env.Name)).Send();
                            }
                            else
                            {
                                m_environments.Add(env.Name, env);
                            }
                        }
                    }
                    catch (Exception ex_)
                    {
                        string msg = string.Format("Failed to add environment {0}. Exception {1}", env.Name, ex_);
                        LogLayer.Error.Location(CLASS, "set_Environments").Append(msg).Send();
                    }
                }
            }
        }
         
        [NonSerialized]
        private EnvironmentType? environmentType;
        public EnvironmentType EnvironmentType
        {
            get
            {
                if (environmentType == null)
                {
                    ResolveEnvironmentType();
                }
                return environmentType.Value;
            }
        }

        [NonSerialized]
        private EnvironmentType? environmentTypeForSort;

        public EnvironmentType EnvironmentTypeForSort
        {
            get
            {
                if (environmentTypeForSort == null)
                {
                    ResolveEnvironmentType();
                }
                return environmentTypeForSort.Value;
            }
        }

        private static readonly Dictionary<string, EnvironmentType> environmentTypeMap = new Dictionary<string, EnvironmentType>()
            {
                {"production", EnvironmentType.Prod},
                {"prod", EnvironmentType.Prod},
                {"production64bit", EnvironmentType.Prod},
                {"production(Readonly)", EnvironmentType.Prod},
                {"previous", EnvironmentType.Prod},
                {"prod pilot", EnvironmentType.Prod},
                {"prodpilot", EnvironmentType.Prod},
                {"pilot", EnvironmentType.Prod},
                {"prodalpha", EnvironmentType.Prod},
                {"prodbeta", EnvironmentType.Prod},
                {"prod beta",EnvironmentType.Prod},
                {"beta", EnvironmentType.Prod},
                {"uat", EnvironmentType.UAT},
                {"next gen", EnvironmentType.QA},
                {"prodsim", EnvironmentType.Prod},
                {"simulator", EnvironmentType.Prod},
                {"sim", EnvironmentType.Prod},
                {"qa", EnvironmentType.QA},
                {"dev", EnvironmentType.DEV},
                {"support", EnvironmentType.Prod},
                {"daily", EnvironmentType.DEV}
            };

         
        private void ResolveEnvironmentType()
        {  
            string name = Name.ToLower(); 
            if (environmentTypeMap.ContainsKey(name))
            {
                environmentType = environmentTypeMap[name];
            }
            else if (name.StartsWith("prod"))
            {
                environmentType = EnvironmentType.Prod;
            }
            else if (name.Contains("uat"))
            {
                environmentType = EnvironmentType.UAT;
            }
            else if (name.Contains("qa"))
            {
                environmentType = EnvironmentType.QA;
            }
            else if (name.Contains("dev"))
            {
                environmentType = EnvironmentType.DEV;
            }
            else if (m_environments.Values.Count > 0)
            {
                var groupCounts = new Dictionary<EnvironmentType, int>()
                    {
                        {EnvironmentType.Prod, 0},
                        {EnvironmentType.UAT, 0},
                        {EnvironmentType.QA, 0},
                        {EnvironmentType.DEV, 0}
                    }; 
                foreach (Model.Environment environment in m_environments.Values)
                {
                    string mailGroup = environment.MailgroupString.Replace("iedlauncher-dev", string.Empty);
                    if (mailGroup.Contains("_prod") ||
                        mailGroup.Contains("_pilot") ||
                        mailGroup.Contains("_beta")||
                        mailGroup.Contains("-prod") ||
                        mailGroup.Contains("-pilot") ||
                        mailGroup.Contains("-beta")) groupCounts[EnvironmentType.Prod]++;
                    if (mailGroup.Contains("_uat") || mailGroup.Contains("-uat")) groupCounts[EnvironmentType.UAT]++;
                    if (mailGroup.Contains("_qa") ||
                        mailGroup.Contains("_nextgen") ||
                        mailGroup.Contains("-qa") ||
                        mailGroup.Contains("-nextgen")) groupCounts[EnvironmentType.QA]++;
                    if (mailGroup.Contains("_dev") || mailGroup.Contains("-dev")) groupCounts[EnvironmentType.DEV]++;
                }
                var pair = groupCounts.OrderByDescending(env_ => env_.Value).First();

                environmentType = pair.Value > 0 ? pair.Key : EnvironmentType.Others;
            }
            else
            {
                environmentType = EnvironmentType.Others;
            }

            //this is a special group, which should belong to Prod environment type logically, but should be sorted as low as a dev env group   
            if (name == "support")
            {
                environmentTypeForSort = EnvironmentType.DEV;
            }
            else
            {
                environmentTypeForSort = EnvironmentType;
            }

        }

        internal static EnvironmentType GetEnvironmentTypeForSort(string environmentGroupName_)
        {
            string name = environmentGroupName_.ToLower();
            if (name == "support")
            {
                return EnvironmentType.DEV;
            }
            if (environmentTypeMap.ContainsKey(name))
            {
               return environmentTypeMap[name];
            }
            if (name.StartsWith("prod"))
            {
                return EnvironmentType.Prod;
            }
            if (name.Contains("uat"))
            {
                return EnvironmentType.UAT;
            }
            if (name.Contains("qa"))
            {
                return EnvironmentType.QA;
            }
            if (name.Contains("dev"))
            {
                return EnvironmentType.DEV;
            }
            return EnvironmentType.Others;
        }
        #endregion

        #region Public Methods
 
        /// <summary>
        /// Gets a XML string representing the object
        /// </summary>
        /// <returns></returns>
        public string ToXMLString()
        {
            return Utils.GetXMLString(this);
        }

        #endregion

        #region IComparable Members

        /// <summary>
        /// Compares two environment group objects using the name
        /// </summary>
        /// <param name="obj">The object to compare with - must be of type EnvironmentGroup.</param>
        /// <returns></returns>
        public int CompareTo(object obj)
        {
            EnvironmentGroup group = obj as EnvironmentGroup;
            if (group != null)
            {
                return this.Name.CompareTo(group.Name);
            }
            else
            {
                throw new ArgumentException("EnvironmentGroup object required for a comparison");
            }
        }

        #endregion
    }

    public enum EnvironmentType
    {
        Prod = 0,
        UAT= 1,
        QA = 2,
        DEV = 3,
        Others = 4
    }
}
