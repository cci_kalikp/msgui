﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace MSDesktop.Launcher.Model
{

    /// <summary>
    /// Represents a favorite override key/boolean
    /// </summary>
    [System.Xml.Serialization.XmlRoot("Favorite")]
    public class Favorite : IComparable, INotifyPropertyChanged
    {
        #region Private Member Variables
        private const string CLASS_NAME = "Favorite";
        private bool m_favorite = false;

        #endregion

        #region Constructors

        /// <summary>
        /// Default constructor
        /// NOTE: This is required by XMLSerializer
        /// </summary>
        public Favorite()
        {
        }

        /// <summary>
        /// The standard constructor for general usage
        /// </summary>
        /// <param name="key_">Unique key of the icon</param>
        /// <param name="favorite_">Flag if favorite or not</param>
        public Favorite(string key_, bool favorite_)
        {
            this.Key = key_;
            this.Value = favorite_;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// The key of the environment
        /// </summary>
        [XmlAttribute("iconId")]
        public string Key { get; set; }

        /// <summary>
        /// Flag indicating whether app should filter as a user favorite
        /// </summary>
        [XmlText]
        public bool Value
        {
            get
            {
                return m_favorite;
            }
            set
            {
                m_favorite = value;
                OnPropertyChanged("Value");
            }
        }

        /// <summary>
        /// Flag indicating whether app should filter as a user favorite
        /// </summary>
        [XmlIgnore]
        public bool? OriginalValue
        {
            get;
            set;
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Gets a XML string representing the object
        /// </summary>
        /// <returns></returns>
        public string ToXMLString()
        {
            return Utils.GetXMLString(this);
        }

        #endregion

        #region IComparable Members

        /// <summary>
        /// Compares two favorite objects using their keys
        /// </summary>
        /// <param name="obj">The object to compare with - must be of type Favorite.</param>
        /// <returns></returns>
        public int CompareTo(object obj)
        {
            Favorite favorite = obj as Favorite;
            if (favorite != null)
            {
                return this.Key.CompareTo(favorite.Key);
            }
            else
            {
                throw new ArgumentException("Favorite object required for a comparison");
            }
        }
        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName_)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName_));
            }
        }
        #endregion
    }
}
