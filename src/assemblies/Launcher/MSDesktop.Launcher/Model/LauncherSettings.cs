﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml.Serialization;
using MSDesktop.Launcher.UI;
using MorganStanley.Desktop.Loader.Configuration;
using MorganStanley.Desktop.Loader.Configuration.Helpers;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;  
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication;

namespace MSDesktop.Launcher.Model
{
    public class LauncherSettings
    {
 
        public LauncherSettings()
        {
            Title = Launcher.LauncherName; 
            HTTPServerPort = LauncherConsts.GetHTTPServerPort(true);
            StayAlive = true;

        }

        public bool SuppressCrossLauncher { get; set; } 
        public int HTTPServerPort { get; set; }
        public string CPSConfigFile { get; set; }
        
        public bool ForceModernUI { get; set; }

        public bool DefaultToModernUI { get; set; }

        public bool StayAlive { get; set; }
         
        public string Theme { get; set; }
         
        public string Title { get; set; }

        public string IconFile { get; set; }

        private ImageSource image;
        [XmlIgnore]
        public ImageSource Image
        {
            get { return image ?? (image = PathToImageConverter.ConvertToImage(IconFile) ?? Launcher.ApplicationIcon); }
        }
        private Icon icon;
        [XmlIgnore]
        public Icon Icon
        {
            get
            {
                if (icon == null)
                {
                    IconFile = PathUtilities.ReplaceEnvironmentVariables(IconFile);
                    if (!string.IsNullOrEmpty(IconFile) && File.Exists(IconFile))
                    {
                        icon = new Uri(IconFile).ToIcon(new PngBitmapEncoder());
                    }
                    else
                    {
                        icon = new Uri(@"pack://application:,,,/UI/Icons/ms_logo_16x16.ico", UriKind.RelativeOrAbsolute).ToIcon(
                            new PngBitmapEncoder());
                    }
                }
                return icon;
            }
        }
          

        public ModuleElements Modules { get; set; }

        public IsolatedSubsystems IsolatedSubsystems { get; set; }

        public EntitlementConfiguration Entitlement { get; set; }

        [XmlIgnore]
        public bool IsDefault { get; set; }

        [XmlIgnore]
        public bool AllowSingleMachine
        {
            get { return IsDefault && Launcher.AllowSingleMachine; }
        } 

        public MSDesktopConfiguration ConvertToConfiguration()
        {
            if ((IsolatedSubsystems != null && IsolatedSubsystems.Subsystems != null && IsolatedSubsystems.Subsystems.Count > 0) ||
                (Modules != null && Modules.Modules != null && Modules.Modules.Count > 0))
            {
                var config = new MSDesktopConfiguration(true);
                config.Modules = Modules;
                config.IsolatedSubsystems = IsolatedSubsystems;
                if (Entitlement != null)
                {
                    config.Entitlement = Entitlement;
                    config.Environment = new MorganStanley.Desktop.Loader.Configuration.Environment()
                        {
                            EnvironProviderId = EnvironProviderId.CommandLine,
                            ModernLabelsEnabled = true
                        };
                }
                return config;
            }
            return null;
        }
    }
}
