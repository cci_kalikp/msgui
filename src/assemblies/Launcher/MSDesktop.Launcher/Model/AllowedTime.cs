#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2006 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

              $Id: //eai/msdotnet/msgui/trunk/assemblies/Launcher/MSDesktop.Launcher/Model/AllowedTime.cs#2 $
  $DateTime: 2013/12/25 05:08:11 $
    $Change: 860054 $
    $Author: caijin $
_____________________________________________________________________________*/
#endregion File Info Header

using System;

namespace MSDesktop.Launcher.Model
{
	/// <summary>
	/// Represents an allowed time range during which an environment is available for use
	/// </summary>
	[System.Xml.Serialization.XmlRoot("AllowedTime")]
	public class AllowedTime 
	{
		#region Private Member Variables

		AllowedTimeEntry m_start;
		AllowedTimeEntry m_end;
		
		#endregion

		#region Constructors

		/// <summary>
		/// Default constructor
		/// NOTE: This is required by XMLSerializer
		/// </summary>
		public AllowedTime()
		{
		}
	
		/// <summary>
		/// Standard constructor for general usage 
		/// </summary>
		/// <param name="startDay_">The start day as a string (e.g. 'Monday')</param>
		/// <param name="startTime_">The start time as a string (e.g. '09:00')</param>
		/// <param name="endDay_">The end day as a string (e.g. 'Monday')</param>
		/// <param name="endTime_">The end time as a string (e.g. '18:00')</param>
		public AllowedTime(string startDay_, string startTime_, string endDay_, string endTime_)
		{
			this.Start = new AllowedTimeEntry(startDay_, startTime_);
			this.End = new AllowedTimeEntry(endDay_, endTime_);
		}

		#endregion

		#region Public Properties

		/// <summary>
		/// The start day and time
		/// </summary>
		public AllowedTimeEntry Start
		{
			get
			{
				return m_start;
			}
			set
			{
				m_start = value;
			}
		}

		/// <summary>
		/// The end day and time
		/// </summary>
		public AllowedTimeEntry End
		{
			get
			{
				return m_end;
			}
			set
			{
				m_end = value;
			}
		}
		
		#endregion
	}
}
