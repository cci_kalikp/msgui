﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace MSDesktop.Launcher.Model
{
    /// <summary>
    /// Represents an alternateverison
    /// </summary>
    [System.Xml.Serialization.XmlRoot("Impersonation")]
    public class AlternateVersion : IComparable, INotifyPropertyChanged
    {
        #region Private Member Variables
        private const string CLASS_NAME = "Impersonation";
        #endregion

        #region Constructors

        /// <summary>
        /// Default constructor
        /// NOTE: This is required by XMLSerializer
        /// </summary>
        public AlternateVersion()
        {
        }

        /// <summary>
        /// The standard constructor for general usage
        /// </summary>
        /// <param name="key_">Unique key of the icon</param>
        /// <param name="commandLineArgument_">If impersonation is true, the command line argument to pass in.</param>
        /// <param name="ldapGroup_">The ldapGroup that will trigger the alternate version if you are a member</param>
        public AlternateVersion(string key_, string ldapGroup_)
        {
            this.Key = key_;
            this.LdapGroup = ldapGroup_;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// The key of the environment
        /// </summary>
        [XmlAttribute("iconId")]
        public string Key
        {
            get;
            set;
        }

        /// <summary>
        /// The alternate version to run if appropriate
        /// </summary>
        [XmlText]
        public string Value
        {
            get;
            set;
        }

        /// <summary>
        /// LDAP group you should be in to trigger the alternate version
        /// </summary>
        [XmlAttribute("ldapGroup")]
        public string LdapGroup
        {
            get;
            set;
        }

        public string[] LdapGroupsSplit
        {
            get { return LdapGroup.Split(';'); }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Gets a XML string representing the object
        /// </summary>
        /// <returns></returns>
        public string ToXMLString()
        {
            return Utils.GetXMLString(this);
        }

        #endregion

        #region IComparable Members

        /// <summary>
        /// Compares two auto start objects using their keys
        /// </summary>
        /// <param name="obj">The object to compare with - must be of type AutoStart.</param>
        /// <returns></returns>
        public int CompareTo(object obj)
        {
            AlternateVersion autoStart = obj as AlternateVersion;
            if (autoStart != null)
            {
                return this.Key.CompareTo(autoStart.Key);
            }
            else
            {
                throw new ArgumentException("AlternateVersion object required for a comparison");
            }
        }
        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName_)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName_));
            }
        }
        #endregion
    }
}