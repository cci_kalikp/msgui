﻿namespace MSDesktop.Launcher
{
    public static class Consts
    {
        public const int StartProcessTimeout = 300000; //5 min
        public const int ProcessReadyTimeout = 120000; //2 min 
        public const int NetworkOverheadTime = 2000;//2 sec
        public const int EstimatedProcessStartupSeconds = 4; //4 sec
        public const int LongEstimatedProcessStartupSeconds = 15;//15 sec

    }
}
