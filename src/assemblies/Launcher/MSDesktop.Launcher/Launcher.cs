#region File Info header
/*_____________________________________________________________________________
  Copyright (c) 2006 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/Launcher/MSDesktop.Launcher/Launcher.cs#33 $
  $DateTime: 2014/10/29 02:23:15 $
    $Change: 902903 $
    $Author: caijin $
_____________________________________________________________________________*/
#endregion File Info header

using System;
using System.Text;
using System.Threading;
using System.IO;
using System.Windows.Forms;
using System.Windows.Media;
using System.Xml;
using Infragistics.Windows.OutlookBar;
using MSDesktop.HTTPServer;
using MSDesktop.Launcher.MessageDispatcher; 
using Microsoft.Practices.Unity;
using MSDesktop.Launcher.UI;
using MorganStanley.Desktop.Loader;
using MorganStanley.Desktop.Loader.Configuration;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.Shell;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.SmartApi;
using MorganStanley.MSDotNet.MSGui.Themes;
using Environment = System.Environment;


namespace MSDesktop.Launcher
{
    using System.Windows;

    /// <summary>
    /// Point Of entry into the Launcher.
    /// </summary>
    public class Launcher
    {
        #region public static state

        private const string CLASS_NAME = "Launcher";
        private const int MAX_LOG_DAYS = 20;

        public static readonly string DEFAULT_ROOT_FILE_NAME = @"\\san01b\DevAppsGML\dist\ied\PROJ\launcherconfig\prod\root.xml";


        internal static readonly ImageSource ApplicationIcon = new ImageSourceConverter().ConvertFromString("pack://application:,,,/MSDesktop.Launcher;component/UI/Icons/mslogo.ico") as ImageSource;
            //new BitmapImage(new Uri("pack://application:,,,/MSDesktop.Launcher;component/UI/Icons/mslogo.ico",  UriKind.Absolute));
          
        #endregion

        #region Private Member Variables

        internal static ApplicationController m_controller;
        public static bool UseModernUI { get; private set; }
        public static ApplicationController Controller
        {
            get { return m_controller; }
        }

        #endregion

        #region public static methods

        private static XamOutlookBar outlookBar;  // just have this here to trigger load of the assembly
        public static readonly string SystemLocation = System.Environment.GetEnvironmentVariable("SYS_LOC");
        public static readonly string ApplicationFolder = System.Environment.GetEnvironmentVariable("AppFolder");
        public static readonly string LogFolder =  System.Environment.GetEnvironmentVariable("LogFolder");
        public static readonly string UserFile = Path.Combine(ApplicationFolder, "Local.config");
        public static readonly string ExtraUserFile = Path.Combine(ApplicationFolder, "LocalEx.config");
        public static readonly string LogFile = Path.Combine(LogFolder, "Launcher.log");
        public static readonly string BackupFolder = Path.Combine(ApplicationFolder, "Backup");
        public static readonly string BackupFile = Path.Combine(BackupFolder, "Root.config");

        public static readonly bool AllowSingleMachine =
            System.Environment.GetEnvironmentVariable("AllowSingleMachine") == "True";

        public static string LauncherName;

        public static void StartLauncher(bool show_, string[] args_)
        {
            try
            {
                if (!string.IsNullOrEmpty(LauncherName))
                {
                    Thread.CurrentThread.Name = "MSDesktop.Launcher(" + LauncherName + ")";
                }
                else
                {
                    Thread.CurrentThread.Name = "MSDesktop.Launcher"; 
                }
                PurgeOldLogFiles(MAX_LOG_DAYS);

                string userName;
                string rootFile;
                bool attemptingImpersonation; 
                ParseParams(args_, out userName, out rootFile, out attemptingImpersonation);

                if (rootFile == null)
                {
                    rootFile = DEFAULT_ROOT_FILE_NAME;
                }

                m_controller = new ApplicationController(rootFile, userName, attemptingImpersonation);
                m_controller.ShuttingDown += m_controller_ShuttingDown;
                if (System.Windows.Application.Current == null) return;
                framework = new Framework(); 
                FilePersistenceStorage storage = new FilePersistenceStorage(m_controller.Settings.Title);
                storage.AlternativeFolder = ApplicationFolder;
                storage.BaseFolder = Path.Combine(@"U:\Application Data\Morgan Stanley", Environment.GetEnvironmentVariable("AppFolderBase"));
                framework.SetupPersistenceStorage(storage);
                framework.SetShellMode(ShellMode.LauncherBarAndFloatingWindows, true);
                framework.HideOrLockShellElements(HideLockUIGranularity.LockedLayoutMode |
                                                  HideLockUIGranularity.LockedLayoutSoftMode |
                                                  HideLockUIGranularity.LegacyLockedLayoutMode);
                framework.EnableNonOwnedWindows(); 
                framework.SetLayoutLoadStrategy(LayoutLoadStrategy.NeverAsk);

                if (!string.IsNullOrEmpty(m_controller.Settings.Theme))
                {
                    switch (m_controller.Settings.Theme.ToLower())
                    {
                        case "black":
                            framework.AddBlackTheme();
                            break;
                        case "blue":
                            framework.AddBlueTheme();
                            break;
                        case "simple":
                            framework.AddSimpleTheme();
                            break;
                        case "backgroundModifiedBlue":
                            framework.AddBackgroundModifiedBlueTheme();
                            break;
                        case "white":
                            framework.AddWhiteTheme();
                            break; 
                    }  
                    
                }

                UseModernUI = m_controller.Settings.ForceModernUI ||  m_controller.ExtraUserSettings.UseModernUI;
                if (UseModernUI)
                {
                    if (string.IsNullOrEmpty(m_controller.Settings.Theme) )
                    {
                        framework.AddWhiteTheme();
                    }

                    framework.SetAppBarTitle(m_controller.Settings.Title);

                    framework.AddModule<LauncherModule>();
                    framework.SetLauncherBarHeight(50, 100);
                    framework.SetLauncherBarButtonMaxHeight(30);
                    framework.EnableTypeaheadDropdownOptionsSearch();
                }
                else
                {
                    if (string.IsNullOrEmpty(m_controller.Settings.Theme))
                    {
                        framework.AddSimpleTheme();
                    }
                     
                    outlookBar = new XamOutlookBar(); 
                }

                //framework.SetApplicationIcon(m_controller.Settings.Icon);
                framework.SetApplicationIcon(m_controller.Settings.Image);
                framework.SetAppBarTitle(m_controller.Settings.Title);
                framework.SetApplicationName(LauncherName);
                if (m_controller.Settings.StayAlive)
                {
                    framework.AddModule<LauncherTrayIconModule>();               
                }
                 
                framework.EnableHTTPServer(m_controller.Settings.HTTPServerPort, LauncherConsts.EnableHash, LauncherConsts.HashSeed);
                framework.SetCpsConfigFile(m_controller.Settings.CPSConfigFile);
                framework.SetImcCommunicationStatus(m_controller.Settings.SuppressCrossLauncher ? CommunicationChannelStatus.Disabled : CommunicationChannelStatus.Optional);
                framework.EnableMessageRoutingCommunication(true);
                framework.EnableCrossLauncherMessage("You can now close this window");
                framework.DisableSplashScreen();
                framework.HideLayoutNameOnLauncherBar();
                framework.EnableSmartApi();
                if (!show_) show_ = !Controller.Settings.StayAlive;
                if (!show_)
                {
                    framework.SetInvisibleMainControllerMode(ShellModeExtension.InvisibleMainControllerModeEnum.HideWindow);
                }
                MSDesktopConfiguration config = Controller.Settings.ConvertToConfiguration();
                if (config == null)
                {
                    framework.Start();
                }
                else
                {
                    StartUp.Start(framework, config);
                }
                if (show_)
                {
                    ShowMainWindow();
                }
            }
            catch (Exception ex)
            {
                ExceptionViewer.ShowFatalException("Failed during startup.", ex);
            }
        }


        static void m_controller_ShuttingDown(object sender, System.ComponentModel.CancelEventArgs e)
        {
            ShutDown(); 
        }

        private static Framework framework;
        private static MainWindow mainWindow;
        private static IShell shell;
        private static bool closingInternally = false;
        private static bool isTaskBar = false;
        private static void EnsureShell()
        {
            if (shell == null)
            {
                shell = framework.Container.Resolve<IShell>();
                shell.OnClosing = () =>
                {
                    if (m_controller.Settings.StayAlive && !closingInternally)
                    {
                        framework.SetInvisibleMainControllerMode(
                            ShellModeExtension.InvisibleMainControllerModeEnum.HideWindow); 
                        shell.MainWindow.Hide();
                        return false;
                    }
                    return true;
                };
                BarWindow bar = shell.MainWindow as BarWindow;
                if (bar != null)
                {
                    bar.TaskbarStateChanged += new EventHandler(bar_TaskbarStateChanged); 
                }

            }
        }


        public static Window MainWindow
        {
            get
            {
                if (UseModernUI)
                {
                    EnsureShell();
                    return shell.MainWindow;
                }
                else
                {
                    return mainWindow;
                } 
            }
        }
        public static void ShowMainWindow()
        {
            if (UseModernUI)
            {
                framework.SetInvisibleMainControllerMode(ShellModeExtension.InvisibleMainControllerModeEnum.None);
                EnsureShell();
                BarWindow bar = shell.MainWindow as BarWindow;
                if (bar != null && isTaskBar != bar.IsTaskbar)
                {
                    bar.SetDock(isTaskBar); 
                } 
                shell.Show();
                ResizeBar();
                shell.MainWindow.Activate();
            }
            else
            {
                if (mainWindow == null)
                {
                    mainWindow = new MainWindow(m_controller);
                    mainWindow.Closed += new EventHandler(mainWindow_Closed);
                }
                mainWindow.Show();
                mainWindow.Activate();
            }

        }

        private static void ResizeBar()
        {
            BarWindow bar = shell.MainWindow as BarWindow;
            if (bar != null)
            {
                if (bar.IsTaskbar)
                {
                    bar.ClearValue(FrameworkElement.MaxWidthProperty);
                }
                else
                {
                    if (bar.Width > Screen.PrimaryScreen.WorkingArea.Width - 15)
                    {
                        bar.Left = 7;
                    }
                    bar.MaxWidth = Screen.PrimaryScreen.WorkingArea.Width - 15;
                }
                isTaskBar = bar.IsTaskbar;
            }
        }
        static void bar_TaskbarStateChanged(object sender, EventArgs e)
        {
            //happened when closed
            if (!shell.MainWindow.IsVisible) return;
            
            ResizeBar();
        }

        private static bool exited = false;
        public static void ShutDown()
        {
            if (mainWindow != null)
            {
                if (mainWindow.CheckAccess())
                {
                    mainWindow.Close();
                }
                else
                {
                    mainWindow.Dispatcher.Invoke(new Action(mainWindow.Close));
                }
                mainWindow = null;
            }
            EnsureShell();
            if (shell != null)
            {
                closingInternally = true;
                try
                {
                    BarWindow bar = shell.MainWindow as BarWindow;
                    if (bar != null)
                    {
                        bar.TaskbarStateChanged -= new EventHandler(bar_TaskbarStateChanged);
                    }

                    if (shell.MainWindow.CheckAccess())
                    {
                        shell.Close();
                    }
                    else
                    {
                        shell.MainWindow.Dispatcher.Invoke(new Action(shell.Close));
                    }
                }
                finally
                {
                    closingInternally = false;
                }

            }

        }

        static void mainWindow_Closed(object sender, EventArgs e)
        { 
            (sender as Window).Closed -= new EventHandler(mainWindow_Closed);
            mainWindow = null;
            if (!Launcher.Controller.Settings.StayAlive)
            {
                ShutDown();
            }
        }

        /// <summary>
        /// Tries to parse parameters passed into the process from command line.
        /// Using out params for Username, RootFileName and scheduler config directory  
        /// </summary>
        /// <param name="args_">Arguments from cmd line </param>
        /// <param name="userName_">Out param for UserName</param>
        /// <param name="rootFileName_">Out param for RootFileName</param>
        public static void ParseParams(string[] args_, out string userName_, out string rootFileName_,
                                       out bool attemptingImpersonation_)
        { 
            userName_ = System.Environment.UserName.ToLower();
                //If you log in to Windows with upper case, then it will send that to Eclipse, where everything is lower case.
            rootFileName_ = null; 
            attemptingImpersonation_ = false;

            for (int i = 0; i < args_.Length; i++)
            {
                if (System.String.Compare(args_[i], "-i", System.StringComparison.OrdinalIgnoreCase) == 0 ||
                    System.String.Compare(args_[i], "/i", System.StringComparison.OrdinalIgnoreCase) == 0)
                {
                    attemptingImpersonation_ = true;
                    if (args_.Length > i)
                    {
                        //Set to next next argument and increment it
                        userName_ = args_[++i];
                    }
                } 
                else
                {
                    rootFileName_ = PathUtilities.ReplaceEnvironmentVariables(args_[i]);
                }
            }
        }

        private static void PurgeOldLogFiles(int olderThanDays)
        { 
            if (olderThanDays > 0 && Directory.Exists(LogFolder))
            {
                DateTime cutOffDate = DateTime.Now.Subtract(new TimeSpan(olderThanDays, 0, 0, 0));
                LogLayer.Debug.Location(CLASS_NAME, "PurgeOldLogFiles()")
                        .Append("Deleting files dated older than ", cutOffDate.ToString("yyyy-MM-dd"))
                        .Send();
                DirectoryInfo logDirInfo = new DirectoryInfo(LogFolder);

                foreach (FileInfo logFile in logDirInfo.GetFiles("*.log"))
                {
                    if (logFile.CreationTime.Date <= cutOffDate.Date)
                    {
                        try
                        {
                            logFile.Delete();
                        }
                        catch (Exception ex_)
                        {
                            LogLayer.Debug.Location(CLASS_NAME, "PurgeOldLogFiles()")
                                    .Append("Cannot delete ", logFile.ToString(), " file, ", ex_.ToString())
                                    .Send();
                        }
                    }
                }
            }
        } 

        /// <summary>
        /// Loads XMLDocument from config file specified in BACKUP_DIR Directory
        /// </summary>
        /// <param name="state_"></param>
        /// <param name="fileName_"></param>
        public static XmlDocument GetConfigDoc(string fileName_)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(Path.Combine(Launcher.BackupFile, fileName_));
            return doc;
        }

        /// <summary>
        /// Writes Xml state to file specified in a BACKUP Folder
        /// </summary>
        /// <param name="state_"></param>
        /// <param name="fileName_"></param>
        public static void BackupXMLConfig(XmlNode state_, string fileName_)
        {
            EnsureDirectoryExists(BackupFolder);

            XmlTextWriter xw = new XmlTextWriter(Path.Combine(BackupFolder, fileName_), Encoding.UTF8);
            xw.Formatting = Formatting.Indented;
            state_.WriteContentTo(xw);

            xw.Flush();
            xw.Close();
        }

        internal static void EnsureDirectoryExists(string dirName_)
        {
            if (!Directory.Exists(dirName_))
            {
                Directory.CreateDirectory(dirName_);
            }
        }
        #endregion
    }

}
