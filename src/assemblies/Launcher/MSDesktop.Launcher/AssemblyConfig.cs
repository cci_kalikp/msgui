using System.Reflection; 
using System.Resources; 
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication;

[assembly: AssemblyTitle(LauncherConsts.DefaultApplicationName)]
[assembly: AssemblyDescription(LauncherConsts.DefaultApplicationName + " Library")]
[assembly: AssemblyConfiguration("")]

//[assembly: AssemblyCompany("Morgan Stanley")]
[assembly: AssemblyProduct(LauncherConsts.DefaultApplicationName)]
[assembly: AssemblyCopyright("Copyright � Morgan Stanley")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: NeutralResourcesLanguageAttribute("en")] 

[assembly: AssemblyDelaySign(false)]
//[assembly: AssemblyKeyFile(@"\\ms\dev\msdotnet\msdotnet\incr\src\Assemblies\MSDotNet\StrongName.snk")]
[assembly: AssemblyKeyName("")]
