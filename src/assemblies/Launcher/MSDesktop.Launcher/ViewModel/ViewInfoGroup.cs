using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using MSDesktop.Launcher.Model;

namespace MSDesktop.Launcher.UI
{
    /// <summary>
    /// This class is used to group together groups of icons.  e.g. an application group containing lists of icons
    /// broken up by region.  Or an environment group containing lists of icons broken up by  
    /// </summary>
    public class ViewInfoGroup
    {
        internal const string FavoriteGroupName = "Favorites";
        internal static readonly string FavoriteIconPath = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), @"UI\Icons\favorites_32.png");

        public string Name { get; set; }
        public string IconPath { get; set; }
        public IList<ViewInfoIconGroup> ViewInfoIconList { get; set; }
        private bool Equals(ViewInfoGroup other)
        {
            return this.Name == other.Name && this.IconPath == other.IconPath;
        }

        public bool IsFavoritesGroup
        {
            get { return Name == FavoriteGroupName; }
        }

        public ViewInfoGroup(bool isFavorites_ = false)
        {
            if (isFavorites_)
            {
                IconPath = FavoriteIconPath;
                Name = FavoriteGroupName;
            }
        }
        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = (Name != null ? Name.GetHashCode() : 0); 
                hashCode = (hashCode * 397) ^ (IconPath != null ? IconPath.GetHashCode() : 0);
                return hashCode;
            } 
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((ViewInfoGroup)obj);
        }
    }


    public class EnvironmentGroupNameComparer:IComparer<string>
    {
        public int Compare(string x, string y)
        {
            if (x == y) return 0;
            EnvironmentType typeX = EnvironmentGroup.GetEnvironmentTypeForSort(x);
            EnvironmentType typeY = EnvironmentGroup.GetEnvironmentTypeForSort(y);
            return EnvironmentGroupComparer.Compare(x, typeX, y, typeY);
        }
    }

    public class EnvironmentGroupComparer : IComparer<EnvironmentGroup>
    {
        private static readonly Dictionary<EnvironmentType, IList<string>> sortOrders = new Dictionary<EnvironmentType, IList<string>>()
            {
                {
                    EnvironmentType.Prod,
                    //reverse the order
                    new Stack<string>( 
                        new List<string>
                            {
                                "Production",
                                "Prod",
                                "PROD",
                                "Production64bit",
                                "Production(Readonly)",
                                "Previous",
                                "Prod Pilot",
                                "ProdPilot",
                                "Pilot",
                                "ProdAlpha",
                                "ProdBeta",
                                "Prod Beta",
                                "Beta",
                                "ProdSim",
                                "Simulator",
                                "Sim"
                            }).ToArray()
                },
                {
                    EnvironmentType.UAT,
                    new Stack<string>( 
                        new List<string>
                            {
                                "UAT", 
                                "Uat"
                            }).ToArray()
                },
                {
                    EnvironmentType.QA,
                    new Stack<string>( 
                        new List<string>
                            {
                                "QA", 
                                "Next Gen"
                            }).ToArray()
                },
                {
                    EnvironmentType.DEV,
                    new Stack<string>( 
                        new List<string>
                            {
                                "Dev",
                                "DEV",
                                //this is a special group, which should belong to Prod environment type logically, but should be sorted as low as a dev env group   
                                "Support",                              
                                "Daily",
                                "UAT"
                            }).ToArray()
                }
            };

        public static int Compare(string x, EnvironmentType typeX, string y, EnvironmentType typeY)
        {
            int result = typeX - typeY;
            if (result != 0) return result;
            IList<string> sortOrder;
            if (sortOrders.TryGetValue(typeX, out sortOrder))
            {
                int indexEnv1 = sortOrder.IndexOf(x);
                int indexEnv2 = sortOrder.IndexOf(y);
                //higher index means higher priority, since we reversed the sort orders,
                if (indexEnv1 != indexEnv2) return indexEnv2 - indexEnv1;

            }
            return String.Compare(x, y, StringComparison.Ordinal); //do a simple alpha numeric sort as the last resort

        }

        public int Compare(EnvironmentGroup x, EnvironmentGroup y)
        {

            if (x == null && y != null) return 1;
            if (y == null && x != null) return -1;
            if (y == null && x == null) return 0;
            return Compare(x.Name, x.EnvironmentTypeForSort, y.Name, y.EnvironmentTypeForSort);
         }
    }

     
}