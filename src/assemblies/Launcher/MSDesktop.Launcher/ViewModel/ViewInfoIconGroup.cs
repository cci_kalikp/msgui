using System.Collections.Generic;
using System.Windows;
using MSDesktop.Launcher.Model;

namespace MSDesktop.Launcher.UI
{
    /// <summary>
    /// This class is used to group a set of icons together.  e.g. all for a specific application or
    /// all for a specific environment group
    /// </summary>
    public class ViewInfoIconGroup
    {
        /// <summary>
        /// Application Name
        /// </summary>
        public string ViewInfoGroupName { get; set; } 
        public EnvironmentType EnvironmentType { get; set; }
        public string Name { get; set; }

        public string IconPath { get; set; }
        private bool _isExpanded;
        public bool IsExpanded
        {
            get { return _isExpanded; }
            set
            {
                _isExpanded = value;
                ExpandedChanged();
            }
        }

        public IList<ViewInfo> ViewInfoList { get; set; }

        public event RoutedEventHandler IsExpandedChanged;

        private void ExpandedChanged()
        {
            if (IsExpandedChanged != null)
            {
                IsExpandedChanged(this, null);
            }
        }


        private bool Equals(ViewInfoIconGroup other)
        {
            return this.Name == other.Name && this.ViewInfoGroupName == other.ViewInfoGroupName && IconPath == other.IconPath;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = (ViewInfoGroupName != null ? ViewInfoGroupName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Name != null ? Name.GetHashCode() : 0); 
                hashCode = (hashCode * 397) ^ (IconPath != null ? IconPath.GetHashCode() : 0);
                return hashCode;
            }
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((ViewInfoIconGroup)obj);
        }
    }
}