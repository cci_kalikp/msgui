#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2006 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

              $Id: //eai/msdotnet/msgui/trunk/assemblies/Launcher/MSDesktop.Launcher/ViewModel/ViewInfo.cs#22 $
  $DateTime: 2014/07/28 05:34:31 $
    $Change: 890260 $
    $Author: caijin $
_____________________________________________________________________________*/
#endregion File Info Header

using System.ComponentModel;
using System.IO;
using System.Linq;
using System;
using System.Collections;
using System.Collections.Generic; 
using System.Globalization;
using System.Threading;
using System.Windows; 
using System.Windows.Input;
using MSDesktop.Launcher.Model; 
using Microsoft.Practices.Composite.Presentation.Commands;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;

namespace MSDesktop.Launcher.UI
{

    /// <summary>
    /// Represents a flattened view of the available icons.
    /// [Used to isolate the view layers from changes to the underlying root model]
    /// </summary> 
    public class ViewInfo : ViewModelBase, ICloneable
    {
        #region Private Member Variables

        private string m_appDescription = String.Empty;
        private string m_meta = String.Empty;
        private string m_project = String.Empty;
        private string m_appID = string.Empty;
        private string m_appIconPath = String.Empty;
        private Model.EnvironmentGroup m_environmentGroup;
        private Model.Environment m_environ = null;
        private string m_arguments = String.Empty;
        private Model.Application m_application;
        private string m_timeGroup = String.Empty;
        private SysLoc[] m_sysLocs;
        private AutoStart m_autoStart;
        private Impersonation m_impersonation;
        private string m_uniqueKey = String.Empty;
        private IEnumerable<ViewInfo> m_ensemble;
        private Services.Configuration m_config;
        private Favorite m_favorite; 
        #endregion
        #region Consts
        private static readonly string CLASS = "ViewInfo";
        #endregion Consts

        #region Constructors 
        public ViewInfo(Model.Application app_, EnvironmentGroup group_, Model.Environment env_, ActiveRelease release_, bool usingAlternateVersion_, Services.Configuration config_, IEnumerable<ViewInfo> ensemble_)
        {
            this.IsHidden = (app_ != null && app_.IsHidden) || (group_ != null && group_.IsHidden) || (env_ != null && env_.IsHidden);

            m_application = app_;
            m_appDescription = app_.Description;
            m_meta = app_.Meta;
            m_project = app_.Project;
            m_appID = app_.Description;
            m_appIconPath = app_.IconFile;
            MessageDispatchFailMessageOverride = app_.MessageDispatchFailMessageOverride;
            MessageDispatchSucceedMessageOverride = app_.MessageDispatchSucceedMessageOverride;
            MessageDispatchFailHtml = app_.MessageDispatchFailHtml;
            MessageDispatchSucceedHtml = app_.MessageDispatchSucceedHtml;
            if (release_ != null)
            {
                EstimatedStartupSeconds = release_.EstimatedStartupSeconds;          
            }
            if (EstimatedStartupSeconds == 0)
            {
                EstimatedStartupSeconds = app_.EstimatedStartupSeconds;
            }
            if (EstimatedStartupSeconds == 0)
            {
                EstimatedStartupSeconds = Consts.EstimatedProcessStartupSeconds;
            }
            m_environmentGroup = group_;
            m_environ = env_;
            Release = release_;
            m_ensemble = ensemble_;
            m_config = config_;
            if (env_ != null)
            {
                m_arguments = env_.Arguments;
                Mailgroups = env_.MailgroupString;
                m_timeGroup = env_.AllowedTimeGroup;
                //allow semi colon separated sys locs. Mostly to cover non japan asia.
                //e.g. <SysLoc>*.hk.as;*.tw.as;*.*.au</SysLoc>
                ArrayList list = new ArrayList();
                if (env_.SysLoc != null)
                {
                    DisplaySysLocs = env_.SysLoc;
                    string[] sysLocs = env_.SysLoc.Split(';');
                    foreach (string sysLocString in sysLocs)
                    {
                        string sysLocStr = sysLocString;
                        if (string.IsNullOrEmpty(sysLocStr))
                        {
                            //default to *.*.*
                            sysLocStr = "*.*.*";
                        }
                        SysLoc sysLoc = new SysLoc(sysLocStr);
                        list.Add(sysLoc);
                    }
                    m_sysLocs = (SysLoc[])list.ToArray(typeof(SysLoc));
                }
                if (env_.AutoStart != null)
                {
                    AutoStart = env_.AutoStart;
                    if (AutoStart.Key != null)
                    {
                        m_uniqueKey = AutoStart.Key;
                    }
                }
                if (env_.Impersonation != null)
                {
                    Impersonation = env_.Impersonation;
                }
                if (env_.Favorite != null)
                {
                    Favorite = env_.Favorite;
                }
            }
            UsingAlternateVersion = usingAlternateVersion_;
            StartProcessCommand = new DelegateCommand<object>(new Action<object>(o_ => Launcher.Controller.StartProcess(this)));
            ToggleAutoStartCommand = new DelegateCommand<object>(new Action<object>(o_ => Launcher.Controller.AddAutoStartOverride(this, !AutoStartValue)));
            ToggleImpersonationCommand = new DelegateCommand<object>(new Action<object>(o_ => Launcher.Controller.AddImpersonationOverride(this, !ImpersonationValue)));
            ToggleFavoriteCommand = new DelegateCommand(o_ => Launcher.Controller.AddFavoriteOverride(this, !this.FavoriteValue));
            DeleteLocalCacheCommand = new DelegateCommand<object>(new Action<object>(o_ =>
            {
                bool confirmed = false;
                string message = "This will delete the local copy of this application copied from AFS." +
                                 System.Environment.NewLine +
                                 "If you proceed, any running instance will be killed in order to delete the files";
                const string title = "Clear Local Cache";
                if (!Launcher.UseModernUI)
                {
                    MessageBoxResult result = MessageBox.Show(message, title, MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.Yes);

                    confirmed = (result == MessageBoxResult.Yes);
                }
                else
                {
                    var result = TaskDialog.Show(new TaskDialogOptions()
                    {
                        Title = title,
                        Content = message,
                        MainIcon = VistaTaskDialogIcon.Warning,
                        CommonButtons = TaskDialogCommonButtons.YesNo,
                        DefaultButtonIndex = 0,
                        ForceNullOwner = true
                    });
                    confirmed = result.Result == TaskDialogSimpleResult.Yes;
                }
                if (confirmed)
                {
                    Launcher.Controller.DeleteLocalCache(this);
                }

            }));
        }

  
        #endregion

        #region Public Properties 
 
        public static readonly string CopyLocalRootPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.LocalApplicationData)
                + @"\Morgan Stanley\";
         
        /// <summary>
        /// returns Icon FileName
        /// </summary>
        public string IconFileName
        {
            get
            {
                if (!string.IsNullOrEmpty(m_environ.IconFile)) return m_environ.IconFile;
                if (!string.IsNullOrEmpty(Release.IconFile)) return Release.IconFile;
                return @"\\san01b\DevAppsGML\dist\ied\PROJ\launcher\resources\MS.ico";

            }
        }

        /// <summary>
        /// A description of the application
        /// </summary>
        public string ApplicationDescription
        {
            get
            {
                return m_appDescription;
            }
        }

        public bool IsHidden { get; private set; }
        private string GenerateIconDescription(bool bIncludeFavoriteTag)
        {
            string displayValue = String.Empty;
            if (AutoStartValue)
            {
                displayValue = "Auto Start";
            }

            if (bIncludeFavoriteTag && FavoriteValue)
            {
                if (displayValue != String.Empty)
                {
                    // we are already auto starting so add the comma
                    displayValue += ", ";
                }

                displayValue += "Favorite";
            }

            if (ImpersonationValue)
            {
                if (displayValue != String.Empty)
                {
                    // we are already auto starting so add the comma
                    displayValue += ", ";
                }

                displayValue += "Support Access";
            }

            if (UsingAlternateVersion)
            {
                if (displayValue != String.Empty)
                {
                    // we are already showing something else so add the comma
                    displayValue += ", ";
                }

                // just show the alternate release link for now.
                displayValue += Release.Version;
            }

            if (displayValue != string.Empty)
            {
                // wrap it in new line and brackets
                displayValue = System.Environment.NewLine + "(" + displayValue + ")";
            }

            return (EnvironmentName != null ? EnvironmentName + displayValue : String.Empty);
        }
        /// <summary>
        /// Describes the icon by environment and whether the icon auto starts.
        /// </summary>
        public string IconDescription
        {
            get
            {
                return GenerateIconDescription(false);
            }
        }

        /// <summary>
        /// Describes the icon by environment and whether the icon auto starts and includes a tag if the item is a user favorite.
        /// </summary>
        public string IconDescriptionWithFavoriteIndicator
        {
            get
            {
                return GenerateIconDescription(true);
            }
        }
         
        /// <summary>
        /// A combination of the application description and environment name
        /// </summary>
        public string ApplicationAndIconDescription
        {
            get { return ApplicationDescription + (!String.IsNullOrEmpty(IconDescription) ? System.Environment.NewLine + IconDescription : String.Empty); }
        }

        /// <summary>
        /// A combination of the application description and environment name used for Windows Shortcuts
        /// </summary>
        public string ShortcutDescription
        {
            get
            {
                return String.Format("{0} - [{1}]", m_appDescription, EnvironmentName);
            }
        }

        /// <summary>
        /// The name group in which this environment is permissioned
        /// </summary>
        public string EnvironmentGroupName
        {
            get
            {
                return m_environmentGroup == null ? string.Empty : m_environmentGroup.Name;
            }
        }

        /// <summary>
        /// The name of the environment
        /// </summary>
        public string EnvironmentName
        {
            get
            {
                return (m_environ != null ? m_environ.Name : string.Empty);
            }
        }

        internal Model.Environment Environment { get { return m_environ; } }

        public string EnvironmentType
        {
            get { return m_environmentGroup == null ? string.Empty : m_environmentGroup.EnvironmentType.ToString(); }
        }

        public EnvironmentGroup EnvironmentGroup
        {
            get { return m_environmentGroup; }
        }
        /// <summary>
        /// The working directory to use
        /// </summary>
        public string WorkingDirectory
        {
            get
            {
                if (Release.CodeLocation == IEDCodeLocation.COPYLOCAL)
                {
                    return GetPath(IEDCodeLocation.COPYLOCAL);
                }
                else
                {
                    return Release.WorkingDirectory;
                }
            }
        }

        /// <summary>
        /// The command line arguments to use
        /// </summary>
        public string Arguments
        {
            get
            {
                string arguments;
                if (Impersonation != null && Impersonation.Value == true)
                {
                    arguments = (String.IsNullOrEmpty(m_arguments)) ? m_arguments : m_arguments + " ";  // add a space if there are already some arguments.
                    arguments += Impersonation.CommandLineArgument;
                }
                else
                {
                    arguments = m_arguments;
                }
                if (!string.IsNullOrEmpty(ExtraArguments))
                {
                    arguments += String.IsNullOrEmpty(arguments) ? ExtraArguments : (" " + ExtraArguments);
                }
                return arguments;
            }
        }

        public string ExtraArguments { get; set; }

        /// <summary>
        /// The meta name (business area)
        /// </summary>
        public string Meta
        {
            get
            {
                return m_meta;
            }
        }

        /// <summary>
        /// The project name
        /// </summary>
        public string Project
        {
            get
            {
                return m_project;
            }
        }

        /// <summary>
        /// Returns the release version number
        /// </summary>
        public string Version
        {
            get
            {
                return Release.Version;
            }
        }

        /// <summary>
        /// Returns the path to the app icon 
        /// </summary>
        public string AppIconPath
        {
            get
            {
                return m_appIconPath;
            }
        }

        /// <summary>
        /// Returns the type of filesystem in use.
        /// [AFS or WINDIST]
        /// </summary>
        public IEDCodeLocation FileSystem
        {
            get
            {
                return Release.CodeLocation;
            }
        }

        /// <summary>
        /// Returns the name of the time group specifying the system availability
        /// </summary>
        public string TimeGroup
        {
            get
            {
                return m_timeGroup;
            }
        }

        /// <summary>
        /// Returns the location in which the environment is run
        /// </summary>
        public SysLoc[] SysLocs
        {
            get
            {
                return m_sysLocs;
            }
        }

        /// <summary>
        /// Sys Locs for display
        /// </summary>
        public string DisplaySysLocs { get; private set; }

        public string Mailgroups
        {
            get;
            private set;
        }

        public string CompleteFilename
        {
            get
            {
                string path = null;
                switch (Release.CodeLocation)
                {
                    case IEDCodeLocation.DIRECT:
                        path = Release.Executable;
                        break;
                    default:
                        path = GetPath(Release.CodeLocation) + Release.Executable;
                        break;
                }
                return PathUtilities.ReplaceEnvironmentVariables(path);
            }
        }

        /// <summary>
        /// Returns a unique key for the icon.
        /// [{Meta}.{Project}.{EnvironmentName}]
        /// </summary>
        public string UniqueKey
        {
            get
            {
                if (m_uniqueKey != String.Empty)
                {
                    return m_uniqueKey;
                }
                else
                {
                    return UniqueKeyCalculated;
                }
            }
        }

        public string UniqueKeyCalculated
        {
            get
            {
                if (m_environmentGroup == null)
                {
                    return string.Format("{0}.{1}.{2}.{3}", m_meta, m_project, Release.Version, Release.Executable.Replace(".exe", "") + m_arguments);
                }
                else
                {
                    return String.Format("{0}.{1}.{2}.{3}", m_meta, m_project, m_environmentGroup.Name, EnvironmentName);
                }
            }
        }

        /// <summary>
        /// returns the application key for indexing into the hash table <see cref="Model.Application.UniqueKey"/>
        /// </summary>
        public string ApplicationKey
        {
            get
            { 
                return m_application.UniqueKey;
            }
        }
         
        /// <summary>
        /// Returns the tooltip for the icon.
        /// </summary>
        public string ToolTip
        {
            get
            {
                string mailgroups = this.Mailgroups;
                // make multiple mailgroups easier to read in the tool tip
                if (mailgroups.Contains(';'))
                {
                    mailgroups = System.Environment.NewLine + "  " + mailgroups.Replace(";", System.Environment.NewLine + "  ").TrimEnd();
                }
                return String.Format("Executable: {0}{4}Arguments: {1}{4}UniqueKey: {2}{4}Mailgroup(s): {3}", this.CompleteFilename, this.Arguments, this.UniqueKey, mailgroups, System.Environment.NewLine);
            }
        }

        /// <summary>
        /// Gets the filename for the package that is downloaded and uncompressed locally
        /// </summary>
        public string Package
        {
            get { return Release.Package; }
        }

        /// <summary>
        /// Gets the filename for the package that is downloaded and uncompressed locally
        /// </summary>
        public string PackageName
        {
            get
            {
                try
                {
                    return IsPackageAbsolutePath
                             ? Package.Split(new[] { '\\', '/' }, StringSplitOptions.RemoveEmptyEntries).LastOrDefault()
                             : Package;
                }
                catch (Exception ex)
                {
                    LogLayer.Warning.Location(CLASS, "PackageName")
                            .Append(string.Format("Viewinfo {0} for {1} has invalid package name ({2})! {3} , {4}", UniqueKey,
                                                  m_environ.Name, Package ?? "null package name", System.Environment.NewLine, ex.Message));
                    return Package;
                }
            }
        }

        public bool IsPackageAbsolutePath
        {
            get
            {
                return Release.CodeLocation == IEDCodeLocation.COPYLOCALDIRECT;
            }
        }
        /// <summary>
        /// Returns actual Process ImageName - Loader.exe
        /// </summary>
        public string ImageName
        {
            get { return Release.ImageName; }
        }

        /// <summary>
        /// Indicates whether the app will autostart
        /// </summary>
        public AutoStart AutoStart
        {
            get { return m_autoStart; }
            set
            {
                if (m_autoStart != null)
                {
                    m_autoStart.PropertyChanged -= userOverride_PropertyChanged;
                }

                m_autoStart = value;
                m_autoStart.PropertyChanged += userOverride_PropertyChanged;
            }
        }

        public bool AutoStartValue
        {
            get { return m_autoStart != null && m_autoStart.Value; }  
        }

        /// <summary>
        /// Indicates whether the app will start with impersonation
        /// </summary>
        public Impersonation Impersonation
        {
            get { return m_impersonation; }
            set
            {
                if (m_impersonation != null)
                {
                    m_impersonation.PropertyChanged -= userOverride_PropertyChanged;
                }

                m_impersonation = value;
                m_impersonation.PropertyChanged += userOverride_PropertyChanged;
            }
        }


        public bool ImpersonationValue
        {
            get { return m_impersonation != null && m_impersonation.Value; }
        }

        /// <summary>
        /// Indicates whether the app will show as a favorite
        /// </summary>
        public Favorite Favorite
        {
            get { return m_favorite; }
            set
            {
                if (m_favorite != null)
                {
                    m_favorite.PropertyChanged -= userOverride_PropertyChanged;
                }

                m_favorite = value;
                m_favorite.PropertyChanged += userOverride_PropertyChanged;
            }
        }

        public bool FavoriteValue
        {
            get { return m_favorite != null && m_favorite.Value; }
        }

        public bool IsCopyLocal
        {
            get { return FileSystem == IEDCodeLocation.COPYLOCAL; }
        }

        /// <summary>
        /// Depends Relation based on the Environment's DepensOn relationship - <see cref="Model.Environment.DependsOn"/>
        /// </summary>
        /// <remarks>
        /// DependsOn are dynamic property, only those allowed to run and are dependencies to current main ViewInfo shall be returned
        /// CAVEAT: returned list may contain ViewInfo which has already started, filter is required!
        /// </remarks>
        public IEnumerable<ViewInfo> DependsOn
        {
            get
            {
                return GetRunnableViewInfoDepends(this);
            }
        }

        public string MessageDispatchFailMessageOverride { get; private set; }
        public string MessageDispatchSucceedMessageOverride { get; private set; }
        public string MessageDispatchFailHtml { get; private set; }
        public string MessageDispatchSucceedHtml { get; private set; }
        public int EstimatedStartupSeconds { get; set; }

        private IEnumerable<ViewInfo> GetRunnableViewInfoDepends(ViewInfo viewinfo)
        {
            List<ViewInfo> closure = new List<ViewInfo> { };

            if (viewinfo != null && viewinfo.m_environ != null && viewinfo.m_environ.DependsOn != null && m_ensemble != null)
            {
                foreach (var depend in viewinfo.m_environ.DependsOn)
                {
                    var dep_viewinfo = m_ensemble.FirstOrDefault(v => v.EnvironmentName == depend);

                    if (dep_viewinfo != null)
                    {
                        closure.AddRange(GetRunnableViewInfoDepends(dep_viewinfo));
                        if (dep_viewinfo.TimeWindowAllowed())
                        {
                            // the list may contains the ViewInfo which may have already started.
                            closure.Add(dep_viewinfo);
                        }
                        else
                        {
                            LogLayer.Warning.Location(CLASS, "GetRunnableViewInfoDepends").Append(string.Format("Viewinfo {0} for {1} depends on {2}, not allowed to run in the time window!", viewinfo.UniqueKey, viewinfo.m_environ.Name, depend));
                        }
                    }
                    else
                    {
                        LogLayer.Warning.Location(CLASS, "GetRunnableViewInfoDepends").Append(string.Format("Viewinfo {0} for {1} depends on {2}, not Entitled to run!", viewinfo.UniqueKey, viewinfo.m_environ.Name, depend));
                    }
                }
            }
            return closure;
        }


        /// <summary>
        /// Is <paramref name="viewinfo"/>'s time window allowed to run?
        /// </summary>
        /// <param name="viewinfo"></param>
        private bool TimeWindowAllowed()
        {
            TimeSpan timeNow = DateTime.Now.Subtract(DateTime.Today);
            //var environ = m_environDict[viewinfo.EnvironmentName];
            var environ = m_environ;
            CultureInfo dtParsingCulture = Thread.CurrentThread.CurrentCulture;

            AllowedTimeGroup allowedTimeGroup = null;
            if (m_config == null) return false;
            var app = m_config.Root.GetApplicationByKey(ApplicationKey);
            if (app != null)
            {
                allowedTimeGroup = app.GetTimeGroup(TimeGroup);
                if (allowedTimeGroup == null)
                {
                    throw new ArgumentOutOfRangeException(string.Format("AllowedTimeGroup {0} not found in Application {1}", TimeGroup, ApplicationKey));
                }
            }
            else
            {
                throw new ArgumentOutOfRangeException(string.Format("viewinfo with id {0} cannot be found!", ApplicationKey));
            }

            bool isAllowedToRun = false;

            foreach (var at in allowedTimeGroup.AllowedTimes)
            {
                DayOfWeek startWeekday = DayOfWeek.Sunday;
                DayOfWeek endWeekday = DayOfWeek.Sunday;
                if (Enum.IsDefined(typeof(DayOfWeek), at.Start.Day))
                {
                    startWeekday = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), at.Start.Day, true);
                }
                else
                {
                    LogLayer.Warning.Location(CLASS, "TimeWindowAllowedFor").Append(string.Format("in AllowedTimeGroup {0}, undefined Start WeekOfDay", at.Start.Day));
                    continue;
                }
                if (Enum.IsDefined(typeof(DayOfWeek), at.End.Day))
                {
                    endWeekday = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), at.End.Day, true);
                }
                else
                {
                    LogLayer.Warning.Location(CLASS, "TimeWindowAllowedFor").Append(string.Format("in AllowedTimeGroup {0}, undefined End WeekOfDay {1}", at.End.Day));
                    continue;
                }
                DateTime startTime = DateTime.ParseExact(at.Start.Time, Model.ScheduleEvent.TIME_FORMAT, dtParsingCulture);
                DateTime endTime = DateTime.ParseExact(at.End.Time, Model.ScheduleEvent.TIME_FORMAT, dtParsingCulture);
                if (DateTime.Now.DayOfWeek == startWeekday && timeNow >= startTime.Subtract(DateTime.Today)
                  && DateTime.Now.DayOfWeek == endWeekday && timeNow <= endTime.Subtract(DateTime.Today))
                {
                    isAllowedToRun = true;
                    break;
                }
            }
            return isAllowedToRun;
        }


        private void userOverride_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Value")
            {
                this.OnPropertyChanged("AutoStartValue");
                this.OnPropertyChanged("ImpersonationValue");
                this.OnPropertyChanged("FavoriteValue");
                this.OnPropertyChanged("IconDescription");
                this.OnPropertyChanged("IconDescriptionWithFavoriteIndicator");
            }
        }
        #endregion

        #region Protected Properties
        protected internal ActiveRelease Release { get; set; }
        protected bool UsingAlternateVersion { get; set; }
        #endregion Protected Properties

        #region Public Methods
 
        /// <summary>
        /// Returns the complete path for the executable.
        /// [AFS - \\san01b\DevAppsGML\dist\{Meta}\PROJ\{Project}\{Version}\{Executable}]
        /// [AFSDEV - \\san01b\DevAppsGML\dist\{Meta}\PROJ\{Project}\{Version}\{Executable}]
        /// [COPYLOCAL - C:\Docs&Settings\username\ApplicationData\Morgan Stanley\{ApplicationKey}\bin\{Version}\{Executable}]
        /// </summary>
        public string GetPath(IEDCodeLocation location_)
        {
            switch (location_)
            {
                case IEDCodeLocation.AFS:
                    if (IsPackageAbsolutePath)
                    {
                        return Package;
                    }
                    return @"\\san01b\DevAppsGML\dist\" + m_meta + @"\PROJ\" + m_project + @"\" + Version + @"\";
                case IEDCodeLocation.AFSDEV:
                    if (IsPackageAbsolutePath)
                    {
                        return Package;
                    }
                    return @"\\ms\dev\" + m_meta + @"\" + m_project + @"\" + Version + @"\install\common\";
                case IEDCodeLocation.COPYLOCAL:
                    return CopyLocalRootPath + ApplicationDescription + @"\bin\" + Version + @"\";
                case IEDCodeLocation.COPYLOCALTEMP:
                    return System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData)
                      + @"\Morgan Stanley\Launcher\CachedPackages\" + ApplicationDescription + @"\" + Version + @"\";
                case IEDCodeLocation.DIRECT:
                    return Release.Executable;
                case IEDCodeLocation.COPYLOCALDIRECT:
                    return Package;
                default:
                    return null;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetPackagePath()
        {
            string path = this.GetPath(Release.CodeLocation);

            if (Release.CodeLocation == IEDCodeLocation.COPYLOCAL)
            {
                path = this.GetPath(IEDCodeLocation.AFS) + @"common\";  // if we're using copy local, the assumption is the package is on AFS.
                //adding common because want to make sure we are not looking at symlink for packages
                if (!File.Exists(path + Package))
                {
                    // if its not there e.g. because there is no common install,  just go back to the symlink version.
                    path = this.GetPath(IEDCodeLocation.AFS);
                }
            }

            if (Release.CodeLocation != IEDCodeLocation.COPYLOCALDIRECT)
            {
                // Direct will have full package name.  The others need it appending
                path += Package;
            }

            return path;
        }

        #endregion Public Methods

        #region Commands
        public ICommand ToggleAutoStartCommand { get; private set; }
        public ICommand ToggleImpersonationCommand { get; private set; }
        public ICommand DeleteLocalCacheCommand { get; private set; }
        public ICommand StartProcessCommand { get; private set; }
        public ICommand ToggleFavoriteCommand { get; private set; } 
        #endregion

        #region Private Helpers
        private bool IsShortUniqueKey(string key)
        {
            string shortKey = GetShortKey(key);
            // What is a Shortkey (what are considered to be a short key)
            //   1. shortKey == m_meta + "." + m_project 
            //   2. an alias 
            return shortKey == m_meta + "." + m_project || !key.Equals(shortKey);

        }
        private string GetShortKey(string key)
        {
            if (string.IsNullOrEmpty(key)) throw new ArgumentException("Key");
            return key.Substring(0, key.LastIndexOf(m_appID) == -1 ? key.Length : key.LastIndexOf(m_appID) + 1);
        }
        private bool IsLongUniqueKey(string key)
        {
            return !IsShortUniqueKey(key);
        }
        #endregion Private Helpers
 


        private bool Equals(ViewInfo other)
        {
           return  this.UniqueKey == other.UniqueKey && this.IconFileName == other.IconFileName && this.ApplicationDescription == other.ApplicationDescription;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = (UniqueKey != null ? UniqueKey.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (IconFileName != null ? IconFileName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (IconDescription != null ? IconDescription.GetHashCode() : 0); 
                return hashCode;
            } 
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((ViewInfo)obj);
        }

        object ICloneable.Clone()
        {
            return this.Clone();
        }

        public ViewInfo Clone()
        {
            return new ViewInfo(this.m_application, this.m_environmentGroup, this.m_environ, this.Release,
                                        this.UsingAlternateVersion, this.m_config, this.m_ensemble);
        }
    }


}
