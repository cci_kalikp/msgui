﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSDesktop.Launcher.UI
{

    public class FavoriteChangedEventArgs : EventArgs
    {
        public FavoriteChangedEventArgs(ViewInfo item_)
        {
            Item = item_; 
        }

        public ViewInfo Item { get; private set; } 
    }
     
}
