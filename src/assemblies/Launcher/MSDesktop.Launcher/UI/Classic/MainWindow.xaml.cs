﻿#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2010 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

  $Id: //eai/msdotnet/msgui/trunk/assemblies/Launcher/MSDesktop.Launcher/UI/Classic/MainWindow.xaml.cs#17 $
  $DateTime: 2014/09/16 11:37:16 $
    $Change: 897128 $
    $Author: caijin $
_____________________________________________________________________________*/
#endregion File Info Header

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Shell;
using Infragistics.Windows.OutlookBar;
using MSDesktop.Launcher.Model; 
using Environment = System.Environment;

namespace MSDesktop.Launcher.UI
{
    internal static class CustomCommands
    {
        public static RoutedCommand NewUI = new RoutedCommand();
        public static RoutedCommand SingleMachine = new RoutedCommand();
        public static RoutedCommand Location = new RoutedCommand();
        public static RoutedCommand Groups = new RoutedCommand();
        public static RoutedCommand FavoriteAsFilter = new RoutedCommand();
        public static RoutedCommand FilterByFavorite = new RoutedCommand();
        public static RoutedCommand Details = new RoutedCommand();
        public static RoutedCommand ReloadApp = new RoutedCommand();
        public static RoutedCommand ReloadUser = new RoutedCommand();
        public static RoutedCommand Run = new RoutedCommand();
        public static RoutedCommand ClearCache = new RoutedCommand();
        public static RoutedCommand About = new RoutedCommand();
    }

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ApplicationController _controller;
        private static string _currentGroup = String.Empty;
        private static List<string> _expandedSubGroups = new List<string>();

        public MainWindow(ApplicationController controller_)
        {
            InitializeComponent();
            Icon = controller_.Settings.Image;
            Title = controller_.Settings.Title; 
            _controller = controller_;
            _controller.ModelChanged += new ApplicationController.ModelChangedEventHandler(_controller_ModelChanged);
            _controller.FavoriteChanged += new EventHandler<FavoriteChangedEventArgs>(_controller_FavoriteChanged);

            _menuItemSysLoc.Header = "Filter by this location (" + Launcher.SystemLocation + ")";
            SyncMenu();
            RefreshList();

            if (!controller_.Settings.StayAlive)
            {
                this.ResizeMode = ResizeMode.CanMinimize;
            }
        }

        void _controller_FavoriteChanged(object sender, FavoriteChangedEventArgs e)
        {
            if (_controller.ExtraUserSettings.FavoriteAsFilter && _controller.UserSettings.FilterByFavorite &&
                !e.Item.FavoriteValue)
            {
                RefreshList();
            }
        }

        private void AboutMenuItem_Click(object sender, RoutedEventArgs e)
        {
            _controller.ShowAboutForm(null, false);
        }

        private delegate void RefreshDelegate();

        private void _controller_ModelChanged()
        {
            if (this.Dispatcher.Thread == Thread.CurrentThread)
            {
                this.RefreshList();
            }
            else
            {
                this.Dispatcher.BeginInvoke(new RefreshDelegate(RefreshList));
            }
        }

        /// <summary>
        /// Refresh the list of available icons.
        /// </summary>
        private void RefreshList()
        {
            try
            {
                HookEvents(false); 
                if (_groupsMenuItem.IsChecked)
                {
                    _outlookBar.GroupsSource = _controller.GetViewListByApplicationThenEnvironment(_menuItemSysLoc.IsChecked, null, _controller.ExtraUserSettings.FavoriteAsFilter, _controller.UserSettings.FilterByFavorite);
                    if (_outlookBar.Groups.Count > 0)
                    {
                        foreach (OutlookBarGroup group in _outlookBar.Groups)
                        {
                            IList<ViewInfoIconGroup> grouplist = group.Content as IList<ViewInfoIconGroup>;
                            bool atLeastOneGroupExpanded = false;
                            // need this to guard against the previous selected sub group not being there any more, so need to expand the first one
                            foreach (ViewInfoIconGroup subgroup in grouplist)
                            {
                                if (_expandedSubGroups.Count == 0)
                                {
                                    subgroup.IsExpanded = (subgroup.EnvironmentType == EnvironmentType.Prod);

                                    if (subgroup.IsExpanded)
                                    {
                                        atLeastOneGroupExpanded = true;
                                    }
                                }
                                else if (_expandedSubGroups.Contains(subgroup.Name) &&
                                         String.Equals(group.Header, _currentGroup))
                                {
                                    subgroup.IsExpanded = true;
                                    atLeastOneGroupExpanded = true;
                                }
                            }

                            if (!atLeastOneGroupExpanded)
                            {
                                grouplist[0].IsExpanded = true;
                            }

                            if (String.Equals(group.Header, _currentGroup))
                            {
                                _outlookBar.SelectedGroup = group;
                            }
                        }

                        if (_currentGroup == String.Empty)
                        {
                            _outlookBar.SelectedGroup = _outlookBar.Groups[0];
                            _currentGroup = _outlookBar.SelectedGroup.Header as string;
                        }
                    }

                    HookEvents(true);
                }
                else
                {
                    _listView.ItemsSource =
                        _controller.GetVisibleViewListSortedByApplicationAndEnvironment(_menuItemSysLoc.IsChecked, null,
                        _controller.ExtraUserSettings.FavoriteAsFilter && _controller.UserSettings.FilterByFavorite);
                }
            }
            catch (Exception ex)
            {
                ExceptionViewer.ShowException("Failed while refreshing icon list.", ex);
            }
        }

        private void SyncMenu()
        {
            _menuItemSysLoc.IsChecked = _controller.UserSettings.FilterBySysLoc;
            _favoriteAsFilterMenuItem.IsChecked = _controller.ExtraUserSettings.FavoriteAsFilter;
            _filterByFavoriteMenuItem.IsChecked = _controller.UserSettings.FilterByFavorite;
            _filterByFavoriteMenuItem.IsEnabled = _controller.ExtraUserSettings.FavoriteAsFilter; 
            _menuItemUseModernUI.IsChecked = _controller.ExtraUserSettings.UseModernUI;
        }
        private void ReloadUserMenuItem_Click(object sender, RoutedEventArgs e)
        {
            Cursor previousCursor = this.Cursor;
            this.Cursor = Cursors.Wait;

            try
            {
                _controller.ReloadUserConfig(); 
            }
            finally
            {
                this.Cursor = previousCursor;
            }
        }

        private void ReloadApplicationMenuItem_Click(object sender, RoutedEventArgs e)
        {
            Cursor previousCursor = this.Cursor;
            this.Cursor = Cursors.Wait;

            try
            {
                _controller.ReloadApplicationConfig();
            }
            finally
            {
                this.Cursor = previousCursor;
            }
        }

        private void ClearCacheMenuItem_Click(object sender, RoutedEventArgs e)
        {

            MessageBoxResult result = MessageBox.Show(this,
                                                      "This will delete all local copies of applications copied from AFS." +
                                                      Environment.NewLine +
                                                      "If you proceed, any running apps will be killed in order to delete their files",
                                                      "Clear Local Cache", MessageBoxButton.YesNo,
                                                      MessageBoxImage.Warning,
                                                      MessageBoxResult.Yes);

            if (result == MessageBoxResult.Yes)
            {
                Cursor previousCursor = this.Cursor;
                this.Cursor = Cursors.Wait;

                try
                {
                    _controller.DeleteLocalCache();
                }
                finally
                {
                    this.Cursor = previousCursor;
                }
            }
        }

        private void ModernUIMenuItem_Click(object sender, RoutedEventArgs e)
        {
            _menuItemUseModernUI.IsChecked = !_menuItemUseModernUI.IsChecked;
            _controller.ExtraUserSettings.UseModernUI = _menuItemUseModernUI.IsChecked;
            _controller.SaveExtraUserConfig();
            _controller.RestartLauncher();
        }
         

        private void SysLocMenuItem_Click(object sender, RoutedEventArgs e)
        {
            _menuItemSysLoc.IsChecked = !_menuItemSysLoc.IsChecked;
            _controller.UserSettings.FilterBySysLoc = _menuItemSysLoc.IsChecked;
            _controller.SaveUserConfig();
            RefreshList();
        } 

        private void GroupsMenuItem_Click(object sender, RoutedEventArgs e)
        {
            HookEvents(false);
            if (!_groupsMenuItem.IsChecked)
            {
                _groupsMenuItem.IsChecked = true;
                _outlookBar.Visibility = Visibility.Visible;
                _listView.Visibility = Visibility.Collapsed;
                _detailsMenuItem.IsChecked = false;

                _listView.ItemsSource = null;
                _outlookBar.GroupsSource = _controller.GetViewListByApplicationThenEnvironment(_menuItemSysLoc.IsChecked, null, _controller.ExtraUserSettings.FavoriteAsFilter, _controller.UserSettings.FilterByFavorite);

                if (_outlookBar.Groups.Count > 0)
                {
                    _outlookBar.SelectedGroup = _outlookBar.Groups[0];
                }
                HookEvents(true);
            } 
        }

        private void _selectedGroupChanged(object sender, RoutedEventArgs e)
        {
            _expandedSubGroups.Clear();
            if (_groupsMenuItem.IsChecked)
            {
                if (_outlookBar.SelectedGroup != null)
                {
                    _currentGroup = _outlookBar.SelectedGroup.Header as string;
                    IList<ViewInfoIconGroup> grouplist = _outlookBar.SelectedGroup.Content as IList<ViewInfoIconGroup>;
                    foreach (ViewInfoIconGroup subGroup in grouplist)
                    {
                        if (subGroup.IsExpanded)
                        {
                            _expandedSubGroups.Add(subGroup.Name);
                        }
                    }
                }
            }
        }

        private void DetailsMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (!_detailsMenuItem.IsChecked)
            {
                _detailsMenuItem.IsChecked = true;
                _groupsMenuItem.IsChecked = false;

                _listView.Visibility = Visibility.Visible;
                _outlookBar.Visibility = Visibility.Collapsed;

                _outlookBar.GroupsSource = null;
                _listView.ItemsSource =
                    _controller.GetVisibleViewListSortedByApplicationAndEnvironment(_menuItemSysLoc.IsChecked, null, _controller.UserSettings.FilterByFavorite);
            }
        }


        private void FavoriteAsFilterUIMenuItem_Click(object sender, RoutedEventArgs e)
        {
            _favoriteAsFilterMenuItem.IsChecked = !_favoriteAsFilterMenuItem.IsChecked;
            _controller.ExtraUserSettings.FavoriteAsFilter = _favoriteAsFilterMenuItem.IsChecked;
            _filterByFavoriteMenuItem.IsEnabled = _controller.ExtraUserSettings.FavoriteAsFilter;
            _controller.SaveExtraUserConfig();
            RefreshList();
        }

        private void FilterByFavoriteMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (!_controller.ExtraUserSettings.FavoriteAsFilter) return;
            _filterByFavoriteMenuItem.IsChecked = !_filterByFavoriteMenuItem.IsChecked;
            _controller.UserSettings.FilterByFavorite = _filterByFavoriteMenuItem.IsChecked;
            _controller.SaveUserConfig();
            RefreshList();
        }

        private void _listView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                ViewInfo info = _listView.SelectedItem as ViewInfo;
                if (info != null)
                {
                    _controller.StartProcess(info); 
                }
            }
        }

       

        private void RunPackageMenuItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                RunDialog dialog = new RunDialog(_controller, _currentGroup);
                dialog.Parameters.Owner = this;
                dialog.ShowDialog();
                _controller.StartProcess(dialog.ViewInfo);
            }
            catch (Exception ex_)
            {
                LogLayer.Error.Location("MainWindow", "RunPackageMenuItem_Click")
                        .Append("Error running Run window " + ex_.Message)
                        .Send();
            }
        }

        private void HookEvents(bool hook_)
        {
            if (hook_)
            {
                _outlookBar.SelectedGroupChanged += _selectedGroupChanged;
            }
            else
            {
                _outlookBar.SelectedGroupChanged -= _selectedGroupChanged;
            }
            foreach (OutlookBarGroup outlookBarGroup in _outlookBar.Groups)
            {
                IList<ViewInfoIconGroup> grouplist = outlookBarGroup.Content as IList<ViewInfoIconGroup>;
                foreach (ViewInfoIconGroup subGroup in grouplist)
                {
                    if (hook_)
                    {
                        subGroup.IsExpandedChanged += _selectedGroupChanged;
                    }
                    else
                    {
                        subGroup.IsExpandedChanged -= _selectedGroupChanged;
                    }
                }
            }
        }
    }
}
