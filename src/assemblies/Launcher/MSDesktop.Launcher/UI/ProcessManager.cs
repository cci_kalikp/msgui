#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2006 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

              $Id: //eai/msdotnet/msgui/trunk/assemblies/Launcher/MSDesktop.Launcher/UI/ProcessManager.cs#37 $
  $DateTime: 2014/09/17 08:28:31 $
    $Change: 897265 $
    $Author: caijin $
_____________________________________________________________________________*/
#endregion File Info Header

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading; 
using MSDesktop.Launcher.MessageDispatcher;
using MSDesktop.Launcher.MessageDispatcher.Helpers;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication;
using System.Management;

namespace MSDesktop.Launcher.UI
{
    /// <summary>
    /// Provides process launching functionality
    /// </summary>
    public class ProcessManager
    {
        #region Private Member Variables

        private const string CLASS_NAME = "ProcessManager";
        private Process m_process;
        private string _imageName = string.Empty;
        private readonly ManualResetEvent ipcReadyEvent = new ManualResetEvent(false);
        private readonly ManualResetEvent exitedEvent = new ManualResetEvent(false); 
        private string instanceName;
        private string applicationName;
        private DateTime? startTime;
        private DateTime? hibernateTime;
        private readonly int estimatedStartupSeconds;
        private readonly string applicationKey;
        private readonly string fileName;
        private bool processDisposed = false;
        #endregion

        #region Events

        public event EventHandler<ProcessStateChangedEventArgs> ProcessStateChanged;

        #endregion

        #region Constructors

        /// <summary>
        /// Main constructor.
        /// </summary> 
        public ProcessManager(ViewInfo viewInfo_)
        {
            estimatedStartupSeconds = viewInfo_.EstimatedStartupSeconds;
            applicationKey = viewInfo_.UniqueKey;
            fileName = viewInfo_.CompleteFilename; 
            Initialize(viewInfo_.Arguments, viewInfo_.ImageName, viewInfo_.WorkingDirectory);
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the underlying process object
        /// </summary>
        public Process Process
        {
            get { return m_process; }
        }

        #endregion

        #region Public Methods

        public void KillProcess()
        {
            if (State != ProcessState.Exited && 
                State != ProcessState.Hibernating)
            {
                m_process.Kill();
                State = ProcessState.Exited;
            } 
        }

        public bool Started
        {
            get
            {
                if (state == ProcessState.Exiting)
                {
                    int index = WaitHandle.WaitAny(new WaitHandle[] { ipcReadyEvent, exitedEvent }, Consts.ProcessReadyTimeout);
                    if (index == 0) return true;
                    if (index == 1) return false;
                }
                return (state == ProcessState.None ||
                        state == ProcessState.Started ||
                        state == ProcessState.Starting);
            }
        }

        private ProcessState state = ProcessState.None;
        public ProcessState State
        {
            get
            {
                if (state == ProcessState.Starting && hibernateTime != null)
                {
                    double timeSpan = ((TimeSpan)(DateTime.Now - hibernateTime.Value)).TotalSeconds;
                    if (timeSpan > Consts.LongEstimatedProcessStartupSeconds)
                    {
                        state = ProcessState.Hibernating;
                    } 
                }
                return state;
            }
            set
            {
                if (state != value)
                {
                    LogLayer.Debug.Location(CLASS_NAME, "set_State").Append("state of " + fileName + " is set to " + value).SendQuietly();
                    state = value;
                    if (value == ProcessState.Exited)
                    { 
                        ApplicationMessageHandler.ApplicationIPCReady -= ApplicationIpcReady;
                        hibernateTime = null;
                    }
                    else if (value == ProcessState.Starting)
                    {
                        ApplicationMessageHandler.ApplicationIPCReady -= ApplicationIpcReady;
                        ApplicationMessageHandler.ApplicationIPCReady += ApplicationIpcReady; 
                        instanceName = null;
                        hibernateTime = null;
                    }
                    if (value == ProcessState.Exited)
                    { 
                        //Debug.WriteLine(String.Format("Process Id {0} has exited at {1} with exit code {2}.",
                        //           this.m_process.Id, DateTime.Now, this.m_process.ExitCode));
                        DisposeProcess();
                        instanceName = null;
                        exitedEvent.Set();
                    }
                    else if (value == ProcessState.Starting ||
                        value == ProcessState.Exiting)
                    {
                        startTime = DateTime.Now;  
                        exitedEvent.Reset(); 
                        ipcReadyEvent.Reset();
                    }
                    else if (value == ProcessState.Started)
                    {
                        ipcReadyEvent.Set(); 
                    }
                    var copy = ProcessStateChanged;
                    if (copy != null)
                    {
                        copy(this, new ProcessStateChangedEventArgs(this, state));
                    }
                }
            }
        }
       
        /// <summary>
        /// Start the process
        /// </summary>
        public void StartProcess(object state_)
        {
            try
            {
                State = ProcessState.Starting;
                string message = null;
                if (!System.IO.File.Exists(fileName))
                { 
                    LogLayer.Error.Location(CLASS_NAME, "StartProcess()").Append(String.Format("Failed to open '{0}', file not found.", fileName)).Send();
                    State = ProcessState.Exited;
                    return;
                }

                string msg = String.Format("Starting '{0}'.",
          this.m_process.StartInfo.FileName);
                LogLayer.Debug.Location(CLASS_NAME, "StartProcess()").Append(msg).Send(); 


                if (!m_process.Start())
                {
                    State = ProcessState.Exited;
                    message = String.Format("Failed to start '{0}' at {1} with Id {2}.",
                                            fileName, this.m_process.StartTime,
                                            this.m_process.Id);
                }
                else
                {
                    message = String.Format("Started '{0}' at {1} with Id {2}.",
                                            fileName, this.m_process.StartTime,
                                            this.m_process.Id);  
                }
                LogLayer.Info.Location(CLASS_NAME, "StartProcess()").Append(message).Send();

            }
            catch (Exception ex_)
            {
                string msg = String.Format("Failed while starting '{0}'.", fileName);
                LogLayer.Error.Location(CLASS_NAME, "StartProcess()").Append(msg, ex_.ToString()).Send();
                 State = ProcessState.Exited;
            }
        }

        private void ApplicationIpcReady(object sender_, ApplicationIPCReadyEventArgs e_)
        {
            if (e_.Message.ApplicationKey == applicationKey || 
                (string.IsNullOrEmpty(e_.Message.ApplicationKey) &&  
                 e_.Message.MachineName == Environment.MachineName &&
                 RefreshProcessIds().Contains(e_.Message.ProcessId))) 
            {
                if (e_.Message.State == ApplicationState.Exited)
                {
                    State = ProcessState.Exited;
                    return;
                }
                if (e_.Message.State == ApplicationState.Exiting)
                {
                    State = ProcessState.Exiting;
                    return;
                }
                int currentProcessId = -1;
                if (!processDisposed)
                {
                    try
                    {
                        currentProcessId = m_process.Id; 
                    }
                    catch  
                    { 
                    }
                }
                applicationName = e_.Message.ApplicationName;
                if (processDisposed ||
                    (currentProcessId != e_.Message.ProcessId))
                {
                    if (m_process != null)
                    {
                        DisposeProcess();
                    }
                    try
                    {
                        m_process = Process.GetProcessById(e_.Message.ProcessId);
                        AttachExitEvent();
                        hibernateTime = null;
                        if (e_.Message.State == ApplicationState.IPCReady)
                        {
                            instanceName = e_.Message.InstanceName;
                        } 
                        State = e_.Message.State == ApplicationState.IPCReady ? ProcessState.Started : ProcessState.Starting;
                    }
                    catch
                    {
                        State = ProcessState.Exited;
                    }
                }
                else
                {
                    hibernateTime = null;
                    if (e_.Message.State == ApplicationState.IPCReady)
                    {
                        instanceName = e_.Message.InstanceName;
                    } 
                    State = e_.Message.State == ApplicationState.IPCReady ? ProcessState.Started : ProcessState.Starting;
                }

            } 

        } 

        #endregion

        #region Private Helper Methods

        /// <summary>
        /// Perform the initialization of the process and its start information.
        /// </summary> 
        /// <param name="arguments_">The command line arguments for the executable.</param>
        /// <param name="workingDir_">The working directory to use.</param>
        private void Initialize(string arguments_, string imageName_, string workingDir_)
        {
            m_process = new Process(); 
            AttachExitEvent();

            ProcessStartInfo psi = new ProcessStartInfo();
            if (fileName.EndsWith(".cmd") || fileName.EndsWith(".bat"))
            {
                string commandExe = Environment.GetEnvironmentVariable("ComSpec");
                if (!string.IsNullOrEmpty(commandExe))
                {
                    psi.FileName = commandExe;
                    //psi.CreateNoWindow = true;
                    psi.Arguments = string.Format("/C \"{0}\" {1}", fileName, arguments_);
                } 
            }
            
            if (string.IsNullOrEmpty(psi.FileName))
            {
                psi.FileName = fileName; 

                if (!String.IsNullOrEmpty(arguments_))
                {
                    psi.Arguments = arguments_;
                }
            }
             
            if (!String.IsNullOrEmpty(workingDir_))
            {
                psi.WorkingDirectory = PathUtilities.GetAbsolutePath(workingDir_);
            }

            if (!String.IsNullOrEmpty(imageName_))
            {
                _imageName = imageName_;
            }

            if (psi.FileName.EndsWith(".cmd") || psi.FileName.EndsWith(".bat"))
            {
                //use command line argument
                string arg = string.IsNullOrEmpty(psi.Arguments) ? string.Empty : (psi.Arguments + " ");
                arg += string.Format("/{0}=\"{1}\" /{2}=\"{3}\"", LauncherConsts.ApplicationUniqueKeyVariable, applicationKey, LauncherConsts.LauncherNameVariable, Launcher.LauncherName);
                psi.Arguments = arg;

            }
            else if (psi.FileName.EndsWith(".exe"))
            {
                psi.EnvironmentVariables[LauncherConsts.ApplicationUniqueKeyVariable] = applicationKey;
                psi.EnvironmentVariables[LauncherConsts.LauncherNameVariable] = Launcher.LauncherName;
                psi.UseShellExecute = false;
            }
            m_process.StartInfo = psi;
        }

        #endregion

        #region Protected methods
 
        #endregion

        #region Event Handlers 

        private void GetChildProcesses(int processId_, List<int> childProcessIds_)
        { 
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(
"SELECT * " +
"FROM Win32_Process " +
"WHERE ParentProcessId=" + processId_);

            ManagementObjectCollection collection = searcher.Get();
            if (collection.Count > 0)
            {
                foreach (var item in collection)
                {
                    int childProcessId = Convert.ToInt32(item["ProcessId"]);
                    if (!childProcessIds_.Contains(childProcessId))
                    {
                        try
                        {
                            childProcessIds_.Add(childProcessId); 
                        }
                        catch
                        {

                        }
                    }
                }
            } 
        }


        private List<int> RefreshProcessIds()
        { 
            if (processIds.Count == 0 && m_process != null)
            {
                try
                {
                    processIds.Add(m_process.Id); 
                }
                catch  
                {
                     
                }
            }
            Process newProcess = null;
            List<int> newProcessIds = new List<int>();
            foreach (var processId in processIds)
            {
                Process tempProcess = null;
                try
                {
                    tempProcess = Process.GetProcessById(processId);
                    newProcessIds.Add(processId);
                    if (newProcess == null)
                    {
                        newProcess = tempProcess;
                    }
                }
                catch
                { 
                }

                GetChildProcesses(processId, newProcessIds);
            }
            processIds = newProcessIds; 
            if (newProcess == null && newProcessIds.Count > 0)
            {
                foreach (var newProcessId in newProcessIds)
                {
                    try
                    {
                        newProcess = Process.GetProcessById(newProcessId); 
                        break;
                    }
                    catch 
                    {
                         
                    }
                }
            }
            if (newProcess != null)
            {
                DisposeProcess(); 
                m_process = newProcess;
                AttachExitEvent();
            } 
            return processIds;
        }

        private void DisposeProcess()
        {
            if (processDisposed) return;
            if (m_process == null)
            {
                processDisposed = true;
                return;
            }
            m_process.Exited -= new System.EventHandler(this.Process_Exited);
            try
            {
                m_process.Dispose(); 
            }
            catch 
            { 
                
            }
            processDisposed = true;

        }

        private void AttachExitEvent()
        {
            try
            {
                m_process.EnableRaisingEvents = true;
                m_process.Exited += new System.EventHandler(this.Process_Exited);
                processDisposed = false;
            }
            catch 
            {

            }
        }
        private List<int> processIds = new List<int>(); 
        /// <summary>
        /// Handle a process exit 
        /// </summary>
        /// <param name="sender">The sender of the event.</param>
        /// <param name="e">Standard event arguments.</param>
        private void Process_Exited(object sender, System.EventArgs e)
        {  
            RefreshProcessIds();
            if (processIds.Count > 0) return;
            
            if (m_process != null)
            {
                string msg;
                try
                {
                    int exitCode = m_process.ExitCode;
                    if (exitCode > 0)
                    {
                        var newProcess = Process.GetProcessById(exitCode);

                        //Make sure the process we caught using ExitCode is the process we expect to catch
                        //and not some other random process
                        if (System.String.CompareOrdinal(newProcess.ProcessName, _imageName.Replace(".exe", string.Empty)) == 0)
                        {
                            msg = String.Format("Original process {0} with id {1} exited.  Reattached to new process {2} with Id {3}.",
            m_process.ProcessName, m_process.Id,
            m_process.ProcessName, m_process.Id);
                            Debug.WriteLine(msg);
                            DisposeProcess();
                            m_process = newProcess;
                            AttachExitEvent();
                            return;
                        }
                    }
                }
                catch  
                { 
                }
               
            }
 
           
            if (State == ProcessState.Started ||
                State == ProcessState.Hibernating )
            {
                State = ProcessState.Exited;
            } 
            else if (State == ProcessState.Starting)
            {
                 //if (m_process != null)
                 //{
                 //    try
                 //    {
                 //        if (!string.IsNullOrEmpty(m_process.StartInfo.FileName) && m_process.StartInfo.FileName.EndsWith(".exe"))
                 //        {
                 //            string directory = Path.GetDirectoryName(m_process.StartInfo.FileName);
                 //            if (!string.IsNullOrEmpty(directory))
                 //            {
                 //                string[] files = Directory.GetFiles(directory);
                 //                int msDotnetDllCounts = 0; 
                 //                foreach (var file in files)
                 //                {
                 //                    if ((file.StartsWith(Path.Combine(directory, "MSDotNet.")) || 
                 //                        file.StartsWith(Path.Combine(directory, "MSDesktop.")) || 
                 //                        file.StartsWith(Path.Combine(directory, "Concord.")))
                 //                         && file.EndsWith(".dll"))
                 //                    {
                 //                        msDotnetDllCounts++;
                 //                    } 
                 //                }
                 //                if (msDotnetDllCounts > 5)
                 //                {
                 //                    State = ProcessState.Exited;
                 //                    return;
                 //                }
                 //            }
                 //        }
                 //    }
                 //    catch 
                 //    { 
                         
                 //    }
                 //}
                hibernateTime = DateTime.Now;
            }
        }

        internal string Instance
        {
            get { return instanceName; }
        }

        internal string ApplicationName
        {
            get { return applicationName; }
        }
         
        internal bool IsFullyStartedByEstimation
        {
            get
            {
                if (startTime == null) return false;
                if (State == ProcessState.Started) return true;
                double timeSpan = ((TimeSpan) (DateTime.Now - startTime.Value)).TotalSeconds;
                return timeSpan > estimatedStartupSeconds;
            }
        }
        internal bool WaitForIPCReady
        {
            get
            {
                try
                {
                    if (this.State == ProcessState.Exited)
                    {
                        return false;
                    }
                    if (this.State == ProcessState.Started)
                    {
                        return true;
                    }
                }
                catch
                {
                }
                //wait for at most 5 min
                int index = WaitHandle.WaitAny(new WaitHandle[] {ipcReadyEvent, exitedEvent}, Consts.ProcessReadyTimeout);
                if (index == 0) return true;
                if (index == 1) return false;
                LogLayer.Info.Location(CLASS_NAME, "WaitForIPCReady")
                        .Append(string.Format("Process '{0}' has been started by without IPC enabled",
                                fileName));
                return false;
            }
        }

        #endregion
    }
}