﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSDesktop.Launcher.UI
{

    public class MRUListChangedEventArgs : EventArgs
    {
        public MRUListChangedEventArgs(ViewInfo item_, ListChangeAction action_)
        {
            Item = item_;
            Action = action_;
        }

        public ViewInfo Item { get; private set; }
        public ListChangeAction Action { get; private set; }
    }

    public enum ListChangeAction
    {
        Add,
        Remove,
        Move
    }
}
