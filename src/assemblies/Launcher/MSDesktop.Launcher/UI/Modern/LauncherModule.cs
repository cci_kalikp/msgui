using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Infragistics.Controls.Menus;
using MSDesktop.Launcher.Model;
using Microsoft.Practices.Composite.Modularity;
using MSDesktop.Launcher.UI;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers.ShellMenu;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager; 
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using Application = System.Windows.Application; 
using Environment = System.Environment;

namespace MSDesktop.Launcher
{
    public class LauncherModule : IModule
    {
        private readonly IChromeManager manager;
        private const int MaxIconCount = 40; 
        private readonly IApplicationOptions options;

        private readonly ResourceDictionary contextMenuResource = new ResourceDictionary()
            {
                Source =
                    new Uri("pack://application:,,,/MSDesktop.Launcher;component/UI/ViewInfoContextMenu.xaml",
                            UriKind.RelativeOrAbsolute)
            };

        public LauncherModule(IChromeManager manager_, IApplicationOptions options_, IChromeRegistry registry_)
        {
            this.manager = manager_;
            this.options = options_; 
            DialogExtensions.ChromeManager = manager_;
            DialogExtensions.ChromeRegistry = registry_;
        }

        private WidgetViewContainer buttonFilterByEnv;
        public void Initialize()
        {
            #region Remove Layout related functions

            options.RemoveOptionPage("MSDesktop", "Application Settings");
            options.RemoveOptionPage("MSDesktop", "Layout Manager");
            manager.RemoveWidget(ShellMenuExtensions.ApplicationMenuFactories.AfterLayoutManagementSeparator);
            manager.RemoveWidget(ShellMenuExtensions.ApplicationMenuFactories.DeleteLayoutButton);
            manager.RemoveWidget(ShellMenuExtensions.ApplicationMenuFactories.SaveLayoutButton);
            manager.RemoveWidget(ShellMenuExtensions.ApplicationMenuFactories.SaveLayoutMenu);
            manager.RemoveWidget(ShellMenuExtensions.ApplicationMenuFactories.LoadLayoutMenu);
            manager.RemoveWidget(ShellMenuExtensions.ApplicationMenuFactories.TopmostButton);
            if (Launcher.Controller.Settings.StayAlive)
            {
                manager.RemoveWidget(ShellMenuExtensions.ApplicationMenuFactories.MinimizeButton);
                IWidgetViewContainer closeWidget =
                    manager.GetWidget(ShellMenuExtensions.ApplicationMenuFactories.ExitButtonWithSeparator);
                MenuItem closeItem = closeWidget.Content as
             MenuItem;
                if (closeItem != null)
                {
                    closeItem.Header = "Hide";
                }
                else
                {
                    EventHandler contentReady = null;
                    closeWidget.ContentReady += contentReady = (sender_, args_) =>
                        {
                            MenuItem item = closeWidget.Content as MenuItem;
                            if (item != null)
                            {
                                item.Header = "Hide";
                            }
                            closeWidget.ContentReady -= contentReady;
                        };
                }
            } 
            #endregion

            #region Add View Buttons

            var buttonFilterBySysLoc = manager.PlaceWidget("btnFilterBySysLoc", ChromeArea.ApplicationMenu,
                                                          new InitialButtonParameters()
                                                          {
                                                              Text =
                                                                  "Filter by this location (" + Launcher.SystemLocation + ")",
                                                              IsChecked =
                                                                  Launcher.Controller.UserSettings.FilterBySysLoc,
                                                              Gesture = new KeyGesture(Key.L, ModifierKeys.Control),
                                                              ShowInputGesture = true,
                                                              Click =
                                                                  (sender_, args_) =>
                                                                  {
                                                                      Launcher.Controller.UserSettings
                                                                              .FilterBySysLoc =
                                                                          !Launcher.Controller.UserSettings
                                                                                   .FilterBySysLoc;
                                                                      (sender_ as MenuItem).IsChecked =
                                                                          Launcher.Controller.UserSettings
                                                                                  .FilterBySysLoc;
                                                                      Launcher.Controller.SaveUserConfig();
                                                                      FillApplicationList();
                                                                  }
                                                          });

            var buttonFavoriteAsFilter = manager.PlaceWidget("btnFavoriteAsFilter", ChromeArea.ApplicationMenu,
                                                           new InitialButtonParameters()
                                                               {
                                                                   Text =
                                                                       "Favorite as filter",
                                                                   IsChecked =
                                                                       Launcher.Controller.ExtraUserSettings.FavoriteAsFilter, 
                                                                   Click =
                                                                       (sender_, args_) =>
                                                                           {
                                                                               Launcher.Controller.ExtraUserSettings
                                                                                       .FavoriteAsFilter =
                                                                                   !Launcher.Controller.ExtraUserSettings
                                                                                            .FavoriteAsFilter;
                                                                               (sender_ as MenuItem).IsChecked =
                                                                                   Launcher.Controller.ExtraUserSettings
                                                                                           .FavoriteAsFilter;  
                                                                               Launcher.Controller.SaveExtraUserConfig();
                                                                               FillApplicationList();
                                                                           }
                                                               });
            var buttonFilterByFavorite = manager.PlaceWidget("btnFilterByFavorite", ChromeArea.ApplicationMenu,
                                                           new InitialButtonParameters()
                                                           {
                                                               Text =
                                                                   "Filter by favorites",
                                                               Enabled = Launcher.Controller.ExtraUserSettings.FavoriteAsFilter,
                                                               Gesture = new KeyGesture(Key.F, ModifierKeys.Control),
                                                               ShowInputGesture = true,
                                                               IsChecked =
                                                                   Launcher.Controller.UserSettings.FilterByFavorite,
                                                               Click =
                                                                   (sender_, args_) =>
                                                                   {
                                                                       Launcher.Controller.UserSettings
                                                                               .FilterByFavorite =
                                                                           !Launcher.Controller.UserSettings
                                                                                    .FilterByFavorite;
                                                                       (sender_ as MenuItem).IsChecked =
                                                                           Launcher.Controller.UserSettings
                                                                                   .FilterByFavorite;
                                                                       Launcher.Controller.SaveUserConfig();
                                                                       FillApplicationList();
                                                                   }
                                                           });
            if (buttonFilterByFavorite.Content != null)
            {
                MenuItem menu = buttonFilterByFavorite.Content as MenuItem;
                if (menu != null)
                {
                    BindingOperations.SetBinding(menu, MenuItem.IsEnabledProperty, new Binding() { Source = Launcher.Controller.ExtraUserSettings, Path = new PropertyPath("FavoriteAsFilter") });
                }
            }
            else
            {
                EventHandler contentReady = null;
                buttonFilterByFavorite.ContentReady += contentReady = (sender_, args_) =>
                {
                    MenuItem menu = buttonFilterByFavorite.Content as MenuItem;
                    if (menu != null)
                    {
                        BindingOperations.SetBinding(menu, MenuItem.IsEnabledProperty, new Binding() { Source = Launcher.Controller.ExtraUserSettings, Path = new PropertyPath("FavoriteAsFilter") });
                    }
                    buttonFilterByFavorite.ContentReady -= contentReady;
                };
            }
           

            buttonFilterByEnv = manager.PlaceWidget("btnFilterByEnv", ChromeArea.ApplicationMenu,
                                                    new InitialDropdownButtonParameters()
                                                        {
                                                            Text = "Filter by environment"
                                                        }) as WidgetViewContainer;
            FillEnvironmentList();

            manager.PlaceWidget("Separator" + Guid.NewGuid().ToString(), ChromeArea.ApplicationMenu,
                                new InitialDropdownSeparatorParameters());

            #endregion

            #region Add Switch mode button

            IWidgetViewContainer buttonClassic = null;
            if (!Launcher.Controller.Settings.ForceModernUI)
            {
                buttonClassic = manager.PlaceWidget("btnClassicUI", ChromeArea.ApplicationMenu,
                               new InitialButtonParameters()
                               {
                                   Text = "Switch to classic UI and restart",
                                   Gesture = new KeyGesture(Key.O, ModifierKeys.Control),
                                   ShowInputGesture = true,
                                   Click =
                                       (sender_, args_) =>
                                       {
                                           Launcher.Controller.ExtraUserSettings.UseModernUI =
                                               !Launcher.Controller.ExtraUserSettings
                                                        .UseModernUI;
                                           Launcher.Controller.SaveExtraUserConfig();
                                           (sender_ as MenuItem).IsChecked =
                                               !Launcher.Controller.ExtraUserSettings
                                                        .UseModernUI;
                                           Launcher.Controller.RestartLauncher();
                                       }

                               });

                
            }
             if (buttonClassic != null)
            {
                manager.PlaceWidget("Separator" + Guid.NewGuid().ToString(), ChromeArea.ApplicationMenu,
                    new InitialDropdownSeparatorParameters());

            }

            #endregion

            #region Add Tools Button

            manager.PlaceWidget("btnReloadApplicationConfig", ChromeArea.ApplicationMenu,
                                new InitialButtonParameters()
                                    {
                                        Text = "Reload Application Config",
                                        Gesture = new KeyGesture(Key.F5, ModifierKeys.None),
                                        ShowInputGesture = true,
                                        Click =
                                            (sender_, args_) => ExecuteTimeConsumingTask(Launcher.Controller.ReloadApplicationConfig)
                                    });
            manager.PlaceWidget("btnReloadLocalSettings", ChromeArea.ApplicationMenu,
                                new InitialButtonParameters()
                                    {
                                        Text = "Reload Local Settings",
                                        Gesture = new KeyGesture(Key.F5, ModifierKeys.Control),
                                        ShowInputGesture = true,
                                        Click = (sender_, args_) => ExecuteTimeConsumingTask(new Action(() => Launcher.Controller.ReloadUserConfig()))
                                    });

            manager.PlaceWidget("btnRunPackage", ChromeArea.ApplicationMenu, new InitialButtonParameters()
                {
                    Text = "Run package...",
                    Gesture = new KeyGesture(Key.R, ModifierKeys.Control),
                    ShowInputGesture = true,
                    Click = (sender_, args_) =>
                        {
                            RunDialog dialog = new RunDialog(Launcher.Controller, string.Empty);
                            dialog.Parameters.Owner = Application.Current.MainWindow;
                            dialog.ShowDialog();
                            Launcher.Controller.StartProcess(dialog.ViewInfo);
                        }
                });
            manager.PlaceWidget("btnClearCache", ChromeArea.ApplicationMenu, new InitialButtonParameters()
                {
                    Text = "Clear local cache...",
                    Click = (sender_, args_) =>
                        {
                            var result = TaskDialog.Show(new TaskDialogOptions()
                                {
                                    Content = "This will delete all local copies of applications copied from AFS." +
                                              Environment.NewLine +
                                              "If you proceed, any running apps will be killed in order to delete their files",
                                    Title = "Clear Local Cache",
                                    CommonButtons = TaskDialogCommonButtons.YesNo,
                                    MainIcon = VistaTaskDialogIcon.Warning
                                });

                            if (result.Result == TaskDialogSimpleResult.Yes)
                            {
                                ExecuteTimeConsumingTask(Launcher.Controller.DeleteLocalCache);
                            }
                        }
                });

            manager.PlaceWidget("btnAbout", ChromeArea.ApplicationMenu, new InitialButtonParameters()
                {
                    Text = "About...",
                    Gesture = new KeyGesture(Key.A, ModifierKeys.Control),
                    ShowInputGesture = true,
                    Click = (sender_, args_) => Launcher.Controller.ShowAboutForm(null, false)
                });

            #endregion

            FillApplicationList();
            Launcher.Controller.ModelChanged += Controller_ModelChanged; 
            Launcher.Controller.FavoriteChanged +=Controller_FavoriteChanged;

        }

        void Controller_FavoriteChanged(object sender, FavoriteChangedEventArgs e)
        { 
            if (Launcher.Controller.ExtraUserSettings.FavoriteAsFilter && !Launcher.Controller.UserSettings.FilterByFavorite)
            {
                IWidgetViewContainer viewWidget;
                if (viewInfoMenuItems.TryGetValue(e.Item, out viewWidget))
                {
                    MenuItem menuItem = viewWidget.Content as MenuItem;
                    if (menuItem != null)
                    {
                        menuItem.Header = e.Item.IconDescriptionWithFavoriteIndicator;
                    }
                }
                return;
            }
            FillApplicationList();
        }

        private void Controller_ModelChanged()
        {
            FillApplicationList();
            FillEnvironmentList();
        }

        private readonly Dictionary<EnvironmentType, IWidgetViewContainer> environmentWidgets =
            new Dictionary<EnvironmentType, IWidgetViewContainer>();

        private void FillEnvironmentList()
        {
            if (environmentWidgets.Count == 0)
            {
                buttonFilterByEnv.AddWidget(new InitialButtonParameters()
                    {
                        Text = "All environments",
                        IsChecked = string.IsNullOrEmpty(Launcher.Controller.ExtraUserSettings.DefaultEnvironment),
                        Click = (sender_, args_) =>
                            { 
                                MenuItem item = sender_ as MenuItem;
                                if (!item.IsChecked)
                                {
                                    foreach (MenuItem envGroupItem in buttonFilterByEnv.VisualChildren)
                                    {
                                        if (envGroupItem != item)
                                        {
                                            envGroupItem.IsChecked = false;
                                        }
                                    }
                                    item.IsChecked = true;
                                    Launcher.Controller.ExtraUserSettings.DefaultEnvironment = null;
                                    Launcher.Controller.SaveExtraUserConfig();
                                    FillApplicationList();
                                }
                            }
                    });
            }
            WidgetViewContainerHelper.ClearParents(environmentWidgets.Values);
            foreach (var environmentType in Launcher.Controller.AvailableEnvironmentTypes)
            {
                buttonFilterByEnv.CreateOrReAdd(environmentType, environmentWidgets, type_ =>
                                                                                     buttonFilterByEnv.AddWidget(new InitialButtonParameters
                                                                                                                     ()
                                                                                         {
                                                                                             Text = type_.ToString(),
                                                                                             IsChecked =
                                                                                                 Launcher.Controller
                                                                                                         .ExtraUserSettings
                                                                                                         .DefaultEnvironment ==
                                                                                                 type_.ToString(),
                                                                                             Click = (sender_, args_) =>
                                                                                                 {
                                                                                                     MenuItem item =
                                                                                                         sender_ as
                                                                                                         MenuItem;
                                                                                                     if (!item.IsChecked)
                                                                                                     {
                                                                                                         foreach (
                                                                                                             MenuItem
                                                                                                                 envGroupItem
                                                                                                                 in
                                                                                                                 buttonFilterByEnv
                                                                                                                     .VisualChildren
                                                                                                             )
                                                                                                         {
                                                                                                             if (
                                                                                                                 envGroupItem !=
                                                                                                                 item)
                                                                                                             {
                                                                                                                 envGroupItem
                                                                                                                     .IsChecked
                                                                                                                     =
                                                                                                                     false;
                                                                                                             }
                                                                                                         }
                                                                                                         item.IsChecked
                                                                                                             = true;
                                                                                                         Launcher
                                                                                                             .Controller
                                                                                                             .ExtraUserSettings
                                                                                                             .DefaultEnvironment
                                                                                                             =
                                                                                                             item.Header
                                                                                                             as string;
                                                                                                         Launcher
                                                                                                             .Controller
                                                                                                             .SaveExtraUserConfig
                                                                                                             ();
                                                                                                         FillApplicationList
                                                                                                             ();
                                                                                                     }
                                                                                                 }
                                                                                         }
                                                                                         ));
            }

        }

        private readonly Dictionary<ViewInfo, IWidgetViewContainer> viewInfoButtons =
            new Dictionary<ViewInfo, IWidgetViewContainer>();

        private readonly Dictionary<ViewInfo, IWidgetViewContainer> viewInfoMenuItems =
            new Dictionary<ViewInfo, IWidgetViewContainer>();

        private readonly Dictionary<ViewInfoIconGroup, WidgetViewContainer> groupWidgets =
            new Dictionary<ViewInfoIconGroup, WidgetViewContainer>();

        private readonly Dictionary<ViewInfo, WidgetViewContainer> invisibleViewWidgets =
            new Dictionary<ViewInfo, WidgetViewContainer>();

        private readonly Dictionary<ViewInfoIconGroup, WidgetViewContainer> invisibleGroupWidgets =
            new Dictionary<ViewInfoIconGroup, WidgetViewContainer>();

        private readonly Dictionary<ViewInfoGroup, WidgetViewContainer> invisibleApplicationWidgets =
            new Dictionary<ViewInfoGroup, WidgetViewContainer>();

        private WidgetViewContainer invisibleApplicationRoot;

        private readonly Dictionary<ViewInfo, IWidgetViewContainer> favoriteViewInfoButtons =
    new Dictionary<ViewInfo, IWidgetViewContainer>(); 
        private readonly Dictionary<ViewInfo, IWidgetViewContainer> favoriteViewInfoMenuItems =
            new Dictionary<ViewInfo, IWidgetViewContainer>();
        readonly Dictionary<ViewInfoIconGroup, WidgetViewContainer> favoriteGroupWidgets = new Dictionary<ViewInfoIconGroup, WidgetViewContainer>();
        private IWidgetViewContainer favoriteApplicationWidget;

        private void FillApplicationList()
        {
            var controller = Launcher.m_controller;
            WidgetViewContainer launcherBarArea =
                ((ChromeManagerBase)manager)[ChromeArea.LauncherBar] as WidgetViewContainer;
            WidgetViewContainerHelper.ClearParents(viewInfoButtons.Values);
            WidgetViewContainerHelper.ClearParents(viewInfoMenuItems.Values);
            WidgetViewContainerHelper.ClearParents(groupWidgets.Values);
            WidgetViewContainerHelper.ClearParents(invisibleApplicationWidgets.Values);
            WidgetViewContainerHelper.ClearParents(invisibleGroupWidgets.Values);
            WidgetViewContainerHelper.ClearParents(invisibleViewWidgets.Values);
            WidgetViewContainerHelper.ClearParents(favoriteGroupWidgets.Values);
            WidgetViewContainerHelper.ClearParents(favoriteViewInfoButtons.Values); 
            WidgetViewContainerHelper.ClearParents(favoriteViewInfoMenuItems.Values);
            if (favoriteApplicationWidget != null) favoriteApplicationWidget.ClearParent();
            if (invisibleApplicationRoot != null) invisibleApplicationRoot.ClearParent();
            favoriteApplicationWidget = null;
  
            var applications = controller.GetViewListByApplicationThenEnvironment(
                controller.UserSettings.FilterBySysLoc,
                controller.ExtraUserSettings.DefaultEnvironment, controller.ExtraUserSettings.FavoriteAsFilter, controller.UserSettings.FilterByFavorite);
            IList<ViewInfoGroup> allApplications = new List<ViewInfoGroup>();

            bool filtered = controller.UserSettings.FilterBySysLoc ||
                            (!string.IsNullOrEmpty(controller.ExtraUserSettings.DefaultEnvironment) && controller.AvailableEnvironmentTypes.Count() > 1) ||
                            (controller.ExtraUserSettings.FavoriteAsFilter && controller.UserSettings.FilterByFavorite);
            if (filtered)
            {
                allApplications = controller.GetViewListByApplicationThenEnvironment(false, null, controller.ExtraUserSettings.FavoriteAsFilter, false);
            }
            int count = applications.SelectMany(application_ => application_.ViewInfoIconList).Sum(environmentGroup_ => environmentGroup_.ViewInfoList.Count);

            bool useDropdown = count > MaxIconCount;
            foreach (ViewInfoGroup application in applications)
            {
                var applicationCopied = application;
                WidgetViewContainer applicationContainer =
                    manager[ChromeArea.LauncherBar][application.Name] as
                    WidgetViewContainer;
                if (application.IsFavoritesGroup)
                {
                    favoriteApplicationWidget = applicationContainer;
                }
                foreach (ViewInfoIconGroup environmentGroup in application.ViewInfoIconList)
                { 
                    WidgetViewContainer parent = null;
                    if (useDropdown)
                    {
                        parent = applicationContainer.
                            CreateOrReAdd(environmentGroup, applicationCopied.IsFavoritesGroup ? favoriteGroupWidgets : groupWidgets,
                                          group_ =>
                                          applicationContainer.AddWidget(
                                              group_.Name + "." + Guid.NewGuid().ToString(),
                                              new InitialDropdownButtonParameters()
                                              {
                                                  Image = PathToImageConverter.ConvertToImage(group_.IconPath),
                                                  Text = group_.Name
                                              }
                                              ) as WidgetViewContainer);


                    }
                    else
                    {
                        parent = applicationContainer;
                    }

                    if (useDropdown)
                    {
                        SetContextMenu(parent, application.Name);   
                    }

                    foreach (ViewInfo viewInfo in environmentGroup.ViewInfoList)
                    {
                        var button = parent.CreateOrReAdd(viewInfo,
                                                        applicationCopied.IsFavoritesGroup ? 
                                                        (useDropdown ? favoriteViewInfoMenuItems : favoriteViewInfoButtons) : 
                                                        (useDropdown ? viewInfoMenuItems : viewInfoButtons),
                                                          v_ => CreateViewInfoWidget(v_, parent,
                                                              applicationCopied.IsFavoritesGroup || (controller.ExtraUserSettings.FavoriteAsFilter && controller.UserSettings.FilterByFavorite)));
                        if (!useDropdown)
                        {
                            SetContextMenu(button, application.Name); 
                        }

                    }
                }
            }

            if (allApplications.Count > 0)
            {
                List<ViewInfoGroup> hiddenApplications = new List<ViewInfoGroup>();
                foreach (var application in allApplications)
                {
                    if (applications.FirstOrDefault(app_ => app_.Name == application.Name) == null)
                        hiddenApplications.Add(application);
                }

                if (hiddenApplications.Count == 0) return;

                launcherBarArea.CreateOrReAdd(ref invisibleApplicationRoot, () =>
                                                                            launcherBarArea.AddWidget(new InitialDropdownButtonParameters
                                                                                                          ()
                                                                            {
                                                                                Text =
                                                                                    "Applications filtered out"
                                                                            }) as WidgetViewContainer);
                foreach (var application in hiddenApplications)
                {
                    WidgetViewContainer applicationWidget = invisibleApplicationRoot.CreateOrReAdd(application,
                                                                                                   invisibleApplicationWidgets,
                                                                                                   application_ =>
                                                                                                   invisibleApplicationRoot
                                                                                                       .AddWidget(
                                                                                                           application_
                                                                                                               .Name +
                                                                                                           "." +
                                                                                                           Guid
                                                                                                               .NewGuid
                                                                                                               ()
                                                                                                               .ToString
                                                                                                               (),
                                                                                                           new InitialDropdownButtonParameters
                                                                                                               ()
                                                                                                           {
                                                                                                               Text
                                                                                                                   =
                                                                                                                   application_
                                                                                                           .Name,
                                                                                                               Image
                                                                                                                   =
                                                                                                                   string
                                                                                                                       .IsNullOrEmpty
                                                                                                                       (application_
                                                                                                                            .IconPath)
                                                                                                                       ? null
                                                                                                                       : PathToImageConverter.ConvertToImage(application_.IconPath) 
                                                                                                           }) as
                                                                                                   WidgetViewContainer);

                    foreach (ViewInfoIconGroup envGroup in application.ViewInfoIconList)
                    {
                        WidgetViewContainer groupWidget = applicationWidget.CreateOrReAdd(envGroup,
                                                                                          invisibleGroupWidgets,
                                                                                          group_ =>
                                                                                          applicationWidget
                                                                                              .AddWidget(
                                                                                                  group_.Name + "." +
                                                                                                  Guid.NewGuid()
                                                                                                      .ToString(),
                                                                                                  new InitialDropdownButtonParameters
                                                                                                      ()
                                                                                                  {
                                                                                                      Text =
                                                                                                          group_
                                                                                                  .Name,
                                                                                                      Image = PathToImageConverter.ConvertToImage(group_.IconPath)
                                                                                                  }) as
                                                                                          WidgetViewContainer);

                        foreach (ViewInfo viewInfo in envGroup.ViewInfoList)
                        {
                            groupWidget.CreateOrReAdd(viewInfo, invisibleViewWidgets,
                                                      info_ => CreateViewInfoWidget(info_, groupWidget, true));
                        }
                    }
                }
            }


        }

        private void SetContextMenu(IWidgetViewContainer widget_, string applicationName_)
        {
            var cntxtMgr = new ContextMenuManager
            {
                ModifierKeys = ModifierKeys.Control,
                OpenMode = OpenMode.RightClick
            };
            XamContextMenu hiddenViewMenu = new XamContextMenu
            {
                Placement = PlacementMode.AlignedBelow
            };
            hiddenViewMenu.Tag = applicationName_;
            hiddenViewMenu.Opening += new EventHandler<OpeningEventArgs>(hiddenViewMenu_Opening);
            cntxtMgr.ContextMenu = hiddenViewMenu;

            if (widget_.Content != null)
            {
                Infragistics.Controls.Menus.ContextMenuService.SetManager(
                    widget_.Content as DependencyObject, cntxtMgr);
            }
            else
            {
                widget_.ContentReady +=
                    (sender_, args_) => Infragistics.Controls.Menus.ContextMenuService.SetManager(
                        ((WidgetViewContainer)sender_).Content as DependencyObject, cntxtMgr);
            }
        }

        readonly Dictionary<XamMenuItem, Tuple<EventHandler, PropertyChangedEventHandler>> menuItemEventHandlers = new Dictionary<XamMenuItem, Tuple<EventHandler, PropertyChangedEventHandler>>(); 
        void hiddenViewMenu_Opening(object sender, OpeningEventArgs e)
        {
            XamContextMenu menu = sender as XamContextMenu;
            if (menu == null)
            {
                e.Cancel = true;
                return;
            }
            ClearItems(menu);
            string applicationName = menu.Tag as string;
            if (string.IsNullOrEmpty(applicationName))
            {
                e.Cancel = true;
                return;
            }
            if (!Launcher.Controller.UserSettings.FilterBySysLoc &&
                (string.IsNullOrEmpty(Launcher.Controller.ExtraUserSettings.DefaultEnvironment) ||
                Launcher.Controller.AvailableEnvironmentTypes.Count() == 1))
            {
                e.Cancel = true;
                return;
            }
            var comparer = new EnvironmentGroupComparer();
            List<ViewInfo> hiddenViews = new List<ViewInfo>(Launcher.Controller.GetInvisibleViewList(applicationName,
                                                                                                     Launcher.Controller
                                                                                                             .UserSettings
                                                                                                             .FilterBySysLoc
                                                                                                         ? Launcher
                                                                                                               .Controller
                                                                                                               .Config
                                                                                                               .SysLoc
                                                                                                         : null,
                                                                                                     Launcher.Controller
                                                                                                             .ExtraUserSettings
                                                                                                             .DefaultEnvironment));
            hiddenViews.Sort(
                (v1_, v2_) =>

                    {
                        int result = comparer.Compare(v1_.EnvironmentGroup, v2_.EnvironmentGroup);
                        if (result != 0) return result;
                        return System.String.Compare(v1_.IconDescription, v2_.IconDescription, System.StringComparison.Ordinal);
                    }); 
            if (hiddenViews.Count == 0)
            {
                e.Cancel = true;
                return;
                
            }
            string lastEnvGroupName = null;
            foreach (var hiddenView in hiddenViews)
            {
                if (lastEnvGroupName != hiddenView.EnvironmentGroupName && lastEnvGroupName != null)
                {
                    menu.Items.Add(new XamMenuSeparator());
                }
                lastEnvGroupName = hiddenView.EnvironmentGroupName;
                XamMenuItem item = new XamMenuItem();
                menu.Items.Add(item);
                item.Header = hiddenView.IconDescription;
                item.Icon = new Image { Source = PathToImageConverter.ConvertToImage(hiddenView.IconFileName) };
                EventHandler handler = null;
                var viewCopy = hiddenView;
                item.Click += handler = (sender_, args_) => Launcher.Controller.StartProcess(viewCopy.UniqueKey);

                PropertyChangedEventHandler handler2 = null;
                item.Tag = viewCopy;
                viewCopy.PropertyChanged += handler2 = (o_, eventArgs_) =>
                {
                    item.Header = ((ViewInfo)o_).IconDescription;
                };

                ContextMenu menu2 = contextMenuResource["ContextMenu"] as ContextMenu;
                menu2.DataContext = viewCopy; 
                item.ContextMenu = menu2;

                menuItemEventHandlers[item] = Tuple.Create(handler, handler2);

            }
        }
         
        private void ClearItems(XamContextMenu menu_)
        { 
            for (int i = menu_.Items.Count - 1; i >= 0; i--)
            {
                var menuItem = menu_.Items[i] as XamMenuItem;
                menu_.Items.RemoveAt(i);
                if (menuItem == null) return;
                Tuple<EventHandler, PropertyChangedEventHandler> handlers;
                if (menuItemEventHandlers.TryGetValue(menuItem, out handlers))
                {
                    menuItem.Click -= handlers.Item1;
                    ViewInfo view = menuItem.Tag as ViewInfo;
                    if (view != null) view.PropertyChanged -= handlers.Item2;
                    menuItemEventHandlers.Remove(menuItem);
                }
            }
        }

        private WidgetViewContainer CreateViewInfoWidget(ViewInfo viewInfo_, IWidgetViewContainer parent_, bool hideFavoriteDescription_)
        {
            var viewInfoCopied = viewInfo_;
            var button = parent_.AddWidget(viewInfo_.UniqueKey + "." + Guid.NewGuid().ToString(),
                                           new InitialButtonParameters
                                               {
                                                   Image = PathToImageConverter.ConvertToImage(viewInfo_.IconFileName),
                                                   Click = (s_, e_) => Launcher.Controller.StartProcess(viewInfoCopied.UniqueKey),
                                                   Text = hideFavoriteDescription_ ? viewInfo_.IconDescription : viewInfo_.IconDescriptionWithFavoriteIndicator,
                                                   ToolTip = viewInfo_.ToolTip,
                                               }) as WidgetViewContainer;
            button.ContentReady += (sender_, args_) =>
                {
                    MenuItem item = ((WidgetViewContainer)sender_).Content as MenuItem;
                    if (item != null)
                    {
                        viewInfo_.PropertyChanged += (o_, eventArgs_) =>
                            {
                                item.Header = hideFavoriteDescription_ ?  ((ViewInfo)o_).IconDescription : ((ViewInfo)o_).IconDescriptionWithFavoriteIndicator;
                            };
                    }
                    else
                    {
                        LauncherBarButton launcherButton =((WidgetViewContainer)sender_).Content as LauncherBarButton;
                        if (launcherButton != null)
                        {

                            viewInfo_.PropertyChanged += (o_, eventArgs_) =>
                                {
                                    launcherButton.MSGuiButton.Text = hideFavoriteDescription_ ? ((ViewInfo)o_).IconDescription : ((ViewInfo)o_).IconDescriptionWithFavoriteIndicator;
                                };
                        }
                    }


                    ContextMenu menu = contextMenuResource["ContextMenu"] as ContextMenu;
                    menu.DataContext = viewInfo_;

                    ((FrameworkElement)((WidgetViewContainer)sender_).Content).ContextMenu = menu;

                };

            if (button.Content != null)
            {
                button.Content = button.Content;
            }

            return button;

        }

        

        private void ExecuteTimeConsumingTask(Action action_)
        {
            Window w = Application.Current.MainWindow;
            Cursor previousCursor = null;
            if (w != null)
            {
                previousCursor = w.Cursor;
                w.Cursor = Cursors.Wait;
            }
            try
            {
                action_();
            }
            finally
            {
                if (w != null)
                {
                    w.Cursor = previousCursor;
                }
            }
        }
    }
}