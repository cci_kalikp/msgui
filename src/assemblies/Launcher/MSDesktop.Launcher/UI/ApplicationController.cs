#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2006 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

              $Id: //eai/msdotnet/msgui/trunk/assemblies/Launcher/MSDesktop.Launcher/UI/ApplicationController.cs#45 $
  $DateTime: 2014/09/22 04:32:16 $
    $Change: 897770 $
    $Author: caijin $
_____________________________________________________________________________*/
#endregion File Info Header

using System;
using System.IO;
using System.Linq;
using System.Collections;
using System.Diagnostics;
using System.ComponentModel;
using System.Reflection;
using System.Windows;
using System.Windows.Shell;
using System.Windows.Threading;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using MSDesktop.Launcher.Model;
using MSDesktop.Launcher.Services;
using MorganStanley.MSDotNet.Directory; 
using Application = System.Windows.Application;
using Environment = System.Environment;

namespace MSDesktop.Launcher.UI
{
    using System.Threading;

    /// <summary>
    /// The main application controller.
    /// [Controls interaction between the model and views]
    /// </summary>
    public class ApplicationController
    {
        #region Public Events

        /// <summary>
        /// Delegate for a model changed event
        /// </summary>
        public delegate void ModelChangedEventHandler();

        /// <summary>
        /// Indicates that the model has been changed
        /// </summary>
        public event ModelChangedEventHandler ModelChanged;

        /// <summary>
        /// Would be raised when app is ShuttingDown;
        /// </summary>
        public event CancelEventHandler ShuttingDown;

        public event EventHandler<FavoriteChangedEventArgs> FavoriteChanged; 
        #endregion

        #region Consts

        private const string CLASS = "ApplicationController";
        private static readonly int REFRESH_TIMER_HOURS = 4;
        #endregion

        #region Private Member Variables

        //Original Arguments
        private List<string> _args;

        //The configuration service
        private Services.Configuration _config;

        //The reference to the main about window instance
        private AboutWindow aboutWindow;

        //A hash of the items which are currently available to the user
        private IEnumerable<ViewInfo> _viewInfoList;
        private List<EnvironmentType> _environmentTypes = new List<EnvironmentType>();

        //An attempt to impersonate this userName
        private string _tryUserName;

        //Refresh timer to fire off at specified intervals to refresh application _config
        private DispatcherTimer _refreshTimer;

        //Refresh timer control automatic restart of launcher at midnight each day
        private DispatcherTimer _processRestartTimer;

        //Save the last refresh time
        private DateTime _appConfigUpdateTime;

        private LauncherSettings settings;
        //the users machine specific settings
        private Model.UserSettings.UserSettings _userSettings;

        //extra user settings
        private Model.UserSettings.ExtraUserSettings _extraUserSettings;

        // The actual class responsible for starting processes.
        private readonly ProcessLaunchManager _processLaunchManager;

        // Class to manage start/end times 
        private readonly Scheduler _scheduler; 

        #endregion

        #region Constructors

        /// <summary>
        /// Standard constructor which loads the root configuration
        /// </summary>
        public ApplicationController(string rootConfigFilename_, string userName_,
                                     bool attemptingImpersonation_)
        {

            //Handle any untrapped thread exceptions
            Application.Current.DispatcherUnhandledException += Current_DispatcherUnhandledException;
            AppDomain.CurrentDomain.UnhandledException +=
                new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

            this.CopyLocalReleases = new SortedDictionary<Model.Application, List<string>>();
            //Initialize a new ProcessLaunchManager
            _processLaunchManager = new ProcessLaunchManager(Dispatcher.CurrentDispatcher);
            _scheduler = new Scheduler(_processLaunchManager);

            if (attemptingImpersonation_)
            {
                // first check impersonation is allowed by the system user.
                if (UserCanImpersonate(System.Environment.UserName.ToLower()))
                {
                    // if impersonation is attempted, the passed in user name will be the username to impersonate
                    _tryUserName = userName_;
                    LogLayer.Info.Location(CLASS, ".ctor").Append("Allowing impersonation as user " + userName_).Send();

                }
                else
                {
                    // otherwise use the system username
                    _tryUserName = System.Environment.UserName.ToLower();
                    LogLayer.Info.Location(CLASS, ".ctor")
                            .Append("Failed to impersonate user " + userName_ +
                                    ". Join mailgroup iedlauncher_impersonation to impersonate.")
                            .Send();
                    DialogExtensions.ShowMessage(null,
                                                 String.Format(
                                                     "Failed to impersonate user {0}. Join mailgroup iedlauncher_impersonation to impersonate.{2}{2}Running as yourself ({1}).",
                                                     userName_, System.Environment.UserName.ToLower(),
                                                     Environment.NewLine), "Warning", MessageBoxImage.Warning);

                }
            }
            else
            {
                _tryUserName = userName_;
            }

            _config = new Services.Configuration(rootConfigFilename_);

            //load the user _config first so we can then use it to override any default values in the app _config.
            LoadUserConfig();
            LoadLauncherConfig();
            _args = new List<string>(System.Environment.GetCommandLineArgs());
            _args.RemoveAt(0);
            if (Settings.AllowSingleMachine && !UserCrossMachineMode)
            {
                try
                {
                    Process.Start(
                        new ProcessStartInfo(@"\\san01b\DevAppsGML\dist\ied\PROJ\launcher\prod\IED.Launcher.exe",
                                             string.Join(" ", _args)));
                    Environment.Exit(0);
                }
                catch
                {

                }
            }

            LoadExtraUserConfig();
            // load the LDAP group names this user is a member of 
            List<string> groupNames = LoadLDAPGroups(_tryUserName, true);

            LoadAppConfig(true, groupNames);
            //this is the initial load.  covers auto starting appsscheduled for today but before launcher ran

            //Load up timer to run refresh at midnight and repeat every day.
            TimeSpan refreshStart = TimeSpan.FromHours(REFRESH_TIMER_HOURS);
            StartRefreshTimer(refreshStart);
            StartProcessRestartTimer();
        }

        #endregion

        #region Public Properties

        public string UserName
        {
            get { return _tryUserName; }
        }

        public DateTime AppConfigUpdateTime
        {
            get { return _appConfigUpdateTime; }
        }

        public LauncherSettings Settings
        {
            get { return settings; }
        }
        public Model.UserSettings.UserSettings UserSettings
        {
            get { return _userSettings; }
        }

        public Model.UserSettings.ExtraUserSettings ExtraUserSettings
        {
            get { return _extraUserSettings; }
        }


        // List of Releases that have copy local turned on, by application
        public SortedDictionary<Model.Application, List<string>> CopyLocalReleases { get; private set; }

        public Configuration Config
        {
            get { return _config; }
        }

        #endregion Public Properties

        #region Public Methods

        /// <summary>
        /// Starts a refresh timer in specified timespan to run refresh on a daily basis
        /// </summary>
        /// <param name="startIn_"></param>
        private void StartRefreshTimer(TimeSpan interval_)
        {
            _refreshTimer = new DispatcherTimer(interval_, DispatcherPriority.Normal, RefreshTimerRan,
                                                Dispatcher.CurrentDispatcher);
            _refreshTimer.Start();
            LogLayer.Info.Location(CLASS, "StartRefreshTimer()")
                    .Append("Scheduling new timer instance to kick off in ", interval_.ToString(), " repeating every ",
                            interval_.ToString(), ".")
                    .Send();
        }

        /// <summary>
        /// Starts a refresh timer in specified timespan to run refresh on a daily basis
        /// </summary>
        /// <param name="startIn_"></param>
        private void StartProcessRestartTimer()
        {
            TimeSpan timeToRestart = GetTimespanToRestart();

            _processRestartTimer = new DispatcherTimer(timeToRestart, DispatcherPriority.Normal, RestartTimerRan,
                                                       Dispatcher.CurrentDispatcher);
            _processRestartTimer.Start();

            LogLayer.Info.Location(CLASS, "StartProcessRestartTimer()")
                    .Append(string.Format("Restart scheduled to happen in {0} hours {1} minutes, {2} seconds.",
                                          Math.Floor(timeToRestart.TotalHours),
                                          timeToRestart.Minutes,
                                          timeToRestart.Seconds)).Send();
        }
         

        /// <summary>
        /// Triggers the sending of a ModelChanged() event
        /// </summary>
        public void OnModelChanged(bool initialLoad_, List<string> groupNames_)
        {
            UpdateViewList(groupNames_);

            if (this.ModelChanged != null)
            {
                ModelChanged();
            }

            RefreshMRUList();
            //update the scheduler - pass in the initial load parameter.  If true it will start up
            //any process set to autostart today even if time has passed.  Covers launcher starting on monday morning
            _scheduler.ApplySchedule(initialLoad_);
        }

        private bool refreshingUI = false;
        public bool RefreshingUI
        {
            get { return refreshingUI; }
        }
        /// <summary>
        /// Forces the application configuration to be re-loaded
        /// </summary>
        public void ReloadApplicationConfig()
        {
            try
            {
                if (refreshingUI) return;
                refreshingUI = true;
                try
                {
                    List<string> groupNames = LoadLDAPGroups(_tryUserName, true);
                    LoadAppConfig(false, groupNames);
                }
                finally 
                {

                    refreshingUI = false;
                }

            }
            catch (Exception ex)
            {
                ExceptionViewer.ShowException("Failed while re-loading application configuration.", ex);
            }
        }

        /// <summary>
        /// Forces the user configuration to be re-loaded
        /// </summary>
        public void ReloadUserConfig()
        {
            try
            {
                LoadUserConfig();
                LoadExtraUserConfig();
            }
            catch (Exception ex)
            {
                ExceptionViewer.ShowException("Failed while re-loading user configuration.", ex);
            }
        }

        /// <summary>
        /// Persist the user settings back to disk. 
        /// </summary>
        public void SaveUserConfig()
        {
            try
            {
                Config.WriteUserConfig(_userSettings);
            }
            catch (Exception ex)
            {
                ExceptionViewer.ShowException("Failed while re-saving user configuration.", ex);
            }
        }

        /// <summary>
        /// Persist the extra user settings back to disk. 
        /// </summary>
        public void SaveExtraUserConfig()
        {
            try
            {
                Config.WriteExtraUserConfig(_extraUserSettings);
            }
            catch (Exception ex)
            {
                ExceptionViewer.ShowException("Failed while re-saving extra user configuration.", ex);
            }
        }

        public void DeleteLocalCache()
        {
            LogLayer.Info.Location(CLASS, "DeleteLocalCache").Append("Attempting to delete entire local cache.").Send();

            foreach (ViewInfo info in _viewInfoList)
            {
                this.DeleteLocalCache(info);
            }
        }


        public void DeleteLocalCache(ViewInfo info_)
        {
            try
            {
                LogLayer.Info.Location(CLASS, "DeleteLocalCache").Append("Attempting to delete local cache for " + info_.ApplicationDescription + " " + info_.IconDescription).Send();
                string appPath = info_.GetPath(IEDCodeLocation.COPYLOCAL);

                if (info_.FileSystem == IEDCodeLocation.COPYLOCAL)
                {
                    if (_processLaunchManager.IsProcessStarted(info_) ||
                        _processLaunchManager.IsProcessStarted(info_.GetPath(IEDCodeLocation.COPYLOCAL)))
                    {
                        _processLaunchManager.KillProcess(info_, true);
                        Thread.Sleep(1000); // hold for a second to allow process to die.
                    }

                    if (Directory.Exists(appPath))
                    {
                        System.IO.Directory.Delete(appPath, true);
                        LogLayer.Info.Location(CLASS, "DeleteLocalCache").Append("Deleted local cache for " + info_.ApplicationDescription + " " + info_.IconDescription).Send();
                    }
                    else
                    {
                        LogLayer.Info.Location(CLASS, "DeleteLocalCache").Append("Nothing to delete locally for " + info_.ApplicationDescription + " " + info_.IconDescription).Send();
                    }

                }
            }
            catch (Exception ex)
            {
                ExceptionViewer.ShowException(
                    "Failed while deleting local cache for application: " + info_.ApplicationDescription, ex);
            }
        }

        public void AddAutoStartOverride(ViewInfo info_, bool autoStart_)
        { 
            LogLayer.Info.Location(CLASS, "AddAutoStartOverride")
                    .Append(
                        String.Format(
                            "Attempting to add user override autostart on application {0}.  Override value is: {1}",
                            info_.ApplicationAndIconDescription, autoStart_.ToString()))
                    .Send();

            if (info_.AutoStart == null)
            {
                info_.AutoStart = new Model.AutoStart(info_.UniqueKey, autoStart_);
            }
            else
            {
                info_.AutoStart.Value = autoStart_;
            }

            _userSettings.UpdateAutoStartOverride(info_.AutoStart, info_.UniqueKey, autoStart_);
            SaveUserConfig();

            var app = Config.Root.GetApplicationByKey(info_.ApplicationKey);
            if (app == null) return;
            
            //update scheduler - either remove/add process
            _scheduler.PrepareSchedule(info_, app.GetTimeGroup(info_.TimeGroup));

            //apply changes.
            _scheduler.ApplySchedule(false);
        }

        public void AddImpersonationOverride(ViewInfo info_, bool impersonate_)
        { 
            LogLayer.Info.Location(CLASS, "AddImpersonationOverride")
                    .Append(
                        String.Format(
                            "Attempting to add user override impersonate on application {0}.  Override value is: {1}",
                            info_.ApplicationAndIconDescription, impersonate_.ToString()))
                    .Send();

            if (info_.Impersonation == null)
            {
                LogLayer.Info.Location(CLASS, "AddImpersonationOverride")
                        .Append(String.Format("Cannot add impersonate setting when one doesn't already exist."))
                        .Send();
            }
            else
            {
                info_.Impersonation.Value = impersonate_;
            }

            _userSettings.UpdateImpersonationOverride(info_.UniqueKey, impersonate_);
            SaveUserConfig();
        }


        public void AddFavoriteOverride(ViewInfo info_, bool favorite_)
        {
            LogLayer.Info.Location(CLASS, "AddFavoriteOverride").Append(String.Format("Attempting to add user override favorite on application {0}.  Override value is: {1}", info_.ApplicationAndIconDescription, favorite_.ToString())).Send();

            if (info_.Favorite == null)
            {
                info_.Favorite = new Model.Favorite(info_.UniqueKey, favorite_);
            }
            else
            {
                info_.Favorite.Value = favorite_;
            }

            _userSettings.UpdateFavoriteOverride(info_.Favorite, info_.UniqueKey, favorite_);
            UpdateFavoriteView(info_);
            SaveUserConfig();
        }

        private void UpdateFavoriteView(ViewInfo viewInfo_)
        { 
            var copy = FavoriteChanged;
            if (copy != null)
            {
                copy(this, new FavoriteChangedEventArgs(viewInfo_));
            }
            if (currentViews == null) return; 
            
            var favoriteGroup = currentViews[0];
            if (viewInfo_.FavoriteValue)
            {
                if (!favoriteGroup.IsFavoritesGroup)
                {
                    favoriteGroup = new ViewInfoGroup(true); 
                    favoriteGroup.ViewInfoIconList = new ObservableCollection<ViewInfoIconGroup>(); 
                    currentViews.Insert(0, favoriteGroup);
                } 
            }
            if (!favoriteGroup.IsFavoritesGroup) return;
            var favoriteApplication =
                     favoriteGroup.ViewInfoIconList.FirstOrDefault(g_ => g_.Name == viewInfo_.ApplicationDescription);
            if (favoriteApplication == null)
            {
                favoriteApplication = new ViewInfoIconGroup();
                favoriteApplication.ViewInfoGroupName = favoriteGroup.Name;
                favoriteApplication.EnvironmentType = EnvironmentType.Prod; //make it to be expanded by default
                favoriteApplication.Name = viewInfo_.ApplicationDescription; // Name is Application Description (what we grouped by) 
                favoriteApplication.ViewInfoList = new ObservableCollection<ViewInfo>();
                favoriteApplication.IconPath = (!String.IsNullOrEmpty(viewInfo_.AppIconPath)
                                                    ? viewInfo_.AppIconPath
                                                    : viewInfo_.IconFileName);
                List<ViewInfoIconGroup> groups = new List<ViewInfoIconGroup>(favoriteGroup.ViewInfoIconList);
                groups.Add(favoriteApplication);
                groups.Sort((g1_, g2_) => System.String.Compare(g1_.Name, g2_.Name, System.StringComparison.Ordinal));
                int index = groups.IndexOf(favoriteApplication);
                if (index < groups.Count - 1)
                {
                    favoriteGroup.ViewInfoIconList.Insert(index, favoriteApplication);
                }
                else
                {
                    favoriteGroup.ViewInfoIconList.Add(favoriteApplication);
                } 
            }

            if (viewInfo_.FavoriteValue)
            {
                EnvironmentGroupComparer comparer = new EnvironmentGroupComparer();
                List<ViewInfo> items = new List<ViewInfo>(favoriteApplication.ViewInfoList);
                items.Add(viewInfo_);
                items.Sort((v1_, v2_) => 
                    {
                        int result = comparer.Compare(v1_.EnvironmentGroup, v2_.EnvironmentGroup);
                        if (result != 0) return result; 
                        if (v1_.EnvironmentGroup == v2_.EnvironmentGroup)
                        {
                            return System.String.Compare(v1_.EnvironmentName, v2_.EnvironmentName, System.StringComparison.Ordinal);
                        }
                        return items.IndexOf(v1_) - items.IndexOf(v2_);
                    }
                    );
                int index = items.IndexOf(viewInfo_);
                if (index < items.Count - 1)
                { 
                    favoriteApplication.ViewInfoList.Insert(index, viewInfo_); 
                }
                else
                {
                    favoriteApplication.ViewInfoList.Add(viewInfo_);
                } 
            }
            else
            {
                favoriteApplication.ViewInfoList.Remove(viewInfo_);
                if (favoriteApplication.ViewInfoList.Count == 0)
                {
                    favoriteGroup.ViewInfoIconList.Remove(favoriteApplication);
                    if (favoriteGroup.ViewInfoIconList.Count == 0)
                    {
                        currentViews.Remove(favoriteGroup);
                    }
                }
                else  
                {
                    favoriteApplication.IconPath = (!String.IsNullOrEmpty(favoriteApplication.ViewInfoList[0].AppIconPath)
                                    ? favoriteApplication.ViewInfoList[0].AppIconPath
                                    : favoriteApplication.ViewInfoList[0].IconFileName); 
                }
            }
        }
        public void ClearAllAutoStartOverrides()
        {
            _userSettings.ClearAutoStarts();
            SaveUserConfig();
        }

        /// <summary>
        /// Shows the about box to the user
        /// </summary>
        public void ShowAboutForm(System.Windows.Window owner_, bool showInBottomRightOfScreen_)
        {
            if (aboutWindow == null)
            {
                aboutWindow = new AboutWindow(this);
                aboutWindow.Parameters.Owner = owner_;
                if (owner_ == null)
                {
                    aboutWindow.Parameters.StartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
                }
                else if (showInBottomRightOfScreen_)
                {
                    aboutWindow.Parameters.StartupLocation = System.Windows.WindowStartupLocation.Manual;
                    aboutWindow.Parameters.Left = SystemParameters.PrimaryScreenWidth - (aboutWindow.Width);
                    aboutWindow.Parameters.Top = SystemParameters.PrimaryScreenHeight - (aboutWindow.Height);
                }
                else
                {
                    aboutWindow.Parameters.StartupLocation = System.Windows.WindowStartupLocation.CenterOwner;
                }

                aboutWindow.ShowDialog();
                aboutWindow = null;
            }
        }

 

        /// <summary>
        /// Shows a fatal exception to the user, and then exits the application
        /// </summary>
        /// <param name="currentActivity_"></param>
        /// <param name="ex_"></param>
        public void ShowFatalException(string currentActivity_, Exception ex_)
        {
            ExceptionViewer.ShowFatalException(currentActivity_, ex_);
            this.OnShuttingDown(new CancelEventArgs());
        }

        /// <summary>
        /// Start a new process based on the key of a view info item.
        /// </summary>
        /// <param name="key_">A valid view info key.</param>
        public void StartProcess(string key_)
        {
            if (key_ != null)
            {
                ViewInfo info = GetViewInfoForKey(key_);
                StartProcess(info);
            }
        }

        public void StartProcess(ViewInfo info_)
        {
            if (info_ != null)
            {
                LogLayer.Info.Location(CLASS, "StartProcess")
                        .Append(String.Format("User requested to start application {0}.",
                                              info_.ApplicationAndIconDescription))
                        .Send();
                _processLaunchManager.StartProcess(info_, true);
                this.UpdateJumpList(info_);  
            }
        }

        public bool SyncStartProcess(ViewInfo viewInfo_, bool alwaysNewApp_, out string instanceName_)
        {
            instanceName_ = null; 
            if (!alwaysNewApp_ &&
                (_processLaunchManager.IsProcessStarted(viewInfo_) || _processLaunchManager.IsProcessDownloading(viewInfo_)))
            {
                return _processLaunchManager.IsProcessReadyForIPC(viewInfo_);
            }
            LogLayer.Info.Location(CLASS, "StartProcess")
                    .Append(String.Format("User requested to start application {0} by email link.",
                                          viewInfo_.ApplicationAndIconDescription))
                    .Send();
            var processManager =_processLaunchManager.StartProcess(viewInfo_);
            //if (Application.Current == null) return false;
            //Application.Current.Dispatcher.Invoke(new Action(() => _processLaunchManager.StartProcess(info)),
            //                                      DispatcherPriority.Normal);
            if (!alwaysNewApp_) return IsProcessReadyForIPC(viewInfo_);
            if (processManager != null && processManager.WaitForIPCReady)
            {
                instanceName_ = processManager.Instance;
                return true;
            }
            return false;
        }
         
        /// <summary>
        /// Retrieves an array of (filtered) view info items grouped by application
        /// </summary>
        /// <param name="filterByLocation_">Determines if the list is filtered based on workstation location.</param>
        /// <returns>An array of view info items for displaying to the user.</returns>
        internal IEnumerable<IGrouping<string, ViewInfo>>  GetVisibleViewList(bool filterByLocation_, string environmentType_, bool filterByFavorites_)
        {
 
            return GetVisibleViewList(new Predicate<ViewInfo>(viewInfo_ =>
                {
                    if (viewInfo_.IsHidden) return false;
                    if (!string.IsNullOrEmpty(environmentType_) && !viewInfo_.EnvironmentType.Equals(environmentType_))
                        return false;
                    if (filterByFavorites_ && !viewInfo_.FavoriteValue) return false;
                        return !filterByLocation_ || viewInfo_.SysLocs == null ||
                           (viewInfo_.SysLocs != null && viewInfo_.SysLocs.Contains(Config.SysLoc));
                })); 
        }

        internal IEnumerable<IGrouping<string, ViewInfo>> GetVisibleViewList(Predicate<ViewInfo> filter_)
        {
            return _viewInfoList
                .Where(viewInfo_ => filter_(viewInfo_))
                .GroupBy(viewInfo_ => viewInfo_.ApplicationDescription)
                .OrderBy(app_ => app_.Key); // sort alphabetically by Application name
        }

        internal IEnumerable<ViewInfo> GetInvisibleViewList(string application_, SysLoc locationFilter_, string environmentTypeFilter_)
        {
            if (locationFilter_ == null && string.IsNullOrEmpty(environmentTypeFilter_)) return new List<ViewInfo>();
            return _viewInfoList.Where(v_ =>

                {
                    if (System.String.Compare(v_.ApplicationDescription, application_, System.StringComparison.Ordinal) != 0) return false;
                    if (locationFilter_ != null)
                    {
                        if (v_.SysLocs == null || !v_.SysLocs.Contains(locationFilter_)) return true;
                    }
                    if (!string.IsNullOrEmpty(environmentTypeFilter_))
                    {
                        if (!v_.EnvironmentType.Equals(environmentTypeFilter_)) return true;
                    }
                    return false;
                }
                );
        }
        internal IEnumerable<EnvironmentType> AvailableEnvironmentTypes
        {
            get { return _environmentTypes; }
        }


        #endregion

        #region Private Helper Methods

        /// <summary>
        /// Returns time to midnight plus random number of seconds from 1 to 600 (10 mins)
        /// </summary>
        /// <returns></returns>
        private TimeSpan GetTimespanToRestart()
        {
            //Get time for Midnight
            DateTime mightnightTonight = DateTime.Today.AddDays(1);
            //Get timespan from Now until midnight
            TimeSpan timeUntilMidnight = mightnightTonight.Subtract(DateTime.Now);

            //Get Random number of second within a range of 10 minutes
            Random r = new Random();
            return timeUntilMidnight.Add(TimeSpan.FromSeconds(r.Next(1, 600)));
        }

        /// <summary>
        /// Retrieve a view info item based on a key.
        /// </summary>
        /// <param name="key_">A valid view info key.</param>
        /// <returns>The matching view info item. </returns>
        internal ViewInfo GetViewInfoForKey(string key_)
        {
            return _viewInfoList.FirstOrDefault(viewInfo => viewInfo.UniqueKey == key_);
        }

        internal bool IsProcessStarted(ViewInfo viewInfo_)
        {
            return _processLaunchManager.IsProcessStarted(viewInfo_);
        }

        internal bool IsProcessReadyForIPC(ViewInfo viewInfo_)
        {
            return _processLaunchManager.IsProcessReadyForIPC(viewInfo_);
        }

        internal string GetInstanceName(string viewInfoKey_)
        {
            ViewInfo info = GetViewInfoForKey(viewInfoKey_);
            if (info == null) return null;
            var process = _processLaunchManager.GetProcess(info);
            return process == null ? null : process.Instance;
        } 

        internal Process GetRunningInstance(string viewInfoKey_)
        {
            ViewInfo info = GetViewInfoForKey(viewInfoKey_);
            if (info == null) return null;
            var process = _processLaunchManager.GetProcess(info);
            if (process != null &&  process.Started)
            {
                return process.Process;
            }
            return null;
             
        }

        private IList<ViewInfoGroup> currentViews = null;
        
        internal IList<ViewInfoGroup> GetViewListByApplicationThenEnvironment(bool filterByLocation_, string environmentType_, bool favoriteAsFilter_, bool filterByFavorite_)
        { 
            var groupedByApplication = GetVisibleViewList(filterByLocation_, environmentType_, favoriteAsFilter_ && filterByFavorite_);

            IList<ViewInfoGroup> collection = null;
            ViewInfoGroup favoriteGroup = null; 
            if (!favoriteAsFilter_)
            {
                collection = new ObservableCollection<ViewInfoGroup>();
                favoriteGroup = new ViewInfoGroup(true)
                {
                    ViewInfoIconList = new ObservableCollection<ViewInfoIconGroup>()
                };
                collection.Add(favoriteGroup); 
                currentViews = collection;
            }
            else
            {
                collection = new List<ViewInfoGroup>();
                currentViews = null;
            }

            foreach (var applicationGroup in groupedByApplication)
            {
                ViewInfoGroup group = new ViewInfoGroup();
                group.Name = applicationGroup.Key; // Name is Application Description (what we grouped by) 
                group.ViewInfoIconList = new List<ViewInfoIconGroup>();
                    // create an empty list of environments which will be populate below.

                var groupedByEnvironment = applicationGroup
                    .GroupBy(viewInfo => viewInfo.EnvironmentGroup) // group this application by environment
                    .OrderBy(envGroup => envGroup.Key, new EnvironmentGroupComparer());
                    // sort with our custom sorted to put environments in right order

                List<ViewInfo> favoriteItems = new List<ViewInfo>();
                foreach (var environmentGroup in groupedByEnvironment)
                {
                    ViewInfoIconGroup iconGroup = new ViewInfoIconGroup();
                    iconGroup.ViewInfoGroupName = group.Name;
                    iconGroup.EnvironmentType = environmentGroup.Key.EnvironmentType;
                    iconGroup.ViewInfoList = environmentGroup.ToList(); 
                    ViewInfo firstViewInfoInGroup = environmentGroup.First();
                    iconGroup.IconPath = !string.IsNullOrEmpty(firstViewInfoInGroup.EnvironmentGroup.IconPath)
                                             ? firstViewInfoInGroup.EnvironmentGroup.IconPath
                                             : (!String.IsNullOrEmpty(firstViewInfoInGroup.AppIconPath)
                                                    ? firstViewInfoInGroup.AppIconPath
                                                    : firstViewInfoInGroup.IconFileName);
                    if (string.IsNullOrEmpty(group.IconPath) && !string.IsNullOrEmpty(iconGroup.IconPath))
                    {
                        group.IconPath = iconGroup.IconPath;
                    }
                        // already bucketed so just dump all elements into a list
                    iconGroup.Name = environmentGroup.Key.Name; // we grouped by EnvrionmentGroup so that is the key
                    group.ViewInfoIconList.Add(iconGroup);
                    if (!favoriteAsFilter_)
                    {
                       favoriteItems.AddRange(environmentGroup.Where(v_=>v_.FavoriteValue));
                    }
                }

                if (favoriteItems.Count > 0 && favoriteGroup != null)
                {
                    ViewInfoIconGroup favoriteApplication = new ViewInfoIconGroup();
                    favoriteApplication.ViewInfoGroupName = favoriteGroup.Name;
                    favoriteApplication.EnvironmentType = EnvironmentType.Prod; //make it to be expanded by default
                    favoriteApplication.Name = applicationGroup.Key; // Name is Application Description (what we grouped by) 
                    favoriteApplication.ViewInfoList = new ObservableCollection<ViewInfo>(favoriteItems);
                    favoriteApplication.IconPath = (!String.IsNullOrEmpty(favoriteItems[0].AppIconPath)
                                                        ? favoriteItems[0].AppIconPath
                                                        : favoriteItems[0].IconFileName);
                    favoriteGroup.ViewInfoIconList.Add(favoriteApplication); 
                }
                collection.Add(group);
            }
             if (favoriteGroup != null && favoriteGroup.ViewInfoIconList.Count == 0)
             {
                 collection.RemoveAt(0);
             }
            return collection;
        }


        internal IEnumerable<ViewInfo> GetVisibleViewListSortedByApplicationAndEnvironment(bool filterByLocation_,
                                                                                   string environmentType_, 
                                                                                   bool filterByFavorites_)
        {
            var groupedByApplication = GetVisibleViewList(filterByLocation_, environmentType_, filterByFavorites_);

            List<ViewInfo> sortedList = new List<ViewInfo>();

            foreach (var applicationGroup in groupedByApplication)
            {
                var environmentGroupList = applicationGroup
                    .GroupBy(viewInfo => viewInfo.EnvironmentGroup) // group this application by environment
                    .OrderBy(envGroup => envGroup.Key, new EnvironmentGroupComparer());
                // sort with our custom sorted to put environments in right order

                foreach (var environmentGroup in environmentGroupList)
                {
                    sortedList.AddRange(environmentGroup.ToList());
                }
            }

            return sortedList;
        }

        private void UpdateViewList(List<string> groupNames_)
        {
            IList<ViewInfo> list = new List<ViewInfo>(); 
            _environmentTypes.Clear();
            CopyLocalReleases.Clear();

            Model.Root root = Config.Root;
            lock (root.SyncRoot)
            {
                foreach (Model.Application app in root.Applications)
                {
                    List<string> executables = null;
                    foreach (Model.ActiveRelease release in app.ActiveReleases)
                    {
                        if (release.CodeLocation == IEDCodeLocation.COPYLOCAL)
                        {
                            if (executables == null)
                            {
                                executables = new List<string>();
                                CopyLocalReleases.Add(app, executables);
                                executables.Add(release.Executable);
                            }
                            else if (!executables.Contains(release.Executable))
                            {
                                executables.Add(release.Executable);
                            }
                        }
                    }

                    foreach (Model.EnvironmentGroup group in app.EnvironmentGroups)
                    { 

                        foreach (Model.Environment env in group.Environments)
                        {
                            bool isEntitledTo = false;
                            if (env.Mailgroups != null)
                            {
                                foreach (string mailgroup in env.Mailgroups)
                                {
                                    if (groupNames_.Contains(mailgroup))
                                    {
                                        isEntitledTo = true;
                                        LogLayer.Info.Location(CLASS, "UpdateViewList")
                                                .Append(
                                                    String.Format(
                                                        "User found in group {0} for application {1} environment {2}",
                                                        mailgroup, app.Description, env.Name))
                                                .Send();
                                        break;
                                    }
                                }
                            }

                            if (isEntitledTo)
                            {
                                // Lookup to see if alternate release is active (i.e. user is in one of the alternate groups).
                                bool isInAlternateGroup = false;
                                if (env.AlternateVersion != null)
                                {
                                    foreach (string mailgroup in env.AlternateVersion.LdapGroupsSplit)
                                    {
                                        if (groupNames_.Contains(mailgroup))
                                        {
                                            isInAlternateGroup = true;
                                            LogLayer.Info.Location(CLASS, "UpdateViewList")
                                                    .Append(
                                                        String.Format(
                                                            "User found in group {0} for alternate version {1}",
                                                            mailgroup, env.AlternateVersion.Value))
                                                    .Send();
                                            break;
                                        }
                                    }
                                }

                                ActiveRelease release = app.GetRelease(env.Version);
                                if (isInAlternateGroup)
                                {
                                    // if in the alternate group, set the release up to be the alternate one.
                                    release = app.GetRelease(env.AlternateVersion.Value);
                                }

                                if (!_environmentTypes.Contains(group.EnvironmentType))
                                {
                                    _environmentTypes.Add(group.EnvironmentType);
                                }
                                ViewInfo info = new ViewInfo(app, group, env, release, isInAlternateGroup, _config, list);

                                if (info.Impersonation != null)
                                {
                                    info.Impersonation.Groups = groupNames_;
                                }

                                if (info.AutoStart != null)
                                {
                                    // set the original value now, before the user override happens
                                    info.AutoStart.OriginalValue = info.AutoStart.Value;
                                }
                                if (info.Favorite != null)
                                {
                                    // set the original value now, before the user override happens
                                    info.Favorite.OriginalValue = info.Favorite.Value;
                                }

                                bool? autoStartOverride = _userSettings.GetAutoStartOverride(info);
                                if (autoStartOverride != null)
                                {
                                    info.AutoStart.Value = autoStartOverride.Value;
                                    LogLayer.Info.Location(CLASS, "UpdateViewList")
                                            .Append(
                                                String.Format(
                                                    "Using user override for autostart on application {0}.  Override value is: {1}",
                                                    info.ShortcutDescription, autoStartOverride.Value.ToString()))
                                            .Send();
                                }

                                bool? impersonationOverride = _userSettings.GetImpersonationOverride(info);
                                if (impersonationOverride != null)
                                {
                                    if (!info.Impersonation.Visible)
                                    {
                                        // what has happened here is a previously available impersonation is not, e.g. because it was removed, so delete it from user config
                                        _userSettings.UpdateImpersonationOverride(info.UniqueKey, false);
                                        SaveUserConfig();
                                        LogLayer.Info.Location(CLASS, "UpdateViewList")
                                                .Append(
                                                    String.Format(
                                                        "Removing user override for impersonation on application {0}.",
                                                        info.ShortcutDescription))
                                                .Send();
                                    }
                                    else
                                    {
                                        info.Impersonation.Value = impersonationOverride.Value;
                                        LogLayer.Info.Location(CLASS, "UpdateViewList").Append(
                                            String.Format(
                                                "Using user override for impersonation on application {0}.  Override value is: {1}",
                                                info.ShortcutDescription, impersonationOverride.Value.ToString()))
                                                .Send();
                                    }
                                }
                                bool? favoriteOverride = _userSettings.GetFavoriteOverride(info.UniqueKey);
                                if (favoriteOverride != null)
                                {
                                    info.Favorite.Value = favoriteOverride.Value;
                                    LogLayer.Info.Location(CLASS, "UpdateViewList").Append(String.Format("Using user override for favorite on application {0}.  Override value is: {1}", info.ShortcutDescription, favoriteOverride.Value.ToString())).Send();
                                }

                                try
                                {
                                    list.Add(info);
                                }
                                catch (Exception ex_)
                                {
                                    throw new ApplicationException(
                                        String.Format(
                                            "More than one environment in {0} has auto start iconId attribute set to {1}.  {0} application config must be fixed.",
                                            app.ID, info.UniqueKey), ex_);
                                }
                                 

                                //set up the launcher scheduler
                                _scheduler.PrepareSchedule(info, app.GetTimeGroup(env.AllowedTimeGroup));
                            }
                        }
                    }
                }
            }

            _environmentTypes.Sort();
            _viewInfoList = list;
             
        }

        private void LoadLauncherConfig()
        {
            Config.ReadLauncherConfig();
            settings = Config.Settings;
        }
        /// <summary>
        /// Load the application configuration
        /// [Throws a number of different exception].
        /// </summary>
        private void LoadAppConfig(bool initialLoad_, List<string> groupNames_)
        {
            Config.ReadRootConfig(groupNames_);
            settings = Config.Settings;
            OnModelChanged(initialLoad_, groupNames_);
            //update the time the _config was refreshed.
            _appConfigUpdateTime = DateTime.Now;
        }

        private void LoadUserConfig()
        {
            try
            {
                _userSettings = Config.ReadUserConfig();
            }
            catch (Exception ex_)
            {
                LogLayer.Error.Location(CLASS, "LoadUserConfig").Append(ex_).Send();
            }
        }

        private void LoadExtraUserConfig()
        {
            try
            {
                _extraUserSettings = Config.ReadExtraUserConfig();
            }
            catch (Exception ex_)
            {
                LogLayer.Error.Location(CLASS, "LoadExtraUserConfig").Append(ex_).Send();
            }
        }

        private List<string> LoadLDAPGroups(string userName_, bool logUserGroups_)
        {
            string backupGroupListFilePath = Path.Combine(Launcher.BackupFolder, "LdapGroupList.txt");
            List<string> groupList = new List<string>();
            try
            {
                using (Search s = new FWD2Search())
                {
                    s.Username = "cn=ied.launcher, ou=ldapids, o=Morgan Stanley";
                    s.Password = "launcher01";
                    s.SearchResultSize = 10000;
                    LogLayer.Info.Location(CLASS, "LoadLDAPGroups").Append("Starting LDAP query.").Send();
                    //groupList.Add("iedlauncher_prada");
                    //groupList.Add("ieddailystatus");
                    //groupList.Add("iedpro");
                    //groupList.Add("ied-mcr");
                    //groupList.Add("iedadam");
                    //groupList.Add("ied_jap");
                    //groupList.Add("iedcoach");
                    //groupList.Add("iedlauncher_batman_beta");
                    //groupList.Add("iedlauncher_batman_pilot_hk");
                    //groupList.Add("iedlauncher_batman_pilot_ln");
                    //groupList.Add("iedlauncher_batman_pilot_ny");
                    //groupList.Add("iedlauncher_batman_pilot_tk");
                    //groupList.Add("iedlauncher_batman_qa");
                    //groupList.Add("iedlauncher_iceman_eu_prod");
                    //groupList.Add("iedlauncher_iceman_eu_prodpilot");
                    //groupList.Add("iedlauncher_iceman_hk_prod");
                    //groupList.Add("iedlauncher_iceman_hk_prodpilot");
                    //groupList.Add("iedlauncher_iceman_ny_prod IED");
                    //groupList.Add("iedlauncher_iceman_ny_prodpilot");
                    //groupList.Add("iedlauncher_iceman_qa");
                    //groupList.Add("iedlauncher_mmq_hk_exo_prod");
                    //groupList.Add("iedlauncher_mmq_hk_nextgen_beta");
                    //groupList.Add("iedlauncher_mmq_hk_nextgen_sales_prod");
                    //groupList.Add("iedlauncher_mmq_hk_nextgen_trading_prod");
                    //groupList.Add("iedlauncher_mmq_hk_somm_prod");
                    //groupList.Add("iedlauncher_mmq_ln_exo_clean_prod");
                    //groupList.Add("iedlauncher_mmq_ln_exo_quoting_beta");
                    //groupList.Add("iedlauncher_mmq_ln_exo_quoting_prod");
                    //groupList.Add("iedlauncher_mmq_ln_exo_sales_prod");
                    //groupList.Add("iedlauncher_mmq_ln_exo_trading_beta");
                    //groupList.Add("iedlauncher_mmq_ln_exo_trading_prod");
                    //groupList.Add("iedlauncher_mmq_ln_exo_valuations_prod");
                    //groupList.Add("iedlauncher_mmq_ln_nextgen_beta");
                    //groupList.Add("iedlauncher_mmq_ln_nextgen_sales_prod");
                    //groupList.Add("iedlauncher_mmq_ln_nextgen_trading_prod");
                    //groupList.Add("iedlauncher_pharos_dev");
                    //groupList.Add("iedlauncher_pharos_prod");
                    //groupList.Add("iedlauncher_pharos_prod");
                    //groupList.Add("iedlauncher_pharos_qa");
                    //groupList.Add("iedlauncher_rfqworkshop_qa");
                    //groupList.Add("iedlauncher_salesdesktop_prodna");
                    //groupList.Add("iedlauncher_tic_prodna");
                    //groupList.Add("iedlauncher_tic_qana");
                    //groupList.Add("traderdesktop_users");
                    //groupList.Add("riskview-users");
                    //groupList.Add("riskvision_dev");
                    //groupList.Add("cfm-products");
                    //groupList.Add("vistagui");
                    //groupList.Add("vista_admins");
                    //groupList.Add("wasp-admin");
                    //groupList.Add("cartman-dev");
                    //groupList.Add("estariedhk");
                    //groupList.Add("marketmonitor_perm_admin");
                    //groupList.Add("bcm-dev");
                    //Clean out each time so that on reload we reset (e.g. if user has been removed from a group).
                    Person ppp = s.GetPersonByLogon(userName_); //Gets the Person with the Logon username 
                    string primaryid = s.GetPersonPrimaryID(ppp); //Gets the msfwID for the Person
                    Hashtable groups = s.GetGroupsByUser(primaryid); //Gets the groups w.r.t to the msfwID  

                    LogLayer.Info.Location(CLASS, "LoadLDAPGroups")
                            .Append(string.Format("User {0} has {1} groups associated.",
                                                  userName_, groups.Count)).Send();

                    foreach (Group group in groups.Values)
                    {
                        groupList.Add(group.GroupName);

                        if (logUserGroups_ && group.GroupName.StartsWith("iedlauncher"))
                        {
                            LogLayer.Info.Location(CLASS, "LoadLDAPGroups")
                                    .Append(string.Format("Found launcher LDAP group {0}",
                                                          group.GroupName)).Send();
                        }
                    }
                }
            }
            catch (Exception ex_)
            {
                LogLayer.Error.Location(CLASS, "LoadLDAPGroups")
                        .Append(string.Format("{0}{1}{2}", ex_, Environment.NewLine,
                                              ex_.StackTrace)).Send();

                if (File.Exists(backupGroupListFilePath))
                {
                    LogLayer.Info.Location(CLASS, "LoadLDAPGroups")
                            .Append(string.Format("Reading backup list of LDAP groups from " + backupGroupListFilePath))
                            .Send();

                    using (StreamReader reader = File.OpenText(backupGroupListFilePath))
                    {
                        while (!reader.EndOfStream)
                        {
                            groupList.Add(reader.ReadLine());
                        }
                    }
                }
                else
                {
                    ExceptionViewer.ShowFatalException(
                        "Could not get list of the users LDAP groups, and there is no backup.", null);
                }
            }

            try
            {
                Launcher.EnsureDirectoryExists(Launcher.BackupFolder);

                // Delete the old eclipse backup config.  After this has been released to prod, this can be removed.
                if (File.Exists(Path.Combine(Launcher.BackupFolder, "Entitlements.config")))
                {
                    File.Delete(Path.Combine(Launcher.BackupFolder, "Entitlements.config"));
                }

                if (groupList.Count > 0)
                {
                    // Delete the old
                    if (File.Exists(backupGroupListFilePath))
                    {
                        File.Delete(backupGroupListFilePath);
                    }

                    // Create new
                    using (StreamWriter stream = File.CreateText(backupGroupListFilePath))
                    {
                        stream.AutoFlush = true;
                        foreach (string group in groupList)
                        {
                            stream.WriteLine(group);
                        }
                    }
                }
            }
            catch (Exception ex_)
            {
                LogLayer.Error.Location(CLASS, "LoadLDAPGroups")
                        .Append(string.Format("{0}{1}{2}", ex_, Environment.NewLine, ex_.StackTrace))
                        .Send();
            }
            return groupList;
        }


        private bool UserCanImpersonate(string userName_)
        {
            //TODO: NOTE: for test please remove it~~~~~~~~~~~~~~~~~~~~~~
            //if (userName_ == "rpnmail") return true;
            List<string> groupList = LoadLDAPGroups(userName_, false);
            return groupList.Contains("iedlauncher_impersonation");
        }

        public bool UserCrossMachineMode
        {
            get
            {
                List<string> groupList = LoadLDAPGroups(Environment.UserName.ToLower(), false);
                return groupList.Contains("iedlauncher_crossmachine");
            }
        }


        public event EventHandler<MRUListChangedEventArgs> MRUListChanged; 
        private List<string> jumpList = new List<string>();

        internal void UpdateJumpList(ViewInfo info_)
        {
            if (info_.EnvironmentGroup == null ||  //started by run package 
                info_.IsHidden)
            {
                return;
            }
            if (System.Environment.OSVersion.Version.Major > 5) // not on XP
            {
                if (!jumpList.Contains(info_.UniqueKey))
                {
                    Process myProcess = Process.GetCurrentProcess();
                    JumpTask task = new JumpTask();
                    task.ApplicationPath = myProcess.MainModule.FileName;
                    task.Arguments = "startApp " + info_.UniqueKey;
                    task.CustomCategory = info_.ApplicationDescription;
                    task.Description = info_.ApplicationAndIconDescription;
                    task.Title = info_.IconDescription;
                    task.WorkingDirectory = info_.WorkingDirectory;
                    task.IconResourcePath = info_.IconFileName;

                    JumpList list = JumpList.GetJumpList(App.Current);
                    list.JumpItems.Add(task);
                    list.Apply();
                    jumpList.Add(info_.UniqueKey);
                }
                else
                {
                    JumpList list = JumpList.GetJumpList(App.Current);
                    foreach (JumpTask task in list.JumpItems)
                    {
                        if (task.Description == info_.ApplicationAndIconDescription)
                        {
                            JumpList.AddToRecentCategory(task);
                            list.Apply();
                        }
                    }
                }
            }
            UpdateMRUList(info_);
        }

        private List<ViewInfo> mruList = new List<ViewInfo>();
        private const int MAX_MRU_ITEMS = 5;

        private void RefreshMRUList()
        {
            EventHandler<MRUListChangedEventArgs> copy = null;
            for (int i = mruList.Count - 1; i >= 0; i--)
            {
                ViewInfo info = mruList[i];
                if (!this._viewInfoList.Contains(info))
                {
                    mruList.RemoveAt(i);
                    copy = MRUListChanged;
                    if (copy != null)
                    {
                        copy(this, new MRUListChangedEventArgs(info, ListChangeAction.Remove));
                    } 
                }
            }
        }
        private void UpdateMRUList(ViewInfo info_)
        {
            EventHandler<MRUListChangedEventArgs> copy = null; 
            if (mruList.IndexOf(info_) == 0) //already the first one
            {
                return;
            }
             
            bool removed = mruList.Remove(info_);
            mruList.Insert(0, info_);
            copy = MRUListChanged;
            if (copy != null)
            {
                copy(this, new MRUListChangedEventArgs(info_, removed ? ListChangeAction.Move : ListChangeAction.Add));
            }

            if (mruList.Count > MAX_MRU_ITEMS)
            {
                var info2 = mruList[MAX_MRU_ITEMS];
                mruList.RemoveAt(MAX_MRU_ITEMS);
                if (copy != null)
                {
                    copy(this, new MRUListChangedEventArgs(info2, ListChangeAction.Remove));
                }
            }

        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Handler for Restart timer.  Kills this process starts new one. 
        /// </summary>
        /// <param name="state_"></param>
        private void RestartTimerRan(object state_, EventArgs args_)
        {
            RestartLauncher();
        }

        /// <summary>
        /// Handler for RefreshTimer 
        /// </summary>
        /// <param name="state_"></param>
        private void RefreshTimerRan(object state_, EventArgs args_)
        {
            LogLayer.Info.Location(CLASS, "RefreshTimerRan()")
                    .Append("RefreshTimer running at ", DateTime.Now.ToString())
                    .Send();
            try
            {
                ReloadApplicationConfig();
            }
            catch (Exception ex_)
            {
                string msg = string.Format("Error occurred while running a nighly refresh task. {0}", ex_);
                LogLayer.Error.Location(CLASS, "RefreshTimerRan").Append(msg).Send();
            }
        }

        /// <summary>
        /// Handles any unexpected thread exceptions cleanly
        /// </summary>
        /// <param name="sender_">The sender object.</param>
        /// <param name="e_">The thread exception argunments.</param>
        private void Current_DispatcherUnhandledException(object sender,
                                                          System.Windows.Threading.DispatcherUnhandledExceptionEventArgs
                                                              e)
        {
            LogLayer.Emergency.Location(CLASS, "Current_DispatcherUnhandledException")
                    .Append(e.Exception + Environment.NewLine + e.Exception.StackTrace)
                    .Send();

            ShowFatalException("An untrapped thread exception has occurred.", e.Exception);
            e.Handled = true;
            ;
        }

        /// <summary>
        /// Handles any unexpected thread exceptions cleanly
        /// </summary>
        /// <param name="sender_">The sender object.</param>
        /// <param name="e_">The thread exception argunments.</param>
        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            if (e.IsTerminating) return;
            Exception exception = e.ExceptionObject as Exception;
            if (exception != null)
            {
                LogLayer.Emergency.Location(CLASS, "CurrentDomain_UnhandledException")
                        .Append(exception + Environment.NewLine +
                                exception.StackTrace).Send();
            }
            else
            {
                LogLayer.Emergency.Location(CLASS, "CurrentDomain_UnhandledException")
                        .Append("Unhandled exception, no exception object.")
                        .Send();
            }
            ShowFatalException("An untrapped thread exception has occurred.", exception); 
        }

        #endregion

        #region Protected Methods

        protected virtual void OnShuttingDown(CancelEventArgs args_)
        {
            if (ShuttingDown != null)
            {
                ShuttingDown(this, args_);
            }
        }

        internal void RestartLauncher()
        {
            LogLayer.Info.Location(CLASS, "RestartTimerRan()")
                    .Append("Killing app running at ", DateTime.Now.ToString())
                    .Send();
            try
            {
                Process myProcess = Process.GetCurrentProcess();
                Process restartJob = new Process();
                FileInfo fi = new FileInfo(myProcess.MainModule.FileName);

                ProcessStartInfo restartJobInfo = new ProcessStartInfo();
                restartJobInfo.FileName = string.Concat(fi.DirectoryName, @"\MSDesktop.StartProcess.exe");
                string processArg = myProcess.MainModule.FileName;
                foreach (var arg in _args)
                {
                    processArg = string.Concat(processArg, " ", (arg.Contains(" ") && !arg.StartsWith("\"") ? string.Format("\"{0}\"", arg) : arg));

                }
                restartJobInfo.Arguments = string.Concat(processArg, " /k ",
                                                         myProcess.Id.ToString());
                restartJobInfo.UseShellExecute = true;
                restartJobInfo.WindowStyle = ProcessWindowStyle.Hidden;
                restartJob.StartInfo = restartJobInfo;
  
                LogLayer.Info.Location(CLASS, "RestartTimerRan()").Append("Running ", restartJobInfo.FileName, " ",
                                                                          restartJobInfo.Arguments).Send();
                restartJob.Start();
                this.OnShuttingDown(new CancelEventArgs());

            }
            catch (Exception ex_)
            {
                string msg = string.Format("Error occurred while running a nighly restart job. {0}", ex_);
                LogLayer.Error.Location(CLASS, "RestartTimerRan").Append(msg).Send();
            }
        }

        #endregion
    }
}
