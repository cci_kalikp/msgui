#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2006 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

              $Id: //eai/msdotnet/msgui/trunk/assemblies/Launcher/MSDesktop.Launcher/UI/ProcessLaunchManager.cs#17 $
  $DateTime: 2014/07/28 05:34:31 $
    $Change: 890260 $
    $Author: caijin $
_____________________________________________________________________________*/
#endregion File Info Header

using System;
using System.Collections;
using System.IO;
using System.Net;
using System.Threading;
using System.Diagnostics;
using System.Windows;
using System.Windows.Threading;
using MSDesktop.Launcher.Model;
using System.Collections.Generic;

namespace MSDesktop.Launcher.UI
{
    /// <summary>
    /// Manages the process launching process
    /// Maintains a list of processes to start and end for 1 day.
    /// </summary>
    public class ProcessLaunchManager
    {
        #region const

        private readonly string UNKNOWN_PID = "Unknown";

        #endregion

        #region Private state

        private static readonly string CLASS = "ProcessLaunchManager";

        private Dictionary<ViewInfo, ProcessManager> _processList = new Dictionary<ViewInfo, ProcessManager>(); 
        private Dictionary<ViewInfo,ProcessManager> _downloadingProcessList = new Dictionary<ViewInfo, ProcessManager>(); 
        private Hashtable _startList = new Hashtable();

        private object _scheduleSync = new object();

        private readonly DispatcherTimer _schedulerTimer;

        //Create a singleton launch manager instance
        private EventHandler<ProcessStateChangedEventArgs> _processStateChangedHandler;

        #endregion

        #region Ctor()

        // TODO:
        // refactor to remove dependency on controller_
        //Make the constructor private to force use of the singleton
        public ProcessLaunchManager(Dispatcher dispatcher_)
        {
            _processStateChangedHandler = new EventHandler<ProcessStateChangedEventArgs>(ProcessStateChanged);
            _schedulerTimer = new DispatcherTimer(TimeSpan.FromMinutes(1), DispatcherPriority.Normal,
                                                  new EventHandler(RunScheduledTasks), dispatcher_);
            _schedulerTimer.Stop(); // don't start initially
        }

        #endregion

        #region Public Properties

        public Hashtable StartList
        {
            get
            {
                Hashtable startList = new Hashtable();
                lock (_scheduleSync)
                {
                    //Make a local copy
                    foreach (ViewInfo viewInfo in _startList.Keys)
                    {
                        startList[viewInfo] = _startList[viewInfo];
                    }
                }
                return startList;
            }
        }

        #endregion Public Properties

        #region Public Methods

        /// <summary>
        /// Schedules to start a process defined by ViewInfo at a specified time.
        /// </summary>
        /// <param name="viewInfo_"></param>
        /// <param name="startTime_"></param>
        public void ScheduleStart(ViewInfo viewInfo_, TimeSpan startTime_)
        {
            lock (_scheduleSync)
            {
                _startList[viewInfo_] = startTime_;
                LogLayer.Info.Location(CLASS, "ScheduleStart")
                        .Append(String.Format("Added application {0} to start list to start at {1}",
                                              viewInfo_.ShortcutDescription, startTime_.ToString()))
                        .Send();
            }

            ScheduleTimerControl();
        }


        /// <summary>
        /// Launches a new process.
        /// </summary>
        /// <param name="viewInfo_">Contains all the details about the process to be started.</param>
        /// <param name="userInitiated_">Was this initiated from the UI.</param>
        public ProcessManager StartProcess(ViewInfo viewInfo_, bool userInitiated_ = false)
        {
            if (userInitiated_)
            {
                if (IsProcessDownloading(viewInfo_)) return null;
                if (_processList.ContainsKey(viewInfo_) && !_processList[viewInfo_].IsFullyStartedByEstimation) return _processList[viewInfo_];          
            }
            try
            {
                ProcessManager processMgr = new ProcessManager(viewInfo_);
                if (viewInfo_.FileSystem == IEDCodeLocation.COPYLOCAL || viewInfo_.FileSystem == IEDCodeLocation.COPYLOCALDIRECT)
                {
                    //Check the AFS directory exists - i.e. that we have connection to AFS
                    if (viewInfo_.IsPackageAbsolutePath || (viewInfo_.GetPath(IEDCodeLocation.AFS) != null && Directory.Exists(viewInfo_.GetPath(IEDCodeLocation.AFS))))

                    {
                        try
                        {
                            bool downloadNew = true;
                            string currentPackage = viewInfo_.GetPath(IEDCodeLocation.COPYLOCAL) + viewInfo_.Package;
                            string newPackage = viewInfo_.GetPackagePath();

                            if (newPackage.StartsWith("http://"))
                            {
                                HttpWebRequest request = HttpWebRequest.Create(newPackage) as HttpWebRequest;
                                request.Method = "HEAD";
                                HttpWebResponse response = request.GetResponse() as HttpWebResponse;

                                if (response != null && (response.StatusCode != HttpStatusCode.Accepted && response.StatusCode != HttpStatusCode.OK))
                                {
                                    throw new ApplicationException(String.Format("Cannot find package in source path {0}", newPackage));
                                }
                            }
                            else
                            {
                                if (!File.Exists(newPackage))
                                {
                                    throw new ApplicationException(String.Format("Cannot find package in source path {0}",
                                                                                 newPackage));
                                }
                            }


                            if (File.Exists(currentPackage))
                            {
                                DateTime currentVersion = File.GetLastWriteTimeUtc(currentPackage);
                                DateTime newVersion = File.GetLastWriteTimeUtc(newPackage);

                                if (currentVersion == newVersion)
                                {
                                    LogLayer.Info.Location(CLASS, "DownloadAndUnzipPackage")
                                            .Append(
                                                String.Format(
                                                    "New {0} and old {1} last edit timestamps match (new {2} vs current {3}) so not downloading again",
                                                    newPackage, currentPackage, newVersion, currentVersion))
                                            .Send();
                                    downloadNew = false;
                                }
                                else
                                {
                                    LogLayer.Info.Location(CLASS, "DownloadAndUnzipPackage")
                                            .Append(
                                                String.Format(
                                                    "New package {0} has different time stamp {1} to old package {2} timestamp {3}.  Attempting download of new version",
                                                    newPackage, newVersion, currentPackage, currentVersion))
                                            .Send();
                                }
                            }

                            if (downloadNew)
                            {
                                LogLayer.Info.Location(CLASS, "DownloadAndUnzipPackage").Append(String.Format("Downloading package {0} for the first time.", newPackage)).Send();

                                if (this.IsProcessStarted(viewInfo_) ||
                                    this.IsProcessStarted(viewInfo_.GetPath(IEDCodeLocation.COPYLOCAL)))
                                {
                                    if (userInitiated_)
                                    {
                                        MessageBoxResult result = MessageBoxResult.Cancel; 
                                        System.Windows.Application.Current.Dispatcher.Invoke(new Action(() =>
                                            {
                                                // only ask if the user if they started it.  if it's e.g. auto start, just kill it.
                                                result =
                                                    DialogExtensions.ShowMessage(null,
                                                                                 "A new version is available.  You cannot download this new version while the existing version is running."
                                                                                 + System.Environment.NewLine +
                                                                                 "If you proceed, any existing running versions will be killed.",
                                                                                 "Run Application",
                                                                                 MessageBoxImage.Warning,
                                                                                 MessageBoxButton.OKCancel);
                                            }));


                                        if (result == MessageBoxResult.Cancel)
                                        {
                                            return null;
                                        }
                                    }

                                    //kill it so we can delete it, including any others running from the same path.
                                    KillProcess(viewInfo_, true);
                                }
                                lock (_downloadingProcessList)
                                {
                                    if (_downloadingProcessList.ContainsKey(viewInfo_))
                                    {
                                        _downloadingProcessList[viewInfo_].KillProcess();
                                    }
                                    _downloadingProcessList[viewInfo_] = processMgr;              
                                }
                                try
                                {
                                    System.Windows.Application.Current.Dispatcher.Invoke(new Action(() =>
                                        {
                                            ProgressDialog dialog = new ProgressDialog(viewInfo_);
                                            dialog.ShowDialog(); 
                                        }));

                                }
                                finally
                                {
                                    lock (_downloadingProcessList)
                                    {
                                        _downloadingProcessList.Remove(viewInfo_);
                                    }
                                }

                            }
                        }
                        catch (Exception ex)
                        {
                            LogLayer.Error.Location(CLASS, "StartProcess").Append(
                                String.Format(
                                    "Error while getting latest local copy of application {0}",
                                    viewInfo_.ApplicationAndIconDescription) + ex + ex.StackTrace).Send();
                        }
                    }

                    if (viewInfo_.CompleteFilename == null || !File.Exists(viewInfo_.CompleteFilename))
                    {
                        // If there's no local copy there's nothing we can do.  So report an error to the user
                        LogLayer.Error.Location(CLASS, "StartProcess").Append(
                            String.Format(
                                "User requested to start a local application {0}. But it is not on disk and AFS is inaccessible",
                                viewInfo_.ApplicationAndIconDescription)).Send();
                        DialogExtensions.ShowMessage(null, "Invalid package path or executable", "Run Application", MessageBoxImage.Error);
                        return null;
                    }
                }
                ProcessManager oldProcess;
                if (_processList.TryGetValue(viewInfo_, out oldProcess))
                {
                    if (oldProcess.State == ProcessState.Hibernating)
                    {
                        oldProcess.State = ProcessState.Exited;
                    }
                }
                processMgr.ProcessStateChanged += _processStateChangedHandler;

                ThreadPool.QueueUserWorkItem(new WaitCallback(processMgr.StartProcess));

                lock (_processList)
                {
                    //TODO: Possibly add logic to ask users if they would want to run a 2nd instance.
                    //Set the key, don't always use ADD
                    _processList[viewInfo_] = processMgr;
                }
                return processMgr;

            }
            catch (Exception ex_)
            {
                ExceptionViewer.ShowException("Attempting to start " + viewInfo_.ApplicationAndIconDescription, ex_);
                return null;
            }
        }

        public void KillProcess(ViewInfo viewInfo_, bool killProcessRunningFromSamePath_)
        {
            ProcessManager pm;
            List<ProcessManager> pmList = new List<ProcessManager>();
            if (!_processList.TryGetValue(viewInfo_, out pm) && killProcessRunningFromSamePath_)
            {
                foreach (ViewInfo viewInfo in _processList.Keys)
                {
                    if (viewInfo.GetPath(IEDCodeLocation.COPYLOCAL) == viewInfo_.GetPath(IEDCodeLocation.COPYLOCAL))
                    {
                        pmList.Add(_processList[viewInfo]);
                            // if the exact process isn't running, look for path matches for copy local, kill it too as we need to free up the paths.
                    }
                }
            }

            try
            {
                if (pm != null)
                {
                    pm.KillProcess();
                }

                if (pmList.Count > 0)
                {
                    foreach (ProcessManager pmFromList in pmList)
                    {
                        pmFromList.KillProcess();
                    }
                }
            }
            catch (Exception ex_)
            {
                LogKillFailure(pm.Process.Id.ToString(), ex_, "KillProcess");
            }
        }

        public void ClearSchedule()
        {
            lock (_scheduleSync)
            {
                _startList.Clear();
                ScheduleTimerControl();
            }
        }

        #endregion

        #region Internal Methods

        public bool IsProcessStarted(ViewInfo viewinfo_)
        {
            lock (_processList)
            {
                bool started = _processList.ContainsKey(viewinfo_) && _processList[viewinfo_].Started;
                if (started)
                {

                    LogLayer.Info.Location(CLASS, "IsProcessStarted").Append(
        String.Format(
            "application {0} is started", viewinfo_.UniqueKey)).Send();
                }
                else
                {
                    LogLayer.Info.Location(CLASS, "IsProcessStarted").Append(
String.Format(
"application {0} is not started", viewinfo_.UniqueKey)).Send();

                }

                return started;
            }
        }
         
        public bool IsProcessDownloading(ViewInfo viewInfo_)
        {
            lock (_downloadingProcessList)
            {
                return _downloadingProcessList.ContainsKey(viewInfo_);
            }
        }
       
        public bool IsProcessReadyForIPC(ViewInfo viewInfo_)
        {
            ProcessManager process = null;
            lock (_processList)
            {
                if (!_processList.TryGetValue(viewInfo_, out process))
                {
                    if (!_downloadingProcessList.TryGetValue(viewInfo_, out process))
                    {
                        return false;                      
                    }
                }
            }
            if (process.State == ProcessState.Hibernating)
            {
                return false;
            }
            return process.WaitForIPCReady;
        }

 


        public ProcessManager GetProcess(ViewInfo viewInfo_)
        {
            lock (_processList)
            {
                ProcessManager process = null;
                if (_processList.TryGetValue(viewInfo_, out process))
                {
                    return process;
                }
            }
            return null;

        }

        public bool IsProcessStarted(string path_)
        {
            lock (_processList)
            {
                foreach (var info in _processList)
                {
                    if (info.Key.GetPath(IEDCodeLocation.COPYLOCAL) == path_ && info.Value.Started)
                    { 
                        // if its also a process copied local running in the same directory return true
                        return true;
                    }
                }
            }
            return false;
        }
        #endregion Internal Methods

        #region Private methods
        //Controls whether schedule timer is to be run or not.
        private void ScheduleTimerControl()
        {
            if (_startList.Count == 0)
            {
                //Stop the timer
                if (_schedulerTimer.IsEnabled)
                {
                    _schedulerTimer.Stop();
                }
            }
            else
            {
                if (!_schedulerTimer.IsEnabled)
                {
                    //Restart time - run now and then on timer.
                    RunScheduledTasks(this, null);
                    _schedulerTimer.Start();
                }
            }
        }

        /// <summary>
        /// Callback for the Schedule timer thread. Tries to Start and Stop tasks as per schedule.
        /// </summary>
        /// <param name="state_"></param>
        private void RunScheduledTasks(object state_, EventArgs eventArgs_)
        {
            try
            {
                StartScheduledTasks();
            }
            catch (Exception ex_)
            {
                string msg = String.Format("Exception occurred on the scheduler thread {0}.", ex_.ToString());
                Debug.WriteLine(msg);
            }
        }

        /// <summary>
        /// Checks scheduled start time of enabled processes 
        /// and starts those that are scheduled to be started
        /// </summary>
        private void StartScheduledTasks()
        {
            ArrayList birthList = new ArrayList();
            ArrayList decoFromList = new ArrayList();

            //Add all processes to the deathList
            lock (_startList.SyncRoot)
            {
                TimeSpan timeNow = DateTime.Now.Subtract(DateTime.Today);

                foreach (ViewInfo viewInfo in _startList.Keys)
                {
                    TimeSpan schedToStart = (TimeSpan)_startList[viewInfo];
                    double minuesToBirth = schedToStart.Subtract(timeNow).TotalMinutes;

                    //If ScheduledToStart time is up to 5 minutes before now(passed) 
                    //Start process, otherwise ignore and remove from the startlist for today.
                    if (minuesToBirth <= 0 && minuesToBirth > -5)
                    {
                        LogLayer.Info.Location(CLASS, "StartScheduledTasks").Append(String.Format("Time to start application {0}!", viewInfo.ShortcutDescription)).Send();
                        birthList.Add(viewInfo);
                        decoFromList.Add(viewInfo);
                    }
                    else if (minuesToBirth < -5)
                    {
                        LogLayer.Info.Location(CLASS, "StartScheduledTasks").Append(String.Format("More than 5 minutes after application {0} should be started.  Removing it from the list.", viewInfo.ShortcutDescription)).Send();
                        decoFromList.Add(viewInfo);
                    }
                }

                foreach (ViewInfo vi in decoFromList)
                {
                    _startList.Remove(vi);
                }
            }

            foreach (ViewInfo viewInfo in birthList)
            {
                //var depends = new List<ViewInfo>(_controller.GetRunnableViewInfoDepends(viewInfo, true));
                var depends = viewInfo.DependsOn;

                lock (_startList.SyncRoot)
                {
                    foreach (var vi in depends)
                    {
                        if (_startList.Contains(vi))
                        {
                            _startList.Remove(vi);
                        }
                    }
                }

                foreach (var vi in depends)
                {
                    if (!IsProcessStarted(vi))
                        StartProcess(vi);
                }

                StartProcess(viewInfo);
            }
        }

        private void LogKillFailure(string pid_, Exception ex_, string methodName_)
        {
            if (pid_.Trim().Length == 0)
                pid_ = UNKNOWN_PID;

            string msg = String.Format("Failed to shutdown process '{0}'. Exception {1}", pid_, ex_);
            LogLayer.Warning.Location(CLASS, methodName_).Append(msg).Send();
        }
        #endregion

        #region EventHandlers
        private void ProcessStateChanged(object sender_, ProcessStateChangedEventArgs args_)
        {
            if (args_.State != ProcessState.Exited) return;
            ViewInfo viewInfo = null;
            lock (_processList)
            {
                ProcessManager procMgr = sender_ as ProcessManager;
                if (procMgr != null)
                {
                    procMgr.ProcessStateChanged -= _processStateChangedHandler;

                    if (_processList.ContainsValue(procMgr))
                    {
                        foreach (ViewInfo vi in _processList.Keys)
                        {
                            if (object.ReferenceEquals(_processList[vi], sender_))
                            {
                                viewInfo = vi;
                                break;
                            }
                        }
                        if (viewInfo != null)
                        {
                            _processList.Remove(viewInfo);
                        }
                        if (viewInfo != null)
                        {
                            LogLayer.Info.Location(CLASS, "ProcessStateChanged").Append(
            String.Format(
            "application {0} exits", viewInfo.UniqueKey)).Send();
                        }
                    }
                }
            }


        }
        #endregion
    }
}
