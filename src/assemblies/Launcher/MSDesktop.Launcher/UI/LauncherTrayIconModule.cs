﻿using System;
using System.Collections.Generic; 
using System.Linq; 
using System.Windows.Controls; 
using Microsoft.Practices.Composite.Modularity;
using MSDesktop.Launcher.UI;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager; 
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.SystemTray; 

namespace MSDesktop.Launcher
{
    class LauncherTrayIconModule:IModule
    {
        private readonly IChromeManager chromeManager;
        private SystemTrayIconArea trayParent;

        readonly Dictionary<ViewInfo, IWidgetViewContainer> currentMruWidgets = new Dictionary<ViewInfo, IWidgetViewContainer>();
        readonly Dictionary<ViewInfo, IWidgetViewContainer> removedMruWidgets = new Dictionary<ViewInfo, IWidgetViewContainer>();
        private IWidgetViewContainer mruSplitter;

        readonly Dictionary<ViewInfo, WidgetViewContainer> favoriteViewWidgets = new Dictionary<ViewInfo, WidgetViewContainer>();
        readonly Dictionary<ViewInfoIconGroup, WidgetViewContainer> favoriteGroupWidgets = new Dictionary<ViewInfoIconGroup, WidgetViewContainer>();
        private IWidgetViewContainer favoriteApplicationWidget;

        readonly Dictionary<ViewInfo, WidgetViewContainer> viewWidgets = new Dictionary<ViewInfo, WidgetViewContainer>();
        readonly Dictionary<ViewInfoIconGroup, WidgetViewContainer> groupWidgets = new Dictionary<ViewInfoIconGroup, WidgetViewContainer>();
        readonly Dictionary<ViewInfoGroup, WidgetViewContainer> applicationWidgets = new Dictionary<ViewInfoGroup, WidgetViewContainer>();
        readonly List<IWidgetViewContainer> staticWidgets = new List<IWidgetViewContainer>();
        private WidgetViewContainer menuSplitter;
        private WidgetViewContainer firstRealApplicationWidget = null;

        public LauncherTrayIconModule(IChromeManager chromeManager_, IChromeRegistry chromeRegistry_)
        {
            this.chromeManager = chromeManager_;
            DialogExtensions.ChromeManager = chromeManager_;
            DialogExtensions.ChromeRegistry = chromeRegistry_;
        }

        public void Initialize()
        {
            trayParent = chromeManager.AddWidget("ApplicationTrayIconId", new InitialTrayIconParameters()
                {
                    Icon = Launcher.Controller.Settings.Icon,
                    Text = Launcher.Controller.Settings.Title,
                    
                }) as SystemTrayIconArea;
             FillTrayIconMenu();

             Launcher.Controller.ModelChanged += Controller_ModelChanged;
             Launcher.Controller.MRUListChanged += Controller_MRUListChanged;
             Launcher.Controller.FavoriteChanged += Controller_FavoriteChanged;
             Launcher.Controller.UserSettings.PropertyChanged += UserSettings_PropertyChanged;
             Launcher.Controller.ExtraUserSettings.PropertyChanged += ExtraUserSettings_PropertyChanged;
        }

        void Controller_FavoriteChanged(object sender, FavoriteChangedEventArgs e)
        {
            if (Launcher.Controller.ExtraUserSettings.FavoriteAsFilter && Launcher.Controller.UserSettings.FilterByFavorite)
            {
                FillTrayIconMenu();
                return;
            } 

            WidgetViewContainer viewWidget;
            if (viewWidgets.TryGetValue(e.Item, out viewWidget))
            {
                MenuItem menuItem = viewWidget.Content as MenuItem;
                if (menuItem != null)
                {
                    menuItem.Header = e.Item.IconDescriptionWithFavoriteIndicator;
                }
            }
            if (Launcher.Controller.ExtraUserSettings.FavoriteAsFilter && !Launcher.Controller.UserSettings.FilterByFavorite) return;

            SystemTrayMenuItemArea favoriteApplicationWidgetCasted =
                favoriteApplicationWidget as SystemTrayMenuItemArea;
             if (e.Item.FavoriteValue)
             {
                 //ensure favorite application
                 if (favoriteApplicationWidget == null)
                 {
                     favoriteApplicationWidget = chromeManager.AddWidget(ViewInfoGroup.FavoriteGroupName + "." + Guid.NewGuid().ToString(),
                     new InitialTrayMenuItemParameters()
                     {
                         Text = ViewInfoGroup.FavoriteGroupName,
                         Image = PathToImageConverter.ConvertToImage(ViewInfoGroup.FavoriteIconPath)
                     }, trayParent);
                     favoriteApplicationWidget.ClearParent();
                     favoriteApplicationWidgetCasted =favoriteApplicationWidget as SystemTrayMenuItemArea;
                 }
                 if (favoriteApplicationWidget.Parent == null)
                 {
                     int index = trayParent.VisualChildren.IndexOf(firstRealApplicationWidget.Content);
                     trayParent.Insert(index, favoriteApplicationWidget);
                 }

                 //ensure favorite group
                 WidgetViewContainer favoriteGroupWidget = null;
                 bool needToAddGroup = true;
                 ViewInfoIconGroup favoriteGroup =
                     favoriteGroupWidgets.FirstOrDefault(pair_ => pair_.Key.Name == e.Item.ApplicationDescription).Key;
                 if (favoriteGroup == null)
                 {
                     favoriteGroup = new ViewInfoIconGroup();
                     favoriteGroup.ViewInfoGroupName = favoriteGroup.Name; 
                     favoriteGroup.Name = e.Item.ApplicationDescription; // Name is Application Description (what we grouped by) 
                     favoriteGroup.ViewInfoList = new List<ViewInfo>();
                     favoriteGroup.IconPath = (!String.IsNullOrEmpty(e.Item.AppIconPath)
                                                         ? e.Item.AppIconPath
                                                         : e.Item.IconFileName);
                     favoriteGroupWidget = chromeManager.AddWidget(favoriteGroup.Name + "." + Guid.NewGuid().ToString(),
                                             new InitialTrayMenuItemParameters()
                                                 {
                                                     Text = favoriteGroup.Name,
                                                     Image = PathToImageConverter.ConvertToImage(favoriteGroup.IconPath)
                                                 }, trayParent) as WidgetViewContainer;
                     favoriteGroupWidget.ClearParent();
                     favoriteGroupWidgets[favoriteGroup] = favoriteGroupWidget;
                 }
                 else
                 {
                     favoriteGroupWidget = favoriteGroupWidgets[favoriteGroup];
                     if (favoriteGroupWidget.Parent != null)
                     {
                         needToAddGroup = false;
                     }
                 }

                 if (needToAddGroup)
                 {
                     List<ViewInfoIconGroup> groups = new List<ViewInfoIconGroup>();
                     foreach (var group in favoriteGroupWidgets)
                     {
                         if (group.Value.Parent != null)
                         {
                             groups.Add(group.Key);
                         }
                     }
                     groups.Add(favoriteGroup);
                     groups.Sort((g1_, g2_) => System.String.Compare(g1_.Name, g2_.Name, System.StringComparison.Ordinal));
                     int index2 = groups.IndexOf(favoriteGroup);
                     if (index2 < groups.Count - 1)
                     {
                         favoriteApplicationWidgetCasted.Insert(index2, favoriteGroupWidget);
                     }
                     else
                     {
                         favoriteApplicationWidgetCasted.Add(favoriteGroupWidget);
                     } 
                 }
                 
                 //add the favorite view finally
                 WidgetViewContainer favoriteViewWidget = null;
                 if (!favoriteViewWidgets.TryGetValue(e.Item, out favoriteViewWidget))
                 {
                     favoriteViewWidget = AddMenuItem(favoriteGroupWidget, e.Item, true) as WidgetViewContainer;
                     favoriteViewWidget.ClearParent();
                     favoriteViewWidgets[e.Item] = favoriteViewWidget;
                 }
                 int index3 = 0;
                 if (!needToAddGroup)
                 {
                     List<ViewInfo> items = new List<ViewInfo>();
                     foreach (var widgetViewContainer in favoriteViewWidgets)
                     {
                         if (widgetViewContainer.Value.Parent == favoriteGroupWidget)
                         {
                             items.Add(widgetViewContainer.Key);
                         }
                     }
                     items.Add(e.Item);
                     EnvironmentGroupComparer comparer = new EnvironmentGroupComparer();
                     items.Sort((v1_, v2_) =>
                     {
                         int result = comparer.Compare(v1_.EnvironmentGroup, v2_.EnvironmentGroup);
                         if (result != 0) return result;
                         if (v1_.EnvironmentGroup == v2_.EnvironmentGroup)
                         {
                             return System.String.Compare(v1_.EnvironmentName, v2_.EnvironmentName, System.StringComparison.Ordinal);
                         }
                         return items.IndexOf(v1_) - items.IndexOf(v2_);
                     }
                   );
                     index3 = items.IndexOf(e.Item);
                 }
                 (favoriteGroupWidget as SystemTrayMenuItemArea).Insert(index3, favoriteViewWidget);
                
             }
             else if (favoriteApplicationWidget != null && favoriteApplicationWidget.Parent != null)
             {
                 WidgetViewContainer favoriteViewWidget;
                 if (favoriteViewWidgets.TryGetValue(e.Item, out favoriteViewWidget))
                 {
                     var favoriteGroupWidget = favoriteViewWidget.Parent as WidgetViewContainer;
                     if (favoriteGroupWidget != null)
                     {
                         favoriteViewWidget.ClearParent();
                         if (!favoriteGroupWidget.Children.Any())
                         {
                            favoriteGroupWidget.ClearParent();
                             if (!((WidgetViewContainer) favoriteApplicationWidget).Children.Any())
                             {
                                 favoriteApplicationWidget.ClearParent();
                             }
                         }
                     }
                 }
                 
             }
        }

 
        void Controller_MRUListChanged(object sender, MRUListChangedEventArgs e)
        {
            if (trayParent == null) return;
            IWidgetViewContainer viewContainer = null;
            if (e.Action == ListChangeAction.Add)
            { 
                if (e.Item.IsHidden) return;
                if (!removedMruWidgets.TryGetValue(e.Item, out viewContainer))
                {
                    viewContainer = AddMenuItem(trayParent, e.Item, false, e.Item.ApplicationDescription + " : " + e.Item.EnvironmentName);
                    trayParent.RemoveWidget(viewContainer);
                    currentMruWidgets.Add(e.Item, viewContainer);
                }
                else
                {
                    removedMruWidgets.Remove(e.Item);
                    currentMruWidgets.Add(e.Item, viewContainer);
                }
                trayParent.Insert(0, viewContainer);
                if (currentMruWidgets.Count == 1)
                {
                    if (mruSplitter == null)
                    {
                        mruSplitter = chromeManager.AddWidget("Separator" + Guid.NewGuid().ToString(), new InitialTrayMenuSeparatorParameters(), trayParent);
                        trayParent.RemoveWidget(mruSplitter);
                    }
                    trayParent.Insert(1, mruSplitter);
                }

            }
            else if (e.Action == ListChangeAction.Move)
            {
                if (currentMruWidgets.TryGetValue(e.Item, out viewContainer))
                {
                    trayParent.RemoveWidget(viewContainer);
                    trayParent.Insert(0, viewContainer); 
                } 
            }
            else
            { 
                if (currentMruWidgets.TryGetValue(e.Item, out viewContainer))
                {
                    removedMruWidgets.Add(e.Item, viewContainer);
                    currentMruWidgets.Remove(e.Item);
                    trayParent.RemoveWidget(viewContainer); 
                } 
                if (currentMruWidgets.Count == 0 && mruSplitter != null) 
                {
                    trayParent.RemoveWidget(mruSplitter);
                }
            } 
        }

        void Controller_ModelChanged()
        {
            FillTrayIconMenu();
        }

        void ExtraUserSettings_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "DefaultEnvironment" ||  e.PropertyName == "FavoriteAsFilter" )
            {
                FillTrayIconMenu();
            }
        }

        private void UserSettings_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "FilterBySysLoc" || e.PropertyName == "FilterByFavorite")
            { 
                FillTrayIconMenu();
            }
        }

        private IWidgetViewContainer AddMenuItem(IWidgetViewContainer parentMenu_, ViewInfo info_, bool inFavoriteGroup_, string text_=null)
        {
            bool needsToUpdateText = string.IsNullOrEmpty(text_);
            if (needsToUpdateText)
            {
                text_ = inFavoriteGroup_ ||
                        (Launcher.Controller.ExtraUserSettings.FavoriteAsFilter &&
                         Launcher.Controller.UserSettings.FilterByFavorite)
                            ? info_.IconDescription
                            : info_.IconDescriptionWithFavoriteIndicator;
            }
            var button = chromeManager.AddWidget(info_.UniqueKey + "." + Guid.NewGuid().ToString(),
                new InitialTrayMenuItemParameters()
                {
                    Text = text_,
                    Image =  PathToImageConverter.ConvertToImage(info_.IconFileName),
                    Click = (sender_, args_) => Launcher.Controller.StartProcess(info_.UniqueKey)
                }, parentMenu_);

            if (needsToUpdateText)
            {
                button.ContentReady += (sender_, args_) =>
                {
                    MenuItem item = button.Content as MenuItem;
                    if (item != null)
                    {
                        info_.PropertyChanged += (o_, eventArgs_) =>
                        {
                            item.Header = inFavoriteGroup_ || (Launcher.Controller.ExtraUserSettings.FavoriteAsFilter && Launcher.Controller.UserSettings.FilterByFavorite)
                                ? ((ViewInfo)o_).IconDescription : ((ViewInfo)o_).IconDescriptionWithFavoriteIndicator;
                        };
                    }

                };

                if (button.Content != null)
                {
                    button.Content = button.Content;
                }

            }

            return button;
        }
         
        private void FillTrayIconMenu()
        {
            WidgetViewContainerHelper.ClearParents(viewWidgets.Values);
            WidgetViewContainerHelper.ClearParents(groupWidgets.Values);
            WidgetViewContainerHelper.ClearParents(applicationWidgets.Values);
            WidgetViewContainerHelper.ClearParents(favoriteGroupWidgets.Values);
            WidgetViewContainerHelper.ClearParents(favoriteViewWidgets.Values);
            WidgetViewContainerHelper.ClearParents(staticWidgets);
            if (favoriteApplicationWidget != null) favoriteApplicationWidget.ClearParent();
            if (menuSplitter != null) menuSplitter.ClearParent();
            firstRealApplicationWidget = null;
            // application groups
            var groupedByApplication =
                Launcher.Controller.GetViewListByApplicationThenEnvironment(Launcher.Controller.UserSettings.FilterBySysLoc, Launcher.UseModernUI ?
                Launcher.Controller.ExtraUserSettings.DefaultEnvironment : null, Launcher.Controller.ExtraUserSettings.FavoriteAsFilter, Launcher.Controller.UserSettings.FilterByFavorite);

            
            foreach (ViewInfoGroup application in groupedByApplication)
            {
                WidgetViewContainer applicationWidget = null;
                ViewInfoGroup applicationCopied = application;
                if (application.IsFavoritesGroup)
                {
                    trayParent.CreateOrReAdd(ref favoriteApplicationWidget, () =>
                       chromeManager.AddWidget(applicationCopied.Name + "." + Guid.NewGuid().ToString(),
                     new InitialTrayMenuItemParameters()
                     {
                         Text = applicationCopied.Name,
                         Image = PathToImageConverter.ConvertToImage(applicationCopied.IconPath)
                     }, trayParent));
                    applicationWidget = favoriteApplicationWidget as WidgetViewContainer;
                }
                else
                {
                    applicationWidget = trayParent.CreateOrReAdd(application, applicationWidgets,
                   application_ =>
                       chromeManager.AddWidget(application_.Name + "." + Guid.NewGuid().ToString(),
                     new InitialTrayMenuItemParameters()
                     {
                         Text = application_.Name,
                         Image = PathToImageConverter.ConvertToImage(application_.IconPath)
                     }, trayParent) as WidgetViewContainer);
                 
                    if (firstRealApplicationWidget == null)
                    {
                        firstRealApplicationWidget = applicationWidget;
                    }
                }
                
                foreach (ViewInfoIconGroup envGroup in application.ViewInfoIconList)
                {
                    WidgetViewContainer groupWidget = applicationWidget.CreateOrReAdd(envGroup, applicationCopied.IsFavoritesGroup ? favoriteGroupWidgets: groupWidgets, group_ =>
                        chromeManager.AddWidget(group_.Name + "." + Guid.NewGuid().ToString(),
                         new InitialTrayMenuItemParameters()
                         {
                             Text = group_.Name,
                             Image = PathToImageConverter.ConvertToImage(group_.IconPath)
                         }, applicationWidget) as WidgetViewContainer);

                    foreach (ViewInfo viewInfo in envGroup.ViewInfoList)
                    {
                        groupWidget.CreateOrReAdd(viewInfo, applicationCopied.IsFavoritesGroup ? favoriteViewWidgets: viewWidgets,
                                                  info_ => AddMenuItem(groupWidget, info_, applicationCopied.IsFavoritesGroup) as WidgetViewContainer);
                    }
                } 
                
            }
             
            if (groupedByApplication.Any())
            {
                trayParent.CreateOrReAdd(
                    ref menuSplitter,
                    () => chromeManager.AddWidget("Separator" + Guid.NewGuid().ToString(),
                                                new InitialTrayMenuSeparatorParameters(), trayParent) as
                        WidgetViewContainer);
            }

            if (staticWidgets.Count == 0)
            {
                staticWidgets.Add(chromeManager.AddWidget("btnShowMainWindow",
            new InitialTrayMenuItemParameters() { Text = "Show Main Window...", Click = (sender_, args_) => Launcher.ShowMainWindow() }, trayParent));
                staticWidgets.Add(chromeManager.AddWidget("btnTrayRunPackage",
                                        new InitialTrayMenuItemParameters()
                                        {
                                            Text = "Run Package...",
                                            Click =
                                                (sender_, args_) =>
                                                {
                                                    try
                                                    {
                                                        RunDialog dialog = new RunDialog(Launcher.Controller, String.Empty);
                                                        dialog.ShowDialog();
                                                        Launcher.Controller.StartProcess(dialog.ViewInfo);
                                                    }
                                                    catch (Exception ex_)
                                                    {
                                                        LogLayer.Error.Location("LauncherTrayIconModule", "btnRunPackage_Click").Append("Error running Run window " + ex_.Message).Send();
                                                    }
                                                }
                                        }, trayParent));

                 staticWidgets.Add(chromeManager.AddWidget("Separator" + Guid.NewGuid().ToString(), new InitialTrayMenuSeparatorParameters(), trayParent));

                 staticWidgets.Add(chromeManager.AddWidget("btnTrayAbout",
                            new InitialTrayMenuItemParameters()
                            {
                                Text = "About...",
                                Click = (sender_, args_) => Launcher.Controller.ShowAboutForm(null, false)

                            }, trayParent));

            }
            else
            {
                foreach (var staticWidget in staticWidgets)
                {
                    trayParent.Add(staticWidget);
                }
            }
        }
         
    }
}
