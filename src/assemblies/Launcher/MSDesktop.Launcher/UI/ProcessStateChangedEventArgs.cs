﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSDesktop.Launcher.UI
{
    public class ProcessStateChangedEventArgs:EventArgs
    {
        public ProcessStateChangedEventArgs(ProcessManager process_, ProcessState state_)
        {
            Process = process_;
            State = state_;
        }

        public ProcessState State { get; private set; }
        public ProcessManager Process { get; private set; }
    }

    public enum ProcessState
    {
        None,
        Exited,
        Exiting,
        Starting,
        Started,
        Hibernating,
    }
}
