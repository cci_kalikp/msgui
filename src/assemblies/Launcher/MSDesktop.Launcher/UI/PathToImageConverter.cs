﻿#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2006 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

              $Id: //eai/msdotnet/msgui/trunk/assemblies/Launcher/MSDesktop.Launcher/UI/PathToImageConverter.cs#5 $
  $DateTime: 2014/04/02 03:13:42 $
    $Change: 874505 $
    $Author: caijin $
_____________________________________________________________________________*/
#endregion File Info Header

using System;
using System.Globalization;
using System.IO;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MSDesktop.Launcher.UI
{
  public class PathToImageConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        return ConvertToImage(value as string); 
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }

      public static BitmapImage ConvertToImage(string path_)
      {
          try
          {
              path_ = PathUtilities.ReplaceEnvironmentVariables(path_);
              if (!string.IsNullOrEmpty(path_) && File.Exists(path_))
              {
                  return new BitmapImage(new Uri(path_));
              }
          }
          catch (Exception ex_)
          {
              LogLayer.Error.Location("PathToImageConverter", "ConvertToImage").Append("Problem getting icon image.", ex_.ToString()).Send();
          }
          return null;
      }
  }
}
