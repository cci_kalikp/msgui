﻿using System;
using System.Globalization;
using System.Windows.Data;
using Infragistics.Windows.OutlookBar;

namespace MSDesktop.Launcher.UI
{ 

  public class ViewInfoToTextConverter : IMultiValueConverter
  {
    public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
    {
      try
      {
          XamOutlookBar bar = (XamOutlookBar)values[0];
          bool favorite = System.Convert.ToBoolean(values[1]);
          //show text without favorite hint when
          //1) the item is in favorite virtual application
          return (bar.SelectedGroup.Header.ToString() == ViewInfoGroup.FavoriteGroupName || 
              //2) favorite is filtered, 
              //3) the item is not a favorite
              (Launcher.Controller.ExtraUserSettings.FavoriteAsFilter && (Launcher.Controller.UserSettings.FilterByFavorite || !favorite))) 
              ? values[3] : values[2];
      }
      catch (Exception ex_)
      {
        LogLayer.Error.Location("ViewInfoToTextConverter", "Convert").Append("Error generating Text.", ex_.ToString()).Send();
      }
      return null;
    }

    public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
