#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2006 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

              $Id: //eai/msdotnet/msgui/trunk/assemblies/Launcher/MSDesktop.Launcher/UI/Dialogs/ExceptionViewer.cs#5 $
  $DateTime: 2014/04/02 10:11:29 $
    $Change: 874650 $
    $Author: caijin $
_____________________________________________________________________________*/
#endregion File Info Header

using System;
using System.Text;
using System.Windows;
using System.Threading;
using System.Windows.Threading;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication;

namespace MSDesktop.Launcher.UI
{
  /// <summary>
  /// Utility class for presenting exceptions to the user.
  /// [Current implementation uses a standard windows message box]
  /// </summary>
  public class ExceptionViewer
  {
    #region delegate definition
    public delegate void ShowMessageDelegate(string currentActivity_, Exception ex_, MessageBoxImage icon_, string threadInfo_);
    #endregion

      private static string GetLauncherName()
      {
          return (Launcher.Controller != null && Launcher.Controller.Settings != null)
                     ? Launcher.Controller.Settings.Title
                     : Launcher.LauncherName;
      }

    /// <summary>
    /// Show a standard exception to the user.
    /// </summary>
    /// <param name="currentActivity_">The current activity in process when the exception occurred.</param>
    /// <param name="ex_">The exception object.</param>
    public static void ShowException(string currentActivity_, Exception ex_)
    {
        var options = new TaskDialogOptions
        {
            Title = GetLauncherName(),
            MainInstruction = currentActivity_,
            Content = ex_.Message,
            MainIcon = VistaTaskDialogIcon.Error,
            ExpandedInfo = ex_.GetDetailedMessage(),
            Owner = null,
            ForceNullOwner = true,
            AllowDialogCancellation = false,
        };
        try
        {
            LogError(currentActivity_, ex_);
            TaskDialog.Show(options);

        }
        catch(Exception ex2_)
        {
            LogLayer.Error.Location("ExceptionViewer", "ShowException").Append("Error showing exception" + ex2_).Send();
        }
    }
       
    /// <summary>
    /// Show a fatal exception to the user.
    /// </summary>
    /// <param name="currentActivity_">The current activity in process when the exception occurred.</param>
    /// <param name="ex_">The exception object.</param>
    public static void ShowFatalException(string currentActivity_, Exception ex_)
    {
        var options = new TaskDialogOptions
        {
            Title = GetLauncherName(),
            MainInstruction = String.Format("{0}  The application will now close.", currentActivity_),
            Content = ex_ == null ? null : ex_.Message,
            MainIcon = VistaTaskDialogIcon.Error,
            ExpandedInfo = ex_ == null ? null : ex_.GetDetailedMessage(),
            Owner = null,
            ForceNullOwner = true,
            AllowDialogCancellation = false,
        };
        try
        {
            LogError(currentActivity_, ex_);
            TaskDialog.Show(options);
        }
        catch (Exception ex2_)
        {
            LogLayer.Error.Location("ExceptionViewer", "ShowFatalException").Append("Error showing exception" + ex2_).Send();
        } 
    }

    /// <summary>
    /// Presents the exception message to the user.
    /// </summary>
    /// <param name="currentActivity_">The current activity in progress.</param>
    /// <param name="ex_">The exception object.</param>
    /// <param name="icon_">The type of icon to show.</param>
    private static void LogError(string currentActivity_, Exception ex_)
    {
        StringBuilder sb = new StringBuilder();

        sb.Append(currentActivity_);


        if (ex_ != null && ex_.Message != null && ex_.Message.Length > 0)
        {
            if (sb.Length > 0)
            {
                sb.Append(Environment.NewLine);
            }

            sb.Append(ex_.Message);
            sb.Append(Environment.NewLine);
            sb.Append(ex_.StackTrace);
        }

        string msg = sb.ToString();

        // log to file as well
        LogLayer.Error.Location("ExceptionViewer", "LogError").Append(msg).Send(); 
 
    }
  }
}
