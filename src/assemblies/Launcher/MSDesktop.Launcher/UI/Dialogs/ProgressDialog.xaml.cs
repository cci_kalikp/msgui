﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using MSDesktop.Launcher.Model;
using MSDesktop.Launcher.Services;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MSDesktop.Launcher.UI
{
  /// <summary>
  /// Interaction logic for ProgressDialog.xaml
  /// </summary>
  public partial class ProgressDialog : UserControl, IDialog
  {
    private BackgroundWorker _backgroundWorker;
    private ViewInfo _viewInfo;

    private bool _downloadComplete;
    
    public ProgressDialog()
    {
      InitializeComponent();

    }

    public ProgressDialog(ViewInfo viewInfo_) 
    {
      _viewInfo = viewInfo_;
      InitializeComponent();
      this.Parameters = new InitialLauncherDialogWindowParameters();
      Parameters.Caption = "Downloading package";
      Parameters.Width = 350;
      Parameters.SizingMethod = SizingMethod.SizeToContent;
      Parameters.StartupLocation = WindowStartupLocation.CenterScreen;
      Parameters.ShowInTaskbar = false;
      Parameters.Topmost = true;
      Parameters.ResizeMode = ResizeMode.NoResize;
      Parameters.WindowStyle = WindowStyle.ToolWindow;
      Parameters.Icon = Launcher.Controller.Settings.Image;


      StatusText.Text = "Downloading and unzipping " + _viewInfo.ApplicationDescription + " " + _viewInfo.IconDescription;
      _backgroundWorker = ((BackgroundWorker)this.FindResource("_backgroundWorker"));
      _backgroundWorker.RunWorkerAsync();
      this.Loaded += new RoutedEventHandler(ProgressDialog_Loaded);
    }

  

    private void _backgroundWorker_DoWork(object sender_, DoWorkEventArgs e)
    {
      Thread.Sleep(1000);  // Sleep for one second to allow for the dialog to show up 
      CopyLocalManager copyLocal = new CopyLocalManager();
      // copy it down, unzip.  Internally this will check for existing version and compare time stamp etc.
      copyLocal.DownloadAndUnzipPackage(_viewInfo, _backgroundWorker);

    }

    private void _backgroundWorker_RunWorkerCompleted(object sender_, RunWorkerCompletedEventArgs e)
    {
      LogLayer.Info.Location("ProgressDialog", "_backgroundWorker_RunWorkerCompleted").Append().Send();
      _downloadComplete = true;
      this.Close();
    }

    private void _backgroundWorker_ProgressChanged(object sender_, ProgressChangedEventArgs e)
    {
      Progress.Value = e.ProgressPercentage;

      if(Progress.Value == 0)
      {
        StatusText.Text = "Copying package...";
      }
      if(Progress.Value == 25)
      {
        StatusText.Text = "Unzipping package...";
      }
      else if(Progress.Value == 75)
      {
        StatusText.Text = "Copying to run location...";
      }
      else if (Progress.Value == 95)
      {
        StatusText.Text = "Cleaning up...";
      }
      else if (Progress.Value == 100)
      {
        StatusText.Text = "Complete.  Launching " + _viewInfo.ApplicationDescription + _viewInfo.IconDescription;
      }
    }

    void ProgressDialog_Loaded(object sender, RoutedEventArgs e)
    {
        Window w = Window.GetWindow(this);
        this.Loaded -= new RoutedEventHandler(ProgressDialog_Loaded);
        w.Closing += new CancelEventHandler(w_Closing);
    }

    void w_Closing(object sender, CancelEventArgs e)
    {
        if (!_downloadComplete)
        {
            // prevent closing this window until the download is complete
            e.Cancel = true;
        }
        else
        {
            ((Window)sender).Closing += new CancelEventHandler(w_Closing);
        }
    }

    public InitialLauncherDialogWindowParameters Parameters { get; private set; }
  }
}
