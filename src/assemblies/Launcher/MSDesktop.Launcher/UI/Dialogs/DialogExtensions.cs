﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using MSDesktop.Launcher.UI;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MSDesktop.Launcher
{
    public static class DialogExtensions
    {
        internal static IChromeManager ChromeManager;
        internal static IChromeRegistry ChromeRegistry;  
        public static void Close(this UserControl control_)
        {
            var window = Window.GetWindow(control_);
            if (window != null) window.Close();
        }
         
        public static bool? ShowDialog(this IDialog dialog_)
        {
            InitialLauncherDialogWindowParameters parameters = dialog_.Parameters;
            if (Launcher.UseModernUI)
            {
                string factoryId = dialog_.GetType().Name + Guid.NewGuid().ToString();
                //instead of using the recommended RegisterWindowFactory method, we still use the RegisterDialogFactory,
                //since in this way, we could save our effort to achieve the effect of center parent windows location
#pragma warning disable 0618
                ChromeRegistry.RegisterDialogFactory(factoryId, window_ =>
                {
                    window_.Content = dialog_;
                    if (parameters.SizingMethod == SizingMethod.Custom)
                    {
                        window_.SizeToContent = SizeToContent.Manual;
                        window_.Width = parameters.Width;
                        window_.Height = parameters.Height;
                    }
                    else if (parameters.Width <= 0)
                    {
                        window_.SizeToContent = SizeToContent.Width;
                        window_.Height = parameters.Height;
                    }
                    else if (parameters.Height <= 0)
                    {
                        window_.SizeToContent = SizeToContent.Height;
                        window_.Width = parameters.Width;
                    }
                    else
                    {
                        window_.SizeToContent = SizeToContent.WidthAndHeight;
                    }
                    window_.ResizeMode = parameters.ResizeMode;
                    if (parameters.Icon != null)
                    {
                        window_.Icon = parameters.Icon;
                    }
                    window_.Title = parameters.Caption;
                    window_.WindowStartupLocation = parameters.StartupLocation;
                    if (parameters.StartupLocation == WindowStartupLocation.Manual)
                    {
                        if (parameters.Left != null) window_.Left = parameters.Left.Value;
                        if (parameters.Top != null) window_.Top = parameters.Top.Value;
                    }
                    if (parameters.Owner != null)
                    {
                        window_.Owner = parameters.Owner;
                    }
                    window_.Topmost = parameters.Topmost;
                    return true;
                });
#pragma warning restore 0618 
                return ChromeManager.CreateDialog(factoryId).ShowDialog();
#pragma warning restore 0618

            }
            else
            {
                Window w = new Window();
                w.Title = parameters.Caption;
                w.Icon =parameters.Icon;
                w.ResizeMode = parameters.ResizeMode;
                w.WindowStyle = parameters.WindowStyle;
                if (parameters.SizingMethod == SizingMethod.Custom)
                {
                    w.SizeToContent = SizeToContent.Manual;
                    w.Width = parameters.Width;
                    w.Height = parameters.Height;
                }
                else if (parameters.Width <= 0)
                {
                    w.SizeToContent = SizeToContent.Width;
                    w.Height = parameters.Height;
                }
                else if (parameters.Height <= 0)
                {
                    w.SizeToContent = SizeToContent.Height;
                    w.Width = parameters.Width;
                }
                else
                {
                    w.SizeToContent = SizeToContent.WidthAndHeight;
                }
                w.WindowStartupLocation = parameters.StartupLocation;
                if (parameters.StartupLocation == WindowStartupLocation.Manual)
                {
                    if (parameters.Left != null) w.Left = parameters.Left.Value;
                    if (parameters.Top != null) w.Top = parameters.Top.Value;
                }
                w.ShowInTaskbar = false;
                w.Owner = parameters.Owner;
                w.Content = dialog_;
                w.Topmost = parameters.Topmost;
                return w.ShowDialog();
            }
        }

 

        public static MessageBoxResult ShowMessage(Window owner_, string message_, string title_, MessageBoxImage icon_, MessageBoxButton buttons_ = MessageBoxButton.OK)
        {
            if (!Launcher.UseModernUI)
            {

                return  owner_ == null ? 
                    MessageBox.Show(message_, title_, buttons_, icon_):
                    MessageBox.Show(owner_, message_, title_, buttons_, icon_);
            }
            else
            {
                VistaTaskDialogIcon icon = VistaTaskDialogIcon.Information;
                switch (icon_)
                { 
                    case MessageBoxImage.Error:
                        icon = VistaTaskDialogIcon.Error; 
                        break;
                    case MessageBoxImage.Warning:
                        icon = VistaTaskDialogIcon.Warning;
                        break;
                    case MessageBoxImage.Information:
                        icon = VistaTaskDialogIcon.Information;
                        break; 
                    default:
                        break;
                        
                }
                TaskDialogCommonButtons buttons = TaskDialogCommonButtons.Close;
                switch (buttons_)
                {
                    case MessageBoxButton.OK: 
                        break;
                    case MessageBoxButton.OKCancel:
                        buttons = TaskDialogCommonButtons.OKCancel;
                        break;
                    case MessageBoxButton.YesNoCancel:
                        buttons = TaskDialogCommonButtons.YesNoCancel;
                        break;
                    case MessageBoxButton.YesNo:
                        buttons = TaskDialogCommonButtons.YesNo;
                        break;
                    default:
                        break;
                }
                TaskDialogOptions options = new TaskDialogOptions()
                    {
                        Title = title_,
                        Content = message_,
                        CommonButtons = buttons,
                        MainIcon = icon,
                        ForceNullOwner = true,
                        Owner = owner_, 
                    };
                TaskDialogResult result = TaskDialog.Show(options);
                switch (result.Result)
                {
                    case TaskDialogSimpleResult.None:
                        return MessageBoxResult.None;
                    case TaskDialogSimpleResult.Ok:
                        return MessageBoxResult.OK;
                    case TaskDialogSimpleResult.Cancel:
                        return MessageBoxResult.Cancel; 
                    case TaskDialogSimpleResult.Yes:
                        return MessageBoxResult.Yes;
                    case TaskDialogSimpleResult.No:
                        return MessageBoxResult.No; 
                    default:
                        return MessageBoxResult.None;
                }
            }
        }
    }

    public sealed class InitialLauncherDialogWindowParameters : InitialWindowParameters
    {
        public InitialLauncherDialogWindowParameters()
        {
            IsModal = true;
            Transient = true;
            ShowInTaskbar = true;
            WindowStyle = WindowStyle.SingleBorderWindow;

        }

        public Window Owner { get; set; }
        public WindowStartupLocation StartupLocation { get; set; }
        public double? Left { get; set; }
        public double? Top { get; set; }
        public ImageSource Icon { get; set; }
        public WindowStyle WindowStyle { get; set; }
        public string Caption { get; set; }

    }
}