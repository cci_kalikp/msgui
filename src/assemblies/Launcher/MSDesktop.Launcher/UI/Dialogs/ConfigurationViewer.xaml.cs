﻿

using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Xml;
using MSDesktop.Launcher.UI;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MSDesktop.Launcher
{
  /// <summary>
  /// Interaction logic for ConfigurationViewer.xaml
  /// </summary>
  public partial class ConfigurationViewer : UserControl, IDialog
  {
    public ConfigurationViewer(string fileToLoad_)
    {
      this.InitializeComponent();
        this.Parameters = new InitialLauncherDialogWindowParameters();
        Parameters.Caption = "Configuration Viewer";
        Parameters.Height = 480;
        Parameters.Width = 640;
        Parameters.SizingMethod = SizingMethod.Custom;
        Parameters.StartupLocation = WindowStartupLocation.CenterScreen; 
        Parameters.ShowInTaskbar = false;
        Parameters.Icon = Launcher.Controller.Settings.Image;

        _pathText.Text = fileToLoad_;

      XmlDocument doc = Launcher.GetConfigDoc(fileToLoad_);

      // format the XML nicely
      using (MemoryStream ms = new MemoryStream())
      {
        XmlTextWriter writer = new XmlTextWriter(ms, System.Text.Encoding.Unicode);
        writer.Formatting = Formatting.Indented;
        doc.WriteContentTo(writer);
        writer.Flush();
        ms.Flush();
        ms.Position = 0;
        using (StreamReader sr = new StreamReader(ms))
        {
          _mainText.Text = sr.ReadToEnd();
        }
      }
    }

    private void Button_Click(object sender, System.Windows.RoutedEventArgs e)
    {
      this.Close();
    }



    public InitialLauncherDialogWindowParameters Parameters { get; private set; }
  }
}