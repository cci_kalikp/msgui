﻿using System;
using System.Windows;
using System.IO;
using System.Diagnostics;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication;

namespace MSDesktop.Launcher.UI
{
  /// <summary>
  /// Interaction logic for AboutWindow.xaml
  /// </summary>
    public partial class AboutWindow : UserControl, IDialog
  {
    private const string CLASS_NAME = "AboutForm";

    private ApplicationController m_controller;

    public AboutWindow(ApplicationController controller_)
    {
      InitializeComponent();

      m_controller = controller_;

      _applicationName.Content = m_controller.Settings.Title;
      _applicationVersion.Content = Application.Current.MainWindow.GetType().Assembly.GetName().Version.ToString(3);
      _appConfigUpdateTime.Content = m_controller.AppConfigUpdateTime.ToString("HH:mm:ss ddd dd-MMM-yyyy");

#if DEBUG
      _buildType.Content = "[Debug Build]";
#else
			_buildType.Content = "Release Build";
#endif
      Parameters = new InitialLauncherDialogWindowParameters();
        Parameters.Icon = Launcher.Controller.Settings.Image;
        Parameters.Height = 340;
        Parameters.Width = 300;
        Parameters.SizingMethod = SizingMethod.Custom;
        Parameters.Caption = "About " + m_controller.Settings.Title;
        Parameters.ResizeMode = ResizeMode.NoResize;
        Parameters.ShowInTaskbar = false;
        Parameters.WindowStyle = WindowStyle.ToolWindow; 
    }

    public InitialLauncherDialogWindowParameters Parameters { get; private set; }

    private void Button_Click(object sender, System.Windows.RoutedEventArgs e)
    {
      this.Close();
    }

    private void _configButton_Click(object sender, System.Windows.RoutedEventArgs e)
    {
      try
      {
        ConfigurationViewer viewer = new ConfigurationViewer(Launcher.BackupFile);
        viewer.ShowDialog();

      }
      catch (Exception ex_)
      {
        DialogExtensions.ShowMessage(null, "Could not display config file", "Error with config file", MessageBoxImage.Warning);
        LogLayer.Error.Location(CLASS_NAME, "_configButton_Click").Append(ex_.ToString()).Send();
      }
    }

    private void _restartButtonClick(object sender, System.Windows.RoutedEventArgs e)
    {
      this.Close();
      m_controller.RestartLauncher();
    }

    private void _openLogButtonClick(object sender, RoutedEventArgs e)
    {
        var logFile = Launcher.LogFile;
      try
      {
        var processStartInfo = new ProcessStartInfo(logFile)
        {
          UseShellExecute = true,
        };
        Process.Start(processStartInfo);
      }
      catch (Exception ex_)
      {
          DialogExtensions.ShowMessage(null, string.Format("Error open log {0}! Error\n{1}", logFile, ex_.Message), "Error!", MessageBoxImage.Error);
      }
    }

  }
}
