﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls; 
using Microsoft.Win32;
using MSDesktop.Launcher.Model;
using MorganStanley.MSDotNet.MSGui.Core; 

namespace MSDesktop.Launcher.UI
{
  /// <summary>
  /// Interaction logic for RunDialog.xaml
  /// </summary>
  public partial class RunDialog : UserControl, IDialog
  {
    private ApplicationController _controller;

    public RunDialog()
    {
      InitializeComponent();
      this.Parameters = new InitialLauncherDialogWindowParameters();
      Parameters.Caption = "Run";
      Parameters.Width = 490;
      Parameters.SizingMethod = SizingMethod.SizeToContent;
      Parameters.StartupLocation = WindowStartupLocation.CenterScreen;
      Parameters.ShowInTaskbar = false;
      Parameters.Topmost = true;
      Parameters.ResizeMode = ResizeMode.NoResize;
      Parameters.WindowStyle = WindowStyle.ToolWindow;
      Parameters.Icon = Launcher.Controller.Settings.Image;

      _fileNameText.TextChanged += new TextChangedEventHandler(_fileNameText_TextChanged);
      _runButton.IsEnabled = false;
    }

    public RunDialog(ApplicationController controller_, string currentlySelectedGroup_) : this()
    {
      _controller = controller_;

      _applicationCombo.ItemsSource = controller_.CopyLocalReleases.Keys;

      _applicationCombo.DisplayMemberPath = "Description";

        if (!string.IsNullOrEmpty(currentlySelectedGroup_))
        {
            var selectedApp =
                controller_.CopyLocalReleases.Keys.FirstOrDefault(a_ => a_.Description == currentlySelectedGroup_);
            if (selectedApp != null)
            {
                _applicationCombo.SelectedItem = selectedApp;
            }
        } 
    }

    public ViewInfo ViewInfo { get; private set; }

    void _fileNameText_TextChanged(object sender, TextChangedEventArgs e)
    {
      if (_fileNameText.Text.ToLower().EndsWith("zip") && _fileNameText.Text.ToLower().StartsWith(@"\\ms\dist") && File.Exists(_fileNameText.Text))
      {
        _runButton.IsEnabled = true;
        _runButton.IsDefault = true;
        _browserButton.IsDefault = false;
        _errorTextBlock.Visibility = Visibility.Collapsed;
        string[] splitPath = _fileNameText.Text.Split('\\');
        string package = splitPath[splitPath.Length - 1];
        _executableTextBox.Text = package.Replace(".zip", ".exe").Replace(".ZIP", ".exe");
      }
      else
      {
        _executableTextBox.Text = String.Empty;
        _errorTextBlock.Visibility = Visibility.Visible;
        _runButton.IsEnabled = false;
      }
    }

    void browseButton_Click(object sender, RoutedEventArgs e)
    {
      OpenFileDialog openFileDialog = new OpenFileDialog();
      openFileDialog.Filter = "Packages (*.zip)|*.zip";
      openFileDialog.ShowDialog(Window.GetWindow(this));
      _fileNameText.Text = openFileDialog.FileName;
    }

    void runButton_Click(object sender, RoutedEventArgs e)
    {
      string[] splitPath = _fileNameText.Text.Split('\\');
      string package = splitPath[splitPath.Length - 1];
      string meta = splitPath[4];
      string project = splitPath[6];
      string release = splitPath[7];

      ActiveRelease activeRelease = new ActiveRelease(release, _executableTextBox.Text, "COPYLOCAL", null, null, package);
        var app = _applicationCombo.SelectedItem as Model.Application ?? 
            new Model.Application()
            {
                Description = (_applicationCombo.SelectedItem ?? string.Empty).ToString(),
                Meta = meta,
                Project = project,
                ID = m_appID, 
            };
        Model.Environment env = new Model.Environment() {Arguments = _argumentsTextBox.Text};
      ViewInfo = new ViewInfo(app, null, env, activeRelease, false, null, null);

      this.Close();
    }

    private void _applicationCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
    { 
      _versionCombo.ItemsSource = null;
      _packageCombo.ItemsSource = null;
      _fileNameText.Text = String.Empty;
      _executableTextBox.Text = String.Empty;

      Model.Application selectedApp = _applicationCombo.SelectedItem as Model.Application;
      if (selectedApp != null)
      {
          var activeReleases = _controller.CopyLocalReleases[selectedApp]; 
          _executableTextBox.ItemsSource = activeReleases; 
          _executableTextBox.Text = activeReleases[0];

          string directory = String.Format(@"\\san01b\DevAppsGML\dist\{0}\PROJ\{1}\", selectedApp.Meta, selectedApp.Project);
          List<string> keys = new List<string>();
          if (Directory.Exists(directory))
          {
              foreach (string dir in Directory.EnumerateDirectories(directory))
              {
                  keys.Add(dir.Substring(dir.LastIndexOf("\\") + 1));
              }
              keys.Sort((s1_, s2_) => System.String.Compare(s1_, s2_, System.StringComparison.Ordinal));
              _versionCombo.ItemsSource = keys;
          }
      }

    }

    private void _versionCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
        Model.Application selectedApp = _applicationCombo.SelectedItem as Model.Application;
        if (selectedApp == null) return;
      string version = _versionCombo.SelectedItem as string;
        if (version == null) return;
      _packageCombo.ItemsSource = null;
      _fileNameText.Text = String.Empty;
      _executableTextBox.Text = String.Empty; 

        SortedList<string, string> sortedList = new SortedList<string, string>();
        string directory = String.Format(@"\\san01b\DevAppsGML\dist\{0}\PROJ\{1}\{2}\", selectedApp.Meta, selectedApp.Project, version);
        if (Directory.Exists(directory))
        {
          foreach (string zip in Directory.EnumerateFiles(directory, "*.ZIP"))
          {
            sortedList.Add(zip.Substring(zip.LastIndexOf("\\") + 1), String.Empty);
          }
        }
        _packageCombo.ItemsSource = sortedList.Keys;

        if (_packageCombo.Items.Count > 0)
        {
          _packageCombo.SelectedItem = _packageCombo.Items[0];
        } 
    }

      private void _packageCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
      {
          Model.Application application = _applicationCombo.SelectedItem as Model.Application;
          if (application == null) return;
          string version = _versionCombo.SelectedItem as string;
          if (version == null) return;
          string package = _packageCombo.SelectedItem as string;
          if (package == null) return; 

          string file = String.Format(@"\\san01b\DevAppsGML\dist\{0}\PROJ\{1}\{2}\{3}", application.Meta, application.Project, version,
                                      package); 
          _fileNameText.Text = file;

      }

      #region Private Member Variables
    private string m_appID;
    #endregion Private Member Variables

    public InitialLauncherDialogWindowParameters Parameters { get; private set; }
  }
}
