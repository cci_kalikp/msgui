﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSDesktop.Launcher.UI
{
    public interface IDialog
    {
        InitialLauncherDialogWindowParameters Parameters { get; }
    }
}
