﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Impl;

namespace MSDesktop.Launcher.MessageDispatcher
{
    internal static class Extension
    {
        public static void EnableCrossLauncherMessage(this Framework framework_, string sucessOutputOverride_=null, string failureOutputOverride_=null)
        {
            framework_.AddModule<ApplicationMessageHandler>();
            ApplicationMessageHandler.SucceedMessageOverride = sucessOutputOverride_;
            ApplicationMessageHandler.FailMessageOverride = failureOutputOverride_;
        }
    } 
    
}
