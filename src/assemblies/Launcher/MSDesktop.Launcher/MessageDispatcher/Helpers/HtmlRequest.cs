﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.HTTPServer; 

namespace MSDesktop.Launcher.MessageDispatcher.Helpers
{
    [Serializable]
    public class HtmlRequest : MessageWithHttpResponse<HtmlRequestResponse>
    {
        public string PageId { get; set; }
    }

    public class HtmlRequestResponse : IMessageResponse
    { 
        public string Sequence { get; set; }

        public string Content { get; set; }
    }
}
