﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Windows;
using MSDesktop.Launcher.MessageDispatcher.Helpers;
using MSDesktop.Launcher.MessageDispatcher.Messages;
using MSDesktop.MessageRouting;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication;
using Newtonsoft.Json;
using DataFormat = MorganStanley.MSDotNet.MSGui.Impl.HTTPServer.DataFormat;

namespace MSDesktop.Launcher.MessageDispatcher
{
    internal class StatusReporter
    {
        private readonly IRoutedPublisher<DispatchableLauncherMessageResponse> remoteResponsePublisher;
        private readonly IModulePublisher<DispatchableLauncherMessageResponse> localResponsePublisher;
        private readonly IModulePublisher<HtmlRequestResponse> htmlResponsePublisher;
        private readonly string htmlPagePublisherId = "pubid5AEA7CB8";
        public StatusReporter(ICommunicator machineWideCommunicator_, IRoutingCommunicator crossMachineCommunicator_)
        {
            localResponsePublisher = machineWideCommunicator_.GetPublisher<DispatchableLauncherMessageResponse>();
            remoteResponsePublisher = crossMachineCommunicator_.GetPublisher<DispatchableLauncherMessageResponse>();
            crossMachineCommunicator_.GetSubscriber<DispatchableLauncherMessageResponse>()
                        .GetObservable()
                        .Subscribe(PublishResponseLocally);
            machineWideCommunicator_.GetSubscriber<HtmlRequest>().GetObservable().Subscribe(GenerateHtml);

            string url = DispatchableLauncherMessage.RequestBuilder.BuildRegisterPulisherUrl(typeof(HtmlRequest), ref htmlPagePublisherId);
            using (WebClient webClient = new WebClient())
            {
                webClient.DownloadString(url);
            }
            htmlResponsePublisher = machineWideCommunicator_.GetPublisher<HtmlRequestResponse>(); 
        }

        public void SuppressReport(string messageId_)
        {
            silentMessages.Add(messageId_);
        }

        public void ReportStatus(DispatchableLauncherMessage message_, string status_, bool suceeded_)
        {
            string content = suceeded_ ? status_ : string.Format("<font color='red'>{0}</font>", status_);
            var response = message_.GetResponse(content);
            response.RawContent = status_;
            response.Suceeded = suceeded_;
            var talkMessage = message_ as IDispatchableLauncherMessageInternal;
            if (talkMessage != null && !string.IsNullOrEmpty(talkMessage.SourceHostName) &&
                !talkMessage.SourceHostName.Equals(Environment.MachineName))
            {
                response.TargetHostName = talkMessage.SourceHostName;
                remoteResponsePublisher.Publish(response);
            }
            else
            {
                PublishResponseLocally(response);
            }
        }


        public void ReportProgress(string status_, int progress_, string messageId_)
        {
            LogLayer.Debug.Location("StatusReporter", "ReportProgress").Append(messageId_ + ": " + status_).SendQuietly();
 

            if (silentMessages.Contains(messageId_)) return;
            TaskDialogHandle handler;
            lock (handlers)
            {
                if (!handlers.TryGetValue(messageId_, out handler))
                {
                    handler = new TaskDialogHandle();
                    handlers.Add(messageId_, handler);

                    TaskDialog.ShowNoneModal(new TaskDialogOptions()
                    {
                        Title = Launcher.Controller.Settings.Title,
                        ProgressBarInitialValue = progress_,
                        ProgressBarMaximum = 100,
                        ProgressBarMinimum = 0,
                        Content = status_,
                        AllowDialogCancellation = false,
                        CommonButtons = TaskDialogCommonButtons.NoneAndNotClosable
                    }, handler);
                }
            }
            handler.ProgressBarCurrentValue = progress_;
            handler.Content = status_;
        }
        private const string TemplateWithoutLink = @"<!DOCTYPE html>
<html>
<body> 
<p>{0}</p>
</body>
</html>
";
        private const string TemplateWithLink = @"<!DOCTYPE html>
<html>
<body>
<p>{0}</p></b>
<a href='{1}'>Click here for more information</a>
</body>
</html>
";
        Dictionary<string, string> htmlMap = new Dictionary<string, string>();  
        private void PublishResponseLocally(DispatchableLauncherMessageResponse response_)
        {
            string title = null;
            string path = null;
            var viewInfo = Launcher.Controller.GetViewInfoForKey(response_.ApplicationKey);
            string pageId = null;
            if (response_.Suceeded)
            {
                if (viewInfo != null)
                {
                    title = viewInfo.MessageDispatchSucceedMessageOverride;
                    if (!string.IsNullOrEmpty(viewInfo.MessageDispatchSucceedHtml))
                    {
                        path = viewInfo.MessageDispatchSucceedHtml;
                        pageId = viewInfo.UniqueKey + ".S";             
                    }
                }
                if (string.IsNullOrEmpty(title) && ApplicationMessageHandler.SucceedMessageOverride != null)
                {
                    title = ApplicationMessageHandler.SucceedMessageOverride; 
                }
            }
            else
            {
                if (viewInfo != null)
                {
                    title = viewInfo.MessageDispatchFailMessageOverride;
                    if (!string.IsNullOrEmpty(viewInfo.MessageDispatchFailHtml))
                    {
                        path = viewInfo.MessageDispatchFailHtml;
                        pageId = viewInfo.UniqueKey + ".F";
                    } 
                }
                if (string.IsNullOrEmpty(title) && ApplicationMessageHandler.FailMessageOverride != null)
                {
                    title = ApplicationMessageHandler.FailMessageOverride; 
                }
            }
            if (string.IsNullOrEmpty(title))
            {
                title = response_.Content;
            }
            if (!string.IsNullOrEmpty(path))
            {
                htmlMap[pageId] = path;
                path = DispatchableLauncherMessage.RequestBuilder.BuildPulishUrl(
                    JsonConvert.SerializeObject(
                    new HtmlRequest() { PageId = pageId }, 
                    new JsonSerializerSettings(){ NullValueHandling = NullValueHandling.Ignore}),
                     DataFormat.Json, htmlPagePublisherId);
            }
            response_.Content = string.IsNullOrEmpty(path)
                                    ? string.Format(TemplateWithoutLink, title)
                                    : string.Format(TemplateWithLink, title, path);

            localResponsePublisher.PublishRelativeToCurrent(response_, CommunicationTargetFilter.MachineInstance);
            CloseProgressWindow(response_.Sequence, response_.RawContent);
        }

        private void CloseProgressWindow(string sequence_, string result_)
        {
            lock (silentMessages)
            {
                silentMessages.Remove(sequence_);      
            }
            TaskDialogHandle handler = null;
            lock (handlers)
            {
                if (handlers.TryGetValue(sequence_, out handler))
                {
                    handlers.Remove(sequence_);
                }
            }

            if (handler != null)
            {
                Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                {
                    handler.ProgressBarCurrentValue = 100;
                    handler.Content = result_;
                }));
                Thread.Sleep(1000);
                Application.Current.Dispatcher.BeginInvoke(new Action(() => handler.Close()));
            }

        }

        readonly Dictionary<string, TaskDialogHandle> handlers = new Dictionary<string, TaskDialogHandle>();
        readonly List<string> silentMessages = new List<string>();  
        private void GenerateHtml(HtmlRequest request_)
        {
            string path;
            if (!htmlMap.TryGetValue(request_.PageId, out path))
            {
                if (request_.PageId.Length > 2)
                {
                    string applicationKey = request_.PageId.Remove(request_.PageId.Length - 2);
                    var viewInfo = Launcher.Controller.GetViewInfoForKey(applicationKey);
                    path = request_.PageId.EndsWith(".S")
                               ? viewInfo.MessageDispatchSucceedHtml
                               : viewInfo.MessageDispatchFailHtml;
                } 
            }
            try
            {
                if (!string.IsNullOrEmpty(path) && File.Exists(path))
                {
                    string content = File.ReadAllText(PathUtilities.ReplaceEnvironmentVariables(path));
                    htmlResponsePublisher.PublishRelativeToCurrent(request_.GetResponse(content), CommunicationTargetFilter.MachineInstance);
                }
                else
                {
                    htmlResponsePublisher.PublishRelativeToCurrent(request_.GetResponse("Failed to open the page"), CommunicationTargetFilter.MachineInstance);
                }
            }
            catch 
            {
                htmlResponsePublisher.PublishRelativeToCurrent(request_.GetResponse("Failed to open the page"), CommunicationTargetFilter.MachineInstance);
            }

        }
    }
}
