﻿using System;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication;

namespace MSDesktop.Launcher.MessageDispatcher.Helpers
{
    internal class ApplicationIPCReadyEventArgs:EventArgs
    {
        public ApplicationIPCReadyEventArgs(ReceiverReadyMessage readyMessage_)
        {
            Message = readyMessage_;
        }

        public ReceiverReadyMessage Message { get; private set; }
    }
}
