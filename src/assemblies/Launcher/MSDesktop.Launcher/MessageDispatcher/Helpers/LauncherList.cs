﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel; 
using System.Threading;
using System.Timers;
using MSDesktop.Launcher.MessageDispatcher.Messages; 
using MSDesktop.MessageRouting;
using MSDesktop.MessageRouting.Routing;
using MSDesktop.MessageRouting.ZeroConfig;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication;
using Timer = System.Timers.Timer;

namespace MSDesktop.Launcher.MessageDispatcher.Helpers
{ 
    public class LauncherList
    {
        private readonly IRouteExplorer routeExplorer;
        private readonly IRoutingCommunicator routingCommunicator;
        private IRoutedPublisher<PingLauncherMessage> pingLauncherPublisher; 
        private readonly Timer timer = new Timer(300000); //refresh every 5 minute
        private DateTime lastRefreshTime;
        private ObservableCollection<Endpoint> launchers;
        private ObservableCollection<Endpoint> tempLaunchers;
        private const string CLASS_NAME = "LauncherList"; 
        private List<Endpoint> localEndPoints; 
        internal LauncherList(IRouteExplorer routeExplorer_, IRoutingCommunicator routingCommunicator_, Communicator communicator_)
        {
            routeExplorer = routeExplorer_;
            routingCommunicator = routingCommunicator_;
            if (communicator_.CpsInitialized || Launcher.Controller.Settings.SuppressCrossLauncher)
            {
                if (!communicator_.CpsEnabled || Launcher.Controller.Settings.SuppressCrossLauncher)
                {
                    localEndPoints = new List<Endpoint>() { Endpoint.LocalEndpoint };
                }
                else
                {
                    SetupLauncherRefresher();   
                }
            }
            else
            {
                EventHandler cpsInitializedHandler = null;
                communicator_.AfterCpsInitialized += cpsInitializedHandler = (sender_, args_) =>
                    {
                        communicator_.AfterCpsInitialized -= cpsInitializedHandler;
                        if (!communicator_.CpsEnabled)
                        {
                            localEndPoints = new List<Endpoint>() { Endpoint.LocalEndpoint };
                        }
                        else
                        {
                            SetupLauncherRefresher();
                        }
                    };
            } 
            
        }

        private void SetupLauncherRefresher()
        {
            routingCommunicator.GetSubscriber<PingLauncherMessage>().GetObservable().Subscribe();
            pingLauncherPublisher = routingCommunicator.GetPublisher<PingLauncherMessage>();
            RefreshLaunchers();
            routeExplorer.SubscriptionAvailableEvent += routeExplorer_SubscriptionAvailableEvent;
            routeExplorer.SubscriptionUnavailableEvent += routeExplorer_SubscriptionUnavailableEvent;
            timer.Enabled = true;
            timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
        }

        void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (((TimeSpan)(DateTime.Now - lastRefreshTime)).TotalSeconds > 240) //launcher list is older than 4 min 
              {
                  RefreshLaunchers();
              }
        }

        void routeExplorer_SubscriptionUnavailableEvent(object sender, SubscriptionUnavailableEventArgs e)
        {
            lock (launchers)
            {
                LogLayer.Info.Location(CLASS_NAME, "routeExplorer_SubscriptionUnavailableEvent")
                       .Append(string.Format("Launcher at {0} goes offline", e.SubscriberIdentity)).Send();
                launchers.Remove(e.SubscriberIdentity);
            }
            if (tempLaunchers != null)
            {
                lock (tempLaunchers)
                {
                    launchers.Remove(e.SubscriberIdentity);
                }
            }
        }

        void routeExplorer_SubscriptionAvailableEvent(object sender, SubscriptionAvailableEventArgs e)
        {
            if (e.MessageType == typeof(TalkToApplicationMessage) )
            {
                lock (launchers)
                {
                    if (!launchers.Contains(e.SubscriberIdentity) && e.SubscriberIdentity.Application == Endpoint.LocalEndpoint.Application)
                    {
                        LogLayer.Info.Location(CLASS_NAME, "routeExplorer_SubscriptionAvailableEvent")
       .Append(string.Format("Launcher at {0} goes online", e.SubscriberIdentity)).Send();
                        launchers.Add(e.SubscriberIdentity);
                    }
                }
                if (tempLaunchers != null)
                {
                    lock (tempLaunchers)
                    {
                        if (!tempLaunchers.Contains(e.SubscriberIdentity))
                        {
                            tempLaunchers.Add(e.SubscriberIdentity);
                        }
                    }
                }
            }
        }

        public void RefreshLaunchers()
        {
            lastRefreshTime = DateTime.Now;
            if (launchers == null)
            {
                launchers = routeExplorer.GetSubscribedApplications(typeof(TalkToApplicationMessage));
            }
            else
            {
                tempLaunchers = routeExplorer.GetSubscribedApplications(typeof(TalkToApplicationMessage));
                Timer timer2 = new Timer(10000); //wait for 10 sec
                timer2.Enabled = true;
                timer2.Elapsed += new ElapsedEventHandler(timer2_Elapsed);
            }
       
        }

        void timer2_Elapsed(object sender, ElapsedEventArgs e)
        {
            Timer timer = sender as Timer;
            timer.Enabled = false;
            timer.Elapsed -= timer2_Elapsed;
            timer.Dispose();
            lock (launchers)
            {
                if (tempLaunchers == null) return;
                lock (tempLaunchers)
                {
                    foreach (var tempLauncher in tempLaunchers)
                    {
                        if (!launchers.Contains(tempLauncher))
                        {
                            LogLayer.Info.Location(CLASS_NAME, "RefreshLaunchers")
    .Append(string.Format("Launcher at {0} goes online", tempLauncher)).Send();
                        }
                    }
                    foreach (var launcher in launchers)
                    {
                        if (!tempLaunchers.Contains(launcher))
                        {
                            LogLayer.Info.Location(CLASS_NAME, "RefreshLaunchers")
    .Append(string.Format("Launcher at {0} goes offline", launcher)).Send();
                        }
                    }
                    launchers = tempLaunchers;
                }
                tempLaunchers = null;
         
            }
        }


        public List<Endpoint> Launchers
        {
            get
            {
                if (localEndPoints != null)
                {
                    return localEndPoints;
                }
                lock (launchers)
                {
                    var launchersLocal = new List<Endpoint>();
                    launchersLocal.AddRange(launchers);
                    if (((TimeSpan)(DateTime.Now - lastRefreshTime)).TotalSeconds > 5)
                    //launcher list is older than 5 sec
                    {
                        launchersLocal = ProbeTargets(launchersLocal, pingLauncherPublisher,
                                     endpoint_ => new PingLauncherMessage() { Target = endpoint_ }, DeliveryStrategyAttribute.DefaultTimeout + 1000).FindAll(endPoint_ => endPoint_.Application == Endpoint.LocalEndpoint.Application);
                        if (launchersLocal.Count != launchers.Count)
                        {
                            lock (launchers)
                            {
                                for (int i = launchers.Count - 1; i >= 0; i--)
                                {
                                    var launcher = launchers[i];
                                    if (!launchersLocal.Contains(launcher))
                                    {
                                        LogLayer.Info.Location(CLASS_NAME, "ProbeTargets")
                                                .Append(string.Format("Launcher at {0} goes offline", launcher)).Send();
                                        launchers.RemoveAt(i);
                                    }
                                }
                            }
                        }
                    }
                    if (launchersLocal.Count == 0)
                    {
                        launchersLocal.Add(Endpoint.LocalEndpoint);
                    }
                    return launchersLocal;
                }

            }
        }

        public List<Endpoint> ValidateLaunchers<TMessage>(IRoutedPublisher<TMessage> publisher_,
                                                Func<Endpoint, TMessage> createMessage_, int timeout_)
        {
            return ProbeTargets(Launchers, publisher_, createMessage_, timeout_);
        }
         

        private List<Endpoint> ProbeTargets<TMessage>(List<Endpoint> candidateTargets_, IRoutedPublisher<TMessage> publisher_, Func<Endpoint, TMessage> createMessage_, int timeout_)
        {
            var ackTargets = new List<Endpoint>();
            var deliverEvents = new List<WaitHandle>();
            foreach (var candidateTarget in candidateTargets_)
            {
                var copyTarget = candidateTarget;
                var indefinitiveTalkToAppMessage = createMessage_(copyTarget);
                var deliverEvent = new AutoResetEvent(false);
                deliverEvents.Add(deliverEvent);
                var di2 = publisher_.Publish(indefinitiveTalkToAppMessage);
                if (di2.Status == DeliveryStatus.Failed)
                {
                    deliverEvent.Set();
                    deliverEvents.Remove(deliverEvent);
                }
                else
                {
                    PropertyChangedEventHandler delivered = null;
                    di2.PropertyChanged += delivered = (sender_, args_) =>
                    {
                        var diLocal = sender_ as DeliveryInfo;
                        if (diLocal != null)
                        {
                            if (diLocal.Status == DeliveryStatus.Delivered)
                            {
                                ackTargets.Add(copyTarget);
                                deliverEvent.Set();
                                deliverEvents.Remove(deliverEvent);
                                di2.PropertyChanged -= delivered;
                            }
                            else if (diLocal.Status == DeliveryStatus.Failed)
                            {
                                deliverEvent.Set();
                                deliverEvents.Remove(deliverEvent);
                                di2.PropertyChanged -= delivered;
                            }
                        }
                    };
                }

            }
            WaitHandle[] deliverEventArray = null;
            lock (deliverEvents)
            {
                deliverEventArray = deliverEvents.ToArray();
            }
            if (deliverEventArray.Length > 0)
            {
                WaitHandle.WaitAll(deliverEventArray, timeout_);              
            }
            return ackTargets;
        }
         
        public ObservableCollection<Endpoint> LaunchersReading
        {
            get { return tempLaunchers ?? launchers; }
        }
 
    }
}
