﻿using System;
using MSDesktop.Launcher.UI;
using MSDesktop.MessageRouting;
using MSDesktop.MessageRouting.Routing;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication;

namespace MSDesktop.Launcher.MessageDispatcher.Messages
{
    [Serializable] 
    [DeliveryStrategy(Fallback = false)]
    public class TalkToApplicationMessageBypassRouting : DispatchableLauncherMessage, IBypassRouting, IDispatchableLauncherMessageInternal
    {
        public TalkToApplicationMessageBypassRouting()
        { 
        }
        public TalkToApplicationMessageBypassRouting(DispatchableLauncherMessage message_, ViewInfo viewInfo_, Endpoint target_)
        {
            this.Sequence = message_.Sequence;
            this.AppKey = message_.AppKey;
            this.ViewInfo = new ViewInfoSerializable(viewInfo_);
            this.AlwaysNewApp = message_.AlwaysNewApp;
            this.Payload = message_.Payload;
            this.CanNewApp = message_.CanNewApp;
            this.CanSaveRoute = message_.CanSaveRoute;
            this.SourceHostName = Environment.MachineName;
            this.Target = target_; 
            if (viewInfo_.EnvironmentGroup == null)
            {
                ApplicationName = viewInfo_.ApplicationDescription + ":" + viewInfo_.Version + "\\" + viewInfo_.Release.Executable ;

            }
            else
            {
                ApplicationName = viewInfo_.ApplicationDescription + ":" + viewInfo_.EnvironmentName ;
            }
            if (!string.IsNullOrEmpty(viewInfo_.ExtraArguments))
            {
                ApplicationName += ": " + viewInfo_.ExtraArguments;
            }
        }

        public TalkToApplicationMessageBypassRouting(DispatchableLauncherMessage message_, ViewInfoSerializable viewInfo_, string applicationName_, Endpoint target_)
        {
            this.Sequence = message_.Sequence;
            this.AppKey = message_.AppKey;
            this.ViewInfo = viewInfo_;
            this.AlwaysNewApp = message_.AlwaysNewApp;
            this.Payload = message_.Payload;
            this.CanNewApp = message_.CanNewApp;
            this.CanSaveRoute = message_.CanSaveRoute;
            this.SourceHostName = Environment.MachineName;
            this.Target = target_;
            this.ApplicationName = applicationName_; 
        }

        public string SourceHostName { get; set; }
        public string ApplicationName { get; set; } 
        public ViewInfoSerializable ViewInfo { get; set; }
        [FallbackGroupComparator]
        public virtual bool SameApplication(object message_)
        {
            if (this == message_) return true;
            var m = message_ as TalkToApplicationMessageBypassRouting; 
            if (m == null) return false;
            return m.AppKey == AppKey;
        }
         
        public virtual string GetInfo()
        {
            return string.Format("Send message to Application '{0}'", ApplicationName);
        }

        public Endpoint Target { get; set; }

    }
}
