﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSDesktop.Launcher.MessageDispatcher.Messages
{
    public interface IDispatchableLauncherMessageInternal
    {
        string ApplicationName { get; set; }
        string SourceHostName { get; set; } 
    }
}
