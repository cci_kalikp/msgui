﻿using System;
using MSDesktop.Launcher.UI;
using MSDesktop.MessageRouting;
using MSDesktop.MessageRouting.Message;
using MSDesktop.MessageRouting.Routing;

namespace MSDesktop.Launcher.MessageDispatcher.Messages
{
    //instead of a BroadcastAttribute to mark the message, we implement IBypassRouting to send message to all available subscribers
    //since if using BroadcastAttribute, onces one delivery succeeded, the message would be removed from the queue, so we no longer know if it's been delivered to any more targets
    [Serializable]
    [DeliveryStrategy(Fallback = false,Timeout=Consts.ProcessReadyTimeout)]
    public class PingApplicationMessage : IProvideMessageInfo,IBypassRouting
    {
        public ViewInfoSerializable ViewInfo { get; set; }
        public Endpoint Target { get; set; } 
        public string ApplicationName { get; set; } 
        [FallbackGroupComparator]
        public bool SameApplication(object message_)
        {
            if (this == message_) return true;
            var m = message_ as PingApplicationMessage;
            if (m == null) return false;
            return m.ViewInfo.UniqueKey.Equals(this.ViewInfo.UniqueKey) && 
                (m.ViewInfo.ExtraArguments ?? string.Empty) == (this.ViewInfo.ExtraArguments ?? string.Empty);
        }

        public string GetInfo()
        {
            return string.Format("Probe '{0}' to see if Application '{1}' is running", Target, ApplicationName);
        }

        public override string ToString()
        {
            return GetInfo();
        }

    }
}
