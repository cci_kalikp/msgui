﻿using System;
using System.Linq;
using MSDesktop.Launcher.Model;
using MSDesktop.Launcher.UI;

namespace MSDesktop.Launcher.MessageDispatcher.Messages
{
    [Serializable]
    public class ViewInfoSerializable
    {
        public string ApplicationKey { get; set; }
        public string EnvironmentGroupName { get; set; }
        public string EnvironmentName { get; set; }
        

        public ActiveRelease Release { get; set; }
        public string UniqueKey { get; set; }
        public string ExtraArguments { get; set; }
        public string EnvironmentArguments { get; set; }

        public ViewInfoSerializable()
        {
        }

        public ViewInfoSerializable(ViewInfo viewInfo_)
        {
            this.ApplicationKey = viewInfo_.ApplicationKey;
            this.Release = viewInfo_.Release;
            if (viewInfo_.Environment != null)
            {
                this.EnvironmentName = viewInfo_.Environment.Name;
                EnvironmentArguments = viewInfo_.Environment.Arguments;
            } 
            this.EnvironmentGroupName = viewInfo_.EnvironmentGroupName;
            this.UniqueKey = viewInfo_.UniqueKey;
            this.ExtraArguments = viewInfo_.ExtraArguments;

        }

        public ViewInfo ConvertToViewInfo()
        {
            if (!string.IsNullOrEmpty(UniqueKey))
            {
                var viewInfo = Launcher.Controller.GetViewInfoForKey(UniqueKey);
                if (viewInfo != null)
                {
                    if (string.IsNullOrEmpty(ExtraArguments)) return viewInfo;

                    viewInfo = viewInfo.Clone();
                    viewInfo.ExtraArguments = ExtraArguments;
                    return viewInfo;
                }
            }
            Model.Application app = null;
            if (!string.IsNullOrEmpty(ApplicationKey))
            {
                app = Launcher.Controller.Config.Root.GetApplicationByKey(ApplicationKey);
            }
            EnvironmentGroup environmentGroup = null;
            if (!string.IsNullOrEmpty(EnvironmentGroupName) && app != null)
            {
                environmentGroup = app.EnvironmentGroups.FirstOrDefault(eg_ => eg_.Name == EnvironmentGroupName);
            }
            Model.Environment environment = null;
            if (!string.IsNullOrEmpty(EnvironmentName) && environmentGroup != null)
            {
                environment = environmentGroup.Environments.FirstOrDefault(e_ => e_.Name == EnvironmentName);
            }
            else
            {
                environment = new Model.Environment() { Arguments = EnvironmentArguments };
            } 
            return new ViewInfo(app, environmentGroup, environment, Release, false, null, null) { ExtraArguments = this.ExtraArguments };
        }
    }
}
