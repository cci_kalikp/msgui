﻿using System;
using MSDesktop.MessageRouting;
using MSDesktop.MessageRouting.Message;
using MSDesktop.MessageRouting.Routing;

namespace MSDesktop.Launcher.MessageDispatcher.Messages
{
    [Serializable]
    [DeliveryStrategy(Fallback = false, Timeout = Consts.StartProcessTimeout)]
    public class StartApplicationMessage : IProvideMessageInfo, IBypassRouting
    {
        public ViewInfoSerializable ViewInfo { get; set; }
        public string ApplicationName { get; set; } 
        public bool AlwaysNewApp { get; set; }

        public Endpoint Target { get; set; }
 
        public string GetInfo()
        {
            return string.Format("Start Application '{0}'", ApplicationName);
        }
    }
}
