﻿using System;
using MSDesktop.MessageRouting;
using MSDesktop.MessageRouting.Message;
using MSDesktop.MessageRouting.Routing; 

namespace MSDesktop.Launcher.MessageDispatcher.Messages
{
    [Serializable]
    [DeliveryStrategy(Fallback = false)]
    public class PingLauncherMessage : IProvideMessageInfo,IBypassRouting 
    { 
        public Endpoint Target { get; set; }

        public string GetInfo()
        {
            return string.Format("Probe '{0}' to see if '{1}' is running", Target, Launcher.Controller.Settings.Title);
        }
    }
}
