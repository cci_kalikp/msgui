﻿using System;
using MSDesktop.Launcher.UI;
using MSDesktop.MessageRouting.Message;
using MSDesktop.MessageRouting.Routing;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication;

namespace MSDesktop.Launcher.MessageDispatcher.Messages
{
    [Serializable]
    //time out = ping launcher timeout + ping application timeout + start process timeout + network overhead
    [DeliveryStrategy(Fallback = false, Timeout = Consts.StartProcessTimeout + Consts.NetworkOverheadTime + DeliveryStrategyAttribute.DefaultTimeout * 2)]
    public class TalkToApplicationMessage : DispatchableLauncherMessage, IProvideMessageInfo, IDispatchableLauncherMessageInternal
    { 
        public TalkToApplicationMessage()
        { 
        }
        public TalkToApplicationMessage(DispatchableLauncherMessage message_, ViewInfo viewInfo_)
        {
            this.Sequence = message_.Sequence;
            this.AppKey = message_.AppKey;
            this.ViewInfo = new ViewInfoSerializable(viewInfo_);
            this.AlwaysNewApp = message_.AlwaysNewApp;
            this.Payload = message_.Payload;
            this.CanNewApp = message_.CanNewApp;
            this.CanSaveRoute = message_.CanSaveRoute;
            this.SourceHostName = Environment.MachineName;
            if (viewInfo_.EnvironmentGroup == null)
            {
                ApplicationName = viewInfo_.ApplicationDescription + ":" + viewInfo_.Version + "\\" + viewInfo_.Release.Executable;

            }
            else
            {
                ApplicationName = viewInfo_.ApplicationDescription + ":" + viewInfo_.EnvironmentName;
            }
            if (!string.IsNullOrEmpty(viewInfo_.ExtraArguments))
            {
                ApplicationName += ": " + viewInfo_.ExtraArguments;
            }
        }

        public string SourceHostName { get; set; }
        public string ApplicationName { get; set; }
        public string InstanceName { get; set; }
        public ViewInfoSerializable ViewInfo { get; set; }
        [FallbackGroupComparator]
        public bool SameApplication(object message_)
        {
            if (this == message_) return true;
            var m = message_ as TalkToApplicationMessage; 
            if (m == null) return false;
            return m.AppKey == AppKey;
        }
         
        public virtual string GetInfo()
        {
            return string.Format("Send message to Application '{0}'", ApplicationName);
        }
          
    }
}
