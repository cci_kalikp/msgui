﻿using System;
using System.Collections.Generic; 
using System.ComponentModel; 
using System.IO;
using System.Linq;
using System.Net; 
using System.Threading;
using System.Windows;
using MSDesktop.Launcher.MessageDispatcher.Helpers;
using MSDesktop.Launcher.MessageDispatcher.Messages;
using MSDesktop.Launcher.MessageDispatcher.Persistence;
using MSDesktop.Launcher.MessageDispatcher.RouteFinders;
using MSDesktop.Launcher.Model;
using MSDesktop.Launcher.UI;
using MSDesktop.MessageRouting;
using MSDesktop.MessageRouting.GUI.Model;
using MSDesktop.MessageRouting.GUI.View;
using MSDesktop.MessageRouting.Routing;
using MSDesktop.MessageRouting.ZeroConfig;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Unity; 
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using System.Reactive.Linq; 
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication;
using Application = System.Windows.Application;
using Environment = System.Environment;

namespace MSDesktop.Launcher.MessageDispatcher
{
    public class ApplicationMessageHandler : IModule
    {
        private readonly ICommunicator machineWideCommunicator;
        private readonly IRoutingCommunicator crossMachineCommunicator; 
        private readonly IUnityContainer container; 
        private IRouteExplorer routeExplorer;
        private LauncherList targetLaunchers;
        private LauncherScreenCaptureSink launcherScreenCaptureSink;
        private ApplicationScreenCaptureSink applicationScreenCaptureSink;
        private LauncherRouteManager routeManager; 
        

        private IRoutedPublisher<TalkToApplicationMessage> talkToApplicationPulisher;
        private IRoutedPublisher<TalkToApplicationMessageBypassRouting> talkToApplicationBypassRoutingPublisher; 
        private IRoutedPublisher<PingApplicationMessage> pingApplicationPulisher;
        private IRoutedPublisher<StartApplicationMessage> startApplicationPublisher; 

        private IRoutedSubscriber<TalkToApplicationMessage> talkToApplicationSubscriber;
        private IRoutedSubscriber<TalkToApplicationMessageBypassRouting> talkToApplicationBypassRoutingSubscriber; 
        private IRoutedSubscriber<PingApplicationMessage> pingApplicationSubscriber;
        private IRoutedSubscriber<StartApplicationMessage> startApplicationSubscriber;

        private IModulePublisher<LauncherMessage> messagePublisher;
        private readonly Dictionary<string, DispatchableLauncherMessage> messagesDispatching = new Dictionary<string, DispatchableLauncherMessage>();
        private StatusReporter reporter;

        private static readonly string CLASS_NAME = typeof (ApplicationMessageHandler).Name;
        internal static event EventHandler<ApplicationIPCReadyEventArgs> ApplicationIPCReady; 
        internal static string SucceedMessageOverride;
        internal static string FailMessageOverride;
        
        public ApplicationMessageHandler(IApplication application_, ICommunicator machineWideCommunicator_, IRoutingCommunicator crossMachineCommunicator_, IUnityContainer container_)
        {
            machineWideCommunicator = machineWideCommunicator_;  
            crossMachineCommunicator = crossMachineCommunicator_;
            container = container_;
            application_.ApplicationLoaded += new EventHandler(application_ApplicationLoaded); 

        }

        #region Initialization
        public void Initialize()
        { 
        }


        void application_ApplicationLoaded(object sender, EventArgs e)
        {
            //TODO: NOTE: for test please remove it~~~~~~~~~~~~~~~~~~~~~~
            //Endpoint.LocalEndpoint.Username = Launcher.Controller.UserName;
            container.Resolve<IRouteManager>().UseDefaultRouting = false;
            routeManager = new LauncherRouteManager(container.Resolve<IApplicationOptions>(), container.Resolve<IPersistenceService>());
            ((IApplication)sender).ApplicationLoaded -= new EventHandler(application_ApplicationLoaded);
            var endpointSelector = container.Resolve<IEndpointSelector>();
            endpointSelector.AddEndpointSelectionStep(1500, ResolveLauncherTarget);
            routeExplorer = container.Resolve<IRouteExplorer>();
            targetLaunchers = new LauncherList(routeExplorer, crossMachineCommunicator, machineWideCommunicator as Communicator);

            machineWideCommunicator.GetSubscriber<DispatchableLauncherMessage>().GetObservable().Subscribe(RouteMessage);

            talkToApplicationPulisher = crossMachineCommunicator.GetPublisher<TalkToApplicationMessage>();
            talkToApplicationBypassRoutingPublisher = crossMachineCommunicator.GetPublisher<TalkToApplicationMessageBypassRouting>();
            pingApplicationPulisher = crossMachineCommunicator.GetPublisher<PingApplicationMessage>();
            startApplicationPublisher = crossMachineCommunicator.GetPublisher<StartApplicationMessage>();

            talkToApplicationSubscriber = crossMachineCommunicator.GetSubscriber<TalkToApplicationMessage>();
            talkToApplicationSubscriber.FilterMessage +=
                (sender_, args_) => args_.IsAcceptable = Launcher.Controller.IsProcessStarted(args_.Message.ViewInfo.ConvertToViewInfo());
            talkToApplicationSubscriber.GetObservable().Where(m_ => Launcher.Controller.IsProcessStarted(m_.ViewInfo.ConvertToViewInfo())).Subscribe(TalkToApplication);

            talkToApplicationBypassRoutingSubscriber = crossMachineCommunicator.GetSubscriber<TalkToApplicationMessageBypassRouting>();
            talkToApplicationBypassRoutingSubscriber.FilterMessage +=
                (sender_, args_) => args_.IsAcceptable = Launcher.Controller.IsProcessStarted(args_.Message.ViewInfo.ConvertToViewInfo());
            talkToApplicationBypassRoutingSubscriber.GetObservable().Where(m_ => Launcher.Controller.IsProcessStarted(m_.ViewInfo.ConvertToViewInfo())).Subscribe(TalkToApplication);


            pingApplicationSubscriber = crossMachineCommunicator.GetSubscriber<PingApplicationMessage>();
            //it's ok for us to make  the PingApplication MessageFilter to hold for a long time, since this is a usually targeted to a live target usually
            pingApplicationSubscriber.FilterMessage +=
                 (sender_, args_) =>
                 {
                     try
                     {
                         args_.IsAcceptable = Launcher.Controller.IsProcessReadyForIPC(args_.Message.ViewInfo.ConvertToViewInfo());
                     }
                     catch (Exception ex)
                     {
                         LogLayer.Error.Location(CLASS_NAME, "PingApplication").AppendException(ex).Send();
                     }
                 };
            pingApplicationSubscriber.GetObservable().Where(m_ => Launcher.Controller.IsProcessStarted(m_.ViewInfo.ConvertToViewInfo())).Subscribe();
            startApplicationSubscriber = crossMachineCommunicator.GetSubscriber<StartApplicationMessage>();
            //it's ok for us to make  the StartApplication MessageFilter to hold for a long time, since this is a usually targeted to a live target usually
            startApplicationSubscriber.FilterMessage +=
                (sender_, args_) =>
                {
                    try
                    {
                        string instanceName;
                        args_.IsAcceptable = Launcher.Controller.SyncStartProcess(args_.Message.ViewInfo.ConvertToViewInfo(), args_.Message.AlwaysNewApp, out instanceName);
                        args_.ExtraInformation = instanceName;
                    }
                    catch (Exception ex)
                    {
                        LogLayer.Error.Location(CLASS_NAME, "StartApplication").AppendException(ex).Send();
                    }
                };

            startApplicationSubscriber.GetObservable().Subscribe();

            launcherScreenCaptureSink = new LauncherScreenCaptureSink(crossMachineCommunicator);
            launcherScreenCaptureSink.SubscribeScreenCaptureRequest();
            applicationScreenCaptureSink = new ApplicationScreenCaptureSink(crossMachineCommunicator);
            applicationScreenCaptureSink.SubscribeScreenCaptureRequest();

            string publisherId = LauncherConsts.DispatchApplicationMessagePublisherId;
            string url = DispatchableLauncherMessage.RequestBuilder.BuildRegisterPulisherUrl(typeof(DispatchableLauncherMessage), ref publisherId);
            using (WebClient webClient = new WebClient())
            {
                webClient.DownloadString(url);
            }

            messagePublisher = machineWideCommunicator.GetPublisher<LauncherMessage>();
            machineWideCommunicator.GetSubscriber<ReceiverReadyMessage>().GetObservable().Subscribe(m_ =>
            {
                var copy = ApplicationIPCReady;
                if (copy != null)
                {
                    copy(null, new ApplicationIPCReadyEventArgs(m_));
                }
            });
            machineWideCommunicator.GetSubscriber<MessageReceivedMessage>().GetObservable().Subscribe(ApplicationMessageReached);
            reporter = new StatusReporter(machineWideCommunicator, crossMachineCommunicator);
        }

        #endregion

        #region HttpServerModule message subscriber 

        //use kraken to dispatch message to launchers by broadcast or by specifying target (if route persisted)
        private void RouteMessage(DispatchableLauncherMessage message_)
        {
            try
            {
                LogLayer.Debug.Location(CLASS_NAME, "RouteMessage").Append(message_.Sequence + ": Message Received").SendQuietly();
                if (message_.HideProgress)
                {
                    reporter.SuppressReport(message_.Sequence);
                }

                ViewInfo viewInfo = GetViewInfo(message_);
                if (viewInfo == null)
                {
                    return;
                }

                reporter.ReportProgress("Received HTTP request", 5, message_.Sequence);

                if (!message_.AlwaysNewApp)
                {
                    var target = routeManager.GetRoute(message_.AppKey);
                    if (target != null)
                    {
                        var talkToAppMessage = new TalkToApplicationMessageBypassRouting(message_, viewInfo, target);
                        if (DispatchMessageDirectly(talkToAppMessage))
                        {
                            LogLayer.Debug.Location(CLASS_NAME, "RouteMessage").Append(message_.Sequence + ": DispatchMessageDirectly succeeded").SendQuietly();
                            return;
                        }
                    }

                }

                var talkToAppMessage2 = new TalkToApplicationMessage(message_, viewInfo);
                talkToApplicationPulisher.Publish(talkToAppMessage2); 

            }
            catch (Exception ex)
            { 
                 LogLayer.Error.Location(CLASS_NAME, "RouteMessage").AppendException(ex).Send();
            }
        }
         
        #endregion

        #region Message Routing 
        private EndpointSelection ResolveLauncherTarget(EndpointSelectorChain chain_)
        {
            var talkMessage = chain_.Message as TalkToApplicationMessage;
            if (talkMessage == null)
            {
                DispatchableLauncherMessageResponse appMessageResponse = chain_.Message as DispatchableLauncherMessageResponse;
                if (appMessageResponse == null) return chain_.Next();
                return new EndpointSelection() { Endpoint = new Endpoint() { Application = Endpoint.LocalEndpoint.Application, Username = Endpoint.LocalEndpoint.Username, Hostname = appMessageResponse.TargetHostName }, Persist = false };
            }

            Endpoint target = null;

            reporter.ReportProgress(string.Format("Probing application '{0}' on all machines with launcher running", talkMessage.ApplicationName), 20, talkMessage.Sequence);

            //broadcast it to all launchers and check ack for valid targets
            var ackTargets = talkMessage.AlwaysNewApp ? new List<Endpoint>() : GetAvailableLaunchers(talkMessage);

            //no valid target and doesn't allow start new application, fail and return
            if (ackTargets.Count > 0 || talkMessage.CanNewApp || talkMessage.AlwaysNewApp)
            {
                bool persistRoute;
                string errorMessage = null;
                if (ackTargets.Count == 0)
                {
                    target = ChooseLauncherTarget(ackTargets, talkMessage, out persistRoute, out errorMessage);
                }
                else if (ackTargets.Count > 1)
                {
                    target = ChooseLauncherTarget(ackTargets, talkMessage, out persistRoute, out errorMessage);
                }
                else
                {
                    target = ackTargets[0];
                    reporter.ReportProgress(string.Format("Found application '{0}' on '{1}'", talkMessage.ApplicationName, target.Hostname), 70, talkMessage.Sequence);
                    persistRoute = talkMessage.CanSaveRoute;
                }
                if (!string.IsNullOrEmpty(errorMessage))
                {
                    LogLayer.Info.Location(CLASS_NAME, "ChooseLauncherTarget").Append(errorMessage).Send();
                    reporter.ReportStatus(talkMessage, errorMessage, false);
                    return null;
                }
                if (persistRoute)
                {
                    reporter.ReportProgress(string.Format("Saving route to application '{0}' on '{1}' for later usage", talkMessage.ApplicationName, target.Hostname), 75, talkMessage.Sequence);
                    routeManager.AddOrUpdateRoute(talkMessage.AppKey, talkMessage.ApplicationName, target);
                }
            } 

            if (target == null)
            {
                string errorMessage =
                       string.Format("Failed to find any valid application '{0}' to send message to", talkMessage.ApplicationName);

                LogLayer.Info.Location(CLASS_NAME, "ResolveLauncherTarget").Append(errorMessage).Send();
                reporter.ReportStatus(talkMessage, errorMessage, false);  
                return null;
            } 
            return new EndpointSelection() { Endpoint = target, Persist = false };
        }
          

        #endregion

        #region Real Application Message Subscribers 
        //use internal bus to send the message to the application
        private void TalkToApplication(DispatchableLauncherMessage message_)
        {
            try
            {
                if (!message_.ContainsMessage) // no message to send
                {
                    reporter.ReportStatus(message_, string.Format("Application '{0}' on '{1}' started successfully", ((IDispatchableLauncherMessageInternal)message_).ApplicationName, Environment.MachineName), true); 
                    return;
                }

                //cannot do reportprogress here, since report progress is local
                //reporter.ReportProgress(string.Format("Waiting for application '{0}' on '{1}' to start", ((IDispatchableLauncherMessageInternal)message_).ApplicationName, Environment.MachineName), 90, message_.Sequence);
                
                if (message_ is TalkToApplicationMessageBypassRouting && 
                    !Launcher.Controller.IsProcessReadyForIPC(GetViewInfo(message_)))
                {
                    reporter.ReportStatus(message_, string.Format("Failed to dispatch message to application '{0}' on '{1}' ", ((IDispatchableLauncherMessageInternal)message_).ApplicationName, Environment.MachineName), false);
                    return;
                }
                message_.Payload.Sequence = message_.Sequence;
                string instance = null;
                TalkToApplicationMessage talkToApplicationMessage = message_ as TalkToApplicationMessage;
                if (talkToApplicationMessage != null)
                {
                    instance = talkToApplicationMessage.InstanceName;
                }
                if (string.IsNullOrEmpty(instance))
                {
                    instance = Launcher.Controller.GetInstanceName(message_.AppKey);
                }

                if (string.IsNullOrEmpty(instance))
                {
                    messagePublisher.Publish(message_.Payload, CommunicationTargetFilter.Machine,
                                             Environment.MachineName);
                }
                else
                { 
                    messagePublisher.Publish(message_.Payload, CommunicationTargetFilter.MachineInstance,
                                             Environment.MachineName, instance);
                }
                lock (messagesDispatching)
                {
                    messagesDispatching[message_.Sequence] = message_;
                }
            }
            catch (Exception ex)
            {
                reporter.ReportStatus(message_, string.Format("Failed to dispatch message to application '{0}' on '{1}' ", ((IDispatchableLauncherMessageInternal)message_).ApplicationName, Environment.MachineName), false); 
                LogLayer.Error.Location(CLASS_NAME, "TalkToApplication").AppendException(ex).Send();
            }
        }

        private void ApplicationMessageReached(MessageReceivedMessage receivedMessage_)
        {
            lock (messagesDispatching)
            {
                DispatchableLauncherMessage dispatchedMessage;
                if (messagesDispatching.TryGetValue(receivedMessage_.Sequence, out dispatchedMessage))
                {
                    string message = string.Format(receivedMessage_.Succeeded ?
                        "Message sent to application '{0}' on '{1}' successfully" :
                        "Failed to send message to application '{0}' on '{1}'",
                        ((IDispatchableLauncherMessageInternal)dispatchedMessage).ApplicationName, Environment.MachineName);
                    LogLayer.Info.Location(CLASS_NAME, "ApplicationMessageReached").Append(message);
                    reporter.ReportStatus(dispatchedMessage, message, receivedMessage_.Succeeded);
                    messagesDispatching.Remove(receivedMessage_.Sequence);
                }
            }

        }
        #endregion

        #region Helper Methods
        private bool DispatchMessageDirectly(TalkToApplicationMessageBypassRouting talkToAppMessage_)
        {
            reporter.ReportProgress(
                    string.Format("Try using saved route to send message to application '{0}' on '{1}'",
                                  talkToAppMessage_.ApplicationName, talkToAppMessage_.Target.Hostname), 10, talkToAppMessage_.Sequence);

            var di = talkToApplicationBypassRoutingPublisher.Publish(talkToAppMessage_);

            PropertyChangedEventHandler delivered = null;
            AutoResetEvent deliverEvent = new AutoResetEvent(false);
            di.PropertyChanged += delivered = (sender_, args_) =>
            {
                var diLocal = sender_ as DeliveryInfo;
                if (diLocal != null)
                {
                    if (diLocal.Status == DeliveryStatus.Delivered)
                    {
                        di.PropertyChanged -= delivered;
                        deliverEvent.Set();
                    }
                    else if (diLocal.Status == DeliveryStatus.Failed)
                    {
                        di.PropertyChanged -= delivered;
                        deliverEvent.Set();
                    }
                }
            };
            deliverEvent.WaitOne(DeliveryStrategyAttribute.DefaultTimeout + 1000);
            if (di.Status == DeliveryStatus.Delivered) return true; //succeeded in one shot 

            var obosoleteTarget = routeManager.GetRoute(talkToAppMessage_.AppKey);
            if (obosoleteTarget != null)
            {
                if (obosoleteTarget.Equals(talkToAppMessage_.Target))
                {
                    routeManager.RemoveRoute(talkToAppMessage_.AppKey);
                    reporter.ReportProgress(string.Format("Application '{0}' on '{1}' is no longer available, remove the saved route", talkToAppMessage_.ApplicationName, obosoleteTarget.Hostname), 15, talkToAppMessage_.Sequence);
                    return false;
                }
                //if not the same, try again
                return
                    DispatchMessageDirectly(new TalkToApplicationMessageBypassRouting(talkToAppMessage_,
                                                                                      talkToAppMessage_.ViewInfo,
                                                                                      talkToAppMessage_.ApplicationName,
                                                                                      obosoleteTarget));
            }
            return false;
        }

        private List<Endpoint> GetAvailableLaunchers(TalkToApplicationMessage message_)
        {
            lock (targetLaunchers)
            {
                return targetLaunchers.ValidateLaunchers(pingApplicationPulisher,
    endpoint_ => new PingApplicationMessage() { ViewInfo = message_.ViewInfo, ApplicationName = message_.ApplicationName, Target = endpoint_ },
     Consts.ProcessReadyTimeout + Consts.NetworkOverheadTime); 
            }

        }

        private Endpoint ChooseLauncherTarget(List<Endpoint> availableTargets_, TalkToApplicationMessage talkToAppMessage_, out bool routeSaved_, out string errorMessage_)
        {
            routeSaved_ = talkToAppMessage_.CanSaveRoute;
            errorMessage_ = null;
            RouteModel selectedRoute = null;
            bool saveRoute = talkToAppMessage_.CanSaveRoute;
            bool startNewApplication = availableTargets_.Count == 0;
            if (availableTargets_.Count == 0)
            {
                var launchers = targetLaunchers.Launchers;
                if (launchers.Count == 1)
                {
                    selectedRoute = new RouteModel()
                        {
                            Application = launchers[0].Application,
                            Hostname = launchers[0].Hostname,
                            IsOnline = true
                        }; 
                }
                else if (!talkToAppMessage_.ContainsMessage) //this message is for starting application only
                {
                    var candidateLauncher = routeManager.GetRoute(talkToAppMessage_.AppKey);
                    if (candidateLauncher != null && launchers.FirstOrDefault(e_=>e_.Application.Equals(candidateLauncher.Application) && e_.Hostname.Equals(candidateLauncher.Hostname)) != null)
                    {
                        selectedRoute = new RouteModel()
                        {
                            Application = candidateLauncher.Application,
                            Hostname = candidateLauncher.Hostname,
                            IsOnline = true
                        };
                    }
                    
                }
            }
 
            if (selectedRoute == null)
            {
                Application.Current.Dispatcher.Invoke(new Action(() =>
                {
                    RouteFinder routeFinder = null; 
                    if (availableTargets_.Count == 0)
                    {
                        reporter.ReportProgress(string.Format("Choosing launcher to start '{0}'", talkToAppMessage_.ApplicationName), 30, talkToAppMessage_.Sequence);
                        routeFinder = new RouteFinder(new LauncherRouteViewModel(crossMachineCommunicator, launcherScreenCaptureSink, targetLaunchers, talkToAppMessage_.ApplicationName))
                            {
                                Width = 400,
                                SizeToContent = SizeToContent.Height
                            };
                    }
                    else
                    {
                        reporter.ReportProgress(string.Format("Choosing application '{0}' on which machine to dispatch message", talkToAppMessage_.ApplicationName), 30, talkToAppMessage_.Sequence);
                        routeFinder = new RouteFinder(new ApplicationRouteViewModel(crossMachineCommunicator, applicationScreenCaptureSink, availableTargets_, GetAvailableLaunchers, talkToAppMessage_));
                    }
                    routeFinder.RouteViewModel.AllowSaveRoute = talkToAppMessage_.CanSaveRoute;
                    routeFinder.RouteViewModel.SaveRoute = talkToAppMessage_.CanSaveRoute; 
                    routeFinder.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    routeFinder.Topmost = true;
                    if (routeFinder.ShowDialog().GetValueOrDefault())
                    {
                        selectedRoute = routeFinder.RouteViewModel.SelectedRoute;
                        if (talkToAppMessage_.CanSaveRoute)
                        {
                            saveRoute = routeFinder.RouteViewModel.SaveRoute;
                        }
                        if (!startNewApplication)
                        {
                            reporter.ReportProgress(string.Format("Chosen application '{1}' on '{0}' to dispatch message", selectedRoute.Hostname,
    talkToAppMessage_.ApplicationName), 70, talkToAppMessage_.Sequence); 
                        } 
                    }
                })); 
            }
            if (selectedRoute == null)
            { 
                errorMessage_ = string.Format("No launcher chosen to start application '{0}'", talkToAppMessage_.ApplicationName);
                return null;
            }
            routeSaved_ = saveRoute;
            var target = new Endpoint()
            {
                Hostname = selectedRoute.Hostname,
                Application = selectedRoute.Application,
            };
            if (startNewApplication)
            { 
                var startAppMessage = new StartApplicationMessage()
                {
                    ViewInfo = talkToAppMessage_.ViewInfo, 
                    ApplicationName = talkToAppMessage_.ApplicationName,
                    AlwaysNewApp = talkToAppMessage_.AlwaysNewApp,
                    Target = target
                };
                reporter.ReportProgress(string.Format("Starting application '{1}' on '{0}'", target.Hostname, talkToAppMessage_.ApplicationName), 40, talkToAppMessage_.Sequence);

                var di = startApplicationPublisher.Publish(startAppMessage);
                if (!talkToAppMessage_.ContainsMessage) //this message is for starting application only, no need to wait until started
                {
                    return target;
                }
                if (di.Status == DeliveryStatus.Failed)
                {
                    errorMessage_ = string.Format("Failed to start application '{0}' on '{1}'",
                                                   talkToAppMessage_.ApplicationName, target.Hostname); 
                    return null;
                }
                PropertyChangedEventHandler delivered = null;
                AutoResetEvent deliveredEvent = new AutoResetEvent(false);
                di.PropertyChanged += delivered = (sender_, args_) =>
                {
                    var diLocal = sender_ as DeliveryInfo;
                    if (diLocal != null)
                    {
                        if (diLocal.Status == DeliveryStatus.Delivered)
                        {
                            deliveredEvent.Set();
                            di.PropertyChanged -= delivered;
                        }
                        else if (diLocal.Status == DeliveryStatus.Failed)
                        {
                            deliveredEvent.Set();
                            di.PropertyChanged -= delivered;
                        }
                    }
                };

                deliveredEvent.WaitOne(Consts.StartProcessTimeout + Consts.NetworkOverheadTime); 
                if (di.Status == DeliveryStatus.Failed)
                {
                    errorMessage_ = string.Format("Failed to start application '{0}' on '{1}'",
                                                   talkToAppMessage_.ApplicationName, target.Hostname);
                    return null;
                }
                talkToAppMessage_.InstanceName = di.ExtraInformation;
                reporter.ReportProgress(string.Format("Started application '{0}' on '{1}'", talkToAppMessage_.ApplicationName, target.Hostname), 70, talkToAppMessage_.Sequence);
            }
            return target; 
        }
      
        private ViewInfo GetViewInfo(DispatchableLauncherMessage message_)
        {

            if (string.IsNullOrEmpty(message_.AppKey) && 
                string.IsNullOrEmpty(message_.PackagePath) && 
                string.IsNullOrEmpty(message_.AppName))
            {
                ReportError(message_, "No application key or package path or package information specified");
                return null; 
            }
            if (!string.IsNullOrEmpty(message_.AppKey))
            {
                ViewInfo viewInfo = Launcher.Controller.GetViewInfoForKey(message_.AppKey);
                if (viewInfo == null)
                {
                    ReportError(message_, string.Format("Invalid/unprivileged application key '{0}'", message_.AppKey));
                    return null;
                }
                if (!string.IsNullOrEmpty(message_.ExtraArguments))
                {
                    viewInfo.ExtraArguments = message_.ExtraArguments;
                }
                return viewInfo;
            } 

            string package, version;
            Model.Application app = null;
            if (!string.IsNullOrEmpty(message_.PackagePath))
            { 
                string path = null;
                if (!message_.PackagePath.ToLower().StartsWith(@"\\san01b\DevAppsGML\dist\") || 
                    !File.Exists(message_.PackagePath) || 
                    !message_.PackagePath.ToLower().EndsWith(".zip"))
                {
                    ReportError(message_, "Invalid package path:" + message_.PackagePath + Environment.NewLine +
                          @"Package must exist, must be located on \\ms\dist and must be a .zip file");
                    return null;
                }

                path = message_.PackagePath.Substring(10);
                string[] splitPath = path.Split(new []{'\\'}, StringSplitOptions.RemoveEmptyEntries);
                if (splitPath.Length != 5)
                {
                    ReportError(message_, "Invalid package path:" + message_.PackagePath + Environment.NewLine +
      @"Package must exist, must be located on \\ms\dist and must be a .zip file");
                }
                string meta = splitPath[splitPath.Length - 5];
                string project = splitPath[splitPath.Length - 3];
                app =
                    Launcher.Controller.CopyLocalReleases.Keys.FirstOrDefault(
                        key_ => key_.Meta == meta && key_.Project == project);
                if (app == null)
                {
                    ReportError(message_, "Invalid package path:" + message_.PackagePath + Environment.NewLine +
@"Package doesn't belong to a valid application");
                    return null;
                }
                version = splitPath[splitPath.Length - 2];
                package = splitPath[splitPath.Length - 1];  
            }
            else
            {
                if (string.IsNullOrEmpty(message_.Version))
                {
                    ReportError(message_, "Version is missing to run package");
                    return null;
                }
                if (string.IsNullOrEmpty(message_.Package))
                {
                    ReportError(message_, "Package is missing to run package");
                    return null;
                }

                 app =
    Launcher.Controller.CopyLocalReleases.Keys.FirstOrDefault(
        key_ =>  key_.Description == message_.AppName);
                if (app == null)
                {
                    ReportError(message_, string.Format("Invalid/unprivileged application name '{0}' to run package", app));
                    return null;
                } 
                package = message_.Package;
                version = message_.Version; 
            }

            var executable = string.IsNullOrEmpty(message_.Executable) ?
                Launcher.Controller.CopyLocalReleases[app][0] : message_.Executable;
            var activeRelease = new ActiveRelease(version, executable, "COPYLOCAL", null, null, package);
            var env = new Model.Environment() {Arguments = message_.ExtraArguments};
            var viewInfo2 = new ViewInfo(app, null, env, activeRelease, false, null, null);
            message_.AppKey = viewInfo2.UniqueKeyCalculated; 
            return viewInfo2;
        }

        private void ReportError(DispatchableLauncherMessage message_, string errorMessage_)
        { 
            LogLayer.Error.Location(CLASS_NAME, "RouteMessage").Append(errorMessage_).Send();
            reporter.ReportStatus(message_, errorMessage_, false); 
        }
        #endregion
    }
}
