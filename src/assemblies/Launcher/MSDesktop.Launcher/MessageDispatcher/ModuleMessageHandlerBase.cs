﻿using System; 
using System.Net;  
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Unity;  
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using System.Reactive.Linq;
using MorganStanley.MSDotNet.MSGui.Core.Persistence.DelayedLoading;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication;

namespace MSDesktop.Launcher.MessageDispatcher
{
    public abstract class ModuleMessageHandlerBase : IModule
    {
        protected readonly ICommunicator machineWideCommunicator;
         
        protected ModuleMessageHandlerBase(ICommunicator machineWideCommunicator_)
        {
            machineWideCommunicator = machineWideCommunicator_;   
            DelayedProfileLoadingExtensionPoints.AllModulesLoaded += WebSupportExtensionPoints_AllModulesLoaded;

        }

        #region Initialization
        public virtual void Initialize()
        { 
        }
         
        void WebSupportExtensionPoints_AllModulesLoaded(object sender, AllModulesLoadedEventArgs e)
        {
            machineWideCommunicator.GetSubscriber<DispatchableLauncherModuleMessage>().GetObservable().Where(AcceptMessage).Subscribe(HandleMessage);
 
            string publisherId = LauncherConsts.DispatchModuleMessagePublisherId;
            string url = DispatchableLauncherMessage.RequestBuilder.BuildRegisterPulisherUrl(typeof(DispatchableLauncherModuleMessage), ref publisherId);
            using (WebClient webClient = new WebClient())
            {
                webClient.DownloadString(url);
            }
            OnLoaded();
        } 

        #endregion

        protected virtual void OnLoaded(){}
        protected abstract bool AcceptMessage(DispatchableLauncherModuleMessage message_);
        protected abstract void HandleMessage(DispatchableLauncherModuleMessage message_);

    }
}
