﻿using System;
using System.Collections.Generic;
using System.Linq;
using MSDesktop.MessageRouting.Routing;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MSDesktop.Launcher.MessageDispatcher.Persistence
{
    public class LauncherRouteManager
    { 
        private readonly LauncherConfigStore configStore;
        public LauncherRouteManager(IApplicationOptions options_, IPersistenceService persistenceService_)
        {
            configStore = new LauncherConfigStore(persistenceService_); 
            options_.AddOptionPage("Launcher Route", "Stored Routes", new SavedRoutesView(this));
        }

        public IEnumerable<ApplicationRoute> Routes
        {
            get { return configStore.Value == null ? new List<ApplicationRoute>() : configStore.Value.Routes; }
        }


        public void RemoveRoute(string applicationKey_)
        {
            lock (configStore)
            {
                if (configStore.Value != null && configStore.Value.Routes != null)
                {
                    var route =
                        configStore.Value.Routes.FirstOrDefault(
                            a => a.ApplicationKey.Equals(applicationKey_));
                    if (route != null)
                    {
                        configStore.Value.Routes.Remove(route);
                    }
                }
            }

        }

        public void RemoveRoute(ApplicationRoute route_)
        {
            lock (configStore)
            {
                if (configStore.Value != null && configStore.Value.Routes != null)
                {
                    configStore.Value.Routes.Remove(route_);
                }  
            }

        }

        public void AddOrUpdateRoute(string applicationKey_, string applicationName_, Endpoint target_)
        {
            lock (configStore)
            {
                RemoveRoute(applicationKey_);
                var route = new ApplicationRoute
                { 
                    ApplicationKey = applicationKey_,
                    ApplicationName = applicationName_, 
                    Target = target_,
                    CreatedAt = DateTime.Now
                };

                configStore.Value.Routes.Add(route); 
            }
           
        }

        public Endpoint GetRoute(string applicationKey_)
        {
            lock (configStore)
            {
                if (configStore.Value != null && configStore.Value.Routes != null)
                {
                    var route = configStore.Value.Routes.FirstOrDefault(
                            a => a.ApplicationKey.Equals(applicationKey_));
                    if (route != null) return route.Target;
                }
            }

            return null;
        }
    }
}
