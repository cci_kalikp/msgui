﻿using System;
using System.Xml.Serialization;
using MSDesktop.MessageRouting.Routing;

namespace MSDesktop.Launcher.MessageDispatcher.Persistence
{
    [XmlRoot("Route")] 
    public class ApplicationRoute
    {
        [XmlElement("ApplicationKey")]
        public string ApplicationKey { get; set; }
        [XmlElement("ApplicationName")]
        public string ApplicationName { get; set; }
        [XmlElement("Target")]
        public Endpoint Target { get; set; }

        [XmlElement("CreatedAt")]
        public DateTime? CreatedAt { get; set; }

        public override string ToString()
        {
            return string.Format("Route to {0} for Application '{1}'", Target.Hostname, ApplicationName);
        }
    }
}
