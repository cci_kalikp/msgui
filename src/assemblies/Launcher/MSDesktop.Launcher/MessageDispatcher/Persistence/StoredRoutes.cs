﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace MSDesktop.Launcher.MessageDispatcher.Persistence
{
    [XmlRoot("LauncherConfigStoreValue")]
    public class LauncherConfigStoreValue
    {
        [XmlArray("Routes")]
        [XmlArrayItem("Route")]
        public List<ApplicationRoute> Routes { get; set; }

        public LauncherConfigStoreValue()
        {
            this.Routes = new List<ApplicationRoute>();
        }
    }
}
