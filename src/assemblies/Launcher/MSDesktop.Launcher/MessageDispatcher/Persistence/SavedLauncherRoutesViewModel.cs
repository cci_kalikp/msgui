﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Controls;
using System.Collections.ObjectModel;
using System.Windows.Input; 
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;

namespace MSDesktop.Launcher.MessageDispatcher.Persistence
{
    public class SavedLauncherRoutesViewModel : ViewModelBase
    {
        private readonly LauncherRouteManager routeManager;

        public ObservableCollection<ApplicationRoute> Routes { get; private set; }

        private List<ApplicationRoute> selectedRoutes;

        public List<ApplicationRoute> SelectedRoutes
        {
            get { return selectedRoutes; }
            set
            {
                selectedRoutes = value;
                OnPropertyChanged("SelectedRoutes");
            }
        }

        public void RouteSelectionChanged(DataGrid dgv)
        {
            if (dgv.SelectedItems != null)
            {
                SelectedRoutes = ((IList<object>)dgv.SelectedItems).Select(obj => obj as ApplicationRoute).Where(a => a != null).ToList();
            }
            else
            {
                SelectedRoutes = new List<ApplicationRoute>();
            }
        }
        #region Commands 

        public ICommand DeleteHostsCommand { get; private set; }

        public ICommand DeleteAllHostsCommand { get; private set; }

        #endregion

        public SavedLauncherRoutesViewModel(LauncherRouteManager routeManager_)
        {
            this.routeManager = routeManager_;
             
            this.DeleteHostsCommand = new DelegateCommand(DeleteHosts);
            this.DeleteAllHostsCommand = new DelegateCommand(DeleteAllHosts); 
            
            this.Routes = new ObservableCollection<ApplicationRoute>(routeManager.Routes);
        }


        readonly List<ApplicationRoute> routesDeleted = new List<ApplicationRoute>(); 
        public void DeleteHosts(object parameter)
        {

            if (TaskDialog.ShowMessage(
                "Are you sure you want to remove the selected routes?",
                "Remove selected routes",
                TaskDialogCommonButtons.YesNo, VistaTaskDialogIcon.Warning) == TaskDialogSimpleResult.Yes)
            {
                foreach (var item in this.SelectedRoutes)
                {
                    this.Routes.Remove(item);
                    routesDeleted.Add(item);
                }
            }
        }

        private void DeleteAllHosts(object parameter)
        {

            if (TaskDialog.ShowMessage(
                "Are you sure you want to remove ALL routes?",
                "Remove ALL routes",
                TaskDialogCommonButtons.YesNo, VistaTaskDialogIcon.Warning) == TaskDialogSimpleResult.Yes)
            {
                routesDeleted.AddRange(Routes);
                this.Routes.Clear();

            }
        }

        public void Save()
        { 
            foreach (var applicationRoute in routesDeleted)
            {
                routeManager.RemoveRoute(applicationRoute);
            } 
        }

 
    }
}
