﻿using System;
using System.Xml.Linq;
using System.Xml.Serialization; 
using MorganStanley.MSDotNet.MSGui.Core;

namespace MSDesktop.Launcher.MessageDispatcher.Persistence
{
    class LauncherConfigStore
    {
      private const string myPersistorName = "Launcher_Config_CB282A68_D462_4746_9C4E_DC964D984E6F";

        protected virtual string PersistorName
        {
            get
            {
                return String.Format("{0}_{1}", myPersistorName, Environment.MachineName);
            }
        }

        private readonly IPersistenceService persistenceService;

        public LauncherConfigStore(IPersistenceService persistenceService)
        {
            this.persistenceService = persistenceService;
            this.persistenceService.AddGlobalPersistor(PersistorName, RestoreCustomState, SaveCustomState);
            this.Value = new LauncherConfigStoreValue();
        }

        public LauncherConfigStoreValue Value { get; set; }

        private XDocument SaveCustomState()
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(LauncherConfigStoreValue));

            XDocument doc = new XDocument();
            using (var writer = doc.CreateWriter())
            {
                xmlSerializer.Serialize(writer, this.Value);
            }

            return doc;
        }

        private void RestoreCustomState(XDocument state)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(LauncherConfigStoreValue));

            using (var reader = state.Root.CreateReader())
            {
                this.Value = (LauncherConfigStoreValue)xmlSerializer.Deserialize(reader);
            }

        }
    }
}
