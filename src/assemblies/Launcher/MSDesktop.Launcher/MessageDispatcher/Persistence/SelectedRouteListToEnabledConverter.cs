﻿using System;
using System.Collections.Generic;
using System.Windows.Data;
using System.Globalization;

namespace MSDesktop.Launcher.MessageDispatcher.Persistence
{
    public class SelectedRouteListToEnabledConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var routes = value as IList<ApplicationRoute>;

            if (routes == null)
                return false;

            return routes.Count > 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
