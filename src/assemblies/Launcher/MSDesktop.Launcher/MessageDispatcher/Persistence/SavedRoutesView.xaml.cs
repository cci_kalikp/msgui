﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using MSDesktop.Launcher.MessageDispatcher.Helpers;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Options;

namespace MSDesktop.Launcher.MessageDispatcher.Persistence
{
    public partial class SavedRoutesView : UserControl, MorganStanley.MSDotNet.MSGui.Core.IOptionView, ISubItemAwareSeachablePage
    {
        public SavedLauncherRoutesViewModel SavedLauncherRoutesViewModel { get; private set; }

        private readonly LauncherRouteManager routeManager;

        public SavedRoutesView(LauncherRouteManager routeManager_)
        {
            InitializeComponent();
            this.routeManager = routeManager_; 
        }

        public new UIElement Content
        {
            get 
            { 
                return this; 
            }
        }

        public void OnCancel()
        {
            // TODO
        }

        public void OnDisplay()
        {
            this.SavedLauncherRoutesViewModel = new SavedLauncherRoutesViewModel(routeManager);
            this.DataContext = SavedLauncherRoutesViewModel;
        }

        public void OnHelp()
        {
             
        } 

        public void OnOK()
        {
            SavedLauncherRoutesViewModel.Save();
        } 

        public IList<KeyValuePair<string, object>> GetMatchedItems(string textToSearch_)
        {
            if (SavedLauncherRoutesViewModel == null) this.SavedLauncherRoutesViewModel = new SavedLauncherRoutesViewModel(routeManager);
            var matchedRoutes = new List<KeyValuePair<string, object>>();
            foreach (var route in SavedLauncherRoutesViewModel.Routes)
            {

                if (route.ApplicationName.ToLower().Contains(textToSearch_.ToLower()) ||
                    route.Target.Hostname.Contains(textToSearch_.ToLower()))
                {
                    matchedRoutes.Add(new KeyValuePair<string, object>(route.ToString(), route));
                }
            } 

            return matchedRoutes;
        }

        public void LocateItem(object item_)
        {
            var route = item_ as ApplicationRoute;
            var item =
                   SavedLauncherRoutesViewModel.Routes.FirstOrDefault(r_ => r_.ApplicationKey == route.ApplicationKey);
            if (item == null) return;
             Datagrid.SelectedItem = item;
            var row = Datagrid.ItemContainerGenerator.ContainerFromItem(item) as DataGridRow;
            if (row != null)
            {
                row.Highlight();
            }
        }

        private void Datagrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SavedLauncherRoutesViewModel.RouteSelectionChanged(Datagrid);
        }

    }
}
