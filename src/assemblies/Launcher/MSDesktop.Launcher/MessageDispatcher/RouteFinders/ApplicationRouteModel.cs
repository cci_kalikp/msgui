﻿using MSDesktop.MessageRouting.GUI.Model;

namespace MSDesktop.Launcher.MessageDispatcher.RouteFinders
{
    class ApplicationRouteModel:RouteModel
    {
        public string ApplicationKey { get; set; }
    }
}
