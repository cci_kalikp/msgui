﻿using System;
using MSDesktop.MessageRouting.GUI.Message;

namespace MSDesktop.Launcher.MessageDispatcher.RouteFinders
{
    [Serializable]
    public class ApplicationScreenCaptureRequestMessage:ScreenCaptureRequestMessage
    {
        public string ApplicationKey { get; set; }
         
    }
}
