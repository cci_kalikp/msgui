using MSDesktop.Launcher.MessageDispatcher.Helpers;
using MSDesktop.MessageRouting;
using MSDesktop.MessageRouting.GUI.ViewModel;

namespace MSDesktop.Launcher.MessageDispatcher.RouteFinders
{
    class LauncherRouteViewModel:RouteViewModel
    {
        private readonly LauncherList targetLaunchers;
        public LauncherRouteViewModel(IRoutingCommunicator routingCommunicator_,  LauncherScreenCaptureSink screenCaptureSink_, LauncherList targetLaunchers_, string applicationName_)
            : base(null, routingCommunicator_, null, screenCaptureSink_, null, null)
        {
            MessageInfo = "Choose launcher to start " + applicationName_;
            this.targetLaunchers = targetLaunchers_;
            this.ScreenCaptureErrorText = "Launcher is hidden.";

        }

        protected override System.Collections.ObjectModel.ObservableCollection<MessageRouting.Routing.Endpoint> ListSubscribedApplications()
        {
            targetLaunchers.RefreshLaunchers();
            return targetLaunchers.LaunchersReading;
        }

        protected override System.Collections.ObjectModel.ObservableCollection<MessageRouting.Routing.Endpoint> RefreshSubscribedApplications()
        {
            targetLaunchers.RefreshLaunchers();
            return targetLaunchers.LaunchersReading;
        }
         
    }
}
