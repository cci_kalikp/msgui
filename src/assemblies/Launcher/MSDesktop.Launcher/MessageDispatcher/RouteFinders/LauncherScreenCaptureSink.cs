﻿using System.Windows.Interop;
using MSDesktop.MessageRouting;
using MSDesktop.MessageRouting.GUI;
using MSDesktop.MessageRouting.GUI.Message;
using MSDesktop.MessageRouting.GUI.Util;

namespace MSDesktop.Launcher.MessageDispatcher.RouteFinders
{
    class LauncherScreenCaptureSink : ScreenCaptureSinkImpl
    {
        public LauncherScreenCaptureSink(IRoutingCommunicator communicator_):base(communicator_)
        {
            
        }

        protected override ScreenCaptureResponseMessage RepondScreenCaptureRequest(ScreenCaptureRequestMessage request_)
        {
             
            try
            {
                var window = Launcher.MainWindow;
                if (window != null && window.IsVisible)
                {
                    return
                        ScreenCapturer.CaptureWindow(
                            new WindowInteropHelper(window).Handle);
                }

            }
            catch  
            { 
                return new ScreenCaptureResponseMessage();
            }
            return new ScreenCaptureResponseMessage();
        }
    }
}
