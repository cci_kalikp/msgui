﻿using MSDesktop.MessageRouting;
using MSDesktop.MessageRouting.GUI;
using MSDesktop.MessageRouting.GUI.Message;
using MSDesktop.MessageRouting.GUI.Model;
using MSDesktop.MessageRouting.GUI.Util;
using MSDesktop.MessageRouting.Routing;

namespace MSDesktop.Launcher.MessageDispatcher.RouteFinders
{
    class ApplicationScreenCaptureSink : ScreenCaptureSinkBase<ApplicationScreenCaptureRequestMessage>
    {
        public ApplicationScreenCaptureSink(IRoutingCommunicator communicator_)
            : base(communicator_)
        {
            
        }

        protected override ScreenCaptureResponseMessage RepondScreenCaptureRequest(ApplicationScreenCaptureRequestMessage request_)
        {
            var process = Launcher.Controller.GetRunningInstance(request_.ApplicationKey);
            if (process == null)
            {
                return new ScreenCaptureResponseMessage();
            }
            try
            {
                return ScreenCapturer.CaptureWindow(process); 
            } 
            catch  
            { 
                return new ScreenCaptureResponseMessage();
            } 
        }

        protected override ApplicationScreenCaptureRequestMessage ConvertToRequest(RouteModel target_)
        {
            ApplicationRouteModel model = target_ as ApplicationRouteModel;
           return new ApplicationScreenCaptureRequestMessage()
                {
                    ApplicationKey = model.ApplicationKey,
                    Origin = Endpoint.LocalEndpoint,
                    Target = new Endpoint() { Application = model.Application, Hostname = model.Hostname }
                };

        }
    }
}
