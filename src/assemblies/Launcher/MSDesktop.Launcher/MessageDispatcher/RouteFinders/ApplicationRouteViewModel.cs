﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel; 
using System.Windows;
using MSDesktop.Launcher.MessageDispatcher.Messages;
using MSDesktop.MessageRouting;
using MSDesktop.MessageRouting.GUI.ViewModel;
using MSDesktop.MessageRouting.Routing;

namespace MSDesktop.Launcher.MessageDispatcher.RouteFinders
{
    class ApplicationRouteViewModel:RouteViewModel
    { 
        private readonly List<Endpoint> availableApplications;
        private readonly TalkToApplicationMessage message;
        private ObservableCollection<Endpoint> applications = new ObservableCollection<Endpoint>(); 
        private readonly Func<TalkToApplicationMessage, List<Endpoint>> applicationRefresher; 
        public ApplicationRouteViewModel(IRoutingCommunicator routingCommunicator_, ApplicationScreenCaptureSink screenCaptureSink_,
            List<Endpoint> availableApplications_, Func<TalkToApplicationMessage, List<Endpoint>> applicationRefresher_, TalkToApplicationMessage message_)
            : base(null, routingCommunicator_, null, screenCaptureSink_, null, null)
        {
            MessageInfo = MessageInfo = "Choose application";
            this.availableApplications = availableApplications_;
            this.applicationRefresher = applicationRefresher_;
            message = message_;
             

        }

        protected override ObservableCollection<Endpoint> ListSubscribedApplications()
        {
            var result = new ObservableCollection<Endpoint>(availableApplications);
            IsRoutesLoading = false;
            return result;
        }

        protected override ObservableCollection<Endpoint> RefreshSubscribedApplications()
        {
            IsRoutesLoading = true; 
            applications = new ObservableCollection<Endpoint>();
            System.Threading.Tasks.Task.Factory.StartNew(
                new Action(
                    () =>
                        {
                            var result = applicationRefresher(message);
                            if (Application.Current != null)
                            {
                                Application.Current.Dispatcher.Invoke(
                                    new Action(() => result.ForEach(e_ => applications.Add(e_))));
                            }
                        }
                         ));
            return applications;
        }

        protected override MessageRouting.GUI.Model.RouteModel ConvertToRouteModel(Endpoint target_)
        {
            return new ApplicationRouteModel()
                {
                    Application = target_.Application,
                    Hostname = target_.Hostname,
                    ApplicationKey = message.AppKey,
                    DisplayName = message.ApplicationName,
                    IsOnline = true
                };
        }
         
    }
}
