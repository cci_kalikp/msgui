﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows; 
using MSDesktop.Launcher.UI;
using MorganStanley.IED.Concord.Configuration;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication;
using MorganStanley.MSDotNet.MSLog;
using MorganStanley.MSDotNet.My;

namespace MSDesktop.Launcher
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private readonly Guid _appGuid = new Guid("{2BEB6257-F76A-492C-AE8F-5CC5A271AA85}");
         
        //static App()
        //{
        //  MorganStanley.MSDotNet.Runtime.AssemblyResolver.Load();
        //}
        
        
        protected override void OnStartup(StartupEventArgs e)
        {
            //Environment.SetEnvironmentVariable(PathUtilities.ExampleRootVariable, @"C:\Users\caijin\shadow\ada\ada2\msdotnet\msgui\trunk\examples");
            //Environment.SetEnvironmentVariable(PathUtilities.ExeRelativeVariable, @"\bin\Debug");
            string[] args = e.Args;
            string launcherName;
            bool allowSingleMachine;
            bool show;
            PreParseArgs(ref args, out launcherName, out allowSingleMachine, out show);
            string appFolderBase = "Launcher";
            if (!string.IsNullOrEmpty(launcherName))
            {
                appFolderBase += @"\" + MakeValidFileName(launcherName);
            }
            Environment.SetEnvironmentVariable("AppFolderBase", appFolderBase);

            string appFolder =
                Path.Combine(
                    Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Morgan Stanley"),
                    appFolderBase);
            Environment.SetEnvironmentVariable("AppFolder", appFolder);
            Environment.SetEnvironmentVariable("LogFolder", Path.Combine(appFolder, "Log")); 
            Environment.SetEnvironmentVariable("AllowSingleMachine", allowSingleMachine.ToString());
            SingleInstance si = new SingleInstance(_appGuid, launcherName);
            si.ArgsRecieved += new SingleInstance.ArgsHandler(si_ArgsRecieved);
            si.Run(() =>
                {

                    MSLoggerFactory.Provider = new MSLog4NetLoggerProvider();
                    MorganStanley.MSDotNet.MSLog.MSLog.Redirectlog4netDestination();

                    LogLayer.Info.Location("App", "Info").Append( 
                        "********************************************** STARTUP ******************************************").
                        Append("AFS Cell: " + System.Environment.GetEnvironmentVariable("AFS_ThisCell")).
                        Append("SYS Loc: " + Launcher.SystemLocation).Send();

                    Launcher.LauncherName = string.IsNullOrEmpty(launcherName) ? LauncherConsts.DefaultApplicationName : launcherName;
                    Launcher.StartLauncher(show, args);

                    return this.MainWindow;
                }, args);

        }
        private static string MakeValidFileName(string name_)
        {
            string invalidChars = Regex.Escape(new string(Path.GetInvalidFileNameChars()));
            string invalidReStr = string.Format(@"([{0}]|\.\.)", invalidChars);
            return Regex.Replace(name_, invalidReStr, "_");
        }
        private void PreParseArgs(ref string[] args_, out string launcherName_, out bool allowSingleMachine_, out bool show_)
        {
            launcherName_ = null;
            allowSingleMachine_ = true;
            show_ = false;
            List<string> newArgs = new List<string>(); 
            for (int i = 0; i < args_.Length; i++)
            {
                if (launcherName_ == null)
                {
                    if (System.String.Compare(args_[i], "-name", System.StringComparison.OrdinalIgnoreCase) == 0 ||
                        System.String.Compare(args_[i], "/name", System.StringComparison.OrdinalIgnoreCase) == 0 ||
                        System.String.Compare(args_[i], "-n", System.StringComparison.OrdinalIgnoreCase) == 0 ||
                        System.String.Compare(args_[i], "/n", System.StringComparison.OrdinalIgnoreCase) == 0)
                    {
                        if (i + 1 < args_.Length)
                        { 
                            launcherName_ = args_[i + 1];
                            i++;
                            allowSingleMachine_ = false;
                            continue; 
                        }
                    }
                    
                } 
                if (System.String.Compare(args_[i], "-f", System.StringComparison.OrdinalIgnoreCase) == 0 ||
                         System.String.Compare(args_[i], "/f", System.StringComparison.OrdinalIgnoreCase) == 0 ||
                         System.String.Compare(args_[i], "-force", System.StringComparison.OrdinalIgnoreCase) == 0 ||
                         System.String.Compare(args_[i], "/force", System.StringComparison.OrdinalIgnoreCase) == 0)
                {  
                    allowSingleMachine_ = false;
                    continue;
                }
                if (System.String.Compare(args_[i], "-s", System.StringComparison.OrdinalIgnoreCase) == 0 ||
                         System.String.Compare(args_[i], "/s", System.StringComparison.OrdinalIgnoreCase) == 0 ||
                         System.String.Compare(args_[i], "-show", System.StringComparison.OrdinalIgnoreCase) == 0 ||
                         System.String.Compare(args_[i], "/show", System.StringComparison.OrdinalIgnoreCase) == 0)
                {
                    show_ = true;
                    continue; 
                }
                if (args_[i].StartsWith("-env:", StringComparison.InvariantCultureIgnoreCase))
                {
                    continue;
                }
                newArgs.Add(args_[i]);
            }
            args_ = newArgs.ToArray(); 
        }
        private void si_ArgsRecieved(string[] args)
        {
            if (args.Length > 0 && args[0] == "startApp")
            {
                string key = string.Empty;
                foreach (string arg in args)
                {
                    if (arg == "startApp")
                    {
                        continue;
                    }
                    else
                    {
                        key += arg + " ";
                    }
                }
                Launcher.Controller.StartProcess(key.Trim());
            }
            else
            {
                Launcher.ShowMainWindow();
            }
        }
    }
}
