#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2006 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

              $Id: //eai/msdotnet/msgui/trunk/assemblies/Launcher/MSDesktop.Launcher/Services/Configuration.cs#12 $
  $DateTime: 2014/10/17 10:01:14 $
    $Change: 901461 $
    $Author: caijin $
_____________________________________________________________________________*/
#endregion File Info Header

using System; 
using System.Xml.Serialization; 
using MorganStanley.MSDotNet.MSGui.Core; 

using System.IO;
using System.Xml;

using MorganStanley.MSDotNet.MSXml;

namespace MSDesktop.Launcher.Services
{
  using System.Collections.Generic;
  using Model;
  using Environment = System.Environment;

  /// <summary>
  /// Reads in the application configuration
  /// </summary>
  public class Configuration
  {
    #region Constants
    private const string CLASS_NAME = "Configuration";
    //Default the environment variable entries 
    #endregion

    #region Private Member Variables

    private SysLoc _systemSysLoc; 

    //The complete root configuration filename
    private string m_rootConfigFilename;

    //The current application configuration
    private Model.Root m_root;

      private Model.LauncherSettings m_settings;
       
    private bool _usingBackup = false;

    #endregion

    #region Constructors
    /// <summary>
    /// The main constructor
    /// </summary>
    public Configuration(string rootConfigFilename_)
    {
      _systemSysLoc = SysLoc.MySysLoc;

      m_rootConfigFilename = rootConfigFilename_;
    }

    #endregion

    #region Public Properties
    /// <summary>
    /// Gets the root application configuration object
    /// </summary>
    public Model.Root Root
    {
      get
      {
        return m_root;
      }
    }

      public LauncherSettings Settings
      {
          get { return m_settings; }
      }
    /// <summary>
    /// Returns the workstation's system location
    /// </summary>
    public SysLoc SysLoc
    {
      get
      {
        return _systemSysLoc;
      }
    }

    public bool UsingBackup
    {
      get
      {
        return _usingBackup;
      }
    }
    #endregion

    #region Public Methods

    #region Application Config

    /// <summary>
    /// Reads an application configuration file.
    /// </summary>
    /// <param name="filename_">The full filename of the application configuration input file.</param>
    /// <returns>An application configuration object</returns>
    public static Model.Application ReadApplicationConfig(string filename_)
    {
      string[] pathSplit = filename_.Split(':');
      string path = pathSplit[1];     // Strip off the file: from the front of file://ms/dist/...
      if (pathSplit.Length > 2)
      {
        // possible local testing e.g. file://C:/dev/testing...
        // so reconstruct second part
        path = pathSplit[1].Substring(2)  // need to strip off leading // in front of C
          + ":" + pathSplit[2];
      }

      FileInfo file = new FileInfo(path);
      if (!file.Exists)
      {
        // if file is not disted to this cell, it will exist but will have size of 1KB so check for that
        LogLayer.Info.Location(CLASS_NAME, "ReadApplicationConfig").Append(string.Format("Cannot load app config - path not visible from this cell: {0}", path)).Send();
        return null;
      }

      System.Xml.XmlTextReader reader = new System.Xml.XmlTextReader(path);
      object obj = DeserializeObjectFromReader(reader, typeof(Model.Application), path);
      Model.Application app = obj as Model.Application;
      return app;
    }

      /// <summary>
      /// Reads a root configuration file.
      /// [NOTE: Uses MSXmlProcessingReader to allow XIncludes for the application configuration files]
      /// </summary>
      public void ReadRootConfig(List<string> groupNames_)
      {
          //HACK - MSXMLProcessingReader does not work with XMLSerializer so we first 
          //need to load into a string via a Xml Document and then Deserialize using a standard XmlTextReader		
          string xml = ReadRootConfigIntoString(groupNames_);
          object obj = DeserializeObjectFromString(xml, typeof (Model.Root));

          m_root = obj as Model.Root;
          if (m_root != null && (m_settings == null || m_settings.IsDefault))
          {
              m_settings = m_root.LauncherSettings;
          }
      }

      public void ReadLauncherConfig()
      {
          if (!File.Exists(m_rootConfigFilename))
          {
              LogLayer.Error.Location(CLASS_NAME, "ReadLauncherConfig").Append(string.Format("Cannot access {0}", m_rootConfigFilename)).Send();
          }
          else
          {
              XmlDocument doc = new XmlDocument();
              doc.Load(m_rootConfigFilename);
              var node = doc.SelectSingleNode("//Applications");
              if (node != null)
              {
                  node.ParentNode.RemoveChild(node);
              }
              var root = DeserializeObjectFromString(doc.OuterXml, typeof(Model.Root)) as Root;
              if (root != null)
              {
                  m_settings = root.LauncherSettings;
              } 
          }

          if (m_settings == null)
          {
              m_settings = new LauncherSettings();
          } 
      }
    /// <summary>
    /// Write an application configuration to a file.
    /// </summary>
    /// <param name="app_">The application object to save.</param>
    /// <param name="filename_">The complete output filename.</param>
    public void WriteApplicationConfig(Model.Application app_, string filename_)
    {
      SerializeObjectToFile(app_, filename_);
    }
    #endregion Application Config

    #region User Config

    /// <summary>
    /// Get the users locally persisted settings.
    /// </summary>
    /// <returns></returns>
    public Model.UserSettings.UserSettings ReadUserConfig()
    {
      Model.UserSettings.UserSettings userSettings;
      string path = Launcher.UserFile;
      if (File.Exists(path))
      {
        System.Xml.XmlTextReader reader = new System.Xml.XmlTextReader(path);
        object obj = DeserializeObjectFromReader(reader, typeof(Model.UserSettings.UserSettings), path);
        userSettings = obj as Model.UserSettings.UserSettings;
      }
      else
      {
        userSettings = new Model.UserSettings.UserSettings();
      }
      return userSettings;
    }

    public Model.UserSettings.ExtraUserSettings ReadExtraUserConfig()
    {
        Model.UserSettings.ExtraUserSettings extraUserSettings;
        string path = Launcher.ExtraUserFile;
        if (File.Exists(path))
        {
            System.Xml.XmlTextReader reader = new System.Xml.XmlTextReader(path);
            object obj = DeserializeObjectFromReader(reader, typeof(Model.UserSettings.ExtraUserSettings), path);
            extraUserSettings = obj as Model.UserSettings.ExtraUserSettings;
        }
        else
        {
            extraUserSettings = new Model.UserSettings.ExtraUserSettings() {UseModernUI = Settings.DefaultToModernUI};
        }
        return extraUserSettings;
    }

    /// <summary>
    /// Saves the user configuration to a file
    /// </summary>
    public void WriteUserConfig(Model.UserSettings.UserSettings userSettings_)
    {
      SerializeObjectToFile(userSettings_, Launcher.UserFile);
    }

    public void WriteExtraUserConfig(Model.UserSettings.ExtraUserSettings userSettings_)
    {
        SerializeObjectToFile(userSettings_, Launcher.ExtraUserFile);
    }
    #endregion User Config

    #endregion Public Methods

    #region Private Helper Methods

    /// <summary>
    /// Attempts to close down a xml reader.
    /// </summary>
    /// <param name="reader_">The xml reader to close.</param>
    private static void CloseReader(System.Xml.XmlReader reader_)
    {
      if (reader_ != null)
      {
        try
        {
          reader_.Close();
        }
        catch (Exception ex_)
        {
          LogLayer.Error.Location(CLASS_NAME, "CloseReader").Append(string.Format("Error closing reader. Exception {0}", ex_.ToString())).Send();
        }
      }
    }

    /// <summary>
    /// Attempts to close down a xml writer.
    /// </summary>
    /// <param name="writer_">The xml writer to close.</param>
    private void CloseWriter(System.Xml.XmlWriter writer_)
    {
      if (writer_ != null)
      {
        try
        {
          writer_.Flush();
          writer_.Close();
        }
        catch (Exception ex_)
        {
          LogLayer.Error.Location(CLASS_NAME, "CloseWriter").Append(string.Format("Error closing writer. Exception {0}", ex_.ToString())).Send();
        }
      }
    }

      private static Dictionary<Type, XmlSerializer> serializers = new Dictionary<Type, XmlSerializer>(); 
      /// <summary>
      /// Deserializes an object from a XmlTextReader using the XmlSerializer
      /// </summary>
      /// <param name="reader_">The reader to Deserialize from.</param>
      /// <param name="type_">The type of object to Deserialize.</param>
      /// <param name="path_">file path</param>
      /// <returns>The Deserialized object.</returns> 
      private static object DeserializeObjectFromReader(XmlTextReader reader_, Type type_, string path_)
      {
          try
          {
              XmlSerializer valueSer;
              if (!serializers.TryGetValue(type_, out valueSer))
              {
                   valueSer = new System.Xml.Serialization.XmlSerializer(type_);
                   serializers.Add(type_, valueSer);
              }
              object obj = valueSer.Deserialize(reader_);
              return obj;
          }
          catch (System.InvalidOperationException ex_)
          {
              if (ex_ != null && ex_.InnerException != null)
              {
                  if (ex_.InnerException is IOException)
                  {
                      LogLayer.Info.Location(CLASS_NAME, "DeserializeObjectFromReader")
                              .Append(
                                  string.Format(
                                      "Problem reading application file (path : {0}). Likely this app config not disted to this cell.",
                                      path_))
                              .Send();
                      return null;
                  }
                  else if (ex_.InnerException is UnauthorizedAccessException)
                  {
                      LogLayer.Info.Location(CLASS_NAME, "DeserializeObjectFromReader")
                              .Append(string.Format("Problem reading application file (path : {0}). ", path_) +
                                      ex_.InnerException)
                              .Send();
                      return null;
                  }
              }
              throw;

          }
          finally
          {
              CloseReader(reader_);
          }
      }

      /// <summary>
    /// Deserializes a xml string into an object.
    /// </summary>
    /// <param name="xml_">The Xml string to Deserialize.</param>
    /// <param name="type_">The type of object to Deserialize.</param>
    private object DeserializeObjectFromString(string xml_, Type type_)
    {
      System.Xml.XmlTextReader reader = new XmlTextReader(new StringReader(xml_));
      object obj = DeserializeObjectFromReader(reader, type_, string.Empty);
      return obj;
    }

    /// <summary>
    /// Reads a root configuration file (including XIncludes) into a string 
    /// </summary>
    private string ReadRootConfigIntoString(List<string> groupNames_)
    {
      MSXmlProcessingReader processingReader = null;
      System.Xml.XmlDocument doc = null;
      MemoryStream ms = null;
      XmlTextWriter msWriter = null;
      string xml = null;

      try
      {
        doc = new XmlDocument();
        try
        {
          LoadDocumentWithIncludes(doc, m_rootConfigFilename);
        }
        catch (Exception ex_)
        {
          LogLayer.Error.Location(CLASS_NAME, "ReadRootConfigIntoString").Append(string.Format("Failed to load Root.xml from file specified {0}. Trying backup file. Exception {1}", m_rootConfigFilename, ex_.ToString())).Send();
          try
          {
            doc = Launcher.GetConfigDoc(Launcher.BackupFile);
            _usingBackup = true;
          }
          catch (Exception backupex_)
          {
            LogLayer.Error.Location(CLASS_NAME, "ReadRootConfigIntoString").Append(string.Format("Failed to load backup Root.config. Trying backup file. Fatal Exception {0}", backupex_.ToString())).Send();
            throw new ApplicationException(
              "Failed to load " + m_rootConfigFilename + " file. Backup failed too. Root.xml is missing from this cell" + Environment.GetEnvironmentVariable("AFS_ThisCell") + Environment.NewLine +
              "Please contact iedlauncher-dev", backupex_);
          }
        }

        RemoveUnathorizedApps(doc, groupNames_);

        //Using Memory stream to hold an indented version of the XML document
        //to avoid a bug with MSXMLProcessingReader, which skips a root element of the 
        //2nd xi:include if the 2 xi:includes are side-by-side without even a whitespace
        //HACK:

        ms = new MemoryStream();
        msWriter = new XmlTextWriter(ms, System.Text.Encoding.UTF8);
        msWriter.Formatting = Formatting.Indented;
        doc.WriteTo(msWriter);
        msWriter.Flush();

        //Go to beginning
        ms.Position = 0;
        XmlTextReader reader = new System.Xml.XmlTextReader(ms, XmlNodeType.Document, null);
        //Use the MSXmlProcessingReader to support XIncludes
        processingReader = new MSXmlProcessingReader(reader);

        doc = new System.Xml.XmlDocument();
        try
        {
          doc.Load(processingReader);
        }
        catch (Exception ex_)
        {
          LogLayer.Error.Location(CLASS_NAME, "ReadRootConfigIntoString").Append(string.Format("Failed to load Root.config from file specified {0}. Trying backup file. Exception {1}", m_rootConfigFilename, ex_.ToString())).Send();
          try
          {
            doc = Launcher.GetConfigDoc(Launcher.BackupFile);
            _usingBackup = true;
          }
          catch (Exception backupex_)
          {
            FileNotFoundException fnex = ex_ as FileNotFoundException;
            LogLayer.Error.Location(CLASS_NAME, "ReadRootConfigIntoString").Append(string.Format("Failed to load backup Root.config. Trying backup file. Fatal Exception {0}", backupex_.ToString())).Send();
            if (fnex != null)
            {
              throw new ApplicationException(
                "Failed to load " + m_rootConfigFilename + " Backup failed too." + Environment.NewLine + Environment.NewLine +
                Launcher.LauncherName + " failed to load because this application config was not found:" + Environment.NewLine + Environment.NewLine + fnex.FileName + Environment.NewLine + Environment.NewLine +
                "Please make sure that file is disted to this cell(" +
                Environment.GetEnvironmentVariable("AFS_ThisCell") + ")", backupex_);
            }
            else
            {
              throw new ApplicationException(
                "Failed to load " + m_rootConfigFilename + "file. Backup failed too." + Environment.NewLine +
                "Please contact iedlauncher-dev", backupex_);
            }
          }
        }

        //Save the backup file ONLY if we successfully retrieved a disted version.
        if (!_usingBackup)
        {
          try
          {   //Make a backup of expanded XML File 
            Launcher.BackupXMLConfig(doc, Launcher.BackupFile);
          }
          catch (Exception ex_)
          {
            LogLayer.Error.Location(CLASS_NAME, "ReadRootConfigIntoString").Append(string.Format("Failed to save a backup of an expanded Root file. Loading from backup found fail. Exception {0}", ex_.ToString())).Send();

          }
        }
        xml = doc.DocumentElement.OuterXml;
      }
      finally
      {
        CloseReader(processingReader);
        CloseWriter(msWriter);
      }

      return xml;
    } 
    /// <summary>
    /// Loads a document and recursively includes files references by an IncludeFile element
    /// </summary>
    /// <param name="doc"></param>
    /// <param name="fileName"></param>
    private static void LoadDocumentWithIncludes(XmlDocument doc, string fileName)
    {
        var uri = new Uri(fileName);
        if (uri.IsFile)
        { 
            fileName = uri.LocalPath;
        }
        string text = File.ReadAllText(fileName);
        text = PathUtilities.ReplaceEnvironmentVariables(text);
        doc.LoadXml(text);
      foreach (XmlElement includeNode in doc.SelectNodes("//IncludeFile"))
      {
        try
        {
          var path = includeNode.Attributes["path"].Value;
          var includeDoc = new XmlDocument(doc.NameTable);
          try
          {
            LoadDocumentWithIncludes(includeDoc, path.Replace('/', '\\'));
          }
          catch (Exception ex_)
          {
            LogLayer.Error.Location(CLASS_NAME, "LoadDocumentWithIncludes").Append(string.Format("Failed to load included path {0}. Exception {1}", path, ex_.ToString())).Send();
            continue;
          }
          var xpathAttribute = includeNode.Attributes["xpath"];
          var xpath = xpathAttribute == null ? "." : xpathAttribute.Value;
          foreach (XmlNode newNode in includeDoc.DocumentElement.SelectNodes(xpath))
          {
            includeNode.ParentNode.InsertBefore(doc.ImportNode(newNode, true), includeNode);
          }
        }
        finally
        {
          includeNode.ParentNode.RemoveChild(includeNode);
        }
      }
    }

    /// <summary>
    /// Traverses through the root XML Document before xi:include, removing unentitled apps
    /// If the app says it uses mailgroups, we have to open it up and look through the file to remove it.
    /// </summary>
    /// <param name="root_"></param>
    private static void RemoveUnathorizedApps(XmlDocument root_, List<string> groupNames_)
    {
      XmlNamespaceManager mgr = new XmlNamespaceManager(new NameTable());
      mgr.AddNamespace("xi", "http://www.w3.org/2001/XInclude");

      foreach (XmlNode xiInclNode in root_.DocumentElement.SelectNodes("Applications/xi:include", mgr))
      {
        if (LDAPAppEntitled(xiInclNode, groupNames_))
        {
          // we don't need to check the Eclipse as this is valid so just continue
          continue;
        }
        else
        {
          xiInclNode.ParentNode.RemoveChild(xiInclNode);
        }
      }
    }

    private static bool LDAPAppEntitled(XmlNode xiInclNode_, List<string> groupNames_)
    {
      try
      {
        if (xiInclNode_.Attributes["href"].Value == null)
        {
          return false;
        }

        Application application = ReadApplicationConfig(xiInclNode_.Attributes["href"].Value);

        if (application == null)
        {
          return false;
        }

        foreach (EnvironmentGroup envGroup in application.EnvironmentGroups)
        {
          foreach (Model.Environment env in envGroup.Environments)
          {
            if (env.Mailgroups != null)
            {
              foreach (string mailgroup in env.Mailgroups)
              {
                if (groupNames_.Contains(mailgroup))
                {
                  return true;
                }
              }
            }
          }
        }
      }
      catch (Exception ex_)
      {
        LogLayer.Error.Location(CLASS_NAME, "LDAPAppEntitled").Append(string.Format("Problem reading application file (app id : {0}) to check for LDAP usage {1}", xiInclNode_.Attributes["appID"].Value, ex_.ToString())).Send();
      }

      return false;
    }

    /// <summary>
    /// Serializes an object to a file.
    /// </summary>
    private void SerializeObjectToFile(object obj_, string filename_)
    {
      System.Xml.XmlTextWriter writer = null;

      try
      {
        System.Xml.Serialization.XmlSerializer valueSer = new System.Xml.Serialization.XmlSerializer(obj_.GetType());
        writer = new System.Xml.XmlTextWriter(filename_, System.Text.Encoding.UTF8);
        writer.Formatting = System.Xml.Formatting.Indented;
        valueSer.Serialize(writer, obj_);
      }
      finally
      {
        CloseWriter(writer);
      }
    }

    #endregion
  }
}
