﻿#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2006 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

              $Id: //eai/msdotnet/msgui/trunk/assemblies/Launcher/MSDesktop.Launcher/Services/CopyLocalManager.cs#4 $
  $DateTime: 2014/02/09 22:41:22 $
    $Change: 865813 $
    $Author: caijin $
_____________________________________________________________________________*/
#endregion File Info Header

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Security.Permissions;
using System.Threading;
using MSDesktop.Launcher.Model;
using MSDesktop.Launcher.UI;

namespace MSDesktop.Launcher.Services
{
    public class CopyLocalManager
    {
        private const string CLASS = "CopyLocalManager";

        public CopyLocalManager()
        {
        }

        public void DownloadAndUnzipPackage(ViewInfo viewInfo_, BackgroundWorker worker_)
        {
            string newPackage = viewInfo_.GetPackagePath();
            string packageName = viewInfo_.PackageName;
            string destPath = viewInfo_.GetPath(IEDCodeLocation.COPYLOCAL);
            string tempPath = viewInfo_.GetPath(IEDCodeLocation.COPYLOCALTEMP);

            try
            {
                worker_.ReportProgress(0);

                // Copy from AFS to local
                if (CheckFileOrDirPermissioning(tempPath))
                {
                    CopyFiles(newPackage, tempPath, packageName);
                    worker_.ReportProgress(25);

                    // Unpack
                    if (CheckFileOrDirPermissioning(tempPath + "Output"))
                    {
                        Unzip(tempPath + packageName, tempPath + "Output", worker_);

                        // Copy to actual path for running
                        if (CheckFileOrDirPermissioning(destPath))
                        {
                            CopyDirectory(tempPath + "Output", destPath);

                            // copy over the latest package so it's there to compare date stamps to next time
                            CopyFiles(tempPath + packageName, destPath, packageName);
                            worker_.ReportProgress(95);

                            // delete the cached package
                            Directory.Delete(tempPath, true);
                            worker_.ReportProgress(100);
                            Thread.Sleep(1000); //sleep for a second so user can see progress complete.
                        }
                    }
                }
            }
            catch (InvalidCrcException ex_)
            {
                if (Directory.Exists(tempPath))
                {
                    Directory.Delete(tempPath, true);
                }

                //log invalid package.  sticking to current version 
                LogLayer.Error.Location(CLASS, "CheckFileOrDirPermissioning").Append(String.Format("Invalid package {0}. Ex {1}", newPackage, ex_)).Send();

                throw;
            }
            catch (Exception ex_)
            {
                LogLayer.Error.Location(CLASS, "CheckFileOrDirPermissioning").Append(String.Format("Error downloading/directory updating. {0}", ex_)).Send();
                throw;
            }
        }

        private static void CopyFiles(string sourcePath_, string targetPath_, string packageName_)
        {
            if (sourcePath_.Contains("http://"))
            {
                WebClient webClient = new WebClient();
                webClient.DownloadFile(sourcePath_, targetPath_ + packageName_);
            }
            else
            {
                File.Copy(sourcePath_, targetPath_ + packageName_, true);
            }
        }

        private bool Unzip(string filePath_, string targetpath_, BackgroundWorker worker_)
        {
            bool validUnzip = true;
            ICSharpCode.SharpZipLib.Zip.ZipFile zipFile = null;
            ArrayList corruptFiles = new ArrayList();

            try
            {
                zipFile = new ICSharpCode.SharpZipLib.Zip.ZipFile(filePath_);

                IEnumerator zipFilesEnumerator = zipFile.GetEnumerator();
                int BLOCK_SIZE = 8096 * 8;
                byte[] buffer = new Byte[BLOCK_SIZE];

                int fileCount = 0;
                double totalCount = zipFile.Count;
                while (zipFilesEnumerator.MoveNext())
                {
                    fileCount++;
                    double percentageUnzipped = fileCount / totalCount;
                    int percentageComplete = Convert.ToInt32(percentageUnzipped * 100 / 2);   // divide by 2 because unzipping is 50% of the whole download & unzip process.  Copying is arbitrarily the other 50%
                    worker_.ReportProgress(25 + percentageComplete);


                    ICSharpCode.SharpZipLib.Zip.ZipEntry zipEntry = (ICSharpCode.SharpZipLib.Zip.ZipEntry)zipFilesEnumerator.Current;
                    ICSharpCode.SharpZipLib.Checksums.Crc32 crc = new ICSharpCode.SharpZipLib.Checksums.Crc32();

                    try
                    {
                        Stream s = zipFile.GetInputStream(zipEntry);

                        if (zipEntry.Name.Contains(@"/") || zipEntry.Name.Contains(@"\"))
                        {
                            string[] subdirectories = zipEntry.Name.Split(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
                            string fullPath = targetpath_;
                            for (int i = 0; i < subdirectories.Length - 1; i++)
                            {
                                fullPath += Path.DirectorySeparatorChar + subdirectories[i];
                                CheckFileOrDirPermissioning(fullPath);
                            }
                        }


                        using (FileStream fs = new FileStream(targetpath_ + Path.DirectorySeparatorChar + zipEntry.Name, FileMode.OpenOrCreate, FileAccess.Write, FileShare.Read))
                        {
                            while (true)
                            {
                                int sizeRead = s.Read(buffer, 0, buffer.Length);
                                if (sizeRead > 0)
                                {
                                    fs.Write(buffer, 0, sizeRead);
                                    crc.Update(buffer, 0, sizeRead);
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }
                    }
                    catch (Exception e_)
                    {
                        validUnzip = false;
                        LogLayer.Info.Location(CLASS, "Unzip").Append(String.Format("Could not write file at '{0}'. Exception {0} {1}", targetpath_, e_, e_.StackTrace)).Send();
                        throw new DownloaderFileAccessException("Could not gain access to write zip file at " + targetpath_, e_);
                    }

                    if (crc.Value != zipEntry.Crc)
                    {
                        LogLayer.Info.Location(CLASS, "Unzip").Append(String.Format("File '{0}' failed crc check", zipEntry.Name)).Send();
                        corruptFiles.Add(zipEntry.Name);
                    }
                }
            }
            catch (InvalidCrcException)
            {
                throw;
            }
            catch (DownloaderFileAccessException hfa_)
            {
                throw hfa_;
            }
            catch (Exception ex)
            {
                LogLayer.Error.Location(CLASS, "Unzip").Append(String.Format("Error unzipping file {0} {1}", ex, ex.StackTrace)).Send();
                throw new DownloaderUnzipException("Error unzipping file " + ex.Message, ex);
            }
            finally
            {
                if (zipFile != null)
                {
                    zipFile.Close();
                }
            }

            if (corruptFiles.Count > 0)
            {
                string message = "Invalid crc:";
                foreach (string file in corruptFiles)
                {
                    message += " " + file;
                }

                throw new InvalidCrcException(message);
            }

            return validUnzip;
        }

        private bool CheckFileOrDirPermissioning(string path_)
        {
            try
            {
                FileIOPermission fiop = new FileIOPermission(FileIOPermissionAccess.AllAccess, path_);
                fiop.Demand();
                if (!Directory.Exists(path_))
                {
                    Directory.CreateDirectory(path_);
                }
                return true;
            }
            catch (Exception ex_)
            {
                // Trace it
                LogLayer.Error.Location(CLASS, "CheckFileOrDirPermissioning").Append(String.Format("Error demanding permission for {0}. Ex {1}", path_, ex_)).Send();
                ExceptionViewer.ShowException("Error demanding permission for " + path_, ex_);
                return false;
            }
        }

        private void CopyDirectory(string srcPath_, string destPath_)
        {
            String[] fileList;
            if (destPath_[destPath_.Length - 1] != Path.DirectorySeparatorChar)
            {
                destPath_ += Path.DirectorySeparatorChar;
            }

            if (Directory.Exists(destPath_))
            {
                // if it DOES exist (i.e. updating a previous version), delete it first which will make sure we clean out all existing files.
                Directory.Delete(destPath_, true);
            }

            // we know for sure the directory doesn't exist as we just deleted it.  so create it again now
            Directory.CreateDirectory(destPath_);

            fileList = Directory.GetFiles(srcPath_);
            List<string> filesAndSubdirectories = new List<string>(fileList);
            filesAndSubdirectories.AddRange(Directory.GetDirectories(srcPath_));  //Need to add any sub directories to the list of items to be copied.

            foreach (string file in filesAndSubdirectories)
            {
                // Sub directories 
                if (Directory.Exists(file))
                {
                    CopyDirectory(file, destPath_ + Path.GetFileName(file));
                }
                else   // files in directory 
                {
                    File.Copy(file, destPath_ + Path.GetFileName(file), true);
                }
            }
        }
    }

    public class InvalidCrcException : ApplicationException
    {
        public InvalidCrcException(string message_)
            : base(message_)
        {
        }

        public InvalidCrcException(string message_, Exception inner_)
            : base(message_, inner_)
        {
        }
    }

    public class DownloaderException : ApplicationException
    {
        public DownloaderException(string message_, Exception inner_)
            : base(message_, inner_)
        {
            // 
        }

        public DownloaderException(string message_, string code_)
            : base(message_)
        {
            code = code_;
        }

        public string Code { get { return code; } }

        private readonly string code = "";
    }

    public class DownloaderDownloadException : ApplicationException
    {
        public DownloaderDownloadException(string message_, Exception inner_)
            : base(message_, inner_)
        {
            // 
        }

        public DownloaderDownloadException(string message_, string code_)
            : base(message_)
        {
            code = code_;
        }

        public string Code { get { return code; } }

        private readonly string code = "";
    }

    public class DownloaderUnzipException : ApplicationException
    {
        public DownloaderUnzipException(string message_, Exception inner_)
            : base(message_, inner_)
        {
            // 
        }

        public DownloaderUnzipException(string message_, string code_)
            : base(message_)
        {
            code = code_;
        }

        public string Code { get { return code; } }

        private readonly string code = "";
    }

    public class DownloaderFileAccessException : ApplicationException
    {
        public DownloaderFileAccessException(string message_, Exception inner_)
            : base(message_, inner_)
        {
            // 
        }

        public DownloaderFileAccessException(string message_, string code_)
            : base(message_)
        {
            code = code_;
        }

        public string Code { get { return code; } }

        private readonly string code = "";
    }

}
