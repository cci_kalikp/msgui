#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/Launcher/MSDesktop.Launcher/LogLayer.cs#3 $
  $DateTime: 2014/01/24 22:57:03 $
    $Change: 863890 $
    $Author: caijin $
_____________________________________________________________________________*/
#endregion

using System.Reflection;
using MorganStanley.MSDotNet.MSLog;

namespace MSDesktop.Launcher
{
  internal class LogLayer
  {
    private const string META = "msdotnet";
    private const string PROJECT = "msdesktop";
    private static readonly string REVISION = Assembly.GetExecutingAssembly().GetName().Version.ToString();

    public static MSLogMessage Alert 
    {
      get { return MSLog.Alert().SetLayer(META, PROJECT, REVISION); }
    }

    public static MSLogMessage Critical 
    {
      get { return MSLog.Critical().SetLayer(META, PROJECT, REVISION); }
    }

    public static MSLogMessage Debug
    {
      get { return MSLog.Debug().SetLayer(META, PROJECT, REVISION); }
    }

    public static MSLogMessage Emergency
    {
      get { return MSLog.Emergency().SetLayer(META, PROJECT, REVISION); }
    }

    public static MSLogMessage Error
    {
      get { return MSLog.Error().SetLayer(META, PROJECT, REVISION); }
    }

    public static MSLogMessage Info 
    {
      get { return MSLog.Info().SetLayer(META, PROJECT, REVISION); }
    }

    public static MSLogMessage Notice 
    {
      get { return MSLog.Notice().SetLayer(META, PROJECT, REVISION); }
    }

    public static MSLogMessage Warning 
    {
      get { return MSLog.Warning().SetLayer(META, PROJECT, REVISION); }
    }
  }
}
