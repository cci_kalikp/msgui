#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2006 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

              $Id: //eai/msdotnet/msgui/trunk/assemblies/Launcher/MSDesktop.Launcher/Scheduler.cs#4 $
  $DateTime: 2014/02/09 22:41:22 $
    $Change: 865813 $
    $Author: caijin $
_____________________________________________________________________________*/
#endregion File Info Header

using System;
using System.Threading;
using System.Globalization;
using System.Collections;
using MSDesktop.Launcher.UI;

namespace MSDesktop.Launcher
{
  /// <summary>
  /// Manages Daily Schedules
  /// </summary>
  public class Scheduler
  {
    #region Private Instance members
    private const string CLASS_NAME = "Scheduler";
    private readonly ArrayList _scheduleEvents = new ArrayList();
    private readonly ProcessLaunchManager _processLaunchManager;
    #endregion Private instance members
    
    #region Constructor
    //Prevent from instantiation
    public Scheduler(ProcessLaunchManager processLaunchManager_)
    {
      _processLaunchManager = processLaunchManager_;
    }
    #endregion Constructor

    #region Private Methods
    /// <summary>
    /// Adds event to the list to be scheduled the next time scheduler run at midnight.
    /// </summary>
    /// <param name="event_"></param>
    private void AddEvent(Model.ScheduleEvent event_)
    {
      ArrayList.Synchronized(_scheduleEvents).Add(event_);
    }

    /// <summary>
    /// set the events up in the launch manager
    /// </summary>
    /// <param name="initialLoad_">This is true the first time the app starts to cover apps autostarting earlier on same day launcher was started</param>
    private void ScheduleEvents(bool initialLoad_)
    {
      ArrayList syncEvents = ArrayList.Synchronized(_scheduleEvents);

      _processLaunchManager.ClearSchedule();

      foreach(Model.ScheduleEvent evt in syncEvents)
      {
        LogLayer.Info.Location(CLASS_NAME, "ScheduleEvents").Append(String.Format("Scheduling event: {0}", evt.ToString())).Send();
        evt.SetTodaysSchedule(initialLoad_);
      }
    }

    private void ClearEvents(ViewInfo viewInfo_)
    {
      ArrayList syncEvents = ArrayList.Synchronized(_scheduleEvents);

      //reverse over the collection to remove
      for(int i = syncEvents.Count-1; i > -1; i--)
      {
        if(((Model.ScheduleEvent)syncEvents[i]).ViewInfo.UniqueKey == viewInfo_.UniqueKey)
        {
          ArrayList.Synchronized(_scheduleEvents).Remove(syncEvents[i]);
        }
      }
    }
    #endregion Private Methods

    #region Static Methods 
    /// <summary>
    /// Activate the prepared schedule
    /// </summary>
    /// <param name="initialLoad_">This is true the first time the app starts to cover apps autostarting earlier on same day launcher was started</param>
    public void ApplySchedule(bool initialLoad_)
    {
       ScheduleEvents(initialLoad_);
    }

    public void PrepareSchedule(ViewInfo viewInfo_, Model.AllowedTimeGroup timeGroup_ )
    {
      if(timeGroup_ == null)
      {
        LogLayer.Info.Location(CLASS_NAME, "PrepareSchedule").Append(String.Format("Time group is not specified for app {0}", viewInfo_.ApplicationKey )).Send();
        return;
      }

      //Clear out otherwise we get dupes
      ClearScheduledEvents(viewInfo_);
      Model.StartEvent start = new Model.StartEvent(viewInfo_, _processLaunchManager); 
      CultureInfo dtParsingCulture = Thread.CurrentThread.CurrentCulture;

      foreach(Model.AllowedTime at in timeGroup_.AllowedTimes)
      {
        if(Enum.IsDefined(typeof(DayOfWeek), at.Start.Day))
        {
          DayOfWeek weekDay = (DayOfWeek) Enum.Parse(typeof(DayOfWeek), at.Start.Day, true);
          start.SetTime(weekDay, DateTime.ParseExact(at.Start.Time, Model.ScheduleEvent.TIME_FORMAT, dtParsingCulture));
        } 
      }

      //only add the events if the flag is set.
      if(viewInfo_.AutoStartValue)
      {
        AddEvent(start); 
      }
    }

    public void ClearScheduledEvents(ViewInfo viewInfo_)
    {
      ClearEvents(viewInfo_);
    }
    #endregion Static Methods 
  }
}


