﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.IED.Concord.Configuration;

namespace MSDesktop.Launcher
{
    public class FileConfigurationStore:FileConfigurationStoreEx
    {
        protected override string GetUserDirectory(string username_, string app_, string reg_, string env_, string role_)
        {
            string dir = String.Empty;

            if (role_ != null)
            {
                dir = String.Format(@"{0}\Configuration\{1}\{2}\{3}", Environment.GetEnvironmentVariable("AppFolder"), reg_, env_, role_);
            }
            else
            {
                dir = String.Format(@"{0}\Configuration\{1}\{2}", Environment.GetEnvironmentVariable("AppFolder"), reg_, env_);
            }

            if (!System.IO.Directory.Exists(dir))
            {
                System.IO.Directory.CreateDirectory(dir);
            }

            return dir;
        }
    }
}
