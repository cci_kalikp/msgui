using System;
using System.Runtime.InteropServices;

namespace MSDesktop.StartProcess
{
	/// <summary>
	/// Win32 Message sender is a utility class used to wrap logic for sending Windows Messages.
	/// </summary>
	/// 

  #region Enum Definitions
  [Flags]
  public   enum MessageBroadcastFlags : uint
  {     
    BSF_QUERY            = 0x00000001,
    BSF_IGNORECURRENTTASK       = 0x00000002,
    BSF_FLUSHDISK           = 0x00000004,
    BSF_NOHANG              = 0x00000008,
    BSF_POSTMESSAGE         = 0x00000010,
    BSF_FORCEIFHUNG         = 0x00000020,
    BSF_NOTIMEOUTIFNOTHUNG      = 0x00000040,
    BSF_ALLOWSFW            = 0x00000080,
    BSF_SENDNOTIFYMESSAGE       = 0x00000100,
    BSF_RETURNHDESK         = 0x00000200,
    BSF_LUID            = 0x00000400,
  }

  [Flags]
  public  enum MessageBroadcastRecipients : uint
  {
    BSM_ALLCOMPONENTS       = 0x00000000,
    BSM_VXDS        = 0x00000001,
    BSM_NETDRIVER       = 0x00000002,
    BSM_INSTALLABLEDRIVERS  = 0x00000004,
    BSM_APPLICATIONS    = 0x00000008,
    BSM_ALLDESKTOPS     = 0x00000010,
  }
  #endregion

	public class Win32MessageSender
	{
    #region Win32 API Definitions
    [DllImport("user32.dll", SetLastError=true, CharSet=CharSet.Auto)]
    public static extern uint RegisterWindowMessage(string lpString);

    [DllImport("user32", SetLastError=true, EntryPoint="BroadcastSystemMessage")]
    public static extern int BroadcastSystemMessageRecipients(MessageBroadcastFlags dwFlags, ref MessageBroadcastRecipients lpdwRecipients, uint uiMessage, IntPtr wParam, IntPtr lParam);

    [DllImport("user32", SetLastError=true)]
    public static extern int BroadcastSystemMessage(MessageBroadcastFlags dwFlags, IntPtr lpdwRecipients, uint uiMessage, IntPtr wParam, IntPtr lParam);
    #endregion
	}
}
