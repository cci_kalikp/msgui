using System.Reflection; 
using System.Resources;

[assembly: AssemblyTitle("MSDesktop ProcessStart")]
[assembly: AssemblyDescription("MSDesktop Out of process launcher")]
[assembly: AssemblyConfiguration("")]
//[assembly: AssemblyCompany("Morgan Stanley")]
[assembly: AssemblyProduct("MSDesktop Launcher Application")]
[assembly: AssemblyCopyright("Copyright � Morgan Stanley")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: NeutralResourcesLanguageAttribute("en-US")] 

[assembly: AssemblyDelaySign(false)]
[assembly: AssemblyKeyFile(@"\\ms\dev\msdotnet\msdotnet\incr\src\Assemblies\MSDotNet\StrongName.snk")]
[assembly: AssemblyKeyName("")]
