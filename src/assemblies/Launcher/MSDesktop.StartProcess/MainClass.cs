#region Copyright
/*_____________________________________________________________________________
  Copyright (c) 2006 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/Launcher/MSDesktop.StartProcess/MainClass.cs#8 $
  $DateTime: 2014/03/24 05:36:31 $
    $Change: 872658 $
    $Author: caijin $
_____________________________________________________________________________*/
#endregion

using System;
using System.Diagnostics;
using System.Threading; 
using log4net;

namespace MSDesktop.StartProcess
{
  /// <summary>
  /// Use this program instead of windows START command to 
  /// add an ability to catch PID of the process this guy starts.
  ///
  /// Returning processid as the %ERRORLEVEL%
  /// Pick it up in cmd file as %ERRORLEVEL% env variable
  /// then use EXIT %ERRORLEVEL% to pass it out.
  /// </summary>
  class MainClass
  {
    #region Private constants
    private static readonly ILog log = LogManager.GetLogger(typeof(MainClass));

    private const string SHUTDOWN_APP_MSG_NAME = "Launcher_App_Shutdown_Application";
    private const string LAUNCHER_APP_NAME = "msdesktop.launcher.exe";
    #endregion

    public static void Main(string[] args_)
    {
       Thread.Sleep(15000);  //sleep for 15 seconds to allow the previous instance to die naturally.
      log4net.Config.XmlConfigurator.Configure(); // Loads configuration from app.config
      log.Info("Started...sleeping for 15s...");

      if (args_.Length > 0)
      {
        string processIDToKill;
        string processToStart;
        string processArgs;
        string workDir;

        ParseParams(args_, out processIDToKill, out processToStart, out processArgs, out workDir);

        log.Info(String.Format("Found these arguments: process to kill={0}, process to start={1}, workDir={2}, processArgs={3}",
                                processIDToKill, processToStart, workDir, processArgs)); 

        //Exit if processtoStart was not specified
        if (processToStart == null)
        {
          return;                                                                                                                                                                                                                                                         
        }

        Process p = new Process();
        ProcessStartInfo psi = new ProcessStartInfo();
        psi.FileName = processToStart;
        psi.WorkingDirectory = Environment.CurrentDirectory;
        psi.Arguments = processArgs;
        psi.UseShellExecute = false;

        if (workDir.Length > 0)
        {
          psi.WorkingDirectory = workDir;
        }
        p.StartInfo = psi;
        if (processIDToKill != null)
        {
          try
          {
            Process killP = Process.GetProcessById(int.Parse(processIDToKill));
            uint killMSG = Win32MessageSender.RegisterWindowMessage(SHUTDOWN_APP_MSG_NAME);

            //Send this Windows Broadcast message only if the process we're killing is the launcher process.
            if (killP.MainModule.FileName.ToLower().IndexOf(LAUNCHER_APP_NAME) > -1)
            {
              //Broadcasting Message to all applications
              MessageBroadcastRecipients rec = MessageBroadcastRecipients.BSM_APPLICATIONS;

              //Attempt to kill using broadcasting message
              Win32MessageSender.BroadcastSystemMessageRecipients(MessageBroadcastFlags.BSF_IGNORECURRENTTASK | MessageBroadcastFlags.BSF_FORCEIFHUNG,
                ref rec,
                killMSG,
                IntPtr.Zero,
                IntPtr.Zero);
            }

            if (!killP.HasExited)
            {
              killP.Kill();
            }

            if (killP.HasExited)
            {
              log.Info(String.Format("Successfully killed process {0}.", processIDToKill));
            }
          }
          catch (Exception ex_)
          {
            log.Info(String.Format("Could not kill process {0}. Error {1}.", processIDToKill, ex_.Message));
          }
        }
        try
        {
          log.Info(String.Format("Starting {0}.", p.StartInfo.FileName));
          bool result = p.Start();

          if (result)
          {
            log.Info(String.Format("Started process {0}.", p.StartInfo.FileName));
          }
          else
          {
            log.Info(String.Format("FAILED to start process {0}.", p.StartInfo.FileName));
          }
        }
        catch (Exception ex_)
        {
          log.Info(ex_.Message + ex_.StackTrace);
        }
        //returning processid as the %ERRORLEVEL%
        //Pick it up in cmd file as %ERRORLEVEL% env variable
        //then use EXIT %ERRORLEVEL% to pass it out.
        Environment.Exit(p.Id);
      }
    }

    /// <summary>
    /// Tries to parse parameters passed into the process from command line.
    /// Using out params for processID to kill  and processToStart
    /// </summary>
    /// <param name="args_">Arguments from cmd line </param>
    /// <param name="userName_">Out param for UserName</param>
    /// <param name="rootFileName_">Out param for RootFileName</param>
    /// <param name="schedulerDir_">Out param for scheduler config directory</param>
    private static void ParseParams(string[] args_, out string killProcessID_, out string processToStart_, out string processArgs_, out string pwd_)
    {
      bool killFound = false;
      bool processFound = false;
      bool pwdFound = false;
      killProcessID_ = null;
      processToStart_ = null;
      processArgs_ = string.Empty;
      pwd_ = string.Empty;


      for (int i = 0; i < args_.Length; i++)
      {
        // /k or -k would be a kill (case insensitive)
        if ((string.Compare(args_[i], "-k", true) == 0 ||
          string.Compare(args_[i], "/k", true) == 0))
        {
          if (killFound)
          {
            break;
          }
          if (processToStart_ != null)
          {
            processFound = true;
          }


          if (args_.Length > i)
          {
            //Set to next next argument and increment it
            killProcessID_ = args_[++i];
          }
          killFound = true;
        }
        else // /d or -d would be a process working directory (case insensitive)
          if ((string.Compare(args_[i], "-d", true) == 0 || string.Compare(args_[i], "/d", true) == 0))
          {
            if (pwdFound)
            {
              break;
            }
            if (processToStart_ != null)
            {
              processFound = true;
            }


            if (args_.Length > i)
            {
              //Set to next next argument and increment it
              pwd_ = args_[++i];
            }
            pwdFound = true;
          }
          else
          {
            if (processFound)
            {
              break;
            }
            if (processToStart_ == null)
            {
              processToStart_ = args_[i];
            }
            else
            {
                processArgs_ = string.Concat(processArgs_, " ", (args_[i].Contains(" ") && !args_[i].StartsWith("\"") ? string.Format("\"{0}\"", args_[i]) : args_[i]));
            }
          }
      }
      processArgs_ = processArgs_.Trim();
    }
  }
}
