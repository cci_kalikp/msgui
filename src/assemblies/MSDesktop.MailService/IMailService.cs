﻿
namespace MSDesktop.MailService
{
    public interface IMailService
    {
        /// <summary>
        /// Retrieves the name of this mail service.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Creates a new mail message, with fields pre-filled based on mail service configuration.
        /// </summary>
        /// <returns></returns>
        MsDesktopMailMessage CreateMailMessage();

        /// <summary>
        /// Sends the specified mail message.
        /// </summary>
        /// <param name="message"></param>
        void Send(MsDesktopMailMessage message);
    }
}
