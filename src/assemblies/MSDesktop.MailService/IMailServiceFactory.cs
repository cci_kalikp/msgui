﻿
namespace MSDesktop.MailService
{
    /// <summary>
    /// Represents the email service for MSDesktop-based applications.
    /// </summary>
    public interface IMailServiceFactory
    {
        /// <summary>
        /// Checks to see if a mailing service with the specified name is configured in the application configuration store.
        /// </summary>
        bool IsServiceConfigured(string serviceName);

        /// <summary>
        /// Retrieves the mail service instance by its name, as specified in the configuration. 
        /// </summary>
        /// <param name="serviceName"></param>
        /// <returns></returns>
        IMailService GetService(string serviceName);

        /// <summary>
        /// Creates a new instance of an email service, with specified default parameters for the message. 
        /// This service isn't named and can't be retrieved by name using GetService() method at a later time.
        /// </summary>
        /// <param name="smtpServer">SMTP server to send email through.</param>
        /// <param name="port">SMTP server port.</param>
        /// <param name="defaultFrom">Default value for Sender email address</param>
        /// <param name="defaultTo">Default recipient of the message. Only one email address is allowed.</param>
        /// <param name="defaultSubject">Default value for subject of the email message.</param>
        /// <returns></returns>
        IMailService CreateNewService(string smtpServer, int port = 25, string defaultFrom = null, string defaultTo = null,
                                                              string defaultSubject = null);
    }
}
