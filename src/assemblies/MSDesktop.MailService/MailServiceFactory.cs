﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using MorganStanley.IED.Concord.Configuration;

namespace MSDesktop.MailService
{
    public sealed class MailServiceFactory : IMailServiceFactory
    {
        private readonly IDictionary<string, IMailService> _mailServices = new ConcurrentDictionary<string, IMailService>();

        public MailServiceFactory()
        {
            var configurator = (Configurator) ConfigurationManager.GetConfig("MailConfig");
            var sr = new XmlSerializer(typeof (XmlMailServiceConfiguration), new XmlRootAttribute("MailServiceConfig"));

            var node = configurator.GetNode(".", new XmlNamespaceManager(new NameTable()));

            if (!string.IsNullOrEmpty(node.InnerXml))
            {
                XmlMailServiceConfiguration configuration;
                using (var stringReader = new StringReader(node.OuterXml))
                using (var reader = new XmlTextReader(stringReader))
                {
                    configuration = (XmlMailServiceConfiguration) sr.Deserialize(reader);
                }

                foreach (var entry in configuration.MailServiceConfigEntries)
                {
                    var newEntryConfig = new MailServiceConfig(
                           entry.Name,
                           entry.SmtpServer, 
                           entry.SmtpPort,
                           entry.Sender,
                           entry.ToRecipients,
                           entry.CcRecipients,
                           entry.Subject);
                    _mailServices[entry.Name] = new MsDesktopMailService(newEntryConfig);
                }
            }
        }

        /// <summary>
        /// Checks to see if a mailing service with the specified name is configured in the application configuration store.
        /// </summary>
        public bool IsServiceConfigured(string serviceName)
        {
            return _mailServices.ContainsKey(serviceName);
        }

        /// <summary>
        /// Retrieves the mail service instance by its name, as specified in the configuration. 
        /// </summary>
        /// <param name="serviceName"></param>
        /// <returns></returns>
        public IMailService GetService(string serviceName)
        {
            return _mailServices[serviceName];
        }

        /// <summary>
        /// Creates a new instance of an email service, with specified default parameters for the message. 
        /// This service isn't named and can't be retrieved by name using GetService() method at a later time.
        /// </summary>
        /// <param name="smtpServer">SMTP server to send email through.</param>
        /// <param name="port">SMTP server port.</param>
        /// <param name="defaultFrom">Default value for Sender email address</param>
        /// <param name="defaultTo">Default recipient of the message. Only one email address is allowed.</param>
        /// <param name="defaultSubject">Default value for subject of the email message.</param>
        /// <returns></returns>
        public IMailService CreateNewService(string smtpServer, int port = 25, string defaultFrom = null, string defaultTo = null,
                                            string defaultSubject = null)
        {
            var recipients = defaultFrom == null ? new string[] {} : new[] {defaultTo};

            var config = new MailServiceConfig(null, smtpServer, port, defaultFrom, recipients, null, defaultSubject);
            return new MsDesktopMailService(config);
        }

        /// <summary>
        /// Used to store and deserialize mail configuration entries.
        /// Please DO NOT USE in your code.
        /// </summary>
        [Serializable]
        public class XmlMailServiceConfiguration
        {
            public MailServiceConfigEntry[] MailServiceConfigEntries { get; set; }
        }

        /// <summary>
        /// Used to store and deserialize mail configuration entries.
        /// Please DO NOT USE in your code.
        /// </summary>
        [Serializable]
        public class MailServiceConfigEntry
        {
            private MailServiceConfigEntry()
            {
            }

            public MailServiceConfigEntry(MailServiceConfig config)
            {
                Name = config.Name;
                SmtpServer = config.SmtpServer;
                ToRecipients = config.DefaultTo.ToArray();
                SmtpPort = config.SmtpPort;
            }

            public string Name { get; set; }
            public string SmtpServer { get; set; }
            public int SmtpPort { get; set; }
            public string Sender { get; set; }
            public string[] ToRecipients { get; set; }
            public string[] CcRecipients { get; set; }
            public string Subject { get; set; }
        }
    }
}
