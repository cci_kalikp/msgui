﻿using System.Collections.Generic;

namespace MSDesktop.MailService
{
    /// <summary>
    /// Represents and email message to be used with MSDesktop email service.
    /// </summary>
    public sealed class MsDesktopMailMessage
    {
        private readonly List<MsDesktopMailAttachment> _attachments = new List<MsDesktopMailAttachment>();
        private readonly List<string> _toRecipientsAddesses = new List<string>(1);
        private readonly List<string> _ccRecipientsAddresses = new List<string>();
        public bool IsBodyHtml = false;

        internal MsDesktopMailMessage(MailServiceConfig config)
        {
            if (config.DefaultTo != null)
                _toRecipientsAddesses.AddRange(config.DefaultTo);

            if (!string.IsNullOrEmpty(config.DefaultFrom))
                Sender = config.DefaultFrom;

            if (config.DefaultCc != null)
                _ccRecipientsAddresses.AddRange(config.DefaultCc);

            if (!string.IsNullOrEmpty(config.DefaultSubject))
                Subject = config.DefaultSubject;
        }

        public string Sender { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }

        /// <summary>
        /// Retrieves the collection of attachments of this email message.
        /// </summary>
        public IEnumerable<MsDesktopMailAttachment> Attachments
        {
            get { return _attachments; }
        }

        /// <summary>
        /// Retrieves the email addresses of To recipients of the message.
        /// </summary>
        public IEnumerable<string> ToRecipientsAddesses
        {
            get { return _toRecipientsAddesses; }
        }

        /// <summary>
        /// Retrieves the email addresses of CC recipients of the message.
        /// </summary>
        public IEnumerable<string> CcRecipientsAddresses
        {
            get { return _ccRecipientsAddresses; }
        }

        /// <summary>
        /// Adds a To recipient email address to this message.
        /// </summary>
        /// <param name="recipientEmail">Email address to add.</param>
        public void AddToRecipient(string recipientEmail)
        {
            _toRecipientsAddesses.Add(recipientEmail);
        }

        /// <summary>
        /// Adds a CC recipient email address to this message.
        /// </summary>
        /// <param name="recipientEmail">Email address to add.</param>
        public void AddCcRecipient(string recipientEmail)
        {
            _ccRecipientsAddresses.Add(recipientEmail);
        }

        /// <summary>
        /// Adds an attachment to the email message.
        /// </summary>
        /// <param name="attachment">Attachment to add.</param>
        public void AddAttachment(MsDesktopMailAttachment attachment)
        {
            _attachments.Add(attachment);
        }
    }
}
