﻿
namespace MSDesktop.MailService
{
    /// <summary>
    /// Represents an email attachment used with MS Desktop Mail service
    /// </summary>
    public sealed class MsDesktopMailAttachment
    {
        private readonly string _name;
        private readonly byte[] _contents;

        /// <summary>
        /// Construct the object representing an attachment.
        /// </summary>
        /// <param name="name">Attachment file name.</param>
        /// <param name="contents">Binary content of the attachment.</param>
        public MsDesktopMailAttachment(string name, byte[] contents)
        {
            _contents = contents;
            _name = name;
        }

        /// <summary>
        /// Retrieves the file name of the attachment.
        /// </summary>
        public string Name
        {
            get { return _name; }
        }

        /// <summary>
        /// Retrieves the vinary content of the attachment.
        /// </summary>
        public byte[] Contents
        {
            get { return _contents; }
        }
    }
}
