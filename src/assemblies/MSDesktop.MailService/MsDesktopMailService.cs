﻿using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using MailMessage = System.Net.Mail.MailMessage;

namespace MSDesktop.MailService
{
    /// <summary>
    /// Represents the email service for MSDesktop-based applications.
    /// </summary>
    internal sealed class MsDesktopMailService : IMailService
    {
        private readonly MailServiceConfig _config;

        internal MsDesktopMailService(MailServiceConfig config)
        {
            _config = config;
        }

        /// <summary>
        /// Retrieves the name of this mail service.
        /// </summary>
        public string Name
        {
            get { return _config.Name; }   
        }

        /// <summary>
        /// Creates a new mail message, with fields pre-filled based on mail service configuration.
        /// </summary>
        /// <returns></returns>
        public MsDesktopMailMessage CreateMailMessage()
        {
            return new MsDesktopMailMessage(_config);
        }

        /// <summary>
        /// Sends the specified mail message.
        /// </summary>
        /// <param name="message"></param>
        public void Send(MsDesktopMailMessage message)
        {
            // Create the new mail message
            using (var smtpClientMessage = new MailMessage())
            using (var client = new SmtpClient(_config.SmtpServer, _config.SmtpPort))
            {
                // Populate the message attributes
                smtpClientMessage.From = new MailAddress(message.Sender);
                smtpClientMessage.IsBodyHtml = message.IsBodyHtml;

                foreach (var address in message.ToRecipientsAddesses)
                    smtpClientMessage.To.Add(new MailAddress(address));

                foreach (var address in message.CcRecipientsAddresses)
                    smtpClientMessage.CC.Add(new MailAddress(address));

                smtpClientMessage.Body = message.Body;
                smtpClientMessage.Subject = message.Subject;

                // Transfer the attachment information
                var attachments = new List<SmtpAttachment>();
                try
                {
                    foreach (var attachment in message.Attachments)
                    {
                        var stream = new MemoryStream(attachment.Contents, false);
                        var smtpAttachment = new Attachment(stream, attachment.Name);
                        attachments.Add(new SmtpAttachment(stream, smtpAttachment));
                        smtpClientMessage.Attachments.Add(smtpAttachment);
                    }

                    client.Send(smtpClientMessage);
                }
                finally
                {
                    // Dispose of all the created objects
                    foreach (var disposableAttachment in attachments)
                    {
                        disposableAttachment.Attachment.Dispose();
                        disposableAttachment.Stream.Dispose();
                    }
                }

            }
        }

        private struct SmtpAttachment
        {
            private readonly Attachment _attachment;
            private readonly Stream _stream;

            public SmtpAttachment(Stream stream, Attachment attachment)
            {
                _stream = stream;
                _attachment = attachment;
            }

            public Attachment Attachment
            {
                get { return _attachment; }
            }

            public Stream Stream
            {
                get { return _stream; }
            }
        }
    }
}
