﻿using System.Collections.Generic;

namespace MSDesktop.MailService
{
    public class MailServiceConfig
    {
        private readonly string _name;
        private readonly string _smtpServer;
        private readonly int _smtpPort;
        private readonly string _defaultFrom;
        private readonly List<string> _defaultTo = new List<string>();
        private readonly List<string> _defaultCc = new List<string>();
        private readonly string _defaultSubject;

        internal MailServiceConfig(string name, string smtpServer, int smtpPort = 25, string defaultFrom = null,
                                   IEnumerable<string> defaultTo = null, IEnumerable<string> defaultCc = null,
                                   string defaultSubject = null)
        {
            _name = name;
            _smtpServer = smtpServer;
            _smtpPort = smtpPort;
            _defaultFrom = defaultFrom;
            _defaultSubject = defaultSubject;

            if (defaultTo != null)
                DefaultTo.AddRange(defaultTo);

            if (defaultCc != null)
                DefaultCc.AddRange(defaultCc);
        }

        public string SmtpServer
        {
            get { return _smtpServer; }
        }

        public string DefaultFrom
        {
            get { return _defaultFrom; }
        }

        public List<string> DefaultTo
        {
            get { return _defaultTo; }
        }

        public List<string> DefaultCc
        {
            get { return _defaultCc; }
        }

        public string DefaultSubject
        {
            get { return _defaultSubject; }
        }

        public string Name
        {
            get { return _name; }
        }

        public int SmtpPort
        {
            get { return _smtpPort; }
        }
    }
}
