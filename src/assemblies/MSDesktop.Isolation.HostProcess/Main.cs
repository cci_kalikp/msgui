﻿using System;

namespace MSDesktop.Isolation
{
    internal class Program
	{
		[STAThread]
		public static void Main(string[] args)
		{
            ProcessMain.IsolatedProcessMain(args);
		}
	}
}