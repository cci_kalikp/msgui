﻿using System.ServiceModel;
using MSDesktop.Isolation.Interfaces;

namespace MSDesktop.Isolation
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, UseSynchronizationContext = false)]
    internal sealed class ProcessAddIn : IXProcProcessAddIn
    {
        private volatile IIsolatedSubsystemStarter _starter;

        public void ShutDown()
        {
            if (_starter != null)
            {
                _starter.Shutdown();
            }
        }

        public void ProceedWithInitialization(long profileFileHandle, int fileLength, string profileName)
        {
            _starter.ProceedWithInitialization(profileFileHandle, fileLength, profileName);
        }

        public void SetStarter(IIsolatedSubsystemStarter starter)
        {
            _starter = starter;
        }

        public void ReloadProfile(string currentProfile, long layoutFileHandle, int layoutFileSize)
        {
            _starter.ReloadProfile(currentProfile, layoutFileHandle, layoutFileSize);
        }

        public void CreateWindow(string factoryId, string viewId)
        {
            _starter.CreateWindow(factoryId, viewId);
        }
    }
}
