﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.ServiceModel;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using System.Xml.Linq;
using Infragistics.Windows.Ribbon;
using MSDesktop.Isolation.Interfaces;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Win32;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.Factories;
using MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging;
using MorganStanley.MSDotNet.MSGui.Impl.ProcessIsolation;
using MSDesktop.Isolation.Proxies;

namespace MSDesktop.Isolation
{
    /// <summary>
    /// This class is used for subsystem isolation functionality internally and is not intended to be used from your code.
    /// Its instances are instantiated in the context of the isolated process and are used to communicate with the host.
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    internal sealed class UiAddIn : IXProcUiAddIn, IKeyboardInputSite, IMSDesktopModule
    {
        private static readonly DispatcherWorkerThread WorkerThread = new DispatcherWorkerThread();

        private readonly IMSDesktopSubsystemSite _site;
        private readonly IFormatter _formatter;
        private readonly IChromeElementInitializerRegistry _chromeRegistry;
        private readonly string _factoryId;
        private readonly IWindowViewContainer _container;

        private volatile IXProcWindowSite _hostSite;
        private HwndSource _hwndSource; // child window containing the add-in control
        private FrameworkElement _frameworkElement;

        public event EventHandler<MessageArrivedEventArgs> MessageArrived;

        internal UiAddIn(FrameworkElement userControl, IChromeElementInitializerRegistry chromeRegistry,
                         string factoryId, IWindowViewContainer container)
            : this(null, userControl, chromeRegistry, factoryId)
        {
            _container = container;
        }

        internal UiAddIn(IMSDesktopSubsystemSite site, FrameworkElement userControl,
                         IChromeElementInitializerRegistry chromeRegistry, string factoryId)
        {
            _factoryId = factoryId;
            _chromeRegistry = chromeRegistry;
            _formatter = new BinaryFormatter();
            _frameworkElement = userControl;
            _site = site;
        }

        public void Initialize()
        {
            /*
			Create a window to host the add-in content. The OS allows a window from one process to be parented
			by a window from another. Essentially, native windows let us stitch UI between the two processes.
			(A WPF element tree is contained within an AppDomain and runs all on a single thread.) 
			We have to run a message loop around this window and help with key forwarding and tabbing into&out. 
        
			Keyboard input handling generally works better if MSGs straight off the Win32 message loop are
			"preprocessed", before passing them to DefWinowProc(). This helps with correct handling of
			various accelerators, in particular with some ActiveX controls (like the one WebBrowser wraps),
			and avoids occasional IME translation mishaps. WPF directly exposes its message loop via the
			ComponentDispatcher.ThreadPreprocessMessage event. When HwndSource owns a top-level window,
			it subscribes itself to ThreadPreprocessMessage and does all the correct routing internally
			(largely via its IKeyboardInputSink implementation). But we need a child window for the add-in.
			The trick is to first initialize the HwndSource as a top-level window, while keeping the window
			invisible, and then immediately convert it to a child one. Fortunately, the native window manager
			supports this well.
			*/

            // var isolatedInstance = _userControlToActivate != null ? _container.Resolve(_userControlToActivate) : null;
            // var control = isolatedInstance as UserControl;

            if (_frameworkElement != null)
            {
                // We have an isolated view here
                // _frameworkElement = control;
                var hwndSource = CreateHwndSource(_frameworkElement);
                _hwndSource = hwndSource;

                // Enable tabbing out of the add-in. (It is possible to keep Tab cycle within but let only Ctrl+Tab
                // move the focus out.)
                KeyboardNavigation.SetTabNavigation(_frameworkElement, KeyboardNavigationMode.Continue);
                KeyboardNavigation.SetControlTabNavigation(_frameworkElement, KeyboardNavigationMode.Continue);
                ((IKeyboardInputSink) hwndSource).KeyboardInputSite = this;

                // This message loop hook facilitates "bubbling" unhandled keys to the host. We critically rely
                // on HwndSource having already registered its hook so that this one is invoked last.
                ComponentDispatcher.ThreadPreprocessMessage += ThreadPreprocessMessage;

                if (_site != null)
                {
                    ThreadPool.QueueUserWorkItem(o =>
                        {
                            var addInUri =
                                ProcessIsolationUtility.RegisterServerChannel<IXProcUiAddIn>(
                                    ChromeManagerProxy.BaseServerName, this);
                            var addInSiteUri = _site.AddWindow(addInUri, _hwndSource.Handle.ToInt64(), null, string.Empty, null);
                            _hostSite = ProcessIsolationUtility.InitClientChannel<IXProcWindowSite>(addInSiteUri);
                        }); // We're ready!
                }
            }
        }

        internal UserControl IsolatedControl
        {
            set { _frameworkElement = value; }
        }

        private static HwndSource CreateHwndSource(FrameworkElement control)
        {
            var hwsp = new HwndSourceParameters("MSDotNet.MSGui.Impl.Isolation") {WindowStyle = 0};
            var hwndSource = new HwndSource(hwsp) {SizeToContent = SizeToContent.Manual};

            FrameworkElement controlElement;
            var window = control as Window;
            if (window != null)
            {
                var actualContent = window.Content as FrameworkElement;
                window.Content = null;

                controlElement = new ContentControl
                    {
                        Content = actualContent,
                        Name = window.Name,
                    };

                window.Close();
            }
            else
            {
                controlElement = control;
            }

            Win32.ConvertToChildWindow(hwndSource.Handle);
            hwndSource.RootVisual = controlElement;
            return hwndSource;
        }

        public void SubscribeV2V(Type messageType)
        {
            _hostSite.SubscribeV2V(_container.GetId(), messageType.FullName);
        }

        public void SetRibbonButtonSize(string size)
        {
            // We should have our button's container in _frameworkElement. 
            var sizingMode = (RibbonToolSizingMode)Enum.Parse(typeof(RibbonToolSizingMode), size);
            _frameworkElement.Dispatcher.Invoke(() =>
                {
                    var buttonControl = ((StackPanel) _frameworkElement).Children[0] as ButtonTool;
                    if (buttonControl != null)
                    {
                        object boxedEnumValue = sizingMode;

                        // The goal here is to mimic the behavior of the ribbon. Ribbon can set the property 
                        // since it has required authorization key. Reflect that key out - it's kept in internal variable 
                        // in Infgragistics DLL
                        var fieldInfo = typeof (RibbonToolHelper).GetField("SizingModePropertyKey",
                                                                           BindingFlags.GetField | BindingFlags.Static |
                                                                           BindingFlags.NonPublic);

                        if (fieldInfo != null)
                        {
                            var value = fieldInfo.GetValue(null);
                            buttonControl.SetValue((DependencyPropertyKey)value, boxedEnumValue);

                            buttonControl.SetValue(
                                RibbonGroupPanel.SizingModeVersionProperty,
                                1 + (int) buttonControl.GetValue(RibbonGroupPanel.SizingModeVersionProperty));
                        }
                    }
                });
        }
        
        /*
        public override object InitializeLifetimeService()
        {
            // null means a lease that doesn't expire. 
            // IXProcAddIn.ShutDown() will trigger cleanup and process exit.
            return null;
        }
        */

        #region IXProcAddIn Members

        public bool InvokeClosing()
        {
            var args = ((WindowViewContainer) _container).InvokeClosing();
            return args.Cancel;
        }

        public void InvokeClosed()
        {
            ((WindowViewContainer)_container).InvokeClosed();
        }

        // All calls to IXProcAddIn arrive on thread pool threads. Access to the HwndSource and its content has
        // to be on the HwndSource's thread. The easiest way to switch is via the Dispatcher. Unfortunately, 
        // this gets a little awkward to do repeadly and especially when it comes to getting results back. 
        // Alternately, this technique can be used to implement a thread-bound remotable object: 
        // http://shevaspace.blogspot.com/2007/03/aop-style-thread-synchronization.html

        void IXProcUiAddIn.Load(string stringdoc)
        {
            if (string.IsNullOrEmpty(stringdoc))
                return;
            //MessageBox.Show(stringdoc);
            try
            {

                var doc = XDocument.Load(new StringReader(stringdoc));
                if (_frameworkElement != null)
                {
                    //MessageBox.Show("Control is not null");
                    var load = _frameworkElement.GetType()
                                                .GetMethod("Load",
                                                           BindingFlags.FlattenHierarchy | BindingFlags.Instance |
                                                           BindingFlags.Public);

                    if (load != null)
                    {
                        //MessageBox.Show("Load is not null");
                        load.Invoke(_frameworkElement, new object[] {doc});
                    }
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.ToString());
            }

        }

        string IXProcUiAddIn.Save()
        {
            if (_frameworkElement != null)
            {
                var save = _frameworkElement.GetType()
                                            .GetMethod("Save",
                                                       BindingFlags.FlattenHierarchy | BindingFlags.Instance |
                                                       BindingFlags.Public);
                if (save != null)
                {
                    try
                    {
                        var obj = save.Invoke(_frameworkElement, new object[] {});
                        if (obj is XDocument)
                            return (obj as XDocument).ToString(SaveOptions.None);
                    }
                    catch (Exception exc)
                    {
                        MessageBox.Show(exc.ToString());
                    }
                }

                WindowFactoryHolder creator;
                if (_chromeRegistry.TryGetWindowFactoryHolder(_factoryId, out creator))
                {
                    // Compose the isolated state from actual state of the window 
                    // and a factory ID element
                    XDocument newDocument;
                    if (creator.DehydrateHandler != null)
                    {
                        var internalState = creator.DehydrateHandler(_container);
                        newDocument = new XDocument(new XElement(
                                                        "IsolatedView",
                                                        new XAttribute("FactoryId", _factoryId),
                                                        internalState.Root));
                    }
                    else
                    {
                        newDocument = new XDocument(new XElement(
                                                        "IsolatedView",
                                                        new XAttribute("FactoryId", _factoryId)));
                    }

                    return newDocument.ToString();
                }
            }

            return null;
        }

        bool IXProcUiAddIn.TabInto(TraversalRequest request)
        {
            return
                (bool)
                _hwndSource.Dispatcher.Invoke(
                    (DispatcherOperationCallback)
                    ((tr) => ((IKeyboardInputSink) _hwndSource).TabInto((TraversalRequest) tr)), DispatcherPriority.Send,
                    request);
        }

        void IXProcUiAddIn.ShutDown()
        {
            //using (StreamWriter writer = new StreamWriter(@"c:\msde\smulovic\shutdown.txt"))
            //{ writer.WriteLine("In IXProcAddIn.ShutDown"); }
            if (_hwndSource != null)
            {
                _hwndSource.Dispatcher.Invoke(delegate
                    {
                        //using (StreamWriter writer = new StreamWriter(@"c:\msde\smulovic\shutdown2.txt"))
                        //{ writer.WriteLine("In IXProcAddIn.ShutDown"); }
                        _hwndSource.Dispatcher.InvokeShutdown(); // This will lead to process exit.
                        //using (StreamWriter writer = new StreamWriter(@"c:\msde\smulovic\shutdown3.txt"))
                        //{ writer.WriteLine("In IXProcAddIn.ShutDown"); }
                        //_hwndSource.Dispose();
                        //using (StreamWriter writer = new StreamWriter(@"c:\msde\smulovic\shutdown4.txt"))
                        //{ writer.WriteLine("In IXProcAddIn.ShutDown"); }
                        if (Application.Current != null)
                        {
                            Application.Current.Shutdown();
                        }
                        //using (StreamWriter writer = new StreamWriter(@"c:\msde\smulovic\shutdown5.txt"))
                        //{ writer.WriteLine("In IXProcAddIn.ShutDown"); }
                    });
            }
        }

        #endregion

        private static readonly uint[] KeyMsgsToBubble = new[]
            {
                Win32.WM_KEYDOWN, Win32.WM_KEYUP, Win32.WM_SYSKEYDOWN, Win32.WM_SYSKEYUP,
                Win32.WM_SYSCHAR, Win32.WM_SYSDEADCHAR
            };

        private void ThreadPreprocessMessage(ref MSG msg, ref bool handled)
        {
            var wm = unchecked((uint) msg.message);
            if (!handled && KeyMsgsToBubble.Contains(wm))
            {
                // Because in its handling of a key the host may want to call into the add-in or may trigger
                // a window message that needs to be dispatched synchronously to the add-in's window, we have
                // to keep the UI thread responsive to window messages. This is accomplished thanks to the 
                // semi-permeable managed blocking that the Dispatcher normally allows to happen in an STA.
                // (For deep background, see http://blogs.msdn.com/cbrumme/archive/2004/02/02/66219.aspx.)
                var callback = (DispatcherOperationCallback) (m => HostSite
                                                                       .TranslateAccelerator
                                                                       (new MsgProxy((MSG) m)));

                handled = (bool) WorkerThread.Dispatcher.Invoke(callback, msg);
            }
        }

        private IntPtr OnWindowMessage(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            return IntPtr.Zero;
        }

        void IKeyboardInputSite.Unregister()
        {
        }

        IKeyboardInputSink IKeyboardInputSite.Sink
        {
            get { return _hwndSource; }
        }

        public void Paint(byte[] backgroundImage)
        {
            // Deserialize the image
            BitmapSource frame;
            using (var stream = new MemoryStream(backgroundImage))
            {
                var decoder = new PngBitmapDecoder(stream, BitmapCreateOptions.None, BitmapCacheOption.OnLoad);
                frame = decoder.Frames.First();
            }

            _frameworkElement.Dispatcher.BeginInvoke(() =>
                {
                    var drawingVisual = new DrawingVisual();
                    using (var context = drawingVisual.RenderOpen())
                    {
                        var imageRect = new Rect(0, 0, _frameworkElement.RenderSize.Width,
                                                 _frameworkElement.RenderSize.Height);
                        context.DrawRectangle(new SolidColorBrush(Colors.LightBlue), null, imageRect);
                        context.DrawImage(frame, imageRect);
                    }

                    ((StackPanel) _frameworkElement).Background = new VisualBrush(drawingVisual);
                    _frameworkElement.InvalidateVisual();
                });
        }

        public IXProcWindowSite HostSite
        {
            get { return _hostSite; }
            internal set { _hostSite = value; }
        }

        internal IntPtr Handle
        {
            get { return _hwndSource.Handle; }
        }

        private object DispatcherInvokeCallback(object tr)
        {
            return HostSite.TabOut((TraversalRequest) tr);
        }

        bool IKeyboardInputSite.OnNoMoreTabStops(TraversalRequest request)
        {
            // Tabbing out implies focus change. WM_SETFOCUS (and possibly related messages) needs to be sent
            // synchronously to the add-in's window when the host claims focus. To enable dispatching of this
            // message, the UI thread has to be available. (Same issue as in ThreadPreprocessMessage().)
            return
                (bool) WorkerThread.Dispatcher.Invoke((DispatcherOperationCallback) DispatcherInvokeCallback, request);
        }

        public void Receive(byte[] payloadBytes)
        {
            object payload;
            using (var stream = new MemoryStream(payloadBytes))
            {
                payload = _formatter.Deserialize(stream);
            }

            MessageArrived(this, new MessageArrivedEventArgs(payload));
        }
    };
}

//namespace
