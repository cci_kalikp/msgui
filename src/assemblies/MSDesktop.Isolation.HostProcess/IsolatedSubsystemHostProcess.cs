﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.ServiceModel;
using MorganStanley.MSDotNet.Runtime;
using MSDesktop.Isolation.HostContracts;
using MSDesktop.Isolation.Interfaces;

namespace MSDesktop.Isolation
{
    /// <summary>
    /// Static class for starting the isolated subsystem in an arbitrary context.
    /// </summary>
    public static class IsolatedSubsystemProcess
    {
        /// <summary>
        /// Starts and bootstraps the isolated subsystem.
        /// </summary>
        /// <param name="settingsUri">Host process settings URI</param>
        public static void StartIsolatedSubsystem(string settingsUri)
        {
            // Register the channel for process add-in
            var processAddIn = new ProcessAddIn();
            var processAddinUri = ProcessIsolationUtility.RegisterServerChannel<IXProcProcessAddIn>(
                "IsolatedProcessAddIn",
                processAddIn);

            // Get the settings
            IsolatedSubsystemSettings settings;
            var channel = ProcessIsolationUtility.InitClientChannel<IIsolatedSubsystemSite>(settingsUri);
            try
            {
                settings = channel.GetIsolatedSubsystemSettings(Process.GetCurrentProcess().Id, settingsUri,
                                                                processAddinUri);
            }
            finally
            {
                ((IClientChannel) channel).Dispose();
            }

            if (settings.AssemblyResolutionFiles != null && settings.AssemblyResolutionFiles.Length > 0)
            {
                InitAr(settings.AssemblyResolutionFiles);
            }
            if (string.IsNullOrEmpty(settings.IsolatedSubsystemLocation))
            {
                settings.IsolatedSubsystemLocation = AppDomain.CurrentDomain.BaseDirectory;
            }

            var msdeConfigFiles = settings.IsAdditionalArRequired
                                      ? settings.AssemblyResolutionFiles
                                      : null;

            var starter = IsolatedSubsystemStarterFactory.CreateStarter(settings, msdeConfigFiles);
            processAddIn.SetStarter(starter);
            starter.Start(settingsUri, settings);
        }

        private static void InitAr(IEnumerable<string> files)
        {
            try
            {
                foreach (var line in files)
                {
                    AssemblyResolver.Load(line);
                }
            }
            catch (Exception exc)
            {
                Debug.WriteLine(exc.ToString());
            }
        }
    }
}
