﻿using System;
using System.Linq;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;
using MSDesktop.Isolation.Interfaces;
using MSDesktop.Isolation.Prism;
using MSDesktop.Isolation.Proxies;

namespace MSDesktop.Isolation
{
    internal interface IModuleHandler
    {
        void RegisterTypes();
        bool IsModule(Type type);
        void InitializeModule(object module, int moduleIndex, IsolatedSubsystemSettings settings);
    }

    internal static class ModuleHandler
    {
        private static IModuleHandler _instance;
        private static IModuleHandler Instance
        {
            get
            { 
                return _instance;
            }
        }

        public static void InitializeSubsystemCore(IUnityContainer unityContainer, HostContracts.IsolatedSubsystemSettings settings)
        {
            Instance.RegisterTypes();

            ProfileLoadUtility.InitializeStorage(unityContainer, settings);

            // Add persistors (isolated process implementation)
            AddPersistors(unityContainer);
        }

        public static bool IsModule(this Type type)
        {
            return Instance.IsModule(type);
        }

        public static bool IsSubsystem(this Type type)
        {
            return
                type.GetInterfaces()
                    .Any(t => t == typeof (ISubsystemModuleCollection) || t == typeof (ISubsystemModuleCollectionEx));
        }

        public static void Initialize(IUnityContainer container, bool usePrism)
        {
            BootstrapperBase.UsePrismAsInfrasturcture = usePrism;
            if(usePrism)
                _instance = new PrismHandler(container);
            else 
                _instance = new Composite.CompositeHandler(container);
        }

        public static void InitializeModule(object module, int moduleIndex, IsolatedSubsystemSettings settings)
        {
            Instance.InitializeModule(module, moduleIndex, settings);
        }

        private static void AddPersistors(IUnityContainer container)
        {
            // Add the view manager persistor
            var persistenceService = container.Resolve<IPersistenceService>();
            var viewManagerPersistor = new ViewManagerPersistor(container);
            persistenceService.AddPersistor(ViewManagerPersistor.PersistorName, viewManagerPersistor.LoadState, viewManagerPersistor.SaveState);

            // Add the IPC persistors
            var ipc = container.Resolve<InterProcessMessenger>();
            var localPersistor = container.Resolve<LocalConnectionsPersistor>();
            var extPersistor = new ExternalConnectionManager(ipc) as IPersistable;

            persistenceService.AddPersistor(localPersistor.PersistorId, localPersistor.LoadState, localPersistor.SaveState);
            persistenceService.AddPersistor(extPersistor.PersistorId, extPersistor.LoadState, extPersistor.SaveState);
        }
    }
}
