﻿using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MSDesktop.Isolation.UiAddIns;

namespace MSDesktop.Isolation.Proxies
{
    internal sealed class MarshalingPublisherProxy<TMessage> : IPublisher<TMessage>, ILocationHolder
    {
        private readonly ViewUiAddinBase _addIn;
        private readonly IFormatter _formatter = new BinaryFormatter();

        public MarshalingPublisherProxy(ViewUiAddinBase addIn)
        {
            _addIn = addIn;
        }

        public void Publish(TMessage message, CommunicationTargetFilter locationFilter = CommunicationTargetFilter.All,
                            params string[] location)
        {
            byte[] messageBytes;
            using (var stream = new MemoryStream())
            {
                _formatter.Serialize(stream, message);
                messageBytes = stream.GetBuffer();
            }

            _addIn.Send(messageBytes);
        }

        public CafGuiApplicationInfoProxy Application { get; set; }
        public string Tab { get; set; }
    }
}
