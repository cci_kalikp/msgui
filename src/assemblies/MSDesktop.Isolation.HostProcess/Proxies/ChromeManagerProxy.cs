﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.Factories;
using MorganStanley.MSDotNet.MSGui.Impl.ProcessIsolation;
using MSDesktop.Isolation.Interfaces;
using MSDesktop.Isolation.UiAddIns;
using IStatusBar = MorganStanley.MSDotNet.MSGui.Core.IStatusBar;

namespace MSDesktop.Isolation.Proxies
{
    internal sealed class ChromeManagerProxy : IChromeManager
    {
        internal const string BaseServerName = "WPFProcessAddIn";

        private readonly IDictionary<string, IWidgetViewContainer> _createdWidgets;
        private readonly IDictionary<string, ViewUiAddinBase> _uiAddins;
        private readonly IChromeElementInitializerRegistry _chromeRegistry;
        private readonly ObservableCollection<IWindowViewContainer> _createdWindows;
        internal readonly IMSDesktopSubsystemSite _site;
        private readonly IWidgetFactory _widgetFactory;
        private readonly IWindowFactory _windowFactory;
        private readonly Dispatcher _dispatcher;
        private readonly HostContracts.IsolatedSubsystemSettings _subsystemSettings;

        public ChromeManagerProxy(IChromeElementInitializerRegistry chromeRegistry, IMSDesktopSubsystemSite site,
                                  IWidgetFactory widgetFactory, IWindowFactory windowFactory, HostContracts.IsolatedSubsystemSettings subsystemSettings)
        {
            _createdWidgets = new Dictionary<string, IWidgetViewContainer>();
            _uiAddins = new Dictionary<string, ViewUiAddinBase>();
            _createdWindows = new ObservableCollection<IWindowViewContainer>();
            _windowFactory = windowFactory;
            _widgetFactory = widgetFactory;
            _site = site;
            _chromeRegistry = chromeRegistry;
            _dispatcher = Dispatcher.CurrentDispatcher;
            _subsystemSettings = subsystemSettings;
        }

        public IWindowViewContainer CreateWindow(string factoryId, InitialWindowParameters initialParameters)
        {
            var windowContainer = (IWindowViewContainer) _dispatcher.Invoke(
                    (Func<object, object>) CreateWindowProc,
                    new Tuple<string, InitialWindowParameters>(factoryId, initialParameters));

            return windowContainer;
        }

        private object CreateWindowProc(object obj)
        {
            var param = (Tuple<string, InitialWindowParameters>) obj;
            return CreateWindowCore(param.Item1, param.Item2);
        }

        private IWindowViewContainer CreateWindowCore(string factoryId, InitialWindowParameters initialParameters)
        {
            var placeableParams = initialParameters as InitialPlaceableWindowParameters;
            string viewId = null;
            if (placeableParams != null)
            {
                viewId = placeableParams.ViewId;
            }

            var windowConainer = _windowFactory.CreateWindowInstance(factoryId, initialParameters, viewId, true);
            PlaceContainerContents(windowConainer.ID, factoryId, initialParameters, windowConainer, () => ((IXProcUiAddIn)_uiAddins[windowConainer.ID]).Save());
            return windowConainer;
        }

        internal void PlaceContainerContents(string viewId, string factoryId, InitialWindowParameters initialParameters,
                                             IWindowViewContainer windowConainer, Func<string> stateGetter)
        {
            // Now, we handle the container just like an isolated view.
            var frameworkElement = (FrameworkElement) windowConainer.Content;

            // TODO (hrechkin): Just create the handle of the framework element.
            ViewUiAddinBase addin;
            if(!_uiAddins.TryGetValue(windowConainer.ID, out addin))
            {
                addin = new WindowUiAddIn(frameworkElement, _chromeRegistry, windowConainer, this);
                _uiAddins.Add(windowConainer.ID, addin);
            }

            // Even if the addin is early-created, it's still not initialized
            addin.Initialize();

            var initialParametersProxy = new InitialWindowParametersProxy
                {
                    ViewId = viewId,
                    FactoryId = factoryId,
                    EnforceSizeRestrictions = initialParameters.EnforceSizeRestrictions,
                    InitialLocation = (Enums.InitialLocation) (int) initialParameters.InitialLocation,
                    IsModal = initialParameters.IsModal,
                    ResizeMode = (Enums.ResizeMode) (int) initialParameters.ResizeMode,
                    ShowFlashBorder = initialParameters.ShowFlashBorder,
                    SingletonKey = initialParameters.SingletonKey,
                    SizingMethod = (Enums.SizingMethod) (int) initialParameters.SizingMethod,
                    Topmost = initialParameters.Topmost,
                    Transient = initialParameters.Transient,
                    UseDockManager = initialParameters.UseDockManager
                };

            _createdWindows.Add(windowConainer);

            var initialState = stateGetter != null ? stateGetter() : string.Empty;
            var closure = new PlaceContainerContentsClosure(_site, (WindowUiAddIn)addin, windowConainer, initialParametersProxy, initialState);
            ThreadPool.QueueUserWorkItem(PlaceContainerContentsProc, closure);
        }

        public IDialogWindow CreateDialog(string factoryId)
        {
            return null;
        }

        public IWidgetViewContainer PlaceWidget(string factoryId, string location,
                                                InitialWidgetParameters initialParameters)
        {
            return PlaceWidget(factoryId, null, location, initialParameters);
        }

        public ViewUiAddinBase GetOrCreateAddin(IWindowViewContainer container)
        {
            ViewUiAddinBase addin;
            if (!_uiAddins.TryGetValue(container.ID, out addin))
            {
                addin = new WindowUiAddIn(_chromeRegistry, container, this);
                _uiAddins.Add(container.ID, addin);
            }

            return addin;
        }

        public IWidgetViewContainer PlaceWidget(string factoryId, string root, string location,
                                                InitialWidgetParameters initialParameters)
        {
            var addin = CreateControlRemoteAddin(factoryId, initialParameters);

            // Adjust the location
            var ribbonPath = location;
            var hostRibbonPath = _subsystemSettings.SubsystemRibbonPath;
            if (!string.IsNullOrWhiteSpace(hostRibbonPath))
            {
                // Go over the parts of the path
                var hostPathParts = hostRibbonPath.Split('/');
                var locationParts = location.Split('/');

                ribbonPath = string.Empty;

                var minLength = Math.Max(hostPathParts.Length, locationParts.Length);
                for (var k = 0; k < minLength; k++)
                {
                    if (k < hostPathParts.Length)
                    {
                        ribbonPath += (hostPathParts[k] + '/');
                    }
                    else
                    {
                        ribbonPath += (locationParts[k] + '/');
                    }
                }

                ribbonPath = ribbonPath.Substring(0, ribbonPath.Length - 1);
            }

            // As usually, we try to drop the call across the wire to background threads.
            // We want to avoid deadlock with the host.
            var additionalData = GetSizingModeStringRepresentation(initialParameters);
            var info = new PlaceWidgetClosure(_site, addin, factoryId, addin.Handle, root, ribbonPath, addin.GetXaml(), null, additionalData);
            ThreadPool.QueueUserWorkItem(PlaceWidgetCallProc, info);
            return null;
        }

        private static void AddWidgetCallProc(object obj)
        {
            var closureInternal = (PlaceWidgetClosure) obj;
            var addInUri = ProcessIsolationUtility.RegisterServerChannel<IXProcUiAddIn>(BaseServerName,
                                                                                        closureInternal.Addin);
            var addInSiteUri = closureInternal.Site.AddWidget(addInUri, 
                                                              closureInternal.FactoryId,
                                                              closureInternal.Handle.ToInt64(),
                                                              closureInternal.Parent.FactoryID,
                                                              closureInternal.Xaml,
                                                              closureInternal.WidgetAdditionalData);
            closureInternal.Addin.HostSite =
                ProcessIsolationUtility.InitClientChannel<IXProcWindowSite>(addInSiteUri);
        }

        private static void PlaceWidgetCallProc(object obj)
        {
            var closureInternal = (PlaceWidgetClosure) obj;

            var addInUri = ProcessIsolationUtility.RegisterServerChannel<IXProcUiAddIn>(BaseServerName,
                                                                                        closureInternal.Addin);
            var addInSiteUri = closureInternal.Site.PlaceWidget(addInUri, 
                                                                closureInternal.FactoryId,
                                                                closureInternal.Handle.ToInt64(), 
                                                                closureInternal.Root,
                                                                closureInternal.RibbonPath,
                                                                closureInternal.Xaml,
                                                                closureInternal.WidgetAdditionalData);
            closureInternal.Addin.HostSite = ProcessIsolationUtility.InitClientChannel<IXProcWindowSite>(addInSiteUri);
        }

        public void CreateChrome(bool placeUnassigned)
        {
        }

        public IWidgetViewContainer GetWidget(string factoryId)
        {
            return null;
        }

        public IEnumerable<IWidgetViewContainer> GetWidgets(string factoryId)
        {
            return new List<IWidgetViewContainer>();
        }

        public void RemoveWidget(IWidgetViewContainer placedWidget)
        {
        }

        public void RemoveWidget(string factoryId)
        {
        }

        public void ClearWidget(IWidgetViewContainer placedWidget)
        {
        }
         
        public ObservableWindowCollection Windows
        {
            get
            { 
                return new ObservableWindowCollection(_createdWindows);
            }
        }

         
        public IEnumerable<IWindowViewContainer> GetViews()
        {
            return _createdWindows;
        }

        public event EventHandler LayoutLoaded;
        public event EventHandler<WindowEventArgs> WindowActivated;
        public event EventHandler<WindowEventArgs> WindowDeactivated;
        public event EventHandler<CancellableWindowEventArgs> WindowDeactivating;
        public event EventHandler ApplicationClosed;
        public event EventHandler ProfileProcessingComplete;
        public event EventHandler BeginProfileProcessing;

        public IWidgetViewContainer this[string index]
        {
            get { return null; }
        }

        public IWidgetViewContainer AddWidget(string factoryId, InitialWidgetParameters parameters,
                                              IWidgetViewContainer parent)
        {
            var addin = CreateControlRemoteAddin(factoryId, parameters);
            var buttonSizeData = GetSizingModeStringRepresentation(parameters);
            var closure = new PlaceWidgetClosure(_site, addin, factoryId, addin.Handle, null, null, addin.GetXaml(),
                                                 parent, buttonSizeData);

            ThreadPool.QueueUserWorkItem(AddWidgetCallProc, closure);
            return null;
        }

        private static string GetSizingModeStringRepresentation(InitialWidgetParameters parameters)
        {
            string buttonSizeString = null;
            var buttonParameters = parameters as InitialButtonParametersBase;
            if (buttonParameters != null)
            {
                switch (buttonParameters.Size)
                {
                    case ButtonSize.Large:
                        buttonSizeString = "ImageAndTextLarge";
                        break;
                    case ButtonSize.Small:
                        buttonSizeString = "ImageAndTextNormal";
                        break;
                }
            }

            return buttonSizeString;
        }

        private ControlUiAddIn CreateControlRemoteAddin(string factoryId, InitialWidgetParameters parameters)
        {
            // Create the container and the FrameworkElement using the factory
            IWidgetViewContainer widget;
            if (!_createdWidgets.TryGetValue(factoryId, out widget))
            {
                // Create new widget instance
                widget = _widgetFactory.CreateWidgetInstance(factoryId, parameters);
                InitializeEmptyWidget(parameters, widget);
                _createdWidgets.Add(factoryId, widget);
            }
            else
            {
                // Clone the existing widget
                var actualWidgetParameters = widget.Parameters;
                var clonedWidget = _widgetFactory.CreateWidgetInstance(factoryId, actualWidgetParameters);
                InitializeEmptyWidget(actualWidgetParameters, clonedWidget);

                widget = clonedWidget;
            }

            var addin = new ControlUiAddIn((FrameworkElement)widget.Content, _subsystemSettings.ShellMode);
            addin.Initialize();
            return addin;
        }

        internal void FireLayoutLoaded()
        {
            var handler = LayoutLoaded;
            if (handler != null)
            {
                handler(this, null);
            }
        }

        private void InitializeEmptyWidget(InitialWidgetParameters parameters, IWidgetViewContainer widget)
        {
            WidgetFactoryHolder factoryHolder;
            var parametersType = parameters != null ? parameters.GetType() : typeof (InitialWidgetParameters);
            if (_chromeRegistry.TryGetWidgetFactoryHolder(parametersType, out factoryHolder) ||
                _chromeRegistry.TryGetWidgetFactoryHolder(widget.FactoryID, out factoryHolder))
            {
                factoryHolder.BuildUp()(widget, null);
            }
        }

        public IRibbon Ribbon
        {
            get { return null; }
        }

        public ITabbedDock Tabwell
        {
            get { return null; }
        }

        public IStatusBar StatusBar
        {
            get { return null; }
        }

        public ITitleBar TitleBar
        {
            get { return null; }
        }

        public event EventHandler<DragOrDropOnWindowEventArgs> DropOnShell;
        public event EventHandler<DragOrDropOnWindowEventArgs> PreviewDropOnShell;
        public event EventHandler<DragOrDropOnWindowEventArgs> DragEnterOnShell;
        public event EventHandler<DragOrDropOnWindowEventArgs> DragOverOnShell;
        public event EventHandler<DragOrDropOnWindowEventArgs> DragLeaveOnShell;
        public event EventHandler<DragOrDropOnWindowEventArgs> PreviewDragEnterOnShell;
        public event EventHandler<DragOrDropOnWindowEventArgs> PreviewDragOverOnShell;
        public event EventHandler<DragOrDropOnWindowEventArgs> PreviewDragLeaveOnShell;

        //todo: support it later on 

        public IWorkspaces Workspaces
        {
            get { return null; }
        }

        private static void PlaceContainerContentsProc(object obj)
        {
            var closureInternal = (PlaceContainerContentsClosure)obj;
            var uiAddIn = closureInternal.AddIn;
            IXProcUiAddIn xprocUiAddIn = uiAddIn;

            var addInUri = ProcessIsolationUtility.RegisterServerChannel(BaseServerName, xprocUiAddIn);
            var initialState = closureInternal.InitialState;
            var addInSiteUri = closureInternal.Site.AddWindow(addInUri, uiAddIn.Handle.ToInt64(),
                                                              closureInternal.InitialParameters,
                                                              closureInternal.Container.Title,
                                                              initialState); 
            uiAddIn.HostSite = ProcessIsolationUtility.InitClientChannel<IXProcWindowSite>(addInSiteUri);
            var window = closureInternal.Container as WindowViewContainer;
            if (window != null)
            {
                window.InvokeCreated();
            }

        }

        /// <summary>
        /// We need this immutable class to properly marshal calls between the threads, since closures don't do this properly.
        /// </summary>
        private sealed class PlaceContainerContentsClosure
        {
            private readonly IMSDesktopSubsystemSite _site;
            private readonly WindowUiAddIn _addIn;
            private readonly IWindowViewContainer _container;
            private readonly InitialWindowParametersProxy _initialParameters;
            private readonly string _initialState;

            internal PlaceContainerContentsClosure(IMSDesktopSubsystemSite site, WindowUiAddIn addIn,
                                                   IWindowViewContainer container,
                                                   InitialWindowParametersProxy initialParameters, 
                                                   string initialState)
            {
                _initialState = initialState;
                _initialParameters = initialParameters;
                _container = container;
                _addIn = addIn;
                _site = site;
            }

            public IMSDesktopSubsystemSite Site
            {
                get { return _site; }
            }

            public WindowUiAddIn AddIn
            {
                get { return _addIn; }
            }

            public IWindowViewContainer Container
            {
                get { return _container; }
            }

            public InitialWindowParametersProxy InitialParameters
            {
                get { return _initialParameters; }
            }

            public string InitialState
            {
                get { return _initialState; }
            }
        }

        /// <summary>
        /// We need this immutable class to properly marshal calls between the threads, since closures don't do this properly.
        /// </summary>
        private sealed class PlaceWidgetClosure
        {
            private readonly UiAddinBase _addin;
            private readonly string _factoryId;
            private readonly IntPtr _handle;
            private readonly string _root;
            private readonly string _ribbonPath;
            private readonly string _xaml;
            private readonly IMSDesktopSubsystemSite _site;
            private readonly IWidgetViewContainer _parent;
            private readonly string _widgetAdditionalData;

            internal PlaceWidgetClosure(IMSDesktopSubsystemSite site, UiAddinBase addin, string factoryId, IntPtr handle,
                                        string root,
                                        string ribbonPath, string xaml, IWidgetViewContainer parent, string widgetAdditionalData)
            {
                _widgetAdditionalData = widgetAdditionalData;
                _parent = parent;
                _site = site;
                _xaml = xaml;
                _ribbonPath = ribbonPath;
                _root = root;
                _handle = handle;
                _factoryId = factoryId;
                _addin = addin;
            }

            public string FactoryId
            {
                get { return _factoryId; }
            }

            public UiAddinBase Addin
            {
                get { return _addin; }
            }

            public IntPtr Handle
            {
                get { return _handle; }
            }

            public string Root
            {
                get { return _root; }
            }

            public string RibbonPath
            {
                get { return _ribbonPath; }
            }

            public string Xaml
            {
                get { return _xaml; }
            }

            public IMSDesktopSubsystemSite Site
            {
                get { return _site; }
            }

            public IWidgetViewContainer Parent
            {
                get { return _parent; }
            }

            public string WidgetAdditionalData
            {
                get { return _widgetAdditionalData; }
            }
        }

        internal void RemoveAddIn(WindowUiAddIn windowUiAddIn)
        {
            var key = _uiAddins.Keys.First(id => _uiAddins[id] == windowUiAddIn);
            _uiAddins.Remove(key);
            _createdWindows.Remove(windowUiAddIn.ViewContainer);
        }
    }

    public static class ChromeManagerProxyExtensions
    {
        public static void AddRemoteShowWindowButton(this IChromeManager _, string widgetFactoryId, string text, string windowFactoryId,
            string root, string location)
        {
            var cmp = _ as ChromeManagerProxy;

            if (cmp == null)
                throw new InvalidOperationException("AddRemoteShowWindowButton is only supported from an isolated module");

            cmp._site.AddRemoteShowWindowButton(widgetFactoryId, text, windowFactoryId, root, location);
        }
    }

}
