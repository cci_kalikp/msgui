﻿using MorganStanley.MSDotNet.MSGui.Core;

namespace MSDesktop.Isolation.Proxies
{
    internal sealed class StatusBarItemProxy : IStatusBarItem
    {
        public void UpdateStatus(StatusLevel level_, string statusText_)
        {
        }

        public void ClearStatus()
        {
        }

        public object Control
        {
            get { return null; }
        }

        public event StatusUpdatedEventHandler StatusUpdated;
    }
}
