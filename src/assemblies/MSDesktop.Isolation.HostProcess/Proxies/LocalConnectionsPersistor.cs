﻿using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;

namespace MSDesktop.Isolation.Proxies
{
    internal sealed class LocalConnectionsPersistor : IPersistable
    {
        private XDocument _state;

        public void LoadState(XDocument state_)
        {
            _state = state_;
        }

        public XDocument SaveState()
        {
            return new XDocument();
        }

        public string PersistorId 
        {
            get { return typeof (IpcLocalConnectionManager).FullName; }
        }

        public XDocument State
        {
            get { return _state; }
        }
    }
}
