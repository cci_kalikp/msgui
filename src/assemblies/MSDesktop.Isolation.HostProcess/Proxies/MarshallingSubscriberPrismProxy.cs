﻿using System;
using System.Reactive.Linq;
using Microsoft.Practices.Prism.Events;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MSDesktop.Isolation.UiAddIns;

namespace MSDesktop.Isolation.Proxies
{
    internal sealed class MarshalingSubscriberPrismProxy<TMessage> : ISubscriber<TMessage>, ILocationHolder
    {
        private readonly ViewUiAddinBase _addin;
        private readonly IEventAggregator _eventAggregator;
        private readonly CompositePresentationEvent<TMessage> _msgArriveEvent;
        private IObservable<TMessage> _observable;

        public MarshalingSubscriberPrismProxy(ViewUiAddinBase addin)
        {
            _addin = addin;
            _eventAggregator = new EventAggregator();
            _msgArriveEvent = _eventAggregator.GetEvent<CompositePresentationEvent<TMessage>>();
            _addin.RegisterSubscriber(typeof(TMessage), Receive); // MessageArrived += _addin_MessageArrived;
        }

        public void Subscribe(Action<TMessage> messageHandler)
        {
            var observable = GetObservable();
            observable.Subscribe(messageHandler);
        }

        public IObservable<TMessage> GetObservable()
        {
            if (_observable == null)
            {
                _observable = Observable
                    .Create((Func<IObserver<TMessage>, Action>)GetObservableAction)
                    .Publish()
                    .RefCount();
            }

            return _observable;
        }

        public Type GetMessageType()
        {
            return typeof(TMessage);
        }

        public void Receive(object message)
        {
            _msgArriveEvent.Publish((TMessage)message);
        }

        public CafGuiApplicationInfoProxy Application { get; set; }
        public string Tab { get; set; }

        private Action GetObservableAction(IObserver<TMessage> observer)
        {
            SubscriptionToken token;
            lock (_msgArriveEvent)
            {
                token = _msgArriveEvent.Subscribe(observer.OnNext, true);
            }

            return () => _msgArriveEvent.Unsubscribe(token);
        }

        private void _addin_MessageArrived(object sender, MessageArrivedEventArgs e)
        {
            Receive(e.Message);
        }
    }
}
