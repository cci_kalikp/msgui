﻿using System;
using System.Reactive.Linq;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Presentation.Events;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MSDesktop.Isolation.UiAddIns;

namespace MSDesktop.Isolation.Proxies
{
    internal sealed class MarshalingSubscriberCompositeProxy<TMessage> : ISubscriber<TMessage>, ILocationHolder
    {
        private readonly ViewUiAddinBase _addin;
        private readonly IEventAggregator _eventAggregator;
        private readonly CompositePresentationEvent<TMessage> _msgArriveEvent;
        private readonly string _viewId;
        private IObservable<TMessage> _observable;

        public MarshalingSubscriberCompositeProxy(ViewUiAddinBase addin)
        {
            _addin = addin;
            _eventAggregator = new EventAggregator();
            _msgArriveEvent = _eventAggregator.GetEvent<CompositePresentationEvent<TMessage>>();
            _addin.RegisterSubscriber(typeof(TMessage), Receive);//  += _addin_MessageArrived;
        }

        internal MarshalingSubscriberCompositeProxy(ViewUiAddinBase addIn, string viewId)
            : this(addIn)
        {
            _viewId = viewId;
        }

        public void Subscribe(Action<TMessage> messageHandler)
        {
            var observable = GetObservable();
            observable.Subscribe(messageHandler);
        }

        public IObservable<TMessage> GetObservable()
        {
            if (_observable == null)
            {
                _observable = Observable
                    .Create((Func<IObserver<TMessage>, Action>) GetObservableAction)
                    .Publish()
                    .RefCount();
            }

            return _observable;
        }

        public Type GetMessageType()
        {
            return typeof(TMessage);
        }

        public void Receive(object message)
        {
            _msgArriveEvent.Publish((TMessage) message);
        }

        public CafGuiApplicationInfoProxy Application { get; set; }
        public string Tab { get; set; }

        private Action GetObservableAction(IObserver<TMessage> observer)
        {
            SubscriptionToken token;
            lock (_msgArriveEvent)
            {
                token = _msgArriveEvent.Subscribe(observer.OnNext, true);
            }

            if (!string.IsNullOrEmpty(_viewId))
            {
                // Pass the subscribe call to addin.
                _addin.SubscribeV2V(_viewId, typeof(TMessage).AssemblyQualifiedName);
            }

            return () => _msgArriveEvent.Unsubscribe(token);
        }

        private void _addin_MessageArrived(object sender, MessageArrivedEventArgs e)
        {
            Receive(e.Message);
        }
    }
}
