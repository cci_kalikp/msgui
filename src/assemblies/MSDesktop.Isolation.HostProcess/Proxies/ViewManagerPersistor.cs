﻿using System.Linq;
using System.Xml;
using System.Xml.Linq;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.ViewModels;

namespace MSDesktop.Isolation.Proxies
{
    internal sealed class ViewManagerPersistor
    {
        private const string ViewStateElementName = "ViewState";
        private const string StateElementName = "State";
        
        private readonly IUnityContainer _container;

        public ViewManagerPersistor(IUnityContainer container)
        {
            _container = container;
        }

        internal const string PersistorName = "MSGui.Internal.ViewManager";

        public void LoadState(XDocument state)
        {
            const string panesElementName = "Panes";
             
            var mainElement = state.Element(TabbedDockViewModel.NODE_NAME) ?? state.Element(FloatingOnlyDockViewModel.NODE_NAME);
            if (mainElement != null) //tabdocked view
            {
                var tabsElement = mainElement.Element("Tabs");
                var tabsNodes = tabsElement != null ? tabsElement.Elements("Tab") : new XElement[] { };

                // Process Tabs node. Each Tab node will have Panes node - those are docked panes.
                foreach (var tabNode in tabsNodes)
                {
                    var dockedPanesNode = tabNode.Element(panesElementName);
                    if (dockedPanesNode != null)
                    {
                        ProcessPanesNode(dockedPanesNode);
                    }
                }

                // Process separate Panes element. Those will be floating panes.
                var floatingPanesNode = mainElement.Element(panesElementName);
                ProcessPanesNode(floatingPanesNode); 
            }
        }

        private void ProcessPanesNode(XElement panesNode)
        {
            foreach (var node in panesNode.Elements())
            {
                // Take apart attiributes of the node
                if (node.Name == "IView")
                {
                    // Parse the ID attribute
                    var idAttribute = node.Attribute("Id");
                    var viewId = string.Empty;
                    var creatorId = string.Empty;
                    if (idAttribute != null && !string.IsNullOrEmpty(idAttribute.Value))
                    {
                        viewId = idAttribute.Value;
                    }

                    // Parse the CreatorID attribute
                    var creatorIdAttr = node.Attribute("CreatorID");
                    if (creatorIdAttr != null && !string.IsNullOrEmpty(creatorIdAttr.Value))
                    {
                        creatorId = creatorIdAttr.Value;
                    }

                    // viewState
                    var viewState = node.Element(ViewStateElementName) ?? new XElement(ViewStateElementName);

                    // stateToPass
                    XDocument stateToPass = null;
                    var stateElem = node.Element(StateElementName) ?? new XElement(StateElementName);

                    var root = stateElem.Elements().FirstOrDefault(e => e.NodeType == XmlNodeType.Element);
                    if (root != null)
                    {
                        stateToPass = new XDocument(root);
                    }

                    var moduleGroup = _container.Resolve<IModuleGroup>();
                    moduleGroup.CreateWindowFromLayout(
                        creatorId,
                        new InitialWindowParameters(),
                        viewId,
                        stateToPass,
                        new XDocument(viewState),
                        v => { });
                }
            }
        }

        public XDocument SaveState()
        {
            return new XDocument();
        }

        public void LoadIsolatedModuleState(XDocument state)
        {
            var originalFactoryId = state.Root.Attribute("FactoryId").ToString();

            var originalContent = state.Root.Ancestors().FirstOrDefault();
            if (originalContent != null && !originalContent.IsEmpty)
            {
                
            }
        }
    }
}
