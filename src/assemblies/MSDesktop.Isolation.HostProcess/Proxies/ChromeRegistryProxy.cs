﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.Factories;

namespace MSDesktop.Isolation.Proxies
{
    internal sealed class ChromeRegistryProxy : IChromeRegistry, IChromeElementInitializerRegistry
    {
        private readonly Dictionary<string, WindowFactoryHolder> _windowHandlerRegistrations = new Dictionary<string, WindowFactoryHolder>();
        private readonly Dictionary<string, WidgetFactoryHolder> _widgetHandlerRegistrations = new Dictionary<string, WidgetFactoryHolder>();
        private readonly Dictionary<Type, WidgetFactoryHolder> _parameterTypeHandlerRegistrations = new Dictionary<Type, WidgetFactoryHolder>();

        private readonly ObservableCollection<string> _windowFactoryIDs = new ObservableCollection<string>();
        private readonly ObservableCollection<string> _widgetFactoryIDs = new ObservableCollection<string>();

        public void RegisterFactory(string ID, InitializeWindowHandler initWindowHandler, DehydrateWindowHandler dehydrateWindowHandler, InitializeWidgetHandler initWidgetHandler, DehydrateWidgetHandler dehydrateWidgetHandler)
        {
        }

        public void RegisterWindowFactory(string ID, InitializeWindowHandler initHandler,
                                                          DehydrateWindowHandler dehydrateHandler)
        {
            this.RegisterWindowFactory(ID).SetInitHandler(initHandler).SetDehydrateHandler(dehydrateHandler);
        }

        public IWindowFactoryHolder RegisterWindowFactory(string ID)
        {
            var wfh = new WindowFactoryHolder{ Name = ID };
            _windowHandlerRegistrations[ID] = wfh;
            _windowFactoryIDs.Add(ID);
            return wfh;
        }


        public void RegisterDialogFactory(string ID, InitializeDialogHandler initHandler)
        {
        }


        public IWidgetFactoryHolder RegisterWidgetFactory(string ID)
        {
            var wfh = new WidgetFactoryHolder { Name = ID };
            _widgetHandlerRegistrations[ID] = wfh;
            _widgetFactoryIDs.Add(ID);
            return wfh;

        }
        public void RegisterWidgetFactory(string ID, InitializeWidgetHandler initHandler = null,
                                          DehydrateWidgetHandler dehydrateHandler = null)
        {
            RegisterWidgetFactory(ID).SetInitHandler(initHandler).SetDehydrateHandler(dehydrateHandler);
        }

        public void RegisterGlobalStoredWidgetFactory(string ID, InitializeWidgetHandler initHandler = null,
                                                      DehydrateWidgetHandler dehydrateHandler = null)
        {
            RegisterWidgetFactory(ID).SetInitHandler(initHandler).SetDehydrateHandler(dehydrateHandler, true);
        }


        public IWidgetFactoryHolder RegisterWidgetFactoryMapping<T>() where T : InitialWidgetParameters
        {
            var holder = new WidgetFactoryHolder();
            _parameterTypeHandlerRegistrations[typeof(T)] = holder;
            return holder;
        }

        public void RegisterWidgetFactoryMapping<T>(InitializeWidgetHandler factory) where T : InitialWidgetParameters
        {
            RegisterWidgetFactoryMapping<T>().SetInitHandler(factory);
        }
 

        public void RegisterWidgetDehydrateCallbackMapping<T>(DehydrateWidgetHandler callback) where T : InitialWidgetParameters
        {
            RegisterWidgetFactoryMapping<T>().SetDehydrateHandler(callback);
        }

        public bool TryGetWidgetFactoryHolder(Type t, out WidgetFactoryHolder factory)
        {
            return _parameterTypeHandlerRegistrations.TryGetValue(t, out factory);
        }

        public bool TryGetWindowFactoryHolder(string factoryId, out WindowFactoryHolder factory)
        {
            return _windowHandlerRegistrations.TryGetValue(factoryId, out factory);
        }

        public bool TryGetWidgetFactoryHolder(string factoryId, out WidgetFactoryHolder factory)
        {
            return _widgetHandlerRegistrations.TryGetValue(factoryId, out factory);
        }

        public void RegisterWindowFactoryMapping<F, T>()
            where F : InitialWindowParameters
            where T : F
        {  
            // Not implemented.
        }

        public bool TryGetWindowFactoryHolder(Type t, out WindowFactoryHolder factory)
        {
            // Not implemented.
            factory = null;
            return false;
        }
        public void ChainWindowFactoryMapping<T>(InitializeWindowHandlerStep extraHandler_) where T : InitialWindowParameters
        {
            // Not implemented.
        }
        public ReadOnlyObservableCollection<string> WindowFactoryIDs
        {
            get { return new ReadOnlyObservableCollection<string>(_windowFactoryIDs); }
        }

        public ReadOnlyObservableCollection<string> WidgetFactoryIDs
        {
            get { return new ReadOnlyObservableCollection<string>(_widgetFactoryIDs); }
        }
    }
}
