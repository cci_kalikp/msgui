﻿using System;
using System.Collections.Generic;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;

namespace MSDesktop.Isolation.Proxies
{
    internal sealed class IsolatedV2VConnectionManager : IV2VConnectionManager
    {
        public void SignUpAutoHook(IWindowViewContainer viewContainer,
                                   Action<IEnumerable<IWindowViewContainer>> hookupCallback,
                                   Action<IEnumerable<IWindowViewContainer>> unhookedCallback)
        {
        }

        public void SignOffAutoHook(IWindowViewContainer viewContainer)
        {
        }

        public AutoHookMode AutoHookMode
        {
            set { }
        }
    }
}
