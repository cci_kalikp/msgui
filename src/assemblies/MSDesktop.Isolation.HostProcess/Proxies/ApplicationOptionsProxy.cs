﻿using System.Threading;
using System.Windows;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.ProcessIsolation;
using MSDesktop.Isolation.Interfaces;
using MSDesktop.Isolation.UiAddIns;

namespace MSDesktop.Isolation.Proxies
{
    internal sealed class ApplicationOptionsProxy : IApplicationOptions
    {
        private readonly IMSDesktopSubsystemSite _site;

        public ApplicationOptionsProxy(IMSDesktopSubsystemSite site)
        {
            _site = site;
        }

        public void AddOptionPage(string path, string title, IOptionView view)
        {
            var uiElement = view.Content;
            var addin = new OptionPageUiAddIn((FrameworkElement) uiElement, view);
            addin.Initialize();

            var closure = new AddOptionPageClosure(path, addin, _site, title);
            ThreadPool.QueueUserWorkItem(AddOptionsPageProc, closure);
        }

        public void AddViewModelBasedOptionPage<TViewModel>(string path, string title, bool global) where TViewModel : IOptionViewViewModel
        {
        }

        public void RemoveOptionPage(string path, string title)
        {
        }
         
        public void AddOptionPage(string path, string title, IOptionView view, int priority)
        {
            throw new System.NotImplementedException();
        }

        public void AddViewModelBasedOptionPage<TViewModel>(string path, string title, bool global, int priority) where TViewModel : IOptionViewViewModel
        {
            throw new System.NotImplementedException();
        }

        private static void AddOptionsPageProc(object obj)
        {
            var closure = (AddOptionPageClosure) obj;

            var addInUri =
                ProcessIsolationUtility.RegisterServerChannel<IXProcUiAddIn>(ChromeManagerProxy.BaseServerName, closure.Addin);

            closure.SubsystemSite.AddOptionsPage(closure.Path, closure.Title, addInUri, closure.Addin.Handle.ToInt64());
            
        }

        private sealed class AddOptionPageClosure
        {
            private readonly string _path;
            private readonly UiAddinBase _addin;
            private readonly string _title;
            private readonly IMSDesktopSubsystemSite _subsystemSite;

            public AddOptionPageClosure(string path, UiAddinBase addin, IMSDesktopSubsystemSite subsystemSite, string title)
            {
                _subsystemSite = subsystemSite;
                _title = title;
                _addin = addin;
                _path = path;
            }

            public string Path
            {
                get { return _path; }
            }

            public UiAddinBase Addin
            {
                get { return _addin; }
            }

            public string Title
            {
                get { return _title; }
            }

            public IMSDesktopSubsystemSite SubsystemSite
            {
                get { return _subsystemSite; }
            }
        }
    }
}
