﻿using System;
using System.Collections.Generic;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MSDesktop.Isolation.Proxies
{
    internal sealed class StatusBarProxy : IStatusBar
    {
        private readonly HashSet<string> _keys = new HashSet<string>();
        private readonly StatusBarItemProxy _mainStatusBarItem = new StatusBarItemProxy();

        public ICollection<string> Keys
        {
            get { return _keys; }
        }

        public IStatusBarItem MainItem
        {
            get { return _mainStatusBarItem; }
        }

        public IStatusBarItem this[string statusBarItemKey]
        {
            get { throw new NotImplementedException(); }
        }

        public void AddItem(string key, IStatusBarItem item, StatusBarItemAlignment alignment)
        {
        }

        public string AddItem(IStatusBarItem item, StatusBarItemAlignment alignment)
        {
            return string.Empty;
        }

        public void AddTextStatusItem(string key, StatusBarItemAlignment alignment)
        {
        }

        public string AddTextStatusItem(StatusBarItemAlignment alignment)
        {
            return string.Empty;
        }

        public void AddProgressBarStatusItem(string key, double from, double to, StatusBarItemAlignment alignment)
        {
        }

        public string AddProgressBarStatusItem(double from, double to, StatusBarItemAlignment alignment)
        {
            return string.Empty;
        }

        public void AddSeparator(string key, StatusBarItemAlignment alignment)
        {
        }

        public string AddSeparator(StatusBarItemAlignment alignment)
        {
            return string.Empty;
        }

        public void RemoveItem(string key)
        {
        }

        public bool ContainsItem(string key)
        {
            return false;
        }
    }
}
