﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using MSDesktop.Isolation.UiAddIns;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;

namespace MSDesktop.Isolation.Proxies
{
    internal sealed class EventRegistratorProxy : IEventRegistrator
    {
        private readonly IMessageTypeRegistrator _typeRegistrator;
        private readonly ChromeManagerProxy _chromeManager;
        private readonly IDictionary<string, object> _registeredPublishers = new ConcurrentDictionary<string, object>();
        private readonly IDictionary<string, object> _registeredSubscribers = new ConcurrentDictionary<string, object>();

        public EventRegistratorProxy(IChromeManager chromeManager, IMessageTypeRegistrator typeRegistrator)
        {
            _typeRegistrator = typeRegistrator;
            _chromeManager = (ChromeManagerProxy) chromeManager;
        }

        public ISubscriber<TMessage> RegisterSubscriber<TMessage>(IViewContainerBase viewContainer)
        {
            return RegisterSubscriber<TMessage>(viewContainer, false);
        }

        public ISubscriber<TMessage> RegisterSubscriber<TMessage>(IViewContainerBase viewContainer,
                                                                  bool receiveLastMessage)
        {
            _typeRegistrator.RegisterType<TMessage>();

            var viewId = viewContainer.GetId();
            var uiAddin = _chromeManager.GetOrCreateAddin((IWindowViewContainer)viewContainer);

            // TODO (hrechkin): We support registration of IWindowViewContainer-based subscribers
            var subscriber = new MarshalingSubscriberCompositeProxy<TMessage>(uiAddin, viewId);

            var windowViewContainer = viewContainer as IWindowViewContainer;
            if (windowViewContainer != null)
            {
                _registeredSubscribers[windowViewContainer.ID] = subscriber;
            }

            return subscriber;
        }

        public bool UnregisterSubscriber<TMessage>(ISubscriber<TMessage> subscriber)
        {
            throw new NotImplementedException();
        }

        public bool UnregisterSubscriber<TMessage>(IViewContainerBase viewContainer)
        {
            throw new NotImplementedException();
        }

        public IPublisher<TMessage> RegisterPublisher<TMessage>(IViewContainerBase viewContainer)
        {
            _typeRegistrator.RegisterType<TMessage>();

            var viewId = viewContainer.GetId();
            var uiAddin = _chromeManager.GetOrCreateAddin((IWindowViewContainer) viewContainer);

            var publisher = new MarshalingPublisherProxy<TMessage>(uiAddin);

            // TODO (hrechkin): We support registration of IWindowViewContainer-based subscribers
            var windowViewContainer = viewContainer as IWindowViewContainer;
            if (windowViewContainer != null)
            {
                _registeredPublishers[windowViewContainer.ID] =publisher;
            }

            // Register this view as the publisher with the host
            uiAddin.RegisterAsPublisher(viewId, typeof (TMessage).AssemblyQualifiedName);

            return publisher;
        }

        public bool UnregisterPublisher<TMessage>(IPublisher<TMessage> publisher)
        {
            throw new NotImplementedException();
        }

        public bool UnregisterPublisher<TMessage>(IViewContainerBase viewContainer)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<string> GetCompatiblePublishers<TMessage>()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<string> GetCompatibleSubscribers<TMessage>()
        {
            throw new NotImplementedException();
        }

        public ICollection<Type> GetSubscribedTypes()
        {
            throw new NotImplementedException();
        }

        public ICollection<Type> GetPublishedTypes()
        {
            throw new NotImplementedException();
        }

        public IPublisher<TMessage> GetRegisteredPublisher<TMessage>(IViewContainerBase viewContainer)
        {
            var windowViewContainer = viewContainer as IWindowViewContainer;
            if (windowViewContainer != null)
            {
                return (IPublisher<TMessage>) _registeredPublishers[windowViewContainer.ID];
            }

            throw new InvalidOperationException(
                "We support registration of publishers and subscribers for Views only in isolated processes");
        }

        public ISubscriber<TMessage> GetRegisteredSubscriber<TMessage>(IViewContainerBase viewContainer)
        {
            var windowViewContainer = viewContainer as IWindowViewContainer;
            if (windowViewContainer != null)
            {
                return (ISubscriber<TMessage>) _registeredSubscribers[windowViewContainer.ID];
            }

            throw new InvalidOperationException(
                "We support registration of publishers and subscribers for Views only in isolated processes");
        }

        public void Connect<TMessage1, TMessage2>(IViewContainerBase publishContainer,
                                                  IViewContainerBase subscribeContainer)
        {
            throw new NotImplementedException();
        }

        public void Connect<TMessage1, TMessage2>(string publishContainer, string subscribeContainer)
        {
            throw new NotImplementedException();
        }

        public void Disconnect<TMessage1, TMessage2>(IViewContainerBase publishContainer,
                                                     IViewContainerBase subscribeContainer)
        {
            throw new NotImplementedException();
        }

        public void Disconnect<TMessage1, TMessage2>(string publishViewId, string subscribeViewId)
        {
            throw new NotImplementedException();
        }

        public ISubscriber<TMessage> RegisterMarshalingSubscriber<TMessage>(IViewContainerBase viewContainer,
                                                                            object addinHost)
        {
            throw new NotImplementedException();
        }

        public IPublisher<TMessage> RegisterMarshalingPublisher<TMessage>(IViewContainerBase viewContainer,
                                                                          object addinHost)
        {
            throw new NotImplementedException();
        }

        public IAdapterService AdapterService
        {
            get { return null; }
        }
    }
}
