﻿using System.IO;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MSDesktop.Isolation.Proxies
{
    internal sealed class IsolatedPersistenceStorage : IPersistenceStorage
    {
        private XDocument _layout;

        public void SaveState(XDocument state, string name)
        {
        }

        public XDocument LoadState(string name)
        {
            return _layout;
        }

        public void DeleteState(string name)
        {
        }

        public void RenameState(string oldName, string newName)
        {
        }

        public ReadOnlyProfilesCollection AvailableProfiles
        {
            get { return new ReadOnlyProfilesCollection(new ProfilesCollection()); }
        }

        public void InitializeStorage()
        {
        }

        public void Apply()
        {
        }

        public string DefaultName { get; set; }

        internal void SetLayout(string layout)
        {
            using (var reader = new StringReader(layout))
            {
                _layout = XDocument.Load(reader);
            }
        }
    }
}
