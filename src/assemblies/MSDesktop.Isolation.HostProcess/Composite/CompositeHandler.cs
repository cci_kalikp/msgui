﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using MSDesktop.Isolation.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Core.Persistence.DelayedLoading;
using MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence.DelayedLoading;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication.Composite;
using MSDesktop.Isolation.Proxies;
using MSDesktop.MessageRouting;
using MSDesktop.MessageRouting.GUI.Modules;

namespace MSDesktop.Isolation.Composite
{
    internal abstract class HandlerBase : IModuleHandler
    {
        private readonly IUnityContainer _container;

        protected HandlerBase(IUnityContainer container)
        {
            _container = container;
        }

        protected IUnityContainer Container
        {
            get { return _container; }
        }

        public virtual void RegisterTypes()
        {
            _container.RegisterType<IEventAggregator, EventAggregator>(new ContainerControlledLifetimeManager());

            var eventAggregatorWrapper = new EventAggregatorWrapper(_container.Resolve<IEventAggregator>());
            var msDesktopEventAggregator = new EventAggregatorRegistrator(eventAggregatorWrapper);
            _container.RegisterInstance<IMSDesktopEventAggregator>(msDesktopEventAggregator);
            _container.RegisterInstance<IIPCEventAggregator>(msDesktopEventAggregator);

            _container.RegisterInstance<IEventAggregator>(new MSDesktopEventAggregatorWrapper(msDesktopEventAggregator));

            _container.RegisterType<ICommunicator, Communicator>(new ContainerControlledLifetimeManager());
            IPCCompositePresentationEventContainerHolder.Container = _container;

            var persistenceService = new PersistenceService();
            _container.RegisterInstance<IPersistenceService>(persistenceService,
                                                             new ContainerControlledLifetimeManager());
            _container.RegisterInstance<IItemsDictionaryService>(persistenceService,
                                                                 new ContainerControlledLifetimeManager());

            var concordStrage = new IsolatedPersistenceStorage();
            _container.RegisterInstance<IPersistenceStorage>(concordStrage, new ContainerControlledLifetimeManager());
            _container.RegisterInstance(concordStrage, new ContainerControlledLifetimeManager());

            _container.RegisterType<IStatusBar, StatusBarProxy>(new ContainerControlledLifetimeManager());
            _container.RegisterType<PersistenceProfileService, PersistenceProfileService>(
                new ContainerControlledLifetimeManager());
            _container.RegisterType<IAdapterService, ViewMessageAdapterService>(
                new ContainerControlledLifetimeManager());
            _container.RegisterType<IMessageTypeRegistrator, MessageTypeRegistrator>(
                new ContainerControlledLifetimeManager());
            _container.RegisterType<IDelayedProfileLoader, DelayedProfileLoader>(
                new ContainerControlledLifetimeManager());

            _container.RegisterType<LocalConnectionsPersistor>(new ContainerControlledLifetimeManager());
            _container.RegisterType<IFormatter, CustomizedFormatter>(new ContainerControlledLifetimeManager());
            _container.RegisterType<IV2VConnectionManager, IsolatedV2VConnectionManager>(
                new ContainerControlledLifetimeManager());
            _container.RegisterType<RoutedCommunicationModule>(new ContainerControlledLifetimeManager());
            _container.RegisterType<IEventRegistrator, EventRegistratorProxy>(new ContainerControlledLifetimeManager());
            _container.RegisterType<MessageRoutingGuiModule>(new ContainerControlledLifetimeManager());
            _container.RegisterType<IApplicationOptions, ApplicationOptionsProxy>(
                new ContainerControlledLifetimeManager());
            _container.RegisterType<CommunicationAddIn>(new ContainerControlledLifetimeManager());
        }

        public abstract bool IsModule(Type type);

        public virtual void InitializeModule(object module, int moduleIndex, IsolatedSubsystemSettings settings)
        {
            // Prepare the communication addin and register it with the host
            var communicationAddIn = Container.Resolve<CommunicationAddIn>();
            communicationAddIn.Initialize();
        }
    }

    internal sealed class CompositeHandler : HandlerBase
    {
        public CompositeHandler(IUnityContainer container) : base(container)
        {
        }

        /*
        public void RegisterTypes()
        {
            _container.RegisterType<IEventAggregator, EventAggregator>(new ContainerControlledLifetimeManager());

            var eventAggregatorWrapper = new EventAggregatorWrapper(_container.Resolve<IEventAggregator>());
            var msDesktopEventAggregator = new EventAggregatorRegistrator(eventAggregatorWrapper);
            _container.RegisterInstance<IMSDesktopEventAggregator>(msDesktopEventAggregator);
            _container.RegisterInstance<IIPCEventAggregator>(msDesktopEventAggregator);

            _container.RegisterInstance<IEventAggregator>(new MSDesktopEventAggregatorWrapper(msDesktopEventAggregator));

            _container.RegisterType<ICommunicator, Communicator>(new ContainerControlledLifetimeManager());
            IPCCompositePresentationEventContainerHolder.Container = _container;

            var persistenceService = new PersistenceService();
            _container.RegisterInstance<IPersistenceService>(persistenceService,
                                                             new ContainerControlledLifetimeManager());
            _container.RegisterInstance<IItemsDictionaryService>(persistenceService,
                                                                 new ContainerControlledLifetimeManager());

            var concordStrage = new IsolatedPersistenceStorage();
            _container.RegisterInstance<IPersistenceStorage>(concordStrage, new ContainerControlledLifetimeManager());
            _container.RegisterInstance(concordStrage, new ContainerControlledLifetimeManager());

            _container.RegisterType<IStatusBar, StatusBarProxy>(new ContainerControlledLifetimeManager());
            _container.RegisterType<PersistenceProfileService, PersistenceProfileService>(
                new ContainerControlledLifetimeManager());
            _container.RegisterType<IAdapterService, ViewMessageAdapterService>(
                new ContainerControlledLifetimeManager());
            _container.RegisterType<IMessageTypeRegistrator, MessageTypeRegistrator>(
                new ContainerControlledLifetimeManager());
            _container.RegisterType<IDelayedProfileLoader, DelayedProfileLoader>(
                new ContainerControlledLifetimeManager());

            _container.RegisterType<LocalConnectionsPersistor>(new ContainerControlledLifetimeManager());
            _container.RegisterType<IFormatter, CustomizedFormatter>(new ContainerControlledLifetimeManager());
            _container.RegisterType<RoutedCommunicationModule>(new ContainerControlledLifetimeManager());
            _container.RegisterType<IEventRegistrator, InterProcessMessenger>(new ContainerControlledLifetimeManager());
            _container.RegisterType<MessageRoutingGuiModule>(new ContainerControlledLifetimeManager());
            _container.RegisterType<IApplicationOptions, ApplicationOptionsProxy>(
                new ContainerControlledLifetimeManager());
            _container.RegisterType<CommunicationAddIn>(new ContainerControlledLifetimeManager());
        }
         */

        public override bool IsModule(Type type)
        {
            var interfaces = type.GetInterfaces();
            return interfaces.Contains(typeof (IModule)) || interfaces.Contains(typeof (IMSDesktopModule));
        }

        public override void InitializeModule(object module, int moduleIndex, IsolatedSubsystemSettings settings)
        {
            base.InitializeModule(module, moduleIndex, settings);
            var moduleWithExtraInformation = module as IModuleWithExtraInformation;

            if (moduleWithExtraInformation != null)
            {
                moduleWithExtraInformation.SetExtraInformation(moduleIndex, settings);
            }
            // We can have our module implementing either IModule or IMSDesktopModule
            var unityModule = module as IModule;
            if (unityModule != null)
            {
                unityModule.Initialize();
                return;
            }

            var msdesktopModule = module as IMSDesktopModule;
            if (msdesktopModule != null)
            {
                msdesktopModule.Initialize();
                return;
            }

            throw new InvalidOperationException(
                "Composite handler can initialize only Unity's IModule or IMSDesktopModule-based modules");
        }
    }
}
