﻿using System;
using System.Linq;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MSDesktop.Isolation.Composite;
using MSDesktop.Isolation.Interfaces;

namespace MSDesktop.Isolation.Prism
{
    internal sealed class PrismHandler : HandlerBase
    {
        public PrismHandler(IUnityContainer container) : base(container)
        {
        }

        public override bool IsModule(Type type)
        {
            var interfaces = type.GetInterfaces();
            return interfaces.Contains(typeof(IModule)) || interfaces.Contains(typeof(IMSDesktopModule));
        }

        public override void InitializeModule(object module, int moduleIndex, Proxies.IsolatedSubsystemSettings settings)
        {
            // Initialize the communication addin
            // Prepare the communication addin and register it with the host
            base.InitializeModule(module, moduleIndex, settings);
            var moduleWithExtraInformation = module as IModuleWithExtraInformation;
            if (moduleWithExtraInformation != null)
            {
                moduleWithExtraInformation.SetExtraInformation(moduleIndex, settings);
            }

            // We can have our module implementing either IModule or IMSDesktopModule
            var prismModule = module as IModule;
            if (prismModule != null)
            {
                prismModule.Initialize();
                return;
            }

            var msdesktopModule = module as IMSDesktopModule;
            if (msdesktopModule != null)
            {
                msdesktopModule.Initialize();
                return;
            }

            throw new InvalidOperationException(
                "Composite handler can initialize only Prism's IModule or IMSDesktopModule-based modules");
 
        }
    }
}
