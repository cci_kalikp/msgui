﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.ServiceModel;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging;
using MorganStanley.MSDotNet.MSGui.Impl.ProcessIsolation;
using MSDesktop.Isolation.Interfaces;
using MSDesktop.Isolation.Proxies;

namespace MSDesktop.Isolation
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, UseSynchronizationContext = false)]
    internal sealed class CommunicationAddIn : IXProcCommunicationAddIn
    {
        private readonly IFormatter _formatter;
        private readonly ICommunicator _communicator;
        private readonly ConcurrentDictionary<Type, object> _publishers = new ConcurrentDictionary<Type, object>();

        public string Url { get; private set; }

        public CommunicationAddIn(ICommunicator communicator)
        {
            _communicator = communicator;
            _formatter = new BinaryFormatter();
        }

        public void Initialize()
        {
            Url = ProcessIsolationUtility.RegisterServerChannel<IXProcCommunicationAddIn>(
                ChromeManagerProxy.BaseServerName, 
                this);

            _communicator.EnableIpcImc(AppEnvironment.All);
        }

        public void Receive(byte[] payloadBytes)
        {
            object payload;
            using (var stream = new MemoryStream(payloadBytes))
            {
                payload = _formatter.Deserialize(stream);
            }

            // We need a publisher for the message's type. 
            var messageType = payload.GetType();
            var publisher = _publishers.GetOrAdd(
                messageType,
                mt => MessagingReflectionUtilities.GetPublisher(_communicator, messageType));

            MessagingReflectionUtilities.Publish/*FromIsolationHost*/(publisher, payload);
        }
    }
}
