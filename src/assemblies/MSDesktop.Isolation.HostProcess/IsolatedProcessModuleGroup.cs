﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Linq;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Persistence.DelayedLoading;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.Factories;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity;
using MorganStanley.MSDotNet.MSGui.Impl.ProcessIsolation;
using MSDesktop.Isolation.Interfaces;
using MSDesktop.Isolation.Proxies;

namespace MSDesktop.Isolation
{
    internal sealed class IsolatedProcessModuleGroup : ModuleGroupBase
    {
        private readonly object _communicationAddinLock = new object();
        private readonly IChromeElementInitializerRegistry _chromeRegistry;
        private readonly IWindowFactory _windowFactory;
        private readonly ChromeManagerProxy _chromeManager;
        private readonly IMSDesktopSubsystemSite _subsystemSite;
        private readonly LocalConnectionsPersistor _persistor;
        private readonly IFormatter _formatter;

        private volatile CommunicationAddIn _communicationAddIn;

        public IsolatedProcessModuleGroup(
            IUnityContainer container,
            IChromeElementInitializerRegistry chromeRegistry,
            IWindowFactory windowFactory,
            IChromeManager chromeManager,
            IMSDesktopSubsystemSite subsystemSite,
            LocalConnectionsPersistor persistor)
            : base(container)
        {
            _formatter = new BinaryFormatter();
            _persistor = persistor;
            _subsystemSite = subsystemSite;
            _chromeManager = chromeManager as ChromeManagerProxy;
            _windowFactory = windowFactory;
            _chromeRegistry = chromeRegistry;
        }

        public event EventHandler DelayedModuleInitialized;

        private CommunicationAddIn CommunicationAddIn
        {
            get
            {
                if (_communicationAddIn == null)
                {
                    lock (_communicationAddinLock)
                    {
                        if (_communicationAddIn == null)
                        {
                            var communicationAddIn = Container.Resolve<CommunicationAddIn>();
                            communicationAddIn.Initialize();
                            _communicationAddIn = communicationAddIn;
                        }
                    }
                }

                return _communicationAddIn;
            }
        }

        private void OnDelayedModuleInitialized()
        {
            var handler = DelayedModuleInitialized;
            if (handler != null) handler(this, EventArgs.Empty);
        }

        public override void CreateWindowFromLayout(string factoryId, InitialWindowParameters initialParameters, string viewId,
                                           XDocument stateToPass, XDocument viewState, Action<IWindowViewContainer> postCreateHandler)
        {
            // Transfer the call here
            WindowFactoryHolder factoryHolder;

            // Just like save, we treat IsolatedWindowFactory window factory specially
            // TODO (hrechkin): Unify this construction and save logic, so that IsolatedWindwoFactory isn't a special case
            if (factoryId.Equals(ProcessIsolationExtensions.IsolatedProcessWindowFactoryName, StringComparison.OrdinalIgnoreCase) && stateToPass != null)
            {
                var originalFactoryId = stateToPass.Root.Attribute("FactoryId").Value;
                var originalContent = stateToPass.Root.Elements().FirstOrDefault();

                // Replace the variables
                factoryId = originalFactoryId;

                stateToPass = (originalContent != null && !originalContent.IsEmpty)
                                  ? new XDocument(originalContent)
                                  : new XDocument();
            }

            if (_chromeRegistry.TryGetWindowFactoryHolder(factoryId, out factoryHolder))
            {
                var windowViewContainer = _windowFactory.CreateWindowInstance(factoryId, initialParameters, viewId, true,
                                                                              stateToPass, viewState);

                _chromeManager.PlaceContainerContents(viewId, factoryId, initialParameters, windowViewContainer, () => stateToPass == null ? string.Empty : stateToPass.ToString(SaveOptions.None));
            }
        }

        public override void RegisterDelayedLoadModule(IDelayedLoadModule module)
        {
            /* ignore */
        }

        public override void DelayedLoadModuleInitialized(IDelayedLoadModule module)
        {
            OnDelayedModuleInitialized();
        }

        public override void DelayedLoadModuleInitializationFailed(IDelayedLoadModule module)
        {
            /* ignore */
        }

        public override IWindowViewContainer CreateGroupWindow(string factoryId, InitialWindowParameters parameters)
        {
            /* ignore */
            return null;
        }

        public override void RegisterSubsystemWindowFactory(IRemoteWindowFactory remoteFactory, IEnumerable<string> windowFactories)
        {
            /* ignore */
        }

        public override void UnRegisterSubsystemWindowFactory(IEnumerable<string> windowFactories)
        {
            /* ignore */
        }

        public override bool ViewLoadedCompletely(XDocument connectionState)
        {
            // In the isolated process context, we get the connection state from LocalConnectionsPersistor
            string state = string.Empty;
            if (_persistor.State != null)
            {
                state = _persistor.State.ToString();
            }

            _subsystemSite.ViewLoadedCompletely(state);

            // TODO this should be the same logic as in HostProcessModuleGroup
            return true;
        }

        public override void SubscribeM2M(string messageType)
        {
            _subsystemSite.SubscribeM2M(messageType, CommunicationAddIn.Url);
        }

        public override void PublishM2M(object message)
        {
            byte[] messageBytes;
            using (var stream = new MemoryStream())
            {
                _formatter.Serialize(stream, message);
                messageBytes = stream.GetBuffer();
            }

            _subsystemSite.PublishM2M(messageBytes);
        }
    }
}
