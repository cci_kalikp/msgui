﻿using System;
using System.IO;
using System.Linq; 
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging; 
using MSDesktop.Isolation.AddInProxy;
using MSDesktop.Isolation.Enums;
using MorganStanley.MSDotNet.MSGui.Core.Win32;
using MSDesktop.Isolation.Controls;
using MSDesktop.Isolation.Utils; 

namespace MSDesktop.Isolation.HwndHosts
{
    internal class ControlHwndSource : HwndSource
    {
        public ControlHwndSource()
            : this(new HwndSourceParameters("MSDotNet.MSGui.Impl.Isolation")
                {
                    WindowStyle = 0
                })
        {
        }

        private ControlHwndSource(HwndSourceParameters parameters)
            : base(parameters)
        {
            SizeToContent = SizeToContent.WidthAndHeight;
        }

        public override Visual RootVisual
        {
            get { return base.RootVisual; }
            set
            {
                base.RootVisual = value;
                AddHook(WndProc);
            }
        }

        private IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            switch ((uint) msg)
            {
                case Win32.WM_MOUSEMOVE:

                    // Call TrackMouseEvent to receive this message again
                    var trackEvent = new Win32.TRACKMOUSEEVENT
                        {
                            dwFlags = (int) Win32.TrackMouseEventFalgs.TME_LEAVE,
                            hwndTrack = hwnd
                        };

                    Win32.TrackMouseEvent(trackEvent);
                    break;
                case Win32.WM_MOUSELEAVE:
                    var mousePos = System.Windows.Forms.Control.MousePosition;
                    var rect = new Win32.RECT();
                    Win32.GetWindowRect(hwnd, ref rect);

                    // Block the message if the mouse is inside the rectangle
                    if (mousePos.X > rect.Left && mousePos.X < rect.Right - 1 && mousePos.Y > rect.Top &&
                        mousePos.Y < rect.Bottom - 1)
                    {
                        handled = true;
                    }

                    break;
                case Win32.WM_WINDOWPOSCHANGING:
                    var windowPos = (Win32.WINDOWPOS) Marshal.PtrToStructure(lParam, typeof (Win32.WINDOWPOS));
                    var frameworkElement = RootVisual as IsolatedControlContainer;
                    if (frameworkElement != null && windowPos.cx > 0 && windowPos.cy > 0)
                    {
                        // Resize the element
                        frameworkElement.SetDesiredSize(new Size(windowPos.cx, windowPos.cy));
                        frameworkElement.InvalidateMeasure();
                        frameworkElement.InvalidateArrange();
                    }

                    break;
                case IsolationConstants.WmUserMessage:
                    handled = ProcessUserMessage(wParam, lParam);
                    break;
            }

            return IntPtr.Zero;
        }

        protected virtual bool ProcessUserMessage(IntPtr wParam, IntPtr lParam)
        {
            var operation = Win32.LOWORD(wParam);
            switch ((WindowMessageOperation) operation)
            { 
                case WindowMessageOperation.ControlBackgroundPaint:
                    HandleBackgroundRepaint(lParam, Win32.HIWORD(wParam));
                    return true;
                default:
                    return false;
            }
        }

        private void HandleBackgroundRepaint(IntPtr fileHandle, int imageSize)
        {
            byte[] imageBytes = CommunicationMappedFile.GetFileBytes(fileHandle, imageSize);
            Paint(imageBytes);
        }

     


        private void Paint(byte[] backgroundImage)
        {
            var frameworkElement = (IsolatedControlContainer)RootVisual;

            // Deserialize the image
            ImageSource frame;
            using (var stream = new MemoryStream(backgroundImage))
            {
                var decoder = new PngBitmapDecoder(stream, BitmapCreateOptions.None, BitmapCacheOption.OnLoad);
                frame = decoder.Frames.First();
            }

            var drawingVisual = new DrawingVisual();
            using (var context = drawingVisual.RenderOpen())
            {
                var imageRect = new Rect(0, 0, frameworkElement.RenderSize.Width,
                                         frameworkElement.RenderSize.Height);
                context.DrawRectangle(new SolidColorBrush(Colors.LightBlue), null, imageRect);
                context.DrawImage(frame, imageRect);
            }

            frameworkElement.Background = new VisualBrush(drawingVisual);
            frameworkElement.InvalidateVisual();
        }
    }
}
