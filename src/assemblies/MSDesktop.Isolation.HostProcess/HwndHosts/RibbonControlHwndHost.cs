﻿using System; 
using System.Reflection; 
using System.Windows;
using Infragistics.Windows.Ribbon;
using MSDesktop.Isolation.Controls;
using MSDesktop.Isolation.Enums;
using MorganStanley.MSDotNet.MSGui.Core.Win32;
using RibbonToolSizingMode = MSDesktop.Isolation.Enums.RibbonToolSizingMode;

namespace MSDesktop.Isolation.HwndHosts
{
    class RibbonControlHwndHost:ControlHwndSource
    {
        protected override bool ProcessUserMessage(IntPtr wParam, IntPtr lParam)
        { 
            var operation = (WindowMessageOperation)Win32.LOWORD(wParam);
            if (operation == WindowMessageOperation.SetButtonSizingMode)
            {
                SetButtonSizingMode((RibbonToolSizingMode)lParam.ToInt32());
                return true;
            }
            return base.ProcessUserMessage(wParam, lParam);
        }
        private void SetButtonSizingMode(RibbonToolSizingMode mode)
        {
            var buttonElement = ((IsolatedControlContainer)RootVisual).Children[0];
            var actualButtonSize = ToInfragisticsSizingMode(mode);
            SetButtonSizeInternal(actualButtonSize, buttonElement);
        }


        private static Infragistics.Windows.Ribbon.RibbonToolSizingMode ToInfragisticsSizingMode(RibbonToolSizingMode proxyMode)
        {
            switch (proxyMode)
            {
                case RibbonToolSizingMode.ImageAndTextLarge:
                    return Infragistics.Windows.Ribbon.RibbonToolSizingMode.ImageAndTextLarge;
                case RibbonToolSizingMode.ImageAndTextNormal:
                    return Infragistics.Windows.Ribbon.RibbonToolSizingMode.ImageAndTextNormal;
                case RibbonToolSizingMode.ImageOnly:
                    return Infragistics.Windows.Ribbon.RibbonToolSizingMode.ImageOnly;
            }

            return Infragistics.Windows.Ribbon.RibbonToolSizingMode.ImageAndTextLarge;
        }

        private static void SetButtonSizeInternal(Infragistics.Windows.Ribbon.RibbonToolSizingMode sizingMode, UIElement buttonElement)
        {
            // var sizingMode = (Infragistics.Windows.Ribbon.RibbonToolSizingMode)Enum.Parse(typeof(Infragistics.Windows.Ribbon.RibbonToolSizingMode), size);
            var buttonControl = buttonElement as ButtonTool;
            if (buttonControl != null)
            {
                object boxedEnumValue = sizingMode;

                // The goal here is to mimic the behavior of the ribbon. Ribbon can set the property 
                // since it has required authorization key. Reflect that key out - it's kept in internal variable 
                // in Infgragistics DLL
                var fieldInfo = typeof(RibbonToolHelper).GetField("SizingModePropertyKey",
                                                                   BindingFlags.GetField | BindingFlags.Static |
                                                                   BindingFlags.NonPublic);

                if (fieldInfo != null)
                {
                    var value = fieldInfo.GetValue(null);
                    buttonControl.SetValue((DependencyPropertyKey)value, boxedEnumValue);

                    buttonControl.SetValue(
                        RibbonGroupPanel.SizingModeVersionProperty,
                        1 + (int)buttonControl.GetValue(RibbonGroupPanel.SizingModeVersionProperty));
                }
            }
        }
    }
}
