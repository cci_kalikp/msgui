﻿using System;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;
using MorganStanley.MSDotNet.MSGui.Core;
using MSDesktop.Isolation.Enums;
using MSDesktop.Isolation.Utils;

namespace MSDesktop.Isolation.HwndHosts
{
    internal sealed class OptionsPageHwndSource : HwndSource
    {
        private readonly IOptionView _optionView;

        // private const uint WmUserMessage = Win32.WM_USER + 755;
        internal OptionsPageHwndSource(IOptionView optionView)
            : base(new HwndSourceParameters("MSDotNet.MSGui.Impl.Isolation")
                {
                    WindowStyle = 0
                })
        {
            _optionView = optionView;
            SizeToContent = SizeToContent.WidthAndHeight;
        }


        public override Visual RootVisual
        {
            get { return base.RootVisual; }
            set
            {
                base.RootVisual = value;
                AddHook(WndProc);
            }
        }

        private IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            switch ((uint) msg)
            {
                case IsolationConstants.WmUserMessage:
                    handled = ProcessWmUserMessage(wParam);
                    break;
            }

            return IntPtr.Zero;
        }

        private bool ProcessWmUserMessage(IntPtr wParam)
        {
            var messageCode = wParam.ToInt32();
            switch (messageCode)
            {
                case (int)OptionPageOperation.OK:
                    _optionView.OnOK();
                    break;

                case (int)OptionPageOperation.Cancel:
                    _optionView.OnCancel();
                    break;

                case (int)OptionPageOperation.Display:
                    _optionView.OnDisplay();
                    break;

                case (int)OptionPageOperation.Help:
                    _optionView.OnHelp();
                    break;
            }

            return true;
        }
    }
}
