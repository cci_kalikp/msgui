﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Controls;
using MSDesktop.Isolation.Enums;
using MSDesktop.Isolation.UiAddIns;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Core.Persistence.DelayedLoading;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.LauncherBar;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Ribbon;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.Factories;
using MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging.CPSMessaging;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence.DelayedLoading;
using MorganStanley.MSDotNet.MSGui.Impl.ProcessIsolation;
using MSDesktop.Isolation.Interfaces;
using MSDesktop.Isolation.Proxies;
using MSDesktop.MessageRouting;
using MSDesktop.MessageRouting.GUI.Modules;
using MSDesktop.MessageRouting.Isolation;
using ModuleStatus = MorganStanley.MSDotNet.MSGui.Core.ModuleStatus;

namespace MSDesktop.Isolation
{
    [EditorBrowsable(EditorBrowsableState.Never)]
    public sealed class HostProcessContainerInitializer
    {
        internal const string BaseServerName = "WPFProcessAddIn";

        private readonly IMSDesktopSubsystemSite _site;
        private readonly HostContracts.IsolatedSubsystemSettings _settings;
        private readonly IUnityContainer _container;
        private readonly string _subsystemExecuterDirectory;
        private readonly IIsolatedSubsystemStarter _starter;

        public HostProcessContainerInitializer(IIsolatedSubsystemStarter starter, IUnityContainer container, IMSDesktopSubsystemSite site,
                                               HostContracts.IsolatedSubsystemSettings settings,
                                               string subsystemExecuterDirectory)
        {
            _starter = starter;
            _subsystemExecuterDirectory = subsystemExecuterDirectory;
            if (string.IsNullOrEmpty(settings.IsolatedSubsystemLocation))
            {
                settings.IsolatedSubsystemLocation = AppDomain.CurrentDomain.BaseDirectory;
            }

            _container = container;
            _settings = settings;
            _site = site;
        }

        public void Initialize()
        {
            _container.RegisterInstance(_site);

            var chromeRegistry = new ChromeRegistryProxy();

            _container.RegisterInstance<IChromeRegistry>(chromeRegistry, new ContainerControlledLifetimeManager());
            _container.RegisterInstance<IChromeElementInitializerRegistry>(chromeRegistry,
                                                                           new ContainerControlledLifetimeManager());
            _container.RegisterInstance(typeof (IUnityContainer), _container, new ContainerControlledLifetimeManager());

            _container.RegisterType<IWidgetFactory, WidgetFactory>(new ContainerControlledLifetimeManager());
            _container.RegisterType<IWindowFactory, WindowFactory>(new ContainerControlledLifetimeManager());
            _container.RegisterType<IModuleGroup, IsolatedProcessModuleGroup>(new ContainerControlledLifetimeManager());
            _container.RegisterType<IDelayedProfileLoader, DelayedProfileLoader>(
                new ContainerControlledLifetimeManager());

            _container.RegisterType<IChromeManager, ChromeManagerProxy>(new ContainerControlledLifetimeManager());
            if (_settings.ShellMode == ShellMode.LauncherBarAndFloatingWindows ||
                _settings.ShellMode == ShellMode.LauncherBarAndWindow)
            { 
                _container.RegisterType<ICommonFactories, LauncherBarFactories>(new ContainerControlledLifetimeManager());
            }
            else
            {

                _container.RegisterType<ICommonFactories, RibbonFactories>(new ContainerControlledLifetimeManager());
            }
            _container.RegisterInstance(_settings);

            ModuleHandler.Initialize(_container, _settings.UsePrism);
            var settingsContract = new IsolatedSubsystemSettings
                {
                    ShellMode = _settings.ShellMode,
                    AdditionalCommandLine = _settings.AdditionalCommandLine,
                    ApplicationName = _settings.ApplicationName,
                    AssemblyResolutionFiles = _settings.AssemblyResolutionFiles,
                    CpsConfig = _settings.CpsConfig,
                    HostProcessId = _settings.HostProcessId,
                    IsCpsRequired = _settings.IsCpsRequired,
                    IsAdditionalArRequired = _settings.IsAdditionalArRequired,
                    IsolatedSubsystemConfigFile = _settings.IsolatedSubsystemConfigFile,
                    IsolatedSubsystemLocation = _settings.IsolatedSubsystemLocation,
                    IsolatedType = _settings.IsolatedType,
                    UsePrism = _settings.UsePrism,
                    IsHostAppInitialized = _settings.IsHostAppInitialized,
                    LayoutFileHandle = _settings.LayoutFileHandle,
                    LayoutFileSize = _settings.LayoutFileSize,
                    ProfileName = _settings.ProfileName,
                    ExtraInformation = _settings.ExtraInformation
                };

            Type entryType;
            try
            {
                entryType = LoadIsolatedType();
            }
            catch (Exception ex)
            {
                if (!_site.IsControl)
                {
                    _site.SetModuleStatus(_settings.IsolatedType,
                                          ModuleStatus.TypeResolutionFailed.ToProxyModuleStatus(), ex);
                }

                throw;
            }

            if (entryType == null)
            {
                return;
            }

            var isEntryAModule = entryType.IsModule();
            var isEntryASubsystem = entryType.IsSubsystem();

            if (isEntryAModule || isEntryASubsystem)
            {
                if (isEntryAModule)
                {
                    RegisterSubsystemTypes();

                    var moduleInstance = RegisterAndInstantiateModule(_container, entryType);
                    WaitForHostInit();
                    InitializeModule(moduleInstance, 0, settingsContract);

                    if (!typeof (DelayedProfileLoadCoordinatingModule).IsAssignableFrom(entryType))
                    {
                        LoadInitialLayout();
                    }
                    else
                    {
                        var singleModuleGroup = ((IsolatedProcessModuleGroup) _container.Resolve<IModuleGroup>());
                        singleModuleGroup.DelayedModuleInitialized += (sender, args) => LoadInitialLayout();
                    }

                    var application =
                        (MorganStanley.MSDotNet.MSGui.Impl.ProcessIsolation.IsolatedApplication)
                        _container.Resolve<IApplication>();
                    application.InvokeApplicationLoaded();
                }
                else
                {
                    IEnumerable<Type> moduleTypes;
                    if (typeof (ISubsystemModuleCollection).IsAssignableFrom(entryType))
                    {
                        var moduleCollection = (ISubsystemModuleCollection) Activator.CreateInstance(entryType);
                        moduleTypes = moduleCollection.GetModules();
                    }
                    else
                    {
                        var moduleCollection = (ISubsystemModuleCollectionEx) Activator.CreateInstance(entryType);
                        moduleTypes = moduleCollection.GetModules(settingsContract, _subsystemExecuterDirectory);
                    }

                    RegisterSubsystemTypes();
                    WaitForHostInit();
                    
                    var i = 0;
                    foreach (var moduleType in moduleTypes)
                    {
                        var moduleInstance = RegisterAndInstantiateModule(_container, moduleType);
                        InitializeModule(moduleInstance, i++, settingsContract);
                    }

                    // Load the initial layout
                    if (_settings.LayoutFileSize > 0)
                    {
                        var persistenceProfileService = _container.Resolve<PersistenceProfileService>();
                        persistenceProfileService.LoadProfile(persistenceProfileService.CurrentProfile);
                    }

                    var chromeManager = (ChromeManagerProxy) _container.Resolve<IChromeManager>();
                    chromeManager.FireLayoutLoaded();

                    var application =
                        (MorganStanley.MSDotNet.MSGui.Impl.ProcessIsolation.IsolatedApplication)
                        _container.Resolve<IApplication>();
                    application.InvokeApplicationLoaded();
                }

                // Check the chrome registry and register the factory IDs with the host process
                _site.SetApplicationStarted(Process.GetCurrentProcess().Id, chromeRegistry.WindowFactoryIDs);
            }
            else
            {
                var payloadTypeName = _site.GetPayloadType();
                if (payloadTypeName != null)
                {
                    var payloadType = Type.GetType(payloadTypeName);
                    if (payloadType != null && payloadType != typeof (object))
                    {
                        // We need to be able to resolve ISubscriber<PayloadType> to MarshalingSubscriberProxy<PayloadType>
                        RegisterSubscriberTypeMapping(payloadType, _container);
                        RegisterPublisherTypeMapping(payloadType, _container);
                    }
                }

                // Instantiate the user control
                var addin = new IsolatedViewUiAddIn(_site);
                _container.RegisterInstance<ViewUiAddinBase>(addin);

                var isolatedInstance = _container.Resolve(entryType);
                var control = isolatedInstance as UserControl;

                addin.IsolatedControl = control;
                addin.Initialize();
            }
        }

        internal void ReloadProfile(string profileName, long layoutFileHandle, int layoutFileSize)
        {
            ProfileLoadUtility.ReloadProfile(_container, profileName, layoutFileHandle, layoutFileSize);
        }

        private void WaitForHostInit()
        {
            var profileFileInfo = _starter.WaitForInitialize();
            _settings.IsHostAppInitialized = true;
            _settings.ProfileName = profileFileInfo.ProfileName;
            _settings.LayoutFileSize = profileFileInfo.FileLength;
            _settings.LayoutFileHandle = profileFileInfo.FileHandle;

            ProfileLoadUtility.InitializeStorage(_container, _settings);
        }

        private void LoadInitialLayout()
        {
            if (_settings.LayoutFileSize > 0)
            {
                var persistenceProfileService = _container.Resolve<PersistenceProfileService>();
                persistenceProfileService.LoadProfile(persistenceProfileService.CurrentProfile);
            }

            var chromeManager = (ChromeManagerProxy) _container.Resolve<IChromeManager>();
            chromeManager.FireLayoutLoaded();
        }

        private Type LoadIsolatedType()
        {
            return Type.GetType(_settings.IsolatedType, ResolveAssembly, null, true);
        }

        private Assembly ResolveAssembly(AssemblyName asmName)
        {
            var filePathBase = string.Format(@"{0}\{1}", _settings.IsolatedSubsystemLocation, asmName.Name);
            string[] filesToTry = { filePathBase + ".exe", filePathBase + ".dll" };

            var asm = filesToTry
                .Where(File.Exists)
                .Select(Assembly.LoadFrom)
                .FirstOrDefault();
            if (asm != null || string.IsNullOrEmpty(_subsystemExecuterDirectory)) return asm;
            filePathBase = string.Format(@"{0}\{1}", _subsystemExecuterDirectory, asmName.Name);
            filesToTry = new[] { filePathBase + ".exe", filePathBase + ".dll" }; 
            return filesToTry
                .Where(File.Exists)
                .Select(Assembly.LoadFrom)
                .FirstOrDefault(); 
        }

        private void RegisterSubscriberTypeMapping(Type payloadType, IUnityContainer container)
        {
            var typeFrom = typeof(ISubscriber<>).MakeGenericType(payloadType);
            var typeTo = _settings.UsePrism
                             ? typeof(MarshalingSubscriberCompositeProxy<>).MakeGenericType(payloadType)
                             : typeof(MarshalingSubscriberPrismProxy<>).MakeGenericType(payloadType);

            container.RegisterType(typeFrom, typeTo, new ContainerControlledLifetimeManager());
        }

        private static void RegisterPublisherTypeMapping(Type payloadType, IUnityContainer container)
        {
            var typeFrom = typeof(IPublisher<>).MakeGenericType(payloadType);
            var typeTo = typeof(MarshalingPublisherProxy<>).MakeGenericType(payloadType);

            container.RegisterType(typeFrom, typeTo, new ContainerControlledLifetimeManager());
        }

        private void InitializeModule(object moduleInstance, int moduleIndex,
                                      IsolatedSubsystemSettings settingsContract)
        {
            try
            {
                if (moduleInstance.GetType().IsModule())
                {
                    // Isolated module - module initialization will handle its stuff via the proxies it will have.
                    ModuleHandler.InitializeModule(moduleInstance, moduleIndex, settingsContract);
                    _site.SetModuleStatus(moduleInstance.GetType().AssemblyQualifiedName, ModuleStatus.Initialized.ToProxyModuleStatus());
                }
            }
            catch (Exception ex)
            {
                _site.SetModuleStatus(moduleInstance.GetType().AssemblyQualifiedName, ModuleStatus.Exceptioned.ToProxyModuleStatus(), ex);
                throw;
            }
        }

        private object RegisterAndInstantiateModule(IUnityContainer container, Type entryType)
        { 
            container.RegisterType(entryType, new ContainerControlledLifetimeManager());
            var moduleType = ModuleInfoHelper.GetModuleType(entryType);
            if (!_site.IsControl)
            {
                _site.SetModuleStatus(moduleType.AssemblyQualifiedName, ModuleStatus.NotLoaded.ToProxyModuleStatus());
            }
            try
            {
                var isolatedInstance = container.Resolve(entryType);
                _site.SetModuleStatus(moduleType.AssemblyQualifiedName, ModuleStatus.Loaded.ToProxyModuleStatus());
                return isolatedInstance;

            }
            catch (Exception ex)
            {
                _site.SetModuleStatus(moduleType.AssemblyQualifiedName, ModuleStatus.Exceptioned.ToProxyModuleStatus(), ex);
                throw;
            }
        }

        private static IsolatedSubsystemSettings FromHostContractSettings(
            HostContracts.IsolatedSubsystemSettings hostContractSettings)
        {
            var settings = new IsolatedSubsystemSettings
            {
                AdditionalCommandLine = hostContractSettings.AdditionalCommandLine,
                ApplicationName = hostContractSettings.ApplicationName,
                HostProcessId = hostContractSettings.HostProcessId,
                IsolatedSubsystemConfigFile = hostContractSettings.IsolatedSubsystemConfigFile,
                IsolatedSubsystemLocation = hostContractSettings.IsolatedSubsystemLocation,
                IsolatedType = hostContractSettings.IsolatedType,
                IsCpsRequired = hostContractSettings.IsCpsRequired,
                CpsConfig = hostContractSettings.CpsConfig
            };

            return settings;
        }

        private void RegisterSubsystemTypes()
        {
            // Create and configure application instance
            var coreSettings = FromHostContractSettings(_settings);
            var application = new MorganStanley.MSDotNet.MSGui.Impl.ProcessIsolation.IsolatedApplication(coreSettings)
            {
                Name = _settings.ApplicationName
            };

            _container.RegisterInstance<IApplication>(application, new ContainerControlledLifetimeManager());

            // Register the Kraken communication module type, and its helper types
            ModuleHandler.InitializeSubsystemCore(_container, _settings);

            // Default channel settings for isolated process - channels are optional.
            var settings = new CommunicatorSettings(
                _settings.HostProcessId,
                _settings.IsCpsRequired ? CommunicationChannelStatus.Required : CommunicationChannelStatus.Optional,
                _settings.IsCpsRequired ? CommunicationChannelStatus.Required : CommunicationChannelStatus.Optional);
            _container.RegisterInstance<ICommunicatorSettings>(settings);

            // TODO (hrechkin): use the cpsconfig if defined for cps

            RoutedCommunicationModule.MessagingEnvironment = AppEnvironment.All;
            CreateAndInitModule<RoutedCommunicationModule>(_container);
            MessageRoutingGuiModule.HideOptionsPage = true;
            CreateAndInitModule<MessageRoutingGuiModule>(_container);
            //since we enabled kraken, unknown messages would reach all applications, we don't want to throw exception
            CpsModuleMessenger.ThrowOnUnknownType = false;
            if (!_settings.IsCpsRequired)
            {
                RoutedMessageDispatcher.ExceptionAsFailed = true;
            }

            // Initialize common helpers
            CommonHelpers.InitHelpers(_container);

            // Initialize ribbon factories
            var ribbonFactories = _container.Resolve<ICommonFactories>() as RibbonFactories;
            
            var chromeManager = _container.Resolve<IChromeManager>();

            if (ribbonFactories != null)
            {
                ribbonFactories.Initialize(chromeManager);
            }
            else
            {
                var launcherBarFactories = _container.Resolve<ICommonFactories>() as LauncherBarFactories;
                if (launcherBarFactories != null)
                {
                    launcherBarFactories.Initialize(chromeManager);
                }
            }

            var routingStep = _container.Resolve<PreferLocalEndpointsAcrossSubsystems>();
            routingStep.Register();
        }

        private static void CreateAndInitModule<TModule>(IUnityContainer container)
            where TModule : IMSDesktopModule
        {
            var communicationModule = container.Resolve<TModule>();
            communicationModule.Initialize();
        }
    }
}
