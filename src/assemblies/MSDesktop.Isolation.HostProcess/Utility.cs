﻿// Utility code shared by the host and the add-in. For convenience, placed in the add-in assembly,
// which the host references anyway (for the add-in interface). In a real solution, it's a good idea
// to place this stuff in a separate assembly, in particular in order to limit the add-in type
// injection into the host process. (That's generally a problem for security.)

using System.Threading;
using System.Windows.Threading;
//using MorganStanley.MSDotNet.MSGui.Impl.ProcessIsolation;

namespace MSDesktop.Isolation
{
	/// <summary>
	/// Wrapper for a worker thread that has a Dispatcher running on it. This helps us solve some
	/// re-entrancy and deadlock problems.
	/// </summary>
	internal class DispatcherWorkerThread
	{
		public Dispatcher Dispatcher { get; private set; }

		public DispatcherWorkerThread()
		{
			var workerThread = new Thread((ThreadStart)delegate
			{
				this.Dispatcher = Dispatcher.CurrentDispatcher;
				Dispatcher.Run();
			}) { IsBackground = true, Name = "Dispatcher worker" };
			workerThread.Start();
		}
	};
}
