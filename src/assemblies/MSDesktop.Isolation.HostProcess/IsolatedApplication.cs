﻿using System.Windows;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MSDesktop.Isolation.Interfaces;
using Application = System.Windows.Application;

namespace MSDesktop.Isolation
{
    internal sealed class IsolatedApplication : Application
    {
        private readonly HostProcessContainerInitializer _containerInitializer;
        private readonly UnityContainer _container;

        internal IsolatedApplication(IIsolatedSubsystemStarter starter, IMSDesktopSubsystemSite site, HostContracts.IsolatedSubsystemSettings settings, string subsystemExecuterDirectory)
        {
            _container = new UnityContainer();
            _containerInitializer = new HostProcessContainerInitializer(starter, _container, site, settings, subsystemExecuterDirectory);
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            // Set the application settings
            ShutdownMode = ShutdownMode.OnExplicitShutdown;
            _containerInitializer.Initialize();
        }

        internal void ReloadProfile(string profileName, long layoutFileHandle, int layoutFileSize)
        {
            _containerInitializer.ReloadProfile(profileName, layoutFileHandle, layoutFileSize);
        }

        internal void CreateRemoteWindow(string factoryId, string viewId)
        {
            var chromeManager = _container.Resolve<IChromeManager>();

            chromeManager.CreateWindow(factoryId, viewId != null ? new InitialPlaceableWindowParameters(viewId) : new InitialWindowParameters());
        }
    }
}
