using System;
using System.ComponentModel;
using MSDesktop.Isolation.HostContracts;

namespace MSDesktop.Isolation
{
    /// <summary>
    /// Used with process isolation and is not intended to be used or implemented in your code directly.
    /// </summary>
    [EditorBrowsable(EditorBrowsableState.Never)]
    public interface IIsolatedSubsystemStarter
    {
        void Initialize(AppDomain startupDomain, string[] msdeConfigFiles);
        void Start(string uri, IsolatedSubsystemSettings settings);
        void ProceedWithInitialization(long profileFileHandle, int fileLength, string profileName);
        void ReloadProfile(string currentProfile, long layoutFileHandle, int layoutFileSize);
        ProfileFileInfo WaitForInitialize();
        void Shutdown();
        void CreateWindow(string factoryId, string viewId);
    }
}
