﻿using System.ServiceModel;

namespace MSDesktop.Isolation.HostContracts
{
    /// <summary>
    /// Site for subsystem isolation. Not intended to be used or implemented by your code.
    /// </summary>
    [ServiceContract(Name = "IsolatedSubsystemSite", Namespace = "net-pipe://IsolatedSubsystemSettings")]
    public interface IIsolatedSubsystemSite
    {
        [OperationContract]
        IsolatedSubsystemSettings GetIsolatedSubsystemSettings(int processId, string settingsUri, string processAddinUri);
    }
}
