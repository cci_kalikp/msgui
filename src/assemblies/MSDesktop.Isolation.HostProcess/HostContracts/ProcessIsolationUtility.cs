﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.Threading;
using System.Threading.Tasks;

namespace MSDesktop.Isolation.HostContracts
{
    [EditorBrowsable(EditorBrowsableState.Never)]
    public class ProcessIsolationUtility
    {
        private static int _channelCount;

        // Keep track of service hosts
        private static readonly IDictionary<string, ServiceHost> Hosts =
            new ConcurrentDictionary<string, ServiceHost>();

        public static string RegisterServerChannel<T>(string baseServerName, T sessionObject)
        {
            var currentChannelCount = Interlocked.Increment(ref _channelCount);
            return RegisterServerChannel(baseServerName, currentChannelCount, sessionObject);
        }

        public static string RegisterServerChannel<T>(string baseServerName, int currentChannelIndex, T sessionObject)
        {
            // Configure WCF for both server and client here
            var channelUri = string.Format("net.pipe://localhost/{0}-{1}-{2}", baseServerName,
                                           Process.GetCurrentProcess().Id,
                                           currentChannelIndex);

            // configure binding
            var binding = new NetNamedPipeBinding(NetNamedPipeSecurityMode.None)
                {
                    Name = channelUri,
                    CloseTimeout = TimeSpan.MaxValue,
                    OpenTimeout = TimeSpan.MaxValue,
                    ReaderQuotas = {MaxStringContentLength = 5242880},
                };

            var serviceHost = new ServiceHost(sessionObject, new Uri(channelUri))
                {
                    OpenTimeout = TimeSpan.MaxValue,
                    CloseTimeout = TimeSpan.MaxValue,
                };

            serviceHost.AddServiceEndpoint(typeof (T), binding, channelUri);
            serviceHost.Open();

            Hosts.Add(channelUri, serviceHost);
            return channelUri;
        }

        public static void ShutdownServerChannels()
        {
            var hostsList = Hosts.ToList();
            Parallel.ForEach(hostsList, kvp => ProcessHostClosing(kvp.Key, kvp.Value));
        }

        private static void ProcessHostClosing(string hostUrl, ServiceHostBase host)
        {
            Hosts.Remove(hostUrl);
            host.Close();

            var disposableHost = (IDisposable)host;
            disposableHost.Dispose();
        }

        public static T InitClientChannel<T>(string url)
        {
            var myBinding = new NetNamedPipeBinding(NetNamedPipeSecurityMode.None)
                {
                    ReaderQuotas = {MaxStringContentLength = 5242880},
                    MaxReceivedMessageSize = 524880,
                    OpenTimeout = TimeSpan.MaxValue,
                    CloseTimeout = TimeSpan.MaxValue,
                };

            var myEndpoint = new EndpointAddress(url);
            var myChannelFactory = new ChannelFactory<T>(myBinding, myEndpoint);

            var channel = myChannelFactory.CreateChannel(myEndpoint);
            return channel;
        }
    }
}
