﻿
using System.ComponentModel;

namespace MSDesktop.Isolation.HostContracts
{
    [EditorBrowsable(EditorBrowsableState.Never)]
    public sealed class ProfileFileInfo
    {
        private readonly int _fileLength;
        private readonly long _fileHandle;
        private readonly string _profileName;

        internal ProfileFileInfo(long fileHandle, int fileLength, string profileName)
        {
            _profileName = profileName;
            _fileHandle = fileHandle;
            _fileLength = fileLength;
        }

        public int FileLength
        {
            get { return _fileLength; }
        }

        public long FileHandle
        {
            get { return _fileHandle; }
        }

        public string ProfileName
        {
            get { return _profileName; }
        }
    }
}
