﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.Win32;
using MSDesktop.Isolation.HwndHosts;

namespace MSDesktop.Isolation.UiAddIns
{
    internal sealed class OptionPageUiAddIn : UiAddinBase
    {
        private readonly IOptionView _optionView;

        internal OptionPageUiAddIn(FrameworkElement userControl, IOptionView optionView)
            : base(userControl)
        {
            _optionView = optionView;
        }

        public override void Load(string doc)
        {
            throw new System.NotImplementedException();
        }

        public override string Save()
        {
            throw new System.NotImplementedException();
        }

        public override void InvokeClosed()
        {
            throw new System.NotImplementedException();
        }

        public override bool InvokeClosing()
        {
            throw new System.NotImplementedException();
        }

        public override void InvokeClosedQuietly()
        {
            throw new System.NotImplementedException();
        }

        protected override HwndSource CreateHwndSource()
        {
            var containerElement = new StackPanel
            {
                Orientation = Orientation.Vertical,
                VerticalAlignment = VerticalAlignment.Stretch,
            };

            var element = FrameworkElement;
            containerElement.Children.Add(element);

            var hwndSource = new OptionsPageHwndSource(_optionView)
            {
                RootVisual = containerElement
            };

            Win32.ConvertToChildWindow(hwndSource.Handle);
            return hwndSource;
        }
    }
}
