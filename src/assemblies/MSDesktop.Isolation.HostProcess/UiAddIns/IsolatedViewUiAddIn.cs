﻿using System;
using System.IO;
using System.Reflection;
using System.ServiceModel;
using System.Threading;
using System.Windows;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.ProcessIsolation;
using MSDesktop.Isolation.Interfaces;
using MSDesktop.Isolation.Proxies;

namespace MSDesktop.Isolation.UiAddIns
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, UseSynchronizationContext = false)]
    internal sealed class IsolatedViewUiAddIn : ViewUiAddinBase, IMSDesktopModule
    {
        private readonly IMSDesktopSubsystemSite _site;

        public IsolatedViewUiAddIn(IMSDesktopSubsystemSite site)
        {
            _site = site;
        }

        protected override void PostInitialize()
        {
            HookupKeyboardNavigation(HwndSource);

            ThreadPool.QueueUserWorkItem(o =>
            {
                var addInUri =
                    ProcessIsolationUtility.RegisterServerChannel<IXProcUiAddIn>(
                        ChromeManagerProxy.BaseServerName, this);
                var addInSiteUri = _site.AddWindow(addInUri, HwndSource.Handle.ToInt64(), null, string.Empty, null);
                HostSite = ProcessIsolationUtility.InitClientChannel<IXProcWindowSite>(addInSiteUri);
            }); // We're ready!
        }

        internal FrameworkElement IsolatedControl
        {
            set { FrameworkElement = value; }
        }

        public override void Load(string doc)
        {
            try
            {
                FrameworkElement.Dispatcher.Invoke((Action) (() =>
                    {
                        if (!string.IsNullOrEmpty(doc) && FrameworkElement != null)
                        {
                            var xdoc = XDocument.Load(new StringReader(doc));
                            var load = FrameworkElement.GetType()
                                                       .GetMethod("Load",
                                                                  BindingFlags.FlattenHierarchy | BindingFlags.Instance |
                                                                  BindingFlags.Public);

                            if (load != null)
                            {
                                load.Invoke(FrameworkElement, new object[] {xdoc});
                            }
                        }
                    }));
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.ToString());
            }
        }

        public override string Save()
        {
            try
            {
                FrameworkElement.Dispatcher.Invoke((Func<string>) (() =>
                    {
                        var save = FrameworkElement.GetType()
                                                   .GetMethod("Save",
                                                              BindingFlags.FlattenHierarchy | BindingFlags.Instance |
                                                              BindingFlags.Public);
                        if (save != null)
                        {
                            var obj = save.Invoke(FrameworkElement, new object[] {});
                            var document = obj as XDocument;
                            if (document != null)
                            {
                                return document.ToString(SaveOptions.None);
                            }
                        }

                        return null;
                    }));
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.ToString());
            }

            return null;
        }

        /*
        public override void ShutDown()
        {
            try
            {

                if (HwndSource != null)
                {
                    HwndSource.Dispatcher.Invoke(delegate
                        {
                            HwndSource.Dispatcher.InvokeShutdown(); // This will lead to process exit.
                            if (Application.Current != null)
                            {
                                Application.Current.Shutdown();
                            }
                        });
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.ToString());
            }
        }*/

        public override void InvokeClosed()
        {
            throw new NotImplementedException("This operation is supported only in module isolation scenarios only");
        }

        public override bool InvokeClosing()
        {
            throw new NotImplementedException("This operation is supported only in module isolation scenarios only");
        }

        public override void InvokeClosedQuietly()
        {
            throw new NotImplementedException("This operation is supported only in module isolation scenarios only");
        }
    }
}
