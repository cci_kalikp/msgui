﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;
using MorganStanley.MSDotNet.MSGui.Core.Win32;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;

namespace MSDesktop.Isolation.UiAddIns
{
    internal abstract class ViewUiAddinBase : UiAddinBase
    {
        private readonly IFormatter _formatter = new BinaryFormatter();
        private readonly IProducerConsumerCollection<PendingV2VSubscription> _pendingSubscriptions = new ConcurrentQueue<PendingV2VSubscription>();
        private readonly IProducerConsumerCollection<PendingV2VSubscription> _pendingPublications = new ConcurrentQueue<PendingV2VSubscription>();
        private readonly ViewMessageActionsCollection _viewActions = new ViewMessageActionsCollection();

        protected ViewUiAddinBase(FrameworkElement frameworkElement) : base(frameworkElement)
        {
            if (frameworkElement == null)
            {
                // We have early creation of the add-in. Need to know when we get the host to process pending publications/sibscriptions
                HostSiteAvailable += WindowUiAddIn_HostSiteAvailable;
            }
        }

        protected ViewUiAddinBase()
        {
        }

        public override void Receive(byte[] payloadBytes)
        {
            object payload;
            using (var stream = new MemoryStream(payloadBytes))
            {
                payload = _formatter.Deserialize(stream);
            }

            _viewActions.ExecuteActions(payload);
        }

        public void RegisterSubscriber(Type messageType, Action<object> subscriber)
        {
            _viewActions.RegisterAction(messageType, subscriber);
        }

        protected override HwndSource CreateHwndSource()
        {
            FrameworkElement controlElement;
            var window = FrameworkElement as Window;
            if (window != null)
            {
                var actualContent = window.Content as FrameworkElement;
                window.Content = null;

                controlElement = new ContentControl
                {
                    Content = actualContent,
                    Name = window.Name,
                };

                window.Close();
            }
            else
            {
                controlElement = FrameworkElement;
            }

            var hwsp = new HwndSourceParameters("MSDotNet.MSGui.Impl.Isolation") { WindowStyle = 0 };
            var hwndSource = new HwndSource(hwsp)
            {
                SizeToContent = SizeToContent.Manual,
                RootVisual = controlElement
            };

            Win32.ConvertToChildWindow(hwndSource.Handle);
            return hwndSource;
        }

        private void WindowUiAddIn_HostSiteAvailable(object sender, EventArgs e)
        {
            // process pending publications
            PendingV2VSubscription subscription;
            while (_pendingPublications.TryTake(out subscription))
            {
                HostSite.RegisterAsPublisher(subscription.ViewId, subscription.TypeName);
            }

            while (_pendingSubscriptions.TryTake(out subscription))
            {
                HostSite.SubscribeV2V(subscription.ViewId, subscription.TypeName);
            }
        }


        internal void RegisterAsPublisher(string viewId, string typeName)
        {
            if (HostSite != null)
            {
                HostSite.RegisterAsPublisher(viewId, typeName);
            }
            else
            {
                // Queue the pending publication or subscription
                var pendingPublication = new PendingV2VSubscription(viewId, typeName);
                _pendingPublications.TryAdd(pendingPublication);
            }
        }

        internal void SubscribeV2V(string viewId, string typeName)
        {
            if (HostSite != null)
            {
                HostSite.SubscribeV2V(viewId, typeName);
            }
            else
            {
                // Queue the pending publication or subscription
                var pendingSubscription = new PendingV2VSubscription(viewId, typeName);
                _pendingSubscriptions.TryAdd(pendingSubscription);
            }
        }

        internal void Send(byte[] messageBytes)
        {
            HostSite.Send(messageBytes);
        }

        private sealed class PendingV2VSubscription
        {
            private readonly string _viewId;
            private readonly string _typeName;

            public PendingV2VSubscription(string viewId, string typeName)
            {
                _viewId = viewId;
                _typeName = typeName;
            }

            public string ViewId
            {
                get { return _viewId; }
            }

            public string TypeName
            {
                get { return _typeName; }
            }
        }
    }
}
