﻿using System;
using System.Linq;
using System.ServiceModel;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Threading;
using MorganStanley.MSDotNet.MSGui.Core.Win32;
using MSDesktop.Isolation.Interfaces;
using MSDesktop.Isolation.Proxies;

namespace MSDesktop.Isolation.UiAddIns
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, UseSynchronizationContext = false)]
    internal abstract class UiAddinBase : IXProcUiAddIn, IKeyboardInputSite
    {
        private static readonly DispatcherWorkerThread WorkerThread = new DispatcherWorkerThread();
        private static readonly uint[] KeyMsgsToBubble = new[]
            {
                Win32.WM_KEYDOWN, Win32.WM_KEYUP, Win32.WM_SYSKEYDOWN, Win32.WM_SYSKEYUP,
                Win32.WM_SYSCHAR, Win32.WM_SYSDEADCHAR
            };

        private volatile IXProcWindowSite _hostSite;
        
        private FrameworkElement _frameworkElement;
        private HwndSource _hwndSource;

        protected UiAddinBase()
        {
        }
        
        protected UiAddinBase(FrameworkElement frameworkElement)
        {
            _frameworkElement = frameworkElement;
        }

        public void Initialize()
        {
            _hwndSource = CreateHwndSource();
            PostInitialize();
        }

        protected event EventHandler<EventArgs> HostSiteAvailable;

        public IXProcWindowSite HostSite
        {
            protected get { return _hostSite; }
            set
            {
                _hostSite = value;
                if (value != null)
                {
                    var handler = HostSiteAvailable;
                    if (handler != null)
                    {
                        handler(this, EventArgs.Empty);
                    }
                }
            }
        }

        protected FrameworkElement FrameworkElement
        {
            get { return _frameworkElement; }
            set { _frameworkElement = value; }
        }

        protected HwndSource HwndSource
        {
            get { return _hwndSource; }
        }

        public abstract void Load(string doc);
        public abstract string Save();
        public abstract void InvokeClosed();
        public abstract bool InvokeClosing();
        public abstract void InvokeClosedQuietly();

        public virtual void Receive(byte[] payload)
        {
        }

        internal IntPtr Handle
        {
            get { return HwndSource.Handle; }
        }

        protected virtual void PostInitialize()
        {
        }

        protected abstract HwndSource CreateHwndSource();

        protected void HookupKeyboardNavigation(HwndSource hwndSource)
        {
            // Enable tabbing out of the add-in. (It is possible to keep Tab cycle within but let only Ctrl+Tab
            // move the focus out.)
            KeyboardNavigation.SetTabNavigation(_frameworkElement, KeyboardNavigationMode.Continue);
            KeyboardNavigation.SetControlTabNavigation(_frameworkElement, KeyboardNavigationMode.Continue);
            ((IKeyboardInputSink)hwndSource).KeyboardInputSite = this;

            // This message loop hook facilitates "bubbling" unhandled keys to the host. We critically rely
            // on HwndSource having already registered its hook so that this one is invoked last.
            ComponentDispatcher.ThreadPreprocessMessage += ThreadPreprocessMessage;
        }

        private void ThreadPreprocessMessage(ref MSG msg, ref bool handled)
        {
            var wm = unchecked((uint)msg.message);
            if (!handled && KeyMsgsToBubble.Contains(wm) && HostSite != null) 
            {
                // Because in its handling of a key the host may want to call into the add-in or may trigger
                // a window message that needs to be dispatched synchronously to the add-in's window, we have
                // to keep the UI thread responsive to window messages. This is accomplished thanks to the 
                // semi-permeable managed blocking that the Dispatcher normally allows to happen in an STA.
                // (For deep background, see http://blogs.msdn.com/cbrumme/archive/2004/02/02/66219.aspx.) 
                var callback = (DispatcherOperationCallback)(m => HostSite
                                                                   .TranslateAccelerator
                                                                   (new MsgProxy((MSG)m)));

                handled = (bool)WorkerThread.Dispatcher.Invoke(callback, msg);
                
            }
        }

        private object DispatcherInvokeCallback(object tr)
        {
            return HostSite != null && HostSite.TabOut((TraversalRequest)tr);
        }

        bool IKeyboardInputSite.OnNoMoreTabStops(TraversalRequest request)
        {
            // Tabbing out implies focus change. WM_SETFOCUS (and possibly related messages) needs to be sent
            // synchronously to the add-in's window when the host claims focus. To enable dispatching of this
            // message, the UI thread has to be available. (Same issue as in ThreadPreprocessMessage().)
            return
                (bool)WorkerThread.Dispatcher.Invoke((DispatcherOperationCallback)DispatcherInvokeCallback, request);
        }

        IKeyboardInputSink IKeyboardInputSite.Sink
        {
            get { return _hwndSource; }
        }

        void IKeyboardInputSite.Unregister()
        {
        }
    }
}
