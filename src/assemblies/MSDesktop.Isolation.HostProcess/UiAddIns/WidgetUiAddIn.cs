﻿using System;
using System.ServiceModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;
using System.Windows.Markup;
using MSDesktop.Isolation.Enums;
using MorganStanley.MSDotNet.MSGui.Core.Win32;
using MSDesktop.Isolation.Controls;
using MSDesktop.Isolation.HwndHosts;

namespace MSDesktop.Isolation.UiAddIns
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, UseSynchronizationContext = false)]
    internal sealed class ControlUiAddIn : UiAddinBase
    {
        private readonly bool useLauncherBar;
        internal ControlUiAddIn(FrameworkElement userControl_, ShellMode shellMode_)
            : base(userControl_)
        {
            useLauncherBar = shellMode_ == ShellMode.LauncherBarAndWindow || shellMode_ == ShellMode.LauncherBarAndFloatingWindows;
        }

        protected override HwndSource CreateHwndSource()
        {
            var containerElement = useLauncherBar ? new IsolatedControlContainer() : new IsolatedRibbonControlContainer(); 

            var element = FrameworkElement;
            containerElement.Children.Add(element);

            var hwndSource = useLauncherBar ? new ControlHwndSource() : new RibbonControlHwndHost();

            hwndSource.RootVisual = containerElement;
            Win32.ConvertToChildWindow(hwndSource.Handle);
            return hwndSource;
        }

        public override void Load(string doc)
        {
            throw new InvalidOperationException("Not supported on controls.");
        }

        public override string Save()
        {
            throw new InvalidOperationException("Not supported on controls.");
        }

        public override void InvokeClosed()
        {
            throw new InvalidOperationException("Not supported on controls.");
        }

        public override bool InvokeClosing()
        {
            throw new InvalidOperationException("Not supported on controls.");
        }

        public override void InvokeClosedQuietly()
        {
            throw new InvalidOperationException("Not supported on controls.");
        }

        public override void Receive(byte[] payload)
        {
            throw new InvalidOperationException("Not supported on controls.");
        }

        internal string GetXaml()
        {
            try
            { 
                return XamlWriter.Save(FrameworkElement);
            }
            catch
            { 
                return null;
            }
        }
    }
}
