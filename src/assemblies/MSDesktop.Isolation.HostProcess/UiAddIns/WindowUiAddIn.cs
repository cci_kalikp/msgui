﻿using System;
using System.ServiceModel;
using System.Windows;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.Factories;
using MSDesktop.Isolation.Proxies;

namespace MSDesktop.Isolation.UiAddIns
{
    /// <summary>
    /// This class is used for subsystem isolation functionality internally and is not intended to be used from your code.
    /// Its instances are instantiated in the context of the isolated process and are used to communicate with the host.
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, UseSynchronizationContext = false)]
    internal class WindowUiAddIn : ViewUiAddinBase
    {
        private readonly IChromeElementInitializerRegistry _chromeRegistry;
        private readonly IWindowViewContainer _container;
        private readonly ChromeManagerProxy _chromeManagerProxy;

        internal WindowUiAddIn(IChromeElementInitializerRegistry chromeRegistry,
                               IWindowViewContainer container, ChromeManagerProxy chromeManagerProxy)
            : this(null, chromeRegistry, container, chromeManagerProxy)
        {
            _container.ContentChanged += _container_ContentChanged;
        }

        internal WindowUiAddIn(FrameworkElement userControl, IChromeElementInitializerRegistry chromeRegistry, IWindowViewContainer container, ChromeManagerProxy chromeManagerProxy)
            : base(userControl)
        {
            _container = container;
            _chromeManagerProxy = chromeManagerProxy;
            _chromeRegistry = chromeRegistry;
        }

        protected override void PostInitialize()
        {
            base.PostInitialize();
            HookupKeyboardNavigation(HwndSource);
        }

        public override bool InvokeClosing()
        {
            var args = ((WindowViewContainer) _container).InvokeClosing();
            return args.Cancel;
        }

        public override void InvokeClosed()
        {
            ((WindowViewContainer)_container).InvokeClosed();  
            _chromeManagerProxy.RemoveAddIn(this);
        }

        public override void InvokeClosedQuietly()
        {
            ((WindowViewContainer)_container).InvokeClosedQuietly();
            _chromeManagerProxy.RemoveAddIn(this);
        }

        public override void Load(string doc)
        {
            throw new InvalidOperationException("You can use Load operation only on isolated view");
        }

        public override string Save()
        {
            try
            {
                var document = HwndSource.Dispatcher.Invoke((Func<string>) SaveInternal);
                if (document != null)
                {
                    return document.ToString();
                }
            }
            catch (Exception exc)
            {
                //MessageBox.Show(exc.ToString());
            }

            return null;
        }

        private void _container_ContentChanged(object sender, ContentChangedEventArgs e)
        {
            FrameworkElement = (FrameworkElement) e.NewContent;
        }

        private string SaveInternal()
        {
            WindowFactoryHolder creator;
            if (_chromeRegistry.TryGetWindowFactoryHolder(_container.FactoryID, out creator))
            {
                // Compose the isolated state from actual state of the window 
                // and a factory ID element
                XDocument newDocument;
                if (creator.DehydrateHandler != null)
                {
                    var internalState = creator.DehydrateHandler(_container);
                    if (internalState != null)
                    {
                        newDocument = new XDocument(new XElement(
                                                        "IsolatedView",
                                                        new XAttribute("FactoryId", _container.FactoryID),
                                                        internalState.Root));
                        return newDocument.ToString();
                    }
                }

                newDocument = new XDocument(new XElement( "IsolatedView",  new XAttribute("FactoryId", _container.FactoryID)));
                return newDocument.ToString();
            }

            return null;
        }

        internal IWindowViewContainer ViewContainer
        {
            get { return _container; }
        }
    };
}

//namespace
