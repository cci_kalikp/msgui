﻿using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MSDesktop.Isolation.AddInProxy;
using MSDesktop.Isolation.Proxies;
using IsolatedSubsystemSettings = MSDesktop.Isolation.HostContracts.IsolatedSubsystemSettings;

namespace MSDesktop.Isolation
{
    internal static class ProfileLoadUtility
    {
        internal static void ReloadProfile(IUnityContainer container, string profileName, long layoutFileHandle,
                                           int layoutFileSize)
        {
            // Reconfigure the storage
            InitializeStorage(container, profileName, layoutFileHandle, layoutFileSize);

            // Reload and apply the layout
            if (!string.IsNullOrEmpty(profileName))
            {
                var profileService = container.Resolve<PersistenceProfileService>();
                profileService.LoadProfile(profileName);

                var chromeManager = (ChromeManagerProxy)container.Resolve<IChromeManager>();
                chromeManager.FireLayoutLoaded();
            }
        }

        internal static void InitializeStorage(IUnityContainer container, string profileName, long layoutFileHandle,
                                          int layoutFileSize)
        {
            // Set the profile name
            if (!string.IsNullOrEmpty(profileName))
            {
                var profileService = container.Resolve<PersistenceProfileService>();
                profileService.SetCurrentProfile(profileName);
            }

            if (layoutFileHandle != 0)
            {
                var layout = CommunicationMappedFile.ReadString(layoutFileHandle, layoutFileSize);

                // Set the layout
                if (!string.IsNullOrEmpty(layout))
                {
                    var storage = container.Resolve<IsolatedPersistenceStorage>();
                    storage.SetLayout(layout);
                }
            }
        }

        internal static void InitializeStorage(IUnityContainer container, IsolatedSubsystemSettings settings)
        {
            InitializeStorage(container, settings.ProfileName, settings.LayoutFileHandle, settings.LayoutFileSize);
        }
    }
}
