using System;
using System.ComponentModel;
using System.IO;
using System.Security;
using System.Security.Permissions;
using MSDesktop.Isolation.HostContracts;

namespace MSDesktop.Isolation
{
    [EditorBrowsable(EditorBrowsableState.Never)]
    public static class IsolatedSubsystemStarterFactory
    {
        private const string DefaultIsolatedSubsystemConfigFile = @"subsystem.config";

        public static IIsolatedSubsystemStarter CreateStarter(IsolatedSubsystemSettings settings,
                                                              string[] msdeConfigFiles)
        {
            var isolatedSubsystemLocation = settings.IsolatedSubsystemLocation;
            var applicationName = settings.ApplicationName;
            var isolatedSubsystemConfigFile = settings.IsolatedSubsystemConfigFile;

            IIsolatedSubsystemStarter starter;

            // If we have a location for isolated subsystem, and it's not the same as current base path,
            // then we need an appdomain.
            var subsystemLocationTrimmed = TrimPath(isolatedSubsystemLocation);
            var baseDirectoryTrimmed = TrimPath(AppDomain.CurrentDomain.BaseDirectory);
            bool differentDomain;

            if (!string.IsNullOrEmpty(isolatedSubsystemLocation) &&
                !string.Equals(subsystemLocationTrimmed, baseDirectoryTrimmed, StringComparison.OrdinalIgnoreCase))
            {
                // Initial set up appdomain
                var domainSetup = new AppDomainSetup
                    {
                        ApplicationName = applicationName,
                        LoaderOptimization = LoaderOptimization.MultiDomainHost,
                        ApplicationBase = isolatedSubsystemLocation,
                    };

                // Set up domain config file
                var subsystemConfigFileName = string.IsNullOrEmpty(isolatedSubsystemConfigFile)
                                                  ? DefaultIsolatedSubsystemConfigFile
                                                  : isolatedSubsystemConfigFile;

                var configFilePath = Path.Combine(isolatedSubsystemLocation, subsystemConfigFileName);
                if (!File.Exists(configFilePath))
                {
                    configFilePath = null;
                }
                else
                {
                    domainSetup.DisallowCodeDownload = true;
                }

                if (!string.IsNullOrEmpty(configFilePath))
                {
                    domainSetup.ConfigurationFile = configFilePath;
                }

                // Create the domain. If we are using AR, default config file 
                // will have the path to the Resolver capable handling the msde configs.
                var domain = AppDomain.CreateDomain("Isolated Subsystem Domain", null, domainSetup,
                                                    new PermissionSet(PermissionState.Unrestricted));
                var starterType = typeof (IsolatedSubsystemStarter);

                // Fetch this assembly into the new appdomain - otherwise when instantiating starter 
                // Fusion will try to resolve it based on appdomain config, but we don't want it to.
                // domain.Load(starterType.Assembly.GetName());
                // Create the starter in it
                starter = (IIsolatedSubsystemStarter) domain.CreateInstanceFromAndUnwrap(
                    starterType.Assembly.Location,
                    starterType.FullName);
                differentDomain = true;
            }
            else
            {
                starter = new IsolatedSubsystemStarter();
                differentDomain = false;
            }

            var subsystemDomain = differentDomain ? AppDomain.CurrentDomain : null;
            starter.Initialize(subsystemDomain, msdeConfigFiles);
            return starter;
        }

        private static string TrimPath(string path)
        {
            if (path == null) return null;

            path = Path.GetFullPath(path).Trim();
            if (path[path.Length - 1] == '\\')
            {
                path = path.Substring(0, path.Length - 1);
            }

            return path;
        }
    }
}
