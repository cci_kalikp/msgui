﻿using System;
using System.Diagnostics;
using System.Linq;

namespace MSDesktop.Isolation
{
    internal static class ProcessMain
    {
        internal static void IsolatedProcessMain(string[] args)
        {
            if (args.Length > 0)
            {
                try
                {
                    if (args.Any(parameter => string.Equals(parameter, @"/debug", StringComparison.OrdinalIgnoreCase)))
                    {
                        Debugger.Launch();
                    }

                    // args[0] is expected to be the URI of the running remotable object implementing IIsolatedSubsystemSite.
                    var settingsUri = args[0];
                    IsolatedSubsystemProcess.StartIsolatedSubsystem(settingsUri);
                }
                catch (Exception exc)
                {
                    Debug.WriteLine(exc.ToString());
                    LaunchDebugger();
                }
            }
        }

        private static void LaunchDebugger()
        {
#if DEBUG
            // Debugger.Launch();
#endif
        }
    }
}
