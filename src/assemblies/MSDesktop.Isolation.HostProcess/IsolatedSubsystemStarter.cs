using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using MorganStanley.MSDotNet.Runtime;
using MSDesktop.Isolation.HostContracts;
using MSDesktop.Isolation.Interfaces;
using ProcessIsolationUtility = MorganStanley.MSDotNet.MSGui.Impl.ProcessIsolation.ProcessIsolationUtility;

namespace MSDesktop.Isolation
{
    internal sealed class IsolatedSubsystemStarter : MarshalByRefObject, IIsolatedSubsystemStarter
    {
        private const string DefaultArConfigFile = @"subsystem.msde.config";
        private readonly ManualResetEventSlim _initializationEvent = new ManualResetEventSlim();

        private string _subsystemExecuterDirectory;
        private volatile ProfileFileInfo _profileFileInfo;
        private volatile IsolatedApplication _application;

        public void Initialize(AppDomain startupDomain, string[] msdeConfigFiles)
        {
            _subsystemExecuterDirectory = startupDomain == null ? null : startupDomain.BaseDirectory;
            InitializeAssemblyResolution(startupDomain, msdeConfigFiles);
        }

        public void Start(string uri, IsolatedSubsystemSettings settings)
        {
            if (settings.IsHostAppInitialized)
            {
                _profileFileInfo = new ProfileFileInfo(settings.LayoutFileHandle, settings.LayoutFileSize,
                                                       settings.ProfileName);
                _initializationEvent.Set();
            }

            StartInternal(uri, settings);
        }

        public void ProceedWithInitialization(long profileFileHandle, int fileLength, string profileName)
        {
            _profileFileInfo = new ProfileFileInfo(profileFileHandle, fileLength, profileName);
            _initializationEvent.Set();
        }

        public ProfileFileInfo WaitForInitialize()
        {
            _initializationEvent.Wait();
            return _profileFileInfo;
        }

        public void ReloadProfile(string currentProfile, long layoutFileHandle, int layoutFileSize)
        {
            if (_application != null)
            {
                _application.ReloadProfile(currentProfile, layoutFileHandle, layoutFileSize);
            }
        }

        public void CreateWindow(string factoryId, string viewId)
        {
            _application.CreateRemoteWindow(factoryId, viewId);
        }

        private void StartInternal(string uri, IsolatedSubsystemSettings settings)
        {
            var normalExit = false;
            var site = ProcessIsolationUtility.InitClientChannel<IMSDesktopSubsystemSite>(uri);
            var thread = new Thread(data =>
                {
                    try
                    {
                        Thread.CurrentThread.IsBackground = true;

                        var hostProcessId = ((IsolatedSubsystemSettings)data).HostProcessId;
                        Process.GetProcessById(hostProcessId).WaitForExit();
                        if (!normalExit)
                        {
                            Debug.WriteLine("WARNING: Host process quit unexpectedly.");
                            Environment.Exit(2);
                        }
                    }
                    catch (Exception)
                    {
#if DEBUG
                        //Debugger.Launch();
#endif
                        //MessageBox.Show(exc.ToString());
                    }
                });

            thread.Start(settings);

            try
            {
                _application = new IsolatedApplication(this, site, settings, _subsystemExecuterDirectory);
                _application.Run();
                normalExit = true;
            }
            catch (Exception)
            {
#if DEBUG
                Debugger.Launch();
#endif 
            }

        }


        [MethodImpl(MethodImplOptions.NoInlining)]
        internal static void InitializeAssemblyResolution(AppDomain startupDomain, string[] msdeConfigFiles = null)
        {
            if (startupDomain != null)
            {
                if (!TryInitAR(AppDomain.CurrentDomain, true, msdeConfigFiles))
                {
                    TryInitAR(startupDomain, false, msdeConfigFiles);
                }
            }
            else
            {
                TryInitAR(AppDomain.CurrentDomain, false, msdeConfigFiles);
            }

            if (startupDomain != null)
            {
                AssemblyResolver.OnPreAssemblyResolve += OnPreAssemblyResolve;
            }
        }

        private static Assembly OnPreAssemblyResolve(object sender, ResolveEventArgs args, string path)
        {
            return AppDomain.CurrentDomain.GetAssemblies().
                             FirstOrDefault(
                                 asm =>
                                 asm.FullName.Equals(args.Name, StringComparison.OrdinalIgnoreCase));
        }

        private static bool TryInitAR(AppDomain domain, bool suppressException, string[] msdeConfigFiles)
        {
            const string defaultMsdeConfigFileName = @"MSDesktop.Isolation.msde.config";
            if (msdeConfigFiles == null || msdeConfigFiles.Length == 0)
            {
                // Probe for the msde.config file - first in base directory, then look MSDotNet.MSGui.msde.config 
                // in current directory and in ..\..\assemblies
                var defaultConfigFile = Path.Combine(domain.BaseDirectory, DefaultArConfigFile);
                if (!File.Exists(defaultConfigFile))
                {
                    defaultConfigFile = Path.Combine(domain.BaseDirectory, defaultMsdeConfigFileName);
                    if (!File.Exists(defaultConfigFile))
                    {
                        // The last probe will be left to the condition outside
                        var assemblyPath = new FileInfo(Assembly.GetExecutingAssembly().Location).Directory.FullName;
                        defaultConfigFile = Path.Combine(assemblyPath, defaultMsdeConfigFileName);
                    }
                }

                return LoadMsdeFileIfExists(defaultConfigFile, suppressException);
            }

            foreach (var msdeConfigFile in msdeConfigFiles)
            {
                LoadMsdeFileIfExists(msdeConfigFile, suppressException);
            }

            return true;
        }

        private static bool LoadMsdeFileIfExists(string msdeConfigFile, bool suppressException)
        {
            try
            {
                if (File.Exists(msdeConfigFile))
                {
                    AssemblyResolver.Load(msdeConfigFile);
                    return true;
                }
            }
            catch
            {
                if (!suppressException) throw;
            }

            return false;
        }

        public void Shutdown()
        {
            ThreadPool.QueueUserWorkItem(ShutdownInternal);
        }

        private static void ShutdownInternal(object data)
        {
            // Close all server channels
            HostContracts.ProcessIsolationUtility.ShutdownServerChannels();

            // Shutdown the application
            var isolatedApplication = (IsolatedApplication)Application.Current;
            isolatedApplication.Dispatcher.BeginInvokeShutdown(DispatcherPriority.Normal);
        }
    }
}
