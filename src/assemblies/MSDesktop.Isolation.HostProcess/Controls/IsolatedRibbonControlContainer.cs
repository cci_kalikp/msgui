﻿using System;
using System.Reflection;
using System.Windows;
using Infragistics.Windows.Ribbon; 

namespace MSDesktop.Isolation.Controls
{
    internal sealed class IsolatedRibbonControlContainer : IsolatedControlContainer 
    {
        public IsolatedRibbonControlContainer()
        {
            this.Orientation = System.Windows.Controls.Orientation.Vertical;
            this.VerticalAlignment = VerticalAlignment.Stretch;
        }
        protected override void OnVisualChildrenChanged(DependencyObject visualAdded, DependencyObject visualRemoved)
        {
            base.OnVisualChildrenChanged(visualAdded, visualRemoved);

            // Check to see what element did we add
            // If it was a button tool, we need to set its Location dependency property to Ribbon.
            var buttonTool = visualAdded as ButtonTool;
            if (buttonTool != null)
            {
                object boxedLocationValue = ToolLocation.Ribbon;

                // Get property key
                var fieldInfo = typeof (XamRibbon).GetField(
                    "LocationPropertyKey",
                    BindingFlags.GetField | BindingFlags.Static | BindingFlags.NonPublic);

                if (fieldInfo != null)
                {
                    var value = fieldInfo.GetValue(null);
                    buttonTool.SetValue((DependencyPropertyKey)value, boxedLocationValue);
                }
            }
        }
    }
}
