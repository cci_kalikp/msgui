﻿ 
using System.Windows;
using System.Windows.Controls; 
using MorganStanley.MSDotNet.MSGui.Core;

namespace MSDesktop.Isolation.Controls
{
    internal class IsolatedControlContainer : StackPanel
    {
        private Size _desiredSize = new Size(0, 0);

        public void SetDesiredSize(Size size)
        {
            _desiredSize = size;
            Children[0].InvalidateMeasure();
            Children[0].InvalidateArrange();
        }

        protected override Size MeasureOverride(Size constraint)
        {
            if (CoreUtilities.AreClose(_desiredSize.Width, 0)
                && CoreUtilities.AreClose(_desiredSize.Height, 0))
            {
                var baseSize = base.MeasureOverride(constraint);
                return baseSize;
            }

            return _desiredSize;
        }

        protected override Size ArrangeOverride(Size arrangeSize)
        {
            Children[0].Arrange(new Rect(arrangeSize));
            return arrangeSize;
        }


    }
}
