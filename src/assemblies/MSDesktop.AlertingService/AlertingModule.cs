﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml.Linq;
using Infragistics.Windows.Controls;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Unity;

using MorganStanley.Desktop.GUI.Controls.TaskbarIcon;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers.SystemTray;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.ViewContainers;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Controls;

using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;

using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Composite.Presentation.Events;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;


namespace MorganStanley.Desktop.AlertingService
{
    public class AlertingModule : IModule
    {   
        private readonly IChromeManager _chromeManager;
        private readonly IChromeRegistry _chromeRegistry;
        
        

     /*   private AlertToastStack _toastStack;

        private readonly AlertStackViewModel _alertStack = new AlertStackViewModel();*/

        private readonly ICommunicator _moduleCommunicator;

        private IObservable<AlertMessage> _source;

        private readonly HashSet<INotifiable> _notificationReceivers = new HashSet<INotifiable>();


        public AlertingModule(IUnityContainer container, IChromeManager chromeManager, IChromeRegistry chromeRegistry, ICommunicator moduleCommunicator, IpcModuleLoadHacker shellWrapper)
        {
            _chromeManager = chromeManager;
            _chromeRegistry = chromeRegistry;
            _moduleCommunicator = moduleCommunicator;

            container.RegisterInstance(this, new ContainerControlledLifetimeManager());

        }


        public void SetDefaultMainTagAction(Action<AlertMessage> addTag = null, Action<AlertMessage> removeTag = null)
        {
            
        }

      /*  public void SetMainTag(string tag, ImageSource hasTagIcon, ImageSource noTagIcon, 
            Action<AlertMessage> addTag = null, Action<AlertMessage> removeTag = null)
        {
            _alertStack.SetMainTag(tag, hasTagIcon,noTagIcon, addTag, removeTag);
        }


        public void AddSupportedTag(string tag, ImageSource hasTagIcon, ImageSource noTagIcon,
            Action<AlertMessage> addTag = null, Action<AlertMessage> removeTag = null)
        {
            _alertStack.AddSupportedTag(tag, hasTagIcon, noTagIcon, addTag, removeTag);
        }*/


        public void AddNotificationReceiver(INotifiable notifiable)
        {
            _notificationReceivers.Add(notifiable);
        }

        public bool RemoveNotificationReceiver(INotifiable notifiable)
        {
            return _notificationReceivers.Remove(notifiable);
        }

        public void Initialize()
        {
            _chromeRegistry.RegisterWindowFactory("AlertLaunchBar", AlertLaunchBar);
            _chromeManager.CreateChrome(false);
          


           /*var alertTray =  _chromeManager.AddTrayIcon("NotificationTray", new InitialTrayIconParameters()
                                                               {
                                                                   Visibility = Visibility.Visible
                                                               });*/

           
           

            _source = from msg in _moduleCommunicator.GetSubscriber<AlertMessage>().GetObservable()
                         select msg;

            _source.Subscribe((x) =>
                                  {
                                      SendAlert(x);
                                      //_toastStack.Dispatcher.Invoke(new Alert(SendAlert), new []{x});
                                      
                                  });

            var window = _chromeManager.CreateWindow("AlertLaunchBar", new InitialWindowParameters()
                                                              {
                                                                  Singleton = true
                                                              });


           // _systemTrayNotifier = new SystemTrayNotifier(_chromeManager);
            //_systemTrayNotifier.Initialize();


            
            //_systemTrayNotifier.AddSupportedType<AlertMessage>("Template");
            //_systemTrayNotifier.AddSupportedType<AlertMessage, Notification>();
            //_systemTrayNotifier.AddSupportedType<AlertMessageViewModel, AlertDetailBallon >();

            

            /*  _chromeRegistry.RegisterWidgetFactory("Create new window");
            

            var widget = new InitialShowWindowButtonParameters
            {
                ToolTip = "Create new window",
                Image = BitmapFrame.Create(
                     new Uri(@"pack://application:,,,/Images/Info.png",
                         UriKind.RelativeOrAbsolute)),
                WindowFactoryID = "AlertLaunchBar"
            };

            _chromeManager.PlaceWidget("Create new window",
                LauncherBarChromeManager.LAUNCHERBAR_COMMANDAREA_NAME,
                "",
                widget);*/



            //toolwindow.HeaderVisibility = Visibility.Collapsed;
        }


        private delegate void Alert(AlertMessage message);

       /* private void DismissAlert(AlertMessage message)
        {
            var vm = _alertStack.Alerts.FirstOrDefault((v) => v.Alert == message);
            _alertStack.RemoveAlert(vm);
        }*/

        void SendAlert(AlertMessage message)
        {
            //_systemTrayNotifier.Display(message, null);

           
            //_systemTrayNotifier.Notify(message);
            foreach (var notifiable in _notificationReceivers)
            {
                notifiable.Notify(message);
            }
            
            //_alertStack.AddAlert(message);
            Console.WriteLine("Recieved alert");
        }

        bool AlertLaunchBar(IWindowViewContainer windowViewContainer, XDocument state)
        {

            //windowViewContainer.Content = _toastStack;
            windowViewContainer.Title = "Notifications";
            
            return true;
        }
    }
}
