﻿namespace MorganStanley.Desktop.AlertingService
{
    public class AlertItemViewModel
    {
        public string Category { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
