﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Composite.Presentation.Events;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;

namespace MorganStanley.Desktop.AlertingService
{
    public class RuleEngineBase : IDisposable
    {

        private readonly Dictionary<Type, HashSet<object>> _rules = new Dictionary<Type, HashSet<object>>();
        private IRuleParser _ruleParser;

        private readonly ICommunicator _commnicator;

        private readonly Dictionary<Type, object> _observables = new Dictionary<Type, object>();

        public RuleEngineBase(ICommunicator communicator)
        {
            _commnicator = communicator;
        }

        public IRuleParser RuleParser { get { return _ruleParser; } set { _ruleParser = value; } }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEvent"></typeparam>
        /// <param name="statement"></param>
        /// <param name="alertConverter"></param>
        /// <returns>null if statement is invalid</returns>
        public IRule<TEvent> RegisterStatement<TEvent>(string statement)
        {
            var rule =  _ruleParser.ParseRule<TEvent>(statement);
            if(!_rules.ContainsKey(typeof(TEvent)))
            {
                _rules[typeof(TEvent)] = new HashSet<object>();
            }
            _rules[typeof (TEvent)].Add(rule);

            
            if (!_observables.ContainsKey(typeof(TEvent)))
            {
                var observable =
                    _commnicator.GetSubscriber<TEvent>().GetObservable();
                observable.Subscribe(OnRecieve);
                _observables[typeof(TEvent)] = observable;
            }
                

            return rule;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEvent"></typeparam>
        /// <param name="rule"></param>
        /// <returns>false if rule does not exist</returns>
        public bool UnregisterRule<TEvent>(IRule<TEvent> rule)
        {
            var res = false;
            if (_rules.ContainsKey(typeof(TEvent)))
            {
                res = _rules[typeof (TEvent)].Remove(rule);
                if (_rules[typeof(TEvent)].Count == 0)
                {
                    _observables.Remove(typeof (TEvent));
                    _rules.Remove(typeof(TEvent));
                }
                    
            }
            return res;
        }



        public IEnumerable<IRule<TEvent>> GetRules<TEvent>()
        {
            return _rules.ContainsKey(typeof (TEvent))
                       ? from rule in _rules[typeof (TEvent)]
                         select rule as IRule<TEvent>
                       : Enumerable.Empty<IRule<TEvent>>();
        }

       
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEvent"></typeparam>
        /// <returns>true if there are rules of type TEvent in running status</returns>
        public bool IsActive<TEvent>()
        {
            return _rules.ContainsKey(typeof (TEvent))
                       ? (from rule in _rules[typeof (TEvent)]
                          where ((IRule<TEvent>) rule).IsRunning
                          select rule).Count() > 0
                       : false;
        }


        public bool IsSupported<TEvent>()
        {
            return _rules.ContainsKey(typeof (TEvent)) && _rules[typeof (TEvent)].Count > 0;
        }

        public IEnumerable<Type> SupportedEventTypes
        {
            get
            {
                return from pair in _rules
                       where pair.Value.Count > 0
                       select pair.Key;

            }
        }


        // returns true if the rules engine has any rule defined on the given type, which has a constraint “type.propertyName = propertyValue”; false otherwise
        // (it can be used to a similar purpose as above, when the type is generic and represents several ‘kinds’ - which the given property’s value differentiates)
        public bool HasRuleWithConstraint<TEvent>(IRuleConstraint constraint)
        {
            return GetRulesWithConstraint<TEvent>(constraint).Count() > 0;
        }

        public IEnumerable<IRule<TEvent>> GetRulesWithConstraint<TEvent>(IRuleConstraint constraint)
        {
            return _rules.ContainsKey(typeof (TEvent))
                       ? from rule in _rules[typeof (TEvent)]
                         where ((IRule<TEvent>) rule).Constraints.Contains(constraint)
                         select rule as IRule<TEvent>
                       : Enumerable.Empty<IRule<TEvent>>();
        }

        protected virtual void OnRecieve<TEvent>(TEvent message)
        {
            if(!_rules.ContainsKey(typeof(TEvent)))
                return;

            foreach (var ruleObj in _rules[typeof(TEvent)])
            {
                var rule = (IRule<TEvent>) ruleObj;
                var alert = rule.Convert(message);
                if (alert != null)
                {
                    Console.WriteLine("Publish alert: " +  alert.Description);
                    _commnicator.GetPublisher<AlertMessage>().Publish(alert);
                }
                    
            }
        }

        public void Dispose()
        {
           _observables.Clear();
        }
    }

    public interface IRuleParser
    {
        IRule<TEvent> ParseRule<TEvent>(string statement);
        bool IsValidStatement(string statement);
    }
}
