﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.Desktop.AlertingService
{
    /// <summary>
    /// Will monitor TEvent and generate AlertMessage
    /// </summary>
    /// <typeparam name="TEvent"></typeparam>
    public interface IRule<TEvent> : IEquatable<IRule<TEvent>>
    {
        bool IsRunning { get; }
        void Suspend();
        void Resume();

        IEnumerable<IRuleConstraint> Constraints { get; }

        /// <summary>
        /// Null means the input object not trigger the rule
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        AlertMessage Convert(TEvent obj);
    }

   

}
