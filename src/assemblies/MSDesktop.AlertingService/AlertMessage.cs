﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using MorganStanley.Desktop.EntityServices;

namespace MorganStanley.Desktop.AlertingService
{

    public enum AlertLevel
    {
        Critical = 0, Normal = 1, LowPriority = 2
    }

    public class AlertMessage : IDisposable
    {
        private readonly string _id;
        public string Id
        {
            get { return _id; }
        }

        public AlertLevel Level { get; set; }
        //public string CustomizedCategory { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime AlertTime { get; private set; }

        private TimeSpan _liveTimeSpan;
        internal TimeSpan LiveTimeSpan
        {
            get { return _liveTimeSpan; }
            set
            {
                _liveTimeSpan = value;
                if (_timer != null)
                {
                    _timer.Close();
                    _timer = null;
                }

                if(value.TotalMilliseconds <=0)
                    return;
                _timer = new Timer(value.TotalMilliseconds);
                _timer.Start();
                _timer.Elapsed += (s, e) =>
                                      {
                                          _timer.Close();
                                          _timer = null;
                                          var copy = OnExpired;
                                          if (copy != null)
                                          {
                                              copy(this, new EventArgs());
                                          }
                                      };
            }
        }

        private Timer _timer;

        public object AdditionalInfo { get; set; }

        public AlertMessage()
        {
            _id = "Alert_" + Guid.NewGuid().ToString("d");
            AlertTime = DateTime.Now;

            OnClosed += (s, e) => ReleaseTimer();
        }

        public AlertMessage(TimeSpan expireTimeSpan): this()
        {
            LiveTimeSpan = expireTimeSpan;
        }


        private bool _isClosed;
        public bool IsClosed { get { return _isClosed; } }

        public EventHandler OnExpired;
        public EventHandler<CloseAlertEventArgs> OnClosed;


        internal void Close(AlertClosingStatus closingStatus)
        {
            var copy = OnClosed;
            if (copy != null)
            {
                copy(this, new CloseAlertEventArgs() {Status = closingStatus, AlertMessage = this});
            }

            _isClosed = true;
        }

        private void ReleaseTimer()
        {
            if (_timer != null)
            {
                _timer.Close();
                _timer = null;
            }
        }

        public void Dispose()
        {
           ReleaseTimer();
        }


        public enum AlertClosingStatus
        {
            Expired, Omitted, ShouldHandle
        }

        public class CloseAlertEventArgs: EventArgs
        {
            public AlertClosingStatus Status { get; internal set; }
            public AlertMessage AlertMessage { get; internal set; }
        }
    }
}
