﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.Desktop.AlertingService
{
    public interface INotifiable
    {
        void Notify<TMessage>(TMessage message);
        IEnumerable<Type> GetSupportedTypes();
        bool IsTypeSupported<TMessage>();

    }
}
