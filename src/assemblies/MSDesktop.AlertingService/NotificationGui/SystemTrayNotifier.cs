﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Unity;
using MorganStanley.Desktop.GUI.Controls.TaskbarIcon;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers.SystemTray;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.ViewContainers;
using MorganStanley.MSDotNet.MSLog;

namespace MorganStanley.Desktop.AlertingService.NotificationGui
{
    public class SystemTrayNotifier : IModule, INotifiable
    {
        private readonly Dictionary<Type, DataTemplate> _templates = new Dictionary<Type, DataTemplate>();
        private readonly Dictionary<Type, Type> _guis = new Dictionary<Type, Type>();

        private readonly IChromeManager _chromeManager;
        private ITrayIconViewContainer _taskTray;
        private TaskbarIcon _taskIcon;

        private TimeSpan _timeout;

        private const int DefaultTimeOutInSeconds = 3;

        public SystemTrayNotifier(IUnityContainer container, IChromeManager chromeManager)
        {
            _chromeManager = chromeManager;
            container.RegisterInstance(this, new ContainerControlledLifetimeManager());

            _timeout = TimeSpan.FromSeconds(DefaultTimeOutInSeconds);
        }

        public void Initialize()
        {
            _taskTray = _chromeManager.AddTrayIcon("NotificationTray", new InitialTrayIconParameters
            {
                Visibility = Visibility.Collapsed
                
            });

            _taskIcon = _taskTray.Content as TaskbarIcon;

        }

        public void AddSupportedType<TMessage, TGui>() where TGui : FrameworkElement, new ()
        {
            _guis[typeof (TMessage)] = typeof (TGui);
        }


        public void AddSupportedType<TMessage>(string templatekey)
        {
            var template = Application.Current.TryFindResource(templatekey) as DataTemplate;

            if (template == null)
            {
                throw new ApplicationException("No template is defined in Applicaton resource dictionary with key " + templatekey );
            }

            if (_templates.ContainsKey(typeof(TMessage)))
                throw new ApplicationException("Type is already registered");



            _templates[typeof(TMessage)] = template;
        }

        public void AddSupportedType<TMEssage>(DataTemplate template)
        {

            if (_templates.ContainsKey(typeof(TMEssage)))
                throw new ApplicationException("Type is already registered");

            _templates[typeof(TMEssage)] = template;
        }

        public bool RemoveSupportedType<T>()
        {
            return _templates.Remove(typeof (T));
        }

        private void InvokeNotify<TMessage>(TMessage message)
        {
            var msgType = typeof(TMessage);
            if (!_templates.ContainsKey(msgType) && !_guis.ContainsKey(msgType))
            {
                _taskIcon.ShowBalloonTip("Not supported message type", message.ToString(), BalloonIcon.Error);
                var error = MSLog.Error();
                error.Message = "Not supported message type: " + msgType.FullName;
                error.Send();
                return;
            }


            var timeout = _timeout.TotalMilliseconds > 500 ? (int)_timeout.TotalMilliseconds : 500;

            if (_guis.ContainsKey(msgType))
            {
                var content = (FrameworkElement)Activator.CreateInstance(_guis[msgType], new object[] { });
                var ballon = new ContentWrapper{DataContext = message};
                ballon.grid.Children.Add(content);
                _taskIcon.ShowCustomBalloon(ballon, PopupAnimation.Fade, 3000);
            }
            else
            {
                //var vm = new WrapperViewModel(this, message);
                var ballon = new ContentWrapper();
                var contentControl = new ContentControl {ContentTemplate = _templates[msgType], Content = message};
                ballon.grid.Children.Add(contentControl);
                _taskIcon.ShowCustomBalloon(ballon, PopupAnimation.Fade, 3000);
            }
            //_taskIcon.Visibility = Visibility.Visible;
        }

        private delegate void Display<in TMessage>(TMessage message);

        public void Notify<TMessage>(TMessage message)
        {
            _taskIcon.Dispatcher.Invoke(new Display<TMessage>(InvokeNotify), message);
        }

        public void SetTimeOut(TimeSpan timeout)
        {
            _timeout = timeout;
        }

        public IEnumerable<Type> GetSupportedTypes()
        {
            return _templates.Keys.Union(_guis.Keys);
        }

        public bool IsTypeSupported<TMessage>()
        {
            return _templates.ContainsKey(typeof (TMessage)) || _guis.ContainsKey(typeof (TMessage));
        }
    }
}
