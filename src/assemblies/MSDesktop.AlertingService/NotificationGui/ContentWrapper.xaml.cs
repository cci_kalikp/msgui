﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MorganStanley.Desktop.GUI.Controls.TaskbarIcon;

namespace MorganStanley.Desktop.AlertingService.NotificationGui
{
    /// <summary>
    /// Interaction logic for ContentWrapper.xaml
    /// </summary>
    public partial class ContentWrapper : UserControl, IDisposable
    {

        private SystemTrayNotifier _systemTrayNotifier;
        private Timer _timer;

        private bool _isClosing;
        
        public ContentWrapper()
        {
            InitializeComponent();

            TaskbarIcon.AddBalloonClosingHandler(this, OnBalloonClosing);    
        }

        private void OnBalloonClosing(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
            _isClosing = true;
           /* var taskbarIcon = TaskbarIcon.GetParentTaskbarIcon(this);
            taskbarIcon.Visibility = Visibility.Collapsed;*/
        }

        private void grid_MouseEnter(object sender, MouseEventArgs e)
        {
            //if we're already running the fade-out animation, do not interrupt anymore
            //(makes things too complicated for the sample)
            if (_isClosing) return;

            if (_timer != null)
            {
                _timer.Close();
                _timer = null;
            }

            //the tray icon assigned this attached property to simplify access
            var taskbarIcon = TaskbarIcon.GetParentTaskbarIcon(this);
            taskbarIcon.ResetBalloonCloseTimer();
        }

        private void grid_MouseLeave(object sender, MouseEventArgs e)
        {
            //the tray icon assigned this attached property to simplify access
            var taskbarIcon = TaskbarIcon.GetParentTaskbarIcon(this);

            if (_timer != null)
            {
                _timer.Close();
                _timer = null;
            }
            
            _timer = new Timer(1000) { AutoReset = false};

            _timer.Elapsed += (s, args) =>
                                  {
                                      taskbarIcon.CloseBalloon();
                                      _timer.Close();
                                      _timer = null;
                                  };
            _timer.Start();

        }

        private void OnFadeOutCompleted(object sender, EventArgs e)
        {
            var pp = (Popup)Parent;
            pp.IsOpen = false;
        }

        public void Dispose()
        {
            if (_timer != null)
            {
                _timer.Close();
                _timer = null;
            }
        }
    }


   
}
