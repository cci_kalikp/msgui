﻿using System;
using MSDesktop.MessageRouting.Routing;

namespace MSDesktop.MessageRouting
{
    public interface IRoutingCommunicator
    {
        IRoutedPublisher<TMessage> GetPublisher<TMessage>();
        IRoutedSubscriber<TMessage> GetSubscriber<TMessage>();

        [Obsolete("Use Endpoint.LocalEndpoint instead.", true)]
        Endpoint GetLocalEndpoint();

        event EventHandler<MessagePublishedEventArgs> MessagePublished;
        event EventHandler<MessageDeliveredEventArgs> MessageDelivered;
        event EventHandler<MessageFailedEventArgs> MessageFailed;
        event EventHandler<NewSubscriptionEventArgs> NewSubscription; 
    }
}
