﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MSDesktop.MessageRouting.Routing;

namespace MSDesktop.MessageRouting
{
    public class MessageFailedEventArgs : EventArgs
    {
        public object Message { get; set; }
        public DeliveryInfo DeliveryInfo { get; set; }
    }
}
