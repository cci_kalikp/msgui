﻿using System;
using MSDesktop.MessageRouting.Routing;

namespace MSDesktop.MessageRouting
{
    public interface IRoutedPublisher<in TMessage>
    {
        DeliveryInfo Publish(TMessage message); 
        event EventHandler<MessagePublishedEventArgs> MessagePublished;
        event EventHandler<MessageDeliveredEventArgs> MessageDelivered;
        event EventHandler<MessageFailedEventArgs> MessageFailed;
    }
}
