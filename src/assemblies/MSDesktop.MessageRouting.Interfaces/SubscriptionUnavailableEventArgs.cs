﻿using System;
using MSDesktop.MessageRouting.Routing;

namespace MSDesktop.MessageRouting
{
    public sealed class SubscriptionUnavailableEventArgs : EventArgs
    {
        private readonly Endpoint _subscriberIdentity;

        public SubscriptionUnavailableEventArgs(Endpoint subscriberIdentity)
        { 
            _subscriberIdentity = subscriberIdentity;
        }

        public Endpoint SubscriberIdentity
        {
            get { return _subscriberIdentity; }
        }
         
    }
}