﻿using System;

namespace MSDesktop.MessageRouting
{
    public sealed class NewSubscriptionEventArgs : EventArgs
    {
        private readonly Type _messageType;

        public NewSubscriptionEventArgs(Type messageType)
        {
            _messageType = messageType;
        }

        public Type MessageType
        {
            get { return _messageType; }
        }
    }
}
