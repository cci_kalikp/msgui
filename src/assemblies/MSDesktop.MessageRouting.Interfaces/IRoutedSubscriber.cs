﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSDesktop.MessageRouting
{
    public interface IRoutedSubscriber<TMessage>
    {
        IObservable<TMessage> GetObservable();
        event EventHandler<FilterMessagEventArgs<TMessage>> FilterMessage;
    }

    public class FilterMessagEventArgs<TMessage>:EventArgs
    {
        public FilterMessagEventArgs(TMessage message_)
        {
            Message = message_;
        }

        public TMessage Message { get; private set; }
        public bool IsAcceptable { get; set; }
        public string ExtraInformation { get; set; }
    }
}
