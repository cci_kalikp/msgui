﻿using System;
using MSDesktop.MessageRouting.Routing;

namespace MSDesktop.MessageRouting
{
    public sealed class SubscriptionAvailableEventArgs : EventArgs
    {
        private readonly Endpoint _subscriberIdentity;
        private readonly Type _messageType;

        public SubscriptionAvailableEventArgs(Type messageType, Endpoint subscriberIdentity)
        {
            _messageType = messageType;
            _subscriberIdentity = subscriberIdentity;
        }

        public Endpoint SubscriberIdentity
        {
            get { return _subscriberIdentity; }
        }

        public Type MessageType
        {
            get { return _messageType; }
        }
    }
}
