﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace MSDesktop.MessageRouting.Routing
{
    public class DeliveryInfo : INotifyPropertyChanged
    {
        private Endpoint target;
        public Endpoint Target
        {
            get
            {
                return this.target;
            }
            internal set
            {
                this.target = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("Target"));
            }
        }

        private DeliveryStatus status;
        public DeliveryStatus Status
        {
            get
            {
                return this.status;
            }
            internal set
            {
                this.status = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("Status"));
            }
        }

        private int id;
        public int Id
        {
            get
            {
                return this.id;
            }
            internal set
            {
                this.id = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("Id"));
            }
        }

        public string ExtraInformation { get; set; }
        #region INotifyPropertyChanged implementation

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        #endregion
    }
}
