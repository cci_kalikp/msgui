﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSDesktop.MessageRouting.Routing
{
    public interface IEndpointSelector
    {
        /// <summary>
        /// Adds an element to the resolver chain.
        /// </summary>
        /// <param name="priority">Determines the order of execution of elements in the chain. Higher number means earlier execution.</param>
        /// <param name="step">The delegate performing the endpoint selection.</param>
        void AddEndpointSelectionStep(int priority, TargetingStep step);
        
        /// <summary>
        /// Resolve endpoint for a given message.
        /// </summary>
        /// <param name="message">The message to be sent.</param>
        /// <param name="attempt">Number of attempts for this message instance already passed. First run starts with 0.</param>
        /// <returns></returns>
        EndpointSelection ResolveTarget(object message, int attempt);

        int MaximumAttempts { get; set; }
    }

    public delegate EndpointSelection TargetingStep(EndpointSelectorChain chain);
}
