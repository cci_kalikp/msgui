﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSDesktop.MessageRouting.Routing
{
    public sealed class EndpointSelection
    {
        /// <summary>
        /// Gets or sets the determined endpoint.
        /// </summary>
        public Endpoint Endpoint { get; set; }

        /// <summary>
        /// Gets or sets whether the endpoint should be persisted (if available).
        /// </summary>
        public bool Persist { get; set; }
    }
}
