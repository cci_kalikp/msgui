﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSDesktop.MessageRouting.Routing
{
    public sealed class EndpointSelectorChain
    {
        private Stack<TargetingStep> chain; 
        internal Stack<TargetingStep> Chain
        {
            set { this.chain = value; }
        }

        public int Attempt { get; internal set; }
        public object Message { get; internal set; } 
        internal EndpointSelection Execute()
        {
            return this.Next();
        }

        public EndpointSelection Next()
        {
            TargetingStep step = chain.Pop();
            if (step == null) return null;
            return step(this);
        }
    }
}
