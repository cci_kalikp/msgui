﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSDesktop.MessageRouting.Routing
{
    public enum DeliveryStatus
    {
        Pending = 1,
        WaitingForAck = 2,
        Delivered = 3,
        Failed = 4,
        Fallback = 5,
        Rejected = 6,
    }
}
