﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSDesktop.MessageRouting.Routing
{
    /// <summary>
    /// Allows for messages that bypass the internal routing mechanism. Use for direct responses to messages.
    /// 
    /// NOTE: This interfaces replaces MSDesktop.MessageRouting.Routing.IBypassRouting.
    /// </summary>
    public interface IExplicitTarget
    {
        Endpoint Target { get; }
    }

}
