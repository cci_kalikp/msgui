﻿using System;

namespace MSDesktop.MessageRouting.Routing
{
    [Serializable]
    public sealed class Endpoint
    {
        public string Hostname { get; set; }
        public string Application { get; set; }
        public string Username { get; set; }
        public string InstanceName { get; set; }
        public string CpsServer { get; set; }
        public bool Kerberosed { get; set; }

        private bool Equals(Endpoint other)
        {
            return string.Equals(Hostname, other.Hostname)
                   && string.Equals(Application, other.Application)
                   && string.Equals(Username, other.Username)
                   && string.Equals(InstanceName, other.InstanceName);
        }

        #region static values

        private static Endpoint localEndpoint = null;
        private static Endpoint localBroadcast = null;
        private static Endpoint broadcast = null;

        /// <summary>
        /// The local endpoint including the current user, the hostname and the application identity.
        /// </summary>
        public static Endpoint LocalEndpoint
        {
            get
            {
                return localEndpoint;
            }
            internal set
            {
                if (localEndpoint != null)
                    throw new InvalidOperationException("Endpoint.LocalEndpoint can only be set once.");

                localEndpoint = value;
            }
        }

        /// <summary>
        /// Specifies an endpoint that targets all applications on the current machine only.
        /// </summary>
        public static Endpoint LocalBroadcast
        {
            get
            {
                if (localBroadcast == null)
                {
                    localBroadcast = new Endpoint
                    {
                        Username = localEndpoint.Username ,
                        Hostname = localEndpoint.Hostname
                    };
                }

                return localBroadcast;
            }
        }

        /// <summary>
        /// Specifies an endpoint that targets all applications on all machines the current user is logged on to.
        /// </summary>
        public static Endpoint Broadcast
        {
            get
            {
                if (broadcast == null)
                {
                    broadcast = new Endpoint { Username = localEndpoint.Username };
                }

                return broadcast;
            }
        }

        #endregion

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = (Hostname != null ? Hostname.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Application != null ? Application.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Username != null ? Username.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (InstanceName != null ? InstanceName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (CpsServer != null ? CpsServer.GetHashCode() : 0);
                return hashCode;
            }
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Endpoint)obj);
        }

        public override string ToString()
        {
            string description = string.Empty;
            if (!string.IsNullOrEmpty(Hostname)) description += string.Format("Host name: '{0}' ", Hostname);
            if (!string.IsNullOrEmpty(Application)) description += string.Format("Application: '{0}' ", Application);
            if (!string.IsNullOrEmpty(Username)) description += string.Format("User name: '{0}'", Username);
            if (!string.IsNullOrEmpty(InstanceName)) description += string.Format("Process id: {0}", InstanceName);
            if (!string.IsNullOrEmpty(CpsServer)) description += string.Format("CPS Server: {0}", CpsServer);
            return description;
        }

        public bool IsMatch(Endpoint other)
        {
            if (this.Equals(other))
                return true;

            // cross-machine broadcast with target app specified
            if (string.Equals(Username, other.Username)
                && other.Application == this.Application
                && other.Hostname == null)
            {
                return true;
            }

            // cross-machine broadcast without target app
            if (string.Equals(Username, other.Username)
                && other.Application == null
                && other.Hostname == null)
            {
                return true;
            }

            // explicit targeting of user/host/app - no instance tracking yet, will be introduced by Kraken.Lifecycle
            if (string.Equals(Hostname, other.Hostname)
                && string.Equals(Application, other.Application)
                && string.Equals(Username, other.Username)/*
                && other.InstanceName == null*/)
            {
                return true;
            }

            return false;
        }
    }
}
