﻿namespace MSDesktop.MessageRouting.Message
{
    public interface IProvideMessageInfo
    {
        string GetInfo();
    }
}
