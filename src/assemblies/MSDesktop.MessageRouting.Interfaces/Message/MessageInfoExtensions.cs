﻿using System;
using System.Collections.Generic;

namespace MSDesktop.MessageRouting.Message
{
    public static class MessageInfoExtensions
    {
        private static readonly Dictionary<Type, Func<object, string>> messageInfoGetters = new Dictionary<Type, Func<object, string>>();
        public static void SetMessageInfo<T>(string messageInfo_)
        {
            messageInfoGetters[typeof(T)] = _ => messageInfo_;
        }

        public static void SetMessageInfoGetter<T>(Func<T, string> messageInfoGetter_)
        {
            messageInfoGetters[typeof(T)] = m_ => messageInfoGetter_((T)m_);
        }

        public static string GetMessageInfo(this object message_)
        {
            if (message_ == null) return null;
            IProvideMessageInfo provider = message_ as IProvideMessageInfo;
            if (provider != null) return provider.GetInfo();
            Func<object, string> getter;
            if (messageInfoGetters.TryGetValue(message_.GetType(), out getter))
            {
                return getter(message_);
            }
            return null;
        }
    }
}
