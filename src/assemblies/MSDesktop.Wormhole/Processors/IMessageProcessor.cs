﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace MSDesktop.Wormhole.Processors
{
    interface IMessageProcessor
    {
        bool CanProcess(XElement element);
        void Process(XElement element);
    }
}
