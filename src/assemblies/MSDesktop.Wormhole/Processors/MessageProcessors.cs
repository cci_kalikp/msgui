﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Threading;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.MicrosoftShell.Standard;
using Newtonsoft.Json;

namespace MSDesktop.Wormhole.Processors
{
    class CreateNewWindowProcessor : MessageProcessorBase
    {
        public CreateNewWindowProcessor(IHostedApplicationProxyDelegate application, IpcBasedApplicationProxy applicationProxy)
            : base(application, applicationProxy)
        {
        }

        protected override string NodeName
        {
            get
            {
                return "CreateNewWindow";
            }
        }

        public override void Process(XElement element)
        {
            var factoryIdAttribute = element.Attributes("factoryID").FirstOrDefault();
            var windowIdAttribute = element.Attributes("windowID").FirstOrDefault();
            if (factoryIdAttribute == null || windowIdAttribute == null)
                return;

            var factoryId = factoryIdAttribute.Value;
            var windowId = windowIdAttribute.Value;

            Application.CreateWindow(factoryId, windowId);
        }
    }

    class CaptureWindowProcessor : MessageProcessorBase
    {
        public CaptureWindowProcessor(IHostedApplicationProxyDelegate application, IpcBasedApplicationProxy applicationProxy)
            : base(application, applicationProxy)
        {
        }

        protected override string NodeName
        {
            get
            {
                return "CaptureWindow";
            }
        }

        public override void Process(XElement root)
        {
            var windowIdAttribute = root.Attributes("windowID").FirstOrDefault();
            if (windowIdAttribute == null)
                return;

            var desiredTitleAttribute = root.Attributes("desiredTitle").FirstOrDefault();
            if (desiredTitleAttribute == null)
                return;

            var windowID = windowIdAttribute.Value;
            var desiredTitle = desiredTitleAttribute.Value;

            ApplicationProxy.CaptureWindow(windowID, desiredTitle);            
        }
    }

    class WindowStateProcessor : MessageProcessorBase
    {
        public WindowStateProcessor(IHostedApplicationProxyDelegate application, IpcBasedApplicationProxy applicationProxy)
            : base(application, applicationProxy)
        {
        }

        protected override string NodeName
        {
            get
            {
                return "WindowState";
            }
        }

        public override void Process(XElement root)
        {
            var queryIdAttribute = root.Attributes("requestID").FirstOrDefault();
            if (queryIdAttribute == null)
                return;

            var queryId = int.Parse(queryIdAttribute.Value);
            var state = root.Elements().FirstOrDefault();

            ApplicationProxy.EnqueueWindowStateQuery(queryId, state);
        }
    }

    class UpdateWindowProcessor : MessageProcessorBase
    {
        public UpdateWindowProcessor(IHostedApplicationProxyDelegate application, IpcBasedApplicationProxy applicationProxy)
            : base(application, applicationProxy)
        {
        }

        protected override string NodeName
        {
            get
            {
                return "UpdateWindow";
            }
        }

        public override void Process(XElement root)
        {
            var windowIdAttribute = root.Attributes("windowID").FirstOrDefault();
            if (windowIdAttribute == null)
                return;

            var windowId = windowIdAttribute.Value;
            string title = null;
            var titleAttribute = root.Attributes("title").FirstOrDefault();
            if (titleAttribute != null)
                title = titleAttribute.Value;

            Application.UpdateWindow(windowId, title);
        }
    }

    class RegisterWindowFactoryProcessor : MessageProcessorBase
    {
        public RegisterWindowFactoryProcessor(IHostedApplicationProxyDelegate application, IpcBasedApplicationProxy applicationProxy)
            : base(application, applicationProxy)
        {
        }

        protected override string NodeName
        {
            get
            {
                return "RegisterWindowFactory";
            }
        }

        public override void Process(XElement element)
        {
            var firstOrDefault = element.Attributes("factoryID").FirstOrDefault();
            if (firstOrDefault != null)
            {
                String factoryId = firstOrDefault.Value;
                Application.RegisterWindowFactory(factoryId);
            }
        }
    }

    class GetEnvironmentProcessor : MessageProcessorBase
    {
        public GetEnvironmentProcessor(IHostedApplicationProxyDelegate application, IpcBasedApplicationProxy applicationProxy)
            : base(application, applicationProxy)
        {
        }

        protected override string NodeName
        {
            get
            {
                return "GetEnvironment";
            }
        }

        public override void Process(XElement element)
        {
            ApplicationProxy.SendEnvironment();
        }
    }

    class SetTargetProcessIdProcessor : MessageProcessorBase
    {
        public SetTargetProcessIdProcessor(IHostedApplicationProxyDelegate application, IpcBasedApplicationProxy applicationProxy)
            : base(application, applicationProxy)
        {
        }

        protected override string NodeName
        {
            get
            {
                return "SetTargetProcessId";
            }
        }

        public override void Process(XElement element)
        {
            var processIdAttr = element.Attribute("processId");
            if (processIdAttr == null) return;
            var processId = processIdAttr.Value;
            ApplicationProxy.SetTargetProcessId(processId);
        }
    }

    class PlaceWidgetProcessor : MessageProcessorBase
    {
        public PlaceWidgetProcessor(IHostedApplicationProxyDelegate application, IpcBasedApplicationProxy applicationProxy)
            : base(application, applicationProxy)
        {
        }

        protected override string NodeName
        {
            get
            {
                return "PlaceWidget";
            }
        }

        public override void Process(XElement root)
        {
            var location = root.Attribute("location").Value;
            var type = Type.GetType(root.Attribute("type").Value);
            var widgetIdAttr = root.Attribute("widgetId");
            string widgetId = widgetIdAttr == null ? Guid.NewGuid().ToString("N") : widgetIdAttr.Value;
            var parameters = (InitialWidgetParameters) JsonConvert.DeserializeObject(root.Value, type);
            Application.PlaceWidget(location, widgetId, parameters);
        }
    }
}