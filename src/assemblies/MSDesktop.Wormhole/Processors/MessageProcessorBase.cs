﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace MSDesktop.Wormhole.Processors
{
    abstract internal class MessageProcessorBase : IMessageProcessor
    {
        protected readonly IHostedApplicationProxyDelegate Application;
        protected readonly IpcBasedApplicationProxy ApplicationProxy;

        protected MessageProcessorBase(IHostedApplicationProxyDelegate application, IpcBasedApplicationProxy applicationProxy)
        {
            this.Application = application;
            this.ApplicationProxy = applicationProxy;
        }

        protected abstract string NodeName { get; }
        
        public bool CanProcess(XElement element)
        {
            return element.Name.ToString().Equals(NodeName);
        }

        public abstract void Process(XElement element);
    }
}
