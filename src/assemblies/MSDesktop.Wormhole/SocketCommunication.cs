﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Xml.Linq;
using Microsoft.Practices.Composite.Events;

namespace MSDesktop.Wormhole
{
    class SocketCommunication
    {
        private Boolean started = false;

        public Boolean Started { get { return started; } private set { started = value; } }
        private int port;
        private List<TcpSocket> clients;

        public SocketCommunication(int port)
        {
            this.port = port;
        }

        private void ThreadStart()
        {
            Started = true;
            var ipEndPoint = new IPEndPoint(IPAddress.Loopback, this.port);
            var server = new Socket(ipEndPoint.Address.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

            clients = new List<TcpSocket>();
            string rln = null;

            clients.Capacity = 256;
            server.Blocking = false;
            server.Bind(ipEndPoint);
            server.Listen(32);
            this.port = ((IPEndPoint)server.LocalEndPoint).Port;
            Trace.WriteLine(string.Format("{0}: listening to port {1}", IPAddress.Loopback, ipEndPoint.Port));
            byte[] sharedMassiveBuffer = new byte[10240];
            while (true)
            {
                //Accept
                if (server.Poll(0, SelectMode.SelectRead))
                {
                    var sock = new TcpSocket();
                    clients.Add(sock);
                    sock.tcpSock = server.Accept();

                    OnConnected(sock);
                }

                foreach (var client in clients.ToArray())
                {
                    if (!client.tcpSock.Poll(0, SelectMode.SelectRead))
                        continue;

                    if (client.Receive(ref rln) > 0)
                    {
                        try
                        {
                            String str = rln.ToString();
                            Debug.WriteLine("Raw Stuff: " + str);
                            while (str.IndexOf(">") == -1 || !str.EndsWith("\0"))
                            {
                                Thread.Sleep(500);
                                if (client.Receive(ref rln) > 0)
                                {
                                    str += rln;
                                    Debug.WriteLine("Raw Stuff: " + str);
                                }
                                else
                                {
                                    Debug.WriteLine("Waiting for more stuff. Still only have: " + str);
                                }
                            }
                            // DOV DEBUG
                            OnRawReceive(str);
                            //DOV DEBUG
                            OnReceive(str, client);
                        }
                        catch (Exception e)
                        {
                            Debug.WriteLine("Swallwed Error on socket : " + rln + " \n error is " + e.StackTrace);

                        }

                    }
                    else
                    //recv returned <= 0; close the socket
                    {
                        client.tcpSock.Shutdown(SocketShutdown.Both);
                        client.tcpSock.Close();
                        OnDisconnected(client);
                        clients.Remove(client);
                    }
                }
                Thread.Sleep(10);
            }
        }

        public void Start()
        {
            if (started) return;
            Thread t = new Thread(ThreadStart)
            {
                IsBackground = true,
                Name = "Adobe Air integration communication thread"
            };

            t.Start();
        }

        public event EventHandler<DataEventArgs<String>> RawReceive;
        private void OnRawReceive(String s)
        {
            var r = RawReceive;
            if (r != null)
            {
                r(null, new DataEventArgs<String>(s));
            }
        }

        public event EventHandler<XmlDataEventArgs> Receive;
        private void OnReceive(string data, TcpSocket socket)
        {
            var copy = Receive;
            if (copy != null)
            {
                var msgs = data.Split(new char[] { '\0' });
                foreach (var msg in msgs)
                {
                    if (msg == string.Empty)
                        continue;

                    try
                    {
                        var doc = XDocument.Parse(msg);
                        copy(null, new XmlDataEventArgs { Document = doc, Socket = socket });
                    }
                    catch (Exception e)
                    {
                        // TODO - NEed to deal with this a bit better
                        Debug.WriteLine("Warning: Exception thrown on parse of xml: " + msg);
                    }


                }
            }
        }

        public event EventHandler<SocketClientEventArgs> Connected;
        private void OnConnected(TcpSocket socket)
        {
            var copy = Connected;
            if (copy != null)
            {
                copy(null, new SocketClientEventArgs { Socket = socket });
            }
        }

        public event EventHandler<SocketClientEventArgs> Disconnected;
        private void OnDisconnected(TcpSocket socket)
        {
            var copy = Disconnected;
            if (copy != null)
            {
                copy(null, new SocketClientEventArgs { Socket = socket });
            }
        }

    }

}
