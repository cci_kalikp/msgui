﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using System.Windows.Threading;
using System.Xml.Linq;
using MSDesktop.Wormhole.Communication;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Win32;
using MorganStanley.MSDotNet.My;
using Application = System.Windows.Application;
using HorizontalAlignment = System.Windows.HorizontalAlignment;
using NativeMethods = MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.MicrosoftShell.Standard.NativeMethods;

namespace MSDesktop.Wormhole
{
    public class WormholeBridgedApplication : IHostedApplicationProxyDelegate
    {
        private static IMSLogger logger = MSLoggerFactory.CreateLogger<WormholeBridgedApplication>();

        private readonly IChromeRegistry chromeRegistry;
        private readonly IChromeManager chromeManager;
        private readonly IUnityContainer container;
        private readonly ObservableCollection<String> windowFactories = new ObservableCollection<String>();
        private int windowCaptureTimeoutInSeconds = 60;
        private readonly IHostedApplicationProxy hostedApplicationProxy;
        private readonly List<WindowHolder> windows = new List<WindowHolder>();
        private Dispatcher guiDispatcher;

        public event EventHandler ApplicationReady;
        public event EventHandler<WindowCaptureTimedOutEventArgs> WindowCaptureTimedOut;
        public event EventHandler<WindowEventArgs> WindowCaptured;

        /// <summary>
        /// Gets a list of window factories available in the bridged application.
        /// </summary>
        public Collection<String> WindowFactories { get { return windowFactories; } }
        public IIpcBasedApplicationProxy IpcBasedApplicationProxy { get { return hostedApplicationProxy as IpcBasedApplicationProxy; } }public IUnityContainer Container { get { return container; } }

        public int WindowCaptureTimeoutInSeconds
        {
            get
            {
                return windowCaptureTimeoutInSeconds;
            }
            set
            {
                windowCaptureTimeoutInSeconds = value;
            }
        }

        public WormholeBridgedApplication(ApplicationStarter applicationStarter, IWormholeTransport transport, IChromeRegistry chromeRegistry, IChromeManager chromeManager, IUnityContainer container_)
         : this(chromeRegistry, chromeManager, container_)
        {
            hostedApplicationProxy = new IpcBasedApplicationProxy(transport, container, applicationStarter, this);
        }

        public WormholeBridgedApplication(IWormholeTransport transport, IChromeRegistry chromeRegistry, IChromeManager chromeManager, IUnityContainer container_)
            : this(chromeRegistry, chromeManager, container_)
        {
            hostedApplicationProxy = new IpcBasedApplicationProxy(transport, container_, null, this);
        }

        /// <summary>
        /// This ctor is for working with arbitrary applications that won't support the communication protocol.
        /// </summary>
        public WormholeBridgedApplication(IChromeRegistry chromeRegistry, IChromeManager chromeManager,
                                          IUnityContainer container)
        {
            this.chromeRegistry = chromeRegistry;
            this.chromeManager = chromeManager;
            this.container = container;
            container.Resolve<IApplication>().ApplicationClosed += OnApplicationClosed;
            if (hostedApplicationProxy == null)
            {
                hostedApplicationProxy = new SingleWindowApplicationProxy(this);
            }
        }

        private void OnApplicationClosed(object sender, EventArgs eventArgs)
        {
            hostedApplicationProxy.HostClosed();
        }

        public Process Start()
        {
            guiDispatcher = Application.Current.MainWindow == null ? Application.Current.Dispatcher : Application.Current.MainWindow.Dispatcher;
            return hostedApplicationProxy.Start();
        }

        public void CaptureWindow(string windowID, IntPtr windowHandle)
        {
            var holder = this.windows.FirstOrDefault(wh => wh.AirBridgeID == windowID);
            if (holder == null)
            {
                holder = new WindowHolder {AirBridgeID = windowID};
                windows.Add(holder);
            }
            holder.ChildWindow = windowHandle;
            holder.ChildWindowReady.Set();

        }

        private static void Dockwindow(Panel host, IntPtr child)
        {
            var foo = NativeMethods.SetParent(child, host.Handle);

            int childProcessId, hostProcessId;
            var childThreadId = Win32.GetWindowThreadProcessId(child, out childProcessId);
            var hostThreadId = Win32.GetWindowThreadProcessId(host.Handle, out hostProcessId);
            Win32.AttachThreadInput((uint)childThreadId, (uint)hostThreadId, true);

            Debug.WriteLine("SetParent (" + child + "," + host.Handle + ") returned " + foo);
            if (foo.ToInt32() == 0)
            {
                Debug.WriteLine("Im trying to defer invocation because setparent wont work yet!");
                Dispatcher.CurrentDispatcher.BeginInvoke((Action)(() =>
                {
                    foo = NativeMethods.SetParent(child, host.Handle);
                    Debug.WriteLine("Calling deferred SetParent returned " + foo);
                }), DispatcherPriority.ApplicationIdle);
            }

            host.SizeChanged += (s, e) => FixWindow(host, child);
            FixWindow(host, child);
        }

        private static void FixWindow(Panel host, IntPtr child)
        {
            NativeMethods.MoveWindow(child, 0, 0, host.Width, host.Height, true);
        }


        static IEnumerable<IntPtr> EnumerateProcessWindowHandles(int processId)
        {
            var handles = new List<IntPtr>();

            foreach (ProcessThread thread in Process.GetProcessById(processId).Threads)
                NativeMethods.EnumThreadWindows(thread.Id,
                    (hWnd, lParam) => { handles.Add(hWnd); return true; }, IntPtr.Zero);

            foreach (var childProcess in EnumerateChildProcesses(processId))
            {
                handles.AddRange(EnumerateProcessWindowHandles(childProcess));
            }

            return handles;
        }

        private static IEnumerable<int> EnumerateChildProcesses(int processId)
        {
            var mngcls = new ManagementClass("Win32_Process");          
            var processIds = mngcls.GetInstances();
            var processIdsArray = new ManagementBaseObject[processIds.Count];
            processIds.CopyTo(processIdsArray, 0);

            foreach (ManagementObject instance in processIdsArray)
            {
                if (Convert.ToInt32(instance["ParentProcessId"]) == processId)
                {
                    yield return Convert.ToInt32(instance["ProcessId"]);
                }
            }
        }
        
        private Dispatcher GuiDispatcher
        {
            get
            {
                if (guiDispatcher == null)
                {
                    guiDispatcher = Application.Current.MainWindow == null ? Application.Current.Dispatcher : Application.Current.MainWindow.Dispatcher;
                }
                return guiDispatcher;
            }
        }

        #region window factory

        public IWindowFactoryHolder RegisterWindowFactory(string factoryId)
        {
            IWindowFactoryHolder holder = null;
            GuiDispatcher.Invoke(new Action(() =>
                {
                    if (!this.windowFactories.Contains(factoryId))
                    {
                        try
                        {
                            // ADDED BY DOV SO WE CAN SEE ALL AIR FACTORIES
                            holder = chromeRegistry.RegisterWindowFactory(factoryId).SetInitHandler(AirWindowFactory).SetDehydrateHandler(DehydrateAirWindow);                   
                            windowFactories.Add(factoryId); 
                        }
                        catch (Exception ex)
                        {
                            //new UnhandledExceptionTaskDialogLauncher() {UnhandledException = ex}.ShowDialog();
                            System.Windows.MessageBox.Show("Error registering factory " + ex);
                        }
                    }
                    else
                    {
                        System.Windows.MessageBox.Show("Error. Cannot add this factory a second time: " + factoryId);
                    }
                }));
            return holder;
        }

        private bool AirWindowFactory(IWindowViewContainer vc, XDocument state)
        {
            var parameters = vc.Parameters as InitialAirWindowParameters;

            var AirBridgeID =  (parameters == null || parameters.AirBridgeID == null) ? Guid.NewGuid().ToString() : parameters.AirBridgeID;            

            vc.Parameters.SizingMethod = SizingMethod.Custom;

            // set up the host window
            var panel = new Panel
            {
                BackColor = System.Drawing.Color.BurlyWood
            };

            var host = new WindowsFormsHost
            {
                HorizontalAlignment = HorizontalAlignment.Stretch,
                VerticalAlignment = VerticalAlignment.Stretch,
                Child = panel
            };

            vc.Content = host;

            // wire up the capture mechanism
            var holder = this.windows.FirstOrDefault(wh => wh.AirBridgeID == AirBridgeID);
            if (holder == null) {
                holder = new WindowHolder
                {
                    AirBridgeID = AirBridgeID
                };
                windows.Add(holder);
            }
            holder.ViewContainer = vc;

            int timeoutInSeconds = parameters == null || parameters.StartupTimeoutInSeconds == null
                                       ? windowCaptureTimeoutInSeconds
                                       : parameters.StartupTimeoutInSeconds.Value;
            Task.Factory.StartNew(() =>
                {
                    if (holder.ChildWindow == IntPtr.Zero)
                    {
                        if (!holder.ChildWindowReady.Wait(TimeSpan.FromSeconds(timeoutInSeconds)))
                        {
                            logger.Warning("Window capture timed out for view with id {0}", holder.ViewContainer.ID);
                            OnWindowCaptureTimedOut(new WindowCaptureTimedOutEventArgs
                                {
                                    ViewContainer = holder.ViewContainer
                                });
                        }
                    }
                    guiDispatcher.BeginInvoke(new Action(() =>
                        {
                            var info = new Win32.WindowInfo();
                            Win32.GetWindowInfo(holder.ChildWindow, ref info);

                            holder.ViewContainer.Width = info.rcWindow.GetLength();
                            holder.ViewContainer.Height = info.rcWindow.GetHeight();
                            Dockwindow(panel, holder.ChildWindow);
                            var captured = WindowCaptured;
                            if (captured != null)
                            {
                                captured(this, new WindowEventArgs(holder.ViewContainer));
                            }
                        }));
                });

            EventHandler<WindowEventArgs> closeHandler = null;
            closeHandler = (sender, args) =>
            {
                hostedApplicationProxy.CloseWindow(holder.AirBridgeID);
                vc.Closed -= closeHandler;
                vc.ClosedQuietly -= closeHandler;
            };
            vc.Closed += closeHandler;
            vc.ClosedQuietly += closeHandler;

            hostedApplicationProxy.CreateWindow(AirBridgeID, vc, state);
            
            return true;
        }

        public XDocument TestGetWindowXml(IWindowViewContainer vc) { return DehydrateAirWindow(vc); }

        private XDocument DehydrateAirWindow(IWindowViewContainer vc)
        {
            var window = this.windows.FirstOrDefault(w => w.ViewContainer == vc);
            if (window == null)
                return null;
            return hostedApplicationProxy.SaveState(window);
        }

        #endregion

        public void CreateWindow(string factoryId, XDocument state)
        {
            this.chromeManager.CreateWindow(factoryId, new InitialAirWindowParameters { InitialState = state, InitialLocation = InitialLocation.Floating | InitialLocation.PlaceAtCursor });
        }

        protected virtual void OnWindowCaptureTimedOut(WindowCaptureTimedOutEventArgs e)
        {
            var handler = WindowCaptureTimedOut;
            if (handler != null) handler(this, e);
        }

        void IHostedApplicationProxyDelegate.OnApplicationReady()
        {
            var handler = ApplicationReady;
            if (handler != null) handler(this, EventArgs.Empty);
        }

        void IHostedApplicationProxyDelegate.CaptureWindow(string windowID, string desiredTitle, Process targetProcess)
        {
            GuiDispatcher.Invoke((Action)(() =>
                { 
                    var holder = this.windows.FirstOrDefault(wh => wh.AirBridgeID == windowID);
                    if (holder == null) return;
                    foreach (var windowHandle in EnumerateProcessWindowHandles(targetProcess.Id))
                    {
                        var sb = new StringBuilder(1024);
                        NativeMethods.GetWindowText(windowHandle, sb, 1024);
                        var caption = sb.ToString();
                        var sb2 = new StringBuilder(1024);
                        NativeMethods.GetClassName(windowHandle, sb2, 1024);
                        var className = sb2.ToString();
                        Debug.WriteLine("Window Details: " + windowHandle + "->" + className + "(" + caption + ")");
                        // If no match continue looping
                        if (caption != windowID)
                            continue;
                        var b = NativeMethods.IsWindowVisible(windowHandle);

                        // We've gotten to the matching window 
                        if (!b)
                        {
                            Debug.WriteLine("We've found a non visible child but need to capture it! :" + windowID);
                            return;
                        }
                        Debug.WriteLine("We've found a window and are going to capture it: " + windowHandle + "->" + desiredTitle);
                        holder.ChildWindow = windowHandle;
                        //this won't take effect in the case of isolation, until runtime dynamic property change is supported
                        holder.ViewContainer.Title = desiredTitle;
                        holder.ChildWindowReady.Set();
                        return;
                    }
            }));
        }

        void IHostedApplicationProxyDelegate.CreateWindow(string factoryId, string windowId)
        {
            GuiDispatcher.Invoke((Action)(() => chromeManager.CreateWindow(factoryId, new InitialAirWindowParameters { AirBridgeID = windowId, InitiatedByAir = true })));
        }

        void IHostedApplicationProxyDelegate.UpdateWindow(string windowId, string title)
        {
            var window = this.windows.FirstOrDefault(w => w.AirBridgeID == windowId);

            if (window == null)
                return;

            GuiDispatcher.Invoke((Action)(() =>
            {
                if (title != null)
                    window.ViewContainer.Title = title;
            }));
        }

        void IHostedApplicationProxyDelegate.PlaceWidget(string location, string widgetId, InitialWidgetParameters parameters)
        {
            GuiDispatcher.BeginInvoke(new Action(() =>
            {
                chromeManager.PlaceWidget(widgetId, location, parameters);
            }));
        }
    }

    public class WindowCaptureTimedOutEventArgs : EventArgs
    {
        public IWindowViewContainer ViewContainer { get; set; }
    }
    
    internal class WindowHolder
    {
        public readonly ManualResetEventSlim ChildWindowReady = new ManualResetEventSlim();
        public volatile IntPtr ChildWindow;

        public string AirBridgeID { get; set; }
        public IWindowViewContainer ViewContainer { get; set; }
    }

 
    internal class WindowStateHolder
    {
        private static int sequence = 0;

        public int ID { get; private set; }
        public WindowHolder WindowHolder { get; set; }
        public XElement State { get; set; }

        public WindowStateHolder()
        {
            this.ID = ++sequence;
        }
    }

    public delegate Process ApplicationStarter();
}
