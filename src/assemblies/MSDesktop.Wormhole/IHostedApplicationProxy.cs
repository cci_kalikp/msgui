﻿using System.Diagnostics;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MSDesktop.Wormhole
{
    interface IHostedApplicationProxy
    {
        void CloseWindow(string id);
        void CreateWindow(string airBridgeId, IWindowViewContainer vc, XDocument state);
        XDocument SaveState(WindowHolder holder);
        Process Start();
        void HostClosed();
    }
}
