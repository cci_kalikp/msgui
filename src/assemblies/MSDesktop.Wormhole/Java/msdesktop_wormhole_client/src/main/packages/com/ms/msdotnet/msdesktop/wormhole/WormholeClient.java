package com.ms.msdotnet.msdesktop.wormhole;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

public abstract class WormholeClient {

    private Socket msDesktopSocket;
    
    final private int portNumber;
    
    public WormholeClient(int portNumber) {
        this.portNumber = portNumber;
    }

    public void Start() {
        try {
            msDesktopSocket = new Socket("127.0.0.1", portNumber);
            
            BufferedReader in =
                    new BufferedReader(
                        new InputStreamReader(msDesktopSocket.getInputStream()));
            char[] buff = new char[1024];
            int num = 0;
            StringBuilder sb = new StringBuilder();
            
            while ((num = in.read(buff)) != 0) {           
                String messageText = new String(buff, 0, num);
                String[] parts = messageText.split("\0", -1);
                sb.append(parts[0]);
                
                if (parts.length > 1) {
                    processMessage(sb.toString());
                    for (int i = 1; i < parts.length - 1; i++) {
                        processMessage(parts[i]);
                    }
                    sb = new StringBuilder(parts[parts.length - 1]);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException( e );
        } 
    }
    
    private void processMessage(String messageText) {
        try {
            DocumentBuilderFactory dbf1 = DocumentBuilderFactory.newInstance();
            DocumentBuilder db1 = dbf1.newDocumentBuilder();
            Document doc1 = db1.parse(new ByteArrayInputStream(messageText.getBytes()));

            Node createWindowNode = doc1.getElementsByTagName("CreateWindow").item(0);
            Node closeWindowNode = doc1.getElementsByTagName("CloseWindow").item(0);
            Node shutdownNode = doc1.getElementsByTagName("Shutdown").item(0);
            Node getWindowStateNode = doc1.getElementsByTagName("GetWindowState").item(0);

            if (createWindowNode != null) {
                System.out.println("Received CreateWindow");
                String windowId = createWindowNode.getAttributes().getNamedItem("windowID").getTextContent();
                Node stateNode = doc1.getElementsByTagName("State").item(0);
                Node state = stateNode != null ? stateNode.getChildNodes().item(1) : null;
                createWindow(windowId, state);
            } else if (closeWindowNode != null) {
                System.out.println("Received CloseWindow");
                String windowId = closeWindowNode.getAttributes().getNamedItem("windowID").getTextContent();
                closeWindow(windowId);
            } else if (shutdownNode != null) {
                System.out.println("Received Shutdown");
                msDesktopSocket.close();
                System.exit(0);
            } else if (getWindowStateNode != null) {
                System.out.println("Received GetWindowState");
                String windowId = getWindowStateNode.getAttributes().getNamedItem("windowID").getTextContent();
                String requestId = getWindowStateNode.getAttributes().getNamedItem("requestID").getTextContent();
                getWindowState(windowId, requestId);
            } else {
                System.out.println("Received unknown message");
            }
        } catch (SAXException e) {
            throw new RuntimeException(e);
        } catch (ParserConfigurationException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    protected void sendCaptureWindow(String windowId, String desiredTitle) {
        OutputStreamWriter writer;
        try {
            writer = new OutputStreamWriter(msDesktopSocket.getOutputStream());
            writer.write("<CaptureWindow windowID=\""+windowId+"\" desiredTitle=\""+desiredTitle+"\" />");
            writer.write('\0');
            writer.flush(); 
        } catch (IOException e) {
            throw new RuntimeException( e );
        }
    }
    
    protected void sendWindowState(String windowId, String requestId, String state) {
        OutputStreamWriter writer;
        try {
            writer = new OutputStreamWriter(msDesktopSocket.getOutputStream());
            writer.write("<WindowState windowID=\""+windowId+"\" requestID=\""+requestId+"\">"+state+"</WindowState>");
            writer.write('\0');
            writer.flush();         
        } catch (IOException e) {
            throw new RuntimeException( e );
        }                               
    }
    
    public abstract void createWindow(String windowId, Node state);

    public abstract void closeWindow(String windowId);

    public abstract void getWindowState(String windowId, String requestId);
}
