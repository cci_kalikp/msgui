﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Threading;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Win32;

namespace MSDesktop.Wormhole
{
    class SingleWindowApplicationProxy : IHostedApplicationProxy
    {
        private readonly IHostedApplicationProxyDelegate appDelegate;
        private readonly IDictionary<string, ProcessWindowInfo> windows;
        private readonly IDictionary<string, DispatcherTimer> timers;
        
        public SingleWindowApplicationProxy(IHostedApplicationProxyDelegate appDelegate)
        {
            this.appDelegate = appDelegate;
            this.windows = new Dictionary<string, ProcessWindowInfo>();
            this.timers = new Dictionary<string, DispatcherTimer>();
            appDelegate.WindowCaptureTimedOut += AppDelegateOnWindowCaptureTimedOut;
        }

        public void CloseWindow(string id)
        {
            if (!windows.ContainsKey(id)) return;
            var process = windows[id];            
            if (!process.KillOnClose)
            {
                process.Process.CloseMainWindow();
            }
            else
            {
                process.Process.Kill();
            }
            windows.Remove(id);
        }

        public void CreateWindow(string airBridgeId, IWindowViewContainer viewContainer, XDocument state)
        {
            ProcessWindowInfo info = null;
            var processBoundParameters = viewContainer.Parameters as InitialProcessBoundWindowParameters;
            if (processBoundParameters != null)
            {
                info = new ProcessWindowInfo
                    { 
                        ProcessStartInfo = GetProcessStartInfo(processBoundParameters.ProcessExe, processBoundParameters.ProcessArgs, processBoundParameters.WorkingDirectory),
                        KillOnClose = processBoundParameters.KillOnClose, 
                        WindowTitleSubstring = processBoundParameters.WindowTitleSubstring
                    };
            }
            else if (state != null)
            {
                var processBoundState = state.Descendants("ProcessInfo").FirstOrDefault();
                if (processBoundState != null)
                {
                    var killOnCloseNode = processBoundState.Descendants("KillOnClose").FirstOrDefault();
                    var windowTitleSubstringNode =
                        processBoundState.Descendants("WindowTitleSubstring").FirstOrDefault();
                    var processArgsNode = processBoundState.Descendants("ProcessArgs").FirstOrDefault();
                    var processWorkDirNode = processBoundState.Descendants("WorkingDirectory").FirstOrDefault();
                    info = new ProcessWindowInfo
                    { 
                        ProcessStartInfo = GetProcessStartInfo(processBoundState.Descendants("ProcessExe").First().Value,
                        processArgsNode != null ? processArgsNode.Value : null, processWorkDirNode != null ? processWorkDirNode.Value : null),
                        KillOnClose = killOnCloseNode != null && bool.Parse(killOnCloseNode.Value),
                        WindowTitleSubstring = windowTitleSubstringNode != null ? windowTitleSubstringNode.Value : null
                    };
                }
            }
            SetupProcessBoundWindow(airBridgeId, viewContainer, info);
        }

        private static ProcessStartInfo GetProcessStartInfo(string fileName, string arguments, string workingDirectory)
        {
            var processStartInfo = new ProcessStartInfo();
            if (fileName.EndsWith(".cmd") || fileName.EndsWith(".bat"))
            {
                string commandExe = Environment.GetEnvironmentVariable("ComSpec");
                if (!string.IsNullOrEmpty(commandExe))
                {
                    processStartInfo.FileName = commandExe;
                    processStartInfo.Arguments = string.Format("/C \"{0}\" {1}", fileName, arguments);
                }
            }
            
            if (string.IsNullOrEmpty(processStartInfo.FileName))
            {
                processStartInfo.FileName = fileName;
                processStartInfo.Arguments = arguments; 
                processStartInfo.UseShellExecute = true;
            }
            if (!string.IsNullOrEmpty(workingDirectory))
            {
                processStartInfo.WorkingDirectory = workingDirectory;
            }
            return processStartInfo;
        }
        public XDocument SaveState(WindowHolder window)
        {
            var settings = windows[window.AirBridgeID];
            var state = new XDocument();
            var root = new XElement("ProcessInfo");
            root.Add(new XElement("ProcessExe", settings.ProcessStartInfo.FileName));
            root.Add(new XElement("ProcessArgs", settings.ProcessStartInfo.Arguments));
            root.Add(new XElement("WorkingDirectory", settings.ProcessStartInfo.WorkingDirectory));
            root.Add(new XElement("KillOnClose", settings.KillOnClose));
            root.Add(new XElement("WindowTitleSubstring", settings.WindowTitleSubstring));
            state.Add(root);
            return state;
        }
        
        private void AppDelegateOnWindowCaptureTimedOut(object sender, WindowCaptureTimedOutEventArgs windowCaptureTimedOutEventArgs)
        {
            if (!timers.ContainsKey(windowCaptureTimedOutEventArgs.ViewContainer.ID)) return;
            timers[windowCaptureTimedOutEventArgs.ViewContainer.ID].Stop();
            timers.Remove(windowCaptureTimedOutEventArgs.ViewContainer.ID);
        }

        private void SetupProcessBoundWindow(string airBridgeID, IWindowViewContainer vc, ProcessWindowInfo parameters)
        {
            var process = Process.Start(parameters.ProcessStartInfo);
            parameters.Process = process;
            windows[airBridgeID] = parameters;
            if (process == null)
            {
                throw new Exception("The application did not start correctly");
            }

            var timer = new DispatcherTimer {Interval = TimeSpan.FromMilliseconds(500)};
            timers[vc.ID] = timer;
            timer.Tick += (sender, args) =>
                {
                    var handle = IntPtr.Zero;
                    if (!string.IsNullOrEmpty(parameters.WindowTitleSubstring))
                    {
                        var processCandidate = Process.GetProcesses().FirstOrDefault(p => p.MainWindowTitle.Contains(parameters.WindowTitleSubstring));
                        if (processCandidate != null)
                        {
                            handle = processCandidate.MainWindowHandle;
                            parameters.Process = processCandidate;
                            process = processCandidate;
                        }
                    }
                    else
                    {
                        handle = process.MainWindowHandle;
                    }

                    if (handle == IntPtr.Zero) return;

                    timer.Stop();
                    timers.Remove(vc.ID);
                    Win32.SetWindowLong(handle, Win32.GWL_STYLE,
                                        (int)
                                        (Win32.GetWindowLong(handle, Win32.GWL_STYLE) &
                                         ~Win32.WS_BORDER & ~Win32.WS_THICKFRAME));
                    vc.Title = process.MainWindowTitle;
                    appDelegate.CaptureWindow(airBridgeID, handle);
                };
            timer.Start();
        }
        
        public Process Start()
        {
            return null;
        }

        public void HostClosed()
        {
            var windowsCopy = new List<string>(windows.Keys);
            foreach (string id in windowsCopy)
            {
                CloseWindow(id);
            }
        }

        private class ProcessWindowInfo
        {
            public ProcessStartInfo ProcessStartInfo { get; set; }
            public string WindowTitleSubstring { get; set; }
            public Process Process { get; set; }
            public bool KillOnClose { get; set; }

        }
    }
}
