﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MSDesktop.Wormhole
{
    interface IHostedApplicationProxyDelegate
    {
        void CaptureWindow(string airBridgeID, string mainWindowHandle, Process targetProcess);
        void CaptureWindow(string airBridgeID, IntPtr mainWindowHandle);
        void CreateWindow(string factoryId, string windowId);
        void UpdateWindow(string windowId, string title);
        IWindowFactoryHolder RegisterWindowFactory(string factoryId);
        void PlaceWidget(string location, string widgetId, InitialWidgetParameters parameters);
        void OnApplicationReady();
        event EventHandler<WindowCaptureTimedOutEventArgs> WindowCaptureTimedOut;
    }
}
