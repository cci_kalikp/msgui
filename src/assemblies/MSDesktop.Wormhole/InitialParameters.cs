﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MSDesktop.Wormhole
{
    public class InitialAirWindowParameters : InitialWindowParameters
    {
        public XDocument InitialState { get; set; }
        public string AirBridgeID { get; set; }
        public bool InitiatedByAir { get; set; }
        public int? StartupTimeoutInSeconds { get; set; }
        //public TcpSocket Socket { get; set; }
    }

    public class InitialProcessBoundWindowParameters : InitialAirWindowParameters
    {
        public string ProcessExe { get; set; }
        public string ProcessArgs { get; set; }
        public string WorkingDirectory { get; set; }
        public bool KillOnClose { get; set; }
        public string WindowTitleSubstring { get; set; }
    }
}
