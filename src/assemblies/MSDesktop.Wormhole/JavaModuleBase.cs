﻿using System;
using System.Diagnostics;
using System.Management;
using System.Threading;
using MSDesktop.Wormhole.Communication.Tcp;
using MSDesktop.Wormhole.Kraken;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence.DelayedLoading;
using MorganStanley.MSDotNet.My;

namespace MSDesktop.Wormhole
{
    public abstract class JavaModuleBase : DelayedProfileLoadCoordinatingModule
    {
        private static IMSLogger _logger = MSLoggerFactory.CreateLogger<JavaModuleBase>();

        private readonly IChromeRegistry chromeRegistry;
        private readonly IChromeManager chromeManager;
        private readonly IApplication application;
        private readonly IUnityContainer container;

        /// <summary>
        /// Provide a port number to be used to communicate with the Java application.
        /// </summary>
        protected abstract int PortNumber { get; }
        /// <summary>
        /// Provide a command line to be used to start the Java application. The value can be null if
        /// you decide to override the GetProcess method.
        /// </summary>
        protected virtual string Path { get { return null; } }
        /// <summary>
        /// If specified, the application will try to attach to a javaw.exe process with CommandLine containing
        /// the provided string.
        /// </summary>
        protected virtual string ProcessCommandLine { get { return null; } }

        protected virtual bool EnableKrakenCommunication { get { return false; } }

        protected JavaModuleBase(IChromeRegistry chromeRegistry, IChromeManager chromeManager, IApplication application, IUnityContainer container)
            : base(container)
        {
            this.chromeRegistry = chromeRegistry;
            this.chromeManager = chromeManager;
            this.application = application;
            this.container = container;
        }

        /// <summary>
        /// Provide an override if you want to return an instance of the Java application in a non-standard
        /// way (for example if you decide to attach to an existing process).
        /// </summary>
        /// <returns></returns>
        protected virtual Process GetProcess()
        {
            if (Path != null)
            {
                _logger.InfoWithFormat("Starting: {0}", PathUtilities.ReplaceEnvironmentVariables(Path));
                return Process.Start(PathUtilities.ReplaceEnvironmentVariables(Path));
            }
            if (ProcessCommandLine != null)
            {
                string processCommandLine = PathUtilities.ReplaceEnvironmentVariables(ProcessCommandLine);
                _logger.InfoWithFormat("Trying to attach to a process with command line {0}...", processCommandLine);
                while (true)
                {
                    var mngmtClass = new ManagementClass("Win32_Process");
                    foreach (ManagementObject o in mngmtClass.GetInstances())
                    {
                        if (o["Name"].Equals("javaw.exe"))
                        {
                            var commandLine = (string)o["CommandLine"];
                            if (commandLine.Contains(processCommandLine))
                            {
                                var pid = Convert.ToInt32(o["ProcessId"]);
                                var process = Process.GetProcessById(pid);
                                _logger.InfoWithFormat("Will attach to process with PID: {0}", pid);
                                return process;
                            }
                        }
                    }
                    Thread.Sleep(1000);
                }
            }
            throw new ArgumentException("Neither Path nor ProcessCommandLine was specified");
        }

        public override void Initialize()
        {
            base.Initialize();

            var transport = new WormholeTcpServer(PortNumber);
            var javaApplication = new WormholeBridgedApplication(
                GetProcess,
                transport,
                chromeRegistry,
                chromeManager,
                container);

            if (EnableKrakenCommunication)
            {
                javaApplication.EnableKrakenCommunication();
            }

            javaApplication.ApplicationReady += (sender, args) =>
                {
                    _logger.Info("Java application ready");
                    OnModuleInitialized();
                };
            javaApplication.Start();

            application.ApplicationClosed += (sender, args) => transport.Stop();
        }

        protected override void CloseInitializer()
        {
            /* ignore */
        }
    }
}
