﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Forms.Integration; 
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Win32;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.MicrosoftShell.Standard;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions;
using HorizontalAlignment = System.Windows.HorizontalAlignment;
using NativeMethods = MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.MicrosoftShell.Standard.NativeMethods;


namespace MSDesktop.Wormhole
{
    class WormholeModule : IMSDesktopModule
    {
        private static IChromeManager chromeManager;
        private static IChromeRegistry chromeRegistry;
        //private SocketCommunication communicator;k
        //private OverlayWindowManager flexWindowManager;

        public WormholeModule(IChromeRegistry chromeRegistry, IChromeManager chromeManager)
        {
            WormholeModule.chromeManager = chromeManager;
            WormholeModule.chromeRegistry = chromeRegistry;
        }

        public void Initialize()
        {
            //communicator = new SocketCommunication(25761);
            //flexWindowManager = new OverlayWindowManager(communicator, chromeRegistry, chromeManager);

            //communicator.Start();
        }


        public static Process StartProcess(string fileName, string arguments)
        {
            IntPtr hWndDocked = IntPtr.Zero;
            var dockedProcess = Process.Start(fileName, arguments);
            if (dockedProcess == null)
                return null;

            while (hWndDocked == IntPtr.Zero)
            {
                dockedProcess.WaitForInputIdle(1000);
                dockedProcess.Refresh();
                
                if (dockedProcess.HasExited)
                    return null;

                hWndDocked = dockedProcess.MainWindowHandle;
            }
            return dockedProcess;
        }

        public static void CaptureProcessWindows(Process targetProcess)
        {
            int i = 0;
            foreach (var windowHandle in EnumerateProcessWindowHandles(targetProcess.Id))
            {
                var sb2 = new StringBuilder(1024);
                Win32.GetClassName(windowHandle, sb2, 1024);
                var className = sb2.ToString();

                var b = Win32.IsWindowVisible(windowHandle);

                if (b && className == "ApolloRuntimeContentWindow")
                    CreateWindow(windowHandle);
            }
        }

        

        private static void CreateWindow(IntPtr child)
        {
            var guid = Guid.NewGuid().ToString();
            chromeRegistry.RegisterWindowFactory(guid, (container, state) =>
                {
                    var windowStyle = (WS)Win32.GetWindowLong(child, (int)GWL.STYLE);
                    windowStyle -= WS.BORDER;
                    windowStyle -= WS.SIZEBOX;
                    windowStyle -= WS.DLGFRAME;
                    Win32.SetWindowLong(child, (int)GWL.STYLE, (uint)windowStyle);

                    var windowInfo = new Win32.WindowInfo();
                    Win32.GetWindowInfo(child, ref windowInfo);
                    container.Parameters.Width = windowInfo.rcWindow.GetLength();
                    container.Parameters.Height = windowInfo.rcWindow.GetHeight();
                    container.Parameters.SizingMethod = SizingMethod.Custom;

                    var panel = new Panel
                    {
                        BackColor = System.Drawing.Color.BurlyWood
                    };
                    var host = new WindowsFormsHost
                    {
                        HorizontalAlignment = HorizontalAlignment.Stretch,
                        VerticalAlignment = VerticalAlignment.Stretch,
                        Child = panel
                    };

                    container.Content = host;

                    EventHandler<VisibilityEventArgs> hack = null;
                    hack = (sender, args) =>
                     {
                         Dockwindow(panel, child);
                         container.IsVisibleChanged -= hack;
                     };
                    container.IsVisibleChanged += hack;

                    return true;
                });

            chromeManager.CreateWindow(guid, new InitialWindowParameters());
        }



        private static void Dockwindow(Panel host, IntPtr child)
        {
            var foo = NativeMethods.SetParent(child, host.Handle);
            host.SizeChanged += (s, e) => FixWindow(host, child);
            FixWindow(host, child);
        }

        private static void FixWindow(Panel host, IntPtr child)
        {
            NativeMethods.MoveWindow(child, 0, 0, host.Width, host.Height, true);
        }

        static IEnumerable<IntPtr> EnumerateProcessWindowHandles(int processId)
        {
            var handles = new List<IntPtr>();

            foreach (ProcessThread thread in Process.GetProcessById(processId).Threads)
                NativeMethods.EnumThreadWindows(thread.Id,
                    (hWnd, lParam) => { handles.Add(hWnd); return true; }, IntPtr.Zero);

            return handles;
        }

    }
}
