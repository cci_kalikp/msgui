﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.Persistence.DelayedLoading;
using MorganStanley.MSDotNet.MSGui.Core.SmartApi;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence.DelayedLoading;

namespace MSDesktop.Wormhole
{
    public static class WormholeExtensions
    {
        public static readonly IDictionary<object, JavaModuleInfo> Registry = new Dictionary<object, JavaModuleInfo>();
        private static bool wormholeEnabled = false;
        public static void EnableWormhole(this Framework framework)
        {
            if (wormholeEnabled) return; 
            ExtensionsUtils.RequireAssembly(ExternalAssembly.NewtonSoftJson); 
            framework.AddModule<WormholeModule>();
            DelayedLoadingExtensions.EnableDelayedLoading();
            DelayedProfileLoadingExtensionPoints.BeforeModuleInitialized += OnBeforeModuleInitialized;
            wormholeEnabled = true;
        }


        private static void OnBeforeModuleInitialized(object sender, BeforeModuleInitializedEventArgs beforeModuleInitializedEventArgs)
        {
            if (beforeModuleInitializedEventArgs.ExtraInformation != null && beforeModuleInitializedEventArgs.ExtraInformation.Count > 0 &&
                (beforeModuleInitializedEventArgs.Module is JavaModuleBase || beforeModuleInitializedEventArgs.Module is JavaHandlerModuleBase))
            {
                var extraInfo = beforeModuleInitializedEventArgs.ExtraInformation;
                var javaInfo = new JavaModuleInfo();
                foreach (string infoName in extraInfo.Keys)
                {
                    if (infoName.ToLower() == "path")
                    {
                        string path = PathUtilities.ReplaceEnvironmentVariables(extraInfo[infoName]); 
                        javaInfo.Path = path;
                    }
                    else if (infoName.ToLower() == "processcommandline")
                    {
                        javaInfo.ProcessCommandLine = extraInfo[infoName];
                    }
                    else if (infoName.ToLower() == "portnumber")
                    {
                        if (!string.IsNullOrEmpty(extraInfo[infoName]))
                        {
                            int portNumber;
                            if (Int32.TryParse(extraInfo[infoName], out portNumber))
                            {
                                javaInfo.PortNumber = portNumber;
                            }
                        }
                    }
                    else if (infoName.ToLower() == "timeoutinseconds")
                    {
                        if (!string.IsNullOrEmpty(extraInfo[infoName]))
                        {
                            int timeoutInSeconds;
                            if (Int32.TryParse(extraInfo[infoName], out timeoutInSeconds))
                            {
                                javaInfo.TimeoutInSeconds = timeoutInSeconds;
                            }
                        }
                    }
                    else if (infoName.ToLower() == "enablekrakencommunication")
                    {
                        javaInfo.EnableKrakenCommunication = extraInfo[infoName].ToLower() == "true";
                    }
                    else
                    {
                        javaInfo.ExtraInfo[infoName] = extraInfo[infoName];
                    }
                }
                Registry[beforeModuleInitializedEventArgs.Module] = javaInfo;
            }
        }
    }

    public class JavaModuleInfo
    {
        public JavaModuleInfo()
        {
            ExtraInfo = new StringDictionary();
        }
        public string Path { get; set; }
        public string ProcessCommandLine { get; set; }
        public int PortNumber { get; set; }
        public bool EnableKrakenCommunication { get; set; }
        public int TimeoutInSeconds { get; set; }
        public StringDictionary ExtraInfo { get; private set; }
    }
}
