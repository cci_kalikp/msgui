﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Windows;
using System.Xml.Linq;
using MSDesktop.Wormhole.Communication;
using MSDesktop.Wormhole.Communication.Tcp;
using MSDesktop.Wormhole.Processors;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MSDesktop.Wormhole
{
    public interface IIpcBasedApplicationProxy
    {
        void SendSocketText(String s);
        event EventHandler<DataEventArgs<string>> SentText;
        event EventHandler<DataEventArgs<string>> ReceiveText;
        void TestShutdownMessage();
    }

    internal class IpcBasedApplicationProxy : IHostedApplicationProxy, IIpcBasedApplicationProxy
    {
        private readonly IWormholeTransport communicator;
        private readonly IUnityContainer container;
        private IList<IMessageProcessor> processors;
        private readonly ApplicationStarter applicationStarter;
        private readonly IHostedApplicationProxyDelegate application;
        private Process targetProcess = null;

        private readonly List<WindowStateHolder> pendingWindowStateQueries = new List<WindowStateHolder>();
        // DOV ADDED SENDING VIA LISTENER SO WE CAN TAP WIRES
        public event EventHandler<DataEventArgs<String>> SentText;
        public event EventHandler<DataEventArgs<String>> ReceiveText;
        //private StatelessReplyHost server;
        private EndPoint activeEndPoint;


        public IpcBasedApplicationProxy(IWormholeTransport transport, 
            IUnityContainer container, 
            ApplicationStarter starter,
            IHostedApplicationProxyDelegate application)
        {
            this.communicator = transport;
            this.container = container;
            this.communicator.MessageReceived += Communicator_OnMessageReceived;
            this.communicator.ClientConnected += Communicator_OnClientConnected;
            this.communicator.ClientDisconnected += Communicator_OnClientDisconnected;
            this.applicationStarter = starter;
            this.application = application;

            this.processors = new List<IMessageProcessor>();

            processors.Add(new CaptureWindowProcessor(application, this));
            processors.Add(new CreateNewWindowProcessor(application, this));
            processors.Add(new GetEnvironmentProcessor(application, this));
            processors.Add(new PlaceWidgetProcessor(application, this));
            processors.Add(new RegisterWindowFactoryProcessor(application, this));
            processors.Add(new SetTargetProcessIdProcessor(application, this));
            processors.Add(new UpdateWindowProcessor(application, this));
            processors.Add(new WindowStateProcessor(application, this));
        }

        public void CloseWindow(string id)
        {
            // DOV: WHAT HAPPENS IF AIR EXITS THEN WE CLOSE A WINDOW? GET A NULLREF BC WE KILLED THE SOCKET
            SendSocketText("<CloseWindow windowID=\"" + id + "\" />");
        }

        public void CreateWindow(string airBridgeId, IWindowViewContainer vc, XDocument state)
        {
            var parameters = vc.Parameters as InitialAirWindowParameters;
            var InitiatedByAir = (parameters != null) && parameters.InitiatedByAir;

            if (!InitiatedByAir)
            {
                var msgRoot = new XElement("CreateWindow");
                msgRoot.SetAttributeValue("factoryID", vc.FactoryID);
                msgRoot.SetAttributeValue("windowID", airBridgeId);
                // normalize - but for now keep this in so the demo works :)
                msgRoot.SetAttributeValue("factoryId", vc.FactoryID);
                msgRoot.SetAttributeValue("windowId", airBridgeId);
                msgRoot.Add(new XComment("window ID and fact ID should be all caps to be consistent. Dov K adding others to let demo work"));
                var doc = new XDocument();

                if (parameters != null && parameters.InitialState != null)
                {
                    msgRoot.Add(new XElement("InitialParameters", parameters.InitialState.Root));
                }
                if (state != null)
                {
                    msgRoot.Add(new XElement("State", state.Root));
                }
                doc.Add(msgRoot);
                SendSocketText(doc.ToString());
            }
        }

        public XDocument SaveState(WindowHolder window)
        {
            var queryId = RequestwindowState(window);
            int i = 0;
            while (i++ < 1000) // time out after 10 seconds
            {
                var query = pendingWindowStateQueries.ToArray().FirstOrDefault(q => q.ID == queryId);
                if (query != null && query.State != null)
                {
                    return new XDocument(query.State);
                }

                Thread.Sleep(10);
            }
            return null;
        }

        public Process Start()
        {
            Application.Current.Exit += Application_OnExit;

            this.communicator.Start();
            
            if (applicationStarter != null)
            {
                /// DOV - IF TARGET EXITS SHOUDL WE RELAUNCH?
                if (targetProcess != null && !targetProcess.HasExited)
                    throw new InvalidOperationException("The process has already been started.");

                var hWndDocked = IntPtr.Zero;
                targetProcess = this.applicationStarter();
                if (targetProcess == null)
                    return null;

                var limit = 10000;
                while (hWndDocked == IntPtr.Zero && limit > 0)
                {

                    try
                    {
                        targetProcess.WaitForInputIdle(1000);
                        limit -= 1000;
                    }
                    catch
                    {
                    }

                    targetProcess.Refresh();

                    if (targetProcess.HasExited)
                    {
                        targetProcess = null;
                        return null;
                    }
                    hWndDocked = targetProcess.MainWindowHandle;
                }
            }

            return targetProcess;
        }

        public void HostClosed()
        {
            // TODO
        }

        private int RequestwindowState(WindowHolder holder)
        {
            var query = new WindowStateHolder()
            {
                WindowHolder = holder
            };
            this.pendingWindowStateQueries.Add(query);
            SendSocketText("<GetWindowState windowID=\"" + holder.AirBridgeID + "\" requestID=\"" + query.ID + "\" />");

            return query.ID;
        }
        
        internal void EnqueueWindowStateQuery(int queryId, XElement state)
        {
            var query = pendingWindowStateQueries.ToArray().FirstOrDefault(q => q.ID == queryId);
            if (query == null)
                return;

            query.State = state;
        }
        
        public void SendSocketText(String s)
        {
            if (!s.EndsWith("\0"))
                s += "\0";

            this.communicator.Send(activeEndPoint, s);

            var sent = SentText;
            if (sent != null)
            {
                sent(null, new DataEventArgs<String>(s));
            }
            //this.socket.SendLn(s + "\0\n");
        }


        #region Communication
        private void Communicator_OnClientDisconnected(object sender, ConnectionEventArgs e)
        {

        }

        private void Communicator_OnClientConnected(object sender, ConnectionEventArgs e)
        {
            this.activeEndPoint = e.RemoteEndPoint;
        }

        private void Communicator_OnMessageReceived(object sender, MessageEventArgs e)
        {
            var doc = XDocument.Parse(e.Message);

            if (doc.Root == null) return;

            var processor = processors.FirstOrDefault(p => p.CanProcess(doc.Root));
            if (processor != null)
            {
                processor.Process(doc.Root);
            }
        }

        #endregion
        
        internal void SendEnvironment()
        {
            if (container.IsRegistered<IMSDesktopEnvironment>())
            {
                var environment = container.Resolve<IMSDesktopEnvironment>();
                SendSocketText(String.Format("<Environment>" +
                                             "<Env>{0}</Env>" +
                                             "<Region>{2}</Region>" +
                                             "<User>{1}</User>" +
                                             "</Environment>",
                                             environment.Environment, environment.User, environment.Region));
            }
            else
            {
                SendSocketText("<Environment />");
            }
            application.OnApplicationReady();
        }

        internal void SetTargetProcessId(string processId)
        {
            targetProcess = Process.GetProcessById(int.Parse(processId));
            SendSocketText(string.Format("<HostProcessId>{0}</HostProcessId>", Process.GetCurrentProcess().Id));
        }
        
        internal void RegisterMessageProcessor(IMessageProcessor processor)
        {
            processors.Add(processor);
        }
        
        private void Application_OnExit(object sender, ExitEventArgs exitEventArgs)
        {
            try
            {
                SendSocketText("<Shutdown />");
            }
            catch
            {
                Debug.WriteLine("DOV: Socket exception thrown when shutting down. Ignoring");
            }

        }

        public void CaptureWindow(string windowID, string desiredTitle)
        {
            application.CaptureWindow(windowID, desiredTitle, targetProcess);
        }

        public void Communicator_OnRawReceive(object sender, DataEventArgs<String> text)
        {
            var recv = ReceiveText;
            string str = String.Copy(text.Value);
            if (recv != null)
                recv(null, new DataEventArgs<String>(str));
        }

        public void TestShutdownMessage()
        {
            Application_OnExit(null, null);
        }
    }
}
