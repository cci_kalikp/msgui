﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading;
using MSDesktop.Wormhole.Communication.Tcp;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.My;

namespace MSDesktop.Wormhole
{
    public abstract class JavaHandlerModuleBase : IMSDesktopModule
    {
        private static readonly IMSLogger Logger = MSLoggerFactory.CreateLogger<JavaHandlerModuleBase>();
        protected readonly IChromeRegistry chromeRegistry;
        protected readonly IChromeManager chromeManager;
        protected readonly IApplication application;
        protected readonly IUnityContainer container;
         

        protected JavaHandlerModuleBase(IChromeRegistry chromeRegistry_, IChromeManager chromeManager_, IApplication application_, IUnityContainer container_)
        {
            chromeRegistry = chromeRegistry_;
            chromeManager = chromeManager_;
            application = application_;
            container = container_;
        }

        public virtual void Initialize()
        { 
            RegisterJavaApplication(StartJavaApplication());
        }

        protected virtual Process GetProcess()
        {
            if (Path != null)
            {
                Logger.InfoWithFormat("Starting: {0}", PathUtilities.ReplaceEnvironmentVariables(Path));
                return Process.Start(PathUtilities.ReplaceEnvironmentVariables(Path));
            }
            if (ProcessCommandLine != null)
            {
                string processCommandLine = PathUtilities.ReplaceEnvironmentVariables(ProcessCommandLine);
                Logger.InfoWithFormat("Trying to attach to a process with command line {0}...", processCommandLine);
                while (true)
                {
                    var mngmtClass = new ManagementClass("Win32_Process");
                    foreach (ManagementObject o in mngmtClass.GetInstances())
                    {
                        if (o["Name"].Equals("javaw.exe"))
                        {
                            var commandLine = (string)o["CommandLine"];
                            if (commandLine.Contains(processCommandLine))
                            {
                                var pid = Convert.ToInt32(o["ProcessId"]);
                                var process = Process.GetProcessById(pid);
                                Logger.InfoWithFormat("Will attach to process with PID: {0}", pid);
                                return process;
                            }
                        }
                    }
                    Thread.Sleep(1000);
                }
            }
            throw new ArgumentException("Neither Path nor ProcessCommandLine was specified");
        }

        protected virtual WormholeBridgedApplication StartJavaApplication()
        {
            var transport = new WormholeTcpServer(PortNumber);
            var javaApplication = new WormholeBridgedApplication(
                GetProcess,
                transport,
                chromeRegistry,
                chromeManager,
                container);
            javaApplication.Start(); 

            application.ApplicationClosed += (sender_, args_) => transport.Stop();
            return javaApplication;
        }

        protected abstract void RegisterJavaApplication(WormholeBridgedApplication hostApplication_);

        protected abstract int PortNumber { get; }

        /// <summary>
        /// Provide a command line to be used to start the Java application. The value can be null if
        /// you decide to override the GetProcess method.
        /// </summary>
        protected virtual string Path { get { return null; } }
        /// <summary>
        /// If specified, the application will try to attach to a javaw.exe process with CommandLine containing
        /// the provided string.
        /// </summary>
        protected virtual string ProcessCommandLine { get { return null; } }
          
    }
}
