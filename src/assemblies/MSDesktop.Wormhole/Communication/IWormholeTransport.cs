﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using MSDesktop.Wormhole.Communication.Tcp;

namespace MSDesktop.Wormhole.Communication
{
    public interface IWormholeTransport
    {
        void Start();
        void Stop();

        bool Send(EndPoint endPoint, string message);

        event EventHandler<ConnectionEventArgs> ClientConnected;
        event EventHandler<ConnectionEventArgs> ClientDisconnected;
        event EventHandler<MessageEventArgs> MessageReceived;
        event EventHandler<MessageEventArgs> MessageSent;
    }
}
