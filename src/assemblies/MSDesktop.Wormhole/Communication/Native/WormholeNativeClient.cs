﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using MSDesktop.MessageRouting.Routing;
using MSDesktop.Wormhole.Communication.Tcp;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;

namespace MSDesktop.Wormhole.Communication.Native
{
    public class WormholeNativeClient
    {
        private readonly ICommunicator communicator;
        /*
         * Each server and client randomly generate an Id that will be used for message source identification to overcome the shortcomings 
         * of MSDesktop M2M. 
         */
        private readonly string id = Guid.NewGuid().ToString();
        private IDisposable subscriber;
        private string serverId;
        private IModulePublisher<RequestMessage> publisher;

        public WormholeNativeClient(ICommunicator communicator)
        {
            this.communicator = communicator;
        }

        public void Connect(string serverId)
        {
            this.serverId = serverId;
            this.subscriber = this.communicator.GetSubscriber<ResponseMessage>().GetObservable().Subscribe(Subscriber_MessageReceived);
            this.publisher = this.communicator.GetPublisher<RequestMessage>();

            this.communicator.GetPublisher<ConnectMessage>().Publish(new ConnectMessage { Sender = this.id });
        }

        public void Disconnect()
        {
            this.communicator.GetPublisher<DisconnectMessage>().Publish(new DisconnectMessage { Sender = this.id });
            this.subscriber.Dispose();
        }

        private void Subscriber_MessageReceived(ResponseMessage msg)
        {
            var handler2 = this.MessageReceived;
            if (handler2 != null)
            {
                handler2(this, new MessageEventArgs(new WormholeNativeEndpoint { Id = msg.Source }, msg.Message, DeliveryStatus.Delivered));
            } 
        }



        public bool Send(string message)
        {
            var msg = new RequestMessage
            {
                Source = this.id,
                Target = this.serverId,
                Message = message
            };

            this.publisher.Publish(msg);

            return false;
        }

        public event EventHandler<MessageEventArgs> MessageReceived;
        public event EventHandler<MessageEventArgs> MessageSent;
    }
}
