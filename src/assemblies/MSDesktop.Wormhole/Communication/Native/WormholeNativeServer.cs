﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Net;
using System.Text;
using MSDesktop.MessageRouting.Routing;
using MSDesktop.Wormhole.Communication.Tcp;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;

namespace MSDesktop.Wormhole.Communication.Native
{
    public sealed class WormholeNativeServer : IWormholeTransport
    {
        /*
         * Each server and client randomly generate an Id that will be used for message source identification to overcome the shortcomings 
         * of MSDesktop M2M. 
         */
        private readonly string id;

        private readonly ICommunicator communicator;
        private IModulePublisher<ResponseMessage> publisher;
        
        private List<string> clients = new List<string>();
        private IDisposable subscriber;
        private IDisposable connectSubscriber;
        private IDisposable disconnectSubscriber;

        public WormholeNativeServer(string id, ICommunicator communicator)
        {
            this.id = id;
            this.communicator = communicator;
        }

        public void Start()
        {
            this.publisher = this.communicator.GetPublisher<ResponseMessage>();

            this.subscriber = this.communicator.GetSubscriber<RequestMessage>().GetObservable().Subscribe(Subscriber_MessageReceived);
            this.connectSubscriber = this.communicator.GetSubscriber<ConnectMessage>().GetObservable().Subscribe(ConnectSubscriber_MessageReceived);
            this.disconnectSubscriber = this.communicator.GetSubscriber<DisconnectMessage>().GetObservable().Subscribe(DisconnectSubscriber_MessageReceived);
        }

        private void ConnectSubscriber_MessageReceived(ConnectMessage msg)
        {
            if (this.clients.Contains(msg.Sender)) 
                return;

            this.clients.Add(msg.Sender);
            var handler = this.ClientConnected;
            if (handler != null)
            {
                handler(this, new ConnectionEventArgs(new WormholeNativeEndpoint { Id = msg.Sender }));
            }
        }

        private void DisconnectSubscriber_MessageReceived(DisconnectMessage msg)
        {
            if (!this.clients.Contains(msg.Sender)) 
                return;

            this.clients.Remove(msg.Sender);
            var handler = this.ClientDisconnected;
            if (handler != null)
            {
                handler(this, new ConnectionEventArgs(new WormholeNativeEndpoint { Id = msg.Sender }));
            }
        }

        private void Subscriber_MessageReceived(RequestMessage msg)
        {
            var handler2 = this.MessageReceived;
            if (handler2 != null)
            {
                handler2(this, new MessageEventArgs(new WormholeNativeEndpoint { Id = msg.Source }, msg.Message, DeliveryStatus.Delivered));
            }
        }

        public void Stop()
        {
            this.subscriber.Dispose();
        }

        public bool Send(EndPoint endPoint, string message)
        {
            var msg = new ResponseMessage
            {
                Source = this.id,
                Target = (endPoint as WormholeNativeEndpoint).Id,
                Message = message
            };

            this.publisher.Publish(msg);

            return false;
        }

        public event EventHandler<ConnectionEventArgs> ClientConnected;
        public event EventHandler<ConnectionEventArgs> ClientDisconnected;
        public event EventHandler<MessageEventArgs> MessageReceived;
        public event EventHandler<MessageEventArgs> MessageSent;
    }

    public class WormholeNativeEndpoint : EndPoint
    {
        public string Id { get; set; }
    }
}
