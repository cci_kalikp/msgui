﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSDesktop.Wormhole.Communication.Native
{
    /*
     * This file contains two message types used for native MSDesktop wormhole communication. We need two classes as the M2M communicator 
     * used here does not support fully duplex communication but we need to distinguish between messages send to and from the server.
     */


    /// <summary>
    /// A message sent to a <see cref="WormholeNativeServer"/> instance.
    /// </summary>
    [Serializable]
    public class RequestMessage
    {
        public string Source { get; set; }
        public string Target { get; set; }

        public string Message { get; set; }
    }

    /// <summary>
    /// A message send by the <see cref="WormholeNativeServer"/> instance.
    /// </summary>
    [Serializable]
    public class ResponseMessage
    {
        public string Source { get; set; }
        public string Target { get; set; }
        public string Message { get; set; }
    }

    public class ConnectMessage
    {
        public string Sender { get; set; }
    }
    public class DisconnectMessage
    {
        public string Sender { get; set; }
    }
}
