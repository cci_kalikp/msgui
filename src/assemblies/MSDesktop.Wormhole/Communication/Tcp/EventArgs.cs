﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Text;
using MSDesktop.MessageRouting.Routing;

namespace MSDesktop.Wormhole.Communication.Tcp
{

    public class ConnectionEventArgs : EventArgs
    {
        public EndPoint RemoteEndPoint { get; private set; }

        public ConnectionEventArgs(EndPoint remoteEndPoint)
        {
            RemoteEndPoint = remoteEndPoint;
        }
    }

    public class MessageEventArgs : EventArgs, INotifyPropertyChanged
    {
        private DeliveryStatus status;

        public EndPoint RemoteEndPoint { get; private set; }
        public string Message { get; private set; }
        public DeliveryStatus Status
        {
            get { return status; }
            set
            {
                if (value == status) return;
                status = value;
                OnPropertyChanged("Status");
            }
        }

        public MessageEventArgs(EndPoint remoteEndPoint, string message, DeliveryStatus status)
        {
            this.status = status;
            RemoteEndPoint = remoteEndPoint;
            Message = message;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
