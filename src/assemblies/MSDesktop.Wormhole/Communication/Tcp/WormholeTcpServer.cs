﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using MSDesktop.MessageRouting.Routing;

namespace MSDesktop.Wormhole.Communication.Tcp
{
    public sealed class WormholeTcpServer : IWormholeTransport
    {
        private readonly IPAddress address;
        private readonly int port;
        private readonly AutoResetEvent connectionWaitHandle = new AutoResetEvent(false);
        private readonly Thread listenerThread;
        private readonly ConcurrentDictionary<EndPoint, TcpClient> clients = new ConcurrentDictionary<EndPoint, TcpClient>();


        public event EventHandler<ConnectionEventArgs> ClientConnected;
        public event EventHandler<ConnectionEventArgs> ClientDisconnected;
        public event EventHandler<MessageEventArgs> MessageReceived;
        public event EventHandler<MessageEventArgs> MessageSent;

        /// <summary>
        /// Create a new server
        /// </summary>
        /// <param name="address">IP address</param>
        /// <param name="port">Port to listen on</param>
        public WormholeTcpServer(int port)
        {
            this.address = IPAddress.Loopback;
            this.port = port;
            listenerThread = new Thread(Listen) {IsBackground = true};
        }

        /// <summary>
        /// Start listening
        /// </summary>
        public void Start()
        {
            listenerThread.Start();
        }

        /// <summary>
        /// Stop listening
        /// </summary>
        public void Stop()
        {
            listenerThread.Interrupt();
        }

        /// <summary>
        /// Send message
        /// </summary>
        /// <param name="endPoint">Remote endpoint</param>
        /// <param name="message">Message</param>
        /// <returns></returns>
        public bool Send(EndPoint endPoint, string message)
        {
            TcpClient client;
            MessageEventArgs e = null;

            if (clients.TryGetValue(endPoint, out client))
            {
                try
                {

                    var msgHandler = MessageSent;
                    if (msgHandler != null)
                    {
                        e = new MessageEventArgs(client.Client.RemoteEndPoint, message, DeliveryStatus.Pending);
                        msgHandler(this, e);
                    }

                    var bytes = Encoding.ASCII.GetBytes(message);
                    client.GetStream().Write(bytes, 0, bytes.Length);
                    if (e != null)
                    {
                        e.Status = DeliveryStatus.Delivered;
                    }

                    return true;
                }
                catch (IOException)
                {

                }
                catch (SocketException)
                {

                }
                catch (InvalidOperationException)
                {

                }
            }

            if (e != null)
            {
                e.Status = DeliveryStatus.Failed;
            }

            return false;
        }

        private void Listen()
        {
            var listener = new TcpListener(address, port);
            listener.Start();

            while (true)
            {
                try
                {
                    listener.BeginAcceptTcpClient(HandleAsyncConnection, listener);
                    connectionWaitHandle.WaitOne(); //Wait until a client has begun handling an event
                    connectionWaitHandle.Reset(); //Reset wait handle or the loop goes as fast as it can (after first request)
                }
                catch (ThreadInterruptedException)
                {
                    break;
                }
            }

            //listener.Stop();
        }

        private void HandleAsyncConnection(IAsyncResult result)
        {
            var listener = (TcpListener)result.AsyncState;
            var client = listener.EndAcceptTcpClient(result);
            connectionWaitHandle.Set(); //Inform the main thread this connection is now handled

            //TcpClient logic
            clients.TryAdd(client.Client.RemoteEndPoint, client);

            var handler = ClientConnected;
            if (handler != null)
            {
                handler(this, new ConnectionEventArgs(client.Client.RemoteEndPoint));
            }

            new Thread(CheckClient){IsBackground = true}.Start(new Tuple<TcpClient, EndPoint>(client, client.Client.RemoteEndPoint));

            try
            {
                var sb = new StringBuilder();
                var stream = client.GetStream();
                var bytes = new byte[8192];
                int numBytes;

                //Loop to receive all the data sent by the client. 
                while ((numBytes = stream.Read(bytes, 0, bytes.Length)) != 0)
                {
                    string data = Encoding.ASCII.GetString(bytes, 0, numBytes);
                    var parts = data.Split('\0');
                    sb.Append(parts[0]);

                    if (parts.Length > 1)
                    {
                        var msgHandler = MessageReceived;
                        if (msgHandler != null)
                        {
                            msgHandler(this, new MessageEventArgs(client.Client.RemoteEndPoint, sb.ToString(), DeliveryStatus.Delivered));
                        }

                        for (int i = 1; i < parts.Length - 1; i++)
                        {
                            if (msgHandler != null)
                            {
                                msgHandler(this, new MessageEventArgs(client.Client.RemoteEndPoint, parts[i], DeliveryStatus.Delivered));
                            }
                        }

                        sb = new StringBuilder(parts[parts.Length - 1]);
                    }
                }
            }
            catch (IOException)
            {

            }
            catch (SocketException)
            {

            }
            catch (InvalidOperationException)
            {

            }

            client.Close();
        }

        private void CheckClient(object param)
        {
            var tuple = param as Tuple<TcpClient, EndPoint>;
            var client = tuple.Item1;
            var endPoint = tuple.Item2;
            var buff = new byte[1];

            while (true)
            {
                try
                {
                    if (client.Client.Poll(0, SelectMode.SelectRead))
                    {
                        if (client.Client.Receive(buff, SocketFlags.Peek) == 0)
                        {
                            break;
                        }
                    }
                }
                catch (IOException)
                {
                    break;
                }
                catch (SocketException)
                {
                    break;
                }
                catch (ObjectDisposedException)
                {
                    break;
                }

                Thread.Sleep(100);
            }

            clients.TryRemove(endPoint, out client);

            var handler = ClientDisconnected;
            if (handler != null)
            {
                handler(this, new ConnectionEventArgs(endPoint));
            }
        }
    }
}
