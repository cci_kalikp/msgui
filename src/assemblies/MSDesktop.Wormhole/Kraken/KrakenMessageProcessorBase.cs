﻿using System.Xml.Linq;
using MSDesktop.Wormhole.Processors;

namespace MSDesktop.Wormhole.Kraken
{
    abstract class KrakenMessageProcessorBase : IMessageProcessor
    {
        protected readonly KrakenProxy Proxy;

        protected KrakenMessageProcessorBase(KrakenProxy proxy)
        {
            this.Proxy = proxy;
        }

        protected abstract string NodeName { get; }

        public bool CanProcess(XElement element)
        {
            return element.Name.ToString().Equals(NodeName);
        }

        public abstract void Process(XElement element);
    }
}
