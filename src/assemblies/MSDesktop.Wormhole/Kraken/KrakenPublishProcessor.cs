﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using MSDesktop.Wormhole.Processors;
using Newtonsoft.Json;

namespace MSDesktop.Wormhole.Kraken
{
    class KrakenPublishProcessor : IMessageProcessor
    {
        private readonly KrakenProxy proxy;

        public KrakenPublishProcessor(KrakenProxy proxy)
        {
            this.proxy = proxy;
        }

        public bool CanProcess(XElement element)
        {
            return element.Name.ToString().Equals("KrakenPublish");
        }

        public void Process(XElement element)
        {
            var publisherIdAttribute = element.Attribute("publisherId");
            if (publisherIdAttribute == null) return;
            var publisherId = publisherIdAttribute.Value;

            var typeAttribute = element.Attribute("type");
            if (typeAttribute == null) return;
            var type = typeAttribute.Value;

            var requestIdAttribute = element.Attribute("requestId");
            if (requestIdAttribute == null) return;
            var requestId = requestIdAttribute.Value;

            var content = element.Value;
            var message = JsonConvert.DeserializeObject(content, Type.GetType(type));

            proxy.Publish(publisherId, Type.GetType(type), message, requestId);
        }
    }
}
