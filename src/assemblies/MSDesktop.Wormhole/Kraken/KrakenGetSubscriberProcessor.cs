﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using MSDesktop.Wormhole.Processors;

namespace MSDesktop.Wormhole.Kraken
{
    class KrakenGetSubscriberProcessor : KrakenMessageProcessorBase
    {
        public KrakenGetSubscriberProcessor(KrakenProxy proxy) : base(proxy)
        {
        }

        protected override string NodeName
        {
            get
            {
                return "KrakenGetSubscriber";
            }
        }

        public override void Process(XElement element)
        {
            var publisherIdAttribute = element.Attribute("id");
            if (publisherIdAttribute == null) return;
            var subscriberId = publisherIdAttribute.Value;

            var typeAttribute = element.Attribute("type");
            if (typeAttribute == null) return;
            var type = typeAttribute.Value;

            Proxy.GetSubscriber(subscriberId, type);
        }
    }
}
