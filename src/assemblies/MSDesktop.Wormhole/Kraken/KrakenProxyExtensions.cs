﻿using System;
using MSDesktop.MessageRouting;
using Microsoft.Practices.Unity;

namespace MSDesktop.Wormhole.Kraken
{
    public static class KrakenProxyExtensions
    {
        public static void EnableKrakenCommunication(this WormholeBridgedApplication application)
        {
            var applicationProxy = application.IpcBasedApplicationProxy as IpcBasedApplicationProxy;
            if (applicationProxy == null) throw new InvalidOperationException("Cannot enable Kraken communication without transport");
            var krakenProxy = new KrakenProxy(application.Container.Resolve<IRoutingCommunicator>(), applicationProxy);
            applicationProxy.RegisterMessageProcessor(new KrakenGetPublisherProcessor(krakenProxy));
            applicationProxy.RegisterMessageProcessor(new KrakenPublishProcessor(krakenProxy));
            applicationProxy.RegisterMessageProcessor(new KrakenGetSubscriberProcessor(krakenProxy));
        }
    }
}
