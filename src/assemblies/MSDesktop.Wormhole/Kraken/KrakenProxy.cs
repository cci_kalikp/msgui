﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using MSDesktop.MessageRouting;
using MSDesktop.MessageRouting.Routing;
using Newtonsoft.Json;

namespace MSDesktop.Wormhole.Kraken
{
    class KrakenProxy
    {
        private readonly IRoutingCommunicator communicator;
        private readonly IpcBasedApplicationProxy applicationProxy;
        private readonly Dictionary<string, object> publishers;

        public KrakenProxy(IRoutingCommunicator communicator, IpcBasedApplicationProxy applicationProxy)
        {
            this.communicator = communicator;
            this.applicationProxy = applicationProxy;
            this.publishers = new Dictionary<string, object>();
        }

        public void GetPublisher(string publisherId, string typeName)
        {
            var type = Type.GetType(typeName);
            if (type == null) throw new ArgumentException(string.Format("Provided type {0} does not exist", typeName));
            var publisher = typeof (IRoutingCommunicator).GetMethod("GetPublisher")
                                                         .MakeGenericMethod(type)
                                                         .Invoke(communicator, new object[] {});
            publishers[publisherId] = publisher;
        }

        public void Publish(string publisherId, Type type, object message, string requestId)
        {
            var publisher = publishers[publisherId];
            var info = (DeliveryInfo) typeof (IRoutedPublisher<>).MakeGenericType(type)
                                                  .GetMethod("Publish")
                                                  .Invoke(publisher, new[] {message});
            applicationProxy.SendSocketText(string.Format("<KrakenDeliveryInfo id=\"{0}\" hostname=\"{1}\" " +
                                                     "application=\"{2}\" username=\"{3}\" requestId=\"{4}\" publisherId=\"{5}\" />",
                                                     info.Id, info.Target.Hostname, info.Target.Application, info.Target.Username, requestId, publisherId));
        }

        public void GetSubscriber(string subscriberId, string typeName)
        {
            var type = Type.GetType(typeName);
            if (type == null) throw new ArgumentException(string.Format("Provided type {0} does not exist", typeName));
            var subscriber = typeof (IRoutingCommunicator).GetMethod("GetSubscriber")
                                                          .MakeGenericMethod(type)
                                                          .Invoke(communicator, new object[] {});
            var observable = typeof (IRoutedSubscriber<>).MakeGenericType(type)
                                                         .GetMethod("GetObservable")
                                                         .Invoke(subscriber, new object[] {});
            GetType()
                .GetMethod("DoSubscribe", BindingFlags.Instance | BindingFlags.NonPublic)
                .MakeGenericMethod(type)
                .Invoke(this, new object[] {observable, subscriberId, typeName});
        }

        private void DoSubscribe<T>(IObservable<T> observable, string subscriberId, string typeName)
        {
            observable.Subscribe(obj =>
                {
                    var content = JsonConvert.SerializeObject(obj);
                    applicationProxy.SendSocketText(string.Format("<KrakenForwardedMessage subscriberId=\"{0}\" type=\"{2}\">{1}</KrakenForwardedMessage>",
                        subscriberId, content, typeName));
                });
        }
    }
}
