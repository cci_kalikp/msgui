﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using MSDesktop.Wormhole.Processors;

namespace MSDesktop.Wormhole.Kraken
{
    class KrakenGetPublisherProcessor : IMessageProcessor
    {
        private readonly KrakenProxy proxy;

        public KrakenGetPublisherProcessor(KrakenProxy proxy)
        {
            this.proxy = proxy;
        }

        public bool CanProcess(XElement element)
        {
            return element.Name.ToString().Equals("KrakenGetPublisher");
        }

        public void Process(XElement element)
        {
            var publisherIdAttribute = element.Attribute("id");
            if (publisherIdAttribute == null) return;
            var publisherId = publisherIdAttribute.Value;

            var typeAttribute = element.Attribute("type");
            if (typeAttribute == null) return;
            var type = typeAttribute.Value;

            proxy.GetPublisher(publisherId, type);
        }
    }
}
