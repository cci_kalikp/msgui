﻿using System;
using System.Windows;
using System.Windows.Data;

namespace MSDesktop.LoaderConfigEditor.Converters
{
    public class PropertyValueToFontWeightConverter:IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null) return FontWeights.Normal; 
            return (bool) value ? FontWeights.Normal : FontWeights.Bold;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
