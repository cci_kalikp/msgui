﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace MSDesktop.LoaderConfigEditor.Converters
{
    public class ExpandedToDescroptionConverter:IValueConverter
    {
        private readonly string trueText;
        private readonly string falseText;
        public ExpandedToDescroptionConverter(string trueText_, string falseText_)
        {
            trueText = trueText_;
            falseText = falseText_;
        } 
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null || System.Convert.ToBoolean(value)) return trueText;
            return falseText;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
