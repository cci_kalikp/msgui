﻿using System;
using System.Windows.Data;
using System.Xml.Serialization;
using MSDesktop.LoaderConfigEditor.Helpers;

namespace MSDesktop.LoaderConfigEditor.Converters
{
    public class TypeDescriptorToNameConverter:IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            ConfigurationTypeDescriptor descriptor = value as ConfigurationTypeDescriptor;
            if (descriptor != null)
            {
                var type = descriptor.SourceObject.GetType();
                object[] attributes = type.GetCustomAttributes(typeof (XmlRootAttribute), false);
                if (attributes == null || attributes.Length == 0)
                {
                    return descriptor.SourceObject.GetType().Name;
                }
                return ((XmlRootAttribute) attributes[0]).ElementName;
            }
            return value.GetType().Name;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
