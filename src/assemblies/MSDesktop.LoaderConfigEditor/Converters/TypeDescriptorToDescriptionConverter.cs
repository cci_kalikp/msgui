﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Data;
using System.Windows.Documents;
using System.Xml.Serialization;
using MSDesktop.LoaderConfigEditor.Helpers;
using MorganStanley.Desktop.Loader.Configuration.Designer;

namespace MSDesktop.LoaderConfigEditor.Converters
{
    public class TypeDescriptorToDescriptionConverter:IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            ConfigurationTypeDescriptor descriptor = value as ConfigurationTypeDescriptor;
            if (descriptor != null)
            {
                var type = descriptor.SourceObject.GetType();
                object[] attributes = type.GetCustomAttributes(typeof(PropertyDescriptionAttribute), false);
                if (attributes.Length > 0)
                {
                    var description = (PropertyDescriptionAttribute)attributes[0];
                    FlowDocument document = new FlowDocument();
                    document.PagePadding = new Thickness(1); 
                    var paragraph = new Paragraph();
                    paragraph.Inlines.Add(new Run("Description: ") { FontWeight = FontWeights.Bold });
                    paragraph.Inlines.Add(new Run(description.Description));
                    document.Blocks.Add(paragraph);
                    if (!string.IsNullOrEmpty(description.CodeWiki))
                    {
                        string codeWiki = null;
                        if (description.CodeWikiType != CodeWikiType.External)
                        {
                            codeWiki = "http://codewiki.webfarm.ms.com/msdotnet/#!/" + description.CodeWikiType +
                                       "/" + description.CodeWiki.TrimEnd('/') + "/";
                        }
                        else
                        {
                            codeWiki = description.CodeWiki;
                        }

                        var paragraph2 = new Paragraph();
                        paragraph2.Inlines.Add(new Run("CodeWiki: ") { FontWeight = FontWeights.Bold });
                        var link = new Hyperlink(new Run(codeWiki)) { NavigateUri = new Uri(codeWiki) };
                        link.RequestNavigate += (sender_, args_) => Process.Start(args_.Uri.ToString());
                        paragraph2.Inlines.Add(link);
                        document.Blocks.Add(paragraph2);

                    }

                    return document;
                } 
            }
            return  null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
