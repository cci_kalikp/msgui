﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Xml.Serialization;
using MSDesktop.LoaderConfigEditor.Helpers;
using MSDesktop.LoaderConfigEditor.ViewModels;
using MSDesktop.LoaderConfigEditor.Views;
using MorganStanley.Desktop.Loader.Configuration.Helpers;

namespace MSDesktop.LoaderConfigEditor
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>


    public partial class App : Application
    {
         
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e); 
            if (e.Args.FirstOrDefault(arg_ => arg_ == "/GenerateProperties") != null)
            {
                try
                {
                    var config = new ConfigViewModel(new ConfigsViewModel()); 
                    var properties = config.Configuration.ListAllProperties(string.Empty);
                    var serializer = new XmlSerializer(typeof(List<Property>));
                    using (var fs = new FileStream(ConfigurationTypeDescriptor.PropertyDefinitionStore, FileMode.OpenOrCreate))
                    {
                        serializer.Serialize(fs, properties);
                    }
                    Current.Shutdown(0);
                    return;
                }
                catch 
                { 
                }
              
            }

            var context = new ConfigsViewModel();
            foreach (var configFile in e.Args)
            {
                context.LoadConfigCommand.Execute(configFile);
            } 
            var editor = new Editor();
            editor.Resources.MergedDictionaries.Add(new ResourceDictionary
            {
                Source = new Uri("pack://application:,,,/MSDesktop.LoaderConfigEditor;component/Resources/Styles.xaml",
                                 UriKind.RelativeOrAbsolute)
            });
            editor.DataContext = context;
            this.MainWindow = editor;
            editor.Show();
        } 

    }
}
