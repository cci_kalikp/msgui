﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml.Serialization;
using MSDesktop.LoaderConfigEditor.Editors;
using MSDesktop.LoaderConfigEditor.ViewModels;
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.Desktop.Loader.Configuration.Helpers;
using MorganStanley.Desktop.Loader.Infrastructure;
using MorganStanley.MSDotNet.MSGui.Controls.SearchBox;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;
using Xceed.Wpf.Toolkit.PropertyGrid;
using Xceed.Wpf.Toolkit.PropertyGrid.Attributes;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace MSDesktop.LoaderConfigEditor.Helpers
{
    public class ConfigurationItemPropertyDescriptor: PropertyDescriptor, INotifyPropertyChanged
    {
        public event EventHandler PropertyValueChanged;
        private readonly IModifiableConfigViewModel vm;
        private readonly PropertyDescriptor propertyDescriptor;
        public object Owner { get; private set; }
        private object defaultValue = NoValue;
        private static readonly object NoValue = new object();
        private object oldValue = NoValue; 
        private bool valueChanged = false;
        private readonly object defaultOwner;
        private string codeWiki = null;
        private readonly ConfigurationTypeDescriptor typeDescriptor;
        
        public ConfigurationItemPropertyDescriptor(IModifiableConfigViewModel vm_, ConfigurationTypeDescriptor typeDescriptor_, object defaultOwner_, PropertyDescriptor propertyDescriptor_)
            : base(propertyDescriptor_.Name, propertyDescriptor_.Attributes.OfType<Attribute>().ToArray())
        {
            vm = vm_;
            typeDescriptor = typeDescriptor_;
            Owner = typeDescriptor_.SourceObject;
            defaultOwner = defaultOwner_;
            propertyDescriptor = propertyDescriptor_;
            TogglableComplexConfigurationItem configuration = Owner as TogglableComplexConfigurationItem;
            if (configuration != null)
            {
                if (propertyDescriptor_.Name != configuration.EnablementPropertyName)
                {
                    EditableSource = configuration.EnablementPropertyName;
                    EditableSourceConverter = configuration.GetEnablementSourceConverter();
                    EditableHintFormat = configuration.EnablementDescriptionFormat;
                }
            }
            BindableEnablementAttribute attr =
                 propertyDescriptor_.Attributes[typeof(BindableEnablementAttribute)] as BindableEnablementAttribute;
            if (attr != null)
            {
                EditableSource = attr.BindToPropertyName;
                if (attr.ConverterType != null)
                {
                    EditableSourceConverter = Activator.CreateInstance(attr.ConverterType) as IValueConverter;
                }
                else
                {
                    EditableSourceConverter = null;
                }
                EditableHintFormat = attr.DescriptionFormat; 
            }

            BindableValueAttribute attr2 =
                propertyDescriptor_.Attributes[typeof (BindableValueAttribute)] as BindableValueAttribute;
            if (attr2 != null)
            {
                ValueSource = attr2.BindToPropertyName;
                if (attr2.ConverterType != null)
                {
                    ValueSourceConverter = Activator.CreateInstance(attr2.ConverterType) as IMultiValueConverter;
                }
            }
            
            ResetCommand = new DelegateCommand(o_ => ResetValue(Owner),
                o_ => this.CanResetValue(Owner));
            
            RestoreCommand = new DelegateCommand(o_ =>
                {
                    if (oldValue != NoValue)
                    { 
                        this.SetValue(Owner, oldValue);
                        valueChanged = false;
                    }
                }, o_=> valueChanged);
            EditLocallyCommand = new DelegateCommand(o_ =>
                {
                    if (this.PropertyItem == null) return;

                    ConfigurationTypeDescriptor descriptor = PropertyItem.Value as ConfigurationTypeDescriptor;
                    if (descriptor == null) return;
                    var value = descriptor.SourceObject as ExternalComplexConfigurationItem;
                    if (value == null) return;
                    var property =
                        descriptor.GetProperties()[value.EnablementPropertyName] as
                        ConfigurationItemPropertyDescriptor;
                    if (property == null) return;
                    if (!string.IsNullOrEmpty(value.DefinitionFile))
                    {
                        property.SetValue(Owner, null);
                        valueChanged = true;
                    }
                    if (PropertyItem != null)
                    {
                        PropertyItem.IsExpanded = true;
                    }
                }, o_ => PropertyItem != null);
            EditExternallyCommand = new DelegateCommand(o_ =>
            {
                if (this.PropertyItem == null) return;
                ConfigFilePathEditor editor = PropertyItem.Editor as ConfigFilePathEditor;
                if (editor != null)
                {
                    editor.EditExternallyCommand.Execute(o_);
                }
                if (PropertyItem != null)
                {
                    PropertyItem.IsExpanded = false;
                }
            }, o_ => PropertyItem != null);
            VisitCodeWikiCommand = new DelegateCommand(o_ => Process.Start(codeWiki), o_=> codeWiki != null); 
        }

        public override string Name
        {
            get { return propertyDescriptor.Name; }
        }

        public override string DisplayName
        {
            get
            {
                return propertyDescriptor.DisplayName;
            }
        } 
        public FlowDocument RichDescription { get; private set; }
        public bool HasSnapShot { get; private set; }
        public ImageSource SnapShot { get; private set; }
        public bool HasAlternateSnapShot { get; private set; }
        public ImageSource AlternateSnapShot { get; private set; }
        private string fullName;
        public string FullName
        {
            get
            {
                if (fullName == null)
                {
                    fullName = GetFullName();
                }
                return fullName;
            }
        }

        protected override AttributeCollection CreateAttributeCollection()
        {
            PropertyDescriptionAttribute descrAttr = null;
            bool canDisplayAsString = false;
            List<Attribute> attrs = new List<Attribute>();
            foreach (Attribute attribute in base.CreateAttributeCollection())
            { 
                if (attribute is XmlIgnoreAttribute)
                {
                    attrs.Add(new BrowsableAttribute(false));
                }
                else if (attribute is PathAttribute)
                { 
                    if (defaultOwner.GetType() == typeof (CentralizedPlacementHelper))
                    {
                        attrs.Add(new EditorAttribute(typeof(ControlPlacementFilePathEditor), typeof(ControlPlacementFilePathEditor)));  
                    }
                    else if (((PathAttribute) attribute).PathType == PathType.Directory)
                    {
                        attrs.Add(new EditorAttribute(typeof(DirectoryPathEditor), typeof(DirectoryPathEditor))); 
                    }
                    else
                    {
                        attrs.Add(new EditorAttribute(typeof(FilePathEditor), typeof(FilePathEditor)));
                    }
                    continue;
                }
                else if (attribute is FontAttribute)
                {
                    attrs.Add(new EditorAttribute(typeof(FontEditor), typeof(FontEditor)));
                    continue;
                }
                else if (attribute is OrderAttribute)
                {
                    attrs.Add(new PropertyOrderAttribute(((OrderAttribute)attribute).Order));
                    continue;
                }
                else if (attribute is DefaultValueAttribute)
                {
                    this.defaultValue = ((DefaultValueAttribute)attribute).Value;
                    continue;
                }
                else if (attribute is PropertyDescriptionAttribute)
                {
                    descrAttr = (PropertyDescriptionAttribute)attribute;
                    continue;
                }
                else if (attribute is EditAsStringAttribute)
                {
                    canDisplayAsString = true;
                    continue;
                }
                attrs.Add(attribute); 
            }
            if (defaultValue == NoValue)
            {
                defaultValue = defaultOwner.GetType().GetProperty(propertyDescriptor.Name).GetValue(defaultOwner, null); 
            }
            FlowDocument doc;
            string description;
            GetDescription(descrAttr, out description, out doc);
            attrs.Add(new DescriptionAttribute(description));  
            RichDescription = doc;
            OnPropertyChanged("RichDescription");
            var image = GetSnapShot(descrAttr, false);
            if (image != null)
            {
                SnapShot = image;
                HasSnapShot = true;
                OnPropertyChanged("SnapShot");
                OnPropertyChanged("HasSnapShot");
                if (descrAttr != null && !string.IsNullOrEmpty(descrAttr.SnapShotPart))
                {
                    var alternateImage = GetSnapShot(descrAttr, true);
                    AlternateSnapShot = alternateImage;
                    HasAlternateSnapShot = true;
                    OnPropertyChanged("AlternateSnapShot");
                    OnPropertyChanged("HasAlternateSnapShot");
                }
            }

            TogglableComplexConfigurationItem configuration = Owner as TogglableComplexConfigurationItem;
            if (configuration != null && propertyDescriptor.Name == configuration.EnablementPropertyName)
            {
                attrs.Add(new BrowsableAttribute(false));
            } 
            if (typeof(ComplexConfigurationItem).IsAssignableFrom(propertyDescriptor.PropertyType))
            {
                attrs.Add(new ExpandableObjectAttribute());
                if (typeof(EnablableComplexConfigurationItem).IsAssignableFrom(propertyDescriptor.PropertyType))
                {
                    attrs.Add(new EditorAttribute(typeof(EnablableConfigurationItemEditor), typeof(EnablableConfigurationItemEditor)));
                }
                else if (typeof(DisablableComplexConfigurationItem).IsAssignableFrom(propertyDescriptor.PropertyType))
                {
                    attrs.Add(new EditorAttribute(typeof(DisablableConfigurationItemEditor), typeof(DisablableConfigurationItemEditor)));
                }
                else if (typeof (ExternalComplexConfigurationItem).IsAssignableFrom(propertyDescriptor.PropertyType))
                {
                    attrs.Add(new EditorAttribute(typeof(ConfigFilePathEditor), typeof(ConfigFilePathEditor)));
                }
                else if (!canDisplayAsString)
                {
                    attrs.Add(new EditorAttribute(typeof(ExpandableConfigurationItemEditor), typeof(ExpandableConfigurationItemEditor)));
                }
            } 
            else if (propertyDescriptor.PropertyType == typeof (ColorHelper))
            {
                attrs.Add(new EditorAttribute(typeof (ColorHelperEditor), typeof (ColorHelperEditor)));
            }
            else
            {
                Type listItemType = GetListItemType(propertyDescriptor.PropertyType);
                if (listItemType != null)
                {
                    if (!listItemType.IsPrimitive && listItemType != typeof(string))
                    {
                         attrs.Add(new EditorAttribute(typeof(ConfigurationItemCollectionEditor), typeof(ConfigurationItemCollectionEditor)));
                    } 
                }
            }
            
            if (propertyDescriptor.PropertyType == typeof (PointHelper) || 
                propertyDescriptor.PropertyType == typeof(SizeHelper) ||
                propertyDescriptor.PropertyType == typeof(KeyGestureHelper))
            {
                attrs.Add(new EditorAttribute(typeof(TextBoxEditor), typeof(TextBoxEditor)));
            }

            if (propertyDescriptor.PropertyType == typeof (ModifierKeys))
            {
                attrs.Add(new ItemsSourceAttribute(typeof(ModifierKeysSource)));
            }
            return new AttributeCollection(attrs.ToArray());
        }
         
        private ImageSource GetSnapShot(PropertyDescriptionAttribute descriptionAttribute_, bool alternate_)
        {
            if (descriptionAttribute_ == null || descriptionAttribute_.SnapShots == null || descriptionAttribute_.SnapShots.Length == 0)
            {
                return null;
            }
            Color color = string.IsNullOrEmpty(descriptionAttribute_.FlashColor)
                                 ? Colors.Red
                                 : (Color)ColorConverter.ConvertFromString(descriptionAttribute_.FlashColor);
            if (!string.IsNullOrEmpty(descriptionAttribute_.SnapShotPart) && !descriptionAttribute_.SnapShotPartForComparison)
            { 
                return SnapShotHelper.GetSnapShot(descriptionAttribute_.SnapShots, alternate_ ? null : descriptionAttribute_.SnapShotPart, color);
            }
            var snapShots = new Dictionary<string, string>();
            if (descriptionAttribute_.SnapShotDescriptions != null &&
                descriptionAttribute_.SnapShotDescriptions.Length > 0)
            {
                int length = Math.Min(descriptionAttribute_.SnapShots.Length,
                                      descriptionAttribute_.SnapShotDescriptions.Length);
                for (int i = 0; i < length; i++)
                {
                    snapShots.Add(descriptionAttribute_.SnapShots[i], descriptionAttribute_.SnapShots[i]);
                }
            }
            else if (propertyDescriptor.PropertyType == typeof (bool))
            {
                if (descriptionAttribute_.SnapShots.Length >= 2)
                {
                    snapShots.Add(descriptionAttribute_.SnapShots[0], "False");
                    snapShots.Add(descriptionAttribute_.SnapShots[1], "True");
                }
            }
            else if (propertyDescriptor.PropertyType.IsEnum)
            {
                var enums = Enum.GetNames(propertyDescriptor.PropertyType);
                for (int i = 0; i < Math.Min(enums.Length, descriptionAttribute_.SnapShots.Length); i++)
                {
                    snapShots.Add(descriptionAttribute_.SnapShots[i], enums[i]); 
                }
            } 
            
            if (snapShots.Count > 0)
            {
                return SnapShotHelper.GetComparisonSnapShot(snapShots, alternate_ ? null : descriptionAttribute_.SnapShotPart, color); 
            }
            
            return SnapShotHelper.GetSnapShot(descriptionAttribute_.SnapShots, null, color);
        }

        private string GetFullName()
        {
            string name = this.Name;
            var descriptor = this;
            while (descriptor != null)
            {
                var parent = descriptor.typeDescriptor;
                if (parent != null && parent.SourceDescriptor != null)
                {
                    name = parent.SourceDescriptor.Name + "." + name;
                    descriptor = parent.SourceDescriptor;
                    continue;
                }
                return name;
            }
            return name;
        }
        private void GetDescription(PropertyDescriptionAttribute descriptionAttribute_,
                                       out string plainDescription_, out FlowDocument richDescription_)
        { 
            richDescription_ = new FlowDocument();
            richDescription_.PagePadding = new Thickness(1);
            var paragrah = new Paragraph(); 
            paragrah.Inlines.Add(new Run(FullName) { FontWeight = FontWeights.ExtraBold });
            richDescription_.Blocks.Add(paragrah);

            List<string> descriptionLines = new List<string>();
            if (descriptionAttribute_ != null)
            {
                if (!string.IsNullOrEmpty(descriptionAttribute_.Description))
                {
                    string descr = descriptionAttribute_.Description.TrimEnd('.') + ".";
                    descriptionLines.Add(descr);
                    var paragraph2 = new Paragraph();
                    paragraph2.Inlines.Add(new Run("Description: ") { FontWeight = FontWeights.Bold });
                    paragraph2.Inlines.Add(new Run(descr));
                    richDescription_.Blocks.Add(paragraph2);
                }
                if (propertyDescriptor.PropertyType.IsEnum && 
                    descriptionAttribute_.ValueDescriptions != null && descriptionAttribute_.ValueDescriptions.Length > 0)
                {
                    var paragraph3 = new Paragraph();
                    paragraph3.Inlines.Add(new Run("Enumerations: ") { FontWeight = FontWeights.Bold });
                    richDescription_.Blocks.Add(paragraph3);
                    List list = new List(); 
                    list.Margin = new Thickness(0);
                    //list.MarkerStyle = TextMarkerStyle.Circle;
                    string[] values = Enum.GetNames(propertyDescriptor.PropertyType);
                    int length = Math.Min(values.Length, descriptionAttribute_.ValueDescriptions.Length);
                    for (int i = 0; i < length; i++ )
                    {
                        string valueDescription = descriptionAttribute_.ValueDescriptions[i].TrimEnd('.') + ".";
                        descriptionLines.Add(values[i] + ": " + valueDescription);
                        var listItem = new ListItem();
                        var p = new Paragraph();
                        p.Inlines.Add(new Run(values[i] + ": ") { FontWeight = FontWeights.Bold });
                        p.Inlines.Add(new Run(valueDescription));
                        listItem.Blocks.Add(p);
                        list.ListItems.Add(listItem);
                    }
                    richDescription_.Blocks.Add(list);
                }

            }
            if (!typeof(ComplexConfigurationItem).IsAssignableFrom(propertyDescriptor.PropertyType))
            {
                if (defaultValue != null && !string.Empty.Equals(defaultValue))
                {
                    descriptionLines.Add("Default Value: " + defaultValue);

                    var paragraph4 = new Paragraph();
                    paragraph4.Inlines.Add(new Run("Default Value: ") { FontWeight = FontWeights.Bold });
                    paragraph4.Inlines.Add(new Run(defaultValue.ToString()));
                    richDescription_.Blocks.Add(paragraph4);
                }
            }
            else
            {
                string hint = null;
                if (typeof(EnablableComplexConfigurationItem).IsAssignableFrom(propertyDescriptor.PropertyType))
                {
                    hint = "Disabled by default";
                }
                else if (typeof(DisablableComplexConfigurationItem).IsAssignableFrom(propertyDescriptor.PropertyType))
                {
                    hint = "Enabled by default";
                }
                else if (typeof(ExternalComplexConfigurationItem).IsAssignableFrom(propertyDescriptor.PropertyType))
                {
                    hint = "Can be specified locally or in external file";
                }
                if (hint != null)
                {
                    var paragraph5 = new Paragraph();
                    paragraph5.Inlines.Add(new Run(hint));
                    richDescription_.Blocks.Add(paragraph5);
                    descriptionLines.Add(hint);
                } 
            }
            if (!string.IsNullOrEmpty(EditableHintFormat))
            {
                var paragraph5 = new Paragraph();
                paragraph5.Inlines.Add(new Run("Enablement: ") { FontWeight = FontWeights.Bold });
                paragraph5.Inlines.Add(new Run(string.Format("Enabled when " + EditableHintFormat, EditableSource)));
                richDescription_.Blocks.Add(paragraph5); 
            }
            if (descriptionAttribute_ != null)
            {
                if (!string.IsNullOrEmpty(descriptionAttribute_.Example))
                {
                    descriptionLines.Add("Example Value: " + descriptionAttribute_.Example);

                    var paragraph6 = new Paragraph();
                    paragraph6.Inlines.Add(new Run("Example Value: ") { FontWeight = FontWeights.Bold });
                    paragraph6.Inlines.Add(new Run(descriptionAttribute_.Example));
                    richDescription_.Blocks.Add(paragraph6);
                }
                if (!string.IsNullOrEmpty(descriptionAttribute_.CodeWiki))
                {
                    if (descriptionAttribute_.CodeWikiType != CodeWikiType.External)
                    {
                        codeWiki = "http://codewiki.webfarm.ms.com/msdotnet/#!/" + descriptionAttribute_.CodeWikiType +
                                   "/" +
                                   descriptionAttribute_.CodeWiki.TrimEnd('/') + "/";
                    }
                    else
                    {
                        codeWiki = descriptionAttribute_.CodeWiki;
                    }
                    descriptionLines.Add("CodeWiki: " + codeWiki);

                    var paragraph7 = new Paragraph();
                    paragraph7.Inlines.Add(new Run("CodeWiki: ") { FontWeight = FontWeights.Bold });
                    var link = new Hyperlink(new Run(codeWiki)) { NavigateUri = new Uri(codeWiki) };
                    link.RequestNavigate += (sender_, args_) => Process.Start(args_.Uri.ToString());
                    paragraph7.Inlines.Add(link);
                    richDescription_.Blocks.Add(paragraph7);

                }
            }
            plainDescription_ = string.Join(Environment.NewLine, descriptionLines);
        }

        private static Type GetListItemType(Type listType)
        {
            Type type = listType.GetInterfaces().FirstOrDefault((Type i) => i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IList<>));
            if (!(type != null))
            {
                return null;
            }
            return type.GetGenericArguments()[0];
        }

        public ICommand ResetCommand { get; private set; }
        public ICommand RestoreCommand { get; private set; }
        public ICommand EditLocallyCommand { get; private set; } 
        public ICommand EditExternallyCommand { get; private set; }
        public ICommand VisitCodeWikiCommand { get; private set; }
        public PropertyItem PropertyItem { get; set; }
        private bool isDefault = true;
        public bool IsDefault
        {
            get { return isDefault; }
            set
            {
                if (isDefault != value)
                {
                    isDefault = value; 
                    OnPropertyChanged("IsDefault");
                }
            }
        }
          
        public override bool CanResetValue(object component)
        {
            if (this.defaultValue != NoValue)
            {
                object value = propertyDescriptor.GetValue(component);
                if (defaultValue == null)
                { 
                    return defaultValue != value;
                }
                return !ObjectUtilities.ObjectEquals(defaultValue, value); 
            }
            return propertyDescriptor.CanResetValue(component);
        }

        public List<Property> GetChildProperties(string propertyPrefix_)
        {
            if (typeof(IList).IsAssignableFrom(propertyDescriptor.PropertyType))
            {
                var elementType = ObjectUtilities.GetListItemType(propertyDescriptor.PropertyType);
                if (elementType != null && !elementType.IsValueType)
                {
                    propertyPrefix_ += this.FullName + "[].";
                    return new ConfigurationTypeDescriptor(this.vm, Activator.CreateInstance(elementType)).ListAllProperties(propertyPrefix_);
                }
                return new List<Property>();
            }
            if (!typeof(ComplexConfigurationItem).IsAssignableFrom(propertyDescriptor.PropertyType)) return new List<Property>();
            object value = Activator.CreateInstance(propertyDescriptor.PropertyType);
            var descriptor = new ConfigurationTypeDescriptor(null, this, value, true);
            return descriptor.ListAllProperties(propertyPrefix_);
        }

        public List<ISearchResultItem> GetRelatedChildProperties(string searchText_, string propertyPrefix_)
        {
            if (typeof (IList).IsAssignableFrom(propertyDescriptor.PropertyType))
            {
                var elementType = ObjectUtilities.GetListItemType(propertyDescriptor.PropertyType);
                if (elementType != null && !elementType.IsValueType)
                {
                    propertyPrefix_ += this.FullName + "[].";
                    return new ConfigurationTypeDescriptor(this.vm, Activator.CreateInstance(elementType)).GetRelatedProperties(searchText_, propertyPrefix_);
                }
                return new List<ISearchResultItem>();
            }
            if (!typeof(ComplexConfigurationItem).IsAssignableFrom(propertyDescriptor.PropertyType)) return new List<ISearchResultItem>();
           object value = Activator.CreateInstance(propertyDescriptor.PropertyType);
           var descriptor = new ConfigurationTypeDescriptor(null, this, value, true);
           return descriptor.GetRelatedProperties(searchText_, propertyPrefix_);
        }

        private ConfigurationTypeDescriptor currentDescriptor = null;
        public override object GetValue(object component)
        {
            if (currentDescriptor != null)
            {
                currentDescriptor.PropertyChanged -= descriptor_PropertyChanged;
                currentDescriptor = null;
            }
            object value = propertyDescriptor.GetValue(Owner);
            if (oldValue == NoValue)
            {
                oldValue = value; 
                if (value != null)
                {
                    oldValue = ObjectUtilities.CloneObject(value);
                }
            }
            if (defaultValue != NoValue)
            {
                IsDefault =  ObjectUtilities.ObjectEquals(defaultValue, value);  
            }
            bool isDummy = false;
            if (value == null && typeof (INullableConfigurationItem).IsAssignableFrom(propertyDescriptor.PropertyType))
            {
                value = Activator.CreateInstance(propertyDescriptor.PropertyType);
                isDummy = true;
                INullableConfigurationItem value1 = value as INullableConfigurationItem;
                if (value1 != null)
                {
                    value1.InitializeNullObject(Owner);
                } 
            }
            if (value != null && 
                (propertyDescriptor.Attributes[typeof(ExpandableObjectAttribute)] != null || 
                typeof(ComplexConfigurationItem).IsAssignableFrom(propertyDescriptor.PropertyType)))
            {
                currentDescriptor = new ConfigurationTypeDescriptor(vm, this, value, isDummy);
                currentDescriptor.PropertyChanged += descriptor_PropertyChanged;
                return currentDescriptor;
            }

            return value;

        }

        void descriptor_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            valueChanged = true;
        }

        public override void ResetValue(object component)
        {
            if (propertyDescriptor.CanResetValue(component))
            { 
                propertyDescriptor.ResetValue(component);
            }
            else if (defaultValue != NoValue)
            {
                propertyDescriptor.SetValue(component, defaultValue);
            }


            IsDefault = false;
            OnPropertyValueChanged();
            if (oldValue != NoValue)
            {
                if (ObjectUtilities.ObjectEquals(defaultValue, oldValue))
                {
                    valueChanged = false;
                }
            }       
        }

        public override void SetValue(object component, object value)
        {
            ConfigurationTypeDescriptor descriptor = value as ConfigurationTypeDescriptor;
            if (descriptor != null)
            {
                propertyDescriptor.SetValue(Owner, descriptor.SourceObject);
            }
            else
            {
                propertyDescriptor.SetValue(Owner, value);
            }
            if (defaultValue != NoValue)
            {
                IsDefault = ObjectUtilities.ObjectEquals(defaultValue, value);
            }
            OnPropertyValueChanged();
        }

        internal void OnPropertyValueChanged()
        {
            var copy = PropertyValueChanged;
            if (copy != null)
            {
                copy(this, EventArgs.Empty);
            }
            valueChanged = true;
            vm.IsDirty = true;
        }
        public override bool ShouldSerializeValue(object component)
        {
            return propertyDescriptor.ShouldSerializeValue(Owner);
        }

        public override Type ComponentType
        {
            get { return Owner.GetType(); }
        }

        public override bool IsReadOnly
        {
            get { return false; }
        }

        public override Type PropertyType
        {
            get { return propertyDescriptor.PropertyType; }
        }

        public string EditableHintFormat { get; set; }

        public string EditableSource { get; set; }

        public IValueConverter EditableSourceConverter { get; set; }

        public string ValueSource { get; set; }

        public IMultiValueConverter ValueSourceConverter { get; set; }

        

        /// <summary>
        /// Raised when a property on this object has a new value.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises this object's PropertyChanged event.
        /// </summary>
        /// <param name="propertyName_">The property that has a new value.</param>
        protected virtual void OnPropertyChanged(string propertyName_)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                var e = new PropertyChangedEventArgs(propertyName_);
                handler(this, e);
            }
        }

        public override string ToString()
        {
            return FullName;
        }
    }
}
