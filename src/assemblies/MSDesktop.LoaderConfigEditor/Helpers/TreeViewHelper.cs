﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using MorganStanley.Desktop.Loader.Configuration;

namespace MSDesktop.LoaderConfigEditor.Helpers
{
    public class TreeViewHelper
    {
        private static Dictionary<DependencyObject, TreeViewSelectedItemBehavior> behaviors = new Dictionary<DependencyObject, TreeViewSelectedItemBehavior>();

        public static object GetSelectedItem(DependencyObject obj)
        {
            return (object)obj.GetValue(SelectedItemProperty);
        }

        public static void SetSelectedItem(DependencyObject obj, IPlacementElement value)
        {
            obj.SetValue(SelectedItemProperty, value);
        }

        // Using a DependencyProperty as the backing store for SelectedItem.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedItemProperty =
            DependencyProperty.RegisterAttached("SelectedItem", typeof(IPlacementElement), typeof(TreeViewHelper), new UIPropertyMetadata(null, SelectedItemChanged));

        private static void SelectedItemChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            if (!(obj is TreeView))
                return;

            if (!behaviors.ContainsKey(obj))
                behaviors.Add(obj, new TreeViewSelectedItemBehavior(obj as TreeView));

            TreeViewSelectedItemBehavior view = behaviors[obj];
            view.ChangeSelectedItem(e.NewValue as IPlacementElement);
        }

        private class TreeViewSelectedItemBehavior
        {
            TreeView view;
            public TreeViewSelectedItemBehavior(TreeView view)
            {
                this.view = view;
                view.SelectedItemChanged += (sender, e) => SetSelectedItem(view, e.NewValue as IPlacementElement);
            }

            internal void ChangeSelectedItem(IPlacementElement p)
            {
                Stack<IPlacementElement> elements = new Stack<IPlacementElement>(); 
                IPlacementElement current = p;
                while (current != null)
                {
                    elements.Push(current);
                    current = current.Parent;
                }

                if (elements.Count == 0) return;
                elements.Pop();
                if (elements.Count == 0) return;

                current = elements.Pop();
                ItemsControl container = view;
                while (container != null)
                {
                    container = container.ItemContainerGenerator.ContainerFromItem(current) as ItemsControl;
                    var treeItem = (TreeViewItem)container;
                    if (treeItem != null)
                    {
                        treeItem.IsExpanded = true;
                        treeItem.BringIntoView();
                    }
                    if (elements.Count == 0) break;
                    current = elements.Pop();
                }
                TreeViewItem item = (TreeViewItem)container;
                if (item != null)
                {
                    item.IsSelected = true;
                }
            }
        }
    }
}
