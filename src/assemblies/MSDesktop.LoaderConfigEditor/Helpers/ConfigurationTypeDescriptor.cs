using System;
using System.Collections.Generic;
using System.ComponentModel; 
using System.IO;
using System.Threading;
using System.Xml.Serialization;
using MSDesktop.LoaderConfigEditor.ViewModels;
using MorganStanley.Desktop.Loader.Configuration.Helpers;
using MorganStanley.Desktop.Loader.Infrastructure;
using MorganStanley.MSDotNet.MSGui.Controls.SearchBox;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;

namespace MSDesktop.LoaderConfigEditor.Helpers
{
    public class ConfigurationTypeDescriptor : ViewModelBase, ICustomTypeDescriptor
    {
         #region Private fields
        private readonly object sourceObject; 
        private PropertyDescriptorCollection properties;
        private readonly ConfigurationItemPropertyDescriptor sourceDescriptor;
        internal readonly IModifiableConfigViewModel ConfigViewModel;
        private readonly object defaultSourceObject;
        private bool isDummy;
        #endregion

        public ConfigurationTypeDescriptor(IModifiableConfigViewModel configViewModel_, object sourceObject_)
            : this(configViewModel_, null, sourceObject_)
        {
            ConfigViewModel = configViewModel_;
        }
        public ConfigurationTypeDescriptor(IModifiableConfigViewModel configViewModel_, ConfigurationItemPropertyDescriptor sourceDescriptor_, object sourceObject_, bool isDummy_ = false)
        {
            sourceObject = sourceObject_;
            if (sourceObject != null)
            {
                defaultSourceObject = Activator.CreateInstance(sourceObject.GetType());
            }
            sourceDescriptor = sourceDescriptor_;
            ConfigViewModel = configViewModel_;
            isDummy = isDummy_;
            InternalEnablableComplexConfigurationItem config = sourceObject as InternalEnablableComplexConfigurationItem;
            if (config != null)
            {
                config.EnablementChanged += (sender_, args_) => OnPropertyChanged(config.EnablementPropertyName);
            } 
        }

        public object SourceObject
        {
            get { return sourceObject; }
        }

        public ConfigurationItemPropertyDescriptor SourceDescriptor
        {
            get { return sourceDescriptor; }
        }

        public PropertyDescriptorCollection Properties
        {
            get { return properties; }
            set
            {
                if (properties != value)
                {
                    properties = value;
                    OnPropertyChanged("Properties");
                }
            }
        }

        #region Implementation of ICustomtypeDescriptor

        public AttributeCollection GetAttributes()
        {
            return TypeDescriptor.GetAttributes(sourceObject);
        }

        public string GetClassName()
        {
            return TypeDescriptor.GetClassName(sourceObject);
        }

        public string GetComponentName()
        {
            return TypeDescriptor.GetComponentName(sourceObject);
        }

        public TypeConverter GetConverter()
        {
            return null;
        }

        public EventDescriptor GetDefaultEvent()
        {
            return null;
        }

        public PropertyDescriptor GetDefaultProperty()
        {
            return null;
        }

        public object GetEditor(Type editorBaseType_)
        {
            return null;
        }

        public EventDescriptorCollection GetEvents()
        {
            return GetEvents(null);
        }

        public EventDescriptorCollection GetEvents(Attribute[] attributes_)
        {
            return TypeDescriptor.GetEvents(this, attributes_);
        }

        public PropertyDescriptorCollection GetProperties()
        {
            return GetProperties(null);
        }
        

        public PropertyDescriptorCollection GetProperties(Attribute[] attributes_)
        {
            if (Properties == null)
            {
                var propertyDescriptors = new List<PropertyDescriptor>(); 
                foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(sourceObject, attributes_))
                {
                    var descriptor = new ConfigurationItemPropertyDescriptor(ConfigViewModel,this, defaultSourceObject, property);
                    descriptor.PropertyValueChanged += new EventHandler(descriptor_PropertyValueChanged);
                    propertyDescriptors.Add(descriptor);
                }
                Properties = new PropertyDescriptorCollection(propertyDescriptors.ToArray());

            }
            return Properties;
        }
 
        void descriptor_PropertyValueChanged(object sender, EventArgs e)
        {
            var descriptor = (ConfigurationItemPropertyDescriptor)sender;
            if ((this.SourceObject is PointHelper || this.SourceObject is SizeHelper|| this.SourceObject is KeyGestureHelper)
                && sourceDescriptor != null)
            {
                sourceDescriptor.OnPropertyValueChanged();
                
            } 
            if (isDummy && sourceDescriptor != null)
            {
                sourceDescriptor.SetValue(sourceDescriptor.Owner, this.SourceObject);
                isDummy = false;
            }
            OnPropertyChanged(descriptor.Name);
        }

        public object GetPropertyOwner(PropertyDescriptor pd_)
        {
            var customPropertyDescriptor = pd_ as ConfigurationItemPropertyDescriptor;
            if (customPropertyDescriptor != null)
            {
                return customPropertyDescriptor.Owner;
            }
            return null;
        }

        public override string ToString()
        {
            return sourceObject.ToString();
        }

        #endregion

        public const string PropertyDefinitionStore = "Properties.xml";
        public List<Property> ListAllProperties(string propertyPrefix_)
        {
            List<Property> result = new List<Property>();
            foreach (ConfigurationItemPropertyDescriptor property in this.GetProperties())
            {
                if (!property.IsBrowsable) continue;
                Property p = new Property()
                    {
                        Name = propertyPrefix_ + property.FullName,
                        Value = property.Description
                    };
                result.Add(p); 
                result.AddRange(property.GetChildProperties(propertyPrefix_));
            }


            result.Sort((a_, b_) => String.Compare(a_.Name, b_.Name, StringComparison.Ordinal));
            return result;
        }

        private static List<Property> allProperties;
        private static readonly object syncRoot = new object();
        private void EnsureProperties()
        {
            lock (syncRoot)
            {
                if (allProperties != null)
                {
                    return;
                }

                if (File.Exists(PropertyDefinitionStore))
                {
                    var serializer = new XmlSerializer(typeof(List<Property>));
                    try
                    {
                        using (var fs = new FileStream(PropertyDefinitionStore, FileMode.Open))
                        {
                            allProperties = serializer.Deserialize(fs) as List<Property>;
                        }
                    }
                    catch
                    {
                    }
                }
                if (allProperties == null)
                {
                    allProperties = ListAllProperties(string.Empty);
                }
            }
           
        }

        public List<ISearchResultItem> PreHandlingGetRelatedProperties(string searchText_, CancellationToken ct_)
        {
            List<ISearchResultItem> items = new List<ISearchResultItem>();
            List<ISearchResultItem> items2 = new List<ISearchResultItem>();
            if (ct_.IsCancellationRequested)
            { 
                return items;
            }
            EnsureProperties();
            if (ct_.IsCancellationRequested)
            { 
                return items;
            }
            searchText_ = searchText_.ToLower();
            foreach (var property in allProperties)
            { 
                if (ct_.IsCancellationRequested)
                { 
                    return items;
                }
                if (property.Name.ToLower().Contains(searchText_)) 
                {
                    items.Add(new SearchResultItem(property, MatchType.Name, searchText_));
                }
                else
                {
                    string description = property.Value;
                     if (!string.IsNullOrEmpty(description) && description.ToLower().Contains(searchText_))
                     {
                         items2.Add(new SearchResultItem(property, MatchType.Description, searchText_));
                     }
                }
            }
            if (ct_.IsCancellationRequested)
            { 
                return items;
            }
            items.AddRange(items2);
            
            return items;
        }

        public List<ISearchResultItem> GetRelatedProperties(string searchText_, string propertyPrefix_)
        {
            searchText_ = searchText_.ToLower();
            List<ISearchResultItem> items = new List<ISearchResultItem>();
            foreach (ConfigurationItemPropertyDescriptor property in this.GetProperties())
            {
                if (!property.IsBrowsable) continue;
                if (property.Name.ToLower().Contains(searchText_)) 
                {
                    items.Add(new SearchResultItem(propertyPrefix_, property, MatchType.Name, searchText_));
                }
                else
                {
                    string description = property.Description;
                     if (!string.IsNullOrEmpty(description) && description.ToLower().Contains(searchText_))
                     {
                         items.Add(new SearchResultItem(propertyPrefix_, property, MatchType.Description, searchText_));
                     }
                }
                items.AddRange(property.GetRelatedChildProperties(searchText_, propertyPrefix_));
            }
 
            return items;
        }
   
    }
}
