﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls; 
using MorganStanley.Desktop.Loader.Configuration;

namespace MSDesktop.LoaderConfigEditor.Helpers
{
    public class PlacementElementDataTemplateSelector:DataTemplateSelector
    {
        public DataTemplate ReadOnlyTemplate { get; set; }
        public DataTemplate EditableTemplate { get; set; }
        public DataTemplate WidgetTemplate { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            return item is Widget?
                   WidgetTemplate:
                    (item is IEditablePlacementElement
                       ? EditableTemplate
                       : ReadOnlyTemplate);
        }

    }
}
