﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using MSDesktop.LoaderConfigEditor.Properties;

namespace MSDesktop.LoaderConfigEditor.Helpers
{
    static class SnapShotHelper
    {
        static readonly Dictionary<string, List<Rect>> snapShotParts = new Dictionary<string, List<Rect>>();
        static readonly List<string> resources; 
        static SnapShotHelper()
        {

            string[] lines = Resources.SnapShotParts.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var line in lines)
            {
                string[] parts = line.Split(':');
                if (parts.Length != 2) continue;
                string[] rectStrings = parts[1].Split(';');
                List<Rect> rects = new List<Rect>();
                foreach (var rectString in rectStrings)
                {
                    Rect rect = Rect.Parse(rectString);
                    if (rect.IsEmpty) continue;
                    rects.Add(rect);
                }
                if (rects.Count > 0)
                {
                    snapShotParts[parts[0]] = rects;
                }

            }
            resources = new List<string>(GetResourceNames());
        }
        private static IEnumerable<string> GetResourceNames()
        {
            var assembly = Assembly.GetExecutingAssembly();
            string resName = assembly.GetName().Name + ".g.resources";
            using (var stream = assembly.GetManifestResourceStream(resName))
            {
                using (var reader = new System.Resources.ResourceReader(stream))
                {
                    return reader.Cast<DictionaryEntry>().Select(entry =>
                             (string)entry.Key).ToArray();
                }
            }
        }

        public static ImageSource GetSnapShot(string[] snapShotNames_, string snapShotPart_, Color flashColor_)
        {
            DrawingVisual visual = new DrawingVisual();
            double left = 0;
            int pixelWidth = 0;
            int pixelHeight = 0;
            double dpiX = 0, dpiY = 0;
            bool foundImage = false;
            using (DrawingContext context = visual.RenderOpen())
            {
                foreach (var snapShot in snapShotNames_)
                {  
                    BitmapSource image = GetImage(snapShot);
                    if (image == null) continue;
                    foundImage = true;
                    context.DrawImage(image, new Rect(left, 0, image.Width, image.Height));
                    pixelHeight = Math.Max(pixelHeight, image.PixelHeight);
                    pixelWidth += image.PixelWidth + (int)(10 * image.DpiX) / 96;
                    dpiX = image.DpiX;
                    dpiY = image.DpiY;
                    List<Rect> rects;
                    if (snapShotParts.TryGetValue(snapShot + "." + snapShotPart_, out rects))
                    {
                        foreach (var rect in rects)
                        {
                            rect.Offset(left, 0);
                            context.DrawRectangle(null, new Pen(new SolidColorBrush(flashColor_), 2.5), rect); 
                        } 
                    }
                    left += image.Width + 10;
                }  
            }
            if (!foundImage) return null;
            RenderTargetBitmap compositeImage = new RenderTargetBitmap(pixelWidth, pixelHeight, dpiX, dpiY, PixelFormats.Default);
            compositeImage.Render(visual);
            return compositeImage;
             
        }

        public static ImageSource GetComparisonSnapShot(Dictionary<string, string> snapShots_, string snapShotPart_, Color flashColor_)
        {
            if (snapShots_.Count == 0) return null;
            DrawingVisual visual = new DrawingVisual();
            double left = 0; 
            int pixelWidth = 0;
            int pixelHeight = 0;
            double dpiX = 0, dpiY = 0;
            bool foundImage = false; 
            using (DrawingContext context = visual.RenderOpen())
            { 
                foreach (var snapShot in snapShots_)
                {
                    BitmapSource image = GetImage(snapShot.Key);
                    if (image == null) continue;
                    foundImage = true;
                    context.DrawImage(image, new Rect(left, 0, image.Width, image.Height));
                    if (!string.IsNullOrEmpty(snapShotPart_))
                    { 
                        List<Rect> rects;
                        if (snapShotParts.TryGetValue(snapShot.Key + "." + snapShotPart_, out rects))
                        {
                            foreach (var rect in rects)
                            {
                                rect.Offset(left, 0);
                                context.DrawRectangle(null, new Pen(new SolidColorBrush(flashColor_), 2.5), rect);
                            }
                        }
                    }
                    pixelHeight = Math.Max(pixelHeight, image.PixelHeight);
                    pixelWidth += image.PixelWidth + (int)(10 * image.DpiX) / 96;
                    dpiX = image.DpiX;
                    dpiY = image.DpiY;
                    FormattedText text = new FormattedText(snapShot.Value,
                                       System.Globalization.CultureInfo.CurrentCulture,
                                       FlowDirection.LeftToRight,
                                       new Typeface(SystemFonts.SmallCaptionFontFamily, FontStyles.Normal, FontWeights.Bold, FontStretches.Normal),
                                       40 * (image.Width / 500.0) * 14.0 / snapShot.Value.Length , Brushes.Red); 
                    text.MaxTextWidth = image.Width - 10;
                    double x2 = (image.Width - text.Width) / 2 + left;
                    double y2 = (image.Height - text.Height)/2;
                    context.DrawText(text, new Point(x2, y2)); 
                    left += image.Width + 10; 
                } 

            }
            if (!foundImage) return null;
            RenderTargetBitmap compositeImage = new RenderTargetBitmap(pixelWidth, pixelHeight, dpiX, dpiY, PixelFormats.Default);
            compositeImage.Render(visual);
            return compositeImage;
        }

        private static BitmapSource GetImage(string snapShotName_)
        {
            string imageFile = "snapshots/" + snapShotName_.ToLower() + ".png";
            if (!resources.Contains(imageFile)) return null; 
            return new BitmapImage(new Uri("pack://application:,,,/" + imageFile, UriKind.RelativeOrAbsolute));

        }
    }
}
