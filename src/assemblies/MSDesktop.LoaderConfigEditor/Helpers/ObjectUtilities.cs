﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace MSDesktop.LoaderConfigEditor.Helpers
{
    public static class ObjectUtilities
    {
 
        public static bool ListEquals(IList list1_, IList list2_)
        {
            if (list1_ == null)
            {
                return list2_ == null;
            }
            if (list2_ == null)
            {
                return false;
            }
            if (list1_.Count != list2_.Count) return false;
            for (int i = 0; i < list1_.Count; i++)
            {
                if (!ObjectEquals(list1_[i], list2_[i]))
                {
                    return false;
                }
            }
            return true;
        }

        public static bool ObjectEquals(object item1_, object item2_)
        {
            if (item1_ == null)
            {
                return item2_ == null;
            }
            if (item1_.Equals(item2_))
            {
                return true;
            }
            if (item1_.GetType() != item2_.GetType()) return false;
            if (item1_ is ValueType || item1_ is string)
            {
                return false;
            }
            IList list1 = item1_ as IList; 
            if (list1 != null)
            {
                return ListEquals(list1, item2_ as IList);
            }
            foreach (var propertyInfo in item1_.GetType().GetProperties())
            {
                if (!propertyInfo.CanRead)
                {
                    continue;
                }
                object value1 = propertyInfo.GetValue(item1_, null);
                object value2 = propertyInfo.GetValue(item2_, null);
                if (!ObjectEquals(value1, value2))
                {
                    return false;
                }
            }
            return true;
        }

        public static object CloneObject(object originalObj_)
        {
            if (originalObj_ == null) return null;
            if (originalObj_ is ValueType) return originalObj_;
            ICloneable cloneable = originalObj_ as ICloneable; 
            if (cloneable != null) return cloneable.Clone();

            IList list = originalObj_ as IList;
            if (list != null)
            {
                IList newList = Activator.CreateInstance(originalObj_.GetType()) as IList;
                foreach (var item in list)
                {
                    object newItem = CloneObject(item);
                    newList.Add(newItem);
                }
                return newList;
            }

            object copy = Activator.CreateInstance(originalObj_.GetType());
            foreach (var propertyInfo in originalObj_.GetType().GetProperties())
            {
                if (!propertyInfo.CanRead || !propertyInfo.CanWrite)
                {
                    continue;
                }
                object value = propertyInfo.GetValue(originalObj_, null);
                if (value == null) continue;
                propertyInfo.SetValue(copy, CloneObject(value), null); 
            }
            return copy;
        }

        public static Type GetListItemType(Type listType)
        {
            Type type = listType.GetInterfaces().FirstOrDefault<Type>(i => i.IsGenericType && (i.GetGenericTypeDefinition() == typeof(IList<>)));
            if (type == null)
            {
                return null;
            }
            return type.GetGenericArguments()[0];
        }


    }
}
