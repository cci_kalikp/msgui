﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using MSDesktop.LoaderConfigEditor.ViewModels;

namespace MSDesktop.LoaderConfigEditor.Editors
{
    /// <summary>
    /// Interaction logic for ControlPlacementEditor.xaml
    /// </summary>
    public partial class ControlPlacementEditor : Window
    {
        public ControlPlacementEditor()
        {
            InitializeComponent(); 
        }

        private void BtnNewArea_OnClick(object sender_, RoutedEventArgs e_)
        {
            ControlPlacementViewModel model = (ControlPlacementViewModel)this.DataContext;
            if (model.AllowNewArea && !model.AllowMultiTypeNewArea)
            {
                model.NewAreaCommand.Execute(null);
            }
        }

        private void BtnNewItem_OnClick(object sender_, RoutedEventArgs e_)
        {
            ControlPlacementViewModel model = (ControlPlacementViewModel)this.DataContext;
            if (model.AllowNewItem && !model.AllowMultiTypeNewItem)
            {
                model.NewItemCommand.Execute(null);
            }
        }

        private void FrameworkElement_OnTargetUpdated(object sender, DataTransferEventArgs e)
        {
            ControlPlacementViewModel model = (ControlPlacementViewModel)this.DataContext;
            model.IsDirty = true;
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            ControlPlacementViewModel model = this.DataContext as ControlPlacementViewModel;
            if (model != null)
            {
                if (!model.Close())
                {
                    e.Cancel = true;
                }
                else
                {
                    this.DialogResult = !model.IsDirty;
                }
            }
            base.OnClosing(e);
        }
    }
}
