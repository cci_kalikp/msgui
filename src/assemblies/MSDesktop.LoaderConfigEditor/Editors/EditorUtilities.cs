﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Threading;
using MSDesktop.LoaderConfigEditor.Helpers;
using MSDesktop.LoaderConfigEditor.ViewModels;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.Desktop.Loader.Infrastructure;
using MorganStanley.MSDotNet.MSGui.Core;
using Xceed.Wpf.Toolkit.PropertyGrid;
using PropertyItem = Xceed.Wpf.Toolkit.PropertyGrid.PropertyItem;

namespace MSDesktop.LoaderConfigEditor.Editors
{
    public static class EditorUtilities
    {
        private const string AllFileFilter = "|All Files (*.*)|*.*";
        public static string GetFilter(PathType pathType_)
        { 
            switch (pathType_)
            {
                case PathType.XmlFile:
                    return "Xml Files (*.xml)|*.xml" + AllFileFilter;
                case PathType.Image:
                    ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();
                    string filter = string.Empty;
                    string extensions = string.Empty;
                    foreach (var c in codecs)
                    {
                        extensions += ";" + c.FilenameExtension.ToLowerInvariant();
                        string codecName = c.CodecName.Substring(8).Replace("Codec", "Files").Trim();
                        filter = String.Format("{0}|{1} ({2})|{2}", filter, codecName, c.FilenameExtension.ToLowerInvariant());
                    }

                    filter = string.Format("Image Files ({0})|{0}", extensions.TrimStart(';')) + filter;
                    return filter + "|Icon Files(*.ico)|*.ico" + AllFileFilter;
                case PathType.ConfigFile:
                    return "Config files (*.config)|*.config" + AllFileFilter;
                case PathType.MsdeConfigFile:
                    return "MSDE Config files (*.msde.config)|*.msde.config" + AllFileFilter;
                case PathType.AssemblyFile:
                    return "Assembly files (*.dll,*.exe)|*.dll;*.exe" + AllFileFilter;
                case PathType.Executable:
                    return "Programs (*.exe)|*.exe" + AllFileFilter;
                case PathType.HtmlFile:
                    return "Web Page|*.htm;*.shtml;*.html;*.asp;*.aspx;*.jsp;*.php;*.py" + AllFileFilter;
                case PathType.FlexFile:
                    return "Flash file(*.swf)|*.swf" + AllFileFilter;
                case PathType.JavaBatch:
                    return "Batch File(*.cmd;*.bat)|*.cmd;*.bat" + AllFileFilter;
                default:
                    return null;
            }
        }


        public static void FixPropertyGrid(PropertyGrid propertyGrid_)
        { 
            propertyGrid_.ShowTitle = false; 
            propertyGrid_.AutoGenerateProperties = true;
            AddAdvancedOptionsMenu(propertyGrid_);
            propertyGrid_.PreparePropertyItem += (sender_, args_) =>
                {
                    var propertyItem = args_.PropertyItem as PropertyItem;
                    if (propertyItem == null) return;
                    FixPropertyItem(propertyItem);
                }; 

            propertyGrid_.DataContextChanged += new DependencyPropertyChangedEventHandler(propertyGrid__DataContextChanged);

        }
         
        static void propertyGrid__DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            ModifiableConfigViewModelBase context = e.NewValue as ModifiableConfigViewModelBase;
            if (context != null)
            {
                context.SearchResultItemSelected += (sender_, args_) => HighlightProperty(sender as PropertyGrid, args_.Item.PropertyPaths);
            }

        }

        private static void HighlightProperty(PropertyGrid propertyGrid_, string[] propertyNames_)
        {
            string[] subProperties = propertyNames_[0].Split('.');
            int level = 0;
            var item = HighlightProperty(propertyGrid_, null, propertyGrid_.Properties, subProperties, ref level);
            if (item == null) return;
            var propertyDescriptor = item.PropertyDescriptor as ConfigurationItemPropertyDescriptor;
            if (propertyDescriptor == null)
            {
                return;
            }

            IPopupEditor editor = item.Editor as IPopupEditor;
            if (editor == null) return;

            List<string> propertyNamesLeft = new List<string>();
            if (propertyDescriptor.FullName.Length < propertyNames_[0].Length)
            {
                string propertyLeft = propertyNames_[0].Substring(propertyDescriptor.FullName.Length + 1);
                propertyNamesLeft.Add(propertyLeft);
            }
            if (propertyNames_.Length > 1)
            {
                propertyNamesLeft.AddRange(propertyNames_.Skip(1));
            }
            if (propertyNamesLeft.Count == 0) return;
            editor.ShowCollectionDialog(g_ => HighlightProperty(g_, propertyNamesLeft.ToArray()));
        }
         
        private static PropertyItem HighlightProperty(PropertyGrid propertyGrid_, PropertyItem parentProperty_, IList properties_, string[] propertyNames_, ref int level_)
        { 
            foreach (PropertyItem propertyItem in properties_)
            {
                if (propertyItem != null && propertyItem.PropertyDescriptor.Name == propertyNames_[level_])
                {
                    propertyItem.ApplyTemplate();
                    if (propertyItem.IsReadOnly)
                    {
                        var descriptor = propertyItem.PropertyDescriptor as ConfigurationItemPropertyDescriptor;
                        if (descriptor != null)
                        {
                            string hint = descriptor.EditableHintFormat;
                            if (!string.IsNullOrEmpty(hint))
                            {
                                TaskDialog.ShowMessage("Please " + string.Format(hint, descriptor.FullName) + " to view the child property", 
                                    "Failed to locate " + string.Join(".", propertyNames_));
                            }
                        }
                        return null;
                    }
     
                    if (level_ == propertyNames_.Length - 1)
                    {
 
                        propertyItem.BringIntoView();
                        //use lower dispatcher priority to allow the PreparePropertyItem to happen before it, 
                        //so that set "IsSelected" of the property would work correctly by setting PropertyGrid.SelectedPropertyItem
                        Dispatcher.CurrentDispatcher.BeginInvoke(
                            new Action(() =>
                                {
                                    propertyItem.IsSelected = true;
                                }),
                               DispatcherPriority.Background);
                        if (propertyItem.Editor == null) //disabled
                        {
                            return parentProperty_;
                        }
                        return propertyItem;
                    }
                    if (propertyItem.IsExpandable)
                    {
                        propertyItem.IsExpanded = true;
                    } 


                    level_++;  
                    propertyItem.BringIntoView(); 
                    propertyItem.InvalidateMeasure();
                    propertyItem.InvalidateVisual();
 
                    return HighlightProperty(propertyGrid_, propertyItem, propertyItem.Properties, propertyNames_, ref level_);

                } 
            }
            return null;
        }
 
 
        private static void AddAdvancedOptionsMenu(PropertyGrid propertyGrid_)
        {
            propertyGrid_.ShowAdvancedOptions = true;
            ContextMenu menu = new ContextMenu();
            MenuItem item1 = new MenuItem() {Header = "Reset Value"};
            item1.Name = "ResetValue";
            menu.Items.Add(item1);
            MenuItem item2 = new MenuItem() { Header = "Restore Value" };
            item2.Name = "RestoreValue";
            menu.Items.Add(item2);
            MenuItem item3 = new MenuItem();
            item3.Name = "EditLocally";
            item3.Visibility = Visibility.Collapsed;
            menu.Items.Add(item3);
            MenuItem item4 = new MenuItem();
            item4.Name = "EditExternally";
            item4.Visibility = Visibility.Collapsed;
            menu.Items.Add(item4);
            MenuItem item5 = new MenuItem() { Header = "See CodeWiki" };
            item5.Name = "SeeCodeWiki";
            menu.Items.Add(item5);
            menu.Opened += (sender_, args_) =>
                {
                    FrameworkElement parent = (sender_ as ContextMenu).PlacementTarget as FrameworkElement;
                    PropertyItem propertyItem = parent.FindVisualParent<PropertyItem>();
                    if (propertyItem == null) return;
                    BindPropertyItemCommands(propertyItem, menu);
                };
            propertyGrid_.SelectedPropertyItemChanged += (sender_, args_) =>
            {
                var propertyItem = args_.NewValue as PropertyItem;
                if (propertyItem == null) return;
                BindPropertyItemCommands(propertyItem, menu);
            };
            propertyGrid_.AdvancedOptionsMenu = menu;
        }
 
        private static void BindPropertyItemCommands(PropertyItem propertyItem_, ContextMenu menu_)
        {
            var customProperty = propertyItem_.PropertyDescriptor as ConfigurationItemPropertyDescriptor;
            if (customProperty != null)
            {
                foreach (MenuItem item in menu_.Items)
                {
                    if (item.Name == "ResetValue")
                    {
                        item.Header = "Reset Value of " + propertyItem_.PropertyDescriptor.Name;
                        item.Command = customProperty.ResetCommand; 
                    }
                    else if (item.Name == "RestoreValue")
                    {
                        item.Header = "Restore Value of " + propertyItem_.PropertyDescriptor.Name;
                        item.Command = customProperty.RestoreCommand; 
                    }
                    else if (item.Name == "EditLocally")
                    {
                        if (typeof(ExternalComplexConfigurationItem).IsAssignableFrom(customProperty.PropertyType))
                        {
                            item.Visibility = Visibility.Visible;
                            item.Header = "Edit " + propertyItem_.PropertyDescriptor.Name + " Locally";
                            item.Command = customProperty.EditLocallyCommand; 
                        }
                        else
                        {
                            item.Visibility = Visibility.Collapsed;
                        }
                    }
                    else if (item.Name == "EditExternally")
                    {
                        if (typeof(ExternalComplexConfigurationItem).IsAssignableFrom(customProperty.PropertyType))
                        {
                            item.Visibility = Visibility.Visible;
                            item.Header = "Edit " + propertyItem_.PropertyDescriptor.Name + " Externally";
                            item.Command = customProperty.EditExternallyCommand;
                        }
                        else
                        {
                            item.Visibility = Visibility.Collapsed;
                        }
                    }
                    else if (item.Name == "SeeCodeWiki")
                    {
                        item.Header = "See CodeWiki for " + propertyItem_.PropertyDescriptor.Name;
                        item.Command = customProperty.VisitCodeWikiCommand; 
                    }
                } 
            }
        }

        public static readonly DependencyProperty PropertyValueProperty =
            DependencyProperty.RegisterAttached("PropertyValue", typeof (object), typeof (EditorUtilities), new PropertyMetadata(default(object), PropertyValuePropertyChangedCallback));

        private static void PropertyValuePropertyChangedCallback(DependencyObject dependencyObject_, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs_)
        {
            if (ObjectUtilities.ObjectEquals(dependencyPropertyChangedEventArgs_.OldValue, dependencyPropertyChangedEventArgs_.NewValue)) return;
            PropertyItem item = dependencyObject_ as PropertyItem;
            if (item != null)
            {
                ConfigurationItemPropertyDescriptor descriptor =
                    item.PropertyDescriptor as ConfigurationItemPropertyDescriptor;
                if (descriptor != null)
                {
                    descriptor.SetValue(descriptor.Owner, dependencyPropertyChangedEventArgs_.NewValue);
                }
            }
        }

        public static void SetPropertyValue(UIElement customPropertyItem_, object value)
        {
            customPropertyItem_.SetValue(PropertyValueProperty, value);
        }

        public static object GetPropertyValue(UIElement customPropertyItem_)
        {
            return (object) customPropertyItem_.GetValue(PropertyValueProperty);
        }
         
        private static void FixPropertyItem(PropertyItem propertyItem_)
        { 
            ConfigurationItemPropertyDescriptor customProperty = propertyItem_.PropertyDescriptor as ConfigurationItemPropertyDescriptor;
            if (customProperty == null) return;
            customProperty.PropertyItem = propertyItem_;

            if (!string.IsNullOrEmpty(customProperty.EditableSource))
            {
                var binding = new Binding(customProperty.EditableSource);
                binding.Source = customProperty.Owner;
                binding.Mode = BindingMode.OneWay;
                binding.Converter = customProperty.EditableSourceConverter;
                propertyItem_.SetBinding(UIElement.IsEnabledProperty, binding); 
            }
            if (!string.IsNullOrEmpty(customProperty.ValueSource) && 
                customProperty.ValueSourceConverter != null)
            {
                var binding = new MultiBinding(); 
                binding.Bindings.Add(new Binding(customProperty.ValueSource){Source = customProperty.Owner});
                binding.Bindings.Add(new Binding(customProperty.Name) { Source = customProperty.Owner }); 
                binding.Mode = BindingMode.OneWay;
                binding.Converter = customProperty.ValueSourceConverter;
                propertyItem_.SetBinding(EditorUtilities.PropertyValueProperty, binding);  
            }
            if (propertyItem_.Editor != null)
            {
                FixPropertyItemEditor(propertyItem_); 
            }
            else
            {
                PropertyChangedEventHandler changedHandler = null;
                propertyItem_.PropertyChanged += changedHandler = (sender_, args_) =>
                    {
                        if (args_.PropertyName == "Editor" && propertyItem_.Editor != null)
                        {
                            FixPropertyItemEditor(propertyItem_);
                            propertyItem_.PropertyChanged -= changedHandler;
                        }
                    };
            }

        }

 

        private static void FixPropertyItemEditor(PropertyItem propertyItem_)
        {
            if (typeof(EnablableComplexConfigurationItem).IsAssignableFrom(propertyItem_.PropertyType) ||
                typeof(DisablableComplexConfigurationItem).IsAssignableFrom(propertyItem_.PropertyType)) return;
            if (typeof(ComplexConfigurationItem).IsAssignableFrom(propertyItem_.PropertyType) && 
                !typeof(ExternalComplexConfigurationItem).IsAssignableFrom(propertyItem_.PropertyType)) return;
            var binding2 = new Binding("IsDefault");
            binding2.Source = propertyItem_.PropertyDescriptor;
            binding2.Mode = BindingMode.OneWay;
            binding2.Converter = new Converters.PropertyValueToFontWeightConverter();
            //propertyItem_.Editor.SetValue(TextElement.FontWeightProperty, FontWeights.Bold);
            propertyItem_.Editor.SetBinding(TextElement.FontWeightProperty, binding2);

        }
    }
}
