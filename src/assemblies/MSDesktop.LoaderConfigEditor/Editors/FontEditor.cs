﻿using System;
using System.Collections;  
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Media;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace MSDesktop.LoaderConfigEditor.Editors
{
    public class FontEditor : ComboBoxEditor
    {

        protected override IEnumerable CreateItemsSource(Xceed.Wpf.Toolkit.PropertyGrid.PropertyItem propertyItem)
        {
            return Fonts.SystemFontFamilies.Select(current_ => current_.ToString());
        } 
    }

}
