﻿using System;
using System.Collections;
using System.Collections.Generic; 
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using MSDesktop.LoaderConfigEditor.Helpers;
using Xceed.Wpf.Toolkit.PropertyGrid;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace MSDesktop.LoaderConfigEditor.Editors
{
    /// <summary>
    /// Interaction logic for ConfigurationItemCollectionEditor.xaml
    /// </summary>
    public partial class ConfigurationItemCollectionEditor : UserControl, ITypeEditor, IPopupEditor
    {
        private PropertyItem item; 
        public ConfigurationItemCollectionEditor()
        {
            this.InitializeComponent();
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ShowCollectionDialog(null);
        }
          
        public void ShowCollectionDialog(Action<PropertyGrid> handler_)
        {
            var collectionControlDialog = new ConfigurationItemCollectionControlDialog(this.item.PropertyType, item.Instance, item.Value as IList);
            Binding binding = new Binding("Value");
            binding.Source = this.item;
            binding.Mode = (this.item.IsReadOnly ? BindingMode.OneWay : BindingMode.TwoWay);
            BindingOperations.SetBinding(collectionControlDialog, ConfigurationItemCollectionControlDialog.ItemsSourceProperty, binding);

            collectionControlDialog.Resources.MergedDictionaries.Add(new ResourceDictionary
            {
                Source = new Uri("pack://application:,,,/MSDesktop.LoaderConfigEditor;component/Resources/Styles.xaml",
                                 UriKind.RelativeOrAbsolute)
            });
            collectionControlDialog.Owner = Application.Current.MainWindow;
            collectionControlDialog.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            collectionControlDialog.Width = 700;
            collectionControlDialog.Height = 500;
            if (handler_ != null)
            {
                RoutedEventHandler loadedHandler = null;
                collectionControlDialog.Loaded += loadedHandler = (sender_, args_) =>
                {
                    if (collectionControlDialog.PropertyGrid.Items.Count == 0)
                    {
                        ApplicationCommands.New.Execute(ObjectUtilities.GetListItemType(collectionControlDialog.PropertyGrid.ItemsSourceType), collectionControlDialog.PropertyGrid); 
                    }
                    handler_(collectionControlDialog.PropertyGrid.PropertyGrid);
                    collectionControlDialog.Loaded -= loadedHandler;
                };
            }
            collectionControlDialog.ShowDialog();

        }

        public FrameworkElement ResolveEditor(PropertyItem propertyItem_)
        {
            this.item = propertyItem_;
            return this;
        }
    }
}
