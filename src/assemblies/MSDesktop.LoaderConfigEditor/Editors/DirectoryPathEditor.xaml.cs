﻿using System.IO;
using System.Windows;
using System.Windows.Data;
using System.Windows.Forms;
using MSDesktop.LoaderConfigEditor.Helpers;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;
using Binding = System.Windows.Data.Binding;
using PropertyItem = Xceed.Wpf.Toolkit.PropertyGrid.PropertyItem;
using UserControl = System.Windows.Controls.UserControl;

namespace MSDesktop.LoaderConfigEditor.Editors
{
    /// <summary>
    /// Interaction logic for DirectoryPathEditor.xaml
    /// </summary>
    public partial class DirectoryPathEditor : UserControl, ITypeEditor
    {

        protected PropertyItem propertyItem;
        public DirectoryPathEditor()
        {
            InitializeComponent();
        }

        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register("Value", typeof(string), typeof(DirectoryPathEditor), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public string Value
        {
            get { return (string) GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }  

        protected string CurrentDirectory
        {
            get
            {
                var descr = propertyItem.Instance as ConfigurationTypeDescriptor;
                if (descr != null)
                {
                    if (descr.ConfigViewModel != null && !string.IsNullOrEmpty(descr.ConfigViewModel.FilePath))
                    {
                        return Path.GetDirectoryName(descr.ConfigViewModel.FilePath);
                    }
                }
                

                return null;
            }
        }
          

        private void ButtonBase_OnClick(object sender_, RoutedEventArgs e_)
        {
            var openFileDialog = new FolderBrowserDialog();
            if (!string.IsNullOrEmpty(Value) && Directory.Exists(Value))
            {
                openFileDialog.SelectedPath = Value; 
            }
            else
            {
                string currentDir = CurrentDirectory;
                if (!string.IsNullOrEmpty(currentDir))
                {
                    openFileDialog.SelectedPath = currentDir;
                }
            } 
             
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                Value = openFileDialog.SelectedPath;
            }
        }
         
        protected virtual string BindingPath
        {
            get { return "Value"; }
        }

        public FrameworkElement ResolveEditor(PropertyItem propertyItem_)
        {
            propertyItem = propertyItem_; 
            Binding binding = new Binding(BindingPath);
            binding.Source = propertyItem_;
            binding.Mode = propertyItem_.IsReadOnly ? BindingMode.OneWay : BindingMode.TwoWay;
            BindingOperations.SetBinding(this, ValueProperty, binding);
            return this;
        }
    }
}
