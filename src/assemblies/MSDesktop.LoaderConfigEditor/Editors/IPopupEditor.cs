﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xceed.Wpf.Toolkit.PropertyGrid;

namespace MSDesktop.LoaderConfigEditor.Editors
{
    interface IPopupEditor
    {
        void ShowCollectionDialog(Action<PropertyGrid> handler_);
    }
}
