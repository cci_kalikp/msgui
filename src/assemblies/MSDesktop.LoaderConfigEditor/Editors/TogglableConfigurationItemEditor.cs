﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using MSDesktop.LoaderConfigEditor.Converters; 
using Xceed.Wpf.Toolkit.PropertyGrid;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace MSDesktop.LoaderConfigEditor.Editors
{
    public abstract class TogglableConfigurationItemEditor : CheckBoxEditor
    {
        protected abstract string EnablementPropertyName { get; }
        protected abstract bool EnablementReversed { get; }

        protected override void ResolveValueBinding(PropertyItem propertyItem)
        {
            Binding binding = new Binding("Value." + EnablementPropertyName);
            if (EnablementReversed)
            {
                binding.Converter = new MorganStanley.Desktop.Loader.Configuration.Designer.InverseBoolConverter();
            }
            binding.Source = propertyItem; 
            binding.UpdateSourceTrigger = UpdateSourceTrigger.Default;
            binding.Mode = (propertyItem.IsReadOnly ? BindingMode.OneWay : BindingMode.TwoWay);
            BindingOperations.SetBinding(this.Editor, this.ValueProperty, binding);

            Binding binding2 = new Binding("IsExpanded");
            string baseText = EnablementReversed ?  "Uncheck to disable" : "Check to enable";
            binding2.Converter = new ExpandedToDescroptionConverter(baseText, baseText + ", expand to edit details...");
            binding2.Source = propertyItem;
            binding2.UpdateSourceTrigger = UpdateSourceTrigger.Default;
            binding2.Mode = BindingMode.OneWay;
            BindingOperations.SetBinding(this.Editor, ContentControl.ContentProperty, binding2);

        } 
         
    }
}
