﻿
namespace MSDesktop.LoaderConfigEditor.Editors
{
    public class EnablableConfigurationItemEditor : TogglableConfigurationItemEditor
    {
        protected override string EnablementPropertyName
        {
            get { return "Enabled"; }
        }

        protected override bool EnablementReversed
        {
            get { return false; }
        }
    }
}
