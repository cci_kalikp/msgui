﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using MSDesktop.LoaderConfigEditor.Helpers;
using MSDesktop.LoaderConfigEditor.ViewModels; 
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;

namespace MSDesktop.LoaderConfigEditor.Editors
{
    public class ConfigFilePathEditor:FilePathEditor, IPopupEditor
    {
        public ConfigFilePathEditor() 
        {
            var button = new Button() { Content = "Edit" };
            ContentEditor.Margin = new Thickness(2, 0, 0, 0);
            EditExternallyCommand = new DelegateCommand(EditExternally);
            button.Command = EditExternallyCommand;
            ContentEditor.Content = button;
            txtPath.Watermark = "Specify the configuration defined in external file";
            txtPath.TextChanged += new TextChangedEventHandler(txtPath_TextChanged);

        }

        void txtPath_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtPath.Text))
            {
                txtPath.ToolTip = "Clear the external file to specify the configuration locally.";
            }
            else
            {
                txtPath.ToolTip = null;
            }
        }

        public ICommand EditExternallyCommand { get; private set; }
        private void EditExternally(object parameters_)
        {
            ShowCollectionDialog(null);
        }

        protected override string GetFilter()
        {
            return EditorUtilities.GetFilter(PathType.XmlFile);
        }

        protected override string BindingPath
        {
            get
            {
                return "Value.DefinitionFile";
            }
        }

        public void ShowCollectionDialog(Action<Xceed.Wpf.Toolkit.PropertyGrid.PropertyGrid> handler_)
        {
            if (propertyItem.Value != null)
            {
                ConfigurationTypeDescriptor propertyDescriptor = propertyItem.Value as ConfigurationTypeDescriptor;
                if (propertyDescriptor == null) return;
                Type configType = propertyDescriptor.SourceObject.GetType();
                SubConfigViewModel vm;
                string path = PathUtilities.ReplaceEnvironmentVariables(Value);
                if (!string.IsNullOrEmpty(path) && File.Exists(path))
                {
                    vm = new SubConfigViewModel(configType, path, this.CurrentDirectory);
                }
                else
                {
                    vm = new SubConfigViewModel(configType, this.CurrentDirectory);
                    vm.ConfigName = "New config";
                }
                if (!vm.Loaded) return;
                SubConfigEditor editor = new SubConfigEditor();
                editor.Resources.MergedDictionaries.Add(new ResourceDictionary
                {
                    Source = new Uri("pack://application:,,,/MSDesktop.LoaderConfigEditor;component/Resources/Styles.xaml",
                                     UriKind.RelativeOrAbsolute)
                });
                if (handler_ != null)
                {
                    RoutedEventHandler loadedHandler = null;
                    editor.Loaded += loadedHandler = (sender_, args_) =>
                    {
                        handler_(editor.propertyGrid);
                        editor.Loaded -= loadedHandler;
                    };
                }
                editor.DataContext = vm;
                editor.Owner = Application.Current.MainWindow;
                editor.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                if (editor.ShowDialog() == true && !string.IsNullOrEmpty(vm.FilePath))
                {
                    this.Value = vm.FilePath;
                }
            }
        }
    }
}
