﻿using System;
using System.Collections;  
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Media; 
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace MSDesktop.LoaderConfigEditor.Editors
{
    public class ColorHelperEditor : ColorEditor
    {
        protected override void ResolveValueBinding(Xceed.Wpf.Toolkit.PropertyGrid.PropertyItem propertyItem)
        {
            Binding binding = new Binding("Value.Color");
            binding.FallbackValue = Colors.Transparent;
            binding.Source = propertyItem; 
            binding.UpdateSourceTrigger = UpdateSourceTrigger.Default;
            binding.Mode = (propertyItem.IsReadOnly ? BindingMode.OneWay : BindingMode.TwoWay);
            binding.Converter = this.CreateValueConverter();
            BindingOperations.SetBinding(this.Editor, this.ValueProperty, binding);
        }
         
    }
}
