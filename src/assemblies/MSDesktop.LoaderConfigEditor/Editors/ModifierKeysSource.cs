﻿ 
using System.Windows.Input;
using Xceed.Wpf.Toolkit.PropertyGrid.Attributes;

namespace MSDesktop.LoaderConfigEditor.Editors
{
    public class ModifierKeysSource:IItemsSource
    {
        private static readonly ModifierKeysConverter converter = new ModifierKeysConverter();
        public ItemCollection GetValues()
        {
            ItemCollection items = new ItemCollection();
            for (int i = 0; i < (int)ModifierKeys.Windows * 2; i++)
            {
                if (ModifierKeysConverter.IsDefinedModifierKeys((ModifierKeys) i))
                {
                    items.Add((ModifierKeys)i, converter.ConvertToString((ModifierKeys)i)); 

                }
            }
            return items;
        }
    }
}
