﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using MSDesktop.LoaderConfigEditor.ViewModels;

namespace MSDesktop.LoaderConfigEditor.Editors
{
    /// <summary>
    /// Interaction logic for SubConfigEditor.xaml
    /// </summary>
    public partial class SubConfigEditor : Window
    {
        public SubConfigEditor()
        {
            InitializeComponent();
            propertyGrid.IsCategorized = false;
            EditorUtilities.FixPropertyGrid(this.propertyGrid);
            
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            SubConfigViewModel model = this.DataContext as SubConfigViewModel;
            if (model != null)
            {
                if (!model.Close())
                {
                    e.Cancel = true;
                }
                else
                {
                    this.DialogResult = !model.IsDirty;
                }
            }
            base.OnClosing(e);
        }

    }
}
