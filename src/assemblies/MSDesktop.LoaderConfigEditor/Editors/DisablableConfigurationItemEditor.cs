﻿
namespace MSDesktop.LoaderConfigEditor.Editors
{
    public class DisablableConfigurationItemEditor : TogglableConfigurationItemEditor
    {
        protected override string EnablementPropertyName
        {
            get { return "Disabled"; }
        }

        protected override bool EnablementReversed
        {
            get { return true; }
        }
    }
}
