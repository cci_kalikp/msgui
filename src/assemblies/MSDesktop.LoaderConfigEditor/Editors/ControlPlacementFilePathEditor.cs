﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input; 
using MSDesktop.LoaderConfigEditor.ViewModels; 
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;

namespace MSDesktop.LoaderConfigEditor.Editors
{
    public class ControlPlacementFilePathEditor:FilePathEditor, IPopupEditor
    {
        public ControlPlacementFilePathEditor() 
        {
            var button = new Button() { Content = "Edit" };
            ContentEditor.Margin = new Thickness(2, 0, 0, 0);
            EditCommand = new DelegateCommand(Edit);
            button.Command = EditCommand;
            ContentEditor.Content = button;  
        }
         
        public ICommand EditCommand { get; private set; }
        private void Edit(object parameters_)
        {
            ShowCollectionDialog(null);
        }

        protected override string GetFilter()
        {
            return EditorUtilities.GetFilter(PathType.XmlFile);
        }
         
        public void ShowCollectionDialog(Action<Xceed.Wpf.Toolkit.PropertyGrid.PropertyGrid> handler_)
        {
            if (propertyItem.Value != null)
            { 
                ControlPlacementViewModel vm;
                string path = PathUtilities.ReplaceEnvironmentVariables(Value);
                if (!string.IsNullOrEmpty(path) && File.Exists(path))
                {
                    vm = new ControlPlacementViewModel(path, this.CurrentDirectory);
                }
                else
                {
                    vm = new ControlPlacementViewModel(this.CurrentDirectory);
                    vm.ConfigName = "New config";
                }
                if (!vm.Loaded) return;
                ControlPlacementEditor editor = new ControlPlacementEditor(); 
                editor.DataContext = vm;
                editor.Owner = Application.Current.MainWindow;
                editor.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                if (editor.ShowDialog() == true && !string.IsNullOrEmpty(vm.FilePath))
                {
                    this.Value = vm.FilePath;
                }
            }
        }
    }
}
