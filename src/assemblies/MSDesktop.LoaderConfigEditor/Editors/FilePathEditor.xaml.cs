﻿using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using MSDesktop.LoaderConfigEditor.Helpers; 
using Microsoft.Win32; 
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.MSDotNet.MSGui.Core;
using Xceed.Wpf.Toolkit;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;
using PropertyItem = Xceed.Wpf.Toolkit.PropertyGrid.PropertyItem;

namespace MSDesktop.LoaderConfigEditor.Editors
{
    /// <summary>
    /// Interaction logic for FilePathEditor.xaml
    /// </summary> 
    public partial class FilePathEditor : UserControl, ITypeEditor
    { 
        protected PropertyItem propertyItem;
        public FilePathEditor()
        {
            InitializeComponent();
        }

        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register("Value", typeof(string), typeof(FilePathEditor), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public string Value
        {
            get { return (string) GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }  

        protected string CurrentDirectory
        {
            get
            {
                var descr = propertyItem.Instance as ConfigurationTypeDescriptor;
                if (descr != null)
                {
                    if (descr.ConfigViewModel != null && !string.IsNullOrEmpty(descr.ConfigViewModel.FilePath))
                    {
                        return Path.GetDirectoryName(descr.ConfigViewModel.FilePath);
                    }
                }
                

                return null;
            }
        }
          

        private void ButtonBase_OnClick(object sender_, RoutedEventArgs e_)
        {
            var openFileDialog = new OpenFileDialog();  
            openFileDialog.Multiselect = false; 
            openFileDialog.Filter = GetFilter();
            string path = PathUtilities.ReplaceEnvironmentVariables(Value);
            if (!string.IsNullOrEmpty(path) && File.Exists(path))
            {
                openFileDialog.FileName = path; 
            }
            if (!string.IsNullOrEmpty(openFileDialog.FileName) && Path.IsPathRooted(openFileDialog.FileName))
            {
                openFileDialog.InitialDirectory = Path.GetDirectoryName(openFileDialog.FileName); 
                string extension = Path.GetExtension(openFileDialog.FileName).ToLower();
                if (!string.IsNullOrEmpty(extension))
                {
                    string[] items = openFileDialog.Filter.Split('|');
                    for (int i = 0; i < items.Length; i++)
                    {
                        if (items[i].Contains("*" + extension))
                        {
                            openFileDialog.FilterIndex = i/2 + 1;
                            break;
                        }
                    }
                }
                 
            }
            else
            {
                openFileDialog.InitialDirectory = CurrentDirectory;
            }
 
            if (openFileDialog.ShowDialog(Application.Current.MainWindow) == true)
            {
                Value = openFileDialog.FileName;
            }
        }

        protected virtual string GetFilter()
        {
            string propertyName = propertyItem.PropertyDescriptor.Name;
            object instance = propertyItem.Instance;
            if (instance is ConfigurationTypeDescriptor)
            {
                instance = ((ConfigurationTypeDescriptor)instance).SourceObject;
            }
            PathAttribute pathAttribute = instance.GetType()
                    .GetProperty(propertyName)
                    .GetCustomAttributes(true)
                    .FirstOrDefault(a_ => a_ is PathAttribute) as PathAttribute;
            if (pathAttribute != null)
            {
                return EditorUtilities.GetFilter(pathAttribute.PathType); 
            }
            return null;

        } 
      
        protected virtual string BindingPath
        {
            get { return "Value"; }
        }

        public FrameworkElement ResolveEditor(PropertyItem propertyItem_)
        {
            propertyItem = propertyItem_; 
            Binding binding = new Binding(BindingPath);
            binding.Source = propertyItem_; 
            binding.Mode = propertyItem_.IsReadOnly ? BindingMode.OneWay : BindingMode.TwoWay;
            BindingOperations.SetBinding(this, ValueProperty, binding);
            return this;
        }
    }
}
