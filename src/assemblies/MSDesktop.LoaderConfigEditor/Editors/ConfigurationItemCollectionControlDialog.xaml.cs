﻿using System;
using System.Collections;
using System.Windows;
using System.Windows.Controls;
using MSDesktop.LoaderConfigEditor.Helpers;

namespace MSDesktop.LoaderConfigEditor.Editors
{
    /// <summary>
    /// Interaction logic for ConfigurationItemCollectionControlDialog.xaml
    /// </summary>
    public partial class ConfigurationItemCollectionControlDialog : Window
    {
        public ConfigurationItemCollectionControlDialog()
        {
            InitializeComponent();
        }

        internal ConfigurationItemCollectionControl PropertyGrid;
        public ConfigurationItemCollectionControlDialog(Type itemsourceType_, object parentObject_, IList items_)
            : this()
        {
            PropertyGrid = new ConfigurationItemCollectionControl(itemsourceType_, ((ConfigurationTypeDescriptor)parentObject_).ConfigViewModel, items_);
            Grid.SetRow(PropertyGrid, 0);
            this.grdLayout.Children.Add(PropertyGrid);

        }
         
        public static readonly DependencyProperty ItemsSourceProperty = DependencyProperty.Register("ItemsSource", typeof(IList), typeof(ConfigurationItemCollectionControlDialog), new UIPropertyMetadata(null));
        public static readonly DependencyProperty IsReadOnlyProperty = DependencyProperty.Register("IsReadOnly", typeof(bool), typeof(ConfigurationItemCollectionControlDialog), new UIPropertyMetadata(false));
		public IList ItemsSource
		{
			get
			{
                return (IList)base.GetValue(ItemsSourceProperty);
			}
			set
			{
                base.SetValue(ItemsSourceProperty, value);
			}
		}
		 
		public bool IsReadOnly
		{
			get
			{
				return (bool)base.GetValue(IsReadOnlyProperty);
			}
			set
			{
				base.SetValue(IsReadOnlyProperty, value);
			}
		}
 

		private void OkButton_Click(object sender, RoutedEventArgs e)
		{
			PropertyGrid.CommitChanges();
		    this.ItemsSource = PropertyGrid.ItemsSource;
			base.Close();
		}
		private void CancelButton_Click(object sender, RoutedEventArgs e)
		{
            PropertyGrid.RollbackChanges();
			base.Close();
		}
    }
}
