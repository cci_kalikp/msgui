﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Markup;
using System.Windows.Media;
using System.Xml.Serialization;
using Infragistics.Collections;
using MSDesktop.LoaderConfigEditor.Converters;
using MSDesktop.LoaderConfigEditor.Helpers;
using MSDesktop.LoaderConfigEditor.ViewModels;
using MorganStanley.MSDotNet.MSGui.Core;
using Xceed.Wpf.Toolkit; 
using Xceed.Wpf.Toolkit.PropertyGrid;

namespace MSDesktop.LoaderConfigEditor.Editors
{
    public class ConfigurationItemCollectionControl:CollectionControl
    {
        private readonly bool oldDirty = false;
        private readonly IModifiableConfigViewModel vm; 

        public ConfigurationItemCollectionControl(Type itemSourceType_, IModifiableConfigViewModel vm_, IList items_):base()
        {
            vm = vm_;
            ItemsSourceType = itemSourceType_;
            oldDirty = vm.IsDirty;
            this.ItemsSource = items_;
            this.Items.Clear();
            if (items_ != null)
            {
                foreach (var item in items_)
                {
                    if (item is ConfigurationTypeDescriptor) continue;
                    Items.Add(new ConfigurationTypeDescriptor(vm, item));
                }

            }
            if (itemSourceType_.IsGenericType)
            {
                var elementType = itemSourceType_.GetGenericArguments().First() as Type;
                this.NewItemTypes = new List<Type>();
                NewItemTypes.Add(elementType);
                foreach (XmlIncludeAttribute xmlIncludeAttribute in elementType.GetCustomAttributes(typeof(XmlIncludeAttribute), false))
                {
                    this.NewItemTypes.Add(xmlIncludeAttribute.Type); 
                } 
            }



            this.ItemAdding += new ItemAddingRoutedEventHandler(ConfigurationItemCollectionControl_ItemAdding);
            this.ItemAdded += new ItemAddedRoutedEventHandler(ConfigurationItemCollectionControl_ItemAdded);
            this.ItemDeleted += new ItemDeletedRoutedEventHandler(ConfigurationItemCollectionControl_ItemDeleted);
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate(); 
            var itemsListBox = this.GetTemplateChild("_itemsListBox") as ListBox;
            if (itemsListBox != null)
            {
                DataTemplate template = new DataTemplate();
                FrameworkElementFactory tbFactory = new FrameworkElementFactory(typeof(TextBlock));
                tbFactory.SetValue(StackPanel.OrientationProperty, Orientation.Horizontal);
                tbFactory.SetBinding(TextBlock.TextProperty, new Binding(".") { Converter = new TypeDescriptorToNameConverter() }); 

                template.VisualTree = tbFactory;
                itemsListBox.ItemTemplate = template;
            }
            foreach (var grid in this.FindVisualChildren<PropertyGrid>())
            {
                grid.ShowSearchBox = false;
                grid.IsCategorized = false;

                Grid layout = grid.Parent as Grid;
                if (layout != null)
                {
                    Grid parentLayout = layout.Parent as Grid;
                    if (parentLayout != null && parentLayout.ColumnDefinitions.Count > 1)
                    {
                        parentLayout.ColumnDefinitions[0].Width = new GridLength(0.7, GridUnitType.Star); 
                    }
                    layout.RowDefinitions.Clear();
                    layout.RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto }); //label "properties:"
                    layout.RowDefinitions.Add(new RowDefinition(){Height = new GridLength(1, GridUnitType.Star)}); //property grid
                    layout.RowDefinitions.Add(new RowDefinition(){Height =  new GridLength(5)}); //splitter
                    layout.RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto }); //property descriptions
                    layout.RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto }); //property snapshot

                    var headerLabel = layout.FindLogicalChildren<TextBlock>().FirstOrDefault();
                    if (headerLabel != null)
                    {
                        var parent = headerLabel.Parent as Grid;
                        if (parent != null)
                        { 
                            parent.Children.Remove(headerLabel);
                            FlowDocumentScrollViewer docViewer = new FlowDocumentScrollViewer()
                                {
                                    HorizontalScrollBarVisibility = ScrollBarVisibility.Auto,
                                    VerticalScrollBarVisibility = ScrollBarVisibility.Auto
                                };
                            parent.Children.Add(docViewer);
                            docViewer.SetBinding(FlowDocumentScrollViewer.DocumentProperty, new Binding("SelectedItem") { Source = this, Converter = new TypeDescriptorToDescriptionConverter() });
                        } 
                    }
                    grid.ShowSummary = false;
                    GridSplitter splitter = new GridSplitter()
                        {
                            Height = 3,
                            HorizontalAlignment = HorizontalAlignment.Stretch
                        };
                    Grid.SetRow(splitter, 2);
                    Grid.SetColumn(splitter, 1);
                    layout.Children.Add(splitter);
                    FlowDocumentScrollViewer viewer = new FlowDocumentScrollViewer()
                        {
                            HorizontalScrollBarVisibility = ScrollBarVisibility.Auto,
                            VerticalScrollBarVisibility = ScrollBarVisibility.Auto
                        };
                    Grid.SetRow(viewer, 3); 
                    Grid.SetColumn(viewer, 1);
                    var binding = new Binding("SelectedPropertyItem.PropertyDescriptor.RichDescription");
                    binding.Source = grid;
                    BindingOperations.SetBinding(viewer, FlowDocumentScrollViewer.DocumentProperty, binding);
                    layout.Children.Add(viewer);

                    Expander expander = this.FindResource("SnapShot") as Expander;
                    if (expander != null)
                    {
                        expander.DataContext = grid;
                        Grid.SetRow(expander, 4);
                        Grid.SetColumn(expander, 1);
                        layout.Children.Add(expander);
                    }
                }
                PropertyGrid = grid;
                EditorUtilities.FixPropertyGrid(grid); 
            } 
        }

        internal PropertyGrid PropertyGrid;
  
        void ConfigurationItemCollectionControl_ItemDeleted(object sender, ItemEventArgs e)
        {
            vm.IsDirty = true;
        }

        void ConfigurationItemCollectionControl_ItemAdded(object sender, ItemEventArgs e)
        {
            vm.IsDirty = true;
        }

        void ConfigurationItemCollectionControl_ItemAdding(object sender, ItemAddingEventArgs e)
        {
            if (e.Item is ConfigurationTypeDescriptor) return;
            e.Item = new ConfigurationTypeDescriptor(vm, e.Item);
        } 
         
        private IList CreateItemsSource()
        { 
            if (this.ItemsSourceType == null)
            {
                return null;
            }
            return Activator.CreateInstance(ItemsSourceType) as IList;
        }

        public void CommitChanges()
        {
            IList list = ItemsSource ?? CreateItemsSource();
            if (list == null)
            {
                return;
            }
            list.Clear();
            foreach (object current in this.Items)
            {
                object item = current;
                ConfigurationTypeDescriptor descriptor = current as ConfigurationTypeDescriptor;
                if (descriptor != null)
                {
                    item = descriptor.SourceObject;
                }
                list.Add(item);
            }
            this.ItemsSource = list;
        }

        public void RollbackChanges()
        {
            vm.IsDirty = oldDirty; 
        }
    }
}
