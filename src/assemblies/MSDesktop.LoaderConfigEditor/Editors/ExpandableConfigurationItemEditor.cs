﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;
using MSDesktop.LoaderConfigEditor.Converters; 
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace MSDesktop.LoaderConfigEditor.Editors
{
    public class ExpandableConfigurationItemEditor:TextBlockEditor
    {
        protected override void ResolveValueBinding(Xceed.Wpf.Toolkit.PropertyGrid.PropertyItem propertyItem)
        { 
            Binding binding = new Binding("IsExpanded");
            binding.Converter = new ExpandedToDescroptionConverter(null, "Expand to edit...");
            binding.Source = propertyItem;
            binding.UpdateSourceTrigger = UpdateSourceTrigger.Default;
            binding.Mode = BindingMode.OneWay; 
            BindingOperations.SetBinding(this.Editor, this.ValueProperty, binding);
        }
    }
}
