﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using System.Windows.Threading;
using MSDesktop.LoaderConfigEditor.Editors;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using MorganStanley.Desktop.Loader.Configuration;
using MorganStanley.Desktop.Loader.Configuration.Designer;
using MorganStanley.Desktop.Loader.Configuration.Helpers;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;

namespace MSDesktop.LoaderConfigEditor.ViewModels
{
    public class ControlPlacementViewModel:ModifiableConfigViewModelBase
    {
       private readonly string currentDirectory;
        private ChromeConfiguration config;
        public ControlPlacementViewModel(string filePath_, string currentDirectory_):base(typeof(ChromeConfiguration), filePath_, true)
        {
            currentDirectory = currentDirectory_;
            InitializeCore();
        }

        public ControlPlacementViewModel(string currentDirectory_)
            : base(new ChromeConfiguration(), true)
        {
            currentDirectory = currentDirectory_;
            InitializeCore();
        }

        private void InitializeCore()
        {
            config = (ChromeConfiguration)ConfigurationObj;
            Placements = new List<PlacementInfo>();
            if (config == null) return;
            Dictionary<ShellMode, PlacementInfo> placementInfos = new Dictionary<ShellMode, PlacementInfo>();
            if (config.Placements != null)
            {
                foreach (var placement in config.Placements)
                {
                    if (!placementInfos.ContainsKey(placement.ShellMode))
                    {
                        Placements.Add(placement);
                        placementInfos.Add(placement.ShellMode, placement);
                    }
                }
            }
            foreach (ShellMode value in Enum.GetValues(typeof(ShellMode)))
            {
                if (!placementInfos.ContainsKey(value))
                {
                    Placements.Add(new PlacementInfo(){ShellMode = value});
                }
            }
            CurrentPlacement = Placements[0];

            MoveItemUpCommand = new DelegateCommand(MoveItemUp, CanMoveItemUp);
            MoveItemDownCommand = new DelegateCommand(MoveItemDown, CanMoveItemDown); 
            RemoveItemCommand = new DelegateCommand(RemoveItem, o_ => CurrentElement != null && CurrentElement.Parent != null);
            NewItemCommand = new DelegateCommand(NewItem, o_=> AllowNewItem && !AllowMultiTypeNewItem);
            NewAreaCommand = new DelegateCommand(NewArea, o_ => AllowMultiTypeNewItem && !AllowMultiTypeNewArea);
        }

        protected override string FileFilter
        {
            get { return EditorUtilities.GetFilter(PathType.XmlFile); }
        }

        protected override string FileInitialDirectory
        {
            get
            {
                string path = base.FileInitialDirectory;
                if (!string.IsNullOrEmpty(path)) return path;
                return currentDirectory;
            }
        }

        public List<PlacementInfo> Placements { get; set; }

        private PlacementInfo currentPlacement;
        public PlacementInfo CurrentPlacement
        {
            get { return currentPlacement; }
            set
            {
                if (currentPlacement != value)
                {
                    currentPlacement = value;
                    OnPropertyChanged("CurrentPlacement");
                    if (currentPlacement == null || currentPlacement.Children.Count == 0)
                    {
                        CurrentElement = null; 
                    }
                    else
                    {
                        CurrentElement = currentPlacement.Children[0];        
                    }
                    OnPropertyChanged("AllowNewItem");
                    OnPropertyChanged("AllowMultiTypeNewItem");
                    OnPropertyChanged("ValidChildrenElements");
                    OnPropertyChanged("AllowNewArea");
                    OnPropertyChanged("AllowMultiTypeNewArea");
                    OnPropertyChanged("ValidAreas");
                    ShouldExpandAll = true;
                }
            }
        }

        private IPlacementElement currentElement;
        public IPlacementElement CurrentElement
        {
            get { return currentElement; }
            set
            {
                if (currentElement != value)
                { 
                    currentElement = value; 
                    OnPropertyChanged("CurrentElement");
                    OnPropertyChanged("AllowNewItem");
                    OnPropertyChanged("AllowMultiTypeNewItem");
                    OnPropertyChanged("ValidChildrenElements");
                    OnPropertyChanged("AllowNewArea");
                    OnPropertyChanged("AllowMultiTypeNewArea");
                    OnPropertyChanged("ValidAreas");
                }
            }
        }
 
         
        private static bool AllowNewChild(IPlacementElement parent_, out List<Type> allowedTypes_)
        {
            allowedTypes_ = new List<Type>();
            if (parent_.ValidChildrenTypes == null) return false;
            if (parent_.ValidChildrenTypes.Count == 0) return false;
            allowedTypes_.AddRange(parent_.ValidChildrenTypes);
            if (parent_.AllowMultiple) return true; 
            if (parent_.Children == null || parent_.Children.Count == 0) return true;
            foreach (var child in parent_.Children)
            {
                if (allowedTypes_.Contains(child.GetType()))
                {
                    allowedTypes_.Remove(child.GetType());
                }
            }
            return allowedTypes_.Count > 0;
        }

        private List<Type> allowedNewAreas; 
        public bool AllowNewArea
        {
            get { return AllowNewChild(CurrentPlacement, out allowedNewAreas); }
        }

        public bool AllowMultiTypeNewArea
        {
            get { return allowedNewAreas != null && allowedNewAreas.Count > 1; }
        }

        private List<Type> allowedNewItems; 
        public bool AllowNewItem
        {
            get { return AllowNewChild(currentElement ?? CurrentPlacement, out allowedNewItems); }

        }

        public bool AllowMultiTypeNewItem
        {
            get { return allowedNewItems != null && allowedNewItems.Count > 1; }
        }

        public List<PlacementElementViewModel> ValidAreas
        {
            get
            { 
                if (allowedNewAreas == null)
                {
                    AllowNewChild(CurrentPlacement, out allowedNewAreas);
                }
                return allowedNewAreas.Select(childrenType_ =>
                    new PlacementElementViewModel(Activator.CreateInstance(childrenType_) as IPlacementElement, CurrentPlacement, this)).ToList();
            }
        }

        public List<PlacementElementViewModel> ValidChildrenElements
        {
            get
            {
                var parent = currentElement ?? CurrentPlacement;
                if (allowedNewItems == null)
                {
                    AllowNewChild(parent, out allowedNewItems);
                }
                return allowedNewItems.Select(childrenType_ =>
                    new PlacementElementViewModel(Activator.CreateInstance(childrenType_) as IPlacementElement, parent, this)).ToList();
            }
        }

        private bool shouldExpandAll;
        public bool ShouldExpandAll
        {
            get { return shouldExpandAll; }
            set
            {
                if (shouldExpandAll != value)
                {
                    shouldExpandAll = value;
                    OnPropertyChanged("ShouldExpandAll");
                }
            }
        }

        public ICommand MoveItemUpCommand { get; private set; }
        public ICommand MoveItemDownCommand { get; private set; } 
        public ICommand RemoveItemCommand { get; private set; }
        public ICommand NewItemCommand { get; private set; }
        public ICommand NewAreaCommand { get; private set; }

        private static IPlacementElement GetInvalidElement(IPlacementElement parent_, List<string> widgetIds)
        {
            if (parent_.Children != null && parent_.Children.Count > 0)
            {
                List<string> ids = new List<string>();
                foreach (var child in parent_.Children)
                {
                    IEditablePlacementElement editable = child as IEditablePlacementElement;
                    if (editable != null)
                    {
                        if (string.IsNullOrEmpty(editable.Id))
                        {
                            TaskDialog.ShowMessage("Please specify the id", "Control Placement Editor",
                                                   TaskDialogCommonButtons.Close, VistaTaskDialogIcon.Error);
                            return child;
                        }
                        if (editable is Widget)
                        {
                            if (widgetIds.Contains(editable.Id))
                            {
                                TaskDialog.ShowMessage("Please specify a unique widget id", "Control Placement Editor",
                                    TaskDialogCommonButtons.Close, VistaTaskDialogIcon.Error);
                                return child;
                            }
                            widgetIds.Add(editable.Id);
                        }
                        else
                        {
                            if (ids.Contains(editable.Id))
                            {
                                TaskDialog.ShowMessage("Please specify a unique child name", "Control Placement Editor",
    TaskDialogCommonButtons.Close, VistaTaskDialogIcon.Error);
                                return child;

                            }
                            ids.Add(editable.Id);
                        } 
                    }
                    var invalidElement = GetInvalidElement(child, widgetIds);
                    if (invalidElement != null)
                    {
                        return invalidElement;
                    }
                }
            }
            return null;
        }

        protected override bool SaveConfigCore(string fileName_)
        {
            var invalidElement = GetInvalidElement(CurrentPlacement, new List<string>());
            if (invalidElement != null)
            {
                CurrentElement = invalidElement;
                return false;
            }
            foreach (var placement in Placements)
            {
                if (placement == CurrentPlacement) continue;
                invalidElement = GetInvalidElement(placement, new List<string>());
                if (invalidElement != null)
                {
                    CurrentPlacement = placement;
                    Dispatcher.CurrentDispatcher.Invoke(() => CurrentElement = invalidElement,
                                                        DispatcherPriority.Background); 
                    return false;
                }
            } 

            if (config.Placements == null)
            {
                config.Placements = new List<PlacementInfo>();
            }
            else
            { 
                config.Placements.Clear();
            }
            foreach (var placement in Placements)
            {
                if (placement.Children != null && placement.Children.Count > 0)
                { 
                    config.Placements.Add(placement);
                }
            } 
            return base.SaveConfigCore(fileName_);
        }

        private void MoveItemUp(object o_)
        {
            this.IsDirty = true;
            var currentElementCopy = CurrentElement; 
            int oldIndex = CurrentElement.Parent.Children.IndexOf(currentElementCopy);
            currentElementCopy.Parent.Children.RemoveAt(oldIndex);
            currentElementCopy.Parent.Children.Insert(oldIndex - 1, currentElementCopy);
            CurrentElement = currentElementCopy;
        }

        private bool CanMoveItemUp(object o_)
        {
            return currentElement != null && currentElement.Parent != null &&
                  (currentElement is IEditablePlacementElement) &&
                  currentElement.Parent.Children.IndexOf(currentElement) > 0;

        }

        private void MoveItemDown(object o_)
        {
            this.IsDirty = true;
            var currentElementCopy = CurrentElement;
            int oldIndex = CurrentElement.Parent.Children.IndexOf(currentElementCopy);
            currentElementCopy.Parent.Children.RemoveAt(oldIndex);
            currentElementCopy.Parent.Children.Insert(oldIndex + 1, currentElementCopy);
            CurrentElement = currentElementCopy;
        } 

        private bool CanMoveItemDown(object o_)
        {
            return currentElement != null && currentElement.Parent != null &&
                  (currentElement is IEditablePlacementElement) &&
                   currentElement.Parent.Children.IndexOf(currentElement) < currentElement.Parent.Children.Count - 1;
        }

        private void RemoveItem(object o_)
        {
            this.IsDirty = true;
            currentElement.Parent.Children.Remove(currentElement);
           
        }

        private void NewItem(object o_)
        {
            var validItem = ValidChildrenElements[0];
            validItem.NewItemCommand.Execute(o_);
        }

        private void NewArea(object o_)
        {
            var validItem = ValidAreas[0];
            validItem.NewItemCommand.Execute(o_); 
        }
       
    }

     
}
