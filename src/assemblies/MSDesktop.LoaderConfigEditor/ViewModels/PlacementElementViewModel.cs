﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using MorganStanley.Desktop.Loader.Configuration;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;

namespace MSDesktop.LoaderConfigEditor.ViewModels
{
    public class PlacementElementViewModel:ViewModelBase
    {
        private IPlacementElement element; 
        public PlacementElementViewModel(IPlacementElement element_, IPlacementElement parent_, ControlPlacementViewModel model_)
        {
            element = element_; 
            NewItemCommand = new DelegateCommand(_ =>
                {
                    parent_.Children.Add(element_);
                    model_.CurrentElement = element_;
                    model_.IsDirty = true;
                },
                o_ => parent_.ValidChildrenTypes != null );
        }

        public string Name
        {
            get { return element.Name; }
        }

        public ICommand NewItemCommand { get; private set; }
    }
}
