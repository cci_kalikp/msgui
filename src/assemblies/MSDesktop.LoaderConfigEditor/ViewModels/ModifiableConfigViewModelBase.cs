﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Xml.Serialization;
using MSDesktop.LoaderConfigEditor.Helpers;
using Microsoft.Win32;
using MorganStanley.MSDotNet.MSGui.Controls.SearchBox;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;

namespace MSDesktop.LoaderConfigEditor.ViewModels
{
    public abstract class ModifiableConfigViewModelBase : ViewModelBase, IModifiableConfigViewModel, IClosable
    { 
        private static readonly Dictionary<Type, XmlSerializer> serializers = new Dictionary<Type, XmlSerializer>();
        protected object ConfigurationObj;
        public event EventHandler<SearchResultItemSelectedEventArgs> SearchResultItemSelected;
        
        protected ModifiableConfigViewModelBase(Type configType_, string filePath_, bool customDisplay_= false)
        {
            FilePath = filePath_;
            try
            {
                using (var file = new FileStream(filePath_, FileMode.Open, FileAccess.Read))
                {
                    InitializeCore(GetSerializer(configType_).Deserialize(file), customDisplay_);
                }  
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error); 
            } 
        }

        protected ModifiableConfigViewModelBase(object configuration_, bool customDisplay_=false)
        {
            InitializeCore(configuration_, customDisplay_);
        }

        private void InitializeCore(object configuration_, bool customDisplay_)
        { 
            ConfigurationObj = configuration_; 
            SaveConfigCommand = new DelegateCommand(_ => SaveConfig(), o_ => isDirty);
            SaveConfigAsCommand = new DelegateCommand(_ => SaveConfigAs());
            CloseConfigCommand = new DelegateCommand(_ => Close());
            if (!customDisplay_)
            {
                Configuration = new ConfigurationTypeDescriptor(this, ConfigurationObj);
                SearchBox = new SearchBoxViewModel(Configuration) { AsyncSearch = true };
                SearchBox.PropertyChanged += SearchBox_PropertyChanged;
            }

            Loaded = true;
        }
         
        void SearchBox_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "SelectedItem" && SearchBox.SelectedItem != null)
            {
                var copy = SearchResultItemSelected;
                if (copy != null)
                {
                    copy(this, new SearchResultItemSelectedEventArgs(SearchBox.SelectedItem as SearchResultItem));
                } 
            }
        }
         
        private XmlSerializer GetSerializer(Type configType_)
        {
            XmlSerializer serializer;
            if (!serializers.TryGetValue(configType_, out serializer))
            {
                serializer = new XmlSerializer(configType_);
                serializers.Add(configType_, serializer);
            }
            return serializer;
        }
         
        public bool Loaded { get; private set; }

        private string filePath;
        public string FilePath
        {
            get { return filePath; }
            private set
            {
                filePath = value;
                ConfigName = Path.GetFileNameWithoutExtension(filePath);
                OnPropertyChanged("FilePath");
                OnPropertyChanged("ConfigName");
                OnPropertyChanged("DisplayName");
            }
        }

        private bool isDirty = false;
        public bool IsDirty
        {
            get { return isDirty; }
            set
            {
                if (isDirty != value)
                {
                    isDirty = value;
                    OnPropertyChanged("IsDirty");
                    OnPropertyChanged("DisplayName");
                }
            }
        }
        public string ConfigName { get; set; }

        public string DisplayName
        {
            get
            {
                string name = ConfigName;
                if (isDirty)
                {
                    name += "*";
                }
                return name;
            }
        }

        public ISearchBoxViewModel SearchBox { get; set; }
        public ConfigurationTypeDescriptor Configuration { get; set; }

        public ICommand SaveConfigCommand { get; private set; }

        public ICommand SaveConfigAsCommand { get; private set; } 

        public ICommand CloseConfigCommand { get; private set; }

        public virtual bool Close()
        {
            if (isDirty)
            {
                string message = "Do you want to save changes to " +
                                 (string.IsNullOrEmpty(FilePath) ? ConfigName : FilePath) + "?";
                MessageBoxResult result = MessageBox.Show(message, "MSDesktop Configuration Editor",
                                                          MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
                if (result == MessageBoxResult.Cancel) return false;
                if (result == MessageBoxResult.Yes)
                {
                    if (!SaveConfig()) return false;
                }
            }
            return true;
        }

        protected abstract string FileFilter { get; }
        protected virtual string FileInitialDirectory
        {
            get
            {
                if (!string.IsNullOrEmpty(FilePath))
                {
                   return Path.GetDirectoryName(FilePath);
                }
                return null;
            }
        }
        private bool SaveConfig()
        {
            if (!isDirty) return true;
            if (string.IsNullOrEmpty(FilePath))
            {
                return SaveConfigAs();
            }
            return SaveConfigCore(FilePath);
        }

        private bool SaveConfigAs()
        {
            var saveFileDialog = new SaveFileDialog();
            saveFileDialog.InitialDirectory = FileInitialDirectory;
            saveFileDialog.CheckPathExists = true;
            saveFileDialog.Filter = FileFilter;
            saveFileDialog.OverwritePrompt = true;
            saveFileDialog.AddExtension = true;
            if (saveFileDialog.ShowDialog(Application.Current.MainWindow) == true &&
                !string.IsNullOrEmpty(saveFileDialog.FileName))
            {
                if (SaveConfigCore(saveFileDialog.FileName))
                {
                    FilePath = saveFileDialog.FileName;
                    return true;
                }
            }
            return false;
        }

        protected virtual bool SaveConfigCore(string fileName_)
        {
            using (var stringWriter = new Utf8StringWriter())
            {
                GetSerializer(ConfigurationObj.GetType()).Serialize(stringWriter, ConfigurationObj);
                try
                {
                    File.WriteAllText(fileName_, stringWriter.ToString());

                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return false;
                }
                IsDirty = false;
                return true;
            }
        }

        class Utf8StringWriter : StringWriter
        {
            public override Encoding Encoding
            {
                get { return Encoding.UTF8; }
            }
        }
    }

    public class SearchResultItemSelectedEventArgs:EventArgs
    {
        public SearchResultItemSelectedEventArgs(SearchResultItem item_)
        {
            this.Item = item_;
        }

        public SearchResultItem Item { get; private set; }
    }
}
