﻿using System;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Input; 
using MorganStanley.Desktop.Loader.Configuration;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;

namespace MSDesktop.LoaderConfigEditor.ViewModels
{
    public sealed class ConfigViewModel : ModifiableConfigViewModelBase
    { 
        private readonly ConfigsViewModel parent;
        public ConfigViewModel(ConfigsViewModel parent_, string filePath_):base(typeof(MSDesktopConfiguration), filePath_)
        {
            parent = parent_;
            CloseOtherConfigsCommand = new DelegateCommand(_ =>
            {
                for (int i = parent.Configs.Count - 1; i >= 0; i--)
                {
                    if (parent.Configs[i] != this) parent.Configs[i].CloseConfigCommand.Execute(null);
                }
            }, o_ => parent.Configs.Count > 1);        
        }

        public ConfigViewModel(ConfigsViewModel parent_):base(new MSDesktopConfiguration())
        {
            parent = parent_;
            CloseOtherConfigsCommand = new DelegateCommand(_ =>
            {
                for (int i = parent.Configs.Count - 1; i >= 0; i--)
                {
                    if (parent.Configs[i] != this) parent.Configs[i].CloseConfigCommand.Execute(null);
                }
            }, o_ => parent.Configs.Count > 1);
        }
         
        public ICommand CloseOtherConfigsCommand { get; private set; } 
        public override bool Close()
        {
            bool closed = base.Close();
            if (closed)
            {
                parent.Configs.Remove(this);
            }
            return closed;
        }

        protected override string FileFilter
        {
            get { return string.Format("MSDesktop config files (*{0})|*{0}", MSDesktopConfiguration.MSDESKTOP_CONFIG_SUFFIX); }
        }
         
    }
     
}
