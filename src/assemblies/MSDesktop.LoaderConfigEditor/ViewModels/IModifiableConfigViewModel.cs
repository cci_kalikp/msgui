﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSDesktop.LoaderConfigEditor.ViewModels
{
    public interface IModifiableConfigViewModel
    {
        bool IsDirty { get; set; }
        string FilePath { get; }
    }
}
