﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using MSDesktop.LoaderConfigEditor.Helpers;
using MorganStanley.MSDotNet.MSGui.Controls.SearchBox;

namespace MSDesktop.LoaderConfigEditor.ViewModels
{
    public class SearchBoxViewModel: SearchBoxViewModelBase
    {
        private readonly ConfigurationTypeDescriptor configuration;
        public SearchBoxViewModel(ConfigurationTypeDescriptor configuration_)
        {
            this.configuration = configuration_;
            this.WatermarkText = "Search Properties";
        }

        public override bool EnableTypeaheadDropDown
        {
            get { return true; } 
        }

        protected override void  UpdateSearchResultCore(CancellationToken ct_) 
        {
            if (string.IsNullOrWhiteSpace(SearchText))
            {
                SearchResultItems.Clear();
                SelectedItem = null;
                ShowDropDown = false;
                return;
            }
            if (!AsyncSearch && !string.IsNullOrEmpty(lastResultSearchText) &&
                SearchResultItems.Count == 0 &&
                SearchText.ToLower().Contains(lastResultSearchText.ToLower()))
            {
                return;
            }
            //var items = configuration.GetRelatedProperties(SearchText, string.Empty);

            var items = configuration.PreHandlingGetRelatedProperties(SearchText, ct_);
            ShowDropDown = items.Count > 0;
            this.SearchResultItems = items;
            base.OnPropertyChanged("SearchResultItems");
        }
    }
}
