﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Microsoft.Win32;
using MorganStanley.Desktop.Loader.Configuration;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;

namespace MSDesktop.LoaderConfigEditor.ViewModels
{
    public class ConfigsViewModel : ViewModelBase
    {
 
        public  ConfigsViewModel()
        {
            NewConfigCommand = new DelegateCommand(_ => NewConfig());
            LoadConfigCommand = new DelegateCommand(LoadConfig);
            SaveCurrentConfigCommand = new DelegateCommand(_ =>
                {
                    if (CurrentConfig == null) return;
                    CurrentConfig.SaveConfigCommand.Execute(null);
                }, o_ => CurrentConfig != null && CurrentConfig.IsDirty);
            SaveCurrentConfigAsCommand = new DelegateCommand(_ =>
            {
                if (CurrentConfig == null) return;
                CurrentConfig.SaveConfigAsCommand.Execute(null);
            }, o_ => CurrentConfig != null && CurrentConfig.IsDirty);
            LoadConfigsCommand = new DelegateCommand(LoadConfigs);
            SaveConfigsCommand = new DelegateCommand(_ =>
                {
                    foreach (var config in Configs)
                    {
                        config.SaveConfigCommand.Execute(null);
                    }
                }, o_=> Configs.Count(c_=>c_.IsDirty) > 0);
            CloseCurrentConfigCommand = new DelegateCommand(_ =>
            {
                if (CurrentConfig == null) return;
                CurrentConfig.CloseConfigCommand.Execute(null);
            }, o_ => CurrentConfig != null);
            CloseConfigsCommand = new DelegateCommand(_ =>
                {
                    for (int i = Configs.Count - 1; i >= 0; i--)
                    {
                        Configs[i].CloseConfigCommand.Execute(null);
                    }
                }, o_=> Configs.Count > 0);
            Configs = new ObservableCollection<ConfigViewModel>();
            Configs.CollectionChanged += Configs_CollectionChanged;
        }


        public ObservableCollection<ConfigViewModel> Configs { get; private set; }

        private ConfigViewModel currentConfig;
        public ConfigViewModel CurrentConfig
        {
            get
            {
                return currentConfig;
            }
            set
            {
                currentConfig = value;
                OnPropertyChanged("CurrentConfig");
                
            }
        }
        public ICommand NewConfigCommand { get; private set; }
        public ICommand LoadConfigCommand { get; private set; }
        public ICommand SaveCurrentConfigCommand { get; private set; }
        public ICommand SaveCurrentConfigAsCommand { get; private set; }
        public ICommand CloseCurrentConfigCommand { get; private set; }
        public ICommand CloseConfigsCommand { get; private set; }
        public ICommand SaveConfigsCommand { get; private set; }
        public ICommand LoadConfigsCommand { get; private set; }
        private int newCount = 0;
        private void NewConfig()
        {
            var config = new ConfigViewModel(this);
            config.ConfigName = "New config "+ ++newCount; 
            this.Configs.Add(config); 
        }

 
        private void LoadConfig(object parameters)
        {
            string file = parameters as string;
            if (string.IsNullOrEmpty(file))
            {
                var openFileDialog = new OpenFileDialog();
                string filter = string.Format("MSDesktop config files (*{0})|*{0}", MSDesktopConfiguration.MSDESKTOP_CONFIG_SUFFIX);
                openFileDialog.Filter = filter;
                openFileDialog.Multiselect = true;
                if (openFileDialog.ShowDialog(Application.Current.MainWindow) == true)
                {
                    var fileNames = openFileDialog.FileNames;
                    if (fileNames != null)
                    {
                        foreach (var fileName in fileNames)
                        {
                            LoadConfig(fileName);
                        }
                    }
                    return;
                }
            }
            if (string.IsNullOrEmpty(file) || !file.EndsWith(MSDesktopConfiguration.MSDESKTOP_CONFIG_SUFFIX)) return;
            ConfigViewModel config = Configs.FirstOrDefault(c_ => c_.FilePath == file);
            if (config == null)
            {
                file = PathUtilities.GetAbsolutePath(file);
                if (!File.Exists(file))
                {
                    MessageBox.Show(string.Format("Cannot access \"{0}\"", file), "Error", MessageBoxButton.OK);
                }
                try
                {
                    config = new ConfigViewModel(this, file);
                    if (!config.Loaded) return;
                    this.Configs.Add(config);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            } 
        }


        void Configs_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                if (!Configs.Contains(CurrentConfig))
                {
                    CurrentConfig = Configs.Count == 0 ? null : Configs[Configs.Count - 1];
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Add)
            {
                CurrentConfig = e.NewItems[0] as ConfigViewModel;
            }
        }

         
        private void LoadConfigs(object parameter_)
        {
            if (parameter_ == null) return;
            string[] files = parameter_ as string[];
            if (files == null || files.Length == 0) return;
            foreach (string file in files)
            {
                LoadConfig(file);
            }
        }  
    }
}
