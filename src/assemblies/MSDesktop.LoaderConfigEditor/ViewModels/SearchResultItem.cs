﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MSDesktop.LoaderConfigEditor.Helpers;
using MorganStanley.Desktop.Loader.Configuration.Helpers;
using MorganStanley.MSDotNet.MSGui.Controls.SearchBox;

namespace MSDesktop.LoaderConfigEditor.ViewModels
{
    public class SearchResultItem:SearchResultItemBase
    {
        public override string ItemPath
        {
            get { return itemPath; }
        }

        public string[] PropertyPaths { get; private set; }
         
        private readonly string itemPath;
        public SearchResultItem(string propertyPrefix_, ConfigurationItemPropertyDescriptor property_, MatchType matchType_, string searchText_)
        { 
            MatchType = matchType_.ToString() + ":";
            this.SearchText = searchText_;
            itemPath = property_.FullName;
            if (!string.IsNullOrEmpty(propertyPrefix_))
            {
                itemPath = propertyPrefix_ + itemPath;
            }
            PropertyPaths = itemPath.Split(new[] { "[]." }, StringSplitOptions.RemoveEmptyEntries);
            if (matchType_ == ViewModels.MatchType.Description)
            {
                string description = property_.Description;
                int index = description.IndexOf(searchText_, StringComparison.InvariantCultureIgnoreCase);
                if (index >= 0)
                { 
                    int start = 0;
                    for (int i = index; i >= 0; i--)
                    {
                        if (description[i] == ' ' || 
                            description[i] == '\r' || 
                            description[i] == '\n')
                        {
                            start = i;
                            break;
                        }
                    }
                    int end = description.Length;
                    for (int i = index + searchText_.Length; i < description.Length; i++)
                    {
                        if (description[i] == ' ' ||
                            description[i] == '\r' ||
                            description[i] == '\n' || 
                            description[i] == '.')
                        {
                            end = i;
                            break;
                        }
                    }
                    itemPath += " (..." + description.Substring(start, end - start).Trim('\r', '\n') + "...)";
                }
            }
        }

        public SearchResultItem(Property propertyDef_, MatchType matchType_, string searchText_)
        {
            MatchType = matchType_.ToString() + ":";
            this.SearchText = searchText_;
            itemPath = propertyDef_.Name; 
            PropertyPaths = itemPath.Split(new[] { "[]." }, StringSplitOptions.RemoveEmptyEntries);
            if (matchType_ == ViewModels.MatchType.Description)
            {
                string description = propertyDef_.Value;
                int index = description.IndexOf(searchText_, StringComparison.InvariantCultureIgnoreCase);
                if (index >= 0)
                {
                    int start = 0;
                    for (int i = index; i >= 0; i--)
                    {
                        if (description[i] == ' ' ||
                            description[i] == '\r' ||
                            description[i] == '\n')
                        {
                            start = i;
                            break;
                        }
                    }
                    int end = description.Length;
                    for (int i = index + searchText_.Length; i < description.Length; i++)
                    {
                        if (description[i] == ' ' ||
                            description[i] == '\r' ||
                            description[i] == '\n' ||
                            description[i] == '.')
                        {
                            end = i;
                            break;
                        }
                    }
                    itemPath += " (..." + description.Substring(start, end - start).Trim('\r', '\n') + "...)";
                }
            }
        }
    }

    public enum MatchType
    {
        Name,
        Description
    }
}
