﻿using System;
using MSDesktop.LoaderConfigEditor.Editors; 
using MorganStanley.Desktop.Loader.Configuration.Designer;

namespace MSDesktop.LoaderConfigEditor.ViewModels
{
    public class SubConfigViewModel : ModifiableConfigViewModelBase
    {
        private readonly string currentDirectory;
        public SubConfigViewModel(Type subConfigType_, string filePath_, string currentDirectory_):base(subConfigType_, filePath_)
        {
            currentDirectory = currentDirectory_;
        }

        public SubConfigViewModel(Type subConfigType_, string currentDirectory_)
            : base(Activator.CreateInstance(subConfigType_))
        {
            currentDirectory = currentDirectory_;
        }


        protected override string FileFilter
        {
            get { return EditorUtilities.GetFilter(PathType.XmlFile); }
        }

        protected override string FileInitialDirectory
        {
            get
            {
                string path = base.FileInitialDirectory;
                if (!string.IsNullOrEmpty(path)) return path;
                return currentDirectory;
            }
        }

    }
}
