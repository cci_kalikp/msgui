﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using MSDesktop.LoaderConfigEditor.Editors; 
using MorganStanley.MSDotNet.MSGui.Core;
using Xceed.Wpf.Toolkit.PropertyGrid;

namespace MSDesktop.LoaderConfigEditor.Views
{
    /// <summary>
    /// Interaction logic for Editor.xaml
    /// </summary>
    public partial class Editor : Window
    {
        public Editor()
        {
            InitializeComponent();
            tabControl.TabItemAdded += tabControl_TabItemAdded; 
        }

        void tabControl_TabItemAdded(object sender, EventArgs<Infragistics.Windows.Controls.TabItemEx> e)
        {
            var content = e.Value1.Content as FrameworkElement; 
            foreach(var grid in content.FindVisualChildren<PropertyGrid>())
            {
                EditorUtilities.FixPropertyGrid(grid);
            }
        }
         
    }
}
