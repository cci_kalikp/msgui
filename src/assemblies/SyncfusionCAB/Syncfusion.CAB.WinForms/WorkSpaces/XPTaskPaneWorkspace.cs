using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using Microsoft.Practices.CompositeUI;
using Microsoft.Practices.CompositeUI.Utility;
using Microsoft.Practices.CompositeUI.SmartParts;
using Microsoft.Practices.CompositeUI.Services;
using Microsoft.Practices.ObjectBuilder;

using System.Windows.Forms;

using Syncfusion.Windows.Forms.Tools;
using Syncfusion.CAB.WinForms.SmartPartInfos;
using System.Drawing;

namespace Syncfusion.CAB.WinForms.WorkSpaces
{
	/// <summary>
    /// Its a WordXP like control that you can see on the right side of Microsoft Word XP.
    /// </summary>
    [ToolboxBitmap(typeof(XPTaskPaneWorkspace), "ToolboxIcons.XPTaskPaneWorkspace.bmp")]
    public class XPTaskPaneWorkspace : XPTaskPane, IComposableWorkspace<Control, XPTaskPaneSmartPartInfo>
    {
        private Dictionary<Control, XPTaskPage> pages = new Dictionary<Control, XPTaskPage>();
        private WorkspaceComposer<Control, XPTaskPaneSmartPartInfo> composer;
        private bool populatingPages = false;
        private Syncfusion.Windows.Forms.VisualStyle tVisualStyle;

        #region Constructor
        public XPTaskPaneWorkspace()
        {
            composer = new WorkspaceComposer<Control, XPTaskPaneSmartPartInfo>(this);
            tVisualStyle = Syncfusion.Windows.Forms.VisualStyle.Default;
        }
        #endregion

        [ServiceDependency]
        public WorkItem WorkItem
        {
            set
            {
                composer.WorkItem = value;
            }
        }

        #region properties
        /// <summary>
        /// returns a readonly collection of XPTaskPage objects present in the collection
        /// </summary>
        public ReadOnlyDictionary<Control, XPTaskPage> Pages
        {
            get
            {
                return new ReadOnlyDictionary<Control, XPTaskPage>(pages);
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// sets the XPTaskPage properties based on the SmartPartInfo provided
        /// </summary>
        /// <param name="page"></param>
        /// <param name="smartPartInfo"></param>
        private void SetTaskPageProperties(XPTaskPage page, XPTaskPaneSmartPartInfo smartPartInfo)
        {
            if (smartPartInfo.TaskPagePosition == TaskPagePosition.Beginning)
            {
                XPTaskPage[] taskPages = GetTaskPages();
                this.TaskPanePageContainer.Controls.Clear();
                this.TaskPanePageContainer.Controls.Add(page);
                this.TaskPanePageContainer.Controls.AddRange(taskPages);
                this.SelectedPage = page;
                if (smartPartInfo.VisualStyle.HasValue)
                {
                    this.VisualStyle = smartPartInfo.VisualStyle.Value;
                    tVisualStyle = smartPartInfo.VisualStyle.Value;
                }
                else
                    this.VisualStyle = tVisualStyle;
                page.Title = smartPartInfo.Title;
            }
            else
            {
                this.TaskPanePageContainer.Controls.Add(page);
                page.Title = smartPartInfo.Title;
            }
        }

        /// <summary>
        /// returns a TaskPage if found/creates a new instance of taskpage and adds it to the inner list
        /// </summary>
        /// <param name="smartPart"></param>
        /// <returns></returns>
        private XPTaskPage GetOrCreateTabPage(Control smartPart)
        {
            XPTaskPage page = null;
            Control current = smartPart;

            // If the XPTaskPage was added with the control at design-time, it will have a parent control, 
            // and somewhere up its containment chain we'll find one of our tabs.
            while (current != null && page == null)
            {
                current = current.Parent;
                page = current as XPTaskPage;
            }

            if (page == null)
            {
                page = new XPTaskPage();
                page.Controls.Add(smartPart);
                smartPart.Dock = DockStyle.Fill;
                page.Name = Guid.NewGuid().ToString();

                pages.Add(smartPart, page);
            }
            else if (!pages.ContainsKey(smartPart))
            {
                pages.Add(smartPart, page);
            }

            return page;
        }


        /// <summary>
        /// Populates pages for refreshing controls present in the inner list
        /// </summary>
        private void PopulatePages()
        {
            if (!populatingPages && pages.Count != this.TaskPanePageContainer.Controls.Count)
            {
                if (this.Controls.Count > 0 && this.Controls.Count != this.TaskPages.Length)
                {
                    foreach (XPTaskPage page in this.TaskPages)
                    {
                        Control control = GetControlFromPage(page);
                        if (control != null && !composer.SmartParts.Contains(control))
                        {
                            XPTaskPaneSmartPartInfo smartPartInfo = new XPTaskPaneSmartPartInfo(page.Name);
                            try
                            {
                                Show(control, smartPartInfo);
                            }
                            finally
                            {
                                populatingPages = false;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// returns the Control from the XPTaskPage object
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        private Control GetControlFromPage(XPTaskPage page)
        {
            Control control = null;
            if (page.Controls.Count > 0)
            {
                control = page.Controls[0];
            }
            return control;
        }

        /// <summary>
        /// Disposes the Controls once the SmartPart gets closed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ControlDisposed(object sender, EventArgs e)
        {
            Control control = sender as Control;
            if (control != null && this.pages.ContainsKey(control))
                composer.ForceClose(control);
        }

        /// <summary>
        /// returns the index of the SmartPart from the TaskPages Collection
        /// </summary>
        /// <param name="smartPart"></param>
        /// <returns></returns>
        private int GetTaskPageIndex(Control smartPart)
        {
            int key = -1;
            IEnumerator pageEnum = TaskPages.GetEnumerator();
            while (pageEnum.MoveNext())
            {
                if (((XPTaskPage)pageEnum).Name == pages[smartPart].Name)
                    break;
                key++;
            }
            return key;
        }

        /// <summary>
        /// returns the TaskPages collection of objects
        /// </summary>
        /// <returns></returns>
        private XPTaskPage[] GetTaskPages()
        {
            List<XPTaskPage> TaskPages = new List<XPTaskPage>();
            IEnumerator pagesEnum = this.TaskPanePageContainer.Controls.GetEnumerator();
            while (pagesEnum.MoveNext())
            {
                TaskPages.Add(pagesEnum.Current as XPTaskPage);
            }
            return TaskPages.ToArray();
        }

        #endregion

        #region internal methods
        /// <summary>
        /// Populates the Pages if it is added in the design time
        /// </summary>
        protected override void OnCreateControl()
        {
            base.OnCreateControl();
            PopulatePages();
        }

        #endregion

        #region Protected Virtual Implementation

        /// <summary>
        /// Activates the corresponding SmartPart
        /// </summary>
        /// <param name="smartPart"></param>
        protected virtual void OnActivate(Control smartPart)
        {
            PopulatePages();

            int key = GetTaskPageIndex(smartPart);
            this.SelectedPage = this.TaskPages[key];
            TaskPages[key].Show();
        }

        /// <summary>
        /// Applies the SmartPartInfo to the corresponding SmartPart
        /// </summary>
        /// <param name="smartPart"></param>
        /// <param name="smartPartInfo"></param>
        protected virtual void OnApplySmartPartInfo(Control smartPart, XPTaskPaneSmartPartInfo smartPartInfo)
        {
            PopulatePages();
            int key = GetTaskPageIndex(smartPart);
            SetTaskPageProperties(TaskPages[key], smartPartInfo);
        }

        /// <summary>
        /// Closes the SmartPart
        /// </summary>
        /// <param name="smartPart"></param>
        protected virtual void OnClose(Control smartPart)
        {
            PopulatePages();
            this.TaskPanePageContainer.Controls.Remove(smartPart);
            pages.Remove(smartPart);
            smartPart.Disposed -= new EventHandler(ControlDisposed);
        }

        /// <summary>
        /// Shows the SmartPart with the SmartPartInfo applied to it
        /// </summary>
        /// <param name="smartPart"></param>
        /// <param name="smartPartInfo"></param>
        protected virtual void OnShow(Control smartPart, XPTaskPaneSmartPartInfo smartPartInfo)
        {
            PopulatePages();

            XPTaskPage page = GetOrCreateTabPage(smartPart);
            SetTaskPageProperties(page, smartPartInfo);
            page.Dock = DockStyle.Fill;
            smartPart.Disposed += ControlDisposed;
        }

        /// <summary>
        /// Raises the SmartPartActivated Event
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnSmartPartActivated(WorkspaceEventArgs e)
        {
            if (SmartPartActivated != null)
            {
                SmartPartActivated(this, e);
            }
        }

        /// <summary>
        /// Raises the SmartPartClosing Event
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnSmartPartClosing(WorkspaceCancelEventArgs e)
        {
            if (SmartPartClosing != null)
                SmartPartClosing(this, e);
        }

        /// <summary>
        /// returns the XPTaskPaneSmartPartInfo object
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        protected virtual XPTaskPaneSmartPartInfo OnConvertFrom(ISmartPartInfo source)
        {
            return SmartPartInfo.ConvertTo<XPTaskPaneSmartPartInfo>(source);
        }

        /// <summary>
        /// Hides the SmartPart
        /// </summary>
        /// <param name="smartPart"></param>
        protected virtual void OnHide(Control smartPart)
        {
            PopulatePages();
            int key = GetTaskPageIndex(smartPart);
            this.TaskPages[key].Hide();
        }

        #endregion

        #region IComposableWorkspace<Control,SmartPartInfo> Members

        XPTaskPaneSmartPartInfo IComposableWorkspace<Control, XPTaskPaneSmartPartInfo>.ConvertFrom(ISmartPartInfo source)
        {
            return OnConvertFrom(source);
        }

        void IComposableWorkspace<Control, XPTaskPaneSmartPartInfo>.OnActivate(Control smartPart)
        {
            OnActivate(smartPart);
        }

        void IComposableWorkspace<Control, XPTaskPaneSmartPartInfo>.OnApplySmartPartInfo(Control smartPart, XPTaskPaneSmartPartInfo smartPartInfo)
        {
            OnApplySmartPartInfo(smartPart, smartPartInfo);
        }

        void IComposableWorkspace<Control, XPTaskPaneSmartPartInfo>.OnClose(Control smartPart)
        {
            OnClose(smartPart);
        }

        void IComposableWorkspace<Control, XPTaskPaneSmartPartInfo>.OnHide(Control smartPart)
        {
            OnHide(smartPart);
        }

        void IComposableWorkspace<Control, XPTaskPaneSmartPartInfo>.OnShow(Control smartPart, XPTaskPaneSmartPartInfo smartPartInfo)
        {
            OnShow(smartPart, smartPartInfo);
        }

        void IComposableWorkspace<Control, XPTaskPaneSmartPartInfo>.RaiseSmartPartActivated(WorkspaceEventArgs e)
        {
            OnSmartPartActivated(e);
        }

        void IComposableWorkspace<Control, XPTaskPaneSmartPartInfo>.RaiseSmartPartClosing(WorkspaceCancelEventArgs e)
        {
            OnSmartPartClosing(e);
        }

        #endregion

        #region IWorkspace Members

        public void Activate(object smartPart)
        {
            composer.Activate(smartPart);
        }

        public object ActiveSmartPart
        {
            get
            {
                return composer.ActiveSmartPart;
            }
        }

        public void ApplySmartPartInfo(object smartPart, ISmartPartInfo smartPartInfo)
        {
            composer.ApplySmartPartInfo(smartPart, smartPartInfo);
        }

        public void Close(object smartPart)
        {
            composer.Close(smartPart);
        }

        public void Hide(object smartPart)
        {
            composer.Hide(smartPart);
        }

        public void Show(object smartPart)
        {
            if (smartPart != null)
                composer.Show(smartPart);
        }

        public void Show(object smartPart, ISmartPartInfo smartPartInfo)
        {
            if (smartPart != null)
            {
                composer.Show(smartPart, smartPartInfo);
            }
        }

        public event EventHandler<WorkspaceEventArgs> SmartPartActivated;

        public event EventHandler<WorkspaceCancelEventArgs> SmartPartClosing;

        System.Collections.ObjectModel.ReadOnlyCollection<object> IWorkspace.SmartParts
        {
            get
            {
                return composer.SmartParts;
            }
        }

        #endregion

    }
}
