using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.ObjectModel;

using Syncfusion.Windows.Forms.Tools;
using Syncfusion.CAB.WinForms.SmartPartInfos;

using Microsoft.Practices.CompositeUI;
using Microsoft.Practices.CompositeUI.SmartParts;
using Microsoft.Practices.CompositeUI.Utility;


namespace Syncfusion.CAB.WinForms.WorkSpaces
{
	[ToolboxBitmap(typeof(SplashPanelWorkspace), "ToolboxIcons.SplashPanelWorkspace.bmp")]
    public class SplashPanelWorkspace : Panel, IComposableWorkspace<Control, SplashPanelSmartPartInfo>
    {
        private Dictionary<String,Control> controls = new Dictionary<string,Control>();
        private WorkspaceComposer<Control, SplashPanelSmartPartInfo> composer;

        public SplashPanelWorkspace()
        {
            composer = new WorkspaceComposer<Control, SplashPanelSmartPartInfo>(this);
        }

        [ServiceDependency]
        public WorkItem WorkItem
        {
            set
            {
                composer.WorkItem = value;
            }
        }


        #region Private
		/// <summary>
		/// EventHandler for disposing the control
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
        private void ControlDisposed(object sender, EventArgs e)
        {
            Control control = sender as Control;
            if (control != null)
                composer.ForceClose(control);
        }


        /// <summary>
        /// Activates the TopMost Control
        /// </summary>
        private void ActivateTopmost()
        {
            if (this.Controls.Count != 0)
            {
                Activate(this.Controls[0]);
            }
        }

        /// <summary>
        /// Gets the SplashPanel object for the corresponding smartpart
        /// </summary>
        /// <param name="smartPart">Control</param>
        /// <returns>SplashPanel</returns>
        private SplashPanel GetSplashPanel(Control smartPart)
        {
            SplashPanel sp = null;
            if (this.controls.ContainsKey(smartPart.Name))
            {
                sp = smartPart as SplashPanel;
            }
            return sp;
        }

        /// <summary>
        /// Creates a new SplashPanel for the Control and adds up in its internal collection list
        /// </summary>
        /// <param name="smartPart"></param>
        /// <param name="smartPartInfo"></param>
        /// <returns></returns>
        private SplashPanel SetSplashPanel(Control smartPart, SplashPanelSmartPartInfo smartPartInfo)
        {
            SplashPanel sp = new SplashPanel();
            smartPart.BackColor = Color.Transparent;
            sp.Controls.Add(smartPart);
            smartPart.Dock = DockStyle.Fill;
            SetSplashPanelProperties(sp, smartPartInfo);
            return sp;
        }

        /// <summary>
        /// Sets the SplashPanelProperties for the smartpart
        /// </summary>
        /// <param name="smarPart"></param>
        /// <param name="smartPartInfo"></param>
        private void SetSplashPanelProperties(SplashPanel smarPart,SplashPanelSmartPartInfo smartPartInfo)
        {
            smarPart.BackgroundColor = smartPartInfo.BackgroundColor;
            smarPart.ShowAnimation = smartPartInfo.ShowAnimantion;
            smarPart.ShowInTaskbar = smarPart.ShowInTaskbar;
            smarPart.Text = smartPartInfo.Title;
            smarPart.TimerInterval = smartPartInfo.TimerInterval;
            smarPart.AnimationSpeed = smartPartInfo.AnimationSpeed;
            smarPart.ShowAsTopMost = true;
            smarPart.SuspendAutoCloseWhenMouseOver = false;
            if (smarPart.DesktopAlignment != SplashAlignment.Custom)
                smarPart.DesktopAlignment = smartPartInfo.DesktopAlignment;
            smarPart.SlideStyle = smartPartInfo.SlideStyle;
        }
 
	    #endregion

        #region Protected Members
        /// <summary>
        /// Activates the corresponding SplashPanel for the given control
        /// </summary>
        /// <param name="smartPart"></param>
        protected virtual void OnActivate(Control smartPart)
        {
            SplashPanel sp = GetSplashPanel(smartPart);
            if (sp != null)
            {
                sp.BringToFront();
                sp.ShowSplash();
            }
        }

        /// <summary>
        /// Applies the SmartPartInfo to the corresponding control
        /// </summary>
        /// <param name="smartPart"></param>
        /// <param name="smartPartInfo"></param>
        protected virtual void OnApplySmartPartInfo(Control smartPart, SplashPanelSmartPartInfo smartPartInfo)
        {
            SplashPanel sp = GetSplashPanel(smartPart);
            if (sp != null)
            {
                SetSplashPanelProperties(sp, smartPartInfo);
            }
        }

        /// <summary>
        /// Closes the correponding SplashPanel
        /// </summary>
        /// <param name="smartPart"></param>
        protected virtual void OnClose(Control smartPart)
        {
            SplashPanel sp = GetSplashPanel(smartPart);
            if (sp != null)
            {
                this.Controls.Remove(sp);
                this.controls.Remove(smartPart.Name);
                sp.Disposed -= ControlDisposed;
                ActivateTopmost();
            }
        }

        /// <summary>
        /// Hides the corresponding smartpart and shows the topmost control present in the list
        /// </summary>
        /// <param name="smartPart"></param>
        protected virtual void OnHide(Control smartPart)
        {
            SplashPanel sp = GetSplashPanel(smartPart);
            if (sp != null)
            {
                sp.SendToBack();
                ActivateTopmost();
            }
        }

        /// <summary>
        /// Shows the smartpart after applying the smartpartinfo, if the smartpart is already present use the Activate method to show it.
        /// </summary>
        /// <param name="smartPart">smartPart</param>
        /// <param name="smartPartInfo">smartPartInfo</param>
        protected virtual void OnShow(Control smartPart, SplashPanelSmartPartInfo smartPartInfo)
        {
            if (!this.controls.ContainsKey(smartPart.Name))
            {
                SplashPanel sp = SetSplashPanel(smartPart, smartPartInfo);
                sp.Size = this.Size;
                this.Controls.Add(sp);
                sp.BringToFront();

                if (!smartPartInfo.IsModal && smartPartInfo.DesktopAlignment != SplashAlignment.Custom)
                    sp.ShowSplash();
                else if (!smartPartInfo.IsModal && smartPartInfo.DesktopAlignment == SplashAlignment.Custom)
                {
                    sp.ShowSplash(smartPartInfo.CustomPoint, this.Parent as Form, smartPartInfo.IsModal);
                }
                else if (smartPartInfo.IsModal)
                    sp.ShowSplash(new Point(), this.Parent as Form, smartPartInfo.IsModal);
                sp.Disposed += new EventHandler(ControlDisposed);
                this.controls.Add(smartPart.Name, smartPart);
            }
            else
                Activate(smartPart);
        }

        /// <summary>
        /// Raises the SmartPartClosing eventhandler
        /// </summary>
        /// <param name="e"></param>
        protected void OnSmartPartClosing(WorkspaceCancelEventArgs e)
        {
            if (SmartPartClosing != null)
            {
                SmartPartClosing(this, e);
            }
        }

        /// <summary>
        /// Raises the SmartPartActivated eventhandler
        /// </summary>
        /// <param name="e"></param>
        protected void OnSmartPartActivated(WorkspaceEventArgs e)
        {
            if (SmartPartActivated != null)
            {
                SmartPartActivated(this, e);
            }
        }
        
        #endregion

        #region IComposableWorkspace<Control,SplashPanelSmartPartInfo> Members

        SplashPanelSmartPartInfo IComposableWorkspace<Control, SplashPanelSmartPartInfo>.ConvertFrom(ISmartPartInfo source)
        {
            return SplashPanelSmartPartInfo.ConvertTo<SplashPanelSmartPartInfo>(source);
        }

        void IComposableWorkspace<Control, SplashPanelSmartPartInfo>.OnActivate(Control smartPart)
        {
            OnActivate(smartPart);
        }

        void IComposableWorkspace<Control, SplashPanelSmartPartInfo>.OnApplySmartPartInfo(Control smartPart, SplashPanelSmartPartInfo smartPartInfo)
        {
            OnApplySmartPartInfo(smartPart, smartPartInfo);
        }

        void IComposableWorkspace<Control, SplashPanelSmartPartInfo>.OnClose(Control smartPart)
        {
            OnClose(smartPart);
        }

        void IComposableWorkspace<Control, SplashPanelSmartPartInfo>.OnHide(Control smartPart)
        {
            OnHide(smartPart);
        }

        void IComposableWorkspace<Control, SplashPanelSmartPartInfo>.OnShow(Control smartPart, SplashPanelSmartPartInfo smartPartInfo)
        {
            OnShow(smartPart, smartPartInfo);
        }

        void IComposableWorkspace<Control, SplashPanelSmartPartInfo>.RaiseSmartPartActivated(WorkspaceEventArgs e)
        {
            OnSmartPartActivated(e);
        }

        void IComposableWorkspace<Control, SplashPanelSmartPartInfo>.RaiseSmartPartClosing(WorkspaceCancelEventArgs e)
        {
            OnSmartPartClosing(e);
        }

        #endregion

        #region IWorkspace Members

        public void Activate(object smartPart)
        {
            composer.Activate(smartPart);
        }

        public object ActiveSmartPart
        {
            get 
            {
                return composer.ActiveSmartPart;
            }
        }

        public void ApplySmartPartInfo(object smartPart, ISmartPartInfo smartPartInfo)
        {
            composer.ApplySmartPartInfo(smartPart, smartPartInfo);
        }

        public void Close(object smartPart)
        {
            composer.Close(smartPart);
        }

        public void Hide(object smartPart)
        {
            composer.Hide(smartPart);
        }

        public void Show(object smartPart)
        {
            composer.Show(smartPart);
        }

        public void Show(object smartPart, ISmartPartInfo smartPartInfo)
        {
            composer.Show(smartPart, smartPartInfo);
        }

        public event EventHandler<WorkspaceEventArgs> SmartPartActivated;

        public event EventHandler<WorkspaceCancelEventArgs> SmartPartClosing;

        ReadOnlyCollection<object> IWorkspace.SmartParts
        {
            get 
            {
                return composer.SmartParts;
            }
        }

        #endregion
    }
}
