using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Collections;

using Microsoft.Practices.CompositeUI.SmartParts;
using Microsoft.Practices.CompositeUI.Utility;
using Microsoft.Practices.CompositeUI;
using Microsoft.Practices.CompositeUI.WinForms;

using Syncfusion.Windows.Forms.Tools;
using Syncfusion.CAB.WinForms.SmartPartInfos;


namespace Syncfusion.CAB.WinForms.WorkSpaces
{
    /// <summary>
    /// A WorkSpace that displays smart parts within the Docking Manager.
    /// </summary>
    public partial class DockableWorkSpace : Workspace<Control, DockingSmartPartInfo>
    {
        #region Fields

        /// <summary>
        /// Holds a reference to all Docked windows.
        /// </summary>
        private Dictionary<string, Control> windows = new Dictionary<string, Control>();

        /// <summary>
        /// Used to prevent infinite loops when initially creating windows
        /// </summary>
        private bool populatingWindows = false;

        /// <summary>
        /// True if a SmartPart should be activated when its Docked window is activated
        /// </summary>
        private bool setActiveForSmartPart = true;

        /// <summary>
        /// True if design-time dockable windows have been added to the colleciton
        /// </summary>
        private bool initialized = false;

        /// <summary>
        /// The Syncfusion DockingManager responsible for managing docking windows.
        /// </summary>
        private Syncfusion.Windows.Forms.Tools.DockingManager dockManager = null;

        /// <summary>
        /// Temproary storage objects
        /// </summary>
        private Syncfusion.Windows.Forms.VisualStyle tVisualStyle;
        private Syncfusion.Windows.Forms.Office2007Theme tOffice2007Theme;
        private bool dragCancel;
        private DockTabAlignmentStyle tDockTabAlignmentStyle;
        private DockLabelAlignmentStyle tDockLabelAlignmentStyle;
        private bool tFreezeResize;
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="T:DockableWorkSpace"/> class.
        /// </summary>
        /// <param name="dockManager">The dock manager.</param>
        /// <param name="owner">The owner.</param>
        public DockableWorkSpace(Syncfusion.Windows.Forms.Tools.DockingManager dockManager, ContainerControl owner)
        {
            LoadUpParameters(dockManager, owner);
        }

        private void LoadUpParameters(Syncfusion.Windows.Forms.Tools.DockingManager dockManager, ContainerControl owner)
        {
            this.dockManager = dockManager;
            this.dockManager.HostControl = owner;
            this.dockManager.Disposed += new EventHandler(dockManager_Disposed);
            WireUpWindow();
            SetDefaults();
        }

        private void SetDefaults()
        {
            tVisualStyle = Syncfusion.Windows.Forms.VisualStyle.Default;
            tOffice2007Theme = Syncfusion.Windows.Forms.Office2007Theme.Blue;
            tDockTabAlignmentStyle = DockTabAlignmentStyle.Left;
            tDockLabelAlignmentStyle = DockLabelAlignmentStyle.Default;
            tFreezeResize = false;
        }

        #endregion // Constructors

        #region DockingManager Events

        /// <summary>
        /// Handles the Disposed event of the dockManager control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        void dockManager_Disposed(object sender, EventArgs e)
        {
            foreach (string s in this.windows.Keys)
            {
                if (this.InnerSmartParts.Contains(this.windows[s]))
                    this.InnerSmartParts.Remove(this.windows[s]);
                if (this.dockManager.GetEnableDocking(this.windows[s]))
                    this.dockManager.SetEnableDocking(this.windows[s], false);
                this.windows[s].Disposed -= ControlDisposed;
                this.windows[s].Dispose();
            }
            this.windows.Clear();
        }

        /// <summary>
        /// Occurs when a dockable control gets activated.
        /// </summary>
        /// <param name="sender">The docking manager.</param>
        /// <param name="arg">The <see cref="T:Syncfusion.Windows.Forms.Tools.DockActivationChangedEventArgs"/> instance containing the event data.</param>
        /// <remarks>
        /// When DockControlActivated event is triggered, corresponding SmartPart is activated in the form.  
        /// </remarks>
        void dockManager_DockControlActivated(object sender, DockActivationChangedEventArgs arg)
        {
            Control smartPart = arg.Control;
            if (smartPart != null)
            {
                if (this.dockManager.GetEnableDocking(smartPart))
                {
                    //smartPart = dockManager.GetEnableDocking(smartPart) ? null : smartPart;
                    if (smartPart != null && this.setActiveForSmartPart && this.ActiveSmartPart != smartPart)
                    {
                        Activate(smartPart);
                    }
                }
            }
        }

        /// <summary>
        /// Occurs when a control's DockVisibility state is changing.
        /// </summary>
        /// <param name="sender">The DockingManager.</param>
        /// <param name="arg">The <see cref="T:Syncfusion.Windows.Forms.Tools.DockVisibilityChangingEventArgs"/> instance containing the event data.</param>
        /// <remarks>
        /// When DockVisibilityChanging event is triggered, corresponding SmartPart gets hidden.
        /// </remarks>
        void dockManager_DockVisibilityChanging(object sender, DockVisibilityChangingEventArgs arg)
        {
            Control smartPart = arg.Control;
            if (smartPart != null)
            {
                WorkspaceCancelEventArgs cancelArgs = new WorkspaceCancelEventArgs(smartPart);
                base.RaiseSmartPartClosing(cancelArgs);
                arg.Cancel = cancelArgs.Cancel;

                if (cancelArgs.Cancel == false)
                {
                    smartPart.Hide();
                }
            }
        }

        /// <summary>
        /// Occurs after a control's DockVisibility state has changed.
        /// </summary>
        /// <param name="sender">The DockingManager.</param>
        /// <param name="arg">The <see cref="T:Syncfusion.Windows.Forms.Tools.DockVisibilityChangedEventArgs"/> instance containing the event data.</param>
        /// <remarks>
        /// When DockVisibilityChanged event it triggered, corresponding SmartPart is removed from the docked windows list.
        /// </remarks>
        void dockManager_DockVisibilityChanged(object sender, DockVisibilityChangedEventArgs arg)
        {
            Control smartPart = arg.Control;
            if (!this.dockManager.GetDockVisibility(smartPart))
            {
                if (smartPart != null)
                {
                    RemoveEntry(smartPart);
                    base.InnerSmartParts.Remove(smartPart);
                }
            }
        }
        #endregion

        #region Properties

        /// <summary>
        /// Read-only view of WindowDictionary.
        /// </summary>
        [Browsable(false)]
        public ReadOnlyDictionary<string, Control> Windows
        {
            get { return new ReadOnlyDictionary<string, Control>(this.windows); }
        }

        #endregion

        #region Protected

        /// <summary>
        /// Sets the the docked window properties from the DockingSmartPartInfo class instance.
        /// </summary>
        /// <param name="window">The window.</param>
        /// <param name="smartPartInfo">The smart part info.</param>
        protected void SetWindowProperties(Control window, DockingSmartPartInfo smartPartInfo)
        {
            window.Text = smartPartInfo.Title;
            if (smartPartInfo.DockStyle == DockingStyle.Left || smartPartInfo.DockStyle == DockingStyle.Right)
                this.dockManager.DockControl(window, GetParentControl(smartPartInfo.ParentName), (DockingStyle)smartPartInfo.DockStyle, smartPartInfo.Width > 0 ? smartPartInfo.Width : window.Width);
            else if (smartPartInfo.DockStyle == DockingStyle.Top || smartPartInfo.DockStyle == DockingStyle.Bottom)
                this.dockManager.DockControl(window, GetParentControl(smartPartInfo.ParentName), (DockingStyle)smartPartInfo.DockStyle, smartPartInfo.Height > 0 ? smartPartInfo.Height : window.Height);
            else if (smartPartInfo.DockStyle == DockingStyle.Tabbed)
                if (smartPartInfo.ParentName != null)
                    this.dockManager.DockControl(window, GetParentControl(smartPartInfo.ParentName), (DockingStyle)smartPartInfo.DockStyle, GetSizeOfParent(GetParentControl(smartPartInfo.ParentName)));
                else
                    throw new ArgumentNullException("ParentName");
            dragCancel = smartPartInfo.DragCancel;
            this.dockManager.SetDockLabel(window, smartPartInfo.Title);

            if (tDockLabelAlignmentStyle != smartPartInfo.LabelAlignment)
            {
                this.dockManager.DockLabelAlignment = smartPartInfo.LabelAlignment;
                tDockLabelAlignmentStyle = smartPartInfo.LabelAlignment;
            }
            else
                this.dockManager.DockLabelAlignment = tDockLabelAlignmentStyle;

            if (tDockTabAlignmentStyle != smartPartInfo.DockTabAlignmentStyle)
            {
                this.dockManager.DockTabAlignment = smartPartInfo.DockTabAlignmentStyle;
                tDockTabAlignmentStyle = smartPartInfo.DockTabAlignmentStyle;
            }
            else
                this.dockManager.DockTabAlignment = smartPartInfo.DockTabAlignmentStyle;

            if (tFreezeResize != smartPartInfo.FreezeResizing)
            {
                this.dockManager.FreezeResizing = smartPartInfo.FreezeResizing;
                tFreezeResize = smartPartInfo.FreezeResizing;
            }
            else
                this.dockManager.FreezeResizing = tFreezeResize;

            if (smartPartInfo.AutoHideOnLoad)
            {
                this.dockManager.SetAutoHideMode(window, true);
                this.dockManager.HideAutoHiddenControl(true);
            }
            else
            {
                this.dockManager.SetDockVisibility(window, true);
                this.setActiveForSmartPart = false;
                this.dockManager.ActivateControl(window);
                SetActiveSmartPart(window);
                this.setActiveForSmartPart = true;
            }

            if (smartPartInfo.FloatOnLoad)
            {
                this.dockManager.SetFloatOnly(window, true);
            }

            if (this.dockManager.ImageList != null)
                this.dockManager.SetDockIcon(window, smartPartInfo.ImageIndex);

            AddCaptionButton(smartPartInfo);

            if (smartPartInfo.VisualStyle.HasValue)
            {
                this.dockManager.VisualStyle = smartPartInfo.VisualStyle.Value;
                tVisualStyle = smartPartInfo.VisualStyle.Value;
            }
            else
                this.dockManager.VisualStyle = tVisualStyle;
            if (smartPartInfo.Office2007Theme.HasValue)
            {
                this.dockManager.Office2007Theme = smartPartInfo.Office2007Theme.Value;
                tOffice2007Theme = smartPartInfo.Office2007Theme.Value;
            }
            else
                this.dockManager.Office2007Theme = tOffice2007Theme;
        }

        /// <summary>
        /// Adds/Removes the CaptionButtons from the collection
        /// </summary>
        /// <param name="smartPartInfo"></param>
        private void AddCaptionButton(DockingSmartPartInfo smartPartInfo)
        {
            if (smartPartInfo.CaptionButtonInfo != null && smartPartInfo.CaptionButtonInfo.Length != 0)
            {
                foreach (CaptionButtonInfo captionButtonInfo in smartPartInfo.CaptionButtonInfo)
                {
                    if (captionButtonInfo.UseCaptionButton && captionButtonInfo.CaptionButton != null)
                        this.dockManager.CaptionButtons.Add(captionButtonInfo.CaptionButton);
                    else if (!captionButtonInfo.UseCaptionButton && captionButtonInfo.CaptionButton != null && this.dockManager.CaptionButtons.Contains(captionButtonInfo.CaptionButton))
                        this.dockManager.CaptionButtons.Remove(captionButtonInfo.CaptionButton);
                }
            }
        }

        /// <summary>
        /// Gets the parent control to which the control should be docked.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private Control GetParentControl(string name)
        {
            Control parent = null;
            if (name != null)
            {
                if (this.windows.ContainsKey(name))
                {
                    parent = this.windows[name];
                }
                else
                {
                    parent = this.dockManager.HostControl;
                }
            }
            else
                parent = this.dockManager.HostControl;
            return parent;
        }

        private int GetSizeOfParent(Control parentWindow)
        {
            if (this.dockManager.GetDockStyle(parentWindow) == DockingStyle.Left || this.dockManager.GetDockStyle(parentWindow) == DockingStyle.Right)
                return parentWindow.Width;
            else if (this.dockManager.GetDockStyle(parentWindow) == DockingStyle.Top || this.dockManager.GetDockStyle(parentWindow) == DockingStyle.Bottom)
                return parentWindow.Height;
            return 100;
        }

        /// <summary>
        /// Retrieves an array of Docked windows currently managed by the Syncfusion DockingManager
        /// </summary>
        /// <returns><see cref="Control"/>[]</returns>
        private Control[] GetWindows()
        {
            return this.dockManager.ControlsArray;
        }

        /// <summary>
        /// Creates a Docked window if it does not already exist and adds the given control.
        /// </summary>
        /// <param name="smartPart"></param>
        /// <returns><see cref="Control"/></returns>
        protected Control GetOrCreateWindow(Control smartPart)
        {
            Control window = null;

            if (window == null)
            {
                //this.dockManager.SetDockLabel(smartPart, smartPart.Name);
                window = smartPart;
            }

            if (!this.windows.ContainsKey(smartPart.Name))
                this.windows.Add(smartPart.Name, smartPart);
            this._SelectedIndex = GetIndexFromKey(smartPart.Name);


            return window;
        }

        #endregion

        #region Private

        /// <summary>
        /// Attach events needed to signal the SmartPart when a DockedWindow is closed
        /// </summary>
        /// <param name="window">The window.</param>
        private void WireUpWindow()
        {
            this.dockManager.DockControlActivated += new DockActivationChangedEventHandler(dockManager_DockControlActivated);
            //this.dockManager.DockVisibilityChanged += new DockVisibilityChangedEventHandler(dockManager_DockVisibilityChanged);
            this.dockManager.DockVisibilityChanging += new DockVisibilityChangingEventHandler(dockManager_DockVisibilityChanging);
            this.dockManager.DragAllow += new DragAllowEventHandler(dockManager_DragAllow);
        }

        private void UnwireWindow()
        {
            this.dockManager.DockControlActivated -= new DockActivationChangedEventHandler(dockManager_DockControlActivated);
            //this.dockManager.DockVisibilityChanged -= new DockVisibilityChangedEventHandler(dockManager_DockVisibilityChanged);
            this.dockManager.DockVisibilityChanging -= new DockVisibilityChangingEventHandler(dockManager_DockVisibilityChanging);
            this.dockManager.DragAllow -= new DragAllowEventHandler(dockManager_DragAllow);
        }

        void dockManager_DragAllow(object sender, DragAllowEventArgs arg)
        {
            if (dragCancel)
                arg.Cancel = true;
        }

        /// <summary>
        /// Removes the SmartPart from the Window collection
        /// </summary>
        /// <param name="smartPart">The smart part.</param>
        private void RemoveEntry(Control smartPart)
        {
            this.windows.Remove(smartPart.Name);
        }


        //Raises bugs when controls are added from designer. 
        /*private void PopulateWindows()
        {
            if (!populatingWindows)
            {
                Control[] dockableWindows = this.GetWindows();
                if (this.windows.Count != dockableWindows.Length && dockableWindows != null)
                {
                    foreach (Control smartPart in dockableWindows)
                    {
                        if (this.windows.ContainsValue(smartPart) == false)
                        {
                            //CHANGES
                            //WireUpWindow(smartPart);

                            if (smartPart != null && base.SmartParts.Contains(smartPart) == false)
                            {
                                DockingSmartPartInfo smartPartInfo = new DockingSmartPartInfo();
                                GetWindowPropertiesAndApplyInSmartPart(smartPart, smartPartInfo);

                                // Avoid circular calls to this method.
                                this.populatingWindows = true;
                                try
                                {
                                    Show(smartPart, smartPartInfo);
                                }
                                finally
                                {
                                    this.populatingWindows = false;
                                }
                            }
                        }
                    }
                }
            }
        }*/

        private void GetWindowPropertiesAndApplyInSmartPart(Control smartPart, DockingSmartPartInfo smartPartInfo)
        {
            if (!this.dockManager.GetEnableDocking(smartPart))
                this.dockManager.SetEnableDocking(smartPart, true);
            else
                this.dockManager.SetDockVisibility(smartPart, true);
            smartPartInfo.AutoHideOnLoad = this.dockManager.GetAutoHideMode(smartPart);
            smartPartInfo.DockTabAlignmentStyle = this.dockManager.DockTabAlignment;
            smartPartInfo.FreezeResizing = this.dockManager.FreezeResizing;

            if (smartPartInfo.DockStyle == DockingStyle.Left || smartPartInfo.DockStyle == DockingStyle.Right)
                smartPartInfo.Width = this.dockManager.GetControlSize(smartPart).Width;
            else if (smartPartInfo.DockStyle == DockingStyle.Top || smartPartInfo.DockStyle == DockingStyle.Bottom)
                smartPartInfo.Height = this.dockManager.GetControlSize(smartPart).Height;

            smartPartInfo.VisualStyle = this.dockManager.VisualStyle;
            smartPartInfo.Office2007Theme = this.dockManager.Office2007Theme;
            smartPartInfo.Title = this.dockManager.GetDockLabel(smartPart);

        }

        /// <summary>
        /// If the SmartPart is disposed make sure we remove it from our collection
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        private void ControlDisposed(object sender, EventArgs e)
        {
            Control smartPart = sender as Control;
            if (smartPart != null && this.windows.ContainsKey(smartPart.Name))
            {
                smartPart.Disposed -= ControlDisposed;
                // Remove the smartPart from the form to avoid disposing it.
                //window.Controls.Remove(smartPart);
                //CHANGES
                base.InnerSmartParts.Remove(smartPart);
                this.windows.Remove(smartPart.Name);
            }
        }

        /// <summary>
        /// Updates the window with the given SmartPartInfo and shows it
        /// </summary>
        /// <param name="window">The window.</param>
        /// <param name="smartPartInfo">The smart part info.</param>
        private void ShowWindow(Control window, DockingSmartPartInfo smartPartInfo)
        {
            if (!base.InnerSmartParts.Contains(window))
                base.InnerSmartParts.Add(window);
            SetWindowProperties(window, smartPartInfo);
            if (this.dockManager.GetEnableDocking(window))
                this.dockManager.SetDockVisibility(window, true);
            window.Show();
        }

        private int GetIndexFromKey(string key)
        {
            int i = -1;
            foreach (string s in this.windows.Keys)
            {
                i++;
                if (s == key)
                {
                    break;
                }
            }
            return i;
        }

        private string GetKeyFromIndex(int i)
        {
            int j = 0;
            if (this.windows.Count > i)
                foreach (string s in this.windows.Keys)
                {
                    if (j == i)
                        return s;
                    j++;
                }
            return "";
        }

        #endregion

        #region Overrided methods
        /// <summary>
        /// Shows the window for the SmartPart and brings it to the front.
        /// </summary>
        /// <param name="smartPart"></param>
        protected override void OnActivate(Control smartPart)
        {
            this.dockManager.SetEnableDocking(smartPart, true);
            try
            {
                this.setActiveForSmartPart = false;
                if (!this.dockManager.GetDockVisibility(smartPart))
                    this.dockManager.SetDockVisibility(smartPart, true);
                this.dockManager.ActivateControl(smartPart);
                SetActiveSmartPart(smartPart);
                this._SelectedIndex = GetIndexFromKey(smartPart.Name);
                smartPart.Show();
            }
            finally
            {
                this.setActiveForSmartPart = true;
            }
        }

        /// <summary>
        /// Sets the properties on the window based on the information.
        /// </summary>
        protected override void OnApplySmartPartInfo(Control smartPart, DockingSmartPartInfo smartPartInfo)
        {
            //PopulateWindows();
            if (this.windows.ContainsKey(smartPart.Name) == false)
            {
                this.windows.Add(smartPart.Name, smartPart);
            }

            Control window = this.windows[smartPart.Name];
            SetWindowProperties(window, smartPartInfo);
        }

        /// <summary>
        /// Closes the Docked window where the smart part is being shown.
        /// </summary>
        protected override void OnClose(Control smartPart)
        {
            UnwireWindow();
            //PopulateWindows();

            CloseDockedWindow(smartPart);
            SetNextSibiling(smartPart);
            //Control window = this.windows[smartPart.Name];
            //smartPart.Disposed -= ControlDisposed;
            // Remove the smartPart from the form to avoid disposing it.
            //window.Controls.Remove(smartPart);
            //CHANGES
            base.InnerSmartParts.Remove(smartPart);
            this.windows.Remove(smartPart.Name);
            smartPart.Dispose();
        }


        private void CloseDockedWindow(Control window)
        {
            if (this.dockManager.GetEnableDocking(window))
                this.dockManager.SetEnableDocking(window, false);
        }


        /// <summary>
        /// Hides the Docked window where the smart part is being shown.
        /// </summary>
        protected override void OnHide(Control smartPart)
        {
            if (this.dockManager.GetDockVisibility(smartPart))
            {
                //PopulateWindows();
                Control window = this.windows[smartPart.Name];
                ///activate the next control if present
                ActivateSiblingControl();
                smartPart.Hide();

                this.dockManager.SetDockVisibility(smartPart, false);
            }
        }

        private int _SelectedIndex = -1;

        private void ActivateSiblingControl()
        {
            if (_SelectedIndex != -1 && this.windows.Count > 1)//if its the last control then there is no control to activate
            {
                if (this._SelectedIndex > 0)
                {
                    this._SelectedIndex = this._SelectedIndex - 1;
                    OnActivate(this.windows[GetKeyFromIndex(this._SelectedIndex)]);
                    RaiseSmartPartActivated(this.windows[GetKeyFromIndex(this._SelectedIndex)]);
                }
                else if (this._SelectedIndex < this.windows.Count - 1)
                {
                    this._SelectedIndex += 1;
                    OnActivate(this.windows[GetKeyFromIndex(this._SelectedIndex)]);
                    RaiseSmartPartActivated(this.windows[GetKeyFromIndex(this._SelectedIndex)]);
                }
            }
            else
            {
                this.dockManager.ActivateControl(null);
                base.SetActiveSmartPart(null);
            }
        }

        private void SetNextSibiling(Control window)
        {
            int index = GetIndexFromKey(window.Name);
            if (index > 0 && index < this.windows.Count) //index is not the first control
            {
                index--;
            }
            else //index is the first control
            {
                index = -1;
            }
            this._SelectedIndex = index;
            ActivateSiblingControl();
        }

        /// <summary>
        /// Shows a Docked window for the smart part and sets its properties.
        /// </summary>
        protected override void OnShow(Control smartPart, DockingSmartPartInfo smartPartInfo)
        {
            //PopulateWindows();
            if (!this.windows.ContainsKey(smartPart.Name))
            {
                if (!this.dockManager.GetEnableDocking(smartPart))
                    this.dockManager.SetEnableDocking(smartPart, true);

                System.Windows.Forms.Control window = GetOrCreateWindow(smartPart);
                ShowWindow(window, smartPartInfo);
                smartPart.Disposed += ControlDisposed;
            }
            else
            {
                if (!this.dockManager.GetEnableDocking(smartPart))
                    this.dockManager.SetEnableDocking(smartPart, true);
                OnActivate(smartPart);
                RaiseSmartPartActivated(smartPart);
            }
        }

        /// <summary>
        /// Provides conversion from ISmartPartInfo to DockableWindowSmartPartInfo
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        protected override DockingSmartPartInfo ConvertFrom(ISmartPartInfo source)
        {
            return SmartPartInfo.ConvertTo<DockingSmartPartInfo>(source);
        }
        #endregion



    }
}