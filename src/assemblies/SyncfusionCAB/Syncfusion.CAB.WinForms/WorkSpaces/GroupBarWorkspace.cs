using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Collections.ObjectModel;
using System.Windows.Forms;
using Microsoft.Practices.CompositeUI;
using Microsoft.Practices.CompositeUI.SmartParts;
using Microsoft.Practices.ObjectBuilder;
using Microsoft.Practices.CompositeUI.Services;
using Syncfusion.Windows.Forms;
using Syncfusion.CAB.WinForms.SmartPartInfos;
using Syncfusion.Windows.Forms.Tools;

namespace Syncfusion.CAB.WinForms.WorkSpaces
{
    /// <summary>
    /// GroupBar styled workspace inherits from IComposableWorkspace interface and GroupBar control.
    /// </summary>
    [ToolboxBitmap(typeof(GroupBarWorkspace), "ToolboxIcons.GroupBarWorkspace.bmp")]
    public partial class GroupBarWorkspace : GroupBar, IComposableWorkspace<Control, GroupBarSmartPartInfo>
    {
        #region Fields, Properties, and Constructor

        private Dictionary<String, GroupBarItem> groupBarItems = new Dictionary<String, GroupBarItem>();
        private WorkspaceComposer<Control, GroupBarSmartPartInfo> _composer;
        private bool populateGroupBarViews = false;

        private Font tFont;
        private Font tHeaderFont;

        /// <summary>
        /// Initializes a new instance of the <see cref="GroupBarWorkspace"/> class.
        /// </summary>
        public GroupBarWorkspace()
        {
            _composer = new WorkspaceComposer<Control, GroupBarSmartPartInfo>(this);
            this.Name = "GroupBarWorkspace";
            InitializeComponent();
            this.Anchor = AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Left | AnchorStyles.Bottom;
            this.BorderStyle = BorderStyle.FixedSingle;
            this.StackedMode = false;
            this.ShowChevron = false;
            this.Dock = DockStyle.None;
            tFont = this.Font;
            tHeaderFont = this.HeaderFont;
            this.Padding = new Padding(7);
            this.GroupBarItemSelected += new EventHandler(GroupBarWorkspace_GroupBarItemSelected);
        }

        void GroupBarWorkspace_GroupBarItemSelected(object sender, EventArgs e)
        {
            OnSmartPartActivated(new WorkspaceEventArgs(GetControlFromGroupBarItem(this.GroupBarItems[this.SelectedItem])));
        }

        /// <summary>
        /// Sets the work item.
        /// </summary>
        /// <value>The work item.</value>
        [ServiceDependency]
        public WorkItem WorkItem
        {
            set
            {
                _composer.WorkItem = value;
            }

        }

        #endregion

        #region GroupBarWorkspaceMethods

        /// <summary>
        /// Populates the groupbar workspace with groupbar items.
        /// </summary>
        private void PopulateGroupBarItem()
        {
            int count;
            if (populateGroupBarViews == false)
            {
                if (this.Controls.Count > 0 && this.Controls.Count != this.groupBarItems.Count)
                {
                    foreach (Control item in this.Controls)
                    {
                        for (count = 0; count < this.GroupBarItems.Count; count++)
                        {
                            if (this.GroupBarItems[count].Client == item && !(this.groupBarItems.ContainsKey(item.Name)))
                            {
                                groupBarItems.Add(item.Name, this.GroupBarItems[count]);
                                Show(item, new GroupBarSmartPartInfo());
                            }
                        }
                    }
                }
                if (this.Controls.Count == this.groupBarItems.Count)
                    populateGroupBarViews = true;
            }
        }

        /// <summary>
        /// Controls the disposed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ControlDisposed(object sender, EventArgs e)
        {
            Control control = sender as Control;
            if (control != null && this.groupBarItems.ContainsKey(control.Name))
            {
                _composer.ForceClose(control);
            }
        }

        /// <summary>
        /// Creates the group bar item.
        /// </summary>
        /// <param name="smartpart">The smartpart.</param>
        /// <returns></returns>
        private GroupBarItem CreateGroupBarItem(Control smartpart)
        {
            GroupBarItem gbItem = new GroupBarItem();
            Control current = smartpart;
            int count;

            if (smartpart != null && this.Controls.Contains(smartpart))
            {
                for (count = 0; count < this.GroupBarItems.Count; count++)
                {
                    if (this.GroupBarItems[count].Client == smartpart)
                        gbItem = this.GroupBarItems[count];
                }
                return gbItem;
            }
            else
            {
                gbItem.Client = smartpart;

                this.GroupBarItems.Add(gbItem);
                this.groupBarItems.Add(smartpart.Name, gbItem);
                return gbItem;
            }
        }

        /// <summary>
        /// Hooks up the controls added at design-time.
        /// </summary>
        protected override void OnCreateControl()
        {
            base.OnCreateControl();
            PopulateGroupBarItem();
        }

        /// <summary>
        /// Finds the group bar item.
        /// </summary>
        /// <param name="smartpart">The smartpart.</param>
        /// <returns></returns>
        private GroupBarItem FindGroupBarItem(Control smartpart)
        {
            GroupBarItem gbItem = null;
            for (int count = 0; count < this.GroupBarItems.Count; count++)
            {
                if (this.GroupBarItems[count].Client == smartpart)
                    return this.GroupBarItems[count];
            }
            return gbItem;
        }

        /// <summary>
        /// Gets the index for group bar item.
        /// </summary>
        /// <param name="smartPart">The smart part.</param>
        /// <returns></returns>
        private int GetIndexForGroupBarItem(Control smartPart)
        {
            int i = -1;
            for (int count = 0; count < this.GroupBarItems.Count; count++)
            {
                if (this.GroupBarItems[count].Client == smartPart)
                {
                    i = count;
                    break;
                }
            }
            return i;
        }

        /// <summary>
        /// Gets the control from group bar item.
        /// </summary>
        /// <param name="gbItem">The gb item.</param>
        /// <returns></returns>
        private Control GetControlFromGroupBarItem(GroupBarItem gbItem)
        {
            if (gbItem != null && gbItem.Client != null)
            {
                return gbItem.Client as Control;
            }
            return null;
        }

        /// <summary>
        /// Gets the smart part.
        /// </summary>
        /// <param name="smartPart">The smart part.</param>
        /// <returns></returns>
        private Control GetSmartPart(object smartPart)
        {
            Control sp = smartPart as Control;
            if (sp == null) throw new ArgumentException("SmartPart was not a Control");
            return sp;
        }

        /// <summary>
        /// Sets the smart part behaviour.
        /// </summary>
        /// <param name="smartpart">The smartpart.</param>
        /// <param name="groupBarSmartPartInfo">The smart info.</param>
        private void SetSmartPartBehaviour(Control smartpart, GroupBarSmartPartInfo groupBarSmartPartInfo)
        {
            GroupBarItem gbItem = new GroupBarItem();
            gbItem = FindGroupBarItem(smartpart);
            if (gbItem != null)
            {
                int val = 0;

                if (groupBarSmartPartInfo.BarDisplayText == "" || groupBarSmartPartInfo.Title == "")
                {
                    for (int groupBarItem = this.GroupBarItems.Count - 1; groupBarItem > -1; groupBarItem--)
                    {
                        string split = this.GroupBarItems[groupBarItem].Text;

                        if (split.Length >= 12 && split.Substring(0, 12) == "GroupBarItem")
                        {
                            string t = split.Substring(12, split.Length - 12);
                            val = int.Parse(t);
                            val++;
                            break;
                        }
                    }
                    gbItem.Text = "GroupBarItem" + val.ToString();
                }
                else if (groupBarSmartPartInfo.Title != "")
                    gbItem.Text = groupBarSmartPartInfo.Title;
                else if (groupBarSmartPartInfo.BarDisplayText != "")
                    gbItem.Text = groupBarSmartPartInfo.BarDisplayText;

                if (groupBarSmartPartInfo.StackModeWithChevron == true)
                {
                    this.StackedMode = true;
                    this.ShowChevron = true;
                }
                else
                    this.ShowChevron = false;
                this.BarHighlight = groupBarSmartPartInfo.BarHighLighting;
                if (groupBarSmartPartInfo.TopBorderColor.IsEmpty == false || groupBarSmartPartInfo.BottomBorderColor.IsEmpty == false || groupBarSmartPartInfo.LeftBorderColor.IsEmpty == false || groupBarSmartPartInfo.RightBorderColor.IsEmpty == false)
                {
                    this.DrawClientBorder = true;
                    BorderColors color = new BorderColors();
                    color.Top = groupBarSmartPartInfo.TopBorderColor;
                    color.Left = groupBarSmartPartInfo.LeftBorderColor;
                    color.Right = groupBarSmartPartInfo.RightBorderColor;
                    color.Bottom = groupBarSmartPartInfo.BottomBorderColor;
                    if (gbItem != null)
                        gbItem.ClientBorderColors = color;
                }
                if (groupBarSmartPartInfo.BarIcon != null)
                {
                    gbItem.Icon = groupBarSmartPartInfo.BarIcon;
                }

                if (groupBarSmartPartInfo.NavigationPaneIcon != null)
                {
                    this.StackedMode = true;
                    gbItem.NavigationPaneIcon = groupBarSmartPartInfo.NavigationPaneIcon;
                }
                this.GroupBarItemHeight = groupBarSmartPartInfo.GroupBarItemHeight;
                this.StackedMode = groupBarSmartPartInfo.StackMode;
                this.VisualStyle = groupBarSmartPartInfo.VisualStyle;
                this.Office2007Theme = groupBarSmartPartInfo.Office2007Theme;
                this.HeaderHeight = groupBarSmartPartInfo.HeaderHeight;
                this.HeaderBackColor = groupBarSmartPartInfo.HeaderBackColor;
                this.HeaderForeColor = groupBarSmartPartInfo.HeaderForeColor;
                gbItem.Padding = groupBarSmartPartInfo.Padding;

                if (groupBarSmartPartInfo.Font != null)
                {
                    this.Font = groupBarSmartPartInfo.Font;
                    tFont = groupBarSmartPartInfo.Font;
                }
                else
                    this.Font = tFont;
                if (groupBarSmartPartInfo.HeaderFont != null)
                {
                    this.HeaderFont = groupBarSmartPartInfo.HeaderFont;
                    tHeaderFont = groupBarSmartPartInfo.HeaderFont;
                }
                else
                    this.HeaderFont = tHeaderFont;
            }
        }

        /// <summary>
        /// Throws an excpetion if the specified smartpart not in container.
        /// </summary>
        /// <param name="spcontrol">The spcontrol.</param>
        private void ThrowIfNotInContainer(Control spcontrol)
        {
            if (groupBarItems.ContainsKey(spcontrol.Name) == false)
            {
                throw new ArgumentException("SmartPart does not exist in the container, make sure it has been shown before closing/hiding it");
            }
        }

        #endregion

        #region Protected virtual implementations

        /// <summary>
        /// Raises the <see cref="E:SmartPartActivated"/> event.
        /// </summary>
        /// <param name="e">The <see cref="Microsoft.Practices.CompositeUI.SmartParts.WorkspaceEventArgs"/> instance containing the event data.</param>
        protected virtual void OnSmartPartActivated(WorkspaceEventArgs e)
        {
            if (SmartPartActivated != null)
            {
                SmartPartActivated(this, e);
            }
        }

        /// <summary>
        /// Raises the <see cref="SmartPartClosing"/> event.
        /// </summary>
        protected virtual void OnSmartPartClosing(WorkspaceCancelEventArgs e)
        {
            if (SmartPartClosing != null)
            {
                SmartPartClosing(this, e);
            }
        }

        #endregion

        #region IComposableWorkspace<Control,GroupBarSmartPartInfo> Members

        /// <summary>
        /// See <see cref="IComposableWorkspace{TSmartPart, TSmartPartInfo}.OnActivate"/> for more information.
        /// </summary>
        void IComposableWorkspace<Control, GroupBarSmartPartInfo>.OnActivate(Control smartPart)
        {
            this.GroupBarItemSelected -= GroupBarWorkspace_GroupBarItemSelected;

            GroupBarItem gbItem = this.groupBarItems[smartPart.Name];
            for (int count = 0; count < this.GroupBarItems.Count; count++)
            {
                if (gbItem == this.GroupBarItems[count])
                {
                    this.SelectedItem = count;
                    break;
                }
            }
            this.GroupBarItemSelected += GroupBarWorkspace_GroupBarItemSelected;
        }

        /// <summary>
        /// See <see cref="IComposableWorkspace{TSmartPart, TSmartPartInfo}.OnApplySmartPartInfo"/> for more information.
        /// </summary>
        void IComposableWorkspace<Control, GroupBarSmartPartInfo>.OnApplySmartPartInfo(Control smartPart, GroupBarSmartPartInfo groupBarSmartPartInfo)
        {
            if (this.Controls.Contains(smartPart) && this.groupBarItems.ContainsKey(smartPart.Name))
            {
                SetSmartPartBehaviour(smartPart, groupBarSmartPartInfo);
                if (groupBarSmartPartInfo.ActiveGroupBarItem == true)
                    Activate(smartPart);
            }
        }

        /// <summary>
        /// See <see cref="IComposableWorkspace{TSmartPart, TSmartPartInfo}.OnShow"/> for more information.
        /// </summary>
        void IComposableWorkspace<Control, GroupBarSmartPartInfo>.OnShow(Control smartPart, GroupBarSmartPartInfo groupBarSmartPartInfo)
        {
            this.GroupBarItemSelected -= GroupBarWorkspace_GroupBarItemSelected;

            PopulateGroupBarItem();
            GroupBarItem barItem = CreateGroupBarItem(smartPart);
            SetSmartPartBehaviour(smartPart, groupBarSmartPartInfo);
            if (groupBarSmartPartInfo.ActiveGroupBarItem)
            {
                Activate(smartPart);
            }

            this.GroupBarItemSelected -= GroupBarWorkspace_GroupBarItemSelected;
            smartPart.Disposed += ControlDisposed;
        }

        /// <summary>
        /// See <see cref="IComposableWorkspace{TSmartPart, TSmartPartInfo}.OnHide"/> for more information.
        /// </summary>
        void IComposableWorkspace<Control, GroupBarSmartPartInfo>.OnHide(Control smartPart)
        {
            try
            {
                if (smartPart.Visible && this.Controls.Contains(smartPart))
                {
                    int key = GetIndexForGroupBarItem(smartPart);
                    this.GroupBarItems[key].Visible = false;
                    this.Controls[smartPart.Name].Hide();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        /// <summary>
        /// See <see cref="IComposableWorkspace{TSmartPart, TSmartPartInfo}.OnClose"/> for more information.
        /// </summary>
        void IComposableWorkspace<Control, GroupBarSmartPartInfo>.OnClose(Control smartPart)
        {
            try
            {

                ThrowIfNotInContainer(smartPart);
                foreach (GroupBarItem g in this.GroupBarItems)
                {
                    if (smartPart == g.Client && groupBarItems.ContainsKey(smartPart.Name))
                    {
                        this.GroupBarItemSelected -= GroupBarWorkspace_GroupBarItemSelected;
                        groupBarItems.Remove(smartPart.Name);
                        g.Client = null;
                        this.GroupBarItems.Remove(g);
                        this.GroupBarItemSelected += GroupBarWorkspace_GroupBarItemSelected;
                        break;
                    }
                }
                smartPart.Disposed -= ControlDisposed;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        /// <summary>
        /// See <see cref="IComposableWorkspace{TSmartPart, TSmartPartInfo}.RaiseSmartPartActivated"/> for more information.
        /// </summary>
        void IComposableWorkspace<Control, GroupBarSmartPartInfo>.RaiseSmartPartActivated(WorkspaceEventArgs e)
        {
            OnSmartPartActivated(e);
        }

        /// <summary>
        /// See <see cref="IComposableWorkspace{TSmartPart, TSmartPartInfo}.RaiseSmartPartClosing"/> for more information.
        /// </summary>
        void IComposableWorkspace<Control, GroupBarSmartPartInfo>.RaiseSmartPartClosing(WorkspaceCancelEventArgs e)
        {
            OnSmartPartClosing(e);
        }

        /// <summary>
        /// See <see cref="IComposableWorkspace{TSmartPart, TSmartPartInfo}.ConvertFrom"/> for more information.
        /// </summary>
        GroupBarSmartPartInfo IComposableWorkspace<Control, GroupBarSmartPartInfo>.ConvertFrom(ISmartPartInfo source)
        {
            return GroupBarSmartPartInfo.ConvertTo<GroupBarSmartPartInfo>(source);
        }

        #endregion

        #region IWorkspace Members

        /// <summary>
        /// Activates the smartPart on the workspace.
        /// </summary>
        /// <param name="smartPart">The smart part to activate.</param>
        public void Activate(object smartPart)
        {
            _composer.Activate(smartPart);
        }

        /// <summary>
        /// The currently active smart part.
        /// </summary>
        /// <value></value>
        public object ActiveSmartPart
        {
            get { return _composer.ActiveSmartPart; }
        }

        /// <summary>
        /// Applies the smart part info.
        /// </summary>
        /// <param name="smartPart">The smart part.</param>
        /// <param name="PopupSmartPartInfo">The popup smart part info.</param>
        public void ApplySmartPartInfo(object smartPart, ISmartPartInfo SmartPartInfo)
        {
            _composer.ApplySmartPartInfo(smartPart, SmartPartInfo);
        }

        /// <summary>
        /// Closes a smart part. Disposing the smart part is the responsibility of the caller.
        /// </summary>
        /// <param name="smartPart">Smart part to close.</param>
        public void Close(object smartPart)
        {
            try
            {
                if (this.Controls.Count > 0)
                {
                    ThrowIfNotInContainer(GetSmartPart(smartPart));
                    _composer.Close(smartPart);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        /// <summary>
        /// Hides a smart part from the UI.
        /// </summary>
        /// <param name="smartPart">Smart part to hide.</param>
        public void Hide(object smartPart)
        {
            try
            {
                _composer.Hide(smartPart);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        /// <summary>
        /// Shows a smart part in the UI.
        /// </summary>
        /// <param name="smartPart">Smart part to show.</param>
        public void Show(object smartPart)
        {
            if (smartPart != null)
                _composer.Show(smartPart);
        }

        /// <summary>
        /// Shows the specified smart part.
        /// </summary>
        /// <param name="smartPart">The smart part.</param>
        /// <param name="PopupSmartPartInfo">The popup smart part info.</param>
        public void Show(object smartPart, ISmartPartInfo SmartPartInfo)
        {
            if (smartPart != null)
                _composer.Show(smartPart, SmartPartInfo);
        }

        public event EventHandler<WorkspaceEventArgs> SmartPartActivated;

        public event EventHandler<WorkspaceCancelEventArgs> SmartPartClosing;

        /// <summary>
        /// A snapshot of the smart parts currently contained in the workspace.
        /// </summary>
        /// <value></value>
        public ReadOnlyCollection<object> SmartParts
        {
            get { return _composer.SmartParts; }
        }

        #endregion
    }
}
