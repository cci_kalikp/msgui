using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

using Microsoft.Practices.CompositeUI.SmartParts;
using Microsoft.Practices.CompositeUI.Utility;
using Microsoft.Practices.CompositeUI;
using Microsoft.Practices.CompositeUI.WinForms;

using Syncfusion.Windows.Forms.Tools;
using Syncfusion.CAB.WinForms.SmartPartInfos;
using Syncfusion.Windows.Forms.Tools.Enums;

namespace Syncfusion.CAB.WinForms.WorkSpaces
{
	[ToolboxBitmap(typeof(SplitContainerAdvWorkspace), "ToolboxIcons.SplitContainerAdvWorkspace.bmp")]
    public class SplitContainerAdvWorkspace : SplitContainerAdv, IComposableWorkspace<Control, SplitContainerAdvSmartPartInfo>
    {
        private WorkspaceComposer<Control, SplitContainerAdvSmartPartInfo> composer;

        private int tPanel1MinSize;
        private int tPanel2MinSize;
        private Style tStyle;
        private bool tPanel1Collapsed;
        private bool tPanel2Collapsed;
        private int tSplitterDistance;
        private int tSplitterIncrement;
        private int tSplitterWidth;
        private Orientation tOrientation;
        private bool tIsSplitterFixed;

        public SplitContainerAdvWorkspace()
        {
            composer = new WorkspaceComposer<Control, SplitContainerAdvSmartPartInfo>(this);
            tPanel1MinSize = this.Panel1MinSize;
            tPanel2MinSize = this.Panel2MinSize;
            tPanel1Collapsed = this.Panel1Collapsed;
            tPanel2Collapsed = this.Panel2Collapsed;
            tStyle = this.Style;
            tSplitterDistance = this.SplitterDistance;
            tSplitterIncrement = this.SplitterIncrement;
            tSplitterWidth = this.SplitterWidth;
            tStyle = Style.None;
            tOrientation = this.Orientation;
            tIsSplitterFixed = this.IsSplitterFixed;
        }

        [ServiceDependency]
        public WorkItem workItem
        {
            set
            {
                composer.WorkItem = value;
            }
        }

        #region Private Members
        /// <summary>
        /// Adds the SmartPart to the Left/Right of the SplitContainerAdv Panel based on the information sent through the
        /// SmartPartInfo object
        /// </summary>
        /// <param name="smartPart"></param>
        /// <param name="smartPartInfo"></param>
        private void AddControlToPanel(Control smartPart, SplitContainerAdvSmartPartInfo smartPartInfo)
        {
            if (smartPartInfo.PanelSide.HasValue)
            {
                if (smartPartInfo.PanelSide == PanelSide.Left)
                {
                    this.Panel1.Controls.Add(smartPart);
                }
                else if (smartPartInfo.PanelSide == PanelSide.Right)
                {
                    this.Panel2.Controls.Add(smartPart);
                }
            }
            else
            {
                throw new ArgumentNullException("PanelSide argument should not be null");
                return;
            }
            if (smartPartInfo.DockStyle.HasValue)
                smartPart.Dock = smartPartInfo.DockStyle.Value;
            else
                smartPart.Dock = DockStyle.Fill;
            if (smartPartInfo.Style.HasValue)
            {
                this.Style = smartPartInfo.Style.Value;
                this.tStyle = smartPartInfo.Style.Value;
            }
            else
                this.Style = tStyle;
           
            if (smartPartInfo.Orientation.HasValue)
            {
                this.Orientation = smartPartInfo.Orientation.Value;
                this.tOrientation = smartPartInfo.Orientation.Value;
            }
            else
                this.Orientation = tOrientation;

            if (smartPartInfo.Panel1MinSize != this.Panel1MinSize)
            {
                this.Panel1MinSize = smartPartInfo.Panel1MinSize;
                this.tPanel1MinSize = smartPartInfo.Panel1MinSize;
            }
            else
                this.Panel1MinSize = this.tPanel1MinSize;

            if (smartPartInfo.Panel2MinSize != this.Panel2MinSize)
            {
                this.Panel2MinSize = smartPartInfo.Panel2MinSize;
                this.tPanel2MinSize = smartPartInfo.Panel2MinSize;
            }
            else
                this.Panel2MinSize = this.tPanel2MinSize;

            if (smartPartInfo.Panel1Collapsed != this.Panel1Collapsed)
            {
                this.Panel1Collapsed = smartPartInfo.Panel1Collapsed;
                tPanel1Collapsed = smartPartInfo.Panel1Collapsed;
            }
            else
                this.Panel1Collapsed = this.tPanel1Collapsed;

            if (smartPartInfo.Panel2Collapsed != this.Panel2Collapsed)
            {
                this.Panel2Collapsed = smartPartInfo.Panel2Collapsed;
                tPanel2Collapsed = smartPartInfo.Panel2Collapsed;
            }
            else
                this.Panel2Collapsed = tPanel2Collapsed;

            if (smartPartInfo.SplitterDistance != this.SplitterDistance)
            {
                this.SplitterDistance = smartPartInfo.SplitterDistance;
                this.tSplitterDistance = smartPartInfo.SplitterDistance;
            }
            else
                this.SplitterDistance = tSplitterDistance;

            if (smartPartInfo.SplitterIncrement != this.SplitterIncrement)
            {
                this.SplitterIncrement = smartPartInfo.SplitterIncrement;
                this.tSplitterIncrement = smartPartInfo.SplitterIncrement;
            }
            else
                this.SplitterIncrement = tSplitterIncrement;

            if (smartPartInfo.SplitterWidth != this.SplitterWidth)
            {
                this.SplitterWidth = smartPartInfo.SplitterWidth;
                this.tSplitterWidth = smartPartInfo.SplitterWidth;
            }
            else
                this.SplitterWidth = tSplitterWidth;

            if (smartPartInfo.IsSplitterFixed != this.IsSplitterFixed)
            {
                this.IsSplitterFixed = smartPartInfo.IsSplitterFixed;
                this.tIsSplitterFixed = smartPartInfo.IsSplitterFixed;
            }
            else
                this.IsSplitterFixed = smartPartInfo.IsSplitterFixed;
        }


        /// <summary>
        /// Event Handler for disposing control once they are closed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ControlDisposed(object sender, EventArgs e)
        {
            Control control = sender as Control;
            if (control != null)
            {
                composer.ForceClose(control);
            }
        }

        #endregion

        #region Protected Virtual Functions

        /// <summary>
        /// Activates the given SmartPart
        /// </summary>
        /// <param name="smartPart"></param>
        protected virtual void OnActivate(Control smartPart)
        {
            smartPart.BringToFront();
            smartPart.Show();
        }

        /// <summary>
        /// Applies the SmartPartInfo by adding the SmartPart again to the Control Collection List
        /// </summary>
        /// <param name="smartPart"></param>
        /// <param name="smartPartInfo"></param>
        protected virtual void OnApplySmartPartInfo(Control smartPart, SplitContainerAdvSmartPartInfo smartPartInfo)
        {
            AddControlToPanel(smartPart, smartPartInfo);
        }

        /// <summary>
        /// Closes the SmartPart
        /// </summary>
        /// <param name="smartPart"></param>
        protected virtual void OnClose(Control smartPart)
        {
            smartPart.Disposed -= ControlDisposed;
            if (this.Panel1.Controls.Contains(smartPart))
                this.Panel1.Controls.Remove(smartPart);
            else if (this.Panel2.Controls.Contains(smartPart))
                this.Panel2.Controls.Remove(smartPart);
        }

        /// <summary>
        /// Hides the SmartPart
        /// </summary>
        /// <param name="smartPart"></param>
        protected virtual void OnHide(Control smartPart)
        {
            smartPart.Hide();
        }

        /// <summary>
        /// Shows the SmartPart, if the SmartPart is already present in the Control Collection List, It would
        /// activate the appropriate SmartPart accordingly
        /// </summary>
        /// <param name="smartPart"></param>
        /// <param name="smartPartInfo"></param>
        protected virtual void OnShow(Control smartPart, SplitContainerAdvSmartPartInfo smartPartInfo)
        {
            smartPart.Disposed += ControlDisposed;
            AddControlToPanel(smartPart, smartPartInfo);
            Activate(smartPart);
        }

        /// <summary>
        /// Raises the SmartPartActivated event
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnSmartPartActivated(WorkspaceEventArgs e)
        {
            if (SmartPartActivated != null)
            {
                SmartPartActivated(this, e);
            }
        }

        /// <summary>
        /// Raises the SmartPartClosing event
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnSmartPartClosing(WorkspaceCancelEventArgs e)
        {
            if (SmartPartClosing != null)
            {
                SmartPartClosing(this, e);
            }
        }

        #endregion

        #region IComposableWorkspace<Control,SplitContainerAdvSmartPartInfo> Members

        SplitContainerAdvSmartPartInfo IComposableWorkspace<Control, SplitContainerAdvSmartPartInfo>.ConvertFrom(ISmartPartInfo source)
        {
            return SmartPartInfo.ConvertTo<SplitContainerAdvSmartPartInfo>(source);
        }

        void IComposableWorkspace<Control, SplitContainerAdvSmartPartInfo>.OnActivate(Control smartPart)
        {
            OnActivate(smartPart);
        }

        void IComposableWorkspace<Control, SplitContainerAdvSmartPartInfo>.OnApplySmartPartInfo(Control smartPart, SplitContainerAdvSmartPartInfo smartPartInfo)
        {
            OnApplySmartPartInfo(smartPart, smartPartInfo);
        }

        void IComposableWorkspace<Control, SplitContainerAdvSmartPartInfo>.OnClose(Control smartPart)
        {
            OnClose(smartPart);
        }

        void IComposableWorkspace<Control, SplitContainerAdvSmartPartInfo>.OnHide(Control smartPart)
        {
            OnHide(smartPart);
        }

        void IComposableWorkspace<Control, SplitContainerAdvSmartPartInfo>.OnShow(Control smartPart, SplitContainerAdvSmartPartInfo smartPartInfo)
        {
            OnShow(smartPart, smartPartInfo);
        }

        void IComposableWorkspace<Control, SplitContainerAdvSmartPartInfo>.RaiseSmartPartActivated(WorkspaceEventArgs e)
        {
            OnSmartPartActivated(e);
        }

        void IComposableWorkspace<Control, SplitContainerAdvSmartPartInfo>.RaiseSmartPartClosing(WorkspaceCancelEventArgs e)
        {
            OnSmartPartClosing(e);
        }

        #endregion

        #region IWorkspace Members

        public void Activate(object smartPart)
        {
            composer.Activate(smartPart);
        }

        public object ActiveSmartPart
        {
            get
            {
                return composer.ActiveSmartPart;
            }
        }

        public void ApplySmartPartInfo(object smartPart, ISmartPartInfo SplitContainerAdvSmartPartInfo)
        {
            composer.ApplySmartPartInfo(smartPart, SplitContainerAdvSmartPartInfo);
        }

        public void Close(object smartPart)
        {
            composer.Close(smartPart);
        }

        public void Hide(object smartPart)
        {
            composer.Hide(smartPart);
        }

        public void Show(object smartPart)
        {
            composer.Show(smartPart);
        }

        public void Show(object smartPart, ISmartPartInfo SplitContainerAdvSmartPartInfo)
        {
            composer.Show(smartPart, SplitContainerAdvSmartPartInfo);
        }

        public event EventHandler<WorkspaceEventArgs> SmartPartActivated;

        public event EventHandler<WorkspaceCancelEventArgs> SmartPartClosing;

        public System.Collections.ObjectModel.ReadOnlyCollection<object> SmartParts
        {
            get
            {
                return composer.SmartParts;
            }
        }

        #endregion
    }
}
