using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Microsoft.Practices.CompositeUI.Commands;
using Microsoft.Practices.CompositeUI;
using Microsoft.Practices.CompositeUI.SmartParts;

using Syncfusion.Windows.Forms.Tools;
using Syncfusion.CAB.WinForms.SmartPartInfos;
using Microsoft.Practices.CompositeUI.Utility;

namespace Syncfusion.CAB.WinForms.WorkSpaces
{
    /// <summary>
    /// Implements a workspace that shows smartparts in a <see cref="TabControlAdv"/>.
    /// </summary>
    [ToolboxBitmap(typeof(TabControlAdvWorkspace), "ToolboxIcons.TabControlAdvWorkspace.bmp")]
    public class TabControlAdvWorkspace : TabControlAdv, IComposableWorkspace<Control, TabControlAdvSmartPartInfo>
    {
        private Dictionary<Control, TabPageAdv> pages = new Dictionary<Control, TabPageAdv>();
        private WorkspaceComposer<Control, TabControlAdvSmartPartInfo> composer;
        private bool callComposerActivateOnIndexChange = true;
        private bool populatingPages = false;
        private bool alreadyDisposed = false;

        #region PrivateTemp Items

        private int tTabGap;
        private Type tTabStyle;
        private TabAlignment tAlignment;
        private ScrollIncrement tScrollIncrement;
        private Syncfusion.Windows.Forms.Tools.TabSizeMode tSizeMode;

        #endregion


        # region Constructor

        /// <summary>
        /// Initializes a new instance of a TabControlAdvWorkspace class.
        /// </summary>
        public TabControlAdvWorkspace()
        {
            composer = new WorkspaceComposer<Control, TabControlAdvSmartPartInfo>(this);
            this.tTabGap = this.TabGap;
            this.tTabStyle = this.TabStyle;
            this.tAlignment = this.Alignment;
            this.tScrollIncrement = this.ScrollIncrement;
            this.tSizeMode = this.SizeMode;
            this.SelectedIndexChanged += new EventHandler(TabControlAdvWorkspace_SelectedIndexChanged);
        }

        # endregion //Constructor

        /// <summary>
        /// Dependency injection setter property to get the <see cref="WorkItem"/> where the 
        /// object is contained.
        /// </summary>
        [ServiceDependency]
        public WorkItem workItem
        {
            set
            {
                composer.WorkItem = value;
            }
        }

        /// <summary>
        /// Read-only view of WindowDictionary.
        /// </summary>
        [Browsable(false)]
        public ReadOnlyDictionary<Control, TabPageAdv> Pages
        {
            get { return new ReadOnlyDictionary<Control, TabPageAdv>(this.pages); }
        }

        private void ControlDisposed(object sender, EventArgs e)
        {
            Control control = sender as Control;
            if (control != null && pages.ContainsKey(control))
            {
                alreadyDisposed = true;
                composer.ForceClose(control);
            }
        }

        # region IComposableWorkspace related Methods

        /// <summary>
        /// Activates the smart part.
        /// </summary>
        /// <param name="smartPart"></param>
        protected virtual void OnActivateSmartPart(Control smartPart)
        {
            PopulatePages();

            int key = GetTabPageIndex(smartPart);

            try
            {
                callComposerActivateOnIndexChange = false;
                TabPages[key].Show();//show the tabpage before setting it. This causes the tabpage to be refreshed properly
                SelectedTab = TabPages[key];
                //changes - Setting the visibility of smartpart as true
                smartPart.Visible = true;
            }
            finally
            {
                callComposerActivateOnIndexChange = true;
            }
        }

        /// <summary>
        /// Applies the smart part display information to the smart part.
        /// </summary>
        /// <param name="smartPart"></param>
        /// <param name="smartPartInfo"></param>
        protected virtual void OnApplySmartPartInfoSmartPart(Control smartPart, TabControlAdvSmartPartInfo smartPartInfo)
        {
            PopulatePages();
            int key = GetTabPageIndex(smartPart);
            SetTabProperties(TabPages[key], smartPartInfo, smartPart);
            if (smartPartInfo.ActivateTab)
            {
                this.SelectedTab = TabPages[key];
                //changes
                //Activate(smartPart);
            }
        }

        /// <summary>
        /// Closes the smart part.
        /// </summary>
        /// <param name="smartPart"></param>
        protected virtual void OnCloseSmartPart(Control smartPart)
        {
            PopulatePages();
            if (TabPages != null && !alreadyDisposed)
                if (this.TabPages.Contains(this.TabPages[GetTabPageIndex(smartPart)]))
                {
                    this.SelectedIndexChanged -= new EventHandler(TabControlAdvWorkspace_SelectedIndexChanged);
                    this.TabPages.Remove(this.TabPages[GetTabPageIndex(smartPart)]);
                    this.SelectedIndexChanged += new EventHandler(TabControlAdvWorkspace_SelectedIndexChanged);
                }
            pages.Remove(smartPart);
            smartPart.Disposed -= ControlDisposed;
            alreadyDisposed = false;
        }

        private int GetTabPageIndex(Control smartPart)
        {
            int key = -1;
            foreach (TabPageAdv page in TabPages)
            {
                if (page.Name == pages[smartPart].Name)
                {
                    key = this.TabPages.IndexOf(page);
                    break;
                }
            }
            return key;
        }

        /// <summary>
        /// Hides the smart part.
        /// </summary>
        /// <param name="smartPart"></param>
        protected virtual void OnHideSmartPart(Control smartPart)
        {
            //if (smartPart.Visible)
            //{
            PopulatePages();
            int key = GetTabPageIndex(smartPart);
            TabPages[key].Hide();
            bool hideFlag = SetSelectedIndexOnHide();
            if (hideFlag)
                composer.SetActiveSmartPart(this.GetControlFromPage(this.SelectedTab));
            else composer.SetActiveSmartPart(null);
            smartPart.Visible = false;
            //}

        }


        /*
         * Workaround for setting the selectedindex properly when a tabpage is hidden.
         */
        /// <summary>
        /// Sets the selected index on hide.
        /// </summary>
        private bool SetSelectedIndexOnHide()
        {
            this.SelectedIndexChanged -= TabControlAdvWorkspace_SelectedIndexChanged;

            int hiddenTabPages = 0;
            bool flag = false;

            if (this.SelectedIndex <= this.TabPages.Count - 1)
            {
                for (int i = 0; i < this.TabPages.IndexOf(this.SelectedTab); i++)
                {
                    if (this.TabPages[i].TabVisible)
                    {
                        this.SelectedIndex = i;
                        flag = true;
                    }
                    else
                        hiddenTabPages++;
                }
                if (!flag)
                {
                    /*
                     *  One =>      |CurrentSelectedIndex| - this item is hidden
                     *  Two =>      |TabVisible = true|
                     *  Three =>   |TabVisible = true| --- After performing a reverse loop, this item would be set in the <SelectedIndex>.
                     *  Four =>     |TabVisible = false|
                    */
                    for (int i = this.TabPages.Count - 1; i >= 0; i--)
                    {
                        if (this.TabPages[i].TabVisible)
                        {
                            this.SelectedIndex = i;
                            //since it is getting set from reverse direction break the loop
                            break;
                        }
                        else
                            hiddenTabPages++;
                    }
                }
            }

            if (hiddenTabPages == this.TabPages.Count)
            {
                this.SelectedIndexChanged += TabControlAdvWorkspace_SelectedIndexChanged;
                return false;
            }

            this.SelectedIndexChanged += TabControlAdvWorkspace_SelectedIndexChanged;
            return true;
        }

        /// <summary>
        /// Shows the control.
        /// </summary>
        /// <param name="smartPart"></param>
        /// <param name="smartPartInfo"></param>
        protected virtual void OnShowSmartPart(Control smartPart, TabControlAdvSmartPartInfo smartPartInfo)
        {
            this.SelectedIndexChanged -= new EventHandler(TabControlAdvWorkspace_SelectedIndexChanged);
            PopulatePages();
            ResetSelectedIndexIfNoTabs();

            TabPageAdv page = GetOrCreateTabPage(smartPart);
            SetTabProperties(page, smartPartInfo, smartPart);

            if (smartPartInfo.ActivateTab || smartPartInfo.Position == TabPosition.Beginning)
            {
                this.SelectedTab = page;
                RaiseSmartPartActivated(new WorkspaceEventArgs(smartPart));
                //Activate(smartPart);
            }
            //if (smartPartInfo.Position == TabPosition.Beginning)
            //    Activate(smartPart);

            this.SelectedIndexChanged += new EventHandler(TabControlAdvWorkspace_SelectedIndexChanged);
            smartPart.Disposed += ControlDisposed;
        }

        /// <summary>
        /// Raises the <see cref="SmartPartActivated"/> event.
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnSmartPartActivated(WorkspaceEventArgs e)
        {
            if (SmartPartActivated != null)
            {
                SmartPartActivated(this, e);
            }
        }

        private void TabControlAdvWorkspace_SelectedIndexChanged(object sender, EventArgs e)
        {
            OnSmartPartActivated(new WorkspaceEventArgs(GetControlFromSelectedPage()));
        }



        /// <summary>
        /// Raises the <see cref="SmartPartClosing"/> event.
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnSmartPartClosing(WorkspaceCancelEventArgs e)
        {
            if (SmartPartClosing != null)
            {
                SmartPartClosing(this, e);
            }
        }

        /// <summary>
        /// Converts a smart part information to a compatible one for the workspace.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public TabControlAdvSmartPartInfo ConvertFrom(ISmartPartInfo source)
        {
            return SmartPartInfo.ConvertTo<TabControlAdvSmartPartInfo>(source);
        }

        # endregion //IComposable related Methods

        #region TabControlAdv Properties

        private void PopulatePages()
        {
            // If the page count matches don't waste the 
            // time repopulating the pages collection
            if (this.TabPages != null)
            {
                if (!populatingPages && pages.Count != this.TabPages.Count)
                {
                    foreach (TabPageAdv page in this.TabPages)
                    {
                        if (this.pages.ContainsValue(page) == false)
                        {
                            Control control = GetControlFromPage(page);
                            if (control != null)
                            {
                                //changes - Add the control in the pages collection
                                pages.Add(control, page);
                                TabControlAdvSmartPartInfo tabinfo = new TabControlAdvSmartPartInfo();
                                tabinfo.ActivateTab = false;
                                // Avoid circular calls to this method.
                                populatingPages = true;
                                try
                                {
                                    Show(control, tabinfo);
                                    //changes - Making the current page as the selected one.
                                    this.SelectedTab = page;
                                }
                                finally
                                {
                                    populatingPages = false;
                                }
                            }
                        }
                    }
                }
            }
        }

        private TabPageAdv GetOrCreateTabPage(Control smartPart)
        {
            TabPageAdv page = null;

            // If the tab was added with the control at design-time, it will have a parent control, 
            // and somewhere up its containment chain we'll find one of our tabs.
            Control current = smartPart;
            while (current != null && page == null)
            {
                current = current.Parent;
                page = current as TabPageAdv;
            }

            if (page == null)
            {
                page = new TabPageAdv();
                page.Controls.Add(smartPart);
                smartPart.Dock = DockStyle.Fill;
                page.Name = Guid.NewGuid().ToString();
                pages.Add(smartPart, page);
            }
            else if (pages.ContainsKey(smartPart) == false)
            {
                pages.Add(smartPart, page);
            }

            return page;
        }

        private void SetTabProperties(TabPageAdv page, TabControlAdvSmartPartInfo smartPartInfo, Control smartPart)
        {
            page.Text = String.IsNullOrEmpty(page.Text) ? smartPartInfo.Title : page.Text;
            try
            {
                TabPageAdv currentSelection = this.SelectedTab;
                callComposerActivateOnIndexChange = false;
                if (smartPartInfo.Position == TabPosition.Beginning)
                {
                    TabPageAdv[] pages = GetTabPages();
                    this.TabPages.Clear();

                    this.TabPages.Add(page);
                    this.TabPages.AddRange(pages);
                }
                else if (this.TabPages.Contains(page) == false || smartPartInfo.Position == TabPosition.End)
                {
                    TabPageAdv[] pages = GetTabPages();
                    this.TabPages.Clear();

                    this.TabPages.AddRange(pages);
                    this.TabPages.Add(page);
                }



                this.HotTrack = smartPartInfo.HotTrack;
                this.LabelEdit = smartPartInfo.LabelEdit;
                this.ShowScroll = smartPartInfo.ShowScroll;
                this.ThemesEnabled = smartPartInfo.ThemesEnabled;
                this.UserMoveTabs = smartPartInfo.UserMoveTabs;
                this.KeepSelectedTabInFrontRow = smartPartInfo.KeepSelectedTabInFrontRow;
                this.RotateTabsWhenRTL = smartPartInfo.RotateTabsWhenRTL;
                this.RotateTextWhenVertical = smartPartInfo.RotateTextWhenVertical;
                this.Multiline = smartPartInfo.MultiLine;
                this.TabPrimitivesHost.Visible = smartPartInfo.TabPrimitiveVisible;
                this.TabPrimitivesHost.Alignment = smartPartInfo.TabPrimitiveHostAlignment;

                if (smartPartInfo.TabGap.HasValue)
                {
                    this.TabGap = smartPartInfo.TabGap.Value;
                    this.tTabGap = smartPartInfo.TabGap.Value;
                }
                else
                    this.TabGap = tTabGap;

                if (smartPartInfo.TabStyle != null)
                {
                    this.TabStyle = smartPartInfo.TabStyle;
                    this.tTabStyle = smartPartInfo.TabStyle;
                }
                else
                    this.TabStyle = tTabStyle;

                if (smartPartInfo.Alignment.HasValue)
                {
                    this.Alignment = smartPartInfo.Alignment.Value;
                    tAlignment = smartPartInfo.Alignment.Value;
                }
                else
                    this.Alignment = tAlignment;

                if (smartPartInfo.ScrollIncrement.HasValue)
                {
                    this.ScrollIncrement = smartPartInfo.ScrollIncrement.Value;
                    tScrollIncrement = smartPartInfo.ScrollIncrement.Value;
                }
                else
                    this.ScrollIncrement = tScrollIncrement;

                if (smartPartInfo.SizeMode.HasValue)
                {
                    this.SizeMode = smartPartInfo.SizeMode.Value;
                    tSizeMode = smartPartInfo.SizeMode.Value;
                }
                else
                    this.SizeMode = tSizeMode;

                AddCustomTabPrimitives(smartPartInfo);

                // Preserve selection through the operation.
                if (currentSelection != null && this.SelectedTab == null)
                    this.SelectedTab = currentSelection;
            }
            finally
            {
                callComposerActivateOnIndexChange = true;
            }
        }

        private void AddCustomTabPrimitives(TabControlAdvSmartPartInfo smartPartInfo)
        {
            if (smartPartInfo.TabPrimitives != null && smartPartInfo.TabPrimitives.Length != 0)
            {
                foreach (TabPrimitiveInfo customTabPrimitive in smartPartInfo.TabPrimitives)
                {
                    if (customTabPrimitive.UseInTab && customTabPrimitive.TabPrimitive != null)
                        this.TabPrimitivesHost.TabPrimitives.Add(customTabPrimitive.TabPrimitive);
                    else if (!customTabPrimitive.UseInTab && customTabPrimitive.TabPrimitive != null && this.TabPrimitivesHost.TabPrimitives.Contains(customTabPrimitive.TabPrimitive))
                        this.TabPrimitivesHost.TabPrimitives.Remove(customTabPrimitive.TabPrimitive);
                }
            }
        }

        private TabPageAdv[] GetTabPages()
        {
            TabPageAdv[] pages = new TabPageAdv[this.TabPages.Count];
            for (int i = 0; i < pages.Length; i++)
            {
                pages[i] = this.TabPages[i];
            }

            return pages;
        }

        private void ShowExistingTab(Control smartPart)
        {
            int key = GetTabPageIndex(smartPart);
            this.TabPages[key].Show();
        }

        private Control GetControlFromSelectedPage()
        {
            return GetControlFromPage(this.SelectedTab);
        }

        private Control GetControlFromPage(TabPageAdv page)
        {
            Control control = null;
            if (page != null)
            {
                if (page.Controls.Count > 0)
                {
                    control = page.Controls[0];
                }
            }

            return control;
        }

        /*protected override void OnCreateControl()
        {
            base.OnCreateControl();
            PopulatePages();
        }*/

        private void ActivateSiblingTab()
        {
            if (this.SelectedIndex > 0)
            {
                this.SelectedIndex = this.SelectedIndex - 1;
                if (this.SelectedTab != null)
                    composer.SetActiveSmartPart(this.GetControlFromPage(this.SelectedTab));
            }
            else if (this.SelectedIndex < this.TabPages.Count - 1)
            {
                this.SelectedIndex = this.SelectedIndex + 1;
                if (this.SelectedTab != null)
                    composer.SetActiveSmartPart(this.GetControlFromPage(this.SelectedTab));
            }
            else
            {
                composer.SetActiveSmartPart(null);
            }
        }


        private void ResetSelectedIndexIfNoTabs()
        {
            // First control to come in is special. We need to 
            // set the selected index to a non-zero index so we 
            // get the appropriate behavior for activation.
            if (this.TabPages.Count == 0)
            {
                try
                {
                    callComposerActivateOnIndexChange = false;
                    this.SelectedIndex = -1;
                }
                finally
                {
                    callComposerActivateOnIndexChange = true;
                }
            }
        }

        #endregion

        protected override void OnHandleCreated(EventArgs e)
        {
            base.OnHandleCreated(e);
            PopulatePages();
        }

        #region IComposableWorkspace<Control,CustomSPI> Members

        /// <summary>
        /// Called when [activate].
        /// </summary>
        /// <param name="smartPart">The smart part.</param>
        public void OnActivate(Control smartPart)
        {
            this.SelectedIndexChanged -= new EventHandler(TabControlAdvWorkspace_SelectedIndexChanged);
            OnActivateSmartPart(smartPart);
            this.SelectedIndexChanged += new EventHandler(TabControlAdvWorkspace_SelectedIndexChanged);
        }

        /// <summary>
        /// Called when [apply smart part info].
        /// </summary>
        /// <param name="smartPart">The smart part.</param>
        /// <param name="smartPartInfo">The smart part info.</param>
        public void OnApplySmartPartInfo(Control smartPart, TabControlAdvSmartPartInfo smartPartInfo)
        {
            OnApplySmartPartInfoSmartPart(smartPart, smartPartInfo);
        }

        /// <summary>
        /// Called when [close].
        /// </summary>
        /// <param name="smartPart">The smart part.</param>
        public void OnClose(Control smartPart)
        {
            OnCloseSmartPart(smartPart);
        }

        /// <summary>
        /// Called when [hide].
        /// </summary>
        /// <param name="smartPart">The smart part.</param>
        public void OnHide(Control smartPart)
        {
            OnHideSmartPart(smartPart);
        }

        /// <summary>
        /// Called when [show].
        /// </summary>
        /// <param name="smartPart">The smart part.</param>
        /// <param name="smartPartInfo">The smart part info.</param>
        public void OnShow(Control smartPart, TabControlAdvSmartPartInfo smartPartInfo)
        {
            OnShowSmartPart(smartPart, smartPartInfo);
        }

        /// <summary>
        /// Raises the smart part activated.
        /// </summary>
        /// <param name="e">The <see cref="Microsoft.Practices.CompositeUI.SmartParts.WorkspaceEventArgs"/> instance containing the event data.</param>
        public void RaiseSmartPartActivated(WorkspaceEventArgs e)
        {
            OnSmartPartActivated(e);
        }

        /// <summary>
        /// Raises the smart part closing.
        /// </summary>
        /// <param name="e">The <see cref="Microsoft.Practices.CompositeUI.SmartParts.WorkspaceCancelEventArgs"/> instance containing the event data.</param>
        public void RaiseSmartPartClosing(WorkspaceCancelEventArgs e)
        {
            OnSmartPartClosing(e);
        }

        #endregion //IComposableWorkspace Members

        #region IWorkspace Members

        # region Methods

        /// <summary>
        /// Activates the specified smart part within the workspace.
        /// </summary>
        /// <param name="smartPart">Smart part to activate.</param>
        public void Activate(object smartPart)
        {
            composer.Activate(smartPart);
        }

        /// <summary>
        /// Applies the specified smart part info to the specified smart part within the workspace.
        /// </summary>
        /// <param name="smartPart">Smart part to update</param>
        /// <param name="smartPartInfo">Smart part info to apply to the <paramref name="smartPart"/></param>
        public void ApplySmartPartInfo(object smartPart, ISmartPartInfo smartPartInfo)
        {
            composer.ApplySmartPartInfo(smartPart, smartPartInfo);
        }

        /// <summary>
        /// Closes the specified smart part and removes it from the workspace.
        /// </summary>
        /// <param name="smartPart">Smart part to close and remove.</param>
        public void Close(object smartPart)
        {
            composer.Close(smartPart);
        }

        /// <summary>
        /// Hides the specified smart part.
        /// </summary>
        /// <param name="smartPart">Smart part within the workspace that should be hidden.</param>
        public void Hide(object smartPart)
        {
            composer.Hide(smartPart);
        }

        /// <summary>
        /// Shows the specified smart part within the workspace.
        /// </summary>
        /// <param name="smartPart">Smart part to show.</param>
        public void Show(object smartPart)
        {
            composer.Show(smartPart);
        }

        /// <summary>
        /// Shows the smart part within the workspace using the specified smart part info.
        /// </summary>
        /// <param name="smartPart">Smart part that should be displayed</param>
        /// <param name="smartPartInfo">Smart part info to applied to the smart part</param>
        public void Show(object smartPart, ISmartPartInfo smartPartInfo)
        {
            composer.Show(smartPart, smartPartInfo);
        }

        # endregion //Methods

        # region Events

        /// <summary>
        /// Invoked when a smart part is activated.
        /// </summary>
        public event EventHandler<WorkspaceEventArgs> SmartPartActivated;

        /// <summary>
        /// Invoked when a smart part is closing.
        /// </summary>
        public event EventHandler<WorkspaceCancelEventArgs> SmartPartClosing;

        # endregion //Events

        # region Properties

        /// <summary>
        /// Returns the currently active smart part within the workspace.
        /// </summary>
        public object ActiveSmartPart
        {
            get { return composer.ActiveSmartPart; }
        }

        /// <summary>
        /// See <see cref="IWorkspace.SmartParts"/> for more information.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public System.Collections.ObjectModel.ReadOnlyCollection<object> SmartParts
        {
            get { return composer.SmartParts; }
        }

        # endregion //Properties

        #endregion //IWorkspace Members
    }
}
