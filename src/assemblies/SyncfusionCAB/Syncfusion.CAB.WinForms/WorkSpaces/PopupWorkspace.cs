using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using Microsoft.Practices.CompositeUI;
using Microsoft.Practices.CompositeUI.SmartParts;
using Syncfusion.Windows.Forms;
using Syncfusion.CAB.WinForms.SmartPartInfos;

namespace Syncfusion.CAB.WinForms.WorkSpaces
{
    /// <summary>
    /// This is a custom workspace inherits Syncfusion's PopupControlContainer.
    /// </summary>
	[ToolboxBitmap(typeof(PopupWorkspace), "ToolboxIcons.PopupWorkspace.bmp")]
    public class PopupWorkspace : PopupControlContainer , IComposableWorkspace<Control, PopupSmartPartInfo>
    {
        #region Fields

        private WorkspaceComposer<Control, PopupSmartPartInfo> _composer;
        
        /// <summary>
        /// This parts field acts like a hastable. It stores the smartpart.
        /// </summary>      
        private Dictionary<string, Control> parts = new Dictionary<string, Control>();        

        private bool _isDisposing = false;

        private PopupSmartPartInfo CabSmartInfo = new PopupSmartPartInfo();

        #endregion

        #region Constructors & Properties

        //Only one constructor. No overloads.
        public PopupWorkspace()
        {            
            _composer = new WorkspaceComposer<Control, PopupSmartPartInfo>(this);                                   
            this.Name = "PopupContainerWorkspace";            
            this.BorderStyle = BorderStyle.FixedSingle;                    
        }

        /// <summary>
        /// Dependency injection setter property to get the <see cref="WorkItem"/> where the 
        /// object is contained.
        /// </summary>
        [ServiceDependency]
        public WorkItem workItem
        {
            set
            {
                _composer.WorkItem = value;
            }
        }
        #endregion

        #region IComposableWorkspace<Control,PopupSmartPartInfo> Members

        PopupSmartPartInfo IComposableWorkspace<Control, PopupSmartPartInfo>.ConvertFrom(ISmartPartInfo source)
        {
            return PopupSmartPartInfo.ConvertTo<PopupSmartPartInfo>(source);
        }

        void IComposableWorkspace<Control, PopupSmartPartInfo>.OnActivate(Control smartPart)
        {
            smartPart.BringToFront();

            this.Anchor = AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Bottom | AnchorStyles.Left;

            smartPart.Visible = true;

            smartPart.Show();
        }

        /// <summary>
        /// Called when [apply smart part info].
        /// </summary>
        /// <param name="smartPart">The smart part.</param>
        /// <param name="PopupSmartPartInfo">The CAB smart part info.</param>
        void IComposableWorkspace<Control, PopupSmartPartInfo>.OnApplySmartPartInfo(Control smartPart, PopupSmartPartInfo PopupSmartPartInfo)
        {
            PopulateControls();
            if (this.parts.ContainsValue(smartPart))
            {
                SetSmartPartBehaviour(smartPart, PopupSmartPartInfo);
            }
        }

        /// <summary>
        /// When overridden in a derived class, closes and removes the smartPart
        /// from the workspace.
        /// </summary>
        /// <param name="smartPart">The smart part to close.</param>
        void IComposableWorkspace<Control, PopupSmartPartInfo>.OnClose(Control smartPart)
        {
            if (smartPart != null && this.Controls.Contains(smartPart))
            {
                parts.Remove(smartPart.Name);
                this.Controls.Remove(smartPart);
                smartPart.Disposed -= ControlDisposed;
            }
        }

        /// <summary>
        /// Controls the disposed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ControlDisposed(object sender, EventArgs e)
        {
            Control Control = sender as Control;

            if (_isDisposing == false && Control != null)
            {
                _composer.ForceClose(Control);
            }
        }

        /// <summary>
        /// When overridden in a derived class, hides the smartPart
        /// on the workspace.
        /// </summary>
        /// <param name="smartPart">The smart part to hide.</param>
        void IComposableWorkspace<Control, PopupSmartPartInfo>.OnHide(Control smartPart)
        {
            smartPart.Visible = false;
            smartPart.SendToBack();            
        }

        /// <summary>
        /// Called when [show].
        /// </summary>
        /// <param name="smartPart">The smart part.</param>
        /// <param name="PopupSmartPartInfo">The popup smart part info.</param>
        void IComposableWorkspace<Control, PopupSmartPartInfo>.OnShow(Control smartPart, PopupSmartPartInfo PopupSmartPartInfo)
        {
            Control sp = smartPart;
            CabSmartInfo = PopupSmartPartInfo;
            // check if we already have the part in our container...
            if (!parts.ContainsKey(sp.Name))
            {
                // if we don't have it in our container... add it..
                parts.Add(sp.Name, sp);
                this.Controls.Add(smartPart);
            }
            
            //Set the smartpart properties using custom smartpartinfo.
            SetSmartPartBehaviour(smartPart, PopupSmartPartInfo);
            sp.Dock = DockStyle.Fill;
            sp.Visible = true;            
            smartPart.Disposed += ControlDisposed;
            _composer.Activate(smartPart);
        }

        /// <summary>
        /// Raises the <see cref="IWorkspace.SmartPartActivated"/> event on the composed workspace.
        /// </summary>
        /// <param name="e">The <see cref="EventArgs"/> to use for the event.</param>
        void IComposableWorkspace<Control, PopupSmartPartInfo>.RaiseSmartPartActivated(WorkspaceEventArgs e)
        {
            if (SmartPartActivated != null)            {

                SmartPartActivated(this, e);
            }

        }

        /// <summary>
        /// Raises the <see cref="IWorkspace.SmartPartClosing"/> event on the composed workspace.
        /// </summary>
        /// <param name="e">The <see cref="EventArgs"/> to use for the event.</param>
        void IComposableWorkspace<Control, PopupSmartPartInfo>.RaiseSmartPartClosing(WorkspaceCancelEventArgs e)
        {
            if (SmartPartClosing != null)
            {
                SmartPartClosing(this, e);
            }
        }

        #endregion

        #region IWorkspace Members

        /// <summary>
        /// Activates the smartPart on the workspace.
        /// </summary>
        /// <param name="smartPart">The smart part to activate.</param>
        public void Activate(object smartPart)
        {
            _composer.Activate(smartPart);
        }

        /// <summary>
        /// The currently active smart part.
        /// </summary>
        /// <value></value>
        public object ActiveSmartPart
        {
            get { return _composer.ActiveSmartPart; }
        }

        /// <summary>
        /// Applies the smart part info.
        /// </summary>
        /// <param name="smartPart">The smart part.</param>
        /// <param name="PopupSmartPartInfo">The popup smart part info.</param>
        public void ApplySmartPartInfo(object smartPart, ISmartPartInfo PopupSmartPartInfo)
        {
            _composer.ApplySmartPartInfo(smartPart, PopupSmartPartInfo);
        }

        /// <summary>
        /// Closes a smart part. Disposing the smart part is the responsibility of the caller.
        /// </summary>
        /// <param name="smartPart">Smart part to close.</param>
        public void Close(object smartPart)
        {
            try
            {
                if (smartPart != null)
                {
                    ThrowIfNotInContainer(smartPart as Control);
                    _composer.Close(smartPart);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        /// <summary>
        /// Hides a smart part from the UI.
        /// </summary>
        /// <param name="smartPart">Smart part to hide.</param>
        public void Hide(object smartPart)
        {
            try
            {
                if (this.Controls.Contains(smartPart as Control))
                    _composer.Hide(smartPart);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        /// <summary>
        /// Shows a smart part in the UI.
        /// </summary>
        /// <param name="smartPart">Smart part to show.</param>
        public void Show(object smartPart)
        {
            if(smartPart!=null)
            _composer.Show(smartPart);
        }

        /// <summary>
        /// Shows the specified smart part.
        /// </summary>
        /// <param name="smartPart">The smart part.</param>
        /// <param name="PopupSmartPartInfo">The popup smart part info.</param>
        public void Show(object smartPart, ISmartPartInfo PopupSmartPartInfo)
        {
            _composer.Show(smartPart, PopupSmartPartInfo);
        }

        public event EventHandler<WorkspaceEventArgs> SmartPartActivated;

        public event EventHandler<WorkspaceCancelEventArgs> SmartPartClosing;

        public ReadOnlyCollection<object> SmartParts
        {
            get { return _composer.SmartParts; }
        }

        #endregion

        #region PopupWorkspace Methods

        /// <summary>
        /// Set the smartpart properties, such as the container bgcolor and smartpart's forecolor.
        /// </summary>
        /// <param name="smartpart">It is a control that acts as a view</param>
        /// <param name="PopupSmartPartInfo">The popup smart part info.</param>
        private void SetSmartPartBehaviour(Control smartpart, PopupSmartPartInfo PopupSmartPartInfo)
        {
            this.BackColor = PopupSmartPartInfo.BGColor;
            this.ForeColor = PopupSmartPartInfo.FGColor;
        }

        /// <summary>
        /// Hooks up the controls added at design-time.
        /// </summary>
        protected override void OnCreateControl()
        {
            base.OnCreateControl();
            PopulateControls();
        }

        /// <summary>
        /// Generate the addition and display of the design-time controls in the workspace
        /// </summary>
        private void PopulateControls()
        {
            if (parts.Count != this.Controls.Count)
            {
                CabSmartInfo.BGColor = Color.LightBlue;
                CabSmartInfo.FGColor = Color.Red;
                foreach (Control ctrl in this.Controls)
                {
                    if (this.parts.ContainsValue(ctrl) == false)
                    {                        
                        Show(ctrl,CabSmartInfo);
                    }
                }
            }
        }

        private void ThrowIfNotInContainer(Control spcontrol)
        {
            if (parts.ContainsValue(spcontrol) == false)
            {
                throw new ArgumentException("SmartPart does not exist in the container, make sure it has been shown before closing/hiding it");
            }
        }

        #endregion
    }
}
