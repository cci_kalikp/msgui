using System;
using System.Collections.Generic;
using System.Text;

using Syncfusion.Windows.Forms.Tools;

using Microsoft.Practices.CompositeUI.UIElements;
using Microsoft.Practices.CompositeUI.Utility;

namespace Syncfusion.CAB.WinForms.UIElements
{
    public class RibbonControlItemAdapterFactory : IUIElementAdapterFactory
    {

        #region IUIElementAdapterFactory Members

        /// <summary>
        /// returns the appropriate adapter for the object
        /// </summary>
        /// <param name="uiElement"></param>
        /// <returns></returns>
        public IUIElementAdapter GetAdapter(object uiElement)
        {
            Guard.ArgumentNotNull(uiElement, "uiElement");
            if (uiElement is RibbonControlAdv)
                return new RibbonControlItemCollectionAdapter(uiElement as RibbonControlAdv);
            else if (uiElement is RibbonPanel)
                return new RibbonPanelItemCollectionAdapter(uiElement as RibbonPanel);
            throw new ArgumentException("uiElement");
        }

        /// <summary>
        /// Checks the support to the object passed
        /// </summary>
        /// <param name="uiElement"></param>
        /// <returns></returns>
        public bool Supports(object uiElement)
        {
            if (uiElement is RibbonControlAdv)
                return true;
            else if (uiElement is RibbonPanel)
                return true;
            return false;
        }

        #endregion
    }
}
