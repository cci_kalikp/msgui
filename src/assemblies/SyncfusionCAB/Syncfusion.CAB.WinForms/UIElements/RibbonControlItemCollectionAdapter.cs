using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.CompositeUI;
using Microsoft.Practices.CompositeUI.UIElements;
using Microsoft.Practices.CompositeUI.Commands;
using Microsoft.Practices.CompositeUI.Utility;

using Syncfusion.Windows.Forms.Tools;

namespace Syncfusion.CAB.WinForms.UIElements
{
    public class RibbonControlItemCollectionAdapter : UIElementAdapter<ToolStripTabItem>
    {
        private RibbonControlAdv ribbonControlAdv;

        /// <summary>
        /// Initializes a new instance of this class
        /// </summary>
        /// <param name="ribbonControlAdv"></param>
        public RibbonControlItemCollectionAdapter(RibbonControlAdv ribbonControlAdv)
        {
            this.ribbonControlAdv = ribbonControlAdv;
        }

        #region UIElementAdapter Members

        /// <summary>
        /// adds the ToolStripTabItem to the RibbonControl
        /// </summary>
        /// <param name="uiElement"></param>
        /// <returns></returns>
        protected override ToolStripTabItem Add(ToolStripTabItem uiElement)
        {
            Guard.ArgumentNotNull(uiElement, "uiElement");
            this.ribbonControlAdv.Header.AddMainItem(uiElement);
            return uiElement;
        }

        /// <summary>
        /// RibbonControl automatically disposes the ToolStripTabItems once when it is disposed
        /// </summary>
        /// <param name="uiElement"></param>
        protected override void Remove(ToolStripTabItem uiElement)
        {
            Guard.ArgumentNotNull(uiElement, "uiElement");
        }

        #endregion

    }

}
