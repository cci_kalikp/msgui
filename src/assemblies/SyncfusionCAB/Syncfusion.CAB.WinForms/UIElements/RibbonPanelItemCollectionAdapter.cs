using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Practices.CompositeUI;
using Microsoft.Practices.CompositeUI.UIElements;
using Microsoft.Practices.CompositeUI.Commands;
using Microsoft.Practices.CompositeUI.Utility;

using Syncfusion.Windows.Forms.Tools;

namespace Syncfusion.CAB.WinForms.UIElements
{
    public class RibbonPanelItemCollectionAdapter : UIElementAdapter<ToolStripEx>
    {
        private RibbonPanel ribbonPanel;

        /// <summary>
        /// initializes a new instance of this class
        /// </summary>
        /// <param name="ribbonPanel"></param>
        public RibbonPanelItemCollectionAdapter(RibbonPanel ribbonPanel)
        {
            this.ribbonPanel = ribbonPanel;
        }

        #region UIElement Adapter members
        /// <summary>
        /// adds the ToolStripEx object to the ribbonPanel object
        /// </summary>
        /// <param name="uiElement"></param>
        /// <returns></returns>
        protected override ToolStripEx Add(ToolStripEx uiElement)
        {
            Guard.ArgumentNotNull(uiElement, "uiElement");
            this.ribbonPanel.Controls.Add(uiElement);
            return uiElement;
        }

        /// <summary>
        /// removes the ToolStripEx object from the ribbonPanel object
        /// </summary>
        /// <param name="uiElement"></param>
        protected override void Remove(ToolStripEx uiElement)
        {
            Guard.ArgumentNotNull(uiElement, "uiElement");
            this.ribbonPanel.Controls.Remove(uiElement);
        }
        
        #endregion

    }
}
