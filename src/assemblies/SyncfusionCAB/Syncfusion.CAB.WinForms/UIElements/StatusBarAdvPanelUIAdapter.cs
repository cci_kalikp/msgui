using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

using Microsoft.Practices.CompositeUI.UIElements;
using Microsoft.Practices.CompositeUI.Utility;

using Syncfusion.Windows.Forms.Tools;

namespace Syncfusion.CAB.WinForms.UIElements
{
    /// <summary>
    /// An adapter that wraps a <see cref="StatusBarAdvPanel"/> for use as an <see cref="IUIElementAdapter"/> and uses 
    /// the location of the wrapped item to determine where new items are positioned.
    /// </summary>
    public class StatusBarAdvPanelUIAdapter: StatusBarAdvUIAdapter
    {
        # region Member Variables

        private StatusBarAdvPanel statusBarAdvPanel;

        # endregion //Member Variables

        # region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="StatusBarAdvPanelUIAdapter"/> class.
        /// </summary>
        /// <param name="uiElement">Panel whose owning collection will be updated with any added elements.</param>
        public StatusBarAdvPanelUIAdapter(StatusBarAdvPanel uiElement)
            : base((Control)uiElement)
        {
            Guard.ArgumentNotNull(uiElement, "uiElement");

            this.statusBarAdvPanel = (StatusBarAdvPanel)uiElement;
        }

        # endregion //Constructor
    }
}
