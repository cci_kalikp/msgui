using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

using Microsoft.Practices.CompositeUI.UIElements;
using Microsoft.Practices.CompositeUI;

using Syncfusion.Windows.Forms.Tools;


namespace Syncfusion.CAB.WinForms.UIElements
{
    /// <summary>
    /// A <see cref="IUIElementAdapterFactory"/> that produces adapters for the UIElements associated with the StatusBarAdv.
    /// </summary>
    public class StatusBarAdvUIAdapterFactory : IUIElementAdapterFactory
    {
        #region IUIElementAdapterFactory Members

        # region GetAdapter
        /// <summary>
        /// Returns a <see cref="IUIElementAdapter"/> for the specified uielement.
        /// </summary>
        /// <param name="uiElement">StatusBarAdv for which to return a <see cref="IUIElementAdapter"/>.</param>
        /// <returns>A <see cref="IUIElementAdapter"/> that represents the specified <see cref="StatusBarAdv"/>.</returns>
        public IUIElementAdapter GetAdapter(object uiElement)
        {
            if (uiElement is StatusBarAdv)
                return new StatusBarAdvUIAdapter((StatusBarAdv)uiElement);
            if (uiElement is StatusBarAdvPanel)
                return new StatusBarAdvPanelUIAdapter((StatusBarAdvPanel)uiElement);

            throw new ArgumentException("uiElement");
        }
        # endregion //GetAdapter

        #region Supports
        /// <summary>
        /// Indicates if the specified uielement is supported by the adapter factory.
        /// </summary>
        /// <param name="uiElement">The UI element.</param>
        /// <returns>Returns true if the element is StatusBarAdv.</returns>
        public bool Supports(object uiElement)
        {
            return uiElement is StatusBarAdv || uiElement is StatusBarAdvPanel;
        }
        # endregion //Supports

        #endregion //IUIElementAdapterFactory Members
    }
}
