using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

using Microsoft.Practices.CompositeUI;
using Microsoft.Practices.CompositeUI.Utility;
using Microsoft.Practices.CompositeUI.UIElements;

using Syncfusion.Windows.Forms.Tools;


namespace Syncfusion.CAB.WinForms.UIElements
{
    /// <summary>
    /// An adapter that wraps the StatusBarAdv for the use as an <see cref="IUIElementAdapter"/>.
    /// </summary>
    public class StatusBarAdvUIAdapter : UIElementAdapter<Control>
    {
        # region MemberVariables

        private StatusBarAdv statusbaradv;

        # endregion //Member Variables

        # region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="StatusBarAdv"/> class using the specified item.
        /// </summary>
        /// <param name="uiElement">StatusBarAdv represented by the adapter.</param>
        public StatusBarAdvUIAdapter(Control uiElement)
        {
            Guard.ArgumentNotNull(uiElement, "uiElement");
            this.statusbaradv = (StatusBarAdv)uiElement;
        }

        #endregion //Constructor

        # region Properties

        /// <summary>
        /// The StatusBarAdv represented by the adapter.
        /// </summary>
        protected StatusBarAdv StatusBarAdv
        {
            get { return this.statusbaradv; }
        }

        # endregion //Properties

        # region Methods

        # region Add

        /// <summary>
        /// Adds an control to the <see cref="StatusBarAdv"/> associated with the adapter.
        /// </summary>
        /// <param name="uiElement">The control to be added to the StatusBarAdv.</param>
        /// <returns>Returns the item that was added.</returns>
        protected override Control Add(Control uiElement)
        {
            this.statusbaradv.Controls.Add(uiElement);
            return uiElement;
        }

        # endregion //Add

        # region Remove

        /// <summary>
        /// Removes the specified control from the associated <see cref="StatusBarAdv"/>
        /// </summary>
        /// <param name="uiElement">The control to be removed from the StatusBarAdv.</param>
        protected override void Remove(Control uiElement)
        {
            if (this.statusbaradv.Disposing == false)
                this.statusbaradv.Controls.Remove(uiElement);
        }

        # endregion //Remove

        # endregion //Methods
    }
}
