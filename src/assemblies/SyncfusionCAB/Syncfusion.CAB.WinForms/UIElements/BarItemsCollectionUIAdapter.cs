using System;
using System.Collections.Generic;
using System.Text;

using Syncfusion.Windows.Forms.Tools;
using Syncfusion.Windows.Forms.Tools.XPMenus;

using Microsoft.Practices.CompositeUI.Utility;
using Microsoft.Practices.CompositeUI.UIElements;

namespace Syncfusion.CAB.WinForms.UIElements
{
    /// <summary>
    /// An adapter that wraps the <see cref="BarItems"/> for the use of an <see cref="IUIElementAdapter"/>.
    /// </summary>
    public class BarItemsCollectionUIAdapter : UIElementAdapter<BarItem>
    {
        # region Member Variables

        private BarItems barItems;

        private BarManager barManager;

        # endregion // Member Variables

        # region Constructor

        /// <summary>
        /// Initializes a new <see cref="BarItemsCollectionUIAdapter"/> class.
        /// </summary>
        /// <param name="barManager">Bar Manager represented by the UI adapter.</param>
        /// <param name="barItems">Items collection represented by the UI adapter.</param>
        public BarItemsCollectionUIAdapter(BarManager barManager, BarItems barItems)
        {
            Guard.ArgumentNotNull(barManager, "barManager");
            Guard.ArgumentNotNull(barItems, "barItems");

            this.barItems = barItems;
            this.barManager = barManager;
        }

        # endregion //Constructor

        # region Methods

        # region Add

        /// <summary>
        /// Adds a <see cref="BarItem"/> to the <see cref="BarManager"/> associated with the adapter.
        /// </summary>
        /// <param name="uiElement">The BarItem to add.</param>
        /// <returns>The added item.</returns>
        protected override BarItem Add(BarItem uiElement)
        {
            Guard.ArgumentNotNull(uiElement, "uiElement");

            this.barManager.Items.Add(uiElement);
            this.barItems.Insert(this.barItems.Count, uiElement);

            return uiElement;
        }

        # endregion //Add

        # region Remove

        /// <summary>
        /// Removes the specified <see cref="BarItem"/> associated with the adapter.
        /// </summary>
        /// <param name="uiElement">The item to be removed.</param>
        protected override void Remove(BarItem uiElement)
        {
            Guard.ArgumentNotNull(uiElement, "uiElement");

            this.barItems.Remove(uiElement);
        }

        # endregion //Remove

        # endregion //Methods

        # region Properties

        /// <summary>
        /// The <see cref="BarItems"/> collection represented by the adapter.
        /// </summary>
        protected BarItems BarItems
        {
            get { return this.barItems; }
            set { this.barItems = value; }
        }

        /// <summary>
        /// The <see cref="BarManager"/> represented by the adapter.
        /// </summary>
        protected BarManager BarManager
        {
            get { return this.barManager; }
            set { this.barManager = value; }
        }

        # endregion //Properties
    }
}
