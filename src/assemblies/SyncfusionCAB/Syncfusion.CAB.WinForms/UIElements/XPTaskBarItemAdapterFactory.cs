using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

using Syncfusion.Windows.Forms.Tools;

using Microsoft.Practices.CompositeUI.UIElements;
using Microsoft.Practices.CompositeUI.Utility;

namespace Syncfusion.CAB.WinForms.UIElements
{
    public class XPTaskBarItemAdapterFactory : IUIElementAdapterFactory
    {

        #region IUIElementAdapterFactory Members
        /// <summary>
        /// Returns the instance of XPTaskBarItemCollectionAdapter class if it is registered in the CAB Factory
        /// </summary>
        /// <param name="uiElement"></param>
        /// <returns></returns>
        public IUIElementAdapter GetAdapter(object uiElement)
        {
            Guard.ArgumentNotNull(uiElement, "uiElement");

            if (uiElement is XPTaskBar)
                return new XPTaskBarItemCollectionAdapter(uiElement as XPTaskBar);
            else if (uiElement is XPTaskBarBox)
                return new XPTaskBarItemCollectionAdapter(uiElement as XPTaskBarBox);
            else if (uiElement is Panel)
                return new XPTaskBarItemCollectionAdapter();
            throw new ArgumentException("uiElement");
        }

        /// <summary>
        /// Checks the supporting object types related to XPTaskBarItemCollectionAdapter class
        /// </summary>
        /// <param name="uiElement"></param>
        /// <returns></returns>
        public bool Supports(object uiElement)
        {
            return uiElement is XPTaskBar || uiElement is XPTaskBarBox || uiElement is Panel;
        }

        #endregion
    }
}
