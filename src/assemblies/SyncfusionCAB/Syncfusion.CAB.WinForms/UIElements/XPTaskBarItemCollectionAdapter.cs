using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

using Syncfusion.Windows.Forms.Tools;

using Microsoft.Practices.CompositeUI.Utility;
using Microsoft.Practices.CompositeUI.UIElements;

namespace Syncfusion.CAB.WinForms.UIElements
{
    public class XPTaskBarItemCollectionAdapter : IUIElementAdapter
    {
        private XPTaskBar xpTaskBar;
        private XPTaskBarBox xpTaskBarBox;

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the XPTaskBarItemCollectionAdapter class. If XPTaskBar is used to register in the 
        /// UIExtensionSite this constructor should be used.
        /// 
        /// eg:
        /// workitem.UIExtensionSites.RegisterSite("XPTaskBar",xpTaskBarInstance);
        /// xpTaskBarInstance is the XPTaskBar object which is to be registered in the UIExtensionSite
        /// </summary>
        /// <param name="xpTaskBar"></param>
        public XPTaskBarItemCollectionAdapter(XPTaskBar xpTaskBar)
        {
            Guard.ArgumentNotNull(xpTaskBar, "xpTaskBar");
            this.xpTaskBar = xpTaskBar;
        }

        /// <summary>
        /// Initializes a new instance of XPTaskBarItemCollectionAdapter class. If XPTaskBarBox is used to register in the 
        /// UIExtensionSite this constructor should be used.
        /// 
        /// eg:
        /// workitem.UIExtensionSites.RegisterSite("XPTaskBarBox",xpTaskBarBoxInstance);
        /// xpTaskBarBoxInstance is the XPTaskBarBox object which is to be registered in the UIExtensionSite
        /// </summary>
        /// <param name="xpTaskBarBox"></param>
        public XPTaskBarItemCollectionAdapter(XPTaskBarBox xpTaskBarBox)
        {
            Guard.ArgumentNotNull(xpTaskBarBox, "xpTaskBarBox");
            this.xpTaskBar = xpTaskBarBox.Parent as XPTaskBar;
            this.xpTaskBarBox = xpTaskBarBox;
        }

        public XPTaskBarItemCollectionAdapter()
        {
            Guard.ArgumentNotNull(xpTaskBarBox, "xpTaskBarBox");
            this.xpTaskBarBox = new XPTaskBarBox();
        }


        #endregion

        #region IUIElementAdapter Members

        public object Add(object uiElement)
        {
            Guard.ArgumentNotNull(uiElement, "uiElement");
            if (uiElement is XPTaskBarBox)
                this.xpTaskBar.Controls.Add(uiElement as XPTaskBarBox);
            else if (uiElement is XPTaskBarItem)
                this.xpTaskBarBox.Items.Add(uiElement);
            else if (uiElement is Panel)
            {
                this.xpTaskBarBox.Controls.Add(uiElement as Control);
                this.xpTaskBarBox.PreferredChildPanelHeight = ((Panel)uiElement).Height;
            }
            return uiElement;
        }

        public void Remove(object uiElement)
        {
            Guard.ArgumentNotNull(uiElement, "uiElement");
            if (uiElement is XPTaskBarBox)
                this.xpTaskBar.Controls.Remove(uiElement as XPTaskBarBox);
            else if (uiElement is XPTaskBarItem)
                this.xpTaskBarBox.Items.Remove(uiElement);
            else if (uiElement is Panel)
                this.xpTaskBarBox.Controls.Remove(uiElement as Control);
        }

        #endregion
    }
}
