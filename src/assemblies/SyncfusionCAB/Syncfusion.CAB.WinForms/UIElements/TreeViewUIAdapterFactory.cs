using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Practices.CompositeUI.UIElements;
using Microsoft.Practices.CompositeUI.Utility;

using Syncfusion.Windows.Forms.Tools;

namespace Syncfusion.CAB.WinForms.UIElements
{
    /// <summary>
    /// A <see cref="IUIElementAdapterFactory"/> that produces adapters for the UIElements associated with the TreeViewAdv.
    /// </summary>
    public class TreeViewUIAdapterFactory : IUIElementAdapterFactory
    {
        #region IUIElementAdapterFactory Members

        # region GetAdapter

        /// <summary>
        /// Returns a <see cref="IUIElementAdapter"/> for the specified uielement.
        /// </summary>
        /// <param name="uiElement">UIElement for which to return a <see cref="IUIElementAdapter"/>.</param>
        /// <returns>A <see cref="IUIElementAdapter"/> that represents the specified element.</returns>
        public IUIElementAdapter GetAdapter(object uiElement)
        {
            Guard.ArgumentNotNull(uiElement, "uiElement");

            if (uiElement is TreeViewAdv)
                return new TreeNodeAdvCollectionUIAdapter((TreeViewAdv)uiElement,((TreeViewAdv)uiElement).Root);
            if (uiElement is TreeNodeAdv)
                return new TreeNodeAdvUIAdapter((TreeNodeAdv)uiElement);

            throw new ArgumentException("uiElement");
        }

        # endregion //GetAdapter

        # region Supports

        /// <summary>
        /// Indicates if the specified ui element is supported by the adapter factory.
        /// </summary>
        /// <param name="uiElement">The UI Element.</param>
        /// <returns>Returns true for supported elements, otherwise returns false.</returns>
        public bool Supports(object uiElement)
        {
            return uiElement is TreeViewAdv || uiElement is TreeNodeAdv;
        }

        # endregion //Supports

        #endregion //IUIElementAdapterFactory Members
    }
}
