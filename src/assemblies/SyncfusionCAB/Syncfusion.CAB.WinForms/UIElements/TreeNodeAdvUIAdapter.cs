using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Practices.CompositeUI.UIElements;
using Microsoft.Practices.CompositeUI.Utility;

using Syncfusion.Windows.Forms.Tools;

namespace Syncfusion.CAB.WinForms.UIElements
{
    /// <summary>
    /// An adapter that wraps a <see cref="TreeNodeAdv"/> for use as an <see cref="IUIElementAdapter"/> and uses 
    /// the location of the wrapped item to determine where new items are positioned.
    /// </summary>
    public class TreeNodeAdvUIAdapter : TreeNodeAdvCollectionUIAdapter
    {
        # region Member Variables

        private TreeNodeAdv treeNodeAdv;

        # endregion //Member Variables

        # region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TreeNodeAdvUIAdapter"/> class.
        /// </summary>
        /// <param name="treeNodeAdv">Node whose owning collection will be updated with any added elements.</param>
        public TreeNodeAdvUIAdapter(TreeNodeAdv treeNodeAdv)
            :base(treeNodeAdv.TreeView,treeNodeAdv)
		{
            Guard.ArgumentNotNull(treeNodeAdv, "treeNodeAdv");

            this.treeNodeAdv = treeNodeAdv;
        }

        # endregion //Constructor
    }
}
