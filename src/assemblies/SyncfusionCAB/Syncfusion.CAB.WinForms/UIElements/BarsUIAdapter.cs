using System;
using System.Collections.Generic;
using System.Text;

using Syncfusion.Windows.Forms.Tools;
using Syncfusion.Windows.Forms.Tools.XPMenus;

using Microsoft.Practices.CompositeUI.UIElements;
using Microsoft.Practices.CompositeUI.Utility;

namespace Syncfusion.CAB.WinForms.UIElements
{
    /// <summary>
    /// Adapter that wraps the <see cref="Bars"/> collection for use as an <see cref="IUIElementAdapter"/>.
    /// </summary>
    public class BarsUIAdapter : UIElementAdapter<Bar>
    {
        # region Member Variables

        private Bars bars;

        # endregion //Member Variables

        # region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="BarsUIAdapter"/> class.
        /// </summary>
        /// <param name="bars">The Bars collection represented by the UI Adapter.</param>
        public BarsUIAdapter(Bars bars)
        {
            Guard.ArgumentNotNull(bars, "bars");
            this.bars = bars;
        }

        #endregion //Constructor

        # region Methods

        # region Add
        /// <summary>
        /// Adds a <see cref="Bar"/> to the <see cref="Bars"/> collection associated with the adapter.
        /// </summary>
        /// <param name="uiElement">Bar item to add.</param>
        /// <returns>The added item.</returns>
        protected override Bar Add(Bar uiElement)
        {
            this.bars.Add(uiElement);
            return uiElement;
        }

        # endregion //Add

        # region Remove

        /// <summary>
        /// Removes the specified <see cref="Bar"/> from the associated <see cref="Bars"/> collection.
        /// </summary>
        /// <param name="uiElement">The item to be removed.</param>
        protected override void Remove(Bar uiElement)
        {
            this.bars.Remove(uiElement);
        }

        #endregion //Remove

        # endregion //Methods

        # region Properties

        /// <summary>
        /// The <see cref="Bars"/> collection represented by the adapter.
        /// </summary>
        protected Bars BarsCollection
        {
            get { return this.bars; }
            set { this.bars = value; }
        }

        # endregion //Properties
    }
}
