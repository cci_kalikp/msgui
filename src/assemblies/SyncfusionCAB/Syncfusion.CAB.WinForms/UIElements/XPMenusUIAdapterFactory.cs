using System;
using System.Collections.Generic;
using System.Text;

using Syncfusion.Windows.Forms.Tools;
using Syncfusion.Windows.Forms.Tools.XPMenus;

using Microsoft.Practices.CompositeUI.UIElements;
using Microsoft.Practices.CompositeUI.Utility;

namespace Syncfusion.CAB.WinForms.UIElements
{
    /// <summary>
    /// A <see cref="IUIElementAdapterFactory"/> that produces adapters for the UIElements associated with the XPMenus.
    /// </summary>
    public class XPMenusUIAdapterFactory : IUIElementAdapterFactory
    {
        #region IUIElementAdapterFactory Members

        # region GetAdapter

        /// <summary>
        /// Returns a <see cref="IUIElementAdapter"/> for the specified uielement.
        /// </summary>
        /// <param name="uiElement">UIElement for which to return a <see cref="IUIElementAdapter"/>.</param>
        /// <returns>A <see cref="IUIElementAdapter"/> that represents the specified element.</returns>
        public IUIElementAdapter GetAdapter(object uiElement)
        {
            Guard.ArgumentNotNull(uiElement, "uiElement");

            if (uiElement is BarManager)
                return new BarsUIAdapter(((BarManager)uiElement).Bars);
            if (uiElement is Bar)
                return new BarItemsCollectionUIAdapter(((Bar)uiElement).Manager, ((Bar)uiElement).Items);
            if (uiElement is ParentBarItem)
                return new BarItemsCollectionUIAdapter(((ParentBarItem)uiElement).Manager, ((ParentBarItem)uiElement).Items);
            if (uiElement is BarItem)
                return new BarItemsCollectionUIAdapter(((BarItem)uiElement).Manager, ((BarItem)uiElement).Manager.Items);
            
            throw new ArgumentException("uiElement");
        }

        # endregion //GetAdapter

        # region Supports

        /// <summary>
        /// Indicates if the specified ui element is supported by the adapter factory.
        /// </summary>
        /// <param name="uiElement">The UIElement.</param>
        /// <returns>Returns true for supported elements, otherwise returns false.</returns>
        public bool Supports(object uiElement)
        {
            return uiElement is BarManager || uiElement is Bar || uiElement is ParentBarItem || uiElement is BarItem;
        }

        # endregion //Supports

        #endregion //IUIElementAdapterFactory Members
    }
}
