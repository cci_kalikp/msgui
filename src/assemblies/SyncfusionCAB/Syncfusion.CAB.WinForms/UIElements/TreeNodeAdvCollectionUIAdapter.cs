using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Practices.CompositeUI.Utility;
using Microsoft.Practices.CompositeUI.UIElements;

using Syncfusion.Windows.Forms.Tools;

namespace Syncfusion.CAB.WinForms.UIElements
{
    /// <summary>
    /// An adapter that wraps <see cref="TreeNodeAdv"/> for the use of an <see cref="IUIElementAdapter"/>.
    /// </summary>
    public class TreeNodeAdvCollectionUIAdapter : UIElementAdapter<TreeNodeAdv>
    {
        # region Member Variables

        private TreeViewAdv treeViewAdv;

        private TreeNodeAdv treeNodeAdv;

        # endregion //Member Variables

        # region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TreeNodeAdvCollectionUIAdapter"/> class.
        /// </summary>
        /// <param name="treeViewAdv">The <see cref="TreeViewAdv"/> represented by the UI adapter.</param>
        /// <param name="treeNodeAdv">The <see cref="TreeNodeAdv"/> represented by the UI adapter.</param>
        public TreeNodeAdvCollectionUIAdapter(TreeViewAdv treeViewAdv,TreeNodeAdv treeNodeAdv)
        {
            this.treeViewAdv = treeViewAdv;
            this.treeNodeAdv = treeNodeAdv;
        }

        # endregion //Constructor

        # region Methods

        # region Add

        /// <summary>
        /// Adds a <see cref="TreeNodeAdv"/> to the collection associated with the adapter.
        /// </summary>
        /// <param name="uiElement">The node to add.</param>
        /// <returns>The added node.</returns>
        protected override TreeNodeAdv Add(TreeNodeAdv uiElement)
        {
            Guard.ArgumentNotNull(uiElement, "uiElement");
            this.treeNodeAdv.Nodes.Add(uiElement);

            return uiElement;
        }

        # endregion //Add

        # region Remove

        /// <summary>
        /// Removes the specified <see cref="TreeNodeAdv"/> from the associated collection.
        /// </summary>
        /// <param name="uiElement">The item to be removed.</param>
        protected override void Remove(TreeNodeAdv uiElement)
        {
            Guard.ArgumentNotNull(uiElement, "uiElement");

            //this.treeNodeAdv.Nodes.Remove(uiElement);
        }

        # endregion //Remove

        # endregion //Methods

        # region Properties

        /// <summary>
        /// The <see cref="TreeViewAdv"/> that is represented by the adapter.
        /// </summary>
        protected TreeViewAdv TreeViewAdv
        {
            get { return this.treeViewAdv; }
        }

        # endregion //Properties
    }
}
