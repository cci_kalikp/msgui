using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Microsoft.Practices.CompositeUI;

namespace Syncfusion.CAB.WinForms
{
    /// <summary>
    /// A CAB shell application class used to start an application using a specified Form.
    /// </summary>
    /// <typeparam name="TWorkitem">The type of the root application work item.</typeparam>
    /// <typeparam name="TShell">The type of the form for the shell to use.</typeparam>
    public class SyncFormShellApplication<TWorkItem,TShell> : SyncWindowsFormsApplicationBase<TWorkItem,TShell>
        where TWorkItem: WorkItem,new()
        where TShell : Form
    {

        #region Start

        /// <summary>
        /// Calls <see cref="Application.Run(Form)"/> to start the application.
        /// </summary>
        protected override void Start()
        {
            System.Windows.Forms.Application.Run(Shell);
        }

        #endregion //Start
    }
}
