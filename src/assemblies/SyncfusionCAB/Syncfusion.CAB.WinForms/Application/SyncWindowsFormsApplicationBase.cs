using System;
using System.Collections.Generic;
using System.Text;


using Microsoft.Practices.CompositeUI.WinForms;
using Microsoft.Practices.CompositeUI.UIElements;
using Microsoft.Practices.CompositeUI.BuilderStrategies;
using Microsoft.Practices.CompositeUI.Commands;
using Microsoft.Practices.ObjectBuilder;
using Microsoft.Practices.CompositeUI;

using Syncfusion.CAB.WinForms.UIElements;
using Syncfusion.CAB.WinForms.Commands;
using Syncfusion.Windows.Forms.Tools.XPMenus;
using Syncfusion.Windows.Forms.Tools;

namespace Syncfusion.CAB.WinForms
{
    /// <summary>
    /// Defines an abstact cab application which shows a shell based on a Form that uses Syncfusion Essential Tools windows forms components.
    /// </summary>
    /// <typeparam name="TWorkItem">The type of the root application work item.</typeparam>
    /// <typeparam name="TShell">The type for the shell to use.</typeparam>
    public abstract class SyncWindowsFormsApplicationBase<TWorkItem, TShell> : WindowsFormsApplication<TWorkItem, TShell>
        where TWorkItem : WorkItem, new()
    {
        # region Constructor

        /// <summary>
        /// Initializes a new <see cref="SyncWindowsFormsApplicationBase"/>.
        /// </summary>
        protected SyncWindowsFormsApplicationBase()
            : base()
        {
        }

        #endregion //Constructor

        # region AfterShellCreated

        /// <summary>
        /// Refer <see cref="CabShellApplication{T,S}.AfterShellCreated"/>.
        /// </summary>
        protected override void AfterShellCreated()
        {
            base.AfterShellCreated();

            //Registers Command Adapters.
            ICommandAdapterMapService mapService = this.RootWorkItem.Services.Get<ICommandAdapterMapService>();
            mapService.Register(typeof(BarItem), typeof(BarItemCommandAdapter));
            mapService.Register(typeof(TreeNodeAdv), typeof(TreeNodeCommandAdapter));
            mapService.Register(typeof(XPTaskBarItemCollectionAdapter), typeof(XPTaskBarCommandAdapter));
            mapService.Register(typeof(RibbonControlItemCollectionAdapter), typeof(RibbonControlCommandAdapter));

            // Registers UIElement Adapter Factories.
            IUIElementAdapterFactoryCatalog catalog = RootWorkItem.Services.Get<IUIElementAdapterFactoryCatalog>();
            catalog.RegisterFactory(new StatusBarAdvUIAdapterFactory());
            catalog.RegisterFactory(new XPMenusUIAdapterFactory());
            catalog.RegisterFactory(new TreeViewUIAdapterFactory());
            catalog.RegisterFactory(new XPTaskBarItemAdapterFactory());
            catalog.RegisterFactory(new RibbonControlItemAdapterFactory());
        }

        #endregion //After shell created
    }
}
