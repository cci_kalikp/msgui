using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Practices.CompositeUI.Commands;

using Syncfusion.Windows.Forms.Tools.XPMenus;

namespace Syncfusion.CAB.WinForms.Commands
{
    /// <summary>
    /// Class for managing the relationship between a <b>BarItem</b> instances and a <see cref="Command"/>.
    /// </summary>
    public class BarItemCommandAdapter : EventCommandAdapter<BarItem>
    {
        /// <summary>
        /// Initializes a new instance of <see cref="BarItemCommandAdapter"/> class.
        /// </summary>
        public BarItemCommandAdapter()
            : base()
        {
        }

        /// <summary>
        /// Initializes the command adapter with the specified <see cref="BarItem"/>.
        /// </summary>
        /// <param name="item">The BarItem.</param>
        /// <param name="eventName">The name of the event.</param>
        public BarItemCommandAdapter(BarItem item, string eventName)
            : base(item, eventName)
        {
        }
    }
}
