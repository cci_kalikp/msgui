using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Practices.CompositeUI.Commands;

using Syncfusion.Windows.Forms.Tools;

namespace Syncfusion.CAB.WinForms.Commands
{
    /// <summary>
    /// Class for managing the relationship between a <b>TreeNodeAdv</b> instances and a <see cref="Command"/>.
    /// </summary>
    public class TreeNodeCommandAdapter : EventCommandAdapter<TreeNodeAdv>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TreeNodeCommandAdapter"/> class.
        /// </summary>
        public TreeNodeCommandAdapter()
            : base()
        {
        }

        /// <summary>
        /// Initializes a command adapter with the specified <see cref="TreeNodeAdv"/>.
        /// </summary>
        /// <param name="treeNodeAdv">The TreeNodeAdv.</param>
        /// <param name="eventName">The name of the event.</param>
        public TreeNodeCommandAdapter(TreeNodeAdv treeNodeAdv, string eventName)
            : base(treeNodeAdv, eventName)
        {
        }
    }
}
