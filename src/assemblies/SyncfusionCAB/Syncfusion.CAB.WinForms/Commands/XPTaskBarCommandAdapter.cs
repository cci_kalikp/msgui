using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Practices.CompositeUI.Commands;

using Syncfusion.Windows.Forms.Tools;

namespace Syncfusion.CAB.WinForms.Commands
{
    /// <summary>
    /// Class for managing the relationship between a <b>XpTaskBarItem</b> instance and a <see cref="Command"/>.
    /// </summary>
    public class XPTaskBarCommandAdapter : EventCommandAdapter<XPTaskBarItem>
    {
        /// <summary>
        /// Initializes a new instance of <see cref="XpTaskBarItemCommandAdapter"/> class.
        /// </summary>
        public XPTaskBarCommandAdapter()
            : base()
        {

        }

        /// <summary>
        /// Initializes the command adapter with the specified <see cref="XpTaskBarItem"/>.
        /// </summary>
        /// <param name="item">XpTaskBarItem</param>
        /// <param name="eventName">The name of the event.</param>
        public XPTaskBarCommandAdapter(XPTaskBarItem item, string eventName)
            : base(item, eventName)
        {
        }
    }
}
