using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Practices.CompositeUI.Commands;
using Syncfusion.Windows.Forms.Tools;


namespace Syncfusion.CAB.WinForms.Commands
{
    public class RibbonControlCommandAdapter : EventCommandAdapter<RibbonControlAdv>
    {
        /// <summary>
        /// Initializes an instance of the this class
        /// </summary>
        public RibbonControlCommandAdapter()
            : base()
        {
        }

        /// <summary>
        /// Initializes an instance of this class along with the base class to tag the appropriate eventname with the object invoker
        /// </summary>
        /// <param name="ribbonControlAdv"></param>
        /// <param name="eventName"></param>
        public RibbonControlCommandAdapter(RibbonControlAdv ribbonControlAdv, string eventName)
            : base(ribbonControlAdv, eventName)
        {
        }
    }
}
