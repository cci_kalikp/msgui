using System;
using System.Configuration;
using System.Windows.Forms;
using System.IO;
using Microsoft.Practices.CompositeUI;
using Microsoft.Practices.CompositeUI.UIElements;
using Microsoft.Practices.CompositeUI.WinForms;

using Syncfusion.Windows.Forms.Tools;
using Syncfusion.Windows.Forms.Tools.XPMenus;
using System.Globalization;
using Microsoft.Practices.CompositeUI.Utility;
using Microsoft.Practices.CompositeUI.Configuration;


namespace Syncfusion.CAB.WinForms.MenuConfig
{
    public class BarProfileReader
    {
        private string catalogFilePath;

        public BarProfileReader(string catalogFilePath)
        {
            this.catalogFilePath = GetFullPathOrThrowIfInvalid(catalogFilePath);
        }

        public ShellItemsSection ReadMenuProfile()
        {
            ShellItemsSection shellItemSection = new ShellItemsSection();
            if (File.Exists(catalogFilePath))
            {
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(ShellItemsSection));
                FileStream fs = new FileStream(this.catalogFilePath, FileMode.Open);
                shellItemSection = (ShellItemsSection)serializer.Deserialize(fs);
                return shellItemSection;
            }
            return null;
        }

        private string GetFullPathOrThrowIfInvalid(string catalogFilePath)
        {
            // Only change the default if we have a non-empty value.
            Guard.ArgumentNotNullOrEmptyString(catalogFilePath, "catalogFilePath");

            string fullPath = catalogFilePath;

            if (!Path.IsPathRooted(fullPath))
            {
                fullPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, fullPath);
            }

            // Simplify relative path movements.
            fullPath = new FileInfo(fullPath).FullName;

            if (!fullPath.StartsWith(AppDomain.CurrentDomain.BaseDirectory, true, CultureInfo.CurrentCulture))
            {
                throw new Exception(String.Format(
                    CultureInfo.CurrentCulture,
                    "InvalidFilePath",
                    catalogFilePath,
                    AppDomain.CurrentDomain.BaseDirectory));
            }

            return fullPath;
        }
    }


	public static class BarItemUIBuilder
	{
		// Loads the menu items from any XML file and put them into the menu strip, hooking
		// up the menu URIs for command dispatching.
		public static void LoadFromConfig(WorkItem workItem,String fileName)
		{
            ShellItemsSection section = new BarProfileReader(fileName).ReadMenuProfile();
            if (section != null)
            {
                foreach (BarItemElement menuItem in section.BarItems)
                {
                    if (menuItem.BarItemType != null)
                    {
                        if (menuItem.BarItemType.ToLower() == "parentbaritem")
                        {
                            ParentBarItem uiElement = menuItem.ToParentBarItem();
                            AddBarItem(menuItem, workItem, uiElement);
                        }
                        else if (menuItem.BarItemType.ToLower() == "baritem")
                        {
                            BarItem uiElement = menuItem.ToBarItem();
                            AddBarItem(menuItem, workItem, uiElement);
                        }
                    }
                }
            }
		}

        /// <summary>
        /// Create a ShellItemsSection object and pass it here to create its respective baritems
        /// </summary>
        /// <param name="workItem"></param>
        /// <param name="section"></param>
        public static void LoadFromObject(WorkItem workItem, ShellItemsSection section)
        {
            if (section != null)
            {
                foreach (BarItemElement menuItem in section.BarItems)
                {
                    if (menuItem.BarItemType != null)
                    {
                        if (menuItem.BarItemType.ToLower() == "parentbaritem")
                        {
                            ParentBarItem uiElement = menuItem.ToParentBarItem();
                            AddBarItem(menuItem, workItem, uiElement);
                        }
                        else if (menuItem.BarItemType.ToLower() == "baritem")
                        {
                            BarItem uiElement = menuItem.ToBarItem();
                            AddBarItem(menuItem, workItem, uiElement);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Adds the bar item.
        /// </summary>
        /// <param name="menuItem">The menu item.</param>
        /// <param name="workItem">The work item.</param>
        /// <param name="uiElement">The ui element.</param>
        private static void AddBarItem(BarItemElement menuItem, WorkItem workItem, ParentBarItem uiElement)
        {
            workItem.UIExtensionSites[menuItem.Site].Add(uiElement);
            if (menuItem.Register == true)
                workItem.UIExtensionSites.RegisterSite(menuItem.RegistrationSite, uiElement);
            if (!String.IsNullOrEmpty(menuItem.CommandName))
                workItem.Commands[menuItem.CommandName].AddInvoker(uiElement, "Click");
        }

        /// <summary>
        /// Adds the bar item.
        /// </summary>
        /// <param name="menuItem">The menu item.</param>
        /// <param name="workItem">The work item.</param>
        /// <param name="uiElement">The ui element.</param>
        private static void AddBarItem(BarItemElement menuItem, WorkItem workItem, BarItem uiElement)
        {
            workItem.UIExtensionSites[menuItem.Site].Add(uiElement);
            if (!String.IsNullOrEmpty(menuItem.CommandName))
                workItem.Commands[menuItem.CommandName].AddInvoker(uiElement, "Click");
        }
	}
}
