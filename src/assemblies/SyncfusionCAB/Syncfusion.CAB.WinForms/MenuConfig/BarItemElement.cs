//===============================================================================
// Microsoft patterns & practices
// CompositeUI Application Block
//===============================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//===============================================================================

using System;
using System.Configuration;
using System.Windows.Forms;
using System.IO;
using System.Drawing;
using System.Xml.Serialization;
using Syncfusion.Windows.Forms.Tools;
using Syncfusion.Windows.Forms.Tools.XPMenus;
using System.ComponentModel;


namespace Syncfusion.CAB.WinForms.MenuConfig
{
    public class BarItemElement
    {
        /// <summary>
        /// Gets the commandname for the BarItem
        /// </summary>
        [XmlAttributeAttribute]
        public string CommandName;

        /// <summary>
        /// Gets the display name for the label
        /// </summary>
        [XmlAttributeAttribute]
        public string Label;

        /// <summary>
        /// Gets the shortcut key for BarItem
        /// </summary>
        [XmlAttributeAttribute]
        public string ShortCutKey;

        /// <summary>
        /// Gets the sitename for registration
        /// </summary>
        [XmlAttributeAttribute]
        public string Site;

        /// <summary>
        /// If true, then RegistrationSite value must be supplied
        /// </summary>
        [XmlAttributeAttribute]
        public bool Register;

        /// <summary>
        /// Gets the registrationsite
        /// </summary>
        [XmlAttributeAttribute]
        public string RegistrationSite;

        /// <summary>
        /// Specifies the type of baritem, whether it is a ParentBarItem/BarItem
        /// </summary>
        [XmlAttributeAttribute]
        public string BarItemType;

        /// <summary>
        /// Sets the Check mark on the baritem
        /// </summary>
        [XmlAttributeAttribute]
        public bool Checked;

        /// <summary>
        /// Gets the image to be shown in the BarItem. Specify the imagename in a path
        /// </summary>
        [XmlAttributeAttribute]
        public string ImageName;

        [System.Xml.Serialization.XmlArrayAttribute("SeperatorIndices")]
        public int[] Indices;

        /// <summary>
        /// Gets the enable property for the BarItem
        /// </summary>
        [XmlAttributeAttribute]
        public bool Enabled;


        /// <summary>
        /// Gets the paintstyle for the BarItem.
        /// It should be matching with the following words
        /// 
        /// -Default
        /// -TextOnly
        /// -TextOnlyInMenus
        /// -ImageAndText
        /// </summary>
        [XmlAttributeAttribute]
        public string PaintStyle;

        public ParentBarItem ToParentBarItem()
        {
            ParentBarItem result = new ParentBarItem(Label);
            if (!String.IsNullOrEmpty(ShortCutKey))
                result.Shortcut = (Shortcut)Enum.Parse(typeof(Shortcut), ShortCutKey);
            if (Checked.ToString() != "")
                result.Checked = Checked;
            if (!String.IsNullOrEmpty(ImageName))
            {
                ImageExt img = GetImageFromPath(ImageName);
                if (img != null)
                    result.Image = img;
            }
            if (!String.IsNullOrEmpty(PaintStyle))
                result.PaintStyle = GetPaintStyle(PaintStyle);
            if (Indices != null && Indices.Length > 0)
            {
                foreach (int i in Indices)
                    result.SeparatorIndices.Add(i);
            }
            if (Enabled.ToString() != "")
                result.Enabled = Enabled;
            return result;
        }

        public BarItem ToBarItem()
        {
            BarItem result = new BarItem(Label);
            if (!String.IsNullOrEmpty(ShortCutKey))
                result.Shortcut = (Shortcut)Enum.Parse(typeof(Shortcut), ShortCutKey);
            if (Checked.ToString() != "")
                result.Checked = Checked;
            if (!String.IsNullOrEmpty(ImageName))
            {
                ImageExt img = GetImageFromPath(ImageName);
                if (img != null)
                    result.Image = img;
            }
            if (!String.IsNullOrEmpty(PaintStyle))
                result.PaintStyle = GetPaintStyle(PaintStyle);
            if (Enabled.ToString() != "")
                result.Enabled = Enabled;
            return result;
        }

        private Syncfusion.Windows.Forms.Tools.XPMenus.PaintStyle GetPaintStyle(string paintStyle)
        {
            if (paintStyle.ToLower().Contains("default"))
                return Syncfusion.Windows.Forms.Tools.XPMenus.PaintStyle.Default;
            else if (paintStyle.ToLower().Contains("textonly"))
                return Syncfusion.Windows.Forms.Tools.XPMenus.PaintStyle.TextOnly;
            else if (paintStyle.ToLower().Contains("textonlyinmenus"))
                return Syncfusion.Windows.Forms.Tools.XPMenus.PaintStyle.TextOnlyInMenus;
            else if (paintStyle.ToLower().Contains("imageandtext"))
                return Syncfusion.Windows.Forms.Tools.XPMenus.PaintStyle.ImageAndText;
            return Syncfusion.Windows.Forms.Tools.XPMenus.PaintStyle.Default;
        }

        private ImageExt GetImageFromPath(string imageName)
        {
            ImageExt img = null;
            if (File.Exists(imageName))
            {
                img = (ImageExt)Image.FromFile(imageName);
            }
            return img;
        }
    }
}
