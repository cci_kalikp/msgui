//===============================================================================
// Microsoft patterns & practices
// CompositeUI Application Block
//===============================================================================
// Copyright � Microsoft Corporation.  All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS FOR A PARTICULAR PURPOSE.
//===============================================================================

using System.Collections.Generic;
using System.Configuration;

namespace Syncfusion.CAB.WinForms.MenuConfig
{
    [System.Xml.Serialization.XmlRootAttribute("ShellItemsSection", Namespace = "http://schemas.syncfusion.com/cab-profile", IsNullable = false)]
	public class ShellItemsSection
	{
        [System.Xml.Serialization.XmlArrayAttribute("BarItems")]
        public BarItemElement[] BarItems;
	}
}
