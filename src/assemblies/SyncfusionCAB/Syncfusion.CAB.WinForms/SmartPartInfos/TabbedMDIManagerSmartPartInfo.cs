using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;

using Syncfusion.Windows.Forms.Tools;

using Microsoft.Practices.CompositeUI;
using Microsoft.Practices.CompositeUI.SmartParts;
using Microsoft.Practices.CompositeUI.WinForms;

namespace Syncfusion.CAB.WinForms.SmartPartInfos
{
    public class TabbedMDIManagerSmartPartInfo : SmartPartInfo
    {
        #region Constructors

        /// <summary>
        /// Initializes the TabbedMDIManagerSmartPartInfo with default values
        /// </summary>
        public TabbedMDIManagerSmartPartInfo()
        {
        }

        /// <summary>
        /// Initializes the TabbedMDIManagerSmartPartInfo with a Tabstyle
        /// eg: TabbedMDIManagerSmartPartInfo smartPartInfo = new TabbedMDIManagerSmartPartInfo(typeof(TabRendererIE7),true);
        /// </summary>
        /// <param name="TabStyle"></param>
        public TabbedMDIManagerSmartPartInfo(Type TabStyle)
        {
            this.m_TabStyle = TabStyle;
        }

        #endregion

        #region FormProperties

        private bool controlBox = true;
        private bool maximizeButton = true;
        private bool minimizeButton = true;
        private int height = 0;
        private int width = 0;
        private Point location = default(Point);
        private Icon icon = null;

        /// <summary>
        /// Gets/Sets the location of the window.
        /// </summary>
        [Category("Layout")]
        [DefaultValue(null)]
        public Point Location
        {
            get { return location; }
            set { location = value; }
        }

        /// <summary>
        /// The Icon that will appear on the window.
        /// </summary>
        [DefaultValue(null)]
        [Category("Layout")]
        public Icon Icon
        {
            get { return icon; }
            set { icon = value; }
        }

        /// <summary>
        /// Width of the window.
        /// </summary>
        [Category("Layout")]
        [DefaultValue(0)]
        public int Width
        {
            get { return width; }
            set { width = value; }
        }

        /// <summary>
        /// Height of the window.
        /// </summary>
        [Category("Layout")]
        [DefaultValue(0)]
        public int Height
        {
            get { return height; }
            set { height = value; }
        }

        /// <summary>
        /// Make minimize button visible.
        /// </summary>
        [DefaultValue(true)]
        [Category("Layout")]
        public bool MinimizeBox
        {
            get { return minimizeButton; }
            set { minimizeButton = value; }
        }

        /// <summary>
        /// Make maximize button visible.
        /// </summary>
        [DefaultValue(true)]
        [Category("Layout")]
        public bool MaximizeBox
        {
            get { return maximizeButton; }
            set { maximizeButton = value; }
        }

        /// <summary>
        /// Whether the controlbox will be visible.
        /// </summary>
        [DefaultValue(true)]
        [Category("Layout")]
        public bool ControlBox
        {
            get { return controlBox; }
            set { controlBox = value; }
        }
        #endregion

        #region TabManagerProperties

        private Type m_TabStyle = typeof(TabRendererWhidbey);
        private bool m_ThemesEnabled = false;
        private bool m_ShowCloseButton = false;
        private bool m_UseIconsInTab = false;
        private bool m_CloseOnMouseMiddleClick = false;
        private bool m_CloseButtonVisible = false;
        private bool m_Visible = true;
        private bool m_DropDownButtonVisible = true;
        private bool m_AllowTabGroupCustomizing = false;
        private bool m_Horizontal = true;
        private TabAlignment m_TabAlignment = TabAlignment.Top;
        private StringAlignment m_TextAlignment = StringAlignment.Center;
        private int m_TabGap;

        /// <summary>
        /// Gets/Sets the TabStyle value
        /// </summary>
        [Category("Layout")]
        [DefaultValue(typeof(TabRendererWhidbey))]
        public Type TabStyle
        {
            get
            {
                return m_TabStyle;
            }
            set
            {
                m_TabStyle = value;
            }
        }


        /// <summary>
        /// Gets/Sets the ThemesEnabled boolean value
        /// </summary>
        [Category("Layout")]
        [DefaultValue(false)]
        public bool ThemesEnabled
        {
            get
            {
                return m_ThemesEnabled;
            }
            set
            {
                m_ThemesEnabled = value;
            }
        }

        /// <summary>
        /// Gets/Sets the TabAlignment for the MDIForm
        /// </summary>
        [Category("Layout")]
        [DefaultValue(TabAlignment.Top)]
        public TabAlignment TabAlignment
        {
            get
            {
                return m_TabAlignment;
            }
            set
            {
                m_TabAlignment = value;
            }
        }

        /// <summary>
        /// Indicates to show the Close Button Display for each Tab
        /// </summary>
        [Category("Layout")]
        [DefaultValue(false)]
        public bool ShowCloseButton
        {
            get
            {
                return m_ShowCloseButton;
            }
            set
            {
                m_ShowCloseButton = value;
            }
        }

        /// <summary>
        /// Gets/Sets the DropDown Button visible for the TabbedMDIManager
        /// </summary>
        [Category("Layout")]
        [DefaultValue(true)]
        public bool DropDownButtonVisible
        {
            get
            {
                return m_DropDownButtonVisible;
            }
            set
            {
                m_DropDownButtonVisible = value;
            }
        }

        /// <summary>
        /// Gets/Sets the Use of icons in Tab
        /// </summary>
        [Category("Layout")]
        [DefaultValue(false)]
        public bool UseIconsInTab
        {
            get
            {
                return m_UseIconsInTab;
            }
            set
            {
                m_UseIconsInTab = value;
            }
        }

        /// <summary>
        /// Gets/Sets the Closing of Tabs if Middle button of the Mouse is Clicked
        /// </summary>
        [Category("Layout")]
        [DefaultValue(false)]
        public bool CloseOnMouseMiddleClick
        {
            get
            {
                return m_CloseOnMouseMiddleClick;
            }
            set
            {
                m_CloseOnMouseMiddleClick = value;
            }
        }

        /// <summary>
        /// Gets/Sets the TextAlignment for the Tabs
        /// </summary>
        [Category("Layout")]
        [DefaultValue(StringAlignment.Center)]
        public StringAlignment TextAlignment
        {
            get
            {
                return m_TextAlignment;
            }
            set
            {
                m_TextAlignment = value;
            }
        }

        /// <summary>
        /// Gets/Sets the Gap between the Tabs
        /// </summary>
        [Category("Layout")]
        public int TabGap
        {
            get
            {
                return m_TabGap;
            }
            set
            {
                m_TabGap = value;
            }
        }

        /// <summary>
        /// Gets/Sets the Close Button Visible for TabMdiContainer
        /// </summary>
        [Category("Layout")]
        [DefaultValue(false)]
        public bool CloseButtonVisible
        {
            get
            {
                return m_CloseButtonVisible;
            }
            set
            {
                m_CloseButtonVisible = value;
            }
        }


        /// <summary>
        /// Gets/Sets the Visibility of the TabMDIPanel
        /// </summary>
        [Category("Layout")]
        [DefaultValue(true)]
        public bool Visible
        {
            get
            {
                return m_Visible;
            }
            set
            {
                m_Visible = value;
            }
        }


        /// <summary>
        /// Indicates whether the user can drag/drop child form controls in the Tab
        /// </summary>
        [Category("Layout")]
        [DefaultValue(true)]
        public bool AllowTabGroupCustomizing
        {
            get
            {
                return m_AllowTabGroupCustomizing;
            }
            set
            {
                m_AllowTabGroupCustomizing = value;
            }
        }

        /// <summary>
        /// Indicates whether the Tabs should be arranged Horizontally/Vertically
        /// </summary>
        //[Category("Layout")]
        //[DefaultValue(true)]
        //public bool Horizontal
        //{
        //    get
        //    {
        //        return m_Horizontal;
        //    }
        //    set
        //    {
        //        m_Horizontal = value;
        //    }
        //}

        #endregion

    }
}
