using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;

using Syncfusion.Drawing;
using Syncfusion.Windows.Forms.Tools;

using Microsoft.Practices.CompositeUI.SmartParts;

namespace Syncfusion.CAB.WinForms.SmartPartInfos
{
    public class SplashPanelSmartPartInfo : SmartPartInfo
    {
        /// <summary>
        /// Initializes the SplashPanelSmartPartInfo class
        /// </summary>
        public SplashPanelSmartPartInfo()
        {
        }

        #region Private members

        private BrushInfo m_BrushInfo = null;
        private bool m_ShowAnimation = true;
        private bool m_ShowInTaskBar = false;
        private bool m_ShowAsTopMost = true;
        private bool m_SuspendAutoCloseWhenMouseOver = false;
        private bool m_isModal = false;
        private SplashAlignment m_DesktopAlignment;
        private SlideStyle m_SlideStyle;
        private int m_TimerInterval = 5000;
        private int m_AnimationSpeed = 50;
        private Point m_Pt = Point.Empty;
        
        #endregion

        #region Properties
        /// <summary>
        /// Gets/Sets the BackGrounColor for the SplashPanel
        /// </summary>
        [Category("Layout")]
        public BrushInfo BackgroundColor
        {
            get
            {
                if (m_BrushInfo == null)
                    m_BrushInfo = new Syncfusion.Drawing.BrushInfo(Syncfusion.Drawing.GradientStyle.Vertical, System.Drawing.SystemColors.Highlight, System.Drawing.SystemColors.HighlightText);
                return m_BrushInfo;
            }
            set
            {
                m_BrushInfo = value;
            }
        }

        /// <summary>
        /// Gets/Sets the ShowAnimation boolean property
        /// </summary>
        [Category("Layout")]
        [DefaultValue(true)]
        public bool ShowAnimantion
        {
            get
            {
                return m_ShowAnimation;
            }
            set
            {
                m_ShowAnimation = value;
            }
        }

        /// <summary>
        /// Indicates whether the SplashPanel should be shown in the TaskBar
        /// </summary>
        [Category("Layout")]
        [DefaultValue(false)]
        public bool ShowInTaskBar
        {
            get
            {
                return m_ShowInTaskBar;
            }
            set
            {
                m_ShowInTaskBar = value;
            }
        }

        /// <summary>
        /// Indicates the SplashPanel to Shown in the TopMost window
        /// </summary>
        [Category("Layout")]
        [DefaultValue(true)]
        private bool ShowAsTopMost
        {
            get
            {
                return m_ShowAsTopMost;
            }
            set
            {
                m_ShowAsTopMost = value;
            }
        }

        /// <summary>
        /// Indicates whether the SplashPanel should be closed when mouse is over it
        /// </summary>
        [Category("Layout")]
        [DefaultValue(false)]
        private bool SuspendAutoCloseWhenMouseOver
        {
            get
            {
                return m_SuspendAutoCloseWhenMouseOver;
            }
            set
            {
                m_SuspendAutoCloseWhenMouseOver = value;
            }
        }


        /// <summary>
        /// Gets/Sets the DesktopAlignment for the SplashPanel
        /// </summary>
        [Category("Layout")]
        public SplashAlignment DesktopAlignment
        {
            get
            {
                return this.m_DesktopAlignment;
            }
            set
            {
                this.m_DesktopAlignment = value;
            }
        }

        /// <summary>
        /// Gets/Sets the SlideStyle property
        /// </summary>
        [Category("Layout")]
        public SlideStyle SlideStyle
        {
            get
            {
                return m_SlideStyle;
            }
            set
            {
                m_SlideStyle = value;
            }
        }

        /// <summary>
        /// Gets/Sets the Display time interval for the SplashPanel
        /// </summary>
        [Category("Layout")]
        [DefaultValue(5000)]
        public int TimerInterval
        {
            get
            {
                return this.m_TimerInterval;
            }
            set
            {
                this.m_TimerInterval = value;
            }
        }

        /// <summary>
        /// Gets/Sets the animation speed for the SplashPanel
        /// </summary>
        [Category("Layout")]
        [DefaultValue(50)]
        public int AnimationSpeed
        {
            get
            {
                return this.m_AnimationSpeed;
            }
            set
            {
                this.m_AnimationSpeed = value;
            }
        }


        /// <summary>
        /// Indicates the Custom Point of location where the Splash Panel is to be displayed if the 
        /// DesktopAlignment property is set to �Custom�
        /// </summary>
        [Category("Layout")]
        public Point CustomPoint
        {
            get
            {
                return m_Pt;
            }
            set
            {
                m_Pt = value;
            }
        }

        /// <summary>
        /// Indicates whether the Splash Panel is a Modal/Modeless dialog
        /// </summary>
        [Category("Layout")]
        [DefaultValue(false)]
        public bool IsModal
        {
            get
            {
                return m_isModal;
            }
            set
            {
                m_isModal = value;
            }
        }
        
        #endregion
    }
}
