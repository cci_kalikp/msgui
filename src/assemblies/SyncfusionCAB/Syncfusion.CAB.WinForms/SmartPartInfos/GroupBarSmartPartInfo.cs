using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using Microsoft.Practices.CompositeUI;
using Microsoft.Practices.CompositeUI.SmartParts;
using Microsoft.Practices.ObjectBuilder;
using Microsoft.Practices.CompositeUI.Services;
using Syncfusion.Windows.Forms;
using System.ComponentModel;
using System.Windows.Forms;

namespace Syncfusion.CAB.WinForms.SmartPartInfos
{
    /// <summary>
    /// Custom smartpart information for GroupBarWorkspace.
    /// </summary>
    public class GroupBarSmartPartInfo : SmartPartInfo
    {
        #region Private Members

        private bool activeGroupBarItem = false;
        private bool barHighlight = true;
        private Icon barIcon = null;
        private string barText = "";
        private bool ShowStack = false;
        private bool ShowStackChevron = false;
        private Font m_Font;
        private Icon navigationPaneIcon = null;
        private Color topBorderColor = new Color();
        private Color bottomBorderColor = new Color();
        private Color leftBorderColor = new Color();
        private Color rightBorderColor = new Color();
        private VisualStyle style;
        private Office2007Theme m_Office2007Theme;
        private int m_GroupBarItemHeight = 22;
        private int m_HeaderHeight = 26;
        private Font m_HeaderFont;
        private Color m_HeaderBackColor = SystemColors.ControlDark;
        private Color m_HeaderForeColor = Color.White;
        private int padding;

       
        #endregion

        #region Public Members
        /// <summary>
        /// Gets or sets a value indicating whether [active group bar item].
        /// </summary>
        /// <value><c>true</c> if [active group bar item]; otherwise, <c>false</c>.</value>
        public bool ActiveGroupBarItem
        {
            get
            {
                return activeGroupBarItem;
            }
            set
            {
                activeGroupBarItem = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [bar high lightning].
        /// </summary>
        /// <value><c>true</c> if [bar high lightning]; otherwise, <c>false</c>.</value>
        public bool BarHighLighting
        {
            get
            {
                return barHighlight;
            }
            set
            {
                barHighlight = value;
            }
        }
       
        /// <summary>
        /// Gets or sets a value of Padding for GroupBar items.
        /// </summary>
        public int Padding
        {
            get { return padding; }
            set { padding = value; }
        } 
        /// <summary>
        /// Gets or sets the bar icon.
        /// </summary>
        /// <value>The bar icon.</value>
        public System.Drawing.Icon BarIcon
        {
            get
            {
                return barIcon;
            }
            set
            {
                barIcon = value;
            }
        }

        /// <summary>
        /// Gets or sets the bar display text.
        /// </summary>
        /// <value>The bar display text.</value>
        public string BarDisplayText
        {
            get
            {
                return barText;
            }
            set
            {
                barText = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [stack mode with chevron].
        /// </summary>
        /// <value>
        /// <c>true</c> if [stack mode with chevron]; otherwise,<c> false</c>.
        /// </value>
        public bool StackModeWithChevron
        {
            get
            {
                return ShowStackChevron;
            }
            set
            {
                ShowStackChevron = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [stack mode].
        /// </summary>
        /// <value><c>true</c> if [stack mode]; otherwise, <c>false</c>.</value>
        [Category("Layout")]
        [DefaultValue(false)]
        public bool StackMode
        {
            get
            {
                return ShowStack;
            }
            set
            {
                ShowStack = value;
            }
        }

        /// <summary>
        /// Gets or sets the navigation pane icon. If StackMode property is true, then only this property will works.
        /// </summary>
        /// <value>The navigation pane icon.</value>
        [Category("Layout")]
        public System.Drawing.Icon NavigationPaneIcon
        {
            get
            {
                return navigationPaneIcon;
            }
            set
            {
                navigationPaneIcon = value;
            }
        }

        /// <summary>
        /// Gets or sets the color of the top border.
        /// </summary>
        /// <value>The color of the top border.</value>
        [Category("Layout")]
        public Color TopBorderColor
        {
            get
            {
                return topBorderColor;
            }
            set
            {
                topBorderColor = value;
            }
        }

        /// <summary>
        /// Gets or sets the color of the bottom border.
        /// </summary>
        /// <value>The color of the bottom border.</value>
        [Category("Layout")]
        public Color BottomBorderColor
        {
            get
            {
                return bottomBorderColor;
            }
            set
            {
                bottomBorderColor = value;
            }
        }

        /// <summary>
        /// Gets or sets the color of the left border.
        /// </summary>
        /// <value>The color of the left border.</value>
        [Category("Layout")]
        public Color LeftBorderColor
        {
            get
            {
                return leftBorderColor;
            }
            set
            {
                leftBorderColor = value;
            }
        }

        /// <summary>
        /// Gets or sets the color of the right border.
        /// </summary>
        /// <value>The color of the right border.</value>
        [Category("Layout")]
        public Color RightBorderColor
        {
            get
            {
                return rightBorderColor;
            }
            set
            {
                rightBorderColor = value;
            }
        }

        /// <summary>
        /// Gets or sets the container visual style.
        /// </summary>
        /// <value>The visual style.</value>
        [Category("Appearance")]
        public VisualStyle VisualStyle
        {
            get
            {
                return style;
            }
            set
            {
                style = value;
            }
        }

        /// <summary>
        /// Gets/Sets the Height of GroupBar Items
        /// </summary>
        [Category("Appearance")]
        public int GroupBarItemHeight
        {
            get
            {
                return m_GroupBarItemHeight;
            }
            set
            {
                m_GroupBarItemHeight = value;
            }
        }

        /// <summary>
        /// Gets/Sets the Office2007theme for the docked windows
        /// </summary>
        [Category("Appearance")]
        [DefaultValue(Office2007Theme.Blue)]
        public Office2007Theme Office2007Theme
        {
            get
            {
                return m_Office2007Theme;
            }
            set
            {
                m_Office2007Theme = value;
            }
        }


        /// <summary>
        /// Gets/Sets the HeaderHeight
        /// </summary>
        [Category("Appearance")]
        public int HeaderHeight
        {
            get
            {
                return m_HeaderHeight;
            }
            set
            {
                m_HeaderHeight = value;
            }
        }

        /// <summary>
        /// Gets/Sets the Header BackColor
        /// </summary>
        [Category("Appearance")]
        public Color HeaderBackColor
        {
            get
            {
                return m_HeaderBackColor;
            }
            set
            {
                m_HeaderBackColor = value;
            }
        }

        /// <summary>
        /// Gets/Sets the HeaderForeColor
        /// </summary>
        [Category("Appearance")]
        public Color HeaderForeColor
        {
            get
            {
                return m_HeaderForeColor;
            }
            set
            {
                m_HeaderForeColor = value;
            }
        }

        /// <summary>
        /// Gets/Sets the Font
        /// </summary>
        [Category("Appearance")]
        public Font Font
        {
            get
            {
                return m_Font;
            }
            set
            {
                m_Font = value;
            }
        }

        /// <summary>
        /// Gets/Sets the HeaderFont
        /// </summary>
        [Category("Appearance")]
        public Font HeaderFont
        {
            get
            {
                return m_HeaderFont;
            }
            set
            {
                m_HeaderFont = value;
            }
        }

        #endregion

    }
}
