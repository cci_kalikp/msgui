using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Practices.CompositeUI.SmartParts;

using Syncfusion.Windows.Forms.Tools;
using System.ComponentModel;
using Syncfusion.Windows.Forms;

namespace Syncfusion.CAB.WinForms.SmartPartInfos
{
    /// <summary>
    /// Provides smart part  information for an Dockable WorkSpace.
    /// </summary>
    public class DockingSmartPartInfo : SmartPartInfo
    {

        #region Private Members
        private DockLabelAlignmentStyle _labelAlignment = DockLabelAlignmentStyle.Left;
        private DockingStyle _dockStyle = DockingStyle.Left;
        private DockTabAlignmentStyle dockTabAlignmentStyle = DockTabAlignmentStyle.Bottom;
        private int height;
        private int width;
        private string parentName = null;
        private bool autoHideOnLoad = false;
        private int m_ImageIndex;
        private CaptionButtonInfo[] m_CaptionButtonInfo;
        private VisualStyle? m_VisualStyle;
        private Office2007Theme? m_Office2007Theme;
        private bool m_DragCancel;
        private bool m_FreezeResizing;
        private bool m_FloatOnLoad = false;
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="DockingSmartPartInfo"/> class.
        /// </summary>
        public DockingSmartPartInfo()
            : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DockingSmartPartInfo"/> class.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="description">The description.</param>
        public DockingSmartPartInfo(string title, string description)
            : base(title, description)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DockingSmartPartInfo"/> class.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="description">The description.</param>
        /// <param name="dockLabelAlignment">The dock label alignment.</param>
        /// <param name="dockStyle">The dock style.</param>
        public DockingSmartPartInfo(string title, string description, DockLabelAlignmentStyle dockLabelAlignment, DockingStyle dockStyle)
            : this(title, description)
        {
            _labelAlignment = dockLabelAlignment;
            _dockStyle = dockStyle;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the Dock label alignment.
        /// </summary>
        /// <value>The Dock label alignment.</value>
        [Category("Layout")]
        public DockLabelAlignmentStyle LabelAlignment
        {
            get { return _labelAlignment; }
            set { _labelAlignment = value; }
        }

        /// <summary>
        /// Gets or sets the docking style of the docked window.
        /// </summary>
        /// <value>The docking style.</value>
        [Category("Layout")]
        public DockingStyle DockStyle
        {
            get { return _dockStyle; }
            set { _dockStyle = value; }
        }

        /// <summary>
        /// Gets or sets the dock tab alignment style, It accepts the DockTabAlignmentStyle enumerator values{Bottom,Top,Left and Right}.
        /// </summary>
        /// <value>The dock tab alignment style.</value>
        [Category("Layout")]
        public DockTabAlignmentStyle DockTabAlignmentStyle
        {
            get { return this.dockTabAlignmentStyle; }
            set { dockTabAlignmentStyle = value; }
        }

        /// <summary>
        /// Gets/sets the name of the parent control to which the control should be docked. If this is not specified
        /// default HostControl of the DockingManager will be used.
        /// </summary>
        public string ParentName
        {
            get
            {
                return parentName;
            }
            set
            {
                parentName = value;
            }
        }
        /// <summary>
        /// Gets/sets whether the control should autohide on load.
        /// </summary>
        [Category("Layout")]
        public bool AutoHideOnLoad
        {
            get
            {
                return autoHideOnLoad;
            }
            set
            {
                autoHideOnLoad = value;
            }
        }

        /// <summary>
        /// Gets/sets the dock mode of the control to be floating
        /// </summary>
        [Category("Layout")]
        public bool FloatOnLoad
        {
            get
            {
                return m_FloatOnLoad;
            }
            set
            {
                m_FloatOnLoad = value;
            }
        }

        /// <summary>
        /// Gets/Sets the Height of the docking control
        /// </summary>
        [Category("Layout")]
        public int Height
        {
            get
            {
                return height;
            }
            set
            {
                height = value;
            }
        }

        /// <summary>
        /// Gets/Sets the Width of the docking control
        /// </summary>
        [Category("Layout")]
        public int Width
        {
            get
            {
                return width;
            }
            set
            {
                width = value;
            }
        }


        /// <summary>
        /// Gets/Sets the ImageIndex for the Docked Control
        /// </summary>
        public int ImageIndex
        {
            get 
            {
                return m_ImageIndex;
            }
            set
            {
                m_ImageIndex = value;
            }
        }


        /// <summary>
        /// Gets/Sets the Custom CaptionButton for the Docked Control
        /// </summary>
        public CaptionButtonInfo[] CaptionButtonInfo
        {
            get
            {
                return m_CaptionButtonInfo;
            }
            set
            {
                m_CaptionButtonInfo = value;
            }
        }


        /// <summary>
        /// Gets/Sets the VisualStyle for the Docked Controls
        /// </summary>
        [Category("Layout")]
        public VisualStyle? VisualStyle
        {
            get
            {
                return m_VisualStyle;
            }
            set
            {
                m_VisualStyle = value;
            }
        }

        /// <summary>
        /// Gets/Sets the Office2007 ColorScheme for the Docked Controls
        /// </summary>
        [Category("Layout")]
        [DefaultValue(Office2007ColorScheme.Blue)]
        public Office2007Theme? Office2007Theme
        {
            get
            {
                return m_Office2007Theme;
            }
            set
            {
                m_Office2007Theme = value;
            }
        }
        
        /// <summary>
        /// Gets/Sets the Office2007 ColorScheme for the Docked Controls
        /// </summary>
        [Category("Layout")]
        [DefaultValue(false)]
        public bool DragCancel
        {
            get
            {
                return m_DragCancel;
            }
            set
            {
                m_DragCancel = value;
            }
        }

        /// <summary>
        /// FreezeResize the Docked Controls.
        /// </summary>
        [Category("Layout")]
        [DefaultValue(false)]
        public bool FreezeResizing
        {
            get
            {
                return m_FreezeResizing;
            }
            set
            {
                m_FreezeResizing = value;
            }
        }

        private bool isMDIChild = false;
        /// <summary>
        /// Gets or sets a value indicating whether this instance is MDI child.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is MDI child; otherwise, <c>false</c>.
        /// </value>
        public bool IsMDIChild
        {
            get { return isMDIChild; }
            set { isMDIChild = value; }
        }


        #endregion
    }

  
    /// <summary>
    /// Defines the properties that are to be used in CaptionButtons
    /// </summary>
    public class CaptionButtonInfo
    {
        private bool m_UseCaptionButton;
        /// <summary>
        /// Gets a boolean value, "true" to add the CaptionButton, "false" to remove the CaptionButton
        /// </summary>
        public bool UseCaptionButton
        {
            get
            {
                return m_UseCaptionButton;
            }
        }

        private CaptionButton m_CaptionButton;
        /// <summary>
        /// Gets the CaptionButtonObject which is to be added/deleted from the CaptionButtonCollection
        /// </summary>
        public CaptionButton CaptionButton
        {
            get
            {
                return m_CaptionButton;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CaptionButtonInfo"/> class.
        /// </summary>
        /// <param name="useCaptionButton">if set to <c>true</c> [use caption button].</param>
        /// <param name="captionButton">The caption button.</param>
        public CaptionButtonInfo(bool useCaptionButton, CaptionButton captionButton)
        {
            this.m_UseCaptionButton = useCaptionButton;
            this.m_CaptionButton = captionButton;
        }
        
    }
}
