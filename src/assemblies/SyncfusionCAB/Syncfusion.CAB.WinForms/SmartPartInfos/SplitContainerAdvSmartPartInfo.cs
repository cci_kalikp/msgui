using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

using Syncfusion.Drawing;
using Syncfusion.Windows.Forms.Tools;

using Microsoft.Practices.CompositeUI.SmartParts;
using System.ComponentModel;
using Syncfusion.Windows.Forms.Tools.Enums;

namespace Syncfusion.CAB.WinForms.SmartPartInfos
{
    public enum PanelSide
    {
        Left,
        Right
    }

    public class SplitContainerAdvSmartPartInfo : SmartPartInfo
    {
        private PanelSide? m_PanelSide;
        private DockStyle? m_DockStyle;
        private Style? m_Style;
        private int m_Panel1MinSize = 25;
        private int m_Panel2MinSize = 25;
        private Orientation? m_Orientation;
        private bool m_Panel1Collapsed = false;
        private bool m_Panel2Collapsed = false;
        private int m_SplitterDistance = 150;
        private int m_SplitterIncrement = 1;
        private int m_SplitterWidth = 7;
        private bool m_IsSplitterFixed = false;

        #region Constructors
        /// <summary>
        /// Initializes a new instace of SplitConatinerAdvSmartPartInfo class
        /// </summary>
        public SplitContainerAdvSmartPartInfo()
        {
        }

        /// <summary>
        /// Initializes a new instace of SplitConatinerAdvSmartPartInfo class and sets the PanelSide property
        /// </summary>
        /// <param name="panelSide"></param>
        public SplitContainerAdvSmartPartInfo(PanelSide panelSide)
        {
            this.m_PanelSide = panelSide;
        }

        /// <summary>
        /// Initializes a new instace of SplitConatinerAdvSmartPartInfo class, Sets the PanelSide and DockStyle property
        /// </summary>
        /// <param name="panelSide"></param>
        /// <param name="dockStyle"></param>
        public SplitContainerAdvSmartPartInfo(PanelSide panelSide, DockStyle dockStyle, Style style)
        {
            this.m_PanelSide = panelSide;
            this.m_DockStyle = dockStyle;
            this.m_Style = style;
        }

        #endregion

        /// <summary>
        /// Gets or sets a value indicating whether this instance is splitter fixed.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is splitter fixed; otherwise, <c>false</c>.
        /// </value>
        public bool IsSplitterFixed
        {
            get
            {
                return m_IsSplitterFixed;
            }
            set
            {
                m_IsSplitterFixed = value;
            }
        }

        /// <summary>
        /// Gets or sets the size of the panel1 min.
        /// </summary>
        /// <value>The size of the panel1 min.</value>
        [DefaultValue(25)]
        public int Panel1MinSize
        {
            get
            {
                return m_Panel1MinSize;
            }
            set
            {
                m_Panel1MinSize = value;
            }
        }

        /// <summary>
        /// Gets or sets the size of the panel2 min.
        /// </summary>
        /// <value>The size of the panel2 min.</value>
        [DefaultValue(25)]
        public int Panel2MinSize
        {
            get
            {
                return m_Panel2MinSize;
            }
            set
            {
                m_Panel2MinSize = value;
            }
        }

        /// <summary>
        /// Gets or sets the orientation.
        /// </summary>
        /// <value>The orientation.</value>
        public Orientation? Orientation
        {
            get
            {
                return m_Orientation;
            }
            set
            {
                m_Orientation = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [panel1 collapsed].
        /// </summary>
        /// <value><c>true</c> if [panel1 collapsed]; otherwise, <c>false</c>.</value>
        public bool Panel1Collapsed
        {
            get
            {
                return m_Panel1Collapsed;
            }
            set
            {
                m_Panel1Collapsed = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [panel2 collapsed].
        /// </summary>
        /// <value><c>true</c> if [panel2 collapsed]; otherwise, <c>false</c>.</value>
        public bool Panel2Collapsed
        {
            get
            {
                return m_Panel2Collapsed;
            }
            set
            {
                m_Panel2Collapsed = value;
            }
        }

        /// <summary>
        /// Gets or sets the splitter distance.
        /// </summary>
        /// <value>The splitter distance.</value>
        public int SplitterDistance
        {
            get
            {
                return m_SplitterDistance;
            }
            set
            {
                m_SplitterDistance = value;
            }
        }

        /// <summary>
        /// Gets or sets the splitter increment.
        /// </summary>
        /// <value>The splitter increment.</value>
        public int SplitterIncrement
        {
            get
            {
                return m_SplitterIncrement;
            }
            set
            {
                m_SplitterIncrement = value;
            }
        }

        /// <summary>
        /// Gets or sets the width of the splitter.
        /// </summary>
        /// <value>The width of the splitter.</value>
        public int SplitterWidth
        {
            get
            {
                return m_SplitterWidth;
            }
            set
            {
                m_SplitterWidth = value;
            }
        }


        #region Properties
        /// <summary>
        /// Gets/Sets the Side of the Panel in SplitContainerAdv control. Left/Top and Right/Bottom
        /// </summary>
        [Category("Position")]
        public PanelSide? PanelSide
        {
            get
            {
                return this.m_PanelSide;
            }
            set
            {
                this.m_PanelSide = value;
            }
        }


        /// <summary>
        /// Gets/Sets the Dockstyle property
        /// </summary>
        [Category("Layout")]
        public DockStyle? DockStyle
        {
            get
            {
                return m_DockStyle;
            }
            set
            {
                m_DockStyle = value;
            }
        }

        /// <summary>
        /// Gets/Sets the Style for the SplitContainerControl
        /// </summary>
        [Category("Layout")]
        public Style? Style
        {
            get
            {
                return m_Style;
            }
            set
            {
                m_Style = value;
            }
        }
        #endregion
    }
}
