using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel;
using System.Drawing;
using Microsoft.Practices.CompositeUI;
using Microsoft.Practices.CompositeUI.SmartParts;

namespace Syncfusion.CAB.WinForms.SmartPartInfos
{
    public class PopupSmartPartInfo:SmartPartInfo
    {
        public PopupSmartPartInfo()
        {
        }

        #region Fields,Properties
		        private Color bgColorFlag,fgColorFlag;
     
        public Color BGColor
        {
            get
            {
                return bgColorFlag;
            }
            set
            {
                bgColorFlag =value;
            }
        }
        public Color FGColor
        {
            get
            {
                return fgColorFlag;
            }
            set
            {
                fgColorFlag = value;
            }
        }
 
	#endregion    
    } 
}
