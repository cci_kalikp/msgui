using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using Microsoft.Practices.CompositeUI;
using Microsoft.Practices.CompositeUI.SmartParts;
using Syncfusion.Windows.Forms;

namespace Syncfusion.CAB.WinForms.SmartPartInfos
{
    public enum TaskPagePosition
    {
        Beginning,
        End
    }

    public class XPTaskPaneSmartPartInfo : SmartPartInfo
    {
        private TaskPagePosition m_Position = TaskPagePosition.End;
        private VisualStyle? m_VisualStyle;

        #region Constructor

        /// <summary>
        /// Initializes the XPTaskPaneSmartPartInfo class
        /// </summary>
        public XPTaskPaneSmartPartInfo()
        {
        }

        /// <summary>
        /// Initializes the XPTaskPaneSmartPartInfo class and sets the Title property
        /// </summary>
        /// <param name="title"></param>
        public XPTaskPaneSmartPartInfo(String title)
        {
            this.Title = title;
        }

        #endregion

        [Category("Position")]
        [DefaultValue(TaskPagePosition.End)]
        public TaskPagePosition TaskPagePosition
        {
            get
            {
                return m_Position;
            }
            set
            {
                m_Position = value;
            }
        }

        [Category("Appearance")]
        public VisualStyle? VisualStyle
        {
            get
            {
                return m_VisualStyle;
            }
            set
            {
                m_VisualStyle = value;
            }
        }
    }
}
