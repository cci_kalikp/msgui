using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Practices.CompositeUI.SmartParts;
using System.ComponentModel;

namespace Syncfusion.CAB.WinForms.SmartPartInfos
{
    /// <summary>
    /// Specifies the position of the tab page on a <see cref="TabControlAdvWorkspace"/>.
    /// </summary>
    public enum TabPosition
    {
        /// <summary>
        /// Place tab page at beginning.
        /// </summary>
        Beginning,
        /// <summary>
        /// Place tab page at end.
        /// </summary>
        End,
    }

    /// <summary>
    /// A <see cref=" SmartPartInfo"/> that describes how a specific smartpartinfo will be shown.
    /// </summary>
    public class TabControlAdvSmartPartInfo : SmartPartInfo
    {
        /// <summary>
        /// Specifies to activate the currenct Tab
        /// </summary>
        private bool activateTab = false;
        [Category("Behavior")]
        public bool ActivateTab
        {
            get
            {
                return activateTab;
            }
            set
            {
                activateTab = value;
            }
        }
        /// <summary>
        /// Specifies the position of the tab page.
        /// </summary>
        private TabPosition position = TabPosition.End;
        [Category("Appearance")]
        public TabPosition Position
        {
            get
            {
                return position;
            }
            set
            {
                position = value;
            }
        }

        
        private bool m_HotTrack;
        /// <summary>
        /// Gets/Sets the HotTracking of Tabs
        /// </summary>
        [Category("Behavior")]
        [DefaultValue(false)]
        public bool HotTrack
        {
            get
            {
                return m_HotTrack;
            }
            set
            {
                m_HotTrack = value;
            }
        }


        private bool m_LabelEdit;
        /// <summary>
        /// Indicates whether the user can modify the Labels at run-time
        /// </summary>
        [Category("Behavior")]
        [DefaultValue(false)]
        public bool LabelEdit
        {
            get
            {
                return m_LabelEdit;
            }
            set
            {
                m_LabelEdit = value;
            }
        }

        private bool m_ShowScroll;
        /// <summary>
        /// Indicates whether to show/hide scroll buttons when there is not enough space in single line mode
        /// </summary>
        [Category("Behavior")]
        [DefaultValue(false)]
        public bool ShowScroll
        {
            get
            {
                return m_ShowScroll;
            }
            set
            {
                m_ShowScroll = value;
            }
        }

        private bool m_FocusOnTabClick;
        /// <summary>
        /// Indicates that the control should take focus when it is clicked
        /// </summary>
        [Category("Behavior")]
        [DefaultValue(false)]
        public bool FocusOnTabClick
        {
            get
            {
                return m_FocusOnTabClick;
            }
            set
            {
                m_FocusOnTabClick = true;
            }
        }
        
        private bool m_ThemesEnabled;
        /// <summary>
        /// Gets/Sets the Themes for the TabControlAdv
        /// </summary>
        [Category("Appearance")]
        [DefaultValue(false)]
        public bool ThemesEnabled
        {
            get
            {
                return m_ThemesEnabled;
            }
            set
            {
                m_ThemesEnabled = value;
            }
        }

        private bool m_UserMoveTabs;
        /// <summary>
        /// Indicates whether the user can change Tab position within the TabControlAdv through Drag n Drop
        /// </summary>
        [Category("Behavior")]
        [DefaultValue(false)]
        public bool UserMoveTabs
        {
            get
            {
                return m_UserMoveTabs;
            }
            set
            {
                m_UserMoveTabs = value;
            }
        }

        private bool m_KeepSelectedTabInFrontRow;
        /// <summary>
        /// Indicates whether the SelectedTab should be moved to Focus when in multiline mode
        /// </summary>
        [Category("Behavior")]
        [DefaultValue(false)]
        public bool KeepSelectedTabInFrontRow
        {
            get
            {
                return m_KeepSelectedTabInFrontRow;
            }
            set
            {
                m_KeepSelectedTabInFrontRow = value;
            }
        }

        private bool m_RotateTextWhenVertical;
        /// <summary>
        /// Indicates whether the text in Tabs should be rotated to draw horizontally when tab is aligned to left/right
        /// </summary>
        [Category("Appearance")]
        [DefaultValue(false)]
        public bool RotateTextWhenVertical
        {
            get
            {
                return m_RotateTextWhenVertical;
            }
            set
            {
                m_RotateTextWhenVertical = value;
            }
        }

        private bool m_RotateTabsWhenRTL;
        /// <summary>
        /// Indicates whether the Tabs should be rotated when RTL is used
        /// </summary>
        [Category("Appearance")]
        [DefaultValue(false)]
        public bool RotateTabsWhenRTL
        {
            get
            {
                return m_RotateTabsWhenRTL;
            }
            set
            {
                m_RotateTabsWhenRTL = value;
            }
        }



        private bool m_MultiLine;
        /// <summary>
        /// Indicates whether more than one row of text can be displayed
        /// </summary>
        [Category("Appearance")]
        [DefaultValue(false)]
        public bool MultiLine
        {
            get
            {
                return m_MultiLine;
            }
            set
            {
                m_MultiLine = value;
            }
        }

        private bool m_VSLikeScrollButton;
        /// <summary>
        /// Indicates whether the scroll buttons should be drawn with VS MDI Child look and feel
        /// </summary>
        [Category("Appearance")]
        [DefaultValue(false)]
        public bool VSLikeScrollButton
        {
            get
            {
                return m_VSLikeScrollButton;
            }
            set
            {
                m_VSLikeScrollButton = value;
            }
        }

        private int? m_TabGap;
        /// <summary>
        /// Gets/Sets the gap when Tabs are in single line mode
        /// </summary>
        [Category("Appearance")]
        public int? TabGap
        {
            get
            {
                return m_TabGap;
            }
            set
            {
                m_TabGap = value;
            }
        }

        private Type m_TabStyle;
        /// <summary>
        /// Gets/Sets the TabStyle for the Tabs
        /// </summary>
        [Category("Appearance")]
        public Type TabStyle
        {
            get
            {
                return m_TabStyle;
            }
            set
            {
                m_TabStyle = value;
            }
        }

        private System.Windows.Forms.TabAlignment? m_Alignment;
        /// <summary>
        /// Specifies the Alignment of Tabs
        /// </summary>
        [Category("Appearance")]
        public System.Windows.Forms.TabAlignment? Alignment
        {
            get
            {
                return m_Alignment;
            }
            set
            {
                m_Alignment = value;
            }
        }

        private Syncfusion.Windows.Forms.Tools.ScrollIncrement? m_ScrollIncrement;
        /// <summary>
        /// Specifies whether to scroll in Tabs or Pages
        /// </summary>
        [Category("Behavior")]
        public Syncfusion.Windows.Forms.Tools.ScrollIncrement? ScrollIncrement
        {
            get
            {
                return m_ScrollIncrement;
            }
            set
            {
                m_ScrollIncrement = value;
            }
        }

        private Syncfusion.Windows.Forms.Tools.TabSizeMode? m_SizeMode;
        /// <summary>
        /// Gets/Sets the SizeMode on how Tabs would be sized
        /// </summary>
        [Category("Behavior")]
        public Syncfusion.Windows.Forms.Tools.TabSizeMode? SizeMode
        {
            get
            {
                return m_SizeMode;
            }
            set
            {
                m_SizeMode = value;
            }
        }


        private TabPrimitiveInfo[] m_TabPrimitiveInfo;
        /// <summary>
        /// Gets/Sets the TabPrimitiveHost for the TabControlAdv, Use the TabPrimitives array to add TabPrimitives
        /// inside the TabControlAdv.
        /// 
        /// eg:
        /// CustomTabPrimitive ctb = new CustomTabPrimitive(true, new TabPrimitive(TabPrimitiveType.FirstTab, null, System.Drawing.Color.Empty, true, 1, "FirstTab"));
        /// CustomTabPrimitive ctb1 = new CustomTabPrimitive(true, new TabPrimitive(TabPrimitiveType.NextTab, null, System.Drawing.Color.Empty, true, 1, "NextTab"));
        /// tbInfo.TabPrimitives = new CustomTabPrimitive[] { ctb, ctb1}; (where tbInfo is the TabControlAdvSmartPartInfo object)
        /// </summary>
        public TabPrimitiveInfo[] TabPrimitives
        {
            get
            {
                return m_TabPrimitiveInfo;
            }
            set
            {
                this.m_TabPrimitiveInfo = value;
            }
        }

        private bool m_TabPrimitiveVisible;
        /// <summary>
        /// Gets/Sets the TabPrimitive Visibilty. Set this to true if TabPrimitives are added through Primitives[] collection
        /// </summary>
        public bool TabPrimitiveVisible
        {
            get
            {
                return m_TabPrimitiveVisible;
            }
            set
            {
                m_TabPrimitiveVisible = value;
            }
        }

        private Syncfusion.Windows.Forms.Tools.TabPrimitiveHostAlignment m_TabPrimitiveHostAlignment;
        /// <summary>
        /// Gets/Sets the TabPrimitiveHostAlignment
        /// </summary>
        public Syncfusion.Windows.Forms.Tools.TabPrimitiveHostAlignment TabPrimitiveHostAlignment
        {
            get
            {
                return m_TabPrimitiveHostAlignment;
            }
            set
            {
                m_TabPrimitiveHostAlignment = value;
            }
        }

    }

    public class TabPrimitiveInfo
    {
        private bool m_UseInTab;
        /// <summary>
        /// Gets/Sets a boolean value to use the TabPrimitives inside the Tab. 
        /// "True" to add the TabPrimitive, "False" to remove it from the TabPrimitveCollection
        /// </summary>
        public bool UseInTab
        {
            get
            {
                return m_UseInTab;
            }
        }

        private Syncfusion.Windows.Forms.Tools.TabPrimitive m_TabPrimitive;
        /// <summary>
        /// Gets the TabPrimitive object to be added/deleted
        /// </summary>
        public Syncfusion.Windows.Forms.Tools.TabPrimitive TabPrimitive
        {
            get
            {
                return m_TabPrimitive;
            }
        }

        public TabPrimitiveInfo(bool useInTab, Syncfusion.Windows.Forms.Tools.TabPrimitive tabPrimitive)
        {
            this.m_UseInTab = useInTab;
            this.m_TabPrimitive = tabPrimitive;
        }
    }
}
