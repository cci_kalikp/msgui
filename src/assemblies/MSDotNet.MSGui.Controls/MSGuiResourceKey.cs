﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Controls/MSGuiResourceKey.cs#3 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System.ComponentModel;
using System.Reflection;
using System.Windows;

namespace MorganStanley.MSDotNet.MSGui.Controls
{
  [TypeConverter(typeof(MSGuiKeyConverter))]
  internal class MSGuiResourceKey : ComponentResourceKey
  {
    private string m_id;

    public MSGuiResourceKey(string id_)
    {
      m_id = id_;
    }

    public override Assembly Assembly
    {
      get 
      {
        return null;
      }
    }

    public override string ToString()
    {
      return this.m_id.ToString();
    }

    public override bool Equals(object o)
    {
      MSGuiResourceKey key = o as MSGuiResourceKey;
      return ((key != null) && (key.m_id == this.m_id));
    }

    public override int GetHashCode()
    {
      return m_id.GetHashCode();
    }
  }
}
