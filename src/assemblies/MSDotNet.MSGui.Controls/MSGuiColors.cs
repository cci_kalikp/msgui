﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Controls/MSGuiColors.cs#19 $
// $Change: 902189 $
// $DateTime: 2014/10/22 23:25:18 $
// $Author: caijin $

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Media;

namespace MorganStanley.MSDotNet.MSGui.Controls
{
	public static class MSGuiColors
	{
        public static readonly ComponentResourceKey WindowCenterBorderBrushKey =
          new MSGuiResourceKey(MSGuiBasicBrushID.WindowCenterBorderBrush.ToString("g"));

        public static readonly ComponentResourceKey WindowInnerBorderBrushKey =
                  new MSGuiResourceKey(MSGuiBasicBrushID.WindowInnerBorderBrush.ToString("g"));

		public static readonly ComponentResourceKey WindowBackgroundBrushKey =
		  new MSGuiResourceKey(MSGuiBasicBrushID.WindowBackgroundBrush.ToString("g"));

        public static readonly ComponentResourceKey LaunchBarBackgroundBrushKey =
          new MSGuiResourceKey(MSGuiBasicBrushID.LaunchBarBackgroundBrush.ToString("g"));

        public static readonly ComponentResourceKey LaunchBarBorderBrushKey =
          new MSGuiResourceKey(MSGuiBasicBrushID.LaunchBarBorderBrush.ToString("g"));

		public static readonly ComponentResourceKey WindowHeaderBrushKey =
		  new MSGuiResourceKey(MSGuiBasicBrushID.WindowHeaderBrush.ToString("g"));

		public static readonly ComponentResourceKey InactiveWindowHeaderBrushKey =
		  new MSGuiResourceKey(MSGuiBasicBrushID.InactiveWindowHeaderBrush.ToString("g"));

		public static readonly ComponentResourceKey ControlBackgroundBrushKey =
		  new MSGuiResourceKey(MSGuiBasicBrushID.ControlBackgroundBrush.ToString("g"));

		public static readonly ComponentResourceKey PanelBackgroundBrushKey =
		  new MSGuiResourceKey(MSGuiBasicBrushID.PanelBackgroundBrush.ToString("g"));

        public static readonly ComponentResourceKey PanelBackgroundSolidBrushKey =
            new MSGuiResourceKey(MSGuiBasicBrushID.PanelBackgroundSolidBrush.ToString("g"));

		public static readonly ComponentResourceKey TitlesBrushKey =
		  new MSGuiResourceKey(MSGuiBasicBrushID.TitlesBrush.ToString("g"));

		public static readonly ComponentResourceKey InactiveTitlesBrushKey =
		  new MSGuiResourceKey(MSGuiBasicBrushID.InactiveTitlesBrush.ToString("g"));

		public static readonly ComponentResourceKey PrimaryLabelsBrushKey =
		  new MSGuiResourceKey(MSGuiBasicBrushID.PrimaryLabelsBrush.ToString("g"));

		public static readonly ComponentResourceKey SecondaryLabelsBrushKey =
		  new MSGuiResourceKey(MSGuiBasicBrushID.SecondaryLabelsBrush.ToString("g"));

		public static readonly ComponentResourceKey TextFillBrushKey =
		  new MSGuiResourceKey(MSGuiBasicBrushID.TextFillBrush.ToString("g"));
		
		public static readonly ComponentResourceKey InvertedTextFillBrushKey =
		  new MSGuiResourceKey(MSGuiBasicBrushID.InvertedTextFillBrush.ToString("g"));

		public static readonly ComponentResourceKey LaunchBarTitleBrushKey =
          new MSGuiResourceKey(MSGuiBasicBrushID.LaunchBarTitleBrush.ToString("g"));

        public static readonly ComponentResourceKey LauncherBarSeparatorBrushKey =
          new MSGuiResourceKey(MSGuiBasicBrushID.LauncherBarSeparatorBrush.ToString("g"));

		public static readonly ComponentResourceKey ButtonBackgroundBrushKey =
		  new MSGuiResourceKey(MSGuiBasicBrushID.ButtonBackgroundBrush.ToString("g"));

		public static readonly ComponentResourceKey DisabledButtonBackgroundBrushKey =
		  new MSGuiResourceKey(MSGuiBasicBrushID.DisabledButtonBackgroundBrush.ToString("g"));

		public static readonly ComponentResourceKey TextInputBackgroundBrushKey =
		  new MSGuiResourceKey(MSGuiBasicBrushID.TextInputBackgroundBrush.ToString("g"));

		public static readonly ComponentResourceKey DisabledTextInputBackgroundBrushKey =
		  new MSGuiResourceKey(MSGuiBasicBrushID.DisabledTextInputBackgroundBrush.ToString("g"));

		public static readonly ComponentResourceKey BorderBrushKey =
		  new MSGuiResourceKey(MSGuiBasicBrushID.BorderBrush.ToString("g"));

		public static readonly ComponentResourceKey InactiveBorderBrushKey =
		  new MSGuiResourceKey(MSGuiBasicBrushID.InactiveBorderBrush.ToString("g"));

		public static readonly ComponentResourceKey HighlightBorderBrushKey =
		  new MSGuiResourceKey(MSGuiBasicBrushID.HighlightBorderBrush.ToString("g"));


        public static readonly ComponentResourceKey TileItemBorderBrushKey =
          new MSGuiResourceKey(MSGuiBasicBrushID.TileItemBorderBrush.ToString("g"));

		public static readonly ComponentResourceKey WhiteBrushKey = new MSGuiResourceKey(MSGuiMiscBrushID.White.ToString("g"));
		public static readonly ComponentResourceKey WhiteBackgroundBrushKey = new MSGuiResourceKey(MSGuiMiscBrushID.WhiteBackground.ToString("g"));
		public static readonly ComponentResourceKey BlackBrushKey = new MSGuiResourceKey(MSGuiMiscBrushID.Black.ToString("g"));
		public static readonly ComponentResourceKey BlackBackgroundBrushKey = new MSGuiResourceKey(MSGuiMiscBrushID.BlackBackground.ToString("g"));
		public static readonly ComponentResourceKey GreyBrushKey = new MSGuiResourceKey(MSGuiMiscBrushID.Grey.ToString("g"));
		public static readonly ComponentResourceKey GreyBackgroundBrushKey = new MSGuiResourceKey(MSGuiMiscBrushID.GreyBackground.ToString("g"));
		public static readonly ComponentResourceKey RedBrushKey = new MSGuiResourceKey(MSGuiMiscBrushID.Red.ToString("g"));
		public static readonly ComponentResourceKey RedBackgroundBrushKey = new MSGuiResourceKey(MSGuiMiscBrushID.RedBackground.ToString("g"));
		public static readonly ComponentResourceKey OrangeBrushKey = new MSGuiResourceKey(MSGuiMiscBrushID.Orange.ToString("g"));
		public static readonly ComponentResourceKey OrangeBackgroundBrushKey = new MSGuiResourceKey(MSGuiMiscBrushID.OrangeBackground.ToString("g"));
		public static readonly ComponentResourceKey YellowBrushKey = new MSGuiResourceKey(MSGuiMiscBrushID.Yellow.ToString("g"));
		public static readonly ComponentResourceKey YellowBackgroundBrushKey = new MSGuiResourceKey(MSGuiMiscBrushID.YellowBackground.ToString("g"));
		public static readonly ComponentResourceKey GreenBrushKey = new MSGuiResourceKey(MSGuiMiscBrushID.Green.ToString("g"));
		public static readonly ComponentResourceKey GreenBackgroundBrushKey = new MSGuiResourceKey(MSGuiMiscBrushID.GreenBackground.ToString("g"));
		public static readonly ComponentResourceKey BlueBrushKey = new MSGuiResourceKey(MSGuiMiscBrushID.Blue.ToString("g"));
		public static readonly ComponentResourceKey BlueBackgroundBrushKey = new MSGuiResourceKey(MSGuiMiscBrushID.BlueBackground.ToString("g"));
		public static readonly ComponentResourceKey MagentaBrushKey = new MSGuiResourceKey(MSGuiMiscBrushID.Magenta.ToString("g"));
		public static readonly ComponentResourceKey MagentaBackgroundBrushKey = new MSGuiResourceKey(MSGuiMiscBrushID.MagentaBackground.ToString("g"));
		public static readonly ComponentResourceKey BrownBrushKey = new MSGuiResourceKey(MSGuiMiscBrushID.Brown.ToString("g"));
		public static readonly ComponentResourceKey BrownBackgroundBrushKey = new MSGuiResourceKey(MSGuiMiscBrushID.BrownBackground.ToString("g"));

		public static readonly ComponentResourceKey V2VExplicitConnectedColorKey = new MSGuiResourceKey("ViewToViewTile_ExplicitConnected");
		public static readonly ComponentResourceKey V2VExplicitDisconnectedColorKey = new MSGuiResourceKey("ViewToViewTile_ExplicitDisconnected");
		public static readonly ComponentResourceKey V2VAutoConnectedColorKey = new MSGuiResourceKey("ViewToViewTile_AutoConnected");
		public static readonly ComponentResourceKey V2VAutoNonConnectedColorKey = new MSGuiResourceKey("ViewToViewTile_NonAutoConnected");
		public static readonly ComponentResourceKey V2VGroupColorKey = new MSGuiResourceKey("ViewToViewGroupHeaderColor");

        public static ResourceKey CaptionButtonHoverBorderDarkKey = new MSGuiResourceKey(MSGuiCaptionButtonBrushID.CaptionButtonHoverBorderDark.ToString("g"));
        public static ResourceKey CaptionButtonHoverBorderLightKey = new MSGuiResourceKey(MSGuiCaptionButtonBrushID.CaptionButtonHoverBorderLight.ToString("g"));
        public static ResourceKey CaptionButtonHoverCenterKey = new MSGuiResourceKey(MSGuiCaptionButtonBrushID.CaptionButtonHoverCenter.ToString("g"));
        public static ResourceKey CaptionButtonHoverOverlayKey = new MSGuiResourceKey(MSGuiCaptionButtonBrushID.CaptionButtonHoverOverlay.ToString("g"));
        public static ResourceKey CaptionButtonPressedBorderDarkKey = new MSGuiResourceKey(MSGuiCaptionButtonBrushID.CaptionButtonPressedBorderDark.ToString("g"));
        public static ResourceKey CaptionButtonPressedBorderLightKey = new MSGuiResourceKey(MSGuiCaptionButtonBrushID.CaptionButtonPressedBorderLight.ToString("g"));
        public static ResourceKey CaptionButtonPressedCenterKey = new MSGuiResourceKey(MSGuiCaptionButtonBrushID.CaptionButtonPressedCenter.ToString("g"));
        public static ResourceKey CaptionButtonPressedBottomOverlayKey = new MSGuiResourceKey(MSGuiCaptionButtonBrushID.CaptionButtonPressedBottomOverlay.ToString("g"));
        public static ResourceKey CaptionButtonPressedTopOverlayKey = new MSGuiResourceKey(MSGuiCaptionButtonBrushID.CaptionButtonPressedTopOverlay.ToString("g"));
        public static ResourceKey CaptionButtonIconMinimizeNormalForegroundKey = new MSGuiResourceKey(MSGuiCaptionButtonBrushID.CaptionButtonIconMinimizeNormalForeground.ToString("g"));
        public static ResourceKey CaptionButtonIconMinimizeNormalBackgroundKey = new MSGuiResourceKey(MSGuiCaptionButtonBrushID.CaptionButtonIconMinimizeNormalBackground.ToString("g"));
        public static ResourceKey CaptionButtonIconMaxCloseNormalBackgroundKey = new MSGuiResourceKey(MSGuiCaptionButtonBrushID.CaptionButtonIconMaxCloseNormalBackground.ToString("g"));
        public static ResourceKey CaptionButtonIconMaxCloseNormalForegroundKey = new MSGuiResourceKey(MSGuiCaptionButtonBrushID.CaptionButtonIconMaxCloseNormalForeground.ToString("g"));
        public static ResourceKey CaptionButtonIconMaxCloseNormalDisabledForegroundKey = new MSGuiResourceKey(MSGuiCaptionButtonBrushID.CaptionButtonIconMaxCloseNormalDisabledForeground.ToString("g"));
        public static ResourceKey CaptionButtonIconMaxCloseInactiveDisabledForegroundKey = new MSGuiResourceKey(MSGuiCaptionButtonBrushID.CaptionButtonIconMaxCloseInactiveDisabledForeground.ToString("g"));
        public static ResourceKey CaptionButtonIconMinimizeHoverForegroundKey = new MSGuiResourceKey(MSGuiCaptionButtonBrushID.CaptionButtonIconMinimizeHoverForeground.ToString("g"));
        public static ResourceKey CaptionButtonIconMinimizeHoverBackgroundKey = new MSGuiResourceKey(MSGuiCaptionButtonBrushID.CaptionButtonIconMinimizeHoverBackground.ToString("g"));
        public static ResourceKey CaptionButtonIconMaxCloseHoverBackgroundKey = new MSGuiResourceKey(MSGuiCaptionButtonBrushID.CaptionButtonIconMaxCloseHoverBackground.ToString("g"));
        public static ResourceKey CaptionButtonIconMaxCloseHoverForegroundKey = new MSGuiResourceKey(MSGuiCaptionButtonBrushID.CaptionButtonIconMaxCloseHoverForeground.ToString("g"));
        public static ResourceKey CaptionButtonIconMinimizeInactiveForegroundKey = new MSGuiResourceKey(MSGuiCaptionButtonBrushID.CaptionButtonIconMinimizeInactiveForeground.ToString("g"));
        public static ResourceKey CaptionButtonIconMaxCloseInactiveForegroundKey = new MSGuiResourceKey(MSGuiCaptionButtonBrushID.CaptionButtonIconMaxCloseInactiveForeground.ToString("g"));

        public static ResourceKey XamPagerButtonBorderBrushKey = new MSGuiResourceKey("XamPagerButtonBorderBrushKey"); //RibbonBrushKeys.XamPagerButtonBlueGradientFillKey
        public static ResourceKey XamPagerButtonFillKey = new MSGuiResourceKey("XamPagerButtonFillKey"); //RibbonBrushKeys.XamPagerButtonCenterGrayGradientFillKey
        public static ResourceKey XamPagerGlyphFillKey = new MSGuiResourceKey("XamPagerGlyphFillKey"); //RibbonBrushKeys.XamPagerButtonGlyphFillKey
        public static ResourceKey XamPagerButtonHoverBorderBrushKey = new MSGuiResourceKey("XamPagerButtonHoverBorderBrushKey"); //RibbonBrushKeys.XamPagerButtonHoverBorderFillKey
        public static ResourceKey XamPagerButtonHoverFillKey = new MSGuiResourceKey("XamPagerButtonHoverFillKey"); //RibbonBrushKeys.XamPagerButtonHoverFillKey


        public static ResourceKey ToolEnabledForegroundTextFillKey = new MSGuiResourceKey("ToolEnabledForegroundTextFillKey"); //RibbonBrushKeys.ToolEnabledForegroundTextFillKey
        public static ResourceKey ToolDisabledForegroundTextFillKey = new MSGuiResourceKey("ToolDisabledForegroundTextFillKey"); //RibbonBrushKeys.ToolDisabledForegroundTextFillKey
        public static ResourceKey MenuToolPresenterLeftColumnFillKey = new MSGuiResourceKey("MenuToolPresenterLeftColumnFillKey"); //RibbonBrushKeys.MenuToolPresenterLeftColumnFillKey
        public static ResourceKey GalleryItemGroupCaptionStrokeFillKey = new MSGuiResourceKey("GalleryItemGroupCaptionStrokeFillKey"); //RibbonBrushKeys.GalleryItemGroupCaptionStrokeFillKey
        public static ResourceKey MenuHottrackCenterFillKey = new MSGuiResourceKey("MenuHottrackCenterFillKey"); //RibbonBrushKeys.MenuHottrackCenterFillKey
        public static ResourceKey MenuHottrackBorderDarkFillKey = new MSGuiResourceKey("MenuHottrackBorderDarkFillKey"); //RibbonBrushKeys.MenuHottrackBorderDarkFillKey
        public static ResourceKey MenuHottrackBorderLightFillKey = new MSGuiResourceKey("MenuHottrackBorderLightFillKey"); //RibbonBrushKeys.MenuHottrackBorderLightFillKey
        public static ResourceKey CheckMarkStrokeFillKey = new MSGuiResourceKey("CheckMarkStrokeFillKey"); //RibbonBrushKeys.CheckMarkStrokeFillKey
        public static ResourceKey TextHottrackFillKey = new MSGuiResourceKey("TextHottrackFillKey"); //RibbonBrushKeys.TextHottrackFillKey;
        public static ResourceKey IsCheckedBoxCenterFillKey = new MSGuiResourceKey("IsCheckedBoxCenterFillKey"); //RibbonBrushKeys.IsCheckedBoxCenterFillKey
        public static ResourceKey IsCheckedBoxBorderFillKey = new MSGuiResourceKey("IsCheckedBoxBorderFillKey"); //RibbonBrushKeys.IsCheckedBoxBorderFillKey
        public static ResourceKey SubMenuHeaderPopUpBorderDarkFillKey = new MSGuiResourceKey("SubMenuHeaderPopUpBorderDarkFillKey"); //RibbonBrushKeys.SubMenuHeaderPopUpBorderDarkFillKey
        public static ResourceKey SubMenuHeaderPopUpBorderLightFillKey = new MSGuiResourceKey("SubMenuHeaderPopUpBorderLightFillKey"); //RibbonBrushKeys.SubMenuHeaderPopUpBorderLightFillKey
        public static ResourceKey SubMenuHeaderPopUpCenterFillKey = new MSGuiResourceKey("SubMenuHeaderPopUpCenterFillKey"); //RibbonBrushKeys.SubMenuHeaderPopUpCenterFillKey
        public static ResourceKey MenuItemDropDownArrowFillKey = new MSGuiResourceKey("MenuItemDropDownArrowFillKey"); //RibbonBrushKeys.MenuItemDropDownArrowFillKey
        public static ResourceKey MenuItemDropDownArrowHottrackFillKey = new MSGuiResourceKey("MenuItemDropDownArrowHottrackFillKey"); //RibbonBrushKeys.MenuItemDropDownArrowHottrackFillKey
        public static ResourceKey MenuScrollViewerNormalBorderDarkFillKey = new MSGuiResourceKey("MenuScrollViewerNormalBorderDarkFillKey"); //RibbonBrushKeys.MenuScrollViewerNormalBorderDarkFillKey
        public static ResourceKey MenuScrollViewerNormalCenterFillKey = new MSGuiResourceKey("MenuScrollViewerNormalCenterFillKey"); //RibbonBrushKeys.MenuScrollViewerNormalCenterFillKey
        public static ResourceKey MenuScrollViewerNormalGlyphFillKey = new MSGuiResourceKey("MenuScrollViewerNormalGlyphFillKey"); //RibbonBrushKeys.MenuScrollViewerNormalGlyphFillKey
        public static ResourceKey MenuScrollViewerHoverBorderDarkFillKey = new MSGuiResourceKey("MenuScrollViewerHoverBorderDarkFillKey"); //RibbonBrushKeys.MenuScrollViewerHoverBorderDarkFillKey
        public static ResourceKey MenuScrollViewerHoverCenterFillKey = new MSGuiResourceKey("MenuScrollViewerHoverCenterFillKey"); //RibbonBrushKeys.MenuScrollViewerHoverCenterFillKey
        public static ResourceKey MenuScrollViewerHoverBorderLightFillKey = new MSGuiResourceKey("MenuScrollViewerHoverBorderLightFillKey"); //RibbonBrushKeys.MenuScrollViewerHoverBorderLightFillKey
        public static ResourceKey MenuScrollViewerHoverGlyphFillKey = new MSGuiResourceKey("MenuScrollViewerHoverGlyphFillKey"); //RibbonBrushKeys.MenuScrollViewerHoverGlyphFillKey
        public static ResourceKey MenuScrollViewerPressedBorderDarkFillKey = new MSGuiResourceKey("MenuScrollViewerPressedBorderDarkFillKey"); //RibbonBrushKeys.MenuScrollViewerPressedBorderDarkFillKey
        public static ResourceKey MenuScrollViewerPressedCenterFillKey = new MSGuiResourceKey("MenuScrollViewerPressedCenterFillKey"); //RibbonBrushKeys.MenuScrollViewerPressedCenterFillKey
        public static ResourceKey MenuScrollViewerPressedBorderLightFillKey = new MSGuiResourceKey("MenuScrollViewerPressedBorderLightFillKey"); //RibbonBrushKeys.MenuScrollViewerPressedBorderLightFillKey
        public static ResourceKey GenericGlyphHoverBackgroundFillKey = new MSGuiResourceKey("GenericGlyphHoverBackgroundFillKey"); //RibbonBrushKeys.GenericGlyphHoverBackgroundFillKey
        public static ResourceKey GenericGlyphHoverForegroundFillKey = new MSGuiResourceKey("GenericGlyphHoverForegroundFillKey"); //RibbonBrushKeys.GenericGlyphHoverForegroundFillKey 
        public static ResourceKey GenericGlyphDisabledBackgroundFillKey = new MSGuiResourceKey("GenericGlyphDisabledBackgroundFillKey"); //RibbonBrushKeys.GenericGlyphDisabledBackgroundFillKey
        public static ResourceKey GenericGlyphDisabledForegroundFillKey = new MSGuiResourceKey("GenericGlyphDisabledForegroundFillKey"); //RibbonBrushKeys.GenericGlyphDisabledForegroundFillKey
        public static ResourceKey GenericGlyphNormalForegroundFillKey = new MSGuiResourceKey("GenericGlyphNormalForegroundFillKey"); //RibbonBrushKeys.GenericGlyphNormalForegroundFillKey
        public static ResourceKey GenericGlyphNormalBackgroundFillKey = new MSGuiResourceKey("GenericGlyphNormalBackgroundFillKey"); //RibbonBrushKeys.GenericGlyphNormalBackgroundFillKey
        public static ResourceKey ButtonToolPressedBorderDarkFillKey = new MSGuiResourceKey("ButtonToolPressedBorderDarkFillKey"); //RibbonBrushKeys.ButtonToolPressedBorderDarkFillKey
        public static ResourceKey ButtonToolPressedInnerBorderFillKey = new MSGuiResourceKey("ButtonToolPressedInnerBorderFillKey"); //RibbonBrushKeys.ButtonToolPressedInnerBorderFillKey
        public static ResourceKey ButtonToolPressedCenterFillKey = new MSGuiResourceKey("ButtonToolPressedCenterFillKey"); //RibbonBrushKeys.ButtonToolPressedCenterFillKey
        public static ResourceKey ButtonToolCheckedHottrackBorderFillKey = new MSGuiResourceKey("ButtonToolCheckedHottrackBorderFillKey"); //RibbonBrushKeys.ButtonToolCheckedHottrackBorderFillKey
        public static ResourceKey ButtonToolCheckedHottrackInnerBorderFillKey = new MSGuiResourceKey("ButtonToolCheckedHottrackInnerBorderFillKey"); //RibbonBrushKeys.ButtonToolCheckedHottrackInnerBorderFillKey
        public static ResourceKey ButtonToolCheckedHottrackCenterFillKey = new MSGuiResourceKey("ButtonToolCheckedHottrackCenterFillKey"); //RibbonBrushKeys.ButtonToolCheckedHottrackCenterFillKey
        public static ResourceKey ButtonToolCheckedBorderFillKey = new MSGuiResourceKey("ButtonToolCheckedBorderFillKey"); //RibbonBrushKeys.ButtonToolCheckedBorderFillKey
        public static ResourceKey ButtonToolCheckedInnerBorderFillKey = new MSGuiResourceKey("ButtonToolCheckedInnerBorderFillKey"); //RibbonBrushKeys.ButtonToolCheckedInnerBorderFillKey
        public static ResourceKey ButtonToolCheckedCenterFillKey = new MSGuiResourceKey("ButtonToolCheckedCenterFillKey"); //RibbonBrushKeys.ButtonToolCheckedCenterFillKey
        public static ResourceKey ButtonToolHoverBorderDarkFillKey = new MSGuiResourceKey("ButtonToolHoverBorderDarkFillKey"); //RibbonBrushKeys.ButtonToolHoverBorderDarkFillKey
        public static ResourceKey ButtonToolHoverCenterFillKey = new MSGuiResourceKey("ButtonToolHoverCenterFillKey"); //RibbonBrushKeys.ButtonToolHoverCenterFillKey
        public static ResourceKey ButtonToolHoverBorderLightFillKey = new MSGuiResourceKey("ButtonToolHoverBorderLightFillKey");
        public static ResourceKey ButtonToolDisabledBorderDarkFillKey = new MSGuiResourceKey("ButtonToolDisabledBorderDarkFillKey");
        public static ResourceKey ButtonToolDisabledCenterFillKey = new MSGuiResourceKey("ButtonToolDisabledCenterFillKey");
        public static ResourceKey ButtonToolDisabledBorderLightFillKey = new MSGuiResourceKey("ButtonToolDisabledBorderLightFillKey");
        public static ResourceKey DropdownGlyphNormalBackgroundFillKey = new MSGuiResourceKey("DropdownGlyphNormalBackgroundFillKey"); 


        private static Dictionary<string, Dictionary<string, Brush>> brushes = new Dictionary<string, Dictionary<string, Brush>>();
        public static Dictionary<string, Dictionary<string, Brush>> Brushes
        {
            get
            {
                lock (brushes)
                {
                    if (brushes.Count == 0)
                    {
                        ResourceDictionary resourceDictionary = Application.Current.Resources;
                        Dictionary<string, Brush> basicBrushes = new Dictionary<string, Brush>();
                        foreach (MSGuiBasicBrushID brushId in Enum.GetValues(typeof(MSGuiBasicBrushID)).Cast<MSGuiBasicBrushID>())
                        {
                            string name = brushId.ToString("g");
                            Brush basicBrush = resourceDictionary[new MSGuiResourceKey(name)] as Brush;
                            if (basicBrush != null)
                            {
                                basicBrushes.Add(name, basicBrush);
                            }
                        }
                        if (basicBrushes.Count > 0)
                        {
                            brushes.Add("Basic Colors", basicBrushes); 
                        }

                        Dictionary<string, Brush> miscBrushes = new Dictionary<string, Brush>();
                        foreach (MSGuiMiscBrushID brushId in Enum.GetValues(typeof(MSGuiMiscBrushID)).Cast<MSGuiMiscBrushID>())
                        {
                            string name = brushId.ToString("g");
                            Brush miscBrush = resourceDictionary[new MSGuiResourceKey(name)] as Brush;
                            if (miscBrush != null)
                            {
                                miscBrushes.Add(name, miscBrush);
                            }
                        }
                        if (miscBrushes.Count > 0)
                        {
                            brushes.Add("Misc Colors", miscBrushes);                
                        }

                        Dictionary<string, Brush> systemOverriddenBrushes = new Dictionary<string, Brush>(); 
                        Brush systemOverridenBrush = resourceDictionary[SystemColors.HighlightBrushKey] as Brush;
                        if (systemOverridenBrush != null) systemOverriddenBrushes.Add("SystemColors.HighlightBrush", systemOverridenBrush);
                        systemOverridenBrush = resourceDictionary[SystemColors.HighlightTextBrushKey] as Brush;
                        if (systemOverridenBrush != null) systemOverriddenBrushes.Add("SystemColors.HighlightTextBrush", systemOverridenBrush);
                        systemOverridenBrush = resourceDictionary[SystemColors.ControlTextBrushKey] as Brush;
                        if (systemOverridenBrush != null) systemOverriddenBrushes.Add("SystemColors.ControlTextBrush", systemOverridenBrush);
                        if (systemOverriddenBrushes.Count > 0)
                        {
                            brushes.Add("Overridden Colors", systemOverriddenBrushes);
                        }

                    }
                    return brushes;
                }
            }
        }
         
	}
}
