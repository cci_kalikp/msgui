﻿using System;
using System.Collections.Generic;
using System.Linq; 
using System.Windows.Markup;

namespace MorganStanley.MSDotNet.MSGui.Controls.Extensions
{
 
    public class EnumToItemsSourceExtension : MarkupExtension
    {
        private readonly Type type;

        public EnumToItemsSourceExtension(Type type_)
        {
            type = type_;
        }

        public override object ProvideValue(IServiceProvider serviceProvider_)
        {
            return Enum.GetValues(type);
        }
    }
}
