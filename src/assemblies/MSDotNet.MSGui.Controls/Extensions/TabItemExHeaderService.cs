﻿using System.Windows;
using Infragistics.Windows.Controls;
using System.Windows.Controls;
using System.Windows.Media;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MorganStanley.MSDotNet.MSGui.Controls.Extensions
{
    /// <summary>
    /// Instead of overriding the Template for TabItemEx to set the properties for header
    /// We hack by accessing the container "bd"(which is currently used in all themes of TabItemEx) used to hold the header 
    /// This save the effort of creating a new template for TabItemEx, and allows Styles defined upon TabItemEx to work, except the overrides we specified here
    /// The drawback is that once Infragistics changes the current implementation for TabItemEx, this would not take effect anymore
    /// </summary>
    public static class TabItemExHeaderService
    {

        #region Background
        public static readonly DependencyProperty BackgroundProperty =
            DependencyProperty.RegisterAttached("Background", typeof(Brush), typeof(TabItemExHeaderService), new UIPropertyMetadata(null, BackgroundChanged));

        public static Brush GetBackground(DependencyObject obj)
        {
            return (Brush)obj.GetValue(BackgroundProperty);
        }

        public static void SetBackground(DependencyObject obj, Brush value)
        {
            obj.SetValue(BackgroundProperty, value); 
        }
         
        private static void BackgroundChanged(DependencyObject d_, DependencyPropertyChangedEventArgs e_)
        {
            TabItemEx tabItem = d_ as TabItemEx;
            if (tabItem == null) return;
            if (tabItem.IsLoaded)
            {
                SetTabItemBackground(tabItem, e_.NewValue as Brush);
            } 
            SetTabItemBackground(tabItem, e_.NewValue as Brush);
        }

        private static void SetTabItemBackground(TabItemEx tabItem_, Brush brush_)
        {
            DependencyObject headerContainer = GetHeaderContainer(tabItem_);
            if (IsTransparent(brush_))
            {
                headerContainer.ClearValue(TabItemEx.BackgroundProperty);
            }
            else
            {
                headerContainer.SetValue(Control.BackgroundProperty, brush_);
            }
            foreach (Border border in headerContainer.FindVisualChildren<Border>())
            {
                if (IsTransparent(brush_))
                {
                    border.ClearValue(TabItemEx.BackgroundProperty);
                }
                else
                {
                    border.SetValue(TabItemEx.BackgroundProperty, brush_);
                }
            }
        }

        private static bool IsTransparent(Brush b_)
        {
            return b_ == null || b_ == Brushes.Transparent ||
                (b_ is SolidColorBrush && ((SolidColorBrush)b_).Color.R == 0 &&
                ((SolidColorBrush)b_).Color.G == 0 && ((SolidColorBrush)b_).Color.B == 0);
        }
        #endregion

        #region ToolTip
         
        public static object GetToolTip(DependencyObject obj)
        {
            return (object)obj.GetValue(ToolTipProperty);
        }

        public static void SetToolTip(DependencyObject obj, object value)
        {
            obj.SetValue(ToolTipProperty, value);
        }

        // Using a DependencyProperty as the backing store for ToolTip.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ToolTipProperty =
            DependencyProperty.RegisterAttached("ToolTip", typeof(object), typeof(TabItemExHeaderService), new UIPropertyMetadata(null, ToolTipChanged));

        private static void ToolTipChanged(DependencyObject d_, DependencyPropertyChangedEventArgs e_)
        {
            TabItemEx tabItem = d_ as TabItemEx;
            if (tabItem == null) return; 
            DependencyObject headerContainer = GetHeaderContainer(tabItem);
            ToolTipService.SetToolTip(headerContainer, e_.NewValue);
        }
        
        #endregion

        #region Margin
        public static Thickness GetMargin(DependencyObject obj)
        {
            return (Thickness)obj.GetValue(MarginProperty);
        }

        public static void SetMargin(DependencyObject obj, Thickness value)
        {
            obj.SetValue(MarginProperty, value);
        }

        // Using a DependencyProperty as the backing store for ToolTip.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MarginProperty =
            DependencyProperty.RegisterAttached("Margin", typeof(object), typeof(TabItemExHeaderService), new UIPropertyMetadata(new Thickness(0), MarginChanged));

        private static void MarginChanged(DependencyObject d_, DependencyPropertyChangedEventArgs e_)
        {
            TabItemEx tabItem = d_ as TabItemEx;
            if (tabItem == null) return;
            DependencyObject headerContainer = GetHeaderContainer(tabItem);
            headerContainer.SetValue(Control.MarginProperty, e_.NewValue);
        }
        #endregion

        private static DependencyObject GetHeaderContainer(TabItemEx tabItem_)
        {
            tabItem_.ApplyTemplate();
            return tabItem_.Template.FindName("Bd", tabItem_) as DependencyObject ?? tabItem_;
        }
    }
}
