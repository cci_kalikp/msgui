﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MorganStanley.MSDotNet.MSGui.Controls.Extensions
{
    public static class DragDropService
    { 
        public static string GetAcceptedFormats(DependencyObject obj)
        {
            return (string)obj.GetValue(AcceptedFormatsProperty);
        }

        public static void SetAcceptedFormats(DependencyObject obj, string value)
        {
            obj.SetValue(AcceptedFormatsProperty, value);
        }

        // Using a DependencyProperty as the backing store for AcceptedFormats.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AcceptedFormatsProperty =
            DependencyProperty.RegisterAttached("AcceptedFormats", typeof(string), typeof(DragDropService), new UIPropertyMetadata(DataFormats.FileDrop,
               AcceptedFormatsChanged));

        private static void AcceptedFormatsChanged(DependencyObject d_, DependencyPropertyChangedEventArgs e_)
        {
            FrameworkElement control = d_ as FrameworkElement;
            UpdateDragDropStatus(control);
        }

        public static ICommand GetDropAction(DependencyObject obj)
        {
            return (ICommand)obj.GetValue(DropActionProperty);
        }

        public static void SetDropAction(DependencyObject obj, ICommand value)
        {
            obj.SetValue(DropActionProperty, value);
        }

        // Using a DependencyProperty as the backing store for DropAction.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DropActionProperty =
            DependencyProperty.RegisterAttached("DropAction", typeof(ICommand), typeof(DragDropService), new UIPropertyMetadata(DropActionChanged));

        private static void DropActionChanged(DependencyObject d_, DependencyPropertyChangedEventArgs e_)
        {
            FrameworkElement control = d_ as FrameworkElement;
            UpdateDragDropStatus(control);
        }

        private static readonly List<FrameworkElement> hookedControls = new List<FrameworkElement>();
        private static void UpdateDragDropStatus(FrameworkElement control_)
        {
            string format = GetAcceptedFormats(control_);
            ICommand command = GetDropAction(control_);
            if (!hookedControls.Contains(control_))
            {
                control_.HandleUnloaded(c_ =>
                {
                    c_.DragEnter -= new DragEventHandler(control__DragEnter);
                    c_.Drop -= new DragEventHandler(control__Drop);
                    hookedControls.Remove(c_);
                }); 
                hookedControls.Add(control_);
            }

            control_.DragEnter -= new DragEventHandler(control__DragEnter);
            control_.Drop -= new DragEventHandler(control__Drop);
            if (string.IsNullOrEmpty(format) || command == null)
            {
                control_.AllowDrop = false;
            }
            else
            {
                control_.AllowDrop = true;
                control_.DragEnter += new DragEventHandler(control__DragEnter);
                control_.Drop += new DragEventHandler(control__Drop);
            }
        }
 

        static void control__Drop(object sender, DragEventArgs e)
        {
            string format = GetAcceptedFormats(sender as FrameworkElement);
            ICommand command = GetDropAction(sender as FrameworkElement);
            if (command == null || format == null) return;
            object data = e.Data.GetData(format);
            if (command.CanExecute(data))
            {
                command.Execute(data);
            }
        }

        static void control__DragEnter(object sender, DragEventArgs e)
        {
            string format = GetAcceptedFormats(sender as FrameworkElement);
            if (e.Data.GetDataPresent(format, false))
            {
                e.Effects = DragDropEffects.All;
            } 
        }
    }
}
