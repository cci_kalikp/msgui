﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Markup;
using System.Windows.Media;
using MorganStanley.MSDotNet.MSGui.Controls.BarMenu;

namespace MorganStanley.MSDotNet.MSGui.Controls.SplitButton
{
	/// <summary>
	/// Implementation of a Split Button
	/// </summary>
	[TemplatePart(Name = "PART_DropDownButton", Type = typeof(Button))]
    [TemplatePart(Name = "PART_Button")]
    [ContentProperty("ItemsSource")]
    [DefaultProperty("ItemsSource")]
	public class SplitButton : Button, ITool
	{
		// AddOwner Dependency properties
		public static readonly DependencyProperty HorizontalOffsetProperty;
		public static readonly DependencyProperty IsDropDownOpenProperty;
		public static readonly DependencyProperty ModeProperty;
        public static readonly DependencyProperty UsageProperty;
        public static readonly DependencyProperty PlacementProperty;
		public static readonly DependencyProperty PlacementRectangleProperty;
		public static readonly DependencyProperty VerticalOffsetProperty;
	    public static readonly DependencyProperty CaptionProperty;
	    public static readonly DependencyProperty ImageProperty;
	    public static readonly DependencyProperty RibbonDropDownStyleProperty;
	    public static readonly DependencyProperty LauncherBarDropDownStyleProperty;
	    public static readonly DependencyProperty NormalDropDownStyleProperty;
	    public static readonly DependencyProperty RibbonButtonTemplateProperty;
	    public static readonly DependencyProperty LauncherBarButtonTemplateProperty;
        public static readonly DependencyProperty NormalButtonTemplateProperty;
	    public static readonly DependencyProperty IsDropDownEnabledProperty;
		/// <summary>
		/// Static Constructor
		/// </summary>
		static SplitButton()
		{
			DefaultStyleKeyProperty.OverrideMetadata(typeof(SplitButton), new FrameworkPropertyMetadata(typeof(SplitButton)));
			IsDropDownOpenProperty = DependencyProperty.Register("IsDropDownOpen", typeof(bool), typeof(SplitButton), new FrameworkPropertyMetadata(false, new PropertyChangedCallback(OnIsDropDownOpenChanged)));
            IsDropDownEnabledProperty = DependencyProperty.Register("IsDropDownEnabled", typeof(bool), typeof(SplitButton), new FrameworkPropertyMetadata(true));

            ModeProperty = DependencyProperty.Register("Mode", typeof(SplitButtonMode), typeof(SplitButton), new FrameworkPropertyMetadata(SplitButtonMode.Split));
		    CaptionProperty = ToolHelper.CaptionProperty.AddOwner(typeof (SplitButton));
            ImageProperty = ToolHelper.ImageProperty.AddOwner(typeof(SplitButton));
			// AddOwner properties from the ContextMenuService class, we need callbacks from these properties
			// to update the Buttons ContextMenu properties
			PlacementProperty = ContextMenuService.PlacementProperty.AddOwner(typeof(SplitButton), new FrameworkPropertyMetadata(PlacementMode.Bottom, new PropertyChangedCallback(OnPlacementChanged)));
			PlacementRectangleProperty = ContextMenuService.PlacementRectangleProperty.AddOwner(typeof(SplitButton), new FrameworkPropertyMetadata(Rect.Empty, new PropertyChangedCallback(OnPlacementRectangleChanged)));
			HorizontalOffsetProperty = ContextMenuService.HorizontalOffsetProperty.AddOwner(typeof(SplitButton), new FrameworkPropertyMetadata(0.0, new PropertyChangedCallback(OnHorizontalOffsetChanged)));
			VerticalOffsetProperty = ContextMenuService.VerticalOffsetProperty.AddOwner(typeof(SplitButton), new FrameworkPropertyMetadata(0.0, new PropertyChangedCallback(OnVerticalOffsetChanged)));
            
            #region Usage specific 
            UsageProperty = DependencyProperty.Register("Usage", typeof(SplitButtonUsage), typeof(SplitButton), new FrameworkPropertyMetadata(SplitButtonUsage.NormalButton));
            
            RibbonButtonTemplateProperty = DependencyProperty.Register("RibbonButtonTemplate", typeof(ControlTemplate), typeof(SplitButton), new FrameworkPropertyMetadata(null, OnButtonTemplate));
            LauncherBarButtonTemplateProperty = DependencyProperty.Register("LauncherBarButtonTemplate", typeof(ControlTemplate), typeof(SplitButton), new FrameworkPropertyMetadata(null, OnButtonTemplate));
            NormalButtonTemplateProperty = DependencyProperty.Register("NormalButtonTemplate", typeof(ControlTemplate), typeof(SplitButton), new FrameworkPropertyMetadata(null, OnButtonTemplate));

            RibbonDropDownStyleProperty = DependencyProperty.Register("RibbonDropDownStyle", typeof(Style), typeof(SplitButton), new FrameworkPropertyMetadata(null, OnDropDownStyleChanged));
            LauncherBarDropDownStyleProperty = DependencyProperty.Register("LauncherBarDropDownStyle", typeof(Style), typeof(SplitButton), new FrameworkPropertyMetadata(null, OnDropDownStyleChanged));
            NormalDropDownStyleProperty = DependencyProperty.Register("NormalDropDownStyle", typeof(Style), typeof(SplitButton), new FrameworkPropertyMetadata(null, OnDropDownStyleChanged));
             
            #endregion
        }



        public ButtonSize ButtonSize
        {
            get { return (ButtonSize)GetValue(ButtonSizeProperty); }
            set { SetValue(ButtonSizeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ButtonSize.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ButtonSizeProperty = ToolHelper.ButtonSizeProperty.AddOwner(typeof(SplitButton)); 
        
		/*
		 * Overrides
		 * 
		*/
		/// <summary>
		/// OnApplyTemplate override, set up the click event for the dropdown if present in the template
		/// </summary>
		public override void OnApplyTemplate()
		{
			base.OnApplyTemplate();

			// set up the click event handler for the dropdown button
            ButtonBase dropDown = this.Template.FindName("PART_DropDownButton", this) as ButtonBase;
			if (dropDown != null)
				dropDown.Click += Dropdown_Click;

            ButtonBase button = this.Template.FindName("PART_Button", this) as ButtonBase;
            if (button != null)
            {
                button.Click += (sender_, args_) => this.OnClick();
            }
            this.ContextMenuOpening += ((sender_, eventArgs_) =>
                {
                    eventArgs_.Handled = true;
                 
            });
		}

       
		/// <summary>
		///     Handles the Base Buttons OnClick event
		/// </summary>
		protected override void OnClick()
		{
			switch (Mode)
			{
				case SplitButtonMode.Dropdown:
					OnDropdown();
					break;

				default:
					base.OnClick(); // forward on the Click event to the user
					break;
			}
		}

		/*
		 * Properties
		 * 
		*/


		/// <summary>
		/// The Split Button's Items property maps to the base classes ContextMenu.Items property
		/// If used in normal case, the itemsource should be a list of MenuItem
		/// Else, should be a list ot ToolMenuItem
		/// </summary>
        public IEnumerable ItemsSource
		{
			get
			{ 
                EnsureContextMenuIsValid();
				return this.ContextMenu.ItemsSource;
			}
            set
            {
                EnsureContextMenuIsValid();
                this.ContextMenu.ItemsSource = value;
            }
		}

	    public string Caption
	    {
	        get { return (string)this.GetValue(CaptionProperty); }
            set { SetValue(CaptionProperty, value);}
	    }

	    public ImageSource Image
	    {
            get { return (ImageSource) this.GetValue(ImageProperty); }
            set {SetValue(ImageProperty, value);}
	    }
		/*
		 * DependencyProperty CLR wrappers
		 * 
		*/

		/// <summary>
		/// Gets or sets the IsDropDownOpen property. 
		/// </summary>
		public bool IsDropDownOpen
		{
			get { return (bool)GetValue(IsDropDownOpenProperty); }
			set { SetValue(IsDropDownOpenProperty, value); }
		}

	    public bool IsDropDownEnabled
	    {
            get { return (bool) GetValue(IsDropDownEnabledProperty); }
            set { SetValue(IsDropDownEnabledProperty, value); }
	    }


		/// <summary>
		/// Placement of the Context menu
		/// </summary>
		public PlacementMode Placement
		{
			get { return (PlacementMode)GetValue(PlacementProperty); }
			set { SetValue(PlacementProperty, value); }
		}


		/// <summary>
		/// PlacementRectangle of the Context menu
		/// </summary>
		public Rect PlacementRectangle
		{
			get { return (Rect)GetValue(PlacementRectangleProperty); }
			set { SetValue(PlacementRectangleProperty, value); }
		}


		/// <summary>
		/// HorizontalOffset of the Context menu
		/// </summary>
		public double HorizontalOffset
		{
			get { return (double)GetValue(HorizontalOffsetProperty); }
			set { SetValue(HorizontalOffsetProperty, value); }
		}


		/// <summary>
		/// VerticalOffset of the Context menu
		/// </summary>
		public double VerticalOffset
		{
			get { return (double)GetValue(VerticalOffsetProperty); }
			set { SetValue(VerticalOffsetProperty, value); }
		}

		/// <summary>
		/// Defines the Mode of operation of the Button
		/// </summary>
		/// <remarks>
		///     The SplitButton two Modes are
		///     Split (default),    - the button has two parts, a normal button and a dropdown which exposes the ContextMenu
		///     Dropdown            - the button acts like a combobox, clicking anywhere on the button opens the Context Menu
		/// </remarks>
		public SplitButtonMode Mode
		{
			get { return (SplitButtonMode)GetValue(ModeProperty); }
			set { SetValue(ModeProperty, value); }
		}


        #region Button Usage Related
        /// <summary>
        /// Defines the usage of the button
        /// </summary>
        /// <remarks> 
        ///     The SplitButton there Usages are
        ///     RibbonButton            - the button is added as a ribbon button
        ///     LauncherBarButton       - the button is added as a launcher bar button
        ///     Normal (default)        - the button is added in other location
        /// </remarks>
        public SplitButtonUsage Usage
        {
            get { return (SplitButtonUsage)GetValue(UsageProperty); }
            set { SetValue(UsageProperty, value); }
        }

	    public ControlTemplate RibbonButtonTemplate
	    {
            get { return (ControlTemplate) GetValue(RibbonButtonTemplateProperty); }
            set { SetValue(RibbonButtonTemplateProperty, value);}
	    }

        public ControlTemplate LauncherBarButtonTemplate
        {
            get { return (ControlTemplate)GetValue(LauncherBarButtonTemplateProperty); }
            set { SetValue(LauncherBarButtonTemplateProperty, value); }
        }

        public ControlTemplate NormalButtonTemplate
        {
            get { return (ControlTemplate)GetValue(NormalButtonTemplateProperty); }
            set { SetValue(NormalButtonTemplateProperty, value); }
        }

        public Style RibbonDropDownStyle 
        {
            get { return (Style) GetValue(RibbonDropDownStyleProperty); }
            set {SetValue(RibbonDropDownStyleProperty, value);}
        }

        public Style LauncherBarDropDownStyle
        {
            get { return (Style)GetValue(LauncherBarDropDownStyleProperty); }
            set { SetValue(LauncherBarDropDownStyleProperty, value); }
        }

        public Style NormalDropDownStyle
        {
            get { return (Style)GetValue(NormalDropDownStyleProperty); }
            set { SetValue(NormalDropDownStyleProperty, value); }
        }


        private static void OnButtonTemplate(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SplitButton s = (SplitButton)d;
            switch (s.Usage)
            {
                case SplitButtonUsage.RibbonButton:
                    s.Template = s.RibbonButtonTemplate;
                    break;
                case SplitButtonUsage.LauncherBarButton:
                    s.Template = s.LauncherBarButtonTemplate;
                    break;
                default:
                    s.Template = s.NormalButtonTemplate;
                    break;
            }
        }

        private static void OnDropDownStyleChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            SplitButton s = (SplitButton)d;
            s.EnsureContextMenuIsValid();
            if (s.ContextMenu.Style == null)
            {
                switch (s.Usage)
                {
                    case SplitButtonUsage.RibbonButton:
                        s.ContextMenu.Style = s.RibbonDropDownStyle;
                        break;
                    case SplitButtonUsage.LauncherBarButton:
                        s.ContextMenu.Style = s.LauncherBarDropDownStyle;
                        break;
                    default:
                        s.ContextMenu.Style = s.NormalDropDownStyle;
                        break;
                }
            }
        }
        #endregion
        /*
		 * DependencyPropertyChanged callbacks
		 * 
		*/

		private static void OnIsDropDownOpenChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			SplitButton s = (SplitButton)d;
            if (!s.IsDropDownEnabled) return;

			s.EnsureContextMenuIsValid();

			if (!s.ContextMenu.HasItems)
				return;

			bool value = (bool)e.NewValue;

			if (value && !s.ContextMenu.IsOpen)
				s.ContextMenu.IsOpen = true;
			else if (!value && s.ContextMenu.IsOpen)
				s.ContextMenu.IsOpen = false;
		}


		/// <summary>
		/// Placement Property changed callback, pass the value through to the buttons context menu
		/// </summary>
		private static void OnPlacementChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			SplitButton s = d as SplitButton;
			if (s == null) return;

			s.EnsureContextMenuIsValid();
			s.ContextMenu.Placement = (PlacementMode)e.NewValue;
		}

		/// <summary>
		/// PlacementRectangle Property changed callback, pass the value through to the buttons context menu
		/// </summary>
		private static void OnPlacementRectangleChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			SplitButton s = d as SplitButton;
			if (s == null) return;

			s.EnsureContextMenuIsValid();
			s.ContextMenu.PlacementRectangle = (Rect)e.NewValue;
		}

		/// <summary>
		/// HorizontalOffset Property changed callback, pass the value through to the buttons context menu
		/// </summary>
		private static void OnHorizontalOffsetChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			SplitButton s = d as SplitButton;
			if (s == null) return;

			s.EnsureContextMenuIsValid();
			s.ContextMenu.HorizontalOffset = (double)e.NewValue;
		}

		/// <summary>
		/// VerticalOffset Property changed callback, pass the value through to the buttons context menu
		/// </summary>
		private static void OnVerticalOffsetChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			SplitButton s = d as SplitButton;
			if (s == null) return;

			s.EnsureContextMenuIsValid();
			s.ContextMenu.VerticalOffset = (double)e.NewValue;
		}

		/*
		 * Helper Methods
		 * 
		*/

	    private bool dropDownInitialized = false;
		/// <summary>
		/// Make sure the Context menu is not null
		/// </summary>
		private void EnsureContextMenuIsValid()
		{
		    if (this.ContextMenu == null)
		    {
		        this.ContextMenu = new ContextMenu();
                this.ContextMenu.PlacementTarget = this;
                this.ContextMenu.Placement = Placement; 
            }
            if (!dropDownInitialized)
            {  
                this.ContextMenu.Opened += ((sender_, routedEventArgs_) =>
                    {
                        var copy = Opened;
                        if (copy != null)
                        {
                            copy(this, routedEventArgs_);
                        }
                        if (Usage == SplitButtonUsage.NormalButton)
                        {
                            ContextMenu.Width = this.ActualWidth;
                        }
                        IsDropDownOpen = true;
                    });
                this.ContextMenu.Closed += ((sender_, routedEventArgs_) => IsDropDownOpen = false);
                dropDownInitialized = true;
            }
		}
         
		private void OnDropdown()
		{
            if (!IsDropDownEnabled) return;

			EnsureContextMenuIsValid();
            
            var copy = Opening;
            if (copy != null)
            {
                copy(this, EventArgs.Empty);
            }
			if (!this.ContextMenu.HasItems)
				return;

			this.ContextMenu.IsOpen = !IsDropDownOpen; // open it if closed, close it if open
		}

		/*
		 * Events
		 * 
		*/

		/// <summary>
		/// Event Handler for the Drop Down Button's Click event
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Dropdown_Click(object sender, RoutedEventArgs e)
		{
			OnDropdown();
			e.Handled = true;
		}

	    public event EventHandler Opening;
	    public event EventHandler Opened;
	}
}