﻿namespace MorganStanley.MSDotNet.MSGui.Controls.SplitButton
{
    public enum SplitButtonUsage
    {
        NormalButton  = 0,
        RibbonButton = 1,
        LauncherBarButton = 2,
    }
}