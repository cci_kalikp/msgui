﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;

namespace MorganStanley.MSDotNet.MSGui.Controls.SplitButton
{
    /// <summary>
    /// Class used for the ComponentResourceKey
    /// </summary>
    public class SplitButtonResources
    {
        public static ComponentResourceKey VistaSplitButtonStyleKey
        {
            get { return new ComponentResourceKey(typeof(SplitButtonResources), "vistaSplitButtonStyle"); }
        }
    }
}
