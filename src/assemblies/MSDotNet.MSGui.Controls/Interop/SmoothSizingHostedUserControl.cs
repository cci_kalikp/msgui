﻿ 
using System.Drawing; 
using System.Windows.Forms;

namespace MorganStanley.MSDotNet.MSGui.Controls.Interop
{
    public class SmoothSizingHostedUserControl : UserControl, ISmoothSizingHostedControl
    {
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }

        private Size? cachedSize;
        private bool resizing = false;
        protected override void SetBoundsCore(int x, int y, int width, int height, BoundsSpecified specified)
        {
            if (resizing && x == this.Left && y == this.Top)
            {
                if (width < this.Width)
                {
                    cachedSize = new Size(width, height); 

                    if (height > this.Height)
                    {
                        base.SetBoundsCore(x, y, this.Width, height, specified);
                    } 
                    return;
                }
                if (height < this.Height)
                {
                    cachedSize = new Size(width, height);

                    if (width > this.Width)
                    {
                        base.SetBoundsCore(x, y, width, this.Height, specified);
                    } 
                    return;
                }
            }

            base.SetBoundsCore(x, y, width, height, specified);
        }


        public void StartResize()
        {
            resizing = true;
        }

        public void EndResize()
        {
            resizing = false;
            if (cachedSize != null)
            {
                this.Size = cachedSize.Value;
                cachedSize = null;
            }
        }
    }
}
