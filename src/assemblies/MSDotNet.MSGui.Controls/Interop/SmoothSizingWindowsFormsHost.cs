﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using System.Windows.Threading;

namespace MorganStanley.MSDotNet.MSGui.Controls.Interop
{
    public class SmoothSizingWindowsFormsHost:WindowsFormsHost
    {
        //when the windows form host is resized, the size of the child windows form control is applied before the size of the host.
        //this would result in temporary blank area flickering when shrinking the host.
        //to solve this, we postpone the resizing of the child control, so that the host is shrunk first.
        protected override Size ArrangeOverride(Size finalSize_)
        {
            ISmoothSizingHostedControl hostedControl = this.Child as ISmoothSizingHostedControl;
            if (hostedControl != null)
            {
                hostedControl.StartResize();
            }
            var result = base.ArrangeOverride(finalSize_);
            Dispatcher.BeginInvoke(DispatcherPriority.Background, new MethodInvoker(delegate
            {
                if (hostedControl !=
                    null)
                {
                    hostedControl.EndResize();
                }
            }));
            return result;
        } 
    }
}
