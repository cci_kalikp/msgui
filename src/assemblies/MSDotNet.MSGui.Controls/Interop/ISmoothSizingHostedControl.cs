﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Controls.Interop
{
    public interface ISmoothSizingHostedControl
    {
        //flag to suspend the resize if needed
        void StartResize();
        //apply the size if needed here
        void EndResize();
    }
     
}
