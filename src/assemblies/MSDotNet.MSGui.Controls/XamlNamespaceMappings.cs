﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Controls/XamlNamespaceMappings.cs#6 $
// $Change: 823738 $
// $DateTime: 2013/04/10 15:26:24 $
// $Author: smulovic $

using System.Runtime.CompilerServices;
using System.Windows.Markup;

[assembly: XmlnsDefinitionAttribute
  ("http://schemas.morganstanley.com/msdotnet/msgui/xaml/themes", "MorganStanley.MSDotNet.MSGui.Controls")]

[assembly: InternalsVisibleTo("MSDotnet.MSGui.Themes, PublicKey=0024000004800000940000000602000000240000525341310004000001000100A53E632AB17B4AE228F55617FA35B87735382F10F640472009578FEA68385EB2E18DA0E33ACCED3D638FC4F6405EE021A63D2A6A9FDDA7BA4F8C281211E0C74DCBEC713FA74686DBE7B4606F4464C5669B810FD5951104D6B72779C9E45EDFD512E6400F3C429AFFD681E8BAA8B108908610BD62F76ECAF2DD8807BA32B3A5EC")]
[assembly: InternalsVisibleTo("MSDesktop.FX, PublicKey=0024000004800000940000000602000000240000525341310004000001000100a53e632ab17b4ae228f55617fa35b87735382f10f640472009578fea68385eb2e18da0e33acced3d638fc4f6405ee021a63d2a6a9fdda7ba4f8c281211e0c74dcbec713fa74686dbe7b4606f4464c5669b810fd5951104d6b72779c9e45edfd512e6400f3c429affd681e8baa8b108908610bd62f76ecaf2dd8807ba32b3a5ec")]