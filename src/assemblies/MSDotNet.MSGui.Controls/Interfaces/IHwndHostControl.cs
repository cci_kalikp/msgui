﻿using System.Windows;
using System.Windows.Input;

namespace MorganStanley.MSDotNet.MSGui.Controls.Interfaces
{
    internal interface IHwndHostControl
    {
        bool IsVisible { get; }
        bool IsMouseOnControl(Point point);
        void MouseMove(MouseEventArgs eventArgs);
        void MouseEnter(MouseEventArgs eventArgs);
        void MouseLeave(MouseEventArgs eventArgs);
        void MouseLeftButtonUp(MouseEventArgs point);
        void MouseLeftButtonDown(MouseEventArgs point);
    }
}
