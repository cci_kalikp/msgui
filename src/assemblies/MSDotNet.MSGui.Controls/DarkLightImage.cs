﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace MorganStanley.MSDotNet.MSGui.Controls
{
  public class DarkLightImage : Control
  {
    static DarkLightImage()
    {
      DefaultStyleKeyProperty.OverrideMetadata(typeof(DarkLightImage), new FrameworkPropertyMetadata(typeof(DarkLightImage)));
    }

    public static readonly DependencyProperty StretchProperty = DependencyProperty.Register(
      "Stretch", typeof(Stretch), typeof(DarkLightImage), new PropertyMetadata(Stretch.Uniform));

    public static readonly DependencyProperty StretchDirectionProperty = DependencyProperty.Register(
      "StretchDirection", typeof(StretchDirection), typeof(DarkLightImage), new PropertyMetadata(StretchDirection.Both));

    public static readonly DependencyProperty LightSourceProperty = DependencyProperty.Register(
      "LightSource", typeof(ImageSource), typeof(DarkLightImage), new PropertyMetadata(null));

    public static readonly DependencyProperty DarkSourceProperty = DependencyProperty.Register(
      "DarkSource", typeof(ImageSource), typeof(DarkLightImage), new PropertyMetadata(null));

    public ImageSource LightSource { get; set; }
    public ImageSource DarkSource { get; set; }
    public Stretch Stretch { get; set; }
    public StretchDirection StretchDirection { get; set; }
  }
}
