﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MorganStanley.MSDotNet.MSGui.Controls.Controls
{
    /// <summary>
    /// Interaction logic for DiscreteTimePicker.xaml
    /// </summary>
    public partial class DiscreteTimePicker : UserControl
    {
        public DiscreteTimePicker()
        {
            InitializeComponent(); 
        }

 

        public int HourStep
        {
            get { return (int)GetValue(HourStepProperty); }
            set { SetValue(HourStepProperty, value); }
        }

        // Using a DependencyProperty as the backing store for HourStep.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HourStepProperty =
            DependencyProperty.Register("HourStep", typeof(int), typeof(DiscreteTimePicker),
                                        new UIPropertyMetadata(1, 
                                            (o_, args_) => 

                                            {
                                                DiscreteTimePicker picker = o_ as DiscreteTimePicker;
                                                if (picker != null)
                                                {
                                                    picker.CoerceValue(AvailableHoursProperty);
                                                    picker.CoerceValue(HoursProperty);
                                                }
                                            }),
                                            (value_) =>
                                                {
                                                    int hourstep = (int)value_;
                                                    return hourstep > 0 && hourstep < 25 && 24 % hourstep == 0;
                                                }
                );

        public int MinuteStep
        {
            get { return (int)GetValue(MinuteStepProperty); }
            set { SetValue(MinuteStepProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MinuteStep.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MinuteStepProperty =
            DependencyProperty.Register("MinuteStep", typeof(int), typeof(DiscreteTimePicker), new UIPropertyMetadata(5,
           (o_, args_) => 
            {
                DiscreteTimePicker picker = o_ as DiscreteTimePicker;
                if (picker != null)
                {
                    picker.CoerceValue(AvailableMinutesProperty);
                    picker.CoerceValue(MinutesProperty);
                }
            }),
             (value_) => 
            {
                int minutestep = (int)value_;
                return minutestep > 0 && minutestep < 60 && 60 % minutestep == 0;
            }
            );


        public int Hours
        {
            get { return (int)GetValue(HoursProperty); }
            set { SetValue(HoursProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Hours.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HoursProperty =
            DependencyProperty.Register("Hours", typeof(int), typeof(DiscreteTimePicker), new UIPropertyMetadata(0,
             (o_, args_) =>

            {
                DiscreteTimePicker picker = o_ as DiscreteTimePicker;
                if (picker != null && !picker.settingTimeInternally)
                {
                    picker.Time = new TimeSpan(picker.Hours, picker.Minutes, 0);
                }
            },
            (o_, value_) =>
                {
                    DiscreteTimePicker picker = o_ as DiscreteTimePicker;
                    if (picker != null)
                    {
                        int hour = (int)value_;
                        return hour/picker.HourStep * picker.HourStep;
                    }
                    return 0;
                }),
            (value_) =>

            {
                int hour = (int)value_;
                return hour < 24 && hour >= 0;
            }
            );


        public int Minutes
        {
            get { return (int)GetValue(MinutesProperty); }
            set { SetValue(MinutesProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Minute.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MinutesProperty =
            DependencyProperty.Register("Minutes", typeof(int), typeof(DiscreteTimePicker), new UIPropertyMetadata(0, 
             (o_, args_) =>

            {
                DiscreteTimePicker picker = o_ as DiscreteTimePicker;
                if (picker != null && !picker.settingTimeInternally)
                {
                    picker.Time = new TimeSpan(picker.Hours, picker.Minutes, 0);
                }
            }, 
            (o_, value_) =>
                {
                    DiscreteTimePicker picker = o_ as DiscreteTimePicker;
                    if (picker != null)
                    {
                        int minute = (int)value_;
                        return minute / picker.MinuteStep * picker.MinuteStep;
                    }
                    return 0;
                })
                , 
                (value_) =>
            {
                int minute = (int)value_;
                return minute < 60 && minute >= 0;
            }
            ); 
         
        
        public TimeSpan Time
        {
            get { return (TimeSpan)GetValue(TimeProperty); }
            set { SetValue(TimeProperty, value); }
        }

        private bool settingTimeInternally = false;
        // Using a DependencyProperty as the backing store for Time.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TimeProperty =
            DependencyProperty.Register("Time", typeof(TimeSpan), typeof(DiscreteTimePicker), new UIPropertyMetadata(new TimeSpan(0),
            (o_, args_) =>
            {
                DiscreteTimePicker picker = o_ as DiscreteTimePicker;
                if (picker != null && !picker.settingTimeInternally)
                {
                    TimeSpan time = picker.Time;
                    picker.settingTimeInternally = true;
                    try
                    {
                        picker.Hours = time.Hours;
                        picker.Minutes = time.Minutes; 
                        picker.Time = new TimeSpan(picker.Hours, picker.Minutes, 0);
                    }
                    finally
                    {
                        picker.settingTimeInternally = false;
                    }
                }
            }));


        public List<int> AvailableHours
        {
            get { return (List<int>)GetValue(AvailableHoursProperty); }
            set { SetValue(AvailableHoursProperty, value); }
        }

        // Using a DependencyProperty as the backing store for AvailableHours.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AvailableHoursProperty =
            DependencyProperty.Register("AvailableHours", typeof(List<int>), typeof(DiscreteTimePicker), new UIPropertyMetadata(new List<int>
                {
                    0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23
                }, null, (o_, value_) =>
                    {
                        List<int> list = new List<int>();
                        DiscreteTimePicker picker = o_ as DiscreteTimePicker;
                        if (picker != null)
                        { 
                            for (int hour = 0; hour < 24; hour += picker.HourStep)
                            {
                                list.Add(hour);
                            }
                        } 
                        return list;
                    }));



        public List<int> AvailableMinutes
        {
            get { return (List<int>)GetValue(AvailableMinutesProperty); }
            set { SetValue(AvailableMinutesProperty, value); }
        }

        // Using a DependencyProperty as the backing store for AvailableMinutes.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AvailableMinutesProperty =
            DependencyProperty.Register("AvailableMinutes", typeof(List<int>), typeof(DiscreteTimePicker), new UIPropertyMetadata(new List<int>
                {
                    0,5,10,15,20,25,30,35,40,45,50,55
                }, 
                null, 
                (o_, value_) =>
                {
                    List<int> list = new List<int>();
                    DiscreteTimePicker picker = o_ as DiscreteTimePicker;
                    if (picker != null)
                    {
                        for (int minute = 0; minute < 60; minute += picker.MinuteStep)
                        {
                            list.Add(minute);
                        }
                    }
                    return list;
                }));


    }
}
