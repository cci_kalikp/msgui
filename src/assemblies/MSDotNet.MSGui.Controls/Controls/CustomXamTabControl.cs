﻿using System;
using Infragistics.Windows.Controls;
using System.Windows;
using System.Windows.Controls;
using System.Collections;
using System.Collections.Specialized;
using System.Windows.Input;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MorganStanley.MSDotNet.MSGui.Controls.Controls
{
    public class CustomXamTabControl : XamTabControl
    {
        public CustomXamTabControl()
        {
            this.SetResourceReference(FrameworkElement.StyleProperty, typeof(XamTabControl));
        }
        public IList TabItemsSource
        {
            get { return (IList)GetValue(TabItemsSourceProperty); }
            set { SetValue(TabItemsSourceProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TabItemSource.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TabItemsSourceProperty =
            DependencyProperty.Register("TabItemsSource", typeof(IList), typeof(CustomXamTabControl), new UIPropertyMetadata(null, TabItemsSourceChanged));
         
        public object CurrentItem
        {
            get { return (object)GetValue(CurrentItemProperty); }
            set { SetValue(CurrentItemProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CurrentItem.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CurrentItemProperty =
            DependencyProperty.Register("CurrentItem", typeof(object), typeof(CustomXamTabControl), new UIPropertyMetadata(null, CurrentItemChanged)); 


        public DataTemplate TabItemContentTemplate
        {
            get { return (DataTemplate)GetValue(TabItemContentTemplateProperty); }
            set { SetValue(TabItemContentTemplateProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TabItemContentTemplate.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TabItemContentTemplateProperty =
            DependencyProperty.Register("TabItemContentTemplate", typeof(DataTemplate), typeof(CustomXamTabControl), new UIPropertyMetadata(null));
           
        public DataTemplateSelector TabItemContentTemplateSelector
        {
            get { return (DataTemplateSelector)GetValue(TabItemContentTemplateSelectorProperty); }
            set { SetValue(TabItemContentTemplateSelectorProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TabItemContentTemplateSelector.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TabItemContentTemplateSelectorProperty =
            DependencyProperty.Register("TabItemContentTemplateSelector", typeof(DataTemplateSelector), typeof(CustomXamTabControl), new UIPropertyMetadata(null));


        public event EventHandler<EventArgs<TabItemEx>> TabItemAdded;
        public event EventHandler<EventArgs<TabItemEx>> TabItemRemoved;

        protected override void OnSelectionChanged(SelectionChangedEventArgs e_)
        {
            base.OnSelectionChanged(e_);
            if (e_.AddedItems.Count > 0)
            {
                TabItemEx tabItem = e_.AddedItems[0] as TabItemEx;
                if (tabItem != null && tabItem.DataContext != null)
                {
                    CurrentItem = tabItem.DataContext;
                    return;
                }
            } 
            CurrentItem = null; 
        }
 
        public bool DisposeItemsSourceAutomatically
        {
            get { return (bool)GetValue(DisposeItemsSourceAutomaticallyProperty); }
            set { SetValue(DisposeItemsSourceAutomaticallyProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DisposeItemsSourceAutomatically.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DisposeItemsSourceAutomaticallyProperty =
            DependencyProperty.Register("DisposeItemsSourceAutomatically", typeof(bool), typeof(CustomXamTabControl), new PropertyMetadata(true));


        private static void TabItemsSourceChanged(DependencyObject d_, DependencyPropertyChangedEventArgs e_)
        {
            CustomXamTabControl tab = d_ as CustomXamTabControl;
            if (tab.DisposeItemsSourceAutomatically)
            {
                object[] items = new object[tab.Items.Count];
                tab.Items.CopyTo(items, 0);
                foreach (TabItemEx tabItem in items)
                {
                    if (tabItem != null)
                    {
                        tab.DisposeItem(e_.OldValue as IList, tabItem.DataContext);
                        tabItem.Closed -= new RoutedEventHandler(tab.tabItem_Closed);
                        tab.Items.Remove(tabItem);
                        ClearContent(tabItem);
                    }
                }
            }
           
            INotifyCollectionChanged oldNc = e_.OldValue as INotifyCollectionChanged;
            if (oldNc != null)
            {
                oldNc.CollectionChanged -= new NotifyCollectionChangedEventHandler(tab.TabItemsSource_CollectionChanged); 
            }

            if (tab.TabItemsSource != null)
            {
                foreach (object item in tab.TabItemsSource)
                {
                    tab.NewTabItem(item);
                }
                INotifyCollectionChanged nc = tab.TabItemsSource as INotifyCollectionChanged;
                if (nc != null)
                {
                    nc.CollectionChanged += new NotifyCollectionChangedEventHandler(tab.TabItemsSource_CollectionChanged);
                }
            }

        }

        private static void CurrentItemChanged(DependencyObject d_, DependencyPropertyChangedEventArgs e_)
        {
            CustomXamTabControl tab = d_ as CustomXamTabControl;
            if (e_.NewValue != null)
            {
                foreach (TabItemEx tabItem in tab.Items)
                {
                    if (tabItem != null && tabItem.DataContext == e_.NewValue)
                    {
                        tabItem.IsSelected = true;
                        break;
                    }
                }
            } 
        }
        
        void TabItemsSource_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (object item in e.NewItems)
                {
                    NewTabItem(item);
                } 
            } 
            else if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (var oldItem in e.OldItems)
                {
                    foreach (TabItemEx tabItem in Items)
                    {
                        if (tabItem.DataContext == oldItem)
                        {
                            ClearContent(tabItem);
                            tabItem.ExecuteCommand(TabItemExCommands.Close);
                            break;
                        }
                    }
                }
            }
        }

        
        private void NewTabItem(object item_)
        {
            object content = null;
            DataTemplate template = TabItemContentTemplate;
            if (template == null)
            {
                if (this.TabItemContentTemplateSelector != null)
                {
                    template = this.TabItemContentTemplateSelector.SelectTemplate(item_, this);
                    if (template != null)
                    {
                        template.Seal();
                    }
                }
                else
                {
                   template = this.TryFindResource(new DataTemplateKey(item_.GetType())) as DataTemplate;
                }

            }
            if (template != null)
            {
                content = template.LoadContent(); 
            }
            TabItemEx tabItem = new TabItemEx() { DataContext = item_, Content = content };
            tabItem.Closing += tabItem_Closing;
            tabItem.Closed += tabItem_Closed;
            this.AddChild(tabItem);
            if (item_ == CurrentItem)
            {
                tabItem.IsSelected = true;
            }
            var copy = TabItemAdded;
            if (copy != null)
            {
                copy(this, new EventArgs<TabItemEx>(tabItem));
            }
        }

        void tabItem_Closing(object sender_, Infragistics.Windows.Controls.Events.TabClosingEventArgs e)
        {
            IList items = this.TabItemsSource;
            TabItemEx tabItem = (TabItemEx)sender_;
            if (items != null)
            {
                IClosable item = tabItem.DataContext as IClosable;
                if (item != null && !item.Close())
                {
                    e.Cancel = true;
                    return;
                }
            }
            tabItem.Closing -= tabItem_Closing; 

        }
        
        void tabItem_Closed(object sender_, RoutedEventArgs e_)
        {
            IList items = this.TabItemsSource;
            e_.Handled = true;
            TabItemEx tabItem = (TabItemEx)sender_;
            if (items != null)
            {
                object item = tabItem.DataContext;
                DisposeItem(items, item);
                ClearContent(tabItem);
            }
            tabItem.Closed -= tabItem_Closed;
            this.Items.Remove(sender_);
            var copy = TabItemRemoved;
            if (copy != null)
            {
                copy(this, new EventArgs<TabItemEx>(tabItem));
            }
        }

        private void DisposeItem(IList items_, object item_)
        {
            if (items_ != null)
            {
                items_.Remove(item_);
            }
            IDisposable disposable = item_ as IDisposable;
            if (disposable != null)
            {
                disposable.Dispose();
            } 
        }

        private static void ClearContent(TabItemEx tabItem_)
        {
            FocusManager.SetFocusedElement(tabItem_, null); 
            tabItem_.DataContext = null; 
            tabItem_.Content = null;
        }
    }
}
