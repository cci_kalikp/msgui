﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Security;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;

namespace MorganStanley.MSDotNet.MSGui.Controls.Controls
{
     [StyleTypedProperty(Property = "ScrollRightButtonStyle", StyleTargetType = typeof(ButtonBase))] 
     [TemplatePart(Name = "PART_ScrollLeft", Type = typeof(FrameworkElement))]
     [StyleTypedProperty(Property = "ScrollUpButtonStyle", StyleTargetType = typeof(ButtonBase))]
     [StyleTypedProperty(Property = "ScrollDownButtonStyle", StyleTargetType = typeof(ButtonBase))]
    [TemplatePart(Name = "PART_ScrollRight", Type = typeof(FrameworkElement))]
    [TemplatePart(Name = "PART_ScrollDown", Type = typeof(FrameworkElement))] 
    [TemplatePart(Name = "PART_PagerContentPresenter", Type = typeof(PagerContentPresenter))]
    [DesignTimeVisible(false)]
    [StyleTypedProperty(Property = "ScrollLeftButtonStyle", StyleTargetType = typeof(ButtonBase))] 
    [TemplatePart(Name = "PART_ScrollUp", Type = typeof(FrameworkElement))]
     
    public class XamPager : ScrollViewer
    {
        // Fields
        private PagerContentPresenter _contentPresenter;
        private bool _ignoreBringIntoView; 
        private FrameworkElement _scrollDownElement;
        private FrameworkElement _scrollLeftElement;
        private FrameworkElement _scrollRightElement;
        private FrameworkElement _scrollUpElement;

        public static readonly DependencyProperty ScrollDownButtonStyleProperty =
            DependencyProperty.Register("ScrollDownButtonStyle", typeof (Style), typeof (XamPager),
                                        new FrameworkPropertyMetadata(null));


        private static readonly DependencyPropertyKey ScrollDownVisibilityPropertyKey =
            DependencyProperty.RegisterReadOnly("ScrollDownVisibility", typeof (Visibility), typeof (XamPager),
                                                new FrameworkPropertyMetadata(Visibility.Visible,
                                                                              new PropertyChangedCallback(
                                                                                  XamPager.OnScrollVisibilityChanged)));

        public static readonly DependencyProperty ScrollDownVisibilityProperty =
            ScrollDownVisibilityPropertyKey.DependencyProperty;

        public static readonly DependencyProperty ScrollLeftButtonStyleProperty =
            DependencyProperty.Register("ScrollLeftButtonStyle", typeof (Style), typeof (XamPager),
                                        new FrameworkPropertyMetadata(null));

        private static readonly DependencyPropertyKey ScrollLeftVisibilityPropertyKey =
            DependencyProperty.RegisterReadOnly("ScrollLeftVisibility", typeof (Visibility), typeof (XamPager),
                                                new FrameworkPropertyMetadata(Visibility.Visible,
                                                                              new PropertyChangedCallback(
                                                                                  XamPager.OnScrollVisibilityChanged)));


        public static readonly DependencyProperty ScrollLeftVisibilityProperty =
            ScrollLeftVisibilityPropertyKey.DependencyProperty;

        public static readonly DependencyProperty ScrollRightButtonStyleProperty =
            DependencyProperty.Register("ScrollRightButtonStyle", typeof (Style), typeof (XamPager),
                                        new FrameworkPropertyMetadata(null));

        private static readonly DependencyPropertyKey ScrollRightVisibilityPropertyKey =
            DependencyProperty.RegisterReadOnly("ScrollRightVisibility", typeof (Visibility), typeof (XamPager),
                                                new FrameworkPropertyMetadata(Visibility.Visible,
                                                                              new PropertyChangedCallback(
                                                                                  XamPager.OnScrollVisibilityChanged)));

        public static readonly DependencyProperty ScrollRightVisibilityProperty =
            ScrollRightVisibilityPropertyKey.DependencyProperty;


        public static readonly DependencyProperty ScrollUpButtonStyleProperty =
            DependencyProperty.Register("ScrollUpButtonStyle", typeof (Style), typeof (XamPager),
                                        new FrameworkPropertyMetadata(null));


        private static readonly DependencyPropertyKey ScrollUpVisibilityPropertyKey =
            DependencyProperty.RegisterReadOnly("ScrollUpVisibility", typeof (Visibility), typeof (XamPager),
                                                new FrameworkPropertyMetadata(Visibility.Visible,
                                                                              new PropertyChangedCallback(
                                                                                  XamPager.OnScrollVisibilityChanged)));

        public static readonly DependencyProperty ScrollUpVisibilityProperty =
            ScrollUpVisibilityPropertyKey.DependencyProperty;

        // Methods
        static XamPager()
        {
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof (XamPager),
                                                                      new FrameworkPropertyMetadata(typeof (XamPager)));
            CanExecuteRoutedEventHandler handler = new CanExecuteRoutedEventHandler(XamPager.OnCanExecuteScrollCommand);
            CommandManager.RegisterClassCommandBinding(typeof (XamPager),
                                                       new CommandBinding(ScrollBar.LineLeftCommand, null, handler));
            CommandManager.RegisterClassCommandBinding(typeof (XamPager),
                                                       new CommandBinding(ScrollBar.LineRightCommand, null, handler));
            CommandManager.RegisterClassCommandBinding(typeof (XamPager),
                                                       new CommandBinding(ScrollBar.LineUpCommand, null, handler));
            CommandManager.RegisterClassCommandBinding(typeof (XamPager),
                                                       new CommandBinding(ScrollBar.LineDownCommand, null, handler));
            EventManager.RegisterClassHandler(typeof (XamPager), FrameworkElement.RequestBringIntoViewEvent,
                                              new RequestBringIntoViewEventHandler(XamPager.OnRequestBringIntoView));
        }

        public XamPager()
        {
             
        }

        internal FrameworkElement GetScrollElement(PagerScrollDirection direction)
        {
            switch (direction)
            {
                case PagerScrollDirection.Up:
                    return this._scrollUpElement;

                case PagerScrollDirection.Down:
                    return this._scrollDownElement;

                case PagerScrollDirection.Left:
                    return this._scrollLeftElement;

                case PagerScrollDirection.Right:
                    return this._scrollRightElement;
            }
            return null;
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            this._scrollDownElement = base.GetTemplateChild("PART_ScrollDown") as FrameworkElement;
            this._scrollUpElement = base.GetTemplateChild("PART_ScrollUp") as FrameworkElement;
            this._scrollLeftElement = base.GetTemplateChild("PART_ScrollLeft") as FrameworkElement;
            this._scrollRightElement = base.GetTemplateChild("PART_ScrollRight") as FrameworkElement;
            this._contentPresenter = base.GetTemplateChild("PART_PagerContentPresenter") as PagerContentPresenter;
        }

        private static void OnCanExecuteScrollCommand(object sender, CanExecuteRoutedEventArgs e)
        {
            XamPager pager = sender as XamPager;
            if ((pager != null) && (pager.ScrollInfo != null))
            {
                PagerContentPresenter presenter1 = pager._contentPresenter;
                IScrollInfo scrollInfo = pager.ScrollInfo;
            }
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (!e.Handled && (e.OriginalSource != this))
            {
                switch (e.Key)
                {
                    case Key.End:
                    case Key.Home:
                        if (e.KeyboardDevice.Modifiers != ModifierKeys.Control)
                        {
                            break;
                        }
                        return;

                    case Key.Left:
                    case Key.Up:
                    case Key.Right:
                    case Key.Down:
                        if (this.DoesParentHandleScrolling)
                        {
                            break;
                        }
                        this.ProcessArrowKey(e);
                        return;
                }
            }
            base.OnKeyDown(e);
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            if (e.Property == ScrollViewer.ComputedHorizontalScrollBarVisibilityProperty)
            {
                this.VerifyScrollHorizontalVisibility();
            }
            else if (e.Property == ScrollViewer.ComputedVerticalScrollBarVisibilityProperty)
            {
                this.VerifyScrollVerticalVisibility();
            }
            if ((this._contentPresenter != null) &&
                (((e.Property == ScrollLeftVisibilityProperty) || (e.Property == ScrollRightVisibilityProperty)) ||
                 ((e.Property == ScrollUpVisibilityProperty) || (e.Property == ScrollDownVisibilityProperty))))
            {
                this._contentPresenter.InvalidateVisual();
            }
            base.OnPropertyChanged(e);
        }

        private void OnRequestBringIntoView(RequestBringIntoViewEventArgs e)
        {
            PagerContentPresenter presenter = this._contentPresenter;
            if ((!this._ignoreBringIntoView && (presenter != null)) && (base.ScrollInfo != presenter))
            {
                FrameworkElement targetObject = e.TargetObject as FrameworkElement;
                if ((targetObject != null) && presenter.IsAncestorOf(targetObject))
                {
                    Rect targetRect = e.TargetRect;
                    if (targetRect.IsEmpty)
                    {
                        targetRect = new Rect(targetObject.RenderSize);
                    }
                    Rect renderRect = new Rect(presenter.RenderSize);
                    Rect rect = renderRect;
                    presenter.AdjustRect(ref renderRect, PagerScrollDirection.Left);
                    presenter.AdjustRect(ref renderRect, PagerScrollDirection.Right);
                    presenter.AdjustRect(ref renderRect, PagerScrollDirection.Up);
                    presenter.AdjustRect(ref renderRect, PagerScrollDirection.Down);
                    GeneralTransform transform = presenter.TransformToDescendant(targetObject);
                    if (transform != null)
                    {
                        renderRect = transform.TransformBounds(renderRect);
                        rect = transform.TransformBounds(rect);
                        Rect rect4 = Rect.Intersect(targetRect, rect);
                        Rect rect5 = Rect.Intersect(rect4, renderRect);
                        if (rect5 != rect4)
                        {
                            if (rect5.Width != rect4.Width)
                            {
                                double num = 0.0;
                                if (rect4.Right > renderRect.Right)
                                {
                                    targetRect.Width += rect4.Right - renderRect.Right;
                                }
                                if (((rect4.Left - num) - renderRect.Left) < 0.0)
                                {
                                    targetRect.X += (rect4.Left - num) - renderRect.Left;
                                }
                            }
                            if (rect5.Height != rect4.Height)
                            {
                                double num2 = 0.0;
                                if (rect4.Bottom > renderRect.Bottom)
                                {
                                    targetRect.Height += rect4.Bottom - renderRect.Bottom;
                                }
                                if (((rect4.Top - num2) - renderRect.Top) < 0.0)
                                {
                                    targetRect.Y += (rect4.Top - num2) - renderRect.Top;
                                }
                            }
                            e.Handled = true;
                            this._ignoreBringIntoView = true;
                            try
                            {
                                ((FrameworkElement) e.TargetObject).BringIntoView(targetRect);
                            }
                            finally
                            {
                                this._ignoreBringIntoView = false;
                            }
                        }
                    }
                }
            }
        }

        private static void OnRequestBringIntoView(object sender, RequestBringIntoViewEventArgs e)
        {
            (sender as XamPager).OnRequestBringIntoView(e);
        }

        protected override void OnScrollChanged(ScrollChangedEventArgs e)
        {
            this.VerifyScrollHorizontalVisibility();
            this.VerifyScrollVerticalVisibility();
            if ((e.HorizontalChange != 0.0) || (e.VerticalChange != 0.0))
            {
                CommandManager.InvalidateRequerySuggested();
            }
            else
            {
                bool flag = ((e.ExtentWidth - e.ExtentWidthChange) - (e.ViewportWidth - e.ViewportWidthChange)) > 0.0;
                bool flag2 = (e.ExtentWidth - e.ViewportWidth) > 0.0;
                if (flag != flag2)
                {
                    CommandManager.InvalidateRequerySuggested();
                }
            }
            base.OnScrollChanged(e);
        }

        private static void OnScrollVisibilityChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            CommandManager.InvalidateRequerySuggested();
        }

        private void ProcessArrowKey(KeyEventArgs e)
        {
            FocusNavigationDirection left;
            DependencyObject obj2;
            if (((this._contentPresenter != null) && (e.OriginalSource is DependencyObject)) &&
                this._contentPresenter.IsAncestorOf(e.OriginalSource as DependencyObject))
            {
                switch (e.Key)
                {
                    case Key.Left:
                        left = FocusNavigationDirection.Left;
                        goto Label_005F;

                    case Key.Up:
                        left = FocusNavigationDirection.Up;
                        goto Label_005F;

                    case Key.Right:
                        left = FocusNavigationDirection.Right;
                        goto Label_005F;

                    case Key.Down:
                        left = FocusNavigationDirection.Down;
                        goto Label_005F;
                }
            }
            return;
            Label_005F:
            obj2 = Keyboard.FocusedElement as DependencyObject;
            DependencyObject descendant = null;
            if (this._contentPresenter.IsDescendantInView(obj2))
            {
                if (obj2 is UIElement)
                {
                    descendant = ((UIElement) obj2).PredictFocus(left);
                }
                else if (obj2 is ContentElement)
                {
                    descendant = ((ContentElement) obj2).PredictFocus(left);
                }
            }
            else if (this._contentPresenter != null)
            {
                descendant = this._contentPresenter.PredictFocus(left);
            }
            if ((descendant is IInputElement) && this._contentPresenter.IsAncestorOf(descendant))
            {
                bool flag = this._contentPresenter.IsDescendantInView(descendant);
                if (!flag && (e.KeyboardDevice.Modifiers == ModifierKeys.None))
                {
                    e.Handled = true;
                    switch (e.Key)
                    {
                        case Key.Left:
                            if (base.FlowDirection != FlowDirection.RightToLeft)
                            {
                                base.LineLeft();
                                break;
                            }
                            base.LineRight();
                            break;

                        case Key.Up:
                            base.LineUp();
                            break;

                        case Key.Right:
                            if (base.FlowDirection != FlowDirection.RightToLeft)
                            {
                                base.LineRight();
                                break;
                            }
                            base.LineLeft();
                            break;

                        case Key.Down:
                            base.LineDown();
                            break;
                    }
                    base.UpdateLayout();
                    flag = this._contentPresenter.IsDescendantInView(descendant);
                }
                if (flag)
                {
                    ((IInputElement) descendant).Focus();
                    e.Handled = true;
                }
            }
        }

        private void VerifyScrollHorizontalVisibility()
        {
            object obj2;
            object obj3;
            if ((base.ComputedHorizontalScrollBarVisibility == Visibility.Visible) && (base.ScrollInfo != null))
            {
                double num = base.ScrollInfo.ExtentWidth - base.ScrollInfo.ViewportWidth;
                bool flag = base.HorizontalScrollBarVisibility == ScrollBarVisibility.Auto;
                obj2 = (flag && (base.ScrollInfo.HorizontalOffset == 0.0))
                           ? Visibility.Collapsed
                           : Visibility.Visible;
                obj3 = (flag && (base.ScrollInfo.HorizontalOffset >= num))
                           ? Visibility.Collapsed
                           : Visibility.Visible;
            }
            else
            {
                obj2 = obj3 = Visibility.Collapsed;
            }
            base.SetValue(ScrollLeftVisibilityPropertyKey, obj2);
            base.SetValue(ScrollRightVisibilityPropertyKey, obj3);
        }

        private void VerifyScrollVerticalVisibility()
        {
            object obj2;
            object obj3;
            if ((base.ComputedVerticalScrollBarVisibility == Visibility.Visible) && (base.ScrollInfo != null))
            {
                double num = base.ScrollInfo.ExtentHeight - base.ScrollInfo.ViewportHeight;
                bool flag = base.VerticalScrollBarVisibility == ScrollBarVisibility.Auto;
                obj2 = (flag && (base.ScrollInfo.VerticalOffset == 0.0))
                           ? Visibility.Collapsed
                           : Visibility.Visible;
                obj3 = (flag && (base.ScrollInfo.VerticalOffset >= num))
                           ? Visibility.Collapsed
                           : Visibility.Visible;
            }
            else
            {
                obj2 = obj3 = Visibility.Collapsed;
            }
            base.SetValue(ScrollUpVisibilityPropertyKey, obj2);
            base.SetValue(ScrollDownVisibilityPropertyKey, obj3);
        }

        // Properties
        private bool DoesParentHandleScrolling
        {
            get
            {
                Control templatedParent = base.TemplatedParent as Control;
                if (templatedParent != null)
                {
                    try
                    {
                       
                        PropertyInfo property = typeof (Control).GetProperty("HandlesScrolling",
                                                                             BindingFlags.NonPublic |
                                                                             BindingFlags.Instance);
                        if ((null != property) && true.Equals(property.GetValue(templatedParent, null)))
                        {
                            return true;
                        }
                    }
                    catch (MethodAccessException)
                    {
                    }
                    catch (SecurityException)
                    {
                    }
                }
                return false;
            }
        }

        [Bindable(true)]
        public Style ScrollDownButtonStyle
        {
            get { return (Style) base.GetValue(ScrollDownButtonStyleProperty); }
            set { base.SetValue(ScrollDownButtonStyleProperty, value); }
        }

        [Bindable(true), ReadOnly(true)]
        public Visibility ScrollDownVisibility
        {
            get { return (Visibility) base.GetValue(ScrollDownVisibilityProperty); }
        }

        internal IScrollInfo ScrollInfoInternal
        {
            get { return base.ScrollInfo; }
            set { base.ScrollInfo = value; }
        }

        [Bindable(true)]
        public Style ScrollLeftButtonStyle
        {
            get { return (Style) base.GetValue(ScrollLeftButtonStyleProperty); }
            set { base.SetValue(ScrollLeftButtonStyleProperty, value); }
        }

        [Bindable(true), ReadOnly(true)]
        public Visibility ScrollLeftVisibility
        {
            get { return (Visibility) base.GetValue(ScrollLeftVisibilityProperty); }
        }

        [Bindable(true)]
        public Style ScrollRightButtonStyle
        {
            get { return (Style) base.GetValue(ScrollRightButtonStyleProperty); }
            set { base.SetValue(ScrollRightButtonStyleProperty, value); }
        }

        [Bindable(true), ReadOnly(true)]
        public Visibility ScrollRightVisibility
        {
            get { return (Visibility) base.GetValue(ScrollRightVisibilityProperty); }
        }

        [Bindable(true)]
        public Style ScrollUpButtonStyle
        {
            get { return (Style) base.GetValue(ScrollUpButtonStyleProperty); }
            set { base.SetValue(ScrollUpButtonStyleProperty, value); }
        }

        [ReadOnly(true), Bindable(true)]
        public Visibility ScrollUpVisibility
        {
            get { return (Visibility) base.GetValue(ScrollUpVisibilityProperty); }
        }
    }



}
