﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace MorganStanley.MSDotNet.MSGui.Controls.Controls
{
    /// <summary>
    /// Interaction logic for EnvironmentLabel.xaml
    /// </summary>
    public partial class EnvironmentLabel : UserControl
    {
        public EnvironmentLabel()
        {
            InitializeComponent();

        }

    }

    [ValueConversion(typeof(string), typeof(Visibility))]
    public class SubenvironmentVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return Visibility.Collapsed;
            return Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
