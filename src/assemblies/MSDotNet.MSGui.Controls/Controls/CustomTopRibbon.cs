﻿using System;
using System.Collections.Generic;
using System.Linq; 
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Forms;
using System.Windows.Input; 
using Infragistics.Windows.Ribbon;
using Infragistics.Windows.Ribbon.Events;
using MorganStanley.MSDotNet.MSGui.Controls.Interfaces;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MorganStanley.MSDotNet.MSGui.Controls.Controls
{
    /// <summary>
    /// How to test: minimize ribbon and left click the empty area of the ribbon tab to toggle the visibility of ribbon 
    /// </summary>
    public class CustomTopRibbon : XamRibbon
    { 
        private Popup _popup;
        private bool _handlingMouseClick;

        private readonly IList<IHwndHostControl> _isolatedControls;

        public CustomTopRibbon()
        {
            _isolatedControls = new List<IHwndHostControl>();
            RibbonTabItemSelected += CustomTopRibbon_RibbonTabItemSelected;
            RibbonTabItemCloseUp += CustomTopRibbon_RibbonTabItemCloseUp;
        }

        internal bool IsPopupVisible()
        {
            return _popup != null && _popup.Child.IsVisible;
        }

        internal UIElement RootElement
        {
            get { return _popup.Child; }
        }

        internal void AddHostControl(IHwndHostControl control)
        {
            _isolatedControls.Add(control);
        }

        private void CustomTopRibbon_RibbonTabItemCloseUp(object sender, RoutedEventArgs e)
        {
            if (_handlingMouseClick)
            {
                _popup.IsOpen = true;
            }
        }

        private void CustomTopRibbon_RibbonTabItemSelected(object sender, RibbonTabItemSelectedEventArgs e)
        { 

            if (_popup == null)
            {
                // Now, our popup is stored in private _popup field. 
                _popup = ReflHelper.FieldGet(ReflHelper.PropertyGet(this, "RibbonTabControl"), "_popup") as Popup;

                if (_popup != null)
                {
                    _popup.MouseLeftButtonUp += _popup_MouseLeftButtonUp;
                    _popup.MouseLeftButtonDown += _popup_MouseLeftButtonDown;
                    _popup.MouseMove += _popup_MouseMove;

                    if (_popup.AllowsTransparency)
                    {
                        _popup.AllowsTransparency = false;
                    }
                }
            }
        }

        void _popup_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            var mouseScreenPosition = Control.MousePosition;
            var point = new Point(mouseScreenPosition.X, mouseScreenPosition.Y);
            foreach (var host in _isolatedControls.Where(h => h.IsVisible && h.IsMouseOnControl(point)))
            {
                host.MouseMove(e);
            }
        }

        void _popup_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _handlingMouseClick = true;

            var mouseScreenPosition = Control.MousePosition;
            var point = new Point(mouseScreenPosition.X, mouseScreenPosition.Y);
            foreach (var host in _isolatedControls.Where(h => h.IsVisible && h.IsMouseOnControl(point)))
            {
                host.MouseLeftButtonDown(e);
            }

            _handlingMouseClick = false;

            // Queue actual closing of the ribbon on timer after 50 ms, so that a hosted button has a chance to 
            // handle its MouseUp event
            // TODO (hreckin): This isn't exactly right way to do it. 
            // We know that ribbon's Popup collapses on focus change to a window that isn't its child, and 
            // a window hosted in isolated process won't be the Popup child.
            var timer = new Timer
            {
                Interval = 100
            };

            var dispatcher = _popup.Dispatcher;
            timer.Tick += (o, ev) => dispatcher.Invoke(new Action(() =>
            {
                try
                {
                    _popup.IsOpen = false;
                }
                finally
                {
                    timer.Stop();
                    timer.Dispose();
                    timer = null;
                }
            }));

            timer.Start();
        }

        void _popup_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var mouseScreenPosition = Control.MousePosition;
            var point = new Point(mouseScreenPosition.X, mouseScreenPosition.Y);
            foreach (var host in _isolatedControls.Where(h => h.IsVisible && h.IsMouseOnControl(point)))
            {
                host.MouseLeftButtonUp(e);
            }

            _handlingMouseClick = false;
            _popup.IsOpen = false;
        }
    }
}
