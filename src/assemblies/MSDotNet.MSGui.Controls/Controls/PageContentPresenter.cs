﻿using System; 
using System.ComponentModel; 
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MorganStanley.MSDotNet.MSGui.Controls.Controls
{
    internal enum PagerScrollDirection
    {
        Up,
        Down,
        Left,
        Right
    }


    [DesignTimeVisible(false)]
    public class PagerContentPresenter : ContentPresenter, IScrollInfo
    {
        // Fields
        private bool _cachedReserveSpaceForScrollButtons = true;
        private ScrollData _scrollDataInfo;
        private IScrollInfo _scrollInfo;

        public static readonly DependencyProperty CanContentScrollProperty =
            ScrollViewer.CanContentScrollProperty.AddOwner(typeof (PagerContentPresenter),
                                                           new FrameworkPropertyMetadata(
                                                               new PropertyChangedCallback(
                                                                   PagerContentPresenter.OnCanContentScrollChanged)));

        public static readonly DependencyProperty ClipContentHorizontallyProperty =
            DependencyProperty.Register("ClipContentHorizontally", typeof (bool?), typeof (PagerContentPresenter),
                                        new FrameworkPropertyMetadata(null,
                                                                      FrameworkPropertyMetadataOptions.AffectsMeasure));

        public static readonly DependencyProperty ClipContentVerticallyProperty =
            DependencyProperty.Register("ClipContentVertically", typeof (bool?), typeof (PagerContentPresenter),
                                        new FrameworkPropertyMetadata(null,
                                                                      FrameworkPropertyMetadataOptions.AffectsMeasure));

        internal const double LineOffset = 16.0;

        public static readonly DependencyProperty ReserveSpaceForScrollButtonsProperty =
            DependencyProperty.Register("ReserveSpaceForScrollButtons", typeof (bool), typeof (PagerContentPresenter),
                                        new FrameworkPropertyMetadata(true,
                                                                      new PropertyChangedCallback(
                                                                          PagerContentPresenter
                                                                              .OnReserveSpaceForScrollButtonsChanged)));

        // Methods
        internal void AdjustRect(ref Rect renderRect, PagerScrollDirection direction)
        {
            if (this._cachedReserveSpaceForScrollButtons)
            {
                XamPager templatedParent = base.TemplatedParent as XamPager;
                if (templatedParent != null)
                {
                    FrameworkElement scrollElement = templatedParent.GetScrollElement(direction);
                    if ((scrollElement != null) && (scrollElement.Visibility == Visibility.Visible))
                    {
                        Rect rect2 =
                            Rect.Intersect(
                                scrollElement.TransformToVisual(this)
                                             .TransformBounds(new Rect(0.0, 0.0, scrollElement.ActualWidth,
                                                                       scrollElement.ActualHeight)), renderRect);
                        if (!rect2.IsEmpty)
                        {
                            switch (direction)
                            {
                                case PagerScrollDirection.Up:
                                    if (rect2.Height > 0.0)
                                    {
                                        renderRect.Height -= rect2.Height;
                                        renderRect.Y = rect2.Bottom;
                                    }
                                    return;

                                case PagerScrollDirection.Down:
                                    if (rect2.Height > 0.0)
                                    {
                                        renderRect.Height -= rect2.Height;
                                    }
                                    return;

                                case PagerScrollDirection.Left:
                                    if (rect2.Width > 0.0)
                                    {
                                        renderRect.Width -= rect2.Width;
                                        renderRect.X = rect2.Right;
                                    }
                                    return;

                                case PagerScrollDirection.Right:
                                    if (rect2.Width > 0.0)
                                    {
                                        renderRect.Width -= rect2.Width;
                                    }
                                    return;
                            }
                        }
                    }
                }
            }
        } 

        protected override Size ArrangeOverride(Size finalSize)
        {
            if (this.IsScrollClient)
            {
                this.ScrollDataInfo.VerifyScrollData(finalSize, this.ScrollDataInfo._extent);
            }
            UIElement element = (this.VisualChildrenCount > 0) ? (this.GetVisualChild(0) as UIElement) : null;
            if (element != null)
            {
                Rect finalRect = new Rect(element.DesiredSize);
                if (this.IsScrollClient)
                {
                    finalRect.Location = new Point(-this.ScrollDataInfo._offset.X, -this.ScrollDataInfo._offset.Y);
                }
                if (finalSize.Width > finalRect.Width)
                {
                    finalRect.Width = finalSize.Width;
                }
                if (finalSize.Height > finalRect.Height)
                {
                    finalRect.Height = finalSize.Height;
                }
                element.Arrange(finalRect);
            }
            return finalSize;
        }

        internal static void EnsureIsANumber(double number)
        {
            if (double.IsNaN(number))
            {
                throw new ArgumentOutOfRangeException("offset", number, "value cannot be NaN");
            }
        }

        protected override Geometry GetLayoutClip(Size layoutSlotSize)
        {
            IScrollInfo info = this._scrollInfo;
            bool flag = (info != null) && info.CanHorizontallyScroll;
            bool flag2 = (info != null) && info.CanVerticallyScroll;
            if (flag || flag2)
            {
                bool flag3;
                bool flag4;
                Rect renderRect = new Rect(base.RenderSize);
                bool? clipContentVertically = this.ClipContentVertically;
                if (clipContentVertically.HasValue)
                {
                    flag3 = clipContentVertically.Value;
                }
                else
                {
                    flag3 = (flag2 && (info.ViewportHeight < info.ExtentHeight)) &&
                            !CoreUtilities.AreClose(info.ViewportHeight, info.ExtentHeight);
                }
                if (!flag3)
                {
                    renderRect.Y -= 100.0;
                    renderRect.Height += 200.0;
                }
                else
                {
                    this.AdjustRect(ref renderRect, PagerScrollDirection.Up);
                    this.AdjustRect(ref renderRect, PagerScrollDirection.Down);
                }
                bool? clipContentHorizontally = this.ClipContentHorizontally;
                if (clipContentHorizontally.HasValue)
                {
                    flag4 = clipContentHorizontally.Value;
                }
                else
                {
                    flag4 = (flag && (info.ViewportWidth < info.ExtentWidth)) &&
                            !CoreUtilities.AreClose(info.ViewportWidth, info.ExtentWidth);
                }
                if (!flag4)
                {
                    renderRect.X -= 100.0;
                    renderRect.Width += 200.0;
                }
                else
                {
                    this.AdjustRect(ref renderRect, PagerScrollDirection.Left);
                    this.AdjustRect(ref renderRect, PagerScrollDirection.Right);
                }
                Geometry geometry = new RectangleGeometry(renderRect);
                geometry.Freeze();
                return geometry;
            }
            return null;
        }

        internal bool IsDescendantInView(DependencyObject d)
        {
            bool flag = false;
            UIElement element = d as UIElement;
            if ((element == null) && (d is ContentElement))
            {
                element = d.FindParent<UIElement>(null, null);
            }
            if ((element != null) && element.IsDescendantOf(this))
            {
                Rect rect = element.TransformToAncestor(this).TransformBounds(new Rect(element.RenderSize));
                Rect renderRect = new Rect(base.RenderSize);
                this.AdjustRect(ref renderRect, PagerScrollDirection.Left);
                this.AdjustRect(ref renderRect, PagerScrollDirection.Right);
                this.AdjustRect(ref renderRect, PagerScrollDirection.Up);
                this.AdjustRect(ref renderRect, PagerScrollDirection.Down);
                flag = !Rect.Intersect(rect, renderRect).Size.IsEmpty;
            }
            return flag;
        }

        public void LineDown()
        {
            if (this.IsScrollClient)
            {
                this.SetVerticalOffset(this.ScrollDataInfo._offset.Y + 16.0);
            }
        }

        public void LineLeft()
        {
            if (this.IsScrollClient)
            {
                this.SetHorizontalOffset(this.ScrollDataInfo._offset.X - 16.0);
            }
        }

        public void LineRight()
        {
            if (this.IsScrollClient)
            {
                this.SetHorizontalOffset(this.ScrollDataInfo._offset.X + 16.0);
            }
        }

        public void LineUp()
        {
            if (this.IsScrollClient)
            {
                this.SetVerticalOffset(this.ScrollDataInfo._offset.Y - 16.0);
            }
        }

        public Rect MakeVisible(Visual visual, Rect rectangle)
        {
            if ((rectangle.IsEmpty || (visual == null)) || !base.IsAncestorOf(visual))
            {
                return Rect.Empty;
            }
            GeneralTransform transform = visual.TransformToAncestor(this);
            if (transform == null)
            {
                return Rect.Empty;
            }
            Rect rect = transform.TransformBounds(rectangle);
            if (this.IsScrollClient)
            {
                Rect renderRect = new Rect(base.RenderSize);
                this.AdjustRect(ref renderRect, PagerScrollDirection.Left);
                this.AdjustRect(ref renderRect, PagerScrollDirection.Right);
                this.AdjustRect(ref renderRect, PagerScrollDirection.Up);
                this.AdjustRect(ref renderRect, PagerScrollDirection.Down);
                Rect rect3 = Rect.Intersect(rect, renderRect);
                if (rect3.Width != rect.Width)
                {
                    double num = 0.0;
                    if (rect.Right > renderRect.Right)
                    {
                        num = rect.Right - renderRect.Right;
                    }
                    if (((rect.Left - num) - renderRect.Left) < 0.0)
                    {
                        num += (rect.Left - num) - renderRect.Left;
                    }
                    rect.X -= num;
                    num += this.HorizontalOffset;
                    this.SetHorizontalOffset(num);
                }
                if (rect3.Height == rect.Height)
                {
                    return rect;
                }
                double offset = 0.0;
                if (rect.Bottom > renderRect.Bottom)
                {
                    offset = rect.Bottom - renderRect.Bottom;
                }
                if (((rect.Top - offset) - renderRect.Top) < 0.0)
                {
                    offset += (rect.Top - offset) - renderRect.Top;
                }
                rect.Y -= offset;
                offset += this.VerticalOffset;
                this.SetVerticalOffset(offset);
            }
            return rect;
        }

        protected override Size MeasureOverride(Size availableSize)
        {
            Size empty = Size.Empty;
            if (this.VisualChildrenCount > 0)
            {
                Size constraint = availableSize;
                if (this.IsScrollClient)
                {
                    if (this.ScrollDataInfo._canHorizontallyScroll)
                    {
                        constraint.Width = double.PositiveInfinity;
                    }
                    if (this.ScrollDataInfo._canVerticallyScroll)
                    {
                        constraint.Height = double.PositiveInfinity;
                    }
                }
                empty = base.MeasureOverride(constraint);
            }
            if (this.IsScrollClient)
            {
                this.ScrollDataInfo.VerifyScrollData(availableSize, empty);
            }
            if (availableSize.Width < empty.Width)
            {
                empty.Width = availableSize.Width;
            }
            if (availableSize.Height < empty.Height)
            {
                empty.Height = availableSize.Height;
            }
            return empty;
        }

        public void MouseWheelDown()
        {
            if (this.IsScrollClient)
            {
                this.SetVerticalOffset(this.ScrollDataInfo._offset.Y + (SystemParameters.WheelScrollLines*16.0));
            }
        }

        public void MouseWheelLeft()
        {
            if (this.IsScrollClient)
            {
                this.SetHorizontalOffset(this.ScrollDataInfo._offset.X - (SystemParameters.WheelScrollLines*16.0));
            }
        }

        public void MouseWheelRight()
        {
            if (this.IsScrollClient)
            {
                this.SetHorizontalOffset(this.ScrollDataInfo._offset.X + (SystemParameters.WheelScrollLines*16.0));
            }
        }

        public void MouseWheelUp()
        {
            if (this.IsScrollClient)
            {
                this.SetVerticalOffset(this.ScrollDataInfo._offset.Y - (SystemParameters.WheelScrollLines*16.0));
            }
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            this.VerifyScrollInfo();
        }

        private static void OnCanContentScrollChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            PagerContentPresenter presenter = d as PagerContentPresenter;
            if ((presenter != null) && (presenter._scrollDataInfo != null))
            {
                presenter.VerifyScrollInfo();
                presenter.InvalidateMeasure();
            }
        }

        private static void OnReserveSpaceForScrollButtonsChanged(DependencyObject target,
                                                                  DependencyPropertyChangedEventArgs e)
        {
            PagerContentPresenter presenter = target as PagerContentPresenter;
            if (presenter != null)
            {
                presenter._cachedReserveSpaceForScrollButtons = (bool) e.NewValue;
            }
        }

        public void PageDown()
        {
            if (this.IsScrollClient)
            {
                this.SetVerticalOffset(this.ScrollDataInfo._offset.Y + this.ScrollDataInfo._viewport.Height);
            }
        }

        public void PageLeft()
        {
            if (this.IsScrollClient)
            {
                this.SetVerticalOffset(this.ScrollDataInfo._offset.X - this.ScrollDataInfo._viewport.Width);
            }
        }

        public void PageRight()
        {
            if (this.IsScrollClient)
            {
                this.SetVerticalOffset(this.ScrollDataInfo._offset.X + this.ScrollDataInfo._viewport.Width);
            }
        }

        public void PageUp()
        {
            if (this.IsScrollClient)
            {
                this.SetVerticalOffset(this.ScrollDataInfo._offset.Y - this.ScrollDataInfo._viewport.Height);
            }
        }

        public void SetHorizontalOffset(double offset)
        {
            if (this.IsScrollClient)
            {
                EnsureIsANumber(offset);
                offset = Math.Max(offset, 0.0);
                if (!CoreUtilities.AreClose(offset, this.ScrollDataInfo._offset.X))
                {
                    this.ScrollDataInfo._offset.X = offset;
                    if (this.ScrollDataInfo._scrollOwner != null)
                    {
                        this.ScrollDataInfo._scrollOwner.InvalidateScrollInfo();
                    }
                    base.InvalidateArrange();
                }
            }
        }

        public void SetVerticalOffset(double offset)
        {
            if (this.IsScrollClient)
            {
                EnsureIsANumber(offset);
                offset = Math.Max(offset, 0.0);
                if (!CoreUtilities.AreClose(offset, this.ScrollDataInfo._offset.Y))
                {
                    this.ScrollDataInfo._offset.Y = offset;
                    if (this.ScrollDataInfo._scrollOwner != null)
                    {
                        this.ScrollDataInfo._scrollOwner.InvalidateScrollInfo();
                    }
                    base.InvalidateArrange();
                }
            }
        }

        private void VerifyScrollInfo()
        {
            XamPager templatedParent = base.TemplatedParent as XamPager;
            if (templatedParent != null)
            {
                IScrollInfo content = null;
                if (this.CanContentScroll)
                {
                    content = base.Content as IScrollInfo;
                    if (content == null)
                    {
                        ItemsPresenter reference = base.Content as ItemsPresenter;
                        if (reference != null)
                        {
                            reference.ApplyTemplate();
                            if (VisualTreeHelper.GetChildrenCount(reference) > 0)
                            {
                                content = VisualTreeHelper.GetChild(reference, 0) as IScrollInfo;
                            }
                        }
                    }
                }
                if (content == null)
                {
                    content = this;
                }
                if (content != this._scrollInfo)
                {
                    if (this.IsScrollClient)
                    {
                        this._scrollInfo = null;
                    }
                    else if ((this._scrollInfo != null) && (this._scrollInfo.ScrollOwner == templatedParent))
                    {
                        this._scrollInfo.ScrollOwner = null;
                    }
                }
                if (content != null)
                {
                    this._scrollInfo = content;
                    content.ScrollOwner = templatedParent;
                    templatedParent.ScrollInfoInternal = content;
                }
            }
            else if (this._scrollInfo != null)
            {
                templatedParent = this._scrollInfo.ScrollOwner as XamPager;
                if ((templatedParent != null) && (templatedParent.ScrollInfoInternal == this._scrollInfo))
                {
                    templatedParent.ScrollInfoInternal = null;
                }
                this._scrollInfo.ScrollOwner = null;
                this._scrollInfo = null;
                this._scrollDataInfo = null;
            }
        }

        // Properties
        public bool CanContentScroll
        {
            get { return (bool) base.GetValue(CanContentScrollProperty); }
            set { base.SetValue(CanContentScrollProperty, value); }
        }

        public bool CanHorizontallyScroll
        {
            get { return (this.IsScrollClient && this.ScrollDataInfo._canHorizontallyScroll); }
            set
            {
                if (this.IsScrollClient && (this.ScrollDataInfo._canHorizontallyScroll != value))
                {
                    this.ScrollDataInfo._canHorizontallyScroll = value;
                    base.InvalidateMeasure();
                }
            }
        }

        public bool CanVerticallyScroll
        {
            get { return (this.IsScrollClient && this.ScrollDataInfo._canVerticallyScroll); }
            set
            {
                if (this.IsScrollClient && (this.ScrollDataInfo._canVerticallyScroll != value))
                {
                    this.ScrollDataInfo._canVerticallyScroll = value;
                    base.InvalidateMeasure();
                }
            }
        }

        public bool? ClipContentHorizontally
        {
            get { return (bool?) base.GetValue(ClipContentHorizontallyProperty); }
            set { base.SetValue(ClipContentHorizontallyProperty, value); }
        }

        public bool? ClipContentVertically
        {
            get { return (bool?) base.GetValue(ClipContentVerticallyProperty); }
            set { base.SetValue(ClipContentVerticallyProperty, value); }
        }

        public double ExtentHeight
        {
            get
            {
                if (!this.IsScrollClient)
                {
                    return 0.0;
                }
                return this.ScrollDataInfo._extent.Height;
            }
        }

        public double ExtentWidth
        {
            get
            {
                if (!this.IsScrollClient)
                {
                    return 0.0;
                }
                return this.ScrollDataInfo._extent.Width;
            }
        }

        public double HorizontalOffset
        {
            get
            {
                if (!this.IsScrollClient)
                {
                    return 0.0;
                }
                return this.ScrollDataInfo._offset.X;
            }
        }

        private bool IsScrollClient
        {
            get { return (this._scrollInfo == this); }
        }

        public bool ReserveSpaceForScrollButtons
        {
            get { return this._cachedReserveSpaceForScrollButtons; }
            set { base.SetValue(ReserveSpaceForScrollButtonsProperty, value); }
        }

        private ScrollData ScrollDataInfo
        {
            get
            {
                if (this._scrollDataInfo == null)
                {
                    this._scrollDataInfo = new ScrollData();
                }
                return this._scrollDataInfo;
            }
        }

        public ScrollViewer ScrollOwner
        {
            get
            {
                if (this.IsScrollClient)
                {
                    return this.ScrollDataInfo._scrollOwner;
                }
                return null;
            }
            set
            {
                if (this.IsScrollClient)
                {
                    this.ScrollDataInfo._scrollOwner = value;
                }
            }
        }

        public double VerticalOffset
        {
            get
            {
                if (!this.IsScrollClient)
                {
                    return 0.0;
                }
                return this.ScrollDataInfo._offset.Y;
            }
        }

        public double ViewportHeight
        {
            get
            {
                if (!this.IsScrollClient)
                {
                    return 0.0;
                }
                return this.ScrollDataInfo._viewport.Height;
            }
        }

        public double ViewportWidth
        {
            get
            {
                if (!this.IsScrollClient)
                {
                    return 0.0;
                }
                return this.ScrollDataInfo._viewport.Width;
            }
        }

        // Nested Types
        internal class ScrollData
        {
            // Fields
            internal bool _canHorizontallyScroll;
            internal bool _canVerticallyScroll;
            internal Size _extent = new Size();
            internal Vector _offset = new Vector();
            internal ScrollViewer _scrollOwner;
            internal Size _viewport = new Size();

            // Methods
            internal void Reset()
            {
                this._offset = new Vector();
                this._extent = new Size();
                this._viewport = new Size();
            }

            private bool VerifyOffset()
            {
                double x = Math.Max(Math.Min(this._offset.X, this._extent.Width - this._viewport.Width), 0.0);
                double y = Math.Max(Math.Min(this._offset.Y, this._extent.Height - this._viewport.Height), 0.0);
                Vector vector = this._offset;
                this._offset = new Vector(x, y);
                if (CoreUtilities.AreClose(this._offset.X, vector.X))
                {
                    return !CoreUtilities.AreClose(this._offset.Y, vector.Y);
                }
                return true;
            }

            internal void VerifyScrollData(Size viewPort, Size extent)
            {
                if (double.IsInfinity(viewPort.Width))
                {
                    viewPort.Width = extent.Width;
                }
                if (double.IsInfinity(viewPort.Height))
                {
                    viewPort.Height = extent.Height;
                }
                bool flag = ((!CoreUtilities.AreClose(this._viewport.Width, viewPort.Width) ||
                              !CoreUtilities.AreClose(this._viewport.Height, viewPort.Height)) ||
                             !CoreUtilities.AreClose(this._extent.Width, extent.Width)) ||
                            !CoreUtilities.AreClose(this._extent.Width, extent.Width);
                this._viewport = viewPort;
                this._extent = extent;
                flag |= this.VerifyOffset();
                if ((this._scrollOwner != null) && flag)
                {
                    this._scrollOwner.InvalidateScrollInfo();
                }
            }
        }
    }


}
