﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;

namespace MorganStanley.MSDotNet.MSGui.Controls
{ 
    public class PopupList:ListView
    {  
        public static readonly DependencyProperty IsHighlightedProperty =
           DependencyProperty.RegisterAttached("IsHighlighted", typeof(bool), typeof(PopupList), new PropertyMetadata(default(bool)));

        public static void SetIsHighlighted(UIElement element_, bool value_)
        {
            element_.SetValue(IsHighlightedProperty, value_);
        }

        public static bool GetIsHighlighted(UIElement element_)
        {
            return (bool)element_.GetValue(IsHighlightedProperty);
        } 

        public void ClearHighlight()
        {
            HighlightItem(-1);
        }

        public bool HandleKey(Key key_)
        {
            if (key_ == Key.Down)
            {
                if (highlightedIndex < this.Items.Count - 1)
                {
                    HighlightItem(++highlightedIndex);
                }
                else
                {
                    int index = highlightedIndex;
                    HighlightItem(-1);
                    HighlightItem(index < 0 ? 0 : index);
                }
                return true;
            }
            else if (key_ == Key.Up)
            {
                if (highlightedIndex > 0)
                {
                    HighlightItem(--highlightedIndex);
                }
                else
                {
                    HighlightItem(-1);
                    HighlightItem(0);
                }
                return true;
            }
            else if (key_ == Key.Enter)
            {
                if (highlightedIndex < 0) highlightedIndex = 0;
                HighlightItem(highlightedIndex);
                this.SelectedIndex = highlightedIndex;
            }
            return false;
        }




        public Popup Container
        {
            get { return (Popup)GetValue(ContainerProperty); }
            set { SetValue(ContainerProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Container.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ContainerProperty =
            DependencyProperty.Register("Container", typeof(Popup), typeof(PopupList), new UIPropertyMetadata(null, (o_,
                                                                                                                     args_)
                                                                                                                    =>
                {
                    PopupList list = o_ as PopupList;
                    if (list.Container == null) return;
                    list.Container.MouseEnter += (s,e) => list.HighlightItem(-1);
                }));
         
  
        private int highlightedIndex = -1;
        private bool internalMouseMove = false;
        private void HighlightItem(int index_)
        {
            highlightedIndex = index_;
            for (int i = 0; i < this.Items.Count; i++)
            {
                var element = this.ItemContainerGenerator.ContainerFromIndex(i) as UIElement;
                bool highlight = i == index_;
                if (highlight)
                {
                    internalMouseMove = true;
                    this.ScrollIntoView(this.Items[i]);
                    internalMouseMove = false;
                }
                SetIsHighlighted(element, highlight);
            }
        }

 
        protected override void OnPreviewMouseLeftButtonDown(System.Windows.Input.MouseButtonEventArgs e)
        {

            base.OnPreviewMouseLeftButtonDown(e);
            var item = ItemsControl.ContainerFromElement(this, e.OriginalSource as DependencyObject) as ListViewItem;
            if (item != null)
            {
                item.IsSelected = true; 
                Container.IsOpen = false;
            }
        }

        protected override void OnPreviewMouseMove(System.Windows.Input.MouseEventArgs e)
        {
            base.OnPreviewMouseMove(e);
            if (internalMouseMove || Keyboard.IsKeyDown(Key.Down) || Keyboard.IsKeyDown(Key.Up)) return;
            var item = ItemsControl.ContainerFromElement(this, e.OriginalSource as DependencyObject) as ListViewItem;
            if (item != null)
            {
                int index = this.ItemContainerGenerator.IndexFromContainer(item);
                if (index >= 0)
                {
                    HighlightItem(index);
                }
            }
        }
    }
}
