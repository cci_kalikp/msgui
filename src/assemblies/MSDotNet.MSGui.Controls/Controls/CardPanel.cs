﻿ 
using System.ComponentModel; 
using System.Windows;
using System.Windows.Controls;

//copied from infragistics
namespace MorganStanley.MSDotNet.MSGui.Controls.Controls
{
    [DesignTimeVisible(false)]
    public class CardPanel : Panel
    {
        // Methods
        protected override Size ArrangeOverride(Size finalSize)
        {
            UIElementCollection children = base.Children;
            Rect finalRect = new Rect(finalSize);
            int num = 0;
            int count = children.Count;
            while (num < count)
            {
                children[num].Arrange(finalRect);
                num++;
            }
            return finalSize;
        }

        protected override Size MeasureOverride(Size availableSize)
        {
            double width = 0.0;
            double height = 0.0;
            UIElementCollection children = base.Children;
            int num3 = 0;
            int count = children.Count;
            while (num3 < count)
            {
                UIElement element = children[num3];
                element.Measure(availableSize);
                Size desiredSize = element.DesiredSize;
                if (desiredSize.Width > width)
                {
                    width = desiredSize.Width;
                }
                if (desiredSize.Height > height)
                {
                    height = desiredSize.Height;
                }
                num3++;
            }
            return new Size(width, height);
        }
    } 
}
