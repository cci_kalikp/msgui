﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MorganStanley.MSDotNet.MSGui.Controls.Controls
{
    /// <summary>
    /// Interaction logic for ModernEnvironmentLabel.xaml
    /// </summary>
    public partial class ModernEnvironmentLabel : UserControl
    {
        public ModernEnvironmentLabel()
        {
            InitializeComponent();
        }
    }
}
