﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MorganStanley.MSDotNet.MSGui.Controls.Controls
{
    public sealed class EnvironmentLabelViewModel : INotifyPropertyChanged
    {
        private readonly IHidingLockingManager _manager;

        public EnvironmentLabelViewModel(IHidingLockingManager manager_)
        {
            _manager = manager_;
        }

        private IMSDesktopEnvironment _environment;
        private bool _labelsEnabled;
        private bool _modernLabelsEnabled;

        public IMSDesktopEnvironment Environment
        {
            get
            {
                return _environment;
            }
            set
            {
                _environment = value;
                OnPropertyChanged("Environment");
            }
        }

        public bool LabelsEnabled
        {
            get
            {
                if (_manager.Granularity.HasFlag(HideLockUIGranularity.HideEnvironmentLabelling)) return false;
                return _labelsEnabled;
            }
            set
            {
                _labelsEnabled = value;
                OnPropertyChanged("LabelsEnabled");
            }
        }

        public bool ModernLabelsEnabled
        {
            get
            {
                if (_manager.Granularity.HasFlag(HideLockUIGranularity.HideEnvironmentLabelling)) return false;
                return _modernLabelsEnabled;
            }
            set
            {
                _modernLabelsEnabled = value;
                OnPropertyChanged("ModernLabelsEnabled");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName_)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName_));
        }
    }
}
