﻿using System.Windows;
using System.Windows.Controls;

namespace MorganStanley.MSDotNet.MSGui.Controls.Controls
{
    /// <summary>
    /// Interaction logic for ExtendedTreeView.xaml
    /// </summary>
    public partial class ExtendedTreeView
    {
        public ExtendedTreeView()
        {
            InitializeComponent(); 
        }
         
        public object BindableSelectedItem
        {
            get { return (object)GetValue(BindableSelectedItemProperty); }
            set { SetValue(BindableSelectedItemProperty, value); }
        }

        public static readonly DependencyProperty BindableSelectedItemProperty =
            DependencyProperty.Register("BindableSelectedItem", typeof(object), typeof(ExtendedTreeView), new UIPropertyMetadata(null));


        private void ExtendedTreeView_OnSelectedItemChanged(object sender_, RoutedPropertyChangedEventArgs<object> e_)
        {
            BindableSelectedItem = SelectedItem;
        }
         
    }
}
