﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MorganStanley.MSDotNet.MSGui.Controls.Controls
{
    /// <summary>
    /// Interaction logic for MSGuiColorPalette.xaml
    /// </summary>
    public partial class MSGuiColorPalette : UserControl
    {
        private const int CellWidth = 30;
        private const int CellCountInRow = 7;
        public MSGuiColorPalette()
        {
            InitializeComponent();
            CreatePalette();
        }

        private void CreatePalette()
        {
            Dictionary<string, Dictionary<string, Brush>> brushes = MSGuiColors.Brushes;
            if (brushes.Count == 0) return;
            for (int i = 0; i < CellCountInRow; i++)
            {
                Root.ColumnDefinitions.Add(new ColumnDefinition(){Width = new GridLength(1, GridUnitType.Star)});
            }
            int height = 0;
            foreach (var brushSet in brushes)
            {
                int existingRowCount = Root.RowDefinitions.Count;
                height += 30;
                Root.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(30, GridUnitType.Pixel) });
                Label groupText = new Label();
                groupText.Content = brushSet.Key;
                Grid.SetRow(groupText, existingRowCount);
                Grid.SetColumnSpan(groupText, CellCountInRow);
                Root.Children.Add(groupText); 

                for (int row = 0; row < Convert.ToInt32(Math.Ceiling(brushSet.Value.Count / (double)CellCountInRow)); row++)
                {
                    height += CellWidth;
                    Root.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) }); 
                }

                int brushIndex = 0;
                foreach (var brush in brushSet.Value)
                {
                    Rectangle rect = new Rectangle();
                    
                    rect.Fill = brush.Value;
                    rect.Margin = new Thickness(3);
                    string colorText = brush.Value.ToDisplayString(); 
                    string tooltip = brush.Key;
                    if (colorText != null)
                    {
                        tooltip += Environment.NewLine + colorText;
                    }
                    rect.ToolTip = tooltip;
                    string key = brush.Key;
                    rect.MouseUp += (s, e) => Clipboard.SetDataObject(key);
                    Grid.SetRow(rect, brushIndex / CellCountInRow + existingRowCount + 1);
                    Grid.SetColumn(rect, brushIndex % CellCountInRow);
                    Root.Children.Add(rect);
                    brushIndex++;
                }  
            }
            InitialSize = new Size(CellWidth * CellCountInRow, height);
        }

        public Size InitialSize { get; private set; }
    }
}
