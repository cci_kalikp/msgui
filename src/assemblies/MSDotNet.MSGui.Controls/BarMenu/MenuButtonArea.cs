﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MorganStanley.MSDotNet.MSGui.Controls.BarMenu
{
    [DesignTimeVisible(false)]
    public class MenuButtonArea : Control
    {
        // Fields
        private Size _lastArrangeSize;

        private static readonly DependencyProperty HasImageInternalProperty =
            DependencyProperty.Register("HasImageInternal", typeof (bool), typeof (MenuButtonArea),
                                        new FrameworkPropertyMetadata(false,
                                                                      new PropertyChangedCallback(
                                                                          MenuButtonArea.OnHasImageInternalChanged)));

        private static readonly DependencyPropertyKey HasImagePropertyKey =
            DependencyProperty.RegisterReadOnly("HasImage", typeof (bool), typeof (MenuButtonArea),
                                                new FrameworkPropertyMetadata(false));


        public static readonly DependencyProperty HasImageProperty = HasImagePropertyKey.DependencyProperty;
         

        private static readonly DependencyPropertyKey MenuToolPresenterPropertyKey =
            DependencyProperty.RegisterReadOnly("MenuToolPresenter", typeof (MenuToolPresenter), typeof (MenuButtonArea),
                                                new FrameworkPropertyMetadata(null));

        public static readonly DependencyProperty MenuToolPresenterProperty =
            MenuToolPresenterPropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey MenuToolPropertyKey =
            DependencyProperty.RegisterReadOnly("MenuTool", typeof (BarMenuTool), typeof (MenuButtonArea),
                                                new FrameworkPropertyMetadata(null));

        public static readonly DependencyProperty MenuToolProperty = MenuToolPropertyKey.DependencyProperty;
         

        // Methods
        static MenuButtonArea()
        {
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof (MenuButtonArea),
                                                                      new FrameworkPropertyMetadata(
                                                                          typeof (MenuButtonArea)));
            UIElement.FocusableProperty.OverrideMetadata(typeof (MenuButtonArea),
                                                         new FrameworkPropertyMetadata(false));
        }

        protected override Size ArrangeOverride(Size arrangeBounds)
        {
            this._lastArrangeSize = arrangeBounds;
            return base.ArrangeOverride(arrangeBounds);
        }

        private static void OnHasImageInternalChanged(DependencyObject target, DependencyPropertyChangedEventArgs e)
        {
            MenuButtonArea area = target as MenuButtonArea;
            if (area != null)
            {
                area.SetValue(HasImagePropertyKey, e.NewValue);
            }
        }

         
        protected override void OnVisualParentChanged(DependencyObject oldParent)
        {
            base.OnVisualParentChanged(oldParent);
            BarMenuTool tool = DependencyObjectHelper.FindParent<BarMenuTool>(this, null, null);
            if (tool != null)
            {
                base.SetValue(MenuToolPropertyKey, tool); 
                base.SetBinding(HasImageInternalProperty,
                                DependencyObjectHelper.CreateBindingObject(BarMenuTool.HasImageProperty, BindingMode.OneWay,
                                                              tool));
                
            }
            MenuToolPresenter presenter =
                DependencyObjectHelper.FindParent<MenuToolPresenter>(this, null, null);
            if (presenter != null)
            {
                base.SetValue(MenuToolPresenterPropertyKey, presenter); ;
            }
        }

        // Properties
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Bindable(true), ReadOnly(true)]
        public bool HasImage
        {
            get { return (bool) base.GetValue(HasImageProperty); }
        }

         
 

        internal Size LastArrangeSize
        {
            get { return this._lastArrangeSize; }
        }

    
        [EditorBrowsable(EditorBrowsableState.Never), Browsable(false)]
        public BarMenuTool MenuTool
        {
            get { return (BarMenuTool)base.GetValue(MenuToolProperty); }
        }

        [Bindable(true), ReadOnly(true)]
        public MenuToolPresenter MenuToolPresenter
        {
            get { return (MenuToolPresenter) base.GetValue(MenuToolPresenterProperty); }
        }

         
    }


}
