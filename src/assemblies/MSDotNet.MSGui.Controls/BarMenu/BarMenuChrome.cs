﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media; 
using MorganStanley.MSDotNet.MSGui.Core;

namespace MorganStanley.MSDotNet.MSGui.Controls.BarMenu
{
    [DesignTimeVisible(false)]
    public class BarMenuChrome : Decorator
    {
        // Fields 
        private Rect _lastArrangeRect;

        private ChromeLayer[] _layers = new ChromeLayer[]
            {
                new ChromeLayer(OuterFillBrushProperty, OuterPenBrushProperty),
                new ChromeLayer(MiddleFillBrushProperty, MiddlePenBrushProperty),
                new ChromeLayer(InnerFillBrushProperty, InnerPenBrushProperty),
                new ChromeLayer(OverlayFillBrushProperty, null), new ChromeLayer(GlowFillBrushProperty, null)
            };
         
        private MenuButtonArea _menuButtonArea; 
        private FrameworkElement _tool;


         

        
        private static readonly DependencyProperty GlowFillBrushProperty = DependencyProperty.Register("GlowFillBrush",
                                                                                                       typeof (Brush),
                                                                                                       typeof (
                                                                                                           BarMenuChrome
                                                                                                           ),
                                                                                                       new FrameworkPropertyMetadata
                                                                                                           (null,
                                                                                                            FrameworkPropertyMetadataOptions
                                                                                                                .AffectsRender));

        private const int GlowLayer = 4;

        private static readonly DependencyProperty InnerFillBrushProperty = DependencyProperty.Register(
            "InnerFillBrush", typeof (Brush), typeof (BarMenuChrome),
            new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsRender));

        private const int InnerLayer = 2;

        private static readonly DependencyProperty InnerPenBrushProperty = DependencyProperty.Register("InnerPenBrush",
                                                                                                       typeof (Brush),
                                                                                                       typeof (
                                                                                                           BarMenuChrome
                                                                                                           ),
                                                                                                       new FrameworkPropertyMetadata
                                                                                                           (null,
                                                                                                            FrameworkPropertyMetadataOptions
                                                                                                                .AffectsRender));
 

        public static readonly DependencyProperty IsCheckedProperty = DependencyProperty.Register("IsChecked",
                                                                                                  typeof (bool?),
                                                                                                  typeof (
                                                                                                      BarMenuChrome
                                                                                                      ),
                                                                                                  new FrameworkPropertyMetadata
                                                                                                      (false,
                                                                                                       new PropertyChangedCallback
                                                                                                           (BarMenuChrome
                                                                                                                .OnIsCheckedChanged)));

        
        private static readonly DependencyProperty IsMenuCheckedProperty = DependencyProperty.Register("IsMenuChecked",
                                                                                                       typeof (bool),
                                                                                                       typeof (
                                                                                                           BarMenuChrome
                                                                                                           ),
                                                                                                       new FrameworkPropertyMetadata
                                                                                                           (false,
                                                                                                            FrameworkPropertyMetadataOptions
                                                                                                                .AffectsRender));

        private static readonly DependencyProperty IsMenuOpenProperty = DependencyProperty.Register("IsMenuOpen",
                                                                                                    typeof (bool),
                                                                                                    typeof (
                                                                                                        BarMenuChrome
                                                                                                        ),
                                                                                                    new FrameworkPropertyMetadata
                                                                                                        (false,
                                                                                                         FrameworkPropertyMetadataOptions
                                                                                                             .AffectsRender));

        private static readonly DependencyProperty IsMouseOverMenuButtonAreaProperty =
            DependencyProperty.Register("IsMouseOverMenuButtonArea", typeof (bool), typeof (BarMenuChrome),
                                        new FrameworkPropertyMetadata(false,
                                                                      FrameworkPropertyMetadataOptions.AffectsRender,
                                                                      new PropertyChangedCallback(
                                                                          BarMenuChrome
                                                                              .OnIsMouseOverMenuButtonAreaChanged)));

        public static readonly DependencyProperty IsPressedProperty = DependencyProperty.Register("IsPressed",
                                                                                                  typeof (bool),
                                                                                                  typeof (
                                                                                                      BarMenuChrome
                                                                                                      ),
                                                                                                  new FrameworkPropertyMetadata
                                                                                                      (false,
                                                                                                       new PropertyChangedCallback
                                                                                                           (BarMenuChrome
                                                                                                                .OnIsPressedChanged)));

        
        private static readonly DependencyProperty MiddleFillBrushProperty =
            DependencyProperty.Register("MiddleFillBrush", typeof (Brush), typeof (BarMenuChrome),
                                        new FrameworkPropertyMetadata(null,
                                                                      FrameworkPropertyMetadataOptions.AffectsRender));

        private const int MiddleLayer = 1;

        private static readonly DependencyProperty MiddlePenBrushProperty = DependencyProperty.Register(
            "MiddlePenBrush", typeof (Brush), typeof (BarMenuChrome),
            new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsRender));

        private static readonly Thickness OnePixelThickness = new Thickness(1.0);

        private static readonly DependencyProperty OuterFillBrushProperty = DependencyProperty.Register(
            "OuterFillBrush", typeof (Brush), typeof (BarMenuChrome),
            new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsRender));

        private const int OuterLayer = 0;

        private static readonly DependencyProperty OuterPenBrushProperty = DependencyProperty.Register("OuterPenBrush",
                                                                                                       typeof (Brush),
                                                                                                       typeof (
                                                                                                           BarMenuChrome
                                                                                                           ),
                                                                                                       new FrameworkPropertyMetadata
                                                                                                           (null,
                                                                                                            FrameworkPropertyMetadataOptions
                                                                                                                .AffectsRender));

        private static readonly DependencyProperty OverlayFillBrushProperty =
            DependencyProperty.Register("OverlayFillBrush", typeof (Brush), typeof (BarMenuChrome),
                                        new FrameworkPropertyMetadata(null,
                                                                      FrameworkPropertyMetadataOptions.AffectsRender));

        private const int OverlayLayer = 3;

        public static readonly DependencyProperty PaddingProperty = DependencyProperty.Register("Padding",
                                                                                                typeof (Thickness),
                                                                                                typeof (
                                                                                                    BarMenuChrome),
                                                                                                new FrameworkPropertyMetadata
                                                                                                    (null));

        private static readonly DependencyProperty SeparatorLightPenBrushProperty =
            DependencyProperty.Register("SeparatorLightPenBrush", typeof (Brush), typeof (BarMenuChrome),
                                        new FrameworkPropertyMetadata(null,
                                                                      FrameworkPropertyMetadataOptions.AffectsRender));

        private static readonly DependencyProperty SeparatorPenBrushProperty =
            DependencyProperty.Register("SeparatorPenBrush", typeof (Brush), typeof (BarMenuChrome),
                                        new FrameworkPropertyMetadata(null,
                                                                      FrameworkPropertyMetadataOptions.AffectsRender));
 
        private static readonly Thickness TwoPixelThickness = new Thickness(2.0);

        public static readonly DependencyProperty UseRoundedCornersProperty =
            DependencyProperty.Register("UseRoundedCorners", typeof (bool), typeof (BarMenuChrome),
                                        new FrameworkPropertyMetadata(true,
                                                                      new PropertyChangedCallback(
                                                                          BarMenuChrome.OnUseRoundedCornersChanged)));

        private static readonly Thickness ZeroPixelThickness = new Thickness(0.0); 
        // Methods
        public BarMenuChrome()
        {
            base.Loaded += new RoutedEventHandler(this.OnLoaded);
        }

        private static Size Add(Size size, Thickness thickness)
        {
            Size size2 = size;
            size2.Width += thickness.Left + thickness.Right;
            size2.Height += thickness.Top + thickness.Bottom;
            return size2;
        }

        protected override Size ArrangeOverride(Size arrangeSize)
        {
            UIElement child = this.Child;
            if (child != null)
            {
                Rect rect = new Rect(arrangeSize);
                rect = Subtract(Subtract(rect, this.CalculateBorderThickness()), this.Padding);
                child.Arrange(rect);
            }
            this.InitializeLayers(arrangeSize);
            return arrangeSize;
        }

        private Thickness CalculateBorderThickness()
        { 
            return IsInRibbon ? new Thickness(2, 0, 2, 2) : new Thickness(2);
        }

        private Rect GetMenuButtonAreaRect(Size chromeRenderSize)
        {
            Rect rect = new Rect(chromeRenderSize); 
            rect = Subtract(rect, this.Padding); 
            return rect;
        }

        protected override HitTestResult HitTestCore(PointHitTestParameters hitTestParameters)
        {
            Rect rect = new Rect(new Point(), base.RenderSize);
            if (rect.Contains(hitTestParameters.HitPoint))
            {
                return new PointHitTestResult(this, hitTestParameters.HitPoint);
            }
            return base.HitTestCore(hitTestParameters);
        }

        
        private void InitializeLayers()
        {
            this.InitializeLayers(base.RenderSize);
        }

        private void InitializeLayers(Size arrangeSize)
        {
            Rect menuButtonAreaRect = this.GetMenuButtonAreaRect(arrangeSize);
            this._lastArrangeRect = menuButtonAreaRect; 
            for (int i = 0; i < this._layers.Length; i++)
            {
                this._layers[i].Clear();
            } 

            this._layers[4].Initialize(Rect.Inflate(menuButtonAreaRect, -0.5, -0.5), ZeroPixelThickness, 1.0 );

            this._layers[0].Initialize(menuButtonAreaRect, ZeroPixelThickness, 2.0);
            this._layers[1].Initialize(menuButtonAreaRect, new Thickness(1.0, 1.0, 1.0, 0.0), 1.0,
                                       RoundedRectCorners.Top);
            this._layers[2].Initialize(menuButtonAreaRect, OnePixelThickness, 1.0); 
            Rect rect = new Rect(arrangeSize);
            rect = Subtract(rect, this.Padding);
            if (buttonSize == ButtonSize.Large)
            {
                this._layers[3].Initialize(rect, new Thickness(2.0, 0.0, 2.0, 2.0), 0.0);
                return;
            }
            this._layers[3].Initialize(rect, new Thickness(0.0, 2.0, 2.0, 2.0), 0.0); 
        }


        

        protected override Size MeasureOverride(Size constraint)
        {
            Size size = Add(Add(new Size(), this.Padding), this.CalculateBorderThickness());
            Size desiredSize = new Size();
            UIElement child = this.Child;
            if (child != null)
            {
                Size availableSize = constraint;
                if (!double.IsPositiveInfinity(constraint.Width))
                {
                    availableSize.Width = Math.Max((double) (constraint.Width - size.Width), (double) 0.0);
                }
                if (!double.IsPositiveInfinity(constraint.Height))
                {
                    availableSize.Height = Math.Max((double) (constraint.Height - size.Height), (double) 0.0);
                }
                child.Measure(availableSize);
                desiredSize = child.DesiredSize;
            }
            desiredSize.Width += size.Width;
            desiredSize.Height += size.Height;
            desiredSize.Width = Math.Ceiling(desiredSize.Width);
            return desiredSize;
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);
            this.VerifyBrushes();
        }




        public bool IsInRibbon
        {
            get { return (bool)GetValue(IsInRibbonProperty); }
            set { SetValue(IsInRibbonProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsInRibbon.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsInRibbonProperty =
            DependencyProperty.Register("IsInRibbon", typeof(bool), typeof(BarMenuChrome), new UIPropertyMetadata(false));

        
        private ButtonSize buttonSize;

        // Using a DependencyProperty as the backing store for ButtonSize.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ButtonSizeProperty =
            DependencyProperty.Register("ButtonSize", typeof(ButtonSize), typeof(BarMenuChrome), new FrameworkPropertyMetadata(ButtonSize.Small, FrameworkPropertyMetadataOptions.AffectsRender, OnButtonSizeChanged));

        private static void OnButtonSizeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            BarMenuChrome chrome = (BarMenuChrome)d;
            chrome.buttonSize = (ButtonSize)e.NewValue;
            if (chrome.IsArrangeValid && chrome.IsLoaded)
            {
                chrome.InitializeLayers();
            }
            chrome.VerifyBrushes();
        }
         

        private static void OnIsCheckedChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            BarMenuChrome chrome = d as BarMenuChrome;
            if (chrome != null)
            {
                if (chrome._menuButtonArea != null)
                {
                    chrome._menuButtonArea.InvalidateVisual();
                }
                chrome.VerifyBrushes();
            }
        }

          

        private static void OnIsMouseOverMenuButtonAreaChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            BarMenuChrome chrome = (BarMenuChrome) d; 
            chrome.VerifyBrushes();
        }

        private static void OnIsPressedChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((BarMenuChrome) d).VerifyBrushes();
        }

 

        

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            base.Loaded -= new RoutedEventHandler(this.OnLoaded);
            base.Unloaded += new RoutedEventHandler(this.OnUnloaded); 
        }
 

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            if (e.Property == UIElement.IsMouseOverProperty)
            {
                this.VerifyBrushes();
            }
            base.OnPropertyChanged(e);
        }

        protected override void OnRender(DrawingContext drawingContext)
        { 
            for (int i = 0; i < this._layers.Length; i++)
            {
                if (!this._layers[i].IsEmpty)
                {
                    this._layers[i].Draw(drawingContext, this);
                }
            }
            Brush separatorPenBrush = this.SeparatorPenBrush;
            if (separatorPenBrush != null)
            {
                Pen pen = new Pen(separatorPenBrush, 1.0);
                Rect rect = new Rect(base.RenderSize);
                rect = Subtract(rect, this.Padding);
                rect.Inflate((double) -0.5, (double) -0.5);
                drawingContext.DrawLine(pen, new Point(rect.Right, rect.Top), new Point(rect.Right, rect.Bottom)); 
            }
              
        }

       
        private void OnUnloaded(object sender, RoutedEventArgs e)
        {
            base.Loaded += new RoutedEventHandler(this.OnLoaded);
            base.Unloaded -= new RoutedEventHandler(this.OnUnloaded); 
        }

        private static void OnUseRoundedCornersChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            BarMenuChrome chrome = d as BarMenuChrome;
            bool flag = true.Equals(e.NewValue);
            foreach (ChromeLayer layer in chrome._layers)
            {
                layer.AllowRoundedCorners = flag;
            }
            if (chrome.IsArrangeValid && chrome.IsLoaded)
            {
                chrome.InitializeLayers();
                chrome.InvalidateVisual();
            }
        }

        protected override void OnVisualParentChanged(DependencyObject oldParent)
        {
            base.OnVisualParentChanged(oldParent);
            var source = this.FindParent(null, typeof(ITool), null) as FrameworkElement;
            this._tool = source;
            if (_tool != null)
            {
                base.SetBinding(ButtonSizeProperty, DependencyObjectHelper.CreateBindingObject(ToolHelper.ButtonSizeProperty, BindingMode.OneWay, source));

            }
            this._menuButtonArea = (source != null) ? DependencyObjectHelper.FindParent<MenuButtonArea>(this, null, null) : null;
            if (this._menuButtonArea != null)
            {
                base.SetBinding(IsMouseOverMenuButtonAreaProperty, DependencyObjectHelper.CreateBindingObject(UIElement.IsMouseOverProperty, BindingMode.OneWay, this._menuButtonArea));
                base.SetBinding(IsMenuOpenProperty, DependencyObjectHelper.CreateBindingObject(BarMenuTool.IsOpenProperty, BindingMode.OneWay, source));
                //base.SetBinding(IsMenuCheckedProperty, DependencyObjectHelper.CreateBindingObject(BarMenuTool.IsCheckedProperty, BindingMode.OneWay, source)); 
            }
            else
            {
                base.ClearValue(IsMouseOverMenuButtonAreaProperty);
                base.ClearValue(IsMenuOpenProperty);
                base.ClearValue(IsMenuCheckedProperty); 
            }
            

        }

        private static Rect Subtract(Rect rect, Thickness thickness)
        {
            if (!rect.IsEmpty)
            {
                rect.X += thickness.Left;
                rect.Y += thickness.Top;
                rect.Height = Math.Max((double) (rect.Height - (thickness.Top + thickness.Bottom)), (double) 0.0);
                rect.Width = Math.Max((double) (rect.Width - (thickness.Left + thickness.Right)), (double) 0.0);
            }
            return rect;
        }

        private void UpdateBrushReference(DependencyProperty property, object brushReference)
        {
            if (brushReference is ResourceKey)
            {
                base.SetResourceReference(property, brushReference);
            }
            else if (brushReference is Brush)
            {
                base.SetValue(property, brushReference);
            }
            else
            {
                base.ClearValue(property);
            }
        }

        private void VerifyBrushes()
        {
            if (base.IsInitialized)
            {
                object brushReference = null;
                object buttonCheckedHottrackCenterFillKey = null;
                object buttonGroupHoverBorderFillKey = null;
                object buttonGroupPressedBorderFillKey = null;
                object buttonGroupNormalDividerFillKey = null; 
                object menuToolOverlayFillKey = null; 
                if (this._tool != null)
                {
                    RenderStates currentState = this.CurrentState;
                    bool flag = (currentState & RenderStates.HotTracked) != RenderStates.Normal;
                    bool flag2 = (currentState & RenderStates.Checked) != RenderStates.Normal;
                    bool flag3 = (currentState & RenderStates.Pressed) != RenderStates.Normal; 
                    bool flag7 = (currentState & RenderStates.Disabled) != RenderStates.Normal;

                    if (flag3)
                    {
                        //brushReference = MSGuiColors.ButtonPressedBorderDarkFillKey;
                        //buttonGroupHoverBorderFillKey = MSGuiColors.ButtonPressedBorderLightFillKey;
                        //buttonGroupPressedBorderFillKey = MSGuiColors.ButtonPressedCenterFillKey;


                        brushReference = MSGuiColors.ButtonToolPressedBorderDarkFillKey;
                        buttonGroupHoverBorderFillKey = MSGuiColors.ButtonToolPressedInnerBorderFillKey;
                        buttonGroupPressedBorderFillKey = MSGuiColors.ButtonToolPressedCenterFillKey;
                    }
                    else if (flag2)
                    {
                        if (flag)
                        {
                            brushReference = MSGuiColors.ButtonToolCheckedHottrackBorderFillKey;
                            buttonGroupHoverBorderFillKey =
                                MSGuiColors.ButtonToolCheckedHottrackInnerBorderFillKey;
                            buttonGroupPressedBorderFillKey =
                                MSGuiColors.ButtonToolCheckedHottrackCenterFillKey;
                        }
                        else
                        {
                            brushReference = MSGuiColors.ButtonToolCheckedBorderFillKey;
                            buttonGroupHoverBorderFillKey = MSGuiColors.ButtonToolCheckedInnerBorderFillKey;
                            buttonGroupPressedBorderFillKey = MSGuiColors.ButtonToolCheckedCenterFillKey;
                        }
                    }
                    else if (flag)
                    {
                        brushReference = MSGuiColors.ButtonToolHoverBorderDarkFillKey;
                        buttonCheckedHottrackCenterFillKey = MSGuiColors.ButtonToolHoverCenterFillKey;
                        buttonGroupNormalDividerFillKey = MSGuiColors.ButtonToolHoverBorderLightFillKey;
                    }
                    else if (flag7)
                    {
                        brushReference = MSGuiColors.ButtonToolDisabledBorderDarkFillKey;
                        buttonCheckedHottrackCenterFillKey = MSGuiColors.ButtonToolDisabledCenterFillKey;
                        buttonGroupNormalDividerFillKey = MSGuiColors.ButtonToolDisabledBorderLightFillKey;
                    } 
                    this.UpdateBrushReference(OuterFillBrushProperty, buttonCheckedHottrackCenterFillKey);
                    this.UpdateBrushReference(OuterPenBrushProperty, brushReference);
                    this.UpdateBrushReference(MiddlePenBrushProperty, buttonGroupHoverBorderFillKey);
                    this.UpdateBrushReference(MiddleFillBrushProperty, buttonGroupPressedBorderFillKey);
                    //this.UpdateBrushReference(InnerFillBrushProperty, buttonGroupPressedCenterFillKey);
                    this.UpdateBrushReference(InnerPenBrushProperty, buttonGroupNormalDividerFillKey);
                    //this.UpdateBrushReference(GlowFillBrushProperty, buttonCheckedHottrackCenterOverlayFillKey);
                    this.UpdateBrushReference(OverlayFillBrushProperty, menuToolOverlayFillKey);
                    //this.UpdateBrushReference(SeparatorPenBrushProperty, buttonGroupSegmentedHoverDarkSeparatorFillKey);
                    //this.UpdateBrushReference(SeparatorLightPenBrushProperty,
                    //                          largeSegmentedCheckedHottrackSeparatorLightFillKey);
                }
                 
            }
                 
        }

        // Properties
        private RenderStates CurrentState
        {
            get
            {
                RenderStates normal = RenderStates.Normal;
                if (base.IsMouseOver)
                {
                    normal |= RenderStates.HotTracked;
                }
                if (this.IsChecked == true)
                {
                    normal |= RenderStates.Checked;
                }
                if (this.IsPressed)
                {
                    normal |= RenderStates.Pressed;
                }
                BarMenuTool tool = this._tool as BarMenuTool;
                if (tool != null)
                {  
                    if (tool.IsOpen)
                    {
                        return (normal & ~(RenderStates.Pressed | RenderStates.HotTracked));
                    }
                     
                }
                return normal;
            }
        }

        private Brush GlowFillBrush
        {
            get { return (Brush) base.GetValue(GlowFillBrushProperty); }
            set { base.SetValue(GlowFillBrushProperty, value); }
        }

        private Brush InnerFillBrush
        {
            get { return (Brush) base.GetValue(InnerFillBrushProperty); }
            set { base.SetValue(InnerFillBrushProperty, value); }
        }

        private Brush InnerPenBrush
        {
            get { return (Brush) base.GetValue(InnerPenBrushProperty); }
            set { base.SetValue(InnerPenBrushProperty, value); }
        }

        [TypeConverter(typeof(NullableConverter<bool>))]
        public bool? IsChecked
        {
            get { return (bool?) base.GetValue(IsCheckedProperty); }
            set { base.SetValue(IsCheckedProperty, value); }
        }

        
        private bool IsMouseOverMenuButtonArea
        {
            get { return (bool) base.GetValue(IsMouseOverMenuButtonAreaProperty); }
        }

 
        public bool IsPressed
        {
            get { return (bool) base.GetValue(IsPressedProperty); }
            set { base.SetValue(IsPressedProperty, value); }
        }

         
        private Brush MiddleFillBrush
        {
            get { return (Brush) base.GetValue(MiddleFillBrushProperty); }
            set { base.SetValue(MiddleFillBrushProperty, value); }
        }

        private Brush MiddlePenBrush
        {
            get { return (Brush) base.GetValue(MiddlePenBrushProperty); }
            set { base.SetValue(MiddlePenBrushProperty, value); }
        }

        private Brush OuterFillBrush
        {
            get { return (Brush) base.GetValue(OuterFillBrushProperty); }
            set { base.SetValue(OuterFillBrushProperty, value); }
        }

        private Brush OuterPenBrush
        {
            get { return (Brush) base.GetValue(OuterPenBrushProperty); }
            set { base.SetValue(OuterPenBrushProperty, value); }
        }

        private Brush OverlayFillBrush
        {
            get { return (Brush) base.GetValue(OverlayFillBrushProperty); }
            set { base.SetValue(OverlayFillBrushProperty, value); }
        }

        [Bindable(true)]
        public Thickness Padding
        {
            get { return (Thickness) base.GetValue(PaddingProperty); }
            set { base.SetValue(PaddingProperty, value); }
        }

        private Brush SeparatorLightPenBrush
        {
            get { return (Brush) base.GetValue(SeparatorLightPenBrushProperty); }
            set { base.SetValue(SeparatorLightPenBrushProperty, value); }
        }

        private Brush SeparatorPenBrush
        {
            get { return (Brush) base.GetValue(SeparatorPenBrushProperty); }
            set { base.SetValue(SeparatorPenBrushProperty, value); }
        }

        [Bindable(true)]
        public bool UseRoundedCorners
        {
            get { return (bool) base.GetValue(UseRoundedCornersProperty); }
            set { base.SetValue(UseRoundedCornersProperty, value); }
        }

        // Nested Types
        private class ChromeLayer
        {
            // Fields
            private bool _allowRoundedCorners = true;
            private DependencyProperty _brushProperty;
            private Geometry _geometry;
            private double _radius;
            private Rect _rect = Rect.Empty;
            private DependencyProperty _strokeProperty;

            // Methods
            internal ChromeLayer(DependencyProperty brushProperty, DependencyProperty strokeProperty)
            {
                this._brushProperty = brushProperty;
                this._strokeProperty = strokeProperty;
            }

            public void Clear()
            {
                this._geometry = null;
                this._rect = Rect.Empty;
            }

            public void Draw(DrawingContext drawingContext, BarMenuChrome chrome)
            {
                if (!this._rect.IsEmpty || (this._geometry != null))
                {
                    Brush brush = (this._brushProperty != null) ? (chrome.GetValue(this._brushProperty) as Brush) : null;
                    Brush brush2 = (this._strokeProperty != null)
                                       ? (chrome.GetValue(this._strokeProperty) as Brush)
                                       : null;
                    Pen pen = (brush2 != null) ? new Pen(brush2, 1.0) : null;
                    if ((pen != null) || (brush != null))
                    {
                        if (this._geometry == null)
                        {
                            Rect rectangle = this._rect;
                            if (pen != null)
                            {
                                double num = pen.Thickness/2.0;
                                rectangle = Rect.Inflate(this._rect, -num, -num);
                            }
                            if (this._radius > 0.0)
                            {
                                drawingContext.DrawRoundedRectangle(brush, pen, rectangle, this._radius, this._radius);
                            }
                            else
                            {
                                drawingContext.DrawRectangle(brush, pen, rectangle);
                            }
                        }
                        else
                        {
                            drawingContext.DrawGeometry(brush, pen, this._geometry);
                        }
                    }
                }
            }

            public void Initialize(Geometry geometry)
            {
                this._geometry = geometry;
                if ((this._geometry != null) && this._geometry.CanFreeze)
                {
                    this._geometry.Freeze();
                }
                this._rect = Rect.Empty;
                this._radius = 0.0;
            }

            public void Initialize(Rect elementRect, Thickness margins, double radius)
            {
                this.Initialize(elementRect, margins, radius, RoundedRectCorners.All);
            }

            public void Initialize(Rect elementRect, Thickness margins, double radius, RoundedRectCorners corners)
            {
                double edgeThickness = (this._strokeProperty != null) ? 1.0 : 0.0;
                this.Initialize(elementRect, margins, radius, corners, edgeThickness);
            }

            public void Initialize(Rect elementRect, Thickness margins, double radius, RoundedRectCorners corners,
                                   double edgeThickness)
            {
                this.Initialize(elementRect, margins, radius, corners, edgeThickness, RoundedRectSide.Left,
                                RoundedRectSide.Left);
            }

            public void Initialize(Rect elementRect, Thickness margins, double radius, RoundedRectCorners corners,
                                   RoundedRectSide firstSide, RoundedRectSide lastSide)
            {
                double edgeThickness = (this._strokeProperty != null) ? 1.0 : 0.0;
                this.Initialize(elementRect, margins, radius, corners, edgeThickness, firstSide, lastSide);
            }

            public void Initialize(Rect elementRect, Thickness margins, double radius, RoundedRectCorners corners,
                                   double edgeThickness, RoundedRectSide firstSide, RoundedRectSide lastSide)
            {
                this._rect = BarMenuChrome.Subtract(elementRect, margins);
                this._radius = radius;
                if (this._allowRoundedCorners &&
                    (((corners != RoundedRectCorners.All) && (corners != RoundedRectCorners.None)) ||
                     (firstSide != lastSide)))
                {
                    this._geometry = CreateRoundedRectGeometry(this._rect, corners, radius, radius,
                                                                         edgeThickness, firstSide, lastSide);
                    if ((this._geometry != null) && this._geometry.CanFreeze)
                    {
                        this._geometry.Freeze();
                    }
                }
                else
                {
                    if (!this._allowRoundedCorners || (corners == RoundedRectCorners.None))
                    {
                        this._radius = 0.0;
                    }
                    this._geometry = null;
                }
            }

            // Properties
            public bool AllowRoundedCorners
            {
                get { return this._allowRoundedCorners; }
                set { this._allowRoundedCorners = value; }
            }

            public bool IsEmpty
            {
                get { return (this._rect.IsEmpty && (this._geometry == null)); }
            }
        }
        public static Geometry CreateRoundedRectGeometry(Rect rect, RoundedRectCorners roundedCorners, double radiusX, double radiusY, double edgeThickness)
        {
            return CreateRoundedRectGeometry(rect, roundedCorners, radiusX, radiusY, edgeThickness, RoundedRectSide.Left, RoundedRectSide.Left);
        }

        public static Geometry CreateRoundedRectGeometry(Rect rect, RoundedRectCorners roundedCorners, double radiusX, double radiusY, double edgeThickness, RoundedRectSide startSide, RoundedRectSide endSide)
        {
            double num = edgeThickness / 2.0;
            if (startSide == endSide)
            {
                rect.Inflate(-num, -num);
                radiusX = Math.Min(radiusX, rect.Width / 2.0);
                radiusY = Math.Min(radiusY, rect.Height / 2.0);
            }
            else
            {
                int num2 = (int)startSide;
                int num3 = (int)endSide;
                if (num3 <= num2)
                {
                    num3 += 4;
                }
                bool flag = (0 >= num2) || (4 <= num3);
                bool flag2 = (1 >= num2) || (5 <= num3);
                bool flag3 = (2 >= num2) || (6 <= num3);
                bool flag4 = (3 >= num2) || (7 <= num3);
                if (flag)
                {
                    rect.X += num;
                    rect.Width = Math.Max((double)(rect.Width - num), (double)0.0);
                }
                if (flag2)
                {
                    rect.Y += num;
                    rect.Height = Math.Max((double)(rect.Height - num), (double)0.0);
                }
                if (flag3)
                {
                    rect.Width = Math.Max((double)(rect.Width - num), (double)0.0);
                }
                if (flag4)
                {
                    rect.Height = Math.Max((double)(rect.Height - num), (double)0.0);
                }
                radiusX = Math.Min(radiusX, (flag & flag3) ? (rect.Width / 2.0) : rect.Width);
                radiusY = Math.Min(radiusY, (flag2 & flag4) ? (rect.Height / 2.0) : rect.Height);
            }
            if (rect.IsEmpty)
            {
                return null;
            }
            StreamGeometry geometry = new StreamGeometry();
            bool flag5 = (roundedCorners & RoundedRectCorners.TopLeft) == RoundedRectCorners.TopLeft;
            bool flag6 = (roundedCorners & RoundedRectCorners.TopRight) == RoundedRectCorners.TopRight;
            bool flag7 = (roundedCorners & RoundedRectCorners.BottomLeft) == RoundedRectCorners.BottomLeft;
            bool flag8 = (roundedCorners & RoundedRectCorners.BottomRight) == RoundedRectCorners.BottomRight;
            Size size = new Size(radiusX, radiusY);
            Point[] pointArray = new Point[] { flag7 ? new Point(rect.Left, rect.Bottom - radiusY) : rect.BottomLeft, flag5 ? new Point(rect.Left, rect.Top + radiusY) : rect.TopLeft, flag5 ? new Point(rect.X + radiusX, rect.Top) : rect.TopLeft, flag6 ? new Point(rect.Right - radiusX, rect.Top) : rect.TopRight, flag6 ? new Point(rect.Right, rect.Top + radiusY) : rect.TopRight, flag8 ? new Point(rect.Right, rect.Bottom - radiusY) : rect.BottomRight, flag8 ? new Point(rect.Right - radiusX, rect.Bottom) : rect.BottomRight, flag7 ? new Point(rect.Left + radiusX, rect.Bottom) : rect.BottomLeft };
            using (StreamGeometryContext context = geometry.Open())
            {
                int index = (int)startSide * (int)RoundedRectSide.Right;
                int num5 = (endSide == startSide) ? 8 : ((int)endSide * (int)RoundedRectSide.Right + 1);
                if (num5 < index)
                {
                    num5 += 8;
                }
                Point startPoint = pointArray[index];
                context.BeginFigure(startPoint, true, false);
                for (int i = index + 1; i <= num5; i++)
                {
                    int num7 = i % 8;
                    Point point = pointArray[num7];
                    if ((num7 % 2) == 1)
                    {
                        context.LineTo(point, true, false);
                    }
                    else if (point != startPoint)
                    {
                        context.ArcTo(point, size, 0.0, false, SweepDirection.Clockwise, true, false);
                    }
                    startPoint = point;
                }
            }
            return geometry;
        }

 



        private enum RenderStates
        {
            Checked = 4,
            Disabled = 0x10,
            HotTracked = 1,
            //HotTrackedByOtherSegment = 8,
            Normal = 0,
            Pressed = 2
        }
    }

    [Flags]
    public enum RoundedRectCorners
    {
        All = 15,
        Bottom = 12,
        BottomLeft = 4,
        BottomRight = 8,
        Left = 5,
        None = 0,
        Right = 10,
        Top = 3,
        TopLeft = 1,
        TopRight = 2
    }
    public enum RoundedRectSide
    {
        Left = 0,
        Top = 1,
        Right = 2,
        Bottom = 3
    }

 


}
