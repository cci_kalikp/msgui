﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using MorganStanley.MSDotNet.MSGui.Controls.Controls;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MorganStanley.MSDotNet.MSGui.Controls.BarMenu
{
    [DesignTimeVisible(false),
    TemplatePart(Name = "PART_MenuToolPresenterSite", Type = typeof(ContentPresenter)),  
    TemplateVisualState(Name = "Disabled", GroupName = "CommonStates"), 
    TemplateVisualState(Name = "Normal", GroupName = "CommonStates"), 
    TemplateVisualState(Name = "MouseOver", GroupName = "CommonStates") ]
    public class BarMenuTool : ItemsControl, ITool
    {
        public static readonly RoutedEvent OpenedEvent;
        public static readonly RoutedEvent OpeningEvent;
        public static readonly RoutedEvent ClosedEvent; 

        public static readonly ResourceKey SubmenuHeaderTemplateKey; 
        public static readonly ResourceKey SubmenuItemTemplateKey;
        public static readonly DependencyProperty IsOpenProperty;
        public static readonly DependencyProperty InputGestureTextProperty;  
        internal static readonly DependencyProperty ToolMenuItemProperty;
        private ContentPresenter _presenterSite;
        private MenuToolMenu _menu;
        private MenuItem _selectedMenuItem;
        public static readonly ResourceKey MenuItemDropDownArrowStyleKey; 
        public static readonly ResourceKey MenuToolDropDownArrowStyleKey;

        public static readonly DependencyProperty CaptionProperty =
    ToolHelper.CaptionProperty.AddOwner(typeof(BarMenuTool));
        [Bindable(true)]
        public string Caption
        {
            get
            {
                return (string)base.GetValue(CaptionProperty);
            }
            set
            {
                base.SetValue(CaptionProperty, value);
            }
        }



        public static readonly DependencyProperty HasCaptionProperty =
            ToolHelper.HasCaptionProperty.AddOwner(typeof(BarMenuTool));
        [Bindable(true)]
        public bool HasCaption
        {
            get
            {
                return (bool)base.GetValue(HasCaptionProperty);
            }
        }

        public static readonly DependencyProperty HasImageProperty =
            ToolHelper.HasImageProperty.AddOwner(typeof (BarMenuTool));
        [Bindable(true)]
        public bool HasImage
        {
            get
            {
                return (bool)base.GetValue(HasImageProperty);
            }
        } 

        public ImageSource Image
        {
            get { return (ImageSource)GetValue(ImageProperty); }
            set { SetValue(ImageProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Icon.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ImageProperty = ToolHelper.ImageProperty.AddOwner(typeof (BarMenuTool));
         
        public ButtonSize ButtonSize
        {
            get { return (ButtonSize)GetValue(ButtonSizeProperty); }
            set { SetValue(ButtonSizeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ButtonSize.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ButtonSizeProperty = ToolHelper.ButtonSizeProperty.AddOwner(typeof(BarMenuTool));


        public BarMenuTool()
        {
            
        }
        public event RoutedEventHandler Closed
        {
            add
            {
                base.AddHandler(ClosedEvent, value);
            }
            remove
            {
                base.RemoveHandler(ClosedEvent, value);
            }
        }


        public event RoutedEventHandler Opened
        {
            add
            {
                base.AddHandler(OpenedEvent, value);
            }
            remove
            {
                base.RemoveHandler(OpenedEvent, value);
            }
        }


        public event RoutedEventHandler Opening
        {
            add
            {
                base.AddHandler(OpeningEvent, value);
            }
            remove
            {
                base.RemoveHandler(OpeningEvent, value);
            }
        }
        static BarMenuTool()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(BarMenuTool),
                                                          new FrameworkPropertyMetadata(typeof(BarMenuTool)));
 
   
            ClosedEvent = EventManager.RegisterRoutedEvent("Closed", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(BarMenuTool)); 
            OpenedEvent = EventManager.RegisterRoutedEvent("Opened", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(BarMenuTool));
            OpeningEvent = EventManager.RegisterRoutedEvent("Opening", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(BarMenuTool)); 
            IsOpenProperty = DependencyProperty.Register("IsOpen", typeof(bool), typeof(BarMenuTool), new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, new PropertyChangedCallback(OnIsOpenChanged), new CoerceValueCallback(CoerceIsOpened)));
            SubmenuHeaderTemplateKey = new ComponentResourceKey(typeof(BarMenuTool), "SubmenuHeaderTemplateKey");
            SubmenuItemTemplateKey = new ComponentResourceKey(typeof(BarMenuTool), "SubmenuItemTemplateKey");
             //IdProperty = DependencyProperty.RegisterAttached("Id", typeof(string), typeof(BarMenuTool), new FrameworkPropertyMetadata(null, new PropertyChangedCallback(OnIdChanged)));
            InputGestureTextProperty = DependencyProperty.Register("InputGestureText", typeof(string), typeof(BarMenuTool), new FrameworkPropertyMetadata(null));
            ToolMenuItemProperty = DependencyProperty.Register("ToolMenuItem", typeof(ToolMenuItem), typeof(BarMenuTool), new FrameworkPropertyMetadata(null, new PropertyChangedCallback(OnToolMenuItemChanged)));
            FrameworkElement.FocusVisualStyleProperty.OverrideMetadata(typeof(BarMenuTool), new FrameworkPropertyMetadata(new Style()));
            UIElement.FocusableProperty.OverrideMetadata(typeof(BarMenuTool), new FrameworkPropertyMetadata(false));
            ToolTipService.ShowOnDisabledProperty.OverrideMetadata(typeof(BarMenuTool), new FrameworkPropertyMetadata(true));
            ItemsPanelTemplate defaultValue = new ItemsPanelTemplate(new FrameworkElementFactory(typeof(StackPanel)));
            defaultValue.Seal();
            ItemsControl.ItemsPanelProperty.OverrideMetadata(typeof(BarMenuTool), new FrameworkPropertyMetadata(defaultValue));
            ToolTipService.IsEnabledProperty.OverrideMetadata(typeof(BarMenuTool), new FrameworkPropertyMetadata(null, new CoerceValueCallback(CoerceToolTipServiceIsEnabled)));
            MenuItemDropDownArrowStyleKey = new ComponentResourceKey(typeof(BarMenuTool), "MenuItemDropDownArrowStyleKey");
            MenuToolDropDownArrowStyleKey = new ComponentResourceKey(typeof(BarMenuTool), "MenuToolDropDownArrowStyleKey");
  
        }
        public bool IsOpen
        {
            get
            {
                return (bool)base.GetValue(IsOpenProperty);
            }
            set
            {
                base.SetValue(IsOpenProperty, value);
            }
        }


        private static object CoerceIsOpened(DependencyObject target, object value)
        {
            if ((value is bool) && ((bool)value))
            {
                BarMenuTool base2 = target as BarMenuTool;
                if (base2 == null)
                {
                    return value;
                }
                MenuToolPresenter presenter = base2._presenter as MenuToolPresenter;
                if ((presenter != null) && !presenter.IsMenuButtonAreaEnabled)
                {
                    return false;
                }
                base2.RaiseOpeningEvent();
            }
            return value;
        }

 

 

        private static void OnIsOpenChanged(DependencyObject target, DependencyPropertyChangedEventArgs e)
        {
            BarMenuTool d = target as BarMenuTool;
            if ((d != null) && (e.NewValue is bool))
            {
                d.CoerceValue(ToolTipService.IsEnabledProperty);
                bool newValue = (bool)e.NewValue;
                RoutedEventArgs args = new RoutedEventArgs();
                d._bypassRaisingNextOpeningEvent = false;

                if (newValue)
                {
                    if ((d._presenter != null) && !d._presenter.IsKeyboardFocusWithin)
                    {
                        d._presenter.Focus();
                    }
                    
                    d.RaiseOpened(args);
                }
                else
                {
                    if ((d._selectedMenuItem != null) && Selector.GetIsSelected(d._selectedMenuItem))
                    {
                        d._selectedMenuItem.ClearValue(Selector.IsSelectedProperty);
                    }
                    d._selectedMenuItem = null; 
                    d.RaiseClosed(args);
                } 
            }
        }

         

        private static object CoerceToolTipServiceIsEnabled(DependencyObject d, object newValue)
        {
            BarMenuTool base2 = d as BarMenuTool;
            if ((base2 != null) && base2.IsOpen)
            {
                return false;
            }
            return newValue;
        }

        private static void OnToolMenuItemChanged(DependencyObject target, DependencyPropertyChangedEventArgs e)
        {
            BarMenuTool source = target as BarMenuTool;
            if ((source != null))
            {
                ToolMenuItem oldValue = e.OldValue as ToolMenuItem;
                if (oldValue != null)
                {
                    BindingOperations.ClearBinding(oldValue, MenuItem.IsSubmenuOpenProperty);
                }
                ToolMenuItem newValue = e.NewValue as ToolMenuItem;
                if (newValue != null)
                { 
                    newValue.SetBinding(MenuItem.IsSubmenuOpenProperty, new Binding { Path = new PropertyPath(IsOpenProperty), Mode = BindingMode.TwoWay, Source = source });
                    //MenuToolBase rootSourceTool = RibbonToolProxy.GetRootSourceTool(source) as MenuToolBase;
                    //if ((rootSourceTool != null) && (rootSourceTool._currentReparentedToolsOwner == oldValue))
                    //{
                    //    rootSourceTool.SetReparentedToolsOwner(newValue);
                    //}
                }
            }
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate(); 
            ContentPresenter templateChild = base.GetTemplateChild("PART_MenuToolPresenterSite") as ContentPresenter;
            if (this._presenterSite != templateChild)
            {
                if (this._presenterSite != null)
                {
                    this._presenterSite.ClearValue(ContentPresenter.ContentProperty);
                }
                this._presenterSite = templateChild;
                if (this._menu != null)
                {
                    Menu child = this._menu;
                    this._menu = null;
                    base.RemoveLogicalChild(child);
                    child.Items.Clear();
                }
                if (this._presenterSite != null)
                {
                    if (this._menu == null)
                    {
                        this._menu = new MenuToolMenu(this);
                        base.AddLogicalChild(this._menu);
                    }
                    else
                    {
                        this._menu.Items.Clear();
                    }
                    this._menu.Background = Brushes.Transparent;
                    this.EnsurePresenterIsInitialized();
                    this._menu.Items.Add(this._presenter);
                    this._presenterSite.Content = this._menu;
                }
            }

            this.VerifyChildrenCache();

        }
        protected override void OnItemsChanged(NotifyCollectionChangedEventArgs e)
        {
            base.OnItemsChanged(e);
            if (base.IsInitialized)
            {
                if (e.Action != NotifyCollectionChangedAction.Move)
                {
                    VerifyChildrenCache();
                }
                 
            }
        }
         

        protected override DependencyObject GetContainerForItemOverride()
        {
            return new ToolMenuItem();
        }
        protected override bool IsItemItsOwnContainerOverride(object item)
        {
            return (item is ToolMenuItem);
        }
        private bool _bypassRaisingNextOpeningEvent;

        internal void RaiseOpeningEvent()
        {
            if (this._bypassRaisingNextOpeningEvent || this.IsOpen)
            {
                this._bypassRaisingNextOpeningEvent = false;
            }
            else
            { 
                this.RaiseOpening(new RoutedEventArgs());
                this._bypassRaisingNextOpeningEvent = true;
            }
        }
         
        internal void OnMenuItemSelected(ToolMenuItem toolMenuItem, bool isSelected)
        {
            if (isSelected)
            {
                this._selectedMenuItem = toolMenuItem;
            }
            else if (toolMenuItem == this._selectedMenuItem)
            {
                this._selectedMenuItem = null;
            }
        }

 

        protected virtual ToolMenuItem CreateMenuToolPresenter()
        {
            return new MenuToolPresenter();
        }

        private ToolMenuItem _presenter; 

        private void EnsurePresenterIsInitialized()
        {
            if (this._presenter == null)
            {
                this._presenter = this.CreateMenuToolPresenter();
                base.SetValue(ToolMenuItemProperty, this._presenter); 
                this._presenter.SetValue(ToolMenuItem.ToolPropertyKey, this); 
                this._presenter.SetBinding(MenuItem.IconProperty, DependencyObjectHelper.CreateBindingObject(ImageProperty, BindingMode.OneWay, this));
                this._presenter.SetBinding(HeaderedItemsControl.HeaderProperty, DependencyObjectHelper.CreateBindingObject(CaptionProperty, BindingMode.OneWay, this));
                this._presenter.SetBinding(ItemsControl.ItemTemplateProperty, DependencyObjectHelper.CreateBindingObject(ItemsControl.ItemTemplateProperty, BindingMode.OneWay, this));
                this._presenter.SetBinding(ItemsControl.ItemTemplateSelectorProperty, DependencyObjectHelper.CreateBindingObject(ItemsControl.ItemTemplateSelectorProperty, BindingMode.OneWay, this));
                this._presenter.SetBinding(ItemsControl.ItemContainerStyleProperty, DependencyObjectHelper.CreateBindingObject(ItemsControl.ItemContainerStyleProperty, BindingMode.OneWay, this));
                this._presenter.SetBinding(ItemsControl.ItemContainerStyleSelectorProperty, DependencyObjectHelper.CreateBindingObject(ItemsControl.ItemContainerStyleSelectorProperty, BindingMode.OneWay, this));
                this._presenter.SetBinding(ItemsControl.DisplayMemberPathProperty, DependencyObjectHelper.CreateBindingObject(ItemsControl.DisplayMemberPathProperty, BindingMode.OneWay, this));
                this._presenter.SetBinding(ItemsControl.ItemsPanelProperty, DependencyObjectHelper.CreateBindingObject(ItemsControl.ItemsPanelProperty, BindingMode.OneWay, this));
                this._presenter.SetBinding(FrameworkElement.ContextMenuProperty, DependencyObjectHelper.CreateBindingObject(FrameworkElement.ContextMenuProperty, BindingMode.OneWay, this));
                if (ItemsControlItemStringFormatProperty != null)
                {
                    this._presenter.SetBinding(ItemsControlItemStringFormatProperty, DependencyObjectHelper.CreateBindingObject("ItemStringFormat", BindingMode.OneWay, this));
                }
                this._presenter.ItemsSource = this.ItemsToBind;

                this._presenter.SetBinding(MenuItem.IsSubmenuOpenProperty, DependencyObjectHelper.CreateBindingObject(IsOpenProperty, BindingMode.TwoWay, this));
                this.InitializeMenuToolPresenter(this._presenter);
            }
        }

 

        internal void RaiseOpened(RoutedEventArgs args)
        {
            args.RoutedEvent = OpenedEvent;
            args.Source = this;
            this.OnOpened(args);
        }

        protected virtual void OnOpened(RoutedEventArgs args)
        {
            base.RaiseEvent(args);
        }
         


        private void RaiseOpening(RoutedEventArgs args)
        {
            args.RoutedEvent = OpeningEvent;
            args.Source = this; 
            this.OnOpening(args);
        }


        protected virtual void OnOpening(RoutedEventArgs args)
        {
            base.RaiseEvent(args);
        }

        internal void RaiseClosed(RoutedEventArgs args)
        {
            args.RoutedEvent = ClosedEvent;
            args.Source = this;
            this.OnClosed(args);
        }

        protected virtual void OnClosed(RoutedEventArgs args)
        {
            base.RaiseEvent(args);
        }




  

        internal virtual IList ItemsToBind
        {
            get
            { 
                return base.Items;
            }
        }
         
        protected virtual void InitializeMenuToolPresenter(ToolMenuItem menuToolPresenter)
        {
        }



        private static bool? _itemsControlHasItemStringFormatProperty;
        private static DependencyProperty _itemsControlItemStringFormatProperty;

        internal static DependencyProperty ItemsControlItemStringFormatProperty
        {
            get
            {
                if (!_itemsControlHasItemStringFormatProperty.HasValue)
                {
                    DependencyPropertyDescriptor descriptor = DependencyPropertyDescriptor.FromName("ItemStringFormat", typeof(ItemsControl), typeof(ItemsControl));
                    _itemsControlHasItemStringFormatProperty = new bool?(descriptor != null);
                    _itemsControlItemStringFormatProperty = (descriptor == null) ? null : descriptor.DependencyProperty;
                }
                return _itemsControlItemStringFormatProperty;
            }
        }



        [AttachedPropertyBrowsableForChildren]
        public static string GetInputGestureText(DependencyObject d)
        {
            return (string)d.GetValue(InputGestureTextProperty);
        }

        public static void SetInputGestureText(DependencyObject d, string value)
        {
            d.SetValue(InputGestureTextProperty, value);
        }




        private void OnChildElement_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (sender is UIElement)
            {
                if ((bool)e.NewValue)
                {
                    this._totalEnabledChildren++;
                }
                else
                {
                    this._totalEnabledChildren--;
                }
                if (_presenter is MenuToolPresenter)
                {
                    _presenter.CoerceValue(MenuToolPresenter.IsMenuButtonAreaEnabledProperty);
                }
            }
        }
        private ArrayList _childrenCache;
 

        private void VerifyChildrenCache()
        {
            MenuToolPresenter presenter = _presenter as MenuToolPresenter;
            if (presenter != null)
            {
                ItemCollection items = base.Items;
                int count = items.Count;
                Hashtable hashtable = new Hashtable();
                if (this._childrenCache != null)
                {
                    int num2 = this._childrenCache.Count;
                    if (num2 > 0)
                    {
                        for (int j = 0; j < num2; j++)
                        {
                            object key = this._childrenCache[j];
                            hashtable.Add(key, key);
                        }
                        this._childrenCache.Clear();
                    }
                }
                else
                {
                    this._childrenCache = new ArrayList();
                }
                int num4 = this._totalEnabledChildren;
                this._totalEnabledChildren = 0;
                for (int i = 0; i < count; i++)
                {
                    UIElement element = items[i] as UIElement;
                    if (element != null)
                    {
                        if (hashtable.ContainsKey(element))
                        {
                            hashtable.Remove(element);
                        }
                        else
                        {
                            if (element is ToolMenuItem)
                            {
                                (element as ToolMenuItem).ShouldHandleCanExecute = true;
                            }
                            element.IsEnabledChanged += new DependencyPropertyChangedEventHandler(this.OnChildElement_IsEnabledChanged);
                        }
                        this._childrenCache.Add(element);
                        if (element.IsEnabled)
                        {
                            this._totalEnabledChildren++;
                        }
                    }
                    else
                    {
                        this._totalEnabledChildren++;
                    }
                }
                if (hashtable.Count > 0)
                {
                    foreach (DictionaryEntry entry in hashtable)
                    {
                        UIElement element2 = entry.Value as UIElement;
                        if (element2 != null)
                        {
                            element2.IsEnabledChanged -= new DependencyPropertyChangedEventHandler(this.OnChildElement_IsEnabledChanged);
                            ToolMenuItem item = element2 as ToolMenuItem;
                            if (item != null)
                            {
                                item.ShouldHandleCanExecute = false;
                            }
                        }
                    }
                }
                if (num4 < 1)
                {
                    presenter.OnMenuTool_IsEnabledChanged();
                }
                else if (this._totalEnabledChildren < 1)
                {
                    presenter.OnMenuTool_IsEnabledChanged();
                }
            }
        }
         


        private int _totalEnabledChildren;



        internal int TotalEnabledChildren
        {
            get
            {
                return this._totalEnabledChildren;
            }
        }
 




        internal class MenuToolMenu:Menu
        {
            private BarMenuTool _menuTool; 
            private static DependencyProperty _showKeyboardCuesProperty;

            internal MenuToolMenu(BarMenuTool menuTool)
            {
                this._menuTool = menuTool;
            }

            static MenuToolMenu()
            {
                FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(MenuToolMenu), new FrameworkPropertyMetadata(typeof(MenuToolMenu)));
                FrameworkElementFactory factory = new FrameworkElementFactory(typeof(Border));
                factory.SetValue(Border.BorderThicknessProperty, new TemplateBindingExtension(Control.BorderThicknessProperty));
                factory.SetValue(Border.BorderBrushProperty, new TemplateBindingExtension(Control.BorderThicknessProperty));
                factory.SetValue(Border.PaddingProperty, new TemplateBindingExtension(Control.PaddingProperty));
                factory.SetValue(Border.BackgroundProperty, new TemplateBindingExtension(Control.BackgroundProperty));
                factory.SetValue(UIElement.SnapsToDevicePixelsProperty, true);
                FrameworkElementFactory child = new FrameworkElementFactory(typeof(ItemsPresenter));
                child.SetValue(UIElement.SnapsToDevicePixelsProperty, new TemplateBindingExtension(UIElement.SnapsToDevicePixelsProperty));
                factory.AppendChild(child);
                ControlTemplate defaultValue = new ControlTemplate(typeof(MenuToolMenu))
                {
                    VisualTree = factory
                };
                defaultValue.Seal();
                Control.TemplateProperty.OverrideMetadata(typeof(MenuToolMenu), new FrameworkPropertyMetadata(defaultValue));
                Menu.IsMainMenuProperty.OverrideMetadata(typeof(MenuToolMenu), new FrameworkPropertyMetadata(true));
                FrameworkElementFactory root = new FrameworkElementFactory(typeof(CardPanel));
                ItemsControl.ItemsPanelProperty.OverrideMetadata(typeof(MenuToolMenu), new FrameworkPropertyMetadata(new ItemsPanelTemplate(root)));
                EventManager.RegisterClassHandler(typeof(MenuToolMenu), Mouse.LostMouseCaptureEvent, new MouseEventHandler(OnLostMouseCapture));
                EventManager.RegisterClassHandler(typeof(MenuToolMenu), Mouse.PreviewMouseDownOutsideCapturedElementEvent, new MouseButtonEventHandler(OnPreviewMouseDownOutsideCaptured));
                KeyboardNavigation.DirectionalNavigationProperty.OverrideMetadata(typeof(MenuToolMenu), new FrameworkPropertyMetadata(KeyboardNavigationMode.Continue));
                try
                {
                    BindingFlags bindingAttr = BindingFlags.NonPublic | BindingFlags.Static;
                    _showKeyboardCuesProperty = typeof(KeyboardNavigation).GetField("ShowKeyboardCuesProperty", bindingAttr).GetValue(null) as DependencyProperty;
                }
                catch
                {
                }
            }

            protected override void OnIsKeyboardFocusWithinChanged(DependencyPropertyChangedEventArgs e)
            {
                base.OnIsKeyboardFocusWithinChanged(e);
                bool flag = true;
                if (flag.Equals(e.NewValue) && (_showKeyboardCuesProperty != null))
                {
                    base.SetValue(_showKeyboardCuesProperty, true);
                }
            }


            protected override void OnKeyDown(KeyEventArgs e)
            {
                switch (e.Key)
                {
                    case Key.Up:
                    case Key.Down:
                        return;

                    case Key.Escape:
                        {
                            if (base.Items.Count <= 0)
                            {
                                break;
                            }
                            ToolMenuItem item = base.Items[0] as ToolMenuItem;
                            if ((item != null) && item.IsSubmenuOpen)
                            {
                                break;
                            }
                            return;
                        }
                }
                base.OnKeyDown(e);
            }

            private static void OnPreviewMouseDownOutsideCaptured(object sender, MouseButtonEventArgs e)
            {
                MenuToolMenu ancestor = (MenuToolMenu)sender;
                if (((Mouse.Captured == ancestor) && (ancestor._menuTool != null)))
                {
                    DependencyObject reference = ancestor;
                    while (true)
                    {
                        DependencyObject parent = VisualTreeHelper.GetParent(reference);
                        if (parent == null)
                        {
                            break;
                        }
                        reference = parent;
                    }
                    UIElement relativeTo = reference as UIElement;
                    if (relativeTo != null)
                    {
                        IInputElement element2 = relativeTo.InputHitTest(e.GetPosition(relativeTo));
                        if (((element2 is DependencyObject) && ! DependencyObjectHelper.IsDescendantOf(ancestor, (DependencyObject)element2, false, false) && ancestor.IsKeyboardFocusWithin))
                        {
                            relativeTo.Focus();
                        }
                    }
                }
            }

            protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
            {
                base.OnPropertyChanged(e);
                if ((_showKeyboardCuesProperty == null) && (e.Property.Name == "ShowKeyboardCues"))
                {
                    _showKeyboardCuesProperty = e.Property;
                }
            }

            internal static DependencyProperty ShowKeyboardCuesProperty
            {
                get
                {
                    return _showKeyboardCuesProperty;
                }
            }
 


 


            private static void OnLostMouseCapture(object sender, MouseEventArgs e)
            {
                MenuToolMenu menu = sender as MenuToolMenu;
                if (menu != null)
                {
                    menu.Dispatcher.BeginInvoke(DispatcherPriority.Send, new Action(menu.VerifyCapture));
                }
            }


            private void VerifyCapture()
            {
                MenuToolMenu captured = Mouse.Captured as MenuToolMenu;
                if (captured == this)
                {
                    bool flag = true;
                    if (base.Items.Count == 1)
                    {
                        DependencyObject element = base.Items[0] as DependencyObject;
                        if (element != null)
                        {
                            MenuItem item = base.ContainerFromElement(element) as MenuItem;
                            if ((item != null) && item.IsSubmenuOpen)
                            {
                                flag = false;
                            }
                        }
                    }
                    if (flag)
                    {
                        DependencyObject focusedElement = Keyboard.FocusedElement as DependencyObject;
                        if (focusedElement != null)
                        {
                            ToolMenuItem item2 = focusedElement as ToolMenuItem;
                            if (item2 == null)
                            {
                                item2 = focusedElement.FindParent<ToolMenuItem>(null, typeof(Popup));
                            }
                            if (item2 != null)
                            {
                                return;
                            }
                        }
                        base.ReleaseMouseCapture();
                    }
                }
            } 
        }
    }
}
