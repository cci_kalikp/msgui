﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MorganStanley.MSDotNet.MSGui.Controls.BarMenu
{
    [DesignTimeVisible(false)]
    public class DropDownToggle : ContentControl
    {
        // Fields
        public static readonly DependencyProperty IsDroppedDownProperty = DependencyProperty.Register("IsDroppedDown",
                                                                                                      typeof (bool),
                                                                                                      typeof (
                                                                                                          DropDownToggle
                                                                                                          ),
                                                                                                      new FrameworkPropertyMetadata
                                                                                                          (false,
                                                                                                           FrameworkPropertyMetadataOptions
                                                                                                               .BindsTwoWayByDefault,
                                                                                                           new PropertyChangedCallback
                                                                                                               (DropDownToggle
                                                                                                                    .OnIsDroppedDownChanged)));

        private static readonly DependencyPropertyKey IsPressedPropertyKey =
            DependencyProperty.RegisterReadOnly("IsPressed", typeof (bool), typeof (DropDownToggle),
                                                new FrameworkPropertyMetadata(false));
        public static readonly DependencyProperty IsPressedProperty = IsPressedPropertyKey.DependencyProperty;


        [ThreadStatic] private static DropDownToggle s_toggleWithLastMousePress;

        // Methods
        static DropDownToggle()
        {
            UIElement.FocusableProperty.OverrideMetadata(typeof (DropDownToggle),
                                                         new FrameworkPropertyMetadata(false));
            EventManager.RegisterClassHandler(typeof (UIElement), Mouse.PreviewMouseDownEvent,
                                              new MouseButtonEventHandler(DropDownToggle.OnPreviewMouseDown_ClassHandler),
                                              true);
            EventManager.RegisterClassHandler(typeof (UIElement), Mouse.PreviewMouseUpEvent,
                                              new MouseButtonEventHandler(DropDownToggle.OnPreviewMouseUp_ClassHandler),
                                              true);
            EventManager.RegisterClassHandler(typeof (UIElement), Mouse.GotMouseCaptureEvent,
                                              new MouseEventHandler(DropDownToggle.OnGotMouseCapture_ClassHandler), true);
        }

        private static DropDownToggle GetDropDownToggleUnderMouse(object sender, MouseButtonEventArgs e)
        {
            Popup parent = sender as Popup;
            if (parent == null)
            {
                FrameworkElement originalSource = e.OriginalSource as FrameworkElement;
                if (originalSource == null)
                {
                    return null;
                }
                if (VisualTreeHelper.GetParent(originalSource) != null)
                {
                    return null;
                }
                parent = originalSource.Parent as Popup;
                if ((parent == null) || parent.StaysOpen)
                {
                    return null;
                }
            }
            FrameworkElement templatedParent = parent.TemplatedParent as FrameworkElement;
            if (templatedParent == null)
            {
                templatedParent = parent.PlacementTarget as FrameworkElement;
            }
            if (templatedParent == null)
            {
                return null;
            }
            DependencyObject descendant =
                templatedParent.InputHitTest(e.GetPosition(templatedParent)) as DependencyObject;
            if (descendant == null)
            {
                return null;
            }
            DropDownToggle toggle = descendant as DropDownToggle;
            if (toggle == null)
            {
                toggle = DependencyObjectHelper.FindParent<DropDownToggle>(descendant, null, null);
            }
            return toggle;
        }

 

        private static void OnGotMouseCapture_ClassHandler(object sender, MouseEventArgs e)
        {
            if (!(Mouse.Captured is DropDownToggle))
            {
                ResetToggleWithLastMousePress();
            }
        }

        private static void OnIsDroppedDownChanged(DependencyObject target, DependencyPropertyChangedEventArgs e)
        {
            DropDownToggle toggle = target as DropDownToggle;
            if (toggle != null)
            {
                toggle.SetValue(IsPressedPropertyKey, false);
                bool? newValue = (bool?) e.NewValue;
                bool? oldValue = (bool?) e.OldValue; 
            }
        }

        protected override void OnMouseEnter(MouseEventArgs e)
        {
            base.OnMouseEnter(e);
            if (this == s_toggleWithLastMousePress)
            {
                base.SetValue(IsPressedPropertyKey, true);
            }
        }

        protected override void OnMouseLeave(MouseEventArgs e)
        {
            base.OnMouseLeave(e);
            base.SetValue(IsPressedPropertyKey, false);
        }

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);
            if (!e.Handled)
            {
                this.ProcessMouseDown(e);
            }
        }

        protected override void OnMouseLeftButtonUp(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonUp(e);
            if (Mouse.Captured == this)
            {
                base.ReleaseMouseCapture();
                base.SetValue(IsPressedPropertyKey, false);
                ResetToggleWithLastMousePress();
                e.Handled = true;
            }
        }

        private static void OnPreviewMouseDown_ClassHandler(object sender, MouseButtonEventArgs e)
        {
            ResetToggleWithLastMousePress();
            DropDownToggle dropDownToggleUnderMouse = GetDropDownToggleUnderMouse(sender, e);
            if ((dropDownToggleUnderMouse != null) && dropDownToggleUnderMouse.IsDroppedDown)
            {
                dropDownToggleUnderMouse.ProcessMouseDown(e);
            }
        }

        private static void OnPreviewMouseUp_ClassHandler(object sender, MouseButtonEventArgs e)
        {
            ResetToggleWithLastMousePress();
            DropDownToggle dropDownToggleUnderMouse = GetDropDownToggleUnderMouse(sender, e);
            if ((dropDownToggleUnderMouse != null) && dropDownToggleUnderMouse.IsDroppedDown)
            {
                e.Handled = true;
            }
        }

        private void ProcessMouseDown(MouseButtonEventArgs e)
        {
            bool flag = !this.IsDroppedDown;
            this.IsDroppedDown = flag;
            if (!flag)
            {
                base.CaptureMouse();
            }
            if (((Mouse.LeftButton == MouseButtonState.Pressed) && (Mouse.Captured == null)) && base.IsMouseOver)
            {
                base.SetValue(IsPressedPropertyKey, true);
                s_toggleWithLastMousePress = this;
            }
            e.Handled = true;
        }

         

        private static void ResetToggleWithLastMousePress()
        {
            DropDownToggle captured = Mouse.Captured as DropDownToggle;
            if (captured != null)
            {
                captured.ReleaseMouseCapture();
            }
            if (s_toggleWithLastMousePress != null)
            {
                s_toggleWithLastMousePress.SetValue(IsPressedPropertyKey, false);
                s_toggleWithLastMousePress = null;
            }
        }

        // Properties
        [Bindable(true)]
        public bool IsDroppedDown
        {
            get { return (bool) base.GetValue(IsDroppedDownProperty); }
            set { base.SetValue(IsDroppedDownProperty, value); }
        }

        [ReadOnly(true), Bindable(true)]
        public bool IsPressed
        {
            get { return (bool) base.GetValue(IsPressedProperty); }
        }
    }


}
