﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;

namespace MorganStanley.MSDotNet.MSGui.Controls.BarMenu
{
    public static class ToolHelper
    {
        public static readonly DependencyProperty ButtonSizeProperty =
            DependencyProperty.RegisterAttached("ButtonSize", typeof (ButtonSize), typeof (ToolHelper), new PropertyMetadata(ButtonSize.Small));

        public static void SetButtonSize(UIElement element, ButtonSize value)
        {
            element.SetValue(ButtonSizeProperty, value);
        }

        public static ButtonSize GetButtonSize(UIElement element)
        {
            return (ButtonSize) element.GetValue(ButtonSizeProperty);
        }

        public static readonly DependencyProperty ImageProperty =
            DependencyProperty.RegisterAttached("Image", typeof (ImageSource), typeof (ToolHelper), new PropertyMetadata(default(ImageSource), OnImageChanged));

        private static void OnImageChanged(DependencyObject dependencyObject_, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs_)
        {
            if (dependencyPropertyChangedEventArgs_.NewValue == null)
            {
                dependencyObject_.ClearValue(HasImagePropertyKey);
            }
            else
            {
                dependencyObject_.SetValue(HasImagePropertyKey, true);
            }
        }

        public static void SetImage(UIElement element, ImageSource value)
        {
            element.SetValue(ImageProperty, value);
        }

        public static ImageSource GetImage(UIElement element)
        {
            return (ImageSource) element.GetValue(ImageProperty);
        }

        public static readonly DependencyPropertyKey HasImagePropertyKey = DependencyProperty.RegisterAttachedReadOnly("HasImage", typeof(bool), typeof(ToolHelper), new FrameworkPropertyMetadata(false));

        public static readonly DependencyProperty HasImageProperty = HasImagePropertyKey.DependencyProperty;

        public static readonly DependencyProperty CaptionProperty =
            DependencyProperty.RegisterAttached("Caption", typeof (string), typeof (ToolHelper), new PropertyMetadata(default(string), OnCaptionChanged));

        private static void OnCaptionChanged(DependencyObject dependencyObject_, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs_)
        {
           if (string.IsNullOrEmpty(dependencyPropertyChangedEventArgs_.NewValue as string))
           {
                dependencyObject_.ClearValue(HasCaptionPropertyKey);
           }
           else
           {
               dependencyObject_.SetValue(HasCaptionPropertyKey, true);
           }
        }


        public static void SetCaption(UIElement element, string value)
        {
            element.SetValue(CaptionProperty, value);
        }

        public static string GetCaption(UIElement element)
        {
            return (string)element.GetValue(CaptionProperty);
        }
        public static readonly DependencyPropertyKey HasCaptionPropertyKey = DependencyProperty.RegisterAttachedReadOnly("HasCaption", typeof(bool), typeof(ToolHelper), new FrameworkPropertyMetadata(false));

        public static readonly DependencyProperty HasCaptionProperty = HasCaptionPropertyKey.DependencyProperty;



    }
}
