﻿using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using MorganStanley.MSDotNet.MSGui.Controls.Controls;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MorganStanley.MSDotNet.MSGui.Controls.BarMenu
{
    [TemplatePart(Name = "PART_MenuButtonArea", Type = typeof (MenuButtonArea))]
    public class MenuToolPresenter : ToolMenuItem
    {
        // Fields 
        private MenuButtonArea _menuButtonArea;

        internal static readonly DependencyProperty IsMenuButtonAreaEnabledProperty =
            DependencyProperty.Register("IsMenuButtonAreaEnabled", typeof (bool), typeof (MenuToolPresenter),
                                        new FrameworkPropertyMetadata(true,
                                                                      new PropertyChangedCallback(
                                                                          MenuToolPresenter
                                                                              .OnIsMenuButtonAreaEnabledChanged),
                                                                      new CoerceValueCallback(
                                                                          MenuToolPresenter
                                                                              .CoerceIsMenuButtonAreaEnabled)));

      

        internal static readonly DependencyProperty ResizedColumnCountProperty =
            DependencyProperty.Register("ResizedColumnCount", typeof (int), typeof (MenuToolPresenter),
                                        new FrameworkPropertyMetadata(0, FrameworkPropertyMetadataOptions.AffectsMeasure));

        // Methods
        static MenuToolPresenter()
        {
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof (MenuToolPresenter),
                                                                      new FrameworkPropertyMetadata(
                                                                          typeof (MenuToolPresenter)));
            KeyboardNavigation.DirectionalNavigationProperty.OverrideMetadata(typeof (MenuToolPresenter),
                                                                              new FrameworkPropertyMetadata(
                                                                                  KeyboardNavigationMode.Continue));
            UIElement.FocusableProperty.OverrideMetadata(typeof (MenuToolPresenter),
                                                         new FrameworkPropertyMetadata(null,
                                                                                       new CoerceValueCallback(
                                                                                           MenuToolPresenter
                                                                                               .CoerceFocusable))); 
        }

        private static object CoerceFocusable(DependencyObject d, object newValue)
        {
            MenuToolPresenter presenter = (MenuToolPresenter) d;
            if (!presenter.IsMenuButtonAreaEnabled)
            {
                return false;
            }
            return newValue;
        }

        private static object CoerceIsMenuButtonAreaEnabled(DependencyObject target, object value)
        {
            MenuToolPresenter presenter = target as MenuToolPresenter;
            if ((presenter == null) || !((bool) value))
            {
                return value;
            }
            BarMenuTool menuTool = presenter.Tool;
            if (menuTool == null)
            {
                return value;
            }
            if (menuTool.IsEnabled)
            {
                if (menuTool.TotalEnabledChildren >= 1)
                {
                    return value;
                }
                
            }
            return false;
        }

        

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            if (this._menuButtonArea != null)
            {
                BindingOperations.ClearBinding(this._menuButtonArea, UIElement.IsEnabledProperty);
            }
            this._menuButtonArea = base.GetTemplateChild("PART_MenuButtonArea") as MenuButtonArea;
            if (this._menuButtonArea != null)
            {
                base.CoerceValue(IsMenuButtonAreaEnabledProperty);
                this._menuButtonArea.SetBinding(UIElement.IsEnabledProperty,
                                                DependencyObjectHelper.CreateBindingObject(IsMenuButtonAreaEnabledProperty,
                                                                              BindingMode.OneWay, this));
            } 
        }

         
 

        private static void OnIsMenuButtonAreaEnabledChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            d.CoerceValue(UIElement.FocusableProperty); 
        }

        internal void OnMenuTool_IsEnabledChanged()
        {
            if ((this._menuButtonArea != null) && (this.Tool != null))
            {
                base.CoerceValue(IsMenuButtonAreaEnabledProperty);
            }
        }

      
        internal bool IsMenuButtonAreaEnabled
        {
            get { return (bool) base.GetValue(IsMenuButtonAreaEnabledProperty); }
        }

      
    }



}
