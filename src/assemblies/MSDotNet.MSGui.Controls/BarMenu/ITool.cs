﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace MorganStanley.MSDotNet.MSGui.Controls.BarMenu
{
    internal interface ITool
    {
        ButtonSize ButtonSize { get; set; }
        string Caption { get; set; }
        ImageSource Image { get; set; }
    }
}
