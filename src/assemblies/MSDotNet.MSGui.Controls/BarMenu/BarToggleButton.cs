﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace MorganStanley.MSDotNet.MSGui.Controls.BarMenu
{
    public class BarToggleButton: ToggleButton, ITool
    {

        public static readonly DependencyProperty CaptionProperty =
    ToolHelper.CaptionProperty.AddOwner(typeof(BarToggleButton));
        [Bindable(true)]
        public string Caption
        {
            get
            {
                return (string)base.GetValue(CaptionProperty);
            }
            set
            {
                base.SetValue(CaptionProperty, value);
            }
        }



        public static readonly DependencyProperty HasCaptionProperty =
            ToolHelper.HasCaptionProperty.AddOwner(typeof(BarToggleButton));
        [Bindable(true)]
        public bool HasCaption
        {
            get
            {
                return (bool)base.GetValue(HasCaptionProperty);
            }
        }

        public static readonly DependencyProperty HasImageProperty =
            ToolHelper.HasImageProperty.AddOwner(typeof(BarToggleButton));
        [Bindable(true)]
        public bool HasImage
        {
            get
            {
                return (bool)base.GetValue(HasImageProperty);
            }
        }

        public ImageSource Image
        {
            get { return (ImageSource)GetValue(ImageProperty); }
            set { SetValue(ImageProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Icon.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ImageProperty = ToolHelper.ImageProperty.AddOwner(typeof(BarToggleButton));

        public ButtonSize ButtonSize
        {
            get { return (ButtonSize)GetValue(ButtonSizeProperty); }
            set { SetValue(ButtonSizeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ButtonSize.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ButtonSizeProperty = ToolHelper.ButtonSizeProperty.AddOwner(typeof(BarToggleButton));
         

        public bool AllowToggle
        {
            get { return (bool)GetValue(AllowToggleProperty); }
            set { SetValue(AllowToggleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for AllowToggle.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AllowToggleProperty =
            DependencyProperty.Register("AllowToggle", typeof(bool), typeof(BarToggleButton), new UIPropertyMetadata(true)); 

        static BarToggleButton()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(BarToggleButton), new FrameworkPropertyMetadata(typeof(BarToggleButton)));
           
        }

        protected override void OnToggle()
        {
            if (!AllowToggle) return;
            base.OnToggle();
        }
    }
}
