﻿using System;
using System.ComponentModel;
using System.Security;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MorganStanley.MSDotNet.MSGui.Controls.BarMenu
{
    public class ToolMenuItem:MenuItem
    {
        public static readonly ResourceKey SubmenuItemTemplateKey = new ComponentResourceKey(typeof(ToolMenuItem), "SubmenuItemTemplateKey");

        public static readonly ResourceKey SubmenuHeaderTemplateKey = new ComponentResourceKey(typeof(ToolMenuItem), "SubmenuHeaderTemplateKey");

        public static readonly ResourceKey MenuItemDropDownArrowStyleKey = new ComponentResourceKey(typeof(ToolMenuItem), "MenuItemDropDownArrowStyleKey"); 
        public static readonly DependencyProperty ToolProperty;
        internal static readonly DependencyPropertyKey ToolPropertyKey;
        internal static readonly DependencyPropertyKey IsSeparatorPropertyKey;
        public static readonly DependencyProperty IsSeparatorProperty; 
 

        static ToolMenuItem()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ToolMenuItem),
                                                          new FrameworkPropertyMetadata(typeof(ToolMenuItem)));
            ToolPropertyKey = DependencyProperty.RegisterReadOnly("Tool", typeof(BarMenuTool), typeof(ToolMenuItem), new FrameworkPropertyMetadata(null));
            ToolProperty = ToolPropertyKey.DependencyProperty;
            IsSeparatorPropertyKey = DependencyProperty.RegisterReadOnly("IsSeparator", typeof(bool), typeof(ToolMenuItem), new FrameworkPropertyMetadata(false));
            IsSeparatorProperty = IsSeparatorPropertyKey.DependencyProperty;
            ItemsPanelTemplate defaultValue = new ItemsPanelTemplate(new FrameworkElementFactory(typeof(StackPanel)));
            defaultValue.Seal();
            ItemsControl.ItemsPanelProperty.OverrideMetadata(typeof(ToolMenuItem), new FrameworkPropertyMetadata(defaultValue));
            MenuItem.IsSubmenuOpenProperty.OverrideMetadata(typeof(ToolMenuItem), new FrameworkPropertyMetadata(null, new CoerceValueCallback(ToolMenuItem.CoerceIsSubmenuOpen)));
            Selector.IsSelectedProperty.OverrideMetadata(typeof(ToolMenuItem), new FrameworkPropertyMetadata(new PropertyChangedCallback(ToolMenuItem.OnIsSelectedChanged)));
            EventManager.RegisterClassHandler(typeof(ToolMenuItem), Mouse.PreviewMouseDownOutsideCapturedElementEvent, new MouseButtonEventHandler(ToolMenuItem.OnPreviewMouseDownOutsideCapturedElement));
            //EventManager.RegisterClassHandler(typeof(ToolMenuItem), Keyboard.KeyDownEvent, new KeyEventHandler(ToolMenuItem.OnClassKeyDown));
 
        }

        public ToolMenuItem()
        {
            
        }
        private static void OnPreviewMouseDownOutsideCapturedElement(object sender, MouseButtonEventArgs e)
        {
            ToolMenuItem item = sender as ToolMenuItem;
            if (item != null)
            {
                FrameworkElement header = item.Header as FrameworkElement;
                if ((header != null) && ((!(Mouse.Captured is Visual) || !header.IsAncestorOf((DependencyObject)Mouse.Captured)) || (header.InputHitTest(e.GetPosition(header)) == null)))
                {
                    item.ReleaseMouseCaptureForElementTree();
                    if (header.IsKeyboardFocusWithin)
                    {
                        item.Focus();
                    }
                    e.Handled = true;
                }
            }
        }

        private FrameworkElement _elementTreeToCapture;

        private void ReleaseMouseCaptureForElementTree()
        {
            FrameworkElement ancestor = this._elementTreeToCapture;
            this._elementTreeToCapture = null;
            IInputElement captured = Mouse.Captured;
            if (captured != null)
            {
                captured.RemoveHandler(UIElement.LostMouseCaptureEvent, new MouseEventHandler(this.OnElementLostMouseCapture));
                if (DependencyObjectHelper.IsLogicalAncestor(ancestor, captured as DependencyObject))
                {
                    captured.ReleaseMouseCapture();
                }
            }
        }

        private void OnElementLostMouseCapture(object sender, MouseEventArgs e)
        {
            IInputElement element = sender as IInputElement;
            element.RemoveHandler(UIElement.LostMouseCaptureEvent, new MouseEventHandler(this.OnElementLostMouseCapture));
            IInputElement captured = Mouse.Captured;
            if ((captured != null) || !DependencyObjectHelper.IsDescendantOf(this, Keyboard.FocusedElement as DependencyObject, false, false))
            {
                if ((captured is DependencyObject) && DependencyObjectHelper.IsLogicalAncestor(this._elementTreeToCapture, captured as DependencyObject))
                {
                    captured.AddHandler(UIElement.LostMouseCaptureEvent, new MouseEventHandler(this.OnElementLostMouseCapture));
                }
                else
                {
                    this._elementTreeToCapture = null;
                }
            }
            else if ((this._elementTreeToCapture != null) && (this._elementTreeToCapture != element))
            {
                Mouse.Capture(this._elementTreeToCapture, CaptureMode.SubTree);
                if (this._elementTreeToCapture != null)
                {
                    this._elementTreeToCapture.AddHandler(UIElement.LostMouseCaptureEvent, new MouseEventHandler(this.OnElementLostMouseCapture));
                }
            }
        }
  


        private static object CoerceIsSubmenuOpen(DependencyObject target, object value)
        {
            ToolMenuItem item = target as ToolMenuItem;
            if (((item != null) && (value is bool)) && ((bool)value))
            {
                var tool = item.Tool; 
                if (tool != null)
                {
                    tool.RaiseOpeningEvent();
                }
                //item.OnSubMenuOpening();
            }
            return value;
        }
         

        private static void OnIsSelectedChanged(DependencyObject target, DependencyPropertyChangedEventArgs e)
        {
            ToolMenuItem toolMenuItem = target as ToolMenuItem;
            if (toolMenuItem != null)
            { 
                ToolMenuItem parentMenuItem = toolMenuItem.ParentMenuItem;
                if (parentMenuItem != null)
                {
                    BarMenuTool ribbonTool = parentMenuItem.Tool;
                    if (ribbonTool != null)
                    {
                        ribbonTool.OnMenuItemSelected(toolMenuItem, (bool)e.NewValue);
                    }
                }
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Bindable(true), ReadOnly(true)]
        public bool IsSeparator
        {
            get
            {
                return (bool)base.GetValue(IsSeparatorProperty);
            }
        }
 

        private ToolMenuItem _parentMenuItem;
  
        internal ToolMenuItem ParentMenuItem
        {
            get
            {
                if ((this._parentMenuItem == null) && (base.GetType() == typeof(ToolMenuItem)))
                {
                    _parentMenuItem = this.FindParent<ToolMenuItem>(null, null);
                }
                return this._parentMenuItem;
            }
        }


        [Bindable(true), ReadOnly(true)]
        public BarMenuTool Tool
        {
            get
            {
                return (BarMenuTool)base.GetValue(ToolProperty);
            }
        }

        private bool _shouldHandleCanExecute;
 

        internal bool ShouldHandleCanExecute
        {
            get
            {
                return this._shouldHandleCanExecute;
            }
            set
            {
                if (value != this._shouldHandleCanExecute)
                {
                    this._shouldHandleCanExecute = value;
                    this.HookUnhookCommand(base.Command, value);
                    this.UpdateCanExecute();
                }
            }
        }
        private EventHandler _canExecuteChangedHandler;

        private void OnCanExecuteChanged(object sender, EventArgs e)
        {
            this.UpdateCanExecute();
        }
         

        private void HookUnhookCommand(ICommand command, bool hook)
        {
            if (command != null)
            {
                if (this._canExecuteChangedHandler == null)
                {
                    this._canExecuteChangedHandler = new EventHandler(this.OnCanExecuteChanged);
                }
                if (hook)
                {
                    command.CanExecuteChanged += (this._canExecuteChangedHandler);
                }
                else
                {
                    command.CanExecuteChanged -= (this._canExecuteChangedHandler);
                }
            }
        }

        private bool? _canExecute;

        protected override bool IsEnabledCore
        {
            get
            {
                if (!this._canExecute.HasValue)
                {
                    return base.IsEnabledCore;
                }
                return (this._canExecute == true); 
            }
        }

 

        private void UpdateCanExecute()
        {
            if (this._shouldHandleCanExecute)
            {
                this._canExecute = new bool?((base.Command == null) || FrameworkElementHelper.CanExecuteCommand(this));
            }
            else if (this._canExecute.HasValue)
            {
                this._canExecute = null;
            }
            else
            {
                return;
            }
            base.CoerceValue(UIElement.IsEnabledProperty);
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            DependencyProperty property = e.Property;
            if (property == MenuItem.IsSubmenuOpenProperty)
            {
                bool flag = false;
                if ((flag.Equals(e.NewValue) && base.IsKeyboardFocusWithin) && (!base.IsKeyboardFocused && base.IsMouseCaptureWithin))
                {
                    base.Focus();
                }
            }
            base.OnPropertyChanged(e);
            if (property == HeaderedItemsControl.HeaderProperty)
            {
                if (base.Role != MenuItemRole.TopLevelHeader)
                { 
                    FrameworkElement newValue = e.NewValue as FrameworkElement;
                    if (newValue != null)
                    {
                        if (newValue is Separator)
                        {
                            base.SetValue(IsSeparatorPropertyKey, true);
                        }
                        else
                        {
                            base.ClearValue(IsSeparatorPropertyKey);
                        }
                    }
                    else
                    {
                        base.ClearValue(IsSeparatorPropertyKey);
                    }
                }
            }
            else if (this._shouldHandleCanExecute)
            {
                if (property == MenuItem.CommandProperty)
                {
                    this.HookUnhookCommand(e.OldValue as ICommand, false);
                    this.HookUnhookCommand(e.NewValue as ICommand, true);
                    this.UpdateCanExecute();
                }
                else if ((property == MenuItem.CommandParameterProperty) || (property == MenuItem.CommandTargetProperty))
                {
                    this.UpdateCanExecute();
                }
            }
        }

 

 

    }
}
