﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MorganStanley.MSDotNet.MSGui.Controls.BarMenu
{
    [DesignTimeVisible(false), TemplatePart(Name = "PART_Line2", Type = typeof (TextBlock)),
     TemplatePart(Name = "PART_Line1", Type = typeof (TextBlock))]
    public class LargeToolCaptionPresenter : Control
    {
        // Fields
        private bool _cachedGlyphInfoDirty = true;
        private bool _cachedWordInfoDirty = true;
        private InlineUIContainer _glyphInlineUIContainer;
        private Size _glyphSize = Size.Empty;
        private TextBlock _line1;
        private TextBlock _line2;
        private TextBlock _textBlockPrivate = new TextBlock();
        private List<WordInfo> _wordInfos;

        public static readonly DependencyProperty GlyphProperty = DependencyProperty.Register("Glyph",
                                                                                              typeof (UIElement),
                                                                                              typeof (
                                                                                                  LargeToolCaptionPresenter
                                                                                                  ),
                                                                                              new FrameworkPropertyMetadata
                                                                                                  (null,
                                                                                                   FrameworkPropertyMetadataOptions
                                                                                                       .AffectsMeasure,
                                                                                                   new PropertyChangedCallback
                                                                                                       (LargeToolCaptionPresenter
                                                                                                            .OnGlyphChanged)));

        public static readonly DependencyProperty TextProperty = DependencyProperty.Register("Text", typeof (object),
                                                                                             typeof (
                                                                                                 LargeToolCaptionPresenter
                                                                                                 ),
                                                                                             new FrameworkPropertyMetadata
                                                                                                 (null,
                                                                                                  FrameworkPropertyMetadataOptions
                                                                                                      .AffectsMeasure,
                                                                                                  new PropertyChangedCallback
                                                                                                      (LargeToolCaptionPresenter
                                                                                                           .OnTextChanged)));

        // Methods
        static LargeToolCaptionPresenter()
        {
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof (LargeToolCaptionPresenter),
                                                                      new FrameworkPropertyMetadata(
                                                                          typeof (LargeToolCaptionPresenter)));
            UIElement.FocusableProperty.OverrideMetadata(typeof (LargeToolCaptionPresenter),
                                                         new FrameworkPropertyMetadata(false));
        }

        protected override Size MeasureOverride(Size availableSize)
        {
            if ((this._line1 == null) | (this._line2 == null))
            {
                return new Size(1.0, 1.0);
            }
            if (this._cachedWordInfoDirty)
            {
                string[] strArray =
                    ((this.Text is string) ? ((string) this.Text) : string.Empty).Split(new string[] {" "},
                                                                                        StringSplitOptions
                                                                                            .RemoveEmptyEntries);
                this._wordInfos = new List<WordInfo>(strArray.Length);
                for (int i = 0; i < strArray.Length; i++)
                {
                    string word = strArray[i];
                    this._wordInfos.Add(new WordInfo(word, this.MeasureText(word)));
                }
                this._cachedWordInfoDirty = false;
            }
            if (this._cachedGlyphInfoDirty)
            {
                Size empty = Size.Empty;
                if (this._glyphInlineUIContainer != null)
                {
                    this._glyphInlineUIContainer.Child = null;
                    this._glyphInlineUIContainer = null;
                }
                if (this.Glyph != null)
                {
                    this.Glyph.Measure(availableSize);
                    this._glyphSize = this.Glyph.DesiredSize;
                    if (this._glyphInlineUIContainer == null)
                    {
                        this._glyphInlineUIContainer = new InlineUIContainer(this.Glyph);
                        this._glyphInlineUIContainer.SetBinding(ContentElement.IsEnabledProperty,
                                                                DependencyObjectHelper.CreateBindingObject(
                                                                    UIElement.IsEnabledProperty, BindingMode.OneWay,
                                                                    this));
                    }
                }
                this._cachedGlyphInfoDirty = false;
            }
            double width = this.MeasureText(" ").Width;
            this._line1.Inlines.Clear();
            this._line2.Inlines.Clear();
            this._line1.Text = this._line2.Text = "";
            if (this._wordInfos.Count == 0)
            {
                if (this._glyphSize != Size.Empty)
                {
                    this._line1.Inlines.Add(new Run("﻿"));
                    this._line1.Inlines.Add(this._glyphInlineUIContainer);
                }
            }
            else if (this._wordInfos.Count == 1)
            {
                WordInfo info = this._wordInfos[0];
                this._line1.Text = info.Word;
                if (this._glyphSize != Size.Empty)
                {
                    this._line2.Inlines.Add(new Run("﻿"));
                    this._line2.Inlines.Add(this._glyphInlineUIContainer);
                }
            }
            else
            {
                int num2 = this._wordInfos.Count/2;
                if ((num2*2) != this._wordInfos.Count)
                {
                    num2++;
                }
                StringBuilder builder = new StringBuilder(100);
                for (int j = 0; j < num2; j++)
                {
                    WordInfo info2 = this._wordInfos[j];
                    builder.Append(info2.Word);
                    if (j < (num2 - 1))
                    {
                        builder.Append(" ");
                    }
                }
                this._line1.Text = builder.ToString();
                builder = new StringBuilder(100);
                double count = this._wordInfos.Count;
                for (int k = num2; k < count; k++)
                {
                    WordInfo info3 = this._wordInfos[k];
                    builder.Append(info3.Word);
                    builder.Append(" ");
                }
                this._line2.Text = builder.ToString();
                if (this._glyphSize != Size.Empty)
                {
                    this._line2.Inlines.Add(this._glyphInlineUIContainer);
                }
            }
            return base.MeasureOverride(availableSize);
        }

        private Size MeasureText(Inline[] inlines)
        {
            this._textBlockPrivate.Inlines.Clear();
            this._textBlockPrivate.Inlines.AddRange(inlines);
            this._textBlockPrivate.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));
            return this._textBlockPrivate.DesiredSize;
        }

        private Size MeasureText(string text)
        {
            this._textBlockPrivate.Text = text;
            this._textBlockPrivate.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));
            return this._textBlockPrivate.DesiredSize;
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            this._line1 = base.GetTemplateChild("PART_Line1") as TextBlock;
            this._line2 = base.GetTemplateChild("PART_Line2") as TextBlock;
        }

        private static void OnGlyphChanged(DependencyObject target, DependencyPropertyChangedEventArgs e)
        {
            LargeToolCaptionPresenter presenter = target as LargeToolCaptionPresenter;
            if (presenter != null)
            {
                presenter._cachedGlyphInfoDirty = true;
            }
        }

        private static void OnTextChanged(DependencyObject target, DependencyPropertyChangedEventArgs e)
        {
            LargeToolCaptionPresenter presenter = target as LargeToolCaptionPresenter;
            if (presenter != null)
            {
                presenter._cachedWordInfoDirty = true;
            }
        }

        private string TrimTextToLength(string text, double trimToLength)
        {
            Math.Max(trimToLength, 0.0);
            string str = text;
            for (double i = this.MeasureText(str).Width;
                 (i != 0.0) && (i > trimToLength);
                 i = this.MeasureText(str).Width)
            {
                str = str.Substring(0, str.Length - 1);
            }
            return str;
        }

        // Properties
        [Bindable(true)]
        public UIElement Glyph
        {
            get { return (UIElement) base.GetValue(GlyphProperty); }
            set { base.SetValue(GlyphProperty, value); }
        }

        [Bindable(true)]
        public object Text
        {
            get { return base.GetValue(TextProperty); }
            set { base.SetValue(TextProperty, value); }
        }

        // Nested Types
        [StructLayout(LayoutKind.Sequential)]
        private struct WordInfo
        {
            internal string Word;
            internal Size Size;

            internal WordInfo(string word, Size size)
            {
                this.Word = word;
                this.Size = size;
            }
        }
    }

}
