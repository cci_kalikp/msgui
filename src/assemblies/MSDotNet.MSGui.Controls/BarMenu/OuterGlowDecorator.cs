﻿using System;
using System.ComponentModel;
using System.Security;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Effects;

namespace MorganStanley.MSDotNet.MSGui.Controls.BarMenu
{
    [DesignTimeVisible(false)]
    public class OuterGlowDecorator : Decorator
    {
        // Fields
        public static readonly DependencyProperty GlowColorProperty = DependencyProperty.Register("GlowColor",
                                                                                                  typeof (Color),
                                                                                                  typeof (
                                                                                                      OuterGlowDecorator
                                                                                                      ),
                                                                                                  new FrameworkPropertyMetadata
                                                                                                      (Colors.White,
                                                                                                       new PropertyChangedCallback
                                                                                                           (OuterGlowDecorator
                                                                                                                .OnGlowSettingsChanged)));

        public static readonly DependencyProperty GlowOpacityProperty = DependencyProperty.Register("GlowOpacity",
                                                                                                    typeof (double),
                                                                                                    typeof (
                                                                                                        OuterGlowDecorator
                                                                                                        ),
                                                                                                    new FrameworkPropertyMetadata
                                                                                                        (0.6,
                                                                                                         new PropertyChangedCallback
                                                                                                             (OuterGlowDecorator
                                                                                                                  .OnGlowSettingsChanged)));

        public static readonly DependencyProperty IsGlowVisibleProperty = DependencyProperty.Register("IsGlowVisible",
                                                                                                      typeof (bool),
                                                                                                      typeof (
                                                                                                          OuterGlowDecorator
                                                                                                          ),
                                                                                                      new FrameworkPropertyMetadata
                                                                                                          (false,
                                                                                                           FrameworkPropertyMetadataOptions
                                                                                                               .AffectsRender,
                                                                                                           new PropertyChangedCallback
                                                                                                               (OuterGlowDecorator
                                                                                                                    .OnGlowSettingsChanged)));

        // Methods
        protected override Size ArrangeOverride(Size arrangeSize)
        {
            base.InvalidateVisual();
            return base.ArrangeOverride(arrangeSize);
        }

        private void DrawGlow(DrawingContext drawingContext)
        {
            UIElement child = this.Child;
            if (child != null)
            {
                Rect descendantBounds = VisualTreeHelper.GetDescendantBounds(child);
                if (!descendantBounds.IsEmpty)
                {
                    Size size = descendantBounds.Size;
                    if (((size.Width >= 1.0) && (size.Height >= 1.0)) &&
                        !(base.GetValue(UIElement.BitmapEffectProperty) is OuterGlowBitmapEffect))
                    {
                        Color glowColor = this.GlowColor;
                        if (glowColor.A != 0)
                        {
                            Rect rect2 = Rect.Inflate(descendantBounds, 6.0, 6.0);
                            size = rect2.Size;
                            double num = Math.Max((double) (this.GlowOpacity*0.9), (double) 0.0);
                            GradientStopCollection stops = new GradientStopCollection(2);
                            GradientStop stop = new GradientStop
                                {
                                    Offset = 0.0,
                                    Color = glowColor
                                };
                            stops.Add(stop);
                            GradientStop stop2 = new GradientStop
                                {
                                    Offset = 1.0,
                                    Color = Colors.Transparent
                                };
                            stops.Add(stop2);
                            LinearGradientBrush brush4 = new LinearGradientBrush
                                {
                                    Opacity = num,
                                    GradientStops = stops
                                };
                            LinearGradientBrush brush = brush4;
                            RadialGradientBrush brush5 = new RadialGradientBrush
                                {
                                    Opacity = num,
                                    GradientStops = stops,
                                    RadiusX = 1.0,
                                    RadiusY = 1.0
                                };
                            RadialGradientBrush brush2 = brush5;
                            double height = Math.Min((double) (size.Height/2.0), (double) 12.0);
                            double width = Math.Min((double) (size.Width/2.0), (double) 12.0);
                            double num4 = Math.Max((double) (rect2.Width - (width*2.0)), (double) 0.0);
                            double num5 = Math.Max((double) (rect2.Height - (height*2.0)), (double) 0.0);
                            Rect rectangle = new Rect(rect2.X, rect2.Y, width, height);
                            GuidelineSet guidelines = new GuidelineSet
                                {
                                    GuidelinesX = {rect2.Left, rect2.Left + width}
                                };
                            if (num4 > 0.0)
                            {
                                guidelines.GuidelinesX.Add(rect2.Right - width);
                            }
                            guidelines.GuidelinesX.Add(rect2.Right);
                            guidelines.GuidelinesY.Add(rect2.Top);
                            guidelines.GuidelinesY.Add(rect2.Top + height);
                            if (num5 > 0.0)
                            {
                                guidelines.GuidelinesY.Add(rect2.Bottom - height);
                            }
                            guidelines.GuidelinesY.Add(rect2.Bottom);
                            drawingContext.PushGuidelineSet(guidelines);
                            brush2.Center = brush2.GradientOrigin = new Point(1.0, 1.0);
                            drawingContext.DrawRectangle(brush2, null, rectangle);
                            if (num4 > 0.0)
                            {
                                brush.StartPoint = new Point(0.0, 1.0);
                                brush.EndPoint = new Point(0.0, 0.0);
                                drawingContext.DrawRectangle(brush, null,
                                                             new Rect(rect2.X + width, rect2.Y, num4, height));
                            }
                            brush2 = brush2.Clone();
                            brush2.Center = brush2.GradientOrigin = new Point(0.0, 1.0);
                            drawingContext.DrawRectangle(brush2, null, Rect.Offset(rectangle, size.Width - width, 0.0));
                            if (num4 > 0.0)
                            {
                                brush = brush.Clone();
                                brush.StartPoint = new Point(1.0, 0.0);
                                brush.EndPoint = new Point(0.0, 0.0);
                                drawingContext.DrawRectangle(brush, null,
                                                             new Rect(rect2.X, rect2.Y + height, width, num5));
                                if (num5 > 0.0)
                                {
                                    SolidColorBrush brush3 = new SolidColorBrush
                                        {
                                            Color = glowColor,
                                            Opacity = num
                                        };
                                    drawingContext.DrawRectangle(brush3, null,
                                                                 new Rect(rect2.X + width, rect2.Y + height, num4, num5));
                                }
                                brush = brush.Clone();
                                brush.StartPoint = new Point(0.0, 0.0);
                                brush.EndPoint = new Point(1.0, 0.0);
                                drawingContext.DrawRectangle(brush, null,
                                                             new Rect(rect2.Right - width, rect2.Y + height, width, num5));
                            }
                            brush2 = brush2.Clone();
                            brush2.Center = brush2.GradientOrigin = new Point(1.0, 0.0);
                            drawingContext.DrawRectangle(brush2, null, Rect.Offset(rectangle, 0.0, size.Height - height));
                            if (num4 > 0.0)
                            {
                                brush = brush.Clone();
                                brush.StartPoint = new Point(0.0, 0.0);
                                brush.EndPoint = new Point(0.0, 1.0);
                                drawingContext.DrawRectangle(brush, null,
                                                             new Rect(rect2.X + width, rect2.Bottom - height,
                                                                      Math.Max((double) (rect2.Width - (width*2.0)),
                                                                               (double) 0.0), height));
                            }
                            brush2 = brush2.Clone();
                            brush2.Center = brush2.GradientOrigin = new Point(0.0, 0.0);
                            drawingContext.DrawRectangle(brush2, null,
                                                         Rect.Offset(rectangle, size.Width - width, size.Height - height));
                            drawingContext.Pop();
                        }
                    }
                }
            }
        }

        private static void OnGlowSettingsChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as OuterGlowDecorator).VerifyOuterGlowBitmapEffect();
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            if (this.IsGlowVisible)
            {
                this.DrawGlow(drawingContext);
            }
            base.OnRender(drawingContext);
        }

        private void VerifyOuterGlowBitmapEffect()
        {
            if (!this.IsGlowVisible)
            {
                base.ClearValue(UIElement.BitmapEffectProperty);
            }
            else if ((Environment.Version.Major < 4) && !BrowserInteropHelper.IsBrowserHosted)
            {
                try
                {
                    OuterGlowBitmapEffect effect = new OuterGlowBitmapEffect
                        {
                            Opacity = this.GlowOpacity,
                            GlowColor = this.GlowColor
                        };
                    base.SetValue(UIElement.BitmapEffectProperty, effect);
                }
                catch (SecurityException)
                {
                }
            }
        }

        // Properties
        [Bindable(true)]
        public Color GlowColor
        {
            get { return (Color) base.GetValue(GlowColorProperty); }
            set { base.SetValue(GlowColorProperty, value); }
        }

        [Category("Behavior"), Description("Returns or sets the opacity used for the glow."), Bindable(true)]
        public double GlowOpacity
        {
            get { return (double) base.GetValue(GlowOpacityProperty); }
            set { base.SetValue(GlowOpacityProperty, value); }
        }

        [Bindable(true)]
        public bool IsGlowVisible
        {
            get { return (bool) base.GetValue(IsGlowVisibleProperty); }
            set { base.SetValue(IsGlowVisibleProperty, value); }
        }
    }

}
