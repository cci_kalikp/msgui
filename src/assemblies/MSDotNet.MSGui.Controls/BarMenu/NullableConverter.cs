﻿using System;
using System.ComponentModel;

namespace MorganStanley.MSDotNet.MSGui.Controls.BarMenu
{
    public class NullableConverter<T> : NullableConverter where T : struct
    {
        // Fields
        private static readonly Type _nullableType;

        // Methods
        static NullableConverter()
        {
            NullableConverter<T>._nullableType = (Nullable.GetUnderlyingType(typeof(T)) == null) ? typeof(T?) : typeof(T);
        }

        public NullableConverter()
            : base(NullableConverter<T>._nullableType)
        {
        }
    }


}
