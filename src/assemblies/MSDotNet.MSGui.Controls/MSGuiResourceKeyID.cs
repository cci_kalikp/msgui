﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Controls/MSGuiResourceKeyID.cs#11 $
// $Change: 898612 $
// $DateTime: 2014/09/28 23:36:02 $
// $Author: caijin $

namespace MorganStanley.MSDotNet.MSGui.Controls
{
  internal enum MSGuiBasicBrushID
  {
    WindowBackgroundBrush,
    LaunchBarBackgroundBrush,
    WindowHeaderBrush,
    InactiveWindowHeaderBrush,
    ControlBackgroundBrush,   
    PanelBackgroundBrush,
    TitlesBrush,
    InactiveTitlesBrush,
    PrimaryLabelsBrush,
    SecondaryLabelsBrush,
    TextFillBrush,
    LaunchBarTitleBrush,
    ButtonBackgroundBrush,
    DisabledButtonBackgroundBrush,    
    TextInputBackgroundBrush,
    DisabledTextInputBackgroundBrush,
    BorderBrush,
    InactiveBorderBrush,
    HighlightBorderBrush,
	InvertedTextFillBrush,
    PanelBackgroundSolidBrush,
    LaunchBarBorderBrush,
    LauncherBarSeparatorBrush,
    WindowCenterBorderBrush,
    WindowInnerBorderBrush,
    TileItemBorderBrush
  }
    internal enum MSGuiMiscBrushID
    {
        White,
        WhiteBackground,
        Black,
        BlackBackground,
        Grey,
        GreyBackground,
        Red,
        RedBackground,
        Orange,
        OrangeBackground,
        Yellow,
        YellowBackground,
        Green,
        GreenBackground,
        Blue,
        BlueBackground,
        Magenta,
        MagentaBackground,
        Brown,
        BrownBackground, 
    }

    internal enum MSGuiCaptionButtonBrushID
    {
        CaptionButtonHoverBorderDark,
        CaptionButtonHoverBorderLight,
        CaptionButtonHoverCenter,
        CaptionButtonHoverOverlay,
        CaptionButtonPressedBorderDark,
        CaptionButtonPressedBorderLight,
        CaptionButtonPressedCenter,
        CaptionButtonPressedBottomOverlay,
        CaptionButtonPressedTopOverlay,
        CaptionButtonIconMinimizeNormalForeground,
        CaptionButtonIconMinimizeNormalBackground,
        CaptionButtonIconMaxCloseNormalBackground,
        CaptionButtonIconMaxCloseNormalForeground,
        CaptionButtonIconMaxCloseNormalDisabledForeground,
        CaptionButtonIconMaxCloseInactiveDisabledForeground,
        CaptionButtonIconMinimizeHoverForeground,
        CaptionButtonIconMinimizeHoverBackground,
        CaptionButtonIconMaxCloseHoverBackground,
        CaptionButtonIconMaxCloseHoverForeground,
        CaptionButtonIconMinimizeInactiveForeground,
        CaptionButtonIconMaxCloseInactiveForeground
    }
}
