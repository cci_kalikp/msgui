//Copyright (c) Microsoft Corporation.  All rights reserved.
using System;
using System.Runtime.InteropServices;
using System.Text;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop.Win7;

namespace MorganStanley.Desktop.GUI.Controls.TaskDialogInterop.Win7
{
    /// <summary>
    /// Internal class containing most native interop declarations used
    /// throughout the library.
    /// Functions that are not performance intensive belong in this class.
    /// </summary>
 
    internal static class NativeMethods
    {
        #region General Definitions

        // Various helpers for forcing binding to proper 
        // version of Comctl32 (v6).
        [DllImport(ExternDll.Kernel32, SetLastError = true,
        ThrowOnUnmappableChar = true, BestFitMapping = false)]
        internal static extern IntPtr LoadLibrary(
             [MarshalAs(UnmanagedType.LPStr)] string lpFileName);

        [DllImport(ExternDll.Kernel32, SetLastError = true,
            ThrowOnUnmappableChar = true, BestFitMapping = false)]
        internal static extern IntPtr GetProcAddress(
            IntPtr hModule,
            [MarshalAs(UnmanagedType.LPStr)] string lpProcName);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool DeleteObject(IntPtr graphicsObjectHandle);

        #endregion

        #region TaskDialog Definitions

        [DllImport(ExternDll.ComCtl32, CharSet = CharSet.Auto, 
            SetLastError = true)]
        internal static extern HRESULT TaskDialog(
            IntPtr hwndParent,
            IntPtr hInstance,
            [MarshalAs(UnmanagedType.LPWStr)] string pszWindowtitle,
            [MarshalAs(UnmanagedType.LPWStr)] string pszMainInstruction,
            [MarshalAs(UnmanagedType.LPWStr)] string pszContent,
            SafeNativeMethods.TASKDIALOG_COMMON_BUTTON_FLAGS dwCommonButtons,
            [MarshalAs(UnmanagedType.LPWStr)]string pszIcon,
            [In, Out] ref int pnButton);

        [DllImport(ExternDll.ComCtl32, CharSet = CharSet.Auto, 
            SetLastError = true)]
        internal static extern HRESULT TaskDialogIndirect(
            [In] SafeNativeMethods.TASKDIALOGCONFIG pTaskConfig,
            [Out] out int pnButton,
            [Out] out int pnRadioButton,
            [MarshalAs(UnmanagedType.Bool)][Out] out bool pVerificationFlagChecked);

        internal delegate HRESULT TDIDelegate(
            [In] SafeNativeMethods.TASKDIALOGCONFIG pTaskConfig,
            [Out] out int pnButton,
            [Out] out int pnRadioButton,
            [Out] out bool pVerificationFlagChecked);

        
        #endregion
        
    }

}

