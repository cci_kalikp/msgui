//Copyright (c) Microsoft Corporation.  All rights reserved.
using System;
using System.Collections.Generic;
using System.Text;

namespace MorganStanley.Desktop.GUI.Controls.TaskDialogInterop.Win7
{
    /// <summary>
    /// Creates a marquee style progress bar.
    /// </summary>
    public class TaskDialogMarquee : TaskDialogBar
    {
        /// <summary>
        /// Creates a new instance of this class.
        /// </summary>
        public TaskDialogMarquee() { }
    }
}
