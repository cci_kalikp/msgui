//Copyright (c) Microsoft Corporation.  All rights reserved.
using System;
using System.Collections.Generic;
using System.Text;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop.Win7;

namespace MorganStanley.Desktop.GUI.Controls.TaskDialogInterop.Win7
{
    /// <summary>
    /// Sets the state of a task dialog progress bar.
    /// </summary>
    public enum TaskDialogProgressBarState
    {
        /// <summary>
        /// Normal state.
        /// </summary>
        Normal      = SafeNativeMethods.PBST.PBST_NORMAL,
        /// <summary>
        /// An error occurred.
        /// </summary>
        Error       = SafeNativeMethods.PBST.PBST_ERROR,
        /// <summary>
        /// The progress is paused.
        /// </summary>
        Paused      = SafeNativeMethods.PBST.PBST_PAUSED
    }
}
