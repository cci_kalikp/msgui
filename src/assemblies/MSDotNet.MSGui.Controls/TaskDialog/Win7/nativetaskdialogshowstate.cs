//Copyright (c) Microsoft Corporation.  All rights reserved.
using System;

namespace MorganStanley.Desktop.GUI.Controls.TaskDialogInterop.Win7
{
    internal enum NativeDialogShowState
    {
        PreShow,
        Showing,
        Closing, 
        Closed
    }
}
