﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Markup;
using System.Xaml;

namespace MorganStanley.MSDotNet.MSGui.Controls.TaskDialog
{
    /// <summary>
    /// To rename/alias existing resrouce
    /// </summary>
    class ResourceAlias : System.Windows.Markup.MarkupExtension
    {
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            var rootObjectProvider = (IRootObjectProvider)
                serviceProvider.GetService(typeof(IRootObjectProvider));
            if (rootObjectProvider == null) return null;
            var dictionary = rootObjectProvider.RootObject as IDictionary;
            if (dictionary == null) return null;
            object resource = dictionary[ResourceKey];
            if (resource != null) return resource;
            resource = Application.Current.TryFindResource(ResourceKey);
            if (resource != null) return resource;
            if (FallbackResourceKey != null)
            {
                return dictionary[FallbackResourceKey] ?? Application.Current.FindResource(FallbackResourceKey);
            }
            return null;
        }

        public object ResourceKey { get; set; }
        public object FallbackResourceKey { get; set; } 
    }
   
}
