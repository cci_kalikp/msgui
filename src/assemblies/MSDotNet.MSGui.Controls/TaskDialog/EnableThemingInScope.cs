﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Permissions;
using System.Text;
using System.Windows.Forms;

namespace MorganStanley.Desktop.GUI.Controls.TaskDialogInterop
{
  [SuppressUnmanagedCodeSecurity]
  internal class EnableThemingInScope : IDisposable
  {
    // Private data
    private IntPtr cookie;
    private static ACTCTX enableThemingActivationContext;
    private static IntPtr hActCtx;
    private static bool contextCreationSucceeded = false;

    public EnableThemingInScope(bool enable)
    {
      cookie = new IntPtr(-1);
      if (enable && OSFeature.Feature.IsPresent(OSFeature.Themes))
      {
        if (EnsureActivateContextCreated())
        {
          if (!ActivateActCtx(hActCtx, out cookie))
          {
            // Be sure cookie always zero if activation failed
            cookie = new IntPtr(-1);
          }
        }
      }
    }

    ~EnableThemingInScope()
    {
      Dispose(false);
    }

    void IDisposable.Dispose()
    {
      Dispose(true);
    }

    private void Dispose(bool disposing)
    {
      if (cookie == new IntPtr(-1)) return;

      if (DeactivateActCtx(0, cookie))
      {
        // deactivation succeeded...
        cookie = new IntPtr(-1);
      }
    }

    private static bool EnsureActivateContextCreated()
    {
      lock (typeof(EnableThemingInScope))
      {
        if (!contextCreationSucceeded)
        {
          // Pull manifest from the .NET Framework install
          // directory

          string assemblyLoc = null;

          var fiop = new FileIOPermission(PermissionState.None)
                                    {AllFiles = FileIOPermissionAccess.PathDiscovery};
          fiop.Assert();
          try
          {
            assemblyLoc = typeof(Object).Assembly.Location;
          }
          finally
          {
            CodeAccessPermission.RevertAssert();
          }

          string manifestLoc = null;
          string installDir = null;
          if (assemblyLoc != null)
          {
            installDir = Path.GetDirectoryName(assemblyLoc);
            const string manifestName = "XPThemes.manifest";
            manifestLoc = Path.Combine(installDir, manifestName);
          }

          if (manifestLoc != null && installDir != null)
          {
            // Set the lpAssemblyDirectory to the install
            // directory to prevent Win32 Side by Side from
            // looking for comctl32 in the application
            // directory, which could cause a bogus dll to be
            // placed there and open a security hole.
            // Note this will fail gracefully if file specified
            // by manifestLoc doesn't exist.
            enableThemingActivationContext = new ACTCTX
                                               {
                                                 cbSize = Marshal.SizeOf(typeof (ACTCTX)),
                                                 lpSource = manifestLoc,
                                                 lpAssemblyDirectory = installDir,
                                                 dwFlags = ACTCTX_FLAG_ASSEMBLY_DIRECTORY_VALID
                                               };


            hActCtx = CreateActCtx(ref enableThemingActivationContext);
            contextCreationSucceeded = (hActCtx != new IntPtr(-1));
          }
        }

        // If we return false, we'll try again on the next call into
        // EnsureActivateContextCreated(), which is fine.
        return contextCreationSucceeded;
      }
    }

    [DllImport("Kernel32.dll")]
    private extern static IntPtr CreateActCtx(ref ACTCTX actctx);
    [DllImport("Kernel32.dll", SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    private extern static bool ActivateActCtx(IntPtr hActCtx, out IntPtr lpCookie);
    [DllImport("Kernel32.dll", SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    private extern static bool DeactivateActCtx(int dwFlags, IntPtr lpCookie);

    private const int ACTCTX_FLAG_ASSEMBLY_DIRECTORY_VALID = 0x004;

    private struct ACTCTX
    {
      public int cbSize;
      public uint dwFlags;
      public string lpSource;
      public ushort wProcessorArchitecture;
      public ushort wLangId;
      public string lpAssemblyDirectory;
      public string lpResourceName;
      public string lpApplicationName;
    }
  }
}
