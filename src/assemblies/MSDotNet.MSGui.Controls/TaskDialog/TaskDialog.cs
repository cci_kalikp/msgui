﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Timers;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Media;
using System.Windows.Threading;

namespace MorganStanley.Desktop.GUI.Controls.TaskDialogInterop
{
	public partial class TaskDialog
	{
	    private static bool _isThemeAware = true;



		internal const int CommandButtonIDOffset = 2000;
		internal const int RadioButtonIDOffset = 1000;
		internal const int CustomButtonIDOffset = 500;

		/// <summary>
		/// Forces the WPF-based TaskDialog window instead of using native calls.
		/// </summary>
		public static bool ForceEmulationMode { get; set; }


        public static void UseClassicTheme()
        {
            _isThemeAware = false;
        }
		/// <summary>
		/// Occurs when a task dialog is about to show.
		/// </summary>
		/// <remarks>
		/// Use this event for both notification and modification of all task
		/// dialog showings. Changes made to the configuration options will be
		/// persisted.
		/// </remarks>
		public static event TaskDialogShowingEventHandler Showing;
		/// <summary>
		/// Occurs when a task dialog has been closed.
		/// </summary>
		public static new event TaskDialogClosedEventHandler Closed;

	    public static event EventHandler<FailedToDetermineOwnerEventArgs> FailedToDetermineOwner;

	    public static void InvokeFailedToDetermineOwner(FailedToDetermineOwnerEventArgs e)
	    {
	        EventHandler<FailedToDetermineOwnerEventArgs> handler = FailedToDetermineOwner;
	        if (handler != null) handler(null, e);
	    }

	    /// <summary>
		/// Displays a task dialog with the given configuration options.
		/// </summary>
		/// <param name="options">
		/// A <see cref="T:MorganStanley.Desktop.GUI.Controls.TaskDialogInterop.TaskDialogOptions"/> that specifies the
		/// configuration options for the dialog.
		/// </param>
		/// <param name="options">
		/// A <see cref="T:MorganStanley.Desktop.GUI.Controls.TaskDialogInterop.TaskDialogHandle"/> that can
		/// be used to control the dialog from a different thread.
		/// </param>/// 
		/// <returns>
		/// A <see cref="T:MorganStanley.Desktop.GUI.Controls.TaskDialogInterop.TaskDialogResult"/> value that specifies
		/// which button is clicked by the user.
		/// </returns>
		public static TaskDialogResult Show(TaskDialogOptions options, TaskDialogHandle handle=null)
		{
			TaskDialogResult result = null;

			// Make a copy since we'll let Showing event possibly modify them
			TaskDialogOptions configOptions = options;

			OnShowing(new TaskDialogShowingEventArgs(ref configOptions));

			//if (VistaTaskDialog.IsAvailableOnThisOS && !ForceEmulationMode)
			//{
			//  try
			//  {
			//    result = ShowTaskDialog(configOptions);
			//  }
			//  catch (EntryPointNotFoundException)
			//  {
			//    // This can happen on some machines, usually when running Vista/7 x64
			//    // When it does, we'll work around the issue by forcing emulated mode
			//    // http://www.codeproject.com/Messages/3257715/How-to-get-it-to-work-on-Windows-7-64-bit.aspx
			//    ForceEmulationMode = true;
			//    result = ShowEmulatedTaskDialog(configOptions);
			//  }
			//}
			//else
			//{
				result = ShowEmulatedTaskDialog(configOptions, handle, true);
			//}

			OnClosed(new TaskDialogClosedEventArgs(result));

			return result;
		}


        public static void ShowNoneModal(TaskDialogOptions options, TaskDialogHandle handle = null)
        {
            // Make a copy since we'll let Showing event possibly modify them
            TaskDialogOptions configOptions = options;

            OnShowing(new TaskDialogShowingEventArgs(ref configOptions));
            ShowEmulatedTaskDialog(configOptions, handle, false);
        }

		/// <summary>
		/// Displays a task dialog that has a message and that returns a result.
		/// </summary>
		/// <param name="owner">
		/// The <see cref="T:System.Windows.Window"/> that owns this dialog.
		/// </param>
		/// <param name="messageText">
		/// A <see cref="T:System.String"/> that specifies the text to display.
		/// </param>
		/// <returns>
		/// A <see cref="T:MorganStanley.Desktop.GUI.Controls.TaskDialogInterop.TaskDialogSimpleResult"/> value that
		/// specifies which button is clicked by the user.
		/// </returns>
		[Obsolete]
		public static TaskDialogSimpleResult ShowMessage(Window owner, string messageText)
		{
			TaskDialogOptions options = TaskDialogOptions.Default;

			options.Owner = owner;
			options.Content = messageText;
			options.CommonButtons = TaskDialogCommonButtons.Close;

			return Show(options).Result;
		}
		/// <summary>
		/// Displays a task dialog that has a message and that returns a result.
		/// </summary>
		/// <param name="owner">
		/// The <see cref="T:System.Windows.Window"/> that owns this dialog.
		/// </param>
		/// <param name="messageText">
		/// A <see cref="T:System.String"/> that specifies the text to display.
		/// </param>
		/// <param name="caption">
		/// A <see cref="T:System.String"/> that specifies the title bar
		/// caption to display.
		/// </param>
		/// <returns>
		/// A <see cref="T:MorganStanley.Desktop.GUI.Controls.TaskDialogInterop.TaskDialogSimpleResult"/> value that
		/// specifies which button is clicked by the user.
		/// </returns>
		[Obsolete]
		public static TaskDialogSimpleResult ShowMessage(Window owner, string messageText, string caption)
		{
			return ShowMessage(owner, messageText, caption, TaskDialogCommonButtons.Close);
		}
		/// <summary>
		/// Displays a task dialog that has a message and that returns a result.
		/// </summary>
		/// <param name="owner">
		/// The <see cref="T:System.Windows.Window"/> that owns this dialog.
		/// </param>
		/// <param name="messageText">
		/// A <see cref="T:System.String"/> that specifies the text to display.
		/// </param>
		/// <param name="caption">
		/// A <see cref="T:System.String"/> that specifies the title bar
		/// caption to display.
		/// </param>
		/// <param name="buttons">
		/// A <see cref="T:MorganStanley.Desktop.GUI.Controls.TaskDialogInterop.TaskDialogCommonButtons"/> value that
		/// specifies which button or buttons to display.
		/// </param>
		/// <returns>
		/// A <see cref="T:MorganStanley.Desktop.GUI.Controls.TaskDialogInterop.TaskDialogSimpleResult"/> value that
		/// specifies which button is clicked by the user.
		/// </returns>
		[Obsolete]
		public static TaskDialogSimpleResult ShowMessage(Window owner, string messageText, string caption, TaskDialogCommonButtons buttons)
		{
			return ShowMessage(owner, messageText, caption, buttons, VistaTaskDialogIcon.None);
		}
		/// <summary>
		/// Displays a task dialog that has a message and that returns a result.
		/// </summary>
		/// <param name="owner">
		/// The <see cref="T:System.Windows.Window"/> that owns this dialog.
		/// </param>
		/// <param name="messageText">
		/// A <see cref="T:System.String"/> that specifies the text to display.
		/// </param>
		/// <param name="caption">
		/// A <see cref="T:System.String"/> that specifies the title bar
		/// caption to display.
		/// </param>
		/// <param name="buttons">
		/// A <see cref="T:MorganStanley.Desktop.GUI.Controls.TaskDialogInterop.TaskDialogCommonButtons"/> value that
		/// specifies which button or buttons to display.
		/// </param>
		/// <param name="icon">
		/// A <see cref="T:MorganStanley.Desktop.GUI.Controls.TaskDialogInterop.VistaTaskDialogIcon"/> that specifies the
		/// icon to display.
		/// </param>
		/// <returns>
		/// A <see cref="T:MorganStanley.Desktop.GUI.Controls.TaskDialogInterop.TaskDialogSimpleResult"/> value that
		/// specifies which button is clicked by the user.
		/// </returns>
		[Obsolete]
		public static TaskDialogSimpleResult ShowMessage(Window owner, string messageText, string caption, TaskDialogCommonButtons buttons, VistaTaskDialogIcon icon)
		{
			TaskDialogOptions options = TaskDialogOptions.Default;

			options.Owner = owner;
			options.Title = caption;
			options.Content = messageText;
			options.CommonButtons = buttons;
			options.MainIcon = icon;

			return Show(options).Result;
		}
		/// <summary>
		/// Displays a task dialog that has a message and that returns a result.
		/// </summary>
		/// <param name="owner">
		/// The <see cref="T:System.Windows.Window"/> that owns this dialog.
		/// </param>
		/// <param name="title">
		/// A <see cref="T:System.String"/> that specifies the title bar
		/// caption to display.
		/// </param>
		/// <param name="mainInstruction">
		/// A <see cref="T:System.String"/> that specifies the main text to display.
		/// </param>
		/// <param name="content">
		/// A <see cref="T:System.String"/> that specifies the body text to display.
		/// </param>
		/// <param name="expandedInfo">
		/// A <see cref="T:System.String"/> that specifies the expanded text to display when toggled.
		/// </param>
		/// <param name="verificationText">
		/// A <see cref="T:System.String"/> that specifies the text to display next to a checkbox.
		/// </param>
		/// <param name="footerText">
		/// A <see cref="T:System.String"/> that specifies the footer text to display.
		/// </param>
		/// <param name="buttons">
		/// A <see cref="T:MorganStanley.Desktop.GUI.Controls.TaskDialogInterop.TaskDialogCommonButtons"/> value that
		/// specifies which button or buttons to display.
		/// </param>
		/// <param name="mainIcon">
		/// A <see cref="T:MorganStanley.Desktop.GUI.Controls.TaskDialogInterop.VistaTaskDialogIcon"/> that specifies the
		/// main icon to display.
		/// </param>
		/// <param name="footerIcon">
		/// A <see cref="T:MorganStanley.Desktop.GUI.Controls.TaskDialogInterop.VistaTaskDialogIcon"/> that specifies the
		/// footer icon to display.
		/// </param>
		/// <returns></returns>
		[Obsolete]
		public static TaskDialogSimpleResult ShowMessage(Window owner, string title, string mainInstruction, string content, string expandedInfo, string verificationText, string footerText, TaskDialogCommonButtons buttons, VistaTaskDialogIcon mainIcon, VistaTaskDialogIcon footerIcon)
		{
			TaskDialogOptions options = TaskDialogOptions.Default;

			if (owner != null)
				options.Owner = owner;
			if (!String.IsNullOrEmpty(title))
				options.Title = title;
			if (!String.IsNullOrEmpty(mainInstruction))
				options.MainInstruction = mainInstruction;
			if (!String.IsNullOrEmpty(content))
				options.Content = content;
			if (!String.IsNullOrEmpty(expandedInfo))
				options.ExpandedInfo = expandedInfo;
			if (!String.IsNullOrEmpty(verificationText))
				options.VerificationText = verificationText;
			if (!String.IsNullOrEmpty(footerText))
				options.FooterText = footerText;
			options.CommonButtons = buttons;
			options.MainIcon = mainIcon;
			options.FooterIcon = footerIcon;

			return Show(options).Result;
		}

		/// <summary>
		/// Displays a task dialog that has a message and that returns a result.
		/// </summary>
		/// <param name="owner">
		/// The <see cref="T:System.Windows.Window"/> that owns this dialog.
		/// </param>
		/// <param name="messageText">
		/// A <see cref="T:System.String"/> that specifies the text to display.
		/// </param>
		/// <returns>
		/// A <see cref="T:MorganStanley.Desktop.GUI.Controls.TaskDialogInterop.TaskDialogSimpleResult"/> value that
		/// specifies which button is clicked by the user.
		/// </returns>
		public static TaskDialogSimpleResult ShowMessage(string messageText, Window owner = null)
		{
			TaskDialogOptions options = TaskDialogOptions.Default;

			options.Owner = owner;
			options.Content = messageText;
			options.CommonButtons = TaskDialogCommonButtons.Close;

			return Show(options).Result;
		}

		/// <summary>
		/// Displays a task dialog that has a message and that returns a result.
		/// </summary>
		/// <param name="owner">
		/// The <see cref="T:System.Windows.Window"/> that owns this dialog.
		/// </param>
		/// <param name="messageText">
		/// A <see cref="T:System.String"/> that specifies the text to display.
		/// </param>
		/// <param name="caption">
		/// A <see cref="T:System.String"/> that specifies the title bar
		/// caption to display.
		/// </param>
		/// <returns>
		/// A <see cref="T:MorganStanley.Desktop.GUI.Controls.TaskDialogInterop.TaskDialogSimpleResult"/> value that
		/// specifies which button is clicked by the user.
		/// </returns>
		public static TaskDialogSimpleResult ShowMessage(string messageText, string caption, Window owner=null)
		{
			return ShowMessage(messageText, caption, TaskDialogCommonButtons.Close, owner);
		}
		/// <summary>
		/// Displays a task dialog that has a message and that returns a result.
		/// </summary>
		/// <param name="owner">
		/// The <see cref="T:System.Windows.Window"/> that owns this dialog.
		/// </param>
		/// <param name="messageText">
		/// A <see cref="T:System.String"/> that specifies the text to display.
		/// </param>
		/// <param name="caption">
		/// A <see cref="T:System.String"/> that specifies the title bar
		/// caption to display.
		/// </param>
		/// <param name="buttons">
		/// A <see cref="T:MorganStanley.Desktop.GUI.Controls.TaskDialogInterop.TaskDialogCommonButtons"/> value that
		/// specifies which button or buttons to display.
		/// </param>
		/// <returns>
		/// A <see cref="T:MorganStanley.Desktop.GUI.Controls.TaskDialogInterop.TaskDialogSimpleResult"/> value that
		/// specifies which button is clicked by the user.
		/// </returns>
		public static TaskDialogSimpleResult ShowMessage(string messageText, string caption, TaskDialogCommonButtons buttons, Window owner = null)
		{
			return ShowMessage(messageText, caption, buttons, VistaTaskDialogIcon.None, owner);
		}
		/// <summary>
		/// Displays a task dialog that has a message and that returns a result.
		/// </summary>
		/// <param name="owner">
		/// The <see cref="T:System.Windows.Window"/> that owns this dialog.
		/// </param>
		/// <param name="messageText">
		/// A <see cref="T:System.String"/> that specifies the text to display.
		/// </param>
		/// <param name="caption">
		/// A <see cref="T:System.String"/> that specifies the title bar
		/// caption to display.
		/// </param>
		/// <param name="buttons">
		/// A <see cref="T:MorganStanley.Desktop.GUI.Controls.TaskDialogInterop.TaskDialogCommonButtons"/> value that
		/// specifies which button or buttons to display.
		/// </param>
		/// <param name="icon">
		/// A <see cref="T:MorganStanley.Desktop.GUI.Controls.TaskDialogInterop.VistaTaskDialogIcon"/> that specifies the
		/// icon to display.
		/// </param>
		/// <returns>
		/// A <see cref="T:MorganStanley.Desktop.GUI.Controls.TaskDialogInterop.TaskDialogSimpleResult"/> value that
		/// specifies which button is clicked by the user.
		/// </returns>
		public static TaskDialogSimpleResult ShowMessage(string messageText, string caption, TaskDialogCommonButtons buttons, VistaTaskDialogIcon icon, Window owner = null)
		{
			TaskDialogOptions options = TaskDialogOptions.Default;

			options.Owner = owner;
			options.Title = caption;
			options.Content = messageText;
			options.CommonButtons = buttons;
			options.MainIcon = icon;

			return Show(options).Result;
		}
		/// <summary>
		/// Displays a task dialog that has a message and that returns a result.
		/// </summary>
		/// <param name="owner">
		/// The <see cref="T:System.Windows.Window"/> that owns this dialog.
		/// </param>
		/// <param name="title">
		/// A <see cref="T:System.String"/> that specifies the title bar
		/// caption to display.
		/// </param>
		/// <param name="mainInstruction">
		/// A <see cref="T:System.String"/> that specifies the main text to display.
		/// </param>
		/// <param name="content">
		/// A <see cref="T:System.String"/> that specifies the body text to display.
		/// </param>
		/// <param name="expandedInfo">
		/// A <see cref="T:System.String"/> that specifies the expanded text to display when toggled.
		/// </param>
		/// <param name="verificationText">
		/// A <see cref="T:System.String"/> that specifies the text to display next to a checkbox.
		/// </param>
		/// <param name="footerText">
		/// A <see cref="T:System.String"/> that specifies the footer text to display.
		/// </param>
		/// <param name="buttons">
		/// A <see cref="T:MorganStanley.Desktop.GUI.Controls.TaskDialogInterop.TaskDialogCommonButtons"/> value that
		/// specifies which button or buttons to display.
		/// </param>
		/// <param name="mainIcon">
		/// A <see cref="T:MorganStanley.Desktop.GUI.Controls.TaskDialogInterop.VistaTaskDialogIcon"/> that specifies the
		/// main icon to display.
		/// </param>
		/// <param name="footerIcon">
		/// A <see cref="T:MorganStanley.Desktop.GUI.Controls.TaskDialogInterop.VistaTaskDialogIcon"/> that specifies the
		/// footer icon to display.
		/// </param>
		/// <returns></returns>
		public static TaskDialogSimpleResult ShowMessage(string title, string mainInstruction, string content, string expandedInfo, string verificationText, string footerText, TaskDialogCommonButtons buttons, VistaTaskDialogIcon mainIcon, VistaTaskDialogIcon footerIcon, Window owner = null)
		{
			TaskDialogOptions options = TaskDialogOptions.Default;

			if (owner != null)
				options.Owner = owner;
			if (!String.IsNullOrEmpty(title))
				options.Title = title;
			if (!String.IsNullOrEmpty(mainInstruction))
				options.MainInstruction = mainInstruction;
			if (!String.IsNullOrEmpty(content))
				options.Content = content;
			if (!String.IsNullOrEmpty(expandedInfo))
				options.ExpandedInfo = expandedInfo;
			if (!String.IsNullOrEmpty(verificationText))
				options.VerificationText = verificationText;
			if (!String.IsNullOrEmpty(footerText))
				options.FooterText = footerText;
			options.CommonButtons = buttons;
			options.MainIcon = mainIcon;
			options.FooterIcon = footerIcon;

			return Show(options).Result;
		}
         
		internal static VistaTaskDialogCommonButtons ConvertCommonButtons(TaskDialogCommonButtons commonButtons)
		{
			VistaTaskDialogCommonButtons vtdCommonButtons = VistaTaskDialogCommonButtons.None;

			switch (commonButtons)
			{
				default:
				case TaskDialogCommonButtons.None:
                case TaskDialogCommonButtons.NoneAndNotClosable:
					vtdCommonButtons = VistaTaskDialogCommonButtons.None;
					break;
				case TaskDialogCommonButtons.Close:
					vtdCommonButtons = VistaTaskDialogCommonButtons.Close;
					break;
				case TaskDialogCommonButtons.OKCancel:
					vtdCommonButtons = VistaTaskDialogCommonButtons.OK | VistaTaskDialogCommonButtons.Cancel;
					break;
				case TaskDialogCommonButtons.RetryCancel:
					vtdCommonButtons = VistaTaskDialogCommonButtons.Retry | VistaTaskDialogCommonButtons.Cancel;
					break;
				case TaskDialogCommonButtons.YesNo:
					vtdCommonButtons = VistaTaskDialogCommonButtons.Yes | VistaTaskDialogCommonButtons.No;
					break;
				case TaskDialogCommonButtons.YesNoCancel:
					vtdCommonButtons = VistaTaskDialogCommonButtons.Yes | VistaTaskDialogCommonButtons.No | VistaTaskDialogCommonButtons.Cancel;
					break;

			}

			return vtdCommonButtons;
		}
		internal static TaskDialogButtonData ConvertCommonButton(VistaTaskDialogCommonButtons commonButton, System.Windows.Input.ICommand command = null, bool isDefault = false, bool isCancel = false)
		{
			int id = 0;

			switch (commonButton)
			{
				default:
				case VistaTaskDialogCommonButtons.None:
					id = (int)TaskDialogSimpleResult.None;
					break;
				case VistaTaskDialogCommonButtons.OK:
					id = (int)TaskDialogSimpleResult.Ok;
					break;
				case VistaTaskDialogCommonButtons.Yes:
					id = (int)TaskDialogSimpleResult.Yes;
					break;
				case VistaTaskDialogCommonButtons.No:
					id = (int)TaskDialogSimpleResult.No;
					break;
				case VistaTaskDialogCommonButtons.Cancel:
					id = (int)TaskDialogSimpleResult.Cancel;
					break;
				case VistaTaskDialogCommonButtons.Retry:
					id = (int)TaskDialogSimpleResult.Retry;
					break;
				case VistaTaskDialogCommonButtons.Close:
					id = (int)TaskDialogSimpleResult.Close;
					break;
			}

			return new TaskDialogButtonData(id, "_" + commonButton.ToString(), command, isDefault, isCancel);
		}
		internal static int GetButtonIdForCommonButton(TaskDialogCommonButtons commonButtons, int index)
		{
			int buttonId = 0;

			switch (commonButtons)
			{
				default:
				case TaskDialogCommonButtons.None:
				case TaskDialogCommonButtons.Close:
                case TaskDialogCommonButtons.NoneAndNotClosable:
					// We'll set to 0 even for Close, as it doesn't matter that we
					//get the value right since there is only one button anyway
					buttonId = 0;
					break;
				case TaskDialogCommonButtons.OKCancel:
					if (index == 0)
						buttonId = (int)VistaTaskDialogCommonButtons.OK;
					else if (index == 1)
						buttonId = (int)VistaTaskDialogCommonButtons.Cancel;
					else
						buttonId = 0;
					break;
				case TaskDialogCommonButtons.RetryCancel:
					if (index == 0)
						buttonId = (int)VistaTaskDialogCommonButtons.Retry;
					else if (index == 1)
						buttonId = (int)VistaTaskDialogCommonButtons.Cancel;
					else
						buttonId = 0;
					break;
				case TaskDialogCommonButtons.YesNo:
					if (index == 0)
						buttonId = (int)VistaTaskDialogCommonButtons.Yes;
					else if (index == 1)
						buttonId = (int)VistaTaskDialogCommonButtons.No;
					else
						buttonId = 0;
					break;
				case TaskDialogCommonButtons.YesNoCancel:
					if (index == 0)
						buttonId = (int)VistaTaskDialogCommonButtons.Yes;
					else if (index == 1)
						buttonId = (int)VistaTaskDialogCommonButtons.No;
					else if (index == 2)
						buttonId = (int)VistaTaskDialogCommonButtons.Cancel;
					else
						buttonId = 0;
					break;
			}

			return buttonId;
		}

		/// <summary>
		/// Raises the <see cref="E:Showing"/> event.
		/// </summary>
		/// <param name="e">The <see cref="TaskDialogInterop.TaskDialogShowingEventArgs"/> instance containing the event data.</param>
		private static void OnShowing(TaskDialogShowingEventArgs e)
		{
			if (Showing != null)
			{
				Showing(null, e);
			}
		}
		/// <summary>
		/// Raises the <see cref="E:Closed"/> event.
		/// </summary>
		/// <param name="e">The <see cref="TaskDialogInterop.TaskDialogClosedEventArgs"/> instance containing the event data.</param>
		private static void OnClosed(TaskDialogClosedEventArgs e)
		{
			if (Closed != null)
			{
				Closed(null, e);
			}
		}
		//private static TaskDialogResult ShowTaskDialog(TaskDialogOptions options)
		//{
		//  VistaTaskDialog vtd = new VistaTaskDialog();

		//  vtd.WindowTitle = options.Title;
		//  vtd.MainInstruction = options.MainInstruction;
		//  vtd.Content = options.Content;
		//  vtd.ExpandedInformation = options.ExpandedInfo;
		//  vtd.Footer = options.FooterText;

		//  if (options.CommandButtons != null && options.CommandButtons.Length > 0)
		//  {
		//    List<VistaTaskDialogButton> lst = new List<VistaTaskDialogButton>();
		//    for (int i = 0; i < options.CommandButtons.Length; i++)
		//    {
		//      try
		//      {
		//        VistaTaskDialogButton button = new VistaTaskDialogButton();
		//        button.ButtonId = CommandButtonIDOffset + i;
		//        button.ButtonText = options.CommandButtons[i];
		//        lst.Add(button);
		//      }
		//      catch (FormatException)
		//      {
		//      }
		//    }
		//    vtd.Buttons = lst.ToArray();
		//    if (options.DefaultButtonIndex.HasValue
		//      && options.DefaultButtonIndex >= 0
		//      && options.DefaultButtonIndex.Value < vtd.Buttons.Length)
		//      vtd.DefaultButton = vtd.Buttons[options.DefaultButtonIndex.Value].ButtonId;
		//  }
		//  else if (options.RadioButtons != null && options.RadioButtons.Length > 0)
		//  {
		//    List<VistaTaskDialogButton> lst = new List<VistaTaskDialogButton>();
		//    for (int i = 0; i < options.RadioButtons.Length; i++)
		//    {
		//      try
		//      {
		//        VistaTaskDialogButton button = new VistaTaskDialogButton();
		//        button.ButtonId = RadioButtonIDOffset + i;
		//        button.ButtonText = options.RadioButtons[i];
		//        lst.Add(button);
		//      }
		//      catch (FormatException)
		//      {
		//      }
		//    }
		//    vtd.RadioButtons = lst.ToArray();
		//    vtd.NoDefaultRadioButton = (!options.DefaultButtonIndex.HasValue || options.DefaultButtonIndex.Value == -1);
		//    if (options.DefaultButtonIndex.HasValue
		//      && options.DefaultButtonIndex >= 0
		//      && options.DefaultButtonIndex.Value < vtd.RadioButtons.Length)
		//      vtd.DefaultButton = vtd.RadioButtons[options.DefaultButtonIndex.Value].ButtonId;
		//  }

		//  bool hasCustomCancel = false;

		//  if (options.CustomButtons != null && options.CustomButtons.Length > 0)
		//  {
		//    List<VistaTaskDialogButton> lst = new List<VistaTaskDialogButton>();
		//    for (int i = 0; i < options.CustomButtons.Length; i++)
		//    {
		//      try
		//      {
		//        VistaTaskDialogButton button = new VistaTaskDialogButton();
		//        button.ButtonId = CustomButtonIDOffset + i;
		//        button.ButtonText = options.CustomButtons[i];

		//        if (!hasCustomCancel)
		//        {
		//          hasCustomCancel =
		//            (button.ButtonText == "Close"
		//            || button.ButtonText == "Cancel");
		//        }

		//        lst.Add(button);
		//      }
		//      catch (FormatException)
		//      {
		//      }
		//    }

		//    vtd.Buttons = lst.ToArray();
		//    if (options.DefaultButtonIndex.HasValue
		//      && options.DefaultButtonIndex.Value >= 0
		//      && options.DefaultButtonIndex.Value < vtd.Buttons.Length)
		//      vtd.DefaultButton = vtd.Buttons[options.DefaultButtonIndex.Value].ButtonId;
		//    vtd.CommonButtons = VistaTaskDialogCommonButtons.None;
		//  }
		//  else
		//  {
		//    vtd.CommonButtons = ConvertCommonButtons(options.CommonButtons);

		//    if (options.DefaultButtonIndex.HasValue
		//      && options.DefaultButtonIndex >= 0)
		//      vtd.DefaultButton = GetButtonIdForCommonButton(options.CommonButtons, options.DefaultButtonIndex.Value);
		//  }

		//  vtd.MainIcon = options.MainIcon;
		//  vtd.FooterIcon = options.FooterIcon;
		//  vtd.EnableHyperlinks = false;
		//  vtd.ShowProgressBar = false;
		//  vtd.AllowDialogCancellation =
		//    (options.AllowDialogCancellation
		//    || hasCustomCancel
		//    || options.CommonButtons == TaskDialogCommonButtons.Close
		//    || options.CommonButtons == TaskDialogCommonButtons.OKCancel
		//    || options.CommonButtons == TaskDialogCommonButtons.YesNoCancel);
		//  vtd.CallbackTimer = false;
		//  vtd.ExpandedByDefault = false;
		//  vtd.ExpandFooterArea = false;
		//  vtd.PositionRelativeToWindow = true;
		//  vtd.RightToLeftLayout = false;
		//  vtd.NoDefaultRadioButton = false;
		//  vtd.CanBeMinimized = false;
		//  vtd.ShowMarqueeProgressBar = false;
		//  vtd.UseCommandLinks = (options.CommandButtons != null && options.CommandButtons.Length > 0);
		//  vtd.UseCommandLinksNoIcon = false;
		//  vtd.VerificationText = options.VerificationText;
		//  vtd.VerificationFlagChecked = false;
		//  vtd.ExpandedControlText = "Hide details";
		//  vtd.CollapsedControlText = "Show details";
		//  vtd.Callback = null;

		//  TaskDialogResult result = null;
		//  int diagResult = 0;
		//  TaskDialogSimpleResult simpResult = TaskDialogSimpleResult.None;
		//  bool verificationChecked = false;
		//  int radioButtonResult = -1;
		//  int? commandButtonResult = null;
		//  int? customButtonResult = null;

		//  diagResult = vtd.Show((vtd.CanBeMinimized ? null : options.Owner), out verificationChecked, out radioButtonResult);

		//  if (diagResult >= CommandButtonIDOffset)
		//  {
		//    simpResult = TaskDialogSimpleResult.Command;
		//    commandButtonResult = diagResult - CommandButtonIDOffset;
		//  }
		//  else if (radioButtonResult >= RadioButtonIDOffset)
		//  {
		//    simpResult = TaskDialogSimpleResult.Radio;
		//    radioButtonResult -= RadioButtonIDOffset;
		//  }
		//  else if (diagResult >= CustomButtonIDOffset)
		//  {
		//    simpResult = TaskDialogSimpleResult.Custom;
		//    customButtonResult = diagResult - CustomButtonIDOffset;
		//  }
		//  else
		//  {
		//    simpResult = (TaskDialogSimpleResult)diagResult;
		//  }

		//  result = new TaskDialogResult(
		//    simpResult,
		//    (String.IsNullOrEmpty(options.VerificationText) ? null : (bool?)verificationChecked),
		//    ((options.RadioButtons == null || options.RadioButtons.Length == 0) ? null : (int?)radioButtonResult),
		//    ((options.CommandButtons == null || options.CommandButtons.Length == 0) ? null : commandButtonResult),
		//    ((options.CustomButtons == null || options.CustomButtons.Length == 0) ? null : customButtonResult));

		//  return result;
		//}
		private static TaskDialogResult ShowEmulatedTaskDialog(TaskDialogOptions options, TaskDialogHandle handle, bool modal)
		{
            // firstly, determine the owner
		    Window ownerCandidate = null;
            if (options.Owner != null)
            {
                ownerCandidate = options.Owner;
            }
            else
            {
                if (!options.ForceNullOwner)
                {
                    Application.Current.Dispatcher.Invoke(
                        new Action(() =>
                            {
                                ownerCandidate =
                                    Application.Current.Windows.Cast<Window>().FirstOrDefault(x => x.IsActive) ??
                                    Application.Current.Windows.Cast<Window>()
                                               .FirstOrDefault(x => x.Visibility == Visibility.Visible);
                            }));
                    if (ownerCandidate == null)
                    {
                        var newOwnerEventArgs = new FailedToDetermineOwnerEventArgs();
                        InvokeFailedToDetermineOwner(newOwnerEventArgs);
                        ownerCandidate = newOwnerEventArgs.Owner;
                    }
                }
            }

            // the owner might have different thread affinity (example: splash screen)
		    var dispatcher = ownerCandidate == null
		                         ? Application.Current.Dispatcher
		                         : ownerCandidate.Dispatcher;

		    TaskDialogResult result = null;
            dispatcher.Invoke(
                new Action(() => DoShowEmulatedTaskDialog(ownerCandidate, options, handle, modal, out result)));

			return result;
		}


        private static void DoEvents()
        {
            DispatcherFrame frame = new DispatcherFrame();
            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background,
                new DispatcherOperationCallback(f_ =>
                {
                    ((DispatcherFrame)f_).Continue = false;
                    return null;
                }), frame);
            Dispatcher.PushFrame(frame);
        }

	    private const string splitter =
	        "==============================================================================================";
        private static void FallbackToClassicMessageBox(Exception exception_, TaskDialogViewModel originalTaskSetting_)
        {
            string message = "Exception Occurred when showing TaskDialog:" + Environment.NewLine +
                             exception_.ToString();
            if (!string.IsNullOrEmpty(originalTaskSetting_.ExpandedInfo))
            {
                message += Environment.NewLine + splitter + Environment.NewLine +
                           "Original Message:" + Environment.NewLine + originalTaskSetting_.ExpandedInfo;
            }
            else if (!string.IsNullOrEmpty(originalTaskSetting_.Content))
            {
                message += Environment.NewLine + splitter + Environment.NewLine +
                        "Original Message:" + Environment.NewLine + originalTaskSetting_.Content;
             
            }
            MessageBox.Show(message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);  
        }

	    private static void DoShowEmulatedTaskDialog(Window ownerCandidate, TaskDialogOptions options, TaskDialogHandle handle, bool modal_, out TaskDialogResult result)
	    {
            TaskDialogViewModel tdvm = new TaskDialogViewModel(options, handle);
	        result = new TaskDialogResult(TaskDialogSimpleResult.None);
	        TaskDialog td = null;
	        try
	        {
               td = new TaskDialog();
               if (options.CommonButtons == TaskDialogCommonButtons.NoneAndNotClosable)
               {
                   CancelEventHandler closingHandler = null;
                   td.Closing += closingHandler = (sender_, args_) =>
                       {
                           if (!td.internalClosing)
                           {
                               args_.Cancel = true;
                           }
                           else
                           {
                               td.Closing -= closingHandler;
                           }
                       };

               }

                #region For CUIT
                AutomationProperties.SetAutomationId(td, "MSDesktop_TaskDialog");
                #endregion


            }
	        catch (Exception ex)
	        {
                FallbackToClassicMessageBox(ex, tdvm);
                return;
	        }

	        Exception exceptionInShowingTaskDialog = null;
            AutoResetEvent exceptionHandledEvent = new AutoResetEvent(false);
            td.Dispatcher.BeginInvoke(DispatcherPriority.Loaded, new Action(() =>
            {
                Exception copy = exceptionInShowingTaskDialog;
                exceptionInShowingTaskDialog = null;
                exceptionHandledEvent.Set();
                if (copy != null)
                {
                    FallbackToClassicMessageBox(copy, tdvm);
                }
            }));

	        try
	        {
                if (ownerCandidate != null && ownerCandidate.IsLoaded)
                {
                    td.Owner = ownerCandidate;
                }
                 
                td.DataContext = tdvm;
                if (handle != null)
                {
                    handle.Dialog = td;
                    if (options.ProgressBarInitialValue.HasValue)
                        handle.ProgressBarCurrentValue = options.ProgressBarInitialValue;
                }

                if (modal_)
                {
                    //any exception occurred when showing the taskdialog would in another thread, so won't be able to catch it here
                    td.ShowDialog();

                    // Fixing missing password binding
                    tdvm.UserPasswordInput = td.UserPasswordInput.Password;

                    int diagResult = -1;
                    TaskDialogSimpleResult simpResult = TaskDialogSimpleResult.None;
                    bool verificationChecked = false;
                    int radioButtonResult = -1;
                    int? commandButtonResult = null;
                    int? customButtonResult = null;
                    string userInputResult = null;
                    string userPasswordInputResult = null;

                    diagResult = tdvm.DialogResult;
                    verificationChecked = tdvm.VerificationChecked;
                    if (options.UserInputEnabled)
                        userInputResult = tdvm.UserInput;
                    if (options.UserPasswordInputEnabled)
                        userPasswordInputResult = tdvm.UserPasswordInput;

                    if (diagResult >= CommandButtonIDOffset)
                    {
                        simpResult = TaskDialogSimpleResult.Command;
                        commandButtonResult = diagResult - CommandButtonIDOffset;
                    }
                    else if (diagResult >= RadioButtonIDOffset)
                    {
                        simpResult = TaskDialogSimpleResult.Radio;
                        radioButtonResult = diagResult - RadioButtonIDOffset;
                    }
                    else if (diagResult >= CustomButtonIDOffset)
                    {
                        simpResult = TaskDialogSimpleResult.Custom;
                        customButtonResult = diagResult - CustomButtonIDOffset;
                    }
                    // This occurs usually when the red X button is clicked
                    else if (diagResult == -1)
                    {
                        simpResult = TaskDialogSimpleResult.Cancel;
                    }
                    else
                    {
                        simpResult = (TaskDialogSimpleResult)diagResult;
                    }

                    result = new TaskDialogResult(
                        simpResult,
                        (String.IsNullOrEmpty(options.VerificationText) ? null : (bool?)verificationChecked),
                        ((options.RadioButtons == null || options.RadioButtons.Length == 0) ? null : (int?)radioButtonResult),
                        ((options.CommandButtons == null || options.CommandButtons.Length == 0) ? null : commandButtonResult),
                        ((options.CustomButtons == null || options.CustomButtons.Length == 0) ? null : customButtonResult),
                        options.UserInputEnabled ? userInputResult : null,
                        options.UserPasswordInputEnabled ? userPasswordInputResult : null);

                }
                else
                {
                    td.Show();
                }

	        }
	        catch (Exception ex)
	        {
	            exceptionInShowingTaskDialog = ex;
                DoEvents();
                exceptionHandledEvent.WaitOne(100);
                if (exceptionInShowingTaskDialog != null)
                {
                    throw exceptionInShowingTaskDialog;
                }
 
	        } 
	        
	    }

	    private bool internalClosing = false;
	    private bool closed = false;
        internal void InternalClose()
        {
            if (closed) return;
            internalClosing = true;
            try
            {
                this.Close();
                closed = true;
            }
            finally
            {
                internalClosing = false;
            }
        }
	}

    public class FailedToDetermineOwnerEventArgs : EventArgs
    {
        public Window Owner { get; set; }
    }
}
