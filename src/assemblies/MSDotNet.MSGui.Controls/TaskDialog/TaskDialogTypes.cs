﻿using System;
using System.Collections;
using System.Windows;
using System.Collections.Generic;
using System.ComponentModel;

namespace MorganStanley.Desktop.GUI.Controls.TaskDialogInterop
{
	/// <summary>
	/// Specifies the standard buttons that are displayed on a task dialog.
	/// </summary>
	public enum TaskDialogCommonButtons
	{
		/// <summary>
		/// The message box displays no buttons.
		/// </summary>
		None = 0,
		/// <summary>
		/// The message box displays a Close button.
		/// </summary>
		Close = 1,
		/// <summary>
		/// The message box displays Yes and No buttons.
		/// </summary>
		YesNo = 2,
		/// <summary>
		/// The message box displays Yes, No, and Cancel buttons.
		/// </summary>
		YesNoCancel = 3,
		/// <summary>
		/// The message box displays OK and Cancel buttons.
		/// </summary>
		OKCancel = 4,
		/// <summary>
		/// The message box displays Retry and Cancel buttons.
		/// </summary>
		RetryCancel = 5,
        /// <summary>
        /// The message box contains no button and is not closable
        /// </summary>
        NoneAndNotClosable = 6,
	}
	/// <summary>
	/// Defines configuration options for showing a task dialog.
	/// </summary>
	public struct TaskDialogOptions
	{
		/// <summary>
		/// The default <see cref="T:TaskDialogOptions"/> to be used
		/// by all new <see cref="T:TaskDialog"/>s.
		/// </summary>
		/// <remarks>
		/// Use this to make application-wide defaults, such as for
		/// the caption.
		/// </remarks>
		public static TaskDialogOptions Default;

		/// <summary>
		/// The owner window of the task dialog box. If set to null, will default to currently active window.
		/// If you want to force null, please use <see cref="P:ForceNullOwner">ForceNullOwner</see> property.
		/// </summary>
		public System.Windows.Window Owner;
		/// <summary>
		/// Will ignore <see cref="P:Owner">Owner</see> property if set to true.
		/// </summary>
		public bool ForceNullOwner;
		/// <summary>
		/// Caption of the window.
		/// </summary>
		public string Title;
		/// <summary>
		/// A large 32x32 icon that signifies the purpose of the dialog.
		/// </summary>
		public VistaTaskDialogIcon MainIcon;
		/// <summary>
		/// Principal text.
		/// </summary>
		public string MainInstruction;
		/// <summary>
		/// Supplemental text that expands on the principal text.
		/// </summary>
		public string Content;
		/// <summary>
		/// Extra text that will be hidden by default.
		/// </summary>
		public string ExpandedInfo;
		/// <summary>
		/// Standard buttons.
		/// </summary>
		public TaskDialogCommonButtons CommonButtons;
		/// <summary>
		/// Application-defined options for the user.
		/// </summary>
		public string[] RadioButtons;
		/// <summary>
		/// Buttons that are not from the set of standard buttons. Use an
		/// ampersand to denote an access key.
		/// </summary>
		public string[] CustomButtons;
		/// <summary>
		/// Command link buttons.
		/// </summary>
		public string[] CommandButtons;
		/// <summary>
		/// Zero-based index of the button to have focus by default.
		/// </summary>
		public int? DefaultButtonIndex;
		/// <summary>
		/// Text accompanied by a checkbox, typically for user feedback such as
		/// Do-not-show-this-dialog-again options.
		/// </summary>
		public string VerificationText;
		/// <summary>
		/// A small 16x16 icon that signifies the purpose of the footer text.
		/// </summary>
		public VistaTaskDialogIcon FooterIcon;
		/// <summary>
		/// Additional footer text.
		/// </summary>
		public string FooterText;
		/// <summary>
		/// Indicates that the dialog should be able to be closed using Alt-F4,
		/// Escape, and the title bar's close button even if no cancel button
		/// is specified the CommonButtons.
		/// </summary>
		/// <remarks>
		/// You'll want to set this to true if you use CustomButtons and have
		/// a Cancel-like button in it.
		/// </remarks>
		public bool AllowDialogCancellation;
		/// <summary>
		/// By default, all custom buttons close the dialog. This property allows
		/// overriding this behaviour.
		/// </summary>
		public IDictionary<string, Action> CustomButtonActions;
		/// <summary>
		/// Progress bar minimum value. Ignored if ProgressBarInitialValue set to null.
		/// </summary>
		public int? ProgressBarMinimum;
		/// <summary>
		/// Progress bar maximum value. Ignored if ProgressBarInitialValue set to null.
		/// </summary>
		public int? ProgressBarMaximum;
		/// <summary>
		/// Progress bar initial value. Progress bar will not show if set to null.
		/// </summary>
		public int? ProgressBarInitialValue;
		/// <summary>
		/// Will display a TextBox if true.
		/// </summary>
		public bool UserInputEnabled;
        /// <summary>
        /// Will display a TextBox if true.
        /// </summary>
        public bool UserPasswordInputEnabled;
		/// <summary>
		/// Initial user input. Ignored if UserInputEnabled set to false.
		/// </summary>
		public string InitialUserInput;
	}
	/// <summary>
	/// Provides data for all task dialog buttons.
	/// </summary>
	public class TaskDialogButtonData
	{
		/// <summary>
		/// Gets the button's ID value to return when clicked.
		/// </summary>
		public int ID { get; private set; }
		/// <summary>
		/// Gets the button's text label.
		/// </summary>
		public string Text { get; private set; }
		/// <summary>
		/// Gets a value indicating whether or not the button should be the default.
		/// </summary>
		public bool IsDefault { get; private set; }
		/// <summary>
		/// Gets a value indicating whether or not the button should be a cancel.
		/// </summary>
		public bool IsCancel { get; private set; }
		/// <summary>
		/// Gets the button's command.
		/// </summary>
		public System.Windows.Input.ICommand Command { get; private set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="TaskDialogButtonData"/> class.
		/// </summary>
		public TaskDialogButtonData()
		{
		}
		/// <summary>
		/// Initializes a new instance of the <see cref="TaskDialogButtonData"/> struct.
		/// </summary>
		/// <param name="id">The id value for the button.</param>
		/// <param name="text">The text label.</param>
		/// <param name="command">The command to associate.</param>
		/// <param name="isDefault">Whether the button should be the default.</param>
		/// <param name="isCancel">Whether the button should be a cancel.</param>
		public TaskDialogButtonData(int id, string text, System.Windows.Input.ICommand command = null, bool isDefault = false, bool isCancel = false)
		{
			ID = id;
			Text = text;
			Command = command;
			IsDefault = isDefault;
			IsCancel = isCancel;
		}
	}
	/// <summary>
	/// Allows controlling the TaskDialog while it's showing.
	/// </summary>
	public class TaskDialogHandle : INotifyPropertyChanged
	{
		/// <summary>
		/// Use to set progress bar value.
		/// </summary>
		public int? ProgressBarCurrentValue
		{
			get { return progressBarCurrentValue; }
			set
			{
				if (value != progressBarCurrentValue)
				{
					progressBarCurrentValue = value;
					RaisePropertyChanged("ProgressBarCurrentValue");
				}
			}
		}

	    private string content;
	    public string Content
	    {
            get { return content; }
            set 
            { 
                content = value;
                RaisePropertyChanged("Content");
            }
	    }
		/// <summary>
		/// Indicates if dialog is showing.
		/// </summary>
		public bool DialogShowing
		{
			get { return Dialog != null; }
		}

		/// <summary>
		/// Closes the dialog.
		/// </summary>
		public void Close()
		{
			if (DialogShowing)
				Dialog.InternalClose();
		}

		public event PropertyChangedEventHandler PropertyChanged;

		internal TaskDialog Dialog { get; set; }
		
		void RaisePropertyChanged(string propertyName)
		{
			var handler = PropertyChanged;
			if (handler != null)
				handler(this, new PropertyChangedEventArgs(propertyName));
		}

		int? progressBarCurrentValue;
	}
}
