﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace MorganStanley.MSDotNet.MSGui.Controls.SearchBox
{
    public interface ISearchBoxViewModel:INotifyPropertyChanged 
    {
         
        bool EnableTypeaheadDropDown { get; set; }

        bool Searching { get; }

        string SearchText { get; set; }

        string WatermarkText { get; set; }

        bool SearchTextIsNotEmpty { get; }

        ICommand ClearSearchCommand { get; }

        bool ShowDropDown { get; set; }
         
        ISearchResultItem SelectedItem { get; set; }

        IList<ISearchResultItem> SearchResultItems
        {
            get;  
        }
         
    }
}
