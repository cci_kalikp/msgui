﻿namespace MorganStanley.MSDotNet.MSGui.Controls.SearchBox
{
    public abstract class SearchResultItemBase : ISearchResultItem
    {
        public string ItemName { get; set; }
        public object Item { get; set; }
        public string MatchType { get; set; }
        public string SearchText { get; set; }
        public abstract string ItemPath { get; } 
        public override string ToString()
        {
            return ItemPath;
        } 
    } 
}
