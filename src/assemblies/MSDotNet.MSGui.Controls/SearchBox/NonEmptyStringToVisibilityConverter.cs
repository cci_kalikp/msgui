﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace MorganStanley.MSDotNet.MSGui.Controls.SearchBox
{
 
    public class NonEmptyStringToVisibilityConverter : IValueConverter
    { 

        public object Convert(object value_, Type targetType_, object parameter_, CultureInfo culture_)
        {
            string str = value_ as String;
            return string.IsNullOrWhiteSpace(str) ? Visibility.Collapsed : Visibility.Visible;
        }
         
        public object ConvertBack(object value_, Type targetType_, object parameter_, CultureInfo culture_)
        {
            throw new NotImplementedException();
        }
    }
     
}
