﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace MorganStanley.MSDotNet.MSGui.Controls.SearchBox
{
    /// <summary>
    /// Interaction logic for SearchBox.xaml
    /// </summary>
    public partial class SearchBox
    { 
        public SearchBox()
        {
            InitializeComponent();
            EnterBox.PreviewMouseDown += new MouseButtonEventHandler(EnterBox_PreviewMouseDown);
            DropDownBox.MouseEnter += new MouseEventHandler(DropDownBox_MouseEnter);
            ItemsBox.PreviewMouseLeftButtonDown += new MouseButtonEventHandler(ItemsBox_PreviewMouseLeftButtonDown);
            ItemsBox.PreviewMouseMove += new MouseEventHandler(ItemsBox_PreviewMouseMove);
            this.Loaded += new RoutedEventHandler(SearchBox_Loaded);
        }

         

        public static readonly ComponentResourceKey InputStyleKey = new MSGuiResourceKey("InputStyle"); //MSDesktopOverlayTextBoxStyle
        public static readonly ComponentResourceKey InputBorderStyleKey = new MSGuiResourceKey("InputBorderStyle");
        public static readonly ComponentResourceKey DropDownBorderStyleKey = new MSGuiResourceKey("DropDownBorderStyle"); //DropDownBorderStyle
        public static readonly ComponentResourceKey DropDownListStyleKey = new MSGuiResourceKey("DropDownListStyle"); //DropDownListStyle
        public static readonly ComponentResourceKey ClearButtonStyleKey = new MSGuiResourceKey("ClearButtonStyle"); //ClearButtonStyle


        public bool EnableTypeaheadDropDown
        {
            get { return (bool)GetValue(EnableTypeaheadDropDownProperty); }
            set { SetValue(EnableTypeaheadDropDownProperty, value); }
        }

        // Using a DependencyProperty as the backing store for EnableTypeaheadDropDown.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty EnableTypeaheadDropDownProperty =
            DependencyProperty.Register("EnableTypeaheadDropDown", typeof(bool), typeof(SearchBox), new UIPropertyMetadata(false)); 

        void EnterBox_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (EnableTypeaheadDropDown && !string.IsNullOrWhiteSpace(EnterBox.Text) && !DropDownBox.IsOpen && ItemsBox.Items.Count > 0)
            {
                DropDownBox.IsOpen = true;
                HighlightItem(-1); 
                e.Handled = true;
            }
        }


        public static readonly DependencyProperty IsHighlightedProperty =
            DependencyProperty.RegisterAttached("IsHighlighted", typeof (bool), typeof (SearchBox), new PropertyMetadata(default(bool)));

        public static void SetIsHighlighted(UIElement element, bool value)
        {
            element.SetValue(IsHighlightedProperty, value);
        }

        public static bool GetIsHighlighted(UIElement element)
        {
            return (bool) element.GetValue(IsHighlightedProperty);
        }

        void SearchBox_Loaded(object sender, RoutedEventArgs e)
        {
            Window w = Window.GetWindow(this);
            w.SizeChanged += new SizeChangedEventHandler(w_SizeChanged);
            w.LocationChanged += new System.EventHandler(w_LocationChanged);
        }

        void w_LocationChanged(object sender, System.EventArgs e)
        {
            DropDownBox.IsOpen = false;
        }

        void w_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            DropDownBox.IsOpen = false;
        }

        protected override void OnLostFocus(RoutedEventArgs e)
        {
            DropDownBox.IsOpen = false;
            base.OnLostFocus(e);
        }
        protected override void OnLostKeyboardFocus(KeyboardFocusChangedEventArgs e)
        { 
            DropDownBox.IsOpen = false;
            base.OnLostKeyboardFocus(e);
        }

        private void ItemsBox_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (internalMouseMove || Keyboard.IsKeyDown(Key.Down) || Keyboard.IsKeyDown(Key.Up)) return; 
            var item = ItemsControl.ContainerFromElement(ItemsBox, e.OriginalSource as DependencyObject) as ListViewItem;
            if (item != null)
            {
                int index = ItemsBox.ItemContainerGenerator.IndexFromContainer(item);
                if (index >= 0)
                {
                    HighlightItem(index);
                }
            }
        }
         
        protected override void OnPreviewKeyDown(System.Windows.Input.KeyEventArgs e)
        {
            if (EnableTypeaheadDropDown && DropDownBox.IsOpen)
            {
                if (e.Key == Key.Down)
                {
                    if (highlightedIndex < ItemsBox.Items.Count - 1)
                    {
                        HighlightItem(++highlightedIndex);
                    }
                    else
                    {
                        int index = highlightedIndex;
                        HighlightItem(-1);
                        HighlightItem(index < 0 ? 0 : index);
                    } 
                    e.Handled = true;
                }
                else if (e.Key == Key.Up)
                {
                    if (highlightedIndex > 0)
                    {
                        HighlightItem(--highlightedIndex);
                    }
                    else
                    {
                        HighlightItem(-1);
                        HighlightItem(0);
                    }
                    e.Handled = true;
                }
                else if (e.Key == Key.Enter)
                {
                    if (highlightedIndex < 0) highlightedIndex = 0;
                    HighlightItem(highlightedIndex);
                    ItemsBox.SelectedIndex = highlightedIndex;
                }
            }
            base.OnPreviewKeyDown(e);
        }

        //always close the dropdown if an item is selected, even if the selected item is not changed
        void ItemsBox_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var item = ItemsControl.ContainerFromElement(ItemsBox, e.OriginalSource as DependencyObject) as ListViewItem;
            if (item != null)
            {
                item.IsSelected = false;
                item.IsSelected = true;
                DropDownBox.IsOpen = false;
            }
    
        }

        void DropDownBox_MouseEnter(object sender, MouseEventArgs e)
        {
            HighlightItem(-1);
        }
 
 
        private int highlightedIndex = -1;
        private bool internalMouseMove = false;
        private void HighlightItem(int index_)
        {
            highlightedIndex = index_;
            for (int i = 0; i < ItemsBox.Items.Count; i++)
            {
                var element = ItemsBox.ItemContainerGenerator.ContainerFromIndex(i) as UIElement;
                bool highlight = i == index_;
                if (highlight)
                {
                    internalMouseMove = true;
                    ItemsBox.ScrollIntoView(ItemsBox.Items[i]);
                    internalMouseMove = false;
                }
                SetIsHighlighted(element, highlight);
            }
        }
         
    }
}
