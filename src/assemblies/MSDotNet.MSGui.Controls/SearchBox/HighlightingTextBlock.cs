﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace MorganStanley.MSDotNet.MSGui.Controls.SearchBox
{
    /// <summary>
    /// If HighlightedText is not empty and is contained in the Text,
    /// then the HighlightedText will be highlighted using the HighlightForeground and HighlightBackground brush.
    /// </summary>
    public class HighlightingTextBlock : TextBlock
    {
        static HighlightingTextBlock()
        {
            ForegroundProperty.AddOwner(typeof (HighlightingTextBlock),
                                        new FrameworkPropertyMetadata(HighlightedChangedCallback));
            BackgroundProperty.AddOwner(typeof (HighlightingTextBlock),
                                        new FrameworkPropertyMetadata(HighlightedChangedCallback));
        }

        #region Dependency Properties

        public static readonly DependencyProperty HighlightedTextProperty =
            DependencyProperty.Register("HighlightedText", typeof (string), typeof (HighlightingTextBlock),
                                        new PropertyMetadata(string.Empty, HighlightedChangedCallback));

        public static readonly DependencyProperty HighlightForegroundProperty =
            DependencyProperty.Register("HighlightForeground", typeof (Brush), typeof (HighlightingTextBlock),
                                        new PropertyMetadata(new SolidColorBrush(Colors.Black),
                                                             HighlightedChangedCallback));

        public static readonly DependencyProperty HighlightBackgroundProperty =
            DependencyProperty.Register("HighlightBackground", typeof (Brush), typeof (HighlightingTextBlock),
                                        new PropertyMetadata(new SolidColorBrush(Colors.Yellow),
                                                             HighlightedChangedCallback));

        public static readonly DependencyProperty IgnoreCaseCheckForHighlightingTextProperty =
            DependencyProperty.Register("IgnoreCaseCheckForHighlightingText", typeof (bool),
                                        typeof (HighlightingTextBlock),
                                        new PropertyMetadata(true, HighlightedChangedCallback));

        public static readonly DependencyProperty HighlightFirstMatchOnlyProperty =
            DependencyProperty.Register("HighlightFirstMatchOnly", typeof (bool), typeof (HighlightingTextBlock),
                                        new PropertyMetadata(false, HighlightedChangedCallback));




        public string OriginalText
        {
            get { return (string)GetValue(OriginalTextProperty); }
            set { SetValue(OriginalTextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for OriginalText.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty OriginalTextProperty =
            DependencyProperty.Register("OriginalText", typeof(string), typeof(HighlightingTextBlock), new UIPropertyMetadata(string.Empty, HighlightedChangedCallback));

        
        public string Suffix
        {
            get { return (string)GetValue(SuffixProperty); }
            set { SetValue(SuffixProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Suffix.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SuffixProperty =
            DependencyProperty.Register("Suffix", typeof(string), typeof(HighlightingTextBlock), new UIPropertyMetadata(null, HighlightedChangedCallback));


        public string Prefix
        {
            get { return (string)GetValue(PrefixProperty); }
            set { SetValue(PrefixProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Suffix.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PrefixProperty =
            DependencyProperty.Register("Prefix", typeof(string), typeof(HighlightingTextBlock), new UIPropertyMetadata(null, HighlightedChangedCallback));


        /// <summary>
        /// Text to highlight
        /// </summary>
        public string HighlightedText
        {
            get { return (string) GetValue(HighlightedTextProperty); }
            set { SetValue(HighlightedTextProperty, value); }
        }

        /// <summary>
        /// Foreground brush for highlighted text
        /// Default : Black
        /// </summary>
        public Brush HighlightForeground
        {
            get { return (Brush) GetValue(HighlightForegroundProperty); }
            set { SetValue(HighlightForegroundProperty, value); }
        }

        /// <summary>
        /// Background brush for highlighted text
        /// Default : Yellow
        /// </summary>
        public Brush HighlightBackground
        {
            get { return (Brush) GetValue(HighlightBackgroundProperty); }
            set { SetValue(HighlightBackgroundProperty, value); }
        }

        /// <summary>
        /// If true then the highlighted text is search case insensitive.
        /// Default: True -> Case Insensitive
        /// </summary>
        public bool IgnoreCaseCheckForHighlightingText
        {
            get { return (bool) GetValue(IgnoreCaseCheckForHighlightingTextProperty); }
            set { SetValue(IgnoreCaseCheckForHighlightingTextProperty, value); }
        }

        /// <summary>
        /// If true only the 1st match will be highlighted, else all the matches will be highlighted
        /// Default: True -> Only 1st match is highlighted 
        /// </summary>
        public bool HighlightFirstMatchOnly
        {
            get { return (bool) GetValue(HighlightFirstMatchOnlyProperty); }
            set { SetValue(HighlightFirstMatchOnlyProperty, value); }
        }

        #endregion

        #region Private Methods

        private static void HighlightedChangedCallback(DependencyObject d_, DependencyPropertyChangedEventArgs e_)
        {
            HighlightingTextBlock textBlock = d_ as HighlightingTextBlock;
            if (textBlock == null)
            {
                return;
            }
            textBlock.RedoHighlighting();

        }

        #endregion

        #region Protected Methods

        protected virtual void RedoHighlighting()
        {
            Action<string, Brush, Brush> addInline =
                (str_, background_, foreground_) =>
                Inlines.Add(new Run(str_) {Background = background_, Foreground = foreground_});

            string selectedVal = HighlightedText;
            Inlines.Clear();
            if (!string.IsNullOrWhiteSpace(Prefix))
            {
                Inlines.Add(Prefix.Trim() + " "); 
            } 
            if (!string.IsNullOrWhiteSpace(selectedVal) && !string.IsNullOrWhiteSpace(OriginalText))
            {
                string text = OriginalText;
                RegexOptions option = IgnoreCaseCheckForHighlightingText
                                          ? RegexOptions.Singleline | RegexOptions.IgnoreCase |
                                            RegexOptions.CultureInvariant
                                          : RegexOptions.Singleline | RegexOptions.CultureInvariant;

                MatchCollection matchedResults = Regex.Matches(text, Regex.Escape(selectedVal), option);

                if (matchedResults.Count > 0)
                {
                    IEnumerable<Match> matchesToHighlight = HighlightFirstMatchOnly
                                                                ? matchedResults.Cast<Match>().Take(1)
                                                                : matchedResults.Cast<Match>();
                    int last = 0; 
                    foreach (Match matchedResult in matchesToHighlight)
                    {
                        int currentSection = matchedResult.Index;
                        if (currentSection > last)
                        {
                            addInline(text.Substring(last, currentSection - last), Background, Foreground);
                        }
                        addInline(text.Substring(currentSection, matchedResult.Length), HighlightBackground,
                                  HighlightForeground);
                        last = currentSection + matchedResult.Length;
                    }

                    if (last < text.Length)
                    {
                        addInline(text.Substring(last, text.Length - last), Background, Foreground);
                    }
                    if (!string.IsNullOrWhiteSpace(Suffix))
                    {
                        Inlines.Add(" " + Suffix.Trim());
                    }
                    return;
                }
            }

            if (!string.IsNullOrWhiteSpace(OriginalText))
            {
                string text = OriginalText;
                Inlines.Add(text);
                if (!string.IsNullOrWhiteSpace(Suffix))
                {
                    Inlines.Add(" " + Suffix.Trim());
                }
            } 
        }

        #endregion
    }
}
