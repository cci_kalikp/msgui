﻿namespace MorganStanley.MSDotNet.MSGui.Controls.SearchBox
{
    public interface ISearchResultItem
    {
        string ItemName { get; set; }
        object Item { get; set; }
        string MatchType { get; set; }
        string SearchText { get; set; }
        string ItemPath { get; } 
    }
}
