﻿#region Using

using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

#endregion Using

namespace MorganStanley.MSDotNet.MSGui.Controls.SearchBox
{
    public class OverlayTextBox : TextBox
    {
        #region Declarations

        public static readonly DependencyProperty HasTextProperty = DependencyProperty.Register("HasText", typeof (bool),
                                                                                                typeof (OverlayTextBox),
                                                                                                new FrameworkPropertyMetadata
                                                                                                    (false));

        public static readonly DependencyProperty OverlayTextProperty = DependencyProperty.Register("OverlayText",
                                                                                                    typeof (string),
                                                                                                    typeof (
                                                                                                        OverlayTextBox),
                                                                                                    new FrameworkPropertyMetadata
                                                                                                        (""));

        private Timer timer;

        private delegate void Method();

        #endregion Declarations

        #region Constructor

        static OverlayTextBox()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof (OverlayTextBox),
                                                     new FrameworkPropertyMetadata(typeof (OverlayTextBox)));
            TextProperty.OverrideMetadata(typeof (OverlayTextBox), new FrameworkPropertyMetadata(OnTextChangedCallback));

        }

        #endregion Constructor

        #region EventHandlers

        public static void OnTextChangedCallback(object sender_, DependencyPropertyChangedEventArgs args_)
        {
            OverlayTextBox box = sender_ as OverlayTextBox;
            if (box != null)
            {
                bool flag = !string.IsNullOrEmpty(box.Text);
                if (flag != box.HasText)
                {
                    box.SetValue(HasTextProperty, flag);
                }
            }
        }

        #endregion EventHandlers

        #region Public Properties

        public bool HasText
        {
            get { return (bool) GetValue(HasTextProperty); }
        }

        public string OverlayText
        {
            get { return (string) GetValue(OverlayTextProperty); }
            set { SetValue(OverlayTextProperty, value); }
        }


        /// <summary>
        /// Gets and Sets the amount of time to wait after the text has changed before updating the binding
        /// </summary>
        public int DelayTime
        {
            get { return (int) GetValue(DelayTimeProperty); }
            set { SetValue(DelayTimeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DelayTime.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DelayTimeProperty =
            DependencyProperty.Register("DelayTime", typeof (int), typeof (OverlayTextBox), new UIPropertyMetadata(667));


        //override this to update the source if we get an enter or return
        protected override void OnKeyDown(KeyEventArgs e_)
        {

            //we don't update the source if we accept enter
            //update the binding if enter or return is pressed
            if ((!AcceptsReturn) && (e_.Key == Key.Return || e_.Key == Key.Enter))
            {
                //get the binding
                BindingExpression bindingExpression = GetBindingExpression(TextProperty);

                //if the binding is valid update it
                if (BindingCanProceed(bindingExpression))
                {
                    //update the source
                    // ReSharper disable PossibleNullReferenceException
                    bindingExpression.UpdateSource();
                    // ReSharper restore PossibleNullReferenceException
                }
            }
            if (Key.Down == e_.Key)
            {
                e_.Handled = false;
                return;
            }
            base.OnKeyDown(e_);
        }

        protected override void OnPreviewKeyDown(KeyEventArgs e_)
        {
            if (Key.Down == e_.Key)
            {
                e_.Handled = false;
                return;
            }
            base.OnPreviewKeyDown(e_);
        }

        protected override void OnTextChanged(TextChangedEventArgs e_)
        {

            //get the binding
            BindingExpression bindingExpression = GetBindingExpression(TextProperty);

            if (BindingCanProceed(bindingExpression))
            {
                //get rid of the timer if it exists
                if (timer != null)
                {
                    //dispose of the timer so that it wont get called again
                    timer.Dispose();
                }

                //recreate the timer every time the text changes
                timer = new Timer(o_ =>
                    {

                        //create a delegate method to do the binding update on the main thread
                        // ReSharper disable PossibleNullReferenceException
                        Method x = bindingExpression.UpdateSource;
                        // ReSharper restore PossibleNullReferenceException

                        //need to check if the binding is still valid, as this is a threaded timer the text box may have been unloaded etc.
                        if (BindingCanProceed(bindingExpression))
                        {
                            //invoke the delegate to update the binding source on the main (ui) thread
                            Dispatcher.Invoke(x, new object[] {});
                        }
                        //dispose of the timer so that it wont get called again
                        timer.Dispose();

                    }, null, DelayTime, Timeout.Infinite);
            }

            base.OnTextChanged(e_);
        }

        //makes sure a binding can proceed
        private bool BindingCanProceed(BindingExpression bindingExpression_)
        {
            //can't update if there is no BindingExpression
            //can't update if we have no data item
            //can't update if the binding is not active
            //can't update if the parent binding is null
            //don't need to update if the UpdateSourceTrigger is set to update every time the property changes

            return (bindingExpression_ != null) && (bindingExpression_.DataItem != null) &&
                   (bindingExpression_.Status == BindingStatus.Active) && (bindingExpression_.ParentBinding != null)
                   && (bindingExpression_.ParentBinding.UpdateSourceTrigger != UpdateSourceTrigger.PropertyChanged);

        }

        #endregion Public Properties
    }
}
