﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;

namespace MorganStanley.MSDotNet.MSGui.Controls.SearchBox
{
    public abstract class SearchBoxViewModelBase : ViewModelBase, ISearchBoxViewModel
    {
        private string searchText;
        private readonly ICommand clearSearchCommand;
        protected string lastResultSearchText;  

        public virtual bool EnableTypeaheadDropDown { get; set; }

        public bool Searching { get; private set; }

        public bool AsyncSearch { get; set; }
        public string SearchText
        {
            get { return searchText; }
            set
            {
                if (searchText != value)
                {
                    lastResultSearchText = searchText;
                    searchText = value;
                    OnPropertyChanged("SearchText");
                    OnPropertyChanged("SearchTextIsNotEmpty");  
                    UpdateSearchResult();
                }
            }
        }

        public string WatermarkText { get; set; }

        public bool SearchTextIsNotEmpty
        {
            get { return !string.IsNullOrWhiteSpace(SearchText); }
        }

        public ICommand ClearSearchCommand
        {
            get { return clearSearchCommand; }
        }

        private bool showDropDown = false;
        public bool ShowDropDown
        {
            get { return showDropDown; }
            set
            {
                if (showDropDown != value)
                {
                    showDropDown = value;
                    OnPropertyChanged("ShowDropDown");
                }
            }
        }

        private ISearchResultItem selectedItem;
        public ISearchResultItem SelectedItem
        {
            get { return selectedItem; }
            set
            {
                if (selectedItem != value)
                {
                    selectedItem = value;
                    if (selectedItem != null)
                    {
                        ShowDropDown = false;
                    }
                } 
                OnPropertyChanged("SelectedItem");
            }
        }

        public IList<ISearchResultItem> SearchResultItems
        {
            get; 
            protected set;
        }

        protected SearchBoxViewModelBase()
        {
            SearchResultItems = new List<ISearchResultItem>();

            clearSearchCommand = new DelegateCommand(obj_ =>
                {
                    SearchText = string.Empty;         
                });  
        }

        private CancellationTokenSource tokenSource;
        protected void UpdateSearchResult()
        {
            if (AsyncSearch)
            {
                Searching = true;
                OnPropertyChanged("Searching");

                if (tokenSource != null)
                {
                    tokenSource.Cancel(false);     
                }
                tokenSource = new CancellationTokenSource();
                var token = tokenSource.Token;
                Task.Factory.StartNew(
                _ =>
                {
                    try
                    {
                        UpdateSearchResultCore(token);
                    }
                    finally
                    {
                        Searching = false;
                        OnPropertyChanged("Searching");
                    }
                }, token);
            }
            else
            {
                UpdateSearchResultCore(new CancellationToken(false));
            }
        }

        protected abstract void UpdateSearchResultCore(CancellationToken token_);
    }

}
