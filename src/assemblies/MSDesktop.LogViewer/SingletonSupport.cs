﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Ipc;
using System.Text;
using System.Threading;
using System.Windows;
using MSDesktop.Debugging.ViewModels;

namespace MSDesktop.LogViewer
{
    static class SingletonSupport
    {
        static Mutex mutex;
        static IChannel ipcChannel;
        const string Uri = "MSDesktopLogViewer";


        internal static bool InvokeExistingInstance()
        {
            Assembly assembly = Assembly.GetEntryAssembly();
            string name = assembly.FullName;

            mutex = new Mutex(false, name);
            if (!mutex.WaitOne(TimeSpan.Zero, false))
            {
                ActivateFirstInstance(Environment.GetCommandLineArgs().Skip(1));
                return true;
            }
            return false;
        }

        internal static void RegisterAsFirstInstance(Application application_)
        {
            RegisterActivationMonitor();
            application_.Exit += application_Exit; 
        }


        static void application_Exit(object sender_, ExitEventArgs e_)
        {
            //Must be done in this order to avoid a race condition 
            ChannelServices.UnregisterChannel(ipcChannel);
            mutex.ReleaseMutex();
            mutex.Close();
        }

        private static void RegisterActivationMonitor()
        {
            Assembly assembly = Assembly.GetEntryAssembly();
            string name = assembly.FullName;

            //Registering IPC channel
            ipcChannel = new IpcChannel(name);
            ChannelServices.RegisterChannel(ipcChannel, false);

            Type serverType = typeof(ActivationMonitor);
            RemotingConfiguration.RegisterWellKnownServiceType(serverType, Uri, WellKnownObjectMode.SingleCall);
        }

        private static void ActivateFirstInstance(IEnumerable<string> logFiles_)
        {
            Assembly assembly = Assembly.GetEntryAssembly();
            string name = assembly.FullName;
            string url = "ipc://" + name + "/" + Uri;
            Type serverType = typeof(ActivationMonitor);

            RemotingConfiguration.RegisterWellKnownClientType(serverType, url);
            var monitor = new ActivationMonitor();
            monitor.ActivateLogViewer(logFiles_.ToArray());
        }

    }


    public class ActivationMonitor : MarshalByRefObject
    {
        public void ActivateLogViewer(string[] fileNames_)
        {
            Application.Current.Dispatcher.Invoke(new Action(() =>
                {
                    var logViewer = Application.Current.MainWindow;
                    LogsViewModel model = (LogsViewModel) logViewer.DataContext;
                    model.OpenLogsCommand.Execute(fileNames_);
                    if (logViewer.WindowState == WindowState.Minimized)
                    {
                        logViewer.WindowState = WindowState.Normal;
                    }
                }));
        }
    }
}
