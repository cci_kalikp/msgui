﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Xml.Linq;
using MSDesktop.Debugging.Extensions;
using MSDesktop.Debugging.ViewModels;

namespace MSDesktop.LogViewer
{
    public class LogViewerState
    {
        private LogsViewModel logsModel;
        public LogViewerState(LogsViewModel logs_)
        {
            this.logsModel = logs_;
        }

        public XDocument SaveState()
        {
            XElement root = new XElement("State");
            if (logsModel.Logs.Count > 0)
            {
                foreach (LogViewModel log in logsModel.Logs)
                {
                    XElement logElement = new XElement("Log");
                    logElement.SetAttributeValue("FilePath", log.FilePath);
                    if (!string.IsNullOrEmpty(log.TagText))
                    {
                        logElement.SetAttributeValue("TagText", log.TagText);
                    }
                    if (log.TagColor != Colors.Transparent)
                    {
                        logElement.SetAttributeValue("TagColor", log.TagColor.ToString());
                    }
                    root.Add(logElement);
                }
            }
            return new XDocument(root);
        }

        public void LoadSate(XDocument state_)
        {             
            XElement root = state_.Root;
            if (root != null)
            {
                Dictionary<string, TabTagViewModel> logs = new Dictionary<string, TabTagViewModel>();
                DateTime lastVisitedTime = new DateTime();
                string startupDirectory = null;

                foreach (XElement logElement in root.Elements("Log"))
                {
                    XAttribute pathAttr = logElement.Attribute("FilePath");
                    if (pathAttr != null && !string.IsNullOrEmpty(pathAttr.Value) &&
                        !logs.ContainsKey(pathAttr.Value.Trim()))
                    {
                        try
                        {
                            FileInfo file = new FileInfo(pathAttr.Value.Trim());
                            if (!file.Exists) continue;
                            TabTagViewModel tag = new TabTagViewModel();
                            XAttribute tagTextAttr = logElement.Attribute("TagText");
                            if (tagTextAttr != null && !string.IsNullOrEmpty(tagTextAttr.Value))
                            {
                                tag.TagText = tagTextAttr.Value.Trim();
                            }
                            XAttribute tagColorAttr = logElement.Attribute("TagColor");
                            if (tagColorAttr != null && !string.IsNullOrEmpty(tagColorAttr.Value))
                            {
                                try
                                {
                                    object color = new ColorConverter().ConvertFrom(tagColorAttr.Value);
                                    if (color != null && color is Color)
                                    {
                                        tag.TagColor = (Color) color;
                                    }
                                }
                                catch
                                {
                                }
                            }
                            logs.Add(pathAttr.Value.Trim(), tag);
                            if (file.LastWriteTime.CompareTo(lastVisitedTime) > 0)
                            {
                                lastVisitedTime = file.LastWriteTime;
                                startupDirectory = file.DirectoryName;
                            }
                        }
                        catch
                        {

                        }
                    }
                }
                if (logsModel.Logs.Count == 0)
                {
                    foreach (var log in logs)
                    {
                        logsModel.OpenLogCommand.Execute(log.Key);
                        if (logsModel.CurrentLog != null)
                        {
                            logsModel.CurrentLog.TagColor = log.Value.TagColor;
                            logsModel.CurrentLog.TagText = log.Value.TagText;
                        }
                    }
                }
                else
                {
                    foreach (var log in logsModel.Logs)
                    {
                        if (logs.ContainsKey(log.FilePath))
                        {
                            log.TagColor = logs[log.FilePath].TagColor;
                            log.TagText = logs[log.FilePath].TagText;
                        }
                    }
                }

            }
        }
    }
}
