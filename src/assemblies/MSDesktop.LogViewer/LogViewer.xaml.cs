﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MSDesktop.LogViewer
{
    /// <summary>
    /// Interaction logic for LogViewer.xaml
    /// </summary>
    public partial class LogViewer : Window
    {
        private ResourceDictionary rd = new ResourceDictionary()
        {
            Source = new Uri("/MSDesktop.Debugging;component/Views/Log/LogViewContainer.xaml", UriKind.Relative)
        };
        public LogViewer()
        {
            InitializeComponent();
            FrameworkElement view = rd["LogViewContainer"] as FrameworkElement;
            this.Content = view;
        }
    }
}
