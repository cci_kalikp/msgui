﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using MorganStanley.IED.Concord.Configuration;

namespace MSDesktop.LogViewer
{
    public static class ConfigurationHelper
    {
        public delegate void LoadSateMethod(XDocument state_);
        public delegate XDocument SaveStateMethod();
        private static int initComplete = 0;

        private static void Initialize()
        {
            if (initComplete == 0)
            ConfigurationManager.Initialize("app=MSDesktop.LogViewer");
            Interlocked.Exchange(ref initComplete, 1);
        }

        public static void LoadState(string configName_, LoadSateMethod loader_)
        {
            Initialize();
            var userPreferences = (Configurator)ConfigurationManager.GetConfig(configName_);
            if (userPreferences != null)
            {
                try
                {
                    var savedState = userPreferences.GetNode(".", null);
                    if (savedState != null && !string.IsNullOrEmpty(savedState.InnerXml))
                    {
                        XDocument doc = XDocument.Parse(savedState.InnerXml);
                        if (doc.Root != null)
                        {
                            loader_(doc);
                        }
                    }
                }
                catch
                {

                }
            }
        }

        public static void SaveState(string configName_, SaveStateMethod saver_)
        {
            Initialize();
            var userPreferences = (Configurator)ConfigurationManager.GetConfig(configName_);
            if (userPreferences != null)
            {
                var newState = saver_();
                if (newState != null)
                {
                    var xn = new XmlDocument
                    {
                        InnerXml = string.Format("<{1}>{0}</{1}>", newState, configName_)
                    };
                    userPreferences.SetNode(".", null, xn.DocumentElement);
                    userPreferences.Save();
                }

            }
        }
    }
}
