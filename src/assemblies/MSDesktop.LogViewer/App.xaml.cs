﻿ using System; 
 using System.Windows; 
 using MSDesktop.Debugging.ViewModels;
using System.Linq; 

namespace MSDesktop.LogViewer
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>


    public partial class App : Application
    {
        [STAThread]
        static void Main()
        {
            if (SingletonSupport.InvokeExistingInstance())
            {
                return;
            } 

            var application = new Application();
            SingletonSupport.RegisterAsFirstInstance(application);

            var logViewer = new LogViewer();
            var context = new LogsViewModel(null);
            ConfigurationHelper.LoadState("UserPreferences", LogViewerSetting.LoadState);
            context.DisableSort = LogViewerSetting.Current.DisableSort;

            foreach (var logfile in Environment.GetCommandLineArgs().Skip(1))
            {
                context.OpenLogCommand.Execute(logfile); 
            }
             
            var logViewerState = new LogViewerState(context);
            if (LogViewerSetting.Current.RememberCurrentSession)
            {
                ConfigurationHelper.LoadState("LastState", logViewerState.LoadSate);
            }
            
            logViewer.DataContext = context;
            application.MainWindow = logViewer;
            application.MainWindow.Show();
            try
            {
                application.Run();
            }
            finally
            {
                ConfigurationHelper.SaveState("UserPreferences", LogViewerSetting.SaveState);
                if (LogViewerSetting.Current.RememberCurrentSession)
                {
                    ConfigurationHelper.SaveState("LastState", logViewerState.SaveState);  
                }
            } 
        }
         
    }
}
