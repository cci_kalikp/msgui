﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using MSDesktop.MessageRouting.Routing;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.My;

namespace MSDesktop.MessageRouting
{
    internal sealed class RoutedMessageDispatcher
    {
        internal static int TimerInterval = 40;
        internal static DeliveryStrategyAttribute DefaultStrategy = new DeliveryStrategyAttribute();
        private static readonly IMSLogger _logger = MSLoggerFactory.CreateLogger<RoutedMessageDispatcher>();

        private readonly IRoutingCommunicator _communicator;
        private readonly List<MessageHolder> _queue = new List<MessageHolder>();
        private readonly ConcurrentDictionary<Type, DeliveryStrategyAttribute> _strategies = new ConcurrentDictionary<Type, DeliveryStrategyAttribute>();
        private bool _isStarted;
        internal static bool? ExceptionAsFailed;
        internal static bool TrackInternalMessageEvents;
        public RoutedMessageDispatcher(IRoutingCommunicator communicator)
        {
            _communicator = communicator;
        }

        public void Start()
        {
            if (_isStarted)
                return;

            _isStarted = true;
            var thread = new Thread(Loop)
            {
                IsBackground = true,
                Name = "Kraken dispatcher thread for process #" + Process.GetCurrentProcess().Id
            };

            thread.Start();
        }


        internal DeliveryInfo Dispatch(RoutedMessage message, Action action, Action<Action<Endpoint>> fallbackAction = null)
        {
            var ack = message as RoutedMessage<Ack>;
            if (ack != null)
            // is this an ack?
            {
                var msg = SyncToArray(_queue).FirstOrDefault(m => ack.Message.Id == m.Message.Id && ack.Origin.Equals(m.Message.Target));
                if (msg != null)
                // does it ack something sent from this process?
                {
                    msg.Info.ExtraInformation = ack.Message.ExtraInformation;
                    msg.Info.Status = ack.Message.IsAcceptable ? DeliveryStatus.Delivered: DeliveryStatus.Rejected;
                    return new DeliveryInfo
                    {
                        Id = message.Id,
                        Target = message.Target,
                        Status = ack.Message.IsAcceptable ? DeliveryStatus.Delivered : DeliveryStatus.Rejected,
                        ExtraInformation = ack.Message.ExtraInformation
                    };
                }
            }

            if (!_strategies.ContainsKey(message.GetPayloadType()))
            {
                var attr = message.GetPayloadType().GetCustomAttributes(typeof(DeliveryStrategyAttribute), false).FirstOrDefault() as DeliveryStrategyAttribute;

                var addFailed = true;
                while (addFailed)
                {
                    addFailed = !_strategies.TryAdd(message.GetPayloadType(), attr ?? DefaultStrategy);
                }
            }

            var di = new DeliveryInfo
            {
                Id = message.Id,
                Target = message.Target,
                Status = DeliveryStatus.Pending
            };

            lock (_queue)
            {
                _queue.Add(new MessageHolder
                {
                    Message = message,
                    Action = action,
                    FallbackAction = fallbackAction,
                    Info = di
                });
            }
            return di;
        }

        private void Loop()
        {
            _communicator.GetSubscriber<Ack>().GetObservable().Subscribe(ack =>
            {
                {
                    var msg = SyncToArray(_queue).FirstOrDefault(holder => holder.Message.Id == ack.Id);
                    if (msg != null)
                    {
                        msg.Info.ExtraInformation = ack.ExtraInformation;
                        msg.Info.Status = ack.IsAcceptable ? DeliveryStatus.Delivered : DeliveryStatus.Failed;
                    }
                }
            });

            while (true)
            {
                foreach (var holder in SyncToArray(_queue))
                {

                    if (holder.Info.Status == DeliveryStatus.Delivered)
                    {
                        _queue.Remove(holder);
                        continue;
                    }

                    var strategy = _strategies[holder.Message.GetPayloadType()];

                    switch (GetSendAction(holder, strategy))
                    {
                        case DeliveryAction.Send:
                            if (strategy.DeliveryStrategy == DeliveryStrategy.FireAndForget)
                            {
                                holder.Info.Status = DeliveryStatus.Delivered;
                                _queue.Remove(holder);
                            }
                            else
                            {
                                holder.Info.Status = DeliveryStatus.WaitingForAck;
                                holder.Message.Attempts++;
                                holder.Message.LastAttempt = DateTime.Now;
                            }
                            if (ExceptionAsFailed == true)
                            {
                                try
                                {
                                    holder.Action();
                                }
                                catch (InvalidTargetingException)
                                {
                                    throw;
                                }
                                catch(Exception ex)
                                { 
                                     _logger.Error("failed to dispatch message: " + holder.Message.GetPayloadType(), ex);
                                     holder.Info.Status = DeliveryStatus.Failed;
                                     _queue.Remove(holder);
                                }
                            }
                            else
                            {
                                holder.Action();
                            }

                            break;
                        case DeliveryAction.Die:
                            if (strategy.Fallback && holder.FallbackAction != null)
                            {
                                var holderInternal = holder;

                                var fallbackComparator = GetFallbackComparator(holderInternal);


                                var crashedRoutes = SyncToArray(_queue)
                                    .Where(
                                        r =>
                                        r.Info.Target.Equals(holderInternal.Info.Target) &&
                                        fallbackComparator(holderInternal.Message, r.Message))
//                                        holderInternal.Message.GetPayloadType() == r.Message.GetPayloadType())
                                    .ToArray();

                                var crashedRoutesForCallback = crashedRoutes;

                                foreach (var route in crashedRoutes)
                                {
                                    route.Info.Status = DeliveryStatus.Fallback;
                                    route.Wait = true;
                                }

                                Action<Endpoint> callback = newTarget =>
                                {
                                    if (newTarget == null)
                                    {
                                        foreach (var route in crashedRoutesForCallback)
                                        {
                                            route.Info.Status = DeliveryStatus.Failed;
                                            _queue.Remove(route);
                                            route.Wait = false;
                                        }

                                        return;
                                    }

                                    foreach (var route in crashedRoutesForCallback)
                                    {
                                        route.Info.Status = DeliveryStatus.Pending;
                                        route.Info.Target = newTarget;
                                        route.Message.Target = newTarget;
                                        route.Message.Attempts = 0;
                                        route.Wait = false;
                                    }
                                };

                                Task.Factory.StartNew(callback_ => holderInternal.FallbackAction(callback_ as Action<Endpoint>), callback);

                            }
                            else
                            {
                                holder.Info.Status = DeliveryStatus.Failed;
                                _queue.Remove(holder);
                            }

                            break;
                    }
                }

                Thread.Sleep(TimerInterval);
            }
        }

        private Func<object, object, bool> GetFallbackComparator(MessageHolder messageHolder)
        {
            Func<object, object, bool> defaultComparator = (msg1, msg2) => msg1.GetType() == msg2.GetType();

            var msgType = messageHolder.Message.GetPayloadType();
            foreach (var methodInfo in msgType.GetMethods())
            {
                var attr = methodInfo.GetCustomAttributes(typeof(FallbackGroupComparatorAttribute), true).FirstOrDefault();
                if (attr != null)
                {
                    return (msg1, msg2) =>
                    {
                        try
                        {
                            var a = ReflHelper.PropertyGet(msg1, "Message");
                            var b = ReflHelper.PropertyGet(msg2, "Message");
                            // CAREFUL! Access to foreach loop variable in closure. Not an issue even in .net 4.0 as long as we are immediately returning.
                            return (bool) methodInfo.Invoke(a, new[] {b});
                        }
                        catch (Exception)
                        {
                            _logger.Warning(string.Format("Error comparing messages for feedback grouping. Invocation of method marked with FalllbackGroupComparatorAttribute failed for type {0}", msgType.FullName));
                            return defaultComparator(msg1, msg2);
                        }
                    };
                }
            }

            return defaultComparator;
        }

        private DeliveryAction GetSendAction(MessageHolder holder, DeliveryStrategyAttribute strategy)
        {
            if (strategy.DeliveryStrategy == DeliveryStrategy.FireAndForget)
            {
                return holder.Message.Attempts == 0 ? DeliveryAction.Send : DeliveryAction.Die;
            }
            else // (strategy.DeliveryStrategy == DeliveryStrategy.Tracked)
            {
                if (holder.Info.Status == DeliveryStatus.Rejected)
                {
                    return (holder.Message.Attempts < strategy.RetryCount) ? DeliveryAction.Send : DeliveryAction.Die;
                }

                if (holder.Wait || holder.Info.Status == DeliveryStatus.Fallback)
                    return DeliveryAction.Wait;

                if (holder.Message.Attempts == 0)
                    return DeliveryAction.Send;
                 
                var timeout = (strategy.TimeoutType == TimeoutType.Constant)
                    ? strategy.Timeout
                    : strategy.Timeout * (int)Math.Pow(2, holder.Message.Attempts + 1);

                if ((holder.Message.Attempts < strategy.RetryCount))
                {
                    return (DateTime.Now - holder.Message.LastAttempt).TotalMilliseconds > timeout ? DeliveryAction.Send : DeliveryAction.DoNotSend;
                }

                if ((DateTime.Now - holder.Message.LastAttempt).TotalMilliseconds < timeout)
                {
                    return DeliveryAction.DoNotSend;
                }

                return DeliveryAction.Die;
            }
        }

        private static T[] SyncToArray<T>(List<T> list_)
        {
            lock (list_)
            {
                return list_.ToArray();
            }
        } 
        private class MessageHolder
        {
            public RoutedMessage Message { get; set; }
            public Action Action { get; set; }
            public Action<Action<Endpoint>> FallbackAction { get; set; }
            public DeliveryInfo Info { get; set; }
            public bool Wait { get; set; }
        }
    }
}
