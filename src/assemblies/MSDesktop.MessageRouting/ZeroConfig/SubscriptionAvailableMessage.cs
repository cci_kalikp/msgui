﻿using System;
using MSDesktop.MessageRouting.Message;
using MSDesktop.MessageRouting.Routing;

namespace MSDesktop.MessageRouting.ZeroConfig
{
    [Serializable, Broadcast, InternalMessage, DeliveryStrategy(Fallback = false), ManagementMessage]
    public sealed class SubscriptionAvailableMessage:IProvideMessageInfo
    {
        public Type Type { get; set; }
        public Endpoint SubscriberIdentity { get; set; }

        public string GetInfo()
        {
            return string.Format("{0} message is subscribed by {1}", Type.FullName, SubscriberIdentity);
        }
    }
}
