﻿using System;
using System.Collections.ObjectModel;
using MSDesktop.MessageRouting.Routing;

namespace MSDesktop.MessageRouting.ZeroConfig
{
    public interface IRouteExplorer
    {
        ObservableCollection<Endpoint> SubscribedApplications { get; }
        bool IsServerDiscoveryEnabled { get; }

        void BypassSubscribedApplication(string hostName, string application, string instanceName, Type messageType);

        ObservableCollection<Endpoint> GetSubscribedApplications(Type messageType);

        event EventHandler<SubscriptionBypassEventArgs> SubscriptionBypassEvent;
        event EventHandler<SubscriptionAvailableEventArgs> SubscriptionAvailableEvent;
        event EventHandler<SubscriptionUnavailableEventArgs> SubscriptionUnavailableEvent;

        ObservableCollection<Endpoint> GetAllSubscribedApplications();
    }
}
