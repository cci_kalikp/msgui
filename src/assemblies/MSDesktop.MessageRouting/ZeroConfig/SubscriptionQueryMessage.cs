﻿using System;
using MSDesktop.MessageRouting.Message;
using MSDesktop.MessageRouting.Routing;

namespace MSDesktop.MessageRouting.ZeroConfig
{
    [Broadcast, Serializable, InternalMessage, ManagementMessage]
    internal class SubscriptionQueryMessage:IProvideMessageInfo
    {
        public Endpoint Origin { get; set; }
        public int QueryId { get; set; }
        public string MessageType { get; set; }
        public string GetInfo()
        {
            return string.Format("Broadcast query of {0} message (id: {1})", MessageType, QueryId);
        }
    }
}
