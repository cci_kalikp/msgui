﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MSDesktop.MessageRouting.Message;
using MSDesktop.MessageRouting.Routing;

namespace MSDesktop.MessageRouting.ZeroConfig
{
    [Serializable, InternalMessage, DeliveryStrategy(Fallback = false), ManagementMessage]
    public class SubscriptionBypassMessage : IExplicitTarget, IProvideMessageInfo
    {
        public Endpoint Origin { get; set; }
        public Endpoint Target { get; set; }
        public int QueryId { get; set; }
        public string MessageType { get; set; }

        public string GetInfo()
        {
            return string.Format("Query if {0} message(id:{2}) is subscribed by {1}", MessageType, Target, QueryId);
        }
    }
}
