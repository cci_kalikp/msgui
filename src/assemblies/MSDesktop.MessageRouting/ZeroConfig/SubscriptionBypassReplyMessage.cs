﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MSDesktop.MessageRouting.Message;
using MSDesktop.MessageRouting.Routing;

namespace MSDesktop.MessageRouting.ZeroConfig
{
    [Serializable, InternalMessage, DeliveryStrategy(DeliveryStrategy = DeliveryStrategy.FireAndForget, Fallback = false), ManagementMessage]
    public class SubscriptionBypassReplyMessage : IExplicitTarget, IProvideMessageInfo
    {
        public Endpoint Target { get; set; }

        public int QueryId { get; set; }
        public Endpoint SubscriberIdentity { get; set; }
        public DeliveryStatus Status { get; set; }

        public string GetInfo()
        {
            return string.Format("Delivery status of Message id:{0} to {1} is {2}", QueryId, SubscriberIdentity, Status);
        }
    }
}
