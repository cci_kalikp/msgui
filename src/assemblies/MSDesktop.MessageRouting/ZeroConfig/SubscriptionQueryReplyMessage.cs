﻿using System;
using MSDesktop.MessageRouting.Message;
using MSDesktop.MessageRouting.Routing;

namespace MSDesktop.MessageRouting.ZeroConfig
{
    [Serializable, InternalMessage, DeliveryStrategy(Fallback = false), ManagementMessage]
    public sealed class SubscriptionQueryReplyMessage : IExplicitTarget, IProvideMessageInfo
    {
        public Endpoint SubscriberIdentity { get; set; }
        public Endpoint Target { get; set; }
        public int QueryId { get; set; }

        public string GetInfo()
        {
            return string.Format("message id: {0} is subscribed by {1}", QueryId, SubscriberIdentity);
        }
    }
}
