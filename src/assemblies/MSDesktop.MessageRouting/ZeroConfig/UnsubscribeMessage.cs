﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MSDesktop.MessageRouting.Message;
using MSDesktop.MessageRouting.Routing;

namespace MSDesktop.MessageRouting.ZeroConfig
{
    [Broadcast, Serializable, InternalMessage, ManagementMessage]
    class UnsubscribeMessage:IProvideMessageInfo
    {
        public Endpoint Origin { get; set; }

        public string GetInfo()
        {
            return Origin + " logged off";
        }
    }
}
