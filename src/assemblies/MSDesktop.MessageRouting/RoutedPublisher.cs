﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication;
using MorganStanley.MSDotNet.My;
using MSDesktop.MessageRouting.Routing;

namespace MSDesktop.MessageRouting
{
    public sealed class RoutedPublisher<TMessage> : IRoutedPublisher<TMessage>
    {
        private static readonly IMSLogger _logger = MSLoggerFactory.CreateLogger<RoutedPublisher<TMessage>>();
        private readonly IEndpointSelector _endpointSelector;
        private readonly Endpoint _origin;
        private readonly RoutedMessageDispatcher _dispatcher;
        private readonly Dictionary<Type, bool> internalMessageStrategies = new Dictionary<Type, bool>();
        private readonly ConcurrentDictionary<DeliveryInfo, object> messages = new ConcurrentDictionary<DeliveryInfo, object>();
        internal IModulePublisher<RoutedMessage<TMessage>> Publisher { get; set; }

        internal RoutedPublisher(IEndpointSelector endpointSelector, Endpoint origin, RoutedMessageDispatcher dispatcher)
        {
            this._endpointSelector = endpointSelector;
            this._origin = origin;
            this._dispatcher = dispatcher;
        } 
        
        public DeliveryInfo Publish(TMessage message)
        {
            DeliveryInfo di;
            int attempt = 0;

            var chainResolvedTarget = _endpointSelector.ResolveTarget(message, attempt);
 
            if (chainResolvedTarget == null)
            {
                _logger.Error("Message dispatch failed: endpoint selection returned null.");
                return new DeliveryInfo { Status = DeliveryStatus.Failed };
            }

            var targetEndpoint = chainResolvedTarget.Endpoint;
            // todo farm collab will need to circumvent below line
            targetEndpoint.Username = _origin.Username;
            var routedMessage = RoutedMessage<TMessage>.New(message, targetEndpoint, _origin);
            di = _dispatcher.Dispatch(
                   routedMessage,
                   () =>
                       {
                           if (!string.IsNullOrEmpty(routedMessage.Target.CpsServer))
                           {
                               var internalPublisher = Publisher as ModulePublisher<RoutedMessage<TMessage>>;
                               if (internalPublisher != null)
                               {
                                   internalPublisher.PublishToCpsServer(routedMessage, routedMessage.Target.CpsServer, routedMessage.Target.Kerberosed);
                                   return;
                               }
                           }

                           // todo farm kraken mgmt
                           //if (KrakenConfig.IsServerDiscoveryEnabled)
                           //    throw new InvalidTargetingException("Cps server multicast is not supported when Kraken server discovery is enabled.");

                           Publisher.Publish(routedMessage); 
                       },
                   callback =>
                   {
                       if (routedMessage.IsPayloadSerializable())
                       {
                           //does this do ref capture?
                           Fallback(message, ++attempt, callback);
                       }
                   });

            OnMessageDispatched(di, message);
            return di;
        }

        private void Fallback(TMessage message,  int attempt, Action<Endpoint> callback)
        {
            var target = _endpointSelector.ResolveTarget(message, attempt);
            if (target != null && target.Endpoint != null)
            { 
                target.Endpoint.Username = _origin.Username;
            }
            callback(target == null ? null : target.Endpoint); 
        }

        private void OnMessageDispatched(DeliveryInfo deliveryInfo, object message)
        {
            var type = message.GetType();
            if (!RoutedMessageDispatcher.TrackInternalMessageEvents)
            {
                if (!internalMessageStrategies.ContainsKey(type))
                {
                    var attr = type.GetCustomAttributes(typeof(InternalMessageAttribute), false).FirstOrDefault();
                    internalMessageStrategies[type] = attr != null;
                }

                if (internalMessageStrategies[type])
                    return;
            }
          

            messages.TryAdd(deliveryInfo, message);

            deliveryInfo.PropertyChanged += DeliveryInfo_PropertyChanged;

            this.OnMessagePublished(new MessagePublishedEventArgs
            {
                DeliveryInfo = deliveryInfo,
                Message = message
            });
        }

        private void DeliveryInfo_PropertyChanged(object sender, PropertyChangedEventArgs eventArgs)
        {
            var deliveryInfo = (DeliveryInfo)sender;

            switch (eventArgs.PropertyName)
            {
                case "Status":
                    switch (deliveryInfo.Status)
                    {
                        case DeliveryStatus.Failed:
                            deliveryInfo.PropertyChanged -= DeliveryInfo_PropertyChanged;
                            try
                            {
                                object msg;
                                messages.TryRemove(deliveryInfo, out msg);
                                if (msg != null)
                                {
                                    OnMessageFailed(new MessageFailedEventArgs
                                    {
                                        DeliveryInfo = deliveryInfo,
                                        Message = msg
                                    });
                                }
                            }
                            catch
                            {
                                // TODO pokemon
                            }

                            break;
                        case DeliveryStatus.Delivered:
                            deliveryInfo.PropertyChanged -= DeliveryInfo_PropertyChanged;
                            try
                            {
                                object msg;
                                messages.TryRemove(deliveryInfo, out msg);
                                if (msg != null)
                                {
                                    OnMessageDelivered(new MessageDeliveredEventArgs
                                    {
                                        DeliveryInfo = deliveryInfo,
                                        Message = msg
                                    });
                                }
                            }
                            catch
                            {
                                // TODO pokemon
                            }

                            break;
                    }
                    break;
            }
        }

        #region MessagePublishedEvent

        public event EventHandler<MessagePublishedEventArgs> MessagePublished;

        private void OnMessagePublished(MessagePublishedEventArgs eventArgs)
        {
            var holder = this.MessagePublished;
            if (holder != null)
                holder(this, eventArgs);
        }

        #endregion

        #region MessageDeliveredEvent

        public event EventHandler<MessageDeliveredEventArgs> MessageDelivered;

        private void OnMessageDelivered(MessageDeliveredEventArgs eventArgs)
        {
            var holder = this.MessageDelivered;
            if (holder != null)
                holder(this, eventArgs);
        }

        #endregion

        #region MessageFailedEvent

        public event EventHandler<MessageFailedEventArgs> MessageFailed;

        private void OnMessageFailed(MessageFailedEventArgs eventArgs)
        {
            var holder = this.MessageFailed;
            if (holder != null)
                holder(this, eventArgs);
        }

        #endregion

    }

}
