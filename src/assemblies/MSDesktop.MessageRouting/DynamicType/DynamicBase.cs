﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSDesktop.MessageRouting.DynamicType
{
    [Serializable]
    public class DynamicBase
    {
        public string TopicMediatorTypeName { get; set; }
        public string[] Topics { get; set; }
        public string Content { get; set; }
    }
}
