﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Reflection.Emit;

namespace MSDesktop.MessageRouting.DynamicType
{
    /// <summary>
    /// Manages the runtime assembly generation for Kraken soft messaging support.
    /// </summary>
    public static class AssemblyGenerator
    {
        public const string AssemblyNameNamespace = "Kraken.ContentBasedRouting.Messages";
        public const string ContentPropertyName = "Content";

        /// <summary>
        /// Known types in this assembly
        /// </summary>
        private static Dictionary<string, Type> knownTypes;

        private static AssemblyName assemblyName;
        private static AssemblyBuilder assemblyBuilder;
        private static ModuleBuilder module;

        static AssemblyGenerator()
        {
            knownTypes = new Dictionary<string, Type>();

           /* StrongNameKeyPair kp;
            using (FileStream fs = new FileStream("Kraken.snk", FileMode.Open))
            {
                kp = new StrongNameKeyPair(fs);
            }*/

            assemblyName = new AssemblyName(AssemblyNameNamespace);
           // assemblyName.KeyPair = kp;
            assemblyName.Version = new Version(1, 0, 0, 0);

            assemblyBuilder = Thread.GetDomain().DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.Run, null, System.Security.SecurityContextSource.CurrentAssembly);

            module = assemblyBuilder.DefineDynamicModule(AssemblyNameNamespace);

            Thread.GetDomain().AssemblyResolve += (object sender, ResolveEventArgs args) =>
            {
                if (args.Name.Equals(module.Assembly.FullName))
                {
                    return module.Assembly;
                }
                return null;
            };
        }

        /// <summary>
        /// Gets a dynamic type. If no type exists with the given name, new type will be created.
        /// </summary>
        /// <param name="name">Name of the dynamic type</param>
        /// <returns>Dynamic type</returns>
        public static Type GetDynamicType(string name)
        {
            if (knownTypes.ContainsKey(name))
            {
                return knownTypes[name];
            }
            TypeGenerator typeGenerator = new TypeGenerator(module, name, typeof(DynamicBase));
            typeGenerator.CreateProperty<string>(ContentPropertyName);
            Type generatedType = typeGenerator.CreateType();
            knownTypes.Add(name, generatedType);
            return generatedType;
        }


        /// <summary>
        /// Gets a dynamic type instance containing the content. 
        /// If no type exists with the given name, new type will be created.
        /// </summary>
        /// <param name="name">Name of the dynamic type</param>
        /// <param name="content">Content stored in the instance</param>
        /// <returns>Dynamic type instance</returns>
        public static DynamicBase GetTypeInstanceWithContent(string name, string[] topics, string content)
        {
            var instance = TypeGenerator.CreateInstance(GetDynamicType(name));
            instance.Topics = topics;
            instance.Content = content;
            return instance;
        }

        public static void EnsureType(string name)
        {
            GetDynamicType(name);
        }

        #region 
        
        /// <summary>
        /// Gets a dynamic type. If no type exists with the given name, new type will be created.
        /// </summary>
        /// <param name="name">Name of the dynamic type</param>
        /// <returns>Dynamic type</returns>
        public static Type GetDynamicType<T>(string name)
        {
            if (knownTypes.ContainsKey(name))
                return knownTypes[name];

            var typeGenerator = new TypeGenerator(module, name, typeof(T));
            typeGenerator.CreateProperty<string>(ContentPropertyName);
            Type generatedType = typeGenerator.CreateType();
            knownTypes.Add(name, generatedType);
            return generatedType;
        }

        /// <summary>
        /// Gets a dynamic type. If no type exists with the given name, new type will be created.
        /// </summary>
        /// <param name="name">Name of the dynamic type</param>
        /// <returns>Dynamic type</returns>
        public static T GetDynamicTypeInstance<T>(string name)
        {
            return TypeGenerator.CreateInstance<T>(GetDynamicType<T>(name));
        }

        #endregion
    }
}
