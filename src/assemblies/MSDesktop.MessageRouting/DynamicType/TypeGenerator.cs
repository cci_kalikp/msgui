﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.CodeDom.Compiler;
using Microsoft.CSharp;
using System.Reflection.Emit;
using System.Threading;
using System.IO; 

namespace MSDesktop.MessageRouting.DynamicType
{
    public class TypeGenerator
    {
        private TypeBuilder typeBuilder;

        public TypeGenerator(ModuleBuilder module, string typeName, Type baseType)
        {
            this.typeBuilder = module.DefineType(typeName, TypeAttributes.Public | TypeAttributes.Class, baseType);
            CustomAttributeBuilder serializableAttributeBuilder = new CustomAttributeBuilder(
                                 typeof(SerializableAttribute).GetConstructor(Type.EmptyTypes),
                                 new object[] { });

            this.typeBuilder.SetCustomAttribute(serializableAttributeBuilder); 
        }

        public void CreateProperty<T>(string propertyName)
        {
            FieldBuilder field = typeBuilder.DefineField("m_" + propertyName, typeof(T), FieldAttributes.Private);

            PropertyBuilder property =
                typeBuilder.DefineProperty(propertyName,
                                 PropertyAttributes.None,
                                 typeof(T),
                                 new Type[] { typeof(T) });

            MethodAttributes GetSetAttr = MethodAttributes.FamANDAssem | MethodAttributes.Public | MethodAttributes.Virtual | MethodAttributes.HideBySig | MethodAttributes.SpecialName;

            MethodBuilder currGetPropMthdBldr =
                typeBuilder.DefineMethod("get_" + propertyName,
                                           GetSetAttr,
                                           typeof(T),
                                           Type.EmptyTypes);

            ILGenerator currGetIL = currGetPropMthdBldr.GetILGenerator();
            currGetIL.Emit(OpCodes.Ldarg_0);
            currGetIL.Emit(OpCodes.Ldfld, field);
            currGetIL.Emit(OpCodes.Ret);

            MethodBuilder currSetPropMthdBldr =
                typeBuilder.DefineMethod("set_" + propertyName,
                                           GetSetAttr,
                                           null,
                                           new Type[] { typeof(T) });

            ILGenerator currSetIL = currSetPropMthdBldr.GetILGenerator();
            currSetIL.Emit(OpCodes.Ldarg_0);
            currSetIL.Emit(OpCodes.Ldarg_1);
            currSetIL.Emit(OpCodes.Stfld, field);
            currSetIL.Emit(OpCodes.Ret);
 
            property.SetGetMethod(currGetPropMthdBldr);
            property.SetSetMethod(currSetPropMthdBldr);
        }

        public Type CreateType()
        {
            return typeBuilder.CreateType();
        }

        public static void SetPropertyValue<T>(object instance, string propertyName, T value)
        {
            PropertyInfo prop = instance.GetType().GetProperty(propertyName);
            prop.SetValue(instance, value, null);
        }

        public static T GetPropertyValue<T>(object instance, string propertyName)
        {
            PropertyInfo prop = instance.GetType().GetProperty(propertyName);
            return (T)prop.GetValue(instance, null);
        }

        public static DynamicBase CreateInstance(Type type)
        {
            if (type.BaseType == typeof(DynamicBase))
            {
                return (DynamicBase)Activator.CreateInstance(type);
            }
            throw new NotSupportedException("Not a subtype of DynamicBase");
        }

        public static T CreateInstance<T>(Type type)
        {
            if (type.BaseType != typeof (T))
            {
                throw new NotSupportedException("Not a subtype of " + typeof (T).FullName);
            }

            return (T)Activator.CreateInstance(type);
        }
    }
}
