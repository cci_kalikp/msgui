﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using MSDesktop.MessageRouting.Message;
using MSDesktop.MessageRouting.Routing;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication;

namespace MSDesktop.MessageRouting
{
    [Serializable]
    internal abstract class RoutedMessage
    {
        internal static int idSequence;
        public int Id { get; set; }

        public Endpoint Origin { get; set; }
        public Endpoint Target { get; set; }

        public int Attempts { get; set; }
        public DateTime LastAttempt { get; set; }  
        public abstract Type GetPayloadType();
    }

    [Serializable]
    internal sealed class RoutedMessage<TMessage> : RoutedMessage, IMessageWithPayload
    { 
        internal static RoutedMessage<TMessage> New(TMessage message_, Endpoint target_, Endpoint origin_)
        {
            return new RoutedMessage<TMessage>() {Id = ++idSequence, Target = target_, Origin = origin_, Message = message_};
        }
        public TMessage Message { get; set; } 
        public override Type GetPayloadType()
        {
            return typeof(TMessage);
        }

        public bool IsPayloadSerializable()
        {
            var payloadType = GetPayloadType();
            return payloadType.IsTypeSerializable();
        }
    }

    [Serializable]
    [InternalMessage]
    [DeliveryStrategy(DeliveryStrategy = DeliveryStrategy.FireAndForget)]
    internal sealed class Ack : IExplicitTarget, IProvideMessageInfo
    {
        public int Id { get; set; }
        public Endpoint Target { get; set; }
        public bool IsAcceptable { get; set; }
        public string ExtraInformation { get; set; }

        public Ack()
        {
            
        }

        public Ack(RoutedMessage message, bool accept_, string extraInformation_)
        {
            this.Id = message.Id;
            this.Target = message.Origin;
            this.IsAcceptable = accept_;
            this.ExtraInformation = extraInformation_;
        }

        public override string ToString()
        {
            return IsAcceptable
                       ? string.Format("[Ack]: message id {0} received by {1}", this.Id, this.Target)
                       : string.Format("[Ack]: message id {0} rejected by {1}", this.Id, this.Target);
        }

        public string GetInfo()
        {
            return this.ToString();
        }
    }
}
