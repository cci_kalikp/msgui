﻿using System;
using System.Collections.Generic;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MSDesktop.MessageRouting.Routing;
using MSDesktop.MessageRouting.ZeroConfig;

namespace MSDesktop.MessageRouting
{
    internal sealed class RoutingCommunicator : IRoutingCommunicator
    {
        private readonly IUnityContainer _container;
        private readonly IPersistenceService _persistenceService;
        private readonly IApplication _application;

        private RouteManager _routeManager;
        private RoutedMessageDispatcher _dispatcher;
        private readonly IEndpointSelector _endpointSelector;

        private bool _commSystemReady;
        private ICommunicator _communicator;
        private readonly List<Action> _actonsPendingCommSystemReady = new List<Action>();

        internal RoutingCommunicator(IUnityContainer container, IPersistenceService persistenceService, IApplication application, IEndpointSelector endpointSelector)
        {
            _application = application;
            _container = container;
            _endpointSelector = endpointSelector;
            _persistenceService = persistenceService;
        }

        internal ICommunicator Communicator
        {
            set
            {
                if (_communicator != null)
                    // allow for multiple calls and ignore them once the comm system is initialized.
                    return;

                lock (_actonsPendingCommSystemReady)
                {
                    _communicator = value;
                    _commSystemReady = _communicator != null;
                    foreach (var action in _actonsPendingCommSystemReady)
                    {
                        action();
                    }

                    _actonsPendingCommSystemReady.Clear();
                }
            }
        }

        internal void Initialize()
        {
            _dispatcher = new RoutedMessageDispatcher(this);
            _routeManager = new RouteManager(this, _endpointSelector, _persistenceService, _application);
            _container.RegisterInstance<IRouteManager>(_routeManager);
        }

        internal void Start()
        {
            _dispatcher.Start();
        }

        public IRoutedPublisher<TMessage> GetPublisher<TMessage>()
        {
            lock (_actonsPendingCommSystemReady)
            {
                var kp = new RoutedPublisher<TMessage>(_endpointSelector, Endpoint.LocalEndpoint, _dispatcher);

                Action setupPublisher = () =>
                {
                    var p = _communicator.GetPublisher<RoutedMessage<TMessage>>();
                    kp.Publisher = p;
                };

                if (_commSystemReady)
                {
                    setupPublisher();
                }
                else
                {
                    _actonsPendingCommSystemReady.Add(setupPublisher);
                }

            kp.MessagePublished += (sender, args) => OnMessagePublished(args);
            kp.MessageDelivered += (sender, args) => OnMessageDelivered(args);
            kp.MessageFailed += (sender, args) => OnMessageFailed(args);
            return kp;
        }
        }

        public IRoutedSubscriber<TMessage> GetSubscriber<TMessage>()
        {
            return GetSubscriber<TMessage>(null);
        }

        public IRoutedSubscriber<TMessage> GetSubscriber<TMessage>(string contentBasedTypeToken)
        {
            var subscriber = new RoutedSubscriber<TMessage>(this, Endpoint.LocalEndpoint);
            Action setupSubscriber = () =>
            {
                var s = _communicator.GetSubscriber<RoutedMessage<TMessage>>();
                subscriber.Subscriber = s;
            };

            if (_commSystemReady)
            {
                setupSubscriber();
            }
            else
            {
                _actonsPendingCommSystemReady.Add(setupSubscriber);
            }

            OnNewSubscription(new NewSubscriptionEventArgs(typeof(TMessage)));
            return subscriber;
        }

        [Obsolete("Use Endpoint.LocalEndpoint instead.")]
        public Endpoint GetLocalEndpoint()
        {
            return Endpoint.LocalEndpoint;
        }

        #region MessagePublishedEvent

        public event EventHandler<MessagePublishedEventArgs> MessagePublished;

        private void OnMessagePublished(MessagePublishedEventArgs eventArgs)
        {
            var holder = MessagePublished;
            if (holder != null)
                holder(this, eventArgs);
        }

        #endregion

        #region MessageDeliveredEvent

        public event EventHandler<MessageDeliveredEventArgs> MessageDelivered;

        private void OnMessageDelivered(MessageDeliveredEventArgs eventArgs)
        {
            var holder = MessageDelivered;
            if (holder != null)
                holder(this, eventArgs);
        }

        #endregion

        #region MessageFailedEvent

        public event EventHandler<MessageFailedEventArgs> MessageFailed;

        private void OnMessageFailed(MessageFailedEventArgs eventArgs)
        {
            var holder = MessageFailed;
            if (holder != null)
                holder(this, eventArgs);
        }

        #endregion

        #region NewSubscriptionEvent

        public event EventHandler<NewSubscriptionEventArgs> NewSubscription;

        internal void AddInternalSubscriber(Type messageType)
        {
            _routeManager.AddInternalRoute(messageType);
        }

        private void OnNewSubscription(NewSubscriptionEventArgs eventArgs)
        {
            var holder = NewSubscription;
            if (holder != null)
                holder(this, eventArgs);
        }

        #endregion
    }
}
