﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Linq;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MSDesktop.MessageRouting.Routing;

namespace MSDesktop.MessageRouting
{
    public class RoutedSubscriber
    {
        private readonly IRoutingCommunicator _routingCommunicator;
        private readonly IRoutedPublisher<Ack> _ackPublisher;
        private static readonly List<Func<object, bool>> MessageFilters = new List<Func<object, bool>>();
        static readonly Dictionary<Endpoint, HashSet<int>> _receivedMessageIds = new Dictionary<Endpoint, HashSet<int>>();
        static readonly Dictionary<Endpoint, int> _receivedOldestMessageIds = new Dictionary<Endpoint, int>();
        static readonly Dictionary<Endpoint, int> _receivedLatestMessageIds = new Dictionary<Endpoint, int>();

        public RoutedSubscriber(IRoutingCommunicator routingCommunicator)
        {
            _routingCommunicator = routingCommunicator;
            _ackPublisher = routingCommunicator.GetPublisher<Ack>();
        }

        internal IRoutedPublisher<Ack> AckPublisher
        {
            get { return _ackPublisher; }
        }

        protected static bool EvaluateMessageFilters(object message)
        {
            return MessageFilters.Any(filter => filter(message));
        }

        internal static void AddMessageFilter(Func<object, bool> filter)
        {
            MessageFilters.Add(filter);
        }

        private const int MaxMessageIdCacheCount = 100000;
        private const int BatchMessageIdCleanupCount = 100;
        internal static bool IsDuplicateMessage(RoutedMessage routedMessage_)
        {
            // todo farm fix this
            return false;


            Endpoint origin = routedMessage_.Origin;
            HashSet<int> hashSet;
            bool? needsCleanup = null;
            lock (_receivedMessageIds)
            {
                if (!_receivedMessageIds.TryGetValue(routedMessage_.Origin, out hashSet))
                {
                    hashSet = new HashSet<int>();
                    _receivedMessageIds.Add(origin, hashSet);
                    _receivedOldestMessageIds[origin] = routedMessage_.Id;
                    needsCleanup = false;
                }
            }
            lock (_receivedLatestMessageIds)
            {
                int latestMessageId;
                if (_receivedLatestMessageIds.TryGetValue(origin, out latestMessageId))
                {
                    if (routedMessage_.Id - latestMessageId >= 10000) //skipped for quite a lot of messages
                    {
                        needsCleanup = true;
                    }
                }
                _receivedLatestMessageIds[origin] = routedMessage_.Id;
            }
            lock (hashSet)
            {
                if (needsCleanup == null && hashSet.Count > MaxMessageIdCacheCount)
                {
                    needsCleanup = true;
                }
                if (needsCleanup == true)
                {
                    lock (_receivedOldestMessageIds)
                    {
                        int oldestMessageId;
                        if (!_receivedOldestMessageIds.TryGetValue(origin, out oldestMessageId))
                        {
                            oldestMessageId = 1;
                        }
                        if (hashSet.Count <= MaxMessageIdCacheCount)
                        {
                            hashSet.Clear();
                            _receivedOldestMessageIds[origin] = routedMessage_.Id;
                        }
                        else
                        {
                            int i = 0;
                            while (i < BatchMessageIdCleanupCount)
                            {
                                if (hashSet.Remove(oldestMessageId++))
                                {
                                    i++;
                                }
                            }
                            _receivedOldestMessageIds[origin] = oldestMessageId;
                        }

                    }
                }
                return !hashSet.Add(routedMessage_.Id);
            } 

        }
    }

    public class RoutedSubscriber<TMessage> : RoutedSubscriber, IRoutedSubscriber<TMessage>
    {
        private readonly Endpoint _localEndPoint;
        private List<Action> wireUpActions = new List<Action>();
        public event EventHandler<FilterMessagEventArgs<TMessage>> FilterMessage;
        internal RoutedSubscriber(IRoutingCommunicator routingCommunicator, Endpoint localEndpoint)
            : base(routingCommunicator)
        {
            _localEndPoint = localEndpoint;
        }

        private IModuleSubscriber<RoutedMessage<TMessage>> subscriber = null;
        internal IModuleSubscriber<RoutedMessage<TMessage>> Subscriber
        {
            get { return this.subscriber; }
            set
            {
                if (this.subscriber != null)
                    return;

                this.subscriber = value;
                foreach (var action in this.wireUpActions)
                {
                    action();
                }
                this.wireUpActions.Clear();
            }
        }

        public IObservable<TMessage> GetObservable()
        {
            var observable = Observable.Create<RoutedMessage<TMessage>>(observer =>
            {
                var disp = new KrakenDisposeHandler();

                if (this.subscriber != null)
                {
                    disp.Disposable = this.Subscriber.GetObservable().Subscribe(observer);
                }
                else
                {
                    Action wireUpAction = () =>
                    {
                        disp.Disposable = this.Subscriber.GetObservable().Subscribe(observer);
                    };

                    this.wireUpActions.Add(wireUpAction);   
                }

                return disp;     
            });

            if (typeof (TMessage) != typeof (Ack))
                observable
                    .Where(routedMessage => _localEndPoint.IsMatch(routedMessage.Target) || EvaluateMessageFilters(routedMessage))
                    .Subscribe(msg =>
                        {
                            string extraInformation;
                            bool accept = AcceptMessage(msg.Message, out extraInformation);
                            AckPublisher.Publish(new Ack(msg, accept, extraInformation));
                        }
                        );


            return observable
                .Where(routedMessage => (_localEndPoint.IsMatch(routedMessage.Target) || EvaluateMessageFilters(routedMessage)) &&
                                        (KrakenConfig.IsServerDiscoveryEnabled || !IsDuplicateMessage(routedMessage)))
                .Select(krakenMessage => krakenMessage.Message);
        }
         

        private bool AcceptMessage(TMessage message_, out string extraInformation_)
        {
            extraInformation_ = null;
            var copy = FilterMessage;
            if (copy == null) return true;
            var arg = new FilterMessagEventArgs<TMessage>(message_);
            copy(this, arg);
            extraInformation_ = arg.ExtraInformation;
            return arg.IsAcceptable;
        }



        private class KrakenDisposeHandler : IDisposable
        {
            public IDisposable Disposable { get; set; }

            public void Dispose()
            {
                if  (this.Disposable != null)
                    this.Disposable.Dispose();
            }
        }
    }
}
