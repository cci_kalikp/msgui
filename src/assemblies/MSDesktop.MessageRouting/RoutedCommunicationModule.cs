﻿using System;
using System.Diagnostics;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication;
using MSDesktop.MessageRouting.Routing;
using MSDesktop.MessageRouting.Routing.Automation;
using MSDesktop.MessageRouting.ZeroConfig;

namespace MSDesktop.MessageRouting
{
    internal sealed class RoutedCommunicationModule : IMSDesktopModule, IEarlyInitializedFrameworkModule
    {
        internal static AppEnvironment MessagingEnvironment = AppEnvironment.Dev;
        private readonly IUnityContainer _container;
        private ICommunicator _communicator;
        private readonly IApplication _application;
        private readonly IPersistenceService _persistenceService;
        private readonly ChromeManagerBase _chromeManager;
        //private readonly Endpoint _localEndpoint;
        private RouteExplorer _routeExplorer;
        private RoutingCommunicator _routingCommunicator;
        private ResolverChainEndpointSelector _endpointSelector;

        public RoutedCommunicationModule(IUnityContainer container, IApplication application,
                                         IPersistenceService persistenceService, IChromeManager chromeManager)
        {
            Endpoint.LocalEndpoint = new Endpoint
            {
                Hostname = Environment.MachineName,
                Application = application.Name,
                Username = Environment.UserName,
                InstanceName = application.GetApplicationInstance(),
            };

            _container = container;
            _application = application;
            _persistenceService = persistenceService;
            _chromeManager = chromeManager as ChromeManagerBase;
        }

        public void Initialize()
        {
            _endpointSelector = new ResolverChainEndpointSelector();
            _routingCommunicator = new RoutingCommunicator(_container, _persistenceService, _application, _endpointSelector);
            _routingCommunicator.Initialize();

            _routeExplorer = new RouteExplorer(_routingCommunicator);

            _container.RegisterInstance<IRoutingCommunicator>(_routingCommunicator);
            _container.RegisterInstance<IEndpointSelector>(_endpointSelector);
            _container.RegisterInstance<IRouteExplorer>(_routeExplorer);

            var preferLocalEndpoints = _container.Resolve<PreferLocalEndpoint>();
            preferLocalEndpoints.Register();


            if (_chromeManager != null)
            {
                _chromeManager.AfterCreateChrome += ChromeCreated;
            }
            else
            {
                InitAndStartCommunicator();
            }

            //this._chromeManager.Ribbon["Message Routing"]["Deferred M2M"].AddWidget(new InitialButtonParameters
            //{
            //    Text = "Initialize",
            //    Click = kick
            //});


            _routeExplorer.SubscriptionAvailableEvent += SubscriptionAvailable;
        }

        private void ChromeCreated(object sender, EventArgs args)
        {
            InitAndStartCommunicator();
            _chromeManager.AfterCreateChrome -= ChromeCreated;
        }

        private void InitAndStartCommunicator()
        {
            _communicator = _container.Resolve<ICommunicator>();
            ((Communicator)_communicator).EnableKrakenIpcImc(MessagingEnvironment);

            _routingCommunicator.Communicator = _communicator;
            _routingCommunicator.Start();
        }

        private void SubscriptionAvailable(object sender, SubscriptionAvailableEventArgs e)
        {
            if (Endpoint.LocalEndpoint.Equals(e.SubscriberIdentity))
            {
                // Treat all communication to and from this endpoint as a default route
                _routingCommunicator.AddInternalSubscriber(e.MessageType);
            }
        }
    }

}
