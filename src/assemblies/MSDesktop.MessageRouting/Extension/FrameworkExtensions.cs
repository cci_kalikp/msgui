﻿using System;
using System.Collections.Generic;
using MSDesktop.MessageRouting;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Core.SmartApi;
using MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging.CPSMessaging;
using MSDesktop.MessageRouting.Routing;

// ReSharper disable CheckNamespace
namespace MorganStanley.MSDotNet.MSGui.Impl.Extensions
// ReSharper restore CheckNamespace
{
    public static class FrameworkExtensions
    {
        public static void EnableMessageRoutingCommunication(this Framework f)
        {
            ExtensionsUtils.RequireAssembly(ExternalAssembly.ReactiveInterfaces, ExternalAssembly.ReactiveCore, ExternalAssembly.ReactiveLinq);
            f.AddModule<RoutedCommunicationModule>();
            CpsModuleMessenger.ThrowOnUnknownType = false;
        }

        public static void EnableMessageRoutingCommunication(this Framework f, bool dispatchExceptionAsFailed_)
        {
            f.EnableMessageRoutingCommunication();
            RoutedMessageDispatcher.ExceptionAsFailed = dispatchExceptionAsFailed_;
        }

        public static void EnableMessageRoutingCommunication(this Framework f, bool dispatchExceptionAsFailed_,
                                                             bool trackInternalMessageEvents_)
        {
            f.EnableMessageRoutingCommunication(dispatchExceptionAsFailed_);
            RoutedMessageDispatcher.TrackInternalMessageEvents = trackInternalMessageEvents_;
        }

        public static void EnableMessageRoutingCommunication(this Framework f, bool dispatchExceptionAsFailed_,
                                                             bool trackInternalMessageEvents_, bool enableCrossPlatformCommunication_)
        {
            f.EnableMessageRoutingCommunication(dispatchExceptionAsFailed_);
            RoutedMessageDispatcher.TrackInternalMessageEvents = trackInternalMessageEvents_;
            f.Bootstrapper.RegisterJsonFormatter = true;
        }
 
        /// <summary>
        /// Sets the inter-process communication status for the communication channel.
        /// </summary>
        /// <param name="framework"></param>
        /// <param name="status"></param>
        public static void SetIpcCommunicationStatus(this Framework framework, CommunicationChannelStatus status)
        {
            framework.Bootstrapper.SetIpcCommunicationStatus(status);
        }

        /// <summary>
        /// Sets the inter-host communication status for the communication channel.
        /// </summary>
        /// <param name="framework"></param>
        /// <param name="status"></param>
        public static void SetImcCommunicationStatus(this Framework framework, CommunicationChannelStatus status)
        {
            if (status != CommunicationChannelStatus.Required && 
                RoutedMessageDispatcher.ExceptionAsFailed == null)
            {
                RoutedMessageDispatcher.ExceptionAsFailed = true;
            }
            framework.Bootstrapper.SetImcCommunicationStatus(status);
        }

        public static void SetCpsConfigFile(this Framework framework, string configFile)
        {
            CpsServiceLoader.CpsConfigFile = PathUtilities.ReplaceEnvironmentVariables(configFile);
        }

        /// <summary>
        /// Enables Kraken's automatic CPS server discovery. When enabled, Kraken will use MSDesktop-hosted CPS servers to discover
        /// possible target applications and connect to the selected target app's server automatically.
        /// 
        /// WARNING: This is an experimental test feature that uses a hardcoded address to an EAI server machine. Expect the server to be 
        /// down at any moment, DO NOT EVEN THINK ABOUT USING THIS IN PROD YET. Follow http://jive.ms.com/groups/kraken for announcements.
        /// </summary>
        /// <param name="f">Framework instance used to expose the setting.</param>
        /// <param name="primaryServer">Address of th server. Example: ms.tcp://foo.bar.ms.com:9992</param>
        /// <param name="isKerberized">A bool indicating if the connection should be kerberized.</param>
        public static void EnableMessageRoutingServerDiscovery(this Framework f, string primaryServer, bool isKerberized)
        {
            KrakenConfig.IsServerDiscoveryEnabled = true;
            KrakenConfig.PrimaryCpsServerHostName = primaryServer;
            KrakenConfig.PrimaryCpsServerIsKerberized = isKerberized;
        }
    }

    public static class KrakenConfig
    {
        static KrakenConfig()
        {
            IsServerDiscoveryEnabled = false;
        }

        public static bool IsServerDiscoveryEnabled { get; internal set; }

        public static string PrimaryCpsServerHostName { get; internal set; }

        public static bool PrimaryCpsServerIsKerberized { get; internal set; }
    }
}
