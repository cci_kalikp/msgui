﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSDesktop.MessageRouting
{
    public sealed class InvalidTargetingException : InvalidOperationException
    {
        public InvalidTargetingException(string message)
            : base(message)
        {
            
        }
    }
}
