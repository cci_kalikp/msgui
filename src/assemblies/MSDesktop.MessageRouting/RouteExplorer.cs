﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MSDesktop.MessageRouting.Routing;
using MSDesktop.MessageRouting.ZeroConfig;

namespace MSDesktop.MessageRouting
{
    internal class RouteExplorer : IRouteExplorer
    {
        private readonly RoutingCommunicator _routingCommunicator;
        private readonly IRoutedPublisher<SubscriptionQueryMessage> _subscriptionQueryPublisher;
        private readonly IRoutedPublisher<SubscriptionQueryReplyMessage> _subscriptionQueryReplyPublisher;
        private readonly IRoutedPublisher<SubscriptionBypassMessage> _subscriptionBypassPublisher;
        private readonly IRoutedPublisher<SubscriptionBypassReplyMessage> _subscriptionBypassReplyPublisher;
        private readonly IRoutedPublisher<UnsubscribeMessage> _unsubscribePublisher;
        private readonly IRoutedPublisher<SubscriptionAvailableMessage> _subscriptionAvailablePublisher;
        private readonly IDictionary<int, ObservableCollection<Endpoint>> _subscriptionQueryResults = new Dictionary<int, ObservableCollection<Endpoint>>();

        // Yes, subscriptionQueryResults leaks. Deal with it.
        private int _subscriptionQueryIdSequence;

        internal readonly List<Type> LocalSubscriptions = new List<Type>();

        public event EventHandler<SubscriptionBypassEventArgs> SubscriptionBypassEvent;
        public event EventHandler<SubscriptionAvailableEventArgs> SubscriptionAvailableEvent;
        public event EventHandler<SubscriptionUnavailableEventArgs> SubscriptionUnavailableEvent;

        internal RouteExplorer(RoutingCommunicator routingCommunicator)
        {
            _routingCommunicator = routingCommunicator;
            _routingCommunicator.NewSubscription += RoutingCommunicatorOnNewSubscription;
            Application.Current.Exit += Application_Exit;

            _routingCommunicator.GetSubscriber<SubscriptionQueryReplyMessage>().GetObservable().Subscribe(ProcessSubscriptionList);
            _routingCommunicator.GetSubscriber<SubscriptionQueryMessage>().GetObservable().Subscribe(ProcessSubscriptionQuery);

            _routingCommunicator.GetSubscriber<SubscriptionBypassReplyMessage>().GetObservable().Subscribe(ProcessSubscriptionBypassReply);
            _routingCommunicator.GetSubscriber<SubscriptionBypassMessage>().GetObservable().Subscribe(ProcessSubscriptionBypass);

            _routingCommunicator.GetSubscriber<SubscriptionAvailableMessage>().GetObservable().Subscribe(ProcessSubscriptionAvailable);
            _routingCommunicator.GetSubscriber<UnsubscribeMessage>().GetObservable().Subscribe(ProcessUnsubscribe);

            _subscriptionQueryPublisher = _routingCommunicator.GetPublisher<SubscriptionQueryMessage>();
            _subscriptionQueryReplyPublisher = _routingCommunicator.GetPublisher<SubscriptionQueryReplyMessage>();

            _subscriptionBypassPublisher = _routingCommunicator.GetPublisher<SubscriptionBypassMessage>();            
            _subscriptionBypassReplyPublisher = _routingCommunicator.GetPublisher<SubscriptionBypassReplyMessage>();

            _subscriptionAvailablePublisher = _routingCommunicator.GetPublisher<SubscriptionAvailableMessage>();
            _unsubscribePublisher = _routingCommunicator.GetPublisher<UnsubscribeMessage>();

            _routingCommunicator.NewSubscription += PublishAvailableSubscription;
        }

        public ObservableCollection<Endpoint> SubscribedApplications { get; private set; }

        public bool IsServerDiscoveryEnabled
        {
            get { return KrakenConfig.IsServerDiscoveryEnabled; }
        }

        private void PublishAvailableSubscription(object sender, NewSubscriptionEventArgs e)
        {
            if (e.MessageType.GetCustomAttributes(true).All(a => a.GetType() != typeof (InternalMessageAttribute)))
            {
                var subscriptionMessage = new SubscriptionAvailableMessage
                    {
                        SubscriberIdentity = Endpoint.LocalEndpoint,
                        Type = e.MessageType
                    };

                _subscriptionAvailablePublisher.Publish(subscriptionMessage);
            }
        }

        private void RoutingCommunicatorOnNewSubscription(object sender, NewSubscriptionEventArgs e)
        {
            LocalSubscriptions.Add(e.MessageType);
        }

        private void ProcessSubscriptionAvailable(SubscriptionAvailableMessage subscriptionMessage)
        {
            var handler = SubscriptionAvailableEvent;
            if (handler != null)
            {
                var args = new SubscriptionAvailableEventArgs(subscriptionMessage.Type, subscriptionMessage.SubscriberIdentity);
                handler(this, args);
            }
        }

        private void ProcessSubscriptionUnavailable(UnsubscribeMessage subscriptionMessage)
        {
            var handler = SubscriptionUnavailableEvent;
            if (handler != null)
            {
                var args = new SubscriptionUnavailableEventArgs( subscriptionMessage.Origin);
                handler(this, args);
            }
        }

        private void ProcessSubscriptionQuery(SubscriptionQueryMessage query)
        {
            if (query.MessageType != null)
            {
                if (!LocalSubscriptions.Any(ks => ks.FullName.Equals(query.MessageType)))
                    return;
            }

            var reply = new SubscriptionQueryReplyMessage
            {
                SubscriberIdentity = Endpoint.LocalEndpoint,
                Target = query.Origin,
                QueryId = query.QueryId
            };

            if (KrakenConfig.IsServerDiscoveryEnabled)
            {
                reply.SubscriberIdentity.CpsServer = KrakenConfig.PrimaryCpsServerHostName;
                reply.SubscriberIdentity.Kerberosed = KrakenConfig.PrimaryCpsServerIsKerberized;
            }

            _subscriptionQueryReplyPublisher.Publish(reply);
        }

        private void ProcessSubscriptionList(SubscriptionQueryReplyMessage subscriptionListMessage)
        {
            ObservableCollection<Endpoint> resultSet;
            if (_subscriptionQueryResults.TryGetValue(subscriptionListMessage.QueryId, out resultSet))
            {
                lock (resultSet)
                {
                    if (!resultSet.Contains(subscriptionListMessage.SubscriberIdentity))
                    {
                        resultSet.Add(subscriptionListMessage.SubscriberIdentity);
                    }
                }
            }
        }

        private void ProcessSubscriptionBypass(SubscriptionBypassMessage bypass)
        {
            var reply = new SubscriptionBypassReplyMessage
            {
                SubscriberIdentity = Endpoint.LocalEndpoint,
                Target = bypass.Origin,
                QueryId = bypass.QueryId
            };

            reply.Status = !LocalSubscriptions.Any(ks => ks.FullName.Equals(bypass.MessageType)) ? DeliveryStatus.Failed : DeliveryStatus.Delivered;

            _subscriptionBypassReplyPublisher.Publish(reply);
        }

        private void ProcessSubscriptionBypassReply(SubscriptionBypassReplyMessage bypassReply)
        {
            if(bypassReply.Status == DeliveryStatus.Delivered)
            {
                var resultSet = _subscriptionQueryResults[_subscriptionQueryIdSequence];
                var r = resultSet.FirstOrDefault(e => e.Equals(bypassReply.SubscriberIdentity));

                if (r == null)
                {
                    resultSet.Add(bypassReply.SubscriberIdentity);
                }
            }

            OnSubscriptionBypassEvent(
                new SubscriptionBypassEventArgs(bypassReply.SubscriberIdentity, bypassReply.Status));
        }

        public void BypassSubscribedApplication(string hostName, string application,string instanceName, Type messageType)
        {
            var bypass = new SubscriptionBypassMessage
            {
                Origin = Endpoint.LocalEndpoint,
                Target = new Endpoint { Hostname = hostName, Application = application, InstanceName = instanceName },
                MessageType = messageType.FullName,
                QueryId = _subscriptionQueryIdSequence
            };

            var deliveryInfo = _subscriptionBypassPublisher.Publish(bypass);
            deliveryInfo.PropertyChanged += DeliveryInfo_PropertyChanged;
        }

        private void DeliveryInfo_PropertyChanged(object sender, PropertyChangedEventArgs eventArgs)
        {
            var deliveryInfo = (DeliveryInfo)sender;

            switch (eventArgs.PropertyName)
            {
                case "Status":
                    switch (deliveryInfo.Status)
                    {
                        case DeliveryStatus.Failed:
                            OnSubscriptionBypassEvent(
                                new SubscriptionBypassEventArgs(deliveryInfo.Target, DeliveryStatus.Failed));
                            deliveryInfo.PropertyChanged -= DeliveryInfo_PropertyChanged;
                            break;
                        case DeliveryStatus.Delivered:
                            deliveryInfo.PropertyChanged -= DeliveryInfo_PropertyChanged;
                            break;
                    }
                    break;
            }
        }

        public ObservableCollection<Endpoint> GetSubscribedApplications(Type messageType)
        {
            var result = new ObservableCollection<Endpoint>();

            var sqm = new SubscriptionQueryMessage
            {
                Origin = Endpoint.LocalEndpoint,
                MessageType = messageType.FullName,
                QueryId = ++_subscriptionQueryIdSequence
            };

            _subscriptionQueryResults.Add(_subscriptionQueryIdSequence, result);
            _subscriptionQueryPublisher.Publish(sqm);

            return result;
        }

        private void Application_Exit(object sender, ExitEventArgs e)
        {
            _unsubscribePublisher.Publish(
                new UnsubscribeMessage
                {
                    Origin = Endpoint.LocalEndpoint
                });
        }

        private void ProcessUnsubscribe(UnsubscribeMessage message)
        {
            if (!_subscriptionQueryResults.ContainsKey(_subscriptionQueryIdSequence))
                return;

            var resultSet = _subscriptionQueryResults[_subscriptionQueryIdSequence];
            var r = resultSet.FirstOrDefault(e => e.Equals(message.Origin));
            resultSet.Remove(r);
            ProcessSubscriptionUnavailable(message);
        }

        #region SubscriptionBypassEvent

        public void OnSubscriptionBypassEvent(SubscriptionBypassEventArgs eventArgs)
        {
            var handler = SubscriptionBypassEvent;
            if (handler != null)
                handler(this, eventArgs);
        }

        #endregion


        public ObservableCollection<Endpoint> GetAllSubscribedApplications()
        {
            var result = new ObservableCollection<Endpoint>();

            var sqm = new SubscriptionQueryMessage
            {
                Origin = Endpoint.LocalEndpoint,
                QueryId = ++_subscriptionQueryIdSequence
            };

            _subscriptionQueryResults.Add(_subscriptionQueryIdSequence, result);
            _subscriptionQueryPublisher.Publish(sqm);

            return result;
        }
    }
}
