﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.My;

namespace MSDesktop.MessageRouting.Routing
{
    internal class ResolverChainEndpointSelector : IEndpointSelector
    {
        private static readonly IMSLogger _logger = MSLoggerFactory.CreateLogger<ResolverChainEndpointSelector>();
        private readonly Dictionary<int, TargetingStep> TargetingSteps = new Dictionary<int, TargetingStep>();
        private Stack<TargetingStep> masterChain = null;
        private int maximumAttempts = 1;

        public ResolverChainEndpointSelector()
        {
            this.AddDefaultChainElements();
        }

        private void AddDefaultChainElements()
        {
            // Error handler for failed route selection
            AddEndpointSelectionStep(0, this.TargetingFailedStep);

            // IBypassRouting support
            AddEndpointSelectionStep(3000, (chain) =>
            {
                var bypassMessage = chain.Message as IExplicitTarget;
                if (bypassMessage != null)
                {
                    return new EndpointSelection { Endpoint = bypassMessage.Target, Persist = false };
                }

                var legacyBypassMessage = chain.Message as IBypassRouting;
                if (legacyBypassMessage != null)
                {
                    return new EndpointSelection { Endpoint = legacyBypassMessage.Target, Persist = false };
                }

                return chain.Next();
            });

            // broadcast support
            AddEndpointSelectionStep(3001, (chain) =>
            {
                if (chain.Message.GetType().GetCustomAttributes(typeof(BroadcastAttribute), false).Length > 0)
                {
                    return new EndpointSelection { Endpoint = new Endpoint { Username = Endpoint.LocalEndpoint.Username }, Persist = false };
                }

                return chain.Next();
            });

            /*
             * kraken automatic cps server discovery
             * 
             * If enabled, send all marked messages (subscription queries and replies) through the MSDesktop hosted CPS instances,
             * this will be a common server for all apps using this feature.
             */
            if (KrakenConfig.IsServerDiscoveryEnabled)
            {
                AddEndpointSelectionStep(3500, (chain) =>
                {
                    if (chain.Message.GetType().GetCustomAttributes(typeof(ManagementMessageAttribute), false).Length > 0)
                    {
                        return new EndpointSelection
                        {
                            Endpoint = new Endpoint
                            {
                                Username = Endpoint.LocalEndpoint.Username,
                                CpsServer = KrakenConfig.PrimaryCpsServerHostName,
                                Kerberosed = KrakenConfig.PrimaryCpsServerIsKerberized
                            }, 
                            Persist = false
                        };
                    }

                    return chain.Next();
                });
            }

            AddEndpointSelectionStep(3999, chain =>
            {
                var endpoint = chain.Next();
                if (endpoint == null)
                    return null;

                endpoint.Endpoint.Username = Endpoint.LocalEndpoint.Username;
                return endpoint;
            });

            // fallback infinite loop guard
            AddEndpointSelectionStep(4000, (chain) => chain.Attempt > this.MaximumAttempts ? null : chain.Next());
        }

        public void AddEndpointSelectionStep(int priority, TargetingStep step)
        {
            if (TargetingSteps.ContainsKey(priority))
            {
                throw new InvalidOperationException("Priority must be unique.");
            }

            TargetingSteps.Add(priority, step);

            RebuildMasterChain();
        }

        private void RebuildMasterChain()
        {
            //ReSharper disable ForCanBeConvertedToForeach
            var stack = new Stack<TargetingStep>();

            /* We store the master chain in reverse, because when we copy it (ResolveTarget getter) the copy ctor 
             * reverses (due to IEnumerable doing pop/yield cycles on Stack<T>).
             */
            var keys = TargetingSteps.Keys.OrderByDescending(x => x).ToArray();
            for (
                int i = 0; i < keys.Length; i++) //foreach does not guarantee in-order iteration!
            {
                stack.Push(TargetingSteps[keys[i]]);
            }

            masterChain = (stack.Count == 0) ? null : stack;
        }
 
        private EndpointSelection TargetingFailedStep(EndpointSelectorChain chain)
        {
            _logger.Error(string.Format("Route selection failed for message type \"{0}\".", chain.Message.GetType().AssemblyQualifiedName));
            return null;
        }

        public EndpointSelection ResolveTarget(object message, int attempt)
        {
            if (null == message)
            {
                _logger.Error("Route selection failed for null message.");
            }

            if (null == masterChain)
            {
                throw new InvalidOperationException("Message routing has not been initialized properly: endpoint selection chain empty.");
            }

            var chain = new EndpointSelectorChain
            {
                Attempt = attempt,
                Chain = new Stack<TargetingStep>(masterChain),
                Message = message
            };

            return chain.Execute();
        }


        public int MaximumAttempts
        {
            get { return maximumAttempts; }
            set { maximumAttempts = value; }
        }
    }
}
