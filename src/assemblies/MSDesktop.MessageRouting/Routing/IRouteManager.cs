﻿using System;
using System.Collections.Generic;

namespace MSDesktop.MessageRouting.Routing
{
    public interface IRouteManager
    {
        /// <summary>
        /// Gets or sets whether MSDesktop prefers in-app subscriptions.
        /// </summary>
        bool UseDefaultRouting { get; set; } 

        ICollection<Route> DefaultRoutes { get; }

        /// <summary>
        /// Returns if a route exists.
        /// </summary>
        /// <typeparam name="TMessage">The message type to route</typeparam>
        /// <returns></returns>
        bool RouteExists<TMessage>();

        /// <summary>
        /// Adds route value or updates if route exists with the given TMessage
        /// </summary>
        /// <typeparam name="TMessage">The message type to route</typeparam>
        /// <param name="origin"></param>
        /// <param name="target"></param>
        void AddOrUpdateRoute<TMessage>(Endpoint origin, Endpoint target);  

        /// <summary>
        /// 
        /// </summary>
        IEnumerable<Route> Routes { get; }

        bool? EnableAutoSelect { get; set; }
        uint AutoSelectTimeout { get; set; }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="routes"></param>
        void ReplaceAllRoutes(IEnumerable<Route> routes);
    }
     
}
