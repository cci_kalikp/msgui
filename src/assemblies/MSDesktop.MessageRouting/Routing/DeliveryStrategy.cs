﻿namespace MSDesktop.MessageRouting.Routing
{
    public enum DeliveryStrategy
    {
        FireAndForget = 1,
        Tracked = 2
    }
}
