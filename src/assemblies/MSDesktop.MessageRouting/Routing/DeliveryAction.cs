﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSDesktop.MessageRouting.Routing
{
    public enum DeliveryAction
    {
        DoNotSend = 1,
        Send = 2, 
        Die = 3,
        Wait = 4
    }
}
