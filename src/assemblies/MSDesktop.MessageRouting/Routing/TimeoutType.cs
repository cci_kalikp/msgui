﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSDesktop.MessageRouting.Routing
{
    /// <summary>
    /// Specifies the timeout behaviour
    /// </summary>
    public enum TimeoutType
    {
        /// <summary>
        /// Use the same timeout value for consecutive attempts
        /// </summary>
        Constant = 1, 

        /// <summary>
        /// Use double the previous timeout for consecutive attempts - 5s, 10s, 20s, 40s, etc.
        /// </summary>
        Exponential = 2
    }
}
