﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSDesktop.MessageRouting.Routing
{
    /// <summary>
    /// Marks a method on a message to be used for grouping messages at fallback.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class FallbackGroupComparatorAttribute : Attribute
    {
    }
}
