﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSDesktop.MessageRouting.Routing
{
    [AttributeUsage(AttributeTargets.Class)]
    internal class BroadcastAttribute : Attribute
    {
        public bool MachineLocal { get; set; }

        public BroadcastAttribute(bool machineLocal = false)
        {
            MachineLocal = machineLocal;
        }
    }
}
