﻿using System;
using MSDesktop.MessageRouting;

namespace MSDesktop.MessageRouting.Routing
{
    [Obsolete("Please use MSDesktop.MessageRouting.Routing.IExplicitTarget instead.")]
    public interface IBypassRouting
    {
        Endpoint Target { get; }
    }
}
