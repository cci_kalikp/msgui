﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MSDesktop.MessageRouting.Persistence;
using MSDesktop.MessageRouting.Routing.Automation;

namespace MSDesktop.MessageRouting.Routing
{
    /// <summary>
    /// This class implements ze Kraken routing mechanism.
    /// </summary>
    public class RouteManager : IRouteManager
    {
        private readonly IRoutingCommunicator _routingCommunicator;
        private readonly IEndpointSelector _endpointSelector;
        private readonly ConcurrentDictionary<Type, Route> _defaultRoutes = new ConcurrentDictionary<Type, Route>();
        private readonly ConfigStore _configStore;

        public event EventHandler<RouteEventArgs> RouteUpdateAvailable;
        private void OnRouteUpdateAvailable(Route r)
        {
            var copy = this.RouteUpdateAvailable;
            if (copy != null)
            {
                copy(this, new RouteEventArgs{Route = r});
            }
        }

        /// <summary>
        /// Constructs an instance of RouteManager. 
        /// </summary>
        internal RouteManager(IRoutingCommunicator routingCommunicator, IEndpointSelector endpointSelector, IPersistenceService persistenceService, IApplication application, bool routeByDefault = true)
        {
            _routingCommunicator = routingCommunicator;
            _endpointSelector = endpointSelector;
            _routingCommunicator.NewSubscription += NewSubscription;
            _configStore = new ConfigStore(persistenceService);
            UseDefaultRouting = routeByDefault;

            //// Get the isolated subsystem settings, if we are running in an isolated subsystem context
            //var isolatedApplication = application as IsolatedApplication;
            //if (isolatedApplication != null)
            //{
            //    _isolatedSubsystemSettings = isolatedApplication.IsolatedSubsystemSettings;
            //}

            //// Subscribe to route update events
            //var subscriber = routingCommunicator.GetSubscriber<SubsystemRouteMessage>();
            //subscriber.GetObservable().Subscribe(InternalRouteAdded);

            //_subsystemRoutePublisher = routingCommunicator.GetPublisher<SubsystemRouteMessage>();

            endpointSelector.AddEndpointSelectionStep(2000, EndpointSelectionStep);
        }

        public bool UseDefaultRouting
        {
            get { return PreferLocalEndpoint.IsEnabled; }
            set { PreferLocalEndpoint.IsEnabled = value; }
        } 

        public ICollection<Route> DefaultRoutes
        {
            get { return _defaultRoutes.Values; }
        }
         
        private EndpointSelection EndpointSelectionStep(EndpointSelectorChain chain)
        {
            if (_configStore.Value != null && _configStore.Value.Routes != null)
            {
                var messageType = chain.Message.GetType();
                var selectedRoute =
                    _configStore.Value.Routes.FirstOrDefault(a => a.MessageTypeIdentifier.Equals(messageType.FullName));
                if (selectedRoute != null && !string.IsNullOrEmpty(selectedRoute.Target.InstanceName))
                {
                    int processId;
                    if (Int32.TryParse(selectedRoute.Target.InstanceName, out processId))
                    {
                        try
                        {
                            Process.GetProcessById(processId, selectedRoute.Target.Hostname);
                        }
                        catch
                        {
                            selectedRoute = null;
                        }
                    }
                }
                if (selectedRoute == null || chain.Attempt > 0) //if this is not the first attempt, the route we chose failed. let's try downstream (usually involving a GUI) again.
                {
                    Route route;
                    if (UseDefaultRouting && _defaultRoutes.TryGetValue(messageType, out route))
                    {
                        return new EndpointSelection { Endpoint = route.Target, Persist = false };
                    }

                    var downstream = chain.Next();

                    if (downstream != null && downstream.Endpoint != null)
                    {

                        if (downstream.Persist)
                        {
                            var newRoute = new Route
                            {
                                Origin = Endpoint.LocalEndpoint,
                                Target = downstream.Endpoint, // server discovery: if downstream filled in host/kerberos, we will save it.
                                MessageTypeIdentifier = messageType.FullName
                            };

                            AddOrUpdateRoute(newRoute.Origin, newRoute.Target, messageType);
                        }

                        return new EndpointSelection { Endpoint = downstream.Endpoint, Persist = false };
                    }

                    return null;
                }
                 


                return new EndpointSelection { Endpoint = selectedRoute.Target };
            }

            // if the RouteManager is not properly set up exclude it from the routing process.
            return chain.Next();
        }
        public bool RouteExists<TMessage>()
        {
            if (_configStore.Value != null && _configStore.Value.Routes != null)
            {
                return _configStore.Value.Routes.Exists(a => a.MessageTypeIdentifier.Equals(typeof(TMessage).FullName));
            }

            return false;
        }
        public void AddOrUpdateRoute<TMessage>(Endpoint origin, Endpoint target)
        {
            AddOrUpdateRoute(origin, target, typeof(TMessage));
        }
        
        private void AddOrUpdateRoute(Endpoint origin, Endpoint target, Type messageType_)
        {
            if (_configStore.Value != null && _configStore.Value.Routes != null)
            {
                var route =
                    _configStore.Value.Routes.FirstOrDefault(
                        a => a.MessageTypeIdentifier.Equals(messageType_.FullName));
                if (route != null)
                {
                    _configStore.Value.Routes.Remove(route); 
                }
                AddRoute(origin, target, messageType_);
            }
        }
         

        internal void AddInternalRoute(Type messageType)
        {
            var route = new Route
            {
                Origin = Endpoint.LocalEndpoint,
                Target = Endpoint.LocalEndpoint,
                CreatedAt = DateTime.Now,
                MessageTypeIdentifier = messageType.FullName
            };

            _defaultRoutes.TryAdd(messageType, route);
            SendRouteUpdate(route);
        }

         
        //private void InternalRouteAdded(SubsystemRouteMessage routeMessage)
        //{
        //    // Add the route only if it's coming from our host, or one of our isolated processes
        //    bool shouldAddRoute;

        //    if (_isolatedSubsystemSettings != null)
        //    {
        //        // We are in context of isolated subsystem
        //        shouldAddRoute = (_isolatedSubsystemSettings.HostProcessId == routeMessage.HostProcessId);
        //    }
        //    else
        //    {
        //        shouldAddRoute =
        //            ProcessIsolationExtensions.RunningIsolatedProcesses.Contains(routeMessage.IsolatedProcessId);
        //    }

        //    if (shouldAddRoute)
        //    {

        //        Type type = Type.GetType(routeMessage.MessageTypeIdentifier);
        //        if (type == null) return;
        //        var route = new Route
        //            {
        //                Origin = routeMessage.Origin,
        //                Target = routeMessage.Target,
        //                MessageTypeIdentifier = routeMessage.MessageTypeIdentifier,
        //                CreatedAt = DateTime.Now
        //            };

        //        UseDefaultRouting = routeMessage.RouteByDefault;

        //        _defaultRoutes.TryAdd(type, route);
        //    }
        //}

        private void AddRoute(Endpoint origin, Endpoint target, Type messageType_)
        {
            var routeIdentifier = messageType_.FullName;
            var route = new Route
            {
                MessageTypeIdentifier = routeIdentifier,
                Origin = origin,
                Target = target,
                CreatedAt = DateTime.Now
            };

            _configStore.Value.Routes.Add(route);
            SendRouteUpdate(route);
        }

        private void SendRouteUpdate(Route route)
        {
            this.OnRouteUpdateAvailable(route);
        }

        public IEnumerable<Route> Routes
        {
            get
            {
                if (_configStore != null && _configStore.Value != null)
                {
                    return _configStore.Value.Routes;
                }

                return null;
            }
        }

        public bool? EnableAutoSelect
        {
            get
            {
                if (_configStore != null && _configStore.Value != null)
                {
                    return _configStore.Value.EnableAutoSelect;
                }
                return false;
            }
            set
            {
                if (_configStore != null && _configStore.Value != null)
                {
                    _configStore.Value.EnableAutoSelect = value;
                }
            }
        }

        public uint AutoSelectTimeout
        {
            get
            {
                if (_configStore != null && _configStore.Value != null)
                {
                    return _configStore.Value.AutoSelectTimeout;
                }
                return 0;
            }
            set
            {
                if (_configStore != null && _configStore.Value != null)
                {
                    _configStore.Value.AutoSelectTimeout = value;
                }
            }
        }

        public void ReplaceAllRoutes(IEnumerable<Route> routes)
        {
            if (_configStore != null)
            {
                if (_configStore.Value == null)
                {
                    _configStore.Value = new ConfigStoreValue();
                }

                _configStore.Value.Routes = new List<Route>(routes);
            }
        }

        private void NewSubscription(object sender, NewSubscriptionEventArgs e)
        {
            if (e.MessageType.GetInterfaces().Contains(typeof(IExplicitTarget)))
                return;

            if (e.MessageType.GetInterfaces().Contains(typeof(IBypassRouting)))
                return;

            if (e.MessageType.GetCustomAttributes(typeof(InternalMessageAttribute), false).FirstOrDefault() != null)
                return;

            if (e.MessageType.GetCustomAttributes(typeof(BroadcastAttribute), false).FirstOrDefault() != null)
                return;

            var route = new Route
                {
                    Origin = Endpoint.LocalEndpoint,
                    Target = Endpoint.LocalEndpoint,
                    MessageTypeIdentifier = e.MessageType.FullName
                };

            _defaultRoutes.TryAdd(e.MessageType, route);
            SendRouteUpdate(route);
        }
    }

    public sealed class RouteEventArgs : EventArgs
    {
        public Route Route { get; set; }
    }
}
