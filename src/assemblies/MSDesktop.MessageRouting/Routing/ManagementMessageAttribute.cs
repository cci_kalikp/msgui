﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSDesktop.MessageRouting.Routing
{
    /// <summary>
    /// Used to mark internal Kraken messages that are to be sent to the Kraken management servers.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    internal class ManagementMessageAttribute : Attribute
    {
        public bool MachineLocal { get; set; }

        public ManagementMessageAttribute(bool machineLocal = false)
        {
            MachineLocal = machineLocal;
        }
    }
}
