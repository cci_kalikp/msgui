﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSDesktop.MessageRouting.Routing
{
    /// <summary>
    /// Message with this attribute won't be published outside of the library
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class InternalMessageAttribute : Attribute
    {

    }
}
