﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MSDesktop.MessageRouting.Routing
{
    [AttributeUsage(AttributeTargets.Class)]
    public class DeliveryStrategyAttribute : Attribute
    {
        public const int DefaultTimeout = 3000;
        public DeliveryStrategy DeliveryStrategy { get; set; }
        public int RetryCount { get; set; }
        public int Timeout { get; set; }
        public TimeoutType TimeoutType { get; set; }
        public bool Fallback { get; set; }
 

        public DeliveryStrategyAttribute()
        {
            this.DeliveryStrategy = DeliveryStrategy.Tracked;
            this.Fallback = true;
            this.RetryCount = 1;
            this.Timeout = DefaultTimeout;
            this.TimeoutType = TimeoutType.Constant; 
        } 
    }
     
}
