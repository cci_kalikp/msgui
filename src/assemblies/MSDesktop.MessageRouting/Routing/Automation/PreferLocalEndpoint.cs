﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MSDesktop.MessageRouting.Routing.Automation
{
    /// <summary>
    /// Performs automatic routing for in-app subscriptions. 
    /// </summary>
    public sealed class PreferLocalEndpoint
    {
        internal static bool IsEnabled = true;

        private IEndpointSelector endpointSelector;
        private IRoutingCommunicator communicator;
        private readonly List<Type> _defaultRoutedTypes = new List<Type>();

        public PreferLocalEndpoint(IEndpointSelector endpointSelector, IRoutingCommunicator communicator)
        {
            this.endpointSelector = endpointSelector;
            this.communicator = communicator;

            this.communicator.NewSubscription += communicator_NewSubscription;
        }

        private void communicator_NewSubscription(object sender, NewSubscriptionEventArgs e)
        {
            this._defaultRoutedTypes.Add(e.MessageType);
        }

        /// <summary>
        /// Registers the endpoint selection step that prefers local in-app endpoints when available.
        /// </summary>
        /// <param name="priority">The priority in the resolver logic. Defaulted to 2100, 100 above the RouteManager.</param>
        public void Register(int priority = 2100)
        {
            this.endpointSelector.AddEndpointSelectionStep(priority, this.EndpointSelectionStep);
        }

        private EndpointSelection EndpointSelectionStep(EndpointSelectorChain chain)
        {
            if (IsEnabled 
                && chain.Attempt == 0 
                && _defaultRoutedTypes.Contains(chain.Message.GetType()))
            // we attempt default routing only once
            {
                return new EndpointSelection 
                {
                    Endpoint = Endpoint.LocalEndpoint, 
                    Persist = false 
                };
            }

            return chain.Next();
        }
    }
}
