﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Xml.Serialization;
namespace MSDesktop.MessageRouting.Routing
{
    [XmlRoot("Route")]
    public class Route
    {
        //[XmlIgnore]
        //public Type MessageType { get; set; }

        [XmlElement("MessageType")]
        public string MessageTypeIdentifier { get; set; }

        [XmlElement("Origin")]
        public Endpoint Origin { get; set; }

        [XmlElement("Target")]
        public Endpoint Target { get; set; }

        [XmlElement("CreatedAt")]
        public DateTime? CreatedAt { get; set; }
    }
}
