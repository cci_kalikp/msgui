﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using MorganStanley.MSDotNet.MSGui.Core;
using System.Xml.Linq;

namespace MSDesktop.MessageRouting.Persistence
{
    public class ConfigStore
    {
        //Kraken_Connections_GUID to make sure it is unique 
        private const string myPersistorName = "Kraken_Connections_CB282A68_D462_4746_9C4E_DC964D984E6F";
        private static XmlSerializer xmlSerializer = new XmlSerializer(typeof(ConfigStoreValue));

        internal string PersistorName
        {
            get
            {
                return String.Format("{0}_{1}", myPersistorName, Environment.MachineName);
            }
        }

        private readonly IPersistenceService persistenceService;

        public ConfigStore(IPersistenceService persistenceService)
        {
            this.persistenceService = persistenceService;
            this.persistenceService.AddGlobalPersistor(PersistorName, RestoreCustomState, SaveCustomState);
            this.Value = new ConfigStoreValue();
        }

        public ConfigStoreValue Value { get; set; }

        private XDocument SaveCustomState()
        {
            var doc = new XDocument();
            using (var writer = doc.CreateWriter())
            {
                xmlSerializer.Serialize(writer, this.Value);
            }

            return doc;
        }

        private void RestoreCustomState(XDocument state)
        {
            using (var reader = state.Root.CreateReader())
            {
                this.Value = (ConfigStoreValue)xmlSerializer.Deserialize(reader);
            }

        }

    }
}
