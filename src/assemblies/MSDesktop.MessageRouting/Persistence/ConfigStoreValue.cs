﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace MSDesktop.MessageRouting.Persistence
{
    [XmlRoot("ConfigStoreValue")]
    public class ConfigStoreValue
    {
        [XmlArray("Routes")]
        [XmlArrayItem("Route")]
        public List<Routing.Route> Routes { get; set; }


        [XmlElement(IsNullable = true)]
        public bool? EnableAutoSelect { get; set; }

        public uint AutoSelectTimeout { get; set; }

        public ConfigStoreValue()
        {
            this.Routes = new List<Routing.Route>();
        }
    }
}
