﻿using System;

namespace MorganStanley.Desktop.EntityServices.Extensions
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class M2MAttribute : Attribute
    {
    }
}
