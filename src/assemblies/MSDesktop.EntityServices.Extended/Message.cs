﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.Desktop.EntityServices.Entities;
using MorganStanley.Desktop.EntityServices.Keys;

namespace MorganStanley.Desktop.EntityServices.Extensions
{
    [Serializable]
    [M2M]
    public abstract class Message : IEntity<Message, BasicKey<Message>>, IPublishable, ISubscribable
    {
        private readonly BasicKey<Message> _key;
        private readonly DateTime _timestamp;

        protected Message(DateTime? timestamp = null, BasicKey<Message> key = null)
        {
            _key = key ?? new BasicKey<Message>(Guid.NewGuid().ToString());
            _timestamp = timestamp ?? DateTime.UtcNow;
        }

        public DateTime Timestamp { get { return _timestamp; } }
        IKey IEntity.Key { get { return Key; } }
        public BasicKey<Message> Key { get { return _key; } }
    }
}
