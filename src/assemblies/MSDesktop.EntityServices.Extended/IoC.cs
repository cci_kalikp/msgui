﻿using System;
using Microsoft.Practices.Unity;
using MorganStanley.Desktop.EntityServices.Extensions.DataConnectors;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;

namespace MorganStanley.Desktop.EntityServices.Extensions
{
    public static class IoC
    {
        public static IUnityContainer RegisterM2MDataProvider<TMessage>(this IUnityContainer container) where TMessage: class
        {
            var messageType = typeof(TMessage);
            return container.RegisterM2MDataProvider(messageType);
        }

        public static IUnityContainer RegisterM2MDataProvider(this IUnityContainer container, Type messageType)
        {
            container.RegisterType(
                typeof(IM2MDataConnector<>).MakeGenericType(messageType),
                typeof(M2MDataConnector<>).MakeGenericType(messageType),
                new ContainerControlledLifetimeManager(),
                new InjectionConstructor(new ResolvedParameter(typeof(ICommunicator))));

            container.RegisterEntityService(
                messageType,
                typeof (IM2MDataConnector<>).MakeGenericType(messageType),
                typeof (IPassThroughEntityServiceConverter<>).MakeGenericType(messageType));

            return container;
        }
    }
}