﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using MorganStanley.Desktop.EntityServices.Queries;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;

namespace MorganStanley.Desktop.EntityServices.Extensions.DataConnectors
{
    internal class M2MDataConnector<TMessage> : IM2MDataConnector<TMessage> where TMessage : class
    {
        private readonly ICommunicator _communicator;
        private readonly IModulePublisher<TMessage> _publisher;

        private readonly IModuleSubscriber<TMessage> _subscriber;
       

        public M2MDataConnector(ICommunicator communicator)
        {
            _communicator = communicator;
            _publisher = _communicator.GetPublisher<TMessage>();
            _subscriber = _communicator.GetSubscriber<TMessage>();
        }

        public IEnumerable<object> Get(IEnumerable<object> nativeQueries)
        {
            throw new NotSupportedException("Get is not currently a supported operation by the M2M provider");
        }

        public IObservable<IEnumerable<object>> GetObservable(IEnumerable<object> nativeQueries)
        {
            var globalQueries = nativeQueries.Select(x => x as GlobalQuery<TMessage>);
            if (globalQueries.Any(x => x == null))
            {
                throw new NotSupportedException("Only global queries are currently supported by the M2M data connector");
            }
            return _subscriber.GetObservable().Select(message => new[] { (object)message });
        }

        public void Publish(IEnumerable<object> nativeObjects)
        {
            var messages = nativeObjects.Select(x => x as TMessage);
            if (messages.Any(x => x == null))
            {
                throw new ArgumentException(
                    string.Format(
                        "A message published was not of the expected type - {0}", 
                        typeof(TMessage).FullName));
            }
            foreach (var message in messages)
            {
                _publisher.Publish(message);
            }
        }

        public void Delete(IEnumerable<object> nativeQueries)
        {
            throw new NotSupportedException("Delete is not currently a supported operation by the M2M provider");
        }
    }
}
