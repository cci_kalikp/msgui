﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.Threading;
using System.Windows;
using MorganStanley.MSDotNet.MSGui.Core.SmartApi;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication;
using MorganStanley.MSDotNet.MSGui.Impl.ProcessIsolation;
using MorganStanley.MSDotNet.Runtime;
using MSDesktop.Isolation.Interfaces;

namespace MSDesktop.Isolation
{
    internal sealed class XProcEndpointManager : IDisposable
    {
        private const string RelativeExecutableDistLocation = @"..\bin\MSDesktop.Isolation\";

        private static int _addinCounter;
        private static int _instanceCount;

        private readonly IsolatedSubsystemSite _site;
        private readonly string _isolatingExecutableLocation;
        private readonly string _additionalCommandLine;
        private readonly string _hostExecutable;
        private readonly string _isolatingApplicationKey;
        private readonly string _addinSiteUri;
        private readonly PackageDefinition _packageDefinition;
        private readonly int _currentInstanceNumber;

        private ServiceHost _serviceHost;


        private XProcEndpointManager(IMSDesktopSubsystemSite site)
        {
            _site = (IsolatedSubsystemSite) site;
            _currentInstanceNumber = Interlocked.Increment(ref _instanceCount);
            _addinSiteUri = GetAddinSiteUri();

        }

        private XProcEndpointManager(IMSDesktopSubsystemSite site, string isolatingExecutableLocation,
                                     string additionalCommandLine) : this(site)
        {
            _additionalCommandLine = additionalCommandLine;
            _isolatingExecutableLocation = isolatingExecutableLocation;
        }

        internal XProcEndpointManager(IMSDesktopSubsystemSite site, string isolatingApplicationKey) : this(site)
        {
            _isolatingApplicationKey = isolatingApplicationKey;
        }

        internal XProcEndpointManager(IMSDesktopSubsystemSite site, PackageDefinition packageDefinition) : this(site)
        {
            _packageDefinition = packageDefinition;
        }

        internal XProcEndpointManager(IMSDesktopSubsystemSite site, string isolatingExecutableLocation,
                                      string additionalCommandLine, string hostExecutable)
            : this(site, isolatingExecutableLocation, additionalCommandLine)
        {
            _hostExecutable = hostExecutable;
        }

        public int CurrentInstanceNumber
        {
            get { return _currentInstanceNumber; }
        }

        public string AddinSiteUri
        {
            get { return _addinSiteUri; }
        }

        internal void StartProcess(bool respawn = false)
        {
            ThreadPool.QueueUserWorkItem(StartIsolatingProcess, respawn);
        }

        internal static string GetDefaultIsolatedExecutable(bool force32Bit)
        {
            return force32Bit
                       ? @"MSDesktop.Isolation.HostProcess.X86.exe"
                       : @"MSDesktop.Isolation.HostProcess.exe";
        }

        private string GetIsolatingExecutablePath()
        {
            var hostExecutable = _hostExecutable;

            if (!string.IsNullOrEmpty(_isolatingExecutableLocation))
            {
                return Path.Combine(_isolatingExecutableLocation, hostExecutable);
            }

            // If the location for isolating executable is not specified, probe current directory and dist directory
            var msdesktopAsmPath = Path.GetDirectoryName(typeof (Framework).Assembly.Location);
            string executablePath = Path.Combine(msdesktopAsmPath, hostExecutable);
            if (File.Exists(executablePath))
            {
                return executablePath;
            }

            // Try in relative location
            var directoryName =
                new DirectoryInfo(Path.Combine(msdesktopAsmPath, RelativeExecutableDistLocation)).FullName;
            executablePath = Path.Combine(directoryName, hostExecutable);
            if (File.Exists(executablePath))
            {
                return executablePath;
            }

            //try in msgui dist location
            directoryName =
                MorganStanley.MSDotNet.MSGui.Impl.SmartApi.Extensions.GetMSDesktopApplicationLocation(
                    MSDesktopApplication.IsolationHostProcess);

            executablePath = Path.Combine(directoryName, hostExecutable);
            if (File.Exists(executablePath))
            {
                return executablePath;
            }
            var errorMessage = string.Format(
                "Could not locate the isolating executable neither at '{0}' nor at '{1}'",
                msdesktopAsmPath,
                directoryName);

            throw new FileNotFoundException(errorMessage);
        }

        private void StartIsolatingProcess(object respawnAsObject)
        {
            var respawn = (bool) respawnAsObject;
            if (!respawn)
            {
                // Register this new host with its new URL.
                RegisterSite(AddinSiteUri, _site);
            }

            if (string.IsNullOrEmpty(_isolatingApplicationKey) && _packageDefinition == null)
            {
                StartUsingCommandLine(AddinSiteUri);
            }
            else
            {
                StartUsingLauncherMessage(AddinSiteUri);
            }
        }

        private string GetAddinSiteUri()
        {
            // Register the host object with RemotingServices so that the add-in can connect to it.
            // The URI is passed to addIn.exe's command line.
            var currentAddinCounter = Interlocked.Increment(ref _addinCounter);

            var addinSiteUri = "host" + currentAddinCounter;
            var baseUri = GetServerChannelUri("WPFAddInHost");
            addinSiteUri = string.Format("{0}/{1}", baseUri, addinSiteUri);
            return addinSiteUri;
        }

        private void StartUsingCommandLine(string addinSiteUri)
        {
            var subsystemSettings = _site.GetIsolatedSubsystemSettings();
            var startline = !string.IsNullOrEmpty(_additionalCommandLine)
                                ? string.Format("{0} {1}", addinSiteUri, _additionalCommandLine)
                                : addinSiteUri;

            if (subsystemSettings.IsAdditionalArRequired)
            {
                subsystemSettings.AssemblyResolutionFiles = GetArFiles(_site.ExtraAr).ToArray();
            }

            RegisterChildProcess(() => Process.Start(GetIsolatingExecutablePath(), startline));
        }

        private void StartUsingLauncherMessage(string addinSiteUri)
        { 
            EventHandler<ApplicationStartedEventArgs> started = null;
            _site.ApplicationStarted += started = (sender_, args_) =>
                {
                    RegisterChildProcess(() => Process.GetProcessById(args_.ProcessId));
                    _site.ApplicationStarted -= started;
                };
            if (_isolatingApplicationKey != null)
            {
                DispatchableLauncherMessage.StartApplication(_isolatingApplicationKey, addinSiteUri);
                return;
            }

            string arg = _packageDefinition.Executable;
            if (!string.IsNullOrEmpty(arg))
            {
                arg += " " + addinSiteUri;
            }
            else
            {
                arg = addinSiteUri;
            }
            if (!string.IsNullOrEmpty(_packageDefinition.PackagePath))
            {
                DispatchableLauncherMessage.StartPackageByPath(_packageDefinition.PackagePath, _packageDefinition.Executable, arg); 
                return;
            }

            DispatchableLauncherMessage.StartPackage(_packageDefinition.AppName, _packageDefinition.Version, _packageDefinition.Package, _packageDefinition.Executable, arg); 
        }

        private void RegisterChildProcess(Func<Process> processGetter)
        {
            Process addinProcess = null;
            try
            {
                addinProcess = processGetter();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.ToString(), "The isolated process could not be started");
                if (!_site.IsControl)
                {
                    _site.OnIsolatedProcessKilled();
                }
            }
            if (addinProcess != null)
            {
                addinProcess.EnableRaisingEvents = true;
                EventHandler handler = null;
                handler = delegate
                    {
                        addinProcess.Exited -= handler; 
                        if (!_site.IsControl)
                        { 
                            InvokeChildProcessCrashed(addinProcess.Id);
                            _site.OnIsolatedProcessKilled();
                        } 
                    };


                addinProcess.Exited += handler;

                // Trigger started event
                var startedHandler = ChildProcessStarted;
                if (startedHandler != null)
                {
                    var startedEventArgs = new IsolatedProcessStartedEventArgs(addinProcess.Id, null);
                    startedHandler(this, startedEventArgs);
                }
            }
        }

        private static IEnumerable<string> GetArFiles(string extraAr)
        {
            var msdelist = new HashSet<string>();

            if (!string.IsNullOrEmpty(extraAr))
            {
                msdelist.Add(extraAr);
            }
            foreach (var assembly in AssemblyResolver.GetAssemblies())
            {
                // We need to init the variables
                //Console.WriteLine(assembly);
            }

            var fieldInfo = typeof (AssemblyResolver).GetField("m_proxy",
                                                               BindingFlags.NonPublic | BindingFlags.Static);
            if (fieldInfo != null)
            {
                var proxy = fieldInfo.GetValue(null);
                var field = proxy.GetType().GetField("m_impl", BindingFlags.NonPublic | BindingFlags.Instance);
                if (field != null)
                {
                    var impl = field.GetValue(proxy);
                    var info = impl.GetType()
                                   .GetField("m_dependencyManager", BindingFlags.NonPublic | BindingFlags.Instance);
                    if (info != null)
                    {
                        var dep = info.GetValue(impl);
                        var infofield = dep.GetType()
                                           .GetField("m_dependencies",
                                                     BindingFlags.NonPublic | BindingFlags.Instance);
                        if (infofield != null)
                        {
                            var dependencies = infofield.GetValue(dep) as ArrayList;
                            if (dependencies != null)
                            {
                                foreach (
                                    var path in
                                        (from object dependency in dependencies
                                         select
                                             dependency.GetType()
                                                       .GetProperty("IncludePath")
                                                       .GetValue(dependency, null))
                                            .OfType<ArrayList>()
                                            .SelectMany(paths => paths
                                                                     .Cast<string>()
                                                                     .Where(path => !msdelist.Contains(path))))
                                {
                                    msdelist.Add(path);
                                }
                            }
                        }
                    }
                }
            }

            return msdelist;
        }

        internal event EventHandler<IsolatedProcessStartedEventArgs> ChildProcessCrashed;
        internal event EventHandler<IsolatedProcessStartedEventArgs> ChildProcessStarted;

        internal void InvokeChildProcessCrashed(int processId)
        {
            var handler = ChildProcessCrashed;
            if (handler != null)
            {
                handler(this, new IsolatedProcessStartedEventArgs(processId, null));
            }
        }

        // TODO (ahrechkin): This needs to be shared between 
        // isolated process assembly and this one.
        private string GetServerChannelUri(string baseServerName)
        {
            // Configure WCF for both server and client here
            var channelUri = string.Format("net.pipe://localhost/{0}-{1}-{2}", baseServerName,
                                           Process.GetCurrentProcess().Id,
                                           CurrentInstanceNumber);

            return channelUri;
        }

        private void RegisterSite(string hostUri, IMSDesktopSubsystemSite site)
        {
            // configure binding
            var binding = new NetNamedPipeBinding(NetNamedPipeSecurityMode.None)
                {
                    Name = hostUri,
                    CloseTimeout = TimeSpan.MaxValue,
                    OpenTimeout = TimeSpan.MaxValue,
                };

            _serviceHost = new ServiceHost(site, new Uri(hostUri));
            _serviceHost.AddServiceEndpoint(typeof (IMSDesktopSubsystemSite), binding, hostUri);
            _serviceHost.Open();
        }

        public void Dispose()
        {
            _serviceHost.Close();
            ((IDisposable) _serviceHost).Dispose();
        }
    }
}
