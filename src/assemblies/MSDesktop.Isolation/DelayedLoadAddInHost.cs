﻿using System;
using System.Collections.Generic;
using MSDesktop.Isolation.Interfaces;

namespace MSDesktop.Isolation
{
    internal sealed class DelayedLoadAddInHost : IXProcAddInHost
    {
        private volatile IXProcAddInHost _host;

        private readonly object _pendingActionsLock = new object();
        private readonly IList<KeyValuePair<Type, Action<object>>> _pendingActions =
            new List<KeyValuePair<Type, Action<object>>>();

        public IXProcUiAddIn AddIn
        {
            get
            {
                if (_host != null)
                {
                    return _host.AddIn;
                }

                throw new InvalidOperationException("We shouldn't call any properties of methods until an add-in is available");
            }
        }

        public void RegisterMessageHandler(Type messageType, Action<object> messageHandler)
        {
            if (_host != null)
            {
                _host.RegisterMessageHandler(messageType, messageHandler);
            }
            else
            {
                _pendingActions.Add(new KeyValuePair<Type, Action<object>>(messageType, messageHandler));
            }
        }

        internal void SetAddInHost(IXProcAddInHost host)
        {
            _host = host;

            // Transfer subscription registrations
            lock (_pendingActionsLock)
            {
                foreach (var pendingAction in _pendingActions)
                {
                    host.RegisterMessageHandler(pendingAction.Key, pendingAction.Value);
                }

                _pendingActions.Clear();
            }
        }
    }
}
