﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging; 
using MorganStanley.MSDotNet.MSGui.Controls.Interfaces;
using MorganStanley.MSDotNet.MSGui.Core.Win32;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.LauncherBar;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Ribbon;

namespace MSDesktop.Isolation
{
    internal class SubsystemControlHost : SubsystemViewHostBase, IHwndHostControl
    {
        private bool _canRenderChild;
        private readonly AddInProxy.AddInProxy _addinProxy;
 

        internal SubsystemControlHost(IntPtr hwnd, AddInProxy.AddInProxy addinProxy) : base(hwnd, addinProxy)
        {
            _addinProxy = addinProxy;
        } 


        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
        {
            base.OnRenderSizeChanged(sizeInfo);
            _canRenderChild = true;
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);

            if (IsVisible)
            {
                RequestControlRepaint();
            }
        }

        protected override void OnWindowPositionChanged(Rect rcBoundingBox)
        {
            base.OnWindowPositionChanged(rcBoundingBox);
            if (IsVisible)
            {
                RequestControlRepaint();
            }
        } 
        private void RequestControlRepaint()
        { 
            var shouldRepaint = ShouldRepaintChild();
            if (_canRenderChild && shouldRepaint)
            {
                var backgroundVisual = GetBackgroundVisual();

                // TODO (hrechkin): Improve performance of this call by capturing required rectangle only.
                var mainWindowBitmap = new RenderTargetBitmap((int) backgroundVisual.RenderSize.Width,
                                                              (int) backgroundVisual.RenderSize.Height, 96, 96,
                                                              PixelFormats.Pbgra32);

                mainWindowBitmap.Render(backgroundVisual);
                var topLeftPoint = TranslatePoint(new Point(0, 0), backgroundVisual);

                try
                {
                    int left = (int)topLeftPoint.X;
                    if (left <= 0) return;
                    int top = (int)topLeftPoint.Y;
                    if (top <= 0) return;
                    int width = (int)Math.Min(RenderSize.Width, backgroundVisual.RenderSize.Width - topLeftPoint.X);
                    if (width <= 0) return;
                    int height = (int)Math.Min(RenderSize.Height, backgroundVisual.RenderSize.Height - topLeftPoint.Y);
                    if (Height <= 0) return;
                    var rect = new Int32Rect(left, top, width, height);

                    var croppedBitmap = new CroppedBitmap(mainWindowBitmap, rect);

                    // Serialize it into array of bytes
                    var encoder = new PngBitmapEncoder();
                    encoder.Frames.Add(BitmapFrame.Create(croppedBitmap));

                    byte[] imageBytes;
                    using (var stream = new MemoryStream())
                    {
                        encoder.Save(stream);
                        imageBytes = stream.ToArray();
                    }

                    _addinProxy.QueuePaint(imageBytes);
                }
                catch (Exception ex)
                {
                    Debugger.Break();
                }
            }
        }

        private static bool ShouldRepaintChild()
        {
            return Mouse.LeftButton != MouseButtonState.Pressed;
        }

        private static UIElement GetBackgroundVisual()
        {
            var rootContainer = ChromeManagerBase.Instance.GetRootContainer();
            var ribbonArea = rootContainer as RibbonRootArea;

            UIElement rootRibbonElement;
            if (ribbonArea != null && ribbonArea.TryGetRootVisual(out rootRibbonElement))
            {
                return rootRibbonElement;
            }

            var launcherBar = rootContainer as LauncherBarRootArea;
            UIElement launcerBarElement;
            if (launcherBar != null && launcherBar.TryGetRootVisual(out launcerBarElement))
            {
                return launcerBarElement;
            }

            return ChromeManagerBase.Instance.Shell.MainWindow;
        }

        bool IHwndHostControl.IsVisible
        {
            get { return IsVisible; }
        }

        bool IHwndHostControl.IsMouseOnControl(Point point)
        {
            var result = Win32.SendMessage(
                RemoteHwnd,
                (int)Win32.WM_NCHITTEST,
                0,
                Win32.MAKELPARAM((int)point.X, (int)point.Y));

            // We got HTNOWHERE - meaning that the point isn't on the control.
            return result;
        }

        void IHwndHostControl.MouseMove(MouseEventArgs eventArgs)
        {
            var point = eventArgs.GetPosition(this);
            Win32.SendMessage(RemoteHwnd, (int)Win32.WM_MOUSEMOVE, (int)0, Win32.MAKELPARAM((int)point.X, (int)point.Y));
        }

        void IHwndHostControl.MouseEnter(MouseEventArgs eventArgs)
        {
        }

        void IHwndHostControl.MouseLeave(MouseEventArgs eventArgs)
        {
        }

        void IHwndHostControl.MouseLeftButtonUp(MouseEventArgs eventArgs)
        {
            // TODO (hreckin): Add handling of keys here perhaps
            var point = eventArgs.GetPosition(this);
            Win32.SendMessage(RemoteHwnd, (int)Win32.WM_LBUTTONDOWN, 0, Win32.MAKELPARAM((int)point.X, (int)point.Y));
        }

        void IHwndHostControl.MouseLeftButtonDown(MouseEventArgs eventArgs)
        {
            // TODO (hreckin): Add handling of keys here perhaps
            var point = eventArgs.GetPosition(this);
            Win32.SendMessage(RemoteHwnd, (int)Win32.WM_LBUTTONDOWN, 0, Win32.MAKELPARAM((int)point.X, (int)point.Y));
        }
    }
}
