﻿using System;
using System.Windows.Input;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;

namespace MSDesktop.Isolation
{
    internal sealed class ProcessCrashedViewViewModel : ViewModelBase
    {
        private readonly SubsystemRespawnManager _respawnManager;
        private string _labelText;
        private bool _isRestartEnabled;

        public ProcessCrashedViewViewModel(SubsystemRespawnManager respawnManager)
        {
            _respawnManager = respawnManager;
            _isRestartEnabled = true;
            _labelText = "The process rendering this view has crashed.";
            RestartSubsystemCommand = new RelayCommand(RestartSubsystem, CanRestartSubsystem);

            if (respawnManager != null)
            {
                respawnManager.RestartTriggered += MyDelegateOnRestartTriggered;
            }
        }

        public ICommand RestartSubsystemCommand { get; private set; }

        public string LabelText
        {
            get
            {
                return _labelText;
            }
            private set
            {
                _labelText = value;
                OnPropertyChanged("LabelText");
            }
        }

        private void MyDelegateOnRestartTriggered(object sender, EventArgs eventArgs)
        {
            _isRestartEnabled = false;
            LabelText = "The process rendering this view is restarting. Please wait...";
        }
        
        private void RestartSubsystem()
        {
            _respawnManager.RestartSubsystem();
        }

        private bool CanRestartSubsystem()
        {
            return _isRestartEnabled;
        }
    }
}
