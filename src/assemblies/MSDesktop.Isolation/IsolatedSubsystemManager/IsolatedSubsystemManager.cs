﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.ProcessIsolation;

namespace MSDesktop.Isolation.IsolatedSubsystemManager
{
    internal sealed class IsolatedSubsystemManager
    {
        internal readonly IDictionary<string, RunningIsolatedSubsystem> _isolatedSubsystems;
        private readonly IDictionary<int, string> _runningProcesses;

        private IUnityContainer _container;

        public IsolatedSubsystemManager()
        {
            _isolatedSubsystems = new ConcurrentDictionary<string, RunningIsolatedSubsystem>();
            _runningProcesses = new ConcurrentDictionary<int, string>();
        }

        public IUnityContainer Container
        {
            set
            {
                if (_container == null)
                {
                    _container = value;
                }
            }
        }

        public void StartIsolatedProcess(IsolatedSubsystemSite subsystemSite, string isolatingExecutableLocation, string additionalCommandLine, string isolatingExecutabelFileName, bool respawnEnabled, bool isHostInitialized, ProcessIsolationRespawnHandler respawnHandler, Action<string> respawnAction = null)
        {
            subsystemSite.ApplicationStarted += HandleApplicationStarted;

            var endpointManager = new XProcEndpointManager(subsystemSite, isolatingExecutableLocation,
                                                           additionalCommandLine, isolatingExecutabelFileName);

            var subsystemInfo = new RunningIsolatedSubsystem(_container, subsystemSite, endpointManager, respawnEnabled, respawnHandler);
            _isolatedSubsystems.Add(endpointManager.AddinSiteUri, subsystemInfo);

            endpointManager.StartProcess();
        }

        public void RegisterLauncherMessageBasedSubsystem(IUnityContainer container, IsolatedSubsystemSite subsystemSite, XProcEndpointManager endpointManager, bool respawnEnabled, ProcessIsolationRespawnHandler respawnHandler)
        {
            var subsystemInfo = new RunningIsolatedSubsystem(container, subsystemSite, endpointManager, respawnEnabled, respawnHandler);
            _isolatedSubsystems.Add(endpointManager.AddinSiteUri, subsystemInfo);
        }

        public void HandleInitialSubsystemCommunication(string subsystemSiteUri, int isolatedProcessId, string isolatedProcessAddInUri)
        {
            var subsystemInfo = _isolatedSubsystems[subsystemSiteUri];
            _runningProcesses.Add(isolatedProcessId, subsystemSiteUri);
            subsystemInfo.HandleIsolatedProcessInitialCommunication(isolatedProcessId, isolatedProcessAddInUri);
        }

        private void HandleApplicationStarted(object sender, ApplicationStartedEventArgs e)
        {
            var chromeRegistry = _container.Resolve<IChromeRegistry>();

            // Register the window factories in the arguments
            foreach (var remoteFactoryId in e.WindowFactoryIds)
            {
                chromeRegistry.RegisterWindowFactory(remoteFactoryId, RemoteWindowInitHandler, RemoteWindowDehydrateHandler);
            }
        }

        private XDocument RemoteWindowDehydrateHandler(IWindowViewContainer windowViewContainer)
        {
            // Same save routine as in case of placement
            return new XDocument();
        }

        private bool RemoteWindowInitHandler(IWindowViewContainer emptyViewContainer, XDocument state)
        {
            // Create the placeholder - same way as 
            return true;
        }

        private void HandleProfileReload(object sender, CurrentProfileChangedEventArgs args)
        {
            var profilePersistenceService = _container.Resolve<PersistenceProfileService>();
            var storage = _container.Resolve<IPersistenceStorage>();
            var newLayout = storage.LoadState(profilePersistenceService.CurrentProfile);
            var currentProfile = profilePersistenceService.CurrentProfile;

            foreach (var si in _isolatedSubsystems.Values)
            {
                si.TriggerReloadProfile(newLayout, currentProfile);
            }
        }

        public void SignalHostInitialized()
        {
            // Hook up profile reload event
            var profilePersistenceService = _container.Resolve<PersistenceProfileService>();
            profilePersistenceService.ProfileLoaded += HandleProfileReload;


            // Signal the isolated processes that we are good to go and they can 
            // go ahead with initialization and components placement.
            // We will also need to provide the layout in each of them.
            var currentSystems = _isolatedSubsystems.Values.ToList();
            foreach (var si in currentSystems)
            {
                si.ProceedWithInitialization();
            }
        }

        public void ShutdownIsolatedSubsystems()
        {
            foreach (var subsystemInfo in _isolatedSubsystems.Values)
            {
                subsystemInfo.Shutdown();
            }
        }

        public IEnumerable<RunningIsolatedSubsystem> GetIsolatedSubsystems()
        {
            return _isolatedSubsystems.Values;
        }
    }
}
