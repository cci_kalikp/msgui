﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Threading;
using System.Xml.Linq;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.Factories;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity;
using MorganStanley.MSDotNet.MSGui.Impl.ProcessIsolation;
using MSDesktop.Isolation.Interfaces;

namespace MSDesktop.Isolation.IsolatedSubsystemManager
{
    internal sealed class RunningIsolatedSubsystem : IRemoteWindowFactory
    {
        private readonly IsolatedSubsystemSite _isolatedSubsystemSite;
        private readonly XProcEndpointManager _endpointManager;
        private readonly bool _respawnEnabled;
        private readonly ProcessIsolationRespawnHandler _respawnHandler;
        private readonly IUnityContainer _container;

        private volatile int _processId = -1;
        private volatile IXProcProcessAddIn _isolatedProcessAddIn;
        private volatile IEnumerable<string> _registeredFactories;

        internal RunningIsolatedSubsystem(IUnityContainer container, IsolatedSubsystemSite isolatedSubsystemSite, XProcEndpointManager endpointManager, bool respawnEnabled, ProcessIsolationRespawnHandler respawnHandler)
        {
            _container = container;
            _respawnEnabled = respawnEnabled;
            _respawnHandler = respawnHandler;
            _endpointManager = endpointManager;
            _isolatedSubsystemSite = isolatedSubsystemSite;

            _endpointManager.ChildProcessCrashed += HandleChildProcessCrashed;
            _isolatedSubsystemSite.ApplicationStarted += HandleApplicationStarted;
        }

        public int ProcessId
        {
            get { return _processId; }
        }

        public void HandleIsolatedProcessInitialCommunication(int processId, string isolatedProcessAddinUri)
        {
            if (_processId == -1)
            {
                _isolatedProcessAddIn =
                    ProcessIsolationUtility.InitClientChannel<IXProcProcessAddIn>(isolatedProcessAddinUri);
                _processId = processId;
            }
            else
            {
                throw new InvalidOperationException("You can update the isolated process ID only once.");
            }
        }

        public IMSDesktopSubsystemSite IsolatedSubsystemSite
        {
            get { return _isolatedSubsystemSite; }
        }

        public XProcEndpointManager EndpointManager
        {
            get { return _endpointManager; }
        }

        public void ProceedWithInitialization()
        {
            _isolatedSubsystemSite.InitializeMsDesktopObjects();

            if (_isolatedProcessAddIn != null)
            {
                var profileName = _isolatedSubsystemSite.GetCurrentProfileName();
                var profileLength = _isolatedSubsystemSite.LoadLayoutIntoProfileFile();

                // Trigger proceed call
                var profileFile = _isolatedSubsystemSite.ProfileFile;
                var closure = new ReloadProfileClosure(profileFile.IsolatedProcessHandle.ToInt64(), profileLength,
                                                       profileName);

                ThreadPool.QueueUserWorkItem(ProceedWithInitializationProc, closure);
            }
        }

        public void TriggerReloadProfile(XDocument newLayoutDoc, string currentProfile)
        {
            var layout = newLayoutDoc.ToString();

            if (!string.IsNullOrEmpty(layout) && _isolatedSubsystemSite.IsIsolatedProcessInitialized)
            {
                var profileFile = _isolatedSubsystemSite.ProfileFile;
                profileFile.WriteString(layout);

                var closure = new ReloadProfileClosure(profileFile.IsolatedProcessHandle.ToInt64(), profileFile.LastWriteLength,
                                                       currentProfile);
                ThreadPool.QueueUserWorkItem(ReloadProfileProc, closure);
            }
        }

        public void Shutdown()
        {
            if (_isolatedProcessAddIn != null)
            {
                _isolatedProcessAddIn.ShutDown();
            }
        }

        public void CreateRemoteWindow(string factoryId, string viewId)
        {
            ThreadPool.QueueUserWorkItem(o => _isolatedProcessAddIn.CreateWindow(factoryId, viewId));
        }

        private void ProceedWithInitializationProc(object data)
        {
            var closure = (ReloadProfileClosure)data;
            _isolatedProcessAddIn.ProceedWithInitialization(closure.FileHandle, closure.FileLength, closure.ProfileName);
        }

        private void ReloadProfileProc(object data)
        {
            var closure = (ReloadProfileClosure) data;
            _isolatedProcessAddIn.ReloadProfile(closure.ProfileName, closure.FileHandle, closure.FileLength);
        }

        private void HandleApplicationStarted(object sender, ApplicationStartedEventArgs e)
        {
            var moduleGroup = _container.Resolve<IModuleGroup>();
            _registeredFactories = e.WindowFactoryIds;
            moduleGroup.RegisterSubsystemWindowFactory(this, e.WindowFactoryIds);
        }

        private void HandleChildProcessCrashed(object sender, IsolatedProcessStartedEventArgs e)
        {
            // Notify the site so that it can clean up its resources
            var respawnManager = new SubsystemRespawnManager(_container, _isolatedSubsystemSite);
            respawnManager.RestartTriggered += RespawnIsolatedProcess;

            // Unregister the factories
            if (_registeredFactories != null)
            {
                var moduleGroup = _container.Resolve<IModuleGroup>();
                moduleGroup.UnRegisterSubsystemWindowFactory(_registeredFactories);
            }

            _registeredFactories = null;

            // Clean up process addin and process ID
            var processChannel = _isolatedProcessAddIn as IClientChannel;
            if (processChannel != null)
            {
                try
                {
                    if (processChannel.State == CommunicationState.Opened)
                    {
                        processChannel.Close();
                    }
                }
                catch
                {
                }
                finally
                {
                    processChannel.Dispose();
                }
            }

            _isolatedProcessAddIn = null;
            _processId = -1;

            _isolatedSubsystemSite.HandleIsolatedProcessCrash(respawnManager, _respawnEnabled);

            var args = new ProcessIsolationRespawnEventArgs();
            var copy = _respawnHandler;
            if (copy != null)
            {
                copy.OnProcessCrashed(args);
            }
            if (args.Respawn)
            {
                respawnManager.RestartSubsystem();
            }
        }

        private void RespawnIsolatedProcess(object sender, RespawnRequiredEventArgs e)
        {
            _isolatedSubsystemSite.SetSubsystemLayout(e.NewLayout);
            _endpointManager.StartProcess(true);
        }

        // TODO (hreckin): Replace this with ProfileFileInfo 
        private sealed class ReloadProfileClosure
        {
            private readonly int _fileLength;
            private readonly long _fileHandle;
            private readonly string _profileName;

            public ReloadProfileClosure(long fileHandle, int fileLength, string profileName)
            {
                _profileName = profileName;
                _fileHandle = fileHandle;
                _fileLength = fileLength;
            }

            public int FileLength
            {
                get { return _fileLength; }
            }

            public long FileHandle
            {
                get { return _fileHandle; }
            }

            public string ProfileName
            {
                get { return _profileName; }
            }
        }
    }
}
