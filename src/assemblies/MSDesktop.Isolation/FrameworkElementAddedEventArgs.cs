﻿using System;
using System.Windows;
using MSDesktop.Isolation;
using MSDesktop.Isolation.Interfaces;

namespace MorganStanley.MSDotNet.MSGui.Impl.ProcessIsolation
{
    internal sealed class FrameworkElementAddedEventArgs : WindowAvailableEventArgs
    {
        private readonly FrameworkElement _element;

        public FrameworkElementAddedEventArgs(IXProcAddInHost addInHost, FrameworkElement element, IntPtr hwnd)
            : base(addInHost, hwnd)
        {
            _element = element;
        }

        public FrameworkElement Element
        {
            get { return _element; }
        }
    }
}
