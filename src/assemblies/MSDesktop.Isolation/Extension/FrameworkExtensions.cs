﻿using MSDesktop.Isolation;

// ReSharper disable CheckNamespace
namespace MorganStanley.MSDotNet.MSGui.Impl.Extensions
// ReSharper restore CheckNamespace
{
    public static class FrameworkExtensions
    {
        public static void EnableProcessIsolation(this Framework f)
        {
            f.AddModule<ProcessIsolationSupportModule>();
        }
    }
}
