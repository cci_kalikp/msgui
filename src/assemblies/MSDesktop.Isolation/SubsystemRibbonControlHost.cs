﻿ 

using System;
using System.Windows;
using Infragistics.Windows.Ribbon;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Ribbon;

namespace MSDesktop.Isolation
{
    internal sealed class SubsystemRibbonControlHost : SubsystemControlHost
    {
        private readonly AddInProxy.AddInProxy _addinProxy;
        static SubsystemRibbonControlHost()
        {
            // Mimic button behavior - size is Normal by default.
            var propertyMetadata = new FrameworkPropertyMetadata(RibbonToolSizingMode.ImageAndTextNormal);

            RibbonGroup.MaximumSizeProperty.OverrideMetadata(
                typeof(SubsystemControlHost), 
                propertyMetadata);
        }


        internal SubsystemRibbonControlHost(IntPtr hwnd, AddInProxy.AddInProxy addinProxy, string isolatedButtonSize)
            : base(hwnd, addinProxy)
        {
            if (!string.IsNullOrEmpty(isolatedButtonSize))
            {
                SetValue(RibbonGroup.MaximumSizeProperty, Enum.Parse(typeof (RibbonToolSizingMode), isolatedButtonSize));
            }
            _addinProxy = addinProxy;
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);

            var rootContainer = ChromeManagerBase.Instance.GetRootContainer();
            var ribbonArea = rootContainer as RibbonRootArea;
            if (ribbonArea != null)
            {
                ribbonArea.AddHostControl(this);
            }
        }


        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            // Drop the call to the isolated process, so that it can resize. 
            // Wait for the call to come back
            if (e.Property.Name.Equals("SizingMode", StringComparison.OrdinalIgnoreCase))
            {
                _addinProxy.RibbonButtonSizeChanged(e.NewValue);
            }

            base.OnPropertyChanged(e);
        }
         
    }
}
