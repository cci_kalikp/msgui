﻿using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Unity;
using MSDesktop.MessageRouting.Isolation;

namespace MSDesktop.Isolation
{
    internal sealed class ProcessIsolationSupportModule : IModule
    {
        private readonly IUnityContainer _container;
        private PreferLocalEndpointsAcrossSubsystems _routingStep;

        public ProcessIsolationSupportModule(IUnityContainer container)
        {
            _container = container;
        }

        public void Initialize()
        {
            _routingStep = _container.Resolve<PreferLocalEndpointsAcrossSubsystems>();
            _routingStep.Register();
        }
    }
}
