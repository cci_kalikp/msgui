﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Lifetime;
using System.Runtime.Serialization.Formatters.Binary;
using System.ServiceModel;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Threading;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Core.Win32;
using MSDesktop.Isolation.Interfaces;
using MSDesktop.Isolation.Proxies;

namespace MSDesktop.Isolation
{
    /// <summary>
    /// The AddIn object attaches itself to this "host" object. It is separate from IxProcAddInHost because
    /// it needs to be derived from MBR to be remotable.
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, IncludeExceptionDetailInFaults = true, UseSynchronizationContext = false)]
    internal sealed class XProcWindowSite : IXProcWindowSite, ISponsor
    {
        private readonly BinaryFormatter _formatter = new BinaryFormatter();
        private readonly Dispatcher _dispatcher;
        private readonly SubsystemViewHostBase _viewHost;
        private readonly IEventRegistrator _eventRegistrator;
        private readonly IChromeManager _chromeManager;

        internal event EventHandler<MessageArrivedEventArgs> MessageArrived;

        internal XProcWindowSite(SubsystemViewHostBase viewHost)
        {
            _viewHost = viewHost;
            _dispatcher = Dispatcher.CurrentDispatcher;
        }

        internal XProcWindowSite(SubsystemViewHost viewHost, IChromeManager chromeManager,
                                 IEventRegistrator eventRegistrator) : this(viewHost)
        {
            _chromeManager = chromeManager;
            _eventRegistrator = eventRegistrator;
        }

        /*
        public override object InitializeLifetimeService()
        {
            // Set up the host as the sponsor of the lease.
            var lease = (ILease) base.InitializeLifetimeService();
            if (lease.CurrentState == LeaseState.Initial)
            {
                lease.Register(this);
            }

            return lease;
        }*/

        #region IXProcAddInSite implemenentation

        // Note: These calls arrive on thread pool threads. UI work must be done on the UI thread.
        // We switch easily via the Dispatcher.

        public bool TabOut(TraversalRequest request)
        {
            return
                (bool)
                _dispatcher.Invoke((DispatcherOperationCallback) (tr => _viewHost.MoveFocus((TraversalRequest) tr)),
                                   request);
        }

        public bool TranslateAccelerator(MsgProxy msg)
        {
            return (bool) _dispatcher.Invoke((DispatcherOperationCallback) delegate(object msgObj)
                {
                    IKeyboardInputSink keyboardSink = _viewHost;
                    var innerMsg = (MsgProxy) msgObj;

                    // We delegate key processing to the containing HwndSource, via IKIS. It will see that 
                    // a child window ("sink") has focus and will do special routing of the input events using
                    // that child window as the forced target. Thus, any element up to the root visual has a 
                    // chance to see and handle the events.
                    if (!(keyboardSink).HasFocusWithin())
                    {
                        return false;
                    }

                    var hostSink = (IKeyboardInputSink) PresentationSource.FromVisual(_viewHost);
                    var m2 = new MSG
                        {
                            hwnd = new IntPtr(innerMsg.Hwnd),
                            message = innerMsg.Message,
                            lParam = innerMsg.LParam,
                            wParam = innerMsg.WParam,
                            time = innerMsg.Time,
                            pt_x = innerMsg.PtX,
                            pt_y = innerMsg.PtY,
                        };

                    // - IKIS has special rules about which messages are passed to which method.
                    // Here we assume the add-in bubbles only raw key messages plus mnemonic keys (Alt+something).
                    // - Even though the call has hopped from thread to thread, Keyboard.Modifiers produces
                    // correct result here because the input queue and states of the host's and add-in's UI
                    // threads are synchronized. (The window manager automatically attaches thread input when
                    // a window is parented to a cross-thread one.)
                    if (m2.message == Win32.WM_SYSCHAR || m2.message == Win32.WM_SYSDEADCHAR)
                    {
                        return hostSink.OnMnemonic(ref m2, Keyboard.Modifiers);
                    }

                    return hostSink.TranslateAccelerator(ref m2, Keyboard.Modifiers);
                }, msg);
        }

        public void SubscribeV2V(string viewId, string typeName)
        {
            var messageType = Type.GetType(typeName);

            var subscriberMethod =
                _eventRegistrator.GetType().GetMethod("RegisterMarshalingSubscriber").MakeGenericMethod(messageType);

            var viewContainer = GetViewContainer(viewId);
            subscriberMethod.Invoke(_eventRegistrator, new object[] {viewContainer, _viewHost.AddinProxy});
        }

        public void RegisterAsPublisher(string viewId, string typeName)
        {
            var messageType = Type.GetType(typeName);

            var publisherMethod =
                _eventRegistrator.GetType().GetMethod("RegisterMarshalingPublisher").MakeGenericMethod(messageType);

            var viewContainer = GetViewContainer(viewId);
            publisherMethod.Invoke(_eventRegistrator, new object[] {viewContainer, _viewHost.AddinProxy});
        }

        public void Send(byte[] payload)
        {
            var handler = MessageArrived;
            if (handler != null)
            {
                object actualPayload;
                using (var stream = new MemoryStream(payload))
                {
                    actualPayload = _formatter.Deserialize(stream);
                }

                handler(this, new MessageArrivedEventArgs(actualPayload));
            }
        }

        #endregion

        TimeSpan ISponsor.Renewal(ILease lease)
        {
            return /*Handle == IntPtr.Zero ? TimeSpan.Zero : */ lease.RenewOnCallTime;
        }

        private IWindowViewContainer GetViewContainer(string viewId)
        {
            var viewContainer =
                _chromeManager.GetViews()
                              .FirstOrDefault(v => v.ID.Equals(viewId, StringComparison.InvariantCultureIgnoreCase));
            return viewContainer;
        }
    }
}
