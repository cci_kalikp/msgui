﻿using System;

namespace MSDesktop.Isolation
{
    internal sealed class RespawnRequiredEventArgs : EventArgs
    {
        private readonly string _newLayout;

        public RespawnRequiredEventArgs(string newLayout)
        {
            _newLayout = newLayout;
        }

        public string NewLayout
        {
            get { return _newLayout; }
        }
    }
}
