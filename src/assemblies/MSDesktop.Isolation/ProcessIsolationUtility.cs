﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.ServiceModel;
using System.Threading;

namespace MorganStanley.MSDotNet.MSGui.Impl.ProcessIsolation
{
    internal static class ProcessIsolationUtility
    {
        private static int _channelCount;
        // Keep track of service hosts
        private static readonly IDictionary<string, ServiceHost> _hosts =
            new ConcurrentDictionary<string, ServiceHost>();


        public static string RegisterServerChannel<T>(string baseServerName, T sessionObject)
        {
            var currentChannelCount = Interlocked.Increment(ref _channelCount);
            return RegisterServerChannel(baseServerName, currentChannelCount, sessionObject);
        }

        public static string RegisterServerChannel<T>(string baseServerName, int currentChannelIndex, T sessionObject)
        {
            // Configure WCF for both server and client here
            var channelUri = string.Format("net.pipe://localhost/{0}-{1}-{2}", baseServerName,
                                           Process.GetCurrentProcess().Id,
                                           currentChannelIndex);

            // configure binding
            var binding = new NetNamedPipeBinding(NetNamedPipeSecurityMode.None)
                {
                    Name = channelUri,
                    CloseTimeout = TimeSpan.MaxValue,
                    OpenTimeout = TimeSpan.MaxValue,
                    ReaderQuotas = {MaxStringContentLength = 5242880},
                    ReceiveTimeout = TimeSpan.MaxValue,
                    SendTimeout = TimeSpan.MaxValue,
                };

            var serviceHost = new ServiceHost(sessionObject, new Uri(channelUri))
                {
                    CloseTimeout = TimeSpan.MaxValue,
                    OpenTimeout = TimeSpan.MaxValue,
                };

            serviceHost.AddServiceEndpoint(typeof (T), binding, channelUri);
            serviceHost.Open();

            _hosts.Add(channelUri, serviceHost);

            return channelUri;
        }

        internal static T InitClientChannel<T>(string url)
        {
            var myBinding = new NetNamedPipeBinding(NetNamedPipeSecurityMode.None)
                {
                    ReaderQuotas = {MaxStringContentLength = 5242880},
                    MaxReceivedMessageSize = 5242880,
                    OpenTimeout = TimeSpan.MaxValue,
                    CloseTimeout = TimeSpan.MaxValue,
                };

            var myEndpoint = new EndpointAddress(url);
            var myChannelFactory = new ChannelFactory<T>(myBinding, myEndpoint);

            var channel = myChannelFactory.CreateChannel(myEndpoint);
            return channel;
        }
    }
}
