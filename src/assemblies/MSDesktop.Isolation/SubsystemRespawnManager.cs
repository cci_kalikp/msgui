﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Xml.Linq;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MSDesktop.Isolation.Interfaces;

namespace MSDesktop.Isolation
{
    internal sealed class SubsystemRespawnManager
    {
        private readonly IsolatedSubsystemSite _site;
        private readonly IUnityContainer _container;

        public SubsystemRespawnManager(
            IUnityContainer container,
            IMSDesktopSubsystemSite site)
        {
            _container = container;
            _site = (IsolatedSubsystemSite) site;
        }

        public void RestartSubsystem()
        {
            var newLayout = GetNewLayout();
            OnRestartTriggered(newLayout);
        }

        private string GetNewLayout()
        {
            Dictionary<string, XElement> dictionary;

            var persistenceProfileService = _container.Resolve<PersistenceProfileService>();
            persistenceProfileService.CheckIsUnchanged(out dictionary);
            var layoutDoc = PersistenceProfileService.DictToXDocument(dictionary);

            foreach (var xElement in layoutDoc.Descendants("IView"))
            {
                var id = xElement.Attribute("Id").Value;
                if (_site.WindowIds.ContainsKey(id))
                {
                    var factoryId = _site.WindowIds[id];
                    var stateEl = XElement.Parse(string.Format("<State><IsolatedView FactoryId=\"{0}\" /></State>", factoryId));
                    xElement.Add(stateEl);
                }
            }

            var newLayout = layoutDoc.ToString();
            return newLayout;
        }

        public event EventHandler<RespawnRequiredEventArgs> RestartTriggered;

        /*
        private void OnProcessCrash(object sender, IsolatedProcessStartedEventArgs args)
        {
            _endpointManager.ChildProcessCrashed -= OnProcessCrash;
            _endpointManager.Dispose();

            Application.Current.Dispatcher.Invoke(new Action(() =>
                {
                    var chromeManager = _container.Resolve<IChromeManager>();
                    foreach (var isolatedFactory in _site.WidgetFactories)
                    {
                        chromeManager.RemoveWidget(isolatedFactory);
                    }

                    var crashedWindows = chromeManager.Windows.Where(wvc => _site.WindowIds.ContainsKey(wvc.ID));
                    foreach (var wvc in crashedWindows)
                    {
                        var processCrashedViewViewModel = new ProcessCrashedViewViewModel(this);
                        wvc.Content = new ProcessCrashedView {DataContext = processCrashedViewViewModel};
                    }
                }));
        }*/

        private void OnRestartTriggered(string newLayout)
        {
            var handler = RestartTriggered;
            if (handler != null)
            {
                handler(this, new RespawnRequiredEventArgs(newLayout));
            }
        }
    }
}
