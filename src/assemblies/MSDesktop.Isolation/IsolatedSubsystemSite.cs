﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting.Lifetime;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.ServiceModel;
using System.Windows;
using System.Windows.Threading;
using System.Xml.Linq;
using MSDesktop.Isolation.Enums;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.ProcessIsolation;
using MorganStanley.MSDotNet.My;
using MSDesktop.Isolation.AddInProxy;
using MSDesktop.Isolation.Interfaces;
using MSDesktop.Isolation.Proxies;
using InitialLocation = MorganStanley.MSDotNet.MSGui.Core.InitialLocation;
using ModuleStatus = MorganStanley.MSDotNet.MSGui.Core.ModuleStatus;
using ResizeMode = System.Windows.ResizeMode;
using SizingMethod = MorganStanley.MSDotNet.MSGui.Core.SizingMethod;

namespace MSDesktop.Isolation
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, IncludeExceptionDetailInFaults = true, UseSynchronizationContext = false)]
    internal class IsolatedSubsystemSite : IMSDesktopSubsystemSite, ISponsor
    {
        private static readonly IMSLogger Logger = MSLoggerFactory.CreateLogger<IsolatedSubsystemSite>();
        private const string BaseServerName = "XProcWindowSite";

        private readonly string _extraAr;
        private readonly IUnityContainer _container;
        private readonly IList<IModuleLoadInfo> _isolatedModules;
        private readonly Dispatcher _dispatcher;
        private readonly IsolatedSubsystemSettings _subsystemSettings;
        private readonly IFormatter _formatter;
        private readonly IProducerConsumerCollection<string> _widgetFactories;
        private readonly IDictionary<string, string> _windowIds;
        private readonly IsolatedSubsystemManager.IsolatedSubsystemManager _subsystemManager;
        private readonly IList<OptionsPageClosure> _pendingOptionsPages;

        private IModuleLoadInfos _moduleLoadInfos;
        private HostProcessModuleGroup _moduleGroup;
        private IChromeManager _chromeManager;
        private ICommunicator _communicator;
        private IEventRegistrator _eventRegistrator;
        private IApplicationOptions _applicationOptions;
        private CommunicationMappedFile _initialLayoutsFile;

        private volatile MappedFileObjectPool _filePool;
        private volatile IXProcCommunicationAddIn _communicationAddIn;
        private volatile string _overrideLayout;

        public event EventHandler<ApplicationStartedEventArgs> ApplicationStarted;

        protected IsolatedSubsystemSite(string isolatedType, string isolatedSubsystemLocation, bool isHostAppInitialized,
                                        string applicationName, bool needsKraken = false, string krakenCpsConfig = null,
                                        string additionalCommandLine = null, bool needsAr = false, string extraAr = null,
                                        string isolatedSubsystemConfigFile = null, string subsystemRibbonPath = null,
                                        string profileName = null,
                                        bool usePrism = false, string extraInformation = null)
        {
            _extraAr = extraAr;
            _dispatcher = Dispatcher.CurrentDispatcher;
            _formatter = new BinaryFormatter();
            _isolatedModules = new List<IModuleLoadInfo>();
            _widgetFactories = new ConcurrentBag<string>();
            _windowIds = new ConcurrentDictionary<string, string>();

            _subsystemSettings = new IsolatedSubsystemSettings
                {
                    HostProcessId = Process.GetCurrentProcess().Id
                };

            if (string.IsNullOrEmpty(isolatedSubsystemLocation))
            {
                isolatedSubsystemLocation = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            }
            else
            {
                if (!string.Equals(
                    TrimPath(isolatedSubsystemLocation),
                    TrimPath(AppDomain.CurrentDomain.BaseDirectory),
                    StringComparison.OrdinalIgnoreCase))
                {
                    AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;
                }
            }
             
            _subsystemSettings.IsHostAppInitialized = isHostAppInitialized;
            _subsystemSettings.IsolatedSubsystemLocation = isolatedSubsystemLocation;
            _subsystemSettings.IsolatedType = isolatedType;
            _subsystemSettings.CpsConfig = krakenCpsConfig;
            _subsystemSettings.IsCpsRequired = needsKraken;
            _subsystemSettings.ApplicationName = applicationName;
            _subsystemSettings.AdditionalCommandLine = additionalCommandLine;
            _subsystemSettings.IsAdditionalArRequired = needsAr;
            _subsystemSettings.ProfileName = profileName;
            _subsystemSettings.IsolatedSubsystemConfigFile = isolatedSubsystemConfigFile;
            _subsystemSettings.SubsystemRibbonPath = subsystemRibbonPath;
            _subsystemSettings.UsePrism = usePrism;
            _subsystemSettings.ExtraInformation = extraInformation;
        }

        internal IsolatedSubsystemSite(IUnityContainer container,
                                       IsolatedSubsystemManager.IsolatedSubsystemManager subsystemManager,
                                       string isolatedType,
                                       string isolatedSubsystemLocation,
                                       string applicationName,
                                       bool isHostAppInitialized,
                                       bool needsKraken,
                                       string krakenCpsConfig,
                                       string additionalCommandLine,
                                       bool needsAr,
                                       string extraAr,
                                       string isolatedSubsystemConfigFile,
                                       string subsystemRibbonPath,
                                       string profileName,
                                       bool usePrism,
                                       string extraInformation)
            : this(
                isolatedType, isolatedSubsystemLocation, isHostAppInitialized, applicationName, needsKraken,
                krakenCpsConfig,
                additionalCommandLine, needsAr, extraAr, isolatedSubsystemConfigFile, subsystemRibbonPath,
                profileName, usePrism, extraInformation)
        {
            _container = container;
            _subsystemManager = subsystemManager;

            if (isHostAppInitialized)
            {
                InitializeMsDesktopObjects();
            }
            else
            {
                _pendingOptionsPages = new List<OptionsPageClosure>();
            }
        }

        internal IsolatedSubsystemSite(IUnityContainer container,
                                       IsolatedSubsystemManager.IsolatedSubsystemManager subsystemManager,
                                       string isolatedType,
                                       string applicationName,
                                       bool isHostAppInialized,
                                       string profileName)
            : this(container, subsystemManager, isolatedType, null, applicationName, isHostAppInialized, false,
                   null, null, false, null, null, null, profileName, false, null)
        {
            _subsystemSettings.IsolatedSubsystemLocation = null;

        }

        /*
        public IProducerConsumerCollection<string> WidgetFactories
        {
            get { return _widgetFactories; }
        }
        */
        public IDictionary<string, string> WindowIds
        {
            get { return _windowIds; }
        }

        private static string TrimPath(string path)
        {
            path = Path.GetFullPath(path).Trim();
            if (path[path.Length - 1] == '\\')
            {
                path = path.Substring(0, path.Length - 1);
            }

            return path;
        }

        private Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            if (args.Name.Contains(".resources,"))
                return null;
            var asm = new AssemblyName(args.Name);
            try
            {
                return ResolveAssembly(asm);
            }
            catch (InvalidOperationException)
            {
                return null;
            }
        }

        private Assembly ResolveAssembly(AssemblyName asmName)
        {
            var filePathBase1 = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, asmName.Name);
            var filePathBase2 = Path.Combine(_subsystemSettings.IsolatedSubsystemLocation, asmName.Name);
            string[] filesToTry =
                {
                    filePathBase1 + ".exe", filePathBase1 + ".dll", filePathBase2 + ".exe",
                    filePathBase2 + ".dll"
                };

            var asm = filesToTry
                .Where(File.Exists)
                .Select(Assembly.LoadFrom)
                .FirstOrDefault();

            return asm;
        }

        internal string ExtraAr
        {
            get { return _extraAr; }
        }

        public virtual string GetPayloadType()
        {
            return typeof (object).AssemblyQualifiedName;
        }

        private bool _useLauncherBar;
        internal void InitializeMsDesktopObjects()
        {
            var shellMode = _container.Resolve<InitialSettings>().ShellMode;
            _subsystemSettings.ShellMode = (ShellMode)shellMode;
            _useLauncherBar = shellMode ==
                              MorganStanley.MSDotNet.MSGui.Impl.Extensions.ShellMode.LauncherBarAndFloatingWindows ||
                              shellMode == MorganStanley.MSDotNet.MSGui.Impl.Extensions.ShellMode.LauncherBarAndWindow;
            _applicationOptions = _container.Resolve<IApplicationOptions>();
            _eventRegistrator = _container.Resolve<IEventRegistrator>();
            _communicator = _container.Resolve<ICommunicator>();
            _moduleGroup = _container.Resolve<HostProcessModuleGroup>();
            _chromeManager = _container.Resolve<IChromeManager>();
            _moduleLoadInfos = _container.Resolve<IModuleLoadInfos>();

            _communicator.EnableIpcImc(AppEnvironment.Current);

            // Process pending options pages
            if (_pendingOptionsPages != null)
            {
                foreach (var pendingPageClosure in _pendingOptionsPages)
                {
                    AddOptionsPageProc(pendingPageClosure);
                }

                _pendingOptionsPages.Clear();
            }
        }

        internal string GetCurrentProfileName()
        {
            string profileName;
            var initialSettings = GetIsolatedSubsystemSettings();

            if (!string.IsNullOrEmpty(initialSettings.ProfileName))
            {
                profileName = initialSettings.ProfileName;
            }
            else
            {
                var profileService = _container.Resolve<PersistenceProfileService>();
                profileName = profileService.CurrentProfile;
            }

            return profileName;
        }

        internal void SetSubsystemLayout(string layout)
        {
            if (_initialLayoutsFile.IsOpen())
            {
                _initialLayoutsFile.WriteString(layout);
                _subsystemSettings.LayoutFileSize = _initialLayoutsFile.LastWriteLength;
            }
            else
            {
                // Cache the layout and load it once we create the file
                _overrideLayout = layout;
            }
        }

        internal int LoadLayoutIntoProfileFile()
        {
            var profileName = GetCurrentProfileName();
            var storage = _container.Resolve<IPersistenceStorage>();
            var state = storage.LoadState(profileName);
            var layout = state != null
                             ? state.ToString()
                             : string.Empty;

            _initialLayoutsFile.WriteString(layout); 
            return _initialLayoutsFile.LastWriteLength;
        }

        public IsolatedSubsystemSettings GetIsolatedSubsystemSettings(int processId, string settingsUri,
                                                                      string processAddinUri)
        {
            var settings = GetIsolatedSubsystemSettings();
            _filePool = new MappedFileObjectPool(processId);

            if (!_initialLayoutsFile.IsOpen())
            {
                _initialLayoutsFile = _filePool.Take();

                if (_subsystemSettings.IsHostAppInitialized || ProcessIsolationExtensions.IsMsdesktopInitialized())
                {
                    // Write the layout data into the file, and pass the file data in the settings
                    int profileLength;
                    if (string.IsNullOrEmpty(_overrideLayout))
                    {
                        profileLength = LoadLayoutIntoProfileFile();
                    }
                    else
                    {
                        _initialLayoutsFile.WriteString(_overrideLayout);
                        profileLength = _initialLayoutsFile.LastWriteLength;
                        _overrideLayout = null;
                    }

                    settings.IsHostAppInitialized = true;
                    settings.LayoutFileHandle = _initialLayoutsFile.IsolatedProcessHandle.ToInt64();
                    settings.LayoutFileSize = profileLength;

                    var profileName = GetCurrentProfileName();
                    settings.ProfileName = profileName;
                }
            }
            else
            {
                // We assume the correct layout has been loaded into the file
                settings.LayoutFileSize = _initialLayoutsFile.LastWriteLength;
                settings.LayoutFileHandle = _initialLayoutsFile.IsolatedProcessHandle.ToInt64();
            }

            // Update the status with the subsystem manager
            _subsystemManager.HandleInitialSubsystemCommunication(settingsUri, processId, processAddinUri);

            return settings;
        }

        internal bool IsIsolatedProcessInitialized
        {
            get { return _subsystemSettings.HostProcessId > 0; }
        }

        internal IsolatedSubsystemSettings GetIsolatedSubsystemSettings()
        {
            return _subsystemSettings;
        }

        internal void HandleIsolatedProcessCrash(SubsystemRespawnManager respawnManager, bool replaceWindows)
        {
            // Zero the communication addin to prevent its further use - it's in invalid state
            var communicationAddin = _communicationAddIn;
            _communicationAddIn = null;

            // Release the communication addin
            var clientChannel = communicationAddin as IClientChannel;
            if (clientChannel != null)
            {
                try
                {
                    if (clientChannel.State == CommunicationState.Opened)
                    {
                        clientChannel.Close();
                    }
                }
                catch
                {
                }
                finally
                {
                    clientChannel.Dispose();
                }
            }

            _isolatedModules.Clear();

            // Layouts file and file pool tailored to the specific process are no longer necessary
            if (_filePool != null && !_initialLayoutsFile.HostProcessHandle.Equals(IntPtr.Zero))
            {
                if (_initialLayoutsFile.IsOpen())
                {
                    _initialLayoutsFile.Close();
                }
                _filePool.Return(_initialLayoutsFile);
            }

            _filePool = null;

            // Replace the windows
            //if (replaceWindows)
            //{
                _dispatcher.Invoke((Action<SubsystemRespawnManager>)ReplaceWindowsProc, respawnManager);
            //}
        }

        private void ReplaceWindowsProc(SubsystemRespawnManager respawnManager)
        {
            foreach (var isolatedFactory in _widgetFactories)
            {
                _chromeManager.RemoveWidget(isolatedFactory);
            }

            var crashedWindows = _chromeManager.Windows.Where(wvc => _windowIds.ContainsKey(wvc.ID));
            foreach (var wvc in crashedWindows)
            {
                var processCrashedViewViewModel = new ProcessCrashedViewViewModel(respawnManager);
                wvc.Content = new ProcessCrashedView { DataContext = processCrashedViewViewModel };
            }
        }

        public void SubscribeM2M(string messageType, string communicationAddInUrl)
        {
            if (_communicator != null)
            {
                var type = Type.GetType(messageType, true);

                if (_communicationAddIn == null)
                {
                    _communicationAddIn =
                        ProcessIsolationUtility.InitClientChannel<IXProcCommunicationAddIn>(communicationAddInUrl);
                }

                MessagingReflectionUtilities.GetSubscriber(_communicator, type,
                                                           new Action<object>(OnMessageForIsolatedProcess));
            }
        }

        public void PublishM2M(byte[] objectBytestream)
        {
            object message;
            using (var stream = new MemoryStream(objectBytestream))
            {
                message = _formatter.Deserialize(stream);
            }

            if (_communicator != null)
            {
                var type = message.GetType();
                var publisher = MessagingReflectionUtilities.GetPublisher(_communicator, type);
                MessagingReflectionUtilities.Publish(publisher, message);
            }
        }

        private void OnMessageForIsolatedProcess(object message)
        {
            // communicationAddin can be null if the process crashed, but Kraken still sends messages in the background
            if (_communicationAddIn != null)
            {
                // Pass the message to the isolated process
                byte[] bytes;
                using (var stream = new MemoryStream())
                {
                    _formatter.Serialize(stream, message);
                    bytes = stream.GetBuffer();
                }

                _communicationAddIn.Receive(bytes);
            }
        }

        public virtual bool IsControl
        {
            get { return false; }
        }

        internal CommunicationMappedFile ProfileFile
        {
            get { return _initialLayoutsFile; }
        }

        internal void OnIsolatedProcessKilled()
        {
            foreach (ModuleLoadInfo isolatedModule in _isolatedModules)
            {
                if (isolatedModule.Status == ModuleStatus.Loaded ||
                    isolatedModule.Status == ModuleStatus.NotLoaded)
                {
                    isolatedModule.Status = ModuleStatus.ProcessExited;
                }
            }
        }

        public void SetModuleStatus(string moduleTypeName, Enums.ModuleStatus status, Exception exception = null)
        {
            if (_moduleLoadInfos == null || IsControl) return;

            var info = _moduleLoadInfos.GetModuleLoadInfo(moduleTypeName) ??
                       _moduleLoadInfos.AddModuleLoadInfo(moduleTypeName);

            if (!_isolatedModules.Contains(info))
            {
                _isolatedModules.Add(info);
            }

            info.OutOfProcess = true;
            info.Status = (ModuleStatus) (int) status;
            info.Error = exception;
            if (info.Status == ModuleStatus.NotLoaded)
            {
                _moduleLoadInfos.StartLoading(info);
            }
            else if (info.Status == ModuleStatus.Loaded)
            {
                _moduleLoadInfos.EndLoading(info);
            }
        }

        public void SetApplicationStarted(int processId, IEnumerable<string> windowFactoryIds)
        {
            var copy = ApplicationStarted;
            if (copy != null)
            {
                copy(this, new ApplicationStartedEventArgs(processId, windowFactoryIds));
            }
        }

        public void SetLogFilePath(string logFilePath)
        {
            Logger.Emergency(logFilePath, "SetLogFilePath");
        }

        public string AddWidget(string addinUri, string widgetFactoryId, long hwnd, string parentFactoryId,
                                string widgetXaml, string isolatedButtonSize)
        {
            // TODO(hrechkin): Don't hold threadpool threads while UI thread processes its logic.
            var addin = ProcessIsolationUtility.InitClientChannel<IXProcUiAddIn>(addinUri);
            var windowSiteWrapper = new XProcWidgetSiteClosure(addin, this, _chromeManager, parentFactoryId,
                                                               widgetFactoryId, isolatedButtonSize, new IntPtr(hwnd),
                                                               _filePool);

            var windowSite =
                (XProcWindowSite) _dispatcher.Invoke((XProcWindowSiteOperation) AddWidgetProc, windowSiteWrapper);
            _widgetFactories.TryAdd(widgetFactoryId);

            return ProcessIsolationUtility.RegisterServerChannel<IXProcWindowSite>(BaseServerName, windowSite);
        }

        public void AddRemoteShowWindowButton(string widgetFactoryId, string text, string windowFactoryId, string root,
                                              string location)
        {
            _dispatcher.Invoke((Action) (() =>
                {
                    try
                    {
                        _chromeManager.PlaceWidget(widgetFactoryId, location, new InitialButtonParameters
                            {
                                Text = text,
                                Click = (sender, args) =>
                                    {
                                        var site =
                                            _subsystemManager._isolatedSubsystems.Values.FirstOrDefault(
                                                x => x.IsolatedSubsystemSite == this);
                                        if (site != null)
                                            site.CreateRemoteWindow(windowFactoryId, null);
                                    }
                            });
                    }
                    catch (Exception e) { }
                }));
        }

        private object AddWidgetProc(object obj)
        {
            var closureInternal = (XProcWidgetSiteClosure) obj;
            var proxy = new AddInProxy.AddInProxy(closureInternal.Addin, closureInternal.SubsystemSite,
                                                  closureInternal.Hwnd, closureInternal.FilePool);
            var parent = closureInternal.ChromeManager.GetWidget(closureInternal.ParentFactoryId);
            var host = _useLauncherBar ? new SubsystemControlHost(closureInternal.Hwnd, proxy) : new SubsystemRibbonControlHost(closureInternal.Hwnd, proxy, closureInternal.IsolatedButtonSize);
            var windowSite = new XProcWindowSite(host);

            var parameters = new InitialExistingElementWidgetParameters(host);
            closureInternal.ChromeManager.AddWidget(closureInternal.WidgetFactoryId, parameters, parent);

            return windowSite;
        }

        private object PlaceWidgetProc(object obj)
        {
            var closureInternal = (XProcWidgetSiteClosure) obj;
            var proxy = new AddInProxy.AddInProxy(closureInternal.Addin, closureInternal.SubsystemSite,
                                                  closureInternal.Hwnd, closureInternal.FilePool);
            var host = _useLauncherBar ? new SubsystemControlHost(closureInternal.Hwnd, proxy) : new SubsystemRibbonControlHost(closureInternal.Hwnd, proxy, closureInternal.IsolatedButtonSize);
            var parameters = new InitialExistingElementWidgetParameters(host);
            var windowSite = new XProcWindowSite(host); 
            closureInternal.ChromeManager.PlaceWidget(closureInternal.WidgetFactoryId, closureInternal.Root,
                                                      closureInternal.Location, parameters);

            return windowSite;
        }

        public string PlaceWidget(string addinUri, string widgetFactoryId, long hwnd, string root,
                                  string location,
                                  string widgetXaml,
                                  string isolatedButtonSize)
        {
            // TODO(hrechkin): Don't hold threadpool threads while UI thread processes its logic.
            var addIn = ProcessIsolationUtility.InitClientChannel<IXProcUiAddIn>(addinUri);
            var windowSiteWrapper = new XProcWidgetSiteClosure(addIn, this, _chromeManager, widgetFactoryId, root,
                                                               location, isolatedButtonSize, new IntPtr(hwnd), _filePool);

            var windowSite =
                (XProcWindowSite) _dispatcher.Invoke((XProcWindowSiteOperation) PlaceWidgetProc, windowSiteWrapper);
            _widgetFactories.TryAdd(widgetFactoryId);

            return ProcessIsolationUtility.RegisterServerChannel<IXProcWindowSite>(BaseServerName, windowSite);
        }

        public string AddWindow(string addinUri, long hwnd,
                                InitialWindowParametersProxy initialParametersProxy,
                                string windowTitle, string initialState)
        {
            // TODO(hrechkin): Don't hold threadpool threads while UI thread processes its logic.
            var addIn = ProcessIsolationUtility.InitClientChannel<IXProcUiAddIn>(addinUri);
            var windowSiteWrapper = new XProcWindowSiteClosure(addIn, new IntPtr(hwnd), this, windowTitle,
                                                               _eventRegistrator, _chromeManager,
                                                               initialParametersProxy, initialState, _filePool);

            var windowSite =
                (XProcWindowSite) _dispatcher.Invoke((XProcWindowSiteOperation) AddWindowProc, windowSiteWrapper);

            _windowIds[initialParametersProxy.ViewId] = initialParametersProxy.FactoryId;

            return ProcessIsolationUtility.RegisterServerChannel<IXProcWindowSite>(BaseServerName, windowSite);
        }

        public void AddOptionsPage(string path, string title, string addinUri, long hwnd)
        {
            var addin = ProcessIsolationUtility.InitClientChannel<IXProcUiAddIn>(addinUri);
            var closure = new OptionsPageClosure(path, title, addin, new IntPtr(hwnd), this, _filePool);

            _dispatcher.Invoke((Action<object>) AddOptionsPageProc, closure);
        }

        private void AddOptionsPageProc(object obj)
        {
            var closure = (OptionsPageClosure) obj;

            if (_applicationOptions != null)
            {
                var addinProxy = new AddInProxy.AddInProxy(closure.Addin, closure.SubsystemSite, closure.Hwnd,
                                                           closure.FilePool);
                var controlHost = new SubsystemViewHost(closure.Hwnd, addinProxy);
                _applicationOptions.AddOptionPage(closure.Path, closure.Title,
                                                  new IsolatedOptionView(controlHost, closure.Hwnd));
            }
            else
            {
                // Save the closure for insertion once we get the valid application options object
                _pendingOptionsPages.Add(closure);
            }
        }

        private static object AddWindowProc(object obj)
        {
            var closureInternal = (XProcWindowSiteClosure) obj;
            var proxy = new AddInProxy.AddInProxy(closureInternal.Addin, closureInternal.SubsystemSite,
                                                  closureInternal.Hwnd, closureInternal.FilePool);
            var host = new SubsystemViewHost(closureInternal.Hwnd, proxy);

            var initialParametersProxy = closureInternal.InitialWindowParametersProxy;

            // Load the initial state into XDocument
            XDocument xdoc = null;
            if (!string.IsNullOrEmpty(closureInternal.InitialState))
            {
                using (var reader = new StringReader(closureInternal.InitialState))
                {
                    xdoc = XDocument.Load(reader);
                }
            }

            var initialParameters = new InitialIsolatedViewParameters(initialParametersProxy.ViewId, host, xdoc);

            if (initialParametersProxy != null)
            {
                initialParameters.Transient = initialParametersProxy.Transient;
                initialParameters.InitialLocation = (InitialLocation) (int) initialParametersProxy.InitialLocation;
                initialParameters.UseDockManager = initialParametersProxy.UseDockManager;
                initialParameters.ShowFlashBorder = initialParametersProxy.ShowFlashBorder;
                initialParameters.IsModal = initialParametersProxy.IsModal;
                initialParameters.SingletonKey = initialParametersProxy.SingletonKey;
                initialParameters.SizingMethod = (SizingMethod) (int) initialParametersProxy.SizingMethod;
                initialParameters.Topmost = initialParametersProxy.Topmost;
                initialParameters.ResizeMode = (ResizeMode) (int) initialParametersProxy.ResizeMode;
                initialParameters.EnforceSizeRestrictions = initialParametersProxy.EnforceSizeRestrictions;
                initialParameters.Title = closureInternal.WindowTitle;
            }

            var windowSite = new XProcWindowSite(host, closureInternal.ChromeManager, closureInternal.EventRegistrator);
            EventHandler<MessageArrivedEventArgs> onMessageArrived =
                (sender, args) => proxy.TriggerMessageArrived(args.Message);
            windowSite.MessageArrived += onMessageArrived;

            var window = closureInternal.SubsystemSite.CreateWindow(initialParameters.ViewId, initialParameters);

            EventHandler<WindowEventArgs> onWindowClose = null;
            EventHandler<CancelEventArgs> onWindowClosing = null;
            EventHandler<WindowEventArgs> onWindowClosedQuitely = null;

            onWindowClose = (sender, args) =>
                {
                    if (window.Content is ProcessCrashedView) return;
                    proxy.InvokeClosed();
                    windowSite.MessageArrived -= onMessageArrived;
                    window.Closing -= onWindowClosing;
                    window.Closed -= onWindowClose;
                    window.ClosedQuietly -= onWindowClosedQuitely;
                };

            onWindowClosing = (s, e) =>
                {
                    if (window.Content is ProcessCrashedView) return;
                    e.Cancel = proxy.InvokeClosing();
                };

            onWindowClosedQuitely = (sender, args) =>
                {
                    if (window.Content is ProcessCrashedView) return;
                    proxy.InvokeClosedQuietly();
                    windowSite.MessageArrived -= onMessageArrived;
                    window.Closing -= onWindowClosing;
                    window.Closed -= onWindowClose;
                    window.ClosedQuietly -= onWindowClosedQuitely;
                };

            window.Closed += onWindowClose;
            window.Closing += onWindowClosing;
            window.ClosedQuietly += onWindowClosedQuitely;
            window.Title = closureInternal.WindowTitle;

            closureInternal.SubsystemSite.PostCreateWindow(proxy, closureInternal.Hwnd, host);

            return windowSite;
        }

        public void ViewLoadedCompletely(string connectionInfo)
        {
            XDocument doc = null;
            if (!string.IsNullOrEmpty(connectionInfo))
            {
                using (var reader = new StringReader(connectionInfo))
                {
                    doc = XDocument.Load(reader);
                }
            }

            _moduleGroup.ViewLoadedCompletely(doc);
        }

        protected virtual void PostCreateWindow(IXProcAddInHost addIn, IntPtr hwnd, FrameworkElement element)
        {
        }

        protected virtual IWindowViewContainer CreateWindow(string viewId, InitialWindowParameters initialParameters)
        {
            IWindowViewContainer windowViewContainer;
            if (string.IsNullOrEmpty(viewId) ||
                (windowViewContainer = _chromeManager.Windows.FirstOrDefault(w => w.ID == viewId)) == null)
            {
                windowViewContainer =
                    _chromeManager.CreateWindow(ProcessIsolationExtensions.IsolatedProcessWindowFactoryName,
                                                initialParameters);
            }
            else if (windowViewContainer.Content == null || windowViewContainer.Content is ProcessCrashedView)
                // handle respawn
            {
                windowViewContainer.Content =
                    ((InitialIsolatedViewParameters) initialParameters).IsolatedControlHost;
            }

            return windowViewContainer;
        }

        TimeSpan ISponsor.Renewal(ILease lease)
        {
            return lease.RenewOnCallTime;
        }

        private delegate object XProcWindowSiteOperation(object wrapper);

        private abstract class XprocClosureBase
        {
            private readonly IXProcUiAddIn _addin;
            private readonly IsolatedSubsystemSite _subsystemSite;
            private readonly IntPtr _hwnd;
            private readonly MappedFileObjectPool _filePool;

            protected XprocClosureBase(IXProcUiAddIn addin, IntPtr hwnd, IsolatedSubsystemSite subsystemSite,
                                       MappedFileObjectPool filePool)
            {
                _hwnd = hwnd;
                _subsystemSite = subsystemSite;
                _filePool = filePool;
                _addin = addin;
            }

            public IXProcUiAddIn Addin
            {
                get { return _addin; }
            }

            public IsolatedSubsystemSite SubsystemSite
            {
                get { return _subsystemSite; }
            }

            public IntPtr Hwnd
            {
                get { return _hwnd; }
            }

            public MappedFileObjectPool FilePool
            {
                get { return _filePool; }
            }
        }

        private sealed class OptionsPageClosure : XprocClosureBase
        {
            private readonly string _path;
            private readonly string _title;

            public OptionsPageClosure(string path, string title, IXProcUiAddIn addin, IntPtr hwnd,
                                      IsolatedSubsystemSite subsystemSite, MappedFileObjectPool filePool)
                : base(addin, hwnd, subsystemSite, filePool)
            {
                _title = title;
                _path = path;
            }

            public string Path
            {
                get { return _path; }
            }

            public string Title
            {
                get { return _title; }
            }
        }

        private sealed class XProcWindowSiteClosure : XprocClosureBase
        {
            private readonly InitialWindowParametersProxy _initialWindowParametersProxy;
            private readonly string _windowTitle;
            private readonly IEventRegistrator _eventRegistrator;
            private readonly IChromeManager _chromeManager;
            private readonly string _initialState;

            public XProcWindowSiteClosure(IXProcUiAddIn addin, IntPtr hwnd, IsolatedSubsystemSite subsystemSite,
                                          string windowTitle, IEventRegistrator eventRegistrator,
                                          IChromeManager chromeManager,
                                          InitialWindowParametersProxy initialWindowParametersProxy, string initialState,
                                          MappedFileObjectPool filePool)
                : base(addin, hwnd, subsystemSite, filePool)
            {
                _initialState = initialState;
                _chromeManager = chromeManager;
                _eventRegistrator = eventRegistrator;
                _windowTitle = windowTitle;
                _initialWindowParametersProxy = initialWindowParametersProxy;
            }

            public InitialWindowParametersProxy InitialWindowParametersProxy
            {
                get { return _initialWindowParametersProxy; }
            }

            public string WindowTitle
            {
                get { return _windowTitle; }
            }

            public IEventRegistrator EventRegistrator
            {
                get { return _eventRegistrator; }
            }

            public IChromeManager ChromeManager
            {
                get { return _chromeManager; }
            }

            public string InitialState
            {
                get { return _initialState; }
            }
        }

        private sealed class XProcWidgetSiteClosure : XprocClosureBase
        {
            private readonly IChromeManager _chromeManager;
            private readonly string _parentFactoryId;
            private readonly string _widgetFactoryId;
            private readonly string _root;
            private readonly string _location;
            private readonly string _isolatedButtonSize;

            private XProcWidgetSiteClosure(
                IXProcUiAddIn addin,
                IsolatedSubsystemSite subsystemSite,
                IChromeManager chromeManager,
                string parentFactoryId,
                string widgetFactoryId,
                string root,
                string location,
                string isolatedButtonSize,
                IntPtr hwnd,
                MappedFileObjectPool filePool)
                : base(addin, hwnd, subsystemSite, filePool)
            {
                _isolatedButtonSize = isolatedButtonSize;
                _location = location;
                _root = root;
                _widgetFactoryId = widgetFactoryId;
                _parentFactoryId = parentFactoryId;
                _chromeManager = chromeManager;
            }

            public XProcWidgetSiteClosure(
                IXProcUiAddIn addin,
                IsolatedSubsystemSite subsystemSite,
                IChromeManager chromeManager,
                string widgetFactoryId,
                string root,
                string location,
                string isolatedButtonSize,
                IntPtr hwnd,
                MappedFileObjectPool filePool)
                : this(addin, subsystemSite, chromeManager, null, widgetFactoryId, root, location, isolatedButtonSize, hwnd, filePool)
            {
            }

            public XProcWidgetSiteClosure(
                IXProcUiAddIn addin,
                IsolatedSubsystemSite subsystemSite,
                IChromeManager chromeManager,
                string parentFactoryId,
                string widgetFactoryId,
                string isolatedButtonSize,
                IntPtr hwnd,
                MappedFileObjectPool filePool)
                : this(
                    addin, subsystemSite, chromeManager, parentFactoryId, widgetFactoryId, null, null,
                    isolatedButtonSize, hwnd, filePool)
            {
            }

            public IChromeManager ChromeManager
            {
                get { return _chromeManager; }
            }

            public string ParentFactoryId
            {
                get { return _parentFactoryId; }
            }

            public string WidgetFactoryId
            {
                get { return _widgetFactoryId; }
            }

            public string Root
            {
                get { return _root; }
            }

            public string Location
            {
                get { return _location; }
            }

            public string IsolatedButtonSize
            {
                get { return _isolatedButtonSize; }
            }
        }
    }

    internal sealed class ApplicationStartedEventArgs : EventArgs
    {
        private readonly int _processId;
        private readonly IEnumerable<string> _windowFactoryIds;

        public ApplicationStartedEventArgs(int processId, IEnumerable<string> windowFactoryIds)
        {
            _windowFactoryIds = windowFactoryIds;
            _processId = processId;
        }

        public int ProcessId
        {
            get { return _processId; }
        }

        public IEnumerable<string> WindowFactoryIds
        {
            get { return _windowFactoryIds; }
        }
    }
}
