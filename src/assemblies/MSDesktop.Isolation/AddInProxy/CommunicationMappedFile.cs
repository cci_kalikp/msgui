﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core.Win32;

namespace MSDesktop.Isolation.AddInProxy
{
    [StructLayout(LayoutKind.Sequential)]
    internal struct CommunicationMappedFile
    {
        private static IntPtr _fileDataPtr;

        private IntPtr _hostProcessHandle;
        private IntPtr _isolatedProcessHandle;
        private int _lastWriteLength;

        public IntPtr HostProcessHandle
        {
            get { return _hostProcessHandle; }
        }

        public IntPtr IsolatedProcessHandle
        {
            get { return _isolatedProcessHandle; }
        }

        public int LastWriteLength
        {
            get { return _lastWriteLength; }
        }

        internal bool IsInitialized()
        {
            return _hostProcessHandle != IntPtr.Zero && _isolatedProcessHandle != IntPtr.Zero;
        }

        internal void InitializeFromProcess(int processId)
        {
            var processHandle = Win32.OpenProcess(Win32.ProcessAccess.DupHandle, false, processId);
            if (processHandle != IntPtr.Zero)
            {
                try
                {
                    InitializeFromProcess(processHandle);
                }
                finally
                {
                    Win32.CloseHandle(processHandle);
                }
            }
        }

        internal void InitializeFromProcess(IntPtr processHandle)
        {
            // Initialize the handles
            _hostProcessHandle = Win32.CreateFileMapping(Win32.INVALID_HANDLE_VALUE, IntPtr.Zero,
                                                         Win32.FileMapProtection.PageReadWrite, 0, ushort.MaxValue, null);
            Win32.DuplicateHandle(Process.GetCurrentProcess().Handle, _hostProcessHandle, processHandle,
                                  out _isolatedProcessHandle, (uint)Win32.FileMapAccess.FileMapRead, false, 0);
            var error = Win32.GetLastError();
            if (error != 0)
            {
                var message =
                    string.Format(
                        "Failed to duplicate handle of a memory-mapped file into child process. Win32 error is {0}",
                        error);
                throw new IOException(message);
            }
        }

        internal void WriteString(string data)
        {
            var stringBytes = Encoding.UTF8.GetBytes(data);
            WriteData(stringBytes);
        }

        internal static string ReadString(long fileHandle, int fileSize)
        {
            var bytes = GetFileBytes(new IntPtr(fileHandle), fileSize);
            var s = Encoding.UTF8.GetString(bytes);
            return s;
        }

        internal void WriteData(byte[] data)
        {
            var length = data.Length;

            // Write data into host process handle
            var mappedBuffer = Win32.MapViewOfFile(_hostProcessHandle, Win32.FileMapAccess.FileMapWrite, 0, 0,
                                                   new UIntPtr((uint) length));
            try
            {
                Marshal.Copy(data, 0, mappedBuffer, length);
                _lastWriteLength = length;
            }
            finally
            {
                Win32.UnmapViewOfFile(mappedBuffer);
            }
        }

        internal void Close()
        {
            if (!_hostProcessHandle.Equals(IntPtr.Zero))
            {
                Win32.CloseHandle(_hostProcessHandle);
                _hostProcessHandle = IntPtr.Zero;
            }

            //if (!_isolatedProcessHandle.Equals(IntPtr.Zero))
            //{
            //    try
            //    {
            //        Win32.CloseHandle(_isolatedProcessHandle);
            //    }
            //    catch (Exception)
            //    {
                    
            //    }
            //    _isolatedProcessHandle = IntPtr.Zero;
            //}

            _lastWriteLength = 0;
        }

        internal bool IsOpen()
        {
            // Both handles must be present
            return !_isolatedProcessHandle.Equals(IntPtr.Zero)
                   && !_hostProcessHandle.Equals(IntPtr.Zero);
        }

        internal IntPtr MarshalToUnmanaged()
        {
            var ptr = Marshal.AllocHGlobal(Marshal.SizeOf(this));
            Marshal.StructureToPtr(this, ptr, false);
            return ptr;
        }

        internal static CommunicationMappedFile MarshalFromUnmanaged(IntPtr memory, bool autoFree = true)
        {
            var file = (CommunicationMappedFile) Marshal.PtrToStructure(memory, typeof (CommunicationMappedFile));
            if (autoFree)
            {
                Marshal.FreeHGlobal(memory);
            }

            return file;
        }

        internal static byte[] GetFileBytes(IntPtr fileHandle, int fileLength)
        {
            _fileDataPtr = Win32.MapViewOfFile(fileHandle, Win32.FileMapAccess.FileMapRead, 0, 0,
                                              new UIntPtr((uint)fileLength));
            byte[] bytes;
            try
            {
                bytes = new byte[fileLength];
                Marshal.Copy(_fileDataPtr, bytes, 0, fileLength);
            }
            finally
            {
                Win32.UnmapViewOfFile(_fileDataPtr);
            }

            return bytes;
        }
    }
}
