﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core.Win32;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;
using MSDesktop.Isolation.Enums;
using MSDesktop.Isolation.Interfaces;
using MSDesktop.Isolation.Utils;

namespace MSDesktop.Isolation.AddInProxy
{
    internal sealed class AddInProxy : IXProcAddInHost
    {
        private readonly IXProcUiAddIn _addin;
        private readonly IsolatedSubsystemSite _subsystemSite;
        private readonly IntPtr _hwnd;
        private readonly MappedFileObjectPool _filePool;
        private readonly IDictionary<PendingMessageCallbackContainer, Win32.SendAsyncProc> _pendingMessages;
        private readonly ViewMessageActionsCollection _actionsCollection = new ViewMessageActionsCollection(); 

        internal AddInProxy(IXProcUiAddIn addin, IsolatedSubsystemSite subsystemSite, IntPtr hwnd, MappedFileObjectPool filePool)
        {
            _hwnd = hwnd;
            _filePool = filePool;
            _subsystemSite = subsystemSite;
            _addin = addin;
            _pendingMessages = new Dictionary<PendingMessageCallbackContainer, Win32.SendAsyncProc>();
        }

        public IXProcUiAddIn AddIn
        {
            get { return _addin; }
        }

        public void RegisterMessageHandler(Type messageType, Action<object> messageHandler)
        {
            _actionsCollection.RegisterAction(messageType, messageHandler);
        }

        internal void QueueDestroy()
        {
            // QueueCallAddin(() => _addin.ShutDown());
        }

        internal void QueuePaint(byte[] underlyingContentBytes)
        {
            var length = underlyingContentBytes.Length;

            // Get the memory mapped file and write our data there
            var file = _filePool.Take();
            file.WriteData(underlyingContentBytes);

            var wParam = Win32.MAKELONG((int) WindowMessageOperation.ControlBackgroundPaint, length);

            // Perform the async call.
            var marshaledFile = file.MarshalToUnmanaged();

            // Create and register a pending operation, so that it's not garbage collected
            // until callback.
            var operation = new PendingMessageCallbackContainer(_filePool, _pendingMessages);
            Win32.SendAsyncProc managedCallback = operation.AsyncSendProcessingComplete;

            _pendingMessages.Add(operation, managedCallback);
            var messageSendSuccess = Win32.SendMessageCallback(_hwnd, IsolationConstants.WmUserMessage,
                                                               new IntPtr(wParam),
                                                               file.IsolatedProcessHandle, managedCallback,
                                                               marshaledFile);
            if (!messageSendSuccess)
            {
                _pendingMessages.Remove(operation);
                operation.ReturnMarshaledFile(marshaledFile);
            }
        }

        internal void RibbonButtonSizeChanged(object newSizeValue)
        {
            Win32.PostMessage(
                _hwnd,
                IsolationConstants.WmUserMessage,
                Win32.MAKELONG((int) WindowMessageOperation.SetButtonSizingMode, 0),
                (int) ToIsolationResizeMode(newSizeValue.ToString()));
        }

        private static RibbonToolSizingMode ToIsolationResizeMode(string systemMode)
        {
            switch (systemMode)
            {
                case "ImageAndTextLarge":
                    return RibbonToolSizingMode.ImageAndTextLarge;
                case "ImageAndTextNormal":
                    return RibbonToolSizingMode.ImageAndTextNormal;
                case "ImageOnly":
                    return RibbonToolSizingMode.ImageOnly;
            }

            return RibbonToolSizingMode.ImageAndTextLarge;
        }

        internal XDocument Save()
        {
            XDocument xdoc;
            var xml = CallAddinSync(() => _addin.Save());

            if (!string.IsNullOrEmpty(xml))
            {
                using (var reader = new StringReader(xml))
                {
                    xdoc = XDocument.Load(reader);
                }
            }
            else
            {
                xdoc = new XDocument();
            }

            return xdoc;
        }

        internal void Load(XDocument document)
        {
            var xml = document.ToString();
            QueueCallAddin(() => _addin.Load(xml));
        }

        internal void TriggerMessageArrived(object message)
        {
            _actionsCollection.ExecuteActions(message);
        }

        private void QueueCallAddin(Action action)
        {
            ThreadPool.QueueUserWorkItem(o =>
                {

                    try
                    {
                        action();
                    }
                    catch (Exception)
                    {
                        // Trigger process closed exception - pipe should be open.
                        _subsystemSite.OnIsolatedProcessKilled();
                    }
                });
        }

        private T CallAddinSync<T>(Func<T> action)
            where T:class
        {
            try
            {
                var result = action();
                return result;
            }
            catch (PipeException)
            {
                // Trigger process closed exception - pipe should be open.
                _subsystemSite.OnIsolatedProcessKilled();
                return null;
            }
        }

        public bool InvokeClosing()
        {
            return _addin.InvokeClosing();
        }

        public void InvokeClosed()
        {
            _addin.InvokeClosed();
        }

        public void InvokeClosedQuietly()
        {
            _addin.InvokeClosedQuietly();
        }

        private sealed class PendingMessageCallbackContainer
        {
            private readonly MappedFileObjectPool _filePool;
            private readonly IDictionary<PendingMessageCallbackContainer, Win32.SendAsyncProc> _pendingOperations;

            internal PendingMessageCallbackContainer(MappedFileObjectPool filePool, IDictionary<PendingMessageCallbackContainer, Win32.SendAsyncProc> pendingOperations)
            {
                _pendingOperations = pendingOperations;
                _filePool = filePool;
            }

#pragma warning disable 659
            public override bool Equals(object obj)
#pragma warning restore 659
            {
                // All are equals since default implementations of GetHashCode() will return the runtime's ID of this object.
                return true;
            }

            public void AsyncSendProcessingComplete(IntPtr hwnd, uint uMsg, IntPtr dwData, IntPtr lResult)
            {
                ReturnMarshaledFile(dwData);
                _pendingOperations.Remove(this);
            }

            public void ReturnMarshaledFile(IntPtr dwData)
            {
                var file = CommunicationMappedFile.MarshalFromUnmanaged(dwData);
                _filePool.Return(file);
            }
        }
    }
}
