﻿using System;
using MorganStanley.MSDotNet.Channels;
using MorganStanley.MSDotNet.MSGui.Core.Win32;

namespace MSDesktop.Isolation.AddInProxy
{
    internal sealed class MappedFileObjectPool
    {
        private readonly ObjectPool<CommunicationMappedFile> _objectPool = new ObjectPool<CommunicationMappedFile>(int.MaxValue);
        private readonly int _poolProcessId;

        internal MappedFileObjectPool(int poolProcessId, int initialSize = 4)
        {
            _poolProcessId = poolProcessId;
            var processHandle = Win32.OpenProcess(Win32.ProcessAccess.DupHandle, false, poolProcessId);
            if (processHandle != IntPtr.Zero)
            {
                try
                {
                    for (var k = 0; k < initialSize; k++)
                    {
                        var mappedFile = new CommunicationMappedFile();
                        mappedFile.InitializeFromProcess(processHandle);
                        _objectPool.Return(mappedFile);
                    }
                }
                finally
                {
                    Win32.CloseHandle(processHandle);
                }
            }
        }

        public int PoolProcessId
        {
            get { return _poolProcessId; }
        }

        public void Return(CommunicationMappedFile file)
        {
            _objectPool.Return(file);
        }

        public CommunicationMappedFile Take()
        {
            var file = _objectPool.Take();
            if (!file.IsInitialized())
            {
                file.InitializeFromProcess(PoolProcessId);
            }

            return file;
        }
    }
}
