﻿using System;
using MSDesktop.Isolation.Proxies;

namespace MSDesktop.Isolation
{
    internal sealed class IsolatedProcessStartedEventArgs : EventArgs
    {
        private readonly int _processId;
        private readonly IsolatedSubsystemSettings _settings;

        public IsolatedProcessStartedEventArgs(int processId, IsolatedSubsystemSettings settings)
        {
            _settings = settings;
            _processId = processId;
        }

        public int ProcessId
        {
            get { return _processId; }
        }

        public IsolatedSubsystemSettings Settings
        {
            get { return _settings; }
        }
    }
}
