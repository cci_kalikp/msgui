﻿using System;
using System.Runtime.Remoting;

namespace MorganStanley.MSDotNet.MSGui.Impl.ProcessIsolation
{
    /// <summary>
    /// This class is used as part of implementation of Process Isolation feature. 
    /// It is not intended to be used from your code.
    /// </summary>
    [Serializable]
    public sealed class VersionInvariantTypeInfo : IRemotingTypeInfo
    {
        private IRemotingTypeInfo _typeInfo;

        public IRemotingTypeInfo InnerInfo
        {
            get { return _typeInfo; }
            set { _typeInfo = value; }
        }

        public bool CanCastTo(Type fromType, object o)
        {
            return _typeInfo.CanCastTo(fromType, o);
        }

        public string TypeName
        {
            get { return _typeInfo.TypeName; }
            set { _typeInfo.TypeName = value; }
        }
    }
}
