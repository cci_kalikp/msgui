﻿using System;
using System.IO;
using System.Windows.Controls;
using System.Xml.Linq;
using MSDesktop.Isolation;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MSDesktop.Isolation.Interfaces;


namespace MorganStanley.MSDotNet.MSGui.Impl.ProcessIsolation
{
	/// <summary>
	/// Interaction logic for XProcControl.xaml
	/// </summary>
	public partial class XProcControl
	{
	    private readonly string _userControlToActivate;
		private readonly bool _force32Bit;
	    private readonly bool _needsAr;
	    private readonly string _hostDirectory;
	    private readonly string _extraAr;
	    private readonly string _additionalCommandLine;
	    private readonly string _applicationName;
        private readonly Type _payloadType;
        private readonly IChromeManager _chromeManager;
        private readonly DelayedLoadAddInHost _addInHost;

        private XDocument _toLoad;
        private XProcEndpointManager _host;

	    public XProcControl(string userControlToActivate, XDocument state, IChromeManager chromeManager, string applicationName, Type payloadType = null) :
            this(userControlToActivate, state, chromeManager, false, applicationName, payloadType: payloadType)
	    {
	    }

        public XProcControl(string userControlToActivate, XDocument state, IChromeManager chromeManager,
                            bool force32Bit, string applicationName, bool needsAR = false, 
	                        string hostDirectory = null, string extraAR = null,
	                        string additionalCommandLine = null, Type payloadType = null)
	    {
	        _chromeManager = chromeManager;
	        _force32Bit = force32Bit;
	        _needsAr = needsAR;
	        _extraAr = extraAR;
	        _payloadType = payloadType;
	        _additionalCommandLine = additionalCommandLine;
	        _hostDirectory = hostDirectory;
            _applicationName = applicationName;
            _userControlToActivate = userControlToActivate;
            _addInHost = new DelayedLoadAddInHost();
            State = state;
	        InitializeComponent();
	        LoadAddIn();
	    }

	    public void Load(XDocument document)
        {
            _toLoad = document;
        }

	    public XDocument State { get; set; }

	    internal IXProcAddInHost AddInHost
	    {
	        get { return _addInHost; }
	    }

	    public XDocument Save()
	    {
	        XDocument xdoc;
	        var state = _addInHost.AddIn.Save();
            using (var reader = new StringReader(state))
            {
                xdoc = XDocument.Load(reader);
            }

            return xdoc;
        }

		internal void OnIWVCClosed(object sender, EventArgs args)
		{
			UnloadAddIn();
		}

		private void LoadAddIn()
		{
			if (AddinPanel.Child == null)
			{
			    var subsystemSite = new IsolatedSubsystemControlSite(
                    _toLoad, 
                    _userControlToActivate, 
                    _payloadType, 
                    _applicationName, 
                    _hostDirectory, 
                    _additionalCommandLine,
                    _needsAr, 
                    _extraAr);

                // TODO (hrechkin):
                // We don't presently support separate locations for isolating process and isolated subsystem for isolated control functionality
			    var isolatingExecutableLocation = _hostDirectory;
			    _host = new XProcEndpointManager(subsystemSite, isolatingExecutableLocation, _additionalCommandLine,
			                                     XProcEndpointManager.GetDefaultIsolatedExecutable(_force32Bit));
                _host.ChildProcessCrashed += (o, args) => Dispatcher.BeginInvoke((Action)OnChildProcessCrashed);
			    _host.StartProcess();

                subsystemSite.WindowAvailable += OnWindowAvailableAsync;
			}
		}

        void OnWindowAvailableAsync(object sender, FrameworkElementAddedEventArgs e)
        {
            _addInHost.SetAddInHost(e.AddInHost);
            AddinPanel.Child = e.Element;
        }

	    private void OnChildProcessCrashed()
	    {
	        UnloadAddIn();
	        Content = new TextBlock {Text = "This view has crashed. You may close this window."};
	    }

		void UnloadAddIn()
		{
			var host = AddinPanel.Child;
			if (host != null)
			{
				AddinPanel.Child = null;
				// host.Dispose();
			}
		}

		//protected override void OnKeyDown(KeyEventArgs e)
		//{
		//    base.OnKeyDown(e);

		//    if (!e.Handled && e.Key == Key.F5 && 
		//        e.KeyboardDevice.Modifiers == (ModifierKeys.Control|ModifierKeys.Shift))
		//    {
		//        e.Handled = true;
		//        // Do the unloading asynchronously to avoid a deadlock if the key press was bubbled from 
		//        // the add-in. Too much re-entrancy going on!
		//        Dispatcher.BeginInvoke(delegate
		//        {
		//            UnloadAddIn();
		//            LoadAddIn();
		//        });
		//    }
		//}

	}
}
