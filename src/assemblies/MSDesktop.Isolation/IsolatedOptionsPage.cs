﻿using System;
using System.Windows;
using MSDesktop.Isolation.Enums;
using MSDesktop.Isolation.Utils;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.Win32;

namespace MSDesktop.Isolation
{
    internal sealed class IsolatedOptionView : IOptionView
    {
        // TODO (hrechkin):
        // We need to be able to post these requests onto view's UI thread. Blank implementations for now.
        private readonly FrameworkElement _control;
        private readonly IntPtr _hwnd;

        public IsolatedOptionView(FrameworkElement control, IntPtr hwnd)
        {
            _hwnd = hwnd;
            _control = control;
        }

        public void OnOK()
        {
            PostMessageWithOperation(OptionPageOperation.OK);
        }

        public void OnCancel()
        {
            PostMessageWithOperation(OptionPageOperation.Cancel);
        }

        public void OnDisplay()
        {
            PostMessageWithOperation(OptionPageOperation.Display);
        }

        public void OnHelp()
        {
            PostMessageWithOperation(OptionPageOperation.Help);
        }

        public UIElement Content
        {
            get { return _control; }
        }

        private void PostMessageWithOperation(OptionPageOperation operation)
        {
            Win32.PostMessage(_hwnd, IsolationConstants.WmUserMessage, (int)operation, 0);
        }
    }
}
