﻿using System;
using MSDesktop.MessageRouting.Message;
using MSDesktop.MessageRouting.Routing;

namespace MSDesktop.Isolation.Kraken
{
    [Broadcast]
    [InternalMessage]
    [DeliveryStrategy(DeliveryStrategy = DeliveryStrategy.FireAndForget)]// commented by farm: rest is redundant//, Fallback = false, RetryCount = 1)]
    [Serializable]
    internal sealed class SubsystemRouteMessage: IProvideMessageInfo
    {
        public SubsystemRouteMessage()
        {
        }

        public Endpoint Origin { get; set; }
        public Endpoint Target { get; set; }
        public string MessageTypeIdentifier { get; set; }
        public bool RouteByDefault { get; set; }
        public int HostProcessId { get; set; }
        public int IsolatedProcessId { get; set; }

        public string GetInfo()
        {
            return string.Format("Subsystem with process id {0} of host process id {1} subscribes message {2}",
                                 IsolatedProcessId, HostProcessId, MessageTypeIdentifier);
        }
    }
}
