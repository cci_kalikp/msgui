﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.ProcessIsolation;
using MSDesktop.Isolation.Kraken;
using MSDesktop.Isolation.Proxies;
using MSDesktop.MessageRouting.Routing;

namespace MSDesktop.MessageRouting.Isolation
{
    /// <summary>
    /// Performs automatic routing for in-app subscriptions across isolated subsystems.
    /// </summary>
    public sealed class PreferLocalEndpointsAcrossSubsystems
    {
        private static readonly Dictionary<string, Type> TypeCache = new Dictionary<String, Type>();
        internal static bool IsEnabled = true;

        private readonly IEndpointSelector _endpointSelector;
        private readonly IApplication _application;
        private readonly RouteManager _routeManager;
        private readonly Dictionary<Type, Route> _defaultRoutedTypes = new Dictionary<Type, Route>();
        private readonly IsolatedSubsystemSettings _isolatedSubsystemSettings;
        private readonly IRoutedPublisher<SubsystemRouteMessage> _subsystemRoutePublisher;
        private readonly object _routeByTypeLock = new object();

        public PreferLocalEndpointsAcrossSubsystems(IEndpointSelector endpointSelector, IRoutingCommunicator communicator, IApplication application, IRouteManager routeManager)
        {
            _endpointSelector = endpointSelector;
            _application = application;
            _routeManager = routeManager as RouteManager;

            // Get the isolated subsystem settings, if we are running in an isolated subsystem context
            var isolatedApplication = application as IsolatedApplication;
            if (isolatedApplication != null)
            {
                _isolatedSubsystemSettings = isolatedApplication.IsolatedSubsystemSettings;
            }

            var subscriber = communicator.GetSubscriber<SubsystemRouteMessage>();
            subscriber.GetObservable().Subscribe(InternalRouteAdded);

            _subsystemRoutePublisher = communicator.GetPublisher<SubsystemRouteMessage>();


            _routeManager.RouteUpdateAvailable += (sender, args) =>
                {
                    var message = new SubsystemRouteMessage
                        {
                            Origin = args.Route.Origin,
                            Target = args.Route.Target,
                            RouteByDefault = this._routeManager.UseDefaultRouting,
                            MessageTypeIdentifier = args.Route.MessageTypeIdentifier
                        };

                    if (_isolatedSubsystemSettings != null)
                    {
                        // We are in the context of isolated process. Set our process ID and host process ID
                        message.HostProcessId = _isolatedSubsystemSettings.HostProcessId;
                        message.IsolatedProcessId = Process.GetCurrentProcess().Id;
                    }
                    else
                    {
                        // We are running on the host
                        message.HostProcessId = Process.GetCurrentProcess().Id;
                    }

                    _subsystemRoutePublisher.Publish(message);
                };
        }

        //private void communicator_NewSubscription(object sender, NewSubscriptionEventArgs e)
        //{
        //    this._defaultRoutedTypes.Add(e.MessageType);
        //}

        private void InternalRouteAdded(SubsystemRouteMessage routeMessage)
        {
            // Add the route only if it's coming from our host, or one of our isolated processes
            bool shouldAddRoute;

            if (_isolatedSubsystemSettings != null)
            {
                // We are in context of isolated subsystem
                shouldAddRoute = (_isolatedSubsystemSettings.HostProcessId == routeMessage.HostProcessId);
            }
            else
            {
                shouldAddRoute =
                    ProcessIsolationExtensions.RunningIsolatedProcesses.Contains(routeMessage.IsolatedProcessId);
            }

            if (shouldAddRoute)
            {
                lock (_routeByTypeLock)
                {
                    Type type;
                    if (!TryFindType(routeMessage.MessageTypeIdentifier, out type))
                        return;

                    var route = new Route
                        {
                            Origin = routeMessage.Origin,
                            Target = routeMessage.Target,
                            MessageTypeIdentifier = routeMessage.MessageTypeIdentifier,
                            CreatedAt = DateTime.Now
                        };

                    // UseDefaultRouting = routeMessage.RouteByDefault; 
                    _defaultRoutedTypes[type] = route;
                }
            }
        }

        #region Type helper

        public static bool TryFindType(string typeName, out Type t)
        {
            lock (TypeCache)
            {
                if (!TypeCache.TryGetValue(typeName, out t))
                {
                    foreach (Assembly a in AppDomain.CurrentDomain.GetAssemblies())
                    {
                        t = a.GetType(typeName);
                        if (t != null)
                            break;
                    }
                    TypeCache[typeName] = t; // perhaps null
                }
            }
            return t != null;
        }

        #endregion

        /// <summary>
        /// Registers the endpoint selection step that prefers local in-app endpoints when available.
        /// </summary>
        /// <param name="priority">The priority in the resolver logic. Defaulted to 2100, 100 above the RouteManager.</param>
        public void Register(int priority = 2200)
        {
            this._endpointSelector.AddEndpointSelectionStep(priority, this.EndpointSelectionStep);
        }

        private EndpointSelection EndpointSelectionStep(EndpointSelectorChain chain)
        {
            if (IsEnabled
                && chain.Attempt == 0
                && _defaultRoutedTypes.ContainsKey(chain.Message.GetType()))
            // we attempt default routing only once
            {
                return new EndpointSelection
                {
                    Endpoint = Endpoint.LocalEndpoint,
                    Persist = false
                };
            }

            return chain.Next();
        }

    }
}
