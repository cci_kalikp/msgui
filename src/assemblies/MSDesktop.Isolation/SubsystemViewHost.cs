﻿using System;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;
using System.Xml.Linq;
using MSDesktop.Isolation.Enums;
using MSDesktop.Isolation.Utils;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.Win32;

namespace MSDesktop.Isolation
{
    internal abstract class SubsystemViewHostBase : HwndHost, IDehydratableElement
    {
        private readonly IntPtr _remoteHwnd;
        private readonly AddInProxy.AddInProxy _addinProxy;

        protected SubsystemViewHostBase(IntPtr hwnd, AddInProxy.AddInProxy addinProxy)
        {
            _addinProxy = addinProxy;
            _remoteHwnd = hwnd;
        }

        protected IntPtr RemoteHwnd
        {
            get { return _remoteHwnd; }
        }

        public AddInProxy.AddInProxy AddinProxy
        {
            get { return _addinProxy; }
        }

        public XDocument Dehydrate()
        {
            return _addinProxy.Save();
        }

        protected override HandleRef BuildWindowCore(HandleRef hwndParent)
        {
            var parentHandle = hwndParent.Handle;
            IntPtr childHwnd;

            if (Win32.IsWindow(_remoteHwnd))
            {
                childHwnd = _remoteHwnd;
            }
            else
            {
                // The isolated process crashed, but rendering of parent control will still require valid HWND, once we are here.
                // Create a sentinel Control, and get its HWND
                var control = new Control
                    {
                        Width = 0,
                        Height = 0,
                    };

                var hwndSource = new HwndSource(new HwndSourceParameters())
                    {
                        RootVisual = control,
                    };

                // Set the new window style
                childHwnd = hwndSource.Handle;
                var presentStyle = Win32.GetWindowLong(childHwnd, Win32.GWL_STYLE);
                presentStyle |= (int) Win32.WS_CHILD;
                const uint u = (Win32.WS_POPUP | Win32.WS_BORDER | Win32.WS_CAPTION);
                presentStyle = (int) (presentStyle & (~u));
                Win32.SetWindowLong(childHwnd, Win32.GWL_STYLE, presentStyle);
            }

            // Check to see if _remoteHwd is still valid. This can happen if the child process is terminated
            Win32.SetParent(childHwnd, parentHandle);
            return new HandleRef(this, childHwnd);
        }

        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
        {
            base.OnRenderSizeChanged(sizeInfo);
            InvalidateVisual();
        }

        protected override void OnWindowPositionChanged(Rect rcBoundingBox)
        {
            // See http://support.microsoft.com/kb/969728 for explanation
            Win32.SetWindowPos(Handle, IntPtr.Zero,
                               (int) rcBoundingBox.X,
                               (int) rcBoundingBox.Y,
                               (int) rcBoundingBox.Width,
                               (int) rcBoundingBox.Height,
                               Win32.SWP_ASYNCWINDOWPOS
                               | Win32.SWP_NOZORDER
                               | Win32.SWP_NOACTIVATE);
        }

        protected override bool OnMnemonicCore(ref MSG msg, System.Windows.Input.ModifierKeys modifiers)
        {
            //TODO: Optionally, forward mnemonics "down" to the add-in's HwndSource (via IKIS).
            // This would look much like TranslateAccelerator but going in the opposite direction.
            return false;
        }

        protected override void DestroyWindowCore(HandleRef hwnd)
        {
            _addinProxy.QueueDestroy();
        }
    }

    internal sealed class SubsystemViewHost : SubsystemViewHostBase
    {
        public SubsystemViewHost(IntPtr hwnd, AddInProxy.AddInProxy addinProxy) : base(hwnd, addinProxy)
        {
            SizeChanged += SubsystemViewHost_SizeChanged;
        }

        protected override void OnVisualParentChanged(DependencyObject oldParent)
        {
            base.OnVisualParentChanged(oldParent);
        }

        protected override void OnRender(System.Windows.Media.DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);
        }

        protected override Size MeasureOverride(Size constraint)
        {
            /*
            // Measure the view inside us
            var result = Win32.SendMessage(
                RemoteHwnd,
                IsolationConstants.WmUserMessage,
                Win32.MAKELPARAM((int)WindowMessageOperation.WindowMeasure, 0),
                Win32.MAKELPARAM((int)constraint.Width, (int)constraint.Height));

            var size = result != 0
                           ? new Size(Win32.LOWORD(result), Win32.HIWORD(result))
                           : base.MeasureOverride(constraint);*/
            var size = base.MeasureOverride(constraint);
            return size;
        }

        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
        {
            base.OnRenderSizeChanged(sizeInfo);
        }

        protected override Size ArrangeOverride(Size finalSize)
        {
            var size = base.ArrangeOverride(finalSize);
            return size;
        }

        private static void SubsystemViewHost_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            var size = e.NewSize;
        }
    }
}
