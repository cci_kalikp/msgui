﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MSDesktop.Isolation;
using MSDesktop.Isolation.Interfaces;
using MSDesktop.Isolation.IsolatedSubsystemManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.ProcessIsolation
{
    public static class ProcessIsolationExtensions
    {
        internal const string IsolatedProcessWindowFactoryName = "IsolatedWindowFactory";
        private static readonly IsolatedSubsystemManager IsolatedSubsystemManager = new IsolatedSubsystemManager();
        private static bool _handlersHookedUp;
        private static volatile bool _isFrameworkInitialized;

        public static void SetUpProcessIsolationCloseHandlers(this IWindowViewContainer iwcv)
        {
            EventHandler<WindowEventArgs> handler = null;
            handler = delegate
                {
                    iwcv.Closed -= handler;
                    iwcv.ClosedQuietly -= handler;

                    var control = iwcv.Content as XProcControl;
                    if (control != null)
                        control.OnIWVCClosed(iwcv, EventArgs.Empty);
                };
            iwcv.Closed += handler;
            iwcv.ClosedQuietly += handler;
        }

        public static void SetUpProcessIsolatedWrapperFor<T>(this IWindowViewContainer iwcv,
                                                             IChromeManager chromeManager, XDocument state)
            where T : UserControl
        {
            var control = new XProcControl(typeof (T).AssemblyQualifiedName, state, chromeManager, string.Empty);
            HookUpEvents(iwcv);
            iwcv.Content = control;
        }

        public static void SetUpProcessIsolatedWrapperFor<T>(this IWindowViewContainer iwcv, XDocument state,
                                                             string additionalCommandLine, bool is32bit = false,
                                                             bool needsAR = false,
                                                             string hostdirectory = null, string extraAR = null)
            where T : UserControl
        {
            var control = new XProcControl(typeof (T).AssemblyQualifiedName, state, null, is32bit, string.Empty,
                                           needsAR, hostdirectory, extraAR, additionalCommandLine);
            HookUpEvents(iwcv);
            iwcv.Content = control;
        }

        public static void SetUpProcessIsolatedWrapperFor<T>(this IWindowViewContainer iwcv, XDocument state,
                                                             bool is32bit = false, bool needsAR = false,
                                                             string hostdirectory = null, string extraAR = null)
        {
            var control = new XProcControl(typeof (T).AssemblyQualifiedName, state, null, is32bit, string.Empty, needsAR,
                                           hostdirectory,
                                           extraAR);
            HookUpEvents(iwcv);
            iwcv.Content = control;
        }

        public static void RegisterProcessIsolatedWindowFactory<T>(this IChromeRegistry chromeRegistry, string ID,
                                                                   bool is32bit = false,
                                                                   bool needsAR = false, string hostdirectory = null,
                                                                   string extraAR = null)
        {
            XProcControl control = null;
            chromeRegistry.RegisterWindowFactory(ID, (iwcv, state) =>
                {
                    control = new XProcControl(typeof (T).AssemblyQualifiedName, state, null, is32bit, string.Empty,
                                               needsAR,
                                               hostdirectory,
                                               extraAR);
                    HookUpEvents(iwcv);
                    iwcv.Content = control;
                    control.Load(state);
                    return true;
                },
                vc => control.Save());
        }

        private static void HookUpEvents(IWindowViewContainer iwcv)
        {
            EventHandler<WindowEventArgs> handler = null;
            handler = (s, e) =>
                {
                    iwcv.Closed -= handler;
                    iwcv.ClosedQuietly -= handler;

                    var contentControl = iwcv.Content as XProcControl;
                    if (contentControl != null)
                    {
                        contentControl.OnIWVCClosed(iwcv, EventArgs.Empty);
                    }
                };

            iwcv.Closed += handler;
            iwcv.ClosedQuietly += handler;
        }

         public static void SetupProcessIsolatedWrapperUsingLauncherMessage(this Framework framework,
                                                                            PackageDefinition packageDefinition,
                                                                            string isolatedType,
                                                                            string profileName = null,
                                                                            string forceLayout = null,
                                                                            bool respawnEnabled = false)
         {
             if (packageDefinition == null)
             {
                 throw new ArgumentNullException("packageDefinition");
             }
             framework.SetupProcessIsolatedWrapperUsingLauncherMessageInternal(null, packageDefinition, isolatedType, profileName, forceLayout, respawnEnabled);
         }

        public static void SetupProcessIsolatedWrapperUsingLauncherMessage(this Framework framework,
                                                                           string isolatedApplicationKey, 
                                                                           string isolatedType,
                                                                           string profileName=null,
                                                                           string forceLayout=null,
                                                                           bool respawnEnabled = false)
        {
            if (string.IsNullOrEmpty(isolatedApplicationKey))
            {
                throw new ArgumentNullException("isolatedApplicationKey");
            }

            framework.SetupProcessIsolatedWrapperUsingLauncherMessageInternal(isolatedApplicationKey, null, isolatedType, profileName, forceLayout, respawnEnabled);

        }


        private static void SetupProcessIsolatedWrapperUsingLauncherMessageInternal(this Framework framework,
                                                                           string isolatedApplicationKey,
                                                                           PackageDefinition packageDefinition,
                                                                           string isolatedType,
                                                                           string profileName,
                                                                           string forceLayout,
                                                                           bool respawnEnabled)
        {
            var container = framework.Container;
            /*
            var layout = string.Empty;
            if (!string.IsNullOrEmpty(forceLayout))
            {
                layout = forceLayout;
            }
            else if (string.IsNullOrEmpty(profileName))
            {
                var profileService = container.Resolve<PersistenceProfileService>();
                profileName = profileService.CurrentProfile;
                var storage = container.Resolve<IPersistenceStorage>();
                var state = storage.LoadState(profileName);
                if (state != null)
                {
                    layout = state.ToString();
                }
            }*/

            // Get the chrome manager
            var controlTypeName = isolatedType;

            var subsystemSite = new IsolatedSubsystemSite(
                container,
                IsolatedSubsystemManager,
                controlTypeName,
                framework.Bootstrapper.Application.Name,
                framework.Bootstrapper.IsFrameworkInitialized,
                profileName/*,
                layout*/);

            var endpointManager = isolatedApplicationKey != null ? 
                new XProcEndpointManager(subsystemSite, isolatedApplicationKey) : 
                new XProcEndpointManager(subsystemSite, packageDefinition);

            IsolatedSubsystemManager.RegisterLauncherMessageBasedSubsystem(container, subsystemSite, endpointManager, false, null);

            if (respawnEnabled)
            {
                /*
                var respawnManager = new SubsystemRespawnManager(container, endpointManager, subsystemSite, 
                                                                 newLayout => framework.SetupProcessIsolatedWrapperUsingLauncherMessage(
                                                                     isolatedApplicationKey,
                                                                     isolatedType,
                                                                     profileName,
                                                                     newLayout,
                                                                     true));
                respawnManager.Initialize();*/
            }

            endpointManager.StartProcess();
        }

        /// <summary>
        /// Sets up and starts a module in its own isolated process.
        /// </summary>
        /// <typeparam name="T">The type of isolated module, if known at compile time. The type must implement either IModule or ISubsystemModuleCollection interface</typeparam>
        /// <param name="framework">Framework instance.</param>
        /// <param name="isolatedPlatform">Target platform for isolated process.</param>
        /// <param name="isolatedSubsystemLocation">Location of isolated subsystem.</param>
        /// <param name="isolatingExecutableLocation">Location of the isolated subsystem executable that will execute the subsystem.</param>
        /// <param name="isolatingExecutableName">Name of the isolating executable.</param>
        /// <param name="additionalCommandLine">Additional command line to pass into subsystem.</param>
        /// <param name="needsAssemblyResolution">A flag indicating whether or not additional assembly resolution is required.</param>
        /// <param name="extraAssemblyResolution">Location of an additional MSDE.CONFIG file for assembly resolution.</param>
        /// <param name="isolatedSubsystemConfigFile">Location of a config file to be provided for isolated subsystem.</param>
        /// <param name="subsystemRibbonPath">Specified path on the ribbon where buttons from isolated subsystem go.</param>
        /// <param name="profileName">Specifies persistence profile name to be used in the application.</param>
        /// <param name="usePrism">Specifies whether to use Prism as singleton framework.</param>
        public static void SetUpProcessIsolatedWrapper<T>(
            this Framework framework,
            IsolatedProcessPlatform isolatedPlatform = IsolatedProcessPlatform.CurrentCpu,
            string isolatedSubsystemLocation = null,
            string isolatingExecutableLocation = null,
            string isolatingExecutableName = null,
            string additionalCommandLine = null,
            bool needsAssemblyResolution = false,
            string extraAssemblyResolution = null,
            string isolatedSubsystemConfigFile = null,
            string subsystemRibbonPath = null,
            string profileName = null,
            bool usePrism = false,
            string extraInformation=null)
        {
            // Validate the type
            var typeInterfaces = typeof (T).GetInterfaces();
            if (typeof (T).GetInterface("IModule", true) == null &&
                typeof (T).GetInterface("IMSDesktopModule", true) == null &&
                !typeInterfaces.Contains(typeof (ISubsystemModuleCollection)))
            {
                throw new ArgumentException("Type argument T must be either of IModule or ISubsystemModuleCollection",
                                            "T");
            }

            framework.SetUpProcessIsolatedWrapperInternal(
                typeof (T).AssemblyQualifiedName,
                isolatedPlatform == IsolatedProcessPlatform.X86,
                isolatedSubsystemLocation,
                isolatingExecutableLocation,
                isolatingExecutableName,
                additionalCommandLine,
                false,
                needsAssemblyResolution,
                extraAssemblyResolution,
                isolatedSubsystemConfigFile,
                subsystemRibbonPath,
                profileName,
                usePrism,
                extraInformation,
                null,
                false,
                new ProcessIsolationRespawnHandler());
        }

        /// <summary>
        /// Sets up and starts a module in its own isolated process.
        /// </summary>
        /// <param name="framework">Framework instance.</param>
        /// <param name="isolatedType">Full assembly-qualified name of the isolated type.</param>
        /// <param name="isolatedPlatform">Target platform for isolated process.</param>
        /// <param name="isolatedSubsystemLocation">Location of isolated subsystem.</param>
        /// <param name="isolatingExecutableLocation">Location of the isolated subsystem executable that will execute the subsystem.</param>
        /// <param name="isolatingExecutableName">Name of the isolating executable</param>
        /// <param name="additionalCommandLine">Additional command line to pass into subsystem.</param>
        /// <param name="needsAssemblyResolution">A flag indicating whether or not additional assembly resolution is required.</param>
        /// <param name="extraAssemblyResolution">Location of an additional MSDE.CONFIG file for assembly resolution.</param>
        /// <param name="isolatedSubsystemConfigFile">Location of a config file to be provided for isolated subsystem.</param>
        /// <param name="subsystemRibbonPath">Specified path on the ribbon where buttons from isolated subsystem go.</param>
        /// <param name="profileName">Specifies persistence profile name to be used in the application.</param>
        /// <param name="usePrism">Specifies whether to use Prism as singleton framework.</param>
        /// <param name="extraInformation"></param>
        public static void SetUpProcessIsolatedWrapper(
            this Framework framework,
            string isolatedType,
            IsolatedProcessPlatform isolatedPlatform = IsolatedProcessPlatform.CurrentCpu,
            string isolatedSubsystemLocation = null,
            string isolatingExecutableLocation = null,
            string isolatingExecutableName=null,
            string additionalCommandLine = null,
            bool needsAssemblyResolution = false,
            string extraAssemblyResolution = null,
            string isolatedSubsystemConfigFile = null,
            string subsystemRibbonPath = null,
            string profileName = null,
            bool usePrism = false,
            string extraInformation=null)
        {
            framework.SetUpProcessIsolatedWrapperInternal(
                isolatedType,
                isolatedPlatform == IsolatedProcessPlatform.X86,
                isolatedSubsystemLocation,
                isolatingExecutableLocation,
                isolatingExecutableName,
                additionalCommandLine,
                false,
                needsAssemblyResolution,
                extraAssemblyResolution,
                isolatedSubsystemConfigFile,
                subsystemRibbonPath,
                profileName,
                usePrism,
                extraInformation,
                null,
                false,
                new ProcessIsolationRespawnHandler());
        }

        public static void SetUpProcessIsolatedWrapper<T>(this Framework framework, ProcessIsolationOptions options)
        {
            // Validate the type
            var typeInterfaces = typeof (T).GetInterfaces();
            if (typeof (T).GetInterface("IModule", true) == null &&
                typeof (T).GetInterface("IMSDesktopModule", true) == null &&
                !typeInterfaces.Contains(typeof (ISubsystemModuleCollection)))
            {
                throw new ArgumentException("Type argument T must be either of IModule or ISubsystemModuleCollection",
                                            "T");
            }
            framework.SetUpProcessIsolatedWrapperInternal(
                typeof(T).AssemblyQualifiedName,
                options.IsolatedPlatform == IsolatedProcessPlatform.X86,
                options.IsolatedSubsystemLocation,
                options.IsolatingExecutableLocation,
                options.IsolatingExecutableName,
                options.AdditionalCommandLine,
                false,
                options.NeedsAssemblyResolution,
                options.ExtraAssemblyResolution,
                options.IsolatedSubsystemConfigFile,
                options.SubsystemRibbonPath,
                options.ProfileName,
                options.UsePrism,
                options.ExtraInformation,
                null,
                options.RespawnEnabled,
                options.RespawnHandler);
        }

        internal static IEnumerable<int> RunningIsolatedProcesses
        {
            get
            {
                return IsolatedSubsystemManager.GetIsolatedSubsystems().Select(s => s.ProcessId);
            }
        }

        internal static global::MSDesktop.Isolation.Enums.ModuleStatus ToProxyModuleStatus(this ModuleStatus status)
        {
            return (global::MSDesktop.Isolation.Enums.ModuleStatus)(int)status;
        }

        private static void OnApplicationShutdown(object sender, EventArgs args)
        {
            IsolatedSubsystemManager.ShutdownIsolatedSubsystems();
        }

        internal static bool IsMsdesktopInitialized()
        {
            return _isFrameworkInitialized;
        }

        private static void SetUpProcessIsolatedWrapperInternal(
            this Framework framework,
            string typeName,
            bool force32Bit,
            string isolatedSubsystemLocation,
            string isolatingExecutableLocation,
            string isolatingExecutable,
            string additionalCommandLine,
            bool requireKraken,
            bool needsAssemblyResolution,
            string extraAssemblyResolution,
            string isolatedSubsystemConfigFile,
            string subsystemRibbonPath,
            string profileName,
            bool usePrism,
            string extraInformation,
            string forceLayout,
            bool respawnEnabled,
            ProcessIsolationRespawnHandler respawnHandler)
        {
            isolatedSubsystemLocation = PathUtilities.ReplaceEnvironmentVariables(isolatedSubsystemLocation);
            isolatingExecutableLocation = PathUtilities.ReplaceEnvironmentVariables(isolatingExecutableLocation);

            var container = framework.Container;

            var bootstrapper = framework.Bootstrapper;
            if (!_handlersHookedUp)
            {
                if (bootstrapper.IsFrameworkInitialized)
                {
                    _isFrameworkInitialized = bootstrapper.IsFrameworkInitialized;
                }
                else
                {
                    bootstrapper.InitializationFinished += Bootstrapper_InitializationFinished;
                }

                bootstrapper.Application.ApplicationClosed += OnApplicationShutdown;
                _handlersHookedUp = true;
            }

            // Get the chrome manager
            var controlTypeName = typeName;
            var subsystemSite = new IsolatedSubsystemSite(
                container,
                IsolatedSubsystemManager,
                controlTypeName,
                isolatedSubsystemLocation,
                bootstrapper.Application.Name,
                bootstrapper.IsFrameworkInitialized,
                requireKraken,
                null,
                additionalCommandLine,
                needsAssemblyResolution,
                extraAssemblyResolution,
                isolatedSubsystemConfigFile,
                subsystemRibbonPath,
                profileName,
                // forceLayout,
                usePrism,
                extraInformation);

            if (string.IsNullOrEmpty(isolatingExecutable))
            {
                isolatingExecutable = XProcEndpointManager.GetDefaultIsolatedExecutable(force32Bit);
            }

            IsolatedSubsystemManager.Container = container;
            IsolatedSubsystemManager.StartIsolatedProcess(subsystemSite, isolatingExecutableLocation, additionalCommandLine, isolatingExecutable, respawnEnabled, bootstrapper.IsFrameworkInitialized,
                respawnHandler,
                newLayout => framework.SetUpProcessIsolatedWrapperInternal(
                    typeName,
                    force32Bit,
                    isolatedSubsystemLocation,
                    isolatingExecutableLocation,
                    isolatingExecutable,
                    additionalCommandLine,
                    requireKraken,
                    needsAssemblyResolution,
                    extraAssemblyResolution,
                    isolatedSubsystemConfigFile,
                    subsystemRibbonPath,
                    profileName,
                    usePrism,
                    extraInformation,
                    newLayout,
                    true,
                    new ProcessIsolationRespawnHandler()));            
        }

        private static void Bootstrapper_InitializationFinished(object sender, EventArgs e)
        {
            _isFrameworkInitialized = true;
            IsolatedSubsystemManager.SignalHostInitialized();
        }

        public static IXProcAddInHost SetUp32BitProcessIsolatedWrapperFor<T>(this IWindowViewContainer iwcv,
                                                                             XDocument state,
                                                                             Type payloadType = null)
            where T : UserControl
        {
            var control = new XProcControl(typeof (T).AssemblyQualifiedName, state, null, true, string.Empty,
                                           payloadType: payloadType);

            EventHandler<WindowEventArgs> handler = null;
            handler = delegate
                {
                    iwcv.Closed -= handler;
                    iwcv.ClosedQuietly -= handler;
                    ((XProcControl) iwcv.Content).OnIWVCClosed(iwcv, EventArgs.Empty);
                };

            iwcv.Closed += handler;
            iwcv.ClosedQuietly += handler;
            iwcv.Content = control;

            return control.AddInHost;
        }
    }
    
    public sealed class ProcessIsolationOptions
    {
        public ProcessIsolationOptions()
        {
            RespawnHandler = new ProcessIsolationRespawnHandler();
        }

        public string IsolatedType { get; set; }
        public IsolatedProcessPlatform IsolatedPlatform { get; set; }
        public string IsolatedSubsystemLocation { get; set; }
        public string IsolatingExecutableLocation { get; set; }
        public string IsolatingExecutableName { get; set; }
        public string AdditionalCommandLine { get; set; }
        public bool NeedsAssemblyResolution { get; set; }
        public string ExtraAssemblyResolution { get; set; }
        public string IsolatedSubsystemConfigFile { get; set; }
        public string SubsystemRibbonPath { get; set; }
        public string ProfileName { get; set; }
        public bool UsePrism { get; set; }
        public string ExtraInformation { get; set; }
        public bool RespawnEnabled { get; set; }
        public ProcessIsolationRespawnHandler RespawnHandler { get; set; }
    }

    public sealed class ProcessIsolationRespawnHandler
    {
        public event EventHandler<ProcessIsolationRespawnEventArgs> ProcessCrashed;

        internal void OnProcessCrashed(ProcessIsolationRespawnEventArgs e)
        {
            var handler = ProcessCrashed;
            if (handler != null) handler(this, e);
        }
    }

    public class ProcessIsolationRespawnEventArgs : EventArgs
    {
        public bool Respawn { get; set; }
    }

    public class PackageDefinition
    {
        public PackageDefinition(string packagePath_, string executable_ = null, string arguments_ = null)
        {
            PackagePath = packagePath_;
            Executable = executable_;
            Arguments = arguments_;
        }

        public PackageDefinition(string appName_, string version_ = null, string package_ = null,
                                string executable_ = null, string arguments_ = null)
        {
            AppName = appName_;
            Version = version_;
            Package = package_;
            Executable = executable_;
            Arguments = arguments_;
        }


        public string AppName { get; private set; }
        public string Version { get; private set; }
        public string Package { get; private set; }
        public string PackagePath { get; private set; }
        public string Executable { get; private set; }
        public string Arguments { get; private set; }

    }

}
