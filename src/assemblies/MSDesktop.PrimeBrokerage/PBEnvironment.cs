﻿namespace MSDesktop.PrimeBrokerage
{
    public enum PBEnvironment
    {
        Prod = 0,
        Uat = 1,
        Qa = 2,
        Dev = 3,
        Local = 4
    }
}