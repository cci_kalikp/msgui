﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels; 
using System.Xml; 
using MorganStanley.Desktop.Entitlement;
using MorganStanley.MSDotNet.My;
using Binding = System.ServiceModel.Channels.Binding; 
using SoapBinding = MorganStanley.MSDotNet.Channels.Soap.SoapBinding;

namespace MSDesktop.PrimeBrokerage
{
    public class PBEntitlementService : OnlineEntitlementService
    {
        public const string ResourceName = "ResourceName";
        public const string ResourceDescription = "ResourceDescription";

        private static readonly IMSLogger Logger = MSLoggerFactory.CreateLogger<PBEntitlementService>();
        private const string PROD_EXTERNAL_WSDL_SERVICE_CONFIG = "//ms/dist/clienttech/PROJ/pbeservice/prod/config/external_wsdl_service_config.xml";
        private const string UAT_EXTERNAL_WSDL_SERVICE_CONFIG = "//ms/dist/clienttech/PROJ/pbeservice/uat/config/external_wsdl_service_config.xml";
        private const string PROD_INTERNAL_WSDL_SERVICE_CONFIG = "//ms/dist/clienttech/PROJ/pbeservice/prod/config/internal_wsdl_service_config.xml";
        private const string UAT_INTERNAL_WSDL_SERVICE_CONFIG = "//ms/dist/clienttech/PROJ/pbeservice/uat/config/internal_wsdl_service_config.xml";
        const string requestorTag = "msdotnet/msgui/trunk";

        private readonly string domainName;
        private readonly PBEnvironment environment;
        private readonly string firmUseId;
        private readonly string userTag; 
        private readonly bool isClientLink;
        private bool useKerose;
        private bool tcp;
        private readonly PBEntitlementServicesClient service;
        /// <summary>
        /// Create an instance of PBEntitlementService which read entitlement from PBEntitlement Web Service
        /// </summary>
        /// <param name="environment_"></param>
        /// <param name="domainName_">domain name(application name)</param>
        /// <param name="firmUserId_">The ISG short id or the external contact id</param>
        /// <param name="userTag_">domain-specific user tag, e.g. elliotja@ms.com or external login id</param>
        /// <param name="isClientLink_"></param>
        public PBEntitlementService(PBEnvironment environment_, string domainName_, string firmUserId_, string userTag_, bool isClientLink_ = false)
            : base(userTag_)
        {
            if (string.IsNullOrEmpty(domainName_))
                throw new ArgumentNullException("domainName_");
            if (string.IsNullOrEmpty(userTag_) && string.IsNullOrEmpty(firmUserId_))
                throw new ArgumentOutOfRangeException("either userTag_ or firmUserId_ must be provided");
            environment = environment_;
            domainName = domainName_;
            firmUseId = firmUserId_;
            userTag = userTag_;
            isClientLink = isClientLink_;
            var address = GetAddress(); 
            service = new PBEntitlementServicesClient(CreateBinding(), address);
        }
 

        private EndpointAddress GetAddress()
        {
            XmlDocument doc = new XmlDocument();
            if (isClientLink)
            {
                var externalWSDLConfig = (environment == PBEnvironment.Prod ? PROD_EXTERNAL_WSDL_SERVICE_CONFIG : UAT_EXTERNAL_WSDL_SERVICE_CONFIG);
                Logger.Debug(
                    "Reading the Clientlink WSDL configuration from disted location (pushed to DMZ): " +
                    externalWSDLConfig, "GetAddress");
                doc.Load(externalWSDLConfig);
            }
            else
            {
                // Try to read internally from //ms/dist
                var internalWSDLConfig = (environment == PBEnvironment.Prod ? PROD_INTERNAL_WSDL_SERVICE_CONFIG : UAT_INTERNAL_WSDL_SERVICE_CONFIG);
                Logger.Debug("Reading the WSDL configuration from disted location: " + internalWSDLConfig, "GetAddress"); 
                doc.Load(internalWSDLConfig);
            }

            string address = null;
            var nodeWsdl = doc.SelectSingleNode(string.Format("//PBEService[@environment='{0}' and @access='READ_ONLY']/@wsdl", environment.ToString().ToUpper()));
            if (nodeWsdl != null && !string.IsNullOrEmpty(nodeWsdl.Value))
            {
                XmlDocument document = new XmlDocument();
                using (XmlTextReader reader = new XmlTextReader(nodeWsdl.Value))
                {
                    document.Load(reader);
                    var nsManager = new XmlNamespaceManager(doc.NameTable);
                    nsManager.AddNamespace("wsdl", "http://schemas.xmlsoap.org/wsdl/");
                    nsManager.AddNamespace("ns1", "http://schemas.xmlsoap.org/soap/http");
                    nsManager.AddNamespace("ns2", "http://xml.ms.com/ns/eai/string-ktcp");
                    nsManager.AddNamespace("soap", "http://schemas.xmlsoap.org/wsdl/soap/");
                    nsManager.AddNamespace("tns", "http://pbeservice.clienttech.ms.com/");
                    if (environment == PBEnvironment.Local)
                    {
                        var addressNode = document.SelectSingleNode(
                            "//wsdl:service[@name='EntitlementsServerService']/wsdl:port[@name='EntitlementsServerHTTPPort']/soap:address/@location", nsManager);
                        if (addressNode != null) address = addressNode.Value;
                    }
                    else
                    {
                        var addressNode = document.SelectSingleNode(
    "//wsdl:service[@name='EntitlementsServerService']/wsdl:port[@name='EntitlementsServerTCPPort']/soap:address/@location", nsManager);
                        if (addressNode != null) address = addressNode.Value;

                    }
                }
                
            } 
            if (string.IsNullOrEmpty(address))
            {
                var nodeTcp = doc.SelectSingleNode(string.Format("//PBEService[@environment='{0}' and @access='READ_ONLY']/@tcp", environment.ToString().ToUpper()));
                if (nodeTcp != null)
                {
                    address = nodeTcp.Value;
                }
            }

            if (string.IsNullOrEmpty(address))
            {
                throw new Exception("no way to decide the PBEntitlement Web Server address");

            }

            if (address.StartsWith("ktcp://"))
            {
                useKerose = true;
                tcp = true;
                return new EndpointAddress("ms.tcp" + address.Substring(4));
            }
            if (address.StartsWith("tcp://"))
            {
                tcp = true;
                return new EndpointAddress("ms." + address);
            }
            if (address.StartsWith("http://"))
            {
                return new EndpointAddress(address);
            }
            throw new Exception("Invalid address: '" + address + "' found");
        }

        private Binding CreateBinding()
        { 
            if (tcp)
            {
                var binding = new SoapBinding(MessageVersion.Soap11);
                if (useKerose)
                    binding.Connector = "Kerberos";
                binding.TransferMode = TransferMode.StreamedResponse;
                return binding;
            }
            return new BasicHttpBinding() {TransferMode = TransferMode.StreamedResponse};
        }
         
        protected override ICollection<string> GetPermittedActionsFromServer(IEntitlementResource resource_)
        {
            if (!resource_.GetAttributeNames().Contains(ResourceName))
            {
                throw new EntitlementException("ResouceName not specified for the resource", null);
            }
            string requestId = Guid.NewGuid().ToString();
            string resourceName = resource_[ResourceName];
            EntitlementsResponse[] entitlementsResponse = null;
            try
            {
                entitlementsResponse =
               service.GetUserEntitlements(new UserEntitlementsQueries()
               {
                   RequestId = requestId,
                   RequestorTag = requestorTag,
                   UserEntitlementsQuery =
                       new UserEntitlementsQuery[]
                                {
                                    new UserEntitlementsQuery()
                                        {
                                            ResourceName =  resourceName, 
                                            DomainName = domainName,
                                            UserTag = userTag,
                                            FirmUserId = firmUseId,
                                            ResourceType = resource_.ResourceType, 
                                        }
                                }
               });
            }
            catch (FaultException<entitlementsFaultDetails> fault)
            { 
                throw new EntitlementException(fault.Message, fault);
            }
           
            var actions = new List<string>();
            if (entitlementsResponse != null)
            {
                foreach (var entitlementResponse in entitlementsResponse)
                {
                    foreach (var entitlement in entitlementResponse.Entitlements)
                    {
                        if (!actions.Contains(entitlement.ActionName))
                        {
                            actions.Add(entitlement.ActionName);
                        }
                    }
                }
            }


            return actions;
        }

        protected override ICollection<IEntitlementResource> GetPermittedResourcesFromServer(string actionName_, string resourceType_)
        {
            string requestId = Guid.NewGuid().ToString();
            EntitlementsResponse[] entitlementsResponse = null;
            try
            {
                entitlementsResponse =
                service.GetUserEntitlements(new UserEntitlementsQueries()
                {
                    RequestId = requestId,
                    RequestorTag = requestorTag,
                    UserEntitlementsQuery =
                        new UserEntitlementsQuery[]
                                {
                                    new UserEntitlementsQuery()
                                        {
                                            ActionName = actionName_, 
                                            DomainName = domainName,
                                            UserTag = userTag,
                                            FirmUserId = firmUseId,
                                            ResourceType = resourceType_,
                                            IncludeDescription = true,
                                            IncludeDescriptionSpecified = true,
                                            IncludeResourceAttributes = true,
                                            IncludeResourceAttributesSpecified = true
                                        }
                                }
                });
            }
            catch (FaultException<entitlementsFaultDetails> fault)
            { 
                throw new EntitlementException(fault.Message, fault);
            }
            var resources = new List<IEntitlementResource>();
            if (entitlementsResponse != null)
            {
                foreach (var entitlementResponse in entitlementsResponse)
                {
                    foreach (var entitlement in entitlementResponse.Entitlements)
                    {
                        var resource = new EntitlementResource(entitlement.ResourceType)
                        {
                            ResourceName = entitlement.ResourceName,
                            ResourceDescription = entitlement.ResourceDescription
                        };
                        if (entitlement.ResourceAttributes != null)
                        {
                            foreach (var attribute in entitlement.ResourceAttributes)
                            {
                                if (attribute != null && !string.IsNullOrEmpty(attribute.name))
                                { 
                                    resource[attribute.name] = attribute.Value;
                                }
                            }
                        }
                        resources.Add(resource);
                    }
                }
            }
           
            return resources;
        }
    }
}
