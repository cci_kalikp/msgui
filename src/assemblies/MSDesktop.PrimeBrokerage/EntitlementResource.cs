﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.Desktop.Entitlement;

namespace MSDesktop.PrimeBrokerage
{

    internal class EntitlementResource : IEntitlementResource
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EntitlementResource"/> class.
        /// </summary>
        /// <param name="resourceType_">Type of the resource.</param>
        public EntitlementResource(string resourceType_)
        {
            this.resourceType = resourceType_;
            attributes = new Dictionary<string, string>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EntitlementResource"/> class.
        /// </summary>
        /// <param name="resource_">The resource.</param>
        public EntitlementResource(IEntitlementResource resource_)
            : this(resource_.ResourceType)
        {
            foreach (var attrName in resource_.GetAttributeNames())
                attributes.Add(attrName, resource_[attrName]);
        }

        #region IEntitlementResource Members

        /// <summary>
        /// Gets the type of the resource.
        /// </summary>
        /// <value>The type of the resource.</value>
        public string ResourceType
        {
            get { return resourceType; }
        }

        /// <summary>
        /// Gets the attributes.
        /// </summary>
        /// <returns>attribute names</returns>
        public IEnumerable<string> GetAttributeNames()
        {
            return attributes.Keys;
        }

        /// <summary>
        /// Gets the <see cref="System.String"/> with the specified attribute name.
        /// </summary>
        /// <value>attribute value, null if attribute is not supported</value>
        public string this[string attributeName_]
        {
            get { return attributes[attributeName_]; }
            internal set { attributes[attributeName_] = value; }
        }

        #endregion

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <param name="obj_">The <see cref="T:System.Object"/> to compare with the current <see cref="T:System.Object"/>.</param>
        /// <returns>
        /// true if the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>; otherwise, false.
        /// </returns>
        /// <exception cref="T:System.NullReferenceException">
        /// The <paramref name="obj_"/> parameter is null.
        /// </exception>
        public override bool Equals(object obj_)
        {
            var other = obj_ as EntitlementResource;
            if (other == null)
                return false;
            return ResourceComparer.Instance.Equals(this, other);
        }

        /// <summary>
        /// Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        /// A hash code for the current <see cref="T:System.Object"/>.
        /// </returns>
        public override int GetHashCode()
        {
            return ResourceComparer.Instance.GetHashCode(this);
        }

        public override string ToString()
        {
            var sb = new StringBuilder(256);
            sb.Append("{ resourceType: ")
              .Append(this.ResourceType);
            foreach (var kvp in attributes)
            {
                sb.Append(", ")
                  .Append(kvp.Key)
                  .Append(": ")
                  .Append(kvp.Value);
            }
            sb.Append(" }");
            return sb.ToString();
        }

        public string ResourceName
        {
            get { return attributes[PBEntitlementService.ResourceName]; }
            set
            {
                attributes[PBEntitlementService.ResourceName] = value; 
            }
        }

        public string ResourceDescription
        {
            get { return attributes[PBEntitlementService.ResourceDescription]; }
            set 
            {
                attributes[PBEntitlementService.ResourceDescription] = value; 
            }
        }
        private readonly string resourceType;
        private readonly IDictionary<string, string> attributes;
    }
}
