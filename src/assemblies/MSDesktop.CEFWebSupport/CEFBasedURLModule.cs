﻿using System;
using System.IO;
using System.Linq;
using System.Timers;
using System.Web;
using CefSharp.Wpf;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MSDesktop.WebSupport.CEF
{
    public class CEFBasedURLModule : CEFBasedHTMLModule
    {
        public CEFBasedURLModule(IUnityContainer unityContainer_)
            : base(unityContainer_)
        {
        }

        protected override Uri Url
        {
            get
            {
                var encodedUri = HttpUtility.UrlEncode(base.Url.AbsoluteUri);
                return new Uri(CEFWebControlAdapter.URLBasedModuleFakeAddress + "?" + encodedUri);
            }
        }
        internal protected virtual string GetURLProxyHtml()
        {
            return File.ReadAllText("URLProxy.html");
        }
    }

    public class CEFBasedActivatedURLModule : CEFBasedHTMLModule
    {
        private HTMLModuleInfo moduleInfo;

        public CEFBasedActivatedURLModule(IUnityContainer unityContainer_) : base(unityContainer_)
        {
            this.ViewCreated += new EventHandler<WebControlEventArgs>(ActivatedURLModule_ViewCreated);
            this.ViewClosed += new EventHandler<WebControlEventArgs>(ActivatedURLModule_ViewClosed);
        }

        void ActivatedURLModule_ViewCreated(object sender, WebControlEventArgs e)
        {
            if (e.ViewContainer.IsVisible)
            {
                LoadIfNeeded(e.ViewContainer);
            }
            e.ViewContainer.IsVisibleChanged += ViewContainer_IsVisibleChanged;
        }

        void ActivatedURLModule_ViewClosed(object sender, WebControlEventArgs e)
        {
            e.ViewContainer.IsVisibleChanged -= ViewContainer_IsVisibleChanged;
        }

        void ViewContainer_IsVisibleChanged(object sender, MorganStanley.MSDotNet.MSGui.Core.VisibilityEventArgs e)
        {
            if (e.Visible)
            {
                IWindowViewContainer view = sender as IWindowViewContainer;
                if (view != null)
                {
                    LoadIfNeeded(view);
                }
            }
        }

        private void LoadIfNeeded(IWindowViewContainer viewContainer_)
        {
            Timer timer = new Timer(500) { AutoReset = false };
            timer.Elapsed += (sender_, args_) =>
            {
                if (viewContainer_.IsVisible)
                {
                    IWebControl webControl = this.FindViewControl(viewContainer_.ID);
                    WebView control = webControl.Control as WebView;
                    if (control != null)
                    {
                        control.Dispatcher.Invoke((Action)(() => control.ExecuteScript("doIt();")));
                    }
                }
            };
            timer.Enabled = true;

        } 
        
        protected override Uri Url
        {
            get
            {
                var encodedUri = HttpUtility.UrlEncode(base.Url.AbsoluteUri);
                return new Uri(CEFWebControlAdapter.URLBasedActivatedModuleFakeAddress + "?" + encodedUri);
            }
        }

        internal protected virtual string GetURLProxyHtml()
        {
            return File.ReadAllText("URLProxy.html");
        }
    }

   
}
