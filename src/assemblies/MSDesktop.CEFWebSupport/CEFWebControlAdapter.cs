﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Web;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Threading;
using CefSharp;
using CefSharp.Wpf;
using MSDesktop.WebSupport.Properties;
using Microsoft.Win32;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.My;
using Newtonsoft.Json;

namespace MSDesktop.WebSupport.CEF
{
    public static class CEFSettings
    {
        internal static readonly HashSet<string> Plugins = new HashSet<string>();
        internal static bool DeveloperToolsEnabled;

        public static void EnableDeveloperToolsForCEF(this IFramework framework)
        {
            DeveloperToolsEnabled = true;
        }

        public static void AddCefPlugin(this IFramework framework, string path)
        {
            Plugins.Add(path);
        }

        public static event EventHandler<EventArgs<string>> DownloadCompleted;

        internal static void CallDownloadCompleted(IWebControl control, string path)
        {
            if (DownloadCompleted != null)
                DownloadCompleted(control, new EventArgs<string>(path));
        }
    }

    public class CEFWebControlAdapter : IWebControl, IRequestHandler, IDownloadHandler
    {
        private static readonly IMSLogger Logger = MSLoggerFactory.CreateLogger<CEFWebControlAdapter>();

        public static string URLBasedModuleFakeAddress = "http://urlhosting.ms.com/urlhost.html";
        public static string URLBasedActivatedModuleFakeAddress = "http://urlhosting.ms.com/urlactivatedhost.html";
        private static bool m_webCoreInitialized;
        private bool initCompleted;
        private readonly WebView webControl;
        private string path;
        private Stream stream;
        private CEFBasedHTMLModule module;

        public CEFWebControlAdapter(IApplication application_, CEFBasedHTMLModule module_)
        {
            this.module = module_;
            InitializeWebCore(application_);
            webControl = new WebView("", new BrowserSettings
                {
                    XssAuditorEnabled = false,
                    PluginsDisabled = false,
                    UniversalAccessFromFileUrlsAllowed = true,
                    WebSecurityDisabled = true
                }) {RequestHandler = this};
            webControl.RegisterJsObject("NativeCallDispatcher", this);
            webControl.PropertyChanged += _WebControl_PropertyChanged;
            webControl.ConsoleMessage += _WebControl_ConsoleMessage;
        }

        private void _WebControl_ConsoleMessage(object sender, ConsoleMessageEventArgs e)
        {
            if (e.Message.StartsWith("DownloadInitiated:"))
            {
                try
                {

                    var json = e.Message.Substring("DownloadInitiated:".Length);
                    var args = JsonConvert.DeserializeObject<object[]>(json);
                    if (args.Length != 2)
                        return;
                    SaveFileDialog dlg = new SaveFileDialog
                        {
                            FileName = args[0].ToString(),
                            DefaultExt = Path.GetExtension(args[0].ToString())
                        };
                    var result = dlg.ShowDialog();
                    if (result == true)
                    {
                        stream = File.Create(dlg.FileName);
                        stream.Write(Encoding.UTF8.GetBytes(args[1].ToString()), 0, args[1].ToString().Length);
                        stream.Close();
                        stream.Dispose();
                        CEFSettings.CallDownloadCompleted(this, path);
                    }
                }
                catch (Exception exc)
                {
                    Logger.Error("Exception happened on file save", exc);
                }

            }
            Logger.DebugWithFormat("Line {0} Message {1}, Source {2}", e.Line, e.Message, e.Source);
        }

        private void _WebControl_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "IsBrowserInitialized":
                    if (webControl.IsBrowserInitialized)
                        InitCompleted(null, null);
                    break;
            }
        }

        public bool ReceivedData(byte[] data)
        {
            stream.Write(data, 0, data.GetLength(0));
            return true;
        }

        public void Complete()
        {
            stream.Close();
            stream.Dispose();
            stream = null;
            CEFSettings.CallDownloadCompleted(this, path);
        }

        public bool OnBeforeBrowse(IWebBrowser browser, IRequest request, NavigationType naigationvType, bool isRedirect)
        {
            return false;
        }

        public bool OnBeforeResourceLoad(IWebBrowser browser, IRequestResponse requestResponse)
        {
            if (requestResponse.Request.Url.StartsWith(URLBasedModuleFakeAddress))
            { 
                string decodedUrl = HttpUtility.UrlDecode(new Uri(requestResponse.Request.Url).Query.Substring(1));
                HTMLModuleInfo info = WebSupportExtensions.Registry.ContainsKey(module) ? WebSupportExtensions.Registry[module] :
                    WebSupportExtensions.Registry.Values.FirstOrDefault(m_ => m_.Url.AbsoluteUri == decodedUrl);
                if (info == null)
                {
                    return false;
                }
                CEFBasedURLModule urlModule = module as CEFBasedURLModule;
                string proxyHtml = urlModule == null ? File.ReadAllText("URLProxy.html") : urlModule.GetURLProxyHtml();
                proxyHtml = proxyHtml.Replace("$DESTINATION_URL", String.Format("\"{0}\"", decodedUrl))
                                     .Replace("$LOCATION", String.Format("\"{0}\"", info.Location))
                                     .Replace("$TITLE", String.Format("\"{0}\"", info.Title))
                                     .Replace("$IMAGEPATH", String.Format("\"{0}\"", (info.Image ?? string.Empty).Replace(@"\", @"\\")));
                Stream resourceStream = new MemoryStream(Encoding.UTF8.GetBytes(proxyHtml));
                Logger.DebugWithFormat("URLProxy generated for {0} with {1} and {2}", decodedUrl, info.Location,
                                       info.Title);
                requestResponse.RespondWith(resourceStream, "text/html");
            }
            else if (requestResponse.Request.Url.StartsWith(URLBasedActivatedModuleFakeAddress))
            {
                string decodedUrl = HttpUtility.UrlDecode(new Uri(requestResponse.Request.Url).Query.Substring(1));
                HTMLModuleInfo info = WebSupportExtensions.Registry.ContainsKey(module) ? WebSupportExtensions.Registry[module] :
                    WebSupportExtensions.Registry.Values.FirstOrDefault(m_ => m_.Url.AbsoluteUri == decodedUrl);
                if (info == null)
                    return false;
                CEFBasedActivatedURLModule urlModule = module as CEFBasedActivatedURLModule;
                string proxyHtml = urlModule == null ? File.ReadAllText("URLActivatedProxy.html") : urlModule.GetURLProxyHtml();
                proxyHtml = proxyHtml.Replace("$DESTINATION_URL", String.Format("\"{0}\"", decodedUrl))
                                     .Replace("$LOCATION", String.Format("\"{0}\"", info.Location))
                                     .Replace("$TITLE", String.Format("\"{0}\"", info.Title))
                                     .Replace("$IMAGEPATH", String.Format("\"{0}\"", (info.Image ?? string.Empty).Replace(@"\", @"\\")));
                Stream resourceStream = new MemoryStream(Encoding.UTF8.GetBytes(proxyHtml));
                Logger.DebugWithFormat("URLActivatedProxy generated for {0} with {1} and {2}", decodedUrl, info.Location,
                                       info.Title);
                requestResponse.RespondWith(resourceStream, "text/html");
            }

            return false;
        }

        public void OnResourceResponse(IWebBrowser browser, string url, int status, string statusText, string mimeType,
                                       WebHeaderCollection headers)
        {
        }

        public bool GetDownloadHandler(IWebBrowser browser, string mimeType, string fileName, long contentLength,
                                       ref IDownloadHandler handler)
        {
            if (stream != null)
            {
                Logger.Error("Multiple downloads are not supported yet");
                return false;
            }
            path = Path.Combine(Path.GetTempPath(), fileName);
            stream = File.Create(path);
            handler = this;
            return true;
        }

        public bool GetAuthCredentials(IWebBrowser browser, bool isProxy, string host, int port, string realm,
                                       string scheme,
                                       ref string username, ref string password)
        {
            TaskDialogResult result = null;
            Application.Current.Dispatcher.Invoke(
                (Action)
                (() =>
                 result =
                 TaskDialog.Show(new TaskDialogOptions
                     {
                         UserPasswordInputEnabled = true,
                         AllowDialogCancellation = true,
                         CommonButtons = TaskDialogCommonButtons.OKCancel,
                         DefaultButtonIndex = 0,
                         Title = "Authentication Required",
                         VerificationText = "Remember password",
                         InitialUserInput = Environment.UserName,
                         UserInputEnabled = true,
                         MainInstruction = string.Format("The server {0}:{1} requires a username and password. The server says: {2}.", host, port, realm),
                         
                     })));
            if ((result != null) && 
                (result.Result == TaskDialogSimpleResult.Ok) &&
                (!string.IsNullOrEmpty(result.UserPasswordInput)))
            {
                username = result.UserInput;
                password = result.UserPasswordInput;
                return true;
            }
            return false;
        }

        public void Dispose()
        {
            webControl.Dispose();
        }

        public event EventHandler<EventArgs> LoadCompleted;
        public event EventHandler<EventArgs> InitCompleted;
        private Uri uri;

        public void LoadFile(Uri uri_)
        {
            uri = uri_;
            webControl.LoadCompleted += _WebControl_LoadCompleted;
            if (!initCompleted)
                InitCompleted += (sender, args) => webControl.Load(uri.ToString());
            else
                webControl.Load(uri.ToString());
        }

        private void _WebControl_LoadCompleted(object sender, LoadCompletedEventArgs url)
        {
            webControl.LoadCompleted -= _WebControl_LoadCompleted;

            ResourceManager resourceMgr = Resources.ResourceManager;
            var scriptsToLoad = new[]
                {
                    "Utils",
                    "IModule",
                    "IChromeRegistry",
                    "IChromeManager",
                    "ICommunicator",
                    "IEventRegistrator",
                    "IRoutingCommunicator"
                };
            foreach (string script in scriptsToLoad)
            {
                string scriptContent = resourceMgr.GetString(script);
                webControl.EvaluateScript(scriptContent);
            }
            initCompleted = true;
            if (CEFSettings.DeveloperToolsEnabled)
                webControl.ShowDevTools();

            WebControlLoadCompleted(this, EventArgs.Empty);
        }

        public void CallJavascriptFunction(string functionName_, bool sync, params object[] args_)
        {
            var builder = new StringBuilder(functionName_);
            builder.Append("(");
            foreach (object o in args_)
            {
                if (o == null)
                {
                    builder.Append("null,");
                    continue;
                }
                if (o is string)
                {
                    builder.Append("\'");
                }
                builder.Append(o);
                if (o is string)
                {
                    builder.Append("\'");
                }
                builder.Append(",");
            }
            if (args_.Length > 0)
            {
                builder.Remove(builder.Length - 1, 1);
            }
            builder.Append(")");
            Logger.DebugWithFormat("Executing JavaScript : {0}, sync: {1}", builder.ToString(), sync);
            try
            {
                if (sync)
                {
                    webControl.EvaluateScript(builder.ToString(), new TimeSpan(0, 0, 0, 1));
                }
                else
                {
                    webControl.ExecuteScript(builder.ToString());                    
                }
            }
            catch (ScriptException e)
            {
                Logger.WarningWithFormat("Error occured when evaluation script: {0}", e.Message);
            }
        }

        public object CallJavascriptFunctionWithResult(string functionName_, params object[] args_)
        {
            //var builder = new StringBuilder("return ");
            var builder = new StringBuilder();
            builder.Append(functionName_);
            builder.Append("(");
            foreach (object o in args_)
            {
                if (o == null)
                {
                    builder.Append("null,");
                    continue;
                }
                if (o is string)
                {
                    builder.Append("\'");
                }
                builder.Append(o);
                if (o is string)
                {
                    builder.Append("\'");
                }
                builder.Append(",");
            }
            if (args_.Length > 0)
            {
                builder.Remove(builder.Length - 1, 1);
            }
            builder.Append(")");
            Logger.DebugWithFormat("CJSFWR: {0}", builder.ToString());
            object result = null;
            try
            {
                result = webControl.EvaluateScript(builder.ToString(), new TimeSpan(0, 0, 0, 1));
            }
            catch (ScriptException e)
            {
                Logger.WarningWithFormat("Error occured when evaluation script: {0}", e.Message);
            }
            return result;
        }

        public Dispatcher Dispatcher
        {
            get { return webControl.Dispatcher; }
        }

        public object Control
        {
            get { return webControl; }
        }

        public IJavascriptCallsHandler JavascriptCallHandler { get; set; }

        private void WebControlLoadCompleted(object sender_, EventArgs e_)
        {
            EventHandler<EventArgs> copy = LoadCompleted;
            if (copy != null)
            {
                copy(this, e_);
            }
        }


        // ReSharper disable UnusedMember.Local
        public void exec(string callback, string module, string func, string argsInJSON)
            // ReSharper restore UnusedMember.Local
        {
            var args = JsonConvert.DeserializeObject<object[]>(argsInJSON);
            Application.Current.Dispatcher.Invoke(
                (Action) (() =>
                          typeof (IJavascriptCallsHandler).GetMethod(func).
                                                           Invoke(JavascriptCallHandler,
                                                                  BindingFlags.Instance |
                                                                  BindingFlags.Public |
                                                                  BindingFlags.NonPublic |
                                                                  BindingFlags.FlattenHierarchy,
                                                                  null, args, null)));
        }

        private static void InitializeWebCore(IApplication application)
        {
            if (!m_webCoreInitialized)
            {
                string directory = String.Format(@"{0}\Morgan Stanley\CEFWebSession",
                                                 Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData));
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }
                string logdirectory = String.Format(@"{0}\Morgan Stanley\CEFWebSession",
                                                    Environment.GetFolderPath(
                                                        Environment.SpecialFolder.LocalApplicationData));
                if (!Directory.Exists(logdirectory))
                {
                    Directory.CreateDirectory(logdirectory);
                }
                var settings = new Settings
                    {
                        AutoDetectProxySettings = true,
                        LogSeverity = LogSeverity.Verbose,
                        CachePath = directory,
                        LogFile = logdirectory + "\\log" + Process.GetCurrentProcess().Id + ".log",
                        PackLoadingDisabled = !CEFSettings.DeveloperToolsEnabled
                    };
                CEFSettings.Plugins.ToList().ForEach(settings.AddPluginPath);
                CefSharp.CEF.Initialize(settings);
                application.ApplicationClosed += (sender, args) => CefSharp.CEF.Shutdown();
                m_webCoreInitialized = true;
            }
        }
    }
}
