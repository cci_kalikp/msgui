﻿using System;
using System.Windows;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MSDesktop.WebSupport.CEF
{
    public class CEFBasedHTMLModule : HTMLModule
    {
        private readonly IApplication _application;

        public CEFBasedHTMLModule(IUnityContainer unityContainer_)
            : base(unityContainer_)
        {
            _application = unityContainer_.Resolve<IApplication>();
        }

        protected override IWebControl CreateWebControlComponent()
        {
            try
            {
                return new CEFWebControlAdapter(_application, this);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
                return null;
            }
        }
    }

   
}
