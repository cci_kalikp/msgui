﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Impl;
using MSDesktop.MessageRouting.History.GUI.Modules;

namespace MorganStanley.MSDotNet.MSGui.Impl.Extensions
{
/*****************************************************************************
 *                                 ___                                       *
 *                              .-'   `'.                                    *
 *                             /         \                                   *
 *                             |         ;                                   *
 *                             |         |           ___.--,                 *
 *                    _.._     |0) ~ (0) |    _.---'`__.-( (_.               *
 *             __.--'`_.. '.__.\    '--. \_.-' ,.--'`     `""`               *
 *            ( ,.--'`   ',__ /./;   ;, '.__.'`    __                        *
 *            _`) )  .---.__.' / |   |\   \__..--""  """--.,_                *
 *           `---' .'.''-._.-'`_./  /\ '.  \ _.-~~~````~~~-._`-.__.'         *
 *                 | |  .' _.-' |  |  \  \  '.               `~---`          *
 *                  \ \/ .'     \  \   '. '-._)                              *
 *                   \/ /        \  \    `=.__`~-.                           *
 *                   / /\         `) )    / / `"".`\                         *
 *             , _.-'.'\ \        / /    ( (     / /                         *
 *              `--~`   ) )    .-'.'      '.'.  | (                          *
 *                     (/`    ( (`          ) )  '-;                         *
 *                     `      '-;         (-'                                *
 *                                                                           *
 *                          RELEASE ZE KRAKEN!                               *
 *                                                                           *
 *****************************************************************************/
    public static class FrameworkExtensions
    {
        public static void EnableMessageRoutingHistoryGui(this Framework framework)
        {
            framework.AddModule<MessageRoutingHistoryGuiModule>();
            

        }
    }
}
