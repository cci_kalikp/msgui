﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MSDesktop.MessageRouting.History.GUI.Model;
using MSDesktop.MessageRouting.History.GUI.ViewModel;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Options;

namespace MSDesktop.MessageRouting.History.GUI.View
{
    public partial class HistoryView : UserControl, IOptionView, ISubItemAwareSeachablePage
    { 

 
        
        public HistoryViewModel HistoryViewModel { get; private set; }

        private readonly IHistoryManager historyManager;

        public HistoryView(IHistoryManager historyManager)
        {
            InitializeComponent();
            this.historyManager = historyManager;
        }

        public new UIElement Content { get { return this; } }

        public void OnCancel()
        {
        } 

        public void OnDisplay()
        {
            this.HistoryViewModel = new HistoryViewModel(historyManager);
            this.DataContext = HistoryViewModel;
            colInternal.Visibility = HistoryViewModel.ShowInternalMessage ? Visibility.Visible : Visibility.Collapsed;
        }

        public void OnHelp()
        {
        }

        public void OnOK()
        {
        }


 
        public IList<KeyValuePair<string, object>> GetMatchedItems(string textToSearch_)
        {
            if (HistoryViewModel == null) this.HistoryViewModel = new HistoryViewModel(historyManager);
            var matchedHistories = new List<KeyValuePair<string, object>>();
            foreach (var history in HistoryViewModel.HistoryModels)
            {

                if ((!string.IsNullOrEmpty(history.messageInfo) && history.messageInfo.ToLower().Contains(textToSearch_.ToLower())) ||
                    (!string.IsNullOrEmpty(history.TargetHostname) &&history.TargetHostname.ToLower().Contains(textToSearch_.ToLower())))
                {
                    matchedHistories.Add(new KeyValuePair<string, object>(history.ToString(), history));
                }
            }
            return matchedHistories;
        }

        public void LocateItem(object item_)
        {
            var item = HistoryViewModel.HistoryModels.FirstOrDefault(h_ => h_.Id == (item_ as HistoryModel).Id);
            if (item == null) return;
            Datagrid.SelectedItem = item;
            var row = Datagrid.ItemContainerGenerator.ContainerFromItem(item) as DataGridRow;
            if (row != null)
            {
                row.Highlight();
            }
        }

         
    }
}
