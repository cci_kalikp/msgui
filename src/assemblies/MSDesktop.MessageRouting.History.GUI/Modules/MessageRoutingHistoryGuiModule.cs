﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Impl;
using MSDesktop.MessageRouting.History.GUI.View;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MSDesktop.MessageRouting.History.GUI.Modules
{
    public class MessageRoutingHistoryGuiModule : IMSDesktopModule
    {
        private readonly IHistoryManager historyManager;
        private readonly IApplicationOptions applicationOptions;

        public MessageRoutingHistoryGuiModule(IHistoryManager historyManager, IApplicationOptions applicationOptions)
        {
            this.historyManager = historyManager;
            this.applicationOptions = applicationOptions;
        }

        public void Initialize()
        {
            applicationOptions.AddOptionPage("Routed Communication", "History", new HistoryView(historyManager));
        }

    }
}
