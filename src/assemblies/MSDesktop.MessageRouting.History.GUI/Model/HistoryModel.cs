﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using MSDesktop.MessageRouting.Routing;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;

namespace MSDesktop.MessageRouting.History.GUI.Model
{
    public class HistoryModel : ViewModelBase
    {
        public bool IsInternal { get; set; }
        private DateTime when;

        public DateTime When
        {
            get { return when; }
            set
            {
                when = value;
                OnPropertyChanged("When");
            }
        }

        private string targetHostname;

        public string TargetHostname
        {
            get { return targetHostname; }
            set
            {
                targetHostname = value;
                OnPropertyChanged("TargetHostname");
            }
        }

        private string targetApplication;

        public string TargetApplication
        {
            get { return targetApplication; }
            set
            {
                targetApplication = value;
                OnPropertyChanged("TargetApplication");
            }
        }

        private DeliveryStatus status;

        public DeliveryStatus Status
        {
            get { return status; }
            set
            {
                status = value;
                OnPropertyChanged("Status");
            }
        }

        private int id;

        public int Id
        {
            get { return id; }
            set
            {
                id = value;
                OnPropertyChanged("Id");
            }
        }

        internal string messageInfo;

        public string MessageInfo
        {
            get { return messageInfo ?? "Message information is not found."; }
            set
            {
                messageInfo = value;
                OnPropertyChanged("MessageInfo");
            }
        }

        public override string ToString()
        {
            return string.Format("{0} Sent to {1} at {2} ", 
                string.IsNullOrEmpty(messageInfo) ? "Message" : messageInfo, 
                 string.IsNullOrEmpty(this.TargetHostname) ? "all machines" : this.TargetHostname,
                 when); 
        }
 
    }
}
