﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
using System.Collections.ObjectModel;
using System.Windows;
using MSDesktop.MessageRouting.History.GUI.Model;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;

namespace MSDesktop.MessageRouting.History.GUI.ViewModel
{
    public class HistoryViewModel : ViewModelBase
    {
        private readonly IHistoryManager historyManager;

        public ObservableCollection<HistoryModel> HistoryModels { get; private set; }
        public bool ShowInternalMessage { get; set; }
        public HistoryViewModel(IHistoryManager historyManager)
        {
            this.historyManager = historyManager;
            this.HistoryModels = new ObservableCollection<HistoryModel>();
            InitHistoryModels();
        }

        private void InitHistoryModels()
        {
            bool hasInternal = false;
            foreach (var history in historyManager.Histories)
            {
                HistoryModels.Add(
                    new HistoryModel
                    {
                        Id = history.Id,
                        Status = history.Status,
                        TargetApplication = history.Target != null ? history.Target.Application : null,
                        TargetHostname = history.Target != null ? history.Target.Hostname : null,
                        When = history.When,
                        MessageInfo = history.MessageInfo,
                        IsInternal = history.IsInternal
                    });
                if (history.IsInternal) hasInternal = true;
            }
            ShowInternalMessage = hasInternal;

        }
         
    }
}
