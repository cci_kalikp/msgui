﻿#region File Info Header

/*_____________________________________________________________________________
  Copyright (c) 2010 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.
        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.AuthenticationService/FrameworkExtensions.cs#1 $
  $DateTime: 2014/02/20 14:28:23 $
    $Change: 867786 $
    $Author: hrechkin $
_____________________________________________________________________________*/

#endregion

using MSDesktop.AuthenticationService.ImpersonationLogin;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MSDesktop.AuthenticationService
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;


    public static class FrameworkExtensions
    {
        public static void RestoreTextBoxForImpersonation(this IFramework f, bool value = true)
        {
            LoginWindow.RestoreTextBox = value;
        }
    }
}
