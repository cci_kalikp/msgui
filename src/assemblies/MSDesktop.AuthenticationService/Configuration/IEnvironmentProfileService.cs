#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2008 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.AuthenticationService/Configuration/IEnvironmentProfileService.cs#1 $
  $DateTime: 2014/02/20 14:28:23 $
    $Change: 867786 $
    $Author: hrechkin $
_____________________________________________________________________________*/
#endregion File Info Header

using System.Collections.Generic;

namespace MSDesktop.AuthenticationService.Configuration
{
    /// <summary>
    /// Defines an interface to enumerate a list of environment profiles.
    /// </summary>
    public interface IEnvironmentProfileService
    {
        /// <summary>
        /// Gets a list of metadata that describes the modules.
        /// </summary>
        /// <returns>An <see cref="IList{T}"/> of <see cref="IEnvironmentProfile"/>.</returns>
        IList<IEnvironmentProfile> GetAvailableEnvironmentProfiles();

        /// <summary>
        /// Current environment profile
        /// </summary>
        IEnvironmentProfile Current { get; }
    }
}
