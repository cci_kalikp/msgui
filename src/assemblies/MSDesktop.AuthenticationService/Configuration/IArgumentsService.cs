#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2008 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.AuthenticationService/Configuration/IArgumentsService.cs#1 $
  $DateTime: 2014/02/20 14:28:23 $
    $Change: 867786 $
    $Author: hrechkin $
_____________________________________________________________________________*/
#endregion File Info Header

using System.Collections.Specialized;

namespace MSDesktop.AuthenticationService.Configuration
{
    public interface IArgumentsService
    {
        string[] Arguments { get; }
        StringDictionary Parameters { get; }
        string this[string param_] { get; }
    }
}