#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2008 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.AuthenticationService/Configuration/ArgumentsService.cs#1 $
  $DateTime: 2014/02/20 14:28:23 $
    $Change: 867786 $
    $Author: hrechkin $
_____________________________________________________________________________*/
#endregion File Info Header

using System;
using System.Collections.Specialized;
using System.Text.RegularExpressions;

namespace MSDesktop.AuthenticationService.Configuration
{
    public class ArgumentsService : IArgumentsService
    {
        #region Constructors

        public ArgumentsService()
            : this(Environment.GetCommandLineArgs(), 1)
        {
        }

        public ArgumentsService(string arguments)
            : this(arguments.Split(' '), 0)
        {
        }

        public ArgumentsService(string arguments, int start)
            : this(arguments.Split(' '), start)
        {
        }

        public ArgumentsService(string arguments, int start, char delimiter)
            : this(arguments.Split(delimiter), start)
        {
        }

        public ArgumentsService(string[] arguments)
            : this(arguments, 0)
        {
        }

        public ArgumentsService(string[] args, int start)
        {
            Regex splitter = new Regex(@"^-{1,2}|^/|=|:", RegexOptions.IgnoreCase | RegexOptions.Compiled);
            Regex remover = new Regex(@"^['""]?(.*?)['""]?$", RegexOptions.IgnoreCase | RegexOptions.Compiled);
            string parameter = null;
            string[] parts;

            this.Arguments = args;
            this.Parameters = new StringDictionary();

            // Valid parameters forms:
            // {-,/,--}param{ ,=,:}((",')value(",'))
            // Examples: -param1 value1 --param2 /param3:"Test-:-work" /param4=happy -param5 '--=nice=--'
            for (int index = start; index < args.Length; index++)
            {
                string Txt = args[index];

                // Look for new parameters (-,/ or --) and a possible enclosed value (=,:)
                parts = splitter.Split(Txt, 3);
                switch (parts.Length)
                {
                        // Found a value (for the last parameter found (space separator))
                    case 1:
                        if (parameter != null)
                        {
                            if (!this.Parameters.ContainsKey(parameter))
                            {
                                parts[0] = remover.Replace(parts[0], "$1");
                                this.Parameters.Add(parameter, parts[0]);
                            }
                            parameter = null;
                        }
                        // else Error: no parameter waiting for a value (skipped)
                        break;
                        // Found just a parameter
                    case 2:
                        // The last parameter is still waiting. With no value, set it to true.
                        if (parameter != null)
                        {
                            if (!this.Parameters.ContainsKey(parameter))
                            {
                                this.Parameters.Add(parameter, "true");
                            }
                        }
                        parameter = parts[1];
                        break;
                        // Parameter with enclosed value
                    case 3:
                        // The last parameter is still waiting. With no value, set it to true.
                        if (parameter != null)
                        {
                            if (!this.Parameters.ContainsKey(parameter))
                            {
                                this.Parameters.Add(parameter, "true");
                            }
                        }
                        parameter = parts[1];
                        // Remove possible enclosing characters (",')
                        if (!this.Parameters.ContainsKey(parameter))
                        {
                            parts[2] = remover.Replace(parts[2], "$1");
                            this.Parameters.Add(parameter, parts[2]);
                        }

                        parameter = null;
                        break;
                }
            }
            // In case a parameter is still waiting
            if (parameter != null)
            {
                if (!this.Parameters.ContainsKey(parameter)) this.Parameters.Add(parameter, "true");
            }
        }

        #endregion Constructors

        #region Implementation of IArgumentsService

        public string[] Arguments { get; private set; }

        public StringDictionary Parameters { get; private set; }

        public string this[string param_]
        {
            get { return (this.Parameters[param_]); }
        }

        #endregion Implementation of IArgumentsService
    }
}
