#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2008 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.AuthenticationService/Configuration/EnvironmentProfile.cs#1 $
  $DateTime: 2014/02/20 14:28:23 $
    $Change: 867786 $
    $Author: hrechkin $
_____________________________________________________________________________*/
#endregion File Info Header

using System;
using System.Diagnostics;

namespace MSDesktop.AuthenticationService.Configuration
{
    [DebuggerDisplay("Name: {Name}, Details: {Details}")]
    public class EnvironmentProfile : IEnvironmentProfile, IEquatable<EnvironmentProfile>,
                                      IEquatable<IEnvironmentProfile>, IComparable<EnvironmentProfile>,
                                      IComparable<IEnvironmentProfile>
    {
        #region Constructors

        public EnvironmentProfile(string name, string details)
        {
            Name = name;
            Details = details;
        }

        #endregion Constructors

        #region Public Properties

        public string Name { get; private set; }
        public string Details { get; private set; }

        #endregion Public Properties

        #region Base Class Overrides

        public override string ToString()
        {
            return Name;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (EnvironmentProfile)) return false;
            return Equals((EnvironmentProfile) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((Name != null ? Name.GetHashCode() : 0)*397) ^ (Details != null ? Details.GetHashCode() : 0);
            }
        }

        #endregion Base Class Overrides

        #region Implementation of IEquatable<EnvironmentProfile>

        public bool Equals(EnvironmentProfile obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return Equals(obj.Name, Name) && Equals(obj.Details, Details);
        }

        #endregion Implementation of IEquatable<EnvironmentProfile>

        #region Implementation of IEquatable<IEnvironmentProfile>

        public bool Equals(IEnvironmentProfile obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return Equals(obj.Name, Name) && Equals(obj.Details, Details);
        }

        #endregion Implementation of IEquatable<IEnvironmentProfile>

        #region Implementation of IComparable<EnvironmentProfile>

        public int CompareTo(EnvironmentProfile other)
        {
            return (other != null) ? string.Compare(Name, other.Name, StringComparison.InvariantCulture) : 1;
        }

        #endregion Implementation of IComparable<EnvironmentProfile>

        #region Implementation of IComparable<IEnvironmentProfile>

        public int CompareTo(IEnvironmentProfile other)
        {
            return (other != null) ? string.Compare(Name, other.Name, StringComparison.InvariantCulture) : 1;
        }

        #endregion Implementation of IComparable<EnvironmentProfile>
    }
}
