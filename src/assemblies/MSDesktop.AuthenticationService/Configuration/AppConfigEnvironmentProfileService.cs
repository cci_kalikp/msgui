#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2008 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.AuthenticationService/Configuration/AppConfigEnvironmentProfileService.cs#1 $
  $DateTime: 2014/02/20 14:28:23 $
    $Change: 867786 $
    $Author: hrechkin $
_____________________________________________________________________________*/
#endregion File Info Header

using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using Microsoft.Practices.Unity;

namespace MSDesktop.AuthenticationService.Configuration
{
    public class AppConfigEnvironmentProfileService : IEnvironmentProfileService
    {
        #region Declarations

        public static readonly string ProfileConfigSectionName = "profiles";
        public static readonly string CurrentProfileInstanceName = "CurrentProfile";

        private readonly IUnityContainer _container;
        private readonly bool _addDefaultProfile;
        private readonly string _applicationName;

        #endregion Declarations

        #region Constructors

        public AppConfigEnvironmentProfileService(IUnityContainer container, bool addDefaultProfile, string applicationName)
        {
            _applicationName = applicationName;
            _container = container;
            _addDefaultProfile = addDefaultProfile;
        }

        #endregion Constructors

        #region Implementation of IEnvironmentProfileService

        public IList<IEnvironmentProfile> GetAvailableEnvironmentProfiles()
        {
            var profiles = new List<IEnvironmentProfile>();

            var profileConfig = (NameValueCollection) ConfigurationManager.GetSection(ProfileConfigSectionName);
            if (profileConfig != null)
            {
                foreach (string name in profileConfig.Keys)
                {
                    profiles.Add(new EnvironmentProfile(name, profileConfig[name]));
                }
            }

            if (_addDefaultProfile)
            {
                profiles.Add(new EnvironmentProfile("Default", string.Format("app={0}", _applicationName)));
            }

            return profiles;
        }

        public IEnvironmentProfile Current
        {
            get { return _container.Resolve<IEnvironmentProfile>(CurrentProfileInstanceName); }
        }

        #endregion Implementation of IEnvironmentProfileService
    }
}
