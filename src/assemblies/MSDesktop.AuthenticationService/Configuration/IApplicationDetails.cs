#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2008 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.AuthenticationService/Configuration/IApplicationDetails.cs#2 $
  $DateTime: 2014/02/21 02:28:17 $
    $Change: 867876 $
    $Author: caijin $
_____________________________________________________________________________*/
#endregion File Info Header

using System.Windows.Controls;
using System.Windows.Media;

namespace MSDesktop.AuthenticationService.Configuration
{
    public interface IApplicationDetails
    {
        string Name { get; set; }
        string DisplayName { get; set; }
        string Version { get; set; }
        ImageSource Image { get; set; }
    }
}
