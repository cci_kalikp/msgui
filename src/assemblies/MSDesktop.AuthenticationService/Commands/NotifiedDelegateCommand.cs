﻿#region File Info Header

/*_____________________________________________________________________________
  Copyright (c) 2011 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.AuthenticationService/Commands/NotifiedDelegateCommand.cs#1 $
  $DateTime: 2014/02/20 14:28:23 $
    $Change: 867786 $
    $Author: hrechkin $
_____________________________________________________________________________*/

#endregion File Info Header

using System;
using System.ComponentModel;
using System.Linq.Expressions;
using Microsoft.Practices.Composite.Presentation.Commands;

namespace MSDesktop.AuthenticationService.Commands
{


    /// <summary>
    /// A DelegateCommand&lt;T&gt; subclass which raises its CanExecuteChanged
    ///   event when the supplied INotifyPropertyChanged implementation raises a
    ///   PropertyChanged event for the given property.
    ///   <example>
    /// <![CDATA[
    ///   public bool IsEnabled { get; set; }
    ///   public NotifiedDelegateCommand<object> Command { get; private set; }
    ///   public void Initialise()
    ///   {
    ///     Command = new NotifiedDelegateCommand(i => DoSomething(), () => IsEnabled);
    ///   }
    /// ]]>
    ///   </example>
    /// </summary>
    /// <typeparam name="T">
    /// The type of the CommandParameter.
    /// </typeparam>
    public class NotifiedDelegateCommand<T> : DelegateCommand<T>
    {
        #region Constants and Fields

        private readonly INotifyPropertyChanged _notifier;

        private readonly string _notifierProperty;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="NotifiedDelegateCommand{T}"/> class. 
        /// Initialises a new instance of NotifiedDelegateCommand&lt;T&gt;.
        /// </summary>
        /// <param name="executeMethod">
        /// The delegate to execute when Execute is called on the command.
        /// </param>
        /// <param name="canExecuteProperty">
        /// Expression referencing a property which determines the state of this
        ///   ICommand's CanExecute method. 
        ///   Note that the T parameter will, out of necessity, be thrown away, as it
        ///   is not possible to supply a parameter to a property.
        /// </param>
        /// <param name="notifier">
        /// The INotifyPropertyChanged instance where the property expression supplied for
        ///   CanExecute is declared.
        /// </param>
        public NotifiedDelegateCommand(
            Action<T> executeMethod, Expression<Func<T, bool>> canExecuteProperty, INotifyPropertyChanged notifier)
            : this(executeMethod, canExecuteProperty.Compile(), notifier)
        {
            MemberExpression memberExpression = (MemberExpression) canExecuteProperty.Body;
            _notifierProperty = memberExpression.Member.Name;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NotifiedDelegateCommand{T}"/> class. 
        /// Initialises a new instance of NotifiedDelegateCommand&lt;T&gt;.
        /// </summary>
        /// <param name="executeMethod">
        /// The delegate to execute when Execute is called on the command.
        /// </param>
        /// <param name="canExecuteProperty">
        /// Expression referencing a property which determines the state of this
        ///   ICommand's CanExecute method.
        /// </param>
        /// <param name="notifier">
        /// The INotifyPropertyChanged instance where the property expression supplied for
        ///   CanExecute is declared.
        /// </param>
        public NotifiedDelegateCommand(
            Action<T> executeMethod, Expression<Func<bool>> canExecuteProperty, INotifyPropertyChanged notifier)
            : this(executeMethod, CreateCanExecuteMethod(canExecuteProperty), notifier)
        {
            var memberExpression = (MemberExpression) canExecuteProperty.Body;
            _notifierProperty = memberExpression.Member.Name;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NotifiedDelegateCommand{T}"/> class. 
        /// Initialises a new instance of NotifiedDelegateCommand&lt;T&gt;.
        /// </summary>
        /// ///
        /// <param name="executeMethod">
        /// The delegate to execute when Execute is called on the command.
        /// </param>
        /// <param name="canExecuteProperty">
        /// Expression referencing a property which determines the state of this
        ///   ICommand's CanExecute method.
        /// </param>
        public NotifiedDelegateCommand(Action<T> executeMethod, Expression<Func<bool>> canExecuteProperty)
            : this(executeMethod, canExecuteProperty, GetNotifier(canExecuteProperty))
        {
        }

        private NotifiedDelegateCommand(
            Action<T> executeMethod, Func<T, bool> canExecuteMethod, INotifyPropertyChanged notifier)
            : base(executeMethod, canExecuteMethod)
        {
            _notifier = notifier;
            _notifier.PropertyChanged += OnNotifierPropertyChanged;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Create a Func&lt;T, Boolean&gt; delegate that can be used as a DelegateCommand&lt;T&gt;'s CanExecute
        ///   delegate from a Func&lt;Boolean&gt; expression.
        /// </summary>
        /// <param name="canExecuteProperty">
        /// The expression.
        /// </param>
        /// <returns>
        /// A Func&lt;T, Boolean&gt; delegate.
        /// </returns>
        private static Func<T, bool> CreateCanExecuteMethod(Expression<Func<bool>> canExecuteProperty)
        {
            Func<bool> expression = canExecuteProperty.Compile();
            return foo_ => expression();
        }

        /// <summary>
        /// Extracts the object on which a property in a lambda expression is declared.
        /// </summary>
        /// <param name="canExecuteProperty">
        /// A lambda expression referencing a property.
        /// </param>
        /// <returns>
        /// The INotifyPropertyChanged instance.
        /// </returns>
        private static INotifyPropertyChanged GetNotifier(Expression<Func<bool>> canExecuteProperty)
        {
            var notifier =
                ((ConstantExpression) ((MemberExpression) canExecuteProperty.Body).Expression).Value as
                INotifyPropertyChanged;
            return notifier;
        }

        private void OnNotifierPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (string.IsNullOrEmpty(_notifierProperty))
            {
                RaiseCanExecuteChanged();
            }
            else
            {
                if (_notifierProperty == e.PropertyName)
                {
                    RaiseCanExecuteChanged();
                }
            }
        }

        #endregion
    }
}
