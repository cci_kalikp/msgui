﻿#region File Info Header

/*_____________________________________________________________________________
  Copyright (c) 2011 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.AuthenticationService/ImpersonationLogin/LoginWindow.xaml.cs#1 $
  $DateTime: 2014/02/20 14:28:23 $
    $Change: 867786 $
    $Author: hrechkin $
_____________________________________________________________________________*/
#endregion File Info Header

using System;
using System.Windows;
using System.Windows.Input;

namespace MSDesktop.AuthenticationService.ImpersonationLogin
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : IDisposable
    {
        #region Constructors and Destructors

        public static bool RestoreTextBox;

        public LoginWindow()
        {
            InitializeComponent();
        }

        #endregion

        #region Methods

        private void OnLoaded(object sender_, RoutedEventArgs e_)
        {
            Activate();
            BringIntoView();
            if (RestoreTextBox && ((ILoginController)DataContext).AllowImpersonation)
            {
                _usernameField.Visibility = Visibility.Collapsed;
                _usernameFieldText.Visibility = Visibility.Visible;
                if ( (((ILoginController) DataContext).AllowProfileSelection) && 
                    ((ILoginController) DataContext).AvailableProfiles.Count>0)
                {
                    ((ILoginController) DataContext).SelectedProfile =
                        ((ILoginController) DataContext).AvailableProfiles[0];
                }
            }
        }

        #endregion

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            Dispose(true);
        }

        private void Dispose(bool disposing_)
        {
            if (disposing_)
            {
                DataContext = null;
                FocusManager.SetFocusedElement(this, null);
                _profileField = null;
                _userInfoPanel = null;
                _usernameField = null;
                _dock = null;
                Content = null;
            }
        }
    }
}