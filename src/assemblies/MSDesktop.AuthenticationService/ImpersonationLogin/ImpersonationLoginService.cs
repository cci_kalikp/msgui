﻿#region File Info Header

/*_____________________________________________________________________________
  Copyright (c) 2011 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.AuthenticationService/ImpersonationLogin/ImpersonationLoginService.cs#1 $
  $DateTime: 2014/02/20 14:28:23 $
    $Change: 867786 $
    $Author: hrechkin $
_____________________________________________________________________________*/

#endregion File Info Header

using System;
using MorganStanley.IED.Concord.Runtime;
using MorganStanley.MSDotNet.My;
using MSDesktop.AuthenticationService.BasicLogin;
using MSDesktop.AuthenticationService.Configuration;

namespace MSDesktop.AuthenticationService.ImpersonationLogin
{
    public class ImpersonationLoginService : LoginService
    {
        private static readonly IMSLogger Logger = MSLoggerFactory.CreateLogger<ImpersonationLoginService>();

        private readonly Func<ILoginController> _loginControllerProvider;

        public ImpersonationLoginService(Func<ILoginController> loginControllerProvider, ConcordRuntime runtime,
                                         IArgumentsService argumentsService, IEnvironmentProfileService profileService)
            : base(null, runtime, argumentsService, profileService)
        {
            _loginControllerProvider = loginControllerProvider;
        }

        protected override LoginDisplay GetLoginDisplay(string forceLogin, string username, string forceProfile, IEnvironmentProfile profile)
        {
            LoginDisplay displayFlags = LoginDisplay.None;
            if (!string.IsNullOrEmpty(forceLogin))
            {
                displayFlags |= LoginDisplay.Credentials;
            }
            else
            { 
                displayFlags |= LoginDisplay.Credentials;
            }

            if (!String.IsNullOrEmpty(forceProfile) || profile == null)
            {
                displayFlags |= LoginDisplay.Profile;
            }

            return displayFlags;
        }

        protected override ILoginResult GetLoginResult(LoginDisplay displayFlags, IEnvironmentProfile selectedProfile,
                                                       ConcordUserInfo defaultUserInformation)
        {
            if (displayFlags != LoginDisplay.None && _loginControllerProvider != null)
            {
                using (var loginController = _loginControllerProvider())
                {
                    loginController.Initialize(selectedProfile, defaultUserInformation.Username);

                    using (var loginView = new LoginWindow())
                    {
                        loginView.DataContext = loginController;
                        var dialogResult = loginView.ShowDialog();

                        if (dialogResult.HasValue && dialogResult.Value)
                        {
                            return new LoginResult(
                                true,
                                loginController.SelectedProfile,
                                new ConcordUserInfo(null, loginController.Username, string.Empty, null));
                        }
                    }
                }

                if (Logger.IsInfoLoggable())
                {
                    Logger.Info("User canceled operation");
                }
            }
            else
            {
                return new LoginResult(true, selectedProfile, defaultUserInformation);
            }

            return LoginResult.Failed(null, defaultUserInformation);
        }
    }
}
