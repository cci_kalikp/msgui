﻿#region File Info Header

/*_____________________________________________________________________________
  Copyright (c) 2011 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.AuthenticationService/ImpersonationLogin/ILoginController.cs#1 $
  $DateTime: 2014/02/20 14:28:23 $
    $Change: 867786 $
    $Author: hrechkin $
_____________________________________________________________________________*/
#endregion File Info Header

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Input;
using MSDesktop.AuthenticationService.BasicLogin;
using MSDesktop.AuthenticationService.Configuration;

namespace MSDesktop.AuthenticationService.ImpersonationLogin
{
  /// <summary>
  /// An interface for controller class used to bind agains a <see cref="ILoginView"/>
  /// </summary>
  public interface ILoginController : INotifyPropertyChanged, IDisposable
  {
    #region Properties

    /// <summary>
    ///   Gets or sets a value indicating whether the current user is allowed
    ///   to impersonate as some others.
    /// </summary>
    bool AllowImpersonation { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether users can change the currently selected profile.
    /// </summary>
    bool AllowProfileSelection { get; set; }

    /// <summary>
    ///   Gets the details about this current application.
    /// </summary>
    IApplicationDetails ApplicationDetails { get; }

    /// <summary>
    ///   Gets the list of available profiles that users can pick from.
    /// </summary>
    IList<IEnvironmentProfile> AvailableProfiles { get; }

    /// <summary>
    ///   Gets the list of available users that users can pick from.
    /// </summary>
    IEnumerable<string> AvailableUsers { get; }

    /// <summary>
    ///   Gets or sets any error while impersonating.
    /// </summary>
    string ImpersonationError { get; set; }

    /// <summary>
    ///   Gets a value indicating whether the OK button on the view is enabled.
    /// </summary>
    bool IsOkEnabled { get; }

    /// <summary>
    ///   Gets the command that perform the login action (set the result and exit the view).
    /// </summary>
    ICommand LoginCommand { get; }

    /// <summary>
    ///   Gets or sets the result of login.
    /// </summary>
    bool? LoginResult { get; set; }

    /// <summary>
    ///   Gets or sets the selected profile.
    /// </summary>
    IEnvironmentProfile SelectedProfile { get; set; }

    /// <summary>
    ///   Gets or sets the selected user name.
    /// </summary>
    string Username { get; set; }

    #endregion

    #region Public Methods

    /// <summary>
    /// Initialize the login controller with a default selected profile and user name.
    ///   If no profile or user name is default, pass in null.
    /// </summary>
    /// <param name="defaultProfile">
    /// The default profile.
    /// </param>
    /// <param name="defaultUsername">
    /// The default user name.
    /// </param>
    void Initialize(IEnvironmentProfile defaultProfile, string defaultUsername);

    #endregion
  }
}