﻿#region File Info Header

/*_____________________________________________________________________________
  Copyright (c) 2011 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.AuthenticationService/ImpersonationLogin/LoginController.cs#2 $
  $DateTime: 2014/04/08 21:26:23 $
    $Change: 875617 $
    $Author: caijin $
_____________________________________________________________________________*/
#endregion File Info Header

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows.Input;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;
using MorganStanley.MSDotNet.My;
using MSDesktop.AuthenticationService.Commands;
using MSDesktop.AuthenticationService.Configuration;
using MSDesktop.AuthenticationService.Infrastructure;

namespace MSDesktop.AuthenticationService.ImpersonationLogin
{
    /// <summary>
    /// An implementation of <see cref="ILoginController"/>. When users select a profile, 
    ///   this controller will look up the profile's mail group specified in using app.config 
    ///   to find all the users. Subclasses are responsible to provide this directory search.
    /// </summary>
    public abstract class LoginController : ViewModelBase, ILoginController
    {
        #region Constants and Fields

        private static readonly IMSLogger _logger = MSLoggerFactory.CreateLogger<LoginController>();

        private readonly object _lock = new object();

        private readonly Func<IEnumerable<string>> _retrieveAvailableUsersFunc;

        private bool _allowImpersonation = true;

        private bool _allowProfileSelection = true;

        private IEnumerable<string> _availableUsers;

        private string _impersonationError;

        private bool? _loginResult;

        private IDictionary<string, IEnumerable<string>> _mailGroupCache = new Dictionary<string, IEnumerable<string>>();

        private long _numberOfConcurrentQueries;

        private IEnvironmentProfile _selectedProfile;

        private string _username;

        #endregion

        #region Constructors and Destructors

        protected LoginController(IApplicationDetails applicationDetails, IEnvironmentProfileService profileService)
        {
            ApplicationDetails = applicationDetails;
            AvailableProfiles = profileService.GetAvailableEnvironmentProfiles();
            _retrieveAvailableUsersFunc = RetrieveAvailableUsers;
            LoginCommand = new NotifiedDelegateCommand<object>(p => LoginResult = true, () => IsOkEnabled);
        }

        #endregion
 

        #region Properties

        public bool AllowImpersonation
        {
            get { return _allowImpersonation; }

            set
            {
                if (_allowImpersonation != value)
                {
                    _allowImpersonation = value;
                    OnPropertyChanged(PropertyNames.AllowImpersonation);
                }
            }
        }

        public bool AllowProfileSelection
        {
            get { return _allowProfileSelection; }

            set
            {
                if (_allowProfileSelection != value)
                {
                    _allowProfileSelection = value;
                    OnPropertyChanged(PropertyNames.AllowProfileSelection);
                }
            }
        }

        public IApplicationDetails ApplicationDetails { get; private set; }

        public IList<IEnvironmentProfile> AvailableProfiles { get; private set; }

        public IEnumerable<string> AvailableUsers
        {
            get { return _availableUsers; }

            set
            {
                _availableUsers = value;
                AfterAvailableUsersChanged();
                OnPropertyChanged(PropertyNames.AvailableUsers);
            }
        }

        public string ImpersonationError
        {
            get { return _impersonationError; }

            set
            {
                if (ImpersonationError != value)
                {
                    _impersonationError = value;
                    OnPropertyChanged(PropertyNames.ImpersonationError);
                }
            }
        }

        public bool IsOkEnabled
        {
            get
            {
                return (Interlocked.Read(ref _numberOfConcurrentQueries) == 0 && SelectedProfile != null &&
                       !string.IsNullOrEmpty(Username))||LoginWindow.RestoreTextBox;
            }
        }

        public ICommand LoginCommand { get; private set; }

        public string LoginDetails
        {
            get
            {
                return AllowImpersonation
                           ? "Login to a profile with a user name"
                           : "Login to the selected profile. You need to be in this profile's LDAP group to login.";
            }
        }

        public bool? LoginResult
        {
            get { return _loginResult; }

            set
            {
                if (_loginResult != value)
                {
                    _loginResult = value;
                    OnPropertyChanged(PropertyNames.LoginResult);
                }
            }
        }

        public IEnvironmentProfile SelectedProfile
        {
            get { return _selectedProfile; }

            set
            {
                if (_selectedProfile != value)
                {
                    _selectedProfile = value;
                    AfterProfileChanged();
                    OnPropertyChanged(PropertyNames.SelectedProfile);
                }
            }
        }

        public string Username
        {
            get { return _username; }

            set
            {
                if (_username != value)
                {
                    _username = value;
                    OnPropertyChanged(PropertyNames.Username);
                }

                OnPropertyChanged(PropertyNames.IsOkEnabled);
            }
        }

        #endregion

        #region Implemented Interfaces

        #region IDisposable

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            Dispose(true);
        }

        #endregion

        #region ILoginController

        public virtual void Initialize(IEnvironmentProfile defaultProfile, string defaultUsername)
        {
            SelectedProfile = defaultProfile;
            Username = defaultUsername;

            // if there is a profile provided (through the command line arguments), we should disable 
            // the profile selection. 
            if (SelectedProfile != null)
            {
                AllowProfileSelection = false;
            }
        }

        #endregion

        #endregion

        #region Methods

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _availableUsers = null;

                lock (_lock)
                {
                    _mailGroupCache.Clear();
                    _mailGroupCache = null;
                }
            }
        }

        protected abstract IList<string> RetrieveAvailableUsersCore(string profileMailgroup);

        protected virtual void AfterProfileChanged()
        {
            Interlocked.Increment(ref _numberOfConcurrentQueries);
            _retrieveAvailableUsersFunc.BeginInvoke(DoneRetrieveAvailableUsers, null);
            OnPropertyChanged(PropertyNames.IsOkEnabled);
        }

        protected virtual void AfterAvailableUsersChanged()
        {
        }

        /// <summary>
        /// The profile string is expected to be in the name value pair format separated 
        ///   by ";". There should be a parameter providing the mail group associating
        ///   with each profile.
        /// </summary>
        /// <param name="profileString">
        /// The string describing each profile.
        /// </param>
        /// <returns>
        /// The mail group associated with the mail group. Retuns null if there is no mail group found.
        /// </returns>
        private static string RetrieveProfileMailgroup(string profileString)
        {
            var settings = profileString.Split(';');

            return settings
                .Select(s => s.Split('='))
                .Where(nv => (nv.Length == 2 && nv[0] == "mailgroup"))
                .Select(nv => nv[1])
                .FirstOrDefault();
        }

        private void DoneRetrieveAvailableUsers(IAsyncResult ar)
        {
            IEnumerable<string> result = null;

            try
            {
                result = _retrieveAvailableUsersFunc.EndInvoke(ar);
            }
            catch (Exception ex)
            {
                if (_logger.IsErrorLoggable())
                {
                    _logger.Error("Error while retrieving users for profile " + SelectedProfile.Name, ex);
                }
            }
            finally
            {
                Interlocked.Decrement(ref _numberOfConcurrentQueries);
            }

            // We will set the list of users name to a complete new list so WPF
            // will wipe out the current combo box selection after the call to
            // set AvailableUsers so we have to restore it afterwards (only if
            // the previous name belong to the list).
            var previousUserName = Username;
            AvailableUsers = result;
            if (AvailableUsers != null && AvailableUsers.Contains(previousUserName))
            {
                Username = previousUserName;
            }
        }
 
        private IEnumerable<string> RetrieveAvailableUsers()
        {
            if (SelectedProfile == null)
            {
                return new List<string>();
            }

            string profileMailgroup = RetrieveProfileMailgroup(SelectedProfile.Details);

            if (string.IsNullOrEmpty(profileMailgroup))
            {
                return new List<string>();
            }

            lock (_lock)
            {
                if (_mailGroupCache == null)
                {
                    return new List<string>();
                }

                if (!_mailGroupCache.ContainsKey(profileMailgroup))
                {
                    _mailGroupCache[profileMailgroup] = RetrieveAvailableUsersCore(profileMailgroup);
                }

                return _mailGroupCache[profileMailgroup];
            }
        }

        #endregion

        private struct PropertyNames
        {
            #region Constants and Fields

            public static readonly string AllowImpersonation =
                PropertyNameRetriever<ILoginController>.GetPropertyName(x => x.AllowImpersonation);

            public static readonly string AllowProfileSelection =
                PropertyNameRetriever<ILoginController>.GetPropertyName(x => x.AllowProfileSelection);

            public static readonly string AvailableUsers =
                PropertyNameRetriever<ILoginController>.GetPropertyName(x => x.AvailableUsers);

            public static readonly string ImpersonationError =
                PropertyNameRetriever<ILoginController>.GetPropertyName(x => x.ImpersonationError);

            public static readonly string IsOkEnabled =
                PropertyNameRetriever<ILoginController>.GetPropertyName(x => x.IsOkEnabled);

            public static readonly string LoginResult =
                PropertyNameRetriever<ILoginController>.GetPropertyName(x => x.LoginResult);

            public static readonly string SelectedProfile =
                PropertyNameRetriever<ILoginController>.GetPropertyName(x => x.SelectedProfile);

            public static readonly string Username = PropertyNameRetriever<ILoginController>.GetPropertyName(
                x => x.Username);

            #endregion
        }
    }
}
