﻿using System.Collections.Generic;
using MorganStanley.MSDotNet.Directory.DictionarySearch;
using MSDesktop.AuthenticationService.Configuration;

namespace MSDesktop.AuthenticationService.ImpersonationLogin
{
    public sealed class ImpersonationLoginController : LoginController
    {
        public ImpersonationLoginController(IApplicationDetails applicationDetails,
                                            IEnvironmentProfileService profileService) :
                                                base(applicationDetails, profileService)
        {
        }

        protected override IList<string> RetrieveAvailableUsersCore(string profileMailgroup)
        {
            IList<string> users = new List<string>();

            using (var s = new FWD2SearchEx())
            {
                var g = s.GetGroup(profileMailgroup);
                if (g != null)
                {
                    foreach (var p in g.PeopleInGroup.Values)
                    {
                        users.Add(p.LogonID);
                    }
                }
            }

            return users;
        }
    }
}
