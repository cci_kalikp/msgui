﻿#region File Info Header

/*_____________________________________________________________________________
  Copyright (c) 2010 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.
  $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.AuthenticationService/Controls/StringHasValueVisibilityConverter.cs#1 $
  $DateTime: 2014/02/20 14:28:23 $
  $Change: 867786 $
  $Author: hrechkin $
_____________________________________________________________________________*/
#endregion

using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace MSDesktop.AuthenticationService.Controls
{
  /// <summary>
  /// A simple converter that will return visible if the value is not empty or null.
  /// </summary>
  public class StringHasValueVisibilityConverter : IValueConverter
  {
    #region Constructors and Destructors

    /// <summary>
    ///   Initializes a new instance of the <see cref = "StringHasValueVisibilityConverter" /> class. 
    ///   The default value is hidden when string empty and visible otherwise.
    /// </summary>
    public StringHasValueVisibilityConverter()
    {
      VisibilityWhenEmpty = Visibility.Hidden;
      VisibilityWhenNotEmpty = Visibility.Visible;
    }

    #endregion

    #region Properties

    /// <summary>
    ///   Gets or sets the visibility to return when the string is null or empty.
    /// </summary>
    public Visibility VisibilityWhenEmpty { get; set; }

    /// <summary>
    ///   Gets or sets the visibility to return when the string has some value.
    /// </summary>
    public Visibility VisibilityWhenNotEmpty { get; set; }

    #endregion

    #region Implemented Interfaces

    #region IValueConverter

    public object Convert(object value_, Type targetType_, object parameter_, CultureInfo culture_)
    {
      string stringValue = value_ as string;
      return string.IsNullOrEmpty(stringValue) ? VisibilityWhenEmpty : VisibilityWhenNotEmpty;
    }

    public object ConvertBack(object value_, Type targetType_, object parameter_, CultureInfo culture_)
    {
      throw new NotImplementedException();
    }

    #endregion

    #endregion
  }
}