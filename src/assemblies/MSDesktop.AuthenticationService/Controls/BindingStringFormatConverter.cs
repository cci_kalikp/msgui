﻿#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2008 Morgan Stanley, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

  $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.AuthenticationService/Controls/BindingStringFormatConverter.cs#1 $
  $DateTime: 2014/02/20 14:28:23 $
  $Change: 867786 $
  $Author: hrechkin $
_____________________________________________________________________________*/
#endregion File Info Header

using System;
using System.Windows;
using System.Diagnostics;
using System.Windows.Data;
using System.Globalization;

namespace MSDesktop.AuthenticationService.Controls
{
  /// <summary>
  /// Represents the converter that applies a string formatter.
  /// </summary>
  /// <example>
  /// Specify a {} preceding the ConverterParameter to escape the {0}, {1}, ..., {n} parameters:
  /// {Binding Path=Text, ConverterParameter={}{0} - Application Security, Converter={StaticResource _bindingStringFormatConverter}}</example>
  /// <remarks>
  /// This class is used in lieu of BindingBase.StringFormat, which is only supported in: 3.5 SP1, 3.0 SP2:
  /// http://msdn.microsoft.com/en-us/library/system.windows.data.bindingbase.stringformat.aspx</remarks>
  [DebuggerDisplay("Format: {Format}")]
  public sealed class BindingStringFormatConverter : IValueConverter
  {
    #region Public Properties
    /// <summary>
    /// Format to be used if the ConverterParameter is omitted.
    /// </summary>
    public string Format
    {
      get;
      set;
    }
    #endregion Public Properties

    #region IValueConverter Members
    /// <summary>
    /// Formats the value using the Format specified in parameter.
    /// </summary>
    /// <param name="value_">The Value to convert.</param>
    /// <param name="targetType_">This parameter is not used.</param>
    /// <param name="parameter_">This parameter is used as the String.Format format parameter, but can be omitted in lieu of the Format property.</param>
    /// <param name="culture_">This parameter is not used.</param>
    /// <returns></returns>
    public object Convert(object value_, Type targetType_, object parameter_, CultureInfo culture_)
    {
      if (value_ != null && value_ != DependencyProperty.UnsetValue)
      {
        string format = parameter_ as string ?? Format;
        if (!string.IsNullOrEmpty(format))
        {
          string retValue = string.Format(format, value_);

          return retValue;
        }
      }

      return value_;
    }

    /// <summary>
    /// Not Supported Exception.
    /// </summary>
    /// <param name="value_"></param>
    /// <param name="targetType_"></param>
    /// <param name="parameter_"></param>
    /// <param name="culture_"></param>
    /// <returns></returns>
    /// <exception cref="NotSupportedException"/>
    public object ConvertBack(object value_, Type targetType_, object parameter_, CultureInfo culture_)
    {
      Debug.Fail(GetType().FullName + ".ConvertBack is being called.");
      throw new NotSupportedException();
    }
    #endregion IValueConverter Members
  }
}