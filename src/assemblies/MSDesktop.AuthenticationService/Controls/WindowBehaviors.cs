﻿#region File Info Header

/*_____________________________________________________________________________
  Copyright (c) 2010 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.AuthenticationService/Controls/WindowBehaviors.cs#1 $
  $DateTime: 2014/02/20 14:28:23 $
    $Change: 867786 $
    $Author: hrechkin $
_____________________________________________________________________________*/
#endregion File Info Header

using System.Windows;
using System.Windows.Interop;

namespace MSDesktop.AuthenticationService.Controls
{
    /// <summary>
    /// A class comprising of different behaviors related to windows.
    /// </summary>
    public static class WindowBehaviors
    {
        #region Constants and Fields

        public static readonly DependencyProperty DialogResultProperty = DependencyProperty.RegisterAttached(
            "DialogResult", typeof (bool?), typeof (WindowBehaviors),
            new UIPropertyMetadata(null, OnDialogResultChanged));

        /// <summary>
        /// A behavior to indicate that the window should always center vertically on screen 
        /// even after resizing. 
        /// Note: this will cause the window to shake when the user resize manually so it is recommended to disallow window resizing when this is turned on.  
        /// </summary>
        public static readonly DependencyProperty AlwaysVerticallyCenterOnScreenProperty =
            DependencyProperty.RegisterAttached(
                "AlwaysVerticallyCenterOnScreen",
                typeof (bool),
                typeof (WindowBehaviors),
                new UIPropertyMetadata(false, OnAlwaysVerticallyCenterOnScreenChanged));

        #endregion

        #region Public Methods

        public static bool? GetDialogResult(DependencyObject obj_)
        {
            return (bool?) obj_.GetValue(DialogResultProperty);
        }

        public static void SetDialogResult(DependencyObject obj_, bool? value_)
        {
            obj_.SetValue(DialogResultProperty, value_);
        }

        public static bool GetAlwaysVerticallyCenterOnScreen(DependencyObject obj_)
        {
            return (bool) obj_.GetValue(AlwaysVerticallyCenterOnScreenProperty);
        }

        public static void SetAlwaysVerticallyCenterOnScreen(DependencyObject obj_, bool value_)
        {
            obj_.SetValue(AlwaysVerticallyCenterOnScreenProperty, value_);
        }

        #endregion

        #region Methods

        private static void OnDialogResultChanged(DependencyObject d_, DependencyPropertyChangedEventArgs e_)
        {
            var window = d_ as Window;
            if (window != null)
            {
                if (ComponentDispatcher.IsThreadModal)
                {
                    // if this is a modal window and the dialog result is not set then set the new value, this will close 
                    // the dialog.
                    if (window.DialogResult == null)
                    {
                        window.DialogResult = e_.NewValue as bool?;
                    }
                }
                else if (window.DialogResult == null)
                {
                    window.Close();
                }
            }
        }

        private static void OnAlwaysVerticallyCenterOnScreenChanged(
            DependencyObject d_, DependencyPropertyChangedEventArgs e_)
        {
            var window = d_ as Window;
            if (window != null)
            {
                if ((bool) e_.NewValue)
                {
                    window.SizeChanged += RepositionVertically;
                }
                else
                {
                    if ((bool) e_.OldValue)
                    {
                        window.SizeChanged -= RepositionVertically;
                    }
                }
            }
        }

        /// <summary>
        /// Reposition the window so that it is vertically centered on the screen.
        /// </summary>
        /// <param name="sender_">
        /// The window to resize.
        /// </param>
        /// <param name="e_">
        /// The event argument.
        /// </param>
        private static void RepositionVertically(object sender_, SizeChangedEventArgs e_)
        {
            var window = sender_ as Window;
            if (window == null || !window.IsLoaded)
            {
                return;
            }

            Rect workArea = SystemParameters.WorkArea;

            // only reposition the window if it is longer than the screen height
            double actualWindowHeight = window.ActualHeight;
            if ((window.Top + actualWindowHeight) > workArea.Height)
            {
                window.Top = (workArea.Height - actualWindowHeight)/2;
            }
        }

        #endregion
    }
}
