﻿#region File Info Header

/*_____________________________________________________________________________
  Copyright (c) 2010 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.
        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.AuthenticationService/Infrastructure/PropertyNameRetriever.cs#1 $
  $DateTime: 2014/02/20 14:28:23 $
    $Change: 867786 $
    $Author: hrechkin $
_____________________________________________________________________________*/

#endregion

using System;
using System.Linq.Expressions;

namespace MSDesktop.AuthenticationService.Infrastructure
{
    /// <summary>
    /// A helper class to find a property name of a type from an expression.
    /// </summary>
    /// <typeparam name="T">The type of that we need to get the property from</typeparam>
    public class PropertyNameRetriever<T>
    {
        /// <summary>
        /// Find the property name from a specified expression
        /// For example, PropertyNameRetriever.GetPropertyName(someType_ => someType_.Prop)
        /// </summary>
        /// <typeparam name="TResult">The type of the value returned by the expression</typeparam>
        /// <param name="expression">The expression from which we extract the property name</param>
        /// <returns>The property name inside the expression (always assuming the expression is a member expression</returns>
        /// <exception cref="ArgumentException">Throws if the passed in expression is not a member expression</exception>
        public static string GetPropertyName<TResult>(Expression<Func<T, TResult>> expression)
        {
            var memberExpression = expression.Body as MemberExpression;
            if (memberExpression == null)
            {
                throw new ArgumentException("Input expression must be a member expression");
            }

            return memberExpression.Member.Name;
        }
    }
}
