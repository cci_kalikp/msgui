#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2008 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.AuthenticationService/BasicLogin/LoginResult.cs#1 $
  $DateTime: 2014/02/20 14:28:23 $
    $Change: 867786 $
    $Author: hrechkin $
_____________________________________________________________________________*/
#endregion File Info Header

using System.Diagnostics;
using MSDesktop.AuthenticationService.Configuration;
using MorganStanley.IED.Concord.Runtime;

namespace MSDesktop.AuthenticationService.BasicLogin
{
  #region LoginResult Class
  [DebuggerDisplay("Profile: {Profile.Name}, Username={UserInformation.Username}, Success={Success}")]
  public class LoginResult : ILoginResult
  {
    #region Constructors
    public LoginResult(bool success_, IEnvironmentProfile profile_, ConcordUserInfo userInformation_)
    {
      this.Success = success_;
      this.Profile = profile_;
      this.UserInformation = userInformation_;
    }
    #endregion Constructors

    #region Public Properties
    public bool Success { get; set; }
    public IEnvironmentProfile Profile { get; set; }
    public ConcordUserInfo UserInformation { get; set; }
    #endregion Public Properties

    #region Public Methods
    public static ILoginResult Failed()
    {
      return LoginResult.Failed(null, null);
    }

    public static ILoginResult Failed(IEnvironmentProfile profile_, ConcordUserInfo userInformation_)
    {
      return new LoginResult(false, profile_, userInformation_);
    }
    #endregion Public Methods
  }
  #endregion LoginResult Class
}