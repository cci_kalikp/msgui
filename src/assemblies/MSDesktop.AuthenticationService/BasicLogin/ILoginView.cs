﻿#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2008 Morgan Stanley, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

  $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.AuthenticationService/BasicLogin/ILoginView.cs#1 $
  $DateTime: 2014/02/20 14:28:23 $
  $Change: 867786 $
  $Author: hrechkin $
_____________________________________________________________________________*/
#endregion File Info Header

using System;
using System.Windows;
using MSDesktop.AuthenticationService.Configuration;

namespace MSDesktop.AuthenticationService.BasicLogin
{
  [Flags]
  public enum LoginDisplay
  {
    None = 0,
    Credentials = 2, 
    Profile = 4,
    All = Credentials | Profile
  }

  public interface ILoginView
  {
    LoginDisplay DisplayFlags { get; set; }

    IEnvironmentProfile SelectedProfile { get; set; }

    string Username { get; set; }

    string Password { get; set; }

    bool? ShowView();

    bool? ShowView(Window owner_);
  }
}