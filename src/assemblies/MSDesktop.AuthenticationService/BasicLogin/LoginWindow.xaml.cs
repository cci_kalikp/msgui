﻿#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2008 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.AuthenticationService/BasicLogin/LoginWindow.xaml.cs#1 $
  $DateTime: 2014/02/20 14:28:23 $
    $Change: 867786 $
    $Author: hrechkin $
_____________________________________________________________________________*/
#endregion File Info Header

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using MSDesktop.AuthenticationService.Configuration;

namespace MSDesktop.AuthenticationService.BasicLogin
{
  /// <summary>
  /// Interaction logic for LoginWindow.xaml
  /// </summary>
  public partial class LoginWindow : Window, ILoginView
  {
    #region Declarations
    private static readonly DependencyPropertyKey CurrentDisplayPropertyKey =
      DependencyProperty.RegisterReadOnly("CurrentDisplay", typeof(LoginDisplay), typeof(LoginWindow),
      new PropertyMetadata(LoginDisplay.None));

    public static readonly DependencyProperty CurrentDisplayProperty = CurrentDisplayPropertyKey.DependencyProperty;

    public static readonly DependencyProperty PasswordProperty =
      DependencyProperty.Register("Password", typeof(string), typeof(LoginWindow));

    public static readonly DependencyProperty SelectedProfileProperty =
      DependencyProperty.Register("SelectedProfile", typeof(IEnvironmentProfile), typeof(LoginWindow));

    public static readonly DependencyProperty UsernameProperty =
      DependencyProperty.Register("Username", typeof(string), typeof(LoginWindow));

    private readonly EventHandler _passwordChangedHandler;
    private readonly EventHandler _usernameChangedHandler;
    private readonly EventHandler _profileChangedHandler;
    #endregion Declarations

    #region Constructors
    public LoginWindow(IApplicationDetails applicationDetails_, IEnvironmentProfileService profileService_)
    {
      this.ApplicationDetails = applicationDetails_;

      this.AvailableProfiles = profileService_.GetAvailableEnvironmentProfiles();

      this.InitializeComponent();

      DependencyPropertyDescriptor.FromProperty(PasswordProperty, typeof(LoginWindow)).AddValueChanged(this, _passwordChangedHandler = delegate { this.InvalidateCommands(); });
      DependencyPropertyDescriptor.FromProperty(UsernameProperty, typeof(LoginWindow)).AddValueChanged(this, _usernameChangedHandler = delegate { this.InvalidateCommands(); });
      DependencyPropertyDescriptor.FromProperty(SelectedProfileProperty, typeof(LoginWindow)).AddValueChanged(this, _profileChangedHandler = delegate { this.InvalidateCommands(); });
    }
    #endregion Constructors

    #region Base Class Overrides
    protected override void OnClosed(EventArgs e)
    {
      base.OnClosed(e);

      DependencyPropertyDescriptor.FromProperty(PasswordProperty, typeof(LoginWindow)).RemoveValueChanged(this, _passwordChangedHandler);
      DependencyPropertyDescriptor.FromProperty(UsernameProperty, typeof(LoginWindow)).RemoveValueChanged(this, _usernameChangedHandler);
      DependencyPropertyDescriptor.FromProperty(SelectedProfileProperty, typeof(LoginWindow)).RemoveValueChanged(this, _profileChangedHandler);
    }
    #endregion Base Class Overrides

    #region Public Properties
    public LoginDisplay DisplayFlags { get; set; }

    public IList<IEnvironmentProfile> AvailableProfiles { get; private set; }

    public IApplicationDetails ApplicationDetails { get; private set; }

    public LoginDisplay CurrentDisplay
    {
      get { return (LoginDisplay)this.GetValue(LoginWindow.CurrentDisplayProperty); }
      private set { this.SetValue(LoginWindow.CurrentDisplayPropertyKey, value); }
    }

    public string Password
    {
      get { return (string)this.GetValue(LoginWindow.PasswordProperty); }
      set { this.SetValue(LoginWindow.PasswordProperty, value); }
    }

    public IEnvironmentProfile SelectedProfile
    {
      get { return (IEnvironmentProfile)this.GetValue(LoginWindow.SelectedProfileProperty); }
      set { this.SetValue(LoginWindow.SelectedProfileProperty, value); }
    }

    public string Username
    {
      get { return (string)this.GetValue(LoginWindow.UsernameProperty); }
      set { this.SetValue(LoginWindow.UsernameProperty, value); }
    }
    #endregion Public Properties

    #region Public Methods
    public bool? ShowView()
    {
      return this.ShowDialog();
    }

    public bool? ShowView(Window owner_)
    {
      Owner = owner_;
      return this.ShowDialog();
    }
    #endregion Public Methods

    #region Event Handlers
    private void _next_Click(object sender_, RoutedEventArgs e_)
    {
      this.ChangeDisplay(this.CurrentDisplay, this.DisplayFlags);
    }

    private void _ok_Click(object sender_, RoutedEventArgs e_)
    {
      this.DialogResult = true;
    }

    private void Window_Loaded(object sender, RoutedEventArgs e)
    {
      this.Activate();
      this.BringIntoView();
      this.ChangeDisplay(this.CurrentDisplay, this.DisplayFlags);
    }

    private void Window_GotFocus(object sender_, RoutedEventArgs e_)
    {
      TextBox textBox = e_.OriginalSource as TextBox;
      if (textBox != null)
      {
        textBox.SelectAll();
        e_.Handled = true;
        return;
      }

      PasswordBox passwordBox = e_.OriginalSource as PasswordBox;
      if (passwordBox != null)
      {
        passwordBox.SelectAll();
        e_.Handled = true;
        return;
      }
    }
    #endregion Event Handlers

    #region Private Methods
    private void ChangeDisplay(LoginDisplay currentDisplay_, LoginDisplay allowedDisplays_)
    {
      switch (currentDisplay_)
      {
        case LoginDisplay.None:
          if ((allowedDisplays_ & LoginDisplay.Credentials) == LoginDisplay.Credentials)
          {
            _credentialsPanel.Visibility = Visibility.Visible;
            _profilePanel.Visibility = Visibility.Collapsed;

            if ((allowedDisplays_ & LoginDisplay.Profile) == LoginDisplay.Profile)
            {
              _next.Visibility = Visibility.Visible;
              _ok.Visibility = Visibility.Collapsed;
            }
            else
            {
              _next.Visibility = Visibility.Collapsed;
              _ok.Visibility = Visibility.Visible;
              _ok.IsEnabled = (this.SelectedProfile != null);
            }

            if (String.IsNullOrEmpty(this.Username))
            {
              Keyboard.Focus(_usernameField);
            }
            else
            {
              Keyboard.Focus(_passwordField);
            }

            this.CurrentDisplay = LoginDisplay.Credentials;
          }
          else
          {
            this.ChangeDisplay(LoginDisplay.Credentials, allowedDisplays_);
          }
          break;

        case LoginDisplay.Credentials:
          _credentialsPanel.Visibility = Visibility.Collapsed;
          _profilePanel.Visibility = Visibility.Visible;

          _next.Visibility = Visibility.Collapsed;
          _ok.Visibility = Visibility.Visible;
          _ok.IsEnabled = SelectedProfile != null;

          Keyboard.Focus(_profileField);

          this.CurrentDisplay = LoginDisplay.Profile;
          break;
      }
    }

    private void InvalidateCommands()
    {
      _next.IsEnabled = _next.IsVisible &&
                        this.CurrentDisplay == LoginDisplay.Credentials &&
                        !String.IsNullOrEmpty(this.Username) &&
                        !String.IsNullOrEmpty(this.Password);

      if (this.CurrentDisplay == LoginDisplay.Credentials)
      {
        _ok.IsEnabled = _ok.IsVisible &&
                        !String.IsNullOrEmpty(this.Username) &&
                        !String.IsNullOrEmpty(this.Password);
      }
      else
      {
        _ok.IsEnabled = _ok.IsVisible &&
                        this.SelectedProfile != null;
      }
    }
    #endregion Private Methods
  }
}
