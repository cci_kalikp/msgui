#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2008 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.AuthenticationService/BasicLogin/LoginService.cs#1 $
  $DateTime: 2014/02/20 14:28:23 $
    $Change: 867786 $
    $Author: hrechkin $
_____________________________________________________________________________*/
#endregion File Info Header

using System;
using System.Collections.Generic;
using MorganStanley.IED.Concord.Runtime;
using MorganStanley.MSDotNet.My;
using MSDesktop.AuthenticationService.Configuration;

namespace MSDesktop.AuthenticationService.BasicLogin
{
    public class LoginService : ILoginService
    {
        #region Declarations

        private static IMSLogger _logger = MSLoggerFactory.CreateLogger<LoginService>();

        public const string DefaultProfileSettingName = "DefaultProfile";
        public const string LoginRequredSettingName = "LoginRequired";
        public const string UsernameArgument = "username";
        public const string ProfileArgument = "profile";
        public const string PasswordArgument = "password";
        public const string ForceProfileArgument = "forceprofile";
        public const string ForceLoginArgument = "forcelogin"; 

        private readonly Func<ILoginView> _loginViewProvider;
        private readonly ConcordRuntime _runtime;
        private readonly IArgumentsService _arguments;
        private readonly IEnvironmentProfileService _profileService;

        #endregion Declarations

        #region Constructors

        public LoginService(Func<ILoginView> loginViewProvider, ConcordRuntime runtime,
                            IArgumentsService argumentsService, IEnvironmentProfileService profileService)
        {
            _loginViewProvider = loginViewProvider;
            _runtime = runtime;
            _arguments = argumentsService;
            _profileService = profileService;
        }

        #endregion Constructors

        #region Implementation of ILoginService
         
        public ILoginResult Login()
        {
           IEnvironmentProfile selectedProfile;

            var profiles = new List<IEnvironmentProfile>(_profileService.GetAvailableEnvironmentProfiles());

            // see if we wish to override
            string username = _arguments[UsernameArgument];
            string profile = _arguments[ProfileArgument];
            string password = _arguments[PasswordArgument];
            string forceProfile = _arguments[ForceProfileArgument];
            string forceLogin = _arguments[ForceLoginArgument];
            ConcordUserInfo userInformation = !string.IsNullOrEmpty(username)
                                                  ? new ConcordUserInfo(null, username, password, null)
                                                  : new ConcordInternalUserInfo();

            if (profile == null)
            {
                profile = _runtime.Context[DefaultProfileSettingName] as string;
            }

            selectedProfile = profiles.Find(p => p.Name == profile);
            var displayFlags = GetLoginDisplay(forceLogin, username, forceProfile, selectedProfile);
            
            
            return GetLoginResult(displayFlags, selectedProfile, userInformation);
        }
         

        protected virtual LoginDisplay GetLoginDisplay(string forceLogin, string username, string forceProfile, IEnvironmentProfile profile)
        {
            LoginDisplay displayFlags = LoginDisplay.None;
            if (!string.IsNullOrEmpty(forceLogin))
            {
                displayFlags |= LoginDisplay.Credentials;
            } 
            else
            {
                bool loginPrompt;
                Boolean.TryParse(_runtime.Context[LoginRequredSettingName] as string, out loginPrompt);
                if (loginPrompt)
                {
                    displayFlags |= LoginDisplay.Credentials;
                }
            }

            if (!String.IsNullOrEmpty(forceProfile) || profile == null)
            {
                displayFlags |= LoginDisplay.Profile;
            }

            return displayFlags;
        }

        /// <summary>
        /// Get result of the login process. Depending on the display flag, an appropriate view is shown
        /// to get the user and profile information. 
        /// </summary>
        /// <param name="displayFlags">The type of login view to show.</param>
        /// <param name="selectedProfile">The default profile to select when showing the view.</param>
        /// <param name="defaultUserInformation">The default information about the user. In case the login view is not shown, this is what this method returns.</param>
        /// <returns>A login result from the view (if shown) or from the default values (if the view is by-passed</returns>
        protected virtual ILoginResult GetLoginResult(LoginDisplay displayFlags, IEnvironmentProfile selectedProfile,
                                                      ConcordUserInfo defaultUserInformation)
        {
            if (displayFlags != LoginDisplay.None && _loginViewProvider != null)
            {
                ILoginView loginView = _loginViewProvider();
                loginView.Username = defaultUserInformation.Username; 
                loginView.SelectedProfile = selectedProfile;
                loginView.DisplayFlags = displayFlags;
                bool? dialogResult = loginView.ShowView();

                if (dialogResult.HasValue && dialogResult.Value)
                {
                    return new LoginResult(
                        true, loginView.SelectedProfile,
                        new ConcordUserInfo(null, loginView.Username, loginView.Password, null));
                }

                if (_logger.IsInfoLoggable())
                {
                    _logger.Info("User canceled operation");
                }
            }
            else
            {
                return new LoginResult(true, selectedProfile, defaultUserInformation);
            }

            return LoginResult.Failed(null, defaultUserInformation);
        }

        #endregion Implementation of ILoginService
    }
}
