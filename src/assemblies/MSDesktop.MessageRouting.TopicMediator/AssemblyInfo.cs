#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.MessageRouting.TopicMediator/AssemblyInfo.cs#1 $
  $DateTime: 2013/12/19 08:34:48 $
    $Change: 859541 $
    $Author: istvanf $
_____________________________________________________________________________*/
#endregion

using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Resources;

[assembly: AssemblyTitle("Concord Topic Mediator Library")]
[assembly: AssemblyDescription("Concord Topic Mediator Library")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Morgan Stanley")]
[assembly: AssemblyProduct("Morgan Stanley IED Concord Topic Mediator Library")]
[assembly: AssemblyCopyright("Morgan Stanley 2003")]
[assembly: AssemblyTrademark("Morgan Stanley")]
[assembly: AssemblyCulture("")]		


[assembly: AssemblyVersion("2.1.2.*")]

[assembly: AssemblyDelaySign(false)]

//[assembly: AssemblyKeyFile(@"m:\dev\ied\concordkey\prod\install\common\Concord_StrongNameKey.snk")]
//[assembly: AssemblyKeyFile(@"\\san01b\DevAppsGML\dist\ied\PROJ\concordkey\prod\Concord_StrongNameKey.snk")]

[assembly: NeutralResourcesLanguageAttribute("en-US")]