#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.MessageRouting.TopicMediator/Interfaces/IColleaguePublisher.cs#1 $
  $DateTime: 2013/12/19 08:34:48 $
    $Change: 859541 $
    $Author: istvanf $
_____________________________________________________________________________*/
#endregion

#region MorganStanley.IED.Concord.TopicMediator Namespace
namespace MorganStanley.IED.Concord.TopicMediator
{
  #region using
  using System;
  using System.Collections;
  using Application;
  #endregion using

  #region
  /// <include file='xmldocs/IColleaguePublisher.cs.xml' path='doc/doc[@for="ColleagueNotifyDelegate"]/*'/>
  public delegate void ColleagueNotifyDelegate(IColleague sender_, System.Xml.XmlDocument data_, string[] topics_);
  #endregion

  #region IColleaguePublisher Interface
  /// <include file='xmldocs/IColleaguePublisher.cs.xml' path='doc/doc[@for="IColleaguePublisher"]/*'/>
  [Obsolete("This is no longer used, use IColleagueListener instead. This interface and class is provided for backward compatibility only.", false)]
  public interface IColleaguePublisher : IColleague
  {
    #region Constants
    #endregion Constants

    #region Enums
    #endregion Enums

    #region Events
    /// <include file='xmldocs/IColleaguePublisher.cs.xml' path='doc/doc[@for="IColleaguePublisher.Notify"]/*'/>
    event ColleagueNotifyDelegate Notify;
    #endregion Events

    #region Public Properties
    /// <summary>
    /// Obsolete property containing the encoders for this topic.
    /// </summary>
    Hashtable TopicEncoders { get; }
    #endregion Public Properties

    #region Public Methods
    /// <summary>
    /// Adds a topic to this IColleaguePublisher.
    /// </summary>
    /// <param name="topic_"></param>    
    void AddTopic(string topic_);
    /// <include file='xmldocs/IColleaguePublisher.cs.xml' path='doc/doc[@for="IColleaguePublisher.RemoveTopic"]/*'/>
    void RemoveTopic(string topic_);
    /// <include file='xmldocs/IColleaguePublisher.cs.xml' path='doc/doc[@for="IColleaguePublisher.AddFilter"]/*'/>
    void AddFilter(string filter_);
    /// <include file='xmldocs/IColleaguePublisher.cs.xml' path='doc/doc[@for="IColleaguePublisher.RemoveFilter"]/*'/>
    void RemoveFilter(string filter_);
    /// <include file='xmldocs/IColleaguePublisher.cs.xml' path='doc/doc[@for="IColleaguePublisher.Publish1"]/*'/>
    void Publish(string[] topics_, System.Xml.XmlDocument data_);
    /// <include file='xmldocs/IColleaguePublisher.cs.xml' path='doc/doc[@for="IColleaguePublisher.Publish2"]/*'/>
    void Publish(string[] topics_, System.Xml.XmlDocument data_, string[] filters_);
    /// <include file='xmldocs/IColleaguePublisher.cs.xml' path='doc/doc[@for="IColleaguePublisher.Publish3"]/*'/>
    void Publish(IColleague colleague_, System.Xml.XmlDocument data_);
    /// <summary>
    /// 
    /// </summary>
    /// <param name="topics_"></param>
    /// <param name="data_"></param>
    void Publish(string[] topics_, ITopicMediatorMessage data_);
    /// <include file='xmldocs/IColleaguePublisher.cs.xml' path='doc/doc[@for="IColleaguePublisher.Publish2"]/*'/>
    void Publish(string[] topics_, ITopicMediatorMessage data_, string[] filters_);
    /// <include file='xmldocs/IColleaguePublisher.cs.xml' path='doc/doc[@for="IColleaguePublisher.Publish3"]/*'/>
    void Publish(IColleague colleague_, ITopicMediatorMessage data_);
    /// <summary>
    /// Linking stuff - obsolete
    /// </summary>
    [Obsolete("The linking functionality in TopicMediator has been removed: you must now link using LinkManager or your own linking implementation.", true)]
    void AddTopicEncoder(string topic_, string mask_);
    /// <summary>
    /// Linking stuff - obsolete
    /// </summary>
    [Obsolete("The linking functionality in TopicMediator has been removed: you must now link using LinkManager or your own linking implementation.", true)]
    void RemoveTopicEncoder(string topic_, string mask_);
    #endregion Public Methods
	}
  #endregion IColleaguePublisher Interface
}
#endregion MorganStanley.IED.Concord.TopicMediator Namespace
