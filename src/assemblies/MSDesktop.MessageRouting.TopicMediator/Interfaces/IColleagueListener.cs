#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.MessageRouting.TopicMediator/Interfaces/IColleagueListener.cs#1 $
  $DateTime: 2013/12/19 08:34:48 $
    $Change: 859541 $
    $Author: istvanf $
_____________________________________________________________________________*/
#endregion

namespace MorganStanley.IED.Concord.TopicMediator
{
  using System;
  using System.Collections;
  using MorganStanley.MSDotNet.MSNet;
  using Application;

  /// <summary>
  /// The interface to allow an object to listen for messages
  /// </summary>
  public interface IColleagueListener : IColleague
  {

    #region Events

    /// <summary>
    /// Should be raised whenever the OnNotify methods are called
    /// </summary>
    event NotifyEventHandler Notify;
    
    #endregion Events

    #region Public Properties

    /// <summary>
    /// This is the loop on which we can raise messages for this
    /// listener
    /// </summary>
    IMSNetLoop CallbackLoop {get;}

    /// <summary>
    /// The arraylist of topics that this colleague has published
    /// on or will publish on.
    /// </summary>
    string[] PublishedTopics { get;}
    /// <summary>
    /// The arraylist of topics that this colleague has been
    /// subscribed to. Note that these are the topics themselves,
    /// not the handlers that respond to these topics
    /// </summary>
    string[] SubscribedTopics { get;}

    /// <summary>
    /// Called whenever this colleague has been successfully subscribed
    /// to the given topic.
    /// </summary>
    void Subscribed( string topic, bool unsubscribed );
    /// <summary>
    /// Called whenever this colleague is associated with a publish call.
    /// </summary>
    /// <param name="topic"></param>
    void Published(string topic);

    #endregion Public Properties

    #region Public Methods

    /// <summary>
    /// Adds a topic that this IColleagueListener will be notified for.
    /// </summary>
    /// <param name="topic_"></param>    
    void AddTopic(string topic_);
    /// <summary>
    /// Remove a topic so this IColleagueListener will no longer be notified
    /// </summary>
    /// <param name="topic_"></param>
    void RemoveTopic(string topic_);

    /// <summary>
    /// Method that will be called if this listener recieves an XmlDocument.
    /// Note that depending on the originating data type, we will recieve either an
    /// ITopicMediator or an XmlDocument. So you need to react to both of these 
    /// methods.
    /// </summary>
    void OnNotify(NotifyEventArgs e);

    #endregion Public Methods
	}
}
