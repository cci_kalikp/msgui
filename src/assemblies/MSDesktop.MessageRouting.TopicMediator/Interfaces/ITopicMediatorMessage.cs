#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2004 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.MessageRouting.TopicMediator/Interfaces/ITopicMediatorMessage.cs#1 $
  $DateTime: 2013/12/19 08:34:48 $
    $Change: 859541 $
    $Author: istvanf $
_____________________________________________________________________________*/
#endregion File Info Header

using System;
using System.Xml;

namespace MorganStanley.IED.Concord.TopicMediator
{
  /// <summary>
  /// Very basic interface for a message object sent over TopicMediator.
  /// </summary>
  public interface ITopicMediatorMessage
  {
    /// <summary>
    /// Get or set the XmlDocument for this TopicMediator message.
    /// Note that the resulting XmlDocument must represent the
    /// object that implements this interface, and that the document
    /// must conform to the schema defined in TopicMediator.
    /// 
    /// Note also that messages sent using the ITopicMediatorMessage 
    /// object may well be converted to/from the XmlDocument structure
    /// unpredictably so care should be taken to ensure that this property
    /// is implemented efficiently.
    /// </summary>
    XmlDocument XmlDocument 
    { 
      get; 
      set; 
    }

    /// <summary>
    /// Clone the message: return a new copy of the message that referencially
    /// distinct from the original.
    /// </summary>
    ITopicMediatorMessage Clone();
  }
}
