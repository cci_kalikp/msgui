#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2004 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.MessageRouting.TopicMediator/Interfaces/IEnricher.cs#1 $
  $DateTime: 2013/12/19 08:34:48 $
    $Change: 859541 $
    $Author: istvanf $
_____________________________________________________________________________*/
#endregion File Info Header

using System;
using System.Xml;
using MorganStanley.IED.Concord.TopicMediator;

namespace MorganStanley.IED.Concord.MessageEnrichers
{
  /// <summary>
  /// Very basic interface for a TopicMediator enricher object
  /// </summary>
  public interface IEnricher
  {
    /// <summary>
    /// Enrich an ITopicMediatorMessag object with the given topic.
    /// </summary>
    void Enrich(ref ITopicMediatorMessage tm_, string topic_);
    /// <summary>
    /// Enrich an XMLDocument with the given topic.
    /// </summary>
    void Enrich(ref XmlDocument cmDoc_, string topic_);
  }
}
