#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.MessageRouting.TopicMediator/ColleagueListener.cs#1 $
  $DateTime: 2013/12/19 08:34:48 $
    $Change: 859541 $
    $Author: istvanf $
_____________________________________________________________________________*/
#endregion

namespace MorganStanley.IED.Concord.TopicMediator
{
  using System;
  using System.Collections;
  using System.Collections.Specialized;
  using System.Xml;

  using MorganStanley.MSDotNet.MSNet;

  using MorganStanley.IED.Concord.Logging;
  using MorganStanley.IED.Concord.Logging.RequestTracing;

  /// <summary>
  /// Class that implements the ICOlleagueListener interface. Used to subscribe
  /// to TopicMediator messages, including raising events, etc.
  /// </summary>
  internal sealed class ColleagueListener : IColleagueListener, IDisposable
  {

    #region Constants

    public static readonly string CLASS = "Colleague";
    private const string STATE_ELEMENTNAME = "ColleagueListenerState";
    private const string STATE_COLLEAGUEID_ATTRIBUTENAME = "colleagueid";

    #endregion Constants

    #region Private Declarations
    
    private Guid               _colleagueID;
    private IMSNetLoop         _loop;
    private ArrayList _publishedTopics;
    private ArrayList _subscribedTopics;

    private static Hashtable _instances = new Hashtable();

    #endregion Private Declarations

    #region Constructors / creators
   
    /// <summary>
    /// Gets the registered listener for the given colleague, if there is one.
    /// </summary>
    /// <param name="colleague_"></param>
    /// <returns></returns>
    internal static IColleagueListener GetListener(Application.IColleague colleague_)
    {
      if (_instances.ContainsKey( colleague_.ColleagueId ))
      {
        return (IColleagueListener)_instances[colleague_.ColleagueId];
      }
      return null;
    }

    internal ColleagueListener(IMSNetLoop callbackLoop_, string[] topics_,  XmlNode state_, string typeKey_ )
    {
      bool cancel = false;
      ((Application.IColleague)this).LoadState( state_, ref cancel );

      Initialize( callbackLoop_, topics_, _colleagueID, typeKey_ );
    }
    
    internal ColleagueListener(Guid id_, IMSNetLoop callbackLoop_, string[] topics_, string typeKey_ )
    {
      if (Log.IsLogLevelEnabled(LogLevel.Debug))
      {
        TMLogLayer.Debug.SetLocation(CLASS, ".ctor(int id_, TopicMediator mediator_, string[] topics_))").
          WriteFormat("Entering Constructor of Colleague({0}).", id_);
      }

      if (id_ == Guid.Empty)
      {
        // no Guid provided, so make your own
        id_ = Guid.NewGuid();
      }

      Initialize ( callbackLoop_, topics_, id_, typeKey_ );

      if (Log.IsLogLevelEnabled(LogLevel.Debug))
      {
        TMLogLayer.Debug.SetLocation(CLASS, ".ctor(int id_, TopicMediator mediator_, string[] topics_))").
          WriteFormat("Exiting Constructor of Colleague({0}).", id_);
      }
    }

    #endregion Constructors

    #region Finalizer

    ~ColleagueListener()
    {
      if (Log.IsLogLevelEnabled(LogLevel.Debug))
      {
        TMLogLayer.Debug.SetLocation(CLASS, "~ColleagueListener()").
          WriteFormat("Entering Finalizer to ColleagueListener({0}).", _colleagueID);
      }

      Dispose(false);

      if (Log.IsLogLevelEnabled(LogLevel.Debug))
      {
        TMLogLayer.Debug.SetLocation(CLASS, "~Colleague()").
          WriteFormat("Exiting Finalizer to Colleague({0}).", _colleagueID);
      }
    }
    #endregion Finalizer

    #region private helper methods

    private void Initialize( IMSNetLoop loop_, string[] topics_, Guid id_, string typeKey_ )
    {
      #region check params
      if (loop_ == null)
      {
        if (Log.IsLogLevelEnabled(LogLevel.Error))
        {
          TMLogLayer.Error.SetLocation(CLASS, "Initialize").WriteFormat("Loop is null");
        }
      }
      if ( id_ == Guid.Empty)
      {
        if (Log.IsLogLevelEnabled(LogLevel.Error))
        {
          TMLogLayer.Error.SetLocation(CLASS, "Initialize").WriteFormat("Guid is empty.");
        }
      }
      #endregion

      // use the provided- persisted - Guid
      _colleagueID = id_;

      _loop = loop_;

      // unregister and get rid of old ones if there are any.
      if (_instances.ContainsKey( this.ColleagueId ))
      {
        // get rid of an old one if there is one.
        IColleagueListener cl =  (IColleagueListener)_instances[this.ColleagueId];
        TopicMediator.Current.UnregisterTopics( cl );
        _instances.Remove( this.ColleagueId );
      }
      
      _publishedTopics = new ArrayList();
      _subscribedTopics = new ArrayList();
      // autoregister other topics for subscribing and publishing using type key
      if ( typeKey_ != null && typeKey_.Length > 0 )
      {
        this._publishedTopics.AddRange( EnricherTree.Instance.GetPublishedTopicsForType( typeKey_ ));
        TopicMediator.Current.RegisterTopics( this, EnricherTree.Instance.GetSubscribedTopicsForType( typeKey_ ) );
      }

      // register specific topics for subscription
      if (topics_ != null && topics_.Length > 0)
      {
        TopicMediator.Current.RegisterTopics( this, topics_ );
      }
      _instances.Add( this.ColleagueId, this );
    }
   
    private void AddTopic( ArrayList list, string topic, bool remove )
    {
      if ( !remove && !list.Contains(topic) )
      {
        list.Add(topic);
      }
      else if (remove && list.Contains( topic ))
      {
        list.Remove( topic );
      }
    }

    #endregion private helpr methods

    #region Public Properties

    public IMSNetLoop CallbackLoop
    {
      [System.Diagnostics.DebuggerStepThrough()]
      get 
      {
        return _loop;
      }
    }

    /// <summary>
    /// The arraylist of topics that this colleague has published
    /// on or will pubslih on.
    /// </summary>
    public string[] PublishedTopics 
    {
      get
      {
        string [] rtn = new string[ _publishedTopics.Count ];
        this._publishedTopics.CopyTo( rtn );
        return rtn;
      }
    }
    /// <summary>
    /// The arraylist of topics that this colleague has been
    /// subscribed to. Note that these are the topics themselves,
    /// not the handlers that respond to these topics
    /// </summary>
    public string[] SubscribedTopics 
    { 
      get
      {
        string [] rtn = new string[ _subscribedTopics.Count ];
        this._subscribedTopics.CopyTo( rtn );
        return rtn;
      }
    }

    #endregion Public Properties

    #region IDisposable implementation

    private void Dispose(bool disposing)
    {
      if (Log.IsLogLevelEnabled(LogLevel.Debug))
      {
        TMLogLayer.Debug.SetLocation(CLASS, "Dispose( disposing )").
          WriteFormat("Entering Close of Colleague({0}).", _colleagueID);
      }

      try
      {
        TopicMediator.Current.UnregisterTopics(this);
      }
      catch (Exception e)
      {
        TMLogLayer.Warning.SetLocation(CLASS, "Dispose( disposing )").
          Write("Exception: " + e.ToString());
      }

      if (disposing)
      {
        GC.SuppressFinalize(this);
      }
      
      if (Log.IsLogLevelEnabled(LogLevel.Debug))
      {
        TMLogLayer.Debug.SetLocation(CLASS, "Dispose( disposing )").
          WriteFormat("Exiting Close of Colleague({0}).", _colleagueID);
      }
    }

    void IDisposable.Dispose()
    {
      if (Log.IsLogLevelEnabled(LogLevel.Debug))
      {
        TMLogLayer.Debug.SetLocation(CLASS, "Dispose()").
          WriteFormat("Entering Dispose of Colleague({0}).", _colleagueID);
      }

      this.Dispose(true);
      
      if (Log.IsLogLevelEnabled(LogLevel.Debug))
      {
        TMLogLayer.Debug.SetLocation(CLASS, "Dispose()").
          WriteFormat("Exiting Dispose of Colleague({0}).", _colleagueID);
      }
    }

    #endregion

    #region IColleagueListener implementation

    #region Events

    public event NotifyEventHandler Notify;

    #endregion Events

    /// <summary>
    /// Called whenever this colleague has been successfully subscribed
    /// to the given topic.
    /// </summary>
    void IColleagueListener.Subscribed( string topic, bool unsubscribed )
    {
      AddTopic( _subscribedTopics, topic, unsubscribed );
    }

    /// <summary>
    /// Called whenever this colleague is associated with a publish call.
    /// </summary>
    /// <param name="topic"></param>
    void IColleagueListener.Published(string topic)
    {
      AddTopic( _publishedTopics, topic, false );
    }

    /// <summary>
    /// Add this listener as a subscriber to teh given topic
    /// </summary>
    /// <param name="topic_"></param>
    void IColleagueListener.AddTopic(string topic_)
    {
      #region check arguments 
      // can't have a null or blank topic
      if (null == topic_)
      {
        if (Log.IsLogLevelEnabled(LogLevel.Error))
        {
          TMLogLayer.Error.SetLocation(CLASS, "AddTopic(string topic_)").
            WriteFormat("Null topic_ passed to IColleaguePublisher::AddTopic.");
        }
        throw new ArgumentNullException("topic_");
      }

      string topic = topic_.Trim();
      if (0 == topic.Length)
      {
        if (Log.IsLogLevelEnabled(LogLevel.Error))
        {
          TMLogLayer.Error.SetLocation(CLASS, "AddTopic(string topic_)").
            WriteFormat("Zero length topic_ passed to IColleaguePublisher::AddTopic.");
        }
        throw new ArgumentOutOfRangeException("topic_");        
      }

      if (Log.IsLogLevelEnabled(LogLevel.Debug))
      {
        TMLogLayer.Debug.SetLocation(CLASS, "AddTopic(string topic_)").
          WriteFormat("Entering Add Topic({0}(after trim)) to Colleague({1}).", topic, _colleagueID);
      }
      #endregion

      TopicMediator.Current.RegisterTopics(this, new string[] {topic} );
      
      if (Log.IsLogLevelEnabled(LogLevel.Debug))
      {
        TMLogLayer.Debug.SetLocation(CLASS, "AddTopic(string topic_)").
          WriteFormat("Exiting Add Topic({0}(after trim)) to Colleague({1}).", topic_, _colleagueID);
      }
    }

    /// <summary>
    /// Remove this listener as a subscriber to the given topic.
    /// </summary>
    /// <param name="topic_"></param>
    void IColleagueListener.RemoveTopic(string topic_)
    {
      #region check arguments
      // can't have a null or blank topic
      if (null == topic_)
      {
        if (Log.IsLogLevelEnabled(LogLevel.Error))
        {
          TMLogLayer.Error.SetLocation(CLASS, "RemoveTopic(string topic_)").
            WriteFormat("Null topic_ passed to IColleaguePublisher::RemoveTopic.");
        }
        throw new ArgumentNullException("topic_");
      }

      string topic = topic_.Trim();
      if (0 == topic.Length)
      {
        if (Log.IsLogLevelEnabled(LogLevel.Error))
        {
          TMLogLayer.Error.SetLocation(CLASS, "RemoveTopic(string topic_)").
            WriteFormat("Zero length topic_ passed to IColleaguePublisher::RemoveTopic.");
        }
        throw new ArgumentOutOfRangeException("topic_");        
      }

      if (Log.IsLogLevelEnabled(LogLevel.Debug))
      {
        TMLogLayer.Debug.SetLocation(CLASS, "RemoveTopic(string topic_)").
          WriteFormat("Entering Remove Topic({0}(after trim)) from Colleague({1}).", topic, _colleagueID);
      }
      #endregion

      TopicMediator.Current.UnregisterTopics(this, new string[] {topic} );

      if (Log.IsLogLevelEnabled(LogLevel.Debug))
      {
        TMLogLayer.Debug.SetLocation(CLASS, "RemoveTopic(string topic_)").
          WriteFormat("Exiting Remove Topic({0}(after trim)) from Colleague({1}).", topic, _colleagueID);
      }    
    }

    
    /// <summary>
    /// Raise the Notify event
    /// </summary>
    void IColleagueListener.OnNotify(NotifyEventArgs e)
    {
      if (Log.IsLogLevelEnabled(LogLevel.Debug))
      {
        TMLogLayer.Debug.SetLocation(CLASS, "OnNotify(IColleague sender_, System.Xml.XmlDocument data_)").
          WriteFormat("Entering OnNotify. Sender: {0} Data: {1}", e.Publisher.ColleagueId.ToString(), e.XmlDocument.OuterXml);
      }

      if (null != Notify)
      {
        _loop.DispatchCallback(Notify, new object[] { e.Publisher, e });
      }

      if (Log.IsLogLevelEnabled(LogLevel.Debug))
      {
        // if we get to here, outerXml will already have been set in the previous "Debug" statement
        TMLogLayer.Debug.SetLocation(CLASS, "OnNotify(IColleague sender_, System.Xml.XmlDocument data_)").
          WriteFormat("Exiting OnNotify. Sender: {0} Data: {1}", e.Publisher.ColleagueId.ToString(), e.XmlDocument.OuterXml);
      }
    }

    #endregion 

    #region IColleague implementation

    public Guid ColleagueId
    {
      [System.Diagnostics.DebuggerStepThrough()]
      get
      {
        return _colleagueID;
      }
    }

    void Application.IColleague.SaveState( XmlNode state_, ref bool cancel_ )
    {
      if (!cancel_)
      {
        XmlNode colleagueState = state_.OwnerDocument.CreateElement( STATE_ELEMENTNAME );
        XmlAttribute colleagueAttr = state_.OwnerDocument.CreateAttribute ( STATE_COLLEAGUEID_ATTRIBUTENAME );
        colleagueAttr.Value = this.ColleagueId.ToString();
        colleagueState.Attributes.Append( colleagueAttr );
        state_.AppendChild( colleagueState );
      }
    }

    void Application.IColleague.LoadState( XmlNode state_, ref bool cancel_ )
    {
      // create a new listener from the state
      bool found = false;
      if (state_ != null)
      {
        XmlNode colleagueState = state_.SelectSingleNode( STATE_ELEMENTNAME );
        if (colleagueState != null)
        {
          XmlAttribute colleagueAttr = colleagueState.Attributes[ STATE_COLLEAGUEID_ATTRIBUTENAME ];
          if ( colleagueAttr != null)
          {
            found = true;
            _colleagueID = new Guid( colleagueAttr.Value );
          }
        }
      }
      if (!found)
      {
        if ( Log.IsLogLevelEnabled( LogLevel.Error ) )
        {
          TMLogLayer.Error.SetLocation(CLASS, "Application.IColleague.LoadState").WriteFormat("Failed to load the colleague state - missing or corrupt. Have created a new colleague instead.");
          _colleagueID = Guid.NewGuid();
        }
      }
    }

    #endregion
  }
}
