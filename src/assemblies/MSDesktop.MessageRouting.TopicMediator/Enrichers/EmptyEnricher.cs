#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2004 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.MessageRouting.TopicMediator/Enrichers/EmptyEnricher.cs#1 $
  $DateTime: 2013/12/19 08:34:48 $
    $Change: 859541 $
    $Author: istvanf $
_____________________________________________________________________________*/
#endregion File Info Header

using System;
using System.Xml;
using MorganStanley.IED.Concord.TopicMediator;

namespace MorganStanley.IED.Concord.MessageEnrichers
{
	/// <summary>
	/// Clones a given message.
	/// </summary>
	public class EmptyEnricher : IEnricher
	{
    private static EmptyEnricher _instance;
    
    private EmptyEnricher()
    {
    }
  
    static EmptyEnricher()
		{
      _instance = new EmptyEnricher();
		}

    /// <summary>
    /// Get an instance of the Enricher
    /// </summary>
    public static IEnricher Instance
    {
      get
      {
        return _instance;
      }
    }

    /// <summary>
    /// Enrich an ITopicMediatorMessage with the given topic.
    /// </summary>
    /// <param name="tm_"></param>
    /// <param name="topic_"></param>
    public void Enrich( ref ITopicMediatorMessage tm_, string topic_)
    {
      
    }

    /// <summary>
    /// Enrich an XMLDocument with the given topic.
    /// </summary>
    /// <param name="cmDoc_"></param>
    /// <param name="topic_"></param>
    public void Enrich( ref XmlDocument cmDoc_, string topic_)
    {
      
    }
	}
}
