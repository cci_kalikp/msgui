#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2004 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.MessageRouting.TopicMediator/Enrichers/CloneEnricher.cs#1 $
  $DateTime: 2013/12/19 08:34:48 $
    $Change: 859541 $
    $Author: istvanf $
_____________________________________________________________________________*/
#endregion File Info Header

using System;
using System.Xml;
using MorganStanley.IED.Concord.TopicMediator;

namespace MorganStanley.IED.Concord.MessageEnrichers
{
	/// <summary>
	/// Clones a given message.
	/// </summary>
	public class CloneEnricher : IEnricher
	{
    private static CloneEnricher _instance;

    static CloneEnricher()
    {
      _instance = new CloneEnricher();
    }

    /// <summary>
    /// Gets a reusable instance of the ClonerEnricher.
    /// </summary>
    public static CloneEnricher Instance
    {
      get
      {
        return _instance;
      }
    }

    /// <summary>
    /// Creates a ClonerEnricher. You should use the Instance under
    /// most circumstances, as there are no other flavours.
    /// </summary>
		public CloneEnricher()
		{
      // nothing to do.
		}

    /// <summary>
    /// Enrich a ITopicMediatorMessage with the given topic.
    /// </summary>
    /// <param name="tm_"></param>
    /// <param name="topic_"></param>
    public void Enrich( ref ITopicMediatorMessage tm_, string topic_)
    {
      tm_ = tm_.Clone();
    }

    /// <summary>
    /// Enrich an XMLDocument with the given topic.
    /// </summary>
    /// <param name="cmDoc_"></param>
    /// <param name="topic_"></param>
    public void Enrich( ref XmlDocument cmDoc_, string topic_)
    {
      XmlDocument doc = new XmlDocument();
      doc.LoadXml( cmDoc_.OuterXml );
      cmDoc_ = doc;
    }
	}
}
