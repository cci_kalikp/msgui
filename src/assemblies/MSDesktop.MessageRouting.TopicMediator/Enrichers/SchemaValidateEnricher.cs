#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2004 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.MessageRouting.TopicMediator/Enrichers/SchemaValidateEnricher.cs#1 $
  $DateTime: 2013/12/19 08:34:48 $
    $Change: 859541 $
    $Author: istvanf $
_____________________________________________________________________________*/
#endregion File Info Header

using System;
using System.Xml;
using System.Xml.Schema;
using MorganStanley.IED.Concord.TopicMediator;
using MorganStanley.IED.Concord.Logging;

namespace MorganStanley.IED.Concord.MessageEnrichers
{
	/// <summary>
	/// Checks all Documents/objects enriched with this object to 
	/// ensure that they are valid. This class is designed to be 
	/// overriden too if you want to create your own schemas from resource
	/// files or something. By default, no schema validation will be done
	/// if the static DoSchemaValidation property is false. You can override
	/// this behaviour with the Enabled property.
	/// </summary>
  public class SchemaValidateEnricher : IEnricher
  {
    private readonly string CLASS_NAME = "SchemaValidationEnricher";
    private static SchemaValidateEnricher _standardMessageValidateEnricher;
    private XmlSchema _schema;
    private string _schemaName;
    private static bool _doValidation;
    private int _lastErrorCount = 0;

    /// <summary>
    /// Create the standard validation schema enricher.
    /// </summary>
    static SchemaValidateEnricher()
    {
      _doValidation = false; 
      _standardMessageValidateEnricher = new SchemaValidateEnricher();
    }

    /// <summary>
    /// Gets the cached Schema validation enricher for vanilla concord messages.
    /// </summary>
    public static SchemaValidateEnricher StandardMessageValidateEnricher
    {
      [System.Diagnostics.DebuggerStepThrough()]
      get
      {
        return _standardMessageValidateEnricher;
      }
    }

    /// <summary>
    /// Indicates whether or not SchemaValidation should ever be done
    /// </summary>
    public static bool DoSchemaValidation
    {
      [System.Diagnostics.DebuggerStepThrough()]
      get
      {
        return _doValidation;
      }
      [System.Diagnostics.DebuggerStepThrough()]
      set
      {
        _doValidation = value;
      }
    }

    /// <summary>
    /// Creates a SchemaValidateEnricher. This is will create a ConcordMessage 
    /// schema validation based on the xsd file that's in the TopicMediator 
    /// resource object
    /// </summary>
    public SchemaValidateEnricher()
    {
      // default constructor - default schema.
      SchemaDescription = "Standard Message Validation";
    }

    /// <summary>
    /// An inheriting class would want to override this method - this is where the schema is 
    /// created. It is called whenever the Schema is required and it's null.
    /// </summary>
    protected virtual void InitialiseSchema()
    {
      _schema = XmlSchema.Read( new System.Xml.XmlTextReader( typeof(SchemaValidateEnricher).Assembly.GetManifestResourceStream( "MorganStanley.IED.Concord.TopicMediator.Enrichers.Resources.StandardMessage.xsd" ) ),new ValidationEventHandler ( schemaValidateHandler ) );
    }

    /// <summary>
    /// The enabled state of this SchemaValidator. Normally this is simply the same as
    /// DoSchemaValidation - but this property is overridable.
    /// Note that the Enabled setter is empty - it is a place holder so you can override if
    /// you want. 
    /// </summary>
    public virtual bool Enabled
    {
      [System.Diagnostics.DebuggerStepThrough()]
      get
      {   
        return DoSchemaValidation;
      }
      set
      {
        // do nothing
      }
    }

    /// <summary>
    /// The actual schema used to validate XmlDocuments that pass through 
    /// this enricher.
    /// </summary>
    protected virtual XmlSchema Schema
    {
      [System.Diagnostics.DebuggerStepThrough()]
      get
      {
        return _schema;
      }
      [System.Diagnostics.DebuggerStepThrough()]
      set
      {
        _schema = value;
      }
    }

    /// <summary>
    /// Accessor to the SchemaDescription - this is used so that 
    /// you can differentiate between schema validation output.
    /// When overriding this class you can access the schemaName here.
    /// </summary>
    public string SchemaDescription
    {
      [System.Diagnostics.DebuggerStepThrough()]
      get
      {
        return _schemaName;
      }
      [System.Diagnostics.DebuggerStepThrough()]
      set
      {
        _schemaName = value;
      }
    }

    /// <summary>
    /// Get the number of errors that were thrown for the last document validation.
    /// </summary>
    public int LastErrorCount
    {
      [System.Diagnostics.DebuggerStepThrough()]
      get
      {
        return _lastErrorCount;
      }
    }

    private void schemaValidateHandler (object sender, ValidationEventArgs e)
    {
      _lastErrorCount ++;
      if ( Log.IsLogLevelEnabled( LogLevel.Error ) )
      {
        Log.Error.SetLocation( CLASS_NAME, "schemaValidateHandler").WriteFormat("Failed to validate the document with schema {0}: {1}", SchemaDescription, e.Message );
      }
    }

    /// <summary>
    /// Enrich a ITopicMediatorMessage with the given topic.
    /// </summary>
    /// <param name="tm_"></param>
    /// <param name="topic_"></param>
    public virtual void Enrich( ref ITopicMediatorMessage tm_, string topic_)
    {
      // only get the document from the tm_ if the enricher is enabled.
      if (Enabled)
      {
        XmlDocument docToValidate = tm_.XmlDocument;
        Enrich( ref docToValidate, topic_ );
      }
    }

    /// <summary>
    /// This enricher will let every document past - however it will also raise errors
    /// if it finds the passed document does not match the given schema.
    /// </summary>
    /// <param name="cmDoc_"></param>
    /// <param name="topic_"></param>
    public virtual void Enrich( ref XmlDocument cmDoc_, string topic_)
    {
      // only validate if enabled.
      if (this.Enabled)
      {
        _lastErrorCount = 0;
        if ( Schema == null )
        {
          // only load the schema if necessary
          InitialiseSchema();
        }
        try
        { 
          // ensure that the passed in document is referentially distinct from the tested 
          // document.
#pragma warning disable 612,618
          XmlValidatingReader validReader = new XmlValidatingReader( new XmlTextReader( new System.IO.StringReader( cmDoc_.OuterXml ) ) );
#pragma warning restore 612,618
          // and add our schema to the validating reader
          validReader.ValidationType = ValidationType.Schema;
          validReader.Schemas.Add(_schema);
          validReader.ValidationEventHandler += new ValidationEventHandler( this.schemaValidateHandler );
          // finally try to create a document
          while(validReader.Read())
          {
            // nothing to do - we're just going through the xml.
          }
          if (LastErrorCount > 0)
          {
            // throw exception so that we can log the failure below.
            throw new XmlException("Failed to validate message", null );
          }
        }
        catch(Exception ex)
        {
          _lastErrorCount++;
          if ( Log.IsLogLevelEnabled( LogLevel.Error ) )
          {
            Log.Error.SetLocation( CLASS_NAME, "Enrich").WriteFormat("Failed to validate the document with schema {0} and topic {1}", ex, SchemaDescription, topic_ );
          }
        }
      }
    }
	}
}
