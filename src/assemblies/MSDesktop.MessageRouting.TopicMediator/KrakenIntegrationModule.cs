﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Threading;
using System.Xml;
using Microsoft.Practices.Composite.Modularity;
using MSDesktop.MessageRouting;
using MorganStanley.IED.Concord.Configuration;

namespace MorganStanley.IED.Concord.TopicMediator
{
    internal enum KrakenPublishing
    {
        None = 0,
        All = 1,
        NonLinked = 2
    }

    [Flags]
    internal enum KrakenRegistration
    {
        None = 0,
        HasSubscribers = 1,
        ForWindowLinking = 2
    }

    public class KrakenIntegrationModule : IModule
    {
        private static readonly List<Tuple<string, KrakenRegistration>> _pendingRegistrations = new List<Tuple<string, KrakenRegistration>>();
        private static TopicMediator _topicMediator;
        private static KrakenPublishing _krakenPublishing = KrakenPublishing.None;

        private readonly IRoutingCommunicator _communicator;

        

        public KrakenIntegrationModule(IRoutingCommunicator communicator)
        {
            _communicator = communicator;
        }

        public void Initialize()
        {
            var krakenEnabled = false;
            var config = (Configurator)ConfigurationManager.GetConfig("TopicMediator");
            if (config != null)
            {
                var krakenElement = (XmlElement)config.GetNode("Kraken", null);
                if (krakenElement != null)
                {
                    krakenEnabled = true;

                    var attribute = krakenElement.GetAttribute("publish");
                    if (!string.IsNullOrWhiteSpace(attribute))
                    {
                        if (!Enum.TryParse(attribute, true, out _krakenPublishing))
                        {
                            throw new InvalidOperationException("'publish' attribute of Kraken node must be 'None' or 'All' or 'NonLinked'");
                        }
                    }
                }
            }

            if (krakenEnabled)
            {
                _topicMediator = TopicMediator.Current;
                TopicMediator.Current.RegisterKraken(_communicator, Dispatcher.CurrentDispatcher);

                foreach (var pair in _pendingRegistrations)
                {
                    RegisterOnKraken(pair.Item1, pair.Item2);
                }
                _pendingRegistrations.Clear();
            }
        }

        internal static void RegisterOnKraken(string topic, KrakenRegistration krakenRegistration)
        {
            if (_topicMediator == null)
            {
                _pendingRegistrations.Add(Tuple.Create(topic, krakenRegistration));
                return;
            }

            var willPublish = _krakenPublishing == KrakenPublishing.All || (_krakenPublishing == KrakenPublishing.NonLinked && !krakenRegistration.HasFlag(KrakenRegistration.ForWindowLinking));
            
            _topicMediator.RegisterTopicOnKraken(topic, willPublish, krakenRegistration.HasFlag(KrakenRegistration.HasSubscribers));
        }
    }
}
