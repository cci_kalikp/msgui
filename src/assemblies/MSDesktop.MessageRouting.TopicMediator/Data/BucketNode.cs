#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.MessageRouting.TopicMediator/Data/BucketNode.cs#1 $
  $DateTime: 2013/12/19 08:34:48 $
    $Change: 859541 $
    $Author: istvanf $
_____________________________________________________________________________*/
#endregion

#region MorganStanley.IED.Concord.TopicMediator.Data Namespace
namespace MorganStanley.IED.Concord.TopicMediator.Data
{
  #region using
  using System;
  using System.Text;
  #endregion using

  #region BucketNode Class
  /// <summary>
  /// Summary description for BucketNode.
  /// </summary>
  internal class BucketNode
  {

    #region Constants
    #endregion Constants

    #region Enums
    #endregion Enums

    #region Events
    #endregion Events

    #region Private Declarations
    private string                                          _key;
    private BucketNodeHashtable                             _buckets;
    private BucketNode                                      _parent;
    private ColleagueCollection                             _colleagues;
    #endregion Private Declarations

    #region Protected Declarations
    #endregion Protected Declarations

    #region Constructors
    public BucketNode(string key_, BucketNode parent_)
    {
      _key = key_;
      _buckets = new BucketNodeHashtable();
      _colleagues = new ColleagueCollection();
      _parent = parent_;
    }
    #endregion Constructors

    #region Public Properties
    public string Key
    {
      get {return _key;}
    }

    public BucketNodeHashtable Buckets
    {
      get {return _buckets;}
    }

    public ColleagueCollection Colleagues
    {
      get {return _colleagues;}
    }

    public BucketNode Parent
    {
      get {return _parent;}
    }
    #endregion Public Properties

    #region Public Methods
    public override bool Equals(object obj_)
    {
      BucketNode bn = obj_ as BucketNode;
      if (null == bn)
      {   
        return false;
      }

      return (bn._key == _key);
    }

    public override int GetHashCode()
    {
      return _key.GetHashCode();;
    }

    public override string ToString()
    {
      StringBuilder sb = new StringBuilder();

      GetParentKey(this, sb);

      return sb.ToString(0, sb.Length - 1);
    }

    // walk up tree getting parents.
    void GetParentKey(BucketNode bn_, StringBuilder sb_)
    {
      if (null != bn_.Parent)
      {
        GetParentKey(bn_.Parent, sb_);
      }
      sb_.AppendFormat("{0}.", _key);
    }
    #endregion Public Methods

    #region Protected Methods
    #endregion Protected Methods

    #region Private Helper Methods
    #endregion Private Helper Methods

	}
  #endregion BucketNode Class
}
#endregion MorganStanley.IED.Concord.TopicMediator.Data Namespace
