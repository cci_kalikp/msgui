#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2004 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.MessageRouting.TopicMediator/EnricherStructures/TopicHandler.cs#1 $
  $DateTime: 2013/12/19 08:34:48 $
    $Change: 859541 $
    $Author: istvanf $
_____________________________________________________________________________*/
#endregion File Info Header

namespace MorganStanley.IED.Concord.TopicMediator
{
  using System;
  using System.Collections;
  using Application;
  
  internal class TopicHandlerCollection : ICollection, IEnumerable
  {
    private Hashtable _table;

    internal TopicHandlerCollection()
    {
      _table = new Hashtable();
    }

    internal TopicHandler this[ string handlerTopic ]
    {
      get
      {
        return (TopicHandler)_table[ handlerTopic ];
      }
    }

    internal void Add( string name, TopicHandler handler )
    {
      _table.Add( name, handler );
    }

    internal void Remove ( string handlerName )
    {
      if (handlerName != null)
      {
        _table.Remove( handlerName );
      }
    }
    
    #region IEnumerable

    IEnumerator IEnumerable.GetEnumerator()
    {
      return _table.Values.GetEnumerator();
    }

    #endregion

    #region ICollection

    void ICollection.CopyTo( Array array, int arrayIndex )
    {
      _table.CopyTo( array, arrayIndex );
    }

    int ICollection.Count
    {
      get
      {
        return _table.Count;
      }
    }

    internal int Count
    {
      get
      {
        return _table.Count;
      }
    }

    bool ICollection.IsSynchronized
    {
      get
      {
        return _table.IsSynchronized;
      }
    }

    object ICollection.SyncRoot
    {
      get
      {
        return _table.SyncRoot;
      }
    }

    #endregion

  }

  /// <summary>
  /// Summary description for TopicHandler.
  /// </summary>
  internal class TopicHandler
  {
    private Hashtable _publishers;
    private TopicSubscriberCollection _subscribers;
    private EnricherChain _enricherChain;

    /// <summary>
    /// Creates a new TopicHandler with a common 
    /// handler chain
    /// </summary>
    internal TopicHandler( EnricherChain chain_ )
    {
      _publishers = new Hashtable();
      _subscribers = new TopicSubscriberCollection();
      _enricherChain = chain_;
      // add the mandatory standard validation enricher
      _enricherChain.Append( new MessageEnrichers.IEnricher[] { MessageEnrichers.SchemaValidateEnricher.StandardMessageValidateEnricher } );
    }

    /// <summary>
    /// Gets the publishing topics that this topic Handler
    /// would be called for
    /// </summary>
    internal Hashtable Publishers
    {
      get
      {
        return _publishers;
      }
    }

    /// <summary>
    /// Gets the TopicSubscribers for this TopicHandler
    /// </summary>
    internal TopicSubscriberCollection Subscribers
    {
      [System.Diagnostics.DebuggerStepThrough()]
      get
      {
        return _subscribers;
      }
    }

    /// <summary>
    /// Gets the enricherchain for this topic handler
    /// </summary>
    internal EnricherChain EnricherChain
    {
      [System.Diagnostics.DebuggerStepThrough()]
      get
      {
        return _enricherChain;
      }
    }

    private string GetPublisherTopicForList( string[] publisherTopics_ )
    {
      //find the fisrt specific topic that was published to set off this topic handler
      string topicToPublishOn = null;
      foreach( string topic in publisherTopics_ )
      {
        if (_publishers.ContainsKey(topic))
        {
          topicToPublishOn = topic;
          break;
        }
      }
      return topicToPublishOn;
    }
    
    /// <summary>
    /// Delegate used by topicMediator to raise the PrePublish events
    /// </summary>
    internal delegate bool RaisePrePublishEventDelegate( IColleague sender_, IColleagueListener subscriber_, string publisherTopicName_, string commonTopicName_, string subscriptionTopicName_ );

    /// <summary>
    /// Actually publish the given ITopicMediatorMessage.
    /// </summary>
    internal void Publish( ITopicMediatorMessage message_, System.Xml.XmlDocument document_, string[] publisherTopics_, IColleague sender_, RaisePrePublishEventDelegate delegate_ )
    {
      //find the fisrt specific topic that was published to set off this topic handler
      string topicToPublishOn = GetPublisherTopicForList( publisherTopics_ );
        
      if (topicToPublishOn != null)
      {
        EnricherChain publisherChain = ((EnricherChain)_publishers[topicToPublishOn]);
        bool enrichedCommon = false;
        foreach( TopicSubscriber subscriber in this.Subscribers )
        {
          bool enriched = false;
          foreach( IColleagueListener listener in subscriber.Listeners )
          {
            bool cancel = delegate_( sender_, listener, topicToPublishOn, this._enricherChain.OutputTopicName, subscriber.EnricherChain.OutputTopicName );
            if (!cancel)
            {
              if (!enrichedCommon)
              { 
                // only enrich common once, and only if we need to actually publish the message
                if (message_ != null)
                {
                  publisherChain.Enrich( ref message_, publisherChain.InputTopicName );
                  EnricherChain.Enrich( ref message_, this._enricherChain.OutputTopicName );
                }
                else
                {
                  publisherChain.Enrich( ref document_, publisherChain.InputTopicName );
                  EnricherChain.Enrich( ref document_, this._enricherChain.OutputTopicName );
                }
                enrichedCommon = true;
              }

              if (message_ != null)
              {
                // only enrich once for this subscribtion colelction, and only if we need to publish the message
                if (!enriched) 
                {
                  subscriber.EnricherChain.Enrich( ref message_, subscriber.EnricherChain.OutputTopicName );
                }
                listener.OnNotify( new NotifyEventArgs( message_, NotifyMessageCarrier.ITopicMediatorMessage, publisherTopics_, subscriber.EnricherChain.OutputTopicName, listener, sender_ ) );       
              }
              else
              {
                // only enrich once for this subscribtion colelction, and only if we need to publish the message
                if (!enriched) 
                {
                  subscriber.EnricherChain.Enrich( ref document_, subscriber.EnricherChain.OutputTopicName );
                }
                listener.OnNotify( new NotifyEventArgs( document_, NotifyMessageCarrier.XmlDocument, publisherTopics_, subscriber.EnricherChain.OutputTopicName, listener, sender_ ) );
              }
              enriched = true;
            }
          }
        }
      }
    }

  }
}