#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2004 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.MessageRouting.TopicMediator/EnricherStructures/TopicSubscriber.cs#1 $
  $DateTime: 2013/12/19 08:34:48 $
    $Change: 859541 $
    $Author: istvanf $
_____________________________________________________________________________*/
#endregion File Info Header

using System;
using System.Collections;

namespace MorganStanley.IED.Concord.TopicMediator
{
  using Application;

  internal class TopicSubscriberCollection : IEnumerable, ICollection
  {
    private Hashtable _table;

    internal TopicSubscriberCollection()
    {
      _table = new Hashtable();
    }

    #region IEnumerable

    IEnumerator IEnumerable.GetEnumerator()
    {
      return _table.Values.GetEnumerator();
    }

    #endregion

    #region ICollection

    void ICollection.CopyTo( Array array, int arrayIndex )
    {
      _table.CopyTo( array, arrayIndex );
    }

    int ICollection.Count
    {
      get
      {
        return _table.Count;
      }
    }

    internal int Count
    {
      get
      {
        return _table.Count;
      }
    }

    bool ICollection.IsSynchronized
    {
      get
      {
        return _table.IsSynchronized;
      }
    }

    object ICollection.SyncRoot
    {
      get
      {
        return _table.SyncRoot;
      }
    }

    #endregion

    internal TopicSubscriber this[ string subscriberTopic ]
    {
      [System.Diagnostics.DebuggerStepThrough()]
      get
      {
        return (TopicSubscriber)_table[ subscriberTopic ];
      }
    }

    private void Add( string subscriberTopic, TopicSubscriber topicSubscriber )
    {
      _table.Add( subscriberTopic, topicSubscriber );
    }

    internal void Add( string subscriberTopic, EnricherChain topicSubscriberChain )
    {
      TopicSubscriber ts = new TopicSubscriber( topicSubscriberChain );
      _table.Add( subscriberTopic, ts );
    }
  }

  /// <summary>
  /// Summary description for TopicHandler.
  /// </summary>
  internal class TopicSubscriber
  {
    private ArrayList _listeners;
    private EnricherChain _enricherChain;

    internal TopicSubscriber( EnricherChain chain_ )
    {
      _listeners = new ArrayList();
      _enricherChain = chain_;
    }

    /// <summary>
    /// Gets the Subscribing colleagues for this topic
    /// </summary>
    internal ArrayList Listeners
    {
      [System.Diagnostics.DebuggerStepThrough()]
      get
      {
        return _listeners;
      }
    }

    /// <summary>
    /// Checks to see if the ColleagueID represented by this
    /// colleague matches that of any of the listeners in this
    /// collection.
    /// Will return false of colleague is null.
    /// </summary>
    internal bool ContainsColleague( IColleague colleague )
    {
      if (colleague == null)
      {
        return false;
      }
      foreach( IColleague col in _listeners)
      {
        if (col.ColleagueId == colleague.ColleagueId )
        {
          return true;
        }
      }
      return false;
    }

    /// <summary>
    /// Gets the enricher chain for this topic
    /// </summary>
    internal EnricherChain EnricherChain
    {
      [System.Diagnostics.DebuggerStepThrough()]
      get
      {
        return _enricherChain;
      }
    }
  }
}
