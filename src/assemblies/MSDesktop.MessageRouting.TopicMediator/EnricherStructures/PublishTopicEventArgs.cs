#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2004 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.MessageRouting.TopicMediator/EnricherStructures/PublishTopicEventArgs.cs#1 $
  $DateTime: 2013/12/19 08:34:48 $
    $Change: 859541 $
    $Author: istvanf $
_____________________________________________________________________________*/
#endregion File Info Header

using System;

namespace MorganStanley.IED.Concord.TopicMediator
{
  using Concord.Application;
  /// <summary>
  /// Delegate for the PrePublish event, as called by TopicMediator
  /// </summary>
  public delegate void PublishTopicEventHandler (object sender, PublishTopicEventArgs e );

  /// <summary>
  /// Event Args for the pre-publish event that allows
  /// a linker (or other listener) to cancel the publish
  /// event if required. 
  /// 
  /// We are interested in the values of this class so they
  /// must be propogated and so it cannot be a struct
  /// </summary>
  public class PublishTopicEventArgs
  {
    private IColleague _sender;
    private string _commonTopicName;

    /// <summary>
    /// PublishTopic event is used to nofify listeners that we are about to start
    /// publishing on a topic, or that we have completed publishing on a topic.
    /// </summary>
    public PublishTopicEventArgs( IColleague sender_, string commonTopicName_ )
    {
      _sender = sender_;
      _commonTopicName = commonTopicName_;
    }

    #region properties

    /// <summary>
    /// The colleague sending the message
    /// </summary>
    public IColleague Sender
    {
      [System.Diagnostics.DebuggerStepThrough()]
      get
      {
        return _sender;
      }
    }

    /// <summary>
    /// The topic name that the TopicMediator transformed
    /// the sender's topic into.
    /// </summary>
    public string CommonTopicName
    {
      [System.Diagnostics.DebuggerStepThrough()]
      get
      {
        return _commonTopicName;
      }
    }
    #endregion
  }
}
