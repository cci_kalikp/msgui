#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2004 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.MessageRouting.TopicMediator/EnricherStructures/EnricherChain.cs#1 $
  $DateTime: 2013/12/19 08:34:48 $
    $Change: 859541 $
    $Author: istvanf $
_____________________________________________________________________________*/
#endregion File Info Header

using System;
using System.Xml;
using System.Collections;
using MorganStanley.IED.Concord.MessageEnrichers;

namespace MorganStanley.IED.Concord.TopicMediator
{
	/// <summary>
	/// Contains all the information required for one enricher node in the enricher tree
	/// </summary>
	internal class EnricherChain : IEnricher
	{
    private EnricherChain _child;
    private IEnricher _enricher;
    private string _outputTopicName = null;
    private string _inputTopicName = null;


    #region internal configuration constructor

    /// <summary>
    /// Used to create an enricher list from config
    /// </summary>
    internal EnricherChain( XmlNodeList nodes_, string inputTopicName_, string outputTopicName_, Hashtable enrichers_, bool insertCloner_ )
    {
      IEnricher[] enrichers;
      if (nodes_ == null || nodes_.Count == 0)
      {
        enrichers = new IEnricher[0];
      }
      else
      {
        enrichers = new IEnricher[ nodes_.Count ];
        for (int i=0; i<nodes_.Count; i++)
        {
          XmlNode node = nodes_[ i ];
          enrichers[i] = (IEnricher)enrichers_[ node.InnerText ]; 
        }        
      }
      this.Initialise( 0, enrichers, inputTopicName_, outputTopicName_, insertCloner_ );
    }

    #endregion

    #region internal programatic constructors

    /// <summary>
    /// Constructor used to programatically create an enricher chain
    /// </summary>
    internal EnricherChain( IEnricher[] enrichers_, string inputTopicName_, string outputTopicName_, bool insertCloner_ ) : this (0, enrichers_, inputTopicName_, outputTopicName_, insertCloner_)
    {
    }

    #endregion

    #region private constructor and initialisation

    /// <summary>
    /// private constructor for programatic construction
    /// </summary>
    private EnricherChain( int index_, IEnricher[] enrichers_, string inputTopicName_, string outputTopicName_, bool insertCloner_ )
    {
      this.Initialise( index_, enrichers_, inputTopicName_, outputTopicName_, insertCloner_ );
    }

    /// <summary>
    /// Initialise the chain - all constructors call this method
    /// </summary>
    private void Initialise(int index_, IEnricher[] enrichers_, string inputTopicName_, string outputTopicName_, bool insertCloner_ ) 
    {
      _outputTopicName = outputTopicName_;
      _inputTopicName = inputTopicName_;

      // we might need to insert an enricher at the start of the chain:
      if (insertCloner_)
      {
        // insert the Cloner enricher - by not incrementing the 
        // index, the next link in the chain will be enricher[0]
        _enricher = CloneEnricher.Instance;
      }      
      else if  (enrichers_ == null || enrichers_.Length == 0)
      {
        // if there are no entries in the chain, we need to add an empty one
        _enricher = EmptyEnricher.Instance;
      }
      else
      {
        // this is the normal case: take the next entry in the 
        // array of enrichers
        _enricher = enrichers_[ index_ ];
        index_ ++;
      }

      // if there are any more enrichers in the chain, add the next 
      // link. Note that the index will have been incremented by now if
      // it needed to be.
      if (enrichers_ != null && enrichers_.Length > index_)
      {
        _child = new EnricherChain( index_, enrichers_, inputTopicName_, outputTopicName_, false );
      }      
    }
    
    #endregion

    #region implementation of IEnricher interface

    /// <summary>
    /// IEnricher implementation: calls the chain consecutively with each enricher
    /// </summary>
    public void Enrich(ref ITopicMediatorMessage tm_, string topic_)
    {
      _enricher.Enrich( ref tm_, topic_ );
      // then enrich down the chain if necessary
      if (_child != null)
      {
        _child.Enrich( ref tm_, topic_);
      }
    }

    /// <summary>
    /// IEnricher implementation: calls the chain consecutively with each enricher
    /// </summary>
    public void Enrich(ref XmlDocument cmDoc_, string topic_)
    {
      _enricher.Enrich( ref cmDoc_, topic_ );
      // then enrich down the tree if necessary
      if (_child != null)
      {
        _child.Enrich( ref cmDoc_, topic_);
      }
    }

    #endregion

    #region properties 

    private EnricherChain LastChild
    {
      [System.Diagnostics.DebuggerStepThrough()]
      get
      {
        if (this._child == null)
        {
          return this;
        }
        return _child.LastChild;
      }
    }

    /// <summary>
    /// Gets the output topic name for this enricher chain
    /// </summary>
    internal string OutputTopicName
    {
      [System.Diagnostics.DebuggerStepThrough()]
      get
      {
        return _outputTopicName;
      }
    }

    /// <summary>
    /// Gets the input topic name for this enricher chain
    /// </summary>
    internal string InputTopicName
    {
      [System.Diagnostics.DebuggerStepThrough()]
      get
      {
        return _inputTopicName;
      }
    }

    #endregion

    #region other internal methods

    /// <summary>
    /// Append the given array of enrichers to the end of any existing
    /// enricher chain.
    /// </summary>
    /// <param name="toAppend_"></param>
    internal void Append( IEnricher[] toAppend_ )
    {
      if (toAppend_ != null && toAppend_.Length > 0)
      {
        LastChild._child = new EnricherChain( 0, toAppend_, _inputTopicName, _outputTopicName, false );
      }
    }

    #endregion

    #region public methods
    public override string ToString()
    {
      string rtn = _enricher.ToString();
      // then enrich down the chain if necessary
      if (_child != null)
      {
        return rtn + "; " + _child.ToString();
      }
      return rtn;
    }
    #endregion public methods
	}
}
