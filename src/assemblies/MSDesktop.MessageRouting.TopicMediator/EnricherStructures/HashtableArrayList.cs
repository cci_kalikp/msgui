#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2004 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.MessageRouting.TopicMediator/EnricherStructures/HashtableArrayList.cs#1 $
  $DateTime: 2013/12/19 08:34:48 $
    $Change: 859541 $
    $Author: istvanf $
_____________________________________________________________________________*/
#endregion File Info Header

using System;
using System.Collections;

namespace MorganStanley.IED.Concord.TopicMediator
{
	/// <summary>
  /// A class designed to hold arrays of objects
  /// for individual keys
	/// </summary>
	internal class HashtableArrayList
	{
    protected Hashtable _hashtable;
  
    /// <summary>
    /// A class designed to hold arrays of objects
    /// for individual keys
    /// </summary>
    internal HashtableArrayList()
		{
			_hashtable = new Hashtable();
		}
    
    /// <summary>
    /// Returns the arrayList found for 
    /// this Key
    /// </summary>
    internal ArrayList this[ object key ]
    {
      get
      {
        return (ArrayList) _hashtable[ key ];
      }
    }  

    /// <summary>
    /// Checks to see if the arrayEntry is stored in 
    /// an arraylist for the given key. Also false if the
    /// given key is not found.
    /// </summary>
    internal bool Contains( object key, object arrayEntry )
    {
      if ( this[ key ] == null )
      {
        return false;
      }
      else
      {
        return this[ key ].Contains( arrayEntry );
      }
    }

    /// <summary>
    /// Adds an arrayEntry into the array for the 
    /// key object. Creates the array at Key if 
    /// necessary
    /// </summary>
    internal void Add( object key, object arrayEntry )
    {
      ArrayList list = this[ key ];
      if (list == null)
      {
        _hashtable.Add( key, new ArrayList() );
        Add(key, arrayEntry );
        return;
      }
      list.Add( arrayEntry );
    }

    /// <summary>
    /// Safely removes the arrayEntry from the arraylist 
    /// stored at the key. 
    /// </summary>
    internal virtual void RemoveArrayEntry( object key, object arrayEntry )
    {
      ArrayList al = this[key];
      if (al != null)
      {
        al.Remove( arrayEntry );
      }
    }

    /// <summary>
    /// Completely removes any arraylist stored for
    /// the given key.
    /// </summary>
    /// <param name="key"></param>
    internal virtual void RemoveArray( object key )
    {
      _hashtable.Remove( key );
    }
	}
}
