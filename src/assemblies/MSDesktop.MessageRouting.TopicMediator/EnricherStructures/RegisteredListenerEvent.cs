#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2004 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.MessageRouting.TopicMediator/EnricherStructures/RegisteredListenerEvent.cs#1 $
  $DateTime: 2013/12/19 08:34:48 $
    $Change: 859541 $
    $Author: istvanf $
_____________________________________________________________________________*/
#endregion File Info Header

using System;

namespace MorganStanley.IED.Concord.TopicMediator
{
  /// <summary>
  /// Delegate for the RegisterListener event used to indicate that a listener
  /// has been attached or detatched to a particular topic
  /// </summary> 
  public delegate void RegisteredListenerEventHandler (object sender, RegisteredListenerEventArgs e );

  /// <summary>
  /// Class containing the listener and topic just subscribed to in the 
  /// TopicMediator instance.
  /// </summary>
  public class RegisteredListenerEventArgs
  {
    private IColleagueListener _listener;
    private string _topicName;

    /// <summary>
    /// Create an instance of the RegisteredListenerEventArgs class.
    /// </summary>
    /// <param name="topic_"></param>
    /// <param name="listener_"></param>
    public RegisteredListenerEventArgs( string topic_, IColleagueListener listener_ )
    {
      _topicName = topic_;
      _listener = listener_;
    }

    #region properties
    /// <summary>
    /// Get the listener that has been subscribed to the topic
    /// </summary>
    public IColleagueListener Listener
    {
      [System.Diagnostics.DebuggerStepThrough()]  
      get
      {
        return _listener;
      }
    }
    /// <summary>
    /// Get the topic name that this listener has been subscribed to
    /// </summary>
    public string TopicName
    {
      [System.Diagnostics.DebuggerStepThrough()]
      get
      {
        return _topicName;
      }
    }
  #endregion
  }
}
