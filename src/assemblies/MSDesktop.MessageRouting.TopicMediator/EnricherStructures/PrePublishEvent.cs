#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2004 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.MessageRouting.TopicMediator/EnricherStructures/PrePublishEvent.cs#1 $
  $DateTime: 2013/12/19 08:34:48 $
    $Change: 859541 $
    $Author: istvanf $
_____________________________________________________________________________*/
#endregion File Info Header

using System;

namespace MorganStanley.IED.Concord.TopicMediator
{
  using MorganStanley.IED.Concord.Application;
  /// <summary>
  /// Delegate for the PrePublish event, as called by TopicMediator
  /// </summary>
  public delegate void PrePublishEventHandler (object sender, PrePublishEventArgs e );

  /// <summary>
  /// Event Args for the pre-publish event that allows
  /// a linker (or other listener) to cancel the publish
  /// event if required. 
  /// 
  /// We are interested in the values of this class so they
  /// must be propogated and so it cannot be a struct
  /// </summary>
  public class PrePublishEventArgs
  {
    private bool _cancel;
    private IColleague _sender;
    private IColleague _subscriber;
    private string _publisherTopicName;
    private string _commonTopicName;
    private string _subscriberTopicName;

    /// <summary>
    /// PrePublish event args to notify listeners of the intention to publish
    /// a message from the sender to the receiver on the specified topics.
    /// </summary>
    /// <param name="sender_"></param>
    /// <param name="subscriber_"></param>
    /// <param name="publisherTopicName_"></param>
    /// <param name="commonTopicName_"></param>
    /// <param name="subscriberTopicName_"></param>
    public PrePublishEventArgs(IColleague sender_, IColleague subscriber_, string publisherTopicName_, string commonTopicName_, string subscriberTopicName_ )
    {
      _cancel = false;
      _sender = sender_;
      _subscriber = subscriber_;
      _publisherTopicName = publisherTopicName_;
      _commonTopicName = commonTopicName_;
      _subscriberTopicName = subscriberTopicName_;
    }

    #region properties

    /// <summary>
    /// Flag indicating whether or not to send the message
    /// from the sender to the subscriber. 
    /// </summary>
    public bool Cancel
    {
      [System.Diagnostics.DebuggerStepThrough()]
      get
      {
        return _cancel;
      }
      [System.Diagnostics.DebuggerStepThrough()]
      set
      {
        _cancel = value;
      }
    }
    
    /// <summary>
    /// The colleague sending the message
    /// </summary>
    public IColleague Sender
    {
      [System.Diagnostics.DebuggerStepThrough()]
      get
      {
        return _sender;
      }
    }

    /// <summary>
    /// The colleague that will receive the message
    /// if the Cancel flag remains false.
    /// </summary>
    public IColleague Subscriber
    {
      [System.Diagnostics.DebuggerStepThrough()]
      get
      {
        return _subscriber;
      }
    }

    /// <summary>
    /// The topic name that the sender used
    /// </summary>
    public string PublisherTopicName
    {
      [System.Diagnostics.DebuggerStepThrough()]
      get
      {
        return _publisherTopicName;
      }
    }

    /// <summary>
    /// The topic name that the TopicMediator transformed
    /// the sender's topic into.
    /// </summary>
    public string CommonTopicName
    {
      [System.Diagnostics.DebuggerStepThrough()]
      get
      {
        return _commonTopicName;
      }
    }

    /// <summary>
    /// The topic name that this subscriber had subscribed
    /// to in order to receive this message.
    /// </summary>
    public string SubscriberTopicName
    {
      [System.Diagnostics.DebuggerStepThrough()]
      get
      {
        return _subscriberTopicName;
      }
    }  
    #endregion
  }
}
