#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2004 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.MessageRouting.TopicMediator/EnricherStructures/EnricherTree.cs#2 $
  $DateTime: 2013/12/23 14:19:42 $
    $Change: 859913 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion File Info Header

using System;
using System.Linq;
using System.Xml;
using System.Collections;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using MorganStanley.IED.Concord.Configuration;
using MorganStanley.IED.Concord.MessageEnrichers;
using MorganStanley.IED.Concord.Logging;
using System.Text;

namespace MorganStanley.IED.Concord.TopicMediator
{
	/// <summary>
	/// Singleton pattern.
	/// 
	/// Parses and the config for mapping and enriching published 
	/// topics to subscriber topics.
	/// 
	/// This works in three stages:
	/// One publisher emits a message: this may have enrichers.
	/// Each publisher's messages go through the common enrichers.
	/// Each subscriber to that common message may have enrichers. If so, clone.
	/// </summary>
  internal class EnricherTree
  {

    #region constants

    private const string CLASS_NAME = "EnricherTree";
    private const string CONFIG_ROOTNAME = "TopicMediator";
    private const string ELEMENTNAME_ENRICHER_DEFS = "EnricherDefs";
    private const string ELEMENTNAME_TOPIC_HANDLERS = "TopicHandlers";
    private const string ELEMENTNAME_PUBLISHER = "Publishers";
    private const string ELEMENTNAME_COMMON = "Common";
    private const string ELEMENTNAME_CONSTRUCTOR = "Constructor";
    private const string ELEMENTNAME_TYPE = "Type";
    private const string ELEMENTNAME_SUBSCRIBER = "Subscribers";
    private const string ATTRIBUTENAME_CLONE = "clone";
    private const string ATTRIBUTENAME_NAME = "name";
    private const string ATTRIBUTENAME_TYPE = "type";
    private const string ATTRIBUTEVALUE_YES = "yes";
    private const string ATTRIBUTEVALUE_TRUE = "true";
    private const string ATTRIBUTEVALUE_NO = "no";
    private const string ATTRIBUTEVALUE_FALSE = "false";
    private const string ATTRIBUTENAME_DOSCHEMAVALIDATION = "@doschemavalidation";
    private const string CHAINKEY_SEPARATOR = ">>";

    #endregion

    #region declarations

    private static EnricherTree _instance;

    private object _lock = new object();
    private TopicHandlerCollection _topicHandlers = new TopicHandlerCollection();
    private HashtableArrayList _handlersForPublishers = new HashtableArrayList();
    private HashtableArrayList _handlersForSubscribers = new HashtableArrayList();
    private HashtableArrayList _publishedTopicsForTypes = new HashtableArrayList();
    private HashtableArrayList _subscribedTopicsForTypes = new HashtableArrayList();

    private bool _properlyInitialised = false;

    #endregion

    #region Enums

    /// <summary>
    /// Used to asses what type of checking we should be doing on a particular
    /// topic name in a method
    /// </summary>
    private enum CheckType
    {
      Exists,
      ExistsAndUnique,
      DoesNotExist,
      NoCheck
    }

    /// <summary>
    /// Enum to dictate the section of an enricher chain we are dealing with
    /// </summary>
    internal enum ChainSection
    {
      Publisher,
      Common,
      Subscriber,
      Invalid
    }

    #endregion

    #region singleton implementation

    /// <summary>
    /// private constructor for singleton implementation
    /// </summary>
    private EnricherTree()
    {
      Initialise();
    }

    /// <summary>
    /// Static constructor to initialse the singleton
    /// </summary>
    static EnricherTree()
    {
      _instance = new EnricherTree();
    }

    #endregion

    #region internal static properties
    
    /// <summary>
    /// Get the singleton instance of this class
    /// </summary>
    internal static EnricherTree Instance
    {
      [System.Diagnostics.DebuggerStepThrough()]
      get
      {
        return _instance;
      }
    }

    /// <summary>
    /// Ensure that the EnricherTree was initialised correctly
    /// </summary>
    [System.Diagnostics.DebuggerStepThrough()]
    internal static bool CheckInitialised()
    {
      return _instance._properlyInitialised;
    }

    #endregion

    #region Initialising

    /// <summary>
    /// Parse the config (if there is any) and populate the enricher tree hashtable
    /// </summary>
    private void Initialise()
    {
      _properlyInitialised = false;
      Configurator config = (Configurator) ConfigurationManager.GetConfig(CONFIG_ROOTNAME);
      if (config != null)
      { 
        // check the DoValidation attribute
        string svValue = config.GetValue(ATTRIBUTENAME_DOSCHEMAVALIDATION, null);
        SchemaValidateEnricher.DoSchemaValidation = true; // default to ON
        if (svValue != null && svValue.Trim().Length > 0)
        {
          switch ( svValue.Trim().ToLower() )
          {
            case ATTRIBUTEVALUE_FALSE:
            case ATTRIBUTEVALUE_NO:
              SchemaValidateEnricher.DoSchemaValidation = false;
              break;
              // in all other cases leave this as true.
          }
        }

        // warn if validation is on - as this is a performance hit.
        if (SchemaValidateEnricher.DoSchemaValidation)
        {
          TMLogLayer.Emergency.SetLocation(CLASS_NAME, "Initialize").Write( "NOTE: SCHEMA VALIDATION IS ON FOR TOPICMEDIATOR. THIS IS SLOW. ENSURE THAT SCHEMA VALIDATION IS OFF IN PRODUCTION SYSTEMS. ADD THE ATTRIBUTE doschemavalidation=\"False\" to the TopicMediator element in TopicMediator config." );
        }

        // now get the enrichers.
        Hashtable enrichers = new Hashtable();
        try
        {
          // get the enrichers
          PopulateEnrichers( config.GetNode(ELEMENTNAME_ENRICHER_DEFS, null), enrichers );
        }
        catch (Exception e)
        {
          if (Log.IsLogLevelEnabled(LogLevel.Error))
          {
            TMLogLayer.Error.SetLocation(CLASS_NAME, "Initialize").Write( "Failure to parse enrichers ", e );
          }
        }

        try
        {
          // populate the enricher chains
          PopulateChains( config.GetNode(ELEMENTNAME_TOPIC_HANDLERS, null), enrichers );
        }
        catch (Exception e)
        {
          if (Log.IsLogLevelEnabled(LogLevel.Error))
          {
            TMLogLayer.Error.SetLocation(CLASS_NAME, "Initialize").Write( "Failed to parse the chains properly", e);
          }
        }
      }
      _properlyInitialised = true;
      if (Log.IsLogLevelEnabled(LogLevel.Info))
      {
        TMLogLayer.Info.SetLocation(CLASS_NAME, "Initialize").Write( "Completed EnricherTree initialization.");
      }      
    }

    /// <summary>
    /// Processes the actual node containing all the enricher definitions
    /// </summary>
    /// <exception cref="ConfiguratorException">if this configuration file is not correctly parsed.</exception>
    private void PopulateEnrichers( XmlNode node, Hashtable enrichers_ )
    {
      if (node == null)
      {
        if ( Log.IsLogLevelEnabled( LogLevel.Info ) )
        {
          TMLogLayer.Info.SetLocation(CLASS_NAME, "PopulateEnrichers").Write("No enricher-defs node found.");
        }
      }
      else
      {
        // treat every section separately:
        foreach(XmlNode defNode in node.ChildNodes)
        {
          string name = GetAttributeValue(defNode, ATTRIBUTENAME_NAME);   // exits with common topic name
          XmlNode typeNode = defNode.SelectSingleNode(ELEMENTNAME_TYPE, null);
          string typeString = GetAttributeValue(typeNode, ATTRIBUTENAME_NAME);
          XmlNode ctorNode = defNode.SelectSingleNode(ELEMENTNAME_CONSTRUCTOR, null);
        
          Type[] argTypes = new Type[ ctorNode.ChildNodes.Count ];
          object[] args = new object[ ctorNode.ChildNodes.Count ];        
          for (int i=0; i<ctorNode.ChildNodes.Count ;i++)
          {
            string argType = GetAttributeValue( ctorNode.ChildNodes[i], ATTRIBUTENAME_TYPE );
            string argValue = ctorNode.ChildNodes[i].InnerText;
            argTypes[i] = System.Type.GetType( argType );
            args[i] = Convert.ChangeType( argValue, argTypes[i] );
          }

          IEnricher enricher = null;

          string[] className = typeString.Split(",".ToCharArray());
          if (className != null)
          {
            try 
            {        
              if (1 == className.Length)
              {
                enricher = System.Activator.CreateInstance( Type.GetType( className[0].ToString().Trim() ), args ) as IEnricher;
              }
              else
              {
                Type type = Type.GetType( className[0].ToString().Trim() + ", " + className[1].ToString().Trim() );
                enricher = System.Activator.CreateInstance( type, args ) as IEnricher;
              }

              if (enricher == null) 
              {
                if ( Log.IsLogLevelEnabled( LogLevel.Error ) )
                {
                  TMLogLayer.Error.SetLocation(CLASS_NAME, "PopulateEnrichers").Write("Failed to create enricher: " + string.Join( ", ", className ) );
                }
              }
              else
              {
                enrichers_.Add( name, enricher );
              }
            } 
            catch (Exception tex_)
            {
              if ( Log.IsLogLevelEnabled( LogLevel.Error ) )
              {
                TMLogLayer.Error.SetLocation(CLASS_NAME, "PopulateEnrichers").WriteFormat( "Failed to create {0}: Full node OuterXml was : {1}", tex_, typeString, defNode.OuterXml );
              }
            }
          }
        }
      }
    }

    /// <summary>
    /// Processes the actual XMLNode containing all handlers
    /// </summary>
    private void PopulateChains( XmlNode handlersNode, Hashtable enrichers_ )
    {
      if (handlersNode == null)
      {
        if ( Log.IsLogLevelEnabled( LogLevel.Info ) )
        {
          TMLogLayer.Info.SetLocation(CLASS_NAME, "PopulateChains").Write("No enricher-defs node found.");
        }
      }
      else
      {
        // treat every section separately:
        foreach (var handlerNode in handlersNode.ChildNodes.OfType<XmlElement>())
        {
            try
            {
                // we first need to create the topic handler node
                // note so that if the name was not specified, we can
                // generate one.
                string topicHandlerName = AddTopicHandler (
                    GetAttributeValue(handlerNode, ATTRIBUTENAME_NAME),
                    handlerNode.SelectSingleNode(ELEMENTNAME_COMMON),
                    enrichers_ );

                var krakenRegistration = KrakenRegistration.None;
                
                if (handlerNode.SelectSingleNode(ELEMENTNAME_SUBSCRIBER).ChildNodes.OfType<XmlElement>().Any())
                {
                    krakenRegistration = krakenRegistration ^ KrakenRegistration.HasSubscribers;
                }

                var linkableAttribute = handlerNode.GetAttribute("linkable");
                if (!string.IsNullOrWhiteSpace(linkableAttribute) && string.Equals(linkableAttribute.Trim(), "UserLinkable", StringComparison.OrdinalIgnoreCase))
                {
                    krakenRegistration = krakenRegistration ^ KrakenRegistration.ForWindowLinking;
                }

                // get all the publishers
                foreach (var publisherNode in handlerNode.SelectSingleNode(ELEMENTNAME_PUBLISHER).ChildNodes.OfType<XmlElement>())
                {
                    try
                    {
                        AddPublisher(
                            topicHandlerName,   // common topic name
                            GetAttributeValue(publisherNode, ATTRIBUTENAME_NAME), // publisher's topic
                            GetAttributeValue(publisherNode, ATTRIBUTENAME_TYPE), // publisher's type, if any
                            publisherNode,
                            enrichers_, krakenRegistration);
                    }
                    catch (Exception e)
                    {
                        if (Log.IsLogLevelEnabled(LogLevel.Error))
                        {
                            TMLogLayer.Error.SetLocation(CLASS_NAME, "PopulateChains").WriteFormat("Failed to add publisher {0} while constructing handler {1}. Error: {2}{3} Node:{4}{3}", GetAttributeValue(publisherNode, ATTRIBUTENAME_NAME), e, topicHandlerName, e.Message, Environment.NewLine, publisherNode.OuterXml);
                        }
                    }
                }

                // get all the subscribers
                foreach (var subscriberNode in handlerNode.SelectSingleNode(ELEMENTNAME_SUBSCRIBER).ChildNodes.OfType<XmlElement>())
                {
                    try
                    {
                        AddSubscriber(
                            topicHandlerName,   // common topic name
                            GetAttributeValue(subscriberNode, ATTRIBUTENAME_NAME), // subscriber's topic
                            GetAttributeValue(subscriberNode, ATTRIBUTENAME_TYPE), // subscribers's type, if any
                            subscriberNode,
                            enrichers_);
                    }
                    catch (Exception e)
                    {
                        if (Log.IsLogLevelEnabled(LogLevel.Error))
                        {
                            TMLogLayer.Error.SetLocation(CLASS_NAME, "PopulateChains").WriteFormat("Failed to add subscriber {0} while constructing handler {1}. Error: {2}{3} Node:{4}{3}", e, GetAttributeValue(subscriberNode, ATTRIBUTENAME_NAME), topicHandlerName, e.Message, Environment.NewLine, subscriberNode.OuterXml);
                        }
                    }
                }



                ValidateHandler( topicHandlerName );
            }
            catch( Exception e )
            {
                if (Log.IsLogLevelEnabled( LogLevel.Error ))
                {
                    TMLogLayer.Error.SetLocation(CLASS_NAME, "PopulateChains").WriteFormat("Failed to add Handler section {0}. Error: {1}{3} Node:{2}{3}", GetAttributeValue(handlerNode, ATTRIBUTENAME_NAME), e, e.Message, handlerNode.OuterXml, Environment.NewLine );
                }
            }
        }
      }
    }

    #endregion

    #region internal methods

    /// <summary>
    /// Delegate used by topicMediator to raise the PrePublish events
    /// </summary>
    internal delegate void RaiseRegisterListenerEventDelegate( IColleagueListener sender_, string commonTopicName_ );

    /// <summary>
    /// Attach the given listener with the specified topics.
    /// </summary>
    internal void RegisterTopics(IColleagueListener listener_, string[] topics_, RaiseRegisterListenerEventDelegate raiseEvent )
    {
      if (topics_ != null)
      {
        // start off by getting any existing chains that match the given topics
        foreach( string topic in topics_)
        {
          RegisterTopic( listener_, topic, raiseEvent );
        }
      }
    }

    /// <summary>
    /// Attach the given listener with the specified topics.
    /// </summary>
    internal ArrayList RegisterTopic(IColleagueListener listener_, string topic_, RaiseRegisterListenerEventDelegate raiseEvent )
    {
      ArrayList theseSubscriptions = null;
      if (topic_ != null && topic_.Trim().Length > 0)
      {
        theseSubscriptions = RegisterTopic( topic_ );

        foreach( TopicHandler th in theseSubscriptions )
        {
          if (!th.Subscribers[ topic_ ].Listeners.Contains( listener_ ))
          {
            th.Subscribers[ topic_ ].Listeners.Add( listener_ );
            raiseEvent( listener_, th.EnricherChain.OutputTopicName );
          }
        }
      }
      //register the given topic with the listener 
      listener_.Subscribed( topic_, false );
      return theseSubscriptions;
    }

    /// <summary>
    /// Get the list of topic handlers that are triggered by this topic
    /// </summary>
    internal ArrayList RegisterTopic( string topic_ )
    {
      ArrayList theseSubscriptions = null;
      if (topic_ != null && topic_.Trim().Length > 0)
      {
        // start off by getting any existing chains that match the given topic
        theseSubscriptions = this.GetTopicHandlersForSubscription( topic_ );
        
        if (theseSubscriptions.Count == 0)
        {
          // if we didn't find an applicable subscription then we need to add one
          // note that the topic MUST be unique, so we'll use a GUID - the developer
          // won't know anyway.
          string topicHandlerName = this.AddTopicHandler( "", new IEnricher[] {} );
          this.AddPublisher(topicHandlerName, topic_, null, new IEnricher[] { }, KrakenRegistration.HasSubscribers);
          this.AddSubscriber( topicHandlerName, topic_, null, new IEnricher[] {} , false );
          // now get the handlers for that subscription again
          theseSubscriptions = this.GetTopicHandlersForSubscription( topic_ );
        } 
      }
      return theseSubscriptions;
    }

    /// <summary>
    /// Attach the given enricher set to the specified subscription topic.
    /// 
    /// The enricher set will be appended to any existing enricher chains for this
    /// subscribed topic.
    /// 
    /// If there is no subscribed topic matching this topic, a new one will be
    /// created.
    /// </summary>
    internal void AddEnrichersToSubscriberTopic(string subscribedTopic_, IEnricher[] subscribedEnrichers_ )
    {
      if (subscribedTopic_ != null && subscribedTopic_.Trim().Length > 0)
      {
        ArrayList theseSubscriptions = this.GetTopicHandlersForSubscription( subscribedTopic_ );
        
        if (theseSubscriptions.Count == 0)
        {
          // if we didn't find an applicable subscription then we need to add one
          // note that the topic MUST be unique, so we'll use a GUID - the developer
          // won't know anyway.
          string topicHandlerName = this.AddTopicHandler( "", subscribedEnrichers_ );
          this.AddPublisher(topicHandlerName, subscribedTopic_, null, new IEnricher[] { }, KrakenRegistration.HasSubscribers);
          this.AddSubscriber( topicHandlerName, subscribedTopic_, null, new IEnricher[] {} , false );
        } 
        else
        {
          foreach( TopicHandler th in theseSubscriptions )
          {
            th.Subscribers[subscribedTopic_].EnricherChain.Append( subscribedEnrichers_ );
          }
        }
      }
    }

    /// <summary>
    /// Removes the specified topics for this listener.
    /// </summary>
    internal void UnregisterTopics(IColleagueListener listener_, string[] topics_, RaiseRegisterListenerEventDelegate raiseEvent )
    {
      if (topics_ != null)
      {
        // start off by getting any existing chains that match the given topics
        foreach( string topic in topics_)
        {
          // remove the topic from the list in listeners.
          listener_.Subscribed( topic, true );
          // then establish which topics it would be subscribed to
          ArrayList theseSubscriptions = this.GetTopicHandlersForSubscription( topic );
        
          if (theseSubscriptions.Count > 0)
          {
            foreach( TopicHandler th in theseSubscriptions )
            {
              th.Subscribers[topic].Listeners.Remove( listener_ );
              raiseEvent( listener_, th.EnricherChain.OutputTopicName );
            }
          }
        }
      }
    }

    /// <summary>
    /// Removes the given listener from every topic handler
    /// </summary>
    internal void UnregisterTopics( IColleagueListener listener_, RaiseRegisterListenerEventDelegate raiseEvent )
    {
      // have to look through every topic, and subscriber looking for the 
      // topics this listener subscribes to
      foreach(TopicHandler handler in this._topicHandlers)
      {
        foreach( TopicSubscriber s in handler.Subscribers )
        {
          if (s.ContainsColleague( listener_))
          {
            s.Listeners.Remove( listener_ );
            listener_.Subscribed( s.EnricherChain.OutputTopicName, true );
            raiseEvent( listener_, handler.EnricherChain.OutputTopicName );
          }
        }
      }
    }

    /// <summary>
    /// Enables the programmatic addition of a publisher section of an enricher chain
    /// </summary>
    internal void AddPublisher(string handlerName, string publisherTopicName, string publisherType, IEnricher[] publisherEnrichers, KrakenRegistration krakenRegistration)
    {
      EnricherChain chain = new EnricherChain( publisherEnrichers, publisherTopicName, handlerName, false );
      this.AddPublisher(handlerName, publisherTopicName, publisherType, chain, krakenRegistration); 
    }

    /// <summary>
    /// Enables the programmatic addition of a common section of an enricher chain
    /// </summary>
    /// <exception cref="System.ArgumentException">if HandlerName is null or empty</exception>
    internal string AddTopicHandler( string handlerName, IEnricher[] commonEnrichers )
    {
      handlerName = ValidateHandlerName ( handlerName );
      EnricherChain chain = new EnricherChain( commonEnrichers, handlerName, handlerName, false );
      return this.AddTopicHandler( handlerName, chain );
    }

    /// <summary>
    /// Enables the programmatic addition of a subscriber section of an enricher chain
    /// </summary>
    internal void AddSubscriber( string handlerName, string subscriberTopicName, string subscriberType, IEnricher[] subscriberEnrichers, bool clone_ )
    {
      clone_ |= (subscriberEnrichers != null && subscriberEnrichers.Length > 0);

      EnricherChain chain = new EnricherChain( subscriberEnrichers, handlerName, subscriberTopicName, clone_ );
      AddSubscriber( handlerName, subscriberTopicName, subscriberType, chain );
    }

    /// <summary>
    /// Get the list of potentially published topics for this type key. Note that the type is the 
    /// assmebly qualified type
    /// </summary>
    internal string[] GetPublishedTopicsForType( string typeKey_ )
    {
      return GetTopicsForType( _publishedTopicsForTypes, typeKey_ );
    }

    /// <summary>
    /// Get the list of subscribed topics for this type key. Note that the type is the 
    /// assmebly qualified type
    /// </summary>
    internal string[] GetSubscribedTopicsForType( string typeKey_ )
    {
      return GetTopicsForType( _subscribedTopicsForTypes, typeKey_ );
    }

    /// <summary>
    /// Core mehtod for getting the list of topics that a
    /// type has been set up for in config. First tries the full assmebly
    /// qualified type, then the full name.
    /// </summary>
    private string[] GetTopicsForType( HashtableArrayList table_, string typeKey_ )
    {
      if (typeKey_ != null && typeKey_.Length > 0)
      {
        ArrayList list = table_[typeKey_];
        if (list != null)
        {
          string[] rtn = new string[ list.Count ];
          list.CopyTo( rtn );
          return rtn;
        }
      }
      return new string[0];
    }

    /// <summary>
    /// Utility to show what topic handlers exist, which topics publish it, and which subscribe to it.
    /// </summary>
    /// <returns></returns>
    public override string ToString()
    {
      StringBuilder sb = new StringBuilder();
      foreach( TopicHandler th in this._topicHandlers)
      {
        sb.Append("TopicHandler: " + th.EnricherChain.InputTopicName + "\n{\n");
        // show the enrichers (if any)
        EnricherChain commonChain = ((EnricherChain)th.EnricherChain);
        sb.Append("\tEnrichers: {" + th.EnricherChain.ToString() + "}\n");
        // now the publishers
        sb.Append("\tPublish topics:\n\t{\n");
        // get the publishers for this handler
        foreach( string publisherTopic in th.Publishers.Keys )
        {
          // show the enrichers (if any)
          EnricherChain publisherChain = ((EnricherChain)th.Publishers[publisherTopic]);          
          sb.Append("\t\t" + publisherTopic + "\n\t\t\t Enrichers: {" + publisherChain.ToString() + "}\n");
        }
        sb.Append("\t}\n");
        sb.Append("\tSubscribed topics:\n\t{\n");
        // get the subscribers for this handler
        foreach( TopicSubscriber subscriber in th.Subscribers )
        {
          sb.Append("\t\t" + subscriber.EnricherChain.OutputTopicName + " ( has " + subscriber.Listeners.Count + " listeners: " );
          foreach(Application.IColleague coll in subscriber.Listeners)
          {
            sb.Append( coll.ColleagueId.ToString() + ", " );
          }           
          sb.Append( " )\n" );
          // show the enrichers (if any)
          sb.Append("\t\t\t Enrichers: {" + subscriber.EnricherChain.ToString() + "}\n");
        }
        sb.Append("\t}\n}\n\n");
      }
      return sb.ToString();
    }

    #endregion

    #region private methods

    /// <summary>
    /// Ensure that the given handler name is valid, and return a new Guid if not.
    /// </summary>
    private string ValidateHandlerName( string handlerName )
    {
      if (handlerName == null || handlerName.Trim().Length == 0)
      {
        //create a unique name if we must
        handlerName = Guid.NewGuid().ToString();
      }
      return handlerName;
    }
    
    /// <summary>
    /// Check that the TopicHandler is well formed.
    /// </summary>
    private void ValidateHandler( string topicHandlerName ) 
    {
      bool valid = true;
      try
      {
        CheckHandlerExistance( topicHandlerName, CheckType.ExistsAndUnique );
      }
      catch (Exception e)
      {
        valid = false;
        if (Log.IsLogLevelEnabled( LogLevel.Error ))
        {
          TMLogLayer.Error.SetLocation(CLASS_NAME, "ValidateHandler").WriteFormat("Failed to validate Handler {0}. Error: {1}", e, topicHandlerName, e.Message );
        }
      }

      TopicHandler th = this.GetTopicHandlerForName( topicHandlerName );
      if (th != null)
      {
        // note that if th is null the CheckHandlerForExistance method would know already.
        if (th.Publishers.Count == 0)
        {
          valid = false;
          if (Log.IsLogLevelEnabled( LogLevel.Error ))
          {
            TMLogLayer.Error.SetLocation(CLASS_NAME, "ValidateHandler").WriteFormat("Failed to validate Handler {0}. Error: There are no publishing topics for this handler.", topicHandlerName );
          }
        }
        if (th.Subscribers.Count == 0)
        {
          valid = false;
          if (Log.IsLogLevelEnabled( LogLevel.Error ))
          {
            TMLogLayer.Error.SetLocation(CLASS_NAME, "ValidateHandler").WriteFormat("Failed to validate Handler {0}. Error: There are no subscribing topics for this handler.", topicHandlerName );
          }
        }
      }

      if (!valid)
      {
        if (th != null)
        {
          _topicHandlers.Remove( topicHandlerName );
          foreach(TopicSubscriber subsc in th.Subscribers )
          {
            _handlersForSubscribers.RemoveArrayEntry( subsc.EnricherChain.OutputTopicName, th );
          }
          foreach(EnricherChain publ in th.Publishers)
          {
            _handlersForPublishers.RemoveArrayEntry( publ.OutputTopicName, th );
          }
        }
        if (Log.IsLogLevelEnabled( LogLevel.Error ))
        {
          TMLogLayer.Error.SetLocation(CLASS_NAME, "ValidateHandler").WriteFormat("Failed to validate Handler {0}. Removed handler from tree.", topicHandlerName );
        }      
      }
    }

    /// <summary>
    /// Convenience method to safely get the attribute value from an XmlNode
    /// </summary>
    [System.Diagnostics.DebuggerStepThrough()]
    private static string GetAttributeValue(XmlNode node, string attributeName)
    {
      XmlAttribute attr = node.Attributes[attributeName];
      if (attr != null)
      {
        return attr.Value;
      }
      return null;
    }

    /// <summary>
    /// Add topic handler - this is the config hook
    /// </summary>
    /// <exception cref="System.ArgumentException">if HandlerName is null or empty</exception>
    private string AddTopicHandler( string handlerName_, XmlNode chainSection_, Hashtable enrichers_ )
    {
      handlerName_ = ValidateHandlerName ( handlerName_ );
      XmlNodeList chains = null;
      if (chainSection_ != null)
      {
        chains = chainSection_.ChildNodes;
      }
      EnricherChain chain = new EnricherChain( chains, handlerName_, handlerName_, enrichers_, false );        
      return AddTopicHandler( handlerName_, chain );
    }

    /// <summary>
    /// Add publisher - this is the config hook
    /// </summary>
    private void AddPublisher(string handlerName_, string publisherName_, string publisherType_, XmlNode chainSection_, Hashtable enrichers_, KrakenRegistration krakenRegistration)
    {
      EnricherChain chain = new EnricherChain( chainSection_.ChildNodes, publisherName_, handlerName_, enrichers_, false );
      AddPublisher(handlerName_, publisherName_, publisherType_, chain, krakenRegistration);
    }

    /// <summary>
    /// Add subscriber - this is the config hook
    /// </summary>
    private void AddSubscriber( string handlerName_, string subscriberName_, string subscriberType_, XmlNode chainSection_, Hashtable enrichers_ )
    {
      bool clone = false;

      string cloneString = GetAttributeValue( chainSection_, ATTRIBUTENAME_CLONE );
      clone |= ( cloneString != null && ( cloneString.ToLower() == ATTRIBUTEVALUE_YES || cloneString.ToLower() == ATTRIBUTEVALUE_TRUE ) );
      clone |= ( chainSection_.ChildNodes.Count > 0 );

      EnricherChain chain = new EnricherChain( chainSection_.ChildNodes, handlerName_, subscriberName_, enrichers_, clone );
      AddSubscriber( handlerName_, subscriberName_, subscriberType_, chain );
    }

    /// <summary>
    /// Core method for adding a new topic handler to this tree
    /// </summary>
    /// <exception cref="System.ArgumentException">if HandlerName is null or empty</exception>
    private string AddTopicHandler( string handlerName_, EnricherChain chain_ )
    {
      handlerName_ = ValidateHandlerName ( handlerName_ );
      CheckHandlerExistance( handlerName_, CheckType.DoesNotExist );
      TopicHandler topicHandler = new TopicHandler( chain_ );
      _topicHandlers.Add( handlerName_, topicHandler );
      return handlerName_;
    }

    /// <summary>
    /// Core method for adding a publisher topic to a TopicHandler object.
    /// Note that the topichandler MUST exist by this stage.
    /// </summary>
    /// <exception cref="System.ArgumentException">if HandlerName is null or empty</exception>
    private void AddPublisher(string handlerName_, string publisherName_, string publisherType_, EnricherChain chain_, KrakenRegistration krakenRegistration)
    {
      CheckHandlerExistance( handlerName_, CheckType.ExistsAndUnique );
      lock (_lock)
      {
        KrakenIntegrationModule.RegisterOnKraken(publisherName_, krakenRegistration);

        TopicHandler handler = _topicHandlers[ handlerName_ ];
        handler.Publishers.Add( publisherName_, chain_ );
        // now we need to add the handler to the publisher hashtable
        _handlersForPublishers.Add( publisherName_, handler );
        // if a type was specified then  we need to add that to the publishers list too
        if (publisherType_ != null && publisherType_.Trim().Length > 0)
        {
          _publishedTopicsForTypes.Add( publisherType_, publisherName_ );
        }
      }
    }

    /// <summary>
    /// Core method for adding a subscriber topic to a TopicHandler object
    /// Note that the topichandler MUST exist by this stage.
    /// </summary>
    /// <exception cref="System.ArgumentException">if HandlerName is null or empty</exception>
    private void AddSubscriber( string handlerName_, string subscriberName_, string subscriberType_, EnricherChain chain_ )
    {
      CheckHandlerExistance( handlerName_, CheckType.ExistsAndUnique );
      lock (_lock)
      {
        TopicHandler handler = _topicHandlers[ handlerName_ ];
        handler.Subscribers.Add( subscriberName_, chain_ );
        // now we need to add the handler to the publisher hashtable
        _handlersForSubscribers.Add( subscriberName_, handler );
        // if a type was specified then  we need to add that to the subscribers list too
        if (subscriberType_ != null && subscriberType_.Trim().Length > 0)
        {
          _subscribedTopicsForTypes.Add( subscriberType_, subscriberName_ );
        }
      }
    }

    /// <summary>
    /// Gets the topic handler for this name. 
    /// </summary>
    internal TopicHandler GetTopicHandlerForName( string topicHandlerName )
    {
      return this._topicHandlers[topicHandlerName];
    }

    /// <summary>
    /// Gets the topic handlers that will call this topic.
    /// This method will never return null, only empty arraylists
    /// if necessary
    /// </summary>
    internal ArrayList GetTopicHandlersForSubscription( string subscriptionTopic )
    {
      return GetTopicHandlers( _handlersForSubscribers, subscriptionTopic );
    }

    /// <summary>
    /// Gets the topic handlers that will be called by this topic.
    /// This method will never return null, only empty arraylists
    /// if necessary
    /// </summary>
    internal ArrayList GetTopicHandlersForPublisher( string publisherTopic )
    {
      return GetTopicHandlers( _handlersForPublishers, publisherTopic );
    }

    /// <summary>
    /// Generic method for getting handlers for publishers and subscribers
    /// </summary>
    private ArrayList GetTopicHandlers( HashtableArrayList list, string topic )
    {
      ArrayList rtn = list[ topic ];
      if (rtn == null)
      {
        rtn = new ArrayList();
      }
      return rtn;
    }

    /// <summary>
    /// Ensure the argument for the handler is good.
    /// </summary>
    /// <param name="handlerName_"></param>
    /// <param name="checkType"></param>
    /// <exception cref="System.ArgumentException">if HandlerName is null or empty</exception>
    private void CheckHandlerExistance( string handlerName_, CheckType checkType )
    {
      if (handlerName_ == null || handlerName_.Trim().Length == 0)
      {
        throw new ArgumentException("Topic names cannot be null or empty strings.", "handlerName_");
      }
      if ( checkType != CheckType.NoCheck )
      {
        TopicHandler handler = this._topicHandlers[ handlerName_ ];
        switch (checkType)
        {
          case CheckType.DoesNotExist: 
            if (handler != null) 
            {
              throw new ArgumentException("The topic handler name \"" + handlerName_ + "\" already exists!", "handlerName_" );
            }
            break;
          case CheckType.Exists: 
          case CheckType.ExistsAndUnique:
            if (handler == null) 
            {
              throw new ArgumentException("The topic handler name \"" + handlerName_ + "\" does not exist!", "handlerName_" );
            }
            break;
        }
      }
    }

    #endregion

  }
}
