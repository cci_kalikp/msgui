#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2004 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.MessageRouting.TopicMediator/EnricherStructures/NotifyEventArgs.cs#1 $
  $DateTime: 2013/12/19 08:34:48 $
    $Change: 859541 $
    $Author: istvanf $
_____________________________________________________________________________*/
#endregion File Info Header

using System;

namespace MorganStanley.IED.Concord.TopicMediator
{
  using Application;
  /// <summary>
  /// Indicate how the data has been sent to the subscriber - either as a ITopicMediatorMessage or as an XmlDocument
  /// </summary>
  public enum NotifyMessageCarrier
  {
    /// <summary>
    /// The associated object may be cast directly to XmlDocument
    /// </summary>
    XmlDocument,
    /// <summary>
    /// The associated object may be cast directly to an ITopicMediatorMessage
    /// </summary>
    ITopicMediatorMessage
  }

  /// <summary>
  /// Event handler used by the Notify event.
  /// </summary>
  public delegate void NotifyEventHandler ( IColleague sender_, NotifyEventArgs e_ );

  /// <summary>
  /// EventArgs for the Notify event
  /// </summary>
  public class NotifyEventArgs
  {
    private object _data;
    private System.Xml.XmlDocument _doc;
    private ITopicMediatorMessage _iTopicMediatorMessage;

    private NotifyMessageCarrier _dataType;
    private string[] _publishedTopics;
    private string _subscribedTopic;
    private IColleague _subscriber;
    private IColleague _publisher;

    /// <summary>
    /// Create a new ColleagueNotifyEventArgs object
    /// </summary>
    public NotifyEventArgs(object data_, NotifyMessageCarrier dataType_, string[] publishedTopics_, string subscribedTopic_, IColleague subscriber_, IColleague publisher_)
    {
      _data = data_;
      _dataType = dataType_;
      _publishedTopics = publishedTopics_;
      _subscribedTopic = subscribedTopic_;
      _subscriber = subscriber_;
      _publisher = publisher_;
    }

    /// <summary>
    /// Get the sender for that caused this NotifyEvent
    /// </summary>
    public IColleague Publisher { get { return _publisher; } }
    
    /// <summary>
    /// Get the subscriber that has received the message
    /// </summary>
    public IColleague Subscriber { get { return _subscriber; } }

    /// <summary>
    /// Get the data object that could be XmlDocument or ITopicMediatorMessage
    /// depending on the DataType
    /// </summary>
    public object Data { get { return _data; } }
    /// <summary>
    /// The detaType enum that indicates if the data object is
    /// ITopicMediatorMessage or XmlDocument
    /// </summary>
    public NotifyMessageCarrier DataType { get { return _dataType; } }
    
    /// <summary>
    /// The topics that this message was published with, including 
    /// the topic topic that this particular listener was subscribed to
    /// </summary>
    public string[] PublishedTopics { get { return _publishedTopics; } }

    /// <summary>
    /// The topic that this listener was subscribed to 
    /// that resulted in it recieving the message
    /// </summary>
    public string SubscribedTopic { get { return _subscribedTopic; } }

    /// <summary>
    /// Get the data as an XmlDocument. Note that if DataType is not 
    /// XmlDocument, this property will extract the XmlDocument from 
    /// the ITopicMediatorMessage.
    /// </summary>
    public System.Xml.XmlDocument XmlDocument 
    { 
      get 
      { 
        if (_doc == null)
        {
          if (DataType == NotifyMessageCarrier.XmlDocument)
          {
            _doc = Data as System.Xml.XmlDocument;
          }
          else
          {
            _doc = this.ITopicMediatorMessage.XmlDocument;
          }
        }
        return _doc;
      }
    }

    /// <summary>
    /// Get the data as a ITopicMediatorMessage. Note that if DataType is not 
    /// ITopicMediatorMessage, return null, and you will need to create your
    /// own object that implements ITopicMediatorMessage to create using the 
    /// associated XmlDocument
    /// </summary>
    public ITopicMediatorMessage ITopicMediatorMessage 
    {
      get 
      { 
        if(_iTopicMediatorMessage == null)
        {
          if (DataType == NotifyMessageCarrier.ITopicMediatorMessage)
          {
            _iTopicMediatorMessage = Data as ITopicMediatorMessage;
          }
          else
          {
            // cannot create an instance of ITopicMediatorMessage, so leave it
            // up to the user to do that.
            _iTopicMediatorMessage = null;
          }
        }
        return _iTopicMediatorMessage;
      }
    }
  }
}