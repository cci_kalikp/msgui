#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.MessageRouting.TopicMediator/TopicMediator.cs#4 $
  $DateTime: 2014/05/19 05:34:14 $
    $Change: 881406 $
    $Author: istvanf $
_____________________________________________________________________________*/
#endregion

using System.Linq;
using System.Windows.Threading;
using MSDesktop.MessageRouting;
using MSDesktop.MessageRouting.DynamicType;
using MSDesktop.MessageRouting.Message;

namespace MorganStanley.IED.Concord.TopicMediator
{
    #region using

    using System;
    using System.Collections;
    using System.Threading;
    using System.Xml;
    using MorganStanley.MSDotNet.MSNet;
    using MorganStanley.IED.Concord.Logging;
    using Application;
    using System.Collections.Generic;
    using System.Linq.Expressions;

    #endregion using

    /// <summary>
    /// The class that enables messages to be passed from one colleague to
    /// multiple ColleagueListeners and perform any enrichment along the way.
    /// </summary>
    public sealed class TopicMediator
    {
        #region Constants

        private static readonly string CLASS = "TopicMediator";

        #endregion Constants

        #region Private Declarations

        private static readonly TopicMediator _singleton;

        private IRoutingCommunicator _communicator;
        private Dispatcher _dispatcher;
        private readonly Dictionary<Type, object> _krakenPublishers = new Dictionary<Type, object>();
        private readonly HashSet<Type> _krakenSubscribers = new HashSet<Type>();

        #endregion Private decalrations

        

        #region event declarations

        /// <summary>
        /// Called before a topic begins publishing to any 
        /// subscribers
        /// </summary>
        public event PublishTopicEventHandler BeginPublishTopic;

        /// <summary>
        /// Called when a topic has completed publishing to all 
        /// subscribers
        /// </summary>
        public event PublishTopicEventHandler EndPublishTopic;

        /// <summary>
        /// Called whenever topic mediator is about to publish
        /// a message from to a colleague.
        /// </summary>
        public event PrePublishEventHandler PrePublish;

        /// <summary>
        /// Called whenever a listener is subscribed to a topic
        /// </summary>
        public event RegisteredListenerEventHandler RegisteredListener;

        /// <summary>
        /// Called whenever a listener is unsubscribed to a topic
        /// </summary>
        public event RegisteredListenerEventHandler UnregisteredListener;

        #endregion event Declarations

        #region Constructors

        static TopicMediator()
        {
            _singleton = new TopicMediator();
        }



        private TopicMediator()
        {
            EnricherTree.CheckInitialised();

            if (Log.IsLogLevelEnabled(LogLevel.Debug))
            {
                TMLogLayer.Debug.SetLocation(CLASS, ".ctor()").
                           WriteFormat("Completed Constructor of Topic Mediator");
            }
        }

        #endregion Constructors

        #region Public Properties

        /// <summary>
        /// Get the singleton instance 
        /// </summary>
        public static TopicMediator Current
        {
            [System.Diagnostics.DebuggerStepThrough]
            get
            {
                return _singleton;
            }
        }

        #endregion Public Properties

        #region deprecated public methods fo backwards compatibility

        /// <summary>
        /// Create a ColleaguePublisher which can be used to publish your messages. Weirdly, it's also used to listen
        /// to your messages, which begs the question: what's a listener? So now we only have listeners.
        /// </summary>
        [Obsolete("This has now been deprecated - please use the CreateListener methods instead.", false)]
        public IColleaguePublisher CreateColleague(IMSNetLoop callbackLoop_, string[] topics_)
        {
            #region check args

            if (null == callbackLoop_)
            {
                if (Log.IsLogLevelEnabled(LogLevel.Error))
                {
                    TMLogLayer.Debug.SetLocation(CLASS, "CreateColleague(IMSNetLoop callbackLoop_, string[] topics_)").
                               WriteFormat("Null Callback Loop passed to CreateColleague.");
                }
                throw new ArgumentNullException("callbackLoop_");
            }

            if (null == topics_)
            {
                if (Log.IsLogLevelEnabled(LogLevel.Error))
                {
                    TMLogLayer.Debug.SetLocation(CLASS, "CreateColleague(IMSNetLoop callbackLoop_, string[] topics_)").
                               WriteFormat("Null topics passed to CreateColleague.");
                }
                throw new ArgumentNullException("topics_");
            }

            if (0 == topics_.Length)
            {
                if (Log.IsLogLevelEnabled(LogLevel.Error))
                {
                    TMLogLayer.Debug.SetLocation(CLASS, "CreateColleague(IMSNetLoop callbackLoop_, string[] topics_)").
                               WriteFormat("Zero length topics passed to CreateColleague.");
                }
                throw new ArgumentOutOfRangeException("topics_");
            }

            if (Log.IsLogLevelEnabled(LogLevel.Debug))
            {
                string topics = String.Join(",", topics_);
                TMLogLayer.Debug.SetLocation(CLASS, "CreateColleague(IMSNetLoop callbackLoop_, string[] topics_)").
                           WriteFormat("Entering CreateColleague with topics {0}.", topics);
            }

            #endregion checkargs

            Colleague c = new Colleague(callbackLoop_, topics_);

            if (Log.IsLogLevelEnabled(LogLevel.Debug))
            {
                string topics = String.Join(",", topics_);
                TMLogLayer.Debug.SetLocation(CLASS, "CreateColleague(IMSNetLoop callbackLoop_, string[] topics_)").
                           WriteFormat("Exiting CreateColleague with topics {0}.", topics);
            }

            return c;
        }

        #endregion deprecated Public Methods


        #region Public Methods

        /// <summary>
        /// Create a brand new Colleague listener without using a restored state and subscribing to 
        /// no topics.
        /// </summary>
        public IColleagueListener CreateListener(IMSNetLoop callbackLoop_)
        {
            return CreateListener(callbackLoop_, Guid.Empty, null, string.Empty);
        }

        /// <summary>
        /// Create a brand new Colleague listener without using a restored state and subscribing to 
        /// no topics. If you specify a typeKey object, this colleague will be subscribed
        /// to and registered as a publisher for every topic with the same type spec (in config):
        /// <code>Topic name="Vanilla" type="type"</code>
        /// </summary>
        public IColleagueListener CreateListener(IMSNetLoop callbackLoop_, System.Type typeKey_)
        {
            return CreateListener(callbackLoop_, Guid.Empty, null, typeKey_);
        }

        /// <summary>
        /// Create a brand new Colleague listener without using a restored state and subscribing to 
        /// no topics. If you specify a typeKey object, this colleague will be subscribed
        /// to and registered as a publisher for every topic with the same type spec (in config):
        /// <code>Topic name="Vanilla" type="type"</code>
        /// </summary>
        public IColleagueListener CreateListener(IMSNetLoop callbackLoop_, string typeKey_)
        {
            return CreateListener(callbackLoop_, Guid.Empty, null, typeKey_);
        }

        /// <summary>
        /// Create a brand new Colleague Listener, without using a restored state, subscribing
        /// to the provided topics
        /// </summary>
        public IColleagueListener CreateListener(IMSNetLoop callbackLoop_, string[] topics_)
        {
            return this.CreateListener(callbackLoop_, Guid.Empty, topics_, string.Empty);
        }

        /// <summary>
        /// Create a brand new Colleague Listener, without using a restored state, subscribing
        /// to the provided topics. If you specify a typeKey object, this colleague will be subscribed
        /// to and registered as a publisher for every topic with the same type spec (in config):
        /// <code>Topic name="Vanilla" type="type"</code>
        /// </summary>
        public IColleagueListener CreateListener(IMSNetLoop callbackLoop_, string[] topics_, System.Type typeKey_)
        {
            return this.CreateListener(callbackLoop_, Guid.Empty, topics_, typeKey_);
        }

        /// <summary>
        /// Create a brand new Colleague Listener, without using a restored state, subscribing
        /// to the provided topics. If you specify a typeKey object, this colleague will be subscribed
        /// to and registered as a publisher for every topic with the same type spec (in config):
        /// <code>Topic name="Vanilla" type="type"</code>
        /// </summary>
        public IColleagueListener CreateListener(IMSNetLoop callbackLoop_, string[] topics_, string typeKey_)
        {
            return this.CreateListener(callbackLoop_, Guid.Empty, topics_, typeKey_);
        }

        /// <summary>
        /// Create a new Colleague based on the given xml and subscribed to the given topics
        /// </summary>
        public IColleagueListener CreateListener(IMSNetLoop callbackLoop_, XmlNode colleagueState_, string[] topics_)
        {
            // gets the state from the XmlNode, including the topics it was previously subscribed to
            return this.CreateListener(callbackLoop_, colleagueState_, topics_, string.Empty);
        }

        /// <summary>
        /// Create a new Colleague based on the given xml and subscribed to the given topics.
        /// If you specify a typeKey object, this colleague will be subscribed
        /// to and registered as a publisher for every topic with the same type spec (in config):
        /// <code>Topic name="Vanilla" type="type"</code>
        /// </summary>
        public IColleagueListener CreateListener(IMSNetLoop callbackLoop_, XmlNode colleagueState_, string[] topics_, System.Type typeKey_)
        {
            string type = string.Empty;
            if (typeKey_ != null)
            {
                string[] typeFullName = typeKey_.AssemblyQualifiedName.Split(',');
                type = String.Format("{0}, {1}", typeFullName[0].Trim(), typeFullName[1].Trim());
            }
            return this.CreateListener(callbackLoop_, colleagueState_, topics_, type);
        }

        /// <summary>
        /// Create a new Colleague based on the given xml and subscribed to the given topics.
        /// If you specify a typeKey object, this colleague will be subscribed
        /// to and registered as a publisher for every topic with the same type spec (in config):
        /// <code>Topic name="Vanilla" type="type"</code>
        /// </summary>
        public IColleagueListener CreateListener(IMSNetLoop callbackLoop_, XmlNode colleagueState_, string[] topics_, string typeKey_)
        {
            #region check arguments

            if (null == callbackLoop_)
            {
                if (Log.IsLogLevelEnabled(LogLevel.Error))
                {
                    TMLogLayer.Debug.SetLocation(CLASS, "CreateListener( IMSNetLoop callbackLoop_, XmlNode colleagueState_, string[] topics_, object typeKey_ )").
                               WriteFormat("Null Callback Loop passed to CreateColleague.");
                }
                throw new ArgumentNullException("callbackLoop_");
            }

            if (Log.IsLogLevelEnabled(LogLevel.Debug))
            {
                string topics = (topics_ != null) ? String.Join(",", topics_) : "<<No topics supplied>>";
                TMLogLayer.Debug.SetLocation(CLASS, "CreateListener( IMSNetLoop callbackLoop_, XmlNode colleagueState_, string[] topics_, object typeKey_ )").
                           WriteFormat("Entering CreateColleague with topics {0}.", topics);
            }

            #endregion

            // gets the state from the XmlNode, including the topics it was previously subscribed to
            IColleagueListener c = new ColleagueListener(callbackLoop_, topics_, colleagueState_, typeKey_);

            if (Log.IsLogLevelEnabled(LogLevel.Debug))
            {
                string topics = (topics_ != null) ? String.Join(",", topics_) : "<<No topics supplied>>";
                TMLogLayer.Debug.SetLocation(CLASS, "CreateListener( IMSNetLoop callbackLoop_, XmlNode colleagueState_, string[] topics_, System.Type typeKey_ )").
                           WriteFormat("Exiting CreateColleague with topics {0}.", topics);
            }

            return c;
        }

        /// <summary>
        /// Create a new Colleague Listener that will subscibe to the given topics.
        /// This methoid allows you to create the listener from a restored state. This 
        /// allows us to persist an id from one session to the next and is useful
        /// for any linking. The XmlState only contains the GUID anyway, so this is 
        /// equivalent and allows you to implement your own load state.
        /// </summary>
        /// <exception cref="System.ArgumentNullException"></exception>
        public IColleagueListener CreateListener(IMSNetLoop callbackLoop_, Guid colleageID_, string[] topics_)
        {
            return this.CreateListener(callbackLoop_, colleageID_, topics_, string.Empty);
        }

        /// <summary>
        /// Create a new Colleague based on the given xml and subscribed to the given topics.
        /// If you specify a typeKey object, this colleague will be subscribed
        /// to and registered as a publisher for every topic with the same type spec (in config):
        /// <code>Topic name="Vanilla" type="type"</code>
        /// </summary>
        public IColleagueListener CreateListener(IMSNetLoop callbackLoop_, Guid colleageID_, string[] topics_, System.Type typeKey_)
        {
            string type = string.Empty;
            if (typeKey_ != null)
            {
                string[] typeFullName = typeKey_.AssemblyQualifiedName.Split(',');
                type = String.Format("{0}, {1}", typeFullName[0].Trim(), typeFullName[1].Trim());
            }
            return this.CreateListener(callbackLoop_, colleageID_, topics_, type);
        }

        /// <summary>
        /// Create a new Colleague Listener that will subscibe to the given topics.
        /// This methoid allows you to create the listener from a restored state. This 
        /// allows us to persist an id from one session to the next and is useful
        /// for any linking. The XmlState only contains the GUID anyway, so this is 
        /// equivalent and allows you to implement your own load state.
        /// </summary>
        /// <exception cref="System.ArgumentNullException"></exception>
        public IColleagueListener CreateListener(IMSNetLoop callbackLoop_, Guid colleageID_, string[] topics_, string typeKey_)
        {
            #region check arguments

            if (null == callbackLoop_)
            {
                if (Log.IsLogLevelEnabled(LogLevel.Error))
                {
                    TMLogLayer.Debug.SetLocation(CLASS, "CreateColleague(IMSNetLoop callbackLoop_, string[] topics_)").
                               WriteFormat("Null Callback Loop passed to CreateColleague.");
                }
                throw new ArgumentNullException("callbackLoop_");
            }

            if (Log.IsLogLevelEnabled(LogLevel.Debug))
            {
                string topics = (topics_ != null) ? String.Join(",", topics_) : "<<No topics supplied>>";
                TMLogLayer.Debug.SetLocation(CLASS, "CreateColleague(IMSNetLoop callbackLoop_, string[] topics_)").
                           WriteFormat("Entering CreateColleague with topics {0}.", topics);
            }

            #endregion

            IColleagueListener c = new ColleagueListener(colleageID_, callbackLoop_, topics_, typeKey_);

            if (Log.IsLogLevelEnabled(LogLevel.Debug))
            {
                string topics = (topics_ != null) ? String.Join(",", topics_) : "<<No topics supplied>>";
                TMLogLayer.Debug.SetLocation(CLASS, "CreateColleague(IMSNetLoop callbackLoop_, string[] topics_)").
                           WriteFormat("Exiting CreateColleague with topics {0}.", topics);
            }
            return c;
        }

        /// <summary>
        /// Invokes the publish method on a worker thread. This is important to use if the 
        /// enrichment for your message is likely to take a while (for example there are 
        /// product lookups)
        /// </summary>
        /// <param name="sender_"></param>
        /// <param name="publishingTopics_"></param>
        /// <param name="data_"></param>
        /// <returns></returns>
        public bool BeginPublish(IColleague sender_, string[] publishingTopics_, ITopicMediatorMessage data_)
        {
            PublishArgs pa = new PublishArgs(sender_, publishingTopics_, data_, null);
            return ThreadPool.QueueUserWorkItem(new WaitCallback(this.Publish), pa);
        }

        /// <summary>
        /// Invokes the publish method on a worker thread. This is important to use if the 
        /// enrichment for your message is likely to take a while (for example there are 
        /// product lookups)
        /// </summary>
        /// <param name="sender_"></param>
        /// <param name="publishingTopics_"></param>
        /// <param name="data_"></param>
        /// <returns></returns>
        public bool BeginPublish(IColleague sender_, string[] publishingTopics_, System.Xml.XmlDocument data_)
        {
            PublishArgs pa = new PublishArgs(sender_, publishingTopics_, null, data_);
            return ThreadPool.QueueUserWorkItem(new WaitCallback(this.Publish), pa);
        }

        /// <summary>
        /// Call this method to publish a message on a topic.
        /// Optionally you can include a colleague to ignore - 
        /// any subscribers with the same ColleagueID will be
        /// ignored
        /// </summary>
        public void Publish(IColleague sender_, string[] publishingTopics_, ITopicMediatorMessage data_)
        {
            this.Publish(sender_, publishingTopics_, data_, null);
        }

        /// <summary>
        /// Call this method to publish a message directly to another
        /// colleague, without specifiying any topics.
        /// </summary>
        public void Publish(IColleague sender_, IColleague recipient_, ITopicMediatorMessage data_)
        {
            this.Publish(sender_, recipient_, data_, null);
        }

        /// <summary>
        /// Call this method to publish a message directly to another
        /// colleague, without specifiying any topics.
        /// </summary>
        public void Publish(IColleague sender_, IColleague recipient_, XmlDocument data_)
        {
            this.Publish(sender_, recipient_, null, data_);
        }

        /// <summary>
        /// Call this method to publish a message on a topic.
        /// Optionally you can include a colleague to ignore - 
        /// any subscribers with the same ColleagueID will be
        /// ignored
        /// </summary>    
        public void Publish(IColleague sender_, string[] publishingTopics_, System.Xml.XmlDocument data_)
        {
            this.Publish(sender_, publishingTopics_, null, data_);
        }

        /// <summary>
        /// The topics in this instance could/should have been declared in the 
        /// config, so some/all may already be mapped in the EnricherTree object.
        /// 
        /// However, some may not, and these should be treated as simple connections 
        /// (ie the published/subscribed topics are the same, and no enrichers)
        /// </summary>
        public void RegisterTopics(IColleagueListener colleague_, string[] topics_)
        {
            EnricherTree.Instance.RegisterTopics(colleague_, topics_, new EnricherTree.RaiseRegisterListenerEventDelegate(this.RaiseRegisterListenerEvent));
        }

        /// <summary>
        /// The given topic in this instance could/should have been declared in the 
        /// config, so some/all may already be mapped in the EnricherTree object.
        /// 
        /// However, some may not, and these should be treated as simple connections 
        /// (ie the published/subscribed topics are the same, and no enrichers)
        /// </summary>
        public string[] RegisterTopic(IColleagueListener colleague_, string topic_)
        {
            ArrayList list = EnricherTree.Instance.RegisterTopic(colleague_, topic_, new EnricherTree.RaiseRegisterListenerEventDelegate(this.RaiseRegisterListenerEvent));
            return GetTopicHandlerNames(list);
        }

        /// <summary>
        /// Returns a list of TopicHandler names that publishing a message on the
        /// given topic will be triggered. One publisher's topic can be reused
        /// many times in the TopicMediator config.
        /// </summary>
        public string[] GetTopicHandlerNames(string publishedTopic_)
        {
            ArrayList al = EnricherTree.Instance.GetTopicHandlersForPublisher(publishedTopic_);
            return GetTopicHandlerNames(al);
        }

        /// <summary>
        /// Tests the given name to see if a topic exists on this name.
        /// </summary>
        public bool TopicHandlerNameExists(string handlerName_)
        {
            return (EnricherTree.Instance.GetTopicHandlerForName(handlerName_) != null);
        }

        /// <summary>
        /// The given topic in this instance could/should have been declared in the 
        /// config, so some/all may already be mapped in the EnricherTree object.
        /// 
        /// However, some may not, and these should be treated as simple connections 
        /// (ie the published/subscribed topics are the same, and no enrichers)
        /// </summary>
        public string[] RegisterTopic(string topic_)
        {
            ArrayList list = EnricherTree.Instance.RegisterTopic(topic_);
            return GetTopicHandlerNames(list);
        }

        /// <summary>
        /// Add the given topicname as a publisher for the given handler
        /// </summary>
        /// <param name="publisherTopicName_"></param>
        /// <param name="handlerName_"></param>
        /// <returns></returns>
        public void AddTopicAsPublisher(string publisherTopicName_, string handlerName_)
        {
            EnricherTree.Instance.AddPublisher(handlerName_, publisherTopicName_, null, null, KrakenRegistration.HasSubscribers);
        }

        /// <summary>
        /// Add the given topicname as a subscription to the given handler
        /// </summary>
        /// <param name="subscriberTopicName_"></param>
        /// <param name="handlerName_"></param>
        /// <returns></returns>
        public void AddTopicAsSubscriber(string subscriberTopicName_, string handlerName_)
        {
            EnricherTree.Instance.AddSubscriber(handlerName_, subscriberTopicName_, null, null, false);
        }


        /// <summary>
        /// Attach the given enricher set to the specified subscription topic.
        /// 
        /// The enricher set will be appended to any existing enricher chains for this
        /// subscribed topic.
        /// 
        /// If there is no subscribed topic matching this topic, a new one will be
        /// created, raised by a published topic of the same name.
        /// </summary>
        public void AddEnrichersToSubscriberTopic(string topic_, MessageEnrichers.IEnricher[] enrichers_)
        {
            EnricherTree.Instance.AddEnrichersToSubscriberTopic(topic_, enrichers_);
        }

        /// <summary>
        /// Remove the given colleague from the specified topics
        /// </summary>
        /// <param name="colleague_"></param>
        /// <param name="topics_"></param>
        public void UnregisterTopics(IColleagueListener colleague_, string[] topics_)
        {
            EnricherTree.Instance.UnregisterTopics(colleague_, topics_, new EnricherTree.RaiseRegisterListenerEventDelegate(this.RaiseUnregisterListenerEvent));
        }

        /// <summary>
        /// Completely remove the given listener from the tree
        /// </summary>
        /// <param name="colleague_"></param>
        public void UnregisterTopics(IColleagueListener colleague_)
        {
            EnricherTree.Instance.UnregisterTopics(colleague_, new EnricherTree.RaiseRegisterListenerEventDelegate(this.RaiseUnregisterListenerEvent));
        }

        /// <summary>
        /// In some cases we might need to know what topics, when published,
        /// will cause the given topic handler to fire.
        /// </summary>
        public string[] PublishedTopicsForHandlerName(string handlerName)
        {
            TopicHandler th = EnricherTree.Instance.GetTopicHandlerForName(handlerName);
            if (th == null)
            {
                return null;
            }
            string[] rtn = new string[th.Publishers.Count];
            int i = 0;
            foreach (string s in th.Publishers.Keys)
            {
                rtn[i++] = s;
            }
            return rtn;
        }

        /// <summary>
        /// Create a string that shows the way in which messages are being routed via topic Mediator.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            // attempt to indicate links
            return EnricherTree.Instance.ToString();
        }

        /// <summary>
        /// Get teh list of topics for which the given colleague is registered
        /// as a publisher, or has published
        /// </summary>
        /// <param name="colleague_"></param>
        /// <returns></returns>
        public string[] GetPublishedTopics(IColleague colleague_)
        {
            IColleagueListener list = ColleagueListener.GetListener(colleague_);
            if (list != null)
            {
                return list.PublishedTopics;
            }
            return null;
        }

        /// <summary>
        /// Get the list of topics to which the given colleague has subscribed
        /// </summary>
        /// <param name="colleague_"></param>
        /// <returns></returns>
        public string[] GetSubscribedTopics(IColleague colleague_)
        {
            IColleagueListener list = ColleagueListener.GetListener(colleague_);
            if (list != null)
            {
                return list.SubscribedTopics;
            }
            return null;
        }

        #endregion Public Methods

        #region private Methods

        /// <summary>
        /// Convenience method to convert an array of topichandlers into  a string array
        /// </summary>
        private string[] GetTopicHandlerNames(ArrayList list)
        {
            if (list == null)
            {
                return new String[] { };
            }
            string[] rtn = new string[list.Count];
            for (int i = 0; i < list.Count; i++)
            {
                rtn[i] = ((TopicHandler)list[i]).EnricherChain.OutputTopicName;
            }
            return rtn;
        }

        /// <summary>
        /// Delegate to raise the registered listener event
        /// </summary>
        private void RaiseRegisterListenerEvent(IColleagueListener listener_, string topicName_)
        {
            OnRegisteredListenerEvent(new RegisteredListenerEventArgs(topicName_, listener_));
        }

        /// <summary>
        /// Delegate used to raise the unregistered listener event
        /// </summary>
        private void RaiseUnregisterListenerEvent(IColleagueListener listener_, string topicName_)
        {
            OnUnregisteredListenerEvent(new RegisteredListenerEventArgs(topicName_, listener_));
        }

        /// <summary>
        /// Method used to call the RegisteredListner event
        /// </summary>
        private void OnRegisteredListenerEvent(RegisteredListenerEventArgs e)
        {
            if (this.RegisteredListener != null)
            {
                RegisteredListener(this, e);
            }
        }

        /// <summary>
        /// Method used to call the unregisterd listener event
        /// </summary>
        private void OnUnregisteredListenerEvent(RegisteredListenerEventArgs e)
        {
            if (this.UnregisteredListener != null)
            {
                UnregisteredListener(this, e);
            }
        }

        /// <summary>
        /// Delegate used to raise the PrePublish event
        /// </summary>
        private bool RaisePrePublishEvent(IColleague sender_, IColleagueListener subscriber_, string publisherTopicName_, string commonTopicName_, string subscriptionTopicName_)
        {
            PrePublishEventArgs e = new PrePublishEventArgs(sender_, subscriber_, publisherTopicName_, commonTopicName_, subscriptionTopicName_);
            this.OnPrePublish(e);
            return e.Cancel;
        }

        /// <summary>
        /// Actual raise the prepublish event
        /// </summary>
        /// <param name="e"></param>
        private void OnPrePublish(PrePublishEventArgs e)
        {
            if (PrePublish != null)
            {
                PrePublish(this, e);
            }
        }

        /// <summary>
        /// Raise the BeginPublishTopic event - called before any messages 
        /// are sent to subscribers
        /// </summary>
        private void OnBeginPublishTopic(PublishTopicEventArgs e)
        {
            if (BeginPublishTopic != null)
            {
                BeginPublishTopic(this, e);
            }
        }

        /// <summary>
        /// Raise the EndPublishTopic event - called after a message has been sent
        /// to all subscribers
        /// </summary>
        private void OnEndPublishTopic(PublishTopicEventArgs e)
        {
            if (EndPublishTopic != null)
            {
                EndPublishTopic(this, e);
            }
        }

        /// <summary>
        /// The state object for the BeginPublish methods
        /// </summary>
        private class PublishArgs
        {
            public IColleague sender;
            public string[] publishingTopics;
            public ITopicMediatorMessage message;
            public System.Xml.XmlDocument doc;

            public PublishArgs(IColleague sender_, string[] publishingTopics_, ITopicMediatorMessage message_, System.Xml.XmlDocument doc_)
            {
                sender = sender_;
                publishingTopics = publishingTopics_;
                message = message_;
                doc = doc_;
            }
        }

        /// <summary>
        /// The WaitCallBack handler for async invoking
        /// </summary>
        /// <param name="state"></param>
        /// <exception cref="System.ArgumentNullException"></exception>
        private void Publish(object state)
        {
            PublishArgs pa = state as PublishArgs;
            if (pa == null)
            {
                throw new ArgumentException("PublishArgs are required for htis method.");
            }
            this.Publish(pa.sender, pa.publishingTopics, pa.message, pa.doc);
        }

        /// <summary>
        /// The colleague to colleague method
        /// </summary>
        /// <param name="sender_"></param>
        /// <param name="recipient_"></param>
        /// <param name="message_"></param>
        /// <param name="doc_"></param>
        private void Publish(IColleague sender_, IColleague recipient_, ITopicMediatorMessage message_, System.Xml.XmlDocument doc_)
        {
            string topic = "DIRECT";
            // send the begin publish event
            PublishTopicEventArgs publishTopicArgs = new PublishTopicEventArgs(sender_, topic);
            OnBeginPublishTopic(publishTopicArgs);

            // now we need to find the recipient listener to invoke on:
            IColleagueListener recipient = ColleagueListener.GetListener(recipient_);
            if (recipient == null)
            {
                throw new ArgumentException("Could not find the recipient listener. You must have created a listener through TM for TM to invoke the call on the specified colleague.", "recipient_");
            }
            // send the pre-publish event
            PrePublishEventArgs e = new PrePublishEventArgs(sender_, recipient_, topic, topic, topic);
            this.OnPrePublish(e);
            if (!e.Cancel)
            {
                if (message_ != null)
                {
                    recipient.OnNotify(new NotifyEventArgs(message_, NotifyMessageCarrier.ITopicMediatorMessage, new string[] { topic }, topic, recipient, sender_));
                }
                else
                {
                    recipient.OnNotify(new NotifyEventArgs(doc_, NotifyMessageCarrier.XmlDocument, new string[] { topic }, topic, recipient, sender_));
                }
            }
            // finally, send the post publish event
            OnEndPublishTopic(publishTopicArgs);
        }

        /// <summary>
        /// Do the work for publishing messages.
        /// All publishing methods come through here
        /// </summary>
        private void Publish(IColleague sender_, string[] publishingTopics_, ITopicMediatorMessage message_, System.Xml.XmlDocument doc_)
        {
            // you can publish annonymously - so check for that:
            if (sender_ != null)
            {
                // notify the listener that we are publishing on the given topic list
                IColleagueListener sender = ColleagueListener.GetListener(sender_);
                if (sender != null)
                {
                    foreach (string s in publishingTopics_)
                    {
                        sender.Published(s);
                    }
                }
            }
            else
            {
                sender_ = ColleagueInstance.Empty;
            }


            var topics = publishingTopics_.Select(topic =>
                                            {
                                                var krakenMessage = AssemblyGenerator.GetDynamicTypeInstance<ConcordDynamicBaseMessage>(topic);
                                                XmlDocument doc = message_ == null ? doc_ : message_.XmlDocument;
                                                krakenMessage.TopicMediatorTypeName = message_ == null
                                                                                          ? "bareXML"
                                                                                          : message_.GetType()
                                                                                                    .AssemblyQualifiedName;
                                                krakenMessage.Content = doc.OuterXml;
                                                krakenMessage.Topics = publishingTopics_;

                                                if (doc != null && doc.FirstChild != null &&
                                                    doc.FirstChild.FirstChild != null)
                                                {
                                                    if (doc.FirstChild.FirstChild.Attributes != null)
                                                    {
                                                        var action = doc.FirstChild.FirstChild.Attributes["name"];
                                                        if (action != null)
                                                        {
                                                            krakenMessage.Action = action.Value;
                                                        }
                                                    }
                                                }

                                                return new { Topic = topic, Message = krakenMessage };
                                            })
                                          .Where(topic => !PublishToKraken(topic.Message))
                                          .Select(topic => topic.Topic)
                                          .ToList();

            if (topics.Count == 0)
            {
                return;
            }

            // get all the topic handlers for these published topics
            var handlers = new ArrayList();
            foreach (string topic in topics)
            {
                handlers.AddRange(EnricherTree.Instance.GetTopicHandlersForPublisher(topic));
            }

            // strip out any duplicates
            for (int i = handlers.Count - 1; i >= 0; i--)
            {
                // start off by removing the handler - we'll add it again if we want to
                TopicHandler thisHandler = (TopicHandler)handlers[i];
                handlers.RemoveAt(i);
                // this facilitates the duplicate search
                if (handlers.Contains(thisHandler))
                {
                    // this one is a duplicate, so skip it - ie don't re-add it again
                    continue;
                }
                handlers.Add(thisHandler);
            }

            // if there are no handlers at all then we need to create one:
            // note that this is an unusual and non-desirable situation; nobody is
            // listening to what this colleague wants to say, but we are going to create a
            // topic anyway. This is because it's probably a bug in the application's tm
            // config, and so this step may help in any diagnosis
            if (handlers.Count == 0)
            {
                if (Log.IsLogLevelEnabled(LogLevel.Error))
                {
                    TMLogLayer.Error.SetLocation(CLASS, "Publish( IColleague sender_, string[] publishingTopics_, ITopicMediatorMessage message_, System.Xml.XmlDocument doc_)")
                      .WriteFormat("Auto-registering topics that have been published. No colleagues will be listening as no topic handler exists for these topics. This is probably a bug: " + String.Join(";", publishingTopics_));
                }
                // register the topics
                foreach (string topic in publishingTopics_)
                {
                    EnricherTree.Instance.RegisterTopic(topic);
                }
                this.Publish(sender_, publishingTopics_, message_, doc_);
                return;
            }

            // if there are more than one handlers to deal with this message
            // we'll need to clone the message, and only send the original with 
            // the last one
            for (int j = handlers.Count - 1; j >= 0; j--)
            {
                TopicHandler thisHandler = (TopicHandler)handlers[j];
                // indicate our intention to publish:
                OnBeginPublishTopic(new PublishTopicEventArgs(sender_, thisHandler.EnricherChain.OutputTopicName));


                // now check to see if there are any subscribers for the selected topic handlers
                bool doSend = false;
                // examine each subscribing topic in the handler to see if there are 
                // any subscribing listeners for the topic
                foreach (TopicSubscriber subscriber in thisHandler.Subscribers)
                {
                    if (subscriber.Listeners.Count > 0)
                    {
                        // only keep the handler if there is a valid listener other than the sender
                        doSend = true;
                        break;
                    }
                }

                if (doSend)
                {
                    System.Xml.XmlDocument doc = doc_;
                    ITopicMediatorMessage message = message_;
                    if (message_ != null)
                    {
                        // publish the message
                        if (j > 0)
                        {
                            // clone the message if this isn't the first time the message has been sent
                            message = message_.Clone();
                        }
                    }
                    else
                    {
                        // publish the document
                        if (j > 0)
                        {
                            // clone the document if this isn't the first time it's been sent
                            doc = new System.Xml.XmlDocument();
                            doc.LoadXml(doc_.OuterXml);
                        }
                    }
                    thisHandler.Publish(message, doc, publishingTopics_, sender_, new TopicHandler.RaisePrePublishEventDelegate(this.RaisePrePublishEvent));
                }
                OnEndPublishTopic(new PublishTopicEventArgs(sender_, thisHandler.EnricherChain.OutputTopicName));
            }

        }

        public void PublishFromKraken(ConcordDynamicBaseMessage msg)
        {
            XmlDocument doc_ = null;
            IColleague sender_ = null;
            var publishingTopics_ = msg.Topics;
            ITopicMediatorMessage message_ = null;
            var xml = new XmlDocument();
            xml.LoadXml(msg.Content);
            if (msg.TopicMediatorTypeName == "bareXML")
            {
                doc_ = xml;
            }
            else
            {
                var messageType = Type.GetType(msg.TopicMediatorTypeName);
                if (messageType == null)
                    return;
                message_ = Activator.CreateInstance(messageType, xml) as ITopicMediatorMessage;
                
            }

            // you can publish annonymously - so check for that:
            if (sender_ != null)
            {
                // notify the listener that we are publishing on the given topic list
                IColleagueListener sender = ColleagueListener.GetListener(sender_);
                if (sender != null)
                {
                    foreach (string s in publishingTopics_)
                    {
                        var topic = s;
                        _dispatcher.BeginInvoke((Action)(() => sender.Published(topic)));
                    }
                }
            }
            else
            {
                sender_ = ColleagueInstance.Empty;
            }
            // get all the topic handlers for these published topics
            ArrayList handlers = new ArrayList();
            foreach (string topic in publishingTopics_)
            {
                handlers.AddRange(EnricherTree.Instance.GetTopicHandlersForPublisher(topic));
            }

            // strip out any duplicates
            for (int i = handlers.Count - 1; i >= 0; i--)
            {
                // start off by removing the handler - we'll add it again if we want to
                TopicHandler thisHandler = (TopicHandler)handlers[i];
                handlers.RemoveAt(i);
                // this facilitates the duplicate search
                if (handlers.Contains(thisHandler))
                {
                    // this one is a duplicate, so skip it - ie don't re-add it again
                    continue;
                }
                handlers.Add(thisHandler);
            }

            // if there are no handlers at all then we need to create one:
            // note that this is an unusual and non-desirable situation; nobody is
            // listening to what this colleague wants to say, but we are going to create a
            // topic anyway. This is because it's probably a bug in the application's tm
            // config, and so this step may help in any diagnosis
            if (handlers.Count == 0)
            {
                if (Log.IsLogLevelEnabled(LogLevel.Error))
                {
                    TMLogLayer.Error.SetLocation(CLASS, "Publish( IColleague sender_, string[] publishingTopics_, ITopicMediatorMessage message_, System.Xml.XmlDocument doc_)")
                              .WriteFormat("Auto-registering topics that have been published. No colleagues will be listening as no topic handler exists for these topics. This is probably a bug: " + String.Join(";", publishingTopics_));
                }
                // register the topics
                foreach (string topic in publishingTopics_)
                {
                    EnricherTree.Instance.RegisterTopic(topic);
                }

                _dispatcher.BeginInvoke((Action) (() => Publish(sender_, publishingTopics_, message_, doc_)));
                return;
            }

            // if there are more than one handlers to deal with this message
            // we'll need to clone the message, and only send the original with 
            // the last one
            for (int j = handlers.Count - 1; j >= 0; j--)
            {
                TopicHandler thisHandler = (TopicHandler)handlers[j];
                // indicate our intention to publish:
                OnBeginPublishTopic(new PublishTopicEventArgs(sender_, thisHandler.EnricherChain.OutputTopicName));


                // now check to see if there are any subscribers for the selected topic handlers
                bool doSend = false;
                // examine each subscribing topic in the handler to see if there are 
                // any subscribing listeners for the topic
                foreach (TopicSubscriber subscriber in thisHandler.Subscribers)
                {
                    if (subscriber.Listeners.Count > 0)
                    {
                        // only keep the handler if there is a valid listener other than the sender
                        doSend = true;
                        break;
                    }
                }

                if (doSend)
                {
                    System.Xml.XmlDocument doc = doc_;
                    ITopicMediatorMessage message = message_;
                    if (message_ != null)
                    {
                        // publish the message
                        if (j > 0)
                        {
                            // clone the message if this isn't the first time the message has been sent
                            message = message_.Clone();
                        }
                    }
                    else
                    {
                        // publish the document
                        if (j > 0)
                        {
                            // clone the document if this isn't the first time it's been sent
                            doc = new System.Xml.XmlDocument();
                            doc.LoadXml(doc_.OuterXml);
                        }
                    }
                    _dispatcher.BeginInvoke((Action)(() => thisHandler.Publish(message, doc, publishingTopics_, sender_, RaisePrePublishEvent)));
                    
                }
                OnEndPublishTopic(new PublishTopicEventArgs(sender_, thisHandler.EnricherChain.OutputTopicName));
            }
        }

        #endregion  private Methods

        #region implementation of colleague to use as an empty colleague when annonymously publishing

        private class ColleagueInstance : IColleague
        {
            private static IColleague _instance = new ColleagueInstance();

            internal static IColleague Empty
            {
                get
                {
                    return _instance;
                }
            }

            #region Implementation of IColleague

            public void LoadState(System.Xml.XmlNode node_, ref bool cancel_)
            {
                // do nothing
            }

            public void SaveState(System.Xml.XmlNode node_, ref bool cancel_)
            {
                // do nothing
            }

            public System.Guid ColleagueId
            {
                get
                {
                    return new System.Guid();
                }
            }

            #endregion

        }

        #endregion implementation of colleague to use as an empty colleague when annonymously publishing

        #region Kraken integration

        internal void RegisterKraken(IRoutingCommunicator communicator, Dispatcher dispatcher)
        {
            _dispatcher = dispatcher;
            _communicator = communicator;
        }

        internal void RegisterTopicOnKraken(string topic, bool publish, bool subscribe)
        {
            if (!publish && !subscribe)
            {
                return;
            }

            var messageType = AssemblyGenerator.GetDynamicType<ConcordDynamicBaseMessage>(topic);


            var pub = GetKrakenPublisher(_communicator, messageType);

            if (publish && !_krakenPublishers.ContainsKey(messageType))
            {
                _krakenPublishers.Add(messageType, pub);
            }

            if (subscribe && _krakenSubscribers.Add(messageType))
            {
                SubscribeOnKraken(messageType);
            }
        }

        public object GetKrakenPublisher(IRoutingCommunicator kraken, Type t)
        {
            var m = typeof(IRoutingCommunicator).GetMethod("GetPublisher");
            var gm = m.MakeGenericMethod(t);
            var p = gm.Invoke(kraken, null);
            return p;
        }

        private object GetKrakenSubscriber(Type t)
        {
            var m = typeof(IRoutingCommunicator).GetMethod("GetSubscriber");
            var gm = m.MakeGenericMethod(t);
            dynamic s = gm.Invoke(_communicator, null);

            return s;
        }

        public void SubscribeOnKraken(Type t)
        {
            var sD = this.GetKrakenSubscriber(t);

            var type = Type.GetType("MSDesktop.MessageRouting.IRoutedSubscriber`1, MSDesktop.MessageRouting.Interfaces");
            if (type == null)
                return;

            var subscriberType = type.MakeGenericType(t);
            var observable = subscriberType.GetMethod("GetObservable").Invoke(sD, null);
            var method = typeof(TopicMediator).GetMethod("PublishFromKraken");

            var param = Expression.Parameter(t);
            var lambda = Expression.Lambda(Expression.Call(Expression.Constant(this), method, param), param);
            
            //TODO figure out the proper GetMethod call instead of indexing into the result of GetMethods
            var xxx = typeof (ObservableExtensions).GetMethods()[1].MakeGenericMethod(t);
            xxx.Invoke(null, new[] {observable, lambda.Compile()});
        }


        public bool PublishToKraken(ConcordDynamicBaseMessage message)
        {
            var t = message.GetType();

            object pub;
            if (!_krakenPublishers.TryGetValue(t, out pub))
            {
                return false;
            }

            var x = pub.GetType().GetMethod("Publish");
            x.Invoke(pub, new object[] {message});

            return true;
        }

        #endregion
    }

    [Serializable]
    public class ConcordDynamicBaseMessage : IProvideMessageInfo
    {
        public string TopicMediatorTypeName { get; set; }
        public string Content { get; set; }
        public string[] Topics { get; set; }
        public string Action { get; set; }


        public string GetInfo()
        {
            return this.Action;
        }
    }
}