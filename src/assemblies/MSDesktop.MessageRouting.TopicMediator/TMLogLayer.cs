#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.MessageRouting.TopicMediator/TMLogLayer.cs#1 $
  $DateTime: 2013/12/19 08:34:48 $
    $Change: 859541 $
    $Author: istvanf $
_____________________________________________________________________________*/
#endregion

#region MorganStanley.IED.Concord.TopicMediator Namespace
namespace MorganStanley.IED.Concord.TopicMediator
{
  #region using
  using System;
  using System.Reflection;

  using MorganStanley.IED.Concord.Logging;
  #endregion using

  #region TMLogLayer Class
  /// <summary>
  /// Summary description for TMLogLayer.
  /// </summary>
  internal class TMLogLayer
  {

    #region Constants
    public static readonly string META = "IED";
    public static readonly string PROJECT = "Concord.TopicMediator";
    public static readonly string RELEASE = Assembly.GetExecutingAssembly().GetName().Version.ToString();
    #endregion Constants

    #region Enums
    #endregion Enums

    #region Events
    #endregion Events

    #region Private Declarations
    #endregion Private Declarations

    #region Protected Declarations
    #endregion Protected Declarations

    #region Constructors
    #endregion Constructors

    #region Public Properties
    #endregion Public Properties

    #region Public Methods
    public static Log Emergency
    {
      get { return Log.Emergency.SetLayer(META, PROJECT, RELEASE); }
    }
    
    public static Log Alert
    {
      get { return Log.Alert.SetLayer(META, PROJECT, RELEASE); }
    }
    
    public static Log Critical
    {
      get { return Log.Critical.SetLayer(META, PROJECT, RELEASE); }
    }
    
    public static Log Error
    {
      get { return Log.Error.SetLayer(META, PROJECT, RELEASE); }
    }
    
    public static Log Warning
    {
      get { return Log.Warning.SetLayer(META, PROJECT, RELEASE); }
    }
    
    public static Log Notice
    {
      get { return Log.Notice.SetLayer(META, PROJECT, RELEASE); }
    }
    
    public static Log Info
    {
      get { return Log.Info.SetLayer(META, PROJECT, RELEASE); }
    }
    
    public static Log Debug
    {
      get { return Log.Debug.SetLayer(META, PROJECT, RELEASE); }
    }
    #endregion Public Methods

    #region Protected Methods
    #endregion Protected Methods

    #region Private Helper Methods
    #endregion Private Helper Methods

	}
  #endregion TMLogLayer Class
}
#endregion MorganStanley.IED.Concord.TopicMediator Namespace
