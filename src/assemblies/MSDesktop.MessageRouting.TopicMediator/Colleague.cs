#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/MSDesktop.MessageRouting.TopicMediator/Colleague.cs#1 $
  $DateTime: 2013/12/19 08:34:48 $
    $Change: 859541 $
    $Author: istvanf $
_____________________________________________________________________________*/
#endregion

#region MorganStanley.IED.Concord.TopicMediator Namespace
namespace MorganStanley.IED.Concord.TopicMediator
{

  #region using
  using System;
  using System.Collections;
  using System.Collections.Specialized;

  using MorganStanley.MSDotNet.MSNet;

  using MorganStanley.IED.Concord.Logging;
  using MorganStanley.IED.Concord.Logging.RequestTracing;
  using Application;
  #endregion using

  #region Colleague Class

  [Obsolete("This class has been implemented for backwards compatibility. Please use the CreateListener instead.", false)]
  internal sealed class Colleague : IColleagueListener, IColleaguePublisher
  {

    #region Constants
    public static readonly string CLASS = "Colleague";
    #endregion Constants

    #region Enums
    #endregion Enums

    #region Events

    private event ColleagueNotifyDelegate PublisherNotify;

    event ColleagueNotifyDelegate IColleaguePublisher.Notify
    {   
      add { PublisherNotify += value; }
      remove { PublisherNotify -= value; }
    }

    private event NotifyEventHandler ListenerNotify;

    event NotifyEventHandler IColleagueListener.Notify
    {
      add { ListenerNotify += value; }
      remove { ListenerNotify -= value; }
    }
    #endregion Events

    #region Private Declarations
    private IColleagueListener _listener;
    #endregion Private Declarations

    #region Constructors

    internal Colleague(IMSNetLoop callbackLoop_, string[] topics_)
    {
      if (Log.IsLogLevelEnabled(LogLevel.Debug))
      {
        TMLogLayer.Debug.SetLocation(CLASS, ".ctor(int id_, TopicMediator mediator_, string[] topics_))").
          Write("Entering Constructor of Colleague()." );
      }

      _listener = TopicMediator.Current.CreateListener( callbackLoop_, topics_ );

      _listener.Notify += new NotifyEventHandler( this._listener_Notify );

      if (Log.IsLogLevelEnabled(LogLevel.Debug))
      {
        TMLogLayer.Debug.SetLocation(CLASS, ".ctor(int id_, TopicMediator mediator_, string[] topics_))").
          Write("Exiting Constructor of Colleague({0}).");
      }
    }

    private void _listener_Notify( IColleague sender, NotifyEventArgs e)
    {
      if ( ListenerNotify != null ) 
      { 
        ListenerNotify( sender, e  ); 
      }
      if (PublisherNotify != null)
      {
        PublisherNotify( sender, e.XmlDocument, e.PublishedTopics );
      }
    }

    #endregion Constructors

    #region Public Properties

    Guid IColleague.ColleagueId
    {
      get
      {
        return _listener.ColleagueId;
      }
    }

    public IMSNetLoop CallbackLoop
    {
      get {return _listener.CallbackLoop;}
    }

    Hashtable IColleaguePublisher.TopicEncoders
    {
      get
      {
        System.Diagnostics.Debug.Fail("This method of linking will no longer work, as the LinkManager has been moved out to TopicMediator.");
        return null;
      }
    }

    public System.Guid LinkId
    {
      get
      {
        System.Diagnostics.Debug.Fail("This method of linking will no longer work, as the LinkManager has been moved out to TopicMediator.");
        return Guid.Empty;
      }
      set
      {
        System.Diagnostics.Debug.Fail("This method of linking will no longer work, as the LinkManager has been moved out to TopicMediator.");
      }
    }

    #endregion Public Properties

    #region Public Methods

    void IColleaguePublisher.AddTopic(string topic_)
    {
      _listener.AddTopic( topic_ );
    }

    void IColleaguePublisher.RemoveTopic(string topic_)
    {
      _listener.RemoveTopic( topic_ );
    }

    void IColleaguePublisher.AddFilter(string filter_)
    {
      System.Diagnostics.Debug.Fail("This method of linking will no longer work, as the LinkManager has been moved out to TopicMediator.");
    }

    void IColleaguePublisher.RemoveFilter(string filter_)
    {
      System.Diagnostics.Debug.Fail("This method of linking will no longer work, as the LinkManager has been moved out to TopicMediator.");
    }

    void IColleaguePublisher.Publish(string[] topics_, System.Xml.XmlDocument data_)
    {
      TopicMediator.Current.Publish( _listener, topics_, data_ );
    }

    void IColleaguePublisher.Publish(IColleague colleague_, System.Xml.XmlDocument data_)
    {
      TopicMediator.Current.Publish( _listener, colleague_, data_ );
    }

    /// <include file='xmldocs/IColleaguePublisher.cs.xml' path='doc/doc[@for="IColleaguePublisher.Publish2"]/*'/>
    void IColleaguePublisher.Publish(string[] topics_, System.Xml.XmlDocument data_, string[] filters_)
    {
      System.Diagnostics.Debug.Fail("This method of linking will no longer work, as the LinkManager has been moved out to TopicMediator.");
      TopicMediator.Current.Publish( _listener, topics_, data_ );
    }

    void IColleaguePublisher.Publish(string[] topics_, ITopicMediatorMessage data_)
    {
      TopicMediator.Current.Publish( _listener, topics_, data_ );
    }

    void IColleaguePublisher.Publish(IColleague colleague_, ITopicMediatorMessage data_)
    {
      TopicMediator.Current.Publish( _listener, colleague_, data_ );
    }

    void IColleaguePublisher.Publish(string[] topics_, ITopicMediatorMessage data_, string[] filters_)
    {
      System.Diagnostics.Debug.Fail("This method of linking will no longer work, as the LinkManager has been moved out to TopicMediator.");
      TopicMediator.Current.Publish( _listener, topics_, data_ );
    }

    void IColleaguePublisher.AddTopicEncoder(string topic_, string mask_)
    {
      System.Diagnostics.Debug.Fail("This method of linking will no longer work, as the LinkManager has been moved out to TopicMediator.");      
    }

    void IColleaguePublisher.RemoveTopicEncoder(string topic_, string mask_)
    {
      System.Diagnostics.Debug.Fail("This method of linking will no longer work, as the LinkManager has been moved out to TopicMediator.");      
    }

    #endregion Public Methods

    #region Protected Methods
    #endregion Protected Methods

    #region Internal Methods
    #endregion Internal Methods

    #region IColleagueListener interface

    void IColleagueListener.AddTopic(string topic_)
    {
      _listener.AddTopic( topic_ );
    }

    void IColleagueListener.RemoveTopic(string topic_)
    {
      _listener.RemoveTopic( topic_ );
    }

    void IColleagueListener.OnNotify(MorganStanley.IED.Concord.TopicMediator.NotifyEventArgs e)
    {
      _listener.OnNotify( e );
    }

    /// <summary>
    /// The arraylist of topics that this colleague has published
    /// on or will publish on.
    /// </summary>
    string[] IColleagueListener.PublishedTopics 
    { 
      get
      {
        return _listener.PublishedTopics;
      }
    }

    /// <summary>
    /// The arraylist of topics that this colleague has been
    /// subscribed to. Note that these are the topics themselves,
    /// not the handlers that respond to these topics
    /// </summary>
    string[] IColleagueListener.SubscribedTopics 
    {
      get
      {
        return _listener.SubscribedTopics;
      }
    }

    /// <summary>
    /// Called whenever this colleague has been successfully subscribed
    /// to the given topic.
    /// </summary>
    void IColleagueListener.Subscribed( string topic, bool unsubscribed )
    {
      _listener.Subscribed( topic, unsubscribed );
    }
    /// <summary>
    /// Called whenever this colleague is associated with a publish call.
    /// </summary>
    /// <param name="topic"></param>
    void IColleagueListener.Published(string topic)
    {
      _listener.Published( topic );
    }

    #endregion IColleagueListener implementation

    #region Implementation of IColleague
  
    public System.Guid ColleagueId
    {
      get
      {
        return new System.Guid();
      }
    }

    void Application.IColleague.SaveState( System.Xml.XmlNode state_, ref bool cancel_ )
    {
      _listener.SaveState(state_, ref cancel_);
    }

    void Application.IColleague.LoadState( System.Xml.XmlNode state_, ref bool cancel_ )
    {
      _listener.LoadState(state_, ref cancel_);
    }


    #endregion

  }
  #endregion Colleague Class
}
#endregion MorganStanley.IED.Concord.TopicMediator Namespace
