﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MSDesktop.Environment.Providers
{
    internal static class ConcordProfileProviders
    {
        internal static Environ GetEnviron(IUnityContainer container_)
        {
            var profile = container_.Resolve<string>("CurrentConcordProfile");
            if (profile.ToUpper().Contains("QA")) return Environ.QA;
            if (profile.ToUpper().Contains("DEV")) return Environ.Dev;
            if (profile.ToUpper().Contains("PROD")) return Environ.Prod;
            if (profile.ToUpper().Contains("UAT")) return Environ.UAT;
            return Environ.N_A;
        }

        internal static EnvironmentRegion GetRegion(IUnityContainer container_)
        {
            var profile = container_.Resolve<string>("CurrentConcordProfileValue");
            var regionPart = profile.Split(';').FirstOrDefault(p_ => p_.StartsWith("region="));
            if (regionPart != null)
            {
                EnvironmentRegion result;
                if (Enum.TryParse(regionPart.Substring("region=".Length), true, out result))
                {
                    return result;
                }
            }
            return EnvironmentRegion.N_A;
        }

        internal static string GetUser(IUnityContainer container_)
        {
            return container_.Resolve<string>("CurrentConcordUser");
        }
    }
}
