﻿using System;
using System.Linq;
using Microsoft.Practices.Unity;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MSDesktop.Environment.Providers
{
    internal static class CommandLineEnvironProvider
    {
        public static Environ GetEnviron(IUnityContainer container_)
        {
            var showMsgBox = new Action(() => TaskDialog.ShowMessage(
                    string.Format("Application must be started with command line argument -env:[{0}]", string.Join("|", Enum.GetNames(typeof(Environ)))),
                    "Startup Error",
                    TaskDialogCommonButtons.Close,
                    VistaTaskDialogIcon.Error));

            var envArg = System.Environment.GetCommandLineArgs().FirstOrDefault(x => x.StartsWith("-env:", StringComparison.InvariantCultureIgnoreCase));
            if (string.IsNullOrWhiteSpace(envArg))
            {
                showMsgBox();
                System.Environment.Exit(0);
            }

            var envString = envArg.Split(':').Skip(1).FirstOrDefault();
            if (string.IsNullOrEmpty(envString))
            {
                showMsgBox();
                System.Environment.Exit(0);
            }

            Environ env;
            if (!Enum.TryParse(envString.Trim(), true, out env))
            {
                TaskDialog.ShowMessage(
                    string.Format(
                        "Application must be started with command line argument -env:[{0}]\n{1} is unrecognised, you may need to add this env to the Env enum",
                        string.Join("|", Enum.GetNames(typeof(Environ))), envString),
                    "Startup Error",
                    TaskDialogCommonButtons.Close,
                    VistaTaskDialogIcon.Error);
                System.Environment.Exit(0);
            }
            return env;
        }
    }
}
