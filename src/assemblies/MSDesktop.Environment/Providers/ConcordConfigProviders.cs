﻿using Microsoft.Practices.Unity;
using MorganStanley.IED.Concord.Configuration;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MSDesktop.Environment.Providers
{
    internal static class ConcordConfigProviders
    {
        internal static Environ GetEnviron(IUnityContainer container_)
        {
            var config = (Configurator) ConfigurationManager.GetConfig("Environment");
            var environ = config.GetValue("Environ", null) ?? string.Empty;
            if (environ.ToUpper().Contains("QA")) return Environ.QA;
            if (environ.ToUpper().Contains("DEV")) return Environ.Dev;
            if (environ.ToUpper().Contains("PROD")) return Environ.Prod;
            if (environ.ToUpper().Contains("UAT")) return Environ.UAT;
            return Environ.N_A;
        }

        internal static EnvironmentRegion GetRegion(IUnityContainer container_)
        {
            var config = (Configurator)ConfigurationManager.GetConfig("Environment");
            var region = config.GetValue("Region", null) ?? string.Empty;
            if (region.ToUpper().Contains("NY")) return EnvironmentRegion.NY;
            if (region.ToUpper().Contains("EU")) return EnvironmentRegion.EU;
            if (region.ToUpper().Contains("HK")) return EnvironmentRegion.HK;
            if (region.ToUpper().Contains("LN")) return EnvironmentRegion.LN;
            if (region.ToUpper().Contains("TK")) return EnvironmentRegion.TK;
            return EnvironmentRegion.N_A;
        }
    }
}
