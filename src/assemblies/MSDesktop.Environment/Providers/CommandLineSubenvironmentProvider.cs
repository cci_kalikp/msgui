﻿using System;
using System.Linq;
using Microsoft.Practices.Unity;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;

namespace MSDesktop.Environment.Providers
{
    internal static class CommandLineSubenvironmentProvider
    {
        public static string GetSubenvironment(IUnityContainer container_)
        {
            var showMsgBox = new Action(() => TaskDialog.ShowMessage(
                    string.Format("Application must be started with command line argument -subenv:subenvironment"),
                    "Startup Error",
                    TaskDialogCommonButtons.Close,
                    VistaTaskDialogIcon.Error));

            var envArg = System.Environment.GetCommandLineArgs().FirstOrDefault(x => x.StartsWith("-subenv:", StringComparison.InvariantCultureIgnoreCase));
            if (string.IsNullOrWhiteSpace(envArg))
            {
                showMsgBox();
                System.Environment.Exit(0);
            }

            var envString = envArg.Split(':').Skip(1).FirstOrDefault();
            if (string.IsNullOrEmpty(envString))
            {
                showMsgBox();
                System.Environment.Exit(0);
            }
            else
            {
                envString = envString.Trim('"', '\'');
            }
            return envString;
        }
    }
}
