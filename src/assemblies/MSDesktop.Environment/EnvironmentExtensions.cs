﻿using System;
using System.Windows.Media;
using MSDesktop.Environment.Providers;
using MorganStanley.MSDotNet.MSGui.Controls.Controls;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;

namespace MSDesktop.Environment
{
    public static class EnvironmentExtensions
    {
        public static readonly Color DefaultDevLabelColor = Colors.Green;
        public static readonly Color DefaultQaLabelColor = Colors.Yellow;
        public static readonly Color DefaultUatLabelColor = Colors.Orange;
        public static readonly Color DefaultProdLabelColor = Colors.Red;

        public static event EventHandler EnvironmentRegistered;
        
        private static void InvokeEnvironmentRegistered(EventArgs e_)
        {
            EventHandler handler = EnvironmentRegistered;
            if (handler != null) handler(null, e_);
        }

        public static void SetEnvironmentLabelColorMapping(
            this Framework framework_,
            Color devColor_,
            Color qaColor_,
            Color uatColor_,
            Color prodColor_)
        {
            EventHandler handler = null;
            handler = (sender_, args_) =>
                {
                    var environment = (MSDesktopEnvironmentImpl) framework_.Container.Resolve<IMSDesktopEnvironment>();
                    environment.DevLabelColor = new SolidColorBrush(devColor_);
                    environment.QaLabelColor = new SolidColorBrush(qaColor_);
                    environment.UatLabelColor = new SolidColorBrush(uatColor_);
                    environment.ProdLabelColor = new SolidColorBrush(prodColor_);
                    EnvironmentRegistered -= handler;
                };
            EnvironmentRegistered += handler;
        }
 
        public static void EnableEnvironment(
            this Framework framework_,
            Func<Environ> environProvider_,
            Func<EnvironmentRegion> regionProvider_,
            Func<string> usernameProvider_)
        {
            framework_.EnableEnvironment(environProvider_, regionProvider_, usernameProvider_, null);
        }

        public static void EnableEnvironment(
            this Framework framework_,
            Func<Environ> environProvider_,
            Func<EnvironmentRegion> regionProvider_,
            Func<string> usernameProvider_,
            Func<string> subenvironmentProvider_)
        {
            framework_.EnableEnvironment(environProvider_, regionProvider_, usernameProvider_, subenvironmentProvider_, false);
        }

        public static void EnableEnvironment(
            this Framework framework_,
            Func<Environ> environProvider_,
            Func<EnvironmentRegion> regionProvider_,
            Func<string> usernameProvider_,
            Func<string> subenvironmentProvider_,
            bool enableModernLabels_)
        {
            EnableEnvironment(framework_, container_ =>
            {
                var environment = new MSDesktopEnvironmentImpl();
                if (environProvider_ != null)
                {
                    environment.Environment = environProvider_.Invoke();
                }
                if (regionProvider_ != null)
                {
                    environment.Region = regionProvider_.Invoke();
                }
                if (usernameProvider_ != null)
                {
                    environment.User = usernameProvider_.Invoke();
                }
                if (subenvironmentProvider_ != null)
                {
                    environment.Subenvironment = subenvironmentProvider_.Invoke();
                }
                return environment;
            }, enableModernLabels_);
        }

        public static void EnableEnvironment(this Framework framework_,
            EnvironProviderId environProviderId_, RegionProviderId regionProviderId_, UsernameProviderId usernameProviderId_, bool enableModernLabels_ = false)
        {
            framework_.EnableEnvironment(environProviderId_, regionProviderId_, usernameProviderId_, SubenvironmentProviderId.None,
                enableModernLabels_:enableModernLabels_);
        }

        public static void EnableEnvironment(this Framework framework_,
            EnvironProviderId environProviderId_, RegionProviderId regionProviderId_, UsernameProviderId usernameProviderId_,
            SubenvironmentProviderId subenvironmentProviderId_, bool enableModernLabels_ = false)
        {
            EnableEnvironment(framework_, container_ => new MSDesktopEnvironmentImpl
            {
                Environment = GetEnvironProvider(environProviderId_).Invoke(container_),
                Region = GetRegionProvider(regionProviderId_).Invoke(container_),
                User = GetUsernameProvider(usernameProviderId_).Invoke(container_),
                Subenvironment = GetSubenvironmentProvider(subenvironmentProviderId_).Invoke(container_)
            }, enableModernLabels_: enableModernLabels_);
        }

        private static void EnableEnvironment(Framework framework_, Func<IUnityContainer, IMSDesktopEnvironment> fetchEnvironment_, bool enableModernLabels_=false)
        {
            EventHandler<ContainerEventArgs> handler = null;
            handler = delegate(object sender_, ContainerEventArgs args_)
            {
                var environment = fetchEnvironment_.Invoke(args_.Container);
                args_.Container.RegisterInstance(environment);

                var windowManager = args_.Container.Resolve<IWindowManager>() as DockViewModel;
                if (windowManager != null)
                {
                    var viewModel = new EnvironmentLabelViewModel(args_.Container.Resolve<IHidingLockingManager>())
                        {
                            Environment = environment,
                            LabelsEnabled = !enableModernLabels_,
                            ModernLabelsEnabled = enableModernLabels_
                        };
                    windowManager.EnvironmentLabelViewModel = viewModel;
                }

                InvokeEnvironmentRegistered(null);
                framework_.Bootstrapper.AfterPersistenceStorageInitialized -= handler;
            };
            framework_.Bootstrapper.AfterPersistenceStorageInitialized += handler;
        }

        private static Func<IUnityContainer, Environ> GetEnvironProvider(EnvironProviderId environProviderId_)
        {
            switch (environProviderId_)
            {
                case EnvironProviderId.CommandLine:
                    return CommandLineEnvironProvider.GetEnviron;
                case EnvironProviderId.ConcordProfile:
                    return ConcordProfileProviders.GetEnviron;
                case EnvironProviderId.ConcordConfig:
                    return ConcordConfigProviders.GetEnviron;
                default:
                    return container_ => new Environ();
            }
        }

        private static Func<IUnityContainer, EnvironmentRegion> GetRegionProvider(RegionProviderId regionProviderId_)
        {
            switch (regionProviderId_)
            {
                case RegionProviderId.ConcordConfig:
                    return ConcordConfigProviders.GetRegion;
                case RegionProviderId.ConcordProfile:
                    return ConcordProfileProviders.GetRegion;
                default:
                    return container_ => new EnvironmentRegion();
            }
        }

        private static Func<IUnityContainer, string> GetUsernameProvider(UsernameProviderId usernameProviderId_)
        {
            switch (usernameProviderId_)
            {
                case UsernameProviderId.ConcordProfile:
                    return ConcordProfileProviders.GetUser;
                default:
                    return container_ => "";
            }
        }

        private static Func<IUnityContainer, string> GetSubenvironmentProvider(SubenvironmentProviderId subenvirnomentProviderId_)
        {
            switch (subenvirnomentProviderId_)
            {
                case SubenvironmentProviderId.CommandLine:
                    return CommandLineSubenvironmentProvider.GetSubenvironment;
                default:
                    return container_ => null;
            }
        }

        public enum EnvironProviderId
        {
            None, CommandLine, ConcordProfile,
            ConcordConfig
        }

        public enum RegionProviderId
        {
            None,
            ConcordConfig,
            ConcordProfile
        }

        public enum UsernameProviderId
        {
            None, ConcordProfile
        }

        public enum SubenvironmentProviderId
        {
            None, CommandLine
        }
    }
}
