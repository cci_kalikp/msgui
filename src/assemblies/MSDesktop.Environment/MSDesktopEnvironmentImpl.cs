﻿using System.Windows.Media;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MSDesktop.Environment
{
    public class MSDesktopEnvironmentImpl : IMSDesktopEnvironment
    {
         
        public MSDesktopEnvironmentImpl()
        {
            DevLabelColor = new SolidColorBrush(EnvironmentExtensions.DefaultDevLabelColor);
            QaLabelColor = new SolidColorBrush(EnvironmentExtensions.DefaultQaLabelColor);
            UatLabelColor = new SolidColorBrush(EnvironmentExtensions.DefaultUatLabelColor);
            ProdLabelColor = new SolidColorBrush(EnvironmentExtensions.DefaultProdLabelColor);
        }
        public Environ Environment { get; set; }

        public EnvironmentRegion Region { get; set; }

        public string User { get; set; }

        public string Subenvironment { get; set; }

        public Brush DevLabelColor { get; internal set; }

        public Brush QaLabelColor { get; internal set; }

        public Brush UatLabelColor { get; internal set; }

        public Brush ProdLabelColor { get; internal set; }
    }
}
