﻿using System;
using System.Collections.Generic;

namespace MorganStanley.Desktop.EntityServices
{
    public interface IEntityServiceConnector
    {
        IEnumerable<object> Get(IEnumerable<object> nativeQueries);
        IObservable<IEnumerable<object>> GetObservable(IEnumerable<object> nativeQueries);
        void Publish(IEnumerable<object> nativeObjects);
        void Delete(IEnumerable<object> nativeQueries);
    }
}
