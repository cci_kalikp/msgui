﻿using MorganStanley.Desktop.EntityServices.Entities;

namespace MorganStanley.Desktop.EntityServices
{
    internal interface IEntityThreadedObservableService<TEntity> : IEntityService<TEntity>
        where TEntity : class, IEntity
    {
    }
}
