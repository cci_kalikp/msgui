﻿using System;
using System.Threading;
using MorganStanley.Desktop.EntityServices.Queries;

namespace MorganStanley.Desktop.EntityServices
{
    [Flags]
    public enum ServiceMode
    {
        None = 0,
        Tapped = 1,
        Caching = 2,
        Gating = 4,
        Replay = 8,
        Local = 16,
        Cloning = 32
    }

    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class EntityServiceAttribute : Attribute
    {
        private readonly ServiceMode _mode;

        /// <summary>
        /// Specifies the additional services requested as part of the Entity Service.
        /// Some services have their own attribute to support configuring the defaults of that service:
        /// EntityServiceConcurrencyAttribute : Used to control defaults for automatically applied concurrency
        /// EntityServiceCachingAttribute: Used to control defaults for the caching service
        /// </summary>
        /// <param name="mode"></param>
        public EntityServiceAttribute(ServiceMode mode)
        {
            _mode = mode;
        }
      
        /// <summary>
        /// To setup timeout, use EntityServiceCachingAttribute instead
        /// </summary>
        /// <param name="mode"></param>
        /// <param name="number"> no effect </param>
        [Obsolete]
        public EntityServiceAttribute(ServiceMode mode, int number) : this(mode)
        {   
        }

        public ServiceMode Mode { get { return _mode; } }

    }

    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class EntityServiceCachingAttribute : Attribute
    {
        public EntityServiceCachingAttribute()
        {
            TimeoutMinutes = 120;
            TimeoutMinutesFor32Bit = 30;
            AllowMemoryPressureFlushing = false;
            AllowMemoryPressureFlushingFor32Bit = false;
            IsTimeoutAbsolute = false;
            IsTimeoutAbsoluteFor32Bit = false;
            CachingOptions = CachingOptions.Default;
            WhenCaching = WhenCaching.DoDefault;
        }

        public int TimeoutMinutes { get; set; }
        public int TimeoutMinutesFor32Bit { get; set; }
        public bool AllowMemoryPressureFlushing { get; set; }
        public bool AllowMemoryPressureFlushingFor32Bit { get; set; }
        public bool IsTimeoutAbsolute { get; set; }
        public bool IsTimeoutAbsoluteFor32Bit { get; set; }
        public CachingOptions CachingOptions { get; set; }
        public WhenCaching WhenCaching { get; set; }

        internal TimeSpan GetCacheTimeout()
        {
            var timeout = Environment.Is64BitProcess ? TimeoutMinutes : TimeoutMinutesFor32Bit;
            return (timeout <= 0 || timeout == Timeout.Infinite)
                                ? TimeSpan.Zero
                                : TimeSpan.FromMinutes(timeout);
        }

        internal bool GetAllowMemoryPressureFlushing()
        {
            return Environment.Is64BitProcess ? AllowMemoryPressureFlushing : AllowMemoryPressureFlushingFor32Bit;
        }

        internal bool GetIsTimeoutAbsolute()
        {
            return Environment.Is64BitProcess ? IsTimeoutAbsolute : IsTimeoutAbsoluteFor32Bit;
        }
    }

    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class EntityServiceConcurrencyAttribute : Attribute
    {
        public EntityServiceConcurrencyAttribute()
        {
            MaximumConcurrency = -1;
            PerObservableMaximumConcurrency = -1;
        }

        public int MaximumConcurrency { get; set; }
        public int PerObservableMaximumConcurrency { get; set; }
    }
}
