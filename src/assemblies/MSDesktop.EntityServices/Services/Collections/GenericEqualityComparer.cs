﻿using System;
using System.Collections.Generic;

namespace MorganStanley.Desktop.EntityServices.Services.Collections
{
    internal class GenericEqualityComparer<T> : IEqualityComparer<T>
    {
        private readonly Func<T, T, bool> _equals;
        private readonly Func<T, int> _getHashCode;

        public GenericEqualityComparer(Func<T, T, bool> equals, Func<T, int> getHashCode)
        {
            _equals = equals;
            _getHashCode = getHashCode;
        }

        public bool Equals(T x, T y)
        {
            if (ReferenceEquals(x, y))
            {
                return true;
            }

            var xIsNull = ReferenceEquals(x, null);
            var yIsNull = ReferenceEquals(y, null);

            if (xIsNull && yIsNull)
            {
                return true;
            }

            if (xIsNull)
            {
                return false;
            }

            return !yIsNull && _equals(x, y);
        }

        public int GetHashCode(T obj)
        {
            return _getHashCode(obj);
        }
    }
}
