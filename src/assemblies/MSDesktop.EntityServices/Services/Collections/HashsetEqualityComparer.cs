﻿using System.Collections.Generic;
using System.Linq;

namespace MorganStanley.Desktop.EntityServices.Services.Collections
{
    internal class HashsetEqualityComparer<T> : GenericEqualityComparer<HashSet<T>>
        where T : class
    {
        public HashsetEqualityComparer()
            : base((x, y) => x.SetEquals(y),
                   x =>
                       {
                           unchecked
                           {
                               int result = 0;
                               foreach (var query in x.Where(query => query != null))
                               {
                                   result += (result*397) ^ query.GetHashCode();
                               }
                               return result;
                           }
                       }
                )
        {
        }
    }
}
