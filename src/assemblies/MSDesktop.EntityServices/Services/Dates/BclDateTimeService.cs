﻿using System;

namespace MorganStanley.Desktop.EntityServices.Services.Dates
{
    internal class BclDateTimeService : IDateTimeService
    {

        public DateTime Now()
        {
            return DateTime.Now;
        }

        public DateTime UtcNow()
        {
            return DateTime.UtcNow;
        }

        public DateTime Today()
        {
            return DateTime.Today;
        }

    }
}