﻿using System;

namespace MorganStanley.Desktop.EntityServices.Services.Dates
{
    internal interface IDateTimeService
    {
        DateTime Now();
        DateTime UtcNow();
        DateTime Today();
    }
}