﻿using MorganStanley.Desktop.EntityServices.Entities;

namespace MorganStanley.Desktop.EntityServices.Services.Cloning
{
    internal class NoOpEntityCloning : IEntityCloning
    {
        public T Clone<T>(T entity) where T : class, IEntity
        {
            return entity;
        }
    }
}
