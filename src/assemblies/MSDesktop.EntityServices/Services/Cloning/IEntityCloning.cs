﻿using MorganStanley.Desktop.EntityServices.Entities;

namespace MorganStanley.Desktop.EntityServices.Services.Cloning
{
    internal interface IEntityCloning
    {
        T Clone<T>(T entity)
            where T : class, IEntity;
    }
}
