﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using MorganStanley.Desktop.EntityServices.Entities;

namespace MorganStanley.Desktop.EntityServices.Services.Cloning
{
    internal class EntityCloning : IEntityCloning
    {
        public T Clone<T>(T entity) where T : class, IEntity
        {
            if (entity == null)
            {
                return null;
            }

            try
            {

                // Try to clone the object with the clone method on the object...
                var cloneableEntity = entity as IEntityServiceCloneable<T>;
                if (cloneableEntity != null)
                {
                    return cloneableEntity.Clone();
                }

                return DeepClone(entity);
            }
            catch (Exception ex)
            {
                throw new EntityServiceException(string.Format("Error while performing a clone of entity '{0}'", typeof(T).FullName), ex);
            }
        }

        private static T DeepClone<T>(T obj)
        {
            using (var ms = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(ms, obj);
                ms.Position = 0;
                return (T)formatter.Deserialize(ms);
            }
        }
    }
}
