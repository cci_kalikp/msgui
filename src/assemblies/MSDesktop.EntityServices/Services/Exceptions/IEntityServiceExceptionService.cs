﻿using System;

namespace MorganStanley.Desktop.EntityServices.Services.Exceptions
{
    public interface IEntityServiceExceptionService
    {
        void LogException(string message, Exception exception);
    }
}
