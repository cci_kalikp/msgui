﻿using System;
using log4net;

namespace MorganStanley.Desktop.EntityServices.Services.Exceptions
{
    internal class EntityServiceLoggingExceptionService : IEntityServiceExceptionService
    {
        private static readonly ILog _logger = LogManager.GetLogger("EntityServices");

        public void LogException(string message, Exception exception)
        {
            _logger.Error(message, exception);
        }
    }
}