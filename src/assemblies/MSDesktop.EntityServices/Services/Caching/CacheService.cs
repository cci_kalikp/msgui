﻿using System;
using System.Threading;

namespace MorganStanley.Desktop.EntityServices.Services.Caching
{
    internal class CacheService : ICacheService
    {
        public ICache<TKey, TValue> CreateCache<TKey, TValue>(TimeSpan defaultTimeout, bool isAbsolute, bool allowMemoryPressureFlushing, TryFuncWithTimeout<TKey, TValue> repopulationCallback)
        {
            return new SystemCache<TKey, TValue>(defaultTimeout, isAbsolute, allowMemoryPressureFlushing, repopulationCallback);
        }
    }
}
