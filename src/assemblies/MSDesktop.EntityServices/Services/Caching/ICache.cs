﻿using System;
using System.Collections.Generic;

namespace MorganStanley.Desktop.EntityServices.Services.Caching
{
    [Flags]
    internal enum RemovalState
    {
        Normal = 0,
        Evicted = 1,
        KeyRetrained = 2
    }

    internal class RemovedItem<TKey, TValue>
    {
        public RemovedItem(TKey key, TValue value, RemovalState removalState)
        {
            Key = key;
            Value = value;
            RemovalState = removalState;
        }

        public TKey Key { get; set; }
        public TValue Value { get; set; }
        public RemovalState RemovalState { get; private set; }
    }

    internal interface ICache<TKey, TValue> : IDictionary<TKey, TValue>, IObservable<RemovedItem<TKey, TValue>>
    {
        bool TryAdd(TKey key, TValue value);
        bool TryAdd(TKey key, TValue value, int slidingTimeoutInMinutes);
        bool TryAdd(TKey key, TValue value, TimeSpan slidingTimeout);
        bool TryAdd(TKey key, TValue value, DateTime absoluteTimeout);
        
        bool TryUpdate(TKey key, TValue newValue, TValue comparisonValue);
        bool TryUpdate(TKey key, TValue newValue, TValue comparisonValue, int slidingTimeoutInMinutes);
        bool TryUpdate(TKey key, TValue newValue, TValue comparisonValue, TimeSpan slidingTimeout);
        bool TryUpdate(TKey key, TValue newValue, TValue comparisonValue, DateTime absoluteTimeout);

        
        TValue GetOrAdd(TKey key, Func<TKey, TValue> valueFactory);
        TValue GetOrAdd(TKey key, Func<TKey, TValue> valueFactory, int slidingTimeoutInMinutes);
        TValue GetOrAdd(TKey key, Func<TKey, TValue> valueFactory, TimeSpan slidingTimeout);
        TValue GetOrAdd(TKey key, Func<TKey, TValue> valueFactory, DateTime absoluteTimeout);

        TValue GetOrAdd(TKey key, TryFunc<TKey, TValue> valueFactory);
        TValue GetOrAdd(TKey key, TryFunc<TKey, TValue> valueFactory, int slidingTimeoutInMinutes);
        TValue GetOrAdd(TKey key, TryFunc<TKey, TValue> valueFactory, TimeSpan slidingTimeout);
        TValue GetOrAdd(TKey key, TryFunc<TKey, TValue> valueFactory, DateTime absoluteTimeout);

        TValue GetOrAdd(TKey key, TValue value);
        TValue GetOrAdd(TKey key, TValue value, int slidingTimeoutInMinutes);
        TValue GetOrAdd(TKey key, TValue value, TimeSpan slidingTimeout);
        TValue GetOrAdd(TKey key, TValue value, DateTime absoluteTimeout);


        TValue AddOrUpdate(TKey key, Func<TKey, TValue> addValueFactory, Func<TKey, TValue, TValue> updateValueFactory);
        TValue AddOrUpdate(TKey key, Func<TKey, TValue> addValueFactory, Func<TKey, TValue, TValue> updateValueFactory, int slidingTimeoutInMinutes);
        TValue AddOrUpdate(TKey key, Func<TKey, TValue> addValueFactory, Func<TKey, TValue, TValue> updateValueFactory, TimeSpan slidingTimeout);
        TValue AddOrUpdate(TKey key, Func<TKey, TValue> addValueFactory, Func<TKey, TValue, TValue> updateValueFactory, DateTime absoluteTimeout);


        TValue AddOrUpdate(TKey key, TryFunc<TKey, TValue> addValueFactory, TryFunc<TKey, TValue, TValue> updateValueFactory);
        TValue AddOrUpdate(TKey key, TryFunc<TKey, TValue> addValueFactory, TryFunc<TKey, TValue, TValue> updateValueFactory, int slidingTimeoutInMinutes);
        TValue AddOrUpdate(TKey key, TryFunc<TKey, TValue> addValueFactory, TryFunc<TKey, TValue, TValue> updateValueFactory, TimeSpan slidingTimeout);
        TValue AddOrUpdate(TKey key, TryFunc<TKey, TValue> addValueFactory, TryFunc<TKey, TValue, TValue> updateValueFactory, DateTime absoluteTimeout);

        TValue AddOrUpdate(TKey key, TValue addValue, Func<TKey, TValue, TValue> updateValueFactory);
        TValue AddOrUpdate(TKey key, TValue addValue, Func<TKey, TValue, TValue> updateValueFactory, TimeSpan slidingTimeout);
        TValue AddOrUpdate(TKey key, TValue addValue, Func<TKey, TValue, TValue> updateValueFactory, int slidingTimeoutInMinutes);
        TValue AddOrUpdate(TKey key, TValue addValue, Func<TKey, TValue, TValue> updateValueFactory, DateTime absoluteTimeout);


        TValue AddOrUpdate(TKey key, TValue addValue, TryFunc<TKey, TValue, TValue> updateValueFactory);
        TValue AddOrUpdate(TKey key, TValue addValue, TryFunc<TKey, TValue, TValue> updateValueFactory, int slidingTimeoutInMinutes);
        TValue AddOrUpdate(TKey key, TValue addValue, TryFunc<TKey, TValue, TValue> updateValueFactory, TimeSpan slidingTimeout);
        TValue AddOrUpdate(TKey key, TValue addValue, TryFunc<TKey, TValue, TValue> updateValueFactory, DateTime absoluteTimeout);

        bool TryRemove(TKey key, out TValue value);
        KeyValuePair<TKey, TValue>[] ToArray();
    }
}
