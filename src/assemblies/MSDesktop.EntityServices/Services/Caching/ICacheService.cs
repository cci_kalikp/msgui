﻿using System;

namespace MorganStanley.Desktop.EntityServices.Services.Caching
{
    internal delegate bool TryFuncWithTimeout<in T, TResult>(T key, ref TimeSpan timeout, ref bool isAbsolute, out TResult value);

    internal interface ICacheService
    {
        /// <summary>
        /// Creates the cache.
        /// </summary>
        /// <typeparam name="TKey">The type of the key to the cache.</typeparam>
        /// <typeparam name="TValue">The type of the value held within the cache.</typeparam>
        /// <param name="defaultTimeout">The default timeout.  Use Timespan.Zero for no timeout.</param>
        /// <param name="isAbsoluteExpiry">
        /// If set to <c>true</c> the timeout is specified as an absolute timeout (an item will be evicted N time after insertion).  
        /// If set to <c>false</c> the timeout is specified as a sliding timeout (an item will be evicted from the cache N time after last access).
        /// </param>
        /// <param name="allowMemoryPressureFlushing">If set to <c>true</c> entries can be removed from the cache due to memory pressures</param>
        /// <param name="repopulationCallback">
        /// The repopulation callback.  If this is supplied, when an evision occurs the key is retrained. 
        /// If a request for the key is made later the callback is called so that the value can be reloaded.
        /// If the callback is not supplied or is null then when an exvition occurs both the key and the value will 
        /// be evicted and a fetch on the key at a later time will result in a "not in cache" situation.
        /// </param>
        /// <returns>The cache to use</returns>
        ICache<TKey, TValue> CreateCache<TKey, TValue>(TimeSpan defaultTimeout, bool isAbsoluteExpiry, bool allowMemoryPressureFlushing, TryFuncWithTimeout<TKey, TValue> repopulationCallback = null);
    }
}
