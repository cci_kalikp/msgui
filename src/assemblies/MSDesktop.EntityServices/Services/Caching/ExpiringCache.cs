﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Subjects;
using System.Runtime.InteropServices;
using System.Threading;

namespace MorganStanley.Desktop.EntityServices.Services.Caching
{
    [DebuggerDisplay("Count = {Count}")]
    [ComVisible(false)]
    [Serializable]
    internal abstract class ExpiringCache<TKey, TValue, TNativeKey> : IDictionary<TKey, TValue>, IObservable<RemovedItem<TKey, TValue>>
    {
        private readonly ConcurrentDictionary<TKey, CacheContainer> _mappings = new ConcurrentDictionary<TKey, CacheContainer>();

        private readonly Subject<RemovedItem<TKey, TValue>> _subject = new Subject<RemovedItem<TKey, TValue>>();

        private readonly TimeSpan _defaultTimeout;
        private readonly bool _defaultIsAbsoluteExpiry;
        private readonly TryFuncWithTimeout<TKey, TValue> _repopulationCallback;

        protected ExpiringCache(TimeSpan defaultTimeout, bool isAbsoluteExpiry, TryFuncWithTimeout<TKey, TValue> repopulationCallback)
        {
            _defaultTimeout = defaultTimeout;
            _defaultIsAbsoluteExpiry = isAbsoluteExpiry;
            _repopulationCallback = repopulationCallback;
        }

        protected void ItemEvicted(TNativeKey key, Tuple<TKey, TValue> cacheItem)
        {
            CacheContainer container;
            if (_mappings.TryRemove(cacheItem.Item1, out container))
            {
                bool isSet;
                lock (container)
                {
                    isSet = container.IsSet;
                }

                if (isSet)
                {
                    PublishItemRemoved(container.Key, 
                                       cacheItem.Item2,
                                       _repopulationCallback == null
                                           ? RemovalState.Evicted
                                           : RemovalState.Evicted | RemovalState.KeyRetrained);
                }
            }
        }

        protected abstract TNativeKey AddItem(TKey key, TValue cacheValue, TimeSpan? slidingTimeout, DateTime? absoluteTimeout);
        protected abstract void AddOrUpdateItem(TNativeKey nativeKey, TKey key, TValue cacheValue, TimeSpan? slidingTimeout, DateTime? absoluteTimeout);
        protected abstract bool TryRemoveItem(TNativeKey nativeKey, out Tuple<TKey, TValue> cacheItem);
        protected abstract bool TryGetItem(TNativeKey nativeKey, out Tuple<TKey, TValue> cacheItem);

        public IDisposable Subscribe(IObserver<RemovedItem<TKey, TValue>> observer)
        {
            return _subject.Subscribe(observer);
        }

        public bool IsEmpty { get { return _mappings.IsEmpty; } }

        #region IDictionary<TKey,TValue> Members

        public bool ContainsKey(TKey key)
        {
            return _mappings.ContainsKey(key);
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            CacheContainer container;
            if (_mappings.TryGetValue(key, out container))
            {
                if (TryGetItemFromUnderlyingCache(container, out value))
                {
                    return true;
                }
            }

            value = default(TValue);
            return false;
        }

        public void Clear()
        {
            var items = new List<RemovedItem<TKey, TValue>>();
            foreach (var pair in _mappings)
            {
                CacheContainer container;
                if (_mappings.TryRemove(pair.Key, out container))
                {
                    Tuple<TKey, TValue> item = null;
                    bool removeAttempted = false;
                    lock (container)
                    {
                        if (container.IsSet)
                        {
                            removeAttempted = true;
                            TryRemoveItem(container.NativeKey, out item);
                        }
                    }

                    if (removeAttempted)
                    {
                        items.Add(item != null
                                      ? new RemovedItem<TKey, TValue>(container.Key, item.Item2, RemovalState.Normal)
                                      : new RemovedItem<TKey, TValue>(container.Key, default(TValue), RemovalState.Normal | RemovalState.KeyRetrained)
                                 );
                    }
                }
            }

            foreach (var item in items)
            {
                PublishItemRemoved(item);
            }
        }

        void ICollection<KeyValuePair<TKey, TValue>>.CopyTo(KeyValuePair<TKey, TValue>[] array, int index)
        {
            GetTypedEnumeration().ToArray().CopyTo(array, index);
        }

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return GetTypedEnumeration().GetEnumerator();
        }

        void IDictionary<TKey, TValue>.Add(TKey key, TValue value)
        {
            if (!TryAdd(key, value))
            {
                throw new InvalidOperationException(string.Format("key '{0}' already exists in the cache.", key));
            }
        }

        bool IDictionary<TKey, TValue>.Remove(TKey key)
        {
            TValue value;
            return TryRemove(key, out value);
        }

        void ICollection<KeyValuePair<TKey, TValue>>.Add(KeyValuePair<TKey, TValue> keyValuePair)
        {
            ((IDictionary<TKey, TValue>)this).Add(keyValuePair.Key, keyValuePair.Value);
        }

        bool ICollection<KeyValuePair<TKey, TValue>>.Contains(KeyValuePair<TKey, TValue> keyValuePair)
        {
            CacheContainer container;
            if (!_mappings.TryGetValue(keyValuePair.Key, out container))
            {
                return false;
            }

            TValue value;
            return TryGetItemFromUnderlyingCache(container, out value) && EqualityComparer<TValue>.Default.Equals(value, keyValuePair.Value);
        }

        bool ICollection<KeyValuePair<TKey, TValue>>.Remove(KeyValuePair<TKey, TValue> keyValuePair)
        {
            CacheContainer container;
            if (!_mappings.TryRemove(keyValuePair.Key, out container))
            {
                return false;
            }

            Tuple<TKey, TValue> removedItem = null;
            bool removeAttempted = false;
            lock(container)
            {
                if (container.IsSet)
                {
                    removeAttempted = true;
                    if (TryRemoveItem(container.NativeKey, out removedItem))
                    {
                        if (!EqualityComparer<TValue>.Default.Equals(removedItem.Item2, keyValuePair.Value))
                        {
                            TryAdd(keyValuePair.Key, removedItem.Item2);
                            return false;
                        }
                    }
                }
            }

            if (removeAttempted)
            {
                if (removedItem != null)
                {
                    PublishItemRemoved(container.Key, removedItem.Item2, RemovalState.Normal);
                }
                else
                {
                    PublishItemRemoved(new RemovedItem<TKey, TValue>(keyValuePair.Key, default(TValue), RemovalState.Normal | RemovalState.KeyRetrained));
                }                
            }
            return true;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumeration().GetEnumerator();
        }

        public TValue this[TKey key]
        {
            get
            {
                TValue value;
                if (!TryGetValue(key, out value))
                {
                    throw new KeyNotFoundException();
                }

                return value;
            }
            set
            {
                AddOrUpdate(key, value, (localKey, orignialValue) => value);
            }
        }


        public int Count
        {
            get { return _mappings.Count; }
        }

        public ICollection<TKey> Keys
        {
            get { return _mappings.Keys; }
        }

        public ICollection<TValue> Values
        {
            get
            {
                return GetValues().ToArray();
            }
        }

        bool ICollection<KeyValuePair<TKey, TValue>>.IsReadOnly
        {
            get { return false; }
        }

        public KeyValuePair<TKey, TValue>[] ToArray()
        {
            return GetTypedEnumeration().ToArray();
        }

        #endregion

        public bool TryRemove(TKey key, out TValue value)
        {
            CacheContainer container;
            if (!_mappings.TryRemove(key, out container))
            {
                value = default(TValue);
                return false;
            }

            Tuple<TKey, TValue> item = null;
            bool removeAttempted = false;
            lock (container)
            {
                if (container.IsSet)
                {
                    removeAttempted = true;
                    TryRemoveItem(container.NativeKey, out item);
                }
            }

            if (removeAttempted)
            {
                if (item == null)
                {
                    PublishItemRemoved(new RemovedItem<TKey, TValue>(key, default(TValue), RemovalState.Normal | RemovalState.KeyRetrained));
                    value = default(TValue);
                }
                else
                {
                    PublishItemRemoved(container.Key, item.Item2, RemovalState.Normal);
                    value = item.Item2;
                }
                return true;
            }

            value = default(TValue);
            return false;
        }

        #region TryAdd(TKey key, TValue value)
        public bool TryAdd(TKey key, TValue value)
        {
            return _defaultIsAbsoluteExpiry ? TryAdd(key, value, null, DateTime.Now.Add(_defaultTimeout)) :
                                              TryAdd(key, value, _defaultTimeout, null);
        }
        public bool TryAdd(TKey key, TValue value, int slidingTimeoutInMinutes)
        {
            return TryAdd(key, value, CreateTimeSpan(slidingTimeoutInMinutes), null);
        }
        public bool TryAdd(TKey key, TValue value, TimeSpan slidingTimeout)
        {
            return TryAdd(key, value, slidingTimeout, null);
        }
        public bool TryAdd(TKey key, TValue value, DateTime absoluteTimeout)
        {
            return TryAdd(key, value, null, absoluteTimeout);
        }
        private bool TryAdd(TKey key, TValue value, TimeSpan? slidingTimeout, DateTime? absoluteTimeout)
        {
            var container = _mappings.GetOrAdd(key, localKey => new CacheContainer());
            lock (container)
            {
                if (container.IsSet)
                {
                    return false;
                }

                container.SetKeys(AddItem(key, value, slidingTimeout, absoluteTimeout), key);
                return true;
            }            
        }
        #endregion

        #region bool TryUpdate(TKey key, TValue newValue, TValue comparisonValue)
        public bool TryUpdate(TKey key, TValue newValue, TValue comparisonValue)
        {
            return _defaultIsAbsoluteExpiry ? TryUpdate(key, newValue, comparisonValue, null, DateTime.Now.Add(_defaultTimeout)) :
                                              TryUpdate(key, newValue, comparisonValue, _defaultTimeout, null);
        }
        public bool TryUpdate(TKey key, TValue newValue, TValue comparisonValue, int slidingTimeoutInMinutes)
        {
            return TryUpdate(key, newValue, comparisonValue, CreateTimeSpan(slidingTimeoutInMinutes), null);
        }
        public bool TryUpdate(TKey key, TValue newValue, TValue comparisonValue, TimeSpan slidingTimeout)
        {
            return TryUpdate(key, newValue, comparisonValue, slidingTimeout, null);
        }
        public bool TryUpdate(TKey key, TValue newValue, TValue comparisonValue, DateTime absoluteTimeout)
        {
            return TryUpdate(key, newValue, comparisonValue, null, absoluteTimeout);
        }
        private bool TryUpdate(TKey key, TValue newValue, TValue comparisonValue, TimeSpan? slidingTimeout, DateTime? absoluteTimeout)
        {
            CacheContainer container;
            if (!_mappings.TryGetValue(key, out container))
            {
                return false;
            }

            lock (container)
            {
                if (container.IsSet)
                {
                    Tuple<TKey, TValue> item;
                    if (TryGetItem(container.NativeKey, out item))
                    {
                        if (!EqualityComparer<TValue>.Default.Equals(item.Item2, comparisonValue))
                        {
                            return false;
                        }
                    }
                }

                AddOrUpdateItem(container.NativeKey, key, newValue, slidingTimeout, absoluteTimeout);
                return true;
            }            
        }
        #endregion

        #region TValue GetOrAdd(TKey key, TryFunc<TKey, TValue> valueFactory)
        public TValue GetOrAdd(TKey key, TryFunc<TKey, TValue> valueFactory)
        {
            return _defaultIsAbsoluteExpiry ? GetOrAdd(key, valueFactory, null, DateTime.Now.Add(_defaultTimeout)) :
                                              GetOrAdd(key, valueFactory, _defaultTimeout, null);
        }
        public TValue GetOrAdd(TKey key, TryFunc<TKey, TValue> valueFactory, int slidingTimeoutInMinutes)
        {
            return GetOrAdd(key, valueFactory, CreateTimeSpan(slidingTimeoutInMinutes), null);
        }
        public TValue GetOrAdd(TKey key, TryFunc<TKey, TValue> valueFactory, TimeSpan slidingTimeout)
        {
            return GetOrAdd(key, valueFactory, slidingTimeout, null);
        }
        public TValue GetOrAdd(TKey key, TryFunc<TKey, TValue> valueFactory, DateTime absoluteTimeout)
        {
            return GetOrAdd(key, valueFactory, null, absoluteTimeout);
        }
        private TValue GetOrAdd(TKey key, TryFunc<TKey, TValue> valueFactory, TimeSpan? slidingTimeout, DateTime? absoluteTimeout)
        {
            var container = _mappings.GetOrAdd(key, localKey => new CacheContainer());
            if (container.IsSet)
            {
                Tuple<TKey, TValue> item;
                if (TryGetItem(container.NativeKey, out item))
                {
                    return item.Item2;
                }
            }


            // if we're here then it's an add
            TValue newValue;
            if (!valueFactory(key, out newValue))
            {
                return newValue;
            }

            if (!container.IsSet)
            {
                lock (container)
                {
                    if (!container.IsSet)
                    {
                        container.SetKeys(AddItem(key, newValue, slidingTimeout, absoluteTimeout), key);
                        return newValue;
                    }
                }
            }

            //if we're here then the item was inserted from another thread
            lock (container)
            {
                Tuple<TKey, TValue> originalItem;
                if (TryGetItem(container.NativeKey, out originalItem))
                {
                    return originalItem.Item2;
                }

                AddOrUpdateItem(container.NativeKey, key, newValue, slidingTimeout, absoluteTimeout);
            }
            return newValue;
        }
        #endregion

        #region TValue GetOrAdd(TKey key, Func<TKey, TValue> valueFactory)
        public TValue GetOrAdd(TKey key, Func<TKey, TValue> valueFactory)
        {
            return _defaultIsAbsoluteExpiry ? GetOrAdd(key, valueFactory, null, DateTime.Now.Add(_defaultTimeout)) :
                                              GetOrAdd(key, valueFactory, _defaultTimeout, null);
        }
        public TValue GetOrAdd(TKey key, Func<TKey, TValue> valueFactory, int slidingTimeoutInMinutes)
        {
            return GetOrAdd(key, valueFactory, CreateTimeSpan(slidingTimeoutInMinutes), null);
        }
        public TValue GetOrAdd(TKey key, Func<TKey, TValue> valueFactory, TimeSpan slidingTimeout)
        {
            return GetOrAdd(key, valueFactory, slidingTimeout, null);
        }
        public TValue GetOrAdd(TKey key, Func<TKey, TValue> valueFactory, DateTime absoluteTimeout)
        {
            return GetOrAdd(key, valueFactory, null, absoluteTimeout);
        }
        private TValue GetOrAdd(TKey key, Func<TKey, TValue> valueFactory, TimeSpan? slidingTimeout, DateTime? absoluteTimeout)
        {
            return GetOrAdd(key, (TKey localKey, out TValue value) =>
            {
                value = valueFactory(key);
                return true;
            }, slidingTimeout, absoluteTimeout);
        }
        #endregion

        #region TValue GetOrAdd(TKey key, TValue value)
        public TValue GetOrAdd(TKey key, TValue value)
        {
            return GetOrAdd(key, localKey => value);
        }
        public TValue GetOrAdd(TKey key, TValue value, int slidingTimeoutInMinutes)
        {
            return GetOrAdd(key, localKey => value, slidingTimeoutInMinutes);
        }
        public TValue GetOrAdd(TKey key, TValue value, TimeSpan slidingTimeout)
        {
            return GetOrAdd(key, localKey => value, slidingTimeout);
        }
        public TValue GetOrAdd(TKey key, TValue value, DateTime absoluteTimeout)
        {
            return GetOrAdd(key, localKey => value, absoluteTimeout);
        }
        #endregion

        #region TValue AddOrUpdate(TKey key, TryFunc<TKey, TValue> addValueFactory, TryFunc<TKey, TValue, TValue> updateValueFactory)
        public TValue AddOrUpdate(TKey key, TryFunc<TKey, TValue> addValueFactory, TryFunc<TKey, TValue, TValue> updateValueFactory)
        {
            return _defaultIsAbsoluteExpiry ? AddOrUpdate(key, addValueFactory, updateValueFactory, null, DateTime.Now.Add(_defaultTimeout)) :
                                              AddOrUpdate(key, addValueFactory, updateValueFactory, _defaultTimeout, null);
        }
        public TValue AddOrUpdate(TKey key, TryFunc<TKey, TValue> addValueFactory, TryFunc<TKey, TValue, TValue> updateValueFactory, int slidingTimeoutInMinutes)
        {
            return AddOrUpdate(key, addValueFactory, updateValueFactory, CreateTimeSpan(slidingTimeoutInMinutes), null);
        }
        public TValue AddOrUpdate(TKey key, TryFunc<TKey, TValue> addValueFactory, TryFunc<TKey, TValue, TValue> updateValueFactory, TimeSpan slidingTimeout)
        {
            return AddOrUpdate(key, addValueFactory, updateValueFactory, slidingTimeout, null);
        }
        public TValue AddOrUpdate(TKey key, TryFunc<TKey, TValue> addValueFactory, TryFunc<TKey, TValue, TValue> updateValueFactory, DateTime absoluteTimeout)
        {
            return AddOrUpdate(key, addValueFactory, updateValueFactory, null, absoluteTimeout);
        }
        private TValue AddOrUpdate(TKey key, TryFunc<TKey, TValue> addValueFactory, TryFunc<TKey, TValue, TValue> updateValueFactory, TimeSpan? slidingTimeout, DateTime? absoluteTimeout)
        {
            var container = _mappings.GetOrAdd(key, localKey => new CacheContainer());

            if (container.IsSet)
            {
                return UpdateValue(key, container, addValueFactory, updateValueFactory, slidingTimeout, absoluteTimeout);
            }

            //it's a new item
            TValue newValue;
            if (!addValueFactory(key, out newValue))
            {
                return default(TValue);
            }

            if (!container.IsSet)
            {
                lock (container)
                {
                    if (!container.IsSet)
                    {
                        container.SetKeys(AddItem(key, newValue, slidingTimeout, absoluteTimeout), key);
                        return newValue;
                    }
                }
            }

            //before we could insert the new item something else did the insert underneath us so treat like an update!
            return UpdateValue(key, container, addValueFactory, updateValueFactory, slidingTimeout, absoluteTimeout);
        }
        #endregion

        #region TValue AddOrUpdate(TKey key, Func<TKey, TValue> addValueFactory, Func<TKey, TValue, TValue> updateValueFactory)
        public TValue AddOrUpdate(TKey key, Func<TKey, TValue> addValueFactory, Func<TKey, TValue, TValue> updateValueFactory)
        {
            return _defaultIsAbsoluteExpiry
                       ? AddOrUpdate(key, addValueFactory, updateValueFactory, null, DateTime.Now.Add(_defaultTimeout))
                       : AddOrUpdate(key, addValueFactory, updateValueFactory, _defaultTimeout, null);
        }
        public TValue AddOrUpdate(TKey key, Func<TKey, TValue> addValueFactory, Func<TKey, TValue, TValue> updateValueFactory, int slidingTimeoutInMinutes)
        {
            return AddOrUpdate(key, addValueFactory, updateValueFactory, CreateTimeSpan(slidingTimeoutInMinutes), null);
        }
        public TValue AddOrUpdate(TKey key, Func<TKey, TValue> addValueFactory, Func<TKey, TValue, TValue> updateValueFactory, TimeSpan slidingTimeout)
        {
            return AddOrUpdate(key, addValueFactory, updateValueFactory, slidingTimeout, null);
        }
        public TValue AddOrUpdate(TKey key, Func<TKey, TValue> addValueFactory, Func<TKey, TValue, TValue> updateValueFactory, DateTime absoluteTimeout)
        {
            return AddOrUpdate(key, addValueFactory, updateValueFactory, null, absoluteTimeout);
        }
        private TValue AddOrUpdate(TKey key, Func<TKey, TValue> addValueFactory, Func<TKey, TValue, TValue> updateValueFactory, TimeSpan? slidingTimeout, DateTime? absoluteTimeout)
        {
            return AddOrUpdate(
                key,
                (TKey localKey, out TValue value) =>
                {
                    value = addValueFactory(key);
                    return true;
                },
                (TKey localKey, TValue originalValue, out TValue value) =>
                {
                    value = updateValueFactory(localKey, originalValue);
                    return true;
                }, slidingTimeout, absoluteTimeout);
        }
        #endregion

        #region TValue AddOrUpdate(TKey key, TValue addValue, Func<TKey, TValue, TValue> updateValueFactory)
        public TValue AddOrUpdate(TKey key, TValue addValue, Func<TKey, TValue, TValue> updateValueFactory)
        {
            return AddOrUpdate(key, localKey => addValue, updateValueFactory);
        }
        public TValue AddOrUpdate(TKey key, TValue addValue, Func<TKey, TValue, TValue> updateValueFactory, int slidingTimeoutInMinutes)
        {
            return AddOrUpdate(key, localKey => addValue, updateValueFactory, slidingTimeoutInMinutes);
        }
        public TValue AddOrUpdate(TKey key, TValue addValue, Func<TKey, TValue, TValue> updateValueFactory, TimeSpan slidingTimeout)
        {
            return AddOrUpdate(key, localKey => addValue, updateValueFactory, slidingTimeout);
        }
        public TValue AddOrUpdate(TKey key, TValue addValue, Func<TKey, TValue, TValue> updateValueFactory, DateTime absoluteTimeout)
        {
            return AddOrUpdate(key, localKey => addValue, updateValueFactory, absoluteTimeout);
        }
        #endregion

        #region TValue AddOrUpdate(TKey key, TValue addValue, TryFunc<TKey, TValue, TValue> updateValueFactory)
        public TValue AddOrUpdate(TKey key, TValue addValue, TryFunc<TKey, TValue, TValue> updateValueFactory)
        {
            return _defaultIsAbsoluteExpiry ? AddOrUpdate(key, addValue, updateValueFactory, null, DateTime.Now.Add(_defaultTimeout)) :
                                              AddOrUpdate(key, addValue, updateValueFactory, _defaultTimeout, null);
        }
        public TValue AddOrUpdate(TKey key, TValue addValue, TryFunc<TKey, TValue, TValue> updateValueFactory, int slidingTimeoutInMinutes)
        {
            return AddOrUpdate(key, addValue, updateValueFactory, CreateTimeSpan(slidingTimeoutInMinutes), null);
        }
        public TValue AddOrUpdate(TKey key, TValue addValue, TryFunc<TKey, TValue, TValue> updateValueFactory, TimeSpan slidingTimeout)
        {
            return AddOrUpdate(key, addValue, updateValueFactory, slidingTimeout, null);
        }
        public TValue AddOrUpdate(TKey key, TValue addValue, TryFunc<TKey, TValue, TValue> updateValueFactory, DateTime absoluteTimeout)
        {
            return AddOrUpdate(key, addValue, updateValueFactory, null, absoluteTimeout);
        }
        private TValue AddOrUpdate(TKey key, TValue addValue, TryFunc<TKey, TValue, TValue> updateValueFactory, TimeSpan? slidingTimeout, DateTime? absoluteTimeout)
        {
            return AddOrUpdate(
                key,
                (TKey localKey, out TValue result) =>
                {
                    result = addValue;
                    return true;
                },
                updateValueFactory, slidingTimeout, absoluteTimeout);
        }
        #endregion

        //The container is expected to be fully established (IsSet = True) when calling this method
        private TValue UpdateValue(TKey key, CacheContainer container, TryFunc<TKey, TValue> addValueFactory, TryFunc<TKey, TValue, TValue> updateValueFactory, TimeSpan? slidingTimeout, DateTime? absoluteTimeout)
        {
            do
            {
                Tuple<TKey, TValue> originalItem;
                if (!TryGetItem(container.NativeKey, out originalItem))
                {
                    TValue newValue;
                    if (!addValueFactory(key, out newValue))
                    {
                        return default(TValue);
                    }

                    lock (container)
                    {
                        if (!TryGetItem(container.NativeKey, out originalItem))
                        {
                            AddOrUpdateItem(container.NativeKey, key, newValue, slidingTimeout, absoluteTimeout);
                            return newValue;
                        }
                    }
                }

                TValue updateValue;
                if (!updateValueFactory(key, originalItem.Item2, out updateValue))
                {
                    return originalItem.Item2;
                }

                lock (container)
                {
                    //if it's been evicted or the value we first looked at isn't the current one then we can't apply the update
                    Tuple<TKey, TValue> updatedOriginalItem;
                    if (!TryGetItem(container.NativeKey, out updatedOriginalItem) || Equals(updatedOriginalItem.Item2, originalItem.Item2))
                    {
                        AddOrUpdateItem(container.NativeKey, key, updateValue, slidingTimeout, absoluteTimeout);
                        return updateValue;
                    }
                }
            } while (true);
        }

        private bool TryGetItemFromUnderlyingCache(CacheContainer container, out TValue value)
        {
            if (!container.IsSet)
            {
                lock (container)
                {
                    if (!container.IsSet)
                    {
                        value = default(TValue);
                        return false;
                    }
                }
            }

            Tuple<TKey, TValue> item;
            if (TryGetItem(container.NativeKey, out item))
            {
                value = item.Item2;
                return true;
            }

            if (_repopulationCallback != null)
            {
                TimeSpan timeout = _defaultTimeout;
                bool isAbsolute = _defaultIsAbsoluteExpiry;
                if (_repopulationCallback(container.Key, ref timeout, ref isAbsolute, out value))
                {

                    lock (container)
                    {
                        if (isAbsolute)
                        {
                            AddOrUpdateItem(container.NativeKey, container.Key, value, null,
                                            DateTime.Now.Add(timeout));
                        }
                        else
                        {
                            AddOrUpdateItem(container.NativeKey, container.Key, value, timeout, null);
                        }
                    }
                    return true;
                }
            }


            value = default(TValue);
            return false;
        }

        private Tuple<TKey, TValue> GetItem(CacheContainer container)
        {
            Tuple<TKey, TValue> item;
            return container.IsSet && TryGetItem(container.NativeKey, out item) ? item : null;
        }

        private IEnumerable<TValue> GetValues()
        {
            return from mapping in _mappings
                   let item = GetItem(mapping.Value)
                   where item != null
                   select item.Item2;
        }

        private IEnumerable<KeyValuePair<TKey, TValue>> GetTypedEnumeration()
        {
            return from mapping in _mappings
                   let item = GetItem(mapping.Value)
                   where item != null
                   select new KeyValuePair<TKey, TValue>(item.Item1, item.Item2);
        }

        private IEnumerable GetEnumeration()
        {
            return from mapping in _mappings
                   let item = GetItem(mapping.Value)
                   where item != null
                   select new DictionaryEntry(item.Item1, item.Item2);
        }

        private void PublishItemRemoved(TKey key, TValue value, RemovalState removalState)
        {
            _subject.OnNext(new RemovedItem<TKey, TValue>(key, value, removalState));
        }

        private void PublishItemRemoved(RemovedItem<TKey, TValue> removedItem)
        {
            _subject.OnNext(removedItem);
        }

        private static TimeSpan CreateTimeSpan(int timeoutInMinutes)
        {
            if (timeoutInMinutes == Timeout.Infinite)
            {
                return TimeSpan.Zero;
            }

            if (timeoutInMinutes < 1)
            {
                throw new ArgumentOutOfRangeException("timeoutInMinutes", "The value of timeoutInMinutes is negative and is not equal to Timeout.Infinite in minutes.");
            }

            return new TimeSpan(TimeSpan.TicksPerMinute*timeoutInMinutes);
        }

        private class CacheContainer
        {
            private volatile bool  _isSet;

            public void SetKeys(TNativeKey nativeKey, TKey key)
            {
                //Order of these is important.  
                //"IsSet" must be done last so that only once everything is initiaised is everything "set"
                Key = key;
                NativeKey = nativeKey;
                _isSet = true;
            }

            public TNativeKey NativeKey { get; private set; }

            public TKey Key { get; private set; }

            public bool IsSet
            {
                get { return _isSet; }
            }
        }
    }
}
