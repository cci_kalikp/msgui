﻿using System;

namespace MorganStanley.Desktop.EntityServices.Services.Caching
{
    internal class CacheObject<T>
    {
        private readonly object _syncLock = new object();

        private bool _valueSet;
        private T _value;

        public bool SetValue(Func<T> getValue)
        {
            if (!_valueSet)
            {
                lock (_syncLock)
                {
                    if (!_valueSet)
                    {
                        _value = getValue();
                        _valueSet = true;
                        return true;
                    }
                }
            }
            return false;
        }

        public bool SetValue(TryFunc<T> getValue)
        {
            if (!_valueSet)
            {
                lock (_syncLock)
                {
                    if (!_valueSet)
                    {
                        T tempValue;
                        if (getValue(out tempValue))
                        {
                            _value = tempValue;
                            _valueSet = true;
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public T Value { get { return _value; } }
    }
}
