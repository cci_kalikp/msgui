﻿namespace MorganStanley.Desktop.EntityServices.Services.Caching
{
    internal delegate bool TryFunc<TResult>(out TResult value);
    internal delegate bool TryFunc<in T, TResult>(T t, out TResult value);
    internal delegate bool TryFunc<in T1, in T2, TResult>(T1 t1, T2 t2, out TResult value);
    internal delegate bool TryFunc<in T1, in T2, in T3, TResult>(T1 t1, T2 t2, T3 t3, out TResult value);
    internal delegate bool TryFunc<in T1, in T2, in T3, in T4, TResult>(T1 t1, T2 t2, T3 t3, T4 t4, out TResult value);
    internal delegate bool TryFunc<in T1, in T2, in T3, in T4, in T5, TResult>(T1 t1, T2 t2, T3 t3, T4 t4, T5 t5, out TResult value);
}
