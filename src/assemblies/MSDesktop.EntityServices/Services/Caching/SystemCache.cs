﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Runtime.Caching;

namespace MorganStanley.Desktop.EntityServices.Services.Caching
{
    [DebuggerDisplay("Count = {Count}")]
    [ComVisible(false)]
    [Serializable]
    internal class SystemCache<TKey, TValue> : ExpiringCache<TKey, TValue, string>, ICache<TKey, TValue>
    {
        private readonly bool _allowMemoryPressureFlushed;
        private readonly MemoryCache _cache = MemoryCache.Default;

        public SystemCache(TimeSpan defaultTimeout, bool isAbsolute, bool allowMemoryPressureFlushed, TryFuncWithTimeout<TKey, TValue> repopulationCallback)
            : base(defaultTimeout, isAbsolute, repopulationCallback)
        {
            _allowMemoryPressureFlushed = allowMemoryPressureFlushed;
        }

        protected override string AddItem(TKey cacheKey, TValue cacheValue, TimeSpan? slidingTimeout, DateTime? absoluteTimeout)
        {
            var key = Guid.NewGuid().ToString();
            AddOrUpdateItem(key, cacheKey, cacheValue, slidingTimeout, absoluteTimeout);
            return key;
        }

        protected override void AddOrUpdateItem(string key, TKey cacheKey, TValue cacheValue, TimeSpan? slidingTimeout, DateTime? absoluteTimeout)
        {
            CacheItemPolicy cacheItemPolicy = new CacheItemPolicy
            {
                Priority = _allowMemoryPressureFlushed ? CacheItemPriority.Default : CacheItemPriority.NotRemovable,
                RemovedCallback = CacheItemRemovedCallback,
                AbsoluteExpiration = ObjectCache.InfiniteAbsoluteExpiration,
                SlidingExpiration = ObjectCache.NoSlidingExpiration
            };

            if (slidingTimeout != null && (TimeSpan)slidingTimeout != TimeSpan.Zero)
            {
                cacheItemPolicy.SlidingExpiration = (TimeSpan)slidingTimeout;
            }
            else if (absoluteTimeout != null && absoluteTimeout != DateTime.MaxValue)
            {
                cacheItemPolicy.AbsoluteExpiration = (DateTime)absoluteTimeout;
            }
            else if (slidingTimeout == null && absoluteTimeout == null)
            {
                throw new InvalidOperationException("Neither an absolute or a sliding expiring tmoout has been supplied");
            }

            _cache.Set(key, Tuple.Create(cacheKey, cacheValue), cacheItemPolicy);
        }

        protected override bool TryRemoveItem(string key, out Tuple<TKey, TValue> cacheItem)
        {
            if (key == null)
            {
                cacheItem = null;
                return false;
            }

            var value = _cache.Remove(key);
            if (value != null)
            {
                cacheItem = (Tuple<TKey, TValue>)value;
                return true;
            }

            cacheItem = null;
            return false;
        }

        protected override bool TryGetItem(string key, out Tuple<TKey, TValue> cacheItem)
        {
            if (key != null)
            {
                var value = _cache.Get(key);
                if (value != null)
                {
                    cacheItem = (Tuple<TKey, TValue>)value;
                    return true;
                }
            }

            cacheItem = null;
            return false;
        }

        private void CacheItemRemovedCallback(CacheEntryRemovedArguments cacheEntryRemovedArgs)
        {
            if (cacheEntryRemovedArgs.RemovedReason == CacheEntryRemovedReason.Removed)
            {
                //If the item is manually removed from the cache then we're not interested
                return;
            }

            ItemEvicted(cacheEntryRemovedArgs.CacheItem.Key, (Tuple<TKey, TValue>)cacheEntryRemovedArgs.CacheItem.Value);
        }
    }
}
