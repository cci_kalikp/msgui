﻿using System;

namespace MorganStanley.Desktop.EntityServices
{
    public interface IGatingServiceObject
    {
        void SetGating(bool on);
        void Accept();
        bool IsGated();
        string Key { get; }
        DateTime LatestTime { get; }
        DateTime AcceptedTime { get; }
    }
}