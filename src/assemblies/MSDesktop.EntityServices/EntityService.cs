﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using MorganStanley.Desktop.EntityServices.Entities;
using MorganStanley.Desktop.EntityServices.Keys;
using MorganStanley.Desktop.EntityServices.Queries;
using MorganStanley.Desktop.EntityServices.Services.Exceptions;

namespace MorganStanley.Desktop.EntityServices
{
    internal sealed class EntityService<TEntity> : EntityPassthroughService<TEntity> where TEntity : class, IEntity
    {
        private readonly IEntityServiceConnector _dataConnector;
        private readonly IEntityServiceConverter<TEntity> _dataConverter;

        public EntityService(
            IEntityServiceConnector connector,
            IEntityServiceConverter<TEntity> converter,
            IEntityServiceExceptionService exceptionService)
            : base(null, exceptionService)
        {
            _dataConverter = converter;
            _dataConnector = connector;
        }

        public override IEnumerable<TFetchable> Get<TFetchable>(IEnumerable<IQuery<TFetchable>> queries)
        {
            var nonLocalQueries = queries.Where(query => query != null).Where(IsQueryRemote).Cast<IQuery<TEntity>>().ToList();
            if (nonLocalQueries.Count == 0)
            {
                return Enumerable.Empty<TFetchable>();
            }

            var nativeQueries = _dataConverter.ConvertFromQueriesToNativeFormat(nonLocalQueries, QueryConversionType.Fetching);
            if (nativeQueries == null)
            {
                return Enumerable.Empty<TFetchable>();
            }

            var nativeObjects = _dataConnector.Get(nativeQueries);
            return nativeObjects == null ? Enumerable.Empty<TFetchable>() : _dataConverter.ConvertFromNativeFormatToEntities(nativeObjects).OfType<TFetchable>();
        }

        public override IObservable<ObservableResource<TObservable>> GetObservable<TObservable>(IEnumerable<IQuery<TObservable>> queries)
        {
            return GetRefCountedObservable(queries, resolvedQueries =>
            {
                var nativeQueries = _dataConverter.ConvertFromQueriesToNativeFormat(resolvedQueries.Cast<IQuery<TEntity>>(), QueryConversionType.Subscribing);
                if (nativeQueries == null)
                {
                    return Observable.Empty<ObservableResource<TObservable>>();
                }

                return _dataConnector.GetObservable(nativeQueries)
                    .Select(nativeObjects =>
                    {
                        try
                        {
                            if (nativeObjects == null)
                            {
                                return null;
                            }
                            var entities = _dataConverter.ConvertFromNativeFormatToEntities(nativeObjects)
                                                            .Cast<TObservable>()
                                                            .ToList();
                            return entities.Count == 0 ? null : new ObservableResource<TObservable>(entities);
                        }
                        catch (Exception ex)
                        {
                            return new ObservableResource<TObservable>(new EntityServiceException(string.Format("CreateObserverable: Error converting from native format on entity '{0}' for queries '{1}'.", typeof(TEntity).FullName, string.Join(", ", queries.Select(query => query.ToString()))), ex));
                        }
                    })
                    .Where(resource => resource != null);
            });

        }

        public override void Publish<TPublishEntity>(IEnumerable<TPublishEntity> publishEntities)
        {
            var entities = publishEntities.Where(query => query != null)
                                          .Where(entity => IsQueryRemote(entity.Key))
                                          .Cast<IEntity<TEntity, IKey<TEntity>>>()
                                          .ToList();

            if (entities.Count == 0)
            {
                return;
            }

            var nativeEntities = _dataConverter.ConvertFromEntitiesToNativeFormat(entities);
            if (nativeEntities == null)
            {
                return;
            }

            _dataConnector.Publish(nativeEntities);
        }

        public override void Delete<TDeletable>(IEnumerable<IKey<TDeletable>> keys)
        {
            if (keys == null)
            {
                return;
            }

            var nativeKeys = _dataConverter.ConvertFromQueriesToNativeFormat(keys.Cast<IQuery<TEntity>>(), QueryConversionType.Deleting);
            if (nativeKeys == null)
            {
                return;
            }

            _dataConnector.Delete(nativeKeys);
        }

        private static bool IsQueryRemote<TLocalEntity>(IQuery<TLocalEntity> query)
        {
            var localKey = query as ILocalable;
            return localKey == null || !localKey.IsLocal;
        }
    }
}


