﻿using System;
using System.Collections.Generic;
using MorganStanley.Desktop.EntityServices.Entities;
using MorganStanley.Desktop.EntityServices.Keys;

namespace MorganStanley.Desktop.EntityServices
{
    public interface IEntityGatingService : 
        IObservable<GatingState>,
        IObservable<IGatingServiceObject> // Returns a stream of updates hitting the service irrespective of key
    {
        /// <summary>
        /// Accepts All Gated Values
        /// </summary>
        void Accept();

        /// <summary>
        /// Sets gating state for all keys
        /// </summary>
        /// <param name="on">Setting this to true turns gating on and setting it to false turns gating off</param>
        void SetGating(bool on);

        /// <summary>
        /// Returns a list of keys that have passed through the entity gating service since it was created
        /// </summary>
        IEnumerable<IGatingServiceObject> GetLatestObjects();

        /// <summary>
        /// Returns the gating state of this service 
        /// </summary>
        GatingState IsGated();

        ///// <summary>
        ///// Replays all the currently accepted entities for this service
        ///// </summary>
        //void ReplayAcceptedEntities();
    }

    public interface IEntityGatingService<TEntity> :
        IEntityGatingService,
        IEntityService<TEntity>
            where TEntity : class,IEntity
    {
        /// <summary>
        /// Sets gating state for a specific key
        /// </summary>
        /// <param name="key">Key to set gating for</param>
        /// <param name="on">Setting this to true turns gating on and setting it to false turns gating off</param>
        void SetGating(IKey<TEntity> key, bool on);

        /// <summary>
        /// Returns whether the service is currently gating values for a specific key
        /// </summary>
        /// <param name="key">Key that you are querying whether the service is gating values for</param>
        /// <returns>True if the service is gating values, false if the service is passing through any updates</returns>
        bool IsGated(IKey<TEntity> key);

        /// <summary>
        /// Accept a gated update (if one exists) for a given key
        /// Any values that don't match will remain gated
        /// </summary>
        /// <param name="key">Key to accept update for</param>
        void Accept(IKey<TEntity> key);

        /// <summary>
        /// Accept gated updates (if any exist) for the given keys
        /// Any values that don't match will remain gated
        /// </summary>
        /// <param name="keys">List of keys to accept updates for</param>
        void Accept(IEnumerable<IKey<TEntity>> keys);

        /// <summary>
        /// Accept all gated values (if any exist) that match the predicate
        /// Any values that don't match will remain gated
        /// </summary>
        /// <param name="predicate">Only values that match this predicate will be accepted</param>
        void Accept(Predicate<TEntity> predicate);

        /// <summary>
        /// Accept gated updates (if any exist) for the given keys that match the predicate
        /// Any values that don't match will remain gated
        /// </summary>
        /// <param name="key">Key to accept update for</param>
        /// <param name="predicate">Only values that match this predicate will be accepted</param>
        void Accept(IKey<TEntity> key, Predicate<TEntity> predicate);

        /// <summary>
        /// Accept gated updates (if any exist) for the given keys that match the predicate
        /// Any values that don't match will remain gated
        /// </summary>
        /// <param name="keys">List of keys to accept updates for</param>
        /// <param name="predicate">Only values that match this predicate will be accepted</param>
        void Accept(IEnumerable<IKey<TEntity>> keys, Predicate<TEntity> predicate);
    }
}
