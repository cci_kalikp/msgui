﻿using System;

namespace MorganStanley.Desktop.EntityServices
{
    public class GatingServiceObject : IGatingServiceObject
    {
        public GatingServiceObject(
            string key, 
            DateTime latestTime, 
            DateTime acceptedTime,
            Action<bool> setGating,
            Action accept,
            Func<bool> isGated)
        {
            _key = key;
            _acceptedTime = acceptedTime;
            _setGating = setGating;
            _accept = accept;
            _isGated = isGated;
            _latestTime = latestTime;
        }

        private readonly string _key;
        private readonly DateTime _latestTime;
        private readonly DateTime _acceptedTime;
        private readonly Action<bool> _setGating;
        private readonly Action _accept;
        private readonly Func<bool> _isGated;

        public void SetGating(bool on)
        {
            _setGating(on);
        }

        public void Accept()
        {
            _accept();
        }

        public bool IsGated()
        {
            return _isGated();
        }

        public string Key { get { return _key; } }
        public DateTime LatestTime { get { return _latestTime; } }
        public DateTime AcceptedTime { get { return _acceptedTime; } }
    }
}