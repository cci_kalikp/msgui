﻿using MorganStanley.Desktop.EntityServices.Entities;

namespace MorganStanley.Desktop.EntityServices
{
    internal interface IEntityCloningService<TEntity> : IEntityService<TEntity>
        where TEntity : class, IEntity
    {
    }
}