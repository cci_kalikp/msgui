﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using MorganStanley.Desktop.EntityServices.Entities;
using MorganStanley.Desktop.EntityServices.Keys;
using MorganStanley.Desktop.EntityServices.Queries;
using MorganStanley.Desktop.EntityServices.Services.Exceptions;

namespace MorganStanley.Desktop.EntityServices
{
    internal sealed class EntityLocalService<TEntity> : EntityPassthroughService<TEntity>, IEntityLocalService<TEntity>
        where TEntity : class, IEntity
    {
        
        //    // ASSUMPTIONS MADE FOR THIS SERVICE...
        //    // 1. TPublishObject = TEntity
        //    // 2. When you do a Get on an IQuery that is not an IKey there is no way for us to check matches in the cache so we must go to the backing EntityService


        private readonly ConcurrentDictionary<IKey, TEntity> _localEntities = new ConcurrentDictionary<IKey, TEntity>();
        private readonly ISubject<IEnumerable<TEntity>> _subject;

        public EntityLocalService(IEntityService<TEntity> entityService, IEntityServiceExceptionService exceptionService)
            : base(entityService, exceptionService)
        {
            _subject = new Subject<IEnumerable<TEntity>>();
        }

        public override IEnumerable<TFetchable> Get<TFetchable>(IEnumerable<IQuery<TFetchable>> queries)
        {
            var entities = new List<TFetchable>();

            var queriesForExecution = new List<IQuery<TFetchable>>();
            foreach (var query in queries)
            {
                var localableInput = query as ILocalable;
                if (localableInput != null && localableInput.IsLocal & query is IKey)
                {
                    TEntity entity;
                    if (_localEntities.TryGetValue((IKey) query, out entity))
                    {
                        entities.Add((TFetchable)entity);
                    }
                }
                else
                {
                    queriesForExecution.Add(query);
                }
            }

            return queriesForExecution.Count != 0 ? entities.Union(base.Get(queriesForExecution)) : entities;
        }

        public override IObservable<ObservableResource<TObservable>> GetObservable<TObservable>(IEnumerable<IQuery<TObservable>> queries)
        {
            var localKeys = new HashSet<IKey>();
            var remoteQueries = new List<IQuery<TObservable>>();
            foreach (var query in queries)
            {
                var localableInput = query as ILocalable;
                if (localableInput != null && localableInput.IsLocal && query is IKey)
                {
                    localKeys.Add((IKey)query);
                }
                else
                {
                    remoteQueries.Add(query);
                }
            }

            if (localKeys.Count == 0 && remoteQueries.Count == 0)
            {
                return Observable.Empty<ObservableResource<TObservable>>();
            }

            if (localKeys.Count == 0)
            {
                return base.GetObservable(remoteQueries);
            }

            var matchingLocalCacheUpdates = Observable.Create<ObservableResource<TObservable>>(o =>
                {
                    var subscription = _subject.Subscribe(entities =>
                        { 
                            try
                            {
                                var localEntities = entities.Where(entity => entity != null)
                                                            .Where(entity => localKeys.Contains(entity.Key))
                                                            .ToList();
                                if (localEntities.Count > 0)
                                {
                                    o.OnNext(new ObservableResource<TObservable>(localEntities.Cast<TObservable>()));
                                }
                            }
                            catch (Exception ex)
                            {
                                o.OnNext(new ObservableResource<TObservable>(new EntityServiceException(string.Format("GetObserverable error from locally published object on entity '{0}' for queries '{1}'.", typeof(TEntity).Name, string.Join(", ", queries.Select(query => query.ToString()))), ex)));
                            }
                        },
                    o.OnError,
                    o.OnCompleted);
                    return subscription;
                });

            return remoteQueries.Count == 0
                       ? matchingLocalCacheUpdates
                       : matchingLocalCacheUpdates.Merge(base.GetObservable(remoteQueries));        
        }

        public override void Publish<TPublishEntity>(IEnumerable<TPublishEntity> publishEntities)
        {
            var localEntities = new List<TEntity>();
            var remoteEntities = new List<TPublishEntity>();
            foreach (var publishEntity in publishEntities)
            {
                var localableInput = publishEntity.Key as ILocalable;
                if (localableInput != null && localableInput.IsLocal)
                {
                    //local entities must be of type TEntity
                    var entity = publishEntity as TEntity;
                    if (entity == null)
                    {
                        throw new InvalidOperationException(string.Format("Published entity must be of type {0} and cannot be null", typeof(TEntity).FullName));
                    }
                    _localEntities.AddOrUpdate(entity.Key, entity, (key, originalEntity) => entity);
                    localEntities.Add(entity);
                }
                else
                {
                    remoteEntities.Add(publishEntity);
                }
            }  

            if (remoteEntities.Count > 0)
            {
                base.Publish(remoteEntities);
            }

            if (localEntities.Count > 0)
            {
                _subject.OnNext(localEntities);
            }
        }

        public override void Delete<TDeletable>(IEnumerable<IKey<TDeletable>> keys)
        {
            var remoteKeys = new List<IKey<TDeletable>>();
            foreach (var key in keys)
            {
                var localableInput = key as ILocalable;
                if (localableInput != null && localableInput.IsLocal)
                {
                    TEntity removedEntity;
                    _localEntities.TryRemove(key, out removedEntity);
                }
                else
                {
                    remoteKeys.Add(key);
                }
            }

            if (remoteKeys.Count > 0)
            {
                base.Delete(remoteKeys);
            }
        }
    }
}