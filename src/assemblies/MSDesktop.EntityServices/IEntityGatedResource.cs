﻿using System;
using MorganStanley.Desktop.EntityServices.Keys;

namespace MorganStanley.Desktop.EntityServices
{
    public interface IEntityGatedResource<TEntity> :
        IEntityGatedResource,
        IEquatable<IEntityGatedResource<TEntity>>,
        IComparable,
        IComparable<IEntityGatedResource<TEntity>>
    {
        IEntityResourceKey<TEntity> ResourceKey { get; }
        TEntity AcceptedInstance { get; set; }
        TEntity GatedInstance { get; set; }
    }

    public interface IEntityGatedResource
    {
        string KeyText { get; }
        DateTime? AcceptedTimestamp { get; set; }
        DateTime? CurrentTimestamp { get; set; }
        Type EntityType { get; }
        string Symbol { get; }
        string SubId { get; }
        void Accept();
        void Gate();
        void Ungate();
        bool IsGated { get; }
        IEntityResourceKey EntityResourceKey { get; }
    }
}
