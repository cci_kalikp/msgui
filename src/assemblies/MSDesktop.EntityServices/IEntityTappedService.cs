﻿using System;
using MorganStanley.Desktop.EntityServices.Entities;

namespace MorganStanley.Desktop.EntityServices
{
    /// <summary>
    /// Acts as a tap, monitoring all traffic through the entity service stack for this TEntity
    /// </summary>
    /// <typeparam name="TEntity">Entity stream to be tapped</typeparam>
    public interface IEntityTappedService<TEntity> : IEntityService<TEntity>, IObservable<ObservableResource<TEntity>>
        where TEntity : class, IEntity
    {
    }
}
