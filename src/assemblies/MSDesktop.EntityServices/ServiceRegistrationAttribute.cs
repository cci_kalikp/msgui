﻿using System;

namespace MorganStanley.Desktop.EntityServices
{
    [FlagsAttribute]
    public enum ServiceMode
    {

        Default = 0x0,
        Tapped = 0x1,
        Caching = 0x2,
        UpdateCaching = 0x4,
        StaticCaching = 0x8,
        Gating = 0x10,
        Replay = 0x20, 
        Local = 0x40,
        Cloning = 0x80,
        

        

        /**
        
        CachingWithUpdates,
        

        /// <summary>
        /// service -> gatingService -> localService -> client
        /// </summary>
        GatingWithLocal,
        StaticCaching,

        /// <summary>
        /// service -> updatableCachingService -> gatingService -> client
        /// </summary>
        GatingAndCachingWithUpdates,

        /// <summary>
        /// service -> updatableCachingService -> gatingService -> localService -> client
        /// </summary>
        LocallyUpdatableWithGatingAndCachingWithUpdates,

        /// <summary>
        /// service -> cachingService -> gatingService -> client
        /// </summary>
        GatingAndCaching,

        /// <summary>
        /// service -> tapped -> updatableCachingService -> client
        /// </summary>
        TappedCachingWithUpdates,
       

        /// <summary>
        /// service -> tapped -> cachingService -> client
        /// </summary>
        TappedCaching
         * */
    }

    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class ServiceRegistrationAttribute : Attribute
    {
        private readonly ServiceMode _mode;

        public ServiceRegistrationAttribute(ServiceMode mode)
        {
            _mode = mode;
            CacheTimeoutMinutes = 60; // 1 Hour
        }

        public ServiceRegistrationAttribute()
            : this(ServiceMode.Default)
        {
        }

        public int CacheTimeoutMinutes { get; set; }
        public TimeSpan CacheTimeout
        {
            get { return TimeSpan.FromMinutes(CacheTimeoutMinutes); }
        }

        public Type EntityType { get; set; }

        public ServiceMode Mode
        {
            get { return _mode; }
        }
    }
}
