﻿using System;
using System.Collections.Generic;
using System.Reactive;
using System.Reactive.Linq;
using MorganStanley.Desktop.EntityServices.Entities;
using MorganStanley.Desktop.EntityServices.Keys;
using MorganStanley.Desktop.EntityServices.Queries;
using MorganStanley.Desktop.EntityServices.Services.Exceptions;

namespace MorganStanley.Desktop.EntityServices
{
    public static class EntityServiceExtensions
    {
        private static readonly IEntityServiceExceptionService _defaultExceptionService = new EntityServiceLoggingExceptionService();

        public static IEnumerable<TFetchable> Get<TFetchable>(this IEntityService<TFetchable> entityService, IQuery<TFetchable> query)
                where TFetchable : class, IFetchable, IEntity
        {
            return entityService.Get(new[] { query });
        }

        public static IObservable<TObservable> GetObservable<TObservable>(this IEntityService<TObservable> entityService, IQuery<TObservable> query, IEntityServiceExceptionService exceptionService = null) 
            where TObservable : class, ISubscribable, IEntity
        {
            return entityService.GetObservable(new[] {query})
                                .Select(resource =>
                                            {
                                                if (!resource.IsFaulted)
                                                {
                                                    return resource.Entities;
                                                }

                                                if (exceptionService == null)
                                                {
                                                    IEntityServiceExceptionFetcher exceptionServiceFetcher = entityService as IEntityServiceExceptionFetcher;
                                                    exceptionService = exceptionServiceFetcher != null
                                                                           ? exceptionServiceFetcher.GetExceptionService() ?? _defaultExceptionService
                                                                           : _defaultExceptionService;
                                                }
                                                exceptionService.LogException(string.Format("There was an exception while receiving entity type '{0}'", typeof (TObservable).FullName), resource.Exception);
                                                return null;
                                            })
                                .Where(resource => resource != null)
                                .SelectMany(entities => entities);
        }

 

 


        public static void Publish<TEntity, TPublishEntity>(this IEntityService<TEntity> entityService, TPublishEntity publishEntity)
            where TPublishEntity : class, IPublishable, IEntity<TEntity, IKey<TEntity>>
            where TEntity : class, IEntity
        {
            entityService.Publish(new[] { publishEntity });
        }

        public static void Delete<TDeletable>(this IEntityService<TDeletable> entityService, IKey<TDeletable> key)
            where TDeletable : class, IEntity, IDeletable
        {
            entityService.Delete(new[] { key });
        }

        public static IDisposable Subscribe<TEntity>(this IEntityTappedService<TEntity> tappedEntityService, Action<TEntity> onNext, IEntityServiceExceptionService exceptionService = null)
            where TEntity : class, IEntity
        {
            if (tappedEntityService == null)
            {
                throw new ArgumentNullException("tappedEntityService");
            }
            if (onNext == null)
            {
                throw new ArgumentNullException("onNext");
            }

            return tappedEntityService.Subscribe(new AnonymousObserver<TEntity>(onNext), exceptionService);
        }

        public static IDisposable Subscribe<TEntity>(this IEntityTappedService<TEntity> tappedEntityService, Action<TEntity> onNext, Action<Exception> onError, IEntityServiceExceptionService exceptionService = null)
            where TEntity : class, IEntity
        {
            if (tappedEntityService == null)
            {
                throw new ArgumentNullException("tappedEntityService");
            }
            if (onNext == null)
            {
                throw new ArgumentNullException("onNext");
            }

            if (onError == null)
            {
                throw new ArgumentNullException("onError");
            }

            return tappedEntityService.Subscribe(new AnonymousObserver<TEntity>(onNext, onError), exceptionService);
        }


        public static IDisposable Subscribe<TEntity>(this IEntityTappedService<TEntity> tappedEntityService, Action<TEntity> onNext, Action onCompleted, IEntityServiceExceptionService exceptionService = null)
            where TEntity : class, IEntity
        {
            if (tappedEntityService == null)
            {
                throw new ArgumentNullException("tappedEntityService");
            }
            if (onNext == null)
            {
                throw new ArgumentNullException("onNext");
            }

            if (onCompleted == null)
            {
                throw new ArgumentNullException("onCompleted");
            }

            return tappedEntityService.Subscribe(new AnonymousObserver<TEntity>(onNext, onCompleted), exceptionService);
        }


        public static IDisposable Subscribe<TEntity>(this IEntityTappedService<TEntity> tappedEntityService, Action<TEntity> onNext, Action<Exception> onError, Action onCompleted, IEntityServiceExceptionService exceptionService = null)
            where TEntity : class, IEntity
        {
            if (tappedEntityService == null)
            {
                throw new ArgumentNullException("tappedEntityService");
            }
            if (onNext == null)
            {
                throw new ArgumentNullException("onNext");
            }

            if (onError == null)
            {
                throw new ArgumentNullException("onError");
            }

            if (onCompleted == null)
            {
                throw new ArgumentNullException("onCompleted");
            }

            return tappedEntityService.Subscribe(new AnonymousObserver<TEntity>(onNext, onError, onCompleted), exceptionService);
        }

        public static IDisposable Subscribe<TEntity>(this IEntityTappedService<TEntity> tappedEntityService, IObserver<TEntity> observer, IEntityServiceExceptionService exceptionService = null)
            where TEntity : class, IEntity
        {
            return tappedEntityService
                                .Select(resource =>
                                {
                                    if (resource.IsFaulted)
                                    {
                                        if (exceptionService == null)
                                        {
                                            var exceptionServiceFetcher = tappedEntityService as IEntityServiceExceptionFetcher;
                                            exceptionService = exceptionServiceFetcher != null ? 
                                                            exceptionServiceFetcher.GetExceptionService() ?? _defaultExceptionService : 
                                                            _defaultExceptionService;
                                        }

                                        exceptionService.LogException(string.Format("There was an exception while receiving entity type '{0}'", typeof(TEntity).FullName), resource.Exception);
                                        return null;
                                    }
                                    return resource.Entities;
                                })
                                .Where(resource => resource != null)
                                .SelectMany(entities => entities)
                                .Subscribe(observer);
        }
    }
}