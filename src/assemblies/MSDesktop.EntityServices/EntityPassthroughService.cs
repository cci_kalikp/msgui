﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using MorganStanley.Desktop.EntityServices.Entities;
using MorganStanley.Desktop.EntityServices.Keys;
using MorganStanley.Desktop.EntityServices.Queries;
using MorganStanley.Desktop.EntityServices.Services.Caching;
using MorganStanley.Desktop.EntityServices.Services.Collections;
using MorganStanley.Desktop.EntityServices.Services.Exceptions;

namespace MorganStanley.Desktop.EntityServices
{
    internal abstract class EntityPassthroughService<TEntity> : IEntityService<TEntity>, IEntityServiceExceptionFetcher
        where TEntity : class, IEntity
    {
        private readonly IEntityService<TEntity> _entityService;
        private readonly IEntityServiceExceptionService _exceptionService;
        private readonly ConcurrentDictionary<HashSet<IQuery>, CacheObject<IObservable<ObservableResource<TEntity>>>> _querysToObservable = new ConcurrentDictionary<HashSet<IQuery>, CacheObject<IObservable<ObservableResource<TEntity>>>>(new HashsetEqualityComparer<IQuery>());

        protected EntityPassthroughService(IEntityService<TEntity> entityService, IEntityServiceExceptionService exceptionService)
        {
            _entityService = entityService;
            _exceptionService = exceptionService;
        }

        public virtual IEnumerable<TFetchable> Get<TFetchable>(IEnumerable<IQuery<TFetchable>> queries) where TFetchable : class, TEntity, IFetchable
        {
            return _entityService.Get(queries);
        }

        public virtual IObservable<ObservableResource<TObservable>> GetObservable<TObservable>(IEnumerable<IQuery<TObservable>> queries) where TObservable : class, TEntity, ISubscribable
        {
            return _entityService.GetObservable(queries);
        }

        public virtual void Publish<TPublishEntity>(IEnumerable<TPublishEntity> publishEntities) where TPublishEntity : class, IPublishable, IEntity<TEntity, IKey<TEntity>>
        {
            _entityService.Publish(publishEntities);
        }

        public virtual void Delete<TDeletable>(IEnumerable<IKey<TDeletable>> keys) where TDeletable : class, TEntity, IDeletable
        {
            _entityService.Delete(keys);
        }

        IEntityServiceExceptionService IEntityServiceExceptionFetcher.GetExceptionService()
        {
            return _exceptionService;
        }

        protected IObservable<ObservableResource<TObservable>> GetRefCountedObservable<TObservable>(IEnumerable<IQuery<TObservable>> queries, Func<IEnumerable<IQuery<TObservable>>, IObservable<ObservableResource<TObservable>>> observable) where TObservable : class, IEntity, TEntity, ISubscribable
        {
            var resolvedQueries = new HashSet<IQuery>();
            foreach (var query in queries.Where(query => query != null))
            {
                resolvedQueries.Add(query);
            }

            if (resolvedQueries.Count == 0)
            {
                return Observable.Empty<ObservableResource<TObservable>>();
            }

            var cachedObservable = _querysToObservable.GetOrAdd(resolvedQueries, key => new CacheObject<IObservable<ObservableResource<TEntity>>>());
            cachedObservable.SetValue(() => observable(resolvedQueries.Cast<IQuery<TObservable>>()).Do(entities => { },
                                                                          () =>
                                                                          {
                                                                              CacheObject<IObservable<ObservableResource<TEntity>>> removedItem;
                                                                              _querysToObservable.TryRemove(resolvedQueries, out removedItem);
                                                                          })
                                                                       .Cast<ObservableResource<TEntity>>()
                                                                       .Publish()
                                                                       .RefCount());
            return cachedObservable.Value.Cast<ObservableResource<TObservable>>();
        }
    }
}
