﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MorganStanley.Desktop.EntityServices.Services.Collections;
using log4net;
using MorganStanley.Desktop.EntityServices.Entities;
using MorganStanley.Desktop.EntityServices.Keys;
using MorganStanley.Desktop.EntityServices.Queries;
using MorganStanley.Desktop.EntityServices.Services.Caching;
using MorganStanley.Desktop.EntityServices.Services.Exceptions;

namespace MorganStanley.Desktop.EntityServices
{
    internal class EntityStaticCachingService<TEntity> : EntityPassthroughService<TEntity>, IEntityStaticCachingService<TEntity>
        where TEntity : class, IEntity, IFetchable
    {
        private readonly ILog _log = LogManager.GetLogger("EntityCachingService-" + typeof(TEntity));
        private readonly ICache<IQuery, IEnumerable<IKey>> _queryToKeysCache;
        private readonly ConcurrentDictionary<IQuery, ConcurrentDictionary<IKey, TEntity>> _atomicQueryCache;
        private readonly EntityServiceCachingAttribute _cachingDefaults;



        //This cache is used to trigger a removal from the entitycache.  
        //We don't use an expiring cache for the entitycache becuase updates coming from the updating cache means that items in it may never expiry
        private readonly ICache<IOneToOneQuery, bool> _keyToEntityExpiringMechanism;
        private readonly ConcurrentDictionary<IOneToOneQuery, TEntity> _keyToEntityCache = new ConcurrentDictionary<IOneToOneQuery, TEntity>();


        public EntityStaticCachingService(IEntityService<TEntity> entityService, IEntityServiceExceptionService exceptionService, ICacheService cacheService, EntityServiceCachingAttribute cachingDefaults)
            : base(entityService, exceptionService)
        {
            _cachingDefaults = cachingDefaults;
            var cacheTimeout = cachingDefaults.GetCacheTimeout();
            var allowFlushing = cachingDefaults.GetAllowMemoryPressureFlushing();
            var isAbsolute = cachingDefaults.GetIsTimeoutAbsolute();

            _atomicQueryCache = new ConcurrentDictionary<IQuery, ConcurrentDictionary<IKey, TEntity>>();
            _queryToKeysCache = cacheService.CreateCache<IQuery, IEnumerable<IKey>>(cacheTimeout, isAbsolute, allowFlushing);

            _queryToKeysCache.Subscribe(removedItem =>
            {
                if (_log.IsDebugEnabled)
                {
                    _log.DebugFormat("Item removed from query cache with query '{0}'", removedItem.Key);
                }

                UnsubscribeFromUpdates(removedItem.Key);

                //Remove query from the large query optimization cache
                ConcurrentDictionary<IKey, TEntity> dummy;
                _atomicQueryCache.TryRemove(removedItem.Key, out dummy);
            });

            if (cacheTimeout != TimeSpan.Zero)
            {
                _keyToEntityExpiringMechanism = cacheService.CreateCache<IOneToOneQuery, bool>(cacheTimeout, isAbsolute, allowFlushing);
                _keyToEntityExpiringMechanism.Subscribe(removedItem =>
                {
                    if (_log.IsDebugEnabled)
                    {
                        _log.DebugFormat("Item removed from entity cache with key '{0}'", removedItem.Key);
                    }

                    UnsubscribeFromUpdates(removedItem.Key);
                    TEntity cachedEntity;
                    _keyToEntityCache.TryRemove(removedItem.Key, out cachedEntity);
                });
            }
        }

        public override IEnumerable<TFetchable> Get<TFetchable>(IEnumerable<IQuery<TFetchable>> queries)
        {
            var entityQueries = queries.Cast<IQuery>().ToList();

            //Get everything from the cache that we can
            var cachedEntitiesResource = GetCachedEntitiesResource(entityQueries);
            var queriesForExecution = cachedEntitiesResource.NonCachedQueries;
            var entities = cachedEntitiesResource.CachedEntities;


            if (queriesForExecution == null || queriesForExecution.Count == 0)
            {
                //If we were able to get everything from the cache then just return it
                return entities.Select(pair => (TFetchable)pair.Value);
            }

            //execute the remainder of the queries
            var retrievedEntities = base.Get(queriesForExecution.Cast<IQuery<TFetchable>>());

            if (queriesForExecution.Count == 1)
            {
                //If there was only one query then we can optimise
                ResolveFetchedEntities(queriesForExecution[0], retrievedEntities, entities);
            }
            else
            {
                ResolveFetchedEntities(queriesForExecution, retrievedEntities, entities);
            }

            return entities.Select(pair => (TFetchable)pair.Value);
        }

        public override void Delete<TDeletable>(IEnumerable<IKey<TDeletable>> keys)
        {
            var resolvedKeys = keys.ToList();

            base.Delete(resolvedKeys);

            foreach (var key in resolvedKeys)
            {
                TEntity removedEntity;
                if (_keyToEntityCache.TryRemove(key, out removedEntity) && _keyToEntityExpiringMechanism != null)
                {
                    bool removedItem;
                    _keyToEntityExpiringMechanism.TryRemove(key, out removedItem);
                }
            }

            foreach (var entities in _atomicQueryCache.Select(pair => pair.Value))
            {
                foreach (var key in resolvedKeys)
                {
                    TEntity removedItem;
                    entities.TryRemove(key, out removedItem);
                }
            }
        }

        protected void UpdateEntityCache<TObservable>(ObservableResource<TObservable> resource)
            where TObservable : class, TEntity, ISubscribable
        {
            if (resource.IsFaulted)
            {
                return;
            }

            var resolvedEntities = new HashSet<TObservable>(new GenericEqualityComparer<TObservable>((x, y) => x.Key.Equals(y.Key), entity => entity.Key.GetHashCode()));
            foreach (var entity in resource.Entities.Where(entity => entity != null))
            {
                if (resolvedEntities.Add(entity))
                {
                    var localEntity = entity;
                    _keyToEntityCache.AddOrUpdate(entity.Key, entity, (key, old) => localEntity);
                }
            }

            if (resolvedEntities.Count == 0)
            {
                return;
            }

            Parallel.ForEach(_atomicQueryCache, pair =>
            {
                var concurrentDictionary = pair.Value;
                foreach (var entity in resolvedEntities)
                {
                    var inputKey = entity.Key;
                    var inputEntity = entity;

                    TEntity matchingEntity;
                    if (concurrentDictionary.TryGetValue(inputKey, out matchingEntity))
                    {
                        concurrentDictionary.AddOrUpdate(inputKey, entity, (k, old) => inputEntity);
                    }
                }
            });
        }

        protected virtual void UnsubscribeFromUpdates(IQuery query)
        {
            // do nothing as this is a non updating cache
        }

        protected virtual void SubscribeToUpdates(IQuery query)
        {
            // No need to subscribe as this is a non-updating caching service
        }

        private CachedEntitiesResource GetCachedEntitiesResource(IList<IQuery> queries)
        {
            return queries.Count == 1 ?
                        GetSingleQueryCachedEntitiesResource(queries[0]) :
                        GetMultipleQueriesCachedEntitiesResource(queries);
        }

        private CachedEntitiesResource GetSingleQueryCachedEntitiesResource(IQuery query)
        {
            var cachingOptions = query as IWithOptionalCaching;

            if (!CanGetFromCache(cachingOptions))
            {
                return new CachedEntitiesResource(new[] { query }, new Dictionary<IKey, TEntity>());
            }

            var onetoOneQuery = query as IOneToOneQuery;
            if (onetoOneQuery == null)
            {
                IList<IQuery> queriesToExecute;

                //passing null to the callback means that the entities will be returned
                var entities = GetEntitiesFromQueryCache(query, null, out queriesToExecute);
                return new CachedEntitiesResource(CanBeFetched(cachingOptions) ? queriesToExecute : null, entities);
            }

            bool wasCached;
            var entity = GetEntityFromEntityCache(onetoOneQuery, out wasCached);
            if (wasCached)
            {
                return entity != null
                           ? new CachedEntitiesResource(null, new Dictionary<IKey, TEntity> { { entity.Key, entity } })
                           : new CachedEntitiesResource(null, new Dictionary<IKey, TEntity>());
            }

            return new CachedEntitiesResource(CanBeFetched(cachingOptions) ? new[] { query } : null, new Dictionary<IKey, TEntity>());
        }

        private CachedEntitiesResource GetMultipleQueriesCachedEntitiesResource(IEnumerable<IQuery> queries)
        {
            var entities = new Dictionary<IKey, TEntity>();
            var queriesForExecution = new List<IQuery>();
            foreach (var query in queries)
            {
                var cachingOptions = query as IWithOptionalCaching;

                if (!CanGetFromCache(cachingOptions))
                {
                    if (CanBeFetched(cachingOptions))
                    {
                        queriesForExecution.Add(query);
                    }
                }
                else
                {
                    var oneToOneQuery = query as IOneToOneQuery;
                    if (oneToOneQuery != null)
                    {
                        bool wasCached;
                        var entity = GetEntityFromEntityCache(oneToOneQuery, out wasCached);
                        if (entity != null)
                        {
                            entities[entity.Key] = entity;
                        }
                        else if (!wasCached && CanBeFetched(cachingOptions))
                        {
                            queriesForExecution.Add(query);
                        }
                    }
                    else
                    {
                        IList<IQuery> queriesToExecute;
                        GetEntitiesFromQueryCache(query, cachedEntity => { entities[cachedEntity.Key] = cachedEntity; }, out queriesToExecute);

                        if (queriesToExecute != null && CanBeFetched(cachingOptions))
                        {
                            queriesForExecution.AddRange(queriesToExecute);
                        }
                    }
                }
            }

            return new CachedEntitiesResource(queriesForExecution, entities);
        }

        private TEntity GetEntityFromEntityCache(IOneToOneQuery key, out bool wasFetched)
        {
            TEntity cachedEntity;
            if (_keyToEntityCache.TryGetValue(key, out cachedEntity))
            {
                wasFetched = true;
                return cachedEntity;
            }

            wasFetched = false;
            return null;
        }

        private IDictionary<IKey, TEntity> GetEntitiesFromQueryCache(IQuery query, Action<TEntity> addedEntityCallback, out IList<IQuery> queriesToExecute)
        {

            var oneToOneQuery = query as IOneToOneQuery;
            Dictionary<IKey, TEntity> entities = null;

            if (oneToOneQuery != null)
            {
                if (addedEntityCallback == null)
                {
                    entities = new Dictionary<IKey, TEntity>();
                    addedEntityCallback = addedEntity => { entities[addedEntity.Key] = addedEntity; };
                }

                bool wasCached;
                var entity = GetEntityFromEntityCache(oneToOneQuery, out wasCached);
                if (entity == null)
                {
                    queriesToExecute = !wasCached ? new[] { oneToOneQuery } : null;
                }
                else
                {
                    addedEntityCallback(entity);
                    queriesToExecute = null;
                }
            }
            else
            {
                //check to see if this query is in the large query optimization cache
                ConcurrentDictionary<IKey, TEntity> atomicQueryEntry;
                IEnumerable<IKey> keys;
                if (_atomicQueryCache.TryGetValue(query, out atomicQueryEntry) &&
                    _queryToKeysCache.TryGetValue(query, out keys)) // Touch the query cache to reset
                {
                    queriesToExecute = null;

                    //If the callback was not supplied we can simply return our dictionary
                    if (addedEntityCallback == null)
                    {
                        return atomicQueryEntry;
                    }

                    //The callback was supplied so run through our dictionary and call the callback for each entry
                    foreach (var pair in atomicQueryEntry)
                    {
                        addedEntityCallback(pair.Value);
                    }
                    return null;
                }

                if (addedEntityCallback == null)
                {
                    entities = new Dictionary<IKey, TEntity>();
                    addedEntityCallback = addedEntity => { entities[addedEntity.Key] = addedEntity; };
                }

                if (!_queryToKeysCache.TryGetValue(query, out keys) || keys == null)
                {
                    queriesToExecute = new[] { query };
                    return entities;
                }

                var queries = new List<IQuery>();
                foreach (var key in keys)
                {
                    bool wasCached;
                    var entity = GetEntityFromEntityCache(key, out wasCached);
                    if (entity == null)
                    {
                        if (!wasCached)
                        {
                            queries.Add(key);
                        }
                    }
                    else
                    {
                        addedEntityCallback(entity);
                    }
                }

                queriesToExecute = queries.Count == 0 ? null : queries;
            }

            return entities;
        }

        private void AddEntityToEntityCache(IOneToOneQuery oneToOneQuery, TEntity entity)
        {
            _keyToEntityCache.AddOrUpdate(oneToOneQuery, entity, (k, existingEntry) => entity);
            if (_keyToEntityExpiringMechanism != null)
            {
                //Doing a TryAdd  touches the underlying cache (thereby resetting the expiry timeout) but doesn't have the over head of actually updating it if the item already exists.
                var timeout = GetTimeout(oneToOneQuery as IWithOptionalCaching);
                if (timeout != null)
                {
                    _keyToEntityExpiringMechanism.TryAdd(oneToOneQuery, true, (TimeSpan)timeout);
                }
                else
                {
                    _keyToEntityExpiringMechanism.TryAdd(oneToOneQuery, true);
                }
            }

            //Also ensure the actual key gets added to the cache (where the onetoone query isn't the key
            if (entity != null && !entity.Key.Equals(oneToOneQuery))
            {
                _keyToEntityCache.AddOrUpdate(entity.Key, entity, (k, existingEntry) => entity);
                if (_keyToEntityExpiringMechanism != null)
                {
                    //Doing a TryAdd  touches the underlying cache (thereby resetting the expiry timeout) but doesn't have the over head of actually updating it if the item already exists.
                    var timeout = GetTimeout(oneToOneQuery as IWithOptionalCaching);
                    if (timeout != null)
                    {
                        _keyToEntityExpiringMechanism.TryAdd(entity.Key, true, (TimeSpan)timeout);
                    }
                    else
                    {
                        _keyToEntityExpiringMechanism.TryAdd(entity.Key, true);
                    }
                }
            }
        }

        private void ResolveFetchedEntities<TFetchable>(IEnumerable<IQuery> queries, IEnumerable<TFetchable> retrievedEntities, IDictionary<IKey, TEntity> allEntities)
            where TFetchable : class, TEntity, IFetchable
        {
            //Add all of the returned entries to the dictionary
            foreach (var entity in retrievedEntities)
            {
                allEntities[entity.Key] = entity;
            }

            var forDelayedExecution = new List<IQuery>();
            foreach (var pair in queries.Select(query => new { query, cachingOptions = query as IWithOptionalCaching })
                                        .Where(pair => CanBeCached(pair.cachingOptions)))
            {
                var key = pair.query as IKey;
                if (key != null)
                {
                    TEntity entity;
                    if (allEntities.TryGetValue(key, out entity))
                    {
                        AddEntityToEntityCache(key, entity);

                        if (CanSubscribeToUpdates(pair.cachingOptions))
                        {
                            SubscribeToUpdates(key);
                        }
                    }
                    else if (CanCacheEmptyResults(pair.cachingOptions))
                    {
                        AddEntityToEntityCache(key, null);
                    }
                }
                else
                {
                    forDelayedExecution.Add(pair.query);
                }
            }

            if (forDelayedExecution.Count > 0)
            {
                Task.Factory.StartNew(() =>
                {
                    try
                    {
                        PopulateQueryCache(forDelayedExecution,
                                           (key =>
                                           {
                                               TEntity entity;
                                               allEntities.TryGetValue(key, out entity);
                                               return entity;
                                           }));
                    }
                    catch (Exception ex)
                    {
                        _log.Error("Unable to populate backgound queries", ex);
                    }
                });
            }
        }

        private void ResolveFetchedEntities<TFetchable>(IQuery query, IEnumerable<TFetchable> retrievedEntities, IDictionary<IKey, TEntity> allEntities)
            where TFetchable : class, TEntity, IFetchable
        {
            var cachingOptions = query as IWithOptionalCaching;

            if (!CanBeCached(cachingOptions))
            {
                //If no caching
                foreach (var entity in retrievedEntities)
                {
                    allEntities[entity.Key] = entity;
                }
                return;
            }

            var oneToOneQuery = query as IOneToOneQuery;
            if (oneToOneQuery != null)
            {
                //If is a key
                var entity = retrievedEntities.FirstOrDefault();
                if (entity != null)
                {
                    //check to see if there is more than one entry
                    if (retrievedEntities.Skip(1).FirstOrDefault() != null)
                    {
                        throw new InvalidOperationException("Fetching entities by OneToOneQuery or Key should result in only one entity being returned as OneToOneQueries or Keys should represent unique entities.");
                    }

                    AddEntityToEntityCache(oneToOneQuery, entity);

                    if (CanSubscribeToUpdates(cachingOptions))
                    {
                        SubscribeToUpdates(oneToOneQuery);
                    }

                    allEntities[entity.Key] = entity;
                }
                else if (CanCacheEmptyResults(cachingOptions))
                {
                    AddEntityToEntityCache(oneToOneQuery, null);
                }

                return;
            }

            //If is a query

            var keys = new HashSet<IKey>();
            var entities = new List<TEntity>();
            foreach (var entity in retrievedEntities.Where(entity => keys.Add(entity.Key)))
            {
                AddEntityToEntityCache(entity.Key, entity);
                entities.Add(entity);
                allEntities[entity.Key] = entity;
            }

            if (keys.Count > 0 || CanCacheEmptyResults(cachingOptions))
            {
                var timeout = GetTimeout(cachingOptions);
                if (timeout != null)
                {
                    _queryToKeysCache.AddOrUpdate(query, keys, (k, existingQuery) => keys, (TimeSpan)timeout);
                }
                else
                {
                    _queryToKeysCache.AddOrUpdate(query, keys, (k, existingQuery) => keys);
                }

                PopulateAtomicQueryCache(query, entities);

                if (CanSubscribeToUpdates(cachingOptions))
                {
                    SubscribeToUpdates(query);
                }
            }
        }

        private void PopulateQueryCache(IList<IQuery> queries, Func<IKey, TEntity> getEntityCallback)
        {
            if (queries.Count == 1)
            {
                PopulateQueryCache(queries[0], getEntityCallback);
            }

            Parallel.ForEach(queries, query => PopulateQueryCache(query, getEntityCallback));
        }

        private void PopulateQueryCache(IQuery query, Func<IKey, TEntity> getEntityCallback)
        {
            var cachingOptions = query as IWithOptionalCaching;

            var keys = new HashSet<IKey>();
            var entities = base.Get(new[] { (IQuery<TEntity>)query })
                .Select(entity => entity.Key)
                .Select(getEntityCallback)
                .Where(entity => entity != null)
                .Where(entity => keys.Add(entity.Key))
                .ToList();

            if (keys.Count == 0 && !CanCacheEmptyResults(cachingOptions))
            {
                return;
            }

            //Check to see if this is a OneToOneQuery
            var oneToOneQuery = query as IOneToOneQuery;
            if (oneToOneQuery != null)
            {
                //check to make sure there is one one result
                if (keys.Count > 1)
                {
                    throw new InvalidOperationException("Fetching entities by OneToOneQuery or Key should result in only one entity being returned as OneToOneQueries or Keys should represent unique entities.");
                }

                AddEntityToEntityCache(oneToOneQuery, entities.First());
            }
            else
            {
                var timeout = GetTimeout(cachingOptions);
                if (timeout != null)
                {
                    _queryToKeysCache.AddOrUpdate(query, keys, (k, existingQuery) => keys, (TimeSpan)timeout);
                }
                else
                {
                    _queryToKeysCache.AddOrUpdate(query, keys, (k, existingQuery) => keys);
                }

                PopulateAtomicQueryCache(query, entities);
            }

            if (CanSubscribeToUpdates(cachingOptions))
            {
                SubscribeToUpdates(query);
            }
        }

        private void PopulateAtomicQueryCache(IQuery query, ICollection<TEntity> entities)
        {
            var threshold = GetAtomicQueryThreshold(query as IWithOptionalCaching);

            if (threshold > 0 && entities.Count >= threshold)
            {
                var concurrentEntities = new ConcurrentDictionary<IKey, TEntity>(entities.Select(entity => new KeyValuePair<IKey, TEntity>(entity.Key, entity)));
                _atomicQueryCache.AddOrUpdate(query, concurrentEntities, (k, old) => concurrentEntities);
            }
        }

        private static TimeSpan? GetTimeout(IWithOptionalCaching cachingOptions)
        {
            return cachingOptions != null ? cachingOptions.Timeout : null;
        }

        private int GetAtomicQueryThreshold(IWithOptionalCaching cachingOptions)
        {
            if (cachingOptions == null || cachingOptions.ResultsStoredTogetherThreshold == null)
            {
                return 10;
            }

            switch ((int)cachingOptions.ResultsStoredTogetherThreshold)
            {
                case ResultsStoredTogether.Always:
                    return 1;
                case ResultsStoredTogether.Never:
                    return -1;
                default:
                    return (int)cachingOptions.ResultsStoredTogetherThreshold < 1
                               ? 1
                               : (int)cachingOptions.ResultsStoredTogetherThreshold;
            }
        }

        private bool CanGetFromCache(IWithOptionalCaching cachingOptions)
        {
            if (!CanBeCached(cachingOptions))
            {
                return false;
            }

            return cachingOptions == null
                       ? _cachingDefaults.CachingOptions != CachingOptions.RetrieveFromSource
                       : cachingOptions.CachingOptions != CachingOptions.RetrieveFromSource;
        }

        protected bool CanBeCached(IWithOptionalCaching cachingOptions)
        {
            return cachingOptions == null
                       ? _cachingDefaults.CachingOptions != CachingOptions.DontCache
                       : cachingOptions.CachingOptions != CachingOptions.DontCache;
        }

        private bool CanBeFetched(IWithOptionalCaching cachingOptions)
        {
            return cachingOptions == null
                       ? _cachingDefaults.CachingOptions != CachingOptions.RetrieveFromCacheOnly
                       : cachingOptions.CachingOptions != CachingOptions.RetrieveFromCacheOnly;
        }

        private bool CanCacheEmptyResults(IWithOptionalCaching cachingOptions)
        {
            return cachingOptions == null
                       ? !_cachingDefaults.WhenCaching.HasFlag(WhenCaching.DontCacheEmptyResults)
                       : !cachingOptions.WhenCaching.HasFlag(WhenCaching.DontCacheEmptyResults);
        }

        private bool CanSubscribeToUpdates(IWithOptionalCaching cachingOptions)
        {
            return cachingOptions == null
                       ? !_cachingDefaults.WhenCaching.HasFlag(WhenCaching.DontAllowSubscribing)
                       : !cachingOptions.WhenCaching.HasFlag(WhenCaching.DontAllowSubscribing);
        }

        private class CachedEntitiesResource
        {
            private readonly IList<IQuery> _nonCachedQueries;
            private readonly IDictionary<IKey, TEntity> _cachedEntities;

            public CachedEntitiesResource(IList<IQuery> nonCachedQueries, IDictionary<IKey, TEntity> cachedEntities)
            {
                _nonCachedQueries = nonCachedQueries;
                _cachedEntities = cachedEntities;
            }

            public IList<IQuery> NonCachedQueries
            {
                get { return _nonCachedQueries; }
            }

            public IDictionary<IKey, TEntity> CachedEntities
            {
                get { return _cachedEntities; }
            }
        }
    }
}
