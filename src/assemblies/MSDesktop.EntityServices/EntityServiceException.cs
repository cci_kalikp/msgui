﻿using System;
using System.Runtime.Serialization;

namespace MorganStanley.Desktop.EntityServices
{
    [Serializable]
    public class EntityServiceException : Exception
    {
        public EntityServiceException() { }
        public EntityServiceException(string message) : base(message) { }
        public EntityServiceException(string message, Exception inner) : base(message, inner) { }

        protected EntityServiceException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}