﻿using System;
using System.Collections.Generic;

namespace MorganStanley.Desktop.EntityServices.Queries
{
    [Serializable]
    public abstract class QuerySeperator
    {
        public abstract string[] Split(string query);
        public abstract string Join(IEnumerable<string> fields);

        public static readonly QuerySeperator Default = new CharSeperator('/');

        public static QuerySeperator Create(string seperatorChars)
        {
            return seperatorChars.Length == 1
                       ? (QuerySeperator)new CharSeperator(seperatorChars[0])
                       : new StringSeperator(seperatorChars);


        }
    }

    [Serializable]
    public class StringSeperator : QuerySeperator
    {
        public StringSeperator(string seperatorChars)
        {
            SeperatorChars = seperatorChars;
        }

        public string SeperatorChars { get; private set; }

        public override string[] Split(string query)
        {
            return query.Split(new[] { SeperatorChars }, StringSplitOptions.None);
        }

        public override string Join(IEnumerable<string> fields)
        {
            return string.Join(SeperatorChars, fields);
        }

    }

    [Serializable]
    public class CharSeperator : QuerySeperator
    {
        public CharSeperator(char seperatorChar)
        {
            SeperatorChar = seperatorChar;
        }

        public char SeperatorChar { get; private set; }

        public override string[] Split(string query)
        {
            return query.Split(SeperatorChar);
        }

        public override string Join(IEnumerable<string> fields)
        {
            return string.Join(SeperatorChar.ToString(), fields).TrimEnd(new[] { ((CharSeperator)Default).SeperatorChar });
        }
    }
}
