﻿namespace MorganStanley.Desktop.EntityServices.Queries
{
    public enum TappingOptions
    {
        /// <summary>
        /// Let the tapping service use its default behaviour.
        /// </summary>
        Default = 0,

        /// <summary>
        /// Don't allow the results to be tapped
        /// </summary>
        DontTap = 1
    }

    public interface IWithOptionalTapping : IQuery
    {
        /// <summary>
        /// Options to control the behaviour of the tapping service
        /// </summary>
        TappingOptions TappingOptions { get; }
    }
}
