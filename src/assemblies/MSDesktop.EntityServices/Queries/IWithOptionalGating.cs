﻿namespace MorganStanley.Desktop.EntityServices.Queries
{
    public enum GatingOptions
    {
        /// <summary>
        /// Let the gating service use its default behaviour.
        /// </summary>
        Default = 0,

        /// <summary>
        /// Don't allow the results to be gated
        /// </summary>
        DontGate = 1
    }

    public interface IWithOptionalGating : IQuery
    {
        /// <summary>
        /// Options to control the behaviour of the tapping service
        /// </summary>
        GatingOptions GatingOptions { get; }
    }
}
