﻿namespace MorganStanley.Desktop.EntityServices.Queries
{
    public interface IForceFetchable
    {
        bool IsForceFetch { get; }
    }
}
