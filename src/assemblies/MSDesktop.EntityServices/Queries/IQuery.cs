﻿using System;

namespace MorganStanley.Desktop.EntityServices.Queries
{
    public interface IQuery<TEntity> : IQuery, IEquatable<IQuery<TEntity>>
    {
    }

    public interface IOneToOneQuery<TEntity> : IQuery<TEntity>, IOneToOneQuery
    {
    }

    public interface IQuery : IEquatable<IQuery>
    {
    }

    public interface IOneToOneQuery : IQuery
    {
    }
}