﻿namespace MorganStanley.Desktop.EntityServices.Queries
{
    public interface IStaticCachable
    {
        bool IsStatic { get; }
    }
}
