﻿using System;

namespace MorganStanley.Desktop.EntityServices.Queries
{
    public interface ICacheTimeoutAdjustable
    {
        TimeSpan? Timeout { get; }
    }
}
