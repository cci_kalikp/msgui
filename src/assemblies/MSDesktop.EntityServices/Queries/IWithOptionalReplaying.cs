﻿namespace MorganStanley.Desktop.EntityServices.Queries
{
    public enum ReplayOptions
    {
        /// <summary>
        /// Let the replay service use its default behaviour.
        /// </summary>
        Default = 0,

        /// <summary>
        /// Don't allow the results to be replayed
        /// </summary>
        DontReplay = 1
    }

    public interface IWithOptionalReplaying : IQuery
    {
        /// <summary>
        /// Options to control the behaviour of the replay service
        /// </summary>
        ReplayOptions ReplayOptions { get; }
    }
}
