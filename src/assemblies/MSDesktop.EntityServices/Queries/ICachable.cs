﻿namespace MorganStanley.Desktop.EntityServices.Queries
{
    public interface ICachable
    {
        bool IsCachable { get; }
    }
}
