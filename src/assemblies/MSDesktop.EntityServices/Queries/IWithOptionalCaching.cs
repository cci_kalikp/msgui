﻿using System;
using System.Threading;

namespace MorganStanley.Desktop.EntityServices.Queries
{

    public enum CachingOptions
    {
        /// <summary>
        /// Let the caching service use its default behaviour.
        /// </summary>
        Default = 0,

        /// <summary>
        /// Bypass the cache and retrieve directly from the source.  Do not store the results in the cache.
        /// </summary>
        DontCache = 1,

        /// <summary>
        /// Bypass the cache and retrieve directly from the source. Store the results in the cache.
        /// </summary>
        RetrieveFromSource = 2,

        /// <summary>
        /// Only retrieve the results from the cache.
        /// </summary>
        RetrieveFromCacheOnly = 3
    }

    [Flags]
    public enum WhenCaching
    {
        /// <summary>
        /// Take the default action when caching of allowing subscribing for updates (if supported) and caching queries that have no results
        /// </summary>
        DoDefault = 0,

        /// <summary>
        /// Don't subscribe to updates for the retrieved results
        /// </summary>
        DontAllowSubscribing = 1,

        /// <summary>
        /// Don't Cache results when the query returned nothing
        /// </summary>
        DontCacheEmptyResults = 2
    }

    public interface IWithOptionalCaching : IQuery
    {
        /// <summary>
        /// A non default sliding expiry timeout for the results. Returning null meanas use the default timeout.
        /// </summary>
        TimeSpan? Timeout { get; }

        /// <summary>
        /// Store the results of a query so that they can be retrieved and expire as a single unit.
        /// This can be useful for situations where performance is critical or it would invalidate the data if they were to expiry individually.
        /// If not supplied a default threshold is used
        /// </summary>
        int? ResultsStoredTogetherThreshold { get; }


        /// <summary>
        /// Options to control the behaviour of the caching service
        /// </summary>
        CachingOptions CachingOptions { get; }

        /// <summary>
        /// Options to control what happens when caching occurs
        /// </summary>
        WhenCaching WhenCaching { get; }
    }

    //Used with the ResultsStoredTogetherThreshold property of IWithOptionalCaching
    public static class ResultsStoredTogether
    {
        /// <summary>
        /// Always store the the results together
        /// </summary>
        public const int Always = -1;

        /// <summary>
        /// Never store the results together
        /// </summary>
        public const int Never = -2;
    }
}
