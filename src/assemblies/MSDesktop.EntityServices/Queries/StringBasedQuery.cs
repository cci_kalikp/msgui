﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MorganStanley.Desktop.EntityServices.Queries
{
    /// <summary>
    /// The implementation is immutable so if a value needs to be changed a new query should be generated.
    /// </summary>
    [Serializable]
    public abstract class StringBasedQuery : IEquatable<StringBasedQuery>, IQuery
    {
        private string[] _parts;
        private readonly QuerySeperator _seperator;

        protected StringBasedQuery(QuerySeperator seperator, IEnumerable<string> fields)
            : this(seperator, fields != null ? fields.ToArray() : null)
        {
        }

        protected StringBasedQuery(QuerySeperator seperator, params string[] fields)
        {
            if (fields == null || fields.Length == 0)
            {
                throw new ArgumentNullException("fields");
            }

            _seperator = seperator ?? QuerySeperator.Default;
            PartCount = fields.Length;

            bool wasTrimmed = false;

            Query = _seperator.Join(fields.Select(field =>
                                                      {
                                                          if (field == null)
                                                          {
                                                              return "";
                                                          }

                                                          bool trimmed;
                                                          field = Trim(field, out trimmed);
                                                          if (trimmed)
                                                          {
                                                              wasTrimmed = true;
                                                          }
                                                          return field;
                                                      }));

            _parts = wasTrimmed ? fields.Select(field => field == null ? null : field.Trim()).ToArray() : fields;
            IsEmpty = false;
        }

        protected StringBasedQuery(QuerySeperator seperator, int numberOfFields)
        {
            _seperator = seperator ?? QuerySeperator.Default;
            PartCount = numberOfFields;

            _parts = new string[numberOfFields];
            Query = string.Empty;
            IsEmpty = true;
        }


        public int PartCount { get; private set; }

        public bool IsEmpty { get; private set; }

        protected string Query { get; private set; }

        public string this[int index]
        {
            get
            {
                if (index < 0 || index > PartCount - 1)
                {
                    throw new ArgumentException(string.Format("part {0} is out of range for the size for the query part length of {1}", Query, PartCount));
                }
                return _parts[index];
            }
        }

        protected void Create(string query)
        {
            if (!IsEmpty)
            {
                throw new InvalidOperationException("This query has already been intialised");
            }

            if (string.IsNullOrEmpty(query))
            {
                _parts = new string[PartCount];
                Query = string.Empty;
                IsEmpty = true;
                return;
            }

            var parts = _seperator.Split(query);
            ValidatePartLength(ref parts);
            ValidateParts(parts);

            bool wasTrimmed = false;

            //rejoin rather than using the query passed in incase the seperator uses different joiing logic to the split logic
            Query = _seperator.Join(parts.Select(field =>
                                                     {
                                                         if (field == null)
                                                         {
                                                             return "";
                                                         }

                                                         bool trimmed;
                                                         field = Trim(field, out trimmed);
                                                         if (trimmed)
                                                         {
                                                             wasTrimmed = true;
                                                         }
                                                         return field;
                                                     }));

            _parts = wasTrimmed ? parts.Select(field => field == null ? null : field.Trim()).ToArray() : parts;

            Query = _seperator.Join(parts.Select(field => field ?? "")); 
            _parts = parts;
            IsEmpty = false;

            QueryPopulated();
        }

        protected virtual void ValidatePartLength(ref string[] parts)
        {
            if (parts.Length != PartCount)
            {
                throw new InvalidOperationException(string.Format("The query should have contained {0} parts but instead contains {1}", PartCount, parts.Length));
            }
        }

        protected virtual void ValidateParts(string[] parts)
        {

        }

        protected virtual void QueryPopulated()
        {

        }

        public bool Equals(IQuery other)
        {
            return Equals(other as StringBasedQuery);
        }

        public virtual bool Equals(StringBasedQuery other)
        {
            if (other == null)
            {
                return false;
            }

            return GetType() == other.GetType() && Query == other.Query;
        }

        public override bool Equals(object that)
        {
            return Equals(that as StringBasedQuery);
        }

        public override int GetHashCode()
        {
            return IsEmpty ? base.GetHashCode() : Query.GetHashCode();
        }

        private static string Trim(string stringToTrim, out bool trimmed)
        {
            //Copied form .net 4.0 implementation with added out.  Not ideal but will work.
            int start = 0;
            while (start < stringToTrim.Length)
            {
                if (!char.IsWhiteSpace(stringToTrim[start]))
                {
                    break;
                }
                start++;
            }
            
            int end = stringToTrim.Length - 1;
            while (end >= start)
            {
                if (!char.IsWhiteSpace(stringToTrim[end]))
                {
                    break;
                }
                end--;
            }

            return CreateTrimmedString(stringToTrim, start, end, out trimmed);
        }

        private static string CreateTrimmedString(string stringToTrim, int start, int end, out bool trimmed)
        {
            int length = (end - start) + 1;
            if (length == stringToTrim.Length)
            {
                trimmed = false;
                return stringToTrim;
            }
            trimmed = true;
            return length == 0 ? string.Empty : stringToTrim.Substring(start, length);
        }


    }
}
