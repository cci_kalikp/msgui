﻿using System;

namespace MorganStanley.Desktop.EntityServices.Queries
{
    [Serializable]
    public class GlobalQuery<T> : IQuery<T>

    {
        private readonly static GlobalQuery<T> _instance = new GlobalQuery<T>();

        private GlobalQuery() {}

        public static GlobalQuery<T> Value { get { return _instance; } }


        public bool Equals(GlobalQuery<T> other)
        {
            return true;
        }

        public bool Equals(IQuery<T> other)
        {
            return other is GlobalQuery<T>;
        }

        public bool Equals(IQuery other)
        {
            return other is GlobalQuery<T>;
        }

        public override bool Equals(object obj)
        {
            return obj is GlobalQuery<T>;
        }

        public override int GetHashCode()
        {
            return typeof(T).GetHashCode();
        }
    }
}
