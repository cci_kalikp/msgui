﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;
using MorganStanley.Desktop.EntityServices.Entities;
using MorganStanley.Desktop.EntityServices.Queries;
using MorganStanley.Desktop.EntityServices.Services.Exceptions;

namespace MorganStanley.Desktop.EntityServices
{
    internal sealed class EntityReplayService<TEntity> : EntityPassthroughService<TEntity>, IEntityReplayService<TEntity>
        where TEntity : class, IEntity,  IFetchable
    {
        private readonly ConcurrentDictionary<Guid, Action> _observedEntities = new ConcurrentDictionary<Guid, Action>();
        private readonly ConcurrentDictionary<IQuery, ConcurrentDictionary<Guid, Action>> _fetchedQueries = new ConcurrentDictionary<IQuery, ConcurrentDictionary<Guid, Action>>();
        private readonly ConcurrentDictionary<IQuery, ConcurrentDictionary<Guid, Action>> _unfetchedQueries = new ConcurrentDictionary<IQuery, ConcurrentDictionary<Guid, Action>>();

        public EntityReplayService(IEntityService<TEntity> entityService, IEntityServiceExceptionService exceptionService)
            : base(entityService, exceptionService)
        {
        }

        public override IEnumerable<TFetchable> Get<TFetchable>(IEnumerable<IQuery<TFetchable>> queries)
        {
            foreach(var query in queries.Where(query => CanReplay(query as IWithOptionalReplaying)))
            {
                var fetchedActions = _fetchedQueries.GetOrAdd(query, new ConcurrentDictionary<Guid, Action>());

                //move everything from unfetched to fetched
                ConcurrentDictionary<Guid, Action> unfetchedActions;
                if (_unfetchedQueries.TryRemove(query, out unfetchedActions))
                {
                    foreach (var pair in unfetchedActions)
                    {
                        fetchedActions.TryAdd(pair.Key, pair.Value);
                    }
                }
            }
            
            return base.Get(queries);
        }

        public override IObservable<ObservableResource<TObservable>> GetObservable<TObservable>(IEnumerable<IQuery<TObservable>> queries)
        {
            IList<IQuery<TObservable>> replayQueries;
            IList<IQuery<TObservable>> nonreplayQueries;
            SplitQueries(queries, out replayQueries, out nonreplayQueries);

            var observable = Observable.Empty<ObservableResource<TObservable>>();

            if (replayQueries.Count > 0)
            {
                observable = observable.Merge(Observable.Create<ObservableResource<TObservable>>(o =>
                    {
                        var uniqueId = Guid.NewGuid();

                        var updatedActions = new List<ConcurrentDictionary<Guid, Action>>();
                        foreach (var query in queries)
                        {
                            var localQuery = query;

                            ConcurrentDictionary<Guid, Action> actions;
                            if (_fetchedQueries.TryGetValue(query, out actions))
                            {
                                actions.TryAdd(uniqueId, () => o.OnNext(new ObservableResource<TObservable>(base.Get(new[] { localQuery }))));
                            }
                            else
                            {
                                actions = _unfetchedQueries.GetOrAdd(query, new ConcurrentDictionary<Guid, Action>());
                                actions.TryAdd(uniqueId, () => o.OnNext(new ObservableResource<TObservable>(base.Get(new[] { localQuery }))));
                            }
                            updatedActions.Add(actions);
                        }


                        var subscription = base.GetObservable(queries)
                                                .Subscribe(resource =>
                                                    {
                                                        if (resource.IsFaulted)
                                                        {
                                                            o.OnNext(resource);
                                                            return;
                                                        }

                                                        var keys = resource.Entities.Select(entity => entity.Key).Cast<IQuery<TEntity>>().ToList();
                                                        _observedEntities.AddOrUpdate(uniqueId,
                                                                                      key => () => o.OnNext(new ObservableResource<TObservable>(base.Get(keys).Cast<TObservable>())),
                                                                                      (key, existingLamda) => () => o.OnNext(new ObservableResource<TObservable>(base.Get(keys).Cast<TObservable>())));
                                                        o.OnNext(resource);
                                                    });



                        return () =>
                                    {
                                        subscription.Dispose();
                                        Action removedItem;
                                        _observedEntities.TryRemove(uniqueId, out removedItem);
                                        foreach (var action in updatedActions)
                                        {
                                            action.TryRemove(uniqueId, out removedItem);
                                        }
                                    };
                    }));
            }

            return nonreplayQueries.Count > 0 ? observable.Merge(base.GetObservable(nonreplayQueries)) : observable;
        }


        public void Replay()
        {
            Parallel.ForEach(_fetchedQueries.Select(pair => pair.Value).SelectMany(queries => queries), pair => pair.Value());

            Parallel.ForEach(_observedEntities, pair => pair.Value());
        }

        private static bool CanReplay(IWithOptionalReplaying replayOptions)
        {
            return replayOptions == null || replayOptions.ReplayOptions != ReplayOptions.DontReplay;
        }

        private static void SplitQueries<T>(IEnumerable<IQuery<T>> queries, out IList<IQuery<T>> replayQueries, out IList<IQuery<T>> nonReplayQueries)
            where T : class, TEntity
        {
            replayQueries = new List<IQuery<T>>();
            nonReplayQueries = new List<IQuery<T>>();
            foreach (var query in queries)
            {
                if (CanReplay(query as IWithOptionalReplaying))
                {
                    replayQueries.Add(query);
                }
                else
                {
                    nonReplayQueries.Add(query);
                }
            }
        }
    }
}
