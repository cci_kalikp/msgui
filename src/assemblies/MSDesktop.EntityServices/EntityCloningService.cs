﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using MorganStanley.Desktop.EntityServices.Entities;
using MorganStanley.Desktop.EntityServices.Queries;
using MorganStanley.Desktop.EntityServices.Services.Cloning;
using MorganStanley.Desktop.EntityServices.Services.Exceptions;

namespace MorganStanley.Desktop.EntityServices
{
    internal sealed class EntityCloningService<TEntity> : EntityPassthroughService<TEntity>, IEntityCloningService<TEntity>
        where TEntity : class, IEntity
    {
        private readonly IEntityCloning _entityCloning;

        public EntityCloningService(IEntityService<TEntity> entityService, IEntityCloning entityCloning, IEntityServiceExceptionService exceptionService)
            : base(entityService, exceptionService)
        {
            _entityCloning = entityCloning;
        }

        public override IEnumerable<TFetchable> Get<TFetchable>(IEnumerable<IQuery<TFetchable>> queries)
        {
            return base.Get(queries).Select(entity => _entityCloning.Clone(entity));
        }

        public override IObservable<ObservableResource<TObservable>> GetObservable<TObservable>(IEnumerable<IQuery<TObservable>> queries)
        {
            return base.GetObservable(queries)
                       .Select(resource =>
                                   {
                                       if (resource.IsFaulted)
                                       {
                                           return resource;
                                       }

                                       try 
                                       {
                                            return new ObservableResource<TObservable>(resource.Entities.Select(entity => _entityCloning.Clone(entity)).Where(entity => entity != null).ToList());
                                       }
                                       catch(Exception ex )
                                       {
                                           return new ObservableResource<TObservable>(ex);
                                       }
                                   });
        }
    }
}
