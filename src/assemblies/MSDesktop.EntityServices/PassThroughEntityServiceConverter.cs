﻿using System.Collections.Generic;
using System.Linq;
using MorganStanley.Desktop.EntityServices.Entities;
using MorganStanley.Desktop.EntityServices.Keys;
using MorganStanley.Desktop.EntityServices.Queries;

namespace MorganStanley.Desktop.EntityServices
{
    public interface IPassThroughEntityServiceConverter<TEntity> : IEntityServiceConverter<TEntity>
        where TEntity : class, IEntity
    {
    }

    internal class PassThroughEntityServiceConverter<TEntity> : IPassThroughEntityServiceConverter<TEntity>
        where TEntity : class, IEntity
    {
        public virtual IEnumerable<object> ConvertFromEntitiesToNativeFormat(IEnumerable<IEntity<TEntity, IKey<TEntity>>> entities)
        {
            return entities;
        }

        public virtual IEnumerable<TEntity> ConvertFromNativeFormatToEntities(IEnumerable<object> nativeEntities)
        {
            return nativeEntities.OfType<TEntity>();
        }

        public IEnumerable<object> ConvertFromQueriesToNativeFormat(IEnumerable<IQuery<TEntity>> queries, QueryConversionType conversionState)
        {
            return queries;
        }

        public virtual IEnumerable<IQuery<TEntity>> ConvertFromNativeFormatToQueries(IEnumerable<object> nativeQueries)
        {
            return nativeQueries.OfType<IQuery<TEntity>>();
        }
    }
}