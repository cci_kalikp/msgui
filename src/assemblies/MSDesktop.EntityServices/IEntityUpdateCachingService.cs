﻿namespace MorganStanley.Desktop.EntityServices
{
    public interface IEntityUpdateCachingService<TEntity> : IEntityService<TEntity>
        where TEntity : IEntity, new()
    {
    }
}