﻿using System.Collections.Generic;
using MorganStanley.Desktop.EntityServices.Entities;
using MorganStanley.Desktop.EntityServices.Keys;
using MorganStanley.Desktop.EntityServices.Queries;

namespace MorganStanley.Desktop.EntityServices
{
    public enum QueryConversionType
    {
        Fetching,
        Subscribing,
        Deleting
    }


    public interface IEntityServiceConverter<TEntity> where TEntity : class, IEntity
    {
        IEnumerable<object> ConvertFromEntitiesToNativeFormat(IEnumerable<IEntity<TEntity, IKey<TEntity>>> entities);
        IEnumerable<TEntity> ConvertFromNativeFormatToEntities(IEnumerable<object> nativeEntities);

        IEnumerable<object> ConvertFromQueriesToNativeFormat(IEnumerable<IQuery<TEntity>> queries, QueryConversionType conversionType);
    }
}
