﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using MorganStanley.Desktop.EntityServices.Entities;
using MorganStanley.Desktop.EntityServices.Keys;
using MorganStanley.Desktop.EntityServices.Queries;
using MorganStanley.Desktop.EntityServices.Services.Dates;
using MorganStanley.Desktop.EntityServices.Services.Exceptions;

namespace MorganStanley.Desktop.EntityServices
{

	internal sealed class EntityGatingService<TEntity> : EntityPassthroughService<TEntity>, IEntityGatingService<TEntity>
		where TEntity : class, IEntity, ISubscribable, IFetchable, ITimestamped
	{
		private volatile bool _defaultGating;

		private readonly IDateTimeService _dateTimeService;

		private readonly ConcurrentDictionary<IKey<TEntity>, GatedResource<TEntity>> _resources = new ConcurrentDictionary<IKey<TEntity>, GatedResource<TEntity>>();
		private readonly ConcurrentDictionary<Guid, GatedObservableResource> _keysUpdatedOnGatedObservables = new ConcurrentDictionary<Guid, GatedObservableResource>();
		private readonly ConcurrentDictionary<string, IGatingServiceObject> _gatingServiceObjects = new ConcurrentDictionary<string, IGatingServiceObject>();

		private readonly ISubject<GatingState> _gatingStateSubject = new Subject<GatingState>();
		private readonly ISubject<IGatingServiceObject> _gatingServiceObjectSubject = new Subject<IGatingServiceObject>();

		public EntityGatingService(IEntityService<TEntity> entityService, IDateTimeService dateTimeService, IEntityServiceExceptionService exceptionService)
			: base(entityService, exceptionService)
		{
			_dateTimeService = dateTimeService;
			this.Subscribe<IGatingServiceObject>(UpdateGatingServiceObjects);
		}

		/// <summary>
		/// If the entity is gated this will return the last accepted value or it will go to the underlying service 
		/// if no value has ever been accepted.  If the query passed is not a key then the underlying service will 
		/// be called and any gated entities returned will be compared against gated entities.  If a matching 
		/// accepted gated entity exists it will be returned instead.  If the entity returned from the underlying 
		/// service contains a key that is meant to be gated but currently there is no gated entity then the 
		/// returned entity will become the accepted instance for that key and will be returned.  If the entity is 
		/// not meant to be gated it will be returned as normal.
		/// </summary>
		public override IEnumerable<TFetchable> Get<TFetchable>(IEnumerable<IQuery<TFetchable>> queries)
		{
			IList<IQuery<TFetchable>> gatingQueries;
			IList<IQuery<TFetchable>> nonGatingQueries;
			SplitQueries(queries, out gatingQueries, out nonGatingQueries);

			var entities = Enumerable.Empty<TFetchable>();

			if (gatingQueries.Count > 0)
			{
				var queriesToFetchFromService = gatingQueries.ToList();
				var returnEntities = new List<TFetchable>();

				// Keys can be used to retrieve entities directly from the cache (if they are already cached)
				foreach (var key in gatingQueries.OfType<IKey<TEntity>>().Where(IsGated))
				{
					var entityFromCache = GetEntityFromAcceptedArea(key);
					returnEntities.Add((TFetchable)entityFromCache);
					queriesToFetchFromService.Remove(key as IQuery<TFetchable>);
				}

				if (queriesToFetchFromService.Count > 0)
				{
					var entitiesFromService = base.Get(queriesToFetchFromService);
					foreach (var entityFromService in entitiesFromService)
					{
						var key = (IKey<TEntity>)entityFromService.Key;
						_resources.TryAdd(key, new GatedResource<TEntity>
						{
							PreviouslyRetrieved = true
						});
						var now = _dateTimeService.UtcNow();
						if (IsGated(key))
						{
							var currentEntityFromService = entityFromService;

							GatedResource<TEntity> gatedResource;
							if (_resources.TryGetValue(key, out gatedResource) && gatedResource.AcceptedInstance == null)
							{
								// If the accepted instance does not exist then set it as the one retrieved from the service
								gatedResource.AcceptedInstance = new TimestampedInstance<TEntity>
								{
									Instance = currentEntityFromService,
									Timestamp = now
								};
								gatedResource.GatedInstanceTimestamp = now;
								SignalGateChange(key, gatedResource);
							}
							else if (!currentEntityFromService.Equals(gatedResource.AcceptedInstance.Instance))
							{
								// If the accepted instance does exist then just let subscribers know a new gated value exists
								gatedResource.GatedInstanceTimestamp = now;
								SignalGateChange(key, gatedResource);
							}
							returnEntities.Add((TFetchable)gatedResource.AcceptedInstance.Instance);
						}
						else
						{
							SignalGateChange(key, entityFromService.Timestamp, entityFromService.Timestamp);
							returnEntities.Add(entityFromService);
						}
					}
				}

				entities = entities.Union(returnEntities);
			}

			if (nonGatingQueries.Count > 0)
			{
				entities = entities.Union(base.Get(nonGatingQueries));
			}

			return entities;
		}

		/// <summary>
		/// Gets an observable for all newly accepted entities
		/// </summary>
		public override IObservable<ObservableResource<TObservable>> GetObservable<TObservable>(IEnumerable<IQuery<TObservable>> queries)
		{
			return GetRefCountedObservable(queries,
				resolvedQueries =>
				{
					IList<IQuery<TObservable>> gatingQueries;
					IList<IQuery<TObservable>> nonGatingQueries;
					SplitQueries(queries, out gatingQueries, out nonGatingQueries);

					var observable = Observable.Empty<ObservableResource<TObservable>>();
					if (gatingQueries.Count > 0)
					{
						var subject = new Subject<IEnumerable<TEntity>>();
						observable = observable.Merge(Observable.Create<ObservableResource<TObservable>>(o =>
						{
							var uniqueId = Guid.NewGuid();

							var subscription = base.GetObservable(resolvedQueries).Subscribe(resource =>
							{
								if (resource.IsFaulted)
								{
									o.OnNext(resource);
									return;
								}

								try
								{
									var gatedEntities = new List<TObservable>();
									var nonGatedEntities = new List<TObservable>();
									foreach (var entity in resource.Entities)
									{
										var entityKey = (IKey<TEntity>)entity.Key;
										if (!IsGated(entityKey))
										{
											nonGatedEntities.Add(entity);
										}
										else
										{
											gatedEntities.Add(entity);
										}
									}

									if (gatedEntities.Count > 0)
									{
										foreach (var entity in gatedEntities)
										{
											UpdateGatedInstances(entity);
										}

										var observableResource = _keysUpdatedOnGatedObservables.GetOrAdd(uniqueId, key => new GatedObservableResource(subject));
										foreach (var key in gatedEntities.Select(entity => entity.Key))
										{
											observableResource.Keys.TryAdd((IKey<TEntity>)key, true);
										}
									}

									if (nonGatedEntities.Count > 0)
									{
										foreach (var entity in nonGatedEntities)
										{
											SignalGateChange((IKey<TEntity>)entity.Key, entity.Timestamp, entity.Timestamp);
										}
										o.OnNext(new ObservableResource<TObservable>(nonGatedEntities.Cast<TObservable>()));
									}
								}
								catch (Exception ex)
								{
									o.OnNext(new ObservableResource<TObservable>(new EntityServiceException(string.Format("CreateObserverable: Unable to update gatting for entity '{0}'", typeof(TEntity).FullName), ex)));
								}
							},
											o.OnError,
											o.OnCompleted);

							return subscription.Dispose;
						})).Merge(subject.Select(entities => new ObservableResource<TObservable>(entities.Cast<TObservable>())));
					}

					if (nonGatingQueries.Count > 0)
					{
						observable = observable.Merge(base.GetObservable(nonGatingQueries));
					}

					return observable;
				});
		}


		/// <summary>
		/// Accepts all gated values
		/// </summary>
		public void Accept()
		{
			Accept(x => true);
		}

		/// <summary>
		/// Accepts gated value for a given key
		/// </summary>
		public void Accept(IKey<TEntity> key)
		{
			Accept(key, x => true);
		}

		/// <summary>
		/// Accepts gated values for the given keys
		/// </summary>
		public void Accept(IEnumerable<IKey<TEntity>> keys)
		{
			Accept(keys, x => true);
		}

		/// <summary>
		/// Accepts gated value for a given key if it matches the predicate, otherwise does nothing
		/// </summary>
		public void Accept(IKey<TEntity> key, Predicate<TEntity> predicate)
		{
			var entities = base.Get(new[] { key });
			var entity = entities.SingleOrDefault(x => predicate(x));
			if (entity != null)
			{
				UpdateAcceptedInstances(entity);
				UpdateObservablesWithAcceptedEntity(entity);
			}
			else
			{
				var gatedEntityThatDoesntMatchPredicate = entities.SingleOrDefault();
				if (gatedEntityThatDoesntMatchPredicate == null)
				{
					throw new EntityServiceException(string.Format("We passed a key to the underlying service, so we expect one and only one result back.  Instead we received {0} results.", entities.Count()));
				}
				UpdateGatedInstances(gatedEntityThatDoesntMatchPredicate);
			}
		}

		/// <summary>
		/// Accepts gated values for given keys if they match the predicate, otherwise does nothing
		/// </summary>
		public void Accept(IEnumerable<IKey<TEntity>> keys, Predicate<TEntity> predicate)
		{
			var entities = base.Get(keys);
			foreach (var entity in entities.Where(x => predicate(x)))
			{
				UpdateAcceptedInstances(entity);
				UpdateObservablesWithAcceptedEntity(entity);
			}
		}

		/// <summary>
		/// Accepts all gated values that match the given predicate
		/// </summary>
		public void Accept(Predicate<TEntity> predicate)
		{
			var gatedKeysToGet = _resources.Where(x => x.Value.IsGated && x.Value.PreviouslyRetrieved).Select(x => x.Key).Distinct();
			var newEntities = gatedKeysToGet.Any()
				? base.Get(gatedKeysToGet)
				: Enumerable.Empty<TEntity>();

			foreach (var entity in newEntities)
			{
				if (predicate(entity))
				{
					UpdateAcceptedInstances(entity);
				}
				else
				{
					UpdateGatedInstances(entity);
				}
			}

			foreach (var gatedResource in _keysUpdatedOnGatedObservables.Select(pair => new { pair.Value.Subject, pair.Value.Keys }).Where(gatedResource => gatedResource.Keys.Count > 0))
			{
				var entities = new List<TEntity>();
				foreach (var entity in base.Get(gatedResource.Keys.Select(pair => pair.Key)))
				{
					if (predicate(entity))
					{
						UpdateAcceptedInstances(entity);
						entities.Add(entity);
					}
					else
					{
						UpdateGatedInstances(entity);
					}
				}

				if (entities.Count > 0)
				{
					gatedResource.Subject.OnNext(entities);
				}
			}
		}

		/// <summary>
		/// Sets gating on all previously retrieved entities and all values updated on previously subscribed observables
		/// </summary>
		public void SetGating(bool on)
		{
			if (_defaultGating != on)
			{
				_defaultGating = on;
				_gatingStateSubject.OnNext(on ? GatingState.FullyGated : GatingState.NotGated);
			}

			var previouslySeenKeysToGet =
				_resources
					.Where(x => x.Value.PreviouslyRetrieved && x.Value.IsGated != on)
					.Select(x => x.Key)
					.ToList(); // We need the ToList here as we dont want this to be evaluated multiple times throughout this method and return different results as the underlying collection is changing 
			if (!on)
			{
				var keysUpdatedOnGatedObservables = new HashSet<IKey<TEntity>>(_keysUpdatedOnGatedObservables.SelectMany(x => x.Value.Keys.Select(pair => pair.Key)));

				if (previouslySeenKeysToGet.Count > 0)
				{
					foreach (var entity in base.Get(previouslySeenKeysToGet))
					{
						UpdateAcceptedInstances(entity, false);
						if (keysUpdatedOnGatedObservables.Remove((IKey<TEntity>)entity.Key))
						{
							UpdateObservablesWithAcceptedEntity(entity);
						}
					}
				}

				if (keysUpdatedOnGatedObservables.Count > 0)
				{
					foreach (var entity in Get(keysUpdatedOnGatedObservables))
					{
						UpdateAcceptedInstances(entity, false);
						UpdateObservablesWithAcceptedEntity(entity);
					}
				}
			}
			else
			{
				if (previouslySeenKeysToGet.Count > 0)
				{
					foreach (var entity in base.Get(previouslySeenKeysToGet))
					{
						UpdateAcceptedInstances(entity, true);
					}
				}
			}
		}

		/// <summary>
		/// Sets gating for a specified key so that future gets of entities that have that key return the current 
		/// value.  The only caveat to this is that if the key has not previously been retrieved then the next time 
		/// the key is retrieved, that value will be used as the accepted instance
		/// </summary>
		public void SetGating(IKey<TEntity> key, bool on)
		{
			if (!on && _keysUpdatedOnGatedObservables.Any(x => x.Value.Keys.ContainsKey(key)))
			{
				// At the point we turn gating off we want to update any observer with the latest version from the service
				Accept(key);
			}

			GatedResource<TEntity> existingResource;
			if (!_resources.TryGetValue(key, out existingResource) || existingResource.IsGated != on)
			{
				if (_resources
						.Where(x => x.Value.PreviouslyRetrieved && x.Value.IsGated != on)
						.Any(x => x.Key.Equals((IKey)key))) // we only want to get if we've previously got on this key
				{
					if (on)
					{
						var entityFromService = base.Get(new[] { key }).SingleOrDefault();
						if (entityFromService == null)
						{
							throw new EntityServiceException(string.Format("When setting gating for key {0}, entity service did not return a singular value on Get as was expected", key));
						}
						UpdateAcceptedInstances(entityFromService);
					}
				}

				var resource = _resources.AddOrUpdate(
					key,
					k => new GatedResource<TEntity> { IsGated = on },
					(k, old) =>
					{
						old.IsGated = on;
						return old;
					});
				SignalGateChange(key, resource);
				_gatingStateSubject.OnNext(IsGated());
			}
		}

		/// <summary>
		/// Returns whether a given key is currently gated and hence whether values returns from gets will come from 
		/// the underlying service or will be copies of previously retrieved values
		/// </summary>
		public bool IsGated(IKey<TEntity> key)
		{
			GatedResource<TEntity> gatedResource;
			return _resources.TryGetValue(key, out gatedResource)
				? gatedResource.IsGated
				: _defaultGating;
		}


		public IDisposable Subscribe(IObserver<GatingState> observer)
		{
			return _gatingStateSubject.Subscribe(observer);
		}

		/// <summary>
		/// This raises updates for entities already coming through the service
		/// </summary>
		public IDisposable Subscribe(IObserver<IGatingServiceObject> observer)
		{
			return _gatingServiceObjectSubject.Subscribe(observer);
		}

		public IEnumerable<IGatingServiceObject> GetLatestObjects()
		{
			return _gatingServiceObjects.Select(x => x.Value);
		}

		public GatingState IsGated()
		{
			if (_defaultGating)
			{
				if (_resources.Any(x => !x.Value.IsGated))
				{
					return GatingState.PartiallyGated;
				}
				return GatingState.FullyGated;
			}
			if (_resources.Any(x => x.Value.IsGated))
			{
				return GatingState.PartiallyGated;
			}
			return GatingState.NotGated;
		}



		private void SignalGateChange(IKey<TEntity> key, GatedResource<TEntity> gatedResource)
		{
			if (gatedResource != null && gatedResource.AcceptedInstance != null)
			{
				SignalGateChange(key, gatedResource.GatedInstanceTimestamp, gatedResource.AcceptedInstance.Timestamp);
			}
		}

		private void SignalGateChange(IKey<TEntity> key, DateTime currentTimestamp, DateTime acceptedTimestamp)
		{
			var gatingServiceObject = new GatingServiceObject(
				key.ToString(),
				currentTimestamp,
				acceptedTimestamp,
				on => SetGating(key, on),
				() => Accept(key),
				() => IsGated(key));
			_gatingServiceObjectSubject.OnNext(gatingServiceObject);
		}

		private TEntity GetEntityFromAcceptedArea(IKey<TEntity> key)
		{
			GatedResource<TEntity> gatedResource;
			if (_resources.TryGetValue(key, out gatedResource)
				&& gatedResource.AcceptedInstance != null
				&& gatedResource.AcceptedInstance.Instance != null)
			{
				return gatedResource.AcceptedInstance.Instance;
			}

			// If the value isnt already in the accepted area then we need to get it from the underlying service 
			// and put it in the accepted area.
			// This logic will get called only if we do a gate before a get.
			var entitiesFromService = base.Get(new[] { key });

			var entityFromService = entitiesFromService.FirstOrDefault();
			if (entityFromService != null && entitiesFromService.Skip(1).FirstOrDefault() != null)
			{
				throw new InvalidOperationException("More than one entity was returned for a unique key get.");
			}

			var now = _dateTimeService.UtcNow();
			var resource = _resources.AddOrUpdate(
				key,
				x => new GatedResource<TEntity>
				{
					AcceptedInstance = new TimestampedInstance<TEntity>
					{
						Instance = entityFromService,
						Timestamp = now
					},
					GatedInstanceTimestamp = now,
					PreviouslyRetrieved = true,
					IsGated = _defaultGating
				},
				(x, old) =>
				{
					if (old.AcceptedInstance == null)
					{
						old.AcceptedInstance = new TimestampedInstance<TEntity>
						{
							Instance = entityFromService,
							Timestamp = now
						};
						old.GatedInstanceTimestamp = now;
						old.PreviouslyRetrieved = true;
					}
					return old;
				});
			SignalGateChange(key, resource);
			return resource.AcceptedInstance.Instance;
		}

		private void UpdateObservablesWithAcceptedEntity(TEntity entity)
		{
			// This updates all appropriate subscribers and removes the key from the list as the list should only 
			// contains keys that have been updated on the gated observable and not passed down to the subscriber
			var entityKey = entity.Key as IKey<TEntity>;
			var subjects = new Dictionary<Subject<IEnumerable<TEntity>>, List<TEntity>>();
			foreach (var gatedObservableResource in _keysUpdatedOnGatedObservables.Select(pair => pair.Value))
			{
				var entities = new List<TEntity>();
				subjects.Add(gatedObservableResource.Subject, entities);

				bool removedItem;
				if (gatedObservableResource.Keys.TryRemove(entityKey, out removedItem))
				{
					entities.Add(entity);
				}
			}

			foreach (var pair in subjects)
			{
				pair.Key.OnNext(pair.Value);
			}
		}

		private void UpdateGatedInstances(TEntity entity)
		{
			var key = (IKey<TEntity>)entity.Key;
			var now = _dateTimeService.UtcNow();
			_resources.AddOrUpdate(
				key,
				k =>
				{
					var gatedResource = new GatedResource<TEntity>
					{
						GatedInstanceTimestamp = now,
						PreviouslyRetrieved = true,
						IsGated = IsGated(key),
						AcceptedInstance = new TimestampedInstance<TEntity>
						{
							Instance = entity,
							Timestamp = now
						}
					};
					SignalGateChange(k, gatedResource);
					return gatedResource;
				},
				(k, old) =>
				{
					old.GatedInstanceTimestamp = entity.Timestamp;
					if (old.AcceptedInstance == null)
					{
						old.AcceptedInstance = new TimestampedInstance<TEntity>
						{
							Instance = entity,
							Timestamp = now
						};
					}
					SignalGateChange(k, old);
					return old;
				});
		}

		private void UpdateAcceptedInstances(TEntity entity, bool? gating = null)
		{

			var key = (IKey<TEntity>)entity.Key;
			_resources.AddOrUpdate(
				key,
				k =>
				{
					var gatedResource = new GatedResource<TEntity>
					{
						AcceptedInstance = new TimestampedInstance<TEntity>
						{
							Instance = entity,
							Timestamp = entity.Timestamp
						},
						GatedInstanceTimestamp = entity.Timestamp
					};
					if (gating != null)
					{
						gatedResource.IsGated = (bool)gating;
					}
					SignalGateChange(k, gatedResource);
					return gatedResource;
				},
				(k, old) =>
				{
					if (old.AcceptedInstance == null || old.AcceptedInstance.Instance != entity)
					{
						old.AcceptedInstance = new TimestampedInstance<TEntity>
						{
							Instance = entity,
							Timestamp = entity.Timestamp
						};
						old.GatedInstanceTimestamp = entity.Timestamp;
						SignalGateChange(k, old);
					}

					if (gating != null)
					{
						old.IsGated = (bool)gating;
					}
					return old;
				});
		}

		private void UpdateGatingServiceObjects(IGatingServiceObject gatingServiceObject)
		{
			_gatingServiceObjects.AddOrUpdate(
				gatingServiceObject.Key,
				k => gatingServiceObject,
				(k, old) => gatingServiceObject);
		}

		private static void SplitQueries<T>(IEnumerable<IQuery<T>> queries, out IList<IQuery<T>> gatingQueries, out IList<IQuery<T>> nonGatingQueries)
			where T : class, TEntity
		{
			gatingQueries = new List<IQuery<T>>();
			nonGatingQueries = new List<IQuery<T>>();
			foreach (var query in queries)
			{
				var gatingOptions = query as IWithOptionalGating;
				if (gatingOptions != null && gatingOptions.GatingOptions == GatingOptions.DontGate)
				{
					nonGatingQueries.Add(query);
				}
				else
				{
					gatingQueries.Add(query);
				}
			}
		}

		private class GatedObservableResource
		{
			public GatedObservableResource(Subject<IEnumerable<TEntity>> subject)
			{
				Subject = subject;
				Keys = new ConcurrentDictionary<IKey<TEntity>, bool>();
			}

			public ConcurrentDictionary<IKey<TEntity>, bool> Keys { get; private set; }
			public Subject<IEnumerable<TEntity>> Subject { get; private set; }
		}

	}

	internal class GatedResource<TEntity>
	{
		public TimestampedInstance<TEntity> AcceptedInstance { get; set; }
		public DateTime GatedInstanceTimestamp { get; set; }

		// We cant just use the accepted instance for this as we may set the gating before we get an 
		// instance and we still need to know that the key is gated
		public bool IsGated { get; set; }

		// We cant just accept that if a GatedResource exists then it has previously been got as it could 
		// have been created on a call to SetGating()
		public bool PreviouslyRetrieved { get; set; }
	}

	internal class TimestampedInstance<TEntity>
	{
		public TEntity Instance { get; set; }
		public DateTime Timestamp { get; set; }
	}
}