﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity.Utility;
using MorganStanley.Desktop.EntityServices.Entities;
using MorganStanley.Desktop.EntityServices.Keys;
using MorganStanley.Desktop.EntityServices.Queries;
using MorganStanley.Desktop.EntityServices.Services.Caching;
using MorganStanley.Desktop.EntityServices.Services.Cloning;
using MorganStanley.Desktop.EntityServices.Services.Exceptions;

namespace MorganStanley.Desktop.EntityServices
{
    /*internal class EntityReplayAndContinueService<TEntity> : EntityPassthroughService<TEntity> , IEntityReplayAndContinueService<TEntity>
        where TEntity : class, IEntity, IFetchable, ISubscribable
    {
        private readonly ConcurrentDictionary<Guid, Action<object>> _observedEntities = new ConcurrentDictionary<Guid, Action<object>>();
        private readonly IDictionary<object, Guid> _cachedObervables = new Dictionary<object, Guid>();

        //private readonly IEntityService<TEntity> _entityService;
        //private readonly IEntityServiceExceptionService _exceptionService;


        public EntityReplayAndContinueService(IEntityService<TEntity> entityService, IEntityServiceExceptionService exceptionService) : base(entityService, exceptionService)
        {
        }

        public override IObservable<ObservableResource<TObservable>> GetObservable<TObservable>(IEnumerable<IQuery<TObservable>> queries)
         {
             var hot = base.GetObservable(queries)
                 .Publish();

            hot.Connect();
            
            var observable = hot.Replay();
            
            observable.Connect();
             return observable;
         }

    }*/




    internal class EntityReplayAndContinueService<TEntity> : IEntityReplayAndContinueService<TEntity>
    where TEntity : class, IEntity
    {
        private readonly ConcurrentDictionary<IQuery, ConcurrentDictionary<IKey, TEntity>> _queryToEntities;
        private readonly Dictionary<IQuery, Tuple<int, IDisposable>> _subscriptions;

        private readonly IEntityService<TEntity> _baseEntityService;

        public EntityReplayAndContinueService(IEntityService<TEntity> baseEntityService)
        {
            _baseEntityService = baseEntityService;
            _queryToEntities = new ConcurrentDictionary<IQuery, ConcurrentDictionary<IKey, TEntity>>();
            _subscriptions = new Dictionary<IQuery, Tuple<int, IDisposable>>();
        }

        public IEnumerable<TFetchable> Get<TFetchable>(IEnumerable<IQuery<TFetchable>> queries) where TFetchable : class, TEntity, IFetchable
        {
            return _baseEntityService.Get(queries);
        }

        public IObservable<ObservableResource<TObservable>> GetObservable<TObservable>(IEnumerable<IQuery<TObservable>> queries) where TObservable : class, TEntity, ISubscribable
        {
            return Observable.Create<ObservableResource<TObservable>>(
                observer =>
                {
                    // Subscribe to each query individually to maintain a map of queries to latest entity
                    // received per key. We want to subscribe ASAP to ensure we don't miss any updates.
                    lock (_subscriptions)
                    {
                        foreach (var query in queries)
                        {
                            Tuple<int, IDisposable> refCountedSubscription;
                            if (_subscriptions.TryGetValue(query, out refCountedSubscription))
                            {
                                _subscriptions[query] = Tuple.Create(refCountedSubscription.Item1 + 1,
                                                                     refCountedSubscription.Item2);
                            }
                            else
                            {
                                var queryEntites = new ConcurrentDictionary<IKey, TEntity>();
                                if (!_queryToEntities.TryAdd(query, queryEntites))
                                {
                                    throw new ApplicationException("Failed to create query to key mapping");
                                }

                                var subscription =
                                    _baseEntityService.GetObservable(new[] { query }).Subscribe(
                                        observed =>
                                        {
                                            if (observed.IsFaulted)
                                            {
                                                return;
                                            }

                                            foreach (var observedEntity in observed.Entities)
                                            {
                                                var entity = observedEntity;
                                                queryEntites.AddOrUpdate(entity.Key,
                                                                         _ => entity,
                                                                         (_, __) => entity);
                                            }
                                        });

                                _subscriptions.Add(query, Tuple.Create(1, subscription));
                            }
                        }
                    }

                    // Replay Entities - Since LINQ uses lazy evaluation, there should be minimal time 
                    // between "sending" this and subscribing to the real feed. There is still a risk of 
                    // missing some updates, but since the individual queries are subscribed first the 
                    // observer should get the latest entites at the point where it reads the collection
                    var allMatchingEntities = Enumerable.Empty<TEntity>();
                    foreach (var query in queries)
                    {
                        ConcurrentDictionary<IKey, TEntity> queryEntities;
                        if (_queryToEntities.TryGetValue(query, out queryEntities))
                        {
                            allMatchingEntities = allMatchingEntities.Union(queryEntities.Values);
                        }
                    }
                    observer.OnNext(new ObservableResource<TObservable>(allMatchingEntities.Distinct().Cast<TObservable>()));

                    // Add the observer to base entity service
                    var baseSubscription = _baseEntityService.GetObservable(queries).Subscribe(observer);

                    return
                        () =>
                        {
                            // Stop updates to observer
                            baseSubscription.Dispose();

                            // Clear cached entites
                            lock (_subscriptions)
                            {
                                foreach (var query in queries)
                                {
                                    Tuple<int, IDisposable> refCountedSubscription;
                                    if (!_subscriptions.TryGetValue(query, out refCountedSubscription))
                                    {
                                        throw new ApplicationException("Failed to remove query to key mapping");
                                    }

                                    if (refCountedSubscription.Item1 == 1)
                                    {
                                        refCountedSubscription.Item2.Dispose();
                                        _subscriptions.Remove(query);

                                        ConcurrentDictionary<IKey, TEntity> removedEntities;
                                        _queryToEntities.TryRemove(query, out removedEntities);
                                    }
                                    else
                                    {
                                        _subscriptions[query] =
                                            Tuple.Create(refCountedSubscription.Item1 - 1, refCountedSubscription.Item2);
                                    }
                                }
                            }
                        };
                });
        }

        public void Publish<TPublishEntity>(IEnumerable<TPublishEntity> publishEntities) where TPublishEntity : class, IPublishable, IEntity<TEntity, IKey<TEntity>>
        {
            _baseEntityService.Publish(publishEntities);
        }

        public void Delete<TDeletable>(IEnumerable<IKey<TDeletable>> keys) where TDeletable : class, TEntity, IDeletable
        {
            _baseEntityService.Delete(keys);
        }
    }
}
