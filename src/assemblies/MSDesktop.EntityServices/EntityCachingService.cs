﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Reactive.Linq;
using MorganStanley.Desktop.EntityServices.Entities;
using MorganStanley.Desktop.EntityServices.Queries;
using MorganStanley.Desktop.EntityServices.Services.Caching;
using MorganStanley.Desktop.EntityServices.Services.Exceptions;

namespace MorganStanley.Desktop.EntityServices
{
    internal sealed class EntityCachingService<TEntity> : EntityStaticCachingService<TEntity>, IEntityCachingService<TEntity>
       where TEntity : class, ISubscribable, IFetchable, IEntity
    {
        private readonly ConcurrentDictionary<IQuery, CacheObject<IDisposable>> _updateStreams = new ConcurrentDictionary<IQuery, CacheObject<IDisposable>>();

        public EntityCachingService(
            IEntityService<TEntity> entityService,
            IEntityServiceExceptionService exceptionService,
            ICacheService cacheService,
            EntityServiceCachingAttribute cachingDefaults)
            : base(entityService, exceptionService, cacheService, cachingDefaults)
        {
        }

        public override IObservable<ObservableResource<TObservable>> GetObservable<TObservable>(IEnumerable<IQuery<TObservable>> queries)
        {
            return GetRefCountedObservable(queries, resolvedQueries =>
            {
                //split out cachable from non-cachable
                IList<IQuery<TObservable>> cachable;
                IList<IQuery<TObservable>> nonCachable;
                SplitQueries(resolvedQueries, out cachable, out nonCachable);

                var observable = Observable.Empty<ObservableResource<TObservable>>();

                if (cachable.Count > 0)
                {
                    observable = observable.Merge(base.GetObservable(resolvedQueries).Do(UpdateEntityCache));
                }

                if (nonCachable.Count > 0)
                {
                    observable = observable.Merge(base.GetObservable(resolvedQueries));
                }

                return observable;
            });
        }

        protected override void SubscribeToUpdates(IQuery query)
        {
            // create the entry in update streams
            // get the observable and subscribe to it
            // add the subscription dispose to the _updateStreams
            // on update, update the cache

            _updateStreams.GetOrAdd(query, new CacheObject<IDisposable>())
                          .SetValue(() => GetObservable(new[] { (IQuery<TEntity>)query }).Retry().Subscribe(newEntities => { }));
        }

        protected override void UnsubscribeFromUpdates(IQuery query)
        {
            // This removes the caches subscription to the observable, this doesnt mean that other references to the 
            // observable through GetObservable will be stopped.  So we just dispose of our subscription.  Its 
            // interesting to note though that once all subscriptions have been closed then the stream will be shut 
            // down.
            CacheObject<IDisposable> streamToRemove;
            if (_updateStreams.TryRemove(query, out streamToRemove))
            {
                var subscription = streamToRemove.Value;
                if (subscription != null)
                {
                    subscription.Dispose();
                }
            }
        }

        private static void SplitQueries<T>(IEnumerable<IQuery<T>> queries, out IList<IQuery<T>> cachingQueries, out IList<IQuery<T>> nonCachingQueries)
            where T : class, TEntity
        {
            cachingQueries = new List<IQuery<T>>();
            nonCachingQueries = new List<IQuery<T>>();
            foreach (var query in queries)
            {
                var cachingOptions = query as IWithOptionalCaching;
                if (cachingOptions != null && cachingOptions.CachingOptions == CachingOptions.DontCache)
                {
                    nonCachingQueries.Add(query);
                }
                else
                {
                    cachingQueries.Add(query);
                }
            }
        }
    }
}