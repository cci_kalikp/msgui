﻿using MorganStanley.Desktop.EntityServices.Keys;

namespace MorganStanley.Desktop.EntityServices
{
    public interface IEntity<out TKey> : IEntity
    {
        /// <summary>
        /// This property should return a unique string, to be used as a key
        /// in the Containers />, e.g. TradeID, OrderID, etc.
        /// </summary>
        /// <returns>
        /// The unique key to be used to store IEntities
        /// </returns>
        new TKey Key { get; }
    }

    public interface IEntity
    {
        IKey Key { get; }
    }
}
