﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.Desktop.EntityServices.Entities;

namespace MorganStanley.Desktop.EntityServices
{
    interface IEntityReplayAndContinueService
    {
        
    }
    interface IEntityReplayAndContinueService<TEntity> : IEntityService<TEntity>, IEntityReplayAndContinueService where TEntity : class, IEntity
    {
    }
}
