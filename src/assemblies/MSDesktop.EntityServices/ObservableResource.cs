﻿using System;
using System.Collections.Generic;
using MorganStanley.Desktop.EntityServices.Entities;

namespace MorganStanley.Desktop.EntityServices
{
    public sealed class ObservableResource<TEntity>
        where TEntity : class, IEntity
    {
        private readonly Exception _exception;
        private readonly IEnumerable<TEntity> _entities;

        public ObservableResource(IEnumerable<TEntity> entities)
        {
            _entities = entities;
        }

        public ObservableResource(Exception exception)
        {
            _exception = exception;
        }

        public Exception Exception { get { return _exception; } }
        public IEnumerable<TEntity> Entities { get { return _entities; } }

        public bool IsFaulted { get { return _exception != null; } }
    }
}
