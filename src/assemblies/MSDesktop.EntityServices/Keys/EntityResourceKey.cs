﻿using System;

namespace MorganStanley.Desktop.EntityServices.Keys
{
    public class EntityResourceKey<TEntity> : IEntityResourceKey<TEntity>
    {
        private readonly IQuery<TEntity> _query;
        private readonly Type _domainObjectType;

        public EntityResourceKey(IQuery<TEntity> query, Type entityType)
        {
            _query = query;
            _domainObjectType = entityType;
        }

        public IQuery<TEntity> Query { get { return _query; } }
        public Type EntityType { get { return _domainObjectType; } }

        public override bool Equals(object obj)
        {
            return Equals(obj as IEntityResourceKey<TEntity>);
        }

        public bool Equals(IEntityResourceKey<TEntity> other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(other.Query, Query) && Equals(other.EntityType, EntityType);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((Query != null ? Query.GetHashCode() : 0) * 397) ^ 
                    (EntityType != null ? EntityType.GetHashCode() : 0);
            }
        }

        public static bool operator ==(EntityResourceKey<TEntity> left, EntityResourceKey<TEntity> right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(EntityResourceKey<TEntity> left, EntityResourceKey<TEntity> right)
        {
            return !Equals(left, right);
        }
    }
}
