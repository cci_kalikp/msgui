﻿namespace MorganStanley.Desktop.EntityServices.Keys
{
    public interface ILocalable
    {
        bool IsLocal { get; }
    }
}