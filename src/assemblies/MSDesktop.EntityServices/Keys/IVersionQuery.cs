﻿using System;

namespace MorganStanley.Desktop.EntityServices.Keys
{
    public interface IVersionQuery<TEntity> : IVersionQuery, IQuery<TEntity>
    {
    }

    public interface IVersionQuery
    {
        IKey Key { get; }
        string ExtendedKey { get; }
        DateTime? ReferenceDate { get; }
        DateTime Timestamp { get; }
        bool IsExactMatch { get; }
        string UserId { get; }
    }
}