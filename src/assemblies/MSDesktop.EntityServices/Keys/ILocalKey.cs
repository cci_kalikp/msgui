﻿namespace MorganStanley.Desktop.EntityServices.Keys
{
    public interface ILocalKey
    {
        bool IsLocal { get; }
    }
}