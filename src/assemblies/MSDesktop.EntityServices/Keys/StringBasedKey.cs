﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using MorganStanley.Desktop.EntityServices.Queries;

namespace MorganStanley.Desktop.EntityServices.Keys
{
    /// <summary>
    /// The implementation is immutable so if a value needs to be changed a new key should be generated.
    /// </summary>
    [Serializable]
    public abstract class StringBasedKey : StringBasedQuery, IEquatable<StringBasedKey>, IXmlSerializable, IComparable, IComparable<string>, IKey
    {
        private bool _isLocal;

        private readonly string _localStringSuffix;

        protected StringBasedKey(KeySeperator seperator, IEnumerable<string> fields)
            : this(null, seperator, fields)
        {
        }

        protected StringBasedKey(KeySeperator seperator, params string[] fields)
            : this(null, seperator, fields)
        {
        }

        protected StringBasedKey(KeySeperator seperator, int numberOfFields)
            : this(null, seperator, numberOfFields)
        {
        }

        protected StringBasedKey(string localKeyString, KeySeperator seperator, IEnumerable<string> fields)
            : base(seperator, fields)
        {
            _localStringSuffix = localKeyString ?? "_<local>";
            _isLocal = (Query.EndsWith(_localStringSuffix));
        }

        protected StringBasedKey(string localKeyString, KeySeperator seperator, params string[] fields)
            : base(seperator, fields)
        {
            _localStringSuffix = localKeyString ?? "_<local>";
            _isLocal = (Query.EndsWith(_localStringSuffix));
        }

        protected StringBasedKey(string localKeyString, KeySeperator seperator, int numberOfFields)
            : base(seperator, numberOfFields)
        {
            _localStringSuffix = localKeyString ?? "_<local>";
        }

        public static T Create<T>(string key) where T : StringBasedKey, new()
        {
            var newkey = new T();
            newkey.Create(key);
            return newkey;
        }

        protected T CreateLocalKey<T>() where T : StringBasedKey, new()
        {
            if (!IsLocalKey())
            {
                var key = new T();
                key.Create(Query + _localStringSuffix);
                return key;
            }

            //Because a key is immutable we may as well return the same instance
            return (T)this;
        }

        protected T CreateNonLocalKey<T>() where T : StringBasedKey, new()
        {
            if (IsLocalKey())
            {
                var key = new T();
                key.Create(Query.Substring(0, Query.Length - _localStringSuffix.Length));
                return key;
            }

            //Because a key is immutable we may as well return the same instance
            return (T)this;
        }

        protected override void QueryPopulated()
        {
            base.QueryPopulated();
            _isLocal = (Query.EndsWith(_localStringSuffix));
        }

        protected bool IsLocalKey()
        {
            return _isLocal;
        }

        /// <summary>
        /// Equalses the specified other.
        /// </summary>
        /// <param name="other">The other.</param>
        /// <returns></returns>
        public bool Equals(StringBasedKey other)
        {
            return base.Equals(other);
        }

        public static explicit operator string(StringBasedKey key)
        {
            return key.ToString();
        }

        public bool Equals(IKey other)
        {
            return Equals(other as StringBasedKey);
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return Query;
        }


        public int CompareTo(string other)
        {
            return (other == null) ? 1 : Query.CompareTo(other);  // Nulls come first            
        }

        public int CompareTo(object obj)
        {
            if (!(obj is string))
            {
                return 1;
            }

            return CompareTo((string)obj);
        }

        public override bool Equals(object that)
        {
            return Equals(that as StringBasedKey);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// This method is reserved and should not be used. When implementing the IXmlSerializable interface, you should return null (Nothing in Visual Basic) from this method, and instead, if specifying a custom schema is required, apply the <see cref="T:System.Xml.Serialization.XmlSchemaProviderAttribute"/> to the class.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Xml.Schema.XmlSchema"/> that describes the XML representation of the object that is produced by the <see cref="M:System.Xml.Serialization.IXmlSerializable.WriteXml(System.Xml.XmlWriter)"/> method and consumed by the <see cref="M:System.Xml.Serialization.IXmlSerializable.ReadXml(System.Xml.XmlReader)"/> method.
        /// </returns>
        XmlSchema IXmlSerializable.GetSchema()
        {
            return null;
        }

        /// <summary>
        /// Generates an object from its XML representation.
        /// </summary>
        /// <param name="reader">The <see cref="T:System.Xml.XmlReader"/> stream from which the object is deserialized. 
        ///                 </param>
        void IXmlSerializable.ReadXml(XmlReader reader)
        {
            Create(reader.ReadElementString());
        }

        /// <summary>
        /// Converts an object into its XML representation.
        /// </summary>
        /// <param name="writer">The <see cref="T:System.Xml.XmlWriter"/> stream to which the object is serialized. 
        ///                 </param>
        void IXmlSerializable.WriteXml(XmlWriter writer)
        {
            writer.WriteString(IsEmpty ? string.Empty : Query);
        }
    }
}
