﻿using System;

namespace MorganStanley.Desktop.EntityServices.Keys
{
    public interface IEntityResourceKey<TEntity> : IEntityResourceKey, IEquatable<IEntityResourceKey<TEntity>>
    {
        IQuery<TEntity> Query { get; }
        Type EntityType { get; }
    }

    public interface IEntityResourceKey
    {
    }
}
