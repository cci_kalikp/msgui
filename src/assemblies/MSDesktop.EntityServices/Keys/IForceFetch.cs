﻿namespace MorganStanley.Desktop.EntityServices.Keys
{
    public interface IForceFetch
    {
        bool ForceFetch { get; set; }
    }
}
