﻿using System;
using System.Collections.Generic;
using MorganStanley.Desktop.EntityServices.Queries;

namespace MorganStanley.Desktop.EntityServices.Keys
{
    [Serializable]
    public sealed class KeySeperator :  QuerySeperator
    {
        private readonly QuerySeperator _seperator;
        public static new readonly KeySeperator Default = new KeySeperator(new CharSeperator('/'));

        public KeySeperator(QuerySeperator seperator)
        {
            _seperator = seperator;
        }

        public override string[] Split(string key)
        {
            return _seperator.Split(key);
        }

        public override string Join(IEnumerable<string> fields)
        {
            return _seperator.Join(fields);
        }
    }
}
