﻿using System;

namespace MorganStanley.Desktop.EntityServices.Keys
{
    [Serializable]
    public class VersionKey<T> : VersionKey, IVersionQuery<T>
    {
        public VersionKey(
            IKey key,
            DateTime timestamp,
            DateTime? referenceDate = null,
            bool isExactMatch = false)
            : base(key, timestamp, referenceDate, isExactMatch)
        {

        }

        public VersionKey(
            IKey key,
            string extendedKey,
            DateTime? referenceDate,
            DateTime timestamp,
            bool isExactMatch,
            string userId)
            : base(key, extendedKey, referenceDate, timestamp, isExactMatch, userId)
        {

        }

        public VersionKey(IVersionQuery versionKey)
            : base(versionKey.Key, versionKey.ExtendedKey, versionKey.ReferenceDate, versionKey.Timestamp, versionKey.IsExactMatch, versionKey.UserId)
        {
        }

        public virtual bool Equals(IQuery<T> other)
        {
            return Equals(other as VersionKey);
        }

        public new IKey<T> Key
        {
            get { return base.Key as IKey<T>; }
        }
    }

    [Serializable]
    public class VersionKey : IVersionQuery, IEquatable<VersionKey>
    {
        private readonly string _summary;
        private readonly Tuple<IKey, DateTime, DateTime?, bool, string, string> _equalityKey;

        public VersionKey(
            IKey key,
            DateTime timestamp,
            DateTime? referenceDate = null,
            bool isExactMatch = false)
            : this(key, null, referenceDate, timestamp, isExactMatch, Environment.UserName.ToLower())
        {
        }

        public VersionKey(
            IKey key,
            string extendedKey,
            DateTime? referenceDate,
            DateTime timestamp,
            bool isExactMatch,
            string userId)
        {
            Key = key;
            ExtendedKey = extendedKey;
            ReferenceDate = referenceDate;
            Timestamp = timestamp;
            IsExactMatch = isExactMatch;
            UserId = userId;
            _summary = string.Format("{0} ReferenceDate[{1}] Timestamp(GMT)[{2}] [{3}]", Key, ReferenceDate, Timestamp, UserId);
            _equalityKey = Tuple.Create(Key, Timestamp, ReferenceDate, IsExactMatch, extendedKey, userId);
        }

        public IKey Key { get; private set; }
        public string ExtendedKey { get; private set; }

        public DateTime? ReferenceDate { get; private set; }

        public DateTime Timestamp { get; private set; }

        public bool IsExactMatch { get; private set; }

        public string UserId { get; private set; }

        public override string ToString()
        {
            return _summary;
        }

        public bool Equals(VersionKey other)
        {
            if (other == null)
            {
                return false;
            }

            return _equalityKey.Equals(other._equalityKey);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as VersionKey);
        }

        public override int GetHashCode()
        {
            return _equalityKey.GetHashCode();
        }

        public static bool operator ==(VersionKey left, VersionKey right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(VersionKey left, VersionKey right)
        {
            return !Equals(left, right);
        }
    }
}

