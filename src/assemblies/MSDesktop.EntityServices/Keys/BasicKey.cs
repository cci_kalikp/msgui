﻿using System;
using MorganStanley.Desktop.EntityServices.Queries;

namespace MorganStanley.Desktop.EntityServices.Keys
{
    [Serializable]
    public class BasicKey<TEntity> : IEquatable<BasicKey<TEntity>>, IKey<TEntity>
    {
        private readonly string _key;

        public BasicKey(string key)
        {
            if (key == null)
            {
                throw new ArgumentNullException("key", "Key argument must be a valid string but instead it was null");
            }
            _key = key;
        }

        /// <summary>
        /// Equalses the specified other.
        /// </summary>
        /// <param name="other">The other.</param>
        /// <returns></returns>
        public bool Equals(BasicKey<TEntity> other)
        {
            if (other == null)
            {
                return false;
            }
            if (ReferenceEquals(this, other))
            {
                return true;
            }
            // return GetType() == other.GetType() && _key == other._key;
            return _key == other._key;
        }

        public bool Equals(IQuery<TEntity> other)
        {
            return Equals(other as BasicKey<TEntity>);
        }

        public bool Equals(IKey<TEntity> other)
        {
            return Equals(other as BasicKey<TEntity>);
        }

        public bool Equals(IKey other)
        {
            return Equals(other as BasicKey<TEntity>);
        }

        public bool Equals(IQuery other)
        {
            return Equals(other as IQuery<TEntity>);
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return _key;
        }

        public override int GetHashCode()
        {
            return _key.GetHashCode();
        }

        public override bool Equals(object that)
        {
            return Equals(that as BasicKey<TEntity>);
        }
    }
}
