﻿using System;
using MorganStanley.Desktop.EntityServices.Queries;

namespace MorganStanley.Desktop.EntityServices.Keys
{
    public interface IKey<TEntity> : IKey, IOneToOneQuery<TEntity>
    {
    }

    public interface IKey : IOneToOneQuery, IEquatable<IKey>
    {
    }
}
