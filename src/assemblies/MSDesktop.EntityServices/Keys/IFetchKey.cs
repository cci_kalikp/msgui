﻿namespace MorganStanley.Desktop.EntityServices.Keys
{
    /// <summary>
    /// Indicates that the key can be used for fetching, not just observing, where T is the entity
    /// </summary>
    public interface IFetchKey
    {
    }
}
