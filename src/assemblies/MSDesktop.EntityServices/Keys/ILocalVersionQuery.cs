﻿namespace MorganStanley.Desktop.EntityServices.Keys
{
    public interface ILocalVersionQuery<TEntity> : IVersionQuery<TEntity>
    {
        bool IsLocal { get; }
    }
}