﻿using System;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace MorganStanley.Desktop.EntityServices.Keys
{
    /// <summary>
    /// The implementation is immutable so if a value needs to be changed a new key should be generated.
    /// </summary>
    [Serializable]
    public abstract class Key : IEquatable<Key>, IXmlSerializable, IComparable, IComparable<string>, IKey
    {
        private string[] _parts;
        private string _key;
        private bool _isLocal;

        private readonly string _localStringSuffix;

        protected Key(KeySeperator seperator, int numberOfParts, params string[] fields)
        {
            if (seperator == null)
            {
                seperator = KeySeperator.Default;
            }

            _localStringSuffix = GetLocalKeyString();

            Seperator = seperator;
            PartCount = numberOfParts;

            if (fields == null || fields.Length == 0)
            {
                _parts = new string[PartCount];
                _key = string.Empty;
                IsEmpty = true;
            }
            else
            {
                _key = seperator.Join(fields);
                _parts = fields;
                _isLocal = (_key.EndsWith(_localStringSuffix));
                IsEmpty = false;
            }


        }

        public static T Create<T>(string key) where T : Key, new()
        {
            var newkey = new T();
            newkey.Create(key);
            return newkey;
        }

        public int PartCount { get; private set; }

        public KeySeperator Seperator { get; private set; }

        public bool IsEmpty { get; private set; }

        public string this[int index]
        {
            get
            {
                if (index < 0 || index > PartCount - 1)
                {
                    throw new ArgumentException(string.Format("part {0} is out of range for the size for the Key part length of {1}", _key, PartCount));
                }
                return _parts[index];
            }
        }

        private void Create(string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                _parts = new string[PartCount];
                _key = string.Empty;
                IsEmpty = true;
                return;
            }

            var parts = Seperator.Split(key);
            ValidatePartLength(ref parts);
            ValidateParts(parts);

            _key = Seperator.Join(parts); //rejoin rather than using the key passed in incase the seperator uses different joiing logic to the split logic
            _parts = parts;
            _isLocal = (_key.EndsWith(_localStringSuffix));
            IsEmpty = false;
        }

        protected virtual void ValidatePartLength(ref string[] parts)
        {
            if (parts.Length != PartCount)
            {
                throw new InvalidOperationException(string.Format("The key should have contained {0} parts but instead contains {1}", PartCount, parts.Length));
            }
        }

        protected virtual void ValidateParts(string[] parts)
        {

        }

        protected virtual string GetLocalKeyString()
        {
            return "_<local>";
        }

        protected T CreateLocalKey<T>() where T : Key, new()
        {
            if (!IsLocalKey())
            {
                var key = new T();
                key.Create(_key + _localStringSuffix);
                return key;
            }

            //Because a key is immutable we may as well return the same instance
            return (T)this;
        }

        protected T CreateNonLocalKey<T>() where T : Key, new()
        {
            if (IsLocalKey())
            {
                var key = new T();
                key.Create(_key.Substring(0, _key.Length - _localStringSuffix.Length));
                return key;
            }

            //Because a key is immutable we may as well return the same instance
            return (T)this;
        }

        protected bool IsLocalKey()
        {
            return _isLocal;
        }

        /// <summary>
        /// Equalses the specified other.
        /// </summary>
        /// <param name="other">The other.</param>
        /// <returns></returns>
        public bool Equals(Key other)
        {
            if (other == null)
            {
                return false;
            }

            return GetType() == other.GetType() && _key == other._key;
        }

        public static implicit operator string(Key key)
        {
            return key.ToString();
        }

        public bool Equals(IKey other)
        {
            return Equals(other as Key);
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return _key;
        }

        public override int GetHashCode()
        {
            return _key.GetHashCode();
        }

        public int CompareTo(string other)
        {
            return (other == null) ? 1 : _key.CompareTo(other);  // Nulls come first            
        }

        public int CompareTo(object obj)
        {
            if (!(obj is string))
            {
                return 1;
            }

            return CompareTo((string)obj);
        }

        public override bool Equals(object that)
        {
            return Equals(that as Key);
        }

        /// <summary>
        /// This method is reserved and should not be used. When implementing the IXmlSerializable interface, you should return null (Nothing in Visual Basic) from this method, and instead, if specifying a custom schema is required, apply the <see cref="T:System.Xml.Serialization.XmlSchemaProviderAttribute"/> to the class.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Xml.Schema.XmlSchema"/> that describes the XML representation of the object that is produced by the <see cref="M:System.Xml.Serialization.IXmlSerializable.WriteXml(System.Xml.XmlWriter)"/> method and consumed by the <see cref="M:System.Xml.Serialization.IXmlSerializable.ReadXml(System.Xml.XmlReader)"/> method.
        /// </returns>
        XmlSchema IXmlSerializable.GetSchema()
        {
            return null;
        }

        /// <summary>
        /// Generates an object from its XML representation.
        /// </summary>
        /// <param name="reader">The <see cref="T:System.Xml.XmlReader"/> stream from which the object is deserialized. 
        ///                 </param>
        void IXmlSerializable.ReadXml(XmlReader reader)
        {
            Create(reader.ReadElementString());
        }

        /// <summary>
        /// Converts an object into its XML representation.
        /// </summary>
        /// <param name="writer">The <see cref="T:System.Xml.XmlWriter"/> stream to which the object is serialized. 
        ///                 </param>
        void IXmlSerializable.WriteXml(XmlWriter writer)
        {
            writer.WriteString(IsEmpty ? string.Empty : _key);
        }
    }
}
