﻿using System;

namespace MorganStanley.Desktop.EntityServices.Keys
{
    public class LocalVersionQuery<TEntity> : VersionKey<TEntity>, ILocalVersionQuery<TEntity>, IEquatable<LocalVersionQuery<TEntity>>
    {
        private readonly Tuple<IKey, DateTime, DateTime?, bool, string, string, bool> _equalityKey;
        private readonly string _summary;

        public LocalVersionQuery(
            IKey key,
            DateTime timestamp,
            DateTime? referenceDate,
            bool isExactMatch,
            string userId,
            bool isLocal)
            : base(key, null, referenceDate, timestamp, isExactMatch, userId)
        {
            IsLocal = isLocal;
            _equalityKey = Tuple.Create((IKey)Key, Timestamp, ReferenceDate, IsExactMatch, ExtendedKey, UserId, IsLocal);
            _summary = base.ToString() + " isLocal:" + IsLocal;
        }

        public bool IsLocal { get; private set; }

        public override bool Equals(IQuery<TEntity> other)
        {
            return Equals(other as LocalVersionQuery<TEntity>);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as LocalVersionQuery<TEntity>);
        }

        public bool Equals(LocalVersionQuery<TEntity> other)
        {
            return other != null && _equalityKey.Equals(other._equalityKey);
        }

        public override int GetHashCode()
        {
            return _equalityKey.GetHashCode();
        }

        public override string ToString()
        {
            return _summary;
        }

        public static bool operator ==(LocalVersionQuery<TEntity> left, LocalVersionQuery<TEntity> right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(LocalVersionQuery<TEntity> left, LocalVersionQuery<TEntity> right)
        {
            return !Equals(left, right);
        }
    }

}