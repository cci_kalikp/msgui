﻿using System;

namespace MorganStanley.Desktop.EntityServices.Keys
{
    public interface IQuery<TEntity> : IEquatable<IQuery<TEntity>>
    {
    }
}