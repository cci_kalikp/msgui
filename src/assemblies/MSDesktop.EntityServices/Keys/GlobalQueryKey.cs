﻿using System;

namespace MorganStanley.Desktop.EntityServices.Keys
{
    [Serializable]
    public class GlobalQueryKey<T> : IQuery<T>
    {
        public bool Equals(IQuery<T> other)
        {
            return other is GlobalQueryKey<T>;
        }

        public override bool Equals(object obj)
        {
            return obj is GlobalQueryKey<T>;
        }

        public bool Equals(GlobalQueryKey<T> other)
        {
            return true;
        }

        public override int GetHashCode()
        {
            return 69;
        }
    }
}
