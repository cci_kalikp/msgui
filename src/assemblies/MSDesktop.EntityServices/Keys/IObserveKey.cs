﻿namespace MorganStanley.Desktop.EntityServices.Keys
{
    /// <summary>
    /// Include the key in the ecosystem, where T is the entity.  IKey is the existing interface that implements all the needed members of a key
    /// </summary>
    public interface IObserveKey : IFetchKey
    {
    }
}
