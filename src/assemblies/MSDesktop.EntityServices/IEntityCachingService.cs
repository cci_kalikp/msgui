﻿using MorganStanley.Desktop.EntityServices.Entities;

namespace MorganStanley.Desktop.EntityServices
{
    internal interface IEntityCachingService<TEntity> : IEntityService<TEntity>
        where TEntity : class, IEntity
    {
    }
}
