﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using MorganStanley.Desktop.EntityServices.Entities;
using MorganStanley.Desktop.EntityServices.Services.Caching;
using MorganStanley.Desktop.EntityServices.Services.Cloning;
using MorganStanley.Desktop.EntityServices.Services.Dates;
using MorganStanley.Desktop.EntityServices.Services.Exceptions;
using MorganStanley.Desktop.EntityServices.Services.Threading;

namespace MorganStanley.Desktop.EntityServices
{
    public static class UnityExtentions
    {
        public static IUnityContainer RegisterEntityService<TEntity, TConnector, TConverter>(this IUnityContainer container)
            where TEntity : class, IEntity
            where TConnector : class, IEntityServiceConnector
            where TConverter : class, IEntityServiceConverter<TEntity>
        {
            return container.RegisterEntityService(typeof(TEntity), typeof(TConnector), typeof(TConverter));
        }

        public static IUnityContainer RegisterEntityService(this IUnityContainer container, Type entityType, Type connectorType, Type converterType)
        {
            return container.RegisterEntityService(entityType, new ResolvedParameter(connectorType), new ResolvedParameter(converterType));
        }

        public static IUnityContainer RegisterEntityService(this IUnityContainer container, Type entityType, TypedInjectionValue connectorParameter, TypedInjectionValue converterParameter)
        {
            var attributes = entityType.GetCustomAttributes(typeof(EntityServiceAttribute), false);
            return RegisterEntityService(container, entityType, connectorParameter, converterParameter, attributes.Length == 0 ? ServiceMode.None : ((EntityServiceAttribute)attributes[0]).Mode, true, null, null);
        }

        public static IUnityContainer RegisterEntityService<TEntity, TConnector, TConverter>(this IUnityContainer container, ServiceMode mode)
            where TEntity : class, IEntity
            where TConnector : class, IEntityServiceConnector
            where TConverter : class, IEntityServiceConverter<TEntity>
        {
            return RegisterEntityService(container, typeof(TEntity), new ResolvedParameter(typeof(TConnector)), new ResolvedParameter(typeof(TConverter)), mode, false, null, null);
        }

        private static IUnityContainer RegisterEntityService(IUnityContainer container, Type entityType, TypedInjectionValue connectorParameter, TypedInjectionValue converterParameter, ServiceMode mode, bool enforceClonability, EntityServiceCachingAttribute cachingDefaults, EntityServiceConcurrencyAttribute concurrencyDefaults)
        {
            if (entityType.IsValueType)
            {
                throw new ArgumentException("entityType cannot be a value type", "entityType");
            }

            if (!typeof(IEntity).IsAssignableFrom(entityType))
            {
                throw new ArgumentException("entityType must inherit from IEntity", "entityType");
            }

            var connectorType = connectorParameter.ParameterType;
            var converterType = converterParameter.ParameterType;

            if (connectorType.IsValueType)
            {
                throw new ArgumentException("containerType cannot be a value type", "connectorParameter");
            }

            if (!typeof(IEntityServiceConnector).IsAssignableFrom(connectorType))
            {
                throw new ArgumentException("containerType must inherit from IEntityServiceConnector", "connectorParameter");
            }

            if (converterType.IsValueType)
            {
                throw new ArgumentException("converterType cannot be a value type", "converterParameter");
            }

            if (!(converterType.IsGenericType &&
               (converterType.IsInterface && converterType.GetGenericTypeDefinition() == typeof(IEntityServiceConverter<>)) ||
               converterType.GetInterfaces().Any(x => x.IsGenericType && x.GetGenericTypeDefinition() == typeof(IEntityServiceConverter<>))) &&
               entityType.IsAssignableFrom(converterType.GetGenericArguments()[0]))
            {
                throw new ArgumentException("converterType must inherit from IEntityServiceConverter<> and the entity must be the same as the entity type", "converterParameter");
            }

            var isMutable = (entityType.GetFields().Length > 0 || entityType.GetProperties().Any(property => property.CanWrite && property.GetSetMethod() != null && !property.GetSetMethod().IsPrivate));

            //Cloning is needed if:
            // 1) The entity is not imutable
            // 2) The entity is subscribable OR the caching, gating, tapping or local service
            if (enforceClonability && isMutable && !mode.HasFlag(ServiceMode.Cloning))
            {
                if (entityType.GetInterfaces().Any(iface => iface == typeof(ISubscribable)) || mode.HasFlag(ServiceMode.Caching) || mode.HasFlag(ServiceMode.Gating) || mode.HasFlag(ServiceMode.Local) || mode.HasFlag(ServiceMode.Tapped))
                {
                    throw new InvalidOperationException(string.Format("Entity '{0}' has either public fields or public setters but has not requested cloning. When the entity is subscribable OR the caching, gating, tapped or local services are used, in order to protect thread safety, DTO's must either be imutable or have cloning enabled.", entityType.Name));
                }
            }

            RegisterServices(container);

            var previousType = typeof(EntityService<>).MakeGenericType(entityType);

            container.RegisterType(
                previousType,
                new ContainerControlledLifetimeManager(),
                new InjectionConstructor(
                    connectorParameter,
                    converterParameter,
                    new ResolvedParameter<IEntityServiceExceptionService>()));



            //The order the below are registered in is important and logically correct.
            //Don't change the order around!

            //tapped
            if (mode.HasFlag(ServiceMode.Tapped))
            {
                previousType = RegisterTappedService(container, entityType, previousType, isMutable);
            }

            //caching
            if (mode.HasFlag(ServiceMode.Caching))
            {
                if (!typeof(IFetchable).IsAssignableFrom(entityType))
                {
                    throw new InvalidOperationException(string.Format("Entity '{0}' must implement IFetchable to use the Caching service", entityType.FullName));
                }


                if (cachingDefaults == null)
                {
                    var cachingAttributes = entityType.GetCustomAttributes(typeof(EntityServiceCachingAttribute), false);
                    cachingDefaults = cachingAttributes.Length == 0
                                             ? new EntityServiceCachingAttribute()
                                             : (EntityServiceCachingAttribute)cachingAttributes[0];
                }


                //if you've asked for caching then the entity needs to be subscribable
                previousType = typeof(ISubscribable).IsAssignableFrom(entityType)
                                   ? RegisterCachingService(container, entityType, cachingDefaults, previousType)
                                   : RegisterStaticCachingService(container, entityType, cachingDefaults, previousType);
            }

            //gating
            if (mode.HasFlag(ServiceMode.Gating))
            {
                if (!typeof(ISubscribable).IsAssignableFrom(entityType) ||
                    !typeof(IFetchable).IsAssignableFrom(entityType) ||
                    !typeof(ITimestamped).IsAssignableFrom(entityType))
                {
                    throw new InvalidOperationException(string.Format("Entity '{0}' must implement IFetchable, ISubscribable and ITimestamped to use the Gating service", entityType.FullName));
                }
                previousType = RegisterGatingService(container, entityType, previousType);
            }

            //replay
            if (mode.HasFlag(ServiceMode.Replay))
            {
                if (!typeof(IFetchable).IsAssignableFrom(entityType))
                {
                    throw new InvalidOperationException(string.Format("Entity '{0}' must implement IFetchable to use the Replay service", entityType.FullName));
                }
                previousType = RegisterReplayService(container, entityType, previousType);
            }

            //local
            if (mode.HasFlag(ServiceMode.Local))
            {
                previousType = RegisterLocalService(container, entityType, previousType);
            }

            //clone
            if (mode.HasFlag(ServiceMode.Cloning))
            {
                previousType = RegisterCloningService(container, entityType, previousType);
            }

            if (entityType.GetInterfaces().Any(x => x == typeof(ISubscribable)))
            {
                //Always ensure that each observer receives the observation on its own thread if its observable
                previousType = RegisterThreadedObservableService(container, entityType, previousType, concurrencyDefaults);
            }

            container.RegisterType(typeof(IEntityService<>).MakeGenericType(entityType),
                                   new ContainerControlledLifetimeManager(),
                                   new InjectionFactory(cont => cont.Resolve(previousType)));
            return container;
        }


        private static Type RegisterTappedService(IUnityContainer container, Type entityType, Type previousEntityService, bool needsCloning)
        {
            var entityServiceInterface = typeof(IEntityService<>).MakeGenericType(entityType);
            var serviceInterface = typeof(IEntityTappedService<>).MakeGenericType(entityType);
            var serviceClass = typeof(EntityTappedService<>).MakeGenericType(entityType);

            container.RegisterType(serviceInterface,
                                   new ContainerControlledLifetimeManager(),
                                   new InjectionFactory(cont => cont.Resolve(serviceClass, new DependencyOverride(entityServiceInterface, cont.Resolve(previousEntityService)).OnType(serviceClass),
                                                                                           new DependencyOverride<IEntityCloning>(needsCloning ? cont.Resolve<EntityCloning>() : (IEntityCloning)cont.Resolve<NoOpEntityCloning>()).OnType(serviceClass))));


            return serviceInterface;
        }

        private static Type RegisterStaticCachingService(IUnityContainer container, Type entityType, EntityServiceCachingAttribute cachingDefault, Type previousEntityService)
        {
            var entityServiceInterface = typeof(IEntityService<>).MakeGenericType(entityType);
            var serviceInterface = typeof(IEntityStaticCachingService<>).MakeGenericType(entityType);
            var serviceClass = typeof(EntityStaticCachingService<>).MakeGenericType(entityType);

            container.RegisterType(serviceInterface,
                                   new ContainerControlledLifetimeManager(),
                                   new InjectionFactory(cont => cont.Resolve(serviceClass,
                                                                             new DependencyOverride(entityServiceInterface, cont.Resolve(previousEntityService)).OnType(serviceClass),
                                                                             new DependencyOverride<EntityServiceCachingAttribute>(cachingDefault).OnType(serviceClass))));

            return serviceInterface;
        }

        private static Type RegisterCachingService(IUnityContainer container, Type entityType, EntityServiceCachingAttribute cachingDefault, Type previousEntityService)
        {
            var entityServiceInterface = typeof(IEntityService<>).MakeGenericType(entityType);
            var serviceInterface = typeof(IEntityCachingService<>).MakeGenericType(entityType);
            var serviceClass = typeof(EntityCachingService<>).MakeGenericType(entityType);

            container.RegisterType(serviceInterface,
                                   new ContainerControlledLifetimeManager(),
                                   new InjectionFactory(cont => cont.Resolve(serviceClass,
                                                                             new DependencyOverride(entityServiceInterface, cont.Resolve(previousEntityService)).OnType(serviceClass),
                                                                             new DependencyOverride<EntityServiceCachingAttribute>(cachingDefault).OnType(serviceClass))));

            return serviceInterface;
        }

        private static Type RegisterGatingService(IUnityContainer container, Type entityType, Type previousEntityService)
        {
            var entityServiceInterface = typeof(IEntityService<>).MakeGenericType(entityType);
            var serviceInterface = typeof(IEntityGatingService<>).MakeGenericType(entityType);
            var serviceClass = typeof(EntityGatingService<>).MakeGenericType(entityType);

            container.RegisterType(
                serviceInterface,
                new ContainerControlledLifetimeManager(),
                new InjectionFactory(cont => cont.Resolve(serviceClass, new DependencyOverride(entityServiceInterface, cont.Resolve(previousEntityService)).OnType(serviceClass))));

            container.RegisterType(
                typeof(IEntityGatingService),
                entityType.FullName,
                new InjectionFactory(cont => cont.Resolve(serviceInterface)));

            return serviceInterface;
        }

        private static Type RegisterReplayService(IUnityContainer container, Type entityType, Type previousEntityService)
        {
            var entityServiceInterface = typeof(IEntityService<>).MakeGenericType(entityType);
            var serviceInterface = typeof(IEntityReplayService<>).MakeGenericType(entityType);
            var serviceClass = typeof(EntityReplayService<>).MakeGenericType(entityType);

            container.RegisterType(
                serviceInterface,
                new ContainerControlledLifetimeManager(),
                new InjectionFactory(cont => cont.Resolve(serviceClass, new DependencyOverride(entityServiceInterface, cont.Resolve(previousEntityService)).OnType(serviceClass))));


            container.RegisterType(
                typeof(IEntityReplayService),
                entityType.FullName,
                new ContainerControlledLifetimeManager(),
                new InjectionFactory(cont => cont.Resolve(serviceInterface)));

            return serviceInterface;
        }

        private static Type RegisterLocalService(IUnityContainer container, Type entityType, Type previousEntityService)
        {
            var entityServiceInterface = typeof(IEntityService<>).MakeGenericType(entityType);
            var serviceInterface = typeof(IEntityLocalService<>).MakeGenericType(entityType);
            var serviceClass = typeof(EntityLocalService<>).MakeGenericType(entityType);

            container.RegisterType(
                serviceInterface,
                new ContainerControlledLifetimeManager(),
                new InjectionFactory(cont => cont.Resolve(serviceClass, new DependencyOverride(entityServiceInterface, cont.Resolve(previousEntityService)).OnType(serviceClass))));

            return serviceInterface;
        }

        private static Type RegisterCloningService(IUnityContainer container, Type entityType, Type previousEntityService)
        {
            var entityServiceInterface = typeof(IEntityService<>).MakeGenericType(entityType);
            var serviceInterface = typeof(IEntityCloningService<>).MakeGenericType(entityType);
            var serviceClass = typeof(EntityCloningService<>).MakeGenericType(entityType);

            container.RegisterType(serviceInterface,
                                   new ContainerControlledLifetimeManager(),
                                   new InjectionFactory(cont => cont.Resolve(serviceClass, new DependencyOverride(entityServiceInterface, cont.Resolve(previousEntityService)).OnType(serviceClass),
                                                                                           new DependencyOverride<IEntityCloning>(cont.Resolve<EntityCloning>()).OnType(serviceClass))));

            return serviceInterface;
        }

        private static Type RegisterThreadedObservableService(IUnityContainer container, Type entityType, Type previousEntityService, EntityServiceConcurrencyAttribute concurrencyDefaults)
        {
            if (concurrencyDefaults == null)
            {
                var concurrencyAttributes = entityType.GetCustomAttributes(typeof(EntityServiceConcurrencyAttribute), false);
                concurrencyDefaults = concurrencyAttributes.Length == 0
                                          ? new EntityServiceConcurrencyAttribute()
                                          : (EntityServiceConcurrencyAttribute)concurrencyAttributes[0];
            }

            var entityServiceInterface = typeof(IEntityService<>).MakeGenericType(entityType);
            var serviceInterface = typeof(IEntityThreadedObservableService<>).MakeGenericType(entityType);
            var serviceClass = typeof(EntityThreadedObservableService<>).MakeGenericType(entityType);

            int maximumConcurrency = concurrencyDefaults.MaximumConcurrency;
            int perObservableMaximumConcurrency = concurrencyDefaults.PerObservableMaximumConcurrency;

            container.RegisterType(serviceInterface,
                                   new ContainerControlledLifetimeManager(),
                                   new InjectionFactory(cont =>
                                   {

                                       var scheduler = maximumConcurrency >= 1
                                                            ? new QueuedTaskScheduler(TaskScheduler.Default, maximumConcurrency)
                                                            : TaskScheduler.Default;

                                       return cont.Resolve(serviceClass,
                                               new DependencyOverride(entityServiceInterface, cont.Resolve(previousEntityService)).OnType(serviceClass),
                                               new DependencyOverride<TaskScheduler>(scheduler).OnType(serviceClass),
                                               new DependencyOverride<int>(perObservableMaximumConcurrency).OnType(serviceClass));
                                   }));

            return serviceInterface;
        }

        private static void RegisterServices(IUnityContainer container)
        {
            //this allows someone else to register an implementation
            if (!container.IsRegistered<IEntityServiceExceptionService>())
            {
                container.RegisterType<IEntityServiceExceptionService, EntityServiceLoggingExceptionService>(new ContainerControlledLifetimeManager());
            }

            if (!container.IsRegistered<IDateTimeService>())
            {
                container.RegisterType<IDateTimeService, BclDateTimeService>(new ContainerControlledLifetimeManager());
            }

            if (!container.IsRegistered<ICacheService>())
            {
                container.RegisterType<ICacheService, CacheService>(new ContainerControlledLifetimeManager());
            }

            if (!container.IsRegistered(typeof(IPassThroughEntityServiceConverter<>)))
            {
                container.RegisterType(typeof(IPassThroughEntityServiceConverter<>), typeof(PassThroughEntityServiceConverter<>), new ContainerControlledLifetimeManager());
            }
        }
    }
}
