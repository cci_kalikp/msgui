﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using MorganStanley.Desktop.EntityServices.Common.Cache;
using MorganStanley.Desktop.EntityServices.Common.Exceptions;
using MorganStanley.Desktop.EntityServices.Entities;
using MorganStanley.Desktop.EntityServices.Keys;

namespace MorganStanley.Desktop.EntityServices
{
    public class EntityUpdateCachingService<TEntity> : EntityCachingService<TEntity>, IEntityUpdateCachingService<TEntity>
        where TEntity : class, ISubscribable, IFetchable, IEntity, new() 
    {
        private readonly IEntityService<TEntity> _entityService;
        private readonly IExceptionService<EntityUpdateCachingService<TEntity>> _exceptionService;
        private readonly ConcurrentDictionary<IQuery<TEntity>, CacheObject<IObservable<TEntity>>> _observables = new ConcurrentDictionary<IQuery<TEntity>, CacheObject<IObservable<TEntity>>>();
        private readonly ConcurrentDictionary<IQuery<TEntity>, CacheObject<IDisposable>> _updateStreams = new ConcurrentDictionary<IQuery<TEntity>, CacheObject<IDisposable>>();

        public EntityUpdateCachingService(
            IEntityService<TEntity> entityService, 
            ICacheService cacheService, 
            TimeSpan cacheTimeout, 
            IExceptionService<EntityUpdateCachingService<TEntity>> exceptionService)
                : base(entityService, cacheService, cacheTimeout)
        {
            _entityService = entityService;
            _exceptionService = exceptionService;
        }

        protected override void SubscribeToUpdates(IEnumerable<IQuery<TEntity>> queries)
        {
            // create the entry in update streams
            // get the observable and subscribe to it
            // add the subscription dispose to the _updateStreams
            // on update, update the cache
            foreach (var query in queries)
            {
                // We are subscribing here just to hold the connection open to receive updates on.  The moment that
                // we drop the last subscription the cached entities will stop getting updated.
                var currentQuery = query;
                _updateStreams
                    .GetOrAdd(query, k => new CacheObject<IDisposable>())
                    .SetValue(() => GetObservableWithoutQueryRefresh(new[] { currentQuery }).Subscribe(newEntities => { }));
            }
        }

        protected override void UnsubscribeFromUpdates(IQuery<TEntity> query)
        {
            // This removes the caches subscription to the observable, this doesnt mean that other references to the 
            // observable through GetObservable will be stopped.  So we just dispose of our subscription.  Its 
            // interesting to note though that once all subscriptions have been closed then the stream will be shut 
            // down.
            CacheObject<IDisposable> streamToRemove;
            if (_updateStreams.TryRemove(query, out streamToRemove) && streamToRemove.Value != null)
            {
                streamToRemove.Value.Dispose();
            }
        }

        public override IObservable<TObservable> GetObservable<TObservable>(IEnumerable<IQuery<TObservable>> queries)
        {
            return GetObservableWithoutQueryRefresh(queries)
                .Do(x => UpdateQueryKeyCache((IQuery<TEntity>)x.Key, new[] { x.Key }));
        }

        private IObservable<TObservable> GetObservableWithoutQueryRefresh<TObservable>(IEnumerable<IQuery<TObservable>> queries)
        {
            return queries.Cast<IQuery<TEntity>>()
                .Select(query =>
                {
                    var cacheObject = _observables.GetOrAdd(query, x => new CacheObject<IObservable<TEntity>>());
                    cacheObject.SetValue(() => CreateObserverable(query));
                    return cacheObject.Value;
                })
                .Merge()
                .Cast<TObservable>();
        }

        private IObservable<TEntity> CreateObserverable(IQuery<TEntity> query)
        {
            return Observable.Create<TEntity>(o =>
                {
                    var disposable = _entityService.GetObservable(new[] { query }).Subscribe(
                        entity =>
                            {
                                try
                                {
                                    UpdateEntityCache(entity);
                                    o.OnNext(entity);
                                }
                                catch (Exception exc)
                                {
                                    _exceptionService.LogException(string.Format("CreateObserverable: Unable to update entity cache for entity '{0}'", typeof(TEntity).FullName), exc);
                                }
                            },
                        o.OnError,
                        o.OnCompleted);

                    return disposable.Dispose;

                }).Publish().RefCount();
        }
    }
}