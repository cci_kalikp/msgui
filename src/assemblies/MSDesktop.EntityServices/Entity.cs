﻿using System;
using System.Xml.Schema;
using System.Xml.Serialization;
using MorganStanley.Desktop.EntityServices.Keys;

namespace MorganStanley.Desktop.EntityServices
{
    [Serializable]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public abstract class Entity<TEntity, TKey> : IEntity<TKey>, IEquatable<Entity<TEntity, TKey>> 
        where TEntity : class, IEntity<TKey> 
        where TKey : IKey<TEntity>, new()
    {
        [XmlElement(Form = XmlSchemaForm.Unqualified)]
        public virtual TKey Key { get; set; }

        IKey IEntity.Key { get { return Key; } }

        public override bool Equals(object obj)
        {
            return Equals(obj as Entity<TEntity, TKey>);
        }

        public bool Equals(Entity<TEntity, TKey> other)
        {
            if (other == null)
            {
                return false;
            }
            if (ReferenceEquals(this, other))
            {
                return true;
            }
            return Key.Equals(other.Key as IQuery<TEntity>);
        }

        public override int GetHashCode()
        {
            return Key.GetHashCode();
        }

        public static bool operator ==(Entity<TEntity, TKey> left, Entity<TEntity, TKey> right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Entity<TEntity, TKey> left, Entity<TEntity, TKey> right)
        {
            return !Equals(left, right);
        }
    }
}