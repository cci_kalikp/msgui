﻿using System;
using System.Collections.Generic;
using MorganStanley.Desktop.EntityServices.Entities;
using MorganStanley.Desktop.EntityServices.Keys;
using MorganStanley.Desktop.EntityServices.Queries;

namespace MorganStanley.Desktop.EntityServices
{
    public interface IEntityService<TEntity>
        where TEntity : class, IEntity
    {
        /// <summary>
        /// Fetches entities using the supplied queries
        /// </summary>
        /// <typeparam name="TFetchable">An entity that implements IFetchable</typeparam>
        /// <param name="queries">The queries used to fetch with</param>
        /// <returns>An enumerable of fetched entities</returns>
        IEnumerable<TFetchable> Get<TFetchable>(IEnumerable<IQuery<TFetchable>> queries) where TFetchable : class, TEntity, IFetchable;

        /// <summary>
        /// Gets an observable stream of entities based on the passed in queries
        /// </summary>
        /// <typeparam name="TObservable">An entity that implements ISubscribable</typeparam>
        /// <param name="queries">The queries used to observe with</param>
        /// <returns>The observable stream</returns>
        IObservable<ObservableResource<TObservable>> GetObservable<TObservable>(IEnumerable<IQuery<TObservable>> queries) where TObservable : class, TEntity, ISubscribable;

        /// <summary>
        /// Publishes entities
        /// </summary>
        /// <typeparam name="TPublishEntity">An entity that implements IPublishable</typeparam>
        /// <param name="publishEntities">The entities to publish</param>
        void Publish<TPublishEntity>(IEnumerable<TPublishEntity> publishEntities) where TPublishEntity : class, IPublishable, IEntity<TEntity, IKey<TEntity>>;

        /// <summary>
        /// Deletes the entity with the given key
        /// </summary>
        /// <typeparam name="TDeletable">An entity that implements IDeletable</typeparam>
        /// <param name="keys">The keys to delete</param>
        void Delete<TDeletable>(IEnumerable<IKey<TDeletable>> keys) where TDeletable : class, TEntity, IDeletable;
    }
}


