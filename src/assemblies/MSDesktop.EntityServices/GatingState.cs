﻿namespace MorganStanley.Desktop.EntityServices
{
    public enum GatingState
    {
        NotGated = 0,
        PartiallyGated = 1,
        FullyGated = 2
    }
}