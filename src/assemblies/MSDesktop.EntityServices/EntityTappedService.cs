﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Threading.Tasks;
using MorganStanley.Desktop.EntityServices.Entities;
using MorganStanley.Desktop.EntityServices.Queries;
using MorganStanley.Desktop.EntityServices.Services.Cloning;
using MorganStanley.Desktop.EntityServices.Services.Exceptions;

namespace MorganStanley.Desktop.EntityServices
{
    internal sealed class EntityTappedService<TEntity> : EntityPassthroughService<TEntity>, IEntityTappedService<TEntity>
        where TEntity : class, IEntity
    {
        private readonly IEntityCloning _entityCloning;
        private readonly ISubject<ObservableResource<TEntity>> _subject = new Subject<ObservableResource<TEntity>>();

        public EntityTappedService(IEntityService<TEntity> entityService, IEntityServiceExceptionService exceptionService, IEntityCloning entityCloning) : base(entityService, exceptionService)
        {
            _entityCloning = entityCloning;
        }

        public override IEnumerable<TFetchable> Get<TFetchable>(IEnumerable<IQuery<TFetchable>> queries)
        {
            IList<IQuery<TFetchable>> tappableQueries;
            IList<IQuery<TFetchable>> nonTappableQueries;
            SplitQueries(queries, out tappableQueries, out nonTappableQueries);

            var entities = Enumerable.Empty<TFetchable>();
            if (tappableQueries.Count > 0)
            {
                entities = entities.Union(GetTappableEntities(tappableQueries));
            }

            if (nonTappableQueries.Count > 0)
            {
                entities = entities.Union(GetNonTappableEntities(nonTappableQueries));
            }

            return entities;
        }

        public override IObservable<ObservableResource<TObservable>> GetObservable<TObservable>(IEnumerable<IQuery<TObservable>> queries)
        {
            IList<IQuery<TObservable>> tappableQueries;
            IList<IQuery<TObservable>> nonTappableQueries;
            SplitQueries(queries, out tappableQueries, out nonTappableQueries);

            var observable = Observable.Empty<ObservableResource<TObservable>>();
            if (tappableQueries.Count > 0)
            {
                observable = observable.Merge(base.GetObservable(tappableQueries)
                                       .Do(resource =>
                                       {
                                           if (resource.IsFaulted)
                                           {
                                               return;
                                           }
                                           Task.Factory.StartNew(() => _subject.OnNext(new ObservableResource<TEntity>(resource.Entities)));
                                       }));
            }

            if (nonTappableQueries.Count > 0)
            {
                observable = observable.Merge(base.GetObservable(nonTappableQueries));
            }

            return observable;
        }

        public IDisposable Subscribe(IObserver<ObservableResource<TEntity>> observer)
        {
            return _subject.Select(resource =>
                                       {
                                           try
                                           {
                                               return new ObservableResource<TEntity>(resource.Entities.Select(entity => _entityCloning.Clone(entity)).ToList());
                                           }
                                           catch (Exception ex)
                                           {
                                               return new ObservableResource<TEntity>(ex);
                                           }
                                       })
                           .Subscribe(observer);
        }

        private IEnumerable<TFetchable> GetTappableEntities<TFetchable>(IEnumerable<IQuery<TFetchable>> queries)
            where TFetchable : class, TEntity, IFetchable
        {
            var entities = base.Get(queries);
            Task.Factory.StartNew(() =>
                                   {
                                       try
                                       {
                                            _subject.OnNext(new ObservableResource<TEntity>(entities));
                                       }
                                       catch (Exception ex)
                                       {
                                           _subject.OnNext(new ObservableResource<TEntity>(new EntityServiceException(string.Format("Error while sending the tapped entities for entity '{0}'", typeof(TEntity).FullName), ex)));
                                       }
                                   });

            return entities;
        }

        private IEnumerable<TFetchable> GetNonTappableEntities<TFetchable>(IEnumerable<IQuery<TFetchable>> queries)
            where TFetchable : class, TEntity, IFetchable
        {
            return base.Get(queries);
        }

        private static void SplitQueries<T>(IEnumerable<IQuery<T>> queries, out IList<IQuery<T>> tappableQueries, out IList<IQuery<T>> nonTappableQueries)
            where T : class, TEntity
        {
            tappableQueries = new List<IQuery<T>>();
            nonTappableQueries = new List<IQuery<T>>();
            foreach (var query in queries)
            {
                var tappingOptions = query as IWithOptionalTapping;
                if (tappingOptions != null && tappingOptions.TappingOptions == TappingOptions.DontTap)
                {
                    nonTappableQueries.Add(query);
                }
                else
                {
                    tappableQueries.Add(query);
                }
            }            
        }
    }
}
