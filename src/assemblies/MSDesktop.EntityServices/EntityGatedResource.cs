﻿using System;
using MorganStanley.Desktop.EntityServices.Common;
using MorganStanley.Desktop.EntityServices.Keys;

namespace MorganStanley.Desktop.EntityServices
{
    public class EntityGatedResource<TEntity> : IEntityGatedResource<TEntity>, IEquatable<EntityGatedResource<TEntity>>
        where TEntity : IEntity, ITimestampedEntity, new()
    {
        private readonly IEntityGatingService<TEntity> _service;
        private readonly IKey<TEntity> _key;

        public EntityGatedResource(IEntityGatingService<TEntity> service, IKey<TEntity> gateOn)
        {
            _key = gateOn;
            _service = service;
            EntityType = typeof(TEntity);
            ResourceKey = new EntityResourceKey<TEntity>(_key, EntityType);
            var parts = _key.ToString().Split(new[] { '/' });
            Symbol = parts[0];
            SubId = parts.Length > 1 ? parts[1] : string.Empty;
        }

        public void Accept()
        {
            _service.Accept(_key);
        }

        public void Gate()
        {
            _service.SetGating(_key, true);
        }

        public void Ungate()
        {
            _service.SetGating(_key, false);
        }

        public bool IsGated
        {
            get
            {
                return _service.IsGated(_key);
            }
        }

        public IEntityResourceKey EntityResourceKey
        {
            get { return ResourceKey; }
        }

        public TEntity AcceptedInstance { get; set; }
        public TEntity GatedInstance { get; set; }

        public string KeyText { get { return _key.ToString(); } }

        public DateTime? AcceptedTimestamp { get; set; }
        public DateTime? CurrentTimestamp { get; set; }

        public Type EntityType { get; private set; }
        public string Symbol { get; private set; }
        public string SubId { get; private set; }
        public IEntityResourceKey<TEntity> ResourceKey { get; private set; }

        public int CompareTo(object obj)
        {
            return CompareTo(obj as IEntityGatedResource<TEntity>);
        }

        public int CompareTo(IEntityGatedResource<TEntity> other)
        {
            if (other == null)
            {
                return 1;
            }
            return KeyText.CompareTo(other.KeyText);
        }

        public bool Equals(EntityGatedResource<TEntity> other)
        {
            return Equals(other as IEntityGatedResource<TEntity>);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as IEntityGatedResource<TEntity>);
        }

        public bool Equals(IEntityGatedResource<TEntity> other)
        {
            if (other == null || other.EntityType != EntityType)
            {
                return false;
            }
            return ResourceKey.Equals(other.ResourceKey);
        }

        public override int GetHashCode()
        {
            return ResourceKey.GetHashCode();
        }
    }
}
