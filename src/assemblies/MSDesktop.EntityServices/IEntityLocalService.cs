﻿using MorganStanley.Desktop.EntityServices.Entities;

namespace MorganStanley.Desktop.EntityServices
{
    internal interface IEntityLocalService<TEntity> : IEntityService<TEntity>
        where TEntity : class, IEntity
    {
    }
}