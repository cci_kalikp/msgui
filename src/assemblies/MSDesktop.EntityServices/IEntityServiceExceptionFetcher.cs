﻿using MorganStanley.Desktop.EntityServices.Services.Exceptions;

namespace MorganStanley.Desktop.EntityServices
{
    internal interface IEntityServiceExceptionFetcher
    {
        IEntityServiceExceptionService GetExceptionService();
    }
}
