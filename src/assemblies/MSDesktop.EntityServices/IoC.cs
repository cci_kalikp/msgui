﻿using System;
using System.Threading;
using log4net;
using Microsoft.Practices.Unity;
using MorganStanley.Desktop.EntityServices.Common.BCLWrappers;
using MorganStanley.Desktop.EntityServices.Common.Cache;
using MorganStanley.Desktop.EntityServices.Common.Exceptions;
using MorganStanley.Desktop.EntityServices.Keys;
using MorganStanley.Desktop.EntityServices.Data;

namespace MorganStanley.Desktop.EntityServices
{
    public static class IoC
    {
        private static readonly ILog Log = LogManager.GetLogger("IoC");

        public static IUnityContainer RegisterService<TKey, T>(this IUnityContainer container)
            where T : class, IEntity, new()
            where TKey : IKey<T>, IEquatable<TKey>
        {
            return container.RegisterType<IEntityService<T>, EntityService<T>>(
                new ContainerControlledLifetimeManager());
        }

        public static IUnityContainer RegisterStaticCachingService(this IUnityContainer container, Type type)
        {
            container.RegisterType(typeof(EntityStaticCachingService<>).MakeGenericType(type),
                new ContainerControlledLifetimeManager(),
                new InjectionConstructor(new ResolvedParameter(typeof(EntityService<>).MakeGenericType(type))));
            RegisterCloningService(container, type, typeof(EntityStaticCachingService<>).MakeGenericType(type));

            container.RegisterType(typeof(IEntityService<>).MakeGenericType(type), typeof(EntityCloningService<>).MakeGenericType(type));
            container.RegisterType(typeof(IEntityStaticCachingService<>).MakeGenericType(type), typeof(EntityStaticCachingService<>).MakeGenericType(type));

            return container;
        }

        public static IUnityContainer RegisterGatingWithLocalService(this IUnityContainer container, Type type)
        {
            RegisterGatingService(container, type, typeof(EntityService<>).MakeGenericType(type));
            RegisterLocalService(container, type, typeof(EntityGatingService<>).MakeGenericType(type));
            RegisterCloningService(container, type, typeof(EntityLocalService<>).MakeGenericType(type));

            return container.RegisterType(
                typeof(IEntityService<>).MakeGenericType(type),
                typeof(EntityCloningService<>).MakeGenericType(type));
        }

        public static IUnityContainer RegisterGatingAndCachingService(this IUnityContainer container, Type type, TimeSpan timeout)
        {
            RegisterCachingService(container, type, timeout, typeof(EntityService<>).MakeGenericType(type));
            RegisterGatingService(container, type, typeof(EntityCachingService<>).MakeGenericType(type));
            RegisterCloningService(container, type, typeof(EntityGatingService<>).MakeGenericType(type));

            return container.RegisterType(
                typeof(IEntityService<>).MakeGenericType(type),
                typeof(EntityCloningService<>).MakeGenericType(type));
        }

        public static IUnityContainer RegisterGatingAndCachingWithUpdatesService(this IUnityContainer container, Type type, TimeSpan timeout)
        {
            RegisterUpdateCachingService(container, type, timeout, typeof(EntityService<>).MakeGenericType(type));
            RegisterGatingService(container, type, typeof(EntityUpdateCachingService<>).MakeGenericType(type));
            RegisterReplayService(container, type, typeof(EntityGatingService<>).MakeGenericType(type));
            RegisterCloningService(container, type, typeof(EntityReplayService<>).MakeGenericType(type));

            return container.RegisterType(
                typeof(IEntityService<>).MakeGenericType(type),
                typeof(EntityCloningService<>).MakeGenericType(type));
        }

        public static IUnityContainer RegisterTappedGatingAndCachingWithUpdatesService(this IUnityContainer container, Type type, TimeSpan timeout)
        {
            RegisterTappedService(container, type, typeof(EntityService<>).MakeGenericType(type));
            RegisterUpdateCachingService(container, type, timeout, typeof(EntityTappedService<>).MakeGenericType(type));
            RegisterGatingService(container, type, typeof(EntityUpdateCachingService<>).MakeGenericType(type));
            RegisterReplayService(container, type, typeof(EntityGatingService<>).MakeGenericType(type));
            RegisterCloningService(container, type, typeof(EntityReplayService<>).MakeGenericType(type));

            return container.RegisterType(
                typeof(IEntityService<>).MakeGenericType(type),
                typeof(EntityCloningService<>).MakeGenericType(type));
        }

        public static IUnityContainer RegisterLocallyUpdatableWithGatingAndCachingWithUpdatesService(this IUnityContainer container, Type type, TimeSpan timeout)
        {
            RegisterUpdateCachingService(container, type, timeout, typeof(EntityService<>).MakeGenericType(type));
            RegisterGatingService(container, type, typeof(EntityUpdateCachingService<>).MakeGenericType(type));
            RegisterReplayService(container, type, typeof(EntityGatingService<>).MakeGenericType(type));
            RegisterLocalService(container, type, typeof(EntityReplayService<>).MakeGenericType(type));
            RegisterCloningService(container, type, typeof(EntityLocalService<>).MakeGenericType(type));

            return container.RegisterType(
                typeof(IEntityService<>).MakeGenericType(type),
                typeof(EntityCloningService<>).MakeGenericType(type));
        }

        public static IUnityContainer RegisterTappedLocallyUpdatableWithGatingAndCachingWithUpdatesService(this IUnityContainer container, Type type, TimeSpan timeout)
        {
            RegisterTappedService(container, type, typeof(EntityService<>).MakeGenericType(type));
            RegisterUpdateCachingService(container, type, timeout, typeof(EntityTappedService<>).MakeGenericType(type));
            RegisterGatingService(container, type, typeof(EntityUpdateCachingService<>).MakeGenericType(type));
            RegisterReplayService(container, type, typeof(EntityGatingService<>).MakeGenericType(type));
            RegisterLocalService(container, type, typeof(EntityReplayService<>).MakeGenericType(type));
            RegisterCloningService(container, type, typeof(EntityLocalService<>).MakeGenericType(type));

            return container.RegisterType(
                typeof(IEntityService<>).MakeGenericType(type),
                typeof(EntityCloningService<>).MakeGenericType(type));
        }


        public static IUnityContainer RegisterBasicEntityService(this IUnityContainer container, Type type)
        {
            RegisterCloningService(container, type, typeof(EntityService<>).MakeGenericType(type));
            return container.RegisterType(typeof(IEntityService<>).MakeGenericType(type), typeof(EntityCloningService<>).MakeGenericType(type));
        }

        public static IUnityContainer RegisterCachingService(this IUnityContainer container, Type type, TimeSpan timeSpan)
        {
            RegisterCachingService(container, type, timeSpan, typeof(EntityService<>).MakeGenericType(type));
            RegisterCloningService(container, type, typeof(EntityCachingService<>).MakeGenericType(type));

            container.RegisterType(typeof(IEntityService<>).MakeGenericType(type), typeof(EntityCloningService<>).MakeGenericType(type));
            container.RegisterType(typeof(IEntityCachingService<>).MakeGenericType(type), typeof(EntityCachingService<>).MakeGenericType(type));

            return container;
        }

        private static void RegisterCachingService(IUnityContainer container, Type entityType, TimeSpan timeSpan, Type underlyingEntityServiceType)
        {
            container.RegisterType(typeof(EntityCachingService<>).MakeGenericType(entityType),
                                   new ContainerControlledLifetimeManager(),
                                   new InjectionConstructor(new ResolvedParameter(underlyingEntityServiceType),
                                                            new ResolvedParameter<ICacheService>(),
                                                            timeSpan));
        }

        private static void RegisterCloningService(IUnityContainer container, Type entityType, Type underlyingEntityServiceType)
        {
            var exceptionServiceInterfaceType = typeof(IExceptionService<>).MakeGenericType(typeof(EntityCloningService<>).MakeGenericType(entityType));
            container.RegisterType(
                exceptionServiceInterfaceType,
                typeof(ExceptionService<>).MakeGenericType(typeof(EntityCloningService<>).MakeGenericType(entityType)),
                new ContainerControlledLifetimeManager());
            container.RegisterType(typeof(EntityCloningService<>).MakeGenericType(entityType),
                new ContainerControlledLifetimeManager(),
                new InjectionConstructor(
                    new ResolvedParameter(underlyingEntityServiceType),
                    new ResolvedParameter(exceptionServiceInterfaceType)));
        }

        public static IUnityContainer RegisterTappedCachingWithUpdatesService(this IUnityContainer container, Type type, TimeSpan timeSpan)
        {
            RegisterTappedService(container, type, typeof(EntityService<>).MakeGenericType(type));
            RegisterUpdateCachingService(container, type, timeSpan, typeof(EntityTappedService<>).MakeGenericType(type));
            RegisterCloningService(container, type, typeof(EntityUpdateCachingService<>).MakeGenericType(type));

            return container.RegisterType(
                typeof(IEntityService<>).MakeGenericType(type),
                typeof(EntityCloningService<>).MakeGenericType(type));
        }

        public static IUnityContainer RegisterTappedCachingService(this IUnityContainer container, Type type, TimeSpan timeSpan)
        {
            RegisterTappedService(container, type, typeof(EntityService<>).MakeGenericType(type));
            RegisterCachingService(container, type, timeSpan, typeof(EntityTappedService<>).MakeGenericType(type));
            RegisterCloningService(container, type, typeof(EntityCachingService<>).MakeGenericType(type));

            return container.RegisterType(
                typeof(IEntityService<>).MakeGenericType(type),
                typeof(EntityCloningService<>).MakeGenericType(type));
        }

        public static IUnityContainer RegisterTappedService(this IUnityContainer container, Type type)
        {
            RegisterTappedService(container, type, typeof(EntityService<>).MakeGenericType(type));
            RegisterCloningService(container, type, typeof(EntityTappedService<>).MakeGenericType(type));

            return container.RegisterType(
                typeof(IEntityService<>).MakeGenericType(type),
                typeof(EntityCloningService<>).MakeGenericType(type));
        }

        private static void RegisterTappedService(IUnityContainer container, Type entityType, Type underlyingEntityType)
        {
            container.RegisterType(typeof(EntityTappedService<>).MakeGenericType(entityType),
                                   new ContainerControlledLifetimeManager(),
                                   new InjectionConstructor(new ResolvedParameter(underlyingEntityType)));
            container.RegisterType(
                typeof(IEntityTappedService<>).MakeGenericType(entityType),
                typeof(EntityTappedService<>).MakeGenericType(entityType));
        }

        public static IUnityContainer RegisterUpdateCachingService(this IUnityContainer container, Type type, TimeSpan timeSpan)
        {
            RegisterUpdateCachingService(container, type, timeSpan, typeof(EntityService<>).MakeGenericType(type));
            RegisterCloningService(container, type, typeof(EntityUpdateCachingService<>).MakeGenericType(type));

            container.RegisterType(
                typeof(IEntityService<>).MakeGenericType(type),
                typeof(EntityCloningService<>).MakeGenericType(type));
            container.RegisterType(
                typeof(IEntityCachingService<>).MakeGenericType(type),
                typeof(EntityUpdateCachingService<>).MakeGenericType(type));

            return container;
        }


        public static IUnityContainer RegisterUpdateCachingServiceWithoutCloning(this IUnityContainer container, Type type, TimeSpan timeSpan)
        {
            RegisterUpdateCachingService(container, type, timeSpan, typeof(EntityService<>).MakeGenericType(type));

            container.RegisterType(
                typeof(IEntityService<>).MakeGenericType(type),
                typeof(EntityUpdateCachingService<>).MakeGenericType(type));
            container.RegisterType(
                typeof(IEntityCachingService<>).MakeGenericType(type),
                typeof(EntityUpdateCachingService<>).MakeGenericType(type));

            return container;
        }



        private static void RegisterUpdateCachingService(IUnityContainer container, Type entityType, TimeSpan timeSpan, Type underlyingEntityServiceType)
        {
            var exceptionServiceInterfaceType = typeof(IExceptionService<>).MakeGenericType(typeof(EntityUpdateCachingService<>).MakeGenericType(entityType));
            container.RegisterType(
                exceptionServiceInterfaceType,
                typeof(ExceptionService<>).MakeGenericType(typeof(EntityUpdateCachingService<>).MakeGenericType(entityType)),
                new ContainerControlledLifetimeManager());
            container.RegisterType(typeof(EntityUpdateCachingService<>).MakeGenericType(entityType),
                                   new ContainerControlledLifetimeManager(),
                                   new InjectionConstructor(new ResolvedParameter(underlyingEntityServiceType),
                                                            new ResolvedParameter<ICacheService>(),
                                                            timeSpan,
                                                            new ResolvedParameter(exceptionServiceInterfaceType)));
        }

        public static IUnityContainer RegisterGatingService(this IUnityContainer container, Type type, TimeSpan timeSpan)
        {
            RegisterGatingService(container, type, typeof(EntityService<>).MakeGenericType(type));
            RegisterCloningService(container, type, typeof(EntityGatingService<>).MakeGenericType(type));

            container.RegisterType(
                typeof(IEntityService<>).MakeGenericType(type),
                typeof(EntityCloningService<>).MakeGenericType(type));

            return container;
        }

        private static void RegisterGatingService(IUnityContainer container, Type entityType, Type underlyingEntityServiceType)
        {
            var exceptionServiceInterfaceType = typeof(IExceptionService<>).MakeGenericType(typeof(EntityGatingService<>).MakeGenericType(entityType));
            container.RegisterType(
                exceptionServiceInterfaceType,
                typeof(ExceptionService<>).MakeGenericType(typeof(EntityGatingService<>).MakeGenericType(entityType)),
                new ContainerControlledLifetimeManager());
            container.RegisterType(
                typeof(EntityGatingService<>).MakeGenericType(entityType),
                new ContainerControlledLifetimeManager(),
                new InjectionConstructor(
                    new ResolvedParameter(underlyingEntityServiceType),
                    new ResolvedParameter<IDateTimeService>(),
                    new ResolvedParameter(exceptionServiceInterfaceType)));
            container.RegisterType(
                typeof(IEntityGatingService<>).MakeGenericType(entityType),
                typeof(EntityGatingService<>).MakeGenericType(entityType));

            // This is done to allow us to pick up the full list of gated services in gateway viewer...
            container.RegisterType(
                typeof(IEntityGatingService),
                typeof(EntityGatingService<>).MakeGenericType(entityType),
                entityType.FullName);
        }

        private static void RegisterReplayService(IUnityContainer container, Type entityType, Type underlyingEntityServiceType)
        {
            container.RegisterType(
                typeof(EntityReplayService<>).MakeGenericType(entityType),
                new ContainerControlledLifetimeManager(),
                new InjectionConstructor(
                    new ResolvedParameter(underlyingEntityServiceType)));

            // This is done to allow us to pick up the full list of replayable services in gateway viewer...
            container.RegisterType(
                typeof(IEntityReplayService<>).MakeGenericType(entityType),
                typeof(EntityReplayService<>).MakeGenericType(entityType));

            container.RegisterType(
                typeof(IEntityReplayService),
                typeof(EntityReplayService<>).MakeGenericType(entityType),
                entityType.FullName);
        }

        private static void RegisterLocalService(IUnityContainer container, Type entityType, Type underlyingEntityServiceType)
        {
            var exceptionServiceInterfaceType = typeof(IExceptionService<>).MakeGenericType(typeof(EntityLocalService<>).MakeGenericType(entityType));
            container.RegisterType(
                exceptionServiceInterfaceType,
                typeof(ExceptionService<>).MakeGenericType(typeof(EntityLocalService<>).MakeGenericType(entityType)),
                new ContainerControlledLifetimeManager());

            container.RegisterType(
                typeof(EntityLocalService<>).MakeGenericType(entityType),
                new ContainerControlledLifetimeManager(),
                new InjectionConstructor(
                    new ResolvedParameter(underlyingEntityServiceType),
                    new ResolvedParameter(exceptionServiceInterfaceType)));
        }


        public static IUnityContainer RegisterServiceLayer<TEntity>(
            this IUnityContainer container,
            ServiceMode mode, TimeSpan cacheTimeout = new TimeSpan())
        {
            return container.RegisterServiceLayer(typeof (TEntity), mode, cacheTimeout);
        }

        public static IUnityContainer RegisterServiceLayer(
            this IUnityContainer container,
            Type entityType,
            ServiceMode mode, TimeSpan cacheTimeout = new TimeSpan())
        {
           /* var exceptionServiceInterfaceType = typeof(IExceptionService<>).MakeGenericType(typeof(EntityService<>).MakeGenericType(entityType));
            container.RegisterType(
                exceptionServiceInterfaceType,
                typeof(ExceptionService<>).MakeGenericType(typeof(EntityService<>).MakeGenericType(entityType)),
                new ContainerControlledLifetimeManager());*/

            //Basic type
            var underlyingEntityServiceType = typeof(EntityService<>).MakeGenericType(entityType);

           /* container.RegisterType(underlyingEntityServiceType,
                new ContainerControlledLifetimeManager(),
                new InjectionConstructor(new ResolvedParameter(typeof(IDataConnector)),
                    new ResolvedParameter(typeof(IDataConverter<>).MakeGenericType(entityType)),
                    new ResolvedParameter(exceptionServiceInterfaceType),
                    false)
                );*/
            
            foreach (ServiceMode code in Enum.GetValues(typeof(ServiceMode)))
            {
                var layer = mode & code;

                switch (layer)
                {
                    case ServiceMode.Tapped:
                        var serviceType = typeof (EntityTappedService<>).MakeGenericType(entityType);
                        container.RegisterType(serviceType,
                                   new ContainerControlledLifetimeManager(),
                                   new InjectionConstructor(new ResolvedParameter(underlyingEntityServiceType)));
                        underlyingEntityServiceType = serviceType;
                        container.RegisterType(typeof (IEntityTappedService<>).MakeGenericType(entityType), serviceType);
                        break;
                    case ServiceMode.Caching:
                        serviceType = typeof(EntityCachingService<>).MakeGenericType(entityType);
                        container.RegisterType<ICacheService, CacheService>(new ContainerControlledLifetimeManager());
                        container.RegisterType(serviceType,
                                  new ContainerControlledLifetimeManager(),
                                  new InjectionConstructor(new ResolvedParameter(underlyingEntityServiceType),
                                                           new ResolvedParameter<ICacheService>(),
                                                           cacheTimeout));
                        container.RegisterType(typeof (IEntityCachingService<>).MakeGenericType(entityType), serviceType);
                        underlyingEntityServiceType = serviceType;
                        break;

                    case ServiceMode.UpdateCaching:
                        serviceType = typeof (EntityUpdateCachingService<>).MakeGenericType(entityType);
                        var updatingExceptionServiceInterfaceType = 
                            typeof(IExceptionService<>).MakeGenericType(typeof(EntityUpdateCachingService<>).MakeGenericType(entityType));

                        container.RegisterType<ICacheService, CacheService>(new ContainerControlledLifetimeManager());
                        container.RegisterType(
                            updatingExceptionServiceInterfaceType,
                            typeof (ExceptionService<>).MakeGenericType(
                                typeof (EntityUpdateCachingService<>).MakeGenericType(entityType)),
                            new ContainerControlledLifetimeManager());

                        container.RegisterType(serviceType,
                                               new ContainerControlledLifetimeManager(),
                                               new InjectionConstructor(
                                                   new ResolvedParameter(underlyingEntityServiceType),
                                                   new ResolvedParameter<ICacheService>(),
                                                   cacheTimeout,
                                                   new ResolvedParameter(updatingExceptionServiceInterfaceType)));

                        container.RegisterType(typeof (IEntityUpdateCachingService<>).MakeGenericType(entityType),
                                               serviceType);
                        underlyingEntityServiceType = serviceType;
                        break;

                    case ServiceMode.StaticCaching:
                        serviceType = typeof (EntityStaticCachingService<>).MakeGenericType(entityType);
                        container.RegisterType(serviceType,
                            new ContainerControlledLifetimeManager(),
                            new InjectionConstructor(new ResolvedParameter(underlyingEntityServiceType)));
                        container.RegisterType(typeof (IEntityStaticCachingService<>).MakeGenericType(entityType),
                                               serviceType);
                        underlyingEntityServiceType = serviceType;
                        break;

                    case ServiceMode.Gating:
                        serviceType = typeof (EntityGatingService<>).MakeGenericType(entityType);
                        
                        container.RegisterType<IDateTimeService, DateTimeService>(new ContainerControlledLifetimeManager());

                        var gatingExceptionServiceInterfaceType =
                            typeof (IExceptionService<>).MakeGenericType(
                                typeof (EntityGatingService<>).MakeGenericType(entityType));

                        container.RegisterType(gatingExceptionServiceInterfaceType,
                            typeof(ExceptionService<>).MakeGenericType(typeof(EntityGatingService<>).MakeGenericType(entityType)),
                            new ContainerControlledLifetimeManager());

                        container.RegisterType(
                            serviceType,
                            new ContainerControlledLifetimeManager(),
                            new InjectionConstructor(new ResolvedParameter(underlyingEntityServiceType),
                                new ResolvedParameter<IDateTimeService>(),
                                new ResolvedParameter(gatingExceptionServiceInterfaceType)));

                        container.RegisterType(typeof (IEntityGatingService<>).MakeGenericType(entityType), serviceType);
                        underlyingEntityServiceType = serviceType;
                        break;

                    case ServiceMode.Replay:
                        serviceType = typeof (EntityReplayService<>).MakeGenericType(entityType);
                        container.RegisterType(
                            serviceType,
                            new ContainerControlledLifetimeManager(),
                            new InjectionConstructor(
                                new ResolvedParameter(underlyingEntityServiceType)));

                        container.RegisterType(typeof (IEntityReplayService<>).MakeGenericType(entityType), serviceType);
                        underlyingEntityServiceType = serviceType;

                        break;

                    case ServiceMode.Local:
                        serviceType = typeof (EntityLocalService<>).MakeGenericType(entityType);

                        var localExceptionServiceInterfaceType =
                            typeof (IExceptionService<>).MakeGenericType(
                                typeof (EntityLocalService<>).MakeGenericType(entityType));

                        container.RegisterType(
                            localExceptionServiceInterfaceType,
                            typeof (ExceptionService<>).MakeGenericType(
                                typeof (EntityLocalService<>).MakeGenericType(entityType)),
                            new ContainerControlledLifetimeManager());

                        container.RegisterType(
                            serviceType,
                            new ContainerControlledLifetimeManager(),
                            new InjectionConstructor(
                                new ResolvedParameter(underlyingEntityServiceType),
                                new ResolvedParameter(localExceptionServiceInterfaceType)));

                        container.RegisterType(typeof (IEntityLocalService<>).MakeGenericType(entityType), serviceType);
                        underlyingEntityServiceType = serviceType;

                        break;

                    case ServiceMode.Cloning:
                        serviceType = typeof (EntityCloningService<>).MakeGenericType(entityType);
                        var cloningExceptionServiceInterfaceType = 
                            typeof(IExceptionService<>).MakeGenericType(typeof(EntityCloningService<>).MakeGenericType(entityType));
                        container.RegisterType(
                            cloningExceptionServiceInterfaceType,
                            typeof(ExceptionService<>).MakeGenericType(typeof(EntityCloningService<>).MakeGenericType(entityType)),
                            new ContainerControlledLifetimeManager());
                        
                        container.RegisterType(serviceType,
                            new ContainerControlledLifetimeManager(),
                            new InjectionConstructor(
                                new ResolvedParameter(underlyingEntityServiceType),
                                new ResolvedParameter(cloningExceptionServiceInterfaceType)));

                        
                        underlyingEntityServiceType = serviceType;
                        break;

                    case ServiceMode.Default:
                        //this layer is empty
                        break;
                    default:
                        throw new NotImplementedException("The service mode " + layer + " is not supported");

                }
                Console.WriteLine("Layer " + code + ": " + (code == layer).ToString());
            }

            container.RegisterType(typeof (IEntityService<>).MakeGenericType(entityType), underlyingEntityServiceType);

            return container;
        }
        



        public static IUnityContainer RegisterServiceWithServiceAttribute(
            this IUnityContainer container,
            Type type,
            ServiceRegistrationAttribute serviceRegistrationAttribute)
        {
            if (Log.IsDebugEnabled)
            {
                Log.DebugFormat("Registering {0} to Service {1}", type, serviceRegistrationAttribute.Mode);
            }

            return container.RegisterServiceLayer(type, serviceRegistrationAttribute.Mode,
                                                  serviceRegistrationAttribute.CacheTimeout);

            switch (serviceRegistrationAttribute.Mode)
            {
                case ServiceMode.Caching:
                    container.RegisterCachingService(type, serviceRegistrationAttribute.CacheTimeout);
                    break;
                case ServiceMode.Caching | ServiceMode.UpdateCaching:
                    container.RegisterUpdateCachingService(type, serviceRegistrationAttribute.CacheTimeout);
                    break;
                case ServiceMode.Gating:
                    container.RegisterGatingService(type, serviceRegistrationAttribute.CacheTimeout);
                    break;
                case ServiceMode.Gating | ServiceMode.Local:
                    container.RegisterGatingWithLocalService(type);
                    break;
                case ServiceMode.StaticCaching:
                    container.RegisterStaticCachingService(type);
                    break;
                case ServiceMode.Caching | ServiceMode.Gating:
                    container.RegisterGatingAndCachingService(type, serviceRegistrationAttribute.CacheTimeout);
                    break;
                case ServiceMode.Gating | ServiceMode.UpdateCaching:
                    container.RegisterGatingAndCachingWithUpdatesService(type, serviceRegistrationAttribute.CacheTimeout);
                    break;
                case ServiceMode.UpdateCaching | ServiceMode.Gating | ServiceMode.Replay | ServiceMode.Local | ServiceMode.Cloning:
                    container.RegisterLocallyUpdatableWithGatingAndCachingWithUpdatesService(type, serviceRegistrationAttribute.CacheTimeout);
                    break;
                case ServiceMode.Tapped |ServiceMode.UpdateCaching  :
                    container.RegisterTappedCachingWithUpdatesService(type, serviceRegistrationAttribute.CacheTimeout);
                    break;
                case ServiceMode.Tapped | ServiceMode.Caching:
                    container.RegisterTappedCachingService(type, serviceRegistrationAttribute.CacheTimeout);
                    break;
                case ServiceMode.Tapped:
                    container.RegisterTappedService(type);
                    break;
                default:
                    //container.RegisterDataServiceEntityService(type, serviceName);
                    container.RegisterBasicEntityService(type);
                    break;
            }
            return container;
        }
    }
}
