﻿using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using MorganStanley.Desktop.EntityServices.Entities;
using MorganStanley.Desktop.EntityServices.Queries;
using MorganStanley.Desktop.EntityServices.Services.Exceptions;
using MorganStanley.Desktop.EntityServices.Services.Threading;

namespace MorganStanley.Desktop.EntityServices
{
    internal sealed class EntityThreadedObservableService<TEntity> : EntityPassthroughService<TEntity>
        where TEntity : class, IEntity
    {
        private readonly TaskScheduler _scheduler;
        private readonly int _maximumConcurrency;


        public EntityThreadedObservableService(IEntityService<TEntity> entityService, IEntityServiceExceptionService exceptionService, TaskScheduler scheduler, int maximumConcurrency)
            : base(entityService, exceptionService)
        {
            _scheduler = scheduler;
            _maximumConcurrency = maximumConcurrency;
        }

        public override IObservable<ObservableResource<TObservable>> GetObservable<TObservable>(IEnumerable<IQuery<TObservable>> queries)
        {
            return Async(base.GetObservable(queries), _scheduler, _maximumConcurrency);
        }

        private static IObservable<T> Async<T>(IObservable<T> observable, TaskScheduler scheduler, int maximumConcurrency)
        {
            if (scheduler == null)
            {
                scheduler = TaskScheduler.Current;
            }

            if (maximumConcurrency >= 1)
            {
                scheduler = new QueuedTaskScheduler(scheduler, maximumConcurrency);
            }


            return Observable.Create<T>(observer =>
            {
                var disposable = observable.Subscribe(
                    value =>
                    {
                        try
                        {
                            Task.Factory.StartNew(() => observer.OnNext(value), CancellationToken.None,
                                                  TaskCreationOptions.None, scheduler);
                        }
                        catch (Exception ex)
                        {
                            Task.Factory.StartNew(() => observer.OnError(ex), CancellationToken.None,
                                                  TaskCreationOptions.None, scheduler);
                        }
                    },
                    exception => Task.Factory.StartNew(() =>
                    {
                        try
                        {
                            observer.OnError(exception);
                        }
                        catch
                        {
                            //no point raising an error on an error
                        }
                    }, CancellationToken.None, TaskCreationOptions.None, scheduler),
                    () =>
                    {
                        try
                        {
                            Task.Factory.StartNew(observer.OnCompleted, CancellationToken.None,
                                                  TaskCreationOptions.None, scheduler);
                        }
                        catch (Exception ex)
                        {
                            Task.Factory.StartNew(() => observer.OnError(ex), CancellationToken.None,
                                                  TaskCreationOptions.None, scheduler);
                        }
                    });
                return disposable.Dispose;
            });
        }
    }
}
