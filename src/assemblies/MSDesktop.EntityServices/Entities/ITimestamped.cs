﻿using System;

namespace MorganStanley.Desktop.EntityServices.Entities
{
    public interface ITimestamped
    {
        DateTime Timestamp { get; }
    }
}
