﻿using MorganStanley.Desktop.EntityServices.Keys;

namespace MorganStanley.Desktop.EntityServices.Entities
{
    public interface IEntity<TEntity, out TKey> : IEntity
        where TEntity : class
        where TKey : IKey<TEntity>
    {
        /// <summary>
        /// This property should return key which unique identifies an entity of type TEntity
        /// </summary>
        /// <returns>
        /// The unique key identifying an entity
        /// </returns>
        new TKey Key { get; }
    }

    public interface IEntity
    {
        IKey Key { get; }
    }
}
