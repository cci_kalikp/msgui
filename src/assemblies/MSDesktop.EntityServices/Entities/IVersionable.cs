﻿using System;

namespace MorganStanley.Desktop.EntityServices.Entities
{
    public interface IVersionable
    {
        DateTime Timestamp { get; }
        DateTime? ReferenceDate { get; }
    }
}
