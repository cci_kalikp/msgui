﻿namespace MorganStanley.Desktop.EntityServices.Entities
{
    /// <summary>
    /// Marker interface to say it supports subscriptions. Used by the entity service registration code
    /// </summary>
    public interface ISubscribable
    {
    }
}
