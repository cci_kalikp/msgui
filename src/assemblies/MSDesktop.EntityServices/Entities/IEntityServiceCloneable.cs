﻿namespace MorganStanley.Desktop.EntityServices.Entities
{
    public interface IEntityServiceCloneable<out TEntity> where TEntity : IEntity
    {
        TEntity Clone();
    }
}