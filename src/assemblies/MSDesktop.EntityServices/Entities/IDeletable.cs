﻿namespace MorganStanley.Desktop.EntityServices.Entities
{
    /// <summary>
    /// Marker interface to say it, or one of the other objects in its ecosystem, supports deletion.
    /// </summary>
    public interface IDeletable
    {
    }
}
