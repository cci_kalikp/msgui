﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using MorganStanley.Desktop.EntityServices.Keys;

namespace MorganStanley.Desktop.EntityServices.Data
{
    public class XmlDataConverter<TDomainObject> : IXmlDataConverter<TDomainObject>
        where TDomainObject : class, IEntity, new()
    {
        private readonly XmlSerializer _xmlSerializer = new XmlSerializer(typeof(TDomainObject));

        /// <summary>
        /// Convert an XmlElement to the domain object that it represents.
        /// </summary>
        /// <param name="data">An XmlElement containing the serialised representation of the object</param>
        /// <returns>A domain object</returns>
        public TDomainObject Convert(object data)
        {
            TDomainObject domainObject = null;
            var xmlElement = data as XmlElement;
            if (xmlElement != null)
            {
                var xmlNodeReader = new XmlNodeReader(xmlElement);
                domainObject = _xmlSerializer.Deserialize(xmlNodeReader) as TDomainObject;
            }
            else
            {
                var xmlReader = data as XmlReader;
                if (xmlReader != null)
                {
                    domainObject = _xmlSerializer.Deserialize(xmlReader) as TDomainObject;
                }
            }
            return domainObject;
        }

        public IEnumerable<TDomainObject> ConvertMany(IDictionary values)
        {
            if (values == null)
            {
                return new List<TDomainObject>();
            }

            return from object value in values.Values
                   let data = Convert(value)
                   where data != null
                   select data;
        }

        public object ConvertBack<TPublishObject>(TPublishObject domainObject) where TPublishObject : IEntity<IKey<TDomainObject>>
        {
            return domainObject;
        }
    }
}