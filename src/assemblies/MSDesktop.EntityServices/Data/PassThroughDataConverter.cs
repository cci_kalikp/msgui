﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MorganStanley.Desktop.EntityServices.Keys;

namespace MorganStanley.Desktop.EntityServices.Data
{
    public class PassThroughDataConverter<TDomainObject> : IDataConverter<TDomainObject>
        where TDomainObject : class, IEntity, new()
    {
        public TDomainObject Convert(object data)
        {
            return data as TDomainObject;
        }

        public IEnumerable<TDomainObject> ConvertMany(IDictionary values)
        {
            if (values == null)
            {
                return new List<TDomainObject>();
            }

            return from object value in values.Values
                   let data = Convert(value)
                   where data != null
                   select data;
        }

        public object ConvertBack<TPublishObject>(TPublishObject domainObject) where TPublishObject : IEntity<IKey<TDomainObject>>
        {
            return domainObject;
        }
    }
}