﻿using System.Collections;
using System.Collections.Generic;
using MorganStanley.Desktop.EntityServices.Keys;

namespace MorganStanley.Desktop.EntityServices.Data
{
    public interface IDataConverter<TDomainObject> : IDataConverter<TDomainObject, object> where TDomainObject : class, new()
    {
    }

    public interface IDataConverter<TDomainObject, TConvertableObject> : IDataConverterBase where TDomainObject : class, new()
    {
        /// <summary>
        /// This method will return a new instance everytime it is called
        /// </summary>
        TDomainObject Convert(TConvertableObject data);

        /// <summary>
        /// This method will return a list of new instances everytime it is called
        /// </summary>
        IEnumerable<TDomainObject> ConvertMany(IDictionary values);

        TConvertableObject ConvertBack<TPublishObject>(TPublishObject domainObject) where TPublishObject : IEntity<IKey<TDomainObject>>;
    }

    public interface IDataConverterBase
    {
    }
}
