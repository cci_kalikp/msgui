﻿using System;
using System.Collections;
using System.Collections.Generic;
using MorganStanley.Desktop.EntityServices.Keys;

namespace MorganStanley.Desktop.EntityServices.Data
{
    public abstract class ConnectOnInitialisationDataConnector : IDataConnector, IDisposable
    {
        private readonly IDisposable _connection;
        protected abstract IDisposable Connect();

        protected ConnectOnInitialisationDataConnector()
        {
            _connection = Connect();
        }

        public abstract IDictionary Get<TEntity>(IEnumerable<IQuery<TEntity>> queries);
        public abstract IObservable<object> GetUpdates<TEntity>(IEnumerable<IQuery<TEntity>> queries);
        public abstract void Publish(object objectToPublish);
        public abstract IEnumerable<IVersionQuery<TEntity>> GetVersionList<TEntity>(IQuery<TEntity> key, DateTime? startDate, DateTime? endDate);
        public abstract object GetVersion<TEntity>(IVersionQuery<TEntity> key);
        public abstract void Delete<TEntity>(IKey<TEntity> key);

        public void Dispose()
        {
            _connection.Dispose();
        }
    }
}