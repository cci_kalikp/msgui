﻿namespace MorganStanley.Desktop.EntityServices.Data
{
    public interface IXmlDataConverter<TDomainObject> : IDataConverter<TDomainObject>
        where TDomainObject : class, IEntity, new()
    {
    }
}
