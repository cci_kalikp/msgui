﻿using System;
using System.Collections;
using System.Collections.Generic;
using MorganStanley.Desktop.EntityServices.Keys;

namespace MorganStanley.Desktop.EntityServices.Data
{
    public interface IDataConnector
    {
        IDictionary Get<TEntity>(IEnumerable<IQuery<TEntity>> queries);
        IObservable<object> GetUpdates<TEntity>(IEnumerable<IQuery<TEntity>> queries);
        void Publish(object objectToPublish);

        IEnumerable<IVersionQuery<TEntity>> GetVersionList<TEntity>(IQuery<TEntity> key, DateTime? startDate = null, DateTime? endDate = null);
        object GetVersion<TEntity>(IVersionQuery<TEntity> key);
        

        void Delete<TEntity>(IKey<TEntity> key);
    }
}
