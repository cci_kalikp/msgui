﻿using System;
using System.Collections;
using System.Collections.Generic;
using MorganStanley.Desktop.EntityServices.Keys;

namespace MorganStanley.Desktop.EntityServices.Data
{
    public abstract class ConnectOnAccessDataConnector : IDataConnector
    {
        protected abstract IDisposable Connect();

        public abstract IDictionary GetImpl<TEntity>(IEnumerable<IQuery<TEntity>> queries);
        public abstract IObservable<object> GetUpdatesImpl<TEntity>(IEnumerable<IQuery<TEntity>> queries);
        public abstract void PublishImpl(object objectToPublish);
        public abstract IEnumerable<IVersionQuery<TEntity>> GetVersionListImpl<TEntity>(IQuery<TEntity> key, DateTime? startDate, DateTime? endDate);
        public abstract object GetVersionImpl<TEntity>(IVersionQuery<TEntity> key);
        public abstract void DeleteImpl<TEntity>(IKey<TEntity> key);

        public IDictionary Get<TEntity>(IEnumerable<IQuery<TEntity>> queries)
        {
            IDictionary output;
            using (Connect())
            {
                output = GetImpl(queries);
            }
            return output;
        }

        public IObservable<object> GetUpdates<TEntity>(IEnumerable<IQuery<TEntity>> queries)
        {
            IObservable<object> output;
            using (Connect())
            {
                output = GetUpdatesImpl(queries);
            }
            return output;
        }

        public void Publish(object objectToPublish)
        {
            using (Connect())
            {
                PublishImpl(objectToPublish);
            }
        }

        public IEnumerable<IVersionQuery<TEntity>> GetVersionList<TEntity>(IQuery<TEntity> key, DateTime? startDate, DateTime? endDate)
        {
            IEnumerable<IVersionQuery<TEntity>> output;
            using (Connect())
            {
                output = GetVersionListImpl(key, startDate, endDate);
            }
            return output;
        }

        public object GetVersion<TEntity>(IVersionQuery<TEntity> key)
        {
            object output;
            using (Connect())
            {
                output = GetVersionImpl(key);
            }
            return output;
        }

        public void Delete<TEntity>(IKey<TEntity> key)
        {
            using (Connect())
            {
                DeleteImpl(key);
            }
        }
    }
}