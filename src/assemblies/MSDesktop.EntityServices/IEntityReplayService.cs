﻿using MorganStanley.Desktop.EntityServices.Entities;

namespace MorganStanley.Desktop.EntityServices
{
    public interface IEntityReplayService
    {
        /// <summary>
        /// Replays all subscribed on and retrieved entities for this service
        /// </summary>
        void Replay();
    }

    public interface IEntityReplayService<TEntity> : IEntityService<TEntity>, IEntityReplayService
        where TEntity : class, IEntity
    {
    }
}