﻿#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2010 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/Concord.Configuration.Extended/ConcordConfigurationService.cs#3 $
  $DateTime: 2013/01/18 15:24:18 $
    $Change: 813214 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion File Info Header

namespace MorganStanley.IED.Concord.Configuration
{
  using System;
  using System.Xml;
  using System.Xml.Serialization;
  using Exceptions;
  using Extensions;
  using Interfaces;

  /// <summary>
  /// Concord Configuration wrapper to provide access to configuration
  /// files through objects.
  /// </summary>
  public class ConcordConfigurationService : IConcordConfigurationService
  {
    #region Constants
    private const string ClassName = "ConcordConfigurationService";
    #endregion Constants

    #region Declarations
    private readonly object _lock = new object();
    #endregion Declarations

    #region Public Methods
    /// <summary>
    /// Get the root node of a Concord Configuration config file with the given name.
    /// </summary>
    /// <param name="configurationName_">The name of the configuration file.</param>
    /// <returns>An XmlNode, being the config file's root node, or null.</returns>
    public XmlNode GetConfiguration(string configurationName_)
    {
      lock (_lock)
      {
        Configurator configurator = ConfigurationManager.GetConfig(configurationName_) as Configurator;
        return configurator != null ? configurator.GetNode(".", null) : null;
      }
    }

    /// <summary>
    /// Get a value from a Concord Configuration config file with the given name
    /// at the given XPath.
    /// </summary>
    /// <param name="configurationName_">The name of the configuration file.</param>
    /// <param name="xpath_">The XPath to the value.</param>
    /// <returns>The value sought, or null.</returns>
    public string GetValue(string configurationName_, string xpath_)
    {
      lock (_lock)
      {
        Configurator configurator = ConfigurationManager.GetConfig(configurationName_) as Configurator;
        return configurator != null ? configurator.GetValue(xpath_, null) : null;
      }
    }

    public void SetValue(string configurationName_, string xpath_, string value_)
    {
      lock (_lock)
      {
        Configurator configurator = ConfigurationManager.GetConfig(configurationName_) as Configurator;
        if (configurator != null)
        {
          configurator.SetValue(xpath_, null, value_);
          configurator.Save();
        }
      }
    }

    public bool TryDeserialise<T>(string configurationAndRootNodeName_, out T deserialised_)
    {
      try
      {
        deserialised_ = Deserialise<T>(configurationAndRootNodeName_);
        return true;
      }
      catch (Exception ex_)
      {
        LogLayer.Error.SetLocation(ClassName, String.Format("TryDeserialise<{0}>", typeof(T).GetResolvedTypeName())).WriteFormat("Failed to deserialise Concord Configuration file '{0}'", ex_, configurationAndRootNodeName_);
        deserialised_ = default(T);
        return false;
      }
    }

    /// <summary>
    /// Deserialise a Concord Configuration config file to an object of the given type.
    /// </summary>
    /// <typeparam name="T">The type of the object to create when deserialising the config file.</typeparam>
    /// <param name="configurationAndRootNodeName_">The name of the configuration file, and its root node.</param>
    /// <returns>An object of type T, or the default value (null for reference types) if the config file was
    /// not found or some other error occurred.</returns>
    public T Deserialise<T>(string configurationAndRootNodeName_)
    {
      LogLayer.Info.SetLocation(ClassName, String.Format("Deserialise<{0}>", typeof(T).GetResolvedTypeName())).WriteFormat("Loading from Concord Configuration file '{0}'", configurationAndRootNodeName_);

      XmlNode rootNode = null;
      lock (_lock)
      {
        Configurator config = ConfigurationManager.GetConfig(configurationAndRootNodeName_) as Configurator;
        rootNode = config.GetNode(".", null);
      }
      if (rootNode != null)
      {
        using (XmlNodeReader reader = new XmlNodeReader(rootNode))
        {
          XmlRootAttribute rootAttribute = new XmlRootAttribute(configurationAndRootNodeName_);
          XmlSerializer serialiser = new XmlSerializer(typeof(T), null, Type.EmptyTypes, rootAttribute, String.Empty);
          T result = default(T);
          try
          {
            result = (T)serialiser.Deserialize(reader);
          }
          catch (InvalidOperationException inopex_)
          {
            LogLayer.Error.SetLocation(ClassName, String.Format("Deserialise<{0}>", typeof(T).GetResolvedTypeName())).Write("Exception during deserialisation", inopex_);
          }

          return result;
        }
      }

      return default(T);
    }

    /// <summary>
    /// Serialise an object of the given type to the named Concord Configuration file.
    /// </summary>
    /// <typeparam name="T">The type of the object to serialise.</typeparam>
    /// <param name="configurationAndRootNodeName_">The name of the configuration file, and its root node.</param>
    /// <param name="serialisable_">The object to serialise.</param>
    public void Serialise<T>(string configurationAndRootNodeName_, T serialisable_)
    {
      LogLayer.Info.SetLocation(ClassName, String.Format("Serialise<{0}>", typeof(T).GetResolvedTypeName())).WriteFormat("Saving to Concord Configuration file '{0}'", configurationAndRootNodeName_);

      lock (_lock)
      {
        // get the Configurator and its root node
        Configurator config = ConfigurationManager.GetConfig(configurationAndRootNodeName_) as Configurator;
        XmlNode rootNode = config.GetNode(".", null);

        if (config == null)
        {
          throw new ConcordConfigurationServiceException(String.Format("Configurator is null for configuration file '{0}'", configurationAndRootNodeName_));
        }
        else if (rootNode == null)
        {
          throw new ConcordConfigurationServiceException(String.Format("Root node is null for configuration file '{0}'", configurationAndRootNodeName_));
        }

        // get the XML document that the root node belongs to
        XmlDocument document = rootNode.OwnerDocument;

        // remove all the child nodes, as our serialiser wants a blank canvas
        document.RemoveAll();

        // create an XmlWriter
        using (XmlWriter writer = document.CreateNavigator().AppendChild())
        {
          // the root node of the document _must_ match the name of the configuration file
          // (whether this is by convention or whatever I do not know, but it does, so we
          // are preserving this). To achieve this, we supply an XmlRootAttribute (which
          // will override any existing one).
          XmlRootAttribute rootAttribute = new XmlRootAttribute(configurationAndRootNodeName_);
          XmlSerializer serialiser = new XmlSerializer(typeof(T), null, Type.EmptyTypes, rootAttribute, String.Empty);
          serialiser.Serialize(writer, serialisable_);
          writer.Flush();
          writer.Close();

          if (document == null)
          {
            throw new ConcordConfigurationServiceException(String.Format("Serialisation of config file '{0}' has failed; the serialised document object is null", configurationAndRootNodeName_));
          }

          try
          {
            // We then need to save this back into the config file. This
            // seems to throw with relative reliability, probably legitimately
            // given the surgery we are trying to perform...
            config.SetNode(".", null, document.DocumentElement);
          }
          catch (NullReferenceException)
          {
          }

          // then save to disk
          config.Save();
        }
      }
    }
    #endregion Public Methods
  }
}
