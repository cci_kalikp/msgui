﻿#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2010 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/Concord.Configuration.Extended/Exceptions/ConcordConfigurationServiceException.cs#3 $
  $DateTime: 2013/01/18 15:24:18 $
    $Change: 813214 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion File Info Header

using System;

namespace MorganStanley.IED.Concord.Configuration.Exceptions
{
  public class ConcordConfigurationServiceException : Exception
  {
    #region Constructors
    public ConcordConfigurationServiceException(string message_)
      : base(message_)
    {
    }

    public ConcordConfigurationServiceException(string message_, Exception innerException_)
      : base(message_, innerException_)
    {
    }
    #endregion Constructors
  }
}
