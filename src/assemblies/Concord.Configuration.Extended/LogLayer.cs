#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2002 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/Concord.Configuration.Extended/LogLayer.cs#3 $
  $DateTime: 2013/01/18 15:24:18 $
    $Change: 813214 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion

namespace MorganStanley.IED.Concord.Configuration
{
  using System;
  using System.Reflection;
  using Logging;

  internal class LogLayer
  {
    #region Constants
    public static readonly string META = "Concord";
    public static readonly string PROJECT = "Configuration.Extended";
    public static readonly string RELEASE = Assembly.GetExecutingAssembly().GetName().Version.ToString();
    #endregion Constants

    #region Public Methods
    public static Log Emergency
    {
      get { return Log.Emergency.SetLayer(META, PROJECT, RELEASE); }
    }
    
    public static Log Alert
    {
      get { return Log.Alert.SetLayer(META, PROJECT, RELEASE); }
    }
    
    public static Log Critical
    {
      get { return Log.Critical.SetLayer(META, PROJECT, RELEASE); }
    }
    
    public static Log Error
    {
      get { return Log.Error.SetLayer(META, PROJECT, RELEASE); }
    }
    
    public static Log Warning
    {
      get { return Log.Warning.SetLayer(META, PROJECT, RELEASE); }
    }
    
    public static Log Notice
    {
      get { return Log.Notice.SetLayer(META, PROJECT, RELEASE); }
    }
    
    public static Log Info
    {
      get { return Log.Info.SetLayer(META, PROJECT, RELEASE); }
    }
    
    public static Log Debug
    {
      get { return Log.Debug.SetLayer(META, PROJECT, RELEASE); }
    }
    #endregion Public Methods
  }
}
