﻿#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2009 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/Concord.Configuration.Extended/Interfaces/IConfigurationService.cs#3 $
  $DateTime: 2013/01/18 15:24:18 $
    $Change: 813214 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion File Info Header

namespace MorganStanley.IED.Concord.Configuration.Interfaces
{
  public interface IConfigurationService
  {
    /// <summary>
    /// Deserialise a config file to an object of the given type.
    /// </summary>
    /// <typeparam name="T">The type of the object to create when deserialising the config file.</typeparam>
    /// <param name="configurationAndRootNodeName_">The name of the configuration file, and its root node.</param>
    /// <returns>An object of type T, or the default value (null for reference types) if the config file was
    /// not found or some other error occurred.</returns>
    T Deserialise<T>(string configurationAndRootNodeName_);

    /// <summary>
    /// Tries to deserialise a config file to an object of the given type.
    /// Returns true if the config file exists and the file could be
    /// deserialised, otherwise false.
    /// </summary>
    /// <typeparam name="T">The type of the object to create when deserialising the config file.</typeparam>
    /// <param name="configurationAndRootNodeName_">The name of the configuration file, and its root node.</param>
    /// <param name="deserialised_">When the method returns, this will contain the deserialised object.</param>
    /// <returns>True if the config file was found and the deserialisation succeeded, otherwise false.</returns>
    bool TryDeserialise<T>(string configurationAndRootNodeName_, out T deserialised_);

    /// <summary>
    /// Serialise an object of the given type to the named config file.
    /// </summary>
    /// <typeparam name="T">The type of the object to serialise.</typeparam>
    /// <param name="configurationAndRootNodeName_">The name of the configuration file, and its root node.</param>
    /// <param name="serialisable_">The object to serialise.</param>
    void Serialise<T>(string configurationAndRootNodeName_, T serialisable_);
  }
}
