﻿#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2010 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/Concord.Configuration.Extended/Interfaces/IConcordConfigurationService.cs#3 $
  $DateTime: 2013/01/18 15:24:18 $
    $Change: 813214 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion File Info Header

namespace MorganStanley.IED.Concord.Configuration.Interfaces
{
  using System.Xml;

  public interface IConcordConfigurationService : IConfigurationService
  {
    /// <summary>
    /// Get the root node of a Concord Configuration config file with the given name.
    /// </summary>
    /// <param name="configurationName_">The name of the configuration file.</param>
    /// <returns>An XmlNode, being the config file's root node, or null.</returns>
    XmlNode GetConfiguration(string configurationName_);

    /// <summary>
    /// Get a value from a Concord Configuration config file with the given name
    /// at the given XPath.
    /// </summary>
    /// <param name="configurationName_">The name of the configuration file.</param>
    /// <param name="xpath_">The XPath to the value.</param>
    /// <returns>The value sought, or null.</returns>
    string GetValue(string configurationName_, string xpath_);

    void SetValue(string configurationName_, string xpath_, string value_);
  }
}
