﻿#region File Info Header
/*_____________________________________________________________________________
  Copyright (c) 2009 Morgan Stanley & Co. Incorporated, All Rights Reserved
  Unpublished copyright.  All rights reserved.  This material contains
  proprietary information that shall be used or copied only within Morgan
  Stanley, except with written permission of Morgan Stanley.

        $Id: //eai/msdotnet/msgui/trunk/assemblies/Concord.Configuration.Extended/Extensions/TypeExtensions.cs#3 $
  $DateTime: 2013/01/18 15:24:18 $
    $Change: 813214 $
    $Author: smulovic $
_____________________________________________________________________________*/
#endregion File Info Header

namespace MorganStanley.IED.Concord.Configuration.Extensions
{
  using System;

  /// <summary>
  /// Useful extension methods for System.Type.
  /// </summary>
  internal static class TypeExtensions
  {
    #region Type Name Magic
    /// <summary>
    /// Gets the fully resolved pretty-printable name of a given type,
    /// turning all generic parameters into their names.
    /// </summary>
    /// <returns></returns>
    internal static string GetResolvedTypeName(this Type type_)
    {
      if (type_.IsGenericType)
      {
        string genericTypeName = type_.GetGenericTypeDefinition().Name;
        if (genericTypeName.Contains("`"))
        {
          int tickIndex = genericTypeName.IndexOf("`");
          genericTypeName = genericTypeName.Substring(0, tickIndex);
        }
        string typeName = String.Concat(genericTypeName, "<");
        foreach (Type genericTypeArgument in type_.GetGenericArguments())
        {
          typeName += String.Format("{0}, ", GetResolvedTypeName(genericTypeArgument));
        }
        return String.Concat(typeName.Substring(0, typeName.Length - 2), ">");
      }
      else
      {
        return type_.Name;
      }
    }
    #endregion Type Name Magic
  }
}
