﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/XmlHelper.cs#9 $
// $Change: 898806 $
// $DateTime: 2014/09/30 06:59:48 $
// $Author: caijin $

using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;

namespace MorganStanley.MSDotNet.MSGui.Impl
{
    public static class XmlHelper
    {
        public static XElement XmlElementToXElement(XmlElement element_)
        {
            if (element_ == null) return null;
            XPathNavigator navigator = element_.CreateNavigator();
            if (navigator != null)
            {
                return XElement.Load(navigator.ReadSubtree());
            }
            return null;
        }

        public static XmlElement XElementToXmlElement(XElement element_, XmlDocument ownerDocument_)
        {
            return ownerDocument_.ReadNode(element_.CreateReader()) as XmlElement;
        }

        public static XmlElement XElementToXmlElement(XElement element_)
        {
            return new XmlDocument().ReadNode(element_.CreateReader()) as XmlElement;
        }

        public static string TryGetAttribute(XElement elem_, string name_)
        {
            XAttribute attr = elem_.Attribute(name_);
            return attr == null ? null : attr.Value;
        }

        public static void TrySetAttribute(XElement elem_, string name_, string content_)
        {
            if (content_ != null)
            {
                elem_.Add(new XAttribute(name_, content_));
            }
        }

        public static void WriteAttribute(XmlNode node_, string name_, string value_)
        {
            XmlAttribute attribute = node_.Attributes[name_];
            if (attribute == null)
            {
                attribute = node_.OwnerDocument.CreateAttribute(name_);
                node_.Attributes.Append(attribute);
            }
            attribute.Value = value_;
        }
        public static string ReadAttribute(XmlNode node_, string name_, string defaultValue_ = null)
        {
            XmlAttribute attribute = node_.Attributes[name_];
            if (attribute != null)
            {
                return attribute.Value;
            }
            return defaultValue_;
        }

        public static DateTime ReadAttribute(XmlNode node_, string name_, DateTime defaultValue_)
        {
            string s = ReadAttribute(node_, name_);
            if (s == null)
            {
                return defaultValue_;
            }
            return XmlConvert.ToDateTime(s, XmlDateTimeSerializationMode.Utc);
        }

        public static T ReadAttributeWithConverter<T>(XmlNode node_, string name_, T defaultValue_)
        {
            string text = ReadAttribute(node_, name_);
            if (text == null)
            {
                return defaultValue_;
            }
            return
                (T) TypeDescriptor.GetConverter(typeof (T)).ConvertFromString(null, CultureInfo.InvariantCulture, text);
        }

        public static T ReadEnumAttribute<T>(XmlNode node_, string name_, T defaultValue_)
        {
            string str = ReadAttribute(node_, name_);
            if (str == null)
            {
                return defaultValue_;
            }
            return (T) Enum.Parse(defaultValue_.GetType(), str);
        }

        public static bool ReadAttribute(XmlNode node_, string name_, bool defaultValue_)
        {
            string s = ReadAttribute(node_, name_);
            if (s == null)
            {
                return defaultValue_;
            }
            return XmlConvert.ToBoolean(s);
        }

        public static double ReadAttribute(XmlNode node_, string name_, double defaultValue_)
        {
            string s = ReadAttribute(node_, name_, (string) null);
            if (s == null)
            {
                return defaultValue_;
            }
            return XmlConvert.ToDouble(s);
        }

        public static int ReadAttribute(XmlNode node_, string name_, int defaultValue_)
        {
            string s = ReadAttribute(node_, name_, (string)null);
            if (s == null)
            {
                return defaultValue_;
            }
            return XmlConvert.ToInt32(s);
        }
  
        public static void WriteAttributeWithConverter<T>(XmlWriter writer_, string name_, T value_)
        {
            writer_.WriteAttributeString(name_,
                                         TypeDescriptor.GetConverter(typeof (T)).ConvertToString(value_) ?? string.Empty);

        }
        public static void WriteAttributeWithConverter<T>(XmlWriter writer_, string name_, T? value_) where T:struct 
        {
            if (!value_.HasValue) return;
            writer_.WriteAttributeString(name_,
                                         TypeDescriptor.GetConverter(typeof(T)).ConvertToString(value_) ?? string.Empty);

        }
        public static void WriteAttribute(XmlWriter writer_, string name_, DateTime value_)
        {
            writer_.WriteAttributeString(name_, XmlConvert.ToString(value_, XmlDateTimeSerializationMode.Utc));
        }

        public static void WriteAttribute(XmlWriter writer_, string name_, Enum value_)
        {
            writer_.WriteAttributeString(name_, value_.ToString());
        }

        public static void WriteAttribute(XmlWriter writer_, string name_, int value_)
        {
            writer_.WriteAttributeString(name_, XmlConvert.ToString(value_));
        }

        public static void WriteAttribute(XmlWriter writer_, string name_, double value_)
        {
            writer_.WriteAttributeString(name_, XmlConvert.ToString(value_));
        }

        public static void WriteAttribute(XmlWriter writer_, string name_, bool value_)
        {
            writer_.WriteAttributeString(name_, XmlConvert.ToString(value_));
        }
    }
}
