﻿using System;
using System.ComponentModel;
using System.Windows;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MorganStanley.MSDotNet.MSGui.Impl.HidingLocking
{
    internal class HidingLockingManager : IHidingLockingManager
    {
        private HideLockUIGranularity m_granularity;
        public HideLockUIGranularity Granularity
        {
            get
            {
                return m_granularity;
            }
            set
            {
                m_granularity = value;
                InvokeHideUIGranularityChanged(null);
            }
        }

        public void ToggleFlag(HideLockUIGranularity flag)
        {
            if (Granularity.HasFlag(flag))
            {
                Granularity -= flag;
            }
            else
            {
                Granularity |= flag;
            }
        }

        public event EventHandler HideUIGranularityChanged;

        private void InvokeHideUIGranularityChanged(EventArgs e)
        {
            EventHandler handler = HideUIGranularityChanged;
            if (handler != null) handler(this, e);
        }

        public Visibility GetVisibilityForElement(HideLockUIGranularity element)
        {
            return m_granularity.HasFlag(element) ? Visibility.Collapsed : Visibility.Visible;
        }

    }
}
