﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using MorganStanley.MSDotNet.MSGui.Core.SmartApi;

namespace MorganStanley.MSDotNet.MSGui.Impl.SmartApi
{
    class SmartApiResolver : ISmartApiResolver
    {
        internal readonly Dictionary<ExternalAssembly, ExternalAssemblyDescriptor> AssemblyDescriptors;

        private readonly List<ExternalAssemblyDescriptor> _missingAssembiles;

        public SmartApiResolver()
        {
            //Logger.Info("Enabled Smart API assembly resolving");
            AssemblyDescriptors = new Dictionary<ExternalAssembly, ExternalAssemblyDescriptor>();
            AppDomain.CurrentDomain.AssemblyResolve += CurrentDomainOnAssemblyResolve;
        }
 

        private Assembly CurrentDomainOnAssemblyResolve(object sender_, ResolveEventArgs args_)
        {
            if (args_.Name.Contains(".resources,"))
                return null;
            var name = new AssemblyName(args_.Name);
            var afsPath = AssemblyDescriptors.Values.FirstOrDefault(a_ => a_.Name.FullName == name.FullName).AfsPath;
            if (string.IsNullOrEmpty(afsPath))
            { 
                // will ignore version
                afsPath = AssemblyDescriptors.Values.FirstOrDefault(a_ => a_.Name.Name == name.Name).AfsPath;    
                if (!string.IsNullOrEmpty(afsPath))
                {
                    try
                    {
                        string localPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, name.Name);
                        if (File.Exists(localPath + ".dll"))
                        {
                            var assembly = Assembly.LoadFrom(localPath + ".dll");
                            if (assembly != null && assembly.GetName().Name == name.Name)
                            {
                                return assembly;
                            }
                        }
                         if (File.Exists(localPath + ".exe"))
                        {
                            var assembly = Assembly.LoadFrom(localPath + ".exe");
                            if (assembly != null && assembly.GetName().Name == name.Name)
                            {
                                return assembly;
                            }
                        }
                    }
                    catch  
                    { 
                    }
                }  
            }
            if (string.IsNullOrEmpty(afsPath))
            {
                return null;
            }
            try
            {
                return Assembly.LoadFrom(afsPath);
            }
            catch (InvalidOperationException)
            {
                //Logger.CriticalWithFormat("No AFS path registered for the assembly {0}", args_.Name);
                return null;
            }
        }

        public void RequireAssembly(params ExternalAssembly[] assemblies_)
        {
            foreach (var externalAssembly in assemblies_)
            {
                var descriptor = AssemblyDescriptors[externalAssembly];
                //Logger.InfoWithFormat("Will load assembly {0}", descriptor.Name);
                Assembly.Load(descriptor.Name);
            }
        }
    }
}
