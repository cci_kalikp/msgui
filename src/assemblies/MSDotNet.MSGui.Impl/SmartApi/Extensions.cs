﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Reflection; 
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.SmartApi;

namespace MorganStanley.MSDotNet.MSGui.Impl.SmartApi
{
    public static class Extensions
    {
        private const string ConfigurationSectionName = "SmartApiConfiguration";

        private static string _igVersionShort = "v14.1";
        private static string _igVersion = "14.1.2062";
        private static string _igVersionFull = "14.1.20141.2062"; 
        private static string _dnPartsVersion = "2014.5.23.1";
        private static string _dnPartsVersionFull = "2014.05.23.1";
        private static string _msdesktopVersion = Assembly.GetAssembly(typeof (Extensions)).GetName().Version.ToString();
        private static readonly string _msdesktopAssemblyLocation = Path.GetDirectoryName(Assembly.GetAssembly(typeof(Extensions)).Location) + @"\..\..\..\..\assemblies";
        private static readonly string _msdesktopVersionFull;
        private const string NullVersion = "0.0.0.0";

        public static string MSDesktopAFS;
        //public const string MSDesktopAFSEnvName = "MSDesktopAFS";

        static Extensions()
        {
            if (_msdesktopVersion != NullVersion)
            {
                string[] parts = _msdesktopVersion.Split('.');
                if (parts.Length == 4)
                {
                    _msdesktopVersionFull = parts[0] + "." + parts[1].PadLeft(2, '0') + "." + parts[2].PadLeft(2, '0');
                    if (parts[3] != "1")
                    {
                        _msdesktopVersionFull += "." + parts[3];
                    }
                }
            }

            if (_msdesktopVersion != NullVersion)
            {
                MSDesktopAFS = string.Format(@"\\san01b\DevAppsGML\dist\msdotnet\PROJ\msgui\{0}\", _msdesktopVersionFull);
                //Environment.SetEnvironmentVariable(MSDesktopAFSEnvName, MSDesktopAFS);        
            }
        }

        public static void EnableSmartApi()
        {
            EnableSmartApi(null);
        }

        internal static void EnablePostSharpResolver()
        {
            var resolver = new SmartApiResolver();
            AddMSDesktopResolver(resolver, ExternalAssembly.MSDesktopAspectProvider, "AspectProvider");
            //todo: get the postsharp runtime from static ms\dist location 
            AddMSDesktopRedirectResolver2(resolver, ExternalAssembly.PostSharp, "PostSharp",
                "Version=2.1.0.0, Culture=neutral, PublicKeyToken=b13fd38b8f9c99d7", @"..\lib\", @"..\3rd\");
            AddDnpartsResolver(resolver, ExternalAssembly.DnpartsMSLog, "MSLog");
            AddDnpartsResolver(resolver, ExternalAssembly.DnpartsMSTools, "MSTools");
        }

        public static void EnableSmartApi(this Framework framework_)
        {

            if (ExtensionsUtils.Resolver != null) return;
            var resolver = new SmartApiResolver();
            ExtensionsUtils.Resolver = resolver;
 
            var configurationSection = GetVersionsFromConfiguration();

            if (configurationSection != null)
            {
                _igVersionShort = configurationSection["infragisticsVersionShort"] ?? _igVersionShort;
                _igVersion = configurationSection["infragisticsVersion"] ?? _igVersion;
                _igVersionFull = configurationSection["infragisticsVersionFull"] ?? _igVersionFull;
                _dnPartsVersion = configurationSection["dnpartsVersion"] ?? _dnPartsVersion;
                _dnPartsVersionFull = configurationSection["dnpartsVersionFull"] ?? _dnPartsVersionFull;   
            }

            resolver.AssemblyDescriptors[ExternalAssembly.SharpZipLib] = new ExternalAssemblyDescriptor
                {
                    Name = new AssemblyName("ICSharpCode.SharpZipLib, Version=0.86.0.518, Culture=neutral, PublicKeyToken=1b03e6acf1164f73"),
                    AfsPath =
                        "\\\\ms\\dist\\msdotnet\\PROJ\\sharpziplib\\0.86.0\\assemblies\\ICSharpCode.SharpZipLib.dll"
                };
            resolver.AssemblyDescriptors[ExternalAssembly.AttachLink] = new ExternalAssemblyDescriptor
                {
                    Name = new AssemblyName("DeskApps.attachlink-api, Version=1.4.1.18236, Culture=neutral, PublicKeyToken=24b9654a6b7f384a"),
                    AfsPath = "\\\\ms\\dist\\deskapps\\PROJ\\attachlink-api\\1.4.1\\assemblies\\DeskApps.attachlink-api.dll"
                };
            resolver.AssemblyDescriptors[ExternalAssembly.WindowsApiCodePackShell] = new ExternalAssemblyDescriptor
                {
                    Name = new AssemblyName("Microsoft.WindowsAPICodePack.Shell, Version=1.1.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"),
                    AfsPath =
                        "\\\\ms\\dist\\dotnet3rd\\PROJ\\windowsAPICodePack\\1.1\\binaries\\Microsoft.WindowsAPICodePack.Shell.dll"
                };
            AddDnpartsResolver2(resolver, ExternalAssembly.MSDesktopIPC, "MSDesktop.IPC"); 
            AddDnpartsResolver2(resolver, ExternalAssembly.MSDesktopEntitlement, "MSDesktop.Entitlement");
            AddDnpartsResolver2(resolver, ExternalAssembly.MSDesktopServiceDiscovery, "MSDesktop.ServiceDiscovery");  
            resolver.AssemblyDescriptors[ExternalAssembly.ReactiveThreading] = new ExternalAssemblyDescriptor
                {
                    Name = new AssemblyName("System.Reactive.Windows.Threading, Version=2.0.20823.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"),
                    AfsPath =
                        "\\\\ms\\dist\\microsoft\\PROJ\\rx\\2.0.20823\\assemblies\\4.0\\System.Reactive.Windows.Threading.dll"
                };
            resolver.AssemblyDescriptors[ExternalAssembly.ReactiveInterfaces] = new ExternalAssemblyDescriptor
            {
                Name = new AssemblyName("System.Reactive.Interfaces, Version=2.0.20823.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"),
                AfsPath =
                    "\\\\ms\\dist\\microsoft\\PROJ\\rx\\2.0.20823\\assemblies\\4.0\\System.Reactive.Interfaces.dll"
            };
            resolver.AssemblyDescriptors[ExternalAssembly.ReactiveCore] = new ExternalAssemblyDescriptor
                {
                    Name = new AssemblyName("System.Reactive.Core, Version=2.0.20823.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"),
                    AfsPath =
                        "\\\\ms\\dist\\microsoft\\PROJ\\rx\\2.0.20823\\assemblies\\4.0\\System.Reactive.Core.dll"
                };
            resolver.AssemblyDescriptors[ExternalAssembly.ReactiveLinq] = new ExternalAssemblyDescriptor
                {
                    Name =
                        new AssemblyName(
                            "System.Reactive.Linq, Version=2.0.20823.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"),
                    AfsPath =
                        "\\\\ms\\dist\\microsoft\\PROJ\\rx\\2.0.20823\\assemblies\\4.0\\System.Reactive.Linq.dll"
                };
            resolver.AssemblyDescriptors[ExternalAssembly.NewtonSoftJson] = new ExternalAssemblyDescriptor
            {
                Name =
                    new AssemblyName(
                        "Newtonsoft.Json, Version=4.5.0.0, Culture=neutral, PublicKeyToken=30ad4fe6b2a6aeed"),
                AfsPath =
                    @"\\san01b\DevAppsGML\dist\dotnet3rd\PROJ\jsondotnet\5.0.6\assemblies\Net40\Newtonsoft.Json.dll"
            };
            resolver.AssemblyDescriptors[ExternalAssembly.XceedWpfToolkit] = new ExternalAssemblyDescriptor
            {
                Name =
                    new AssemblyName(
                        "Xceed.Wpf.Toolkit, Version=2.0.0.0, Culture=neutral, PublicKeyToken=3e4669d2f30244f4"),
                AfsPath =
                    @"\\san01b\DevAppsGML\dist\dotnet3rd\PROJ\extendedWPFToolkit\2.0.0.0\assemblies\Xceed.Wpf.Toolkit.dll"
            };            
            resolver.AssemblyDescriptors[ExternalAssembly.Interactivity] = new ExternalAssemblyDescriptor
                {
                    Name =
                        new AssemblyName(
                            "System.Windows.Interactivity, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"),
                    AfsPath = @"\\san01b\DevAppsGML\dist\microsoft\PROJ\prism\4.0.0\assemblies\System.Windows.Interactivity.dll"
                };
            resolver.AssemblyDescriptors[ExternalAssembly.MicrosoftComposite] = new ExternalAssemblyDescriptor
                {
                    Name =
                        new AssemblyName(
                            "Microsoft.Practices.Composite, Version=2.0.1.0, Culture=neutral, PublicKeyToken=d0e341ccaeb444f7"),
                    AfsPath = @"\\san01b\DevAppsGML\dist\microsoft\PROJ\composite\2.2.0\assemblies\Microsoft.Practices.Composite.dll"
                };
            resolver.AssemblyDescriptors[ExternalAssembly.MicrosoftCompositePresentation] = new ExternalAssemblyDescriptor
            {
                Name =
                    new AssemblyName(
                        "Microsoft.Practices.Composite.Presentation, Version=2.0.1.0, Culture=neutral, PublicKeyToken=d0e341ccaeb444f7"),
                AfsPath = @"\\san01b\DevAppsGML\dist\microsoft\PROJ\composite\2.2.0\assemblies\Microsoft.Practices.Composite.Presentation.dll"
            };
            resolver.AssemblyDescriptors[ExternalAssembly.MicrosoftCompositeUnityExtensions] = new ExternalAssemblyDescriptor
            {
                Name =
                    new AssemblyName(
                        "Microsoft.Practices.Composite.UnityExtensions, Version=2.0.1.0, Culture=neutral, PublicKeyToken=d0e341ccaeb444f7"),
                AfsPath = @"\\san01b\DevAppsGML\dist\microsoft\PROJ\composite\2.2.0\assemblies\Microsoft.Practices.Composite.UnityExtensions.dll"
            };
            resolver.AssemblyDescriptors[ExternalAssembly.MicrosoftServiceLocation] = new ExternalAssemblyDescriptor
            {
                Name =
                    new AssemblyName(
                        "Microsoft.Practices.ServiceLocation, Version=1.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"),
                AfsPath = @"\\san01b\DevAppsGML\dist\microsoft\PROJ\composite\2.2.0\assemblies\Microsoft.Practices.ServiceLocation.dll"
            };
            resolver.AssemblyDescriptors[ExternalAssembly.MicrosoftCompositeObjectBuilder2] = new ExternalAssemblyDescriptor
            {
                Name =
                    new AssemblyName(
                        "Microsoft.Practices.ObjectBuilder2, Version=2.2.0.0, Culture=neutral, PublicKeyToken=d0e341ccaeb444f7"),
                AfsPath = @"\\san01b\DevAppsGML\dist\microsoft\PROJ\unity\1.2.0\assemblies\Microsoft.Practices.ObjectBuilder2.dll"
            };
            resolver.AssemblyDescriptors[ExternalAssembly.MicrosoftUnity] = new ExternalAssemblyDescriptor
            {
                Name =
                    new AssemblyName(
                        "Microsoft.Practices.Unity, Version=2.0.414.0, Culture=neutral, PublicKeyToken=d0e341ccaeb444f7"),
                AfsPath = @"\\san01b\DevAppsGML\dist\microsoft\PROJ\unity\2.0.0\assemblies\Microsoft.Practices.Unity.dll"
            };

            resolver.AssemblyDescriptors[ExternalAssembly.ConcordRuntime] = new ExternalAssemblyDescriptor
                {
                    Name = new AssemblyName(
                        "Concord.Runtime, Version=2.1.1.0, Culture=neutral, PublicKeyToken=354cc34ba0b81117"
                        ),
                    AfsPath = @"\\san01b\DevAppsGML\dist\concord\PROJ\runtime\2.1.1\assemblies\Concord.Runtime.dll"
                };
            resolver.AssemblyDescriptors[ExternalAssembly.ConcordLogger] = new ExternalAssemblyDescriptor
            {
                Name = new AssemblyName(
                    "Concord.Logger, Version=2.0.0.29705, Culture=neutral, PublicKeyToken=354cc34ba0b81117"
                    ),
                AfsPath = @"\\san01b\DevAppsGML\dist\concord\PROJ\logger\2.0.0\assemblies\Concord.Logger.2.0.dll"
            };
            resolver.AssemblyDescriptors[ExternalAssembly.ConcordApplication] = new ExternalAssemblyDescriptor
            {
                Name = new AssemblyName(
                    "Concord.Application, Version=2.3.0.52938, Culture=neutral, PublicKeyToken=354cc34ba0b81117"
                    ),
                AfsPath = @"\\san01b\DevAppsGML\dist\concord\PROJ\application\2.3.0\assemblies\Concord.Application.2.3.dll"
            };
            resolver.AssemblyDescriptors[ExternalAssembly.ConcordTopicMediator] = new ExternalAssemblyDescriptor
            {
                Name = new AssemblyName(
                    "Concord.TopicMediator, Version=2.1.2.29263, Culture=neutral, PublicKeyToken=354cc34ba0b81117"
                    ),
                AfsPath = @"\\san01b\DevAppsGML\dist\concord\PROJ\topicmediator\2.1.2\assemblies\Concord.TopicMediator.2.1.dll"
            };
            AddMSDesktopRedirectResolver(resolver, ExternalAssembly.ConcordConfiguration, "Concord.Configuration", 
                "Version=2.1.2.29263, Culture=neutral, PublicKeyToken=354cc34ba0b81117", "ConcordConfiguration");
            resolver.AssemblyDescriptors[ExternalAssembly.Log4net] = new ExternalAssemblyDescriptor
            {
                Name = new AssemblyName("log4net, Version=1.2.10.0, Culture=neutral, PublicKeyToken=1b44e1d426115821"),
                AfsPath = @"\\san01b\DevAppsGML\dist\msdotnet\PROJ\log4net\1.2.11\assemblies\log4net.dll"
            };
            resolver.AssemblyDescriptors[ExternalAssembly.GoogleProtocolBuffers] = new ExternalAssemblyDescriptor
                {
                    Name = new AssemblyName("Google.ProtocolBuffers, Version=2.4.1.473, Culture=neutral, PublicKeyToken=55f7125234beb589"),
                    AfsPath = @"\\san01b\DevAppsGML\dist\msdotnet\PROJ\protobuf-csharp-port\2.4.1\assemblies\Google.ProtocolBuffers.dll"
                };
            resolver.AssemblyDescriptors[ExternalAssembly.CefSharp] = new ExternalAssemblyDescriptor
                {
                    Name = new AssemblyName("CefSharp, Version=1.25.4.0, Culture=neutral, PublicKeyToken=d0e341ccaeb444f7"),
                    AfsPath = @"\\san01b\DevAppsGML\dist\dotnet3rd\PROJ\cef\1.25.4.0-nd\common\CefSharp.dll"
                };
            resolver.AssemblyDescriptors[ExternalAssembly.CefSharpWpf] = new ExternalAssemblyDescriptor
            {
                Name = new AssemblyName("CefSharp.Wpf, Version=1.25.4.0, Culture=neutral, PublicKeyToken=d0e341ccaeb444f7"),
                AfsPath = @"\\san01b\DevAppsGML\dist\dotnet3rd\PROJ\cef\1.25.4.0-nd\common\CefSharp.Wpf.dll"
            };
            AddDnpartsResolver(resolver, ExternalAssembly.DnpartsMSNet, "MSNet");
            AddDnpartsResolver(resolver, ExternalAssembly.DnpartsMSNetNative, "MSNet.Native");
            AddDnpartsResolver(resolver, ExternalAssembly.DnpartsMSLog, "MSLog");
            AddDnpartsResolver(resolver, ExternalAssembly.DnpartsMSTools, "MSTools");
            AddDnpartsResolver(resolver, ExternalAssembly.DnpartsMSXml, "MSXml");
            AddDnpartsResolver(resolver, ExternalAssembly.DnpartsChannels, "Channels");
            AddDnpartsResolver(resolver, ExternalAssembly.DnpartsChannelsCps, "Channels.Cps");
            AddDnpartsResolver(resolver, ExternalAssembly.DnpartsProtocolBuffers, "ProtocolBuffers");
            AddDnpartsResolver(resolver, ExternalAssembly.DnpartsCafDispatch, "CafDispatch");
            resolver.AssemblyDescriptors[ExternalAssembly.MSDotNetRuntime] = new ExternalAssemblyDescriptor
                {
                    Name = new AssemblyName("MSDotNet.Runtime, Version=1.0.0.0, Culture=neutral, PublicKeyToken=d0e341ccaeb444f7"),
                    AfsPath = @"\\san01b\DevAppsGML\dist\msdotnet\PROJ\runtime\1.0\assemblies\MSDotNet.Runtime.dll" 
                };

            AddInfragisticsResolver(resolver, ExternalAssembly.Infragistics, null);
            AddInfragisticsResolver(resolver, ExternalAssembly.InfragisticsRibbon, "Ribbon");
            AddInfragisticsResolver(resolver, ExternalAssembly.InfragisticsEditors, "Editors");
            AddInfragisticsResolver(resolver, ExternalAssembly.InfragisticsDockManager, "DockManager");
            AddInfragisticsResolver(resolver, ExternalAssembly.InfragisticsDataPresenter, "DataPresenter");
            AddInfragisticsResolver(resolver, ExternalAssembly.InfragisticsXamMenu, "Controls.Menus.XamMenu");
            AddInfragisticsResolver(resolver, ExternalAssembly.InfragisticsXamColorPicker, "Controls.Editors.XamColorPicker");
            AddInfragisticsResolver(resolver, ExternalAssembly.InfragisticsPersistence, "Persistence");
            AddInfragisticsResolver(resolver, ExternalAssembly.InfragisticsDragAndDrop, "DragDrop");
            AddInfragisticsResolver(resolver, ExternalAssembly.InfragisticsXamOutlookBar, "OutlookBar");
 
            AddMSDesktopResolver(resolver, ExternalAssembly.MSDesktopDebugging, "Debugging");
            AddMSDesktopResolver(resolver, ExternalAssembly.MSDesktopViewModelAPI, "ViewModelAPI");
            AddMSDesktopResolver(resolver, ExternalAssembly.MSDesktopWorkspaces, "Workspaces"); 
            AddMSDesktopResolver(resolver, ExternalAssembly.MSDesktopEnvironment, "Environment");
            AddMSDesktopResolver(resolver, ExternalAssembly.MSDesktopModuleEntitlements, "ModuleEntitlements");
            AddMSDesktopResolver(resolver, ExternalAssembly.MSDesktopMessageRouting, "MessageRouting");
            AddMSDesktopResolver(resolver, ExternalAssembly.MSDesktopMessageRoutingInterfaces, "MessageRouting.Interfaces");
            AddMSDesktopResolver(resolver, ExternalAssembly.MSDesktopMessageRoutingGUI, "MessageRouting.GUI");
            AddMSDesktopResolver(resolver, ExternalAssembly.MSDesktopMessageRoutingHistory, "MessageRouting.History");
            AddMSDesktopResolver(resolver, ExternalAssembly.MSDesktopMessageRoutingHistoryGUI, "MessageRouting.History.GUI");
            AddMSDesktopResolver(resolver, ExternalAssembly.MSDesktopMessageRoutingMonitor, "MessageRouting.Monitor");
            AddMSDesktopResolver(resolver, ExternalAssembly.MSDesktopHTTPServer, "HTTPServer");
            AddMSDesktopResolver(resolver, ExternalAssembly.MSDesktopMailService, "MailService");
            AddMSDesktopResolver(resolver, ExternalAssembly.MSDesktopWormhole, "Wormhole");
            AddMSDesktopResolver(resolver, ExternalAssembly.MSDesktopAuthenticationService, "AuthenticationService");
            AddMSDesktopResolver(resolver, ExternalAssembly.MSDesktopWebSupport, "WebSupport");
            AddMSDesktopResolver(resolver, ExternalAssembly.MSDesktopWebSupportCEF, "WebSupport.CEF", "MSDesktop.CEFWebSupport");
            AddMSDesktopResolver(resolver, ExternalAssembly.MSDesktopCrossMachineApplication, "CrossMachineApplication");
            AddMSDesktopIsolationResolver(resolver, ExternalAssembly.MSDesktopIsolation, "Isolation");
            AddMSDesktopIsolationResolver(resolver, ExternalAssembly.MSDesktopIsolationInterfaces, "Isolation.Interfaces");
        }

        private static NameValueCollection GetVersionsFromConfiguration()
        {
            NameValueCollection configurationSection = null;
            try
            {
                configurationSection = ConfigurationManager.GetSection(ConfigurationSectionName) as NameValueCollection;
            }
            catch (Exception e)
            {
                var configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                var section = configuration.GetSection(ConfigurationSectionName);
                if (section != null && section.SectionInformation.GetRawXml() != null)
                {
                    var configDoc = XDocument.Parse(section.SectionInformation.GetRawXml());
                    configurationSection = new NameValueCollection();
                    foreach (var addElement in configDoc.Descendants("add"))
                    {
                        configurationSection[addElement.Attribute("key").Value] =
                            addElement.Attribute("value").Value;
                    }
                }
            }
            return configurationSection;
        }

        private static void AddInfragisticsResolver(SmartApiResolver resolver, ExternalAssembly key, string name)
        {
            resolver.AssemblyDescriptors[key] = new ExternalAssemblyDescriptor
                {
                    Name =
                        new AssemblyName(
                            String.Format(
                            "InfragisticsWPF4{2}.{0}, Version={1}, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb",
                            _igVersionShort, _igVersionFull, string.IsNullOrEmpty(name) ? "" : "." + name)),
                    AfsPath =
                        String.Format(
                        @"\\san01b\DevAppsGML\dist\msdotnet\PROJ\infragisticswpf\{0}\assemblies\InfragisticsWPF4{2}.{1}.dll", _igVersion,
                        _igVersionShort, string.IsNullOrEmpty(name) ? "" : "." + name)
                };
        }

        private static void AddDnpartsResolver(SmartApiResolver resolver, ExternalAssembly assembly, string name)
        {
            AddDnpartsResolver2(resolver, assembly, "MSDotNet." + name); 
        }

        private static void AddDnpartsResolver2(SmartApiResolver resolver, ExternalAssembly assembly, string fullName)
        {
            resolver.AssemblyDescriptors[assembly] = new ExternalAssemblyDescriptor
            {
                Name =
                    new AssemblyName(
                        String.Format(
                            "{1}, Version={0}, Culture=neutral, PublicKeyToken=d0e341ccaeb444f7",
                            _dnPartsVersion, fullName)),
                AfsPath =
                    String.Format(@"\\san01b\DevAppsGML\dist\msdotnet\PROJ\dnparts\{0}\assemblies\{1}.dll",
                                  _dnPartsVersionFull, fullName)
            };
        }

        private static void AddMSDesktopResolver(SmartApiResolver resolver, ExternalAssembly assembly, string name, string projectFolder = null)
        {
            var descriptor = new ExternalAssemblyDescriptor
            {
                Name =
                    new AssemblyName(
                        String.Format(
                            "MSDesktop.{1}, Version={0}, Culture=neutral, PublicKeyToken=d0e341ccaeb444f7",
                            _msdesktopVersion, name))
            };
            if (_msdesktopVersion != NullVersion)
            {
                descriptor.AfsPath = String.Format(@"\\san01b\DevAppsGML\dist\msdotnet\PROJ\msgui\{0}\assemblies\MSDesktop.{1}.dll",
                                                   _msdesktopVersionFull, name); 
            }
            else
            {
                string fullName = "MSDesktop." + name;

                descriptor.AfsPath = string.Format(@"{0}\{2}{3}\{1}.dll", _msdesktopAssemblyLocation,
                                                   fullName, projectFolder ?? fullName, PathUtilities.GetExecutableRelativePath());
            }
            resolver.AssemblyDescriptors[assembly] = descriptor;
        }

        public static string GetMSDesktopApplicationLocation(MSDesktopApplication application)
        {
            string applicationFolder;
            string localApplicationFolder;
            switch (application)
            {
                case MSDesktopApplication.IsolationHostProcess:
                case MSDesktopApplication.IsolationHostProcessX86:
                    localApplicationFolder = applicationFolder = "MSDesktop.Isolation";
                    /*if (application == MSDesktopApplication.IsolationHostProcessX86)
                    {
                        localApplicationFolder += ".X86";
                    }*/
                    break; 
                case MSDesktopApplication.Launcher:
                    localApplicationFolder = applicationFolder = "Launcher"; 
                    break;
                case MSDesktopApplication.LoaderConfigEditor:
                    localApplicationFolder = applicationFolder = "MSDesktop.LoaderConfigEditor";  
                    break;
                case MSDesktopApplication.LogViewer:
                    localApplicationFolder = applicationFolder = "MSDesktop.LogViewer";
                    break;
                default:
                    return null;
            }
            if (_msdesktopVersion != NullVersion)
            {
                return String.Format(@"\\san01b\DevAppsGML\dist\msdotnet\PROJ\msgui\{0}\bin\{1}",
                                                   _msdesktopVersionFull, applicationFolder);
            }
            return string.Format(@"{0}\{1}{2}", _msdesktopAssemblyLocation, localApplicationFolder, PathUtilities.GetExecutableRelativePath());
             
        }
         

        private static void AddMSDesktopIsolationResolver(SmartApiResolver resolver, ExternalAssembly assembly, string name, string projectFolder = null)
        {
            var descriptor = new ExternalAssemblyDescriptor
            {
                Name =
                    new AssemblyName(
                        String.Format(
                            "MSDesktop.{1}, Version={0}, Culture=neutral, PublicKeyToken=d0e341ccaeb444f7",
                           _msdesktopVersion, name))
            };
            if (_msdesktopVersion != NullVersion)
            {
                descriptor.AfsPath = String.Format(@"\\san01b\DevAppsGML\dist\msdotnet\PROJ\msgui\{0}\bin\MSDesktop.Isolation\MSDesktop.{1}.dll",
                                                   _msdesktopVersionFull, name);
            }
            else
            {
                string fullName = "MSDesktop." + name;

                descriptor.AfsPath = string.Format(@"{0}\{2}{3}\{1}.dll", _msdesktopAssemblyLocation,
                                                   fullName, projectFolder ?? fullName, PathUtilities.GetExecutableRelativePath());
            }
            resolver.AssemblyDescriptors[assembly] = descriptor;
        }

        private static void AddMSDesktopRedirectResolver(SmartApiResolver resolver, ExternalAssembly assembly, string fullName, string versionInfo, string folderName=null)
        {
            var descriptor = new ExternalAssemblyDescriptor
            {
                Name =
                    new AssemblyName(
                        String.Format(
                            "{1}, {0}",
                            versionInfo, fullName))
            };
            if (File.Exists(fullName + ".dll"))
            {
                descriptor.AfsPath = fullName + ".dll";
            }
            else if (_msdesktopVersion != NullVersion)
            {
                descriptor.AfsPath = String.Format(@"\\san01b\DevAppsGML\dist\msdotnet\PROJ\msgui\{0}\assemblies\{1}.dll",
                                                   _msdesktopVersionFull, fullName);
            }
            else
            {
                descriptor.AfsPath = string.Format(@"{0}\{1}{3}\{2}.dll", _msdesktopAssemblyLocation,
                                                   folderName ?? fullName, fullName, PathUtilities.GetExecutableRelativePath());

            }
            resolver.AssemblyDescriptors[assembly] = descriptor;
        }

        private static void AddMSDesktopRedirectResolver2(SmartApiResolver resolver, ExternalAssembly assembly, string fullName, string versionInfo, string localFolderName, string remoteFolderName)
        {
            var descriptor = new ExternalAssemblyDescriptor
            {
                Name =
                    new AssemblyName(
                        String.Format(
                            "{1}, {0}",
                            versionInfo, fullName))
            };
            if (File.Exists(fullName + ".dll"))
            {
                descriptor.AfsPath = fullName + ".dll";
            }
            else if (_msdesktopVersion != NullVersion)
            {
                descriptor.AfsPath = String.Format(@"\\san01b\DevAppsGML\dist\msdotnet\PROJ\msgui\{0}\assemblies\{2}{1}.dll",
                                                   _msdesktopVersionFull, fullName, remoteFolderName);
            }
            else
            {
                descriptor.AfsPath = string.Format(@"{0}\{1}{2}.dll", _msdesktopAssemblyLocation,
                                                   localFolderName, fullName);

            }
            resolver.AssemblyDescriptors[assembly] = descriptor;
        }
    }
}
