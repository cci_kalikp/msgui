﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using Image = System.Drawing.Image;

namespace MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging
{

    public class ViewMetaInfo : IEquatable<ViewMetaInfo>
    {
        private readonly ApplicationInfo _application;

        internal event EventHandler ScreenShotReady;

        internal ViewMetaInfo(ApplicationInfo applicationInfo, string id, IViewContainerBase content)
        {
            LocalId = id;
            _application = applicationInfo;
            ViewContainer = content;
        }

        internal ViewMetaInfo(ApplicationInfo applicationInfo, string id, ViewInfoMessage viewInfoMessage)
        {
            _application = applicationInfo;
            LocalId = id;
            ViewContainer = null;
            Title = viewInfoMessage.Title;
            TabName = viewInfoMessage.Tab;
            _screenShot = viewInfoMessage.ScreenShot;
        }

        private Image _screenShot;

        internal bool IsScreenShotReady { get { return _screenShot != null; } }

        public Image ScreenShot
        {
            get
            {
                var thread = new Thread(new ThreadStart(() =>
                                                            {
                                                                var view = ViewContainer as IWindowViewContainer;
                                                                if (_screenShot == null && view != null)
                                                                {
                                                                    _screenShot = GetScreenShot(view);
                                                                }
                                                            }));
                thread.Start();

                //thread.Join();
                
           /*     var task = new Task(() =>
                                          {
                                              var view = ViewContainer as IWindowViewContainer;
                                              if (_screenShot == null && view != null)
                                              {

                                                  _screenShot = GetScreenShot(view);
                                                  var a = 10;
                                              }
                                          }, TaskCreationOptions.LongRunning
                                          
                    );

                task.Start(TaskScheduler.Default);
                task.Wait(TimeSpan.FromSeconds(10));*/

             /*   var view = ViewContainer as IWindowViewContainer;
                if (_screenShot == null && view != null)
                {

                    _screenShot = GetScreenShot(view);
                    var a = 10;
                }*/
                return _screenShot;
            }
        }

        public string LocalId { get; internal set; }

        public string FullId
        {
            get { return Host + ApplicationName + ": " + Instance + LocalId; }
        }


        internal static bool IslocalViewId(string id)
        {
            return id.StartsWith("view");
        }

        internal static string GetLocalId(ApplicationInfo application, string fullId)
        {
            string prefix = application.Host + application.Name + ": " + application.Instance;
            return fullId.StartsWith(prefix) ? fullId.Substring(prefix.Length) : null;
        }

        internal static string GetFullId(ApplicationInfo application, string localId)
        {
            string prefix = application.Host + application.Name + ": " + application.Instance;
            return prefix + localId;
        }

        public string Title { get; internal set; }
        public string TabName { get; set; }

        public string ApplicationName
        {
            get { return _application.Name; }
        }

        public int Instance
        {
            get
            {
                return int.Parse(_application.Instance);
                if (_application.Instance.Contains("_"))
                {
                    return int.Parse(_application.Instance.Split('_')[1]);
                }
                else
                {
                    return int.Parse(_application.Instance);
                }
                
            }
        }

        public string Host
        {
            get { return _application.Host; }
        }

        public ApplicationInfo Application
        {
            get { return _application; }
        }

        internal void UpdateView(ViewMetaInfo viewMetaInfo)
        {
            Debug.Assert(FullId == viewMetaInfo.FullId);
            Debug.Assert(viewMetaInfo.LocalId != null);

            if (viewMetaInfo.TabName != null)
                TabName = viewMetaInfo.TabName;
            if (viewMetaInfo.Title != null)
                Title = viewMetaInfo.Title;

            if (viewMetaInfo._screenShot != null)
                _screenShot = viewMetaInfo._screenShot;

            if (viewMetaInfo.ViewContainer != null)
                ViewContainer = viewMetaInfo.ViewContainer;
        }

        public IViewContainerBase ViewContainer { get; private set; }

        public static bool IsInSameAppInstance(ViewMetaInfo view1, ViewMetaInfo view2)
        {
            return view1.Application.Equals(view2.Application);
        }

        public bool IsInAppliction(ApplicationInfo application)
        {
            return Application.Equals(application);
        }

        public bool IsInSameAppInstance(ViewMetaInfo other)
        {
            return IsInSameAppInstance(this, other);
        }

        public bool Equals(ViewMetaInfo other)
        {
            return FullId.Equals(other.FullId);
        }

        private static Image GetScreenShot(IWindowViewContainer viewContainer)
        {
            var control = viewContainer.Content as UIElement;

            Image image = null;
            if (control != null)
            {
                image = control.GetScreenShotSync(false);
            }

            return image;
        }
    }
}
