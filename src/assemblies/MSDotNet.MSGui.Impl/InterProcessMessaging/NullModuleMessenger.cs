﻿using System;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;

namespace MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging
{
    internal sealed class NullModuleMessenger: IModuleMessenger
    {
        public event EventHandler<ModuleMessageEventArgs> MessageReceived;
        public void PublishToExternal<TMessage>(TMessage content, CommunicationTargetFilter targetFilter, params string[] location)
        {
        }
    }
}
