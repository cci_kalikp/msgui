﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;

namespace MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging
{
    internal class ExternalViewManger: IDisposable
    {
        private readonly HashSet<ViewMetaInfo> _externalViews = new HashSet<ViewMetaInfo>();
        private readonly InterProcessMessenger _ipcMessenger;

        private int _updatedCount;

        public IEnumerable<ViewMetaInfo> ExternalViews { get { return _externalViews; } }

        internal ExternalViewManger(InterProcessMessenger ipcMessenger)
        {
            _ipcMessenger = ipcMessenger;

            _ipcMessenger.ExternalViewAdded += AddViewHandler;
            _ipcMessenger.ExternalViewRemoved += RemoveViewHandler;
            _ipcMessenger.ExternalViewUpdated += UpdateViewHandler;
            _ipcMessenger.ExternalApplicationClosed += RemoveApplicationHandler;

        }

        void IDisposable.Dispose()
        {
            _ipcMessenger.ExternalViewAdded -= AddViewHandler;
            _ipcMessenger.ExternalViewRemoved -= RemoveViewHandler;
            _ipcMessenger.ExternalViewUpdated -= UpdateViewHandler;
            _ipcMessenger.ExternalApplicationClosed -= RemoveApplicationHandler;
        }

        private void AddViewHandler(object sender, InterProcessMessenger.ViewEventArgs args)
        {
            AddView(args.ViewInfo);
        }

        private void RemoveViewHandler(object sender, InterProcessMessenger.ViewEventArgs args)
        {
            RemoveView(args.ViewInfo);
        }

        private void UpdateViewHandler(object sender, InterProcessMessenger.ViewEventArgs args)
        {
            UpdateView(args.ViewInfo);
        }

        private void AddView(ViewMetaInfo viewMetaInfo)
        {
            var old = _externalViews.FirstOrDefault(v => v.FullId == viewMetaInfo.FullId);
            if(old != null)
            {
                
                Debug.Assert(viewMetaInfo.IsInSameAppInstance(old));
                old.UpdateView(viewMetaInfo);
                return;
            }
            _externalViews.Add(viewMetaInfo);
        }

        private void RemoveView(ViewMetaInfo viewMetaInfo)
        {
            var view = _externalViews.FirstOrDefault(v =>  v.FullId == viewMetaInfo.FullId);
            if (view == null)
            {
                Trace.WriteLine("Remove external view: View not existed" + viewMetaInfo.FullId + ": " + viewMetaInfo.Title);
                return;
            }
                
            Debug.Assert(view.IsInSameAppInstance(viewMetaInfo));
            _externalViews.Remove(view);
        }

        private void UpdateView(ViewMetaInfo viewMetaInfo)
        {
            var view = _externalViews.FirstOrDefault(v => v.FullId == viewMetaInfo.FullId);
            if (view == null)
            {
                AddView(viewMetaInfo);
            }
            else
            {
                ++_updatedCount;
                view.UpdateView(viewMetaInfo);
            }
        }

        private void RemoveApplicationHandler(object sender, InterProcessMessenger.ExternalApplicationEventArgs args)
        {
            RemoveApplication(args.Application);
        }

        private void RemoveApplication(ApplicationInfo application)
        {
            _externalViews.RemoveWhere(v => v.Host == application.Host && v.ApplicationName == application.Name && v.Instance.ToString() == application.Instance);
        }

        public ViewMetaInfo GetViewFromId(string fullId)
        {
            return ExternalViews.FirstOrDefault(view => view.FullId == fullId);
        }


        internal void Outdatedall()
        {
            _updatedCount = 0;

        }
        
        internal bool IsAllUpdated
        {
            get { return _updatedCount >= _externalViews.Count(); }
        }
    }
}
