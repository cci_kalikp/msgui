﻿using System;
using System.Linq;
using System.Reflection;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication;

namespace MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging
{
    internal static class MessagingReflectionUtilities
    {
        public static object GetSubscriber(ICommunicator communicator, Type messageType, object handler)
        {
            var getSubscriberMethod = communicator.GetType().GetMethod("GetSubscriber");
            getSubscriberMethod = getSubscriberMethod.MakeGenericMethod(messageType);
            var subscriber = getSubscriberMethod.Invoke(communicator, new object[] { });
            var observable = subscriber.GetType().GetMethod("GetObservable").Invoke(subscriber, new object[] { });

            var subscribeMethod =
                typeof(ObservableExtensions).GetMethods()
                                             .First(m => m.Name.Equals("Subscribe") && m.GetParameters().Count() == 2);

            subscribeMethod = subscribeMethod.MakeGenericMethod(messageType);
            return subscribeMethod.Invoke(observable, new[] { observable, handler });
        }

        public static void PublishFromIsolationHost(object publisher, object message)
        {
            var type = message.GetType();
            var genericType = typeof (ModulePublisher<>).MakeGenericType(new[] {type});
            var method = genericType.GetMethod(
                "PublishFromIsolationHost",
                BindingFlags.Instance | BindingFlags.NonPublic,
                null,
                new[] {type, typeof (CommunicationTargetFilter)},
                new ParameterModifier[] {});

            method.Invoke(publisher, new[]
                    {
                        message,
                        CommunicationTargetFilter.All,
                    });
        }

        public static void Publish(object publisher, object message)
        {
            var type = message.GetType();
            typeof(IModulePublisher<>)
                .MakeGenericType(new[] { type })
                .GetMethod("Publish", new[] { type, typeof(CommunicationTargetFilter), typeof(string[]) })
                .Invoke(publisher, new[]
                    {
                        message,
                        CommunicationTargetFilter.All,
                        new string[] {}
                    });
        }

        public static object GetPublisher(ICommunicator communicator, Type messageType)
        {
            var getPublisherMethod = communicator.GetType().GetMethod("GetPublisher");
            getPublisherMethod = getPublisherMethod.MakeGenericMethod(new[] { messageType });
            var result = getPublisherMethod.Invoke(communicator, new object[] { });
            return result;
        }
    }
}
