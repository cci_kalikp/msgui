﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;

namespace MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging
{
    internal sealed class ExternalViewsProxy
    {
        private readonly HashSet<string> _viewIds = new HashSet<string>();
        private readonly EventRegistrator _eventRegistrator;
        private readonly InterProcessMessenger _interProcessMessenger;
        private const string ProxyPrefix = "Proxy_";

        internal ExternalViewsProxy(EventRegistrator eventRegistrator, InterProcessMessenger interProcessMessenger)
        {
            _eventRegistrator = eventRegistrator;
            _interProcessMessenger = interProcessMessenger;
        }

        private string EncodeProxyId(string localViewId)
        {
            return ProxyPrefix + localViewId;
        }

        internal void ReigsterLocalPublisher<TMessage>(string localViewId)
        {
            var sub = _eventRegistrator.RegisterSubscriber<TMessage>(EncodeProxyId(localViewId));
            var pub = _eventRegistrator.GetRegisteredPublisher<TMessage>(localViewId);

            ((ILocationHolder)pub).Application = new CafGuiApplicationInfoProxy(_interProcessMessenger.Application);
            ((ILocationHolder)sub).Application = new CafGuiApplicationInfoProxy(_interProcessMessenger.Application);

            _eventRegistrator.Connect<TMessage, TMessage>(localViewId, EncodeProxyId(localViewId));
            sub.GetObservable().Subscribe(msg => SendMessageToExternal(localViewId, msg));
        }

        internal void UnregisterLocalPublisher<TMessage>(string localViewId)
        {
            _eventRegistrator.UnregisterSubscriber<TMessage>(EncodeProxyId(localViewId));
        }

        internal void ReigsterLocalSubscriber<TMessage>(string localViewId)
        {
            var sub = _eventRegistrator.GetRegisteredSubscriber<TMessage>(localViewId);
            ((ILocationHolder)sub).Application = new CafGuiApplicationInfoProxy(_interProcessMessenger.Application);
        }

        internal void UnregisterLocalSubscriber<TMessage>(string localViewId)
        {
            // _eventRegistrator.UnregisterPublisher<TMessage>(EncodeProxyId(localViewId));
        }

        internal IEnumerable<string> GetViews(ApplicationInfo application)
        {
            return _viewIds
                .Where(id => ViewMetaInfo.GetLocalId(application, id) != null);
        }

        #region

        internal void OnExternalApplicationClosed(object sender, InterProcessMessenger.ExternalApplicationEventArgs args)
        {
            var app = args.Application;

            foreach (var view in GetViews(app).ToList())
            {
                RemoveView(view);
            }
        }

        internal void OnExternalViewAdded(object sender, InterProcessMessenger.ViewEventArgs args)
        {
            AddView(args.ViewInfo.FullId);
        }

        internal void OnExternalViewRemoved(object sender, InterProcessMessenger.ViewEventArgs args)
        {
            RemoveView(args.ViewInfo.FullId);
        }

        internal void OnExternalViewUpdated(object sender, InterProcessMessenger.ViewEventArgs args)
        {
            AddView(args.ViewInfo.FullId);
        }

        internal void OnExternalViewRegister(object sender, InterProcessMessenger.ViewRegistrationEventArgs args)
        {
            if (!_viewIds.Contains(args.View.FullId))
                AddView(args.View.FullId);

            ILocationHolder locationHolder;
            if (args.AsPublisher)
            {
                locationHolder = RegisterPublisher(args.MessageType, args.View.FullId) as ILocationHolder;
            }
            else
            {
                locationHolder = RegisterSubscriber(args.MessageType, args.View.FullId) as ILocationHolder;
            }

            locationHolder.Application = new CafGuiApplicationInfoProxy(args.ExternalApplication);
        }

        internal void OnViewUnregister(object sender, InterProcessMessenger.ViewRegistrationEventArgs args)
        {
            if (args.AsPublisher)
            {
                UnregisterPublisher(args.MessageType, args.View.FullId);
            }
            else
            {
                UnregisterSubscriber(args.MessageType, args.View.FullId);
            }
        }

        #endregion

        internal bool IsActiveExternalView(string id)
        {

            if (!_viewIds.Contains(id))
                return false;

            return _eventRegistrator.GetPublishedTypes(id).Count() > 0 ||
                   _eventRegistrator.GetSubscribedTypes(id).Count() > 0;
        }

        private void AddView(string id)
        {
            _viewIds.Add(id);
        }

        /// <summary>
        /// DO NOT DELETE - INVOKED VIA REFLECTION
        /// </summary>
        private void UnregisterExternalSubscriber<TMessage>(string viewId)
        {
            var subscriber = _eventRegistrator.GetRegisteredSubscriber<TMessage>(viewId);
            if (subscriber != null)
                _eventRegistrator.UnregisterSubscriber(subscriber);
        }

        /// <summary>
        /// DO NOT DELETE - INVOKED VIA REFLECTION
        /// </summary>
        private void UnregisterExternalPublisher<TMessage>(string viewId)
        {

            var publisher = _eventRegistrator.GetRegisteredPublisher<TMessage>(viewId);
            if (publisher != null)
                _eventRegistrator.UnregisterPublisher(publisher);
        }

        private void RemoveView(string id)
        {
            if (!_viewIds.Contains(id))
                return;


            var methodInfo = GetType()
                .GetMethod("UnregisterExternalPublisher", BindingFlags.NonPublic | BindingFlags.Instance);
            foreach (var pubType in _eventRegistrator.GetPublishedTypes(id))
            {
                var method = methodInfo.MakeGenericMethod(new [] {pubType});
                method.Invoke(this, new object[] {id});
            }

            methodInfo = GetType()
                .GetMethod("UnregisterExternalSubscriber", BindingFlags.NonPublic | BindingFlags.Instance);
            foreach (var subType in _eventRegistrator.GetSubscribedTypes(id))
            {
                var method = methodInfo.MakeGenericMethod(new [] {subType});
                method.Invoke(this, new object[] {id});
            }


            _viewIds.Remove(id);
        }

        public void SendMessageToLocal<TMessage>(string externalId, TMessage message)
        {
            //var proxyId = EncodeProxyId(localViewId);
            var publisher = _eventRegistrator.GetRegisteredPublisher<TMessage>(externalId);
            if (publisher == null)
                return;
            publisher.Publish(message);
        }

        /// <summary>
        /// DO NOT DELETE - INVOKED VIA REFLECTION
        /// </summary>
        private object RegisterPublisher<TMessage>(string id)
        {
            var publihser = _eventRegistrator.GetRegisteredPublisher<TMessage>(id);
            if (publihser == null)
            {
                publihser = _eventRegistrator.RegisterPublisher<TMessage>(id);
            }
            return publihser;
        }

        private object RegisterPublisher(Type msgType, string id)
        {
            var methodinfo = GetType()
                .GetMethod("RegisterPublisher", BindingFlags.NonPublic | BindingFlags.Instance, null,
                           new[] {typeof (string)}, null);
            var method = methodinfo.MakeGenericMethod(new Type[] {msgType});
            return method.Invoke(this, new object[] {id});
        }

        /// <summary>
        /// DO NOT DELETE - INVOKED VIA REFLECTION
        /// </summary>
        private void UnregisterPublisher<TMessage>(string id)
        {
            _eventRegistrator.UnregisterPublisher<TMessage>(id);
        }

        private void UnregisterPublisher(Type msgType, string id)
        {
            var methodinfo = this.GetType()
                                 .GetMethod("UnregisterPublisher", BindingFlags.NonPublic | BindingFlags.Instance, null,
                                            new Type[] {typeof (string)}, null);
            var method = methodinfo.MakeGenericMethod(new Type[] {msgType});
            method.Invoke(this, new object[] {id});
        }

        /// <summary>
        /// DO NOT DELETE - INVOKED VIA REFLECTION
        /// </summary>
        private object RegisterSubscriber<TMessage>(string id)
        {
            var subscriber = _eventRegistrator.GetRegisteredSubscriber<TMessage>(id);
            if (subscriber == null)
                subscriber = _eventRegistrator.RegisterSubscriber<TMessage>(id);

            return subscriber;
        }


        private object RegisterSubscriber(Type msgType, string id)
        {
            var methodinfo = GetType()
                .GetMethod("RegisterSubscriber", BindingFlags.NonPublic | BindingFlags.Instance, null,
                           new Type[] {typeof (string)}, null);
            var method = methodinfo.MakeGenericMethod(new Type[] {msgType});
            return method.Invoke(this, new object[] {id});
        }


        /// <summary>
        /// DO NOT DELETE - INVOKED VIA REFLECTION
        /// </summary>
        private void UnregisterSubscriber<TMessage>(string id)
        {
            _eventRegistrator.UnregisterSubscriber<TMessage>(id);
        }

        private void UnregisterSubscriber(Type msgType, string id)
        {
            var methodinfo = GetType()
                .GetMethod("UnregisterSubscriber", BindingFlags.NonPublic | BindingFlags.Instance, null,
                           new[] {typeof (string)}, null);
            var method = methodinfo.MakeGenericMethod(new[] {msgType});
            method.Invoke(this, new object[] {id});
        }

        private void SendMessageToExternal(string id, object message)
        {
            _interProcessMessenger.PublishToExternal(id, message);
        }


        internal IEnumerable<string> GetCompatibleSubscribers(Type type)
        {
            var methodInfo = _eventRegistrator.GetType().GetMethod("GetCompatibleSubscribers");
            var method = methodInfo.MakeGenericMethod(new[] {type});
            var subIds = (IEnumerable<string>) method.Invoke(_eventRegistrator, new object[] {});

            return subIds.Where(id => _viewIds.Contains(id)).Distinct();
        }

        internal IEnumerable<string> GetCompatiblePublihsers(Type type)
        {
            var methodInfo = _eventRegistrator.GetType().GetMethod("GetCompatiblePublishers");
            var method = methodInfo.MakeGenericMethod(new[] {type});
            var subIds = (IEnumerable<string>) method.Invoke(_eventRegistrator, new object[] {});

            return subIds.Where(id => _viewIds.Contains(id)).Distinct();
        }

    }
}
