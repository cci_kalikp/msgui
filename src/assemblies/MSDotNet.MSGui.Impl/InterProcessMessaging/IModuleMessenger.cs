﻿using System;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;

namespace MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging
{
    internal interface IModuleMessenger
    {
        event EventHandler<ModuleMessageEventArgs> MessageReceived;
        void PublishToExternal<TMessage>(TMessage content, CommunicationTargetFilter targetFilter, params string[] location);
    }
}
