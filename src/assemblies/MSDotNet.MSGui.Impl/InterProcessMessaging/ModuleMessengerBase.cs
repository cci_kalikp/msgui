﻿using System;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;

namespace MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging
{
    internal abstract class ModuleMessengerBase : IDisposable, IModuleMessenger
    {
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~ModuleMessengerBase()
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
        }

        public event EventHandler<ModuleMessageEventArgs> MessageReceived;

        public abstract void PublishToExternal<TMessage>(TMessage content,
                                                         CommunicationTargetFilter targetFilter,
                                                         params string[] location);

        protected void InvokeMessageReceived(Type msgType, object content)
        {
            var copy = MessageReceived;
            if (copy != null)
            {
                var msgArgs = new ModuleMessageEventArgs(msgType, content);
                copy(this, msgArgs);
            }
        }
    }
}
