﻿using System;
using System.Drawing;

namespace MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging
{
    [Serializable]
    internal struct ViewInfoMessage
    {
        private readonly string _title;
        private readonly string _tab;
        private readonly Image _screenShot;
        private string _id;

        public string ID { get { return _id; }}
        public string Title{get { return _title; }}
        public string Tab { get { return _tab; } }
        public Image ScreenShot{get { return _screenShot; }}

        public ViewInfoMessage(string id, string title, string tab, Image screenshot = null)
        {
            _title = title;
            _tab = tab;
            _id = id;
            _screenShot = screenshot;
        }

        public ViewInfoMessage(ViewMetaInfo viewMetaInfo)
        {
            _title = viewMetaInfo.Title;
            _tab = viewMetaInfo.TabName;
            _id = viewMetaInfo.LocalId;
            _screenShot = null;
            _screenShot = viewMetaInfo.ScreenShot;
        }
    }
}
