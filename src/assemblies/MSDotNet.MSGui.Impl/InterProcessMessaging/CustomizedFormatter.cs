﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting.Messaging;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization.Formatters.Binary;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;
using MessageAttribute = MorganStanley.MSDotNet.MSGui.Core.Messaging.MessageAttribute;

namespace MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging
{
    internal sealed class CustomizedFormatter : IFormatter
    {
        private readonly IRemotingFormatter _formatter;
        private readonly IMessageTypeRegistrator _typeRegistrator;

        public CustomizedFormatter(IMessageTypeRegistrator messageTypeRegistrator)
        {
            _typeRegistrator = messageTypeRegistrator;
            _formatter = new BinaryFormatter() {AssemblyFormat = FormatterAssemblyStyle.Simple};
        }


        public object Deserialize(Stream serializationStream)
        {
            var res = _formatter.Deserialize(serializationStream);

            var messageWrapper = res as MessageWrapper;
            if (messageWrapper != null)
            {
                return messageWrapper.GetContent(_typeRegistrator);
            }

            return res;
        }

        public void Serialize(Stream serializationStream, object graph)
        {
            var msg = new MessageWrapper(graph);
            _formatter.Serialize(serializationStream, msg);
        }

        public ISurrogateSelector SurrogateSelector
        {
            get { return _formatter.SurrogateSelector; }
            set { _formatter.SurrogateSelector = value; }
        }

        public SerializationBinder Binder
        {
            get { return _formatter.Binder; }
            set { _formatter.Binder = value; }
        }

        public StreamingContext Context
        {
            get { return _formatter.Context; }
            set { _formatter.Context = value; }
        }


        [Serializable]
        private sealed class MessageWrapper
        {
            private readonly string _contentTypeId;
            private readonly object _content;

            private static string GetTypeId(Type msgType)
            {

                if (!msgType.IsSerializable)
                    return null;

                if (msgType.GetInterface(typeof(ISerializable).Name) == typeof(ISerializable))
                    return null;


                var meta =
                    msgType.GetCustomAttributes(typeof(MessageAttribute), false).FirstOrDefault() as MessageAttribute;


                if (meta != null && !meta.StrongTyped)
                    return meta.Id;

                return null;
            }

            public MessageWrapper(object obj)
            {
                if (obj == null)
                {
                    _content = null;
                    return;
                }

                var msgType = obj.GetType();

                _contentTypeId = GetTypeId(msgType);

                if (_contentTypeId == null)
                {
                    _content = obj;
                    return;
                }

                _content = new Dictionary<string, object>();
                var dict = _content as IDictionary<string, object>;

                foreach (var propertyInfo in msgType.GetProperties(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public))
                {
                    if (propertyInfo.CanRead && propertyInfo.GetIndexParameters().Length == 0)
                    {
                        var value = propertyInfo.GetValue(obj,
                                                          BindingFlags.Instance | BindingFlags.NonPublic |
                                                          BindingFlags.Public,
                                                          null, null, null);

                        if (GetTypeId(propertyInfo.PropertyType) != null)
                        {
                            dict[propertyInfo.Name] =
                                new MessageWrapper(value);
                        }
                        else
                        {
                            dict[propertyInfo.Name] = value;
                        }
                    }

                }

                foreach (var fieldInfo in msgType.GetFields(BindingFlags.Instance | BindingFlags.NonPublic |
                                                                     BindingFlags.Public))
                {
                    dict[fieldInfo.Name] = new MessageWrapper(fieldInfo.GetValue(obj));
                }
            }


            public object GetContent(IMessageTypeRegistrator messageTypeRegistrator)
            {
                if (_contentTypeId == null)
                    return _content;

                var msgType = messageTypeRegistrator.ResolveType(_contentTypeId);
                var instance = msgType.GetConstructor(new Type[] { }).Invoke(new object[] { });
                var dict = (IDictionary<string, object>)_content;
                foreach (var pair in dict)
                {

                    var value = pair.Value is MessageWrapper
                                    ? ((MessageWrapper)pair.Value).GetContent(messageTypeRegistrator)
                                    : pair.Value;

                    var propInfo = msgType.GetProperty(pair.Key,
                                        BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                    if (propInfo != null && propInfo.CanWrite)
                    {
                        propInfo.SetValue(instance, value,
                                 BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public, null,
                                 null, null);

                    }
                    else
                    {
                        var fieldInfo = msgType.GetField(pair.Key,
                                                         BindingFlags.Instance | BindingFlags.NonPublic |
                                                         BindingFlags.Public);
                        if (fieldInfo != null)
                        {
                            fieldInfo.SetValue(instance, value,
                                               BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public,
                                               null, null);
                        }
                    }

                }

                return instance;
            }
        }
    }
}
