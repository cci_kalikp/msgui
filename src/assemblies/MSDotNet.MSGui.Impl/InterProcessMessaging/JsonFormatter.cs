﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging
{
    class JsonFormatter : IFormatter
    {
        public object Deserialize(Stream serializationStream)
        {
            var serializer = new JsonSerializer();

            using (var sr = new StreamReader(serializationStream))
            using (var jsonTextReader = new JsonTextReader(sr))
            {
                var result = serializer.Deserialize(jsonTextReader);
                return result;
            }
        }

        public void Serialize(Stream serializationStream, object graph)
        {
            var buffer = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(graph));
            serializationStream.Write(buffer, 0, buffer.Count());
        }

        public ISurrogateSelector SurrogateSelector { get; set; }
        public SerializationBinder Binder { get; set; }
        public StreamingContext Context { get; set; }

        public object Deserialize(MemoryStream serializationStream, string messageType, string messageAssembly)
        {
            var serializer = new JsonSerializer();
            var type = Type.GetType(messageType + ", " + messageAssembly);

            if (type != null)
            {
                using (var sr = new StreamReader(serializationStream))
                using (var jsonTextReader = new JsonTextReader(sr))
                {
                    var result = serializer.Deserialize(jsonTextReader, type);
                    return result;
                }
            }

            return null;
        }
    }
}
