﻿
namespace MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging
{
    internal class ExternalMessengerBase 
    {
        internal enum ConnectionAction
        {
            Connect, Disconnect
        }

        internal enum MetaAction
        {
            RegisterPublisher, UnregisterPublisher, RegisterSubscriber, UnregisterSubscriber,
            Connect, Disconnect,
            ViewAdded, ViewRemoved, ViewUpdated,
            RequestAllViews,
            ApplicationClosed
        }
    }
}
