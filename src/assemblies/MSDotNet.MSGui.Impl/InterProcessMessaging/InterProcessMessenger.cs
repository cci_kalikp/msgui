﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;
using MSDesktop.Isolation.Interfaces;

namespace MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging
{
    internal sealed class InterProcessMessenger : IEventRegistrator, IDisposable
    {
        private readonly ExternalViewsProxy _externalViewsProxy;
        private readonly ExternalViewManger _externalViewManager;
        private readonly EventRegistrator _eventRegistrator;
        private readonly ApplicationInfo _applicationInfo;
        private readonly IUnityContainer _unityContainer;

        private IExternalCommunicator _fusionMessenger;
        private IExternalCommunicator _cpsMessenger;

        internal  static bool IsVirtualDesktopSupported = true;

        private readonly object _viewUpdateLock = new object();

        public InterProcessMessenger(IAdapterService adapterService, IApplication application,
                                     IMSDesktopEventAggregator eventAggregator, IMessageTypeRegistrator typeRegistrator,
                                     IUnityContainer container)
        {
            _unityContainer = container;
            var appName = application.Name;
            var instance = application.GetApplicationInstance();

            string desktopName;
            if(IsVirtualDesktopSupported)
                desktopName = DesktopHelper.GetCurrentDesktopName();
            else
            {
                desktopName = "MSDesktop";
            }

            _applicationInfo = new ApplicationInfo(Environment.MachineName, Environment.UserName, desktopName, appName,
                                                   instance);

            var msDesktopThreadedApplication = application as IMSDesktopThreadedApplication;
            var dispatcher = msDesktopThreadedApplication != null
                                 ? msDesktopThreadedApplication.MainWindowDispatcher
                                 : System.Windows.Application.Current.MainWindow.Dispatcher;

            _eventRegistrator = new EventRegistrator(adapterService, eventAggregator, typeRegistrator, dispatcher);
            _unityContainer.RegisterInstance(_eventRegistrator);
            _externalViewsProxy = new ExternalViewsProxy(_eventRegistrator, this);
            _externalViewManager = new ExternalViewManger(this);

            var nullMessenger = new NullExternalCommunicator();

            _fusionMessenger = nullMessenger;
            _cpsMessenger = nullMessenger;

            //_cpsMessenger = new NullExternalCommunicator();

            Initialize();
        }

        public void EnableIpcImc()
        {
            if (!(_fusionMessenger is FusionMessaging.FusionMessenger))
            {
                var fussion = new FusionMessaging.FusionMessenger(_applicationInfo, this);
                if (fussion.Initialize())
                    _fusionMessenger = fussion;  
              
            }

            if (!(_cpsMessenger is CPSMessaging.CpsMessenger))
            {
                var cps = new CPSMessaging.CpsMessenger(_unityContainer, this, _applicationInfo);
                if (cps.Initialize())
                    _cpsMessenger = cps;    
            }
            
        }

        public void DisableIpcImc()
        {
            var nullMessenger = new NullExternalCommunicator();

            _fusionMessenger = nullMessenger;
            _cpsMessenger = nullMessenger;
        }

        internal ExternalViewsProxy ExternalViewsProxy { get { return _externalViewsProxy; } }
        internal ExternalViewManger ExternalViewManager { get { return _externalViewManager; } }

        private void Initialize()
        {
            ExternalApplicationClosed += _externalViewsProxy.OnExternalApplicationClosed;
            ExternalViewAdded += _externalViewsProxy.OnExternalViewAdded;
            ExternalViewRemoved += _externalViewsProxy.OnExternalViewRemoved;
            ExternalViewUpdated += _externalViewsProxy.OnExternalViewUpdated;
            ExternalViewRegisterred += _externalViewsProxy.OnExternalViewRegister;
            ExternalViewUnregisterred += _externalViewsProxy.OnViewUnregister;
            ExternalViewConnected += ConnectedByExternalHandler;
            ExternalViewDisconnected += DisconnectedByExternalHandler;
            //this.AllViewInfoRequested += AllViewRequestHandler;

        }

        void IDisposable.Dispose()
        {
            ExternalApplicationClosed -= _externalViewsProxy.OnExternalApplicationClosed;
            ExternalViewAdded -= _externalViewsProxy.OnExternalViewAdded;
            ExternalViewRemoved -= _externalViewsProxy.OnExternalViewRemoved;
            ExternalViewUpdated -= _externalViewsProxy.OnExternalViewUpdated;
            ExternalViewRegisterred -= _externalViewsProxy.OnExternalViewRegister;
            ExternalViewUnregisterred -= _externalViewsProxy.OnViewUnregister;
            ExternalViewConnected -= ConnectedByExternalHandler;
            ExternalViewDisconnected -= DisconnectedByExternalHandler;
        }

        #region  Handle External Events
        public event EventHandler<ViewRegistrationEventArgs> ExternalViewRegisterred;
        public event EventHandler<ViewRegistrationEventArgs> ExternalViewUnregisterred;

        public event EventHandler<ViewConnectionEventArgs> ExternalViewConnected;
        public event EventHandler<ViewConnectionEventArgs> ExternalViewDisconnected;

        public event EventHandler<ViewEventArgs> ExternalViewAdded;
        public event EventHandler<ViewEventArgs> ExternalViewRemoved;
        public event EventHandler<ViewEventArgs> ExternalViewUpdated;

        public event EventHandler<ExternalApplicationEventArgs> ExternalApplicationClosed;

        public event EventHandler<EventArgs> AllViewInfoRequested;

        internal string ApplicationName { get { return _applicationInfo.Name; } }
        internal int Instance { get { return int.Parse(_applicationInfo.Instance); } }
        internal string Host { get { return _applicationInfo.Host; } }

        internal ApplicationInfo Application { get { return _applicationInfo; } }



        private void InvokeExternalViewRegisterred(ApplicationInfo extApp, ViewMetaInfo externalView, Type type, bool asPublisher)
        {
            var copy = ExternalViewRegisterred;
            if (copy != null)
            {
                copy(this, new ViewRegistrationEventArgs
                               {
                                   ExternalApplication = extApp,
                                   View = externalView,
                                   MessageType = type,
                                   AsPublisher = asPublisher
                               });
            }
        }

        private void InvokeExternalViewUnregisterred(ViewMetaInfo externalView, Type type, bool asPublisher)
        {
            var copy = ExternalViewUnregisterred;
            if (copy != null)
            {
                copy(this, new ViewRegistrationEventArgs
                               {
                                   View = externalView,
                                   MessageType = type,
                                   AsPublisher = asPublisher
                               });
            }
        }

        private void InvokeExternalViewAdded(ViewMetaInfo viewMetaInfo)
        {
            var copy = ExternalViewAdded;
            if (copy != null)
            {
                copy(this, new ViewEventArgs { ViewInfo = viewMetaInfo });
            }
        }

        private void InvokeExternalViewRemoved(ViewMetaInfo viewMetaInfo)
        {
            var copy = ExternalViewRemoved;
            if (copy != null)
            {
                copy(this, new ViewEventArgs { ViewInfo = viewMetaInfo });
            }
        }

        private void InvokeExternalViewUpdated(ViewMetaInfo viewMetaInfo)
        {
            var copy = ExternalViewUpdated;
            if (copy != null)
            {
                copy(this, new ViewEventArgs { ViewInfo = viewMetaInfo });
            }
        }

        private void InvokeExternalViewConnected(ViewMetaInfo externalView, Type externalMsgType, ViewMetaInfo  localView, Type localMsgType, bool externalAsPublisher)
        {
            var copy = ExternalViewConnected;
            if (copy != null)
            {
                copy(this, new ViewConnectionEventArgs
                               {
                                   ExternalView = externalView ,
                                   //ExternalId = externalView,
                                   ExternalType = externalMsgType,
                                   //LocalId = localView,
                                   LocalView = localView,
                                   LocalType = localMsgType,
                                   ExternalAsPublisher = externalAsPublisher
                               });
            }
        }

        private void InvokeExternalViewDisConnected(ViewMetaInfo externalView, Type externalMsgType, ViewMetaInfo localView, Type localMsgType, bool externalAsPublisher)
        {
            var copy = ExternalViewDisconnected;
            if (copy != null)
            {
                copy(this, new ViewConnectionEventArgs
                {
                    //ExternalId = externalView,
                    ExternalView = externalView,
                    ExternalType = externalMsgType,
                    //LocalId = localView,
                    LocalView =  localView,
                    LocalType = localMsgType,
                    ExternalAsPublisher = externalAsPublisher
                });
            }
        }

        private void InvokeAllViewInfoRequested()
        {
            var copy = AllViewInfoRequested;
            if (copy != null)
            {
                copy(this, new EventArgs());
            }
        }

        private void InvokeExternalApplicationClosed(object sender, ExternalApplicationEventArgs args)
        {
            var copy = ExternalApplicationClosed;
            if (copy != null)
            {
                copy(sender, args);
            }
        }
        #endregion

        public IAdapterService AdapterService
        {
            get { return _eventRegistrator.AdapterService; }
        }

        internal bool IsEncodedExternal(string id)
        {
            if (ViewMetaInfo.IslocalViewId(id))
                return false;

            return ViewMetaInfo.GetLocalId(Application, id) == null;

        }


        internal string GetLocalId(string id)
        {
            if (ViewMetaInfo.IslocalViewId(id))
                return id;

            return ViewMetaInfo.GetLocalId(Application, id);
        }


        internal void OnExternalEvent(ExternalMessengerBase.MetaAction action, ApplicationInfo sender, ViewMetaInfo extView, Type extMsgType)
        {
            switch (action)
            {

                case ExternalMessengerBase.MetaAction.ApplicationClosed:
                    InvokeExternalApplicationClosed(this, new ExternalApplicationEventArgs
                                                              {
                                                                  Application = sender
                                                              });

                    break;
                case ExternalMessengerBase.MetaAction.RegisterPublisher:
                    if (!_externalViewsProxy.IsActiveExternalView(extView.FullId))
                    {
                        InvokeExternalViewAdded(extView);
                    }
                    
                    InvokeExternalViewRegisterred(sender, extView, extMsgType, true);

                    break;

                case ExternalMessengerBase.MetaAction.UnregisterPublisher:
                    InvokeExternalViewUnregisterred(extView, extMsgType, true);

                    if (!_externalViewsProxy.IsActiveExternalView(extView.FullId))
                    {
                        InvokeExternalViewRemoved(extView);

                    }
                    break;

                case ExternalMessengerBase.MetaAction.RegisterSubscriber:

                    if (!_externalViewsProxy.IsActiveExternalView(extView.FullId))
                    {
                        InvokeExternalViewAdded(extView);
                    }


                    InvokeExternalViewRegisterred(sender, extView, extMsgType, false);

                    break;

                case ExternalMessengerBase.MetaAction.UnregisterSubscriber:
                    InvokeExternalViewUnregisterred(extView, extMsgType, false);

                    if (!_externalViewsProxy.IsActiveExternalView(extView.FullId))
                    {
                        InvokeExternalViewRemoved(extView);

                    }
                    break;

                case ExternalMessengerBase.MetaAction.ViewAdded:
                    InvokeExternalViewAdded(extView);
                    break;

                case ExternalMessengerBase.MetaAction.ViewRemoved:
                    //extView.ID = meta.ExternalId;
                    InvokeExternalViewRemoved(extView);
                    break;

                case ExternalMessengerBase.MetaAction.ViewUpdated:
                    //extView.ID = meta.ExternalId;
                    lock (_viewUpdateLock)
                    {
                        InvokeExternalViewUpdated(extView);
                        Monitor.PulseAll(_viewUpdateLock);
                    }
                    
                    break;

                case ExternalMessengerBase.MetaAction.RequestAllViews:
                    InvokeAllViewInfoRequested();
                    break;

                default:
                    throw new ApplicationException("Unsupported Action" + action);
            }
        }


        internal void OnExternalConnectionEvent(ExternalMessengerBase.ConnectionAction action, ApplicationInfo extApp, Type extMsgType, ViewMetaInfo extView, Type localMsgType, ViewMetaInfo localView, bool extAsPublisher)
        {
            switch (action)
            {
                case ExternalMessengerBase.ConnectionAction.Connect:

                    if (!extAsPublisher && GetPublishedTypes(extView.FullId).Contains(extMsgType))
                    {
                        InvokeExternalViewRegisterred(extApp, extView,extMsgType,true);    
                    }
                    else if (!GetSubscribedTypes(extView.FullId).Contains(extMsgType))
                    {
                        InvokeExternalViewRegisterred(extApp, extView, extMsgType, false);
                    }

                    InvokeExternalViewConnected(extView, extMsgType, localView, localMsgType, extAsPublisher);
                    break;
                case ExternalMessengerBase.ConnectionAction.Disconnect:
                    InvokeExternalViewDisConnected(extView, extMsgType, localView, localMsgType, extAsPublisher);
                    break;
                default:
                    throw new ApplicationException("Action not supported: " + action);
            }

        }

        internal void OnMessageRecieved(string externalViewId, Type msgType, object content)
        {
            var methodInfo = _externalViewsProxy.GetType().GetMethod("SendMessageToLocal");
            var method = methodInfo.MakeGenericMethod(new [] { msgType });
            method.Invoke(_externalViewsProxy, new [] { externalViewId, content });
        }

        public ISubscriber<TMessage> RegisterSubscriber<TMessage>(IViewContainerBase viewContainer)
        {
            return RegisterSubscriber<TMessage>(viewContainer, false);
        }

        private void UnregisterSubscriberNotificationHandler<TMessage>(object sender, EventArgs args)
        {
            var viewContainer = sender as ViewContainerBase;
            if (viewContainer == null)
                return;

            _fusionMessenger.NotifyUnregister(typeof(TMessage), viewContainer.GetId(), false);
            _cpsMessenger.NotifyUnregister(typeof(TMessage), viewContainer.GetId(), false);
            viewContainer.ReleaseResources -= UnregisterSubscriberNotificationHandler<TMessage>;
        }

        public ISubscriber<TMessage> RegisterMarshalingSubscriber<TMessage>(IViewContainerBase viewContainer,
                                                                            object addinHost)
        {
            var subscriber = _eventRegistrator.RegisterMarshalingSubscriber<TMessage>(viewContainer, addinHost);
            _externalViewsProxy.ReigsterLocalSubscriber<TMessage>(viewContainer.GetId());
            return subscriber;
        }

        public IPublisher<TMessage> RegisterMarshalingPublisher<TMessage>(IViewContainerBase viewContainer,
                                                                          object addinHost)
        {
            var publisher = _eventRegistrator.RegisterMarshalingPublisher<TMessage>(viewContainer, addinHost);
            _externalViewsProxy.ReigsterLocalPublisher<TMessage>(viewContainer.GetId());
            return publisher;
        }

        public ISubscriber<TMessage> RegisterSubscriber<TMessage>(IViewContainerBase viewContainer, bool receiveLastMessage)
        {
            var view = (ViewContainerBase)viewContainer;
            view.ReleaseResources += UnregisterSubscriberNotificationHandler<TMessage>;

            //TODO receive last
            _fusionMessenger.NotifyRegister(typeof(TMessage), viewContainer.GetId(), false);
            _cpsMessenger.NotifyRegister(typeof(TMessage), viewContainer.GetId(), false);
 
            var subscriber = _eventRegistrator.RegisterSubscriber<TMessage>(viewContainer, receiveLastMessage);
            _externalViewsProxy.ReigsterLocalSubscriber<TMessage>(viewContainer.GetId());
           
            return subscriber;
        }

        private static void UnregisterPublishNotificationHandler<TMessage>(object sender, EventArgs eventArgs)
        {
            var view = sender as ViewContainerBase;
            if (view == null)
                return;

            view.ReleaseResources -= UnregisterPublishNotificationHandler<TMessage>;
        }

        public bool UnregisterSubscriber<TMessage>(IViewContainerBase viewContainer)
        {
            _fusionMessenger.NotifyUnregister(typeof(TMessage), viewContainer.GetId(), false);
            _cpsMessenger.NotifyUnregister(typeof(TMessage), viewContainer.GetId(), false);

            _externalViewsProxy.UnregisterLocalSubscriber<TMessage>(viewContainer.GetId());
            return _eventRegistrator.UnregisterSubscriber<TMessage>(viewContainer);
        }

        public IPublisher<TMessage> RegisterPublisher<TMessage>(IViewContainerBase viewContainer)
        {
            _fusionMessenger.NotifyRegister(typeof(TMessage), viewContainer.GetId(), true);
            _cpsMessenger.NotifyRegister(typeof(TMessage), viewContainer.GetId(), true);

            var publisher = _eventRegistrator.RegisterPublisher<TMessage>(viewContainer);
            _externalViewsProxy.ReigsterLocalPublisher<TMessage>(viewContainer.GetId());
            var view = (ViewContainerBase)viewContainer;
            view.ReleaseResources += UnregisterPublishNotificationHandler<TMessage>;
           
            return publisher;
        }

        public bool UnregisterPublisher<TMessage>(IViewContainerBase viewContainer)
        {
            var localViewId = viewContainer.GetId();
            _fusionMessenger.NotifyUnregister(typeof(TMessage), localViewId, true);
            _cpsMessenger.NotifyUnregister(typeof(TMessage), localViewId, true);

            _externalViewsProxy.UnregisterLocalPublisher<TMessage>(localViewId);
            return _eventRegistrator.UnregisterPublisher<TMessage>(viewContainer);
        }

        public IEnumerable<string> GetCompatiblePublishers<TMessage>()
        {
            return _eventRegistrator.GetCompatiblePublishers<TMessage>();
        }

        public IEnumerable<string> GetCompatibleSubscribers<TMessage>()
        {
            return _eventRegistrator.GetCompatibleSubscribers<TMessage>();
        }

        public ICollection<Type> GetSubscribedTypes()
        {
            return _eventRegistrator.GetSubscribedTypes();
        }

        public ICollection<Type> GetPublishedTypes()
        {
            return _eventRegistrator.GetPublishedTypes();
        }

        public IEnumerable<Type> GetPublishedTypes(string id)
        {
            return _eventRegistrator.GetPublishedTypes(id);
        }

        public IEnumerable<Type> GetSubscribedTypes(string id)
        {
            return _eventRegistrator.GetSubscribedTypes(id);
        }

        public IPublisher<TMessage> GetRegisteredPublisher<TMessage>(IViewContainerBase viewContainer)
        {
            return _eventRegistrator.GetRegisteredPublisher<TMessage>(viewContainer);
        }

        public ISubscriber<TMessage> GetRegisteredSubscriber<TMessage>(IViewContainerBase viewContainer)
        {
            return _eventRegistrator.GetRegisteredSubscriber<TMessage>(viewContainer);
        }

        internal void Connect<TMessage1, TMessage2>(ViewMetaInfo publishContainer, ViewMetaInfo subscribeContainer)
        {
            _eventRegistrator.Connect<TMessage1, TMessage2>(publishContainer.LocalId, subscribeContainer.LocalId);
        }

        public void Connect<TMessage1, TMessage2>(IViewContainerBase publishContainer, IViewContainerBase subscribeContainer)
        {
            _eventRegistrator.Connect<TMessage1, TMessage2>(publishContainer, subscribeContainer);
        }

        public void Connect<TMessage1, TMessage2>(string publishContainer, string subscribeContainer)
        {
            _eventRegistrator.Connect<TMessage1,TMessage2>(publishContainer, subscribeContainer);
        }

        public void DisconnectFromExternalSubscriber<TPubMessage, TSubMessage>(string publisher, string externalSubscriber)
        {
            if (!_externalViewsProxy.IsActiveExternalView(externalSubscriber))
                throw new ApplicationException(externalSubscriber + " is not an external view");

            var extView = _externalViewManager.GetViewFromId(externalSubscriber);
            if (extView == null)
                throw new ApplicationException(externalSubscriber + " is not an external view");

            _fusionMessenger.NotifyConnection<TPubMessage, TSubMessage>(ExternalMessengerBase.ConnectionAction.Disconnect, publisher, extView, false);
            _cpsMessenger.NotifyConnection<TPubMessage, TSubMessage>(ExternalMessengerBase.ConnectionAction.Disconnect, publisher, extView, false);

            _eventRegistrator.Disconnect<TPubMessage, TSubMessage>(publisher, externalSubscriber);
        }

        public void ConnectToExternalSubscriber<TPubMessage, TSubMessage>(string publisher, string externalSubscriber)
        {
            if (!_externalViewsProxy.IsActiveExternalView(externalSubscriber))
                throw new ApplicationException(externalSubscriber + " is not an external view");

            var extView = _externalViewManager.GetViewFromId(externalSubscriber);
            if(extView == null)
                throw new ApplicationException(externalSubscriber + " is not an external view");

            _fusionMessenger.NotifyConnection<TPubMessage, TSubMessage>(ExternalMessengerBase.ConnectionAction.Connect, publisher, extView, false);
            _cpsMessenger.NotifyConnection<TPubMessage, TSubMessage>(ExternalMessengerBase.ConnectionAction.Connect, publisher, extView, false);
            _eventRegistrator.Connect<TPubMessage, TSubMessage>(publisher, externalSubscriber);
        }

        public void DisconnectFromExternalPublisher<TSubscribeMessage, TPublishMessage>(string subscriber, string externalPublisher)
        {
            if (!_externalViewsProxy.IsActiveExternalView(externalPublisher))
                throw new ApplicationException(externalPublisher + " is not an external view");

            var extView = _externalViewManager.GetViewFromId(externalPublisher);
            if (extView == null)
                throw new ApplicationException(externalPublisher + " is not an external view");

            _fusionMessenger.NotifyConnection<TSubscribeMessage, TPublishMessage>(ExternalMessengerBase.ConnectionAction.Disconnect,
                subscriber, extView, true);

            _cpsMessenger.NotifyConnection<TSubscribeMessage, TPublishMessage>(ExternalMessengerBase.ConnectionAction.Disconnect,
                subscriber, extView, true); 

            _eventRegistrator.Disconnect<TPublishMessage, TSubscribeMessage>(externalPublisher, subscriber);
        }

        public void ConnectToExternalPublisher<TSubscribeMessage, TPublishMessage>(string subscriber, string externalPublisher)
        {
            if (!_externalViewsProxy.IsActiveExternalView(externalPublisher))
                throw new ApplicationException(externalPublisher + " is not an external view");

            var extView = _externalViewManager.GetViewFromId(externalPublisher);
            if (extView == null)
                throw new ApplicationException(externalPublisher + " is not an external view");

            _fusionMessenger.NotifyConnection<TSubscribeMessage, TPublishMessage>(ExternalMessengerBase.ConnectionAction.Connect, subscriber,
                extView, true);

            _cpsMessenger.NotifyConnection<TSubscribeMessage, TPublishMessage>(ExternalMessengerBase.ConnectionAction.Connect, subscriber,
                extView, true); 
            
            _eventRegistrator.Connect<TPublishMessage, TSubscribeMessage>(externalPublisher, subscriber);
        }

        public void Disconnect<TMessage1, TMessage2>(IViewContainerBase publishContainer, IViewContainerBase subscribeContainer)
        {
            _eventRegistrator.Disconnect<TMessage1, TMessage2>(publishContainer, subscribeContainer);
        }

        public void Disconnect<TMessage1, TMessage2>(string publishViewId, string subscribeViewId)
        {
            _eventRegistrator.Disconnect<TMessage1, TMessage2>(publishViewId, subscribeViewId);
        }

        internal void Disconnect<TMessage1, TMessage2>(ViewMetaInfo publishContainer, ViewMetaInfo subscribeContainer)
        {
            _eventRegistrator.Disconnect<TMessage1, TMessage2>(publishContainer.LocalId, subscribeContainer.LocalId);
        }

        internal Dictionary<KeyValuePair<Type, string>, HashSet<KeyValuePair<Type, string>>> GetConnections()
        {
            return _eventRegistrator.GetConnections();
        }

        public bool UnregisterPublisher<TMessage>(IPublisher<TMessage> publisher)
        {
            throw new ApplicationException("Should not use this method with IPC/IMC enabled event registrator.");
        }

        public bool UnregisterSubscriber<TMessage>(ISubscriber<TMessage> subscriber)
        {
            throw new ApplicationException("Should not use this method with IPC/IMC enabled event registrator.");
        }

        internal void PublishToExternal(string viewId, object message)
        {
            _fusionMessenger.PublishToExternal(viewId, message);
            _cpsMessenger.PublishToExternal(viewId, message);
        }

        public class ViewRegistrationEventArgs : EventArgs
        {
            public ApplicationInfo ExternalApplication { get; internal set; }
            public ViewMetaInfo View { get; internal set; }
            public Type MessageType { get; internal set; }
            public bool AsPublisher { get; internal set; }
        }

        public class ViewConnectionEventArgs : EventArgs
        {
            public ViewMetaInfo LocalView { get; internal set; }
            public ViewMetaInfo ExternalView { get; internal set; }

            //public string ExternalId { get; internal set; }
            public Type ExternalType { get; internal set; }
            public bool ExternalAsPublisher { get; internal set; }
            //public string LocalId { get; internal set; }
            public Type LocalType { get; internal set; }
        }

        public class ViewEventArgs : EventArgs
        {
            public ViewMetaInfo ViewInfo { get; internal set; }
        }

        public class ExternalApplicationEventArgs : EventArgs
        {
            public ApplicationInfo Application { get; internal set; }
        }


        private void ConnectByExternalPublisher<TPublishMessage, TSubscribeMessage>(string externalPublisher, string localSubscriber)
        {
            if (_eventRegistrator.GetRegisteredSubscriber<TSubscribeMessage>(localSubscriber) == null)
            {
                return;
            }

            if (_eventRegistrator.GetRegisteredPublisher<TPublishMessage>(externalPublisher) == null)
                return;


            if (!IsEncodedExternal(externalPublisher))
                throw new ApplicationException(externalPublisher + " is not an external view");

            _eventRegistrator.Connect<TPublishMessage, TSubscribeMessage>(externalPublisher, GetLocalId(localSubscriber));
        }

        private void ConnectByExternalSubscriber<TSubscribeMessage, TPublishMessage>(string externalSubscriber, string localPublisher)
        {
            return;
            /*
            if (!IsEncodedExternal(externalSubscriber))
                throw new ApplicationException(externalSubscriber + " is not an external view");

            _eventRegistrator.Connect<TPublishMessage, TSubscribeMessage>(GetLocalId(localPublisher), externalSubscriber);*/
        }

        private void ConnectedByExternalHandler(object sender, ViewConnectionEventArgs args)
        {
            var name = args.ExternalAsPublisher ? "ConnectByExternalPublisher" : "ConnectByExternalSubscriber";
            var methodInfo = GetType().GetMethod(name,
                BindingFlags.NonPublic | BindingFlags.Instance, null, new [] { typeof(string), typeof(string) },
                null);

            var method = methodInfo.MakeGenericMethod(new [] { args.ExternalType, args.LocalType });
            method.Invoke(this, new object[] { args.ExternalView.FullId, args.LocalView.LocalId });
        }



        private void DisconnectByExternalPublisher<TPublishMessage, TSubscribeMessage>(string externalPublisher, string localSubscriber)
        {
            if (!_externalViewsProxy.IsActiveExternalView(externalPublisher))
                throw new ApplicationException(externalPublisher + " is not an external view");
            var locId = GetLocalId(localSubscriber);
            if (locId == null)
                throw new ApplicationException(externalPublisher + " is not an valid internal view");

            _eventRegistrator.Disconnect<TPublishMessage, TSubscribeMessage>(externalPublisher, locId);
        }

        private void DisconnectByExternalSubscriber<TSubscribeMessage, TPublishMessage>(string externalSubscriber, string localPublisher)
        {
            return;
            /*
                        if (!IsEncodedLocal(externalSubscriber))
                            throw new ApplicationException(externalSubscriber + " is not an external view");

                        _eventRegistrator.Disconnect<TPublishMessage, TSubscribeMessage>(localPublisher, externalSubscriber);*/
        }

        private void DisconnectedByExternalHandler(object sender, ViewConnectionEventArgs args)
        {

            var name = args.ExternalAsPublisher ? "DisconnectByExternalPublisher" : "DisconnectByExternalSubscriber";

            var methodInfo = GetType().GetMethod(name,
                BindingFlags.NonPublic | BindingFlags.Instance, null, new [] { typeof(string), typeof(string) },
                null);

            var method = methodInfo.MakeGenericMethod(new [] { args.ExternalType, args.LocalType });
            method.Invoke(this, new object[] { args.ExternalView.FullId, args.LocalView.LocalId });
        }

        #region




        #endregion

        internal ViewMetaInfo GenerateLocalViewInfo(IWindowViewContainer viewContainer, string tab)
        {
            return new ViewMetaInfo(_applicationInfo, viewContainer.ID, viewContainer)
                       {
                           TabName = tab
                       };
        }

        internal void NotifyRegister(Type msgType, string viewId, bool asPbulisher)
        {
            _fusionMessenger.NotifyRegister(msgType, viewId, asPbulisher);
            _cpsMessenger.NotifyRegister(msgType, viewId, asPbulisher);
        }

        internal void NotifyAllViewUpdated(ViewMetaInfo viewInfo)
        {
            _fusionMessenger.NotifyAllViewUpdated(viewInfo);
            _cpsMessenger.NotifyAllViewUpdated(viewInfo);
        }

        internal Task<bool> RequestAllViews()
        {
            _externalViewManager.Outdatedall();

            _fusionMessenger.RequestAllViews();
            _cpsMessenger.RequestAllViews();

            var future = Task<bool>.Factory.StartNew(() =>
                {
                    lock (_viewUpdateLock)
                    {
                        bool isTimeout = false;
                        while (!isTimeout && !_externalViewManager.IsAllUpdated)
                        {
                            isTimeout = !Monitor.Wait(_viewUpdateLock, TimeSpan.FromSeconds(1));
                        }


                        Monitor.PulseAll(_viewUpdateLock);
                    }
                    /* int count = 0;
                                                              while (!_externalViewManager.IsAllUpdated && count < 100)
                                                              {
                                                                  ++count;
                                                                  Thread.Sleep(100);
                                                              }*/
                    return _externalViewManager.IsAllUpdated;
                });

            return future;

        }

        internal void NotifyApplicationClosed()
        {
            _fusionMessenger.NotifyApplicationClosed();
            _cpsMessenger.NotifyApplicationClosed();
        }

        internal void NotifyAllViewUpdated(WindowViewContainer windowViewContainer, string newTab)
        {
            var viewInfo = new ViewMetaInfo(Application, windowViewContainer.ID, windowViewContainer)
                               {
                                   TabName = newTab,
                                   Title = windowViewContainer.Title
                               };

            NotifyAllViewUpdated(viewInfo);
        }


        public IEnumerable<object> GetAllRegisterredPublishers(IViewContainerBase view)
        {
            return _eventRegistrator.GetAllRegisterredPublishers(view);
        }

        public IEnumerable<object> GetAllRegisterredSubscribers(IViewContainerBase view)
        {
            return _eventRegistrator.GetAllRegisterredSubscribers(view);
        }
    }
}
