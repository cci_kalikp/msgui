﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Channels;
using Google.ProtocolBuffers;
using MorganStanley.Desktop.ServiceDiscovery;
using MorganStanley.MSDotNet.Channels;
using MorganStanley.MSDotNet.Channels.Client;
using MorganStanley.MSDotNet.Channels.Cps;
using MorganStanley.MSDotNet.Channels.Raw;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging.IMC.CPS;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;
using MorganStanley.MSDotNet.My;

namespace MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging.CPSMessaging
{
    internal sealed class CpsModuleMessenger : ModuleMessengerBase
    {
        private static readonly IMSLogger Logger = MSLoggerFactory.CreateLogger<CpsModuleMessenger>();
        private const string MessageTopic = "ModuleMessage";

        private readonly ApplicationInfo _application;
        private readonly CpsServiceLoader _cpsServiceLoader;
        private readonly ILocationHolder _locationHolder = new LocationHolder();
        private readonly TimeSpan _timeOut = TimeSpan.FromSeconds(12);
        private readonly IFormatter _serializer;
        private readonly JsonFormatter _jsonFormatter;
        private readonly AppEnvironment _environment;
        private readonly List<CpsServiceProxy> _clients; 
        private long _sequence;
        private bool _disposed;
        private bool _initialized;

        internal static bool ThrowOnUnknownType = true;

        internal CpsModuleMessenger(ApplicationInfo application, IFormatter serializer, AppEnvironment env, CpsServiceLoader cpsServiceLoader)
        {
            _application = application;
            _cpsServiceLoader = cpsServiceLoader;
            _locationHolder.Application = new CafGuiApplicationInfoProxy(_application);

            _serializer = serializer;
            _jsonFormatter = serializer as JsonFormatter;
            _environment = env;

            _clients = new List<CpsServiceProxy>();
        }


        private void LoadCpsInstances(IEnumerable<IServiceConfig> cpsConfigCollection)
        { 
            foreach (var cpsService in cpsConfigCollection)
            {
                var addr = cpsService.VariableMap["address"];

                var isKerberised = false;
                if (cpsService.VariableMap.ContainsKey("Kerberos"))
                    bool.TryParse(cpsService.VariableMap["Kerberos"], out isKerberised);

                

                const string checker = "{0} is null or {0} = \"{1}\"";
                var sameHostFilter = "Sender.Host != \"" + _application.Host + "\"";
                var filter = "CpsModuleMessage::((" + sameHostFilter + ") and (" +
                             String.Format(checker, "MachineName", _application.Host) + ")"
                             + " and (" + String.Format(checker, "ApplicationName", _application.Name) + ")"
                             + " and (" + String.Format(checker, "Instance", _application.Instance) + ")"
                             + " and (" + String.Format(checker, "UserName", _application.User) + "))";

                var options = new SubscribeOptions
                {
                    Topic = MessageTopic,
                    // Filter = filter
                };
                try
                {
                    var client = new CpsServiceProxy(addr, isKerberised, options, OnMessageReceived);
                    //todo: switch these 2 lines if cps server down at start up is supported
                    client.Initialize();
                    _clients.Add(client);
                }
                catch (Exception ex)
                {
                    Logger.Error("Cannot setup connection to cps server " + addr, ex);
                }
            }
        }

        public bool Initialize()
        {
            if (_disposed)
                throw new ApplicationException("object has been disposed!");

            if (_initialized)
                return true;

            if (!_cpsServiceLoader.Loaded)
                return false;
            LoadCpsInstances(_cpsServiceLoader.GetServiceConfigs());
            if (_clients.Count == 0)
            {
                Logger.Error("Failed to initialize CPSModuleMessenger");
                return false;
            } 
            _initialized = true;
            return true;
        }


        private static AppEnvironment GetEnvironment(CpsModuleMessage.Types.Environment env)
        {
            switch (env)
            {
                case CpsModuleMessage.Types.Environment.AllEnv:
                    return AppEnvironment.All;
                case CpsModuleMessage.Types.Environment.Dev:
                    return AppEnvironment.Dev;
                case CpsModuleMessage.Types.Environment.Prod:
                    return AppEnvironment.Prod;
                case CpsModuleMessage.Types.Environment.QA:
                    return AppEnvironment.QA;
                case CpsModuleMessage.Types.Environment.UAT:
                    return AppEnvironment.UAT;
            }

            throw new ApplicationException("Unsupported environment: " + env);
        }

        private void OnMessageReceived(CpsServerMessage message)
        {
            object content = null;
            var isInAppMessage = false;
            var isMachineLocalMessage = false;
            var isDesktopLocalMessage = false;

            using (message)
            {
                switch (message.Header.Command)
                {
                    case CpsServerMessageHeader.CommandType.Publish:
                        using (var bodyReader = message.GetReaderAtBody())
                        {
                            var msg = bodyReader.GetAdapter<CpsModuleMessage>();


                            isMachineLocalMessage = string.Equals(msg.Sender.Host, _application.Host,
                                                                  StringComparison.OrdinalIgnoreCase);

                            isDesktopLocalMessage = isMachineLocalMessage &&
                                                    string.Equals(msg.Sender.Desktop, _application.Desktop,
                                                                  StringComparison.OrdinalIgnoreCase);

                            isInAppMessage = string.Equals(msg.Sender.Application, _application.Name,
                                                           StringComparison.OrdinalIgnoreCase)
                                             && isDesktopLocalMessage
                                             && string.Equals(msg.Sender.User, _application.User,
                                                           StringComparison.OrdinalIgnoreCase);

                            var env = GetEnvironment(msg.AppEnvironment);
                            if (env != AppEnvironment.All && _environment != AppEnvironment.All && env != _environment)
                                break;

                            using (var stream = new MemoryStream(msg.Content.ToByteArray()))
                            {
                                try
                                {
                                    if (_jsonFormatter != null)
                                    {
                                        content = _jsonFormatter.Deserialize(stream, 
                                                                             msg.MessageType,
                                                                             msg.MessageAssembly);
                                    }
                                    else
                                    {
                                        content = _serializer.Deserialize(stream);                                        
                                    }
                                }
                                catch (Exception)
                                {
                                    if (ThrowOnUnknownType)
                                        throw;
                                }

                            }
                        }

                        break;
                }
            }

            if (content != null && !isInAppMessage && !isDesktopLocalMessage)
            {
                var msgType = content.GetType();
                InvokeMessageReceived(msgType, content);
            }
        }

        public void PublishToServer<TMessage>(string serverAddress, bool kerberos, TMessage content, CommunicationTargetFilter targetFilter,
                                      params string[] location)
        {
            if (string.IsNullOrEmpty(serverAddress)) return;
             
            CpsServiceProxy client;
            bool newClient = false;
            lock (_clients)
            {
                client =
               _clients.FirstOrDefault(
                   c_ => c_.CPSServer.Equals(serverAddress, StringComparison.InvariantCultureIgnoreCase));
                if (client == null)
                {
                    var options = new SubscribeOptions
                    {
                        Topic = MessageTopic,
                        // Filter = filter
                    };
                    client = new CpsServiceProxy(serverAddress, kerberos, options, OnMessageReceived);
                    newClient = true;
                    _clients.Add(client); 
                }
            } 
            if (newClient)
            {
                try
                {
                    client.Initialize();

                }
                catch (Exception ex)
                {
                    throw new Exception("Cannot setup connection to cps server " + serverAddress, ex);
                }
            }
            var writer = new MessageWriter<TMessage>(content, _application, _environment, ++_sequence, _serializer,
                                         targetFilter, location);

            client.PublishMessage(writer, MessageTopic); 
        }

        public override void PublishToExternal<TMessage>(TMessage content, CommunicationTargetFilter targetFilter,
                                                         params string[] location)
        {
            var writer = new MessageWriter<TMessage>(content, _application, _environment, ++_sequence, _serializer,
                                                     targetFilter, location); 
            bool succeeded = false;
            var exceptions = new Dictionary<string, Exception>();
            foreach (var client in _clients)//Publish to all cps instances
            {
                try
                {
                    client.PublishMessage(writer, MessageTopic);
                    succeeded = true;
                }
                catch (Exception ex)
                {
                    exceptions[client.CPSServer] = ex; 
                }
            }
             
            if (succeeded)
            {
                foreach (var exception in exceptions)
                {
                    Logger.Warning("Error publish message to cps server " + exception.Key, exception.Value);
                }
            }
            else
            {
                if (exceptions.Count == 1)
                {
                    throw new Exception("Error publish message to cps server " + exceptions.First().Key, exceptions.First().Value);
                }
                if (exceptions.Count > 1)
                {
                    throw new AggregateException(exceptions.Select(ex_ => new Exception("Error publish message to cps server " + ex_.Key, ex_.Value)));   
                }
            }
           
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (_disposed)
                return;

            foreach (var client in _clients)
            {
                try
                {
                    client.Dispose();
                }
                catch (Exception ex)
                {
                    Logger.Error("Error disposing client for cps server " + client.CPSServer, ex); 
                }
            } 
            _disposed = true;
        }

        private class MessageWriter<TMessage> : CpsMessageBodyWriter
        {
            private readonly TMessage _message;
            private readonly ApplicationInfo _application;
            private readonly long _sequence;

            private readonly CpsModuleMessage.Types.TargetFilter _filter;
            private readonly string _machineName;
            private readonly string _applicationName;
            private readonly string _instance;
            private readonly string _user;
            private readonly CpsModuleMessage.Types.Environment _environment;

            private readonly IFormatter _serialzier;

            public MessageWriter(TMessage message, ApplicationInfo appliation, AppEnvironment env, long seq,
                                 IFormatter serializer,
                                 CommunicationTargetFilter filter, params string[] location)
                : base(CpsModuleMessage.Descriptor.FullName)
            {

                _message = message;
                _application = appliation;
                _sequence = seq;
                _serialzier = serializer;

                switch (filter)
                {
                    case CommunicationTargetFilter.All:
                        _filter = CpsModuleMessage.Types.TargetFilter.All;
                        break;

                    case CommunicationTargetFilter.Machine:
                        _filter = CpsModuleMessage.Types.TargetFilter.Machine;
                        _machineName = location[0];
                        break;

                    case CommunicationTargetFilter.MachineApplication:
                        _filter = CpsModuleMessage.Types.TargetFilter.Machine;
                        _machineName = location[0];
                        _applicationName = location[1];
                        break;

                    case CommunicationTargetFilter.MachineInstance:
                        _filter = CpsModuleMessage.Types.TargetFilter.MachineInstance;
                        _machineName = location[0];
                        _instance = location[1];
                        break;

                    case CommunicationTargetFilter.User:
                        _filter = CpsModuleMessage.Types.TargetFilter.User;
                        _user = location[0];
                        break;

                    case CommunicationTargetFilter.UserHost:
                        _filter = CpsModuleMessage.Types.TargetFilter.UserMachine;
                        _user = location[0];
                        _machineName = location[1];
                        break;

                    case CommunicationTargetFilter.UserHostApp:
                        _filter = CpsModuleMessage.Types.TargetFilter.User;
                        _user = location[0];
                        _machineName = location[1];
                        _applicationName = location[2];
                        break;
                }


                switch (env)
                {
                    case AppEnvironment.All:
                        _environment = CpsModuleMessage.Types.Environment.AllEnv;
                        break;
                    case AppEnvironment.Dev:
                        _environment = CpsModuleMessage.Types.Environment.Dev;
                        break;
                    case AppEnvironment.Prod:
                        _environment = CpsModuleMessage.Types.Environment.Prod;
                        break;
                    case AppEnvironment.QA:
                        _environment = CpsModuleMessage.Types.Environment.QA;
                        break;
                    case AppEnvironment.UAT:
                        _environment = CpsModuleMessage.Types.Environment.UAT;
                        break;
                }

            }

            private static CpsApplicationInfo GetCpsApplicationInfo(ApplicationInfo applicationInfo)
            {
                return CpsApplicationInfo.CreateBuilder()
                                         .SetHost(applicationInfo.Host)
                                         .SetApplication(applicationInfo.Name)
                                         .SetDesktop(applicationInfo.Desktop)
                                         .SetUser(applicationInfo.User)
                                         .SetInstance(applicationInfo.Instance).Build();
            }

            public override void OnWriteBody(IAdaptable writer)
            {

                using (var stream = new MemoryStream())
                {
                    //IRemotingFormatter formatter = new BinaryFormatter();
                    _serialzier.Serialize(stream, _message);
                    var asm = typeof (TMessage).Assembly.FullName;
                    var tp = typeof (TMessage).FullName;
                    var msgBuilder = CpsModuleMessage.CreateBuilder().SetSequence(_sequence)
                                                     .SetSender(GetCpsApplicationInfo(_application))
                                                     .SetMessageAssembly(asm)
                                                     .SetMessageType(tp)
                                                     .SetContent(ByteString.CopyFrom(stream.ToArray()))
                                                     .SetFilter(_filter)
                                                     .SetAppEnvironment(_environment);

                    if (_machineName != null)
                        msgBuilder.SetMachineName(_machineName);
                    if (_applicationName != null)
                        msgBuilder.SetApplicationName(_applicationName);
                    if (_instance != null)
                        msgBuilder.SetInsance(_instance);
                    if (_user != null)
                        msgBuilder.SetUserName(_user);

                    var msg = msgBuilder.Build();

                    var rawWriter = writer.GetAdapter<RawMessageWriter>();
                    msg.WriteTo(rawWriter.OutputStream);
                }

            }
        }
    }
}
