﻿using System;
using MorganStanley.MSDotNet.Channels.Cps;
using MorganStanley.MSDotNet.Channels.Raw;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging.IMC.CPS;

namespace MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging.CPSMessaging
{
    internal class CpsApplicationMetaWriter : CpsMessageBodyWriter
    {
        private readonly ApplicationInfo _application;
        private readonly CpsApplicationMeta.Types.ActionEnum _action;
        public CpsApplicationMetaWriter(ApplicationInfo applicationInfo, ExternalMessengerBase.MetaAction action ) : base(CpsApplicationMeta.Descriptor.FullName)
        {
            _application = applicationInfo;
            _action = GetCpsAction(action);/*
            switch(action)
            {
                case ExternalMessengerBase.MetaAction.RequestAllViews:
                case ExternalMessengerBase.MetaAction.ApplicationClosed:
                    _action = GetCpsAction(action);
                    break;
                default:
                    throw new ApplicationException("Not Application Meta action: " + action.ToString());

            }*/
        }

        private static CpsApplicationInfo GetCpsApplicationInfo(ApplicationInfo applicationInfo)
        {
            return CpsApplicationInfo.CreateBuilder()
                .SetHost(applicationInfo.Host)
                .SetApplication(applicationInfo.Name).SetDesktop(applicationInfo.Desktop).SetUser(applicationInfo.User)
                .SetInstance(applicationInfo.Instance).Build();
        }

        private static CpsApplicationMeta.Types.ActionEnum GetCpsAction(ExternalMessengerBase.MetaAction action)
        {
            CpsApplicationMeta.Types.ActionEnum result;
            Enum.TryParse(action.ToString(), out result);
            return result;
        }

        public override void OnWriteBody(IAdaptable writer)
        {
            var msg =
                CpsApplicationMeta.CreateBuilder().SetAction(_action)
                .SetSender(GetCpsApplicationInfo(_application)).Build();

            var rawWriter = writer.GetAdapter<RawMessageWriter>();
            msg.WriteTo(rawWriter.OutputStream);

        }
    }
}
