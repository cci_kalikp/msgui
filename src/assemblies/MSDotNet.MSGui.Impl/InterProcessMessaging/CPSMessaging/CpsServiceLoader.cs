﻿using System;
using System.Collections.Generic;
using System.IO;
using MorganStanley.Desktop.ServiceDiscovery;
using MorganStanley.IED.Concord.Configuration;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.My;

namespace MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging.CPSMessaging
{
    internal sealed class CpsServiceLoader
    {
        private static readonly IMSLogger Logger = MSLoggerFactory.CreateLogger<Bootstrapper>();

        private readonly IServiceLocationManager _locationManager;
        private readonly ISystemConfig _systemConfig;
        private readonly bool _loaded;
        internal static string CpsConfigFile;

        internal CpsServiceLoader(CommunicationChannelStatus channelStatus)
        {
            try
            {
                var path = CpsConfigFile;
                if (string.IsNullOrEmpty(path))
                {
                    Configurator config = null;

                    try
                    {
                        config = ConfigurationManager.GetConfig("MSDesktopCpsConfig") as Configurator;
                    }
                    catch (Exception ex)
                    {
                        // In isolated scenarios, this can fail, in which case just leave config as null
                        Logger.Error("Could not get CPS config from configuration", ex);
                    }

                    if (config != null)
                    {
                        var node = config.GetNode(".", null);
                        if (node.OuterXml != "<MSDesktopCpsConfig />")
                        {
                            var settings = node.OuterXml;
                            settings = settings.Replace("MSDesktopCpsConfig", "ServiceConfig");
                            path = Path.GetTempFileName();
                            using (var tempstream = new StreamWriter(path, false))
                            {
                                tempstream.Write(settings);
                            }
                        }
                    }
                }
               
                // For improvement in concurrency, read the file into memory and pass
                // memory stream into memory
                if (string.IsNullOrEmpty(path))
                {
                    const string cpsConfigFile = "CpsConfig.xml";
                    path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, cpsConfigFile);
                }

                var locationManagers = new List<IServiceLocationManager>();

                // We expect that the CPS channel is either Required or Optional here
                if (channelStatus == CommunicationChannelStatus.Required ||
                    (channelStatus == CommunicationChannelStatus.Optional && File.Exists(path)))
                {
                    FileServiceLocationManager fileLocationManager;
                    using (var fstream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        fileLocationManager = new FileServiceLocationManager(fstream);
                    }

                    locationManagers.Add(fileLocationManager);
                    _loaded = true;
                }

                _locationManager = new ServiceLocationManager(locationManagers);
                _systemConfig = _locationManager.GetSystem("msdesktop/Communication");
            }
            catch (Exception exception)
            {
                _loaded = false;
                if (channelStatus == CommunicationChannelStatus.Optional)
                {
                    Logger.Error("Cannot load cps configuration", exception);
                    return;
                }
                throw new ApplicationException("Cannot load cps configuration", exception);
            }
        }

        internal bool Loaded
        {
            get { return _loaded; }
        }

        public IEnumerable<IServiceConfig> GetServiceConfigs()
        {
            var enumerator = _systemConfig.GetEnumerator();
            while (enumerator.MoveNext())
            {
                var config =  enumerator.Current;
                if (config.Name == "CPS") yield return config;
            };
        }
    }
}
