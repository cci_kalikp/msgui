﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.ServiceModel;
using System.ServiceModel.Channels;
using Microsoft.Practices.Unity;
using MorganStanley.Desktop.ServiceDiscovery;

using MorganStanley.MSDotNet.Channels;
using MorganStanley.MSDotNet.Channels.Cps;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging.IMC.CPS;

namespace MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging.CPSMessaging
{
    internal class CpsMessenger : IDisposable, IExternalCommunicator
    {
        private readonly InterProcessMessenger _ipc;
        private ICpsClient _client;
        private string _viewMeatSubscribeId;
        private string _connectionMeatSubscribeId;
        private string _contentSubscribeId;
        private string _appMetaSubscribeId;

        private const string ViewMetaTopic = "ViewMeta";
        private const string AppMetaTopic = "ApplicationMeta";
        private const string ConnectionMetaTopic = "ConnectionMeta";
        private const string ContentTopic = "MessageContent";

        private const int TimeOutSeconds = 15;

        private readonly TimeSpan _timeOut = TimeSpan.FromSeconds(TimeOutSeconds);
        private readonly ApplicationInfo _applicationInfo;
        private readonly object _thelock = new object();

        private readonly CpsServiceLoader _cpsServiceLoader;

        

        public CpsMessenger(IUnityContainer unityContainer, InterProcessMessenger ipc, ApplicationInfo applicationInfo )
        {
            _ipc = ipc;
            _applicationInfo = applicationInfo;

            var settings = unityContainer.Resolve<ICommunicatorSettings>();
            _cpsServiceLoader = new  CpsServiceLoader(settings.ImcStatus);
        
        }

        private bool _initialized;
        public bool Initialize()
        {
            lock (_thelock)
            {
                if (_disposed)
                    throw new ApplicationException("Object has already disposed!");

                if (_initialized)
                    return true;


                if (!_cpsServiceLoader.Loaded)
                    return false;

                try
                {
                    var cpsService = _cpsServiceLoader.GetServiceConfigs().First();
                    var addr = new EndpointAddress(cpsService.VariableMap["address"]);

                    var binding = new CustomBinding(
                        new CpsMessageEncoderBindingElement
                        {
                            MessageFormat = CpsMessageFormat.Cps0Gpb
                        },
                        new StringTcpTransportBindingElement
                        {
                            LeaseTimeout = TimeSpan.MaxValue
                        }
                        );

                    _client = new CpsClient(binding, addr);



                    var viewMetaFitler = "CpsViewMeta::Sender.Host != \"" + _applicationInfo.Host + "\"";
                    var options = new SubscribeOptions
                    {
                        Topic = ViewMetaTopic,
                        Filter = viewMetaFitler
                    };

                    _viewMeatSubscribeId =
                        _client.Subscribe(OnViewMetaReceived,
                                          _timeOut, options);



                    options = new SubscribeOptions { Topic = ConnectionMetaTopic, Filter = "CpsConnectionMeta::Sender.Host != \"" + _applicationInfo.Host + "\"" };
                    _connectionMeatSubscribeId =
                        _client.Subscribe(OnConnectionMeatReceived,
                                          _timeOut,
                                          options);

                    options = new SubscribeOptions { Topic = ContentTopic, Filter = "CpsMessageContent::Sender.Host != \"" + _applicationInfo.Host + "\"" };
                    _contentSubscribeId =
                        _client.Subscribe(OnMessageContentReceived,
                                          _timeOut,
                                          options);


                    options = new SubscribeOptions
                    {
                        Topic = AppMetaTopic
                        ,
                        Filter = "CpsApplicationMeta::Sender.Host != \"" + _applicationInfo.Host + "\""
                    };
                    _appMetaSubscribeId =
                        _client.Subscribe(
                            OnApplicationEventReceived, _timeOut,
                            options);
                }
                catch (Exception exception)
                {
                    var msg = "Cannot setup cps connection:" + exception.Message;
                    Trace.WriteLine(msg);
                    var log = MSLog.MSLog.Error();
                    log.Message = exception.Message + "\n" + exception.StackTrace;
                    log.Send();
                    (this as IDisposable).Dispose();

                    throw new ApplicationException("Cannot setup cps connection", exception);
                    return false;
                }

                _initialized = true;

                return _initialized;
            }
           
        }

        private static ApplicationInfo GetApplicationInfo(CpsApplicationInfo cps)
        {
            return new ApplicationInfo(cps.Host, cps.User, cps.Desktop, cps.Application, cps.Instance);
        }

        private static ExternalMessengerBase.MetaAction GetViewAction(CpsViewMeta.Types.ActionEnum action)
        {
            var name = action.ToString();
            ExternalMessengerBase.MetaAction result;
            Enum.TryParse(name, out result);
            return result;
        }

        private static ExternalMessengerBase.MetaAction GetViewAction(CpsApplicationMeta.Types.ActionEnum action)
        {
            var name = action.ToString();
            ExternalMessengerBase.MetaAction result;
            Enum.TryParse(name, out result);
            return result;
        }

        private static ExternalMessengerBase.ConnectionAction GetConnectionAction(CpsConnectionMeta.Types.ConnectionAction action)
        {
            var name = action.ToString();
            ExternalMessengerBase.ConnectionAction result;
            Enum.TryParse(name, out result);
            return result;
        }

        private static ViewMetaInfo GetView(ApplicationInfo sender, CpsViewInfo viewInfo)
        {
            ViewMetaInfo view;
            if (viewInfo.HasScreenShot)
            {
                using (var stream = new MemoryStream(viewInfo.ScreenShot.ToByteArray()))
                {
                    IFormatter formatter = new BinaryFormatter();
                    var screenShot = (Image)formatter.Deserialize(stream);
                    var viewInfoMsg = new ViewInfoMessage(viewInfo.ID, viewInfo.Title, viewInfo.Tab, screenShot);
                    view = new ViewMetaInfo(sender, viewInfo.ID, viewInfoMsg);
                }

            }
            else
            {
                var viewInfoMsg = new ViewInfoMessage(viewInfo.ID, viewInfo.Title, viewInfo.Tab);
                view = new ViewMetaInfo(sender, viewInfo.ID, viewInfoMsg);
            }
            return view;
        }


        private static Type GetMessageType(string assembly, string typename)
        {
            var asm = Assembly.Load(assembly);
            var msgType = asm.GetType(typename);
            return msgType;
        }

        private void OnViewMetaReceived(CpsServerMessage message)
        {
            lock (_thelock)
            {
                using (message)
                {
                    switch (message.Header.Command)
                    {
                        case CpsServerMessageHeader.CommandType.Publish:
                            using (var bodyReader = message.GetReaderAtBody())
                            {
                                var meta = bodyReader.GetAdapter<CpsViewMeta>();
                                var action = GetViewAction(meta.Action);
                                var senderApp = GetApplicationInfo(meta.Sender);
                                ViewMetaInfo view;


                                switch (action)
                                {
                                    case ExternalMessengerBase.MetaAction.RegisterPublisher:
                                    case ExternalMessengerBase.MetaAction.RegisterSubscriber:
                                    case ExternalMessengerBase.MetaAction.UnregisterPublisher:
                                    case ExternalMessengerBase.MetaAction.UnregisterSubscriber:
                                        view = new ViewMetaInfo(senderApp, meta.SenderViewId, null);
                                        var msgType = GetMessageType(meta.SenderMessageAssembly, meta.SenderMessageType);
                                        //Task.Factory.StartNew(
                                        //   () => {

                                        _ipc.OnExternalEvent(action, senderApp, view, msgType);

                                        //    });

                                        break;
                                    case ExternalMessengerBase.MetaAction.ViewAdded:
                                    case ExternalMessengerBase.MetaAction.ViewRemoved:
                                    case ExternalMessengerBase.MetaAction.ViewUpdated:
                                        view = GetView(senderApp, meta.SenderView);
                                        _ipc.OnExternalEvent(action, senderApp, view, null);
                                        break;
                                    default:
                                        throw new ApplicationException("Not supporte action:" + action);
                                }

                            }
                            break;

                    }

                }
            }
           

        }

        private void OnConnectionMeatReceived(CpsServerMessage message)
        {
            lock (_thelock)
            {
                using (message)
                {
                    switch (message.Header.Command)
                    {
                        case CpsServerMessageHeader.CommandType.Publish:
                            using (var bodyReader = message.GetReaderAtBody())
                            {
                                var meta = bodyReader.GetAdapter<CpsConnectionMeta>();
                                var action = GetConnectionAction(meta.Action);
                                var sender = GetApplicationInfo(meta.Sender);

                                var extView = new ViewMetaInfo(sender, meta.SenderViewId, null);
                                var receiver = GetApplicationInfo(meta.Receiver);
                                var localView = new ViewMetaInfo(receiver, meta.ReceiverViewId, null);



                                _ipc.OnExternalConnectionEvent(action, sender, GetMessageType(meta.SenderMessageAssembly, meta.SenderMessageType),
                                    extView, GetMessageType(meta.ReceiverMessageAssembly, meta.ReceiverMessageType), localView, meta.SenderAsPublisher);

                            }
                            break;
                    }
                }
            }
            

        }

        private void OnMessageContentReceived(CpsServerMessage message)
        {
            lock (_thelock)
            {
                using (message)
                {
                    switch (message.Header.Command)
                    {
                        case CpsServerMessageHeader.CommandType.Publish:
                            using (var bodyReader = message.GetReaderAtBody())
                            {
                                var msg = bodyReader.GetAdapter<CpsMessageContent>();
                                var sender = msg.Sender;
                                var viewId = msg.SenderViewId;
                                var extView = new ViewMetaInfo(GetApplicationInfo(sender), viewId, null);

                                using (var stream = new MemoryStream(msg.Content.ToByteArray()))
                                {
                                    IFormatter formatter = new BinaryFormatter();

                                    var content = formatter.Deserialize(stream);
                                    _ipc.OnMessageRecieved(extView.FullId, content.GetType(), content);
                                }
                            }
                            break;
                    }
                }
            }
           
        }

        private void OnApplicationEventReceived(CpsServerMessage message)
        {
            lock (_thelock)
            {
                using (message)
                {
                    switch (message.Header.Command)
                    {
                        case CpsServerMessageHeader.CommandType.Publish:
                            using (var bodyReader = message.GetReaderAtBody())
                            {
                                var meta = bodyReader.GetAdapter<CpsApplicationMeta>();
                                var action = GetViewAction(meta.Action);
                                var senderApp = GetApplicationInfo(meta.Sender);

                                _ipc.OnExternalEvent(action, senderApp, null, null);


                            }
                            break;

                    }

                }
            }
           

        }



        public void NotifyRegister(Type msgType, string viewId, bool asPublisher)
        {
            lock (_thelock)
            {
                var msg = new CpsViewMetaMsgWriter(_applicationInfo);

                var action = asPublisher
                             ? ExternalMessengerBase.MetaAction.RegisterPublisher
                             : ExternalMessengerBase.MetaAction.RegisterSubscriber;


                msg.Initialize(msgType, viewId, action, true);
                var options = new PublishOptions { Topic = ViewMetaTopic };
                _client.Publish(msg, _timeOut, options);
            }
           


        }

        public void NotifyUnregister(Type msgType, string viewId, bool asPublisher)
        {
            lock (_thelock)
            {
                var msg = new CpsViewMetaMsgWriter(_applicationInfo);
                var action = asPublisher
                                 ? ExternalMessengerBase.MetaAction.UnregisterPublisher
                                 : ExternalMessengerBase.MetaAction.UnregisterSubscriber;

                msg.Initialize(msgType, viewId, action, true);

                var options = new PublishOptions { Topic = ViewMetaTopic };
                _client.Publish(msg, _timeOut, options);
            }
            
        }

        public void NotifyAllViewAdded(ViewMetaInfo viewInfo)
        {
            throw new NotImplementedException();
        }

        public void NotifyAllViewRemoved(ViewMetaInfo viewInfo)
        {
            lock (_thelock)
            {
                var msg = new CpsViewMetaMsgWriter(_applicationInfo);
                msg.Initialize(ExternalMessengerBase.MetaAction.ViewRemoved, viewInfo);
                var options = new PublishOptions { Topic = ViewMetaTopic };
                _client.Publish(msg, _timeOut, options);
            }
          
        }

        public void NotifyAllViewUpdated(ViewMetaInfo viewInfo)
        {
            lock (_thelock)
            {
                var msg = new CpsViewMetaMsgWriter(_applicationInfo);
                msg.Initialize(ExternalMessengerBase.MetaAction.ViewUpdated, viewInfo);
                var options = new PublishOptions { Topic = ViewMetaTopic };
                _client.Publish(msg, _timeOut, options);
            }
            
        }

        public void NotifyApplicationClosed()
        {
            lock (_thelock)
            {
                var msg = new CpsApplicationMetaWriter(_applicationInfo, ExternalMessengerBase.MetaAction.ApplicationClosed);
                var options = new PublishOptions { Topic = AppMetaTopic };
                _client.Publish(msg, _timeOut, options);    
            }
            
        }




        public void NotifyConnection<TLocalMsgType, TExtMsgType>(ExternalMessengerBase.ConnectionAction connection, string localViewId, ViewMetaInfo extView, bool extViewAsPublisher)
        {

            lock (_thelock)
            {
                var msg = new CpsConnectionMetaWriter();
                msg.Initialize<TLocalMsgType, TExtMsgType>(connection, new ViewMetaInfo(_applicationInfo, localViewId, null),
                                                           extView, !extViewAsPublisher);

                var options = new PublishOptions { Topic = ConnectionMetaTopic };
                _client.Publish(msg, _timeOut, options);
            }
           

        }

        public void RequestAllViews()
        {
            lock (_thelock)
            {
                var msg = new CpsApplicationMetaWriter(_applicationInfo, ExternalMessengerBase.MetaAction.RequestAllViews);
                var options = new PublishOptions { Topic = AppMetaTopic };
                _client.Publish(msg, _timeOut, options);
            }
           
        }

        public void PublishToExternal(string loalViewId, object message)
        {

            lock (_thelock)
            {
                var msg = new CpsContentMessageWriter();
                msg.Initialize(_applicationInfo, loalViewId, message);
                var options = new PublishOptions { Topic = ContentTopic };
                _client.Publish(msg, _timeOut, options);
            }
           
        }

        #region Release subscriptions



        private bool _disposed;
        public void Dispose()
        {
            if (!_disposed)
            {
                if (_client == null)
                    return;
                switch (_client.Channel.State)
                {
                    case CommunicationState.Opened:
                    case CommunicationState.Opening:
                        _client.Unsubscribe(_viewMeatSubscribeId, _timeOut);
                        _client.Unsubscribe(_connectionMeatSubscribeId, _timeOut);
                        _client.Unsubscribe(_contentSubscribeId, _timeOut);
                        _client.Unsubscribe(_appMetaSubscribeId, _timeOut);
                        _client.Close(_timeOut);
                        break;
                }
            }
            _disposed = true;
            GC.SuppressFinalize(this);
        }

        ~CpsMessenger()
        {
            Dispose();
        }
        #endregion
    }
}
