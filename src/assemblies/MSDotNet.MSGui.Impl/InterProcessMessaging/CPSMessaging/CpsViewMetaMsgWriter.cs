﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using Google.ProtocolBuffers;
using MorganStanley.MSDotNet.Channels.Cps;
using MorganStanley.MSDotNet.Channels.Raw;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging.IMC.CPS;

namespace MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging.CPSMessaging
{
    internal class CpsViewMetaMsgWriter: CpsMessageBodyWriter
    {
        private string _viewId;
        private Type _msgType;
        private bool _isReceiveLastMessage;
        private CpsViewMeta.Types.ActionEnum _action;

        private ViewMetaInfo _extView;
        private readonly ApplicationInfo _application;

        private bool _initialized;

        public CpsViewMetaMsgWriter(ApplicationInfo applicationInfo)
            : base(CpsViewMeta.Descriptor.FullName)
        {
            _application = applicationInfo;
        }

     

        public void Initialize(Type msgType, string viewId, ExternalMessengerBase.MetaAction action, bool isReceiveLastMessage)
        {
            _msgType = msgType;
            _viewId = viewId;

            _action = GetCpsAction(action);

            _isReceiveLastMessage = isReceiveLastMessage;


            _initialized = true;
        }

        public void Initialize(ExternalMessengerBase.MetaAction action, ViewMetaInfo view)
        {

            _action = GetCpsAction(action); 
            _extView = view;

            _initialized = true;
        }

        private static CpsViewInfo GetCpsViewInfo(ViewMetaInfo view)
        {
            var cpsView = CpsViewInfo.CreateBuilder().SetID(view.LocalId);
            if(view.TabName != null)
                cpsView.SetTab(view.TabName);
            if(view.Title != null)
                cpsView.SetTitle(view.Title);

            if (view.ScreenShot != null)
            {
                IFormatter formatter = new BinaryFormatter();
                using (var stream = new MemoryStream())
                {
                    formatter.Serialize(stream, view.ScreenShot);
                    cpsView.SetScreenShot(ByteString.CopyFrom(stream.ToArray()));
                }
            }

            return cpsView.Build();
        }

        private static CpsViewMeta.Types.ActionEnum GetCpsAction(ExternalMessengerBase.MetaAction action)
        {
            CpsViewMeta.Types.ActionEnum result;
            Enum.TryParse(action.ToString(), out result);
            return result;
        }

        private static CpsApplicationInfo GetCpsSenderInfo(ApplicationInfo applicationInfo  )
        {
            return CpsApplicationInfo.CreateBuilder().SetHost(applicationInfo.Host)
                .SetApplication(applicationInfo.Name).SetDesktop(applicationInfo.Desktop).SetUser(applicationInfo.User)
                .SetInstance(applicationInfo.Instance).Build();
        }

        public override void OnWriteBody(IAdaptable writer)
        {

            if(!_initialized)
                throw new ApplicationException("Message not initialized");

            var rawWriter = writer.GetAdapter<RawMessageWriter>();

            var meta =
                CpsViewMeta.CreateBuilder()
                .SetSender(GetCpsSenderInfo(_application)).SetAction(_action);
            if (_extView != null)
            {
                meta.SetSenderViewId(_extView.LocalId).SetSenderView(GetCpsViewInfo(_extView));
            }
            else if (_viewId != null)
            {
                var asm = _msgType.Assembly;

                meta.SetSenderViewId(_viewId).SetSenderMessageAssembly(
                        asm.GetName().Name).SetSenderMessageType(_msgType.FullName);

                if (_action == CpsViewMeta.Types.ActionEnum.RegisterSubscriber)
                {
                    meta.SetIsReceiveLastMessage(_isReceiveLastMessage);
                }
            }
            
            meta.Build().WriteTo(rawWriter.OutputStream);
        }
    }

}
