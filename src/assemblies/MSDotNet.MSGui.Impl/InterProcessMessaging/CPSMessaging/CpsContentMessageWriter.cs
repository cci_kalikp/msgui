﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using Google.ProtocolBuffers;
using MorganStanley.MSDotNet.Channels.Cps;
using MorganStanley.MSDotNet.Channels.Raw;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging.IMC.CPS;

namespace MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging.CPSMessaging
{
    internal class CpsContentMessageWriter : CpsMessageBodyWriter
    {
        private ApplicationInfo _sender;
        private string _senderViewId;
        private object _message;

        private static long _messageSeqNum;

        private bool _initialized;
        public CpsContentMessageWriter()
            : base(CpsMessageContent.Descriptor.FullName)
        {
        }
        
        public void Initialize(ApplicationInfo sender, string viewId, object message)
        {
            _sender = sender;
            _senderViewId = viewId;
            _message = message;
            _initialized = true;
        }

        private static CpsApplicationInfo GetCpsApplicationInfo(ApplicationInfo applicationInfo)
        {
            return CpsApplicationInfo.CreateBuilder()
                .SetHost(applicationInfo.Host)
                .SetApplication(applicationInfo.Name).SetDesktop(applicationInfo.Desktop).SetUser(applicationInfo.User)
                .SetInstance(applicationInfo.Instance).Build();
        }


        public override void OnWriteBody(IAdaptable writer)
        {
            if (!_initialized)
                throw new ApplicationException("Message not initialized.");
           
            
            using (var stream = new MemoryStream())
            {
                IFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, _message);

                var msg = CpsMessageContent.CreateBuilder().SetSequenceNumber(_messageSeqNum++)
                    .SetSender(GetCpsApplicationInfo(_sender)).SetSenderViewId(_senderViewId).
                    SetContent(ByteString.CopyFrom(stream.ToArray())).Build();
                var rawWriter = writer.GetAdapter<RawMessageWriter>();
                msg.WriteTo(rawWriter.OutputStream);
            }


          

            _initialized = false;
        }
    }
}
