﻿using System;
using MorganStanley.MSDotNet.Channels.Cps;
using MorganStanley.MSDotNet.Channels.Raw;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging.IMC.CPS;

namespace MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging.CPSMessaging
{
    internal class CpsConnectionMetaWriter : CpsMessageBodyWriter
    {

        private CpsConnectionMeta.Types.ConnectionAction _action;
        private ViewMetaInfo _localView;
        private ViewMetaInfo _extlView;
        private bool _extViewAsPublisher;

        private bool _initialized;

        private Type _localMsgType;
        private Type _extMsgType;

        public CpsConnectionMetaWriter()
            : base(CpsConnectionMeta.Descriptor.FullName)
        {
        }

        public void Initialize<TLocalMsgType, TExtMsgType>(ExternalMessengerBase.ConnectionAction action, ViewMetaInfo localView, ViewMetaInfo extView, bool extViewAsPublisher)
        {
            _action = GetCpsAction(action);
            _localView = localView;
            _extlView = extView;
            _extViewAsPublisher = extViewAsPublisher;

            _localMsgType = typeof (TLocalMsgType);
            _extMsgType = typeof (TExtMsgType);

            _initialized = true;
        }

        private static CpsApplicationInfo GetCpsApplicationInfo(ApplicationInfo applicationInfo)
        {
            return CpsApplicationInfo.CreateBuilder().SetHost(applicationInfo.Host)
                .SetApplication(applicationInfo.Name).SetDesktop(applicationInfo.Desktop).SetUser(applicationInfo.User)
                .SetInstance(applicationInfo.Instance).Build();
        }

        private static CpsConnectionMeta.Types.ConnectionAction GetCpsAction(ExternalMessengerBase.ConnectionAction action)
        {
            CpsConnectionMeta.Types.ConnectionAction result;
            Enum.TryParse(action.ToString(), out result);
            return result;
        }

        public override void OnWriteBody(IAdaptable writer)
        {
            if(! _initialized )
                throw new ApplicationException("Message not initialized.");

            var msg = CpsConnectionMeta.CreateBuilder()
                .SetAction(_action)
                .SetSender(GetCpsApplicationInfo(_localView.Application))
                .SetSenderViewId(_localView.LocalId)
                .SetSenderMessageAssembly(_localMsgType.Assembly.GetName().Name)
                .SetSenderMessageType(_localMsgType.FullName)
                .SetReceiver(GetCpsApplicationInfo(_extlView.Application)).SetSenderAsPublisher(_extViewAsPublisher)
                .SetReceiverViewId(_extlView.LocalId)
                .SetReceiverMessageAssembly(_extMsgType.Assembly.GetName().Name)
                .SetReceiverMessageType(_extMsgType.FullName)
                .Build();


            var rawWriter = writer.GetAdapter<RawMessageWriter>();
            msg.WriteTo(rawWriter.OutputStream);

            _initialized = false;
        }
    }
}
