﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using MorganStanley.MSDotNet.Channels;
using MorganStanley.MSDotNet.Channels.Client;
using MorganStanley.MSDotNet.Channels.Cps; 

namespace MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging.CPSMessaging
{
    public class CpsServiceProxy:IDisposable
    {
        //private static readonly IMSLogger Logger = MSLoggerFactory.CreateLogger<CpsServiceProxy>();
        static readonly TimeSpan TimeOut = TimeSpan.FromSeconds(12);
        private ICpsClient client; 
        private Dictionary<string, string> subscriptions = new Dictionary<string, string>();
        private readonly bool kerberos;
        private readonly Dictionary<SubscribeOptions, Action<CpsServerMessage>> subscriptionSettings; 
        public CpsServiceProxy(string address_, bool kerberos_, Dictionary<SubscribeOptions, Action<CpsServerMessage>> subscriptions_)
        {
            CPSServer = address_;
            kerberos = kerberos_;
            subscriptionSettings = subscriptions_; 
        }

        public CpsServiceProxy(string address_, bool kerberos_, SubscribeOptions subscribeOptions_, Action<CpsServerMessage> callback_):
        this(address_, kerberos_, new Dictionary<SubscribeOptions, Action<CpsServerMessage>>{{subscribeOptions_,callback_}})
        { 
        }

        public bool Initialized { get; private set; }
        public void Initialize()
        {
            if (Initialized) return;
            lock (this)
            {  
                if (Initialized) return;
                var binding = new CustomBinding(new CpsMessageEncoderBindingElement
                {
                    MessageFormat = CpsMessageFormat.Cps0Gpb
                },
                                new StringTcpTransportBindingElement
                                {
                                    LeaseTimeout = TimeSpan.MaxValue,
                                    Connector = kerberos ? "Kerberos" : "Default"
                                }
    );
                var addr = new EndpointAddress(CPSServer);
                 
                client = ChannelClientFactory.Default.CreateClient<ICpsClient>(binding, addr);
                 
                client.Open(TimeSpan.FromSeconds(10));
                client.Channel.Faulted += (sender, args) =>
                    {
                        Initialized = false;
                        client.Abort();
                    };

                foreach (var subscription in subscriptionSettings)
                {
                    var messageSubscribeId = client.Subscribe(subscription.Value, TimeOut, subscription.Key);
                    subscriptions[subscription.Key.Topic] = messageSubscribeId;
                }
                Initialized = true;
            } 
        }

        public string CPSServer { get; private set; }

        public void PublishMessage(CpsMessageBodyWriter writer_, string topic_)
        {
            if (topic_ == null) return;
            if (!Initialized)
            {
                throw new Exception("Connection to cps server " + CPSServer + " is down");
                //Initialize();
            }
            var options = new PublishOptions {Topic = topic_};
            client.Publish(writer_, TimeOut, options);
        }
         
        public void Dispose()
        {
            if (!Initialized) return;
            lock (this)
            {
                foreach (var subscription in subscriptions.Values)
                {
                    client.Unsubscribe(subscription, TimeOut);
                    client.Close(TimeOut);
                }
                Initialized = false;
            }
 
        }
    }
}
