﻿using System;
namespace MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging
{
    /// <summary>
    /// Null object pattern, do nothing
    /// </summary>

    internal class NullExternalCommunicator : IExternalCommunicator
    {
        public bool Initialize()
        {
            return true;
        }

        public void NotifyRegister(Type msgType, string viewId, bool asPublisher)
        {
            
        }

        public void NotifyUnregister(Type msgType, string viewId, bool asPublisher)
        {
            
        }


        public void NotifyAllViewAdded(ViewMetaInfo viewInfo)
        {
          
        }

        public void NotifyAllViewRemoved(ViewMetaInfo viewInfo)
        {
            
        }

        public void NotifyAllViewUpdated(ViewMetaInfo viewInfo)
        {
           
        }

        public void NotifyApplicationClosed()
        {
           
        }



        public void NotifyConnection<TLocalMsgType, TExtMsgType>(ExternalMessengerBase.ConnectionAction connection, string localViewId, ViewMetaInfo extView, bool extViewAsPublisher)
        {
           

        }

        public void RequestAllViews()
        {
            
        }

        public void PublishToExternal(string loalViewId, object message)
        {
            
        }
    }
}
