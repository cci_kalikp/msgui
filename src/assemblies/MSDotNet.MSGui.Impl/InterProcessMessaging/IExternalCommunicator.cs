﻿using System;

namespace MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging
{
    interface IExternalCommunicator
    {

        bool Initialize();

        void NotifyRegister(Type msgType, string viewId, bool asPublisher);

        void NotifyUnregister(Type msgType, string viewId, bool asPublisher);

        void NotifyAllViewAdded(ViewMetaInfo viewInfo);

        void NotifyAllViewRemoved(ViewMetaInfo viewInfo);

        void NotifyAllViewUpdated(ViewMetaInfo viewInfo);

        void NotifyApplicationClosed();



        void NotifyConnection<TLocalMsgType, TExtMsgType>(ExternalMessengerBase.ConnectionAction connection,
                                                          string localViewId, ViewMetaInfo extView,
                                                          bool extViewAsPublisher);

        void RequestAllViews();

        void PublishToExternal(string loalViewId, object message);
    }
}
