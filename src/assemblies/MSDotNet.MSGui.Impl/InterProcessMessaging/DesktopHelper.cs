﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging
{
    class DesktopHelper
    {
        private const int Defualt_Length = 1000;
        public static string GetCurrentDesktopName()
        {
            var threadId = GetCurrentThreadId();
            var hdesk = GetThreadDesktop(threadId);
            var cache = new byte[Defualt_Length];
            int len;
            if (!GetUserObjectInformation(hdesk, 2, cache, Defualt_Length, out len))
            {

                var errorCode = GetLastError();
                throw new Exception("Cannot get current desktop, error code  " + errorCode);
            }

            return System.Text.ASCIIEncoding.ASCII.GetString(cache, 0, Math.Min(len, Defualt_Length) - 1); //trim our the ending '\0'
        }


        #region
        [DllImport("user32.dll")]
        private static extern IntPtr GetThreadDesktop(uint threadId);

        [DllImport("kernel32.dll")]
        private static extern uint GetCurrentThreadId();

        [DllImport("user32.dll", SetLastError = true)]
        private static extern bool GetUserObjectInformation(IntPtr hObj, int nIndex,
           [Out] byte[] pvInfo, uint nLength, out int lpnLengthNeeded);

        [DllImport("kernel32.dll")]
        private static extern uint GetLastError();
        #endregion

    }
}
