﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging.FusionMessaging
{
    [Serializable]
    internal class IpcMessage
    {
        internal object Content  { get; set; }
        internal string ExternalViewId { get; set; }
    }
}
