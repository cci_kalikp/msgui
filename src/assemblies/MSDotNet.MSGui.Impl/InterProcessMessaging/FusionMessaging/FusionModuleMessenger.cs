﻿using System;
using System.IO;
using System.Runtime.Serialization;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Core.Persistence.DelayedLoading;
using MorganStanley.MSDotNet.MSGui.Impl.FusionMessagingImpl;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;

namespace MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging.FusionMessaging
{
    internal sealed class FusionModuleMessenger : ModuleMessengerBase
    {
        private const string M2MChannel = "M2M_Ipc_Channel";
        private readonly ApplicationInfo _applicationInfo;
        private readonly FusionApplicationMessagingService _messageService;
        private readonly IFormatter _serializer;
        private readonly AppEnvironment _environment;
        private readonly LocationHolder _locationHolder = new LocationHolder();

        private bool _disposed;
        private bool _initialized;

        public FusionModuleMessenger(ApplicationInfo application, IFormatter serializer, AppEnvironment env)
        {
            _applicationInfo = application;
            _environment = env;

            _messageService = new FusionApplicationMessagingService(
                _applicationInfo.Host,
                _applicationInfo.User,
                _applicationInfo.Desktop,
                _applicationInfo.Name,
                _applicationInfo.Instance);
            _messageService.DesktopService.OnDesktopPeerJoin += new EventHandler<DataEventArgs<FSNAppContext>>(DesktopService_OnDesktopPeerJoin); 

            _locationHolder.Application = new CafGuiApplicationInfoProxy(_applicationInfo);
            _serializer = serializer;
        }
        void DesktopService_OnDesktopPeerJoin(object sender, DataEventArgs<FSNAppContext> e)
        {
            DelayedProfileLoadingExtensionPoints.InvokePeerApplicationJoin(FusionApplicationMessagingService.CreateApplicationInfo(e.Data));
        }
        public bool Initialize()
        {
            if (_disposed)
                throw new ApplicationException("Object has already disposed!");

            if (_initialized)
                return true;

            _messageService.Start();
            _messageService.SubscribeChannel<ModuleMessage>(M2MChannel, OnExternalMessageRecieved);

            _initialized = true;
            return true;
        }

        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            try
            {
                _messageService.DesktopService.OnDesktopPeerJoin -= new EventHandler<DataEventArgs<FSNAppContext>>(DesktopService_OnDesktopPeerJoin);
                _messageService.Stop();
                _disposed = true;
            }
            catch (Exception exception)
            {
                var log = MSLog.MSLog.Error();
                log.Message = exception.Message + "\n" + exception.StackTrace;
                log.Send();
            }
        }

      

        private void OnExternalMessageRecieved(string channel, ReceivedMessage<ModuleMessage> message)
        {
            if (message.SenderApplicationInfo.Host != _applicationInfo.Host)
                return;

            var senderInstance = message.SenderApplicationInfo.Instance;
            var applicationInfoInstance = _applicationInfo.Instance;
            if (senderInstance == applicationInfoInstance)
            {
                // Local message - not handled through remote means
                return;
            }

            if (!_locationHolder.BelongsToLocation(message.Payload.TargetFilter, message.Payload.Location))
                return;

            if (message.Payload.Environment != AppEnvironment.All
                && message.Payload.Environment != _environment
                && _environment != AppEnvironment.All)
                return;

            object msg = null; 
            using (var stream = new MemoryStream((byte[]) message.Payload.Content))
            {
                try
                {
                    msg = _serializer.Deserialize(stream);
                }
                catch
                {
                    
                }
                if (msg == null) return; 
            }

            if (AcceptMessage(message, msg))
            {
                InvokeMessageReceived(msg.GetType(), msg);        
            }
        }

        private bool AcceptMessage(ReceivedMessage<ModuleMessage> message_, object payload_)
        {

            if (Communicator.IsIpcForAll) return true;
            //messages between subsystem or host belongs to the same host system
            if (LocationHolderExtension.OfSameHostSystemButNotSameInstance(
        message_.SenderApplicationInfo.Instance,
        _applicationInfo.Instance))
                return true;

            if (payload_ is IMessageWithPayload) return true;
            if (LauncherMessageExtension.IsLauncherMessage(payload_)) return true;

            return false; 
        }

        public override void PublishToExternal<TMessage>(TMessage message, CommunicationTargetFilter targetFilter,
                                                         params string[] location)
        {
            using (var stream = new MemoryStream())
            {
                _serializer.Serialize(stream, message);
                var content = stream.ToArray();
                var msg = new ModuleMessage
                    {
                        //Assembly = typeof (TMessage).Assembly.FullName,
                        //MsgType = typeof (TMessage).FullName,
                        Content = content,
                        TargetFilter = targetFilter,
                        Location = location,
                        Environment = _environment
                    };

                _messageService.PublishChannel(M2MChannel, msg);
            }
        }
    }
}
