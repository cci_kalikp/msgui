﻿using System;

namespace MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging.FusionMessaging
{
    [Serializable]
    internal class ConnectionInfoMessage
    {
        internal bool ExternalAsPublisher { get; set; }
        internal ExternalMessengerBase.ConnectionAction Action { get; set; }

        internal string ExternalId { get; set; }
        internal string ExternalMessageType { get; set; }
        internal string ExternalMessageAssembly { get; set; }

        internal string LocalFullId { get; set; }
        internal string LocalId { get; set; }
        internal string LocalMessageType { get; set; }
        internal string LocalMessageAssembly { get; set; }

    }
}
