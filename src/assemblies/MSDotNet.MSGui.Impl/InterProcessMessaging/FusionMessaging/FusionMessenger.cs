﻿using System;
using System.Diagnostics;
using System.Linq;

using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Core.Persistence.DelayedLoading;
using MorganStanley.MSDotNet.MSGui.Impl.FusionMessagingImpl;

namespace MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging.FusionMessaging
{
    internal class FusionMessenger : IExternalCommunicator, IDisposable
    {
        private readonly FusionApplicationMessagingService _messageService;

        private readonly InterProcessMessenger _registrator;
        private readonly ApplicationInfo _applicationInfo;



        private const string MessageChannel = "IPC_Message_Channel";
        private const string MetaChannel = "IPC_Meta_Channel";
        private const string ConnectionChannel = "IPC_Connection_Channel";

        public FusionMessenger(ApplicationInfo application, InterProcessMessenger registrator)
        {
            
            _applicationInfo = application;

            _messageService = new FusionApplicationMessagingService(
                       _applicationInfo.Host,
                       _applicationInfo.User,
                       _applicationInfo.Desktop,
                       _applicationInfo.Name,
                       _applicationInfo.Instance);
            _messageService.DesktopService.OnDesktopPeerJoin += new EventHandler<DataEventArgs<FSNAppContext>>(DesktopService_OnDesktopPeerJoin); 
            _registrator = registrator;

        }

        void DesktopService_OnDesktopPeerJoin(object sender, DataEventArgs<FSNAppContext> e)
        {
            DelayedProfileLoadingExtensionPoints.InvokePeerApplicationJoin(FusionApplicationMessagingService.CreateApplicationInfo(e.Data));
        }

        #region dispose

        private bool _disposed;
        public void Dispose()
        {
            if(_disposed)
                return;

            try
            {
                /* if(_messageService.IsSubscribed(MetaChannel))
                     _messageService.UnsubscribeChannel<IpcMetaMessage>(MetaChannel, DispatchExternEvents);
                 if(_messageService.IsSubscribed(ConnectionChannel))
                     _messageService.UnsubscribeChannel<ConnectionInfoMessage>(ConnectionChannel, DispatchConnectionEvents);
                 if(_messageService.IsSubscribed(MessageChannel))
                     _messageService.UnsubscribeChannel<IpcMessage>(MessageChannel, DippatchMessage);*/

                _messageService.DesktopService.OnDesktopPeerJoin -= new EventHandler<DataEventArgs<FSNAppContext>>(DesktopService_OnDesktopPeerJoin);
                _messageService.Stop();
                _disposed = true;
            }
            catch (Exception e)
            {
                Trace.WriteLine(e.Message);
            }

            GC.SuppressFinalize(this);
        }

        ~FusionMessenger()
        {
            Dispose();
            GC.SuppressFinalize(this);
        }
        #endregion




        private bool _initialized;
        public bool Initialize()
        {
            if(_disposed)
                throw new ApplicationException("Object has already disposed!");

            if(_initialized)
                return true;


            _messageService.Start();
            _messageService.SubscribeChannel<IpcMetaMessage>(MetaChannel, DispatchExternEvents);
            _messageService.SubscribeChannel<ConnectionInfoMessage>(ConnectionChannel, DispatchConnectionEvents);
            _messageService.SubscribeChannel<IpcMessage>(MessageChannel, DippatchMessage);

            _initialized = true;
            return true;
        }

        internal ApplicationInfo Application{get { return _applicationInfo; }}

        private void DispatchExternEvents(string channel, ReceivedMessage<IpcMetaMessage> message)
        {
            // var extApp = message.SenderApplicationInfo.Name;
            var extApp = new ApplicationInfo(message.SenderApplicationInfo.Host,
                message.SenderApplicationInfo.Desktop,
                message.SenderApplicationInfo.User,
                message.SenderApplicationInfo.Name,
                message.SenderApplicationInfo.Instance);

            if (extApp.Host == Application.Host && extApp.Name == Application.Name && extApp.Instance == Application.Instance)
                return;

            var meta = message.Payload;

            

            var extMsgType = meta.ExternalMessageType != null ? Type.GetType(meta.ExternalMessageType) : null;
            var extView = new ViewMetaInfo(message.SenderApplicationInfo, meta.ExternalId, meta.ExternalView);

            _registrator.OnExternalEvent(meta.Action, extApp, extView, extMsgType);
        }

        private void DispatchConnectionEvents(string channel, ReceivedMessage<ConnectionInfoMessage> message)
        {
            var extApp = new ApplicationInfo(message.SenderApplicationInfo.Host,
                 message.SenderApplicationInfo.Desktop,
                 message.SenderApplicationInfo.User,
                 message.SenderApplicationInfo.Name,
                 message.SenderApplicationInfo.Instance);

            if (extApp.Host == Application.Host && extApp.Name == Application.Name && extApp.Instance == Application.Instance)
                return;

            var meta = message.Payload;

            var extMsgType = meta.ExternalMessageType != null ? Type.GetType(meta.ExternalMessageType) : null;

            var extView = new ViewMetaInfo(message.SenderApplicationInfo, meta.ExternalId, null);
            var localView = new ViewMetaInfo(Application, meta.LocalId, null);
            if (localView.FullId != meta.LocalFullId)
            {
                return;
            }

            var localMsgType = Type.GetType(meta.LocalMessageType);
            _registrator.OnExternalConnectionEvent(meta.Action, extApp, extMsgType, extView, localMsgType, localView, meta.ExternalAsPublisher);
        }

        private void DippatchMessage(string channel, ReceivedMessage<IpcMessage> message)
        {
            var extApp = new ApplicationInfo(message.SenderApplicationInfo.Host,
                 message.SenderApplicationInfo.Desktop,
                 message.SenderApplicationInfo.User,
                 message.SenderApplicationInfo.Name,
                 message.SenderApplicationInfo.Instance);

            if (extApp.Host == Application.Host && extApp.Name == Application.Name && extApp.Instance == Application.Instance)
                return;

            

            var extView = new ViewMetaInfo(message.SenderApplicationInfo, message.Payload.ExternalViewId, null);
            var extId = extView.FullId;

            var localId = ViewMetaInfo.GetLocalId(Application, extId);
            if (localId != null) // It is local message
                return;


            Type msgType = message.Payload.Content.GetType();
            _registrator.OnMessageRecieved(extId, msgType, message.Payload.Content);
        }

     


        public void NotifyRegister(Type msgType, string viewId, bool asPublisher)
        {
            var action = asPublisher
                             ? ExternalMessengerBase.MetaAction.RegisterPublisher
                             : ExternalMessengerBase.MetaAction.RegisterSubscriber;

            var ipc = new IpcMetaMessage
            {
                Action = action,
                ExternalId = viewId,
                ExternalMessageType = msgType.AssemblyQualifiedName
            };

            _messageService.PublishChannel(MetaChannel, ipc);
        }

        public void NotifyUnregister(Type msgType, string viewId, bool asPublisher)
        {
            var action = asPublisher
                             ? ExternalMessengerBase.MetaAction.UnregisterPublisher
                             : ExternalMessengerBase.MetaAction.UnregisterSubscriber;

            var ipc = new IpcMetaMessage
            {
                Action = action,
                ExternalId = viewId,
                ExternalMessageType = msgType.AssemblyQualifiedName
            };

            _messageService.PublishChannel(MetaChannel, ipc);
        }



      

       /* private void NotifyAllViewAdded(IWindowViewContainer view, string tab)
        {
            if (_registrator.GetPublishedTypes(view.ID).Count() == 0 && _registrator.GetSubscribedTypes(view.ID).Count() == 0)
                return;

            var viewInfo = new ViewMetaInfo(_applicationInfo, view.ID, view)
            {
                Title = view.Title,
                TabName = tab
            };

            NotifyAllViewAdded(viewInfo);
        }*/

        public void NotifAllViewRemoved(IWindowViewContainer view)
        {
            var viewInfo = new ViewMetaInfo(_applicationInfo, view.ID, view)
            {
                Title = view.Title
            };
            NotifyAllViewRemoved(viewInfo);
        }

        public void NotifAllViewUpdated(IWindowViewContainer view, string newTab)
        {


            if (_registrator.GetPublishedTypes(view.ID).Count() <= 0 && _registrator.GetSubscribedTypes(view.ID).Count() <= 0)
                return;
            var viewInfo = new ViewMetaInfo(_applicationInfo, view.ID, view)
            {
                TabName = newTab,
                Title = view.Title
            };
            NotifyAllViewUpdated(viewInfo);
        }

        public void NotifyAllViewAdded(ViewMetaInfo viewInfo)
        {
            var ipc = new IpcMetaMessage
            {
                Action = ExternalMessengerBase.MetaAction.ViewAdded,
                ExternalView = new ViewInfoMessage(viewInfo),
                ExternalId = viewInfo.LocalId
            };

            _messageService.PublishChannel(MetaChannel, ipc);
        }

        public void NotifyAllViewRemoved(ViewMetaInfo viewInfo)
        {

            var ipc = new IpcMetaMessage
            {
                Action = ExternalMessengerBase.MetaAction.ViewRemoved,
                ExternalView = new ViewInfoMessage(viewInfo),
                ExternalId = viewInfo.LocalId
            };

            _messageService.PublishChannel(MetaChannel, ipc);
        }

        public void NotifyAllViewUpdated(ViewMetaInfo viewInfo)
        {

            var ipc = new IpcMetaMessage
            {
                Action = ExternalMessengerBase.MetaAction.ViewUpdated,
                ExternalView = new ViewInfoMessage(viewInfo),
                ExternalId = viewInfo.LocalId
            };

            _messageService.PublishChannel(MetaChannel, ipc);
        }

        public void NotifyApplicationClosed()
        {
            var ipc = new IpcMetaMessage
            {
                Action = ExternalMessengerBase.MetaAction.ApplicationClosed
            };
            _messageService.PublishChannel(MetaChannel, ipc);
        }


      

        public void NotifyConnection<TLocalMsgType, TExtMsgType>(ExternalMessengerBase.ConnectionAction connection, string localViewId, ViewMetaInfo extView, bool extViewAsPublisher)
        {
            var localType = typeof (TLocalMsgType);
            var extType = typeof (TExtMsgType);
            
            var ipc = new ConnectionInfoMessage // publisher is external for others
            {
                Action = connection,
                ExternalAsPublisher = !extViewAsPublisher,
                ExternalId = localViewId,
                ExternalMessageAssembly =localType.Assembly.FullName,
                ExternalMessageType = localType.AssemblyQualifiedName,
                LocalId = extView.LocalId,
                LocalFullId = extView.FullId,
                LocalMessageType = extType.AssemblyQualifiedName,
                LocalMessageAssembly = localType.Assembly.FullName
            };
            _messageService.PublishChannel(ConnectionChannel, ipc);
            
        }

        public void RequestAllViews()
        {
            var ipc = new IpcMetaMessage
            {
                Action = ExternalMessengerBase.MetaAction.RequestAllViews
            };
            _messageService.PublishChannel(MetaChannel, ipc);
        }

      

        public void PublishToExternal(string loalViewId, object message)
        {
            var ipc = new IpcMessage
            {
                ExternalViewId = loalViewId,
                Content = message
            };
            _messageService.PublishChannel<IpcMessage>(MessageChannel, ipc);
        }

    }
}
