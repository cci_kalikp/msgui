﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;

namespace MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging.FusionMessaging
{
    [Serializable]
    internal class ModuleMessage
    {
        internal object Content;
        internal AppEnvironment Environment;
        //internal string Assembly;
        //internal string MsgType;
        internal CommunicationTargetFilter TargetFilter;
        internal string[] Location;
    }
}
