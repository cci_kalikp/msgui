﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging.FusionMessaging
{
    [Serializable]
    internal class IpcMetaMessage
    {
        internal ExternalMessengerBase.MetaAction Action { get; set; }

        /// <summary>
        /// relative to message reciever
        /// </summary> 
        internal ViewInfoMessage ExternalView { get; set; }

        internal string ExternalId { get; set; }
        internal string ExternalMessageType { get; set; }
        internal string ExternalMessageAssembly { get; set; }

        internal bool IsReceiveLastMessage { get; set; }
        internal bool ExternalAsPublisher { get; set; }

        /// <summary>
        /// relative to message reciever
        /// </summary>
        internal ViewInfoMessage LocalView { get; set; }
        
        internal bool RequireViewImg { get; set; }
        
    }

    

}
