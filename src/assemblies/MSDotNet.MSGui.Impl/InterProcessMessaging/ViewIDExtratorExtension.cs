﻿using System;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging
{
    internal static class ViewIdExtratorExtension
    {
        public static string GetId(this IViewContainerBase viewContainer)
        {
            // Avoid reflection if we can
            var windowViewContainer = viewContainer as WindowViewContainer;
            if (windowViewContainer != null)
            {
                return windowViewContainer.ID;
            }

            var idProperty = viewContainer.GetType().GetProperty("ID");
            if (idProperty == null)
                throw new InvalidOperationException("Cannot register with an view without ID");
            var id = idProperty.GetValue(viewContainer, null) as string;
            if (id == null)
                throw new InvalidOperationException("Cannot register with an view without valid ID");

            return id;
        }
    }
}
