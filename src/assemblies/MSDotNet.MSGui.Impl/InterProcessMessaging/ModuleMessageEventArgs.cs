﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging
{
    internal class ModuleMessageEventArgs:EventArgs
    {
        public ModuleMessageEventArgs(Type msgType, object content)
        {
            MessageType = msgType;
            Content = content;
        }

        public Type MessageType { get; private set; }
        public object Content { get; private set; }
    }
}
