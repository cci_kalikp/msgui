﻿using System;
using System.Collections.Generic;
using System.Linq;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.ViewModels;

namespace MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging
{
    internal class LocalViewManager: IDisposable
    {
        private readonly IWindowManager _windowManager;
        private readonly InterProcessMessenger _ipc;
        private readonly ApplicationInfo _applicationInfo;
        internal LocalViewManager(InterProcessMessenger ipc, IWindowManager windowManager)
        {
            _windowManager = windowManager;
            _ipc = ipc;
            _applicationInfo = _ipc.Application;

            _ipc.AllViewInfoRequested += AllViewRequestHandler;
        }

        void IDisposable.Dispose()
        {
            _ipc.AllViewInfoRequested -= AllViewRequestHandler;
        }

        private void AllViewRequestHandler(object sender, EventArgs args)
        {
            IEnumerable<WindowViewModel> windowCollection = null;

            if (_windowManager is TabbedDockViewModel)
            {
                var tabbedManager = _windowManager as TabbedDockViewModel;
                windowCollection = tabbedManager.Workspaces.ToList();

            }
            else if (_windowManager is FloatingOnlyDockViewModel)
            {
                var floatingManager = _windowManager as FloatingOnlyDockViewModel;
                windowCollection = floatingManager.Workspaces.ToList();
            }

            if (windowCollection == null)
            {
                throw new ApplicationException("Not supported window manager " + _windowManager.GetType());
            }


            foreach (var ws in windowCollection)
            {
                var tab = ws.Host as ShellTabViewModel;
                var viewInfo = new ViewMetaInfo(_applicationInfo, ws.ViewContainer.ID, ws.ViewContainer as IWindowViewContainer)
                {
                    TabName = tab == null ? null : tab.Title,
                    Title = ws.ViewContainer.Title
                };


               // bool isConnetable = false;
                foreach (var pt in _ipc.GetPublishedTypes(viewInfo.LocalId))
                {
                  //  isConnetable = true;
                    
                    _ipc.NotifyRegister(pt, viewInfo.LocalId, true);
                }
                foreach (var st in _ipc.GetSubscribedTypes(viewInfo.LocalId))
                {
                   // isConnetable = true;
                    _ipc.NotifyRegister(st, viewInfo.LocalId, false);
                }
                //if (isConnetable) 
                    _ipc.NotifyAllViewUpdated(viewInfo);
            }

        }
    }
}
