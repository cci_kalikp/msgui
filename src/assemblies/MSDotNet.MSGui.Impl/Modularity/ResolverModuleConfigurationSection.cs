﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Modularity/ResolverModuleConfigurationSection.cs#7 $
// $Change: 861162 $
// $DateTime: 2014/01/08 12:20:01 $
// $Author: anbuy $

using System.Configuration;
using Microsoft.Practices.Prism.Modularity;


namespace MorganStanley.MSDotNet.MSGui.Impl.Modularity
{
  /// <summary>
  /// A <see cref="ConfigurationSection"/> for module configuration.
  /// </summary>
  public class ResolverModuleConfigurationSection : ConfigurationSection
  {
    /// <summary>
    /// Gets or sets the collection of modules configuration.
    /// </summary>
    /// <value>A <seealso cref="ModuleConfigurationElementCollection"/> of <seealso cref="ModuleConfigurationElement"/>.</value>
    [ConfigurationProperty("", IsDefaultCollection = true, IsKey = false)]
    public ResolverModuleConfigurationElementCollection Modules
    {
      get { return (ResolverModuleConfigurationElementCollection)base[string.Empty]; }
      set { base[string.Empty] = value; }
    }
  }
}