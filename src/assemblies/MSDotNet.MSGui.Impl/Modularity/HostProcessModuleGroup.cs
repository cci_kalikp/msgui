﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Xml.Linq;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Persistence.DelayedLoading;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.Factories;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ProcessIsolation;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;

namespace MorganStanley.MSDotNet.MSGui.Impl.Modularity
{
    internal sealed class HostProcessModuleGroup : ModuleGroupBase
    {
        private readonly IDictionary<string, IWindowViewContainer> _delayLoadHosts;
        private readonly ConcurrentDictionary<string, IRemoteWindowFactory> _remoteWindowFactories;
        private readonly IChromeManager _chromeManager;
        private readonly IWindowFactory _windowFactory;
        private readonly IChromeElementInitializerRegistry _chromeRegistry;
        private readonly IList<IDelayedLoadModule> _modules = new List<IDelayedLoadModule>();
        private readonly object _moduleCountersLock = new object();
        private readonly object _viewsCounterLock = new object();

        private IDelayedProfileLoader _profileLoader;
        private int _modulesInitialized, _modulesFailed;
        private int _viewsLoaded;

        internal HostProcessModuleGroup(IUnityContainer container, IChromeManager chromeManager,
                                        IWindowFactory windowFactory, IChromeElementInitializerRegistry chromeRegistry)
            : base(container)
        {
            _chromeRegistry = chromeRegistry;
            _windowFactory = windowFactory;
            _chromeManager = chromeManager;
            _delayLoadHosts = new Dictionary<string, IWindowViewContainer>();
            _remoteWindowFactories = new ConcurrentDictionary<string, IRemoteWindowFactory>();
        }
        
        public override void RegisterDelayedLoadModule(IDelayedLoadModule module)
        {
            lock (_moduleCountersLock)
            {
                _modules.Add(module);                
            }
        }

        public override void DelayedLoadModuleInitialized(IDelayedLoadModule module)
        {
            lock (_moduleCountersLock)
            {
                _modulesInitialized++;
                ProcessModuleUpdate();                
            }
        }

        public override void DelayedLoadModuleInitializationFailed(IDelayedLoadModule module)
        {
            lock (_moduleCountersLock)
            {
                _modulesFailed++;
                ProcessModuleUpdate();
            }
        }

        public override bool IsWindowFactoryInGroup(string factoryId)
        {
            return _remoteWindowFactories.ContainsKey(factoryId);
        }

        public override IWindowViewContainer CreateGroupWindow(string factoryId, InitialWindowParameters parameters)
        {
            var remoteCreator = _remoteWindowFactories[factoryId];

            // Create the placeholder using isolated factory
            var wvc = _chromeManager.CreateWindow(IsolatedWindowFactories.IsolatedProcessWindowFactoryName, parameters);
            remoteCreator.CreateRemoteWindow(factoryId, wvc.ID);

            return wvc;
        }

        public override void RegisterSubsystemWindowFactory(IRemoteWindowFactory remoteFactory, IEnumerable<string> windowFactories)
        {
            foreach (var windowFactory in windowFactories)
            {
                ((IDictionary<string, IRemoteWindowFactory>) _remoteWindowFactories).Add(windowFactory, remoteFactory);
            }
        }

        public override void UnRegisterSubsystemWindowFactory(IEnumerable<string> windowFactories)
        {
            var chromeRegistry = _chromeRegistry as ChromeRegistry;

            foreach (var windowFactory in windowFactories)
            {
                IRemoteWindowFactory remoteFactory;
                _remoteWindowFactories.TryRemove(windowFactory, out remoteFactory);

                // Unregister them in chrome registry
                if (chromeRegistry != null)
                {
                    chromeRegistry.UnregisterWindowFactory(windowFactory);
                }
            }
        }


        private void ProcessModuleUpdate()
        {
            if (_modules.Count == _modulesInitialized + _modulesFailed)
            {
                ProfileLoader.ReloadCurrentProfile();
            }
        }

        private IDelayedProfileLoader ProfileLoader
        {
            get
            {
                if (_profileLoader == null)
                {
                    _profileLoader = Container.Resolve<IDelayedProfileLoader>();
                }

                return _profileLoader;
            }
        }

        public override void CreateWindowFromLayout(string factoryId, InitialWindowParameters initialParameters, string viewId,
                                           XDocument stateToPass, XDocument viewState, Action<IWindowViewContainer> postCreateHandler)
        {
            var persistenceProfileService = Container.Resolve<PersistenceProfileService>(); 

            if (!ChromeManagerBase.SequentialViewLoad)
            {
                persistenceProfileService.DoDispatchedAction(() => CreateWindowFormLayoutCore(factoryId, initialParameters, viewId, stateToPass, viewState,
                                                                                              postCreateHandler));
            }
            else
            {
                var loadedEvent = new AutoResetEvent(false);
                persistenceProfileService.DoDispatchedAction(() =>
                    {
                        IFeedbackWindow feedbackWindow = null;
                        EventHandler<EventArgs>[] fullyLoaded = {null};
                        FrameworkElement element = null;
                        RoutedEventHandler[] loaded = {null};
                        try
                        {
                            var window = CreateWindowFormLayoutCore(factoryId, initialParameters, viewId, stateToPass, viewState,
                           postCreateHandler);
                            if (window == null)
                            {
                                loadedEvent.Set();
                                return;
                            }
                            feedbackWindow = window.Content as IFeedbackWindow;
                            if (feedbackWindow != null)
                            {
                                feedbackWindow.IsLayoutLoadActive = true;
                                feedbackWindow.FullyLoaded += fullyLoaded[0] = (sender, args) =>
                                    {
                                        loadedEvent.Set();
                                        feedbackWindow.FullyLoaded -= fullyLoaded[0];
                                        fullyLoaded[0] = null;
                                    };
                            }
                            else
                            {
                                element = window.Content as FrameworkElement;
                                if (element == null || element.IsLoaded)
                                {
                                    loadedEvent.Set();
                                    return;
                                }
                                element.Loaded += loaded[0] = (sender, args) =>
                                    {
                                        loadedEvent.Set();
                                        element.Loaded -= loaded[0];
                                        loaded[0] = null;
                                    };

                            }
                        }
                        catch
                        {
                            if (fullyLoaded[0] != null && feedbackWindow != null)
                            {
                                feedbackWindow.FullyLoaded -= fullyLoaded[0];
                            }
                            else if (loaded[0] != null && element != null)
                            {
                                element.Loaded -= loaded[0];
                            }
                            loadedEvent.Set();
                            throw;
                        }  
                    }, false);

                if (WaitHandle.WaitAny(new WaitHandle[] { loadedEvent, ChromeManagerBase.CancelLayoutLoading }, ChromeManagerBase.ViewLoadingTimeout) == 0)
                {
                    //if (loadedEvent.WaitOne(ChromeManagerBase.ViewLoadingTimeout))  
                    Thread.Sleep(ChromeManagerBase.ViewLoadingWaitingTime);
                }

            }
        }

        private IWindowViewContainer CreateWindowFormLayoutCore(string factoryId, InitialWindowParameters initialParameters, string viewId,
                                           XDocument stateToPass, XDocument viewState, Action<IWindowViewContainer> postCreateHandler)
        {
            IWindowViewContainer windowViewContainer;

            // Isolated host window treated differently - as a delay-load host.
            if (!factoryId.Equals(IsolatedWindowFactories.IsolatedProcessWindowFactoryName))
            {
                windowViewContainer = _windowFactory.CreateWindowInstance(factoryId, initialParameters, viewId, false,
                                                                          stateToPass, viewState);

            }
            else
            {
                var isolatedViewParameters = new InitialIsolatedViewParameters(viewId, null, stateToPass, initialParameters);

                // Check stateToPass to see if it contains valid state. We can't create an isolated window without proper state. 
                // The state must contain factory ID as an attribute
                if (stateToPass == null || stateToPass.Root == null
                    || stateToPass.Root.Attributes().FirstOrDefault(a => a.Name == "FactoryId") == default(XAttribute))
                {
                    // Create a panel indicating an error message. It will be isolated control's panel.
                    // We won't expect a window coming in
                    var textBlock = new TextBlock
                        {
                            Text = "There was an error saving this window. Please close this panel and the view."
                        };

                    windowViewContainer = new WindowViewContainer(isolatedViewParameters,
                                                                  IsolatedWindowFactories.IsolatedProcessWindowFactoryName)
                        {
                            Content = textBlock
                        };
                }
                else
                {
                    windowViewContainer = _windowFactory.CreateWindowInstance(
                        IsolatedWindowFactories.IsolatedProcessWindowFactoryName,
                        isolatedViewParameters,
                        viewId,
                        false,
                        stateToPass,
                        viewState);

                    _delayLoadHosts[viewId] = windowViewContainer;
                }
            }

            if (postCreateHandler != null)
            {
                postCreateHandler(windowViewContainer);
            }

            return windowViewContainer;
        }

        public override bool ViewLoadedCompletely(XDocument connectionState)
        {
            lock (_viewsCounterLock)
            {
                var views = _chromeManager.GetViews();
                var totalViews = views.Count(w => w.Content != null && ContentTypes.Contains(w.Content.GetType()));
                _viewsLoaded++;
                if (_viewsLoaded >= totalViews)
                {
                    ProfileLoader.LoadConnections(connectionState);
                    _viewsLoaded = 0;
                    return true;
                }
                return false;
            }
        }

        internal bool TryGetDelayLoadWindow(string viewId, out IWindowViewContainer windowViewContainer)
        {
            return _delayLoadHosts.TryGetValue(viewId, out windowViewContainer);
        }

        public override void SubscribeM2M(string messageType)
        {
        }

        public override void PublishM2M(object message)
        {
        }
    }
}
