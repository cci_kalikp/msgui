﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Modularity/ResolverModuleConfigurationElement.cs#8 $
// $Change: 820226 $
// $DateTime: 2013/03/14 12:04:44 $
// $Author: milosp $

using System;
using System.Collections.Specialized;
using System.Configuration;
using Microsoft.Practices.Composite.Modularity;


namespace MorganStanley.MSDotNet.MSGui.Impl.Modularity
{
  /// <summary>
  /// A configuration element to declare module metadata.
  /// </summary>
  public class ResolverModuleConfigurationElement : ConfigurationElement
  {
	private StringDictionary extras = new StringDictionary();
	  protected override bool OnDeserializeUnrecognizedAttribute(string name, string value)
	  {
		extras.Add(name,value);
		return true;
		//  return base.OnDeserializeUnrecognizedAttribute(name, value);
	  }

	/// <summary>
	/// Initializes a new instance of the <see cref="ResolverModuleConfigurationElement"/> class.
	/// </summary>
	public ResolverModuleConfigurationElement()
	{
	}

	/// <summary>
	/// Initializes a new instance of the <see cref="ResolverModuleConfigurationElement"/> class.
	/// </summary>
	/// <param name="assemblyFile_">The assembly file where the module is located.</param>
	/// <param name="assemblyVersion_">The version of the modules assembly.</param>
	/// <param name="culture_">The culture of the modules assembly.</param>
	/// <param name="assemblyName_">The name of the modules assembly.</param>
	/// <param name="moduleType_">The type of the module.</param>
	/// <param name="moduleName_">The name of the module.</param>
	/// <param name="startupLoaded_">This attribute specifies whether the module is loaded at startup.</param>
	/// <param name="loadModuleBundle_">This attribute specifies whether to load the msde.config for the module.</param>
	/// <param name="publicKeyToken_">The public key of the modules assembly.</param>
	public ResolverModuleConfigurationElement(string assemblyFile_, string assemblyVersion_, string publicKeyToken_, string culture_, string assemblyName_, string moduleType_, string moduleName_, bool startupLoaded_, bool loadModuleBundle_)
	{
	  base["assemblyFile"] = assemblyFile_;
	  base["assemblyName"] = assemblyName_;
	  base["moduleType"] = moduleType_;
	  base["moduleName"] = moduleName_;
	  base["startupLoaded"] = startupLoaded_;
	  base["assemblyVersion"] = assemblyVersion_;
	  base["publicKeyToken"] = publicKeyToken_;
	  base["culture"] = culture_;      
	  base["loadModuleBundle"] = loadModuleBundle_;
	}

    [Obsolete]
    public new string this[string key]
    {
        get
        {
            return extras[key];
        }
    }

      public StringDictionary ExtraValues
      {
          get { return extras; }
      }

		/// <summary>
	/// Gets or sets a value indicating whether to load the msde.config file of the module.
	/// </summary>
	/// <value>true to load the msde.config file of the module.</value>
	[ConfigurationProperty("loadModuleBundle", IsRequired = false, DefaultValue = true)]
	public bool LoadModuleBundle
	{
	  get { return (bool)base["loadModuleBundle"]; }
	  set { base["loadModuleBundle"] = value; }
	}

	/// <summary>
	/// Gets or sets the assembly file.
	/// </summary>
	/// <value>The assembly file.</value>
	[ConfigurationProperty("assemblyFile", IsRequired = false, DefaultValue = "")]
	public string AssemblyFile
	{
	  get { return (string)base["assemblyFile"]; }
	  set { base["assemblyFile"] = value; }
	}

	/// <summary>
	/// Gets or sets the assembly name.
	/// </summary>
	/// <value>The assembly file.</value>
	[ConfigurationProperty("assemblyName", IsRequired = false, DefaultValue = "")]
	public string AssemblyName
	{
	  get { return (string)base["assemblyName"]; }
	  set { base["assemblyName"] = value; }
	}

	/// <summary>
	/// Gets or sets the assembly name.
	/// </summary>
	/// <value>The assembly file.</value>
	[ConfigurationProperty("assemblyVersion", IsRequired = false, DefaultValue = "0.0.0.0")]
	public string AssemblyVersion
	{
	  get { return (string)base["assemblyVersion"]; }
	  set { base["assemblyVersion"] = value; }
	}

	/// <summary>
	/// Gets or sets the assembly name.
	/// </summary>
	/// <value>The assembly file.</value>
	[ConfigurationProperty("publicKeyToken", IsRequired = false, DefaultValue = "null")]
	public string PublicKeyToken
	{
	  get { return (string)base["publicKeyToken"]; }
	  set { base["publicKeyToken"] = value; }
	}

	/// <summary>
	/// Gets or sets the assembly name.
	/// </summary>
	/// <value>The assembly file.</value>
	[ConfigurationProperty("culture", IsRequired = false, DefaultValue = "neutral")]
	public string Culture
	{
	  get { return (string)base["culture"]; }
	  set { base["culture"] = value; }
	}

	/// <summary>
	/// Gets or sets the module type.
	/// </summary>
	/// <value>The module's type.</value>
	[ConfigurationProperty("moduleType", IsRequired = true)]
	public string ModuleType
	{
	  get { return (string)base["moduleType"]; }
	  set { base["moduleType"] = value; }
	}

	/// <summary>
	/// Gets or sets the module name.
	/// </summary>
	/// <value>The module's name.</value>
	[ConfigurationProperty("moduleName", IsRequired = true)]
	public string ModuleName
	{
	  get { return (string)base["moduleName"]; }
	  set { base["moduleName"] = value; }
	}

	/// <summary>
	/// Gets or sets a value indicating whether the module should be loaded at startup.
	/// </summary>
	/// <value>A value indicating whether the module should be loaded at startup.</value>
	[ConfigurationProperty("startupLoaded", IsRequired = false, DefaultValue = true)]
	public bool StartupLoaded
	{
	  get { return (bool)base["startupLoaded"]; }
	  set { base["startupLoaded"] = value; }
	}

	/// <summary>
	/// Gets or sets the modules this module depends on.
	/// </summary>
	/// <value>The names of the modules that this depends on.</value>
	[ConfigurationProperty("dependencies", IsDefaultCollection = true, IsKey = false)]
	public ModuleDependencyCollection Dependencies
	{
	  get { return (ModuleDependencyCollection)base["dependencies"]; }
	  set { base["dependencies"] = value; }
	}
  }
}
