﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Modularity/ResolverConfigurationModuleCatalog.cs#20 $
// $Change: 874505 $
// $DateTime: 2014/04/02 03:13:42 $
// $Author: caijin $

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text; 
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core; 
using MorganStanley.MSDotNet.My;

namespace MorganStanley.MSDotNet.MSGui.Impl.Modularity
{
	/// <summary>
	/// A <see cref="ModuleCatalog"/> that gets the modules metadata from
	/// configuration files.
	/// </summary>
	/// <remarks>
	/// This ModuleCatalog reads the module information from a config section similar to the 'modules' section of the
	/// Composite Application Library (Prism 2.0)'s <see cref="ConfigurationModuleCatalog"/>.
	/// The modules can be specified by assembly name if the <see cref="Runtime.AssemblyResolver"/> is able to resolve them.
	/// By default the module catalog loads the .msde.config file for each module's assembly,
	/// which can be disabled by adding the 'loadModuleBundle="false"' attribute.
	/// </remarks>
	/// <example>
	/// <para>
	/// An example of the modules configuration file is shown below.
	/// </para>
	/// <code language="xml">
	/// <![CDATA[
	///     <?xml version="1.0" encoding="utf-8" ?>
	///     <configuration>
	///         <configSections>
	///             <section name="modules" type="MorganStanley.MSDotNet.MSGui.Impl.Modularity.ResolverModulesConfigurationSection, MSDotNet.MSGui.Impl"></section>
	///         </configSections>
	///         <modules>
	///             <module moduleType="MorganStanley.MSDotNet.CafGUI.Toolkit.Examples.HistoryModule"
	///                     assemblyName="HistoryModule"
	///                     moduleName="HistoryModule"
	///                     startupLoaded="true"/>
	///             <module moduleType="MorganStanley.MSDotNet.CafGUI.Toolkit.Examples.DataGridModule"
	///                     assemblyName="DataGridModule"
	///                     moduleName="DataGridModule"
	///                     loadModuleBundle="false"
	///                     startupLoaded="true">
	///             </module>
	///             <module moduleType="MorganStanley.MSDotNet.CafGUI.Toolkit.Examples.BrowserModule"
	///                     assemblyFile="\\san01b\DevAppsGML\dist\msdotnet\proj\cafgui\2009.42\examples\CafGUI.Toolkit\Composite\BrowserModule\BrowserModule.dll"
	///                     moduleName="BrowserModule"
	///                     startupLoaded="true">
	///                 <dependencies>
	///                     <dependency moduleName="HistoryModule"/>
	///                 </dependencies>
	///             </module>
	///         </modules>
	///     </configuration>
	/// ]]>
	/// </code>
	/// <para>
	/// The 'moduleName' attribute identifies the module, so it can be referenced in the 'dependencies' element.
	/// The 'moduleType' attribute holds the namespace qualified name of the class that implements the <see cref="IModule"/> interface.
	/// The 'loadModuleBundle' attribute specifies whether to load the .msde.config for the module.
	/// The 'startupLoaded' attribute specifies whether the module is loaded at startup.
	/// The 'assemblyVersion' attribute specifies the version of the module's assembly.
	/// The 'assemblyFile' attribute specifies the absolute path to the assembly that holds the module class.
	/// The 'assemblyName' attribute specifies the name of the module's assembly.
	/// </para>
	/// <para>
	/// The 'assemblyFile' attribute can be omitted if the AssemblyResolver can resolve the assembly from the 'assemblyName'.
	/// </para>
	/// </example>
    public class ResolverConfigurationModuleCatalog : ModuleCatalog, IInterceptableModuleCatalog, IModuleInfoInterceptor
	{
		private static readonly IMSLogger m_logger = MSLoggerFactory.CreateLogger<ResolverConfigurationModuleCatalog>();
        protected readonly IModuleLoadInfos m_moduleLoadInfos;
		/// <summary>
		/// Gets or sets the store where the configuration is kept.
		/// </summary>
		public IResolverConfigurationStore Store
		{
			get;
			set;
		}

        private readonly List<IModuleInfoInterceptor> interceptors = new List<IModuleInfoInterceptor>();

		internal IList<Type> m_preregistrations;
		internal IList<Tuple<Type, string>> m_preregistrationsWithNames;
		internal IList<ModuleInfo> m_moduleinfos;

		/// <summary>
		/// Initializes a new instance of the <see cref="ResolverConfigurationModuleCatalog"/> class.
		/// </summary>
		/// <remarks>
		/// It sets the <see cref="Store"/> to use the app.config for retrieving module configuration.
		/// </remarks>
		public ResolverConfigurationModuleCatalog()
		{
			Store = new ResolverConfigurationStore();
			m_preregistrations = new List<Type>(); 
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ResolverConfigurationModuleCatalog"/> class.
		/// </summary>
		/// <remarks>
		/// It sets the <see cref="Store"/> to use the app.config for retrieving module configuration.
		/// </remarks>
		public ResolverConfigurationModuleCatalog(IList<Type> preregistrations)
		{
			Store = new ResolverConfigurationStore();
			m_preregistrations = preregistrations; 
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ResolverConfigurationModuleCatalog"/> class from a file or directory path.
		/// </summary>
		/// <param name="path_">The path to a file or a directory from which to start searching for the
		/// configuration files.</param>
		public ResolverConfigurationModuleCatalog(string path_, IList<Type> preregistrations, IList<Tuple<Type, string>> pregistrationsWithNames, IList<ModuleInfo> moduleInfos,
            IModuleLoadInfos moduleLoadInfos)
		{
			Store = new ResolverConfigurationStore(path_);
			m_preregistrations = preregistrations;
			m_preregistrationsWithNames = pregistrationsWithNames;
			m_moduleinfos = moduleInfos;
		    m_moduleLoadInfos = moduleLoadInfos; 
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ResolverConfigurationModuleCatalog"/> class from a file or directory path.
		/// </summary>
		/// <param name="path_">The path to a file or a directory from which to start searching for the
		/// configuration files.</param>
        public ResolverConfigurationModuleCatalog(IList<Type> preregistrations, IList<Tuple<Type, string>> pregistrationsWithNames, IList<ModuleInfo> moduleInfos,
            IModuleLoadInfos moduleLoadInfos)
		{
			Store = new ResolverConfigurationStore();
			m_preregistrations = preregistrations;
			m_preregistrationsWithNames = pregistrationsWithNames;
			m_moduleinfos = moduleInfos;
            m_moduleLoadInfos = moduleLoadInfos; 
        }

		/// <summary>
		/// Initializes a new instance of the <see cref="ResolverConfigurationModuleCatalog"/> class from a file or directory path.
		/// </summary>
		/// <param name="path_">The path to a file or a directory from which to start searching for the
		/// configuration files.</param>
		public ResolverConfigurationModuleCatalog(string path_, IList<Type> preregistrations)
		{
			Store = new ResolverConfigurationStore(path_);
			m_preregistrations = preregistrations; 
		}

	    private bool initialized = false;
        public override IEnumerable<ModuleInfo> Modules
        {
            get
            {
                if (!initialized)
                {
                    Initialize();
                }
                return base.Modules;
            }
        }

		/// <summary>
		/// Loads the catalog from the configuration.
		/// </summary>
		protected override void InnerLoad()
		{
            initialized = true;
            var moduleInfos = GetModuleInfos();
			AddModules(moduleInfos);
		}

        protected virtual ResolverModuleInfo GetModuleInfo(string moduleName, Type type)
	    {
            return new ResolverModuleInfo(moduleName, type.AssemblyQualifiedName);
	    }

        protected virtual IList<ModuleInfo> GetModuleInfos()
        {
            var stack = new Stack<IModuleInfoInterceptor>();
            stack.Push(this);
            
            for (int i = 0; i < this.interceptors.Count; i++) //foreach does not guarantee in-order iteration!
            {
                stack.Push(this.interceptors[i]);
            }

            var modules = m_moduleinfos == null ? new List<ModuleInfo>() : new List<ModuleInfo>(m_moduleinfos);
            stack.Pop().Intercept(modules, stack);
            return modules;
        }

	    protected virtual void AddModules(IList<ModuleInfo> moduleInfos)
	    {
	        foreach (ModuleInfo moduleInfo in moduleInfos)
	        {
                if (m_moduleLoadInfos != null) m_moduleLoadInfos.AddModuleLoadInfo(moduleInfo);
	            AddModule(moduleInfo);
	        }
	    }

        void IModuleInfoInterceptor.Intercept( IList<ModuleInfo>  modules_, Stack<IModuleInfoInterceptor> interceptors_)
        {

            IResolverConfigurationStore afsStore = Store;
            if (afsStore == null)
            {
                throw new InvalidOperationException("The Configuration store must not be null.");
            }

            ResolverModuleConfigurationSection section = afsStore.RetrieveModuleConfigurationSection();
            IList<ResolverModuleInfo> controlParadigmImplementatorModule = new List<ResolverModuleInfo>();
            if (interceptors_.Count > 0)
            {
                interceptors_.Pop().Intercept(modules_, interceptors_);
            }

            if (section != null)
            {
                foreach (ResolverModuleConfigurationElement element in section.Modules)
                {
                    ResolverModuleInfo info = GetModuleInfo(element);
                    if (info == null || modules_.FirstOrDefault(m_ => m_.ModuleName == info.ModuleName) != null)
                        continue;
                    var type = Type.GetType(info.ModuleType);
                    if (type == null)
                    {
                        IModuleLoadInfo moduleLoadInfo = m_moduleLoadInfos.AddModuleLoadInfo(info);
                        moduleLoadInfo.Status = ModuleStatus.TypeResolutionFailed;
                        int index = info.ModuleType.IndexOf(",", System.StringComparison.Ordinal);
                        string moduleTypeName = info.ModuleType.Substring(0, index);
                        string moduleAssemblyName = info.ModuleType.Substring(index + 1).Trim();
                        moduleLoadInfo.TypeNotFoundReason =
                            string.Format("Failed to load Type {0} from Assembly {1} ", moduleTypeName,
                                          moduleAssemblyName);
                        continue;
                    }
                    if (type.GetInterface(typeof(IControlParadigmImplementatorModule).FullName) != null)
                        controlParadigmImplementatorModule.Add(info);
                    else
                    {
                        if (EnforceModuleLoadingDependency)
                        {
                            foreach (var moduleInfo in modules_)
                            {
                                info.DependsOn.Add(moduleInfo.ModuleName);
                            }
                        }
                        modules_.Add(info);
                    }
                }
            }
            foreach (Type type in m_preregistrations)
            {
                string moduleName = DecodeGenericTypeName(type);
                if (type.GetInterface(typeof(IControlParadigmImplementatorModule).FullName) != null)
                    controlParadigmImplementatorModule.Add(GetModuleInfo(moduleName, type));
                else
                {
                    var mi = GetModuleInfo(moduleName, type);
                    if (EnforceModuleLoadingDependency)
                    {
                        foreach (var moduleInfo in modules_)
                        {
                            mi.DependsOn.Add(moduleInfo.ModuleName);
                        }
                    }
                    modules_.Add(mi);
                }
            }
            if (m_preregistrationsWithNames != null)
            {
                foreach (var tuple in m_preregistrationsWithNames)
                {
                    Type type = tuple.Item1;
                    string moduleName = tuple.Item2;
                    if (type.GetInterface(typeof(IControlParadigmImplementatorModule).FullName) != null)
                        controlParadigmImplementatorModule.Add(GetModuleInfo(moduleName, type));
                    else
                    {
                        var mi = GetModuleInfo(moduleName, type);
                        if (EnforceModuleLoadingDependency)
                        {
                            foreach (var moduleInfo in modules_)
                            {
                                mi.DependsOn.Add(moduleInfo.ModuleName);
                            }
                        }
                        modules_.Add(mi);
                    }
                }
            }

            if (controlParadigmImplementatorModule.Count > 0)
            {
                foreach (ResolverModuleInfo resolverModuleInfo in controlParadigmImplementatorModule)
                {
                    foreach (var moduleInfo in modules_)
                    {
                        resolverModuleInfo.DependsOn.Add(moduleInfo.ModuleName);
                    }
                    modules_.Add(resolverModuleInfo);
                }
            }  
        }
	    /// <summary>
		/// Creates a <see cref="ModuleInfo"/> class
		/// based on the elements data.
		/// </summary>
		/// <returns><see cref="ResolverModuleInfo"/> that represents this module.</returns>
		/// <param name="element_">The element to build the module info from.</param>
		protected virtual ResolverModuleInfo GetModuleInfo(ResolverModuleConfigurationElement element_)
		{
			Assembly assembly;
			string assemblyFileName = string.Empty;

	        // If the AssemblyFile is not specified
			// Manually attempt to load it
			if (string.IsNullOrEmpty(element_.AssemblyFile))
			{
				// Assembly definition for this module
				var sb = new StringBuilder();
				sb.Append(element_.AssemblyName);
				sb.Append(", Version=");
				sb.Append(String.IsNullOrEmpty(element_.AssemblyVersion) ? "0.0.0.0" : element_.AssemblyVersion);
				sb.Append(", Culture=");
				sb.Append(String.IsNullOrEmpty(element_.Culture) ? "neutral" : element_.Culture);
				sb.Append(", PublicKeyToken=");
				sb.Append(String.IsNullOrEmpty(element_.PublicKeyToken) ? "null" : element_.PublicKeyToken);
				string assemblyToLoad = sb.ToString();

				// Get the type of the module to load
				// This causes the module to be loaded into the current AppDomain
				// This will be resolved by the assembly resolver
				// if the module has not been loaded yet.
				// Note: AssemblyVersion = 0.0.0.0 will cause the most precedent
				// version of this assembly to load (specified by the MSDE config)
				// type version independence is a _well_ known behaviour of the
				// assembly resolver.

				// can throw IOException!
				try
				{
					assembly = Assembly.Load(assemblyToLoad);
				}
				catch (Exception exc)
				{
					m_logger.Error(
						string.Format("Assembly {0} couldn't be loaded for module {1}", assemblyToLoad, element_.ModuleType), exc);
				    IModuleLoadInfo info = m_moduleLoadInfos.AddModuleLoadInfo(element_.ModuleType + ", " + assemblyToLoad, ModuleStatus.TypeResolutionFailed);
				    info.Error = exc;
					return null;
				}
				assemblyFileName = assembly.Location;
			}
			else
			{
				try
				{
					assemblyFileName = PathUtilities.GetAbsolutePath(element_.AssemblyFile);
					assembly = Assembly.LoadFile(assemblyFileName);
				}
				catch (Exception exc)
				{
					m_logger.Error(
						string.Format("Assembly {0} couldn't be loaded for module {1}", assemblyFileName, element_.ModuleType), exc);
                    IModuleLoadInfo info = m_moduleLoadInfos.AddModuleLoadInfo(element_.ModuleType + ", " + assemblyFileName, ModuleStatus.TypeResolutionFailed);
				    info.Error = exc;
                    return null;
				}

			}

			if (element_.LoadModuleBundle)
			{
				//Attempt to load the MSDE config file from the
				//AssemblyFile Location
				string msdeConfigFile = assemblyFileName + ".msde.config";
				if (File.Exists(msdeConfigFile))
				{
					AssemblyResolverLoader.Load(msdeConfigFile);
				}
			}

			string moduleType = element_.ModuleType;

			// correct the module name to an assembly qualified name
			if (!moduleType.Contains(","))
			{
				moduleType += ", " + assembly.FullName;
			}

			//Get all the dependencies of this module.
			string[] dependencies = element_.Dependencies.OfType<ModuleDependencyConfigurationElement>().Select(e => e.ModuleName).ToArray();

			return new ResolverModuleInfo(element_.ModuleName, moduleType, dependencies)
			{
				Ref = GetFileAbsoluteUri(assemblyFileName),
				InitializationMode = element_.StartupLoaded ? InitializationMode.WhenAvailable : InitializationMode.OnDemand,
				ExtraValues = element_.ExtraValues 
			};
		}

		protected static string GetFileAbsoluteUri(string filePath_)
		{
			UriBuilder uriBuilder = new UriBuilder();
			uriBuilder.Host = String.Empty;
			uriBuilder.Scheme = Uri.UriSchemeFile;
			uriBuilder.Path = Path.GetFullPath(filePath_);
			Uri fileUri = uriBuilder.Uri;

			return fileUri.ToString();
		}

		internal static bool EnforceModuleLoadingDependency { get; set; }

		internal static string DecodeGenericTypeName(Type type)
		{
			if (type.IsGenericType)
			{
				var name = type.GetGenericTypeDefinition().Name;
				var index = name.LastIndexOf("`", System.StringComparison.Ordinal);
				if (index >= 0)
				{
					name = name.Substring(0, index);
				}
				return name + "<" + string.Join(", ", type.GetGenericArguments().Select(DecodeGenericTypeName)) + ">";
			}
			return type.Name;
		}
 
        void IInterceptableModuleCatalog.AddInterceptor(IModuleInfoInterceptor interceptor_)
        {
            if (!interceptors.Contains(interceptor_))
            {
                interceptors.Add(interceptor_);
            }
        }
    }

	internal static class AssemblyResolverLoader
	{
		internal static void Load(string msdeConfigFile)
		{
			Runtime.AssemblyResolver.Load(msdeConfigFile);
		}
	}

// class
}
