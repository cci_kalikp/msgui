﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.Modularity;

namespace MorganStanley.MSDotNet.MSGui.Impl.Modularity.Prism
{

    /// <summary>
    /// Defines the metadata that describes a module
    /// which could exist on AFS.
    /// </summary>
    [Serializable]
    public class ResolverPrismModuleInfo : ModuleInfo, IMSDesktopModuleInfo
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ResolverModuleInfo"/> class.
        /// </summary>
        public ResolverPrismModuleInfo()
            : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ResolverModuleInfo"/> class.
        /// </summary>
        /// <param name="name_">The module's name.</param>
        /// <param name="type_">The module <see cref="Type"/>'s AssemblyQualifiedName.</param>
        /// <param name="dependsOn_">The modules this instance depends on.</param>
        public ResolverPrismModuleInfo(string name_, string type_, params string[] dependsOn_)
            : base(name_, type_, dependsOn_)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ResolverModuleInfo"/> class.
        /// </summary>
        /// <param name="name_">The module's name.</param>
        /// <param name="type_">The module's type.</param>
        public ResolverPrismModuleInfo(string name_, string type_)
            : base(name_, type_)
        {
        }

        /// <summary>
        /// Gets the assembly name.
        /// </summary>
        /// <value>The assembly name of the module.</value>
        public string AssemblyName { get; private set; }

        /// <summary>
        /// Gets extra values expressed as unrecognized attributes of the configuration element.
        /// </summary>
        public StringDictionary ExtraValues { get; internal set; }
    }
}
