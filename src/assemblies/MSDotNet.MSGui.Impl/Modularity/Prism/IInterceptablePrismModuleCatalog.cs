﻿namespace MorganStanley.MSDotNet.MSGui.Impl.Modularity.Prism
{
    internal interface IInterceptablePrismModuleCatalog
    {
        void AddInterceptor(IPrismModuleInfoInterceptor interceptor_);
    }
     
}
