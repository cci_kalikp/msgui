﻿using System.Collections.Generic;
using Microsoft.Practices.Prism.Modularity;

namespace MorganStanley.MSDotNet.MSGui.Impl.Modularity.Prism
{

    internal interface IPrismModuleInfoInterceptor
    {
        void Intercept(IList<ModuleInfo> modules_, Stack<IPrismModuleInfoInterceptor> interceptors_);  
    }
}
