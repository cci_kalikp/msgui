﻿using System.Collections.Specialized;

namespace MorganStanley.MSDotNet.MSGui.Impl.Modularity
{
    internal interface IMSDesktopModuleInfo
    {
        /// <summary>
        /// Gets the assembly name.
        /// </summary>
        /// <value>The assembly name of the module.</value>
        string AssemblyName { get;  }

        /// <summary>
        /// Gets extra values expressed as unrecognized attributes of the configuration element.
        /// </summary>
        StringDictionary ExtraValues { get; }
    }
}