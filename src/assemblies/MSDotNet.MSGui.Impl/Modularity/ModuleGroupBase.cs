﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Persistence.DelayedLoading;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.Factories;

namespace MorganStanley.MSDotNet.MSGui.Impl.Modularity
{
    internal abstract class ModuleGroupBase : IModuleGroup
    {
        private readonly IUnityContainer _container;
        private readonly HashSet<Type> _contentTypes = new HashSet<Type>();

        protected ModuleGroupBase(IUnityContainer container)
        {
            _container = container;
        }

        public event EventHandler<ModuleGroupInitializedEventArgs> AllModulesInitialized;

        public abstract void CreateWindowFromLayout(string factoryId, InitialWindowParameters initialParameters,
                                                    string viewId,
                                                    XDocument stateToPass, XDocument viewState,
                                                    Action<IWindowViewContainer> postCreateHandler);

        public abstract bool ViewLoadedCompletely(XDocument connectionState);
        public abstract void SubscribeM2M(string messageType);
        public abstract void PublishM2M(object message);

        public abstract void RegisterDelayedLoadModule(IDelayedLoadModule module);
        public abstract void DelayedLoadModuleInitialized(IDelayedLoadModule module);
        public abstract void DelayedLoadModuleInitializationFailed(IDelayedLoadModule module);
        public abstract IWindowViewContainer CreateGroupWindow(string factoryId, InitialWindowParameters parameters);
        public abstract void RegisterSubsystemWindowFactory(IRemoteWindowFactory remoteFactory, IEnumerable<string> windowFactories);
        public abstract void UnRegisterSubsystemWindowFactory(IEnumerable<string> windowFactories);

        public virtual bool IsWindowFactoryInGroup(string factoryId)
        {
            return false;
        }

        public ISet<Type> ContentTypes
        {
            get { return _contentTypes; }
        }

        protected IUnityContainer Container
        {
            get { return _container; }
        }
    }
}
