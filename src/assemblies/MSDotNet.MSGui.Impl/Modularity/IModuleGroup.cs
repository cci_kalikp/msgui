﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Persistence.DelayedLoading;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.Factories;

namespace MorganStanley.MSDotNet.MSGui.Impl.Modularity
{
    internal interface IModuleGroup
    {
        event EventHandler<ModuleGroupInitializedEventArgs> AllModulesInitialized;
        
        // Layout loading modules
        void CreateWindowFromLayout(string factoryId, InitialWindowParameters initialParameters, string viewId,
                                           XDocument stateToPass, XDocument viewState, Action<IWindowViewContainer> postCreateHandler);
        void RegisterDelayedLoadModule(IDelayedLoadModule module);
        void DelayedLoadModuleInitialized(IDelayedLoadModule module);
        void DelayedLoadModuleInitializationFailed(IDelayedLoadModule module);
        bool ViewLoadedCompletely(XDocument connectionState);

        // M2M subscription handling
        void SubscribeM2M(string messageType);
        void PublishM2M(object message);
        ISet<Type> ContentTypes { get; }

        // Cross-subsystem window creation
        bool IsWindowFactoryInGroup(string factoryId);
        IWindowViewContainer CreateGroupWindow(string factoryId, InitialWindowParameters parameters);
        void RegisterSubsystemWindowFactory(IRemoteWindowFactory remoteFactory, IEnumerable<string> windowFactories);
        void UnRegisterSubsystemWindowFactory(IEnumerable<string> windowFactories);
    }
}
