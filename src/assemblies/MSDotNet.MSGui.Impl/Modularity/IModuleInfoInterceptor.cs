﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Composite.Modularity;

namespace MorganStanley.MSDotNet.MSGui.Impl.Modularity
{
    
    internal interface IModuleInfoInterceptor
    {
        void Intercept(IList<ModuleInfo> modules_, Stack<IModuleInfoInterceptor> interceptors_); 
    }
}
