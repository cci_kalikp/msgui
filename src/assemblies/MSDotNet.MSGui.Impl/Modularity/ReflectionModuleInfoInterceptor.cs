﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Reflection;
using System.Text;
using Microsoft.Practices.Composite;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Helpers;

namespace MorganStanley.MSDotNet.MSGui.Impl.Modularity
{
    class ReflectionModuleInfoInterceptor:IModuleInfoInterceptor
    {

        public void Intercept(IList<ModuleInfo> modules_, Stack<IModuleInfoInterceptor> interceptors_)
        {
           if (interceptors_.Count > 0)
            {
                interceptors_.Pop().Intercept(modules_, interceptors_);
            } 
            var entry = (EntryAssemblyHelper.GetEntryAssembly() ??
                     Assembly.GetAssembly(
                         Type.GetType(
                             "MSDotNet.MSGui.Testing.Persistence.PersistenceServiceTest, MSDotNet.MSGui.Testing")));
            var types = entry.GetTypes();
            IList<ResolverModuleInfo> controlParadigmImplementatorModule = new List<ResolverModuleInfo>();
            IList<ModuleInfo> moduleInfos = new List<ModuleInfo>();
            foreach (Type type in types.Where(type => typeof(IModule).IsAssignableFrom(type)))
            {
                if (typeof(IControlParadigmImplementatorModule).IsAssignableFrom(type))
                    controlParadigmImplementatorModule.Add(new ResolverModuleInfo(type.Name, type.AssemblyQualifiedName));
                else
                    moduleInfos.Add(CreateModuleInfo(type));

            }
            // NOTE:
            // the m_preregistrations collection has already processed in the base.InnerLoad();
            if (controlParadigmImplementatorModule.Count > 0)
            {
                foreach (ResolverModuleInfo resolverModuleInfo in controlParadigmImplementatorModule)
                {
                    foreach (var moduleInfo in moduleInfos)
                    {
                        resolverModuleInfo.DependsOn.Add(moduleInfo.ModuleName);
                    }
                    moduleInfos.Add(resolverModuleInfo);
                }

            }

            bool hasModule = modules_.Count > 0;
            foreach (var moduleInfo in moduleInfos)
            {
                if (!hasModule || modules_.FirstOrDefault(m_ => m_.ModuleName == moduleInfo.ModuleName) == null)
                {
                    modules_.Add(moduleInfo);
                }
            } 
        }

        public void Intercept(IList<ModuleInfo> infos_)
        {
        
        }

        private static ResolverModuleInfo CreateModuleInfo(Type type_)
        {
            string moduleName = type_.Name;
            List<string> dependsOn = new List<string>();
            bool onDemand = false;
            var moduleAttribute =
                CustomAttributeData.GetCustomAttributes(type_).FirstOrDefault(
                    cad => cad.Constructor.DeclaringType.FullName == typeof(ModuleAttribute).FullName);

            StringDictionary extraArguments = new StringDictionary();
            if (moduleAttribute != null)
            {
                foreach (CustomAttributeNamedArgument argument in moduleAttribute.NamedArguments)
                {
                    string argumentName = argument.MemberInfo.Name;
                    switch (argumentName)
                    {
                        case "ModuleName":
                            moduleName = (string)argument.TypedValue.Value;
                            break;

                        case "OnDemand":
                            onDemand = (bool)argument.TypedValue.Value;
                            break;

                        case "StartupLoaded":
                            onDemand = !((bool)argument.TypedValue.Value);
                            break;
                        default:
                            extraArguments.Add(argumentName, argument.TypedValue.Value.ToString());
                            break;
                    }
                }
            }

            var moduleDependencyAttributes =
                CustomAttributeData.GetCustomAttributes(type_).Where(
                    cad => cad.Constructor.DeclaringType.FullName == typeof(ModuleDependencyAttribute).FullName);

            foreach (CustomAttributeData cad in moduleDependencyAttributes)
            {
                dependsOn.Add((string)cad.ConstructorArguments[0].Value);
            }

            ResolverModuleInfo moduleInfo = new ResolverModuleInfo(moduleName, type_.AssemblyQualifiedName)
            {
                InitializationMode =
                    onDemand
                        ? InitializationMode.OnDemand
                        : InitializationMode.WhenAvailable,
                Ref = type_.Assembly.CodeBase,
                ExtraValues = extraArguments
            };
            moduleInfo.DependsOn.AddRange(dependsOn);
            return moduleInfo;
        }


    }
}
