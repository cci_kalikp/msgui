﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Modularity/ResolverModuleConfigurationElementCollection.cs#6 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.Collections.Generic;
using System.Configuration;
using Microsoft.Practices.Composite.Modularity;

namespace MorganStanley.MSDotNet.MSGui.Impl.Modularity
{
  /// <summary>
  /// A collection of <see cref="ResolverModuleConfigurationElementCollection"/>.
  /// </summary>
  public class ResolverModuleConfigurationElementCollection : ConfigurationElementCollection
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="ResolverModuleConfigurationElementCollection"/> class.
    /// </summary>
    public ResolverModuleConfigurationElementCollection()
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="ResolverModuleConfigurationElementCollection"/> class.
    /// </summary>
    /// <param name="modules_">The initial set of <see cref="ResolverModuleConfigurationElement"/>.</param>
    public ResolverModuleConfigurationElementCollection(IEnumerable<ResolverModuleConfigurationElement> modules_)
    {
      foreach (ResolverModuleConfigurationElement module in modules_)
      {
        this.BaseAdd(module);
      }
    }

    /// <summary>
    /// Gets a value indicating whether an exception should be raised if a duplicate element is found.
    /// This property will always return true.
    /// </summary>
    /// <value>A <see cref="bool"/> value.</value>
    protected override bool ThrowOnDuplicate
    {
      get { return true; }
    }

    /// <summary>
    /// Gets the type of the <see cref="T:System.Configuration.ConfigurationElementCollection" />.
    /// </summary>
    /// <value>
    /// The <see cref="T:System.Configuration.ConfigurationElementCollectionType" /> of this collection.
    /// </value>
    public override ConfigurationElementCollectionType CollectionType
    {
      get { return ConfigurationElementCollectionType.BasicMap; }
    }

    /// <summary>
    /// Gets the name used to identify this collection of elements in the configuration file when overridden in a derived class.
    /// </summary>
    /// <value>
    /// The name of the collection; otherwise, an empty string.
    /// </value>
    protected override string ElementName
    {
      get { return "module"; }
    }

    /// <summary>
    /// Gets the <see cref="ModuleConfigurationElement"/> located at the specified index in the collection.
    /// </summary>
    /// <param name="index_">The index of the element in the collection.</param>
    /// <returns>A <see cref="ModuleConfigurationElement"/>.</returns>
    public ResolverModuleConfigurationElement this[int index_]
    {
      get { return (ResolverModuleConfigurationElement)BaseGet(index_); }
    }

    /// <summary>
    /// Adds a <see cref="ResolverModuleConfigurationElement"/> to the collection.
    /// </summary>
    /// <param name="module_">A <see cref="ResolverModuleConfigurationElement"/> instance.</param>
    public void Add(ResolverModuleConfigurationElement module_)
    {
      BaseAdd(module_);
    }

    /// <summary>
    /// Tests if the collection contains the configuration for the specified module name.
    /// </summary>
    /// <param name="moduleName_">The name of the module to search the configuration for.</param>
    /// <returns><see langword="true"/> if a configuration for the module is present; otherwise <see langword="false"/>.</returns>
    public bool Contains(string moduleName_)
    {
      return BaseGet(moduleName_) != null;
    }

    /// <summary>
    /// Searches the collection for all the <see cref="ModuleConfigurationElement"/> that match the specified predicate.
    /// </summary>
    /// <param name="match_">A <see cref="Predicate{T}"/> that implements the match test.</param>
    /// <returns>A <see cref="List{T}"/> with the successful matches.</returns>
    public IList<ResolverModuleConfigurationElement> FindAll(Predicate<ResolverModuleConfigurationElement> match_)
    {
      IList<ResolverModuleConfigurationElement> found = new List<ResolverModuleConfigurationElement>();
      foreach (ResolverModuleConfigurationElement moduleElement in this)
      {
        if (match_(moduleElement))
        {
          found.Add(moduleElement);
        }
      }
      return found;
    }

    /// <summary>
    /// Creates a new <see cref="ModuleConfigurationElement"/>.
    /// </summary>
    /// <returns>A <see cref="ModuleConfigurationElement"/>.</returns>
    protected override ConfigurationElement CreateNewElement()
    {
      return new ResolverModuleConfigurationElement();
    }

    /// <summary>
    /// Gets the element key for a specified configuration element when overridden in a derived class.
    /// </summary>
    /// <param name="element_">The <see cref="T:System.Configuration.ConfigurationElement" /> to return the key for. </param>
    /// <returns>
    /// An <see cref="T:System.Object" /> that acts as the key for the specified <see cref="T:System.Configuration.ConfigurationElement" />.
    /// </returns>
    protected override object GetElementKey(ConfigurationElement element_)
    {
      return ((ResolverModuleConfigurationElement)element_).ModuleName;
    }
  }
}
