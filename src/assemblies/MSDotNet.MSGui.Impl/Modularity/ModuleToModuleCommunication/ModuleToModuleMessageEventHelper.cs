﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.Application;

namespace MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication
{
    internal enum MSDesktopThreadOption
    {
        BackgroundThread, PublisherThread, UIThread
    }

    internal interface IModuleToModuleMessageEvent<TPayload>
    {
        void PublishToExternal(TPayload payload);
        void InternalSubscribe(Action<TPayload> action, MSDesktopThreadOption threadOption,
                                        bool keepSubscriberReferenceAlive);
    }

    internal static class ModuleToModuleMessageEventHelper
    {
        private static IModuleToModuleMessageEvent<TMessage> GetPrismEvent<TMessage>(this IMSDesktopEventAggregator msDesktopEventAggregator)
        {
            return msDesktopEventAggregator.GetEvent<MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication.Prism.ModuleToModuleMessageEvent<TMessage>>();
        }

        private static IModuleToModuleMessageEvent<TMessage> GetCompositeEvent<TMessage>(this IMSDesktopEventAggregator msDesktopEventAggregator)
        {
            return msDesktopEventAggregator.GetEvent<MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication.ModuleToModuleMessageEvent<TMessage>>();
        }

        public static IModuleToModuleMessageEvent<TMessage> GetModuleToModuleEvent<TMessage>(this IMSDesktopEventAggregator msDesktopEventAggregator)
        {
            if (BootstrapperBase.UsePrismAsInfrasturcture)
            {
                return msDesktopEventAggregator.GetPrismEvent<TMessage>();
            }
            else
            {
                return msDesktopEventAggregator.GetCompositeEvent<TMessage>();
                
            }
        }
    }
}
