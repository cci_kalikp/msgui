﻿using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging;
using MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging.CPSMessaging;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;

namespace MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication
{
    internal sealed class ModulePublisher<TMessage> : IModulePublisher<TMessage>
    {
        private readonly Communicator _communicator;
        private readonly IModuleMessenger _fusionMessenger;
        private readonly IModuleMessenger _cpsMessenger;
        private readonly ILocationHolder _location;
        private readonly EventRegistrator _eventRegistrator;
        private readonly IModuleGroup _moduleGroup;

        internal ModulePublisher(
            Communicator communicator, 
            IEventRegistrator eventRegistrator, 
            IModuleMessenger fusionMessenger, 
            IModuleMessenger cpsMessenger,
            IModuleGroup moduleGroup)
        {
            _moduleGroup = moduleGroup;
            _communicator = communicator;
            _location = _communicator;
            _fusionMessenger = fusionMessenger;
            _cpsMessenger = cpsMessenger;
            _eventRegistrator = eventRegistrator as EventRegistrator;
        }


        public void Publish(TMessage message)
        {
           // MSLog.MSLog.Info()
            Publish(message, CommunicationTargetFilter.All);
        }

        public void Publish(TMessage message, CommunicationTargetFilter targetFilter, params string[] location)
        {
            PublishToLocal(message, targetFilter, location);
            PublishToExternal(message, null, false, targetFilter, location);

            if (!(message is IMessageWithPayload))
            {
                // _moduleGroup.PublishM2M(message);
            }
        }

        // DON'T DELETE: Used via reflection
        internal void PublishFromIsolationHost(TMessage message, CommunicationTargetFilter targetFilter)
        {
            // Reflection calls usually invoke Publish with empty location
            var location = new string[] {};
            PublishToLocal(message, targetFilter, location);
            PublishToExternal(message, null, false, targetFilter, location);
        }

        public void PublishToCpsServer(TMessage message, string cpsServerAddress, bool kerberos,
                                       CommunicationTargetFilter targetFilter = CommunicationTargetFilter.All,
                                       params string[] location)
        {
            PublishToLocal(message, targetFilter, location);
            PublishToExternal(message, cpsServerAddress, kerberos, targetFilter, location); 
        }

        public void PublishRelativeToCurrent(TMessage message, CommunicationTargetFilter targetFilter)
        {
            switch (targetFilter)
            {
                case CommunicationTargetFilter.All:
                    PublishToLocal(message, targetFilter);
                    PublishToExternal(message, null, false, targetFilter);
                    break;

                case CommunicationTargetFilter.Machine:
                    PublishToLocal(message, targetFilter, _location.Application.Host);
                    PublishToExternal(message, null, false, targetFilter, _location.Application.Host);
                    break;

                case CommunicationTargetFilter.Application:
                    PublishToLocal(message, targetFilter, _location.Application.Name);
                    PublishToExternal(message, null, false, targetFilter, _location.Application.Name);
                    break;
                case CommunicationTargetFilter.MachineApplication:
                    PublishToLocal(message, targetFilter, _location.Application.Host, _location.Application.Name);
                    PublishToExternal(message, null, false, targetFilter, _location.Application.Host, _location.Application.Name);
                    break;
                case CommunicationTargetFilter.MachineInstance:
                    PublishToLocal(message, targetFilter, _location.Application.Host, _location.Application.Instance);
                    PublishToExternal(message, null, false, targetFilter, _location.Application.Host, _location.Application.Instance);
                    break;

                case CommunicationTargetFilter.User:
                    PublishToLocal(message, targetFilter, _location.Application.UserName);
                    PublishToExternal(message, null, false, targetFilter, _location.Application.UserName);
                    break;

                case CommunicationTargetFilter.UserHost:
                    PublishToLocal(message, targetFilter,_location.Application.UserName,  _location.Application.Host);
                    PublishToExternal(message, null, false, targetFilter, _location.Application.UserName, _location.Application.Host);
                    break;

                case CommunicationTargetFilter.UserApp:
                    PublishToLocal(message, targetFilter, _location.Application.UserName, _location.Application.Name);
                    PublishToExternal(message, null, false, targetFilter, _location.Application.UserName, _location.Application.Name);
                    break;
                case CommunicationTargetFilter.UserHostApp:
                    PublishToLocal(message, targetFilter,_location.Application.UserName,  _location.Application.Host, _location.Application.Name);
                    PublishToExternal(message, null, false, targetFilter, _location.Application.UserName, _location.Application.Host, _location.Application.Name);
                    break;
            }
        }

        private void PublishToExternal(TMessage message, string cpsServerAddress, bool kerberos, CommunicationTargetFilter targetFilter, params string[] location)
        {
            if (message.IsObjectSerializable())
            {
                if (IsValidForImc(message))
                {
                    if (!string.IsNullOrEmpty(cpsServerAddress) && _cpsMessenger is CpsModuleMessenger)
                    {
                        ((CpsModuleMessenger)_cpsMessenger).PublishToServer(cpsServerAddress, kerberos, message, targetFilter, location);
                    }
                    else
                    {
                        _cpsMessenger.PublishToExternal(message, targetFilter, location);
                    }
                }
                if (IsValidForIpc(message, targetFilter)) _fusionMessenger.PublishToExternal(message, targetFilter, location); 
            }
        }

        private static bool IsValidForImc(object message_)
        {
            var routedMessage = message_ as IMessageWithPayload;
            if (routedMessage != null)
            {
                return routedMessage.IsPayloadSerializable();
            }
            if (Communicator.IsImcForAll) return true;
            //can add other special cases for imc later on
            return false;
        }

        private bool IsValidForIpc(object message_, CommunicationTargetFilter targetFilter)
        {
            var routedMessage = message_ as IMessageWithPayload;
            if (routedMessage != null)
            {
                return (routedMessage.IsPayloadSerializable() ||
                        targetFilter == CommunicationTargetFilter.MachineInstance);
            }
            if (Communicator.IsIpcForAll) return true;
            if (LauncherMessageExtension.IsLauncherMessage(message_)) return true;
            //can add other special cases for ipc later on
            return false;
        }

        public void PublishToLocal(TMessage message, CommunicationTargetFilter targetFilter = CommunicationTargetFilter.All, params string[] location)
        {
            var publisher = _eventRegistrator.GetRegisteredPublisher<TMessage>(_communicator.GetChannel<TMessage>());
            if (publisher==null)
                return;
            lock (publisher)
            {
                publisher.Publish(message, targetFilter, location);
            }
        }
    }
}
