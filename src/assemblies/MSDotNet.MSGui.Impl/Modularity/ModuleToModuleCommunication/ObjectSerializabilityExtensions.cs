﻿using System;
using System.Linq;
using System.Runtime.Serialization;

namespace MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication
{
    internal static class ObjectSerializabilityExtensions
    {
        public static bool IsTypeSerializable(this Type t)
        {
            if (t.GetInterfaces().Contains(typeof(ISerializable)))
            {
                return true;
            }

            return t.GetCustomAttributes(typeof(SerializableAttribute), false).FirstOrDefault() != null;
        }

        public static bool IsObjectSerializable(this object o)
        {
            return o.GetType().IsTypeSerializable();
        }
    }
}
