﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication
{
    internal interface IInternalMessageEvent<TPayload>
    {
       void PublishToInternal(TPayload payload);

       void PublishToExternal(TPayload payload);

       
    }
}
