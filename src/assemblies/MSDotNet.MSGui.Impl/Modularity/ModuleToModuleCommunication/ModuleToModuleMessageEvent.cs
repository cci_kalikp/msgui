﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2011 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Modularity/ModuleToModuleCommunication/ModuleToModuleMessageEvent.cs#8 $
// $Change: 861342 $
// $DateTime: 2014/01/09 09:37:41 $
// $Author: anbuy $

using System;
using Microsoft.Practices.Composite.Presentation.Events;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;

namespace MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication
{
    public class ModuleToModuleMessageEvent<TPayload> : CompositePresentationEvent<TPayload>, IModuleToModuleMessageEvent<TPayload>
     {
         private readonly M2MInternalMessageEvent<TPayload> _communicationEvent;

         public ModuleToModuleMessageEvent()
         {
             _communicationEvent = Activator.CreateInstance<M2MInternalMessageEvent<TPayload>>();
         }

         internal void PublishToInternal(TPayload payload)
         {
             _communicationEvent.Publish(payload);
         }

         void IModuleToModuleMessageEvent<TPayload>.PublishToExternal(TPayload payload)
         {
             base.Publish(payload);
         }

         internal M2MInternalMessageEvent<TPayload> InternalEvent
         {
             get { return _communicationEvent; }
         }

         public override void Publish(TPayload payload)
         {
             PublishToInternal(payload);
         }

         void IModuleToModuleMessageEvent<TPayload>.InternalSubscribe(Action<TPayload> action, MSDesktopThreadOption threadOption, bool keepSubscriberReferenceAlive)
         {
             Microsoft.Practices.Composite.Presentation.Events.ThreadOption to;
             Enum.TryParse(threadOption.ToString(), out to);
             InternalEvent.Subscribe(action, to, keepSubscriberReferenceAlive);
         }
        
     }

    internal class M2MInternalMessageEvent<TPayload> : CompositePresentationEvent<TPayload>
    {
    }

    /*
    /// <summary>
    /// Custom event type to support adaptation
    /// </summary>
    /// <typeparam name="TPayload">The type of the payload.</typeparam>
    public class ModuleToModuleMessageEvent<TPayload> : CompositePresentationEvent<TPayload>
    {
        private readonly ModuleToModuleMessageEventAggregator m_eventAggregator;

        private readonly Dictionary<SubscriptionToken, StackFrame> m_subscriptionOwners = new Dictionary<SubscriptionToken, StackFrame>();

        /// <summary>
        /// Initializes a new instance of the <see cref="ModuleToModuleMessageEvent&lt;TPayload&gt;"/> class.
        /// </summary>
        /// <param name="eventAggregator_">The event aggregator_.</param>
        public ModuleToModuleMessageEvent(ModuleToModuleMessageEventAggregator eventAggregator_)
        {
            m_eventAggregator = eventAggregator_;
        }

        public override SubscriptionToken Subscribe(Action<TPayload> action_, ThreadOption threadOption_, bool keepSubscriberReferenceAlive_, Predicate<TPayload> filter_)
        {
            var token = base.Subscribe(action_, threadOption_, keepSubscriberReferenceAlive_, filter_);
// ReSharper disable PossibleNullReferenceException
            var frames = new StackTrace().GetFrames();
            var stackFrame = frames[2];
// ReSharper restore PossibleNullReferenceException
            //TODO: authentication & authorization
            var frameType = stackFrame.GetMethod().DeclaringType;
            if (typeof(IModule).IsInstanceOfType(frameType))
            {
                m_subscriptionOwners[token] = stackFrame;
                HashSet<Type> subscribed;
                if (!m_eventAggregator.Subscribers.TryGetValue(frameType, out subscribed))
                {
                    subscribed = new HashSet<Type>();
                    m_eventAggregator.Subscribers[frameType] = subscribed;
                }
                subscribed.Add(typeof (TPayload));
            }
            return token;
        }

        public override void Unsubscribe(SubscriptionToken token_)
        {
            m_subscriptionOwners.Remove(token_);
            //TODO: maintain subscribes in eventaggr
            base.Unsubscribe(token_);
        }

        public override void Unsubscribe(Action<TPayload> subscriber_)
        {
            lock (m_subscriptionOwners)
            {
                IEventSubscription eventSubscription = Subscriptions.Cast<EventSubscription<TPayload>>().FirstOrDefault(evt_ => evt_.Action == subscriber_);
                if (eventSubscription != null)
                {
                    m_subscriptionOwners.Remove(eventSubscription.SubscriptionToken);
                }
                //TODO: maintain subscribes in eventaggr
                base.Unsubscribe(subscriber_);
            }
        }
    }*/
}
