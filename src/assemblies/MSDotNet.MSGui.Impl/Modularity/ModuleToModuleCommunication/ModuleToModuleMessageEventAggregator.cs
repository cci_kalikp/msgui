﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2011 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Modularity/ModuleToModuleCommunication/ModuleToModuleMessageEventAggregator.cs#4 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.Composite.Events;

namespace MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication
{
    /// <summary>
    /// Custom Event Aggregator to support message type adaptation
    /// </summary>
    public class ModuleToModuleMessageEventAggregator : IEventAggregator
    {
        public IDictionary<EventBase, Type> EventTypes { get; protected set; }

        public IDictionary<Type, HashSet<Type>> Subscribers { get; protected set; }

        private readonly List<EventBase> m_events = new List<EventBase>();

        /// <summary>
        /// Initializes a new instance of the <see cref="ModuleToModuleMessageEventAggregator"/> class.
        /// </summary>
        public ModuleToModuleMessageEventAggregator()
        {
            EventTypes = new Dictionary<EventBase, Type>();
            Subscribers = new Dictionary<Type, HashSet<Type>>();
        }

        /// <summary>
        /// Gets the single instance of the event managed by this EventAggregator. Multiple calls to this method with the same <typeparamref name="TEventType"/> returns the same event instance.
        /// </summary>
        /// <typeparam name="TEventType">The type of event to get. This must inherit from <see cref="EventBase"/>.</typeparam>
        /// <returns>A singleton instance of an event object of type <typeparamref name="TEventType"/>.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter")]
        public TEventType GetEvent<TEventType>() where TEventType : EventBase
        {
            var eventType = typeof (TEventType);
            var eventInstance = m_events.FirstOrDefault(evt_ => evt_.GetType() == eventType) as TEventType;
            if (eventInstance == null)
            {
                var eventTypeArgs = eventType.GetGenericArguments();
                eventInstance = eventType.GetConstructors().Any(
                        info_ => info_.GetParameters().Length == 1
                            && info_.GetParameters()[0].ParameterType == typeof (ModuleToModuleMessageEventAggregator))
                    ? Activator.CreateInstance(eventType, this) as TEventType
                    : Activator.CreateInstance<TEventType>();
                m_events.Add(eventInstance);
                if (eventInstance != null && !EventTypes.ContainsKey(eventInstance))
                {
                    if (eventTypeArgs.Length > 0)
                    {
                        EventTypes[eventInstance] = eventTypeArgs[0];
                    }
                }
            }
            return eventInstance;
        }
    }
}
