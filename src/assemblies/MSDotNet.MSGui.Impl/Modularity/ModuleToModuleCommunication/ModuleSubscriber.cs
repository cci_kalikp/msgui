﻿using System;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;

namespace MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication
{
    internal sealed class ModuleSubscriber<TMessage> : IModuleSubscriber<TMessage>
    {
        private readonly EventRegistrator _eventRegistrator;
        private readonly IModuleGroup _moduleGroup;
        private readonly Communicator _communicator;

        internal ModuleSubscriber(Communicator communicator, EventRegistrator eventRegistrator, IModuleGroup moduleGroup)
        {
            _communicator = communicator;
            _eventRegistrator = eventRegistrator;
            _moduleGroup = moduleGroup;
        }

        public IObservable<TMessage> GetObservable()
        {
            var subscriber = _eventRegistrator.GetRegisteredSubscriber<TMessage>(_communicator.GetChannel<TMessage>());

            // _moduleGroup.SubscribeM2M(typeof (TMessage).AssemblyQualifiedName);

            var observable = subscriber.GetObservable();
            return observable;
        }
    }
}
