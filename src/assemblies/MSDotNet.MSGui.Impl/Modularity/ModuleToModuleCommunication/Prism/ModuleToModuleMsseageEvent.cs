﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.Events;

namespace MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication.Prism
{
    public class ModuleToModuleMessageEvent<TPayload> : CompositePresentationEvent<TPayload>,  IModuleToModuleMessageEvent<TPayload>
    {
        private readonly M2MInternalMessageEvent<TPayload> _communicationEvent;

        public ModuleToModuleMessageEvent()
        {
            _communicationEvent = Activator.CreateInstance<M2MInternalMessageEvent<TPayload>>();
        }

        internal void PublishToInternal(TPayload payload)
        {
            _communicationEvent.Publish(payload);
        }

        void IModuleToModuleMessageEvent<TPayload>.PublishToExternal(TPayload payload)
        {
            base.Publish(payload);
        }

        internal M2MInternalMessageEvent<TPayload> InternalEvent
        {
            get { return _communicationEvent; }
        }

        public override void Publish(TPayload payload)
        {
            //PublishToExternal(payload);
            PublishToInternal(payload);
        }

        void IModuleToModuleMessageEvent<TPayload>.InternalSubscribe(
            Action<TPayload> action, 
            MSDesktopThreadOption threadOption, 
            bool keepSubscriberReferenceAlive)
        {
            Microsoft.Practices.Prism.Events.ThreadOption to;
            Enum.TryParse(threadOption.ToString(), out to);
            InternalEvent.Subscribe(action, to, keepSubscriberReferenceAlive);
        }
    }

    internal class M2MInternalMessageEvent<TPayload> : CompositePresentationEvent<TPayload>
    {
    }

}
