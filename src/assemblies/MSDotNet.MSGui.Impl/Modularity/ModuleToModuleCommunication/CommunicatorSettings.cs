﻿using MorganStanley.MSDotNet.MSGui.Core.Messaging;

namespace MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication
{
    internal sealed class CommunicatorSettings : ICommunicatorSettings
    {
        private readonly CommunicationChannelStatus _ipcStatus;
        private readonly CommunicationChannelStatus _imcStatus;
        private readonly int _hostProcessId;

        public CommunicatorSettings(int hostProcessId)
            : this(hostProcessId, CommunicationChannelStatus.Required, CommunicationChannelStatus.Required)
        {
        }

        public CommunicatorSettings(int hostProcessId, CommunicationChannelStatus ipcStatus, CommunicationChannelStatus imcStatus)
        {
            _hostProcessId = hostProcessId;
            _imcStatus = imcStatus;
            _ipcStatus = ipcStatus;
        }

        public CommunicationChannelStatus ImcStatus
        {
            get { return _imcStatus; }
        }

        public CommunicationChannelStatus IpcStatus
        {
            get { return _ipcStatus; }
        }

        public int HostProcessId
        {
            get { return _hostProcessId; }
        }
    }
}
