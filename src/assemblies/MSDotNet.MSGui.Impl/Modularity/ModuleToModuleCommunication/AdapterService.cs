﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2011 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Modularity/ModuleToModuleCommunication/AdapterService.cs#5 $
// $Change: 858188 $
// $DateTime: 2013/12/10 01:31:58 $
// $Author: caijin $

using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Presentation.Events;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication
{
    /// <summary>
    /// Manages adapters that can convert between message types
    /// </summary>
    public class AdapterService
    {
        /// <summary>
        /// Map of source types to collection of corresponding destination types
        /// </summary>
        private readonly IDictionary<Type, IDictionary<Type, Delegate>> m_conversions = new Dictionary<Type, IDictionary<Type, Delegate>>();

        /// <summary>
        /// The Event Aggregator used for subscribing to convertible messages
        /// </summary>
        private readonly ModuleToModuleMessageEventAggregator m_eventAggregator;

        /// <summary>
        /// Initializes a new instance of the <see cref="AdapterService"/> class.
        /// </summary>
        /// <param name="eventAggregator_">The event aggregator.</param>
        public AdapterService(ModuleToModuleMessageEventAggregator eventAggregator_)
            : this(eventAggregator_, new DefaultCostCalculator())
        {
        }

        /// <summary>
        /// Default implementation of a cost calculator
        /// </summary>
        public class DefaultCostCalculator : ICostCalculator<Type, Type>
        {
            /// <summary>
            /// Calculates the cost.
            /// </summary>
            /// <param name="from_">The start node.</param>
            /// <param name="to_">The end node.</param>
            /// <returns>Always 1.</returns>
            public int CalculateCost(Type from_, Type to_)
            {
                return 1;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AdapterService"/> class.
        /// </summary>
        /// <param name="eventAggregator_">The event aggregator.</param>
        /// <param name="costCalculator_">The cost calculator.</param>
        public AdapterService(ModuleToModuleMessageEventAggregator eventAggregator_,
            ICostCalculator<Type, Type> costCalculator_)
        {
            Guard.ArgumentNotNull(eventAggregator_, "eventAggregator_");
            Guard.ArgumentNotNull(costCalculator_, "costCalculator_");
            m_eventAggregator = eventAggregator_;
            CostCalculator = costCalculator_;
        }

        private ICostCalculator<Type, Type> m_costCalculator;
        /// <summary>
        /// Gets or sets the cost calculator.
        /// </summary>
        /// <value>
        /// The cost calculator.
        /// </value>
        public ICostCalculator<Type, Type> CostCalculator
        {
            get
            {
                return m_costCalculator;
            }
            set
            {
                Guard.ArgumentNotNull(value, "value");
                m_costCalculator = value;
            }
        }

        /// <summary>
        /// Calculates the cost of conversion between two nodes/message types
        /// </summary>
        /// <typeparam name="TFrom">The type of start node.</typeparam>
        /// <typeparam name="TTo">The type of the end node.</typeparam>
        public interface ICostCalculator<TFrom, TTo>
        {
            /// <summary>
            /// Calculates the cost.
            /// </summary>
            /// <param name="from_">The start node.</param>
            /// <param name="to_">The end node.</param>
            /// <returns>The cost of the conversion.</returns>
            int CalculateCost(TFrom from_, TTo to_);
        }

        /// <summary>
        /// Adds the adapter.
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <typeparam name="TDestination">The type of the destination.</typeparam>
        /// <param name="adapter_">The adapter.</param>
        /// <returns></returns>
        public SubscriptionToken AddAdapter<TSource, TDestination>(Func<TSource, TDestination> adapter_)
        {
            Guard.ArgumentNotNull(adapter_, "adapter_");

            IDictionary<Type, Delegate> destinations;
            if (m_conversions.TryGetValue(typeof(TSource), out destinations))
            {
                Delegate oldAdapter;
                if (destinations.TryGetValue(typeof(TDestination), out oldAdapter))
                {
                    m_eventAggregator.GetEvent<ModuleToModuleMessageEvent<TSource>>().Unsubscribe(oldAdapter as Action<TSource>);
                }
                destinations[typeof(TDestination)] = adapter_;
            }
            else
            {
                m_conversions[typeof(TSource)] = new Dictionary<Type, Delegate> { { typeof(TDestination), adapter_ } };
            }
            return m_eventAggregator.GetEvent<ModuleToModuleMessageEvent<TSource>>().Subscribe(
                msg_ => AdaptationFilter(typeof(TSource), msg_), ThreadOption.UIThread, true);
        }

        /// <summary>
        /// Filters the adapters and runs them
        /// </summary>
        /// <param name="srcType_">The source type_.</param>
        /// <param name="msg_">The message to be applied.</param>
        private void AdaptationFilter(Type srcType_, object msg_)
        {
            var paths = ShortestPaths(srcType_);
            foreach (var shortestPath in paths)
            {
                var msgType = typeof(ModuleToModuleMessageEvent<>).MakeGenericType(shortestPath.Key.Value);
                var message = m_eventAggregator.GetType().GetMethod("GetEvent").MakeGenericMethod(msgType).Invoke(
                    m_eventAggregator, new object[0]);
                message.GetType().GetMethod("Publish").Invoke(message,
                new object[] {
                    shortestPath.Value.Aggregate(msg_,
                                                 (current_, appliedAdapter_)
                                                 =>
                                                 appliedAdapter_.DynamicInvoke
                                                     (current_))
                }
            );
            }
        }

        /// <summary>
        /// Calculates the shortest adaptation paths between the <paramref name="srcType_">source type</paramref> and destination types.
        /// Uses a slightly modified implementation of Dyjkstra's algorithm for finding the shortest paths in a graph.
        /// </summary>
        /// <param name="srcType_">The source type of the conversion.</param>
        /// <returns>A map from start type - end type pairs to the list of adapters to be chained to apply the conversion</returns>
        private IEnumerable<KeyValuePair<KeyValuePair<Type, Type>, IEnumerable<Delegate>>> ShortestPaths(Type srcType_)
        {
            var result = new Dictionary<KeyValuePair<Type, Type>, IEnumerable<Delegate>>();
            var distance = new Dictionary<Type, int>();
            var previous = new Dictionary<Type, Type>();
            foreach (var end in m_conversions.SelectMany(start_ => start_.Value))
            {
                distance[end.Key] = int.MaxValue;
            }
            distance[srcType_] = 0;
            {
                var nodes = new SortedList<Type, int>(distance, new DistanceComparer(distance));
                while (nodes.Count > 0)
                {
                    var u = nodes.Keys[0];
                    if (distance[u] == int.MaxValue)
                    {
                        break;
                    }
                    nodes.RemoveAt(0);
                    IDictionary<Type, Delegate> vectorEntry;
                    if (m_conversions.TryGetValue(u, out vectorEntry))
                    {
                        foreach (var v in vectorEntry.Keys)
                        {
                            var alt = distance[u] + CostCalculator.CalculateCost(u, v);
                            if (alt >= distance[v])
                            {
                                continue;
                            }
                            distance[v] = alt;
                            previous[v] = u;
                        }
                    }
                }
            }
            foreach (var endPoint in m_eventAggregator.EventTypes.Values.Where(type_ => type_ != srcType_))
            {
                var list = new LinkedList<Delegate>();
                var currType = endPoint;
                Type prevType;
                IDictionary<Type, Delegate> currTypeEntry;
                Delegate adapter;
                while (previous.TryGetValue(currType, out prevType)
                    && m_conversions.TryGetValue(prevType, out currTypeEntry)
                    && currTypeEntry.TryGetValue(currType, out adapter))
                {
                    list.AddFirst(adapter);
                    currType = prevType;
                }
                if (currType == srcType_)
                {
                    result[new KeyValuePair<Type, Type>(srcType_, endPoint)] = list;
                }
            }
            return result;
        }

        /// <summary>
        /// Compares the distance from the source node of two nodes
        /// </summary>
        protected class DistanceComparer : IComparer<Type>
        {
            private readonly IDictionary<Type, int> m_distance;

            /// <summary>
            /// Initializes a new instance of the <see cref="DistanceComparer"/> class.
            /// </summary>
            /// <param name="distance_">The distances.</param>
            public DistanceComparer(IDictionary<Type, int> distance_)
            {
                m_distance = distance_;
            }

            public int Compare(Type x_, Type y_)
            {
                return m_distance[x_] - m_distance[y_];
            }
        }

        /// <summary>
        /// Removes the adapter.
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <typeparam name="TDestination">The type of the destination.</typeparam>
        /// <param name="adapter_">The adapter.</param>
        /// <returns></returns>
        public bool RemoveAdapter<TSource, TDestination>(Func<TSource, TDestination> adapter_)
        {
            Guard.ArgumentNotNull(adapter_, "adapter_");
            IDictionary<Type, Delegate> destinations;
            if (m_conversions.TryGetValue(typeof(TSource), out destinations))
            {
                Delegate oldAdapter;
                if (destinations.TryGetValue(typeof(TDestination), out oldAdapter))
                {
                    m_eventAggregator.GetEvent<ModuleToModuleMessageEvent<TSource>>().Unsubscribe(oldAdapter as Action<TSource>);
                }
                return destinations.Remove(typeof(TDestination));
            }
            return false;
        }
    }
}
