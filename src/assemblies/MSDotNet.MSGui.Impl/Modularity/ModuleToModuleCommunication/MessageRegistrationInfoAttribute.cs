﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2011 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Modularity/ModuleToModuleCommunication/MessageRegistrationInfoAttribute.cs#4 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;

namespace MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication
{
    /// <summary>
    /// An attribute for keeping track of message versions and owners
    /// </summary>
    [AttributeUsage(AttributeTargets.Struct | AttributeTargets.Class | AttributeTargets.Delegate
        | AttributeTargets.Enum | AttributeTargets.Event | AttributeTargets.Interface, AllowMultiple = false, Inherited = false)]
    public class MessageRegistrationInfoAttribute : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MessageRegistrationInfoAttribute"/> class.
        /// </summary>
        /// <param name="mailGroup_">The mail group.</param>
        /// <param name="version_">The version.</param>
        public MessageRegistrationInfoAttribute(string mailGroup_, string version_)
        {
            MailGroup = mailGroup_;
            Version = version_;
        }

        /// <summary>
        /// Gets the version.
        /// </summary>
        public string Version { get; private set; }

        /// <summary>
        /// Gets the mail group.
        /// </summary>
        public string MailGroup { get; private set; }

    }
}
