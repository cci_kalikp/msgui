﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Threading;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSLog;
using MorganStanley.MSDotNet.My;
using MorganStanley.MSDotNet.MSGui.Impl;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Presentation.Events;
using System.Reactive.Linq;
using System.Threading.Tasks;

namespace MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication
{
    [Obsolete]
    public class ModuleCommunicator<T> : IModuleCommunicator<T>
    {
        
        private static readonly IMSLogger _log = MSLoggerFactory.CreateLogger<ModuleCommunicator<T>>();
        

        private readonly ModuleToModuleMessageEvent<T> _event;

        public ModuleCommunicator(IEventAggregator aggregator)
        {
            _event = aggregator.GetEvent<ModuleToModuleMessageEvent<T>>();
            
        }

        public void Publish(T value)
        {
            lock (_event)
            {
                _event.Publish(value);
            }
        }

        public IObservable<T> GetObservable()
        {
            return Async(Observable.Create<T>(observer =>
            {
                SubscriptionToken token;
                lock (_event)
                {
                    token = _event.Subscribe(observer.OnNext, ThreadOption.PublisherThread, true);
                }

                return () => _event.Unsubscribe(token);
            }).Publish().RefCount());
        }

        public static IObservable<T> Async(IObservable<T> observable)
        {
            return Observable.Create<T>(observer =>
            {
                IDisposable subscription;
                if (ShellModeExtension.enableParallelBasedEventing)
                    subscription = observable.Subscribe(value =>
                                                        Task.Factory.StartNew(() => AsyncNext(observer, value)));
                else
                    subscription = observable.Subscribe(value =>
                                                        Dispatcher.CurrentDispatcher.BeginInvoke(
                                                            () => AsyncNext(observer, value)));
                return  subscription.Dispose;

            });
        }

        private static void AsyncNext(IObserver<T> observer, T value)
        {
            try
            {
                observer.OnNext(value);
            }
            catch (Exception ex)
            {
                _log.Error("Error while asynchronously publishing event", ex, "Async");
            }
        }

        public void Publish(T message, CommunicationTargetFilter targetFilter, params string[] location)
        {
            throw new NotImplementedException();
        }

        public void PublishRelativeToCurrent(T message, CommunicationTargetFilter targetFilter)
        {
            throw new NotImplementedException();
        }
    }
}
