﻿using System;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;
namespace MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication
{
    [Obsolete]
    public class M2MHelper
    {
        /// <summary>
        /// For using M2M outside MSDesktop only
        /// </summary>
        /// <returns></returns>
        public static ICommunicator GetCommunicator(IApplication application = null)
        {
            //TODO: double check if it is safe to force not use;
            throw new ApplicationException("Please use MSDesktop.IPC instead");
           /* var eventAggregator = new ModuleToModuleMessageEventAggregator();
            var adapterService = new ViewMessageAdapterService();

            IMessageTypeRegistrator msgTypeRegistrator = new MessageTypeRegistrator();

            var communicator = new Communicator(adapterService, eventAggregator, msgTypeRegistrator, new CustomizedFormatter(msgTypeRegistrator), application);
            return communicator;*/
        }
    }
}
