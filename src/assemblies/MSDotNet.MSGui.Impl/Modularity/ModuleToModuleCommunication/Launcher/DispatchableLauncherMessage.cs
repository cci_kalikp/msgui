﻿using System;
using System.Collections.Generic;
using System.Net;
using MorganStanley.MSDotNet.MSGui.Impl.HTTPServer;
using Newtonsoft.Json;

namespace MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication
{
    [Serializable]
    public class DispatchableLauncherMessage : MessageWithHttpResponse<DispatchableLauncherMessageResponse>
    { 
        public DispatchableLauncherMessage()
        {
            CanNewApp = true;
            CanSaveRoute = true;
            AlwaysNewApp = false;
        }
        public bool ContainsMessage
        {
            get { return Payload != null && Payload.Content != null; }
        }
        public string AppKey { get; set; }
        public string ExtraArguments { get; set; }
        public LauncherMessage Payload { get; set; }  
        public bool CanNewApp { get; set; }
        public bool CanSaveRoute { get; set; }
        public bool HideProgress { get; set; }
        public bool AlwaysNewApp { get; set; }

        #region Used for RunPackage
        public string AppName { get; set; }
        public string Version { get; set; }
        public string Package { get; set; }
        public string PackagePath { get; set; }
        public string Executable { get; set; }
        #endregion

        public override DispatchableLauncherMessageResponse GetResponse(string content_)
        {
            var response = base.GetResponse(content_);
            response.ApplicationKey = this.AppKey;
            return response;
        }

        public static readonly HttpRequestBuilder RequestBuilder = new HttpRequestBuilder(HttpRequestBuilder.Localhost, LauncherConsts.GetHTTPServerPort(false), LauncherConsts.EnableHash, LauncherConsts.HashSeed);
        public static string GenerateLink(object message_, string applicationKey_, SerializationFormat dataFormat_=SerializationFormat.Json, 
            bool allowNewApplication_ = true, bool allowSaveRoute_ = true, 
            string extraArgument_=null, bool alwaysNewApp_=false)
        {
            return GenerateLinkCore(new LauncherMessage(message_, dataFormat_),
                                    new StartupSetting()
                                        {
                                            AppKey = applicationKey_,
                                            Arguments = extraArgument_,
                                            AlwaysNewApp = alwaysNewApp_,
                                            AllowNewApp = allowNewApplication_,
                                            AllowSaveRoute = allowSaveRoute_
                                        });
        }

        public static string GenerateLink(string messageTypeName_, IDictionary<string, object> messageProperties_, 
            string applicationKey_, bool allowNewApplication_ = true, bool allowSaveRoute_ = true, 
            string extraArgument_ = null, bool alwaysNewApp_ = false)
        {
            return GenerateLinkCore(new LauncherMessage(messageTypeName_, messageProperties_),
                                    new StartupSetting()
                                    {
                                        AppKey = applicationKey_,
                                        Arguments = extraArgument_,
                                        AlwaysNewApp = alwaysNewApp_,
                                        AllowNewApp = allowNewApplication_,
                                        AllowSaveRoute = allowSaveRoute_, 
                                    });
        }

        public static string GenerateLink(object message_, string appName_, string version_, string package_, string args_,
            SerializationFormat dataFormat_ = SerializationFormat.Json,
            bool allowNewApplication_ = true, bool allowSaveRoute_ = true, bool alwaysNewApp_ = false)
        {
            return GenerateLinkCore(new LauncherMessage(message_, dataFormat_),
                                    new StartupSetting()
                                    {
                                        AppName = appName_,
                                        Version = version_,
                                        Package = package_,
                                        Arguments = args_,
                                        AllowNewApp = allowNewApplication_,
                                        AllowSaveRoute = allowSaveRoute_,
                                        AlwaysNewApp = alwaysNewApp_
                                    });
        }

        public static string GenerateLink(object message_, string packagePath_, string executable_=null, string args_=null,
          SerializationFormat dataFormat_ = SerializationFormat.Json,
          bool allowNewApplication_ = true, bool allowSaveRoute_ = true, bool alwaysNewApp_ = false)
        {
            return GenerateLinkCore(new LauncherMessage(message_, dataFormat_),
                                    new StartupSetting()
                                    {
                                        PackagePath = packagePath_, 
                                        Executable = executable_,
                                        Arguments = args_,
                                        AllowNewApp = allowNewApplication_,
                                        AllowSaveRoute = allowSaveRoute_,
                                        AlwaysNewApp = alwaysNewApp_
                                    });
        }

        public static string GenerateLink(string messageTypeName_, IDictionary<string, object> messageProperties_,
            string appName_, string version_, string package_, string executable_ = null, string args_ = null, 
            bool allowNewApplication_ = true, bool allowSaveRoute_ = true, bool alwaysNewApp_ = false)
        {
            return GenerateLinkCore(new LauncherMessage(messageTypeName_, messageProperties_),
                                    new StartupSetting()
                                    {
                                        AppName = appName_,
                                        Version = version_,
                                        Package = package_,
                                        Executable = executable_,
                                        Arguments = args_,
                                        AllowNewApp = allowNewApplication_,
                                        AllowSaveRoute = allowSaveRoute_,
                                        AlwaysNewApp = alwaysNewApp_
                                    });
        }

        public static string GenerateLink(string messageTypeName_, IDictionary<string, object> messageProperties_,
    string packagePath_, string executable_ = null, string args_ = null,
    bool allowNewApplication_ = true, bool allowSaveRoute_ = true, bool alwaysNewApp_ = false)
        {
            return GenerateLinkCore(new LauncherMessage(messageTypeName_, messageProperties_),
                                    new StartupSetting()
                                    {
                                        PackagePath = packagePath_,
                                        Executable = executable_,
                                        Arguments = args_,
                                        AllowNewApp = allowNewApplication_,
                                        AllowSaveRoute = allowSaveRoute_,
                                        AlwaysNewApp = alwaysNewApp_
                                    });
        }

        public static string GenerateApplicationStartLink(string applicationKey_, string extraArguments_=null, bool alwaysNewApp_=true, bool allowSaveRoute_=true, bool hideProgress_=true)
        {
            return GenerateLinkCore(new LauncherMessage(),
                new StartupSetting()
                {
                    AppKey = applicationKey_,
                    Arguments = extraArguments_, 
                    AlwaysNewApp = alwaysNewApp_,
                    AllowNewApp = true,
                    AllowSaveRoute = allowSaveRoute_,
                    HideProgress = hideProgress_
                });
        }


        public static string GeneratePackageStartLink(string appName_, string version_, string package_,
            string executable_ = null, string args_ = null, bool alwaysNewApp_ = true, bool allowSaveRoute_ = true, bool hideProgress_ = true)
        {
            return GenerateLinkCore(new LauncherMessage(),
                new StartupSetting()
                {
                    AppName = appName_,
                    Version = version_,
                    Package = package_,
                    Executable = executable_,
                    Arguments = args_,
                    AlwaysNewApp = alwaysNewApp_,
                    AllowNewApp = true,
                    AllowSaveRoute = allowSaveRoute_,
                    HideProgress = hideProgress_
                });
        }

        public static string GeneratePackagePathStartLink(string packagePath_, 
            string executable_ = null, string args_ = null, bool alwaysNewApp_=true, bool allowSaveRoute_=true, bool hideProgress_=true)
        {
            return GenerateLinkCore(new LauncherMessage(),
                new StartupSetting()
                {
                    PackagePath = packagePath_,
                    Executable = executable_,
                    Arguments = args_,
                    AlwaysNewApp = alwaysNewApp_,
                    AllowNewApp = true,
                    AllowSaveRoute = allowSaveRoute_,
                    HideProgress = hideProgress_
                });
        }

        public static void Publish(object message_, string applicationKey_, bool hideProgress_ = true, bool allowSaveRoute_ = true, 
            string extraArgument_=null, bool alwaysNewApp_=false)
        {
            var link = GenerateLinkCore(new LauncherMessage(message_, SerializationFormat.Json),
                                    new StartupSetting()
                                        {
                                            AppKey = applicationKey_,
                                            Arguments = extraArgument_,
                                            AlwaysNewApp = alwaysNewApp_,
                                            AllowNewApp = true,
                                            AllowSaveRoute = allowSaveRoute_,
                                            HideProgress = hideProgress_
                                        });
            ExecuteLinkCore(link);
        }

        public static void Publish(string messageTypeName_, IDictionary<string, object> messageProperties_, string applicationKey_, bool hideProgress_ = true, 
            bool allowSaveRoute_ = true, string extraArgument_ = null, bool alwaysNewApp_ = false)
        {
            var link = GenerateLinkCore(new LauncherMessage(messageTypeName_, messageProperties_),
                                               new StartupSetting()
                                               {
                                                   AppKey = applicationKey_,
                                                   Arguments = extraArgument_,
                                                   AlwaysNewApp = alwaysNewApp_,
                                                   AllowNewApp = true,
                                                   AllowSaveRoute = allowSaveRoute_,
                                                   HideProgress = hideProgress_
                                               }); 
            ExecuteLinkCore(link);
        }

        public static void Publish(object message_, string appName_, string version_, string package_, string executable_ = null, string args_ = null,
           bool hideProgress_ = true, bool allowSaveRoute_ = true, bool alwaysNewApp_ = false)
        {
            var link = GenerateLinkCore(new LauncherMessage(message_, SerializationFormat.Json),
                                    new StartupSetting()
                                    {
                                        AppName = appName_,
                                        Version = version_,
                                        Package = package_,
                                        Executable = executable_,
                                        Arguments = args_,
                                        AllowNewApp = true,
                                        AllowSaveRoute = allowSaveRoute_,
                                        AlwaysNewApp = alwaysNewApp_
                                    }); 
            ExecuteLinkCore(link);
        }

        public static void Publish(object message_, string packagePath_, string executable_ = null, string args_ = null,
         bool hideProgress_ = true, bool allowSaveRoute_ = true, bool alwaysNewApp_ = false)
        {
            var link = GenerateLinkCore(new LauncherMessage(message_, SerializationFormat.Json),
                                    new StartupSetting()
                                    {
                                        PackagePath = packagePath_,
                                        Executable = executable_,
                                        Arguments = args_,
                                        AllowNewApp = true,
                                        AllowSaveRoute = allowSaveRoute_,
                                        AlwaysNewApp = alwaysNewApp_
                                    });
            ExecuteLinkCore(link);
        }


        public static void Publish(string messageTypeName_, IDictionary<string, object> messageProperties_,
          string appName_, string version_, string package_, string executable_=null, string args_=null,
          bool hideProgress_ = true, bool allowSaveRoute_ = true, bool alwaysNewApp_ = false)
        {
            var link = GenerateLinkCore(new LauncherMessage(messageTypeName_, messageProperties_),
                                    new StartupSetting()
                                    {
                                        AppName = appName_,
                                        Version = version_,
                                        Package = package_,
                                        Executable = executable_,
                                        Arguments = args_,
                                        AllowNewApp = true,
                                        AllowSaveRoute = allowSaveRoute_,
                                        AlwaysNewApp = alwaysNewApp_
                                    });
            ExecuteLinkCore(link);
        }

        public static void Publish(string messageTypeName_, IDictionary<string, object> messageProperties_,
         string packagePath_, string executable_ = null, string args_ = null,
         bool hideProgress_ = true, bool allowSaveRoute_ = true, bool alwaysNewApp_ = false)
        {
            var link = GenerateLinkCore(new LauncherMessage(messageTypeName_, messageProperties_),
                                    new StartupSetting()
                                    {
                                        PackagePath = packagePath_,
                                        Executable = executable_,
                                        Arguments = args_,
                                        AllowNewApp = true,
                                        AllowSaveRoute = allowSaveRoute_,
                                        AlwaysNewApp = alwaysNewApp_
                                    });
            ExecuteLinkCore(link);
        }

        public static void StartApplication(string applicationKey_, string extraArguments_=null, bool alwaysNewApp_=true, bool allowSaveRoute_=true)
        {
            var link = GenerateLinkCore(new LauncherMessage(),
                                        new StartupSetting()
                                            {
                                                AppKey = applicationKey_,
                                                Arguments = extraArguments_,
                                                AlwaysNewApp = alwaysNewApp_,
                                                AllowNewApp = true,
                                                AllowSaveRoute = allowSaveRoute_,
                                                HideProgress = true
                                            });
            ExecuteLinkCore(link);
        }

        public static void StartPackage(string appName_, string version_, string package_, string executable_ = null, string args_ = null, bool alwaysNewApp_=true, bool allowSaveRoute_=true)
        {
            var link = GenerateLinkCore(new LauncherMessage(),
                                        new StartupSetting()
                                        {
                                            AppName = appName_,
                                            Version = version_,
                                            Package = package_,
                                            Executable = executable_,
                                            Arguments = args_,
                                            AlwaysNewApp = alwaysNewApp_,
                                            AllowNewApp = true,
                                            AllowSaveRoute = allowSaveRoute_,
                                            HideProgress = true
                                        });
            ExecuteLinkCore(link);
        }

        public static void StartPackageByPath(string packagePath_, string executable_ = null, string args_ = null, bool alwaysNewApp_=true, bool allowSaveRoute_=true)
        {
            var link = GenerateLinkCore(new LauncherMessage(),
                                        new StartupSetting()
                                        {
                                            PackagePath = packagePath_,
                                            Executable = executable_,
                                            Arguments = args_,
                                            AlwaysNewApp = alwaysNewApp_, 
                                            AllowNewApp = true,
                                            AllowSaveRoute = allowSaveRoute_,
                                            HideProgress = true
                                        });
            ExecuteLinkCore(link);
        }

        private class StartupSetting
        {
            public string AppKey { get; set; }
            public bool AlwaysNewApp { get; set; }
            public bool AllowNewApp { get; set; }
            public bool HideProgress { get; set; }
            public bool AllowSaveRoute { get; set; }
            public string AppName { get; set; }
            public string Version { get; set; }
            public string Package { get; set; }
            public string PackagePath { get; set; }
            public string Executable { get; set; }
            public string Arguments { get; set; }
        }
        private static string GenerateLinkCore(LauncherMessage message_, StartupSetting setting_)
        {
            Dictionary<string, object> dictionary = new Dictionary<string, object>()
                {
                    {"Payload",message_}
                };
            if (!string.IsNullOrEmpty(setting_.AppKey))
            {
                dictionary.Add("AppKey", setting_.AppKey);
            }
            else if (!string.IsNullOrEmpty(setting_.PackagePath))
            {
                dictionary.Add("PackagePath", setting_.PackagePath);
            }
            else
            {
                dictionary.Add("AppName", setting_.AppName);
                dictionary.Add("Version", setting_.Version);
                dictionary.Add("Package", setting_.Package);
            }
            if (!string.IsNullOrEmpty(setting_.Executable))
            {
                dictionary.Add("Executable", setting_.Executable);
            }
            if (!string.IsNullOrEmpty(setting_.Arguments))
            {
                dictionary.Add("ExtraArguments", setting_.Arguments);
            }
            if (setting_.AlwaysNewApp) //always new app means can new app 
            {
                dictionary.Add("AlwaysNewApp", true);
            }
            else
            {
                if (!setting_.AllowNewApp)
                {
                    dictionary.Add("CanNewApp", false);
                }
            }
            if (!setting_.AllowSaveRoute)
            {
                dictionary.Add("CanSaveRoute", false);
            }
            
            if (setting_.HideProgress)
            {
                dictionary.Add("HideProgress", true);
            }
            
            string json = JsonConvert.SerializeObject(dictionary, new JsonSerializerSettings(){NullValueHandling = NullValueHandling.Ignore});

            return RequestBuilder.BuildPulishUrl(json, DataFormat.Json, LauncherConsts.DispatchApplicationMessagePublisherId);
        } 

        private static void ExecuteLinkCore(string link_)
        {
            var request = (HttpWebRequest)WebRequest.Create(link_); 
            request.BeginGetResponse(null, null); 
        }
    }

    public enum SerializationFormat
    {
        Binary,
        Json,
    }
}
