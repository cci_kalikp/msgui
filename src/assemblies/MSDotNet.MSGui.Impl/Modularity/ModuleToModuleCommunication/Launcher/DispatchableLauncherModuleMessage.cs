﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Impl.HTTPServer;
using Newtonsoft.Json;

namespace MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication
{
    [Serializable]
    public class DispatchableLauncherModuleMessage
    { 
        public string ModuleName { get; set; }
        public LauncherMessage Payload { get; set; }  
        
        public static string GenerateLink(object message_, string moduleName_=null, SerializationFormat dataFormat_ = SerializationFormat.Json)
        {
            return GenerateLinkCore(new LauncherMessage(message_, dataFormat_), moduleName_);
        }

        public static string GenerateLink(string messageTypeName_, IDictionary<string, object> messageProperties_, string moduleName_=null)
        {
            return GenerateLinkCore(new LauncherMessage(messageTypeName_, messageProperties_), moduleName_);
        }

        public static void Publish(object message_, string moduleName_=null)
        {
            var link = GenerateLinkCore(new LauncherMessage(message_, SerializationFormat.Json), moduleName_);
            ExecuteLinkCore(link);
        }

        public static void Publish(string messageTypeName_, IDictionary<string, object> messageProperties_, string moduleName_ = null)
        {
            var link = GenerateLinkCore(new LauncherMessage(messageTypeName_, messageProperties_), moduleName_);
            ExecuteLinkCore(link);
        }

        private static string GenerateLinkCore(LauncherMessage message_, string moduleName_)
        {
            Dictionary<string, object> dictionary = new Dictionary<string, object>()
                { 
                    {"Payload",message_}
                };
            if (!string.IsNullOrEmpty(moduleName_))
            {
                dictionary["ModuleName"] = moduleName_;
            }
            string json = JsonConvert.SerializeObject(dictionary, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });

            return DispatchableLauncherMessage.RequestBuilder.BuildPulishUrl(json, DataFormat.Json, LauncherConsts.DispatchModuleMessagePublisherId);
        }

        private static void ExecuteLinkCore(string link_)
        {
            var request = (HttpWebRequest)WebRequest.Create(link_);
            request.BeginGetResponse(null, null);
        }
    }
}
