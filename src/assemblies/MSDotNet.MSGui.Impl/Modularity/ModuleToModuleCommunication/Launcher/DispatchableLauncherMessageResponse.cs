﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;

namespace MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication
{
    [Serializable]
    [LauncherMessage]
    public class DispatchableLauncherMessageResponse : IMessageResponse
    { 
        public string Sequence { get; set; }

        public string Content { get; set; }

        public string RawContent { get; set; }

        public bool Suceeded { get; set; }

        public string TargetHostName { get; set; }

        public string ApplicationKey { get; set; }
         
    }
}
