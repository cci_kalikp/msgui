﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication
{
    [Serializable]
    [LauncherMessage]
    public class MessageReceivedMessage
    {
        public string Sequence { get; set; }
        public bool Succeeded { get; set; } 
        public int ProcessId { get; set; }
    }
}
