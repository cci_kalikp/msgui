﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication
{
    [AttributeUsage(AttributeTargets.Class)]
    internal class LauncherMessageAttribute : Attribute
    {
    }
}
