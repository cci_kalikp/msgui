﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication
{
    [Serializable]
    [LauncherMessage]
    public class ReceiverReadyMessage
    { 
        public string InstanceName { get; set; }
        public string MachineName { get; set; }
        public string UserName { get; set; }
        public string ApplicationName { get; set; }
        public int ProcessId { get; set; }
        public string ApplicationKey { get; set; }
        public ApplicationState State { get; set; }
    }

    public enum ApplicationState
    {
        IPCReady = 0,
        Starting = 1,
        Exited = 2,
        Exiting = 3
    }
}
