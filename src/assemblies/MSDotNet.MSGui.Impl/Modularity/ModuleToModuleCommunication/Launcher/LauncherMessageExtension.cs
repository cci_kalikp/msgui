﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication
{
    public static class LauncherMessageExtension
    {
        public static bool IsLauncherMessage(object message_)
        {
            return message_.GetType().GetCustomAttributes(typeof(LauncherMessageAttribute), false).FirstOrDefault() != null;
        }
    }
}
