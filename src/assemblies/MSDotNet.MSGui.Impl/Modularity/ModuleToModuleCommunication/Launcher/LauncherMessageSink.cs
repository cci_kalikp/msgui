﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Core.Persistence.DelayedLoading;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence.DelayedLoading;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;
using MorganStanley.MSDotNet.My; 

namespace MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication
{
    static class LauncherMessageSink
    {
        private static readonly IMSLogger logger = MSLoggerFactory.CreateLogger("LauncherMessageSink");
        private static readonly Dictionary<Type, object> messagePublishers = new Dictionary<Type, object>();
        private static IModulePublisher<MessageReceivedMessage> receivedPublisher;  
        private static ICommunicator communicator;
        private static ApplicationInfo applicationInfo;
        private static IMessageTypeRegistrator messageTypeRegistrator;
        private static string applicationKey;
        private static string launcherName;
        private static IModulePublisher<ReceiverReadyMessage> readyPublisher;
        
        public static void Start(ICommunicator communicator_, IApplication application_, ApplicationInfo applicationInfo_, IMessageTypeRegistrator messageTypeRegistrator_)
        {
            application_.ApplicationLoaded += application__ApplicationLoaded; 
            communicator = communicator_;
            applicationInfo = applicationInfo_;
            messageTypeRegistrator = messageTypeRegistrator_;
            applicationKey = Environment.GetEnvironmentVariable(LauncherConsts.ApplicationUniqueKeyVariable);
            if (string.IsNullOrEmpty(applicationKey))
            {
                string arg = Environment.GetCommandLineArgs().FirstOrDefault(arg_ => arg_.StartsWith("/" + LauncherConsts.ApplicationUniqueKeyVariable));
                if (arg != null)
                {
                    arg = arg.Replace("/" + LauncherConsts.ApplicationUniqueKeyVariable, "").Trim().TrimStart(':', '=').Trim();
                    if (!string.IsNullOrEmpty(arg))
                    {
                        applicationKey = arg.Replace("\"", string.Empty);
                    }
                }
            }

            launcherName = Environment.GetEnvironmentVariable(LauncherConsts.LauncherNameVariable);
            if (string.IsNullOrEmpty(launcherName))
            {
                string arg = Environment.GetCommandLineArgs().FirstOrDefault(arg_ => arg_.StartsWith("/" + LauncherConsts.LauncherNameVariable));
                if (arg != null)
                {
                    arg = arg.Replace("/" + LauncherConsts.LauncherNameVariable, "").Trim().TrimStart(':', '=').Trim();
                    if (!string.IsNullOrEmpty(arg))
                    {
                        launcherName = arg.Replace("\"", string.Empty);
                    }
                } 
            }
            if (string.IsNullOrEmpty(launcherName)) launcherName = LauncherConsts.DefaultApplicationName;

            ((Communicator)communicator).EnableIpc(AppEnvironment.All, false);
            readyPublisher = communicator.GetPublisher<ReceiverReadyMessage>();
            PublishApplicationState(ApplicationState.Starting);
            EventHandler exitingHandler = null;
            Application.Application app = application_ as Application.Application;
            app.ApplicationClosing += exitingHandler = (sender_, args_) => 
                PublishApplicationState(ApplicationState.Exiting);
            EventHandler exitingCancelledHandler;
            app.ApplicationClosingCancelled += exitingCancelledHandler = (sender_, args_) =>
                PublishApplicationState(ApplicationState.IPCReady);

            EventHandler exitHandler = null;
            app.ApplicationClosed += exitHandler = (sender_, args_) =>
                {
                    PublishApplicationState(ApplicationState.Exited);
                    app.ApplicationClosed -= exitHandler;
                    app.ApplicationClosing -= exitingHandler;
                    app.ApplicationClosingCancelled -= exitingCancelledHandler;
                };
            
        }

        static void application__ApplicationLoaded(object sender, EventArgs e)
        {
            if (DelayedProfileLoadingExtensionPoints.DelayLoadProfile)
            {
                DelayedProfileLoadingExtensionPoints.AfterDelayLoadedProfile += DelayedProfileLoadingExtensionPointsOnAfterDelayLoadedProfile; 
            }
            else
            {
                InitialzeChannel();
            }
        }

        private static void DelayedProfileLoadingExtensionPointsOnAfterDelayLoadedProfile(object sender_, EventArgs eventArgs_)
        {
            InitialzeChannel();
            DelayedProfileLoadingExtensionPoints.AfterDelayLoadedProfile -= DelayedProfileLoadingExtensionPointsOnAfterDelayLoadedProfile; 
        }

        private static void InitialzeChannel()
        { 
            if (!((Communicator) communicator).FusionEnabled)
            {
                ((Communicator)communicator).EnableIpc(AppEnvironment.All, false);
            }
            communicator.GetSubscriber<LauncherMessage>().GetObservable().Subscribe(DelegateMessage);
            PublishApplicationState(ApplicationState.IPCReady);
            receivedPublisher = communicator.GetPublisher<MessageReceivedMessage>(); 

        }

        private static void DelegateMessage(LauncherMessage message_)
        {
            try
            {
                object realMessage = null;
                try
                {
                    realMessage = message_.Deserialize(messageTypeRegistrator);
                }
                catch (Exception ex)
                {
                    logger.Error("Failed to deserialize the real message from SerializedMessage", ex);
                    OnFailed(message_);
                    return;
                }
                if (realMessage == null)
                {
                    logger.Error("Failed to deserialize the real message from SerializedMessage due to the type mismatch");
                    OnFailed(message_);
                    return;
                }

                Type messageType = realMessage.GetType();
                object publisher;
                lock (messagePublishers)
                {
                    if (!messagePublishers.TryGetValue(messageType, out publisher))
                    {
                        var getPublisherMethod = communicator.GetType().GetMethod("GetPublisher");
                        getPublisherMethod = getPublisherMethod.MakeGenericMethod(new[] { messageType });
                        publisher = getPublisherMethod.Invoke(communicator, new object[] { });
                        messagePublishers[messageType] = publisher;
                    }
                }
                publisher.GetType().GetMethod("PublishRelativeToCurrent", new Type[] { messageType, typeof(CommunicationTargetFilter) }) 
                         .Invoke(publisher, new object[] { realMessage, CommunicationTargetFilter.MachineInstance });

                OnSuccceded(message_); 
            }
            catch (Exception ex)
            {
                logger.Error("Error delegate SerializedMessage to real message receiver", ex);

                OnFailed(message_);
            }
        }

        private static void OnFailed(LauncherMessage message_)
        {
            receivedPublisher.Publish(new MessageReceivedMessage() { Sequence = message_.Sequence, Succeeded = false }, CommunicationTargetFilter.MachineApplication,
        Environment.MachineName, launcherName);
        }

        private static void OnSuccceded(LauncherMessage message_)
        {
            receivedPublisher.Publish(new MessageReceivedMessage()
            {
                Sequence = message_.Sequence,
                Succeeded = true,
                ProcessId = Process.GetCurrentProcess().Id
            }, CommunicationTargetFilter.MachineApplication,
Environment.MachineName, launcherName);
        }

        private static void PublishApplicationState(ApplicationState state_)
        {
            readyPublisher.Publish(
                new ReceiverReadyMessage()
                {
                    InstanceName = applicationInfo.Instance,
                    MachineName = applicationInfo.Host,
                    UserName = applicationInfo.User,
                    ApplicationName = applicationInfo.Name,
                    ProcessId = Process.GetCurrentProcess().Id,
                    ApplicationKey = applicationKey,
                    State = state_
                }, CommunicationTargetFilter.MachineApplication,
                Environment.MachineName, launcherName);
        }
    }
}
