﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.IED.Concord.Configuration; 

namespace MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication
{
    internal static class LauncherConsts
    {
        private static int httpServerPort = 0;

        public static int GetHTTPServerPort(bool readFromLauncher_)
        {
            lock (typeof (LauncherConsts))
            {
                if (httpServerPort == 0)
                {
                    lock (typeof (LauncherConsts))
                    {
                        string httpServerPortStr = null;
                        if (readFromLauncher_)
                        {
                            httpServerPortStr = System.Configuration.ConfigurationManager.AppSettings["HTTPServerPort"];
                        }
                        else
                        {
                            Configurator config = null;
                            try
                            {
                                config = ConfigurationManager.GetConfig("LauncherConfig") as Configurator;
                            }
                            catch (Exception ex)
                            {

                            }
                            if (config != null)
                            {
                               httpServerPortStr = config.GetValue("HTTPServerPort", null);
                               
                            } 
                        }
                        if (!string.IsNullOrEmpty(httpServerPortStr))
                        {
                            Int32.TryParse(httpServerPortStr, out httpServerPort);
                        }
                        if (httpServerPort == 0) httpServerPort = 10004;
                    }
                }
            }
            return httpServerPort;
        }

        //public static readonly int HTTPServerPort = Settings.Default.HTTPServerPort;
        public const string DispatchApplicationMessagePublisherId = "pubid5AEA7CA9";
        public const string DispatchModuleMessagePublisherId = "pubidid5AEA7CA0";
        public const bool EnableHash = true;
        public const string HashSeed = "Seed9770AFDEC54E";

        public const string DefaultApplicationName = "Sales and Trading Launcher";
        public const string ApplicationUniqueKeyVariable = "ApplicationUniqueKey";
        public const string LauncherNameVariable = "LauncherName";
    }
}
