﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary; 
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;
using Newtonsoft.Json;

namespace MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication
{
    [Serializable]
    [LauncherMessage]
    public class LauncherMessage
    {
        public string Content { get; set; } 
        public string Sequence { get; set; }
        public SerializationFormat Format { get; set; }
        public string Type { get; set; }
        public LauncherMessage()
        {
        }

        public LauncherMessage(object message_, SerializationFormat format_)
        {
            this.Format = format_; 
            if (Format == SerializationFormat.Binary)
            {
                byte[] messageBytes;
                using (var stream = new MemoryStream())
                {
                    new BinaryFormatter().Serialize(stream, message_);
                    messageBytes = stream.GetBuffer();
                }
                Content = Convert.ToBase64String(messageBytes);
            }
            else
            {
                Dictionary<string, object> propertyValues = new Dictionary<string, object>();
                foreach (var propertyInfo in message_.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public))
                {
                    if (propertyInfo.CanRead && propertyInfo.GetIndexParameters().Length == 0)
                    {
                        var value = propertyInfo.GetValue(message_,null);
                        if (!IsNullValue(value))
                        {
                            propertyValues[propertyInfo.Name] = value; 
                        }
                    } 
                }
                var meta =
    message_.GetType().GetCustomAttributes(typeof(MSGui.Core.Messaging.MessageAttribute), false).FirstOrDefault() as MSGui.Core.Messaging.MessageAttribute;
                if (meta != null && !string.IsNullOrEmpty(meta.Id))
                {
                    Type = meta.Id;
                }
                else
                {
                    this.Type = message_.GetType().FullName;        
                }
                this.Content = JsonConvert.SerializeObject(propertyValues);
            }
        }

        private static bool IsNullValue(object value_)
        {
            if (value_ == null) return true;
            if (value_.GetType().IsValueType)
            try
            {
                decimal numeric = Convert.ToDecimal(value_);
                if (numeric == 0m) return true;
            }
            catch  
            {
                 
            }
            return false;
        }

        public LauncherMessage(string messageTypeName_, IDictionary<string, object> messageProperties_)
        {
            this.Format = SerializationFormat.Json;
            this.Type = messageTypeName_;
            this.Content = JsonConvert.SerializeObject(messageProperties_);
        }
         

        static readonly Dictionary<string, Type> types = new Dictionary<string, Type>(); 
        public object Deserialize(IMessageTypeRegistrator messageTypeRegistrator_)
        {
            if (Content == null)
            {
                return null;
            } 

            if (this.Format == SerializationFormat.Binary)
            {

                var bytes = Convert.FromBase64String(Content);
                using (var stream = new MemoryStream(bytes))
                {
                    return new BinaryFormatter().Deserialize(stream);
                }
            }
            else
            { 
                Type type = null;
                lock (types)
                {
                    if (!types.TryGetValue(Type, out type))
                    {
                        try
                        {
                            type = messageTypeRegistrator_.ResolveType(Type);
                        }
                        catch
                        {

                        } 
                        if (type == null)
                        {
                            type = System.Type.GetType(Type);
                            if (type == null)
                            {
                                type = AppDomain.CurrentDomain.GetAssemblies()
                                .Select(a_ => a_.GetType(Type))
                                .FirstOrDefault(t_ => t_ != null);
                            }
                        }
                        if (type != null)
                        {
                            types.Add(Type, type);
                        }
                    }
                }
                
                if (type == null) return null;
                return JsonConvert.DeserializeObject(Content, type); 
            } 
        }

        
    }
}
