﻿using System;

namespace MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication
{
    internal interface IMessageWithPayload
    {
        Type GetPayloadType();
        bool IsPayloadSerializable();
    }
}
