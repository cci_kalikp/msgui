﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.Serialization;

//using Microsoft.Practices.Composite.Presentation.Events;
using Microsoft.Practices.Unity;
//using MorganStanley.MSDotNet.CafGUI.Toolkit.Messaging;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging;
using MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging.CPSMessaging;
using MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging.FusionMessaging;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;


namespace MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication
{
    internal sealed class Communicator : ICommunicator, IDisposable, ILocationHolder
    {
        private readonly EventRegistrator _eventRegistrator;
        private readonly IDictionary<Type, KeyValuePair<string, bool>> _channels;
        private readonly ApplicationInfo _applicationInfo;
        private readonly IMSDesktopEventAggregator _eventAggregator;
        private readonly IFormatter _serializer;
        private readonly IAdapterService _adapterService;
        private readonly IMessageTypeRegistrator _typeRegistrator;
        private readonly LocationHolder _locationHolder;
        private readonly ICommunicatorSettings _settings;

        private IModuleMessenger _fusionMessenger;
        private IModuleMessenger _cpsMessenger;
        private AppEnvironment _enviroment;

        internal static bool IsIpcForAll = false;
        internal static bool IsImcForAll = false;
        private readonly IModuleGroup _moduleGroup;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="adapterService"></param>
        /// <param name="container"></param>
        /// <param name="typeRegistrator"></param>
        /// <param name="serializer"></param>
        /// <param name="settings"></param>
        /// <param name="application">null if this communicator works locally only</param>
        /// <param name="moduleGroup"></param>
        public Communicator(IAdapterService adapterService, IUnityContainer container,
                            IMessageTypeRegistrator typeRegistrator, IFormatter serializer,
                            IApplication application, ICommunicatorSettings settings,
                            IModuleGroup moduleGroup)
        {
            if (settings == null)
            {
                // In isolated modules scenarios, this should be executed in context of the host application.
                settings = new CommunicatorSettings(Process.GetCurrentProcess().Id);
            }

            _moduleGroup = moduleGroup;
            _locationHolder = new LocationHolder(); 
            _channels = new Dictionary<Type, KeyValuePair<string, bool>>();
            _settings = settings;

            if (container.IsRegistered<IMSDesktopEventAggregator>())
            {
                _eventAggregator = container.Resolve<IMSDesktopEventAggregator>();
            }

            _eventRegistrator = new EventRegistrator(adapterService, _eventAggregator, typeRegistrator);
            
            _typeRegistrator = typeRegistrator;

            if (application == null)
                return;


            var appName = application.Name;
            var instance = application.GetApplicationInstance();

            string desktopName;
            if (InterProcessMessenger.IsVirtualDesktopSupported)
                desktopName = DesktopHelper.GetCurrentDesktopName();
            else
            {
                desktopName = "MSDesktop";
            }
            
            _applicationInfo = new ApplicationInfo(Environment.MachineName, Environment.UserName, desktopName, appName,
                                               instance);

            _fusionMessenger = new NullModuleMessenger();
            _cpsMessenger = new NullModuleMessenger();

            _locationHolder.Application = new CafGuiApplicationInfoProxy(_applicationInfo);
            _serializer = serializer;
            _adapterService = adapterService;

            LauncherMessageSink.Start(this, application, _applicationInfo, typeRegistrator);

        }

        public IAdapterService AdapterService
        {
            get { return _adapterService; }
        }

        public void EnableIpc(AppEnvironment env)
        {
            EnableIpc(env, true);
        }
        internal void EnableIpc(AppEnvironment env, bool ipcForAll_)
        { 
            if (_settings.IpcStatus != CommunicationChannelStatus.Disabled)
            {
                _enviroment = env;
                DisposeFusionMessenger(false);

                var fusion = new FusionModuleMessenger(_applicationInfo, _serializer, env);
                if (fusion.Initialize())
                    _fusionMessenger = fusion;

                else _fusionMessenger = new NullModuleMessenger();
                _fusionMessenger.MessageReceived += OnFusionMessageReceived;
            }
            if (ipcForAll_) IsIpcForAll = true;
        }

        internal bool FusionEnabled
        {
            get { return _fusionMessenger is FusionModuleMessenger; }
        }

        internal bool CpsEnabled
        {
            get
            {
                return _cpsMessenger is CpsModuleMessenger;
            }
        }

        internal bool CpsInitialized { get; private set; }

        private void EnableImc(AppEnvironment env,  bool imcForAll_)
        {
            if (_settings.ImcStatus != CommunicationChannelStatus.Disabled)
            {
                _enviroment = env;
                DisposeCpsMessenger(false);

                var loader = new CpsServiceLoader(_settings.ImcStatus);
                var cps = new CpsModuleMessenger(_applicationInfo, _serializer, _enviroment, loader);
                if (cps.Initialize())
                    _cpsMessenger = cps;
                else _cpsMessenger = new NullModuleMessenger();
                _cpsMessenger.MessageReceived += OnCpsMessageReceived;
                CpsInitialized = true;
                var copy = AfterCpsInitialized;
                if (copy != null)
                {
                    copy(this, EventArgs.Empty);
                }
            }
            if (imcForAll_) IsImcForAll = true;
        }

        public void EnableKrakenIpcImc(AppEnvironment env)
        {
            EnableIpc(env, false);
            EnableImc(env, false);
        }

        public void EnableIpcImc(AppEnvironment env)
        { 
            EnableIpc(env, true);
            EnableImc(env, true);
        }

        public void DisableIpcImc()
        {
            DisableImc();
            DisableIpc();
        }

        private void DisableIpc()
        {
            DisposeFusionMessenger();
            _fusionMessenger = new NullModuleMessenger();
            IsIpcForAll = false;
        }


        public void DisableImc()
        {
            DisposeCpsMessenger();
            _cpsMessenger = new NullModuleMessenger();
            IsImcForAll = false;
        }

        /// <summary>
        /// DO NOT REMOVE: INVOKED VIA REFLECTION.
        /// </summary>
        private void DispatchMessage<TMessage>(TMessage message, CommunicationTargetFilter filter, params string[] location)
        {
            var publisher = GetPublisher<TMessage>() as ModulePublisher<TMessage>;
            publisher.PublishToLocal(message, filter, location);
        }

        private void OnCpsMessageReceived(object sender, ModuleMessageEventArgs args)
        {
            OnMessageReceived(sender, args);
        }

        private void OnFusionMessageReceived(object sender, ModuleMessageEventArgs args)
        {
            OnMessageReceived(sender, args);
        }

        private void OnMessageReceived(object sender, ModuleMessageEventArgs args)
        {
            var msgType = args.MessageType;
            if (!IsInChannel(msgType))
            {
                var addChannelMth = GetType().GetMethod("AddChannel");
                addChannelMth.MakeGenericMethod(new[] {msgType}).Invoke(this, new object[] {});
            }

            if (IsInLocalChannel(msgType))
                return;

            var methodInfo = GetType().GetMethod("DispatchMessage", BindingFlags.NonPublic | BindingFlags.Instance);
            var method = methodInfo.MakeGenericMethod(new[] {msgType});


            method.Invoke(this, new[] {args.Content, CommunicationTargetFilter.All, null});
        }


        public void AddChannel<TMessage>()
        {
            _typeRegistrator.RegisterType<TMessage>();
            AddChannel<TMessage>(true);
        }

        public IEnumerable<Type> GetChannels()
        {
            lock (_channels)
            {
                return _channels.Keys;    
            }
            
        }

        public bool IsInExternalChannel<TMessage>()
        {
            lock (_channels)
            {
                if (!IsInChannel<TMessage>())
                    return false;

                return _channels[typeof(TMessage)].Value;    
            }
        }

        private bool IsInLocalChannel(Type msgType)
        {
            lock (_channels)
            {
                return IsInChannel(msgType) && !_channels[msgType].Value;
            }
        }
        public bool IsInLocalChannel<TMessage>()
        {
            return IsInLocalChannel(typeof (TMessage));
        }

        private string GetChannel(Type msgType)
        {
            lock (_channels)
            {
                return _channels.ContainsKey(msgType) ? _channels[msgType].Key : null;    
            }
        }

        public string GetChannel<TMessage>()
        {
            return GetChannel(typeof (TMessage));
        }

        private bool IsInChannel(Type msgType)
        {
            lock (_channels)
            {
                return _channels.ContainsKey(msgType);
            }
        }

        public bool IsInChannel<TMessage>()
        {
            return IsInChannel(typeof (TMessage));
        }

        private void AddChannel<TMessage>(bool joinExternal)
        {
            if (joinExternal && _applicationInfo == null)
                throw new ApplicationException("Join to external in a local only communicator");

            lock (_channels)
            {
                if (IsInChannel<TMessage>())
                {
                    if (_channels[typeof (TMessage)].Value != joinExternal)
                        MSLog.MSLog.Error().Append("Message Type is already in channel" + typeof (TMessage)).Send();
                    return;
                }

                var id = "module_communication_ch" + Guid.NewGuid().ToString("N");
                _channels[typeof (TMessage)] = new KeyValuePair<string, bool>(id, joinExternal);


                var publisher = _eventRegistrator.RegisterPublisher<TMessage>(id);
                ((ILocationHolder) publisher).Application = new CafGuiApplicationInfoProxy(_applicationInfo);

                var subscriber = _eventRegistrator.RegisterSubscriber<TMessage>(id);
                ((ILocationHolder) subscriber).Application = new CafGuiApplicationInfoProxy(_applicationInfo);

                //_eventRegistrator.Connect<TMessage, TMessage>(id, id);
                var modulePublisher = GetPublisher<TMessage>();

                if (_eventAggregator != null)
                {
                    var aggregatorEvent = _eventAggregator.GetModuleToModuleEvent<TMessage>();

                    //hack to connect Aggreaget Event with M2M 
                    subscriber.GetObservable().Subscribe(aggregatorEvent.PublishToExternal);

                    aggregatorEvent.InternalSubscribe(msg =>
                    {
                        modulePublisher.Publish(msg);
                        Console.WriteLine(msg);
                    }, MSDesktopThreadOption.BackgroundThread, true);
                }

                var connectMethodInfo = _eventRegistrator.GetType().GetMethod("Connect",
                                                                              new[] {typeof (string), typeof (string)});
                foreach (var channel in GetChannels())
                {
                    var channelId = _channels[channel].Key;

                    var method = connectMethodInfo.MakeGenericMethod(new[] {typeof (TMessage), channel});
                    method.Invoke(_eventRegistrator, new object[] {id, channelId});

                    method = connectMethodInfo.MakeGenericMethod(new[] {channel, typeof (TMessage)});
                    method.Invoke(_eventRegistrator, new object[] {channelId, id});

                }
            }

        }

        public void RemoveChannel<TMessage>()
        {
            KeyValuePair<string, bool> kvp;
            lock (_channels)
            {
                if (_channels.TryGetValue(typeof(TMessage), out kvp))
                {
                    var methodInfo = _eventRegistrator.GetType()
                                                      .GetMethod("GetRegisteredPublisher", new[] { typeof(string) });
                    methodInfo = methodInfo.MakeGenericMethod(typeof(TMessage));
                    var publisher =
                        methodInfo.Invoke(_eventRegistrator, new object[] { kvp.Key }) as
                        IPublisher<TMessage>;
                    if (publisher != null)
                        _eventRegistrator.UnregisterPublisher(publisher);

                    methodInfo = _eventRegistrator.GetType().GetMethod("GetRegisteredSubscriber", new[] { typeof(string) });
                    methodInfo = methodInfo.MakeGenericMethod(typeof(TMessage));
                    var subscriber =
                        methodInfo.Invoke(_eventRegistrator, new object[] { kvp.Key }) as
                        ISubscriber<TMessage>;

                    if (subscriber != null)
                        _eventRegistrator.UnregisterSubscriber(subscriber);

                    _channels.Remove(typeof(TMessage));
                }
                else
                {
                    MSLog.MSLog.Debug().Append("Message Type is not in channel" + typeof(TMessage)).Send();
                }
            }
           
        }

        public IModulePublisher<TMessage> GetPublisher<TMessage>()
        {
            AddChannel<TMessage>();
            return new ModulePublisher<TMessage>(this, _eventRegistrator, _fusionMessenger, _cpsMessenger, _moduleGroup);
        }

        public IModuleSubscriber<TMessage> GetSubscriber<TMessage>()
        {
            AddChannel<TMessage>();
            return new ModuleSubscriber<TMessage>(this, _eventRegistrator, _moduleGroup);
        }

        public void Dispose()
        {
            DisposeCpsMessenger();
            DisposeFusionMessenger();

            var types = new HashSet<Type>();
            foreach (var type in _channels.Keys)
            {
                types.Add(type);
            }

            foreach (var tp in types)
            {
                var methodInfo = GetType().GetMethod("RemoveChannel");
                methodInfo = methodInfo.MakeGenericMethod(tp);
                methodInfo.Invoke(this, new object[] {});
            }
        }

        private void DisposeFusionMessenger(bool unsubscribe = true)
        {
            DisposeMessenger(_fusionMessenger, unsubscribe);
        }

        private void DisposeCpsMessenger(bool unsubscribe = true)
        {
            DisposeMessenger(_cpsMessenger, unsubscribe);
        }

        private void DisposeMessenger(IModuleMessenger messenger, bool unsubscribe)
        {
            if (unsubscribe && messenger != null)
            {
                messenger.MessageReceived -= OnMessageReceived;
            }

            var disposableMessenger = messenger as IDisposable;
            if (disposableMessenger != null)
            {
                disposableMessenger.Dispose();
            }
        }

        public CafGuiApplicationInfoProxy Application
        {
            get { return _locationHolder.Application; }
            set { _locationHolder.Application = value; }
        }

        public string Tab
        {
            get { return _locationHolder.Tab; }
            set { _locationHolder.Tab = value; }
        }

        public bool BelongsToLocation(CommunicationTargetFilter targetFilter, params string[] locations)
        {
            return _locationHolder.BelongsToLocation(targetFilter, locations);
        }

        internal event EventHandler AfterCpsInitialized;
    }
}
