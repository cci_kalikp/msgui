﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Modularity/ResolverModuleInfo.cs#9 $
// $Change: 869092 $
// $DateTime: 2014/03/03 03:21:59 $
// $Author: caijin $

using System;
using System.Collections.Specialized;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.Prism;

namespace MorganStanley.MSDotNet.MSGui.Impl.Modularity
{
    /// <summary>
    /// Defines the metadata that describes a module
    /// which could exist on AFS.
    /// </summary>
    [Serializable]
    public class ResolverModuleInfo : ModuleInfo, IMSDesktopModuleInfo
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ResolverModuleInfo"/> class.
        /// </summary>
        public ResolverModuleInfo()
            : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ResolverModuleInfo"/> class.
        /// </summary>
        /// <param name="name_">The module's name.</param>
        /// <param name="type_">The module <see cref="Type"/>'s AssemblyQualifiedName.</param>
        /// <param name="dependsOn_">The modules this instance depends on.</param>
        public ResolverModuleInfo(string name_, string type_, params string[] dependsOn_)
            : base(name_, type_, dependsOn_)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ResolverModuleInfo"/> class.
        /// </summary>
        /// <param name="name_">The module's name.</param>
        /// <param name="type_">The module's type.</param>
        public ResolverModuleInfo(string name_, string type_)
            : base(name_, type_)
        {
        }

        /// <summary>
        /// Gets the assembly name.
        /// </summary>
        /// <value>The assembly name of the module.</value>
        public string AssemblyName { get; private set; }

        /// <summary>
        /// Gets extra values expressed as unrecognized attributes of the configuration element.
        /// </summary>
        public StringDictionary ExtraValues { get; internal set; }
    }
}