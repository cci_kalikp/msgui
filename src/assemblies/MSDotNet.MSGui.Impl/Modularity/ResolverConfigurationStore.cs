﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Modularity/ResolverConfigurationStore.cs#8 $
// $Change: 849973 $
// $DateTime: 2013/10/16 17:23:36 $
// $Author: hrechkin $

using System;
using System.Configuration;
using System.IO;

namespace MorganStanley.MSDotNet.MSGui.Impl.Modularity
{
    /// <summary>
    /// Defines a store for the module metadata.
    /// </summary>
    public class ResolverConfigurationStore : IResolverConfigurationStore
    {
        /// <summary>
        /// Base directory for the store.
        /// </summary>
        private readonly string m_path;

        /// <summary>
        /// Initializes a new instance of the <see cref="ResolverConfigurationStore"/> class.
        /// It uses the Application's directory as the base for looking config files.
        /// </summary>
        public ResolverConfigurationStore()
        {
            m_path = null;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ResolverConfigurationStore"/> class.
        /// </summary>
        /// <param name="path_">The path to a file or a directory from which to start searching for the 
        /// configuration files.</param>
        public ResolverConfigurationStore(string path_)
        {
            m_path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, path_ ?? string.Empty);
        }

        /// <summary>
        /// Gets the module configuration data.
        /// </summary>
        /// <returns>A <see cref="ResolverModuleConfigurationSection"/> instance.</returns>
        public ResolverModuleConfigurationSection RetrieveModuleConfigurationSection()
        {
            if (m_path == null)
            {
                Configuration exeConfiguration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                return exeConfiguration.GetSection("modules") as ResolverModuleConfigurationSection;
            }

            if (File.Exists(m_path))
            {
                return GetConfiguration(m_path).GetSection("modules") as ResolverModuleConfigurationSection;
            }

            if (System.IO.Directory.Exists(m_path))
            {
                foreach (
                    string fileName in System.IO.Directory.GetFiles(m_path, "*.config", SearchOption.TopDirectoryOnly))
                {
                    Configuration configuration = GetConfiguration(Path.Combine(m_path, fileName));

                    var section = configuration.GetSection("modules") as ResolverModuleConfigurationSection;

                    if (section != null)
                    {
                        return section;
                    }
                }
            }

            return null;
        }

        private static Configuration GetConfiguration(string configFilePath)
        {
            var map = new ExeConfigurationFileMap {ExeConfigFilename = configFilePath};
            return ConfigurationManager.OpenMappedExeConfiguration(map, ConfigurationUserLevel.None);
        }
    }
}
