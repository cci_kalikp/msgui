﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/ShellWithLauncher.cs#102 $
// $Change: 902331 $
// $DateTime: 2014/10/24 05:43:11 $
// $Author: caijin $

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Linq; 
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml.Linq; 
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Controls.Controls;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Options;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.BarWithWindow;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.LauncherBar;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.ViewModels;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Themes;
using MorganStanley.MSDotNet.My;
using Form = System.Windows.Forms.Form;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;


namespace MorganStanley.MSDotNet.MSGui.Impl.Shell
{
	internal class ShellWithLauncher : IShell, IPersistable, INotifyPropertyChanged
	{
		private static readonly IMSLogger m_logger = MSLoggerFactory.CreateLogger<ShellWithLauncher>();

		private readonly IUnityContainer m_container;
		internal readonly IExpandableBar m_launcherExpandableBar;
		private readonly Window m_window;
		private readonly IWindowManager m_windowManager;
		private readonly Window m_windowManagerWindow;
		private readonly BarItemsHolder m_barItemsHolder;
		private readonly object m_isLoadingOrSaving = new object();

		private PersistenceProfileService m_persistenceProfileService;
		private IProfileMonitor m_profileMonitor;

		private LayoutLoadStrategy m_layoutLoadStrategy;
		private LayoutLoadStrategyCallback m_layoutLoadStrategyCallback;

		private bool m_shown;
		internal readonly OptionsButton m_optionsButton;
		private bool m_themesDisabled;
	    private InitialSettings m_initialSettings;
		private Func<bool> m_tryCloseConcordMainWindow;
		private readonly Application.Application m_application;
		private ApplicationOptions m_options;
		private System.Windows.Controls.Primitives.StatusBar m_statusBar; 
        private TileItemCollectionModel tileItemCollectionModel;
	    private TabbedDock m_tabbedDock; 


	    private UIElement _titleBar;

        private readonly SkipSaveProfileOnExitCondition m_skipSaveProfileOnExitCondition;

		//private int m_quitDialogSeconds = -1;
		//private bool m_quitDialogSave;

		public ShellWithLauncher(IUnityContainer container_, InitialSettings initialSettings_)
		{ 
			m_container = container_;
		    m_initialSettings = initialSettings_;
			m_application = container_.Resolve<IApplication>() as Application.Application;
			if (m_application == null)
			{
				throw new InvalidOperationException("Application must be IApplication");
			}

            m_skipSaveProfileOnExitCondition = initialSettings_.SkipSaveProfileOnExitCondition;
            switch (initialSettings_.ShellMode)
			{
				case ShellMode.LauncherBarAndFloatingWindows:
					m_launcherExpandableBar = new BarWindow(initialSettings_.EnableSoftwareRendering, container_);
					m_window = (Window)m_launcherExpandableBar;
                    _titleBar = ((BarWindow)m_window).TitleBar;

			        ((BarWindow) m_window).MinimizeNeeded += MinimizeNeeded;

					m_window.Title = string.IsNullOrEmpty(initialSettings_.AppBarTitle) ? string.Format("{0}", m_application.Name) : initialSettings_.AppBarTitle;
					m_launcherExpandableBar.Version = string.Format("v{0}", m_application.Version);

					if (m_application.Icon != null)
					{
						m_window.Icon = m_application.Icon;
					}

					//Creating panel that acts like a wrapPanel for buttons and like StackPanel for anything else
					m_barItemsHolder = new BarItemsHolder(m_launcherExpandableBar);
					m_launcherExpandableBar.AddContent(m_barItemsHolder as UIElement);

					//Initialize main button menu items
					m_optionsButton = new OptionsButton((BarWindow)m_launcherExpandableBar);

					FloatingOnlyDockViewModel floatingOnlyDockModel = new FloatingOnlyDockViewModel(container_.Resolve<IHidingLockingManager>(), container_)
					{
						WindowRenameGesture = initialSettings_.WindowRenameGesture,
						EnforceSizeRestrictions = !initialSettings_.DisableEnforcedSizeRestrictions,
						UseOwnedWindows = !initialSettings_.EnableNonOwnedWindows,
						MainWindow = m_window
					};

                    if (DockViewModel.UseHarmonia)
                    {
                        HarmoniaFloatingOnlyDock.Bind(floatingOnlyDockModel);
                        m_windowManagerWindow = m_window;
                    }
                    else
                    {
                        m_windowManagerWindow = new FloatingOnlyDock
                            {
                                DataContext = floatingOnlyDockModel
                            };
                    }
			        m_windowManager = floatingOnlyDockModel;
					m_statusBar = new System.Windows.Controls.Primitives.StatusBar();
                    m_statusBar.Height = ThemeExtensions.StatusBarHeight;
                    m_statusBar.ContextMenu = new ContextMenu();
					break;


				case ShellMode.LauncherBarAndWindow:
					m_window = new WindowWithBar(initialSettings_.EnableSoftwareRendering);
					string titleBase = string.Format("{0} - (v{1})", m_application.Name, m_application.Version);
					m_window.Title = titleBase;

			        _titleBar = ((WindowWithBar) m_window).TitleBar;

                    if (m_application.Icon != null)
                    {
                        m_window.Icon = m_application.Icon;
                    }



					//TODO show current layout too
					m_launcherExpandableBar = new ExpandableBar();
					m_barItemsHolder = new BarItemsHolder(m_launcherExpandableBar);
					m_launcherExpandableBar.AddContent(m_barItemsHolder);
					m_optionsButton = new OptionsButton();
                    m_optionsButton.MaxHeight = ExpandableBar.HeightShrinked;


                    ((WindowWithBar)m_window).LauncherBar = (UIElement)m_launcherExpandableBar;
                    if (ShellModeExtension.HideUIGranularity.HasFlag(HideLockUIGranularity.HideLauncherBar))
                    {
                        ((ExpandableBar) m_launcherExpandableBar).Visibility = Visibility.Collapsed;
                    }

					TabbedDockViewModel dockViewModel = new TabbedDockViewModel(container_.Resolve<IChromeRegistry>() as ChromeRegistry,
                        container_, 
                        container_.Resolve<IHidingLockingManager>())
					{
						WindowRenameGesture = initialSettings_.WindowRenameGesture,
						EnforceSizeRestrictions = !initialSettings_.DisableEnforcedSizeRestrictions,
						UseOwnedWindows = !initialSettings_.EnableNonOwnedWindows,
                        MainWindow = m_window
                        
					};
					m_tabbedDock = new TabbedDock
					{
						DataContext = dockViewModel
					};
					
					m_windowManager = dockViewModel;
					m_windowManagerWindow = m_window;

                    m_window.Content = m_tabbedDock;
			        m_window.Loaded += (sender_, args_) =>
			            {  
                            var label = m_window.FindVisualChildByName<EnvironmentLabel>("ShellWindowEnvironmentLabel");
                            if (label != null)
                            {
                                label.DataContext = dockViewModel.EnvironmentLabelViewModel;  
                            }
			            };
					m_statusBar = new System.Windows.Controls.Primitives.StatusBar();
			        m_statusBar.ContextMenu = new ContextMenu
			            {
			                ItemContainerStyle = (Style) m_window.FindResource("ContextMenuItemStyle")
			            };
                    ((WindowWithBar)m_window).StatusBar = m_statusBar;
                    m_statusBar.Height = ThemeExtensions.StatusBarHeight;

					//m_optionsButton = new OptionsButton(null);)
					break;
			}
            if (ShellModeExtension.MenuPopupAnimationDisabled)
            {
                ((FrameworkElement) m_launcherExpandableBar).Resources.Add(SystemParameters.MenuPopupAnimationKey, PopupAnimation.None);
            } 
		    container_.RegisterInstance(m_windowManager);

		    tileItemCollectionModel = new TileItemCollectionModel(container_.Resolve<IChromeRegistry>() as ChromeRegistry,
		                                                          container_,
                                                                  container_.Resolve<IHidingLockingManager>(), m_windowManager, initialSettings_);
			m_optionsButton.DeleteCurrentCalled += OnDeleteCurrentCalled;
			m_optionsButton.ExitCalled += OnExitCalled;
			m_optionsButton.DockCalled += OnDockCalled;
			m_optionsButton.UnDockCalled += OnUnDockCalled;
			m_optionsButton.SaveCurrentLayoutMenu.Click += new RoutedEventHandler((a, b) => SaveCurrentProfile());
		    m_optionsButton.RedrawNeeded += OnRedrawNeeded;
            m_optionsButton.MinimizeNeeded += MinimizeNeeded;

			m_barItemsHolder.AddChild(m_optionsButton, true);
            if (ChromeManagerBase.MainWindowFocusGrabAtStartupSuppressed)
            {
                m_window.ShowActivated = m_windowManagerWindow.ShowActivated = false;
                EventHandler applicationLoaded = null;
                m_application.ApplicationLoaded += applicationLoaded = (sender_, args_) =>
                {
                    m_window.ShowActivated = m_windowManagerWindow.ShowActivated = true;
                    m_application.ApplicationLoaded -= applicationLoaded;
                };
            }

			m_window.Closing += OnClosing;
			m_window.Closed += OnClosed;

            m_window.StateChanged += OnShellWindowStateChanged;

            m_container.Resolve<IHidingLockingManager>().HideUIGranularityChanged += OnHideUIGranularityChanged;
		    OnHideUIGranularityChanged(null, null);

			//m_windowManagerwindow = new Window();      
			//TabbedDock td = new TabbedDock();
			//td.SetRenameGesture(gesture_);
			//m_windowManagerwindow.Content = td;
			//m_windowManager = td;     

            var persistenceService = container_.Resolve<IPersistenceProfileServiceProxy>();
            persistenceService.Shell = this;
            m_window.EnableMinimizeScreenshotSupport();
		}


	    private void OnShellWindowStateChanged(object sender, EventArgs eventArgs)
	    {
	        m_windowManagerWindow.WindowState = m_window.WindowState;
	    }

	    private void MinimizeNeeded(object sender, EventArgs eventArgs)
	    {
            m_window.WindowState = WindowState.Minimized;
	    }

	    private void OnHideUIGranularityChanged(object sender_, EventArgs eventArgs_)
        {
            if (m_container.Resolve<IHidingLockingManager>()
                           .Granularity
                           .HasFlag(HideLockUIGranularity.LockRelativeWindowsPositions))
            {
                _previousLocation = new Point(m_window.Left, m_window.Top);
                m_window.LocationChanged += OnWindowLocationChanged;
            }
            else
            {
                m_window.LocationChanged -= OnWindowLocationChanged;
            }
        }

	    internal void OnRedrawNeeded(object o, EventArgs e)
	    {
	        RerdawLoadMenu(null, null);
	        RerdawSaveMenu(null, null);
	    }

	    private void OnExitCalled(object sender_, EventArgs e_)
		{
			m_window.Close();
		}

		private void OnDockCalled(object sender_, EventArgs e_)
		{
			m_launcherExpandableBar.SetDock(true);
		}

		private void OnUnDockCalled(object sender_, EventArgs e_)
		{
			m_launcherExpandableBar.SetDock(false);
		}

		private void OnDeleteCurrentCalled(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(m_profileMonitor.CurrentProfile))
			{
				return;
			}
			TaskDialogSimpleResult result = TaskDialog.ShowMessage
			  (string.Format("Are you sure you want to delete the current profile {0}", m_profileMonitor.CurrentProfile),
			   string.Format("Delete {0}?", m_profileMonitor.CurrentProfile),
			   TaskDialogCommonButtons.YesNo,
			   VistaTaskDialogIcon.Warning);
			if (result == TaskDialogSimpleResult.Yes)
			{
				m_persistenceProfileService.DeleteProfile(m_profileMonitor.CurrentProfile);
                RerdawLoadMenu(null, null);
                RerdawSaveMenu(null, null);
			}
		}

		public BarItemsHolder Holder
		{
			get
			{
				return m_barItemsHolder;
			}
		}

		public void LoadState(XDocument state_)
		{
			try
			{
				lock (m_isLoadingOrSaving)
				{
					XElement mainElement = state_.Root;
					if (mainElement != null && mainElement.Name == "MainWindow")
					{
						XAttribute widthAttr = mainElement.Attribute("Width");
						double width;
						if (widthAttr != null && widthAttr.Value != null && Double.TryParse(widthAttr.Value, NumberStyles.Any, CultureInfo.InvariantCulture, out width))
						{
							m_window.Width = width;
						}

						XAttribute heightAttr = mainElement.Attribute("Height");
						double height;
						if (heightAttr != null && heightAttr.Value != null && Double.TryParse(heightAttr.Value, NumberStyles.Any, CultureInfo.InvariantCulture, out height))
						{
							m_window.Height = height;
						}

						XAttribute topAttr = mainElement.Attribute("Top");
						double top;
						if (topAttr != null && topAttr.Value != null && Double.TryParse(topAttr.Value, NumberStyles.Any, CultureInfo.InvariantCulture, out top))
						{
							m_window.Top = top;
						}

						XAttribute leftAttr = mainElement.Attribute("Left");
						double left;
						if (leftAttr != null && leftAttr.Value != null && Double.TryParse(leftAttr.Value, NumberStyles.Any, CultureInfo.InvariantCulture, out left))
						{
							m_window.Left = left;
						}

						XAttribute themeAttr = mainElement.Attribute("Theme");
						if (themeAttr != null && themeAttr.Value != null && !m_themesDisabled)
						{
							SetTheme(themeAttr.Value);
						}

						XAttribute minattr = mainElement.Attribute("WindowState");
						if (minattr != null && minattr.Value != null)
						{
						    m_launcherExpandableBar.State = minattr.Value;
							
							//WindowState windowState = WindowState.Normal;          | 
							////Enum.TryParse(minattr.Value, true, out windowState); |-- .NET4-specific code
							//WindowState = windowState;                             | 
						}

						XAttribute topmostattr = mainElement.Attribute("Topmost");
						if (topmostattr != null && topmostattr.Value != null)
						{
							switch (topmostattr.Value.ToLower())
							{
								case "true":
									m_launcherExpandableBar.IsTopmost = true;
									break;
								case "false":
									m_launcherExpandableBar.IsTopmost = false;
									break;
							}
						}

						XAttribute stateattr = mainElement.Attribute("State");
						if (stateattr != null && stateattr.Value != null)
						{
							switch (stateattr.Value.ToLower())
							{
								case "shrinked":
									if (m_launcherExpandableBar.BarState != LauncherBarState.Shrinked)
										m_launcherExpandableBar.ToggleMode();
									break;
								case "expanded":
									if (m_launcherExpandableBar.BarState != LauncherBarState.Expanded)
										m_launcherExpandableBar.ToggleMode();
									break;
							}
							//WindowState windowState = WindowState.Normal;          | 
							////Enum.TryParse(minattr.Value, true, out windowState); |-- .NET4-specific code
							//WindowState = windowState;                             | 
						}
					}

					if (FloatingWindowUtil.WindowOutOfScreen(m_window))
					{
						m_window.Left = 100;
						m_window.Top = 100;
					}
				}
			}
			catch
			{
				return;
			}
		}

		public XDocument SaveState()
		{
			try
			{
				lock (m_isLoadingOrSaving)
				{
					// <MainWindow WindowState="Maximized" Width="300" Height="400"/>
					var srcTree = new XDocument(
					  new XElement("MainWindow",
								   new XAttribute("WindowState", m_launcherExpandableBar.State),
						//TODO adapt for non-floating case
								   new XAttribute("Topmost", m_launcherExpandableBar.IsTopmost.ToString()),
								   new XAttribute("Width", m_window.Width.ToString(CultureInfo.InvariantCulture)),
								   new XAttribute("Height", m_window.Height.ToString(CultureInfo.InvariantCulture)),
								   new XAttribute("Top", m_window.Top.ToString(CultureInfo.InvariantCulture)),
								   new XAttribute("Left", m_window.Left.ToString(CultureInfo.InvariantCulture)),
								   new XAttribute("Theme", m_application.CurrentThemeName),
								   new XAttribute("State", m_launcherExpandableBar.BarState.ToString())
						)
					  );
					return srcTree;
				}
			}
			catch
			{
				return null;
			}
		}
         
        public string PersistorId
        {
            get { return PersistorIdKey; }
        }

        public const string PersistorIdKey = "MSGui.Internal.ShellWithLauncher";

		#region IThemeChangerMembers
		public void AddTheme(string name_, ImageSource icon_, Theme theme_)
		{
            if (m_application.Themes.ContainsKey(name_)) return;
			m_application.Themes[name_] = theme_;
			var rbt = new MenuItem { Header = name_, Tag = name_, Icon = new Image { Source = icon_ } };
			rbt.Click += OnThemeSelected;
			//rbt.SetValue(MenuItem.MenuItemDescriptionProperty, name_);
			if (m_optionsButton != null)
			{
				m_optionsButton.ThemesMenu.Items.Add(rbt);
			}
		}

		public void SetTheme(string name_)
		{
			m_application.SetTheme(name_, m_initialSettings);
			//foreach (var themeMenuItem in m_optionsButton.themesMenu.Items)
			//{
			//  if (themeMenuItem is MenuItem)
			//  {          
			//    MenuItem rbt = (MenuItem)themeMenuItem;
			//    rbt.Background = Brushes.Transparent;
			//    if ((string)rbt.Tag == name_)
			//    {
			//      rbt.Background = Brushes.LightYellow;
			//      break;
			//    }
			//  }
			//}
		}

        public void SetTheme(Theme theme_)
        {
            if (m_application.Themes.ContainsKey(theme_.Name))
            {
                m_application.Themes.Remove(theme_.Name);
                foreach (var item in m_optionsButton.ThemesMenu.Items)
                {
                    MenuItem rbt = item as MenuItem;
                    if (rbt != null && (string)rbt.Tag == theme_.Name)
                    {
                        m_optionsButton.ThemesMenu.Items.Remove(theme_.Name);
                        break;
                    }
                }
            }
            AddTheme(theme_.Name, theme_.Icon, theme_);
            SetTheme(theme_.Name);
        }

		public void DisableThemeChanger()
		{
			if (m_optionsButton != null)
			{
				m_optionsButton.ThemesMenu.Visibility = Visibility.Collapsed;
			}
			m_themesDisabled = true;

		}

		private void OnThemeSelected(object sender, RoutedEventArgs e)
		{
			var tool = e.Source as MenuItem;
			if (tool != null && tool.Tag != null)
			{
				m_application.SetTheme(tool.Tag as string, m_initialSettings);
				m_persistenceProfileService.Reload();
				//foreach (MenuItem item in m_optionsButton.themesMenu.Items)
				//{
				//  item.Background = Brushes.Transparent;
				//}
				//tool.Background = Brushes.LightYellow;
			}
		}
		#endregion

		public IWindowManager WindowManager
		{
			get { return m_windowManager; }
		}

        public ITileItemManager TileItemManager
        {
            get { return tileItemCollectionModel; }
        }
		public Window MainWindow
		{
			get
			{
				return m_window;
			}
		}

		public System.Windows.Controls.Primitives.StatusBar StatusBar
		{
			get
			{
				return m_statusBar;
			}
		}

	    UIElement IShell.TitleBar
	    {
	        get
	        {
                if (_titleBar == null)
                {
                    throw new NotImplementedException("TitleBar is not implemented for this shell");
                }
	            return _titleBar;
	        }
	    }

	    public Func<bool> TryCloseConcordMainWindow
		{
			set { m_tryCloseConcordMainWindow = value; }
		}

		public void Show()
		{
			if (ShellModeExtension.InvisibleMainControllerMode == ShellModeExtension.InvisibleMainControllerModeEnum.None)
			{
				m_window.Show();
				//m_windowManagerwindow.Owner = m_launcherBarWindow; 
				m_windowManagerWindow.Show(); 
			}
			m_shown = true;
		}
 
	    public void Close()
	    {
            if (DisableClose) return;
	        OnExitCalled(null, null);
	    }

        private bool PresentUnsavedQuestion(out Dictionary<string, XElement> dictionary)
        {
            switch (m_layoutLoadStrategy)
            {
                case LayoutLoadStrategy.Default:
                    //Dictionary<string, XElement> dictionary;
                    return (!m_persistenceProfileService.CheckIsUnchanged(out dictionary));
                case LayoutLoadStrategy.AlwaysAsk:
                    dictionary = null;
                    return true;
                case LayoutLoadStrategy.NeverAskButPromptForExit:
                case LayoutLoadStrategy.NeverAsk:
                    dictionary = null;
                    return false;
                case LayoutLoadStrategy.UseCallback:
                    dictionary = null;
                    return m_layoutLoadStrategyCallback();
                default:
                    dictionary = null;
                    return true;
            }
        }

        private bool ShouldSkipSaveProfile()
        {
            if (m_skipSaveProfileOnExitCondition == SkipSaveProfileOnExitCondition.Never) return false;

            return m_container.Resolve<IModuleLoadInfos>().Infos.Any(
                m_ =>
                    //condition 1: module is blocked by entitlement
                (m_.Status == ModuleStatus.BlockedByEntitlements ||
                 m_.Status == ModuleStatus.BlockedByNoEntitlementsInfo ||
                 m_.Status == ModuleStatus.DependentModuleBlocked) &&
                ((m_skipSaveProfileOnExitCondition & SkipSaveProfileOnExitCondition.ModuleBlockedByEntitlement) == SkipSaveProfileOnExitCondition.ModuleBlockedByEntitlement) ||
                    //condition 2: module type is not resolved
                (m_.Status == ModuleStatus.TypeResolutionFailed &&
                (m_skipSaveProfileOnExitCondition & SkipSaveProfileOnExitCondition.ModuleTypeResolutionFailed) == SkipSaveProfileOnExitCondition.ModuleTypeResolutionFailed) ||
                    //condition 3: exception occurred when loading module
                (m_.Status == ModuleStatus.Exceptioned &&
                (m_skipSaveProfileOnExitCondition & SkipSaveProfileOnExitCondition.ModuleLoadExceptioned) == SkipSaveProfileOnExitCondition.ModuleLoadExceptioned));
        }

		public void LoadProfile(string profile_, bool calledWhenLoading_= false)
        {
            if (calledWhenLoading_)
            {
                IItemsDictionaryService persistenceService = m_container.Resolve<IItemsDictionaryService>();
                persistenceService.RestoreItemsFromDict(new Dictionary<string, XElement>(), ItemScope.GlobalPersistor);
            }
		    if (profile_ != null)
		    {
                Dictionary<string, XElement> dictionary;
                if (m_shown && PresentUnsavedQuestion(out dictionary))
                {
		            TaskDialogSimpleResult result = TaskDialog.ShowMessage(
		                string.Format(
		                    "Do you want to load layout '{0}'? \n All unsaved changes in the current layout will be lost.",
		                    profile_),
		                "Load",
		                TaskDialogCommonButtons.YesNo,
		                VistaTaskDialogIcon.Warning);
		            if (result == TaskDialogSimpleResult.No)
		            {
		                return;
		            }
		        }
		    }
            m_persistenceProfileService.LoadProfile(profile_, calledWhenLoading_); 
		}

	    public void SaveCurrentProfile()
	    {
            m_persistenceProfileService.SaveCurrentProfile();
	    }

	    public void DeleteCurrentProfile()
	    {
	        OnDeleteCurrentCalled(null, null);
	    }

	    public void SaveProfileAs()
	    {
	        CreateNewItemClick(null, null);
	    }

	    public void SetLayoutLoadStrategy(LayoutLoadStrategy layoutLoadStrategy_)
		{
			m_layoutLoadStrategy = layoutLoadStrategy_;
		}

		public void SetLayoutLoadStrategy(LayoutLoadStrategy layoutLoadStrategy_, LayoutLoadStrategyCallback layoutLoadStrategyCallback_)
		{
			m_layoutLoadStrategy = layoutLoadStrategy_;
			m_layoutLoadStrategyCallback = layoutLoadStrategyCallback_;
		}

		public void Initialize()
		{
			m_options = m_container.Resolve<IApplicationOptions>() as ApplicationOptions;
			if (m_options == null)
			{
				throw new InvalidOperationException("IApplicationOptions must be ApplicationOptions");
			}

			m_persistenceProfileService = m_container.Resolve<PersistenceProfileService>();
			m_profileMonitor = m_persistenceProfileService;
			m_profileMonitor.CurrentProfileChanged += CurrentProfileChanged;
			//m_optionsButton.loadLayoutMenu.ItemsSource = m_profileMonitor.AvailableProfiles;

			((INotifyCollectionChanged)m_profileMonitor.AvailableProfiles).CollectionChanged += RerdawSaveMenu;
			((INotifyCollectionChanged)m_profileMonitor.AvailableProfiles).CollectionChanged += RerdawLoadMenu;
			m_launcherExpandableBar.Profile = string.Format("Layout: {0}", CurrentProfile);

            m_optionsButton.Initialize(m_container.Resolve<IChromeManager>(), m_container.Resolve<IChromeRegistry>(), m_container.Resolve<ApplicationOptions>());
            //var tdvm = WindowManager as TabbedDockViewModel;
            //if (tdvm != null)
            //{
            //    tdvm.AddTab(TabbedDockViewModel.MAIN_TAB_NAME);
            //}
		}
        public void PostChromeCreationInitialize()
        {
           if (m_initialSettings.ConcordEnabled)
           {
               BarWindow bar = m_launcherExpandableBar as BarWindow;
               if (bar != null)
               {
                   bar.SetUpConcordStrings();
               } 
            }
        }

		private void CurrentProfileChanged(object sender_, CurrentProfileChangedEventArgs e)
		{
			PropertyChangedEventHandler copy = PropertyChanged;
			if (copy != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs("CurrentProfile"));
			}
			RerdawSaveMenu(null, null);
			RerdawLoadMenu(null, null);
			m_launcherExpandableBar.Profile = string.Format("Layout: {0}", CurrentProfile);
			//Title = m_titleBase + " " + CurrentProfile;      
		}

		private void RerdawSaveMenu(object sender_, NotifyCollectionChangedEventArgs e)
		{
			if (m_optionsButton == null)
			{
				return;
			} 
			MenuItem saveLayoutMenu = m_optionsButton.SaveLayoutMenu;
			//redraw save menu
			saveLayoutMenu.Items.Clear();
			var createNewItem = new MenuItem
								  {
									  Header = "<Create New>",
									  Icon = new Image
											   {
												   Source =
													 new BitmapImage(
													 new Uri(
													   @"pack://application:,,,/MSDotNet.MSGui.Impl;component/Images/Icons/plus.png"))
											   }
								  };

			createNewItem.Click += CreateNewItemClick;
			saveLayoutMenu.Items.Add(createNewItem);
			saveLayoutMenu.Items.Add(new Separator());
			foreach (string profile in m_profileMonitor.CurrentAvailableProfiles)
			{
				var radioTool = new MenuItem
				{
					Header = string.Format("Save as '{0}'", profile),
					//IsChecked = profile == CurrentProfile,
					DataContext = profile
				};
				if (profile == CurrentProfile)
				{
					radioTool.Icon = new Image
									   {
										   Source =
											 new BitmapImage(
											 new Uri(@"pack://application:,,,/MSDotNet.MSGui.Impl;component/Images/Icons/tick.png"))
									   };
					//radioTool.Background = Brushes.LightYellow;
				}
				else
				{
					radioTool.Icon = new Image
									   {
										   Source =
											 new BitmapImage(
											 new Uri(@"pack://application:,,,/MSDotNet.MSGui.Impl;component/Images/Icons/round.png"))
									   };
				}
				string profile2 = profile;
				radioTool.Click += (x, y) =>
				{
					TaskDialogSimpleResult result = TaskDialog.ShowMessage
					  (string.Format("Overwrite layout '{0}'?", profile2), "Overwrite?",
					   TaskDialogCommonButtons.YesNo, VistaTaskDialogIcon.Warning);
					if (result == TaskDialogSimpleResult.Yes)
					{
						m_persistenceProfileService.SaveCurrentProfileAs(profile2);
					}
				};
				saveLayoutMenu.Items.Add(radioTool);
			}
		}

		private void RerdawLoadMenu(object sender_, NotifyCollectionChangedEventArgs e)
		{
			if (m_optionsButton == null)
			{
				return;
			}
			MenuItem saveLayoutMenu = m_optionsButton.LoadLayoutMenu;
			//redraw save menu
			saveLayoutMenu.Items.Clear();

			foreach (string profile in m_profileMonitor.CurrentAvailableProfiles)
			{
				var radioTool = new MenuItem
				{
					Header = profile,
					//IsChecked = profile == CurrentProfile,
					DataContext = profile
				};
				if (profile == CurrentProfile)
				{
					radioTool.Icon = new Image
					{
						Source =
						  new BitmapImage(
						  new Uri(@"pack://application:,,,/MSDotNet.MSGui.Impl;component/Images/Icons/tick.png"))
					};
					//radioTool.Background = Brushes.LightYellow;
				}
				else
				{
					radioTool.Icon = new Image
					{
						Source =
						  new BitmapImage(
						  new Uri(@"pack://application:,,,/MSDotNet.MSGui.Impl;component/Images/Icons/round.png"))
					};
				}

				radioTool.Click += delegate(object sender1_, RoutedEventArgs e_)
									 {
										 var menuItem = sender1_ as MenuItem;
										 if (menuItem != null)
										 {
											 string profileName = menuItem.Header.ToString();
											 LoadProfile(profileName);
										 }
									 };
				saveLayoutMenu.Items.Add(radioTool);
			}
		}

		public string CurrentProfile
		{
			get
			{
				if (m_profileMonitor == null || m_profileMonitor.CurrentProfile == null)
				{
					return string.Empty;
				}
				return m_profileMonitor.CurrentProfile;
			}
		}

		#region Closing
		protected void OnClosing(object sender_, CancelEventArgs e_)
        {
            if (e_.Cancel) return;
            m_application.InvokeApplicationClosing();
            Dictionary<string, XElement> savedStateDictionary;
            if (!ShouldSkipSaveProfile() &&
                (PresentUnsavedQuestion(out savedStateDictionary) || m_layoutLoadStrategy == LayoutLoadStrategy.NeverAskButPromptForExit))
            {
				if (m_application.Settings.ShowExitDialog)
				{
					IExitDialog dialog;

                    if (m_initialSettings.EnableLegacyDialog)
						dialog = new ExitDialog();
					else
						dialog = new ExitTaskDialogLauncher();

					dialog.ProfileName = m_profileMonitor.CurrentProfile ?? "Untitled profile";
					dialog.AppName = m_application.Name;
					//AutomaticSeconds = m_quitDialogSeconds,
					//AutomaticAnswer = m_quitDialogSave
					try
					{
						dialog.Owner = m_window;
					}
					catch
					{
						// Means that window was never shown, not a problem
					}
					bool? result = dialog.ShowDialog();
					if (result != true) //Cancel pressed
					{
						e_.Cancel = true;
                        m_application.InvokeApplicationClosingCancelled();
                        return;
					}
					if (dialog.DontAsk)
					{
						m_application.Settings.SaveOnExit = dialog.SaveProfile;
						m_application.Settings.ShowExitDialog = false;
					}

					if (dialog.SaveProfile)
					{
						if (m_profileMonitor.CurrentProfile == null)
						{
							string profile;
							bool save;
							ShowSaveAsDialog(out profile, out save);

							if (save == true && !String.IsNullOrEmpty(profile))
							{
                                string existingProfile = m_profileMonitor.CurrentAvailableProfiles.FindProfile(profile);
                                if (existingProfile != null)
								{
                                    profile = existingProfile;
                                    TaskDialogSimpleResult result2 = TaskDialog.ShowMessage
                                        (string.Format("Overwrite layout '{0}'?", profile), "Overwrite?", TaskDialogCommonButtons.YesNo,
											VistaTaskDialogIcon.Warning);
									if (result2 == TaskDialogSimpleResult.No)
									{
										return;
									}
								}
								m_persistenceProfileService.SaveItemsDictionaryAs(savedStateDictionary, profile);
							}
						}
						else
						{
							m_persistenceProfileService.SaveItemsDictionary(savedStateDictionary);
						}
					}
				}
				else
				{
					if (m_application.Settings.SaveOnExit)
					{
						if (string.IsNullOrEmpty(m_profileMonitor.CurrentProfile))
						{
							CreateNewItemClick(this, null);
						}
						else
						{
							m_persistenceProfileService.SaveCurrentProfile();
						}
					}
				}
			}

            var persistenceService = m_container.Resolve<IItemsDictionaryService>();
            persistenceService.SaveItemsToDict(ItemScope.GlobalPersistor);


            foreach (Func<bool> action in onClosingActions)
			{
				try
				{
					if (!action())
					{
					    e_.Cancel = true;
                        m_application.InvokeApplicationClosingCancelled();
					    return;
					}
				}
				catch (Exception exc)
				{
					m_logger.Warning("OnClosing action thrown an exception", exc);
				}
			}

            foreach (Action action in onClosedActions)
            {
                try
                {
                    action();
                }
                catch (Exception exc)
                {
                    m_logger.Warning("OnClosed action thrown an exception", exc);
                }
            }
			//m_persistenceProfileService.WaitForEmptyQueue();

			////give Concord ability to process closing
			//bool allowed = true;
			//if (tryCloseConcordMainWindow != null && !e_.Cancel)
			//{
			//  allowed = tryCloseConcordMainWindow();
			//}
			//e_.Cancel = !allowed;      
		}
		
		protected void OnClosed(object sender_, EventArgs eventArgs_)
		{
            m_container.Resolve<IHidingLockingManager>().HideUIGranularityChanged += OnHideUIGranularityChanged;
			m_application.InvokeApplicationClosed();
			System.Windows.Application.Current.Shutdown();
		}

		private void CreateNewItemClick(object sender_, RoutedEventArgs e_)
		{
			string profile;
			bool save;
			ShowSaveAsDialog(out profile, out save);

			if (save == true && !String.IsNullOrEmpty(profile))
			{
                string existingProfile = m_profileMonitor.CurrentAvailableProfiles.FindProfile(profile);
                if (existingProfile != null)
                {
                    profile = existingProfile;
                    TaskDialogSimpleResult result = TaskDialog.ShowMessage
    (string.Format("Overwrite layout '{0}'?", profile), "Overwrite?", TaskDialogCommonButtons.YesNo,
    VistaTaskDialogIcon.Warning);
                    if (result == TaskDialogSimpleResult.No)
                    {
                        return;
                    }
                }
				m_persistenceProfileService.SaveCurrentProfileAs(profile);
                RerdawLoadMenu(null, null);
                RerdawSaveMenu(null, null);
			}
		}

		private void ShowSaveAsDialog(out string profile, out bool save)
		{
            if (m_initialSettings.EnableLegacyDialog)
			{
				var profileNameDialog = new SaveAsLayoutWindow { Owner = m_window };
				bool? profileNameDialogResult = profileNameDialog.ShowDialog();
				profile = profileNameDialog.SelectedText;
				save = profileNameDialogResult.HasValue && profileNameDialogResult.Value;
			}
			else
			{
				TaskDialogOptions options = new TaskDialogOptions()
				{
					UserInputEnabled = true,
					MainInstruction = @"Please provide layout name",
					Title = @"Save layout",
					CommonButtons = TaskDialogCommonButtons.OKCancel,
					MainIcon = VistaTaskDialogIcon.Information,
				};
				TaskDialogResult dialogResult = TaskDialog.Show(options);
				profile = dialogResult.UserInput;
                save = dialogResult.Result == TaskDialogSimpleResult.Ok;
                if (save && string.IsNullOrWhiteSpace(profile))
                {
                    TaskDialog.ShowMessage("Please enter a layout name.");
                    save = false;
                } 
            }
		}

		#endregion

		#region IShell Members

        private IList<Func<bool>> onClosingActions = new List<Func<bool>>();
        private IList<Action> onClosedActions = new List<Action>();
	    private Point _previousLocation;

        Func<bool> IShell.OnClosing
		{
			set { onClosingActions.Add(value); }
		}

        Action IShell.OnClosed
        {
            set { onClosedActions.Add(value); }
        }

        private void OnWindowLocationChanged(object sender_, EventArgs e_)
        {
            var newLocation = new Point(m_window.Left, m_window.Top);
            var delta = newLocation - _previousLocation;
            var tabbedDock = m_window.Content as TabbedDock;
            if (tabbedDock != null)
            {
                tabbedDock.MoveAllWindowsByVector(delta);
            }
            _previousLocation = newLocation;
        }
 

        public bool DisableClose { get; set; }
		//public void SetQuitDialogAutomaticBehaviour(int quitDialogSeconds, bool quitDialogSave)
		//{
		//    m_quitdialogseconds = quitdialogseconds;
		//    m_quitDialogSave = quitDialogSave;
		//}

		#endregion

		public event PropertyChangedEventHandler PropertyChanged;

		public ItemsControl MainMenu
		{
			get
			{
				return m_optionsButton.optionsContextMenu;
			}
		}

		public Form ConcordMSNetLoopForm
		{
			// todo: IShell.ConcordMSNetLoopForm is not meaningful yet to ShellWithLauncher
			get;
			set;
		}
	}
}
