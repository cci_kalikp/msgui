﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Infragistics.Windows.Ribbon;
using System.Windows;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core;
using Microsoft.Practices.Unity.Utility;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell
{
    internal class RecentItemsManager
    {
        private ApplicationMenu appMenu;
        private readonly int maxItemCount;
        private readonly Dictionary<RecentItemInfo, ButtonTool> recentItems = new Dictionary<RecentItemInfo, ButtonTool>(RecentItemInfo.Comparer);
        private Dictionary<RecentItemUsage, Pair<Action<RecentItemInfo>, Func<RecentItemInfo, bool>>> handlers = 
            new Dictionary<RecentItemUsage, Pair<Action<RecentItemInfo>, Func<RecentItemInfo, bool>>>();
        internal RecentItemsManager(IPersistenceService persistenceService_, ApplicationMenu appMenu_, int maxItemCount_ = 10)
        {
            appMenu = appMenu_;
            maxItemCount = maxItemCount_;
            var recentItemInfos = new RecentItemsInfo(this);
            persistenceService_.AddGlobalPersistor(recentItemInfos.PersistorId, recentItemInfos.LoadState, recentItemInfos.SaveState);
        }

        /// <summary>
        /// Regisger global click and validation handler
        /// </summary>
        /// <param name="usage_"></param>
        /// <param name="clickHandler_"></param>
        /// <param name="validateHandler_"></param>
        internal void RegisterHandler(RecentItemUsage usage_, Action<RecentItemInfo> clickHandler_, Func<RecentItemInfo, bool> validateHandler_=null)
        {
            if (clickHandler_ == null) throw new ArgumentNullException("clickHandler_");
            handlers[usage_] = Pair.Make(clickHandler_, validateHandler_);
        }

        internal void Clear()
        {
            recentItems.Clear();
            appMenu.RecentItems.Clear();
        }
         
        internal bool AddRecentItem(RecentItemInfo info_)
        { 
            ButtonTool button = null;
            if (!BindRecentItem(ref button, info_)) return false;
           
            appMenu.RecentItems.Insert(0, button);
            if (appMenu.RecentItems.Count <= maxItemCount) return true; 

            RemoveRecentItemCore(appMenu.RecentItems[maxItemCount] as ButtonTool);

            return true;
        }

        internal bool MoveRecentItem(RecentItemInfo info_, int toIndex_)
        {
            if (appMenu.RecentItems.Count == 0) return false;
            if (appMenu.RecentItems.Count <= toIndex_ || toIndex_ < 0)
            {
                throw new ArgumentOutOfRangeException("Invalid value for toIndex_");
            }
            ButtonTool button = GetButton(info_);
            if (button == null) return false;
            int oldIndex = appMenu.RecentItems.IndexOf(button);
            if (oldIndex < 0) return false;
            appMenu.RecentItems.Move(oldIndex, toIndex_);
            return true;
        }

        internal bool RemoveRecentItem(RecentItemInfo info_)
        {
            return RemoveRecentItemCore(null, info_);
        }

        internal bool UpdateRecentItem(RecentItemInfo oldInfo_, RecentItemInfo newInfo_)
        {
            ButtonTool button = GetButton(oldInfo_);
            if (button == null) return false;
            BindRecentItem(ref button, newInfo_);
            return true; 
        }

        internal IEnumerable<RecentItemInfo> GetItems()
        {
            foreach (ButtonTool button in appMenu.RecentItems)
            {
                RecentItemInfo info = button.Tag as RecentItemInfo;
                if (info != null) yield return info;
            }
        }

        #region Helper Methods

        private ButtonTool GetButton(RecentItemInfo info_)
        {
            if (info_ == null) throw new ArgumentNullException("info_");
            ButtonTool button;
            recentItems.TryGetValue(info_, out button);
            return button;
        }

        private bool RemoveRecentItemCore(ButtonTool button_ = null, RecentItemInfo info_ = null)
        {
            if (button_ == null && info_ != null)
            {
                button_ = GetButton(info_);
            }
            if (button_ == null) return false;
            if (info_ == null)
            {
                info_ = button_.Tag as RecentItemInfo;
            }
            if (info_ != null)
            {
                recentItems.Remove(info_);
            }
            return appMenu.RecentItems.Remove(button_);
        }

        private bool BindRecentItem(ref ButtonTool button_, RecentItemInfo info_)
        {
            if (info_ == null) throw new ArgumentNullException("info_");
            if (recentItems.ContainsKey(info_)) throw new ArgumentOutOfRangeException("The same Recent Item has already been added");
             
            RecentItemInfo oldInfo = button_ == null ? null : button_.Tag as RecentItemInfo;
            if (oldInfo != null)
            {
                recentItems.Remove(oldInfo);
            }             
 
            if (info_.ValidateHandler == null || info_.ClickHandler == null)
            {
                Pair<Action<RecentItemInfo>, Func<RecentItemInfo, bool>> pair;
                if (handlers.TryGetValue(info_.Usage, out pair))
                {
                    if (info_.ClickHandler == null) info_.ClickHandler = pair.First;
                    if (info_.ValidateHandler == null) info_.ValidateHandler = pair.Second;
                }
            }

            //if validation failed, remove the button or don't add it
            if (info_.ValidateHandler != null && !info_.ValidateHandler(info_))
            {
                if (button_ != null)
                {
                    appMenu.RecentItems.Remove(button_);
                }
                return false;
            }

            if (button_ == null)
            {
                button_ = new ButtonTool();
                //always hook the click event, just in case the associated ClickHandler is attached later on
                button_.Click += new RoutedEventHandler((sender, arg) =>
                {
                    ButtonTool button = sender as ButtonTool;
                    RecentItemInfo info = button.Tag as RecentItemInfo;
                    if (info.ClickHandler != null) info.ClickHandler(info);
                });
            }
            button_.Caption = info_.Caption; 
            button_.Tag = info_;
            recentItems.Add(info_, button_);
            return true;
        }
        #endregion
    }
    
    internal class RecentItemInfo
    {
        public RecentItemInfo(string caption_, string parameter_, RecentItemUsage usage_= RecentItemUsage.LayoutProfile,
            Action<RecentItemInfo> clickHandler_=null, Func<RecentItemInfo, bool> validateHandler_=null)
        {
            Caption = caption_;
            Parameter = parameter_;
            Usage = usage_;
        }

        public void SaveToElement(XElement element_)
        {
            if (element_ == null) throw new ArgumentNullException("element_");

            element_.SetAttributeValue("Caption", this.Caption);
            if (!string.IsNullOrWhiteSpace(Parameter))
            { 
                element_.SetAttributeValue("Parameter", Parameter); 
            }
            element_.SetAttributeValue("Usage", this.Usage.ToString()); 
        }

        public static RecentItemInfo LoadFromElement(XElement element_)
        {
            if (element_ == null) return null;

            var captionAttribute = element_.Attribute("Caption");
            if (captionAttribute == null) return null; //a recent item with no caption is meaningless
            var parameterAttribute = element_.Attribute("Parameter"); 
            var usageAttribute = element_.Attribute("Usage");
            RecentItemUsage usage = RecentItemUsage.LayoutProfile;
            if (usageAttribute != null)
            {
                Enum.TryParse<RecentItemUsage>(usageAttribute.Value, out usage);
            }
            return new RecentItemInfo(captionAttribute.Value, parameterAttribute == null ? null : parameterAttribute.Value, usage);
        } 

        public void CopyFrom(RecentItemInfo info_)
        {
            if (info_ == null) throw new ArgumentNullException("info_");
            this.Caption = info_.Caption;
            this.Parameter = info_.Parameter;
            this.Usage = info_.Usage;
        }
        public string Caption
        {
            get;
            private set;
        }
        public string Parameter
        {
            get;
            private set;
        }
        public RecentItemUsage Usage
        {
            get;
            private set;
        }

        public Action<RecentItemInfo> ClickHandler { get; set; }
        public Func<RecentItemInfo, bool> ValidateHandler { get; set; }

        public static readonly IEqualityComparer<RecentItemInfo> Comparer = new RecentItemInfoComparer();
 
        internal class RecentItemInfoComparer : IEqualityComparer<RecentItemInfo>
        {
            public bool Equals(RecentItemInfo x, RecentItemInfo y)
            {
                if (x == y) return true;
                if (x != null && y == null) return false;
                if (x == null && y != null) return false;
                if (x.Usage != y.Usage) return false;
                if (x.Usage == RecentItemUsage.LayoutProfile)
                {
                    return (x.Parameter ?? string.Empty).ToLower().Equals((y.Parameter ?? string.Empty).ToLower());
                }
                return (x.Caption ?? string.Empty).Equals(y.Caption ?? string.Empty) &&
                    (x.Parameter ?? string.Empty).Equals(y.Parameter ?? string.Empty) &&
                    x.Usage.Equals(y.Usage);
            }

            public int GetHashCode(RecentItemInfo obj)
            {
                return obj.Caption.GetHashCode() ^ (string.IsNullOrEmpty(obj.Parameter) ? 0 : obj.Parameter.GetHashCode()) ^ obj.Usage.GetHashCode();
            }
        }
    } 

    internal enum RecentItemUsage
    {
        LayoutProfile = 0
    }
}
