﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Xml;
using System.Xml.Linq;
using Infragistics;
using Infragistics.Windows.Controls;
using Infragistics.Windows.DockManager;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Workspaces;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.Converters;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
    /// <summary>
    /// Please reflect Infragistics.Windows.DockManager.LayoutManager.SaveLayout and LoadLayout when upgrade IG
    /// </summary>
    internal static class XamDockManagerLayoutHelper
    {
        private static readonly string XamVersion; 
        private static readonly string DockManagerAssemblyName;

        static XamDockManagerLayoutHelper()
        {
            var assy = typeof (XamDockManager).Assembly;
            XamVersion = assy.GetName().Version.ToString(); 
            DockManagerAssemblyName = string.Format("InfragisticsWPF4.DockManager.v{0}.{1}", assy.GetName().Version.Major, assy.GetName().Version.Minor);
        }
         
       
        /// <summary>
        /// Writes a valid XamDockManager layout that contains only the panes in the sublayout.
        /// </summary>
        /// <param name="rootPanes">List of root level floating SplitPanes.</param>
        /// <returns>The layout xml.</returns>
        public static XElement WriteSubLayout(IEnumerable<IWindowViewContainerHostRoot> roots, IEnumerable<IWindowViewContainer> containers=null, bool skipLocationInfo = false)
        { 
            List<SplitPane> rootPanes = new List<SplitPane>();
            foreach (var root in roots)
            {
                CustomPaneToolWindow window = root as CustomPaneToolWindow;
                if (window != null)
                {
                    rootPanes.Add(window.Pane);
                }
                else
                {
                    var pane = XamDockHelper.GetRootOfDockedPane(
                        (containers == null ? null : containers.FirstOrDefault())
                        ?? root.Windows.First());
                    if (pane != null)
                    {
                        rootPanes.Add(pane); 
                    }
                }
            }
            var serInfoType = Type.GetType("Infragistics.Windows.DockManager.LayoutManager+SerializationInfo, " + DockManagerAssemblyName);
            var layoutMgrType = Type.GetType("Infragistics.Windows.DockManager.LayoutManager, " + DockManagerAssemblyName);
            var dockableStateType = Type.GetType("Infragistics.Windows.DockManager.DockableState, " + DockManagerAssemblyName); 
            var dockManagerUtilitiesType = Type.GetType("Infragistics.Windows.DockManager.DockManagerUtilities, " + DockManagerAssemblyName);
            var iContentPaneContainerType = Type.GetType("Infragistics.Windows.DockManager.IContentPaneContainer, " + DockManagerAssemblyName); 

            if (serInfoType == null)
                return null;

            var ctor = serInfoType.GetConstructor(BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] {},
                                                  null);
            var serInfo = ctor.Invoke(new object[] {});
            if (serInfo == null)
                return null;

            var panes = ReflHelper.PropertyGet(serInfo, "NamedPanes") as Dictionary<string, ContentPane>;
            if (panes == null)
                return null;

            using (var stm = new MemoryStream())
            using (var xw = new XmlTextWriter(stm, Encoding.UTF8))
            {
                xw.WriteStartElement(LayoutConverter.DockManagerRootName);
                xw.WriteAttributeString("Version", XamVersion);

                #region Copied from IG
                // store content panes
                xw.WriteStartElement("contentPanes");
                foreach (var splitPane in rootPanes)
                {
                    foreach (var pane in XamDockHelper.GetPanes(splitPane))
                    {
                        panes.Add(pane.Name, pane);

                        if (pane.SaveInLayout == false)
                            continue;
                        xw.WriteStartElement("contentPane"); // <contentPane>

                        //if (string.IsNullOrEmpty(pane.Name) || info.NamedPanes.ContainsKey(pane.Name))
                        //    throw new InvalidOperationException(XamDockManager.GetString("LE_SaveLayoutPaneNameMissing"));

                        //// store the panes by name so we can validate the uniqueness
                        //info.NamedPanes.Add(pane.Name, pane); 
                        xw.WriteAttributeString("name", pane.Name);
                        if (!skipLocationInfo || pane.PaneLocation == PaneLocation.Unpinned)
                        {
                            XmlHelper.WriteAttribute(xw, "location", pane.PaneLocation);

                        }
                        // if the pane is not docked/floating but it was at one point, store where that was
                        if (
                            ReflHelper.PropertyGet(ReflHelper.PropertyGet(pane, "PlacementInfo"), "DockableContainer") !=
                            ReflHelper.PropertyGet(ReflHelper.PropertyGet(pane, "PlacementInfo"), "CurrentContainer") )
                        {
                            var dockableContainer =
                                ReflHelper.PropertyGet(ReflHelper.PropertyGet(pane, "PlacementInfo"),
                                                       "DockableContainer");
                            if (dockableContainer != null)
                            {

                                var paneLocation = ReflHelper.PropertyGet(dockableContainer, "PaneLocation",
                                                                          iContentPaneContainerType);
                                var lastDockable = (bool)ReflHelper.StaticMethodInvoke(dockManagerUtilitiesType, "IsDocked",
                                  new Type[]{ typeof(PaneLocation)}, new[] { paneLocation})
        ? ReflHelper.StaticFieldGet(dockableStateType, "Docked")
        : ReflHelper.StaticFieldGet(dockableStateType, "Floating");
                                XmlHelper.WriteAttribute(xw, "lastDockableState", (Enum)lastDockable);
                            }

                        }

                        var lastFloatingSize = (Size)ReflHelper.PropertyGet(pane, "LastFloatingSize");
                        if (lastFloatingSize.IsEmpty && skipLocationInfo)
                        {
                            lastFloatingSize = new Size(pane.ActualWidth, pane.ActualHeight);
                        }
                        if (!lastFloatingSize.IsEmpty && lastFloatingSize.Width > 0 && lastFloatingSize.Height > 0)
                        {
                            XmlHelper.WriteAttributeWithConverter(xw, "lastFloatingSize", lastFloatingSize);
                        }

                        var lastFloatingWindowRect = (Rect)ReflHelper.PropertyGet(pane, "LastFloatingWindowRect"); 
                        if (!lastFloatingWindowRect.IsEmpty)
                        {
                            XmlHelper.WriteAttributeWithConverter(xw, "lastFloatingWindowRect", lastFloatingWindowRect);
                        }

                        XmlHelper.WriteAttributeWithConverter(xw, "lastFloatingLocation", (Point?)ReflHelper.PropertyGet(pane, "LastFloatingLocation"));

                        var lastActivatedTime = (DateTime)ReflHelper.PropertyGet(pane, "LastActivatedTime");
                        if (lastActivatedTime != DateTime.MinValue)
                            XmlHelper.WriteAttribute(xw, "lastActivatedTime", lastActivatedTime);

                        if (!string.IsNullOrEmpty(pane.SerializationId))
                            xw.WriteAttributeString("serializationId", pane.SerializationId);

                        // AS 5/13/08
                        //if (pane.Visibility == Visibility.Collapsed)
                        //	WriteAttribute(writer, IsClosedAttrib, true);
                        if (pane.Visibility != Visibility.Visible)
                            XmlHelper.WriteAttribute(xw, "visibility", pane.Visibility);

                        #region Unpinned

                        if (pane.IsPinned == false)
                        {
                            var container = ReflHelper.PropertyGet(ReflHelper.PropertyGet(pane, "PlacementInfo"),
                                                                   "CurrentContainer");
                            //Debug.Assert(null != container && container is UnpinnedTabAreaInfo);
                            UnpinnedTabArea tabArea = container != null
                                                          ? ReflHelper.PropertyGet(container, "ContainerElement") as
                                                            UnpinnedTabArea
                                                          : null;

                            if (null != tabArea)
                                XmlHelper.WriteAttribute(xw, "unpinnedOrder", tabArea.Items.IndexOf(pane));

                            double flyoutExtent = (double)
                                                  ReflHelper.StaticMethodInvoke(typeof(UnpinnedTabFlyout),
                                                                                "GetFlyoutExtent",
                                                                                new object[] { pane });
                            if (!double.IsNaN(flyoutExtent))
                                XmlHelper.WriteAttribute(xw, "flyoutExtent", flyoutExtent);
                        }

                        #endregion //Unpinned

                        xw.WriteEndElement(); // </contentPane>

                        //ReflHelper.StaticMethodInvoke(layoutMgrType, "WriteContentPane", new[] { xw, pane, serInfo });
                    }
                }
                xw.WriteEndElement(); // </contentPanes>
                #endregion

                // store the layout with references to the content panes
                xw.WriteStartElement("panes");
                foreach (var splitPane in rootPanes)
                {
                    if (skipLocationInfo)
                    {
                        WriteSplitPaneWithoutLocation(xw, splitPane, layoutMgrType, serInfo);
                    }
                    else
                    {
                        ReflHelper.StaticMethodInvoke(layoutMgrType, "WriteSplitPane", new[] { xw, splitPane, true, serInfo });
                    }
                }
                xw.WriteEndElement(); // </panes>
                xw.WriteEndElement(); // </xamDockManager>

                xw.Flush();
                stm.Seek(0, SeekOrigin.Begin);
                 
                using (var xr = new XmlTextReader(stm))
                {
                    xr.MoveToContent();
                    var result = XNode.ReadFrom(xr);
                    XElement subLayout = result as XElement;
                    if (subLayout != null && containers != null)
                    {
                        RemoveIrrelevantPanes(subLayout, containers);
                    }
                    return subLayout;
                } 
            } 
        } 

        private static void WriteSplitPaneWithoutLocation(XmlWriter writer, SplitPane pane, Type layoutMgrType, object serInfo)
        {
            writer.WriteStartElement("splitPane"); 
            writer.WriteAttributeString("splitterOrientation", pane.SplitterOrientation.ToString());
            PaneLocation paneLocation = XamDockManager.GetPaneLocation(pane); 
            switch (paneLocation)
            { 
                case PaneLocation.Floating:
                case PaneLocation.FloatingOnly:
                    {
                        XmlHelper.WriteAttributeWithConverter(writer, "floatingLocation", XamDockManager.GetFloatingLocation(pane));
                        var size = XamDockManager.GetFloatingSize(pane);
                        if (!size.IsEmpty)
                        {
                            XmlHelper.WriteAttributeWithConverter(writer, "floatingSize", size); 
                        }
                        ToolWindow toolWindow = ToolWindow.GetToolWindow(pane);
                        if (toolWindow.WindowState != WindowState.Normal)
                        {
                            XmlHelper.WriteAttributeWithConverter(writer, "windowState", toolWindow.WindowState);
                            var restoreBounds = toolWindow.GetRestoreBounds();
                            if (!restoreBounds.IsEmpty)
                            {
                                XmlHelper.WriteAttributeWithConverter(writer, "restoreBounds", restoreBounds); 
                            }
                            if ((toolWindow.WindowState == WindowState.Minimized))
                            {
                                XmlHelper.WriteAttribute(writer, "restoreToMaximized", true);
                            }
                        }
                        break;
                    }
                case PaneLocation.DockedTop:
                case PaneLocation.DockedLeft:
                case PaneLocation.DockedBottom:
                case PaneLocation.DockedRight:
                    XmlHelper.WriteAttributeWithConverter(writer, "floatingSize", new Size(pane.ActualWidth, pane.ActualHeight));
                    break;
            }

            foreach (FrameworkElement element in pane.Panes)
            {
                if ((element is ContentPane) || (element is ContentPanePlaceholder))
                {
                    ReflHelper.StaticMethodInvoke(layoutMgrType, "WriteContentPane", new[] { writer, element, serInfo }); 
                }
                else if (element is SplitPane)
                {
                    ReflHelper.StaticMethodInvoke(layoutMgrType, "WriteSplitPane", new[] { writer, element, false, serInfo });  
                }
                else if (element is TabGroupPane)
                {
                    ReflHelper.StaticMethodInvoke(layoutMgrType, "WriteTabGroup", new[] { writer, element, serInfo });   
                }
            }
            writer.WriteEndElement();
        }

        private static void RemoveIrrelevantPanes(XElement subLayout_, IEnumerable<IWindowViewContainer> containers_)
        {
            var contentPanesNode = subLayout_.Element("contentPanes");
            if (contentPanesNode == null) return;
            var panesIds = new List<string>(containers_.Select(c_ => ContentPaneIdConverter.ConvertToPaneId(c_.ID)));
            List<XElement> contentPaneNodesToRemove = new List<XElement>();
            foreach (var contentPaneNode in contentPanesNode.Elements("contentPane"))
            {
                var nameAttr = contentPaneNode.Attribute("name");
                if (nameAttr == null) continue;
                if (panesIds.Contains(nameAttr.Value)) continue;
                contentPaneNodesToRemove.Add(contentPaneNode);
            }
            if (contentPaneNodesToRemove.Count == 0) return;
            foreach (var contentPaneNodeToRemove in contentPaneNodesToRemove)
            {
                contentPaneNodeToRemove.Remove();
            }
            var panesNode = subLayout_.Element("panes");
            if (panesNode == null) return;
            foreach (var splitPaneNode in new List<XElement>(panesNode.Elements("splitPane")))
            {
                RemoveIrrelevantContentPaneNodes(splitPaneNode, panesIds);
            }
        }

        private static void RemoveIrrelevantContentPaneNodes(XElement parentNode_, List<string> paneIds_)
        {
            List<XElement> elements = new List<XElement>(parentNode_.Elements());
            foreach (var childPaneNode in elements)
            {
                if (childPaneNode.Name == "tabGroup" || childPaneNode.Name == "splitPane")
                {
                    RemoveIrrelevantContentPaneNodes(childPaneNode, paneIds_);
                    int count = childPaneNode.Elements().Count();
                    if (count == 0)
                    {
                        childPaneNode.Remove();
                    }
                    else if (count == 1)
                    {
                        childPaneNode.Parent.Add(childPaneNode.Elements().First());
                        childPaneNode.Remove();
                    }
                }
                else if (childPaneNode.Name == "contentPane")
                {
                    var nameAttr = childPaneNode.Attribute("name");
                    if (nameAttr == null) continue;
                    if (!paneIds_.Contains(nameAttr.Value))
                    {
                        childPaneNode.Remove();
                    }
                }
            }
        }
         
        const Visibility VisibilityNotSet = unchecked((Visibility)(-1));
        public static IEnumerable<IWindowViewContainerHostRoot> ReadSubLayout(string sublayoutname, XElement subLayout, RehydratePaneDelegate callback, 
            bool reuseExisting = false,  SubLayoutLocation locationOverride_=SubLayoutLocation.SavedLocation)
        {
            var rootPanes = new List<SplitPane>();
            var roots = new List<IWindowViewContainerHostRoot>();
            var parseInfoType = Type.GetType("Infragistics.Windows.DockManager.LayoutManager+ParseInfo, " + DockManagerAssemblyName);
            var layoutMgrType = Type.GetType("Infragistics.Windows.DockManager.LayoutManager, " + DockManagerAssemblyName);
            var contentPaneInfoType = Type.GetType("Infragistics.Windows.DockManager.LayoutManager+ContentPaneInfo, " + DockManagerAssemblyName);
            var dockManagerKnownBoxesType = Type.GetType("Infragistics.Windows.DockManager.DockManagerKnownBoxes, " + DockManagerAssemblyName);
            var dockableStateType = Type.GetType("Infragistics.Windows.DockManager.DockableState, " + DockManagerAssemblyName);
            var iContentPaneContainerType = Type.GetType("Infragistics.Windows.DockManager.IContentPaneContainer, " + DockManagerAssemblyName);   

            if (parseInfoType == null)
                return roots;

            var ctor = parseInfoType.GetConstructor(BindingFlags.NonPublic | BindingFlags.Instance, null,
                                                    new[] { typeof(XamDockManager) }, null);
            var parseInfo = ctor.Invoke(new object[] { XamDockManagerExtensions.Manager });
            if (parseInfo == null)
                return roots;

            var panes = ReflHelper.PropertyGet(parseInfo, "LoadedPanes");
            if (panes == null)
                return roots;

            var paneInfos = new List<object>();

            var xd = new XmlDocument() { InnerXml = subLayout.ToString() };

            var contentPaneNodes = xd.SelectNodes(string.Format("/{0}/{1}/contentPanes/*", LayoutConverter.DockLayoutRootName, LayoutConverter.DockManagerRootName));
            if (contentPaneNodes == null)
                return roots;

            var paneMapping = new Dictionary<string, string>();
            foreach (XmlNode xnode in contentPaneNodes)
            {
                string name = xnode.Attributes["name"].Value;
                string serializationId = xnode.Attributes["serializationId"] == null
                                             ? null
                                             : xnode.Attributes["serializationId"].Value;

                var newName = XamDockManagerExtensions.Manager.GetPanes(PaneNavigationOrder.ActivationOrder).Any(p => p.Name == name) && !reuseExisting
                    ?  ContentPaneIdConverter.GeneratePaneId() : name;
                if (name != newName)
                    paneMapping.Add(name, newName);

                callback(name, newName);

                var pane =
                    XamDockManagerExtensions.Manager.GetPanes(PaneNavigationOrder.ActivationOrder).FirstOrDefault(p => p.Name == newName);
                (pane.Parent as SplitPane).Panes.Remove(pane);

                var paneInfo = Activator.CreateInstance(contentPaneInfoType);

                #region copied from IG

                ReflHelper.FieldSet(paneInfo, "Pane", pane);
                ReflHelper.FieldSet(paneInfo, "SerializationId", serializationId);

                Visibility visibility = XmlHelper.ReadEnumAttribute(xnode, "visibility", VisibilityNotSet);

                if (visibility == VisibilityNotSet)
                    visibility = XmlHelper.ReadAttribute(xnode, "isClosed", false) ? Visibility.Collapsed : Visibility.Visible;

                ReflHelper.FieldSet(paneInfo, "Visibility", visibility);

                if (locationOverride_ == SubLayoutLocation.SavedLocation)
                {
                    ReflHelper.FieldSet(paneInfo, "Location", XmlHelper.ReadEnumAttribute(xnode, "location", PaneLocation.Unknown));
    
                }
                else if (locationOverride_ == SubLayoutLocation.DockedInNewTab)
                {
                    ReflHelper.FieldSet(paneInfo, "Location", PaneLocation.DockedTop);
                }
                else
                {
                    ReflHelper.FieldSet(paneInfo, "Location", PaneLocation.Floating);
                    ReflHelper.FieldSet(paneInfo, "LastFloatingSize",
                            XmlHelper.ReadAttributeWithConverter(xnode, "lastFloatingSize", Size.Empty));
                    ReflHelper.FieldSet(paneInfo, "LastFloatingWindowRect",
                                        XmlHelper.ReadAttributeWithConverter(xnode, "lastFloatingWindowRect", Rect.Empty));
                    ReflHelper.FieldSet(paneInfo, "LastFloatingPoint",
                                        XmlHelper.ReadAttributeWithConverter<Point?>(xnode, "lastFloatingLocation", null));
                }

                ReflHelper.FieldSet(paneInfo, "UnpinnedOrder", XmlHelper.ReadAttribute(xnode, "unpinnedOrder", 0));
                ReflHelper.FieldSet(paneInfo, "FlyoutExtent", XmlHelper.ReadAttribute(xnode, "flyoutExtent", double.NaN));
                ReflHelper.FieldSet(paneInfo, "LastActivatedTime",
                                    XmlHelper.ReadAttribute(xnode, "lastActivatedTime", DateTime.MinValue));

                ReflHelper.MethodInvoke(panes, "Add", new[] { pane.Name, paneInfo });
                paneInfos.Add(paneInfo);

                #endregion
            }

            var splitPaneNodes = xd.SelectNodes(string.Format("/{0}/{1}/panes/*", LayoutConverter.DockLayoutRootName, LayoutConverter.DockManagerRootName));
            if (splitPaneNodes == null)
                return roots;

            foreach (XmlNode xNode in splitPaneNodes)
            {
                var paneNode = RemapPanes(xNode, paneMapping);

                var location = locationOverride_ == SubLayoutLocation.SavedLocation ? XmlHelper.ReadEnumAttribute<PaneLocation>(paneNode, "location", PaneLocation.Unknown) : 
                    (locationOverride_ == SubLayoutLocation.DockedInNewTab ? PaneLocation.DockedTop : PaneLocation.Floating);
                 
                switch (location)
                {
                    // AS 5/17/08
                    // Added explicit value checks in case we ever add other locations.
                    //
                    case PaneLocation.DockedBottom:
                    case PaneLocation.DockedLeft:
                    case PaneLocation.DockedRight:
                    case PaneLocation.DockedTop:
                    case PaneLocation.Floating:
                    case PaneLocation.FloatingOnly:
                        break;

                    default:
                    case PaneLocation.Unknown:
                    case PaneLocation.Unpinned:
                    case PaneLocation.Document:
                        Debug.Fail("Unexpected root pane location!");
                        continue;
                }

                // store the current location so we know where the pane would be. this is 
                // necessary since the pane location won't be set until the pane is loaded
                ReflHelper.PropertySet(parseInfo, "CurrentLocation", location);

                var split =
                    ReflHelper.StaticMethodInvoke(layoutMgrType, "LoadSplitPane", new[] { paneNode, parseInfo }) as
                    SplitPane;

                if (split != null)
                {
                    rootPanes.Add(split);
                    InitialPaneLocation initialLocation = GetInitialLocation(location);

                    XamDockManager.SetInitialLocation(split, initialLocation);

                    #region Root Split Props

                    switch (location)
                    {
                        case PaneLocation.DockedBottom:
                        case PaneLocation.DockedTop:
                        case PaneLocation.DockedLeft:
                        case PaneLocation.DockedRight:
                            double extent = XmlHelper.ReadAttribute(paneNode, "extent", double.NaN);

                            if (double.IsNaN(extent) == false)
                            {
                                if (location == PaneLocation.DockedRight || location == PaneLocation.DockedLeft)
                                    split.Width = extent;
                                else
                                    split.Height = extent;
                            }
                            else
                            {
                                // AS 5/17/08 Reuse Group/Split
                                split.ClearValue(FrameworkElement.WidthProperty);

                                // AS 3/26/10 TFS28510
                                split.ClearValue(FrameworkElement.HeightProperty);
                            }
                            break;
                        case PaneLocation.Floating:
                        case PaneLocation.FloatingOnly:
                            Size floatSize = XmlHelper.ReadAttributeWithConverter(paneNode, "floatingSize", Size.Empty);
                            Point floatLocation = XmlHelper.ReadAttributeWithConverter(paneNode, "floatingLocation", new Point());

                            XamDockManager.SetFloatingLocation(split, floatLocation);
                            if (false == floatSize.IsEmpty)
                                XamDockManager.SetFloatingSize(split, floatSize);
                            else
                            {
                                // AS 5/17/08 Reuse Group/Split
                                split.ClearValue(XamDockManager.FloatingSizeProperty);
                            }
                            break;
                    }

                    #endregion //Root Split Props

                    XamDockManagerExtensions.Manager.Panes.Add(split);

                    #region Root Split Props

                    switch (location)
                    {
                        case PaneLocation.Floating:
                        case PaneLocation.FloatingOnly:
                            // AS 1/26/11 NA 2011 Vol 1 - Min/Max/Taskbar
                            // We need the toolwindow so now that its in the panes collection we should have 
                            // that and we can set the window state & restore bounds on it.
                            //
                            WindowState floatState = XmlHelper.ReadAttributeWithConverter(paneNode, "windowState",
                                                                                WindowState.Normal);
                            Rect floatRestoreBounds = XmlHelper.ReadAttributeWithConverter(paneNode, "restoreBounds", Rect.Empty);
                            bool floatRestoreToMax = XmlHelper.ReadAttribute(paneNode, "restoreToMaximized", false);

                            ToolWindow tw = ToolWindow.GetToolWindow(split);
                            Debug.Assert(null != tw, "Unable to get to the toolwindow");

                            if (null != tw)
                            {
                                tw.WindowState = floatState;

                                if (floatState != WindowState.Normal)
                                    tw.SetRestoreBounds(floatRestoreBounds);

                                if (floatState == WindowState.Minimized)
                                    ReflHelper.PropertySet(tw, "RestoreToMaximized", floatRestoreToMax);
                                CustomPaneToolWindow ctw = tw as CustomPaneToolWindow;
                                if (ctw != null)
                                {
                                    ctw.SubLayoutTitle = sublayoutname;
                                }
                            }



                            break;
                    }

                    #endregion //Root Split Props

                    // i don't really want to initialize the location here since 
                    // i would prefer to have it centralized when the panes collection
                    // is processed. that being said we need to know the state when we're
                    // processing unpinned panes and since the state should be known,
                    // we will initialize it here
                    split.SetValue(
                        ReflHelper.StaticFieldGet(typeof(XamDockManager), "PaneLocationPropertyKey") as
                        DependencyPropertyKey,
                        ReflHelper.StaticMethodInvoke(dockManagerKnownBoxesType, "FromValue",
                                                      new Type[] { typeof(PaneLocation) }, new object[] { location }));

                    #region ContentPane fixups

                    //List<UnpinnedTabAreaInfo.SortOrderInfo>[] unpinnedOrder = new List<UnpinnedTabAreaInfo.SortOrderInfo>[4];

                    //for (int i = 0; i < unpinnedOrder.Length; i++)
                    //    unpinnedOrder[i] = new List<UnpinnedTabAreaInfo.SortOrderInfo>();

                    // lastly fix up so that the cp's are where the last dockable location indicated
                    foreach (var cpi in paneInfos)
                    {
                        ContentPane cp = cpi != null ? ReflHelper.FieldGet(cpi, "Pane") as ContentPane : null;

                        if (null != cp)
                        {
                            #region Replace appropriate placeholder

                            object originalLocation = ReflHelper.FieldGet(cpi, "Location");

                            PaneLocation location2 = originalLocation == null
                                                         ? PaneLocation.Unknown
                                                         : (PaneLocation) originalLocation;
                            if (location2 != PaneLocation.Unpinned && locationOverride_ != SubLayoutLocation.SavedLocation)
                            {
                                location2 = locationOverride_ == SubLayoutLocation.DockedInNewTab
                                                               ? PaneLocation.DockedTop
                                                               : PaneLocation.Floating;
                            }
                            switch (location2)
                            {
                                case PaneLocation.Unpinned:
                                    {
                                        //if (cp.PlacementInfo.DockedEdgePlaceholder == null)
                                        //    throw new InvalidOperationException(XamDockManager.GetString("LE_LoadLayoutPanePositionNotFound", cp.Name, cpi.Location));

                                        // put the pane in the docked container
                                        ReflHelper.MethodInvoke(
                                            ReflHelper.PropertyGet(ReflHelper.PropertyGet(cp, "PlacementInfo"),
                                                                   "DockedContainer"), "InsertContentPane",
                                            new object[] { null, cp });
                                         
                                        //unpinnedOrder[(int)side].Add(new UnpinnedTabAreaInfo.SortOrderInfo(cp, cpi.UnpinnedOrder));

                                        // initialize its unpinned state
                                        cp.IsPinned = false;
                                        double flyoutExtent = (double)ReflHelper.FieldGet(cpi, "FlyoutExtent");
                                        if (false == double.IsNaN(flyoutExtent))
                                            ReflHelper.StaticMethodInvoke(typeof(UnpinnedTabFlyout), "SetFlyoutExtent",
                                                                          new object[]
                                                                              {
                                                                                  cp,
                                                                                  flyoutExtent
                                                                              });
                                        else
                                            cp.ClearValue(
                                                ReflHelper.StaticFieldGet(typeof(UnpinnedTabFlyout),
                                                                          "FlyoutExtentProperty") as DependencyProperty);

                                        // process the move right now to avoid a flicker
                                        ReflHelper.MethodInvoke(XamDockManagerExtensions.Manager, "ProcessPinnedState",
                                                                new object[] { cp, cp.PaneLocation, false });
                                        break;
                                    }
                                case PaneLocation.DockedLeft:
                                case PaneLocation.DockedBottom:
                                case PaneLocation.DockedRight:
                                case PaneLocation.DockedTop:
                                    {
                                        //if (cp.PlacementInfo.DockedEdgePlaceholder == null)
                                        //    throw new InvalidOperationException(XamDockManager.GetString("LE_LoadLayoutPanePositionNotFound", cp.Name, cpi.Location));

                                        var target =
                                            ReflHelper.PropertyGet(ReflHelper.PropertyGet(cp, "PlacementInfo"),
                                                                   "DockedContainer");
                                        var method = iContentPaneContainerType.GetMethod("InsertContentPane");
                                        method.Invoke(target, new object[] { null, cp });
                                        break;
                                    }
                                case PaneLocation.Document:
                                    {
                                        Debug.Assert(cp.PaneLocation == PaneLocation.Document);

                                        //if (cp.PaneLocation != PaneLocation.Document)
                                        //    throw new InvalidOperationException(XamDockManager.GetString("LE_LoadLayoutPanePositionNotFound", cp.Name, cpi.Location));

                                        break;
                                    }
                                case PaneLocation.Floating:
                                    {
                                        //if (cp.PlacementInfo.FloatingDockablePlaceholder == null)
                                        //    throw new InvalidOperationException(XamDockManager.GetString("LE_LoadLayoutPanePositionNotFound", cp.Name, cpi.Location));
                                        
                                        var target =
                                            ReflHelper.PropertyGet(
                                                ReflHelper.PropertyGet(ReflHelper.PropertyGet(cp, "PlacementInfo"),
                                                                       "FloatingDockablePlaceholder"), "Container");
                                        var method = iContentPaneContainerType.GetMethod("InsertContentPane");
                                        method.Invoke(target, new object[] { null, cp });

                                        //ReflHelper.MethodInvoke(target, "InsertContentPane", new Type[] { typeof(int?), typeof(ContentPane) }, new object[] { null, cp });
                                        //cp.PlacementInfo.FloatingDockablePlaceholder.Container.InsertContentPane(null, cp);
                                        break;
                                    }
                                case PaneLocation.FloatingOnly:
                                    {
                                        //if (cp.PlacementInfo.FloatingOnlyPlaceholder == null)
                                        //    throw new InvalidOperationException(XamDockManager.GetString("LE_LoadLayoutPanePositionNotFound", cp.Name, cpi.Location));

                                        var target = ReflHelper.PropertyGet(
                                            ReflHelper.PropertyGet(ReflHelper.PropertyGet(cp, "PlacementInfo"),
                                                                   "FloatingOnlyPlaceholder"), "Container");
                                        var method = iContentPaneContainerType.GetMethod("InsertContentPane");
                                        method.Invoke(target, new object[] { null, cp });
                                        //cp.PlacementInfo.FloatingOnlyPlaceholder.Container.InsertContentPane(null, cp);
                                        break;
                                    }
                            }

                            #endregion //Replace appropriate placeholder

                            // if the pane hasn't been activated then update the activation
                            // order based on the saved activation time
                            if ((DateTime)ReflHelper.PropertyGet(cp, "LastActivatedTime") <
                                (DateTime)ReflHelper.FieldGet(cpi, "LastActivatedTime"))
                                ReflHelper.PropertySet(cp, "LastActivatedTime",
                                                       ReflHelper.FieldGet(cpi, "LastActivatedTime"));

                            ReflHelper.PropertySet(cp, "LastFloatingSize", ReflHelper.FieldGet(cpi, "LastFloatingSize"));
                            ReflHelper.PropertySet(cp, "LastFloatingWindowRect",
                                                   ReflHelper.FieldGet(cpi, "LastFloatingWindowRect"));
                            ReflHelper.PropertySet(cp, "LastFloatingLocation",
                                                   ReflHelper.FieldGet(cpi, "LastFloatingPoint"));

                            // restore last dockable state. this is needed when the pane is displayed
                            // as floating only or document and the end user reselects dockable
                            ReflHelper.MethodInvoke(ReflHelper.PropertyGet(cp, "PlacementInfo"),
                                                    "VerifyLastDockableContainer", new Type[] { dockableStateType },
                                                    new object[] { ReflHelper.FieldGet(cpi, "LastDockableState") });

                            // restore closed state
                            // AS 5/13/08
                            //cp.SetValue(FrameworkElement.VisibilityProperty, cpi.IsClosed ? KnownBoxes.VisibilityCollapsedBox : KnownBoxes.VisibilityVisibleBox);
                            cp.SetValue(FrameworkElement.VisibilityProperty,
                                        KnownBoxes.FromValue((Visibility)ReflHelper.FieldGet(cpi, "Visibility")));
                        }
                    }

                    // make the pane manager rebuild the active pane list using the
                    // updated sort order datetime.
                    //DockManager.ActivePaneManager.RefreshActivePaneSortOrder();

                    #endregion //ContentPane fixups
                }
            }

            foreach (var splitPane in rootPanes)
            {
                CustomPaneToolWindow tw = ToolWindow.GetToolWindow(splitPane) as CustomPaneToolWindow;
                if (tw != null)
                {
                    roots.Add(tw);
                }
                else
                {
                    var pane = XamDockHelper.FindOnlyPane(splitPane) as IWindowViewContainerHost;
                     if (pane != null)
                     {
                         
                         var tab = DockHelper.GetRootOfDockedPane(pane.View);
                         if (tab != null)
                         {
                             roots.Add(tab); 
                         }
                     }
                }
            }
            return roots;
            //}
            //}
        }

        private static XmlElement RemapPanes(XmlNode xNode, Dictionary<string, string> paneMapping)
        {
            // remap panes - we need this so we can load the same sublayout more than once
            var xnodeText = xNode.OuterXml;
            foreach (var mapping in paneMapping)
            {
                xnodeText = xnodeText.Replace(mapping.Key, mapping.Value);
            }

            var xdoc = new XmlDocument() {InnerXml = xnodeText};
            var paneNode = xdoc.DocumentElement;

            // assign random IDs to SplitPanes - we need this, otherwise the newly renamed panes would end up where the existing ones are
            Func<XmlElement, object> splitPaneHack = null;
            splitPaneHack = (node) =>
                {
                    var element = node as XmlElement;
                    if (element == null)
                        return null;

                    if ((element.Name == "splitPane" || element.Name == "tabGroup") &&
                        element.HasAttribute("name"))
                    {
                        element.SetAttribute("name", "Z" + Guid.NewGuid().ToString("N"));
                    }

                    foreach (var child in element.ChildNodes.OfType<XmlElement>())
                    {
                        splitPaneHack(child as XmlElement);
                    }

                    return null;
                };

            splitPaneHack(paneNode);

            return paneNode;
        }
         
        private static InitialPaneLocation GetInitialLocation(PaneLocation location)
        {
            switch (location)
            {
                case PaneLocation.DockedBottom:
                    return InitialPaneLocation.DockedBottom;
                case PaneLocation.DockedLeft:
                    return InitialPaneLocation.DockedLeft;
                case PaneLocation.DockedRight:
                    return InitialPaneLocation.DockedRight;
                case PaneLocation.DockedTop:
                    return InitialPaneLocation.DockedTop;
                case PaneLocation.FloatingOnly:
                    return InitialPaneLocation.FloatingOnly;
                case PaneLocation.Floating:
                    return InitialPaneLocation.DockableFloating;
                default:
                    Debug.Fail("Invalid location!");
                    return InitialPaneLocation.FloatingOnly;
            }
        } 
        
    }
     
}
