﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Controls/XamRibbonHelper.cs#15 $
// $Change: 901409 $
// $DateTime: 2014/10/17 05:24:50 $
// $Author: caijin $

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using Infragistics.Windows.Ribbon;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.Converters;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
    internal static class XamRibbonHelper
    {
        public static RibbonGroup GetOrCreateRibbonGroup(XamRibbon ribbon, string tab, string group)
        {
            var namedTab = FindNamedTab(tab, ribbon) ?? CreateAndAddTab(tab, ribbon);

            foreach (var ribbonGroup in namedTab.RibbonGroups)
            {
                if (ribbonGroup.Caption.Equals(group)) return ribbonGroup;
            }
            return CreateAndAddRibbonGroup(group, namedTab);
        }

        public static RibbonGroup CreateRibbonGroup(XamRibbon ribbon, string tab, string group)
        {
            var namedTab = FindNamedTab(tab, ribbon) ?? CreateAndAddTab(tab, ribbon);
            return CreateAndAddRibbonGroup(group, namedTab);
        }

        public static RibbonTabItem CreateOrFindRibbonTab(XamRibbon ribbon, string tab)
        {
            var namedTab = FindNamedTab(tab, ribbon) ?? CreateAndAddTab(tab, ribbon);
            return namedTab;
        }

        private static RibbonTabItem CreateAndAddTab(string tab, XamRibbon ribbon)
        {
            var tabItem = new RibbonTabItem {Header = tab};
            ribbon.Tabs.Add(tabItem);
            return tabItem;
        }

        public static RibbonTabItem FindNamedTab(string name, XamRibbon ribbon)
        {
            if (ribbon != null)
            {
                return (from tab in ribbon.Tabs
                        let header = tab.Header
                        where header != null && header.ToString() == name
                        select tab).FirstOrDefault();
            }
            return null;
        }

        public static RibbonTabItem CreateMenuTab(string name, XamRibbon ribbon)
        {
            var ribbonTab = new RibbonTabItem {Header = name};
            ribbon.Tabs.Add(ribbonTab);
            return ribbonTab;
        }

        public static RibbonGroup CreateAndAddRibbonGroup(string name, RibbonTabItem ribbonTab)
        {
            var ribbonGroup = new RibbonGroup {Caption = Display.FormatCaption(name)};
            var binding = new Binding("Items.Count")
                {
                    RelativeSource = RelativeSource.Self,
                    Converter = new VisibilityConverter()
                };

            ribbonGroup.SetBinding(UIElement.VisibilityProperty, binding);
            ribbonTab.RibbonGroups.Add(ribbonGroup);
            return ribbonGroup;
        }

        public static ItemsControl CreateMenuBar(string name)
        {
            var menuTool = new MenuTool
                {
                    Caption = name,
                    ButtonType = MenuToolButtonType.DropDown
                };

            menuTool.SetValue(RibbonGroup.MaximumSizeProperty, RibbonToolSizingMode.ImageAndTextNormal);
            if (name == "Tools")
            {
                menuTool.SetValue(RibbonGroup.MaximumSizeProperty, RibbonToolSizingMode.ImageAndTextLarge);
                menuTool.LargeImage =
                    menuTool.SmallImage =
                    new BitmapImage(
                        new Uri(@"pack://application:,,,/MSDotNet.MSGui.Impl;component/Images/icons/Tools.png",
                                UriKind.RelativeOrAbsolute));
            }
            return menuTool;
        }

        internal static IList<string> disabledMenuToolList = new List<string>();

        internal static void CleanRibbon(XamRibbon xamRibbon,
                                         IControlParadigmImplementator controlParadigmImplementator)
        {
            foreach (RibbonTabItem tabItem in xamRibbon.Tabs)
            {
                foreach (RibbonGroup ribbonGroup in tabItem.RibbonGroups)
                {
                    var menuToolToBeRemoved =
                        ribbonGroup.Items.OfType<MenuTool>().Where(item => (item).Items.Count == 0).ToArray();
                    var source = ((System.Collections.ObjectModel.ObservableCollection<object>) ribbonGroup.ItemsSource);
                    if (source == null) continue;

                    foreach (var menuTool in menuToolToBeRemoved)
                    {
                        source.Remove(menuTool);
                    }

                    var buttonBaseToBeRemoved =
                        ribbonGroup.Items.OfType<ButtonBase>().Where(item => (item).IsEnabled == false).ToArray();
                    foreach (var buttonBase in buttonBaseToBeRemoved)
                    {
                        source.Remove(buttonBase);
                    }


                    var disabledList = ribbonGroup.Items.OfType<MenuTool>().Where(
                        item => item.Items.OfType<ButtonBase>().Count(item2 => item2.IsEnabled) == 0).ToArray();
                    foreach (var menuTool in disabledList)
                    {
                        disabledMenuToolList.Add(
                            string.Format(
                                "Warning: on Tab {2} RibbonGroup {0} the SubMenu {1} contains no enabled items",
                                ribbonGroup.Caption, menuTool.Caption, tabItem.Header));
                    }
                }
            }
            foreach (var tabItem in xamRibbon.Tabs)
            {
                var ribbonGroupToRemove =
                    tabItem.RibbonGroups.Where(ribbonGroup => ribbonGroup.Items.Count == 0).ToArray();
                foreach (var ribbonGroup in ribbonGroupToRemove)
                {
                    tabItem.RibbonGroups.Remove(ribbonGroup);
                }
            }
            var ribbonTabItemsToRemove = xamRibbon.Tabs.Where(tabItem => tabItem.RibbonGroups.Count == 0).ToArray();
            foreach (var ribbonTabItem in ribbonTabItemsToRemove)
            {
                xamRibbon.Tabs.Remove(ribbonTabItem);
            }
        }
         
    }
}