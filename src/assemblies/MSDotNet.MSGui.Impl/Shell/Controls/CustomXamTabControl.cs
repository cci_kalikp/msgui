﻿////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Controls/CustomXamTabControl.cs#18 $
// $Change: 896821 $
// $DateTime: 2014/09/15 02:20:20 $
// $Author: caijin $

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Shapes;
using Infragistics.DragDrop;
using Infragistics.Windows.Controls;
using Infragistics.Windows.DockManager;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using System.Linq;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions.MVVMDock;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
  /// <summary>
  /// A class deriving from XamTabControl that is used to switch between main window's tabs
  /// We created it to 
  /// 1. be able to apply a global style and do not affect other XamTabControls.
  /// 2. to set context menus for new tabs even when items are set from Xaml through ItemsSource
  /// </summary>
  public class CustomXamTabControl : XamTabControl
  {

      internal static bool DragAndDropEnabled = false;

      public Visibility NewTabButtonVisibility
      {
          get { return (Visibility)GetValue(NewTabButtonVisibilityProperty); }
          set { SetValue(NewTabButtonVisibilityProperty, value); }
      }

      // Using a DependencyProperty as the backing store for NewTabButtonVisibility.  This enables animation, styling, binding, etc...
      public static readonly DependencyProperty NewTabButtonVisibilityProperty =
          DependencyProperty.Register("NewTabButtonVisibility", typeof(Visibility), typeof(CustomXamTabControl), new UIPropertyMetadata(Visibility.Visible));

      private static DataTemplate m_invisibleCursorDataTemplate;

        public CustomXamTabControl()
        {
            NewTabButtonVisibility = ShellModeExtension.GetVisibilityForElement(HideLockUIGranularity.HideNewTabButton);
        }

		internal static Visibility HeaderVisibility { get; set; }

      protected override void OnItemsChanged(NotifyCollectionChangedEventArgs e)
      {

          if (e.Action == NotifyCollectionChangedAction.Reset)
          {
              if (Items != null)
              {
                  foreach (var item in Items)
                  {
                      if (!ShellModeExtension.HideUIGranularity.HasFlag(HideLockUIGranularity.DisableTabwellContextMenu))
                      {
                          SetContextMenu(item);
                      }
                      if (DragAndDropEnabled)
                      {
                          SetDragAndDrop(item);
                      }
                  }
              }
          }

          if (e.NewItems != null)
          {
              foreach (var item in e.NewItems)
              {
                  if (!ShellModeExtension.HideUIGranularity.HasFlag(HideLockUIGranularity.DisableTabwellContextMenu))
                  {
                      SetContextMenu(item);

                  }
                  if (DragAndDropEnabled)
                  {
                      SetDragAndDrop(item);
                  }
              }
          }
          int count = 0;
          foreach (object item in Items)
          {
              if (!ShellModeExtension.HideUIGranularity.HasFlag(HideLockUIGranularity.DisableTabwellContextMenu))
              {
                  UpdateContextMenu(item, count != 0, count != Items.Count - 1);
                  count++;
                  SetContextMenu(item);
              }
              ;
              if (DragAndDropEnabled)
                  SetDragAndDrop(item);
          }
      }

      private void SetDragAndDrop(object item)
      {
          var container = ItemContainerGenerator.ContainerFromItem(item);
          if (container == null)
              return;
          var dragSource = new DragSource {IsDraggable = true};
          dragSource.Drop += DragSourceOnDrop;
          dragSource.DragEnd += DragSourceOnDragEnd;
          var invisibleCursorDataTemplate = GetInvisibleCursorDataTemplate();
          dragSource.DropNotAllowedCursorTemplate = invisibleCursorDataTemplate;
          dragSource.MoveCursorTemplate = invisibleCursorDataTemplate;
          container.SetValue(DragDropManager.DragSourceProperty, dragSource);
          var dropTarget = new DropTarget {IsDropTarget = true};
          container.SetValue(DragDropManager.DropTargetProperty, dropTarget);
      }

      /// <summary>
      /// How To Test: call Framework.EnableTabDragAndDrop(), drag a tab with multiple panes to float, and then drag the float window to another float window and see if the panes retains
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void DragSourceOnDragEnd(object sender, DragDropEventArgs e)
      {
          var drag = e.DragSource as TabItemEx;
          if (drag == null) return;
          if (e.DropTarget != null) return;

          var sourceViewModel = drag.DataContext as ShellTabViewModel;
          var rootPanes = GetRootPanes(sourceViewModel);
          if (rootPanes == null) return;

          var dockViewModel = DataContext as TabbedDockViewModel;
          var dockManager = rootPanes.First().GetParentObject() as XamDockManager;

          var newRootPane = PrepareNewFloatingRootPane(rootPanes, dockManager);

          dockManager.Panes.Add(newRootPane);

          var items = ((ItemsSource as ListCollectionView).SourceCollection) as ObservableCollection<ShellTabViewModel>;
          items.Remove(sourceViewModel);

          if (items.Count == 0) dockViewModel.AddTab("Main");
      }

      /// <summary>
      /// Prepares a set of docked split panes to be moved to a single floating split pane, preserving
      /// the layout.
      /// </summary>
      /// <param name="rootPanes"></param>
      /// <param name="dockManager"></param>
      /// <returns></returns>
      private static SplitPane PrepareNewFloatingRootPane(IEnumerable<SplitPane> rootPanes, XamDockManager dockManager)
      {
          var newRootPane = new SplitPane();
          var currentRootPane = newRootPane;
          var remainingPanes = rootPanes.Count();
          var currentRootPaneSize = dockManager.RenderSize;

          foreach (var splitPane in rootPanes)
          {
              remainingPanes--;
              var paneLocation = XamDockManager.GetPaneLocation(splitPane);
              dockManager.Panes.Remove(splitPane);
              currentRootPane.Panes.Add(splitPane);
              //SplitPane.SetRelativeSize(splitPane, new Size(
              //    (double.IsNaN(splitPane.Width) || splitPane.Width <= 0) ? 1 : splitPane.Width,
              //   (double.IsNaN(splitPane.Height) || splitPane.Height <= 0) ? 1 : splitPane.Height
              //    ));
              SplitPane.SetRelativeSize(splitPane, new Size(splitPane.ActualWidth, splitPane.ActualHeight));

              if (remainingPanes > 0)
              {
                  var newSubPane = new SplitPane();
                  switch (paneLocation)
                  {
                      case PaneLocation.DockedLeft:
                          currentRootPane.SplitterOrientation = Orientation.Vertical;
                          currentRootPane.Panes.Add(newSubPane);
                          currentRootPaneSize.Width -= splitPane.ActualWidth;
                          break;
                      case PaneLocation.DockedRight:
                          currentRootPane.SplitterOrientation = Orientation.Vertical;
                          currentRootPane.Panes.Insert(0, newSubPane);
                          currentRootPaneSize.Width -= splitPane.ActualWidth;
                          break;
                      case PaneLocation.DockedTop:
                          currentRootPane.SplitterOrientation = Orientation.Horizontal;
                          currentRootPane.Panes.Add(newSubPane);
                          currentRootPaneSize.Height -= splitPane.ActualHeight;
                          break;
                      case PaneLocation.DockedBottom:
                          currentRootPane.SplitterOrientation = Orientation.Horizontal;
                          currentRootPane.Panes.Insert(0, newSubPane);
                          currentRootPaneSize.Height -= splitPane.ActualHeight;
                          break;
                      default:
                          Console.WriteLine();
                          break;
                  }
                  SplitPane.SetRelativeSize(newSubPane, currentRootPaneSize);
                  currentRootPane = newSubPane;
              }
              splitPane.ClearValue(WidthProperty);
              splitPane.ClearValue(HeightProperty);
          }

          XamDockManager.SetInitialLocation(newRootPane, InitialPaneLocation.DockableFloating);
          var mousePositionWinForms = System.Windows.Forms.Control.MousePosition;
          XamDockManager.SetFloatingLocation(newRootPane, new Point(mousePositionWinForms.X, mousePositionWinForms.Y));
          XamDockManager.SetFloatingSize(newRootPane, dockManager.RenderSize);
          return newRootPane;
      }

      private static IEnumerable<SplitPane> GetRootPanes(ShellTabViewModel sourceViewModel)
      {
          var rootPanesReference = sourceViewModel.Windows
              .Select(XamDockHelper.GetRootOfDockedPane)
              .Where(p => p != null)
              .Distinct().ToArray();
          if (!rootPanesReference.Any()) return null;
          // this is important to get them in correct order
          var dockManagerPanel = VisualTreeHelper.GetParent(rootPanesReference.First()) as DockManagerPanel;
          return (ReflHelper.PropertyGet(dockManagerPanel, "Panes") as Collection<SplitPane>)
              .Where(p => rootPanesReference.Contains(p)).ToArray();
      }
      
      private void DragSourceOnDrop(object sender, DropEventArgs e)
      {
          if (e.DropTarget == e.DragSource) return;
          var drag = e.DragSource as TabItemEx;
          var drop = e.DropTarget as TabItemEx;

          if (drop != null && drag != null)
          {
              var items = ((ItemsSource as ListCollectionView).SourceCollection) as ObservableCollection<ShellTabViewModel>;
              var sourceViewModel = drag.DataContext as ShellTabViewModel;
              var targetViewModel = drop.DataContext as ShellTabViewModel;
              int insertIndex = items.IndexOf(targetViewModel);
              items.Remove(sourceViewModel);
              items.Insert(insertIndex, sourceViewModel);
              SelectedItem = drag;
          }
      }

      public void RefreshContextMenus()
		{
			OnItemsChanged(new NotifyCollectionChangedEventArgs( NotifyCollectionChangedAction.Reset ));
		}

    private void UpdateContextMenu(object item_, bool enableLeft, bool enableRight)
    {
      DependencyObject container = ItemContainerGenerator.ContainerFromItem(item_);
      if (container != null && container is FrameworkElement)
      {
        ((TabItemMenu) ((FrameworkElement) container).ContextMenu).EnableLeft = enableLeft;
        ((TabItemMenu)((FrameworkElement)container).ContextMenu).EnableRight = enableRight;
				for (int i = 0; i < 7;i++ )
				{
					if (i == 4)
						continue;
					((UIElement) ((FrameworkElement) container).ContextMenu.Items[i]).Visibility = HeaderVisibility;
				}
      }  
    }

    private void SetContextMenu(object item_)
    {
      DependencyObject container = ItemContainerGenerator.ContainerFromItem(item_);
      if (container != null && container is FrameworkElement)
      {
        if (((FrameworkElement)container).ContextMenu == null)
        ((FrameworkElement) container).ContextMenu = new TabItemMenu();
      }      
    }

    private static DataTemplate GetInvisibleCursorDataTemplate()
    {
        if (m_invisibleCursorDataTemplate == null)
        {
            m_invisibleCursorDataTemplate = new DataTemplate();
            var invisibleEllipse = new FrameworkElementFactory(typeof(Ellipse));
            m_invisibleCursorDataTemplate.VisualTree = invisibleEllipse;
        }
        return m_invisibleCursorDataTemplate;
    }

  }
}
