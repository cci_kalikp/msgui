////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Controls/TabbedDock.xaml.cs#72 $
// $Change: 898794 $
// $DateTime: 2014/09/30 05:29:21 $
// $Author: caijin $

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using Infragistics.Windows.Controls;
using Infragistics.Windows.DockManager;
using Infragistics.Windows.DockManager.Dragging;
using Infragistics.Windows.DockManager.Events;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Win32;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions.MVVMDock;
using DragEventArgs = System.Windows.DragEventArgs;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
    /// <summary>
    /// Interaction logic for TabbedDock.xaml
    /// </summary>
    internal partial class TabbedDock
    {
        internal static bool UseImmediatePaneDragging = false;
        internal static bool UseSystemWindowDrag = false;
        internal static bool WindowDeactivationNotificationEnabled = false;
        internal static bool FireDragDropEvents = false;
        internal static bool VisualFeedbackOnPaneOverEdge;
        internal static bool FirePaneDroppedAtScreenEdge;
        internal static bool FireCrossProcessDragDropEvents = false;

        private event EventHandler<TabbedDockPaneDragEndedEventArgs> PaneDragEnded;

        /// <summary>
        /// When FireCrossProcessDragDropEvents is true and the user is dragging a pane over another process's window,
        /// this field will indicate the process id of that process.
        /// </summary>
        private int? currentProcessId;

        internal static bool DisableHeaderTogglePersistor;

        public TabbedDock()
        {
            DataContextChanged += OnDataContextChanged;
            InitializeComponent();
             

            dockManager.PaneDragEnded += dockManager_PaneDragEnded;

            dockManager.ActivePaneChanged += (s, e) =>
                {
                    if (s != dockManager)
                        return;
                    if (e.NewValue != null)
                    {
                        if (e.NewValue is CustomContentPane)
                        {
                            ViewContainerHeaderControl.StickyNow = true;
                            (e.NewValue as CustomContentPane).UpdateContainer(true);
                        }
                    }
                };

            dockManager.PaneDragStarting += (s, e) =>
                {
                    if (s != dockManager)
                        return;

                    if (((DockViewModel) DataContext).HidingLockingManager.Granularity.HasFlag(HideLockUIGranularity.LockViewDragDrop))
                    {
                        e.Cancel = true;
                        return;
                    }
                    //Question:??
                    //same in FloatingOnlyDock.xaml.cs
                    //this hack here is to disable dragging of panes when any of them is set to topmost (take, enabling "EnableTopmostFloatingPanes" or 
                    //set "Topmost" to true and "UseDockManager" to false, in the initial parameter)
                    //however, if EnableTopmostFloatingPanes is used, all the panes in the floating window pinned would be set to top most, thus doesn't allow
                    //even switching panes on this floating window
                    var rc = ReflHelper.FieldGet(e, "_rootContainer") as FrameworkElement;
                    if (rc is CustomPaneToolWindow)
                    {
                        if (e.Panes.OfType<CustomContentPane>().Any(cp => cp.Topmost))
                        {
                            e.Cancel = true;
                        }
                    }
                };

            dockManager.PaneDragOver += (s, e) =>
                {
                    if (((DockViewModel)DataContext).HidingLockingManager.Granularity.HasFlag(
                            HideLockUIGranularity.WorkspaceShellStyleDragDropLock))
                    {
                        if (e.DragAction is NewTabGroupAction ||
                            e.DragAction is AddToGroupAction ||
                            e.DragAction is FloatPaneAction)
                        {
                            e.IsValidDragAction = false;
                            return;
                        }
                    }

                    if (s != dockManager)
                        return;
                    // are we dragging topmost stuff?
                    if (
                        (from cp in e.Panes.OfType<CustomContentPane>()
                         where cp.Topmost
                         select e.DragAction as MoveWindowAction).Any(mwa => mwa == null))
                    {
                        e.IsValidDragAction = false;
                    }

                    if (e.DragAction is NewSplitPaneAction)
                    {
                        var E = e.DragAction as NewSplitPaneAction;
                        if (XamDockHelper.IsTopmost(E.Pane))
                        {
                            e.IsValidDragAction = false;
                        }
                    }
                    else if (e.DragAction is NewTabGroupAction)
                    {
                        var E = e.DragAction as NewTabGroupAction;
                        if (XamDockHelper.IsTopmost(E.Pane))
                        {
                            e.IsValidDragAction = false;
                        }
                    }
                    else if (e.DragAction is AddToGroupAction)
                    {
                        var E = e.DragAction as AddToGroupAction;
                        if (XamDockHelper.IsTopmost(E.Group))
                        {
                            e.IsValidDragAction = false;
                        }
                    }
                };

            dockManager.FloatingWindowDragMode = FloatingWindowDragMode.Immediate;

            dockManager.PaneDragOver += (s, e) =>
                {
                    if (s != dockManager ||
                        e.Panes.Any(p => !(p is CustomContentPane)) ||
                        e.Panes.Any(p => (p as CustomContentPane).AllowPartiallyOffscreen))
                        return;

                    bool ok = false;

                    var mwa = e.DragAction as MoveWindowAction;
                    if (mwa != null)
                    {
                        Rect windowRect = new Rect(mwa.NewLocation.X, mwa.NewLocation.Y, mwa.Window.ActualWidth,
                                                   mwa.Window.ActualHeight);
                        ok = IsRectOnscreen(windowRect);
                        if (!ok)
                        {
                            var previousX = new Rect(mwa.OldLocation.X, mwa.NewLocation.Y, mwa.Window.ActualWidth,
                                                     mwa.Window.ActualHeight);
                            var previousY = new Rect(mwa.NewLocation.X, mwa.OldLocation.Y, mwa.Window.ActualWidth,
                                                     mwa.Window.ActualHeight);

                            if (IsRectOnscreen(previousX))
                            {
                                var step = (int) Math.Sign(mwa.NewLocation.X - mwa.OldLocation.X);

                                for (int i = 0; IsRectOnscreen(previousX); i += step)
                                    previousX = new Rect(mwa.OldLocation.X + i, mwa.NewLocation.Y,
                                                         mwa.Window.ActualWidth, mwa.Window.ActualHeight);

                                mwa.NewLocation = new Point(previousX.X - step, previousX.Y);
                                ok = true;
                            }
                            else if (IsRectOnscreen(previousY))
                            {
                                var step = (int) Math.Sign(mwa.NewLocation.Y - mwa.OldLocation.Y);

                                for (int i = 0; IsRectOnscreen(previousY); i += step)
                                    previousY = new Rect(mwa.NewLocation.X, mwa.OldLocation.Y + i,
                                                         mwa.Window.ActualWidth, mwa.Window.ActualHeight);

                                mwa.NewLocation = new Point(previousY.X, previousY.Y - step);
                                ok = true;
                            }
                        }
                    }

                    if (!ok)
                    {
                        e.IsValidDragAction = false;
                    }
                };

            if (VisualFeedbackOnPaneOverEdge)
            {
                dockManager.PaneDragOver += (s, e) =>
                    {
                        Point absoluteScreenPos = PointToScreen(Mouse.GetPosition(this));
                        var pane = e.Panes.First() as CustomContentPane;
                        if (absoluteScreenPos.X >
                            ScreenSizeHelper.TotalScreenArea.Width - 50 ||
                            absoluteScreenPos.X < 50 ||
                            absoluteScreenPos.Y >
                            ScreenSizeHelper.TotalScreenArea.Height - 50 ||
                            absoluteScreenPos.Y < 50)
                            pane.BorderBrush = Brushes.Red;
                        else
                            pane.BorderBrush = Brushes.Black;
                    };
            }

            if (FirePaneDroppedAtScreenEdge)
            {
                // Here, we will fire both PaneDroppedAtScreenEdge (in case there are subscribers among library clients)
                // and WindowGroupDroppedAtScreenEdge. The former one should be deprecated.
                PaneDragEnded += (s, e) =>
                    {
                        bool paneEventRaised = false;
                        foreach (var pane in e.InfragisticsArgs.Panes)
                        {
                            var absoluteScreenPos = GetAbsoluteScreenPosition(pane);
                            Console.WriteLine("{0}, {1}", absoluteScreenPos.X, absoluteScreenPos.Y);

                            ScreenEdge? edge;
                            if (TryGetNearestEdge(absoluteScreenPos, out edge))
                            {
                                var dockViewModel = DataContext as TabbedDockViewModel;
                                dockViewModel.RaisePaneDroppedAtScreenEdge(pane, edge.Value);

                                // We have a child window of a window group at the edge. Raise the WindowGroupDroppedAtScreenEdge, only once
                                if (!paneEventRaised)
                                {
                                    var toolWindow = ToolWindow.GetToolWindow(pane) as IFloatingWindow;
                                    dockViewModel.RaiseWindowGroupDroppedAtScreenEdge(toolWindow, edge.Value,  e);
                                    paneEventRaised = true;
                                }
                            }
                        }
                    };
            }

            PaneDragEnded +=
                (sender, args) =>
                    {
                        try
                        {
                            var inputElement = InputHitTest(Mouse.GetPosition(this));
                            if (!IsAddNewButton(inputElement)) return;

                            var anyContentPane = args.InfragisticsArgs.Panes.First();
                            var windowViewModel = anyContentPane.DataContext as WindowViewModel;
                            if (windowViewModel == null) return;
                            var containingToolWindow = anyContentPane.TryFindParent<CustomPaneToolWindow>();
                            if (containingToolWindow == null) return;
                            var viewModel = DataContext as TabbedDockViewModel;
                            var rootSplitPane = containingToolWindow.Pane;

                            dockManager.Panes.Remove(rootSplitPane);

                            var currentTab = viewModel.CurrentTab as ShellTabViewModel;
                            var newTab = viewModel.AddTab(windowViewModel.Title);

                            var newRootSplitPane = new SplitPane();
                            XamDockManager.SetInitialLocation(newRootSplitPane, InitialPaneLocation.DockedTop);
                            newRootSplitPane.Panes.Add(rootSplitPane);
                            dockManager.Panes.Add(newRootSplitPane);

                            foreach (var window in args.InfragisticsArgs.Panes.Select(contentPane =>
                                                                     contentPane.DataContext as WindowViewModel))
                            {
                                window.Host = newTab;
                                var windowViewContainer = window.ViewContainer as IWindowViewContainer;
                                // dockManager.Panes.Remove(rootSplitPane) will add these windows to the currentTab
                                if (currentTab.Windows.Contains(windowViewContainer))
                                    currentTab.RemoveWindow(windowViewContainer);
                                newTab.AddWindow(windowViewContainer);
                            }

                            newTab.Activate();
                        }
                        catch (Exception)
                        {
                        }
                    };

            if (FireDragDropEvents)
            {
                dockManager.AllowDrop = true;
                dockManager.Drop += (o_, eventArgs_) => InvokeDragOrDropEventOnViewModel(eventArgs_, DropEvent);
                dockManager.PreviewDrop +=
                    (o_, eventArgs_) => InvokeDragOrDropEventOnViewModel(eventArgs_, PreviewDropEvent);
                dockManager.DragEnter +=
                    (o_, eventArgs_) => InvokeDragOrDropEventOnViewModel(eventArgs_, DragEnterEvent);
                dockManager.DragOver += (o_, eventArgs_) => InvokeDragOrDropEventOnViewModel(eventArgs_, DragOverEvent);
                dockManager.DragLeave +=
                    (o_, eventArgs_) => InvokeDragOrDropEventOnViewModel(eventArgs_, DragLeaveEvent);
                dockManager.PreviewDragEnter +=
                    (o_, eventArgs_) => InvokeDragOrDropEventOnViewModel(eventArgs_, PreviewDragEnterEvent);
                dockManager.PreviewDragOver +=
                    (o_, eventArgs_) => InvokeDragOrDropEventOnViewModel(eventArgs_, PreviewDragOverEvent);
                dockManager.PreviewDragLeave +=
                    (o_, eventArgs_) => InvokeDragOrDropEventOnViewModel(eventArgs_, PreviewDragLeaveEvent);
            }

            if (FireCrossProcessDragDropEvents)
            {
                dockManager.PaneDragEnded += (sender_, args_) =>
                    {
                        if (args_.Panes.Count == 0) return;
                        var toolWindow = ToolWindow.GetToolWindow(args_.Panes.First()) as IFloatingWindow;
                        if (toolWindow == null) return;
                        if (currentProcessId != null)
                        {
                            ((DockViewModel)DataContext).OnCrossProcessDragDropAction(toolWindow, currentProcessId.Value,
                                                                                                CrossProcessDragDropAction.Drop);
                        }
                    };
                dockManager.PaneDragOver += (sender_, args_) =>
                    {
                        if (args_.Panes.Count == 0) return;
                        var toolWindow = ToolWindow.GetToolWindow(args_.Panes.First()) as CustomPaneToolWindow;
                        if (toolWindow == null) return;
                        var processId = GetProcessIdFromToolWindow(toolWindow);
                        if (processId != currentProcessId && currentProcessId != null)
                        {
                            ((DockViewModel)DataContext).OnCrossProcessDragDropAction(toolWindow,
                                                                                       currentProcessId.Value,
                                                                                       CrossProcessDragDropAction
                                                                                           .Leave);
                            currentProcessId = null;
                        }
                        if (currentProcessId == null && processId != Process.GetCurrentProcess().Id)
                        {
                            ((DockViewModel)DataContext).OnCrossProcessDragDropAction(toolWindow as IFloatingWindow, processId,
                                                                                           CrossProcessDragDropAction
                                                                                               .Enter);
                            currentProcessId = processId;
                        }
                    };
            }

            //if (TabbedDock.UseImmediatePaneDragging)
            //{
            //    dockManager.FloatingWindowDragMode = Infragistics.Windows.DockManager.FloatingWindowDragMode.Immediate;
            //}
            //if (TabbedDock.UseSystemWindowDrag)
            //{
            //    dockManager.FloatingWindowDragMode = Infragistics.Windows.DockManager.FloatingWindowDragMode.UseSystemWindowDrag;
            //}

            if (ShellModeExtension.ContentHostModeEnabled)
            {
                var documentHost = new DocumentContentHost();
                dockManager.Content = documentHost;
            }
        }

        private static int GetProcessIdFromToolWindow(ToolWindow toolWindow)
        {
            var point = new System.Drawing.Point(Convert.ToInt32(toolWindow.Left) - 5,
                                                 Convert.ToInt32(toolWindow.Top) - 5);
            var hwnd = NativeUiMethodsHelper.WindowFromPointApi(point);
            int processId;
            Win32.GetWindowThreadProcessId(hwnd, out processId);
            return processId;
        }

        private void dockManager_PaneDragEnded(object sender, PaneDragEndedEventArgs e)
        {
            var handler = PaneDragEnded;
            if (handler != null)
            {
                var eventArgs = new TabbedDockPaneDragEndedEventArgs(e);
                handler(this, eventArgs);

                if (eventArgs.ShouldCloseWindow)
                {
                    foreach (var pane in e.Panes)
                    {
                        pane.Visibility=Visibility.Hidden;
                    }
                }
            }
        }

        private Point GetAbsoluteScreenPosition(ContentPane pane)
        {
            var mouse = Mouse.GetPosition(this);
            Point absoluteScreenPos;
            if (mouse.X < 0 && mouse.Y < 0)
            {
                var window = Window.GetWindow(pane);
                absoluteScreenPos = new Point
                    {
                        X = window.Left,
                        Y = window.Top,
                    };
            }
            else
                absoluteScreenPos = PointToScreen(mouse);
            return absoluteScreenPos;
        }

        private bool TryGetNearestEdge(Point point, out ScreenEdge? edge)
        {
            edge = null;

            if (point.X > ScreenSizeHelper.TotalScreenArea.Width - 50)
                edge = ScreenEdge.Right;
            if (point.X < 50)
                edge = ScreenEdge.Left;
            if (point.Y > ScreenSizeHelper.TotalScreenArea.Height - 50)
                edge = ScreenEdge.Bottom;
            if (point.Y < 50)
                edge = ScreenEdge.Top;

            return edge != null;
        }

        private void InvokeDragOrDropEventOnViewModel(DragEventArgs eventArgs, RoutedEvent type)
        {
            (DataContext as TabbedDockViewModel).InvokeDragOrDropOnWindow(
                new GenericDragOrDropOnWindowEventArgs
                    {
                        DragEventArgs = eventArgs,
                        Window = null,
                        InternalEventType = type
                    });
        }

        private static bool IsAddNewButton(IInputElement inputElement)
        {
            var dependencyObject = inputElement as DependencyObject;
            if (dependencyObject == null) return false;
            if (dependencyObject.TryFindParent<CustomXamTabControl>() == null) return false;
            var button = dependencyObject.TryFindParent<Button>();
            if (button == null) return false;
            return button.Command == Command.AddTabCommand;
        }

        private bool IsRectOnscreen(Rect r)
        {
            double coverage = 0;
            foreach (var screen in System.Windows.Forms.Screen.AllScreens)
            {
                var temp = new Rect(screen.WorkingArea.Left, screen.WorkingArea.Top, screen.WorkingArea.Width,
                                    screen.WorkingArea.Height);
                temp.Intersect(r);

                if (!temp.IsEmpty)
                    coverage += (temp.Width*temp.Height)/(r.Width*r.Height);
                if (coverage > 0.999999999)
                {
                    return true;
                }
            }

            return false;
        }

 

        private void OnDataContextChanged(object sender_, DependencyPropertyChangedEventArgs e_)
        {
            //Binding happens too late. We need this function to set properties as soon as DataContext has been changed
            //same in Floating only dock
            if (e_.NewValue is ILayoutSaveLoadRequester)
            {
                controller.EventHost = e_.NewValue as ILayoutSaveLoadRequester;
            }
            if (e_.NewValue is DockViewModel)
            {
                DockViewModel vm = (DockViewModel) e_.NewValue;
                factory.ItemsSource = vm.Workspaces;

                if (WindowDeactivationNotificationEnabled)
                {
                    XamDockHelper.HwndSourceHookFactory hookFactory =
                        XamDockHelper.GetCheckForActivePaneChangesHookFactory(Window.GetWindow(this), dockManager,
                                                                              DataContext);
                    dockManager.Loaded += (s, e) =>
                        {
                            var mainWindow = Window.GetWindow(this);
                            var mainWindowHandle = (new WindowInteropHelper(mainWindow)).Handle;
                            var hwndSource = HwndSource.FromHwnd(mainWindowHandle);
                            hwndSource.AddHook(hookFactory(mainWindow));
                            hwndSource.AddHook(SetActivePaneAtClick);
                        };
                    dockManager.ToolWindowLoaded += (s, e) =>
                        {
                            var paneToolWindow = e.Window;
                            RoutedEventHandler loaded = null;
                            paneToolWindow.Loaded += loaded = (s1, e1) =>
                                {
                                    paneToolWindow.Loaded -= loaded;
                                    var paneWindow = Window.GetWindow(e.Window);
                                    var paneWindowHandle = (new WindowInteropHelper(paneWindow)).Handle;
                                    var hwndSource = HwndSource.FromHwnd(paneWindowHandle);
                                    hwndSource.AddHook(hookFactory(paneWindow));
                                };
                        };
                }
            }
        }

        // Changing tabs with mouse click might deactivate a pane before the tab is switched;
        // therefore, we must memorize which tab was active at the moment of click
        private IntPtr SetActivePaneAtClick(System.IntPtr hwnd, int msg, System.IntPtr wParam, System.IntPtr lParam,
                                            ref bool handled)
        {
            if (msg == Win32.WM_LBUTTONDOWN)
            {
                (DataContext as TabbedDockViewModel).ActivePaneAtClick = dockManager.ActivePane;
            }
            return IntPtr.Zero;
        }

        public void ShowHideHeaderOnCCPs(Visibility visibility)
        {
            factory.ShowHideHeaderOnCCPs(visibility);
            foreach (var toolWindow in dockManager.GetAllToolWindows())
            {
                ((CustomPaneToolWindow) toolWindow).HeaderVisibility = visibility;
            }
            if (visibility == Visibility.Collapsed)
            {
                dockManager.PaneDragStarting += dockManager_PaneDragStarting;
            }
            else
            {
                dockManager.PaneDragStarting -= dockManager_PaneDragStarting;
            }
            CustomXamTabControl.HeaderVisibility = visibility;
            tabControl.RefreshContextMenus();
        }

        private void dockManager_PaneDragStarting(object sender, PaneDragStartingEventArgs e)
        {
            if (sender != dockManager)
                return;
            e.Cancel = true;
        }

        public void MoveAllWindowsByVector(Vector delta_)
        {
            foreach (var splitPane in dockManager.Panes)
            {
                if (XamDockManager.GetPaneLocation(splitPane) == PaneLocation.Floating ||
                    XamDockManager.GetPaneLocation(splitPane) == PaneLocation.FloatingOnly)
                {
                    XamDockManager.SetFloatingLocation(splitPane,
                        XamDockManager.GetFloatingLocation(splitPane) + delta_);
                }
            }
        }
    }

    internal class TabbedDockPaneDragEndedEventArgs : EventArgs
    {
        private readonly PaneDragEndedEventArgs _infragisticsArgs;
        private bool _shouldCloseWindow;

        public TabbedDockPaneDragEndedEventArgs(PaneDragEndedEventArgs infragisticsArgs)
        {
            _infragisticsArgs = infragisticsArgs;
        }

        public PaneDragEndedEventArgs InfragisticsArgs
        {
            get { return _infragisticsArgs; }
        }

        public bool ShouldCloseWindow
        {
            get { return _shouldCloseWindow; }
        }

        public void MarkWindowForClosing()
        {
            _shouldCloseWindow = true;
        }
    }
}
