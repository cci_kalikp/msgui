﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.ViewModels
{
	internal class WorkspaceViewModel : DockViewModel, IWindowManager
	{
		#region IWindowManager Members

		public void AddNewView(ChromeManager.WindowViewContainer viewContainer_, object additionalData_)
		{
//            var wsfound = Workspaces.FirstOrDefault(ws => ws.ID == viewContainer_.ID);
//            if (wsfound != null) return;

//            WindowViewModel windowViewModel = new WindowViewModel(viewContainer_)
//            {
//#pragma warning disable 612,618
//                InitialViewSettings = new InitialViewSettings(viewContainer_.Parameters),
//#pragma warning restore 612,618
//                Host = additionalData_ as ShellTabViewModel
//            };

//            if (TabbedDockViewModel.RemoveEmptyTabsOnLastChildClosed)
//            {
//                EventHandler closeHandler = null;
//                closeHandler = (s, e) =>
//                {
//                    if (Workspaces.Count((w) => { return w.Host == windowViewModel.Host as ShellTabViewModel; }) < 2)
//                    {
//                        m_tabs.Remove(windowViewModel.Host as ShellTabViewModel);
//                    }
//                    windowViewModel.Closed -= closeHandler;
//                };
//                windowViewModel.Closed += closeHandler;
//            }

//            Workspaces.Add(windowViewModel);
		}

		public void Clean()
		{
			throw new NotImplementedException();
		}

		public System.Xml.Linq.XDocument SaveState(Func<ChromeManager.WindowViewContainer, System.Xml.Linq.XElement> state_)
		{
			throw new NotImplementedException();
		}

		public void BeforeSaveState()
		{
			throw new NotImplementedException();
		}

		public void AfterSaveState()
		{
			throw new NotImplementedException();
		}

		public void LoadState(System.Xml.Linq.XDocument layout_, Action<System.Xml.Linq.XDocument, object> applyNodeAction_)
		{
			throw new NotImplementedException();
		}

		public void BeforeLoadState()
		{
			throw new NotImplementedException();
		}

		public void AfterLoadState()
		{
			throw new NotImplementedException();
		}

		public event EventHandler<WindowManagerActiveChangedEventArgs> ActivePaneChanged;

		public Core.IDialogWindow CreateDialog()
		{
			throw new NotImplementedException();
		}

		#endregion
	}
}
