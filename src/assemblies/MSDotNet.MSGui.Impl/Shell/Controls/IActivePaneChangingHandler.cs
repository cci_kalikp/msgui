﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows; 
namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
	internal interface IActivePaneChangingHandler
	{
        void RaiseActivePaneChanging(UIElement oldValue, UIElement newValue, out bool cancel);
	}
}
