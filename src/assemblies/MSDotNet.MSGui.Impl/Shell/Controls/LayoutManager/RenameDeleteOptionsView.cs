﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Controls/LayoutManager/RenameDeleteOptionsView.cs#28 $
// $Change: 891989 $
// $DateTime: 2014/08/08 03:25:52 $
// $Author: caijin $

using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;
using System.Windows.Controls; 
using System.Windows.Threading;
using Microsoft.Practices.Composite;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using System.Collections.Generic;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using System.Threading;
using MorganStanley.MSDotNet.MSGui.Impl.Options;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.LayoutManager
{
    internal class RenameDeleteOptionsView : IValidatableOptionView, ISubItemAwareSeachablePage
    {
        private readonly LayoutManagerOptionsPageControl m_content;
        private readonly PersistenceProfileService m_service;
        private readonly ObservableCollection<LayoutNameHolder> m_holders = new ObservableCollection<LayoutNameHolder>();
        private CurrentLayoutToFormatSaver m_currentLayout;
        private LayoutNameHolder m_defaulsNameHolder;

        public RenameDeleteOptionsView(PersistenceProfileService service_)
        {
            m_content = new LayoutManagerOptionsPageControl();
            m_service = service_;
            m_content.RenameDeleteControl.listview.ItemsSource = m_holders;
            m_content.StorageInfoControl.DataContext = new StorageInfo(service_.Storage);
            m_defaulsNameHolder = new LayoutNameHolder(service_.Storage.DefaultName);

            m_content.DefaultLayoutControl.DefaultLayoutComboBox.ItemsSource = m_holders;
            //			m_content.DefaultLayoutControl.DataContext = m_defaulsNameHolder;
            //if (service_.Storage is IMultiformatPersistenceStorage)
            //{
            //  m_currentLayout = new CurrentLayoutToFormatSaver(service_);
            //  m_content.CurrentLayoutControl.DataContext = m_currentLayout;
            //}
            //else
            {
                m_content.CurrentLayoutControl.IsEnabled = false;
                m_content.CurrentLayoutControl.Visibility = Visibility.Collapsed;
            }
            Dispatcher.CurrentDispatcher.BeginInvoke(() =>
                {
                    m_service.CurrentAvailableProfiles.CollectionChanged += OnAvailableProfilesOnCollectionChanged;
                });
        }

        public event EventHandler UpdateMenu;

        private void OnAvailableProfilesOnCollectionChanged(object sender_, NotifyCollectionChangedEventArgs args_)
        {
            ReinitializeBindingSource();
        }

        public void OnOK()
        {
            m_service.CurrentAvailableProfiles.CollectionChanged -= OnAvailableProfilesOnCollectionChanged;

            //in case following situation happens, we first to the deletion
            //Rename "ProfileA" to "ProfileB"; Delete "ProfileB" 
            foreach (var layoutNameHolder in m_holders.ToArray())
            {
                if (layoutNameHolder.Delete)
                {
                    m_service.DeleteProfile(layoutNameHolder.Name);
                }
            }

            string newDefaultLayoutName = m_service.Storage.DefaultName;
            if ((m_content != null) && (m_content.DefaultLayoutControl != null) &&
                (m_content.DefaultLayoutControl.DefaultLayoutComboBox != null))
            {
                LayoutNameHolder defaultLayoutHolder =
                    m_content.DefaultLayoutControl.DefaultLayoutComboBox.SelectedItem as LayoutNameHolder;
                if (defaultLayoutHolder != null && !defaultLayoutHolder.Delete)
                {
                    //if the default layout selected is to be deleted by the end user, don't change the Default layout in the system
                    //Question: what if the system default layout is deleted??
                    newDefaultLayoutName =
                        ((LayoutNameHolder) m_content.DefaultLayoutControl.DefaultLayoutComboBox.SelectedItem).Name;
                }
            }

            //if following situation is detected  
            //Rename "ProfileA" to "ProfileB"; Rename "ProfileB" to "ProfileA"
            //we need to rename the profile to a temporary name first and then rename them back later on
            Dictionary<string, LayoutNameHolder> specialLayoutRenaming = GetSpecialRenamings();
            Dictionary<string, LayoutNameHolder> skipLayoutRenaming = new Dictionary<string, LayoutNameHolder>();
            foreach (var layoutNameHolder in m_holders.ToArray())
            {
                if (layoutNameHolder.Delete) continue;

                //compare in an case insensitive way
                if (string.Compare(layoutNameHolder.NewName, layoutNameHolder.Name, true) != 0)
                {
                    if (skipLayoutRenaming.ContainsKey(layoutNameHolder.NewName)) continue;

                    if (!specialLayoutRenaming.ContainsKey(layoutNameHolder.NewName))
                    {
                        //if renaming failed no need to update the default layout name if needed
                        // this is usually due to the missing of the old state
                        //this can happen, when user deleted the default layout and then restart,
                        //this time a new default layout would be created, but not persisted yet,
                        //to rename it would trigger an exception, so that the renaming failed
                        if (!m_service.RenameProfile(layoutNameHolder.Name, layoutNameHolder.NewName, false))
                        {
                            skipLayoutRenaming.Add(layoutNameHolder.NewName, layoutNameHolder);
                            continue;
                        }
                        if (layoutNameHolder.Name == newDefaultLayoutName)
                        {
                            newDefaultLayoutName = layoutNameHolder.NewName;
                            //m_defaulsNameHolder = new LayoutNameHolder(layoutNameHolder.NewName); 
                        }
                    }
                    else
                    {
                        string realNewName = layoutNameHolder.NewName;
                        layoutNameHolder.NewName = layoutNameHolder.NewName + Guid.NewGuid().ToString("N");

                        //if renaming failed no need to go further for the second step of renaming for the special case
                        // this is usually due to the missing of the old state
                        //this can happen, when user deleted the default layout and then restart,
                        //this time a new default layout would be created, but not persisted yet,
                        //to rename it would trigger an exception, so that the renaming failed
                        if (!m_service.RenameProfile(layoutNameHolder.Name, layoutNameHolder.NewName, false))
                        {
                            skipLayoutRenaming.Add(realNewName, layoutNameHolder);
                            LayoutNameHolder switcher;
                            if (specialLayoutRenaming.TryGetValue(layoutNameHolder.Name, out switcher))
                            {
                                skipLayoutRenaming.Add(layoutNameHolder.NewName, switcher);
                            }
                        }
                    }
                }
            }
            bool newDefaultNameSet = false;
            foreach (var special in specialLayoutRenaming)
            {
                if (skipLayoutRenaming.ContainsKey(special.Key)) continue;

                LayoutNameHolder layoutNameHolder = special.Value;
                //if renaming failed rollback the old name
                //this is usually due to try to rename to a special name take "default" which is used by system
                if (!m_service.RenameProfile(layoutNameHolder.NewName, special.Key, false))
                {
                    m_service.RenameProfile(layoutNameHolder.NewName, layoutNameHolder.Name, false);
                }
                else
                {
                    layoutNameHolder.NewName = special.Key;
                    if (!newDefaultNameSet && layoutNameHolder.Name == newDefaultLayoutName)
                    {
                        newDefaultLayoutName = layoutNameHolder.NewName;
                        //m_defaulsNameHolder = new LayoutNameHolder(layoutNameHolder.NewName);
                        newDefaultNameSet = true;
                    }
                }
            }

            if (skipLayoutRenaming.Count > 0)
            {
                string profileNames = string.Empty;
                foreach (var layoutNameHolder in skipLayoutRenaming.Values)
                {
                    profileNames += layoutNameHolder.Name + ", ";
                }
                profileNames = profileNames.Remove(profileNames.Length - 2);
                TaskDialog.ShowMessage(string.Format("Layouts: {0} are not renamed", profileNames), "Warning",
                                       TaskDialogCommonButtons.Close, VistaTaskDialogIcon.Warning,
                                       Window.GetWindow(m_content));
            }
            if (string.Compare(m_service.Storage.DefaultName, newDefaultLayoutName, true) != 0)
            {
                m_service.Storage.DefaultName = newDefaultLayoutName;
            }

            m_service.CurrentAvailableProfiles.CollectionChanged += OnAvailableProfilesOnCollectionChanged;
            ReinitializeBindingSource();
            var copy = UpdateMenu;
            if (copy != null)
                UpdateMenu(null, null);
        }

        private LayoutNameHolder invalidLayout = null;
        public bool OnValidate()
        {
            invalidLayout = null;
            List<string> newNames = new List<string>();
            foreach (var layoutNameHolder in m_holders.ToArray())
            {
                if (layoutNameHolder.Delete)
                {
                    continue;
                }
                if (string.IsNullOrWhiteSpace(layoutNameHolder.NewName))
                {
                    invalidLayout = layoutNameHolder;
                    TaskDialog.ShowMessage("Empty layout names detected", "Invalid Settings",
                                           TaskDialogCommonButtons.Close, VistaTaskDialogIcon.Warning,
                                           Window.GetWindow(m_content));
                    return false;
                }
                string newName = layoutNameHolder.NewName.ToLower();
                if (!newNames.Contains(newName))
                {
                    newNames.Add(newName);
                }
                else
                {
                    invalidLayout = layoutNameHolder;
                    TaskDialog.ShowMessage("Duplicate layout names detected.", "Invalid Settings",
                                           TaskDialogCommonButtons.Close, VistaTaskDialogIcon.Warning,
                                           Window.GetWindow(m_content));
                    return false;
                }
            }
            return true;
        }

        public void OnInvalid()
        {
            if (invalidLayout != null)
            {
                HighlightLayout(invalidLayout);
                invalidLayout = null;
            }
        }

        private Dictionary<string, LayoutNameHolder> GetSpecialRenamings()
        {
            Dictionary<string, LayoutNameHolder> newNames = new Dictionary<string, LayoutNameHolder>();
            Dictionary<string, LayoutNameHolder> oldNames = new Dictionary<string, LayoutNameHolder>();
            foreach (var layoutNameHolder in m_holders.ToArray())
            {
                if (layoutNameHolder.Delete) continue;

                //compare in an case insensitive way
                if (string.Compare(layoutNameHolder.NewName, layoutNameHolder.Name, true) != 0)
                {
                    //we only need to consider the renaming action, since duplicate name check has already been done previously
                    newNames.Add(layoutNameHolder.NewName, layoutNameHolder);
                    oldNames.Add(layoutNameHolder.Name, layoutNameHolder);
                }
            }

            Dictionary<string, LayoutNameHolder> specialLayouts = new Dictionary<string, LayoutNameHolder>();
            foreach (
                string specialNewName in
                    newNames.Keys.Intersect(oldNames.Keys,
                                            StringComparer.Create(Thread.CurrentThread.CurrentCulture, true)))
            {
                specialLayouts.Add(specialNewName, newNames[specialNewName]);
            }
            return specialLayouts;
        }

        public void OnCancel()
        {
        }

        public void OnDisplay()
        {
            ReinitializeBindingSource();
            if (m_currentLayout != null)
            {
                m_currentLayout.Refresh();
            }
        }

        public void OnHelp()
        {
        }

        public UIElement Content
        {
            get { return m_content; }
        }

        private void ReinitializeBindingSource()
        {
            m_holders.Clear();
            m_holders.AddRange(m_service.CurrentAvailableProfiles.Select(profile_ => new LayoutNameHolder(profile_)));

            var defaultLayout =
                m_holders.FirstOrDefault((holder) => { return holder.Name == m_service.Storage.DefaultName; });


            m_content.DefaultLayoutControl.DefaultLayoutComboBox.ItemsSource = null;
            m_content.DefaultLayoutControl.DefaultLayoutComboBox.ItemsSource = m_holders;
            m_content.DefaultLayoutControl.DefaultLayoutComboBox.SelectedIndex = defaultLayout == null
                                                                                     ? -1
                                                                                     : m_holders.IndexOf(defaultLayout);
        }


        public int GetMatchCount(string textToSearch_)
        {
            if (m_holders.Count == 0)
            {
                m_holders.AddRange(m_service.CurrentAvailableProfiles.Select(profile_ => new LayoutNameHolder(profile_)));
            }
            return m_holders.Count(holder_ => holder_.Name.ToLower().Contains(textToSearch_.ToLower()));
        }

        public IList<KeyValuePair<string, object>> GetMatchedItems(string textToSearch_)
        {
            if (m_holders.Count == 0)
            {
                m_holders.AddRange(m_service.CurrentAvailableProfiles.Select(profile_ => new LayoutNameHolder(profile_)));
            }
            var matchedProfiles = new List<KeyValuePair<string, object>>();
            foreach (var layoutNameHolder in m_holders)
            {
                if (layoutNameHolder.Name.ToLower().Contains(textToSearch_.ToLower()))
                {
                    matchedProfiles.Add(new KeyValuePair<string, object>("Layouts → " + layoutNameHolder.Name, layoutNameHolder.Name));
                }
            }
            return matchedProfiles;
        }

        public void LocateItem(object item_)
        {
            string profileName = item_ as string;
            if (!string.IsNullOrEmpty(profileName))
            {
                var profile = 
                    m_holders.FirstOrDefault(layout_ => layout_.Name == profileName);
                if (profile != null)
                {
                    HighlightLayout(profile);
                }
            }
        }

        private void HighlightLayout(LayoutNameHolder layoutNameHolder_)
        {
            m_content.RenameDeleteControl.listview.SelectedItem = layoutNameHolder_;
            m_content.RenameDeleteControl.listview.ScrollIntoView(layoutNameHolder_);
            var item =
                m_content.RenameDeleteControl.listview.ItemContainerGenerator.ContainerFromItem(layoutNameHolder_) as
                ListViewItem;
            if (item != null)
            {
                item.Highlight();
            }
        }
    }
}
