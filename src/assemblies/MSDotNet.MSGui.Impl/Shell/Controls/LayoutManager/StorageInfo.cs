﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Controls/LayoutManager/StorageInfo.cs#6 $
// $Change: 875617 $
// $DateTime: 2014/04/08 21:26:23 $
// $Author: caijin $
 
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.LayoutManager
{
  internal class StorageInfo : Core.ViewModel.ViewModelBase
  {
    public StorageInfo(IPersistenceStorage storage_)
    {
      ProviderName = storage_.GetType().Name;
      IsMultiformat = storage_ is IMultiformatPersistenceStorage;
    }

    private string m_providerName;
    public string ProviderName
    {
      get
      {
        return m_providerName;
      }
      set
      {
        m_providerName = value;
        OnPropertyChanged("ProviderName");
      }
    }

    private bool m_isMutiformat;
    public bool IsMultiformat
    {
      get
      {
        return m_isMutiformat;
      }
      set
      {
        m_isMutiformat = value;
        OnPropertyChanged("IsMultiformat");
      }
    }

    
  }
}
