﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Controls/LayoutManager/CurrentLayoutToFormatSaver.cs#5 $
// $Change: 875617 $
// $DateTime: 2014/04/08 21:26:23 $
// $Author: caijin $

using System.Collections.ObjectModel;  
using System.Windows.Input;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.LayoutManager
{
  internal class CurrentLayoutToFormatSaver : Core.ViewModel.ViewModelBase
  {
    private readonly PersistenceProfileService m_service;

    public CurrentLayoutToFormatSaver(PersistenceProfileService service_)
    {
      m_service = service_;
      m_storage = service_.Storage as IMultiformatPersistenceStorage;

      CommandBinding saveAsBinding = new CommandBinding(SaveAsCommand,
                                                        OnSaveAs,
                                                        (o_, e_) => e_.CanExecute = true);
      CommandManager.RegisterClassCommandBinding(typeof(CurrentLayoutToFormatSaver), saveAsBinding);
      CommandBindings.Add(saveAsBinding);

      if (m_storage != null)
      {
        foreach (string wm in m_storage.SupportedWindowManagers)
        {
          m_availableFormats.Add(new FormatPair{WindowManager = wm, FormatName = m_storage.GetFormatName(wm)});
        }
      }
      Refresh();
    }

    public void Refresh()
    {
      LayoutName = m_service.CurrentProfile;
      string currentFormat = m_storage.GetWindowManager(LayoutName);
      int i = 0;
      foreach (string format in m_storage.SupportedWindowManagers)
      {
        if (format == currentFormat)
        {
          SelectedIndex = i;
          break;          
        }
        i++;
      }
    }

    private void OnSaveAs(object sender_, ExecutedRoutedEventArgs e_)
    {
      foreach (FormatPair format in AvailableFormats)
      {
        if (format == SelectedNewFormat)
        {
          string layoutName = LayoutName;
          string possibleCurrentSuffix = m_storage.GetSuffix(m_storage.GetWindowManager(LayoutName));
          if (layoutName.EndsWith(possibleCurrentSuffix))
          {
            layoutName.Substring(0, layoutName.Length - possibleCurrentSuffix.Length);
          }
          m_service.SaveCurrentProfileAs(layoutName, SelectedNewFormat.WindowManager);
          return;
        }
      }
    }

    private string m_layoutName;
    public string LayoutName
    {
      get
      {
        return m_layoutName;
      }
      set
      {
        m_layoutName = value;
        OnPropertyChanged("LayoutName");
      }
    }

    private FormatPair m_selectedNewFormat;
    public FormatPair SelectedNewFormat
    {
      get
      {
        return m_selectedNewFormat;
      }
      set
      {
        m_selectedNewFormat = value;
        OnPropertyChanged("SelectedNewFormat");
      }
    }

    private int m_selectedIndex;
    public int SelectedIndex
    {
      get
      {
        return m_selectedIndex;
      }
      set
      {
        m_selectedIndex = value;
        OnPropertyChanged("SelectedIndex");
      }
    }

    private readonly ObservableCollection<FormatPair> m_availableFormats = new ObservableCollection<FormatPair>();
    public ObservableCollection<FormatPair> AvailableFormats
    {
      get
      {
        return m_availableFormats;
      }
    }

    private readonly RoutedCommand m_saveAsCommand = new RoutedCommand();
    public ICommand SaveAsCommand
    {
      get
      {
        return m_saveAsCommand;
      }
    }

    private readonly CommandBindingCollection m_commandBindings = new CommandBindingCollection();
    private IMultiformatPersistenceStorage m_storage;

    public CommandBindingCollection CommandBindings
    {
      get
      {
        return m_commandBindings;
      }
    }
       
  }

  class FormatPair
  {
    public string WindowManager
    {
      get; 
      set;
    }
    public string FormatName
    {
      get;
      set;
    }
  }
}
