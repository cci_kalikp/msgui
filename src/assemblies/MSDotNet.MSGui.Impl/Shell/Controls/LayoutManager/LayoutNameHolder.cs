﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Controls/LayoutManager/LayoutNameHolder.cs#8 $
// $Change: 875617 $
// $DateTime: 2014/04/08 21:26:23 $
// $Author: caijin $
 
namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.LayoutManager
{
  internal class LayoutNameHolder:Core.ViewModel.ViewModelBase
  {
    public LayoutNameHolder(string name_)
    {
      Name = name_; 
      NewName = name_;
      Delete = false;
    }


    public string Name
    {
        get;
        private set;
    }

    private string newName;
    public string NewName
    {
        get
        {
            return this.newName;
        }
        set
        {
            this.newName = value;
            OnPropertyChanged("NewName");
            if (string.Compare(this.newName, this.Name, true) != 0)
            {
                NameToDisplay = string.Format(string.Format("{0} (Old Name: {1})", this.newName, this.Name));
            }
            else
            {
                NameToDisplay = this.NewName;
            }
            OnPropertyChanged("NameToDisplay");
        }
    }

    public string NameToDisplay
    {
        get;
        private set;
    }
    private bool delete;
    public bool Delete
    {
        get { return delete; }
        set
        {
            delete = value;
            OnPropertyChanged("Delete");
        }
    }

	public override string ToString()
	{
        return NameToDisplay;
	}
 

  }
}
