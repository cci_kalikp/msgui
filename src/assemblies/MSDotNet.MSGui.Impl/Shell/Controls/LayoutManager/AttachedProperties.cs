﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Controls/LayoutManager/AttachedProperties.cs#4 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System.Windows;
using System.Windows.Input;

namespace MorganStanley.MSDotNet.MSGui.ConcordLayoutMgrModule.Controls
{
  class AttachedProperties
  {
    public static DependencyProperty RegisterCommandBindingsProperty = 
      DependencyProperty.RegisterAttached("RegisterCommandBindings", typeof(CommandBindingCollection), typeof(AttachedProperties), new PropertyMetadata(null, OnRegisterCommandBindingChanged));

    public static void SetRegisterCommandBindings(UIElement element_, CommandBindingCollection value)
    {
      if (element_ != null)
        element_.SetValue(RegisterCommandBindingsProperty, value);
    }
    public static CommandBindingCollection GetRegisterCommandBindings(UIElement element_)
    {
      return (element_ != null ? (CommandBindingCollection)element_.GetValue(RegisterCommandBindingsProperty) : null);
    }
    private static void OnRegisterCommandBindingChanged(DependencyObject sender_, DependencyPropertyChangedEventArgs e_)
    {
      UIElement element = sender_ as UIElement;
      if (element != null)
      {
        CommandBindingCollection bindings = e_.NewValue as CommandBindingCollection;
        if (bindings != null)
        {
          element.CommandBindings.AddRange(bindings);
        }
      }
    }
  }
}
