﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
	internal class WindowManagerActiveChangingEventArgs : EventArgs
	{
#pragma warning disable 612,618
    public IViewContainer OldValue
#pragma warning restore 612,618
    {
      get; private set;
    }
#pragma warning disable 612,618
    public IViewContainer NewValue
#pragma warning restore 612,618
    {
      get;
      private set;
    }
#pragma warning disable 612,618
		public WindowManagerActiveChangingEventArgs(IViewContainer old_, IViewContainer new_)
#pragma warning restore 612,618
    {
      OldValue = old_;
      NewValue = new_;
    }
		public bool Cancel { get; set; }
	}
}
