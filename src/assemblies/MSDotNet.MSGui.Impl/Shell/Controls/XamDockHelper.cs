﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Controls/XamDockHelper.cs#27 $
// $Change: 898815 $
// $DateTime: 2014/09/30 07:55:53 $
// $Author: caijin $

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using Infragistics.Windows.Controls;
using Infragistics.Windows.DockManager;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Win32;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
    internal static class XamDockHelper
    {
        public static SplitPane GetRootOfDockedPane(IWindowViewContainer view)
        {
            return GetRootOfView<XamDockManager>(view) as SplitPane;
        }

        private static DependencyObject GetRootOfView<T>(IWindowViewContainer view)
            where T : class
        {
            IChildViewContainer childView = view as IChildViewContainer;
            if (childView != null)
            {
                view = childView.Parent;
            }
            var contentPanes = DockHelper.GetPanes(new[] { view });
            if (contentPanes == null) return null;
            var contentPane = contentPanes.FirstOrDefault() as ContentPane;
            if (contentPane == null) return null;
            return GetRootOfPane<T>(contentPane);
        }

        private static DependencyObject GetRootOfPane<T>(ContentPane pane)
            where T : class
        {
            DependencyObject pRoot = pane.Parent;
            if (pane.PaneLocation == PaneLocation.Unpinned)
            {
                var tempRoot = ReflHelper.PropertyGet(ReflHelper.PropertyGet(pane, "PlacementInfo"), "DockableContainer") as DependencyObject;
                if (tempRoot != null && tempRoot != pane.Parent)
                {
                    pRoot = tempRoot;
                }
            } 
            while (pRoot != null && !(pRoot.GetParentObject() is T))
            {
                pRoot = pRoot.GetParentObject();
            }

            return pRoot;
        }

        public static PaneTabItem GetTabItem(ContentPane cp_)
        {
            ItemsControl owner = LogicalTreeHelper.GetParent(cp_) as ItemsControl;
            PaneTabItem tab = null;

            if (null != owner)
                tab = owner.ItemContainerGenerator.ContainerFromItem(cp_) as PaneTabItem;

            return tab;
        }
         

        public static int CountPanes(PaneToolWindow window_)
        {
            if (window_ == null || window_.Pane == null || window_.Pane.Panes == null)
            {
                return 0;
            }
            int result = 0;
            foreach (FrameworkElement pane in window_.Pane.Panes)
            {
                result += CountPanes(pane);
            }
            return result;
        }

        #region GetPanes

        public static List<ContentPane> GetPanes(PaneToolWindow window_)
        {
            if (window_ == null || window_.Pane == null || window_.Pane.Panes == null)
            {
                return new List<ContentPane>();
            }
            List<ContentPane> result = new List<ContentPane>();
            foreach (FrameworkElement pane in window_.Pane.Panes)
            {
                result.AddRange(GetPanes(pane));
            }
            return result;
        }


        public static List<ContentPane> GetPanes(object pane_)
        {
            if (pane_ is ContentPane)
            {
                return new List<ContentPane>() { pane_ as ContentPane };
            }
            if (pane_ is TabGroupPane)
            {
                if (((TabGroupPane)pane_).Items.Count == 0)
                {
                    return new List<ContentPane>();
                }
                List<ContentPane> sum = new List<ContentPane>();
                foreach (var child in ((TabGroupPane)pane_).Items)
                {
                    sum.AddRange(GetPanes(child));
                }
                return sum;
            }
            if (pane_ is DocumentContentHost)
            {
                List<ContentPane> sum = new List<ContentPane>();
                var dch = pane_ as DocumentContentHost;
                foreach (var sp in dch.Panes)
                {
                    sum.AddRange(GetPanes(sp));
                }
                return sum;
            }
            if (pane_ is SplitPane)
            {
                List<ContentPane> sum = new List<ContentPane>();
                foreach (var child in ((SplitPane)pane_).Panes)
                {
                    sum.AddRange(GetPanes(child));
                }
                return sum;
            }
            return new List<ContentPane>();
        }
        #endregion
 

        public static int CountPanes(object pane_)
        {
            if (pane_ is ContentPane)
            {
                return 1;
            }

            if (pane_ is TabGroupPane)
            {
                if (((TabGroupPane)pane_).Items.Count == 0)
                {
                    return 0;
                }
                return ((TabGroupPane) pane_).Items.Cast<object>().Sum(child => CountPanes(child));
            }

            if (pane_ is DocumentContentHost)
            {
                return ((DocumentContentHost)pane_).Panes.Sum(sp => CountPanes(sp));
            }

            if (pane_ is SplitPane)
            {
                return ((SplitPane) pane_).Panes.Sum(child => CountPanes(child));
            }
            return 0;
        }

        // When we have a floating window with either only one child ContentPane or
        // one TabGroup, we need to show the appropriate title, icon & etc in the win's header
        // This function is to find either that one ContentPane or the selected Tab in the TabGroupPane
        public static ContentPane FindOnlyPane(object pane_)
        {
            if (pane_ is ContentPane)
            {
                return (ContentPane)pane_;
            }
            if (pane_ is TabGroupPane)
            {
                if (((TabGroupPane)pane_).Items.Count == 0)
                {
                    return null;
                }

                if (((TabGroupPane)pane_).SelectedItem != null)
                {
                    ContentPane tmp = FindOnlyPane(((TabGroupPane)pane_).SelectedItem);
                    if (tmp != null)
                    {
                        return tmp;
                    }
                }
                else
                {
                    foreach (var child in ((TabGroupPane)pane_).Items)
                    {
                        ContentPane tmp = FindOnlyPane(child);
                        if (tmp != null)
                        {
                            return tmp;
                        }
                    }
                }
                return null;
            }
            if (pane_ is SplitPane)
            {
                foreach (var child in ((SplitPane)pane_).Panes)
                {
                    ContentPane tmp = FindOnlyPane(child);
                    if (tmp != null)
                    {
                        return tmp;
                    }
                }
                return null;
            }

            var dch = pane_ as DocumentContentHost;
            if (dch != null)
            {
                if (dch.ActiveDocument != null)
                    return dch.ActiveDocument;

                if (dch.Panes.Count > 0)
                    return FindOnlyPane(dch.Panes[0]);
            }

            return null;
        }

   
        public static ContentPane FindOnlyContentPaneChild(SplitPane pane_)
        {
            return FindOnlyPane(pane_);
        }

        public static void DoForEveryChildContentPane(object pane_, PaneAction action_)
        {
            if (pane_ is ContentPane)
            {
                action_((ContentPane)pane_);
            }
            if (pane_ is TabGroupPane)
            {
                foreach (var child in ((TabGroupPane)pane_).Items)
                {
                    DoForEveryChildContentPane(child, action_);
                }
            }
            if (pane_ is SplitPane)
            {
                foreach (var child in ((SplitPane)pane_).Panes)
                {
                    DoForEveryChildContentPane(child, action_);
                }
            }

            var dch = pane_ as DocumentContentHost;
            if (dch != null)
            {
                foreach (var pane in dch.Panes)
                {
                    DoForEveryChildContentPane(pane, action_);
                }
            }
        }

        public static void SetMetadataIfPossible(ContentPane cp_)
        {
            PaneToolWindow parentWindow = ToolWindow.GetToolWindow(cp_) as PaneToolWindow;
            if (
              parentWindow != null &&
              cp_ != null &&
              Attached.GetViewContainer(cp_) != null &&
              CountPanes(parentWindow) == 1)
            {
                var meta = Attached.GetViewContainerType(cp_);
                Attached.SetViewContainerType(parentWindow, meta);
            }
        }

        public static void SetMetadata(ContentPane cp_, PaneToolWindow win_)
        {
            if (
              win_ != null &&
              cp_ != null &&
              Attached.GetViewContainer(cp_) != null &&
              CountPanes(win_) == 1)
            {
                var meta = Attached.GetViewContainerType(cp_);
                Attached.SetViewContainerType(win_, meta);
            }
        }

        public static string GetSelectedTabName(TabControl tabControl_)
        {
            TabItemEx selectedTab = tabControl_.SelectedItem as TabItemEx;
            if (selectedTab == null)
            {
                return String.Empty;
            }
            return selectedTab.Header.ToString();
        }

        public static string GetTabName(TabItemEx tab_)
        {
            if (tab_ == null || tab_.Header == null)
            {
                return String.Empty;
            }
            return tab_.Header.ToString();
        }

        /// <summary>
        /// Creates a new ImageSource with the specified width/height
        /// </summary>
        /// <param name="source_">Source image to resize</param>
        /// <param name="width_">Width of resized image</param>
        /// <param name="height_">Height of resized image</param>
        /// <returns>Resized image</returns>
        public static ImageSource CreateResizedImage(ImageSource source_, int width_, int height_)
        {
            // Target Rect for the resize operation
            Rect rect = new Rect(0, 0, width_, height_);

            // Create a DrawingVisual/Context to render with
            DrawingVisual drawingVisual = new DrawingVisual();
            using (DrawingContext drawingContext = drawingVisual.RenderOpen())
            {
                drawingContext.DrawImage(source_, rect);
            }

            // Use RenderTargetBitmap to resize the original image
            RenderTargetBitmap resizedImage = new RenderTargetBitmap(
                (int)rect.Width, (int)rect.Height,  // Resized dimensions
                96, 96,                             // Default DPI values
                PixelFormats.Default);              // Default pixel format
            resizedImage.Render(drawingVisual);

            // Return the resized image
            return resizedImage;
        }

        /// <summary>
        /// Finds a parent of a given item on the visual tree.
        /// </summary>
        /// <typeparam name="T">The type of the queried item.</typeparam>
        /// <param name="child_">A direct or indirect child of the
        /// queried item.</param>
        /// <returns>The first parent item that matches the submitted
        /// type parameter. If not matching item can be found, a null
        /// reference is being returned.</returns>
        public static T TryFindParent<T>(this DependencyObject child_)
            where T : DependencyObject
        {
            //get parent item
            DependencyObject parentObject = DependencyObjectHelper.GetParentObject(child_);

            //we've reached the end of the tree
            if (parentObject == null) return null;

            //check if the parent matches the type we're looking for
            T parent = parentObject as T;
            if (parent != null)
            {
                return parent;
            }
            //use recursion to proceed with next level
            return TryFindParent<T>(parentObject);
        }

        public static void DoForEveryPanel(this XamDockManager dockManager_, PaneAction action_)
        {
            List<ContentPane> panes = new List<ContentPane>(dockManager_.GetPanes(PaneNavigationOrder.VisibleOrder));
            foreach (ContentPane pane in panes)
            {
                if (pane != null)
                {
                    action_.Invoke(pane);
                }
            }
        }

        public static IEnumerable<ToolWindow> GetAllToolWindows(this XamDockManager dockManager_)
        {
            HashSet<ToolWindow> result = new HashSet<ToolWindow>();
            var panes = dockManager_.GetPanes(PaneNavigationOrder.VisibleOrder);
            foreach (var contentPane in panes)
            {
                if (contentPane.IsFloating())
                {
                    ToolWindow tw = ToolWindow.GetToolWindow(contentPane);
                    if (tw != null && !result.Contains(tw))
                    {
                        result.Add(tw);
                    }
                }
            }
            return result;
        }


        public delegate void PaneAction(ContentPane pane_);

        public delegate HwndSourceHook HwndSourceHookFactory(Window currentWindow);

        public static HwndSourceHookFactory GetCheckForActivePaneChangesHookFactory(Window mainWindow, XamDockManager dockManager, object dockDataContext)
        {
            bool m_insideHandler = false;
            IDictionary<Window, CustomContentPane> m_activePanes = new Dictionary<Window, CustomContentPane>();
            CustomContentPane m_mouseactivatePane = null;
            return delegate(Window currentWindow)
            {
                return delegate(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
                {
                    if (msg == Win32.WM_LBUTTONDOWN)
                    {
                        var xPos = NativeUiMethodsHelper.GetXFromLParam(lParam);
                        var yPos = NativeUiMethodsHelper.GetYFromLParam(lParam);
                        var element = currentWindow.InputHitTest(new Point(xPos, yPos));
                        if (element is DependencyObject)
                        {
                            var pane = TryFindParent<CustomContentPane>(element as DependencyObject);
                            if (pane != null)
                            {
                                if (dockManager.ActivePane != pane && !m_insideHandler)
                                {
                                    bool cancel;
                                    m_insideHandler = true;
                                    (dockDataContext as IActivePaneChangingHandler).RaiseActivePaneChanging(dockManager.ActivePane, pane, out cancel);
                                    handled = true;
                                    if (!cancel)
                                    {
                                        m_activePanes[currentWindow] = pane;
                                        // have to re-set the cursor because the new value might be used
                                        var screenPoint = currentWindow.PointToScreen(new Point(xPos, yPos));
                                        Win32.SetCursorPos((int)screenPoint.X, (int)screenPoint.Y);
                                        handled = false;
                                    }

                                    m_insideHandler = false;
                                }
                            }
                        }
                    }
                    else if (msg == Win32.WM_ACTIVATE)
                    {
                        if (wParam.ToInt32() == Win32.WA.WA_INACTIVE)
                        {
                            if (dockManager.ActivePane != null && !m_insideHandler)
                            {
                                if (lParam == IntPtr.Zero ||
                                    HwndSource.FromHwnd(lParam) == null ||
                                        (!dockManager
                                            .GetAllToolWindows()
                                            .Select(tw => Window.GetWindow(tw))
                                            .Concat(new Window[] { mainWindow })
                                            .Contains((Window)HwndSource.FromHwnd(lParam).RootVisual as Window)))
                                    return IntPtr.Zero;
                                bool cancel;
                                ContentPane nextPane = null;
                                m_insideHandler = true;
                                if (m_mouseactivatePane != null)
                                    nextPane = m_mouseactivatePane;
                                else if (m_activePanes.ContainsKey(currentWindow))
                                    nextPane = m_activePanes[currentWindow];
                                (dockDataContext as IActivePaneChangingHandler).RaiseActivePaneChanging(dockManager.ActivePane, nextPane, out cancel);
                                if (cancel)
                                {
                                    handled = true;
                                }
                                else
                                {
                                    if (m_mouseactivatePane != null)
                                        m_mouseactivatePane.Activate();
                                    else
                                        ((Window)HwndSource.FromHwnd(lParam).RootVisual).Activate();
                                }
                                m_mouseactivatePane = null;
                                m_insideHandler = false;
                            }
                        }
                    }
                    else if (msg == Win32.WM_MOUSEACTIVATE)
                    {
                        Win32.POINT p;
                        Win32.GetCursorPos(out p);
                        IntPtr h = NativeUiMethodsHelper.WindowFromPointApi(p.Point);
                        if (h != IntPtr.Zero && HwndSource.FromHwnd(h) != null)
                        {
                            Window w = (Window)HwndSource.FromHwnd(h).RootVisual;
                            if (w != null)
                            {
                                var hitTest = VisualTreeHelper.HitTest(w, w.PointFromScreen(new Point(p.X, p.Y)));
                                if (hitTest != null)
                                {
                                    var element = hitTest.VisualHit;
                                    m_mouseactivatePane = TryFindParent<CustomContentPane>(element);
                                }
                            }
                        }
                    }
                    return IntPtr.Zero;
                };
            };
        }


        public static bool IsTopmost(IEnumerable<FrameworkElement> panes)
        {
            foreach (var pane in panes)
            {
                var cp = pane as CustomContentPane;
                if (cp != null && cp.Topmost)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsTopmost(FrameworkElement pane)
        {
            var cp = pane as CustomContentPane;
            if (cp != null)
            {
                return cp.Topmost;
            }

            var sp = pane as SplitPane;
            if (sp != null)
            {
                return IsTopmost(sp.Panes);
            }

            return false;
        }

        public static bool IsFloating(this ContentPane pane_)
        {
            return pane_.PaneLocation == PaneLocation.Floating || pane_.PaneLocation == PaneLocation.FloatingOnly;
        }

    }
}
