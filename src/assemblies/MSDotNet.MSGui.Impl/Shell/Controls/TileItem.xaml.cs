﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows;
using System.Windows.Automation.Peers;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using System.Windows.Input;
using System.Windows.Threading;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Win32;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Interop; 
using MorganStanley.MSDotNet.MSGui.Impl.TestSupport;
using Binding = System.Windows.Data.Binding;
using HorizontalAlignment = System.Windows.HorizontalAlignment;
using Panel = System.Windows.Controls.Panel;
using UserControl = System.Windows.Controls.UserControl;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
    /// <summary>
    /// Interaction logic for TileItem.xaml
    /// </summary>
    [TemplatePart(Name = TitleBarPartName, Type = typeof(TileItemPaneHeaderControl))]
    [TemplatePart(Name = ContentPartName, Type = typeof(ContentControl))]
    [TemplatePart(Name = MoverPartName, Type = typeof(Panel))]
    [TemplatePart(Name = ResizerPartName, Type = typeof(Panel))]
    public partial class TileItem : UserControl, ITitleDisplayer, IWindowViewContainerHost
    {
        private readonly FrameworkElement content;
        private const string TitleBarPartName = "PART_TitleBar";
        private const string ContentPartName = "PART_Content";
        private const string MoverPartName = "PART_Mover";
        private const string ResizerPartName = "PART_Resizer";
        private ContentControl itemControl;
        private readonly TileItemCollectionModel manager;
        static TileItem()
        { 
            DefaultStyleKeyProperty.OverrideMetadata(typeof(TileItem),
                             new FrameworkPropertyMetadata(typeof(TileItem)));

             
        }


        internal TileItem(TileItemViewModel tileItemViewModel_, TileItemCollectionModel manager_)
        {
            manager = manager_;
            TileItemViewContainer container = tileItemViewModel_.ViewContainer as TileItemViewContainer;
            if (container != null)
            {
                container.Visual = this;
            }
            if (tileItemViewModel_.InitialParameters.Resources != null)
            {
                this.Resources.MergedDictionaries.Add(tileItemViewModel_.InitialParameters.Resources);
            }
            //FocusManager.SetFocusedElement(this, Content as IInputElement);
            this.DataContext = tileItemViewModel_;
            InitializeComponent();
            content = tileItemViewModel_.Content as FrameworkElement;
            this.ViewModel = tileItemViewModel_;
            tileItemViewModel_.RequestActivate += tileItemViewModel__RequestActivate;
             
            if (tileItemViewModel_.InitialParameters.SizingMethod == SizingMethod.SizeToContent)
            {
                this.HorizontalContentAlignment = HorizontalAlignment.Stretch;
                this.VerticalContentAlignment = VerticalAlignment.Stretch;
                tileItemViewModel_.DisplayWidth = this.Width = content.Width;
                tileItemViewModel_.DisplayHeight = this.Height = content.Height + (tileItemViewModel_.TitleBarVisiblity == Visibility.Visible ? 22 : 0);

            }
            else
            {
                tileItemViewModel_.DisplayWidth = this.Width = tileItemViewModel_.DefaultSize.Width;
                tileItemViewModel_.DisplayHeight = this.Height = tileItemViewModel_.DefaultSize.Height;
            }

            WindowsFormsHost host = content as WindowsFormsHost;
            if (host != null )
            { 
                 host.Child.Layout += new LayoutEventHandler(Child_Layout);
                tileItemViewModel_.StateChanged += StateChanged;
                MouseEventMessageFilter.RegisterHost(host);
            }
            SetBinding();

            if (tileItemViewModel_.InitialParameters.InitialLocationCallback != null)
            {
                PointF location = tileItemViewModel_.InitialParameters.InitialLocationCallback(tileItemViewModel_.Parent.Width,
                                                                             tileItemViewModel_.Parent.Height, tileItemViewModel_.DisplayWidth, tileItemViewModel_.DisplayHeight);
                tileItemViewModel_.DisplayLeft = location.X;
                tileItemViewModel_.DisplayTop = location.Y;
            }

            tileItemViewModel_.RequestFlash += (sender_, e_) => manager_.FlashExecuter.Flash(this, e_.Value1);
            
            this.Loaded += new RoutedEventHandler(TileItem_Loaded);
            this.SizeChanged += new SizeChangedEventHandler(TileItem_SizeChanged);
        }

        void Child_Layout(object sender, LayoutEventArgs e)
        {
            if (suspendLayout) return;
            SyncHostClip();
        }

 
         
        private void StateChanged(object sender_, EventArgs eventArgs_)
        {
            SyncHostClip();
        }


        void tileItemViewModel__RequestActivate(object sender_, EventArgs e_)
        {
            ViewModel.IsActive = true;
            foreach (var tileItemViewModel in ViewModel.Parent.TileItems)
            {
                ViewModel.IsActive = (tileItemViewModel == ViewModel);
            }
        }

        protected override void OnPreviewGotKeyboardFocus(KeyboardFocusChangedEventArgs e)
        { 
            base.OnPreviewGotKeyboardFocus(e);
            ViewModel.IsActive = true;
            foreach (var tileItemViewModel in ViewModel.Parent.TileItems)
            {
                ViewModel.IsActive = (tileItemViewModel == ViewModel);
            }
            manager.FlashExecuter.PaneActivated(this);
        }

        protected override void OnLostKeyboardFocus(KeyboardFocusChangedEventArgs e)
        {
            base.OnLostKeyboardFocus(e);
            ViewModel.IsActive = false; 
        }

        public override void OnApplyTemplate()
        { 
            base.OnApplyTemplate();
             
            itemControl = GetTemplateChild(ContentPartName) as ContentControl;
            if (itemControl != null)
            {
                itemControl.Content = content;
            } 
            var titlePart = GetTemplateChild(TitleBarPartName) as PaneHeaderControlBase;
            if (titlePart != null)
            {
                titlePart.ContentObject = this.ViewModel;
            }
            var resizer = GetTemplateChild(ResizerPartName);
            if (resizer != null)
            {
                foreach (var thumb in resizer.FindLogicalChildren<Thumb>())
                {
                    thumb.DragCompleted += ResizeThumbDragCompleted;
                    thumb.DragDelta += ResizeThumbDragDelta;
                    thumb.DragStarted += ResizeThumbDragStarted;
                } 
            }
            var mover = GetTemplateChild(MoverPartName);
            if (mover != null)
            {
                foreach (var thumb in mover.FindLogicalChildren<Thumb>())
                {
                    thumb.DragCompleted += MoveThumbDragCompleted;
                    thumb.DragDelta += MoveThumbDragDelta;
                    thumb.DragStarted += MoveThumbDragStarted;
                }
            }  
        }

        void TileItem_Loaded(object sender_, RoutedEventArgs e_)
        {
            //if (this.IsVisible)
            {   
                ViewModel.DisplayWidth = this.ActualWidth;
                ViewModel.DisplayHeight = this.ActualHeight;
                content.ClearValue(WidthProperty);
                content.ClearValue(HeightProperty); 
                SyncHostSize();
                SyncHostClip();
            } 
            ViewModel.PropertyChanged += (s_, e2_) =>
            {
                if (suspendLayout) return;
                if (e2_.PropertyName == "DisplayLeft")
                {
                    Canvas.SetLeft(this, ViewModel.DisplayLeft);
                    SyncHostClip();
                }
                else if (e2_.PropertyName == "DisplayTop")
                {
                    Canvas.SetTop(this, ViewModel.DisplayTop);
                    SyncHostClip();
                }

            };
        }

        internal void RefreshHorizontally(double changeFactor_)
        {
            ViewModel.Width *= changeFactor_;
            ViewModel.Left *= changeFactor_;
            Canvas.SetLeft(this, ViewModel.DisplayLeft); 
        }
        
        internal void RefreshVertically(double changeFactor_)
        {
            ViewModel.Height *= changeFactor_;
            ViewModel.Top *= changeFactor_;
            Canvas.SetTop(this, ViewModel.DisplayTop); 
        }
        
        void TileItem_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (!suspendLayout && this.IsVisible)
            {
                //SyncHostSize();
                SyncHostClip();
            } 
        }

        private void SyncHostSize()
        { 
            WindowsFormsHost host = content as WindowsFormsHost; 
            if (host == null || itemControl == null || itemControl.ActualHeight <= 0 || itemControl.ActualWidth <= 0) return;
            
            host.Child.Size = new System.Drawing.Size((int)(itemControl.ActualWidth - host.Margin.Left - host.Margin.Right),
(int)(itemControl.ActualHeight - host.Margin.Top - host.Margin.Bottom));
            
        }

        private bool clipped = false;
        internal void SyncHostClip()
        {      
            WindowsFormsHost host = content as WindowsFormsHost;
            if (host == null) return; 
            if (this.ViewModel.IsMinimized)
            {
                WindowsFormIntegrationHelper.ClipWindowsFormsHostAll(host);
                clipped = true;
                return;
            }
 
            //calculate the clip here
            FrameworkElement parentControl = this.ViewModel.Parent.Content as FrameworkElement;
            var hostLocation = host.TranslatePoint(new System.Windows.Point(0, 0), this);
            var location = new System.Windows.Point(Canvas.GetLeft(this), Canvas.GetTop(this));
            location.Offset(hostLocation.X, hostLocation.Y);
            Rect rectChild = new Rect(location.X, location.Y, host.ActualWidth, host.ActualHeight);
            Rect rectParent = new Rect(parentControl.RenderSize);
            if (rectChild.Width <= 0 || rectChild.Height <= 0)
            {
                if (rectParent.Contains(location))
                {
                    //don't clip in this case, since the windows form control is not rendered well at this time
                    Debug.WriteLine(this.ViewModel.Title + ".SkipClip Location:" + location);
                    return;
                }
            }
            rectParent.Intersect(rectChild); 
            Debug.WriteLine(this.ViewModel.Title + ".ClipSize:" + rectParent.Size);
            Debug.WriteLine(this.ViewModel.Title + ".RenderSize:" + host.RenderSize);
            // no clip
            if (CoreUtilities.AreClose(rectChild.Width, rectParent.Width) && 
                CoreUtilities.AreClose(rectChild.Height, rectParent.Height)) 
            {
                if (clipped)
                {
                    WindowsFormIntegrationHelper.UnclipWindowsFormsHost(host); 
                }  
            }
            else if (!rectParent.Size.IsEmpty) // the child is out of screen
            {
                rectParent.Offset(-location.X, -location.Y);
                WindowsFormIntegrationHelper.ClipWindowsFormsHost(host, rectParent);
                clipped = true;
            }   
             
        }
        private void SetBinding()
        {
            this.SetBinding(WidthProperty,
                            new Binding("DisplayWidth") {Mode = BindingMode.TwoWay, Source = ViewModel});
            this.SetBinding(HeightProperty,
                            new Binding("DisplayHeight") {Mode = BindingMode.TwoWay, Source = ViewModel});
            if (content != null)
            { 
                this.SetBinding(MinWidthProperty,
                                new Binding("MinWidth") { Mode = BindingMode.TwoWay, Source = content });
                this.SetBinding(MinHeightProperty,
                                new Binding("MinHeight") { Mode = BindingMode.TwoWay, Source = content });
                this.SetBinding(MaxWidthProperty,
                                new Binding("MaxWidth") { Mode = BindingMode.TwoWay, Source = content });
                this.SetBinding(MaxHeightProperty,
                                new Binding("MaxHeight") { Mode = BindingMode.TwoWay, Source = content });
            } 

        }

        internal TileItemViewModel ViewModel { get; private set; }

        public void Flash()
        {
            (ViewModel.ViewContainer as IWindowViewContainer).Flash();
        }


        private void MoveThumbDragStarted(object sender_, DragStartedEventArgs e_)
        {
            if (TileView.ShowPreview)
            {
                ShowPreviews(new Rect(this.RenderSize));
            }
        }

        private void MoveThumbDragDelta(object sender_, DragDeltaEventArgs e_)
        {
            if (TileView.ShowPreview)
            {
                var rect = new Rect(e_.HorizontalChange, e_.VerticalChange, this.ActualWidth, this.ActualHeight);
                ShowPreviews(rect);
            }
            else
            {
                MoveItem(e_.HorizontalChange, e_.VerticalChange); 
            }

        }

        private void MoveThumbDragCompleted(object sender_, DragCompletedEventArgs e_)
        {
            if (!TileView.ShowPreview) return;
            ClearPreviews();
            MoveItem(e_.HorizontalChange, e_.VerticalChange);
        }

        private Window leftResizePreview;
        private Window topResizePreview;
        private Window rightResizePreview;
        private Window bottomResizePreview; 
        private void ResizeThumbDragStarted(object sender_, DragStartedEventArgs e_)
        {
            if (!TileView.ShowPreview) return;
            SetupPreview((Thumb)sender_);
        }


        private void ResizeThumbDragDelta(object sender_, DragDeltaEventArgs e_)
        { 
            var thumb = sender_ as Thumb; 
            if (TileView.ShowPreview)
            {
                UpdateResizePreview(e_.HorizontalChange, e_.VerticalChange, thumb);
            }
            else
            {
                ResizeItem(e_.HorizontalChange, e_.VerticalChange, thumb);
            }
        }

        private void ResizeThumbDragCompleted(object sender_, DragCompletedEventArgs e_)
        {
            if (!TileView.ShowPreview) return; 
            ResizeItem(e_.HorizontalChange, e_.VerticalChange, (Thumb)sender_);
            ClearPreviews();
        }

        private bool suspendLayout;
        private void MoveItem(double horizontalChange_, double verticalChange_)
        {
            var x = Canvas.GetLeft(this) + horizontalChange_;
            var y = Canvas.GetTop(this) + verticalChange_;
            //if (x < ViewModel.Parent.MarginSize)
            //{
            //    x = ViewModel.Parent.MarginSize;
            //}
            //else
            //{
            //    double width = ViewModel.Parent.GetRoundedWidth(this.ActualWidth, x);
            //    double diff = width - ActualWidth;
            //    x += diff;
            //}
            //if (y < ViewModel.Parent.MarginSize)
            //{
            //    y = ViewModel.Parent.MarginSize;
            //}
            //else
            //{
            //    double height = ViewModel.Parent.GetRoundedHeight(this.ActualHeight, y);
            //    double diff = height - ActualHeight;
            //    y += diff;
            //}
            PerformLayout(delegate
            {
                ViewModel.DisplayLeft = ViewModel.Parent.GetNearestEdge(x);
                ViewModel.DisplayTop = ViewModel.Parent.GetNearestEdge(y);
            }); 

        }

        public void PerformLayout(Action action_)
        {
            suspendLayout = true;
            try
            { 
                action_(); 
                Canvas.SetLeft(this, ViewModel.DisplayLeft);
                Canvas.SetTop(this, ViewModel.DisplayTop);
                SyncHostClip();
            }
            finally
            {
                suspendLayout = false;
            }
        }
 
        private void ResizeItem(double horizontalChange_, double verticalChange_, Thumb thumb_)
        {
            PerformLayout(delegate
                {
                    var value = 0d;
                    switch (thumb_.VerticalAlignment)
                    {
                        case VerticalAlignment.Bottom:
                            value = Math.Max(Math.Min(this.ActualHeight + verticalChange_, this.MaxHeight), this.MinHeight);
                            value = ViewModel.Parent.GetRoundSize(value);
                            var maxHeight = ViewModel.Parent.GetMaxHeight(Canvas.GetTop(this));
                            this.Height = maxHeight > value ? value : maxHeight;
                            break;
                        case VerticalAlignment.Top:
                            value = Math.Max(Math.Min(this.ActualHeight - verticalChange_, this.MaxHeight), this.MinHeight);
                            value = ViewModel.Parent.GetRoundSize(value);
                            var top = Canvas.GetTop(this) - value + this.Height;
                            double diff = top - ViewModel.Parent.MarginSize;
                            if (diff < 0)
                            {
                                value += diff;
                                top = ViewModel.Parent.MarginSize;
                            }
                            this.Height = value;
                            ViewModel.DisplayTop = top;
                            break;
                    }

                    switch (thumb_.HorizontalAlignment)
                    {
                        case HorizontalAlignment.Right:
                            value = Math.Max(Math.Min(this.ActualWidth + horizontalChange_, this.MaxWidth), this.MinWidth);
                            value = ViewModel.Parent.GetRoundSize(value);
                            var maxWidth = ViewModel.Parent.GetMaxWidth(Canvas.GetLeft(this));
                            this.Width = maxWidth > value ? value : maxWidth;
                            break;
                        case HorizontalAlignment.Left:
                            value = Math.Max(Math.Min(this.ActualWidth - horizontalChange_, this.MaxWidth), this.MinWidth);
                            value = ViewModel.Parent.GetRoundSize(value);
                            var left = Canvas.GetLeft(this) - value + Width;
                            double diff = left - ViewModel.Parent.MarginSize;
                            if (diff < 0)
                            {
                                value += diff;
                                left = ViewModel.Parent.MarginSize;
                            }
                            this.Width = value;
                            ViewModel.DisplayLeft = left;
                            break;
                    }

                }); 
            
        }
        #region Preview 
        [Flags]
        private enum Direction
        {
            None = 0,
            Bottom = 2,
            Left = 4,
            Right = 8,
            Top = 1
        }

        private struct ClipResult
        {
            public System.Windows.Point Point;
            public Direction ClipDirection;
        }

        public void ClearPreviews()
        {
            HidePreview(Direction.Left | Direction.Right | Direction.Top | Direction.Bottom); 
        }

        private void SetupPreview(Thumb thumb_)
        {
            double x = 0;
            double y = 0;
            switch (thumb_.VerticalAlignment)
            {
                case VerticalAlignment.Top:
                    ShowPreview(Direction.Top, new System.Windows.Point(0, 0), this.ActualWidth);
                    break;
                case VerticalAlignment.Bottom:
                    y = this.ActualHeight;
                    ShowPreview(Direction.Bottom, new System.Windows.Point(0, this.ActualHeight), this.Width);
                    break;
            }
            switch (thumb_.HorizontalAlignment)
            {
                case HorizontalAlignment.Left:
                    ShowPreview(Direction.Left, new System.Windows.Point(0, 0), this.ActualHeight);
                    break;
                case HorizontalAlignment.Right:
                    x = this.ActualWidth;
                    ShowPreview(Direction.Right, new System.Windows.Point(this.ActualWidth, 0), this.ActualHeight);
                    break;
            } 
        }

        private void UpdateResizePreview(double horizontalChange_, double verticalChange_, Thumb thumb_)
        {
            double left = 0;
            double top = 0;
            double width = this.ActualWidth;
            double height = this.ActualHeight; 
            if (thumb_.VerticalAlignment == VerticalAlignment.Top)
            {
                top += verticalChange_;
                height -= verticalChange_;
                 
            }
            else if (thumb_.VerticalAlignment == VerticalAlignment.Bottom)
            {
                height += verticalChange_; 
            }

            if (thumb_.HorizontalAlignment == HorizontalAlignment.Left)
            {
                left += horizontalChange_;
                width -= horizontalChange_; 
            }
            else if (thumb_.HorizontalAlignment == HorizontalAlignment.Right)
            {
                width += horizontalChange_;
                 
            }
            if (width < 0 || height < 0)
            {
                ClearPreviews();
                return;
            }
            ShowPreviews(new Rect(left, top, width, height)); 
        }

        public void ShowPreviews(Rect region_)
        { 
            ShowPreview(Direction.Left, region_.TopLeft, region_.Height);
            ShowPreview(Direction.Right, region_.TopRight, region_.Height);
            ShowPreview(Direction.Top, region_.TopLeft, region_.Width);
            ShowPreview(Direction.Bottom, region_.BottomLeft, region_.Width);
        }

        public void ShowHorizontalPreviews(double leftDiff_, double rightDiff_)
        {
            ShowPreview(Direction.Top, new System.Windows.Point(leftDiff_, 0), this.ActualWidth - leftDiff_ + rightDiff_);
            ShowPreview(Direction.Bottom, new System.Windows.Point(leftDiff_, this.ActualHeight), this.ActualWidth - leftDiff_ + rightDiff_);

        }

        public void ShowVerticalPreviews(double topDiff_, double bottomDiff_)
        {
            ShowPreview(Direction.Left, new System.Windows.Point(0, topDiff_), this.ActualHeight - topDiff_ + bottomDiff_);
            ShowPreview(Direction.Right, new System.Windows.Point(this.ActualWidth, topDiff_), this.ActualHeight - topDiff_ + bottomDiff_);

        }
        private void ShowPreview(Direction dock_, System.Windows.Point startPoint_, double stretch_)
        {
            var point1 = startPoint_;
            var point2 = new System.Windows.Point();
            Window resizePreview = null;
            switch (dock_)
            {
                case Direction.Left:
                    if (leftResizePreview == null)
                    {
                        leftResizePreview = TileView.CreatePreviewWindow(false); 
                    } 
                    resizePreview = leftResizePreview; 
                    point2 = new System.Windows.Point(startPoint_.X, startPoint_.Y + stretch_); 
                    break;
                case Direction.Top:
                    if (topResizePreview == null)
                    {
                        topResizePreview = TileView.CreatePreviewWindow(true); 
                    } 
                    resizePreview = topResizePreview; 
                    point2 = new System.Windows.Point(startPoint_.X + stretch_, startPoint_.Y);  
                    break;
                case Direction.Right:
                    if (rightResizePreview == null)
                    {
                        rightResizePreview = TileView.CreatePreviewWindow(false); 
                    }
                    resizePreview = rightResizePreview;
                    point1.Offset(-rightResizePreview.Width, 0); 
                    point2 = new System.Windows.Point(startPoint_.X, startPoint_.Y + stretch_);  
                    break;
                case Direction.Bottom:
                    if (bottomResizePreview == null)
                    {
                        bottomResizePreview = TileView.CreatePreviewWindow(true); 
                    }
                    resizePreview = bottomResizePreview; 
                    point1.Offset(0, -bottomResizePreview.Height); 
                    point2 = new System.Windows.Point(startPoint_.X + stretch_, startPoint_.Y); 
                    break;
            }
            
            if (resizePreview == null) return;

            var clipResult1 = AdjustPoint(point1); 
            var clipResult2 = AdjustPoint(point2);
            if (clipResult1.ClipDirection.HasFlag(dock_) &&
                clipResult2.ClipDirection.HasFlag(dock_))
            {
                HidePreview(dock_);
                return;
            }
            point1 = clipResult1.Point;
            point2 = clipResult2.Point;
            var abolutePoint = this.PointToScreen(point1);
            resizePreview.Left = abolutePoint.X;
            resizePreview.Top = abolutePoint.Y; 
            var height = point2.Y - point1.Y;
            var width = point2.X - point1.X;
            if (dock_ == Direction.Left || dock_ == Direction.Right)
            {
                if (height <= 0)
                {
                    HidePreview(dock_);
                    return;
                } 
                resizePreview.Height = height;
                if (!resizePreview.IsVisible)
                {
                    resizePreview.Show();
                } 
            }
            else
            {
                if (width <= 0)
                {
                    HidePreview(dock_);
                    return;
                } 
                resizePreview.Width = width;
                if (!resizePreview.IsVisible)
                {
                    resizePreview.Show();
                }
            } 

        }
         

        private void HidePreview(Direction dock_)
        {
            if ((dock_ & Direction.Left) == Direction.Left && leftResizePreview != null)
            {
                leftResizePreview.Hide(); 
            }
            if ((dock_ & Direction.Right) == Direction.Right && rightResizePreview != null)
            {
                rightResizePreview.Hide();
            }
            if ((dock_ & Direction.Top) == Direction.Top && topResizePreview != null)
            { 
                topResizePreview.Hide();
            }
            if ((dock_ & Direction.Bottom) == Direction.Bottom && bottomResizePreview != null)
            { 
                bottomResizePreview.Hide();
            } 
        }


        private ClipResult AdjustPoint(System.Windows.Point point_)
        {
            FrameworkElement parentControl = (FrameworkElement)this.ViewModel.Parent.Content;
            System.Windows.Point location = this.TranslatePoint(point_, parentControl);
            var result = new ClipResult();
            if (location.X < 0)
            {
                point_.Offset(0 - location.X, 0);
                result.ClipDirection |= Direction.Left;
            }
            else if (location.X > parentControl.ActualWidth)
            {
                point_.Offset(parentControl.ActualWidth - location.X, 0);
                result.ClipDirection |= Direction.Right;
            }
            if (location.Y < 0)
            {
                point_.Offset(0, 0 - location.Y);
                result.ClipDirection |= Direction.Top;
            }
            else if (location.Y > parentControl.ActualHeight)
            {
                point_.Offset(0, parentControl.ActualHeight - location.Y);
                result.ClipDirection |= Direction.Bottom;
            }

            result.Point = point_;
            return result;
        }



        #endregion

        #region  CUIT
        protected override System.Windows.Automation.Peers.AutomationPeer OnCreateAutomationPeer()
        {
            var inner = base.OnCreateAutomationPeer();
            var peer = new TileItemAutomationPeer(inner, this);
            return peer;
        }

        private class TileItemAutomationPeer : RecordImageAutomationPeerWrapper
        {
            private const string TileItemAutomationIdPrefix = "TileItem_";

            public TileItemAutomationPeer(AutomationPeer inner, FrameworkElement owner)
                : base(inner, owner)
            {
                RecordImage();
            }

            private TileItem MyOwner { get { return Owner as TileItem; } }

            protected override string GetClassNameCore()
            {
                return typeof(TileItem).Name;
            }

            protected override string GetAutomationIdCore()
            {
                var dc = MyOwner.DataContext as TileItemViewModel;
                if (dc == null)
                    return TileItemAutomationIdPrefix;

                return TileItemAutomationIdPrefix + dc.ViewContainer.ID;
            }

        }
        #endregion


        IWindowViewContainer IWindowViewContainerHost.View
        {
            get { return this.ViewModel.ViewContainer as IWindowViewContainer; }
        }

        IFloatingWindow IWindowViewContainerHost.FloatingWindow
        {
            get
            {
                var pRoot = this.Parent;
                while (pRoot != null && !(pRoot.GetParentObject() is IWindowViewContainerHost))
                {
                    pRoot = pRoot.GetParentObject();
                }

                var host = pRoot as IWindowViewContainerHost;
                return host == null ? null : host.FloatingWindow;
            }
        }

    }
}
