﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel; 
using System.Linq; 
using System.Windows; 
using System.Windows.Input;
using System.Xml.Linq; 
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence; 
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.ViewModels;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions.Flash;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
  
    internal class TileItemCollectionModel : ViewModelBase, ILayoutSaveLoadRequester, ITileItemManager, IWidgetViewContainer
    {
        private readonly ChromeRegistry chromeRegistry; 
        private readonly IUnityContainer container;
        private readonly List<TileViewModel> tileViews = new List<TileViewModel>();
        private readonly IHidingLockingManager hidingLockingManager;
        private ObservableCollection<TileItemViewModel> workspaces;
        private readonly InitialSettings initialSettings;
        private readonly IWindowManager windowManager;
        internal readonly TileItemFlashExecuter FlashExecuter = new TileItemFlashExecuter();

        public TileItemCollectionModel(ChromeRegistry chromeRegistry_, IUnityContainer container_, IHidingLockingManager hidingLockingManager_, 
            IWindowManager windowManager_, InitialSettings initialSettings_)
        {
            this.initialSettings = initialSettings_;
            this.chromeRegistry = chromeRegistry_;
            this.container = container_;
            this.hidingLockingManager = hidingLockingManager_;
            this.windowManager = windowManager_;
            chromeRegistry_.RegisterWindowFactoryMapping<InitialTileParameters>(InitializeTileView);
            chromeRegistry_.RegisterWindowFactoryMapping<InitialTileItemParameters>(InitializeTileItem);
            FlashExecuter.Start();
        }
        	   
        internal static double? InitialViewSpacingSize { get; set; }

	    internal static bool FlatModeEnabledProperty;
        private const string TileViewBarId = "TileViewBarId";
        [FactoryOptions(typeof(TileViewContainer))]
        public bool InitializeTileView(IWindowViewContainer emptyViewContainer_, XDocument state_)
        {
            TileViewContainer tileViewContainer = emptyViewContainer_ as TileViewContainer;
            if (string.IsNullOrEmpty(tileViewContainer.Title) && 
                !string.IsNullOrEmpty(tileViewContainer.Parameters.WorkspaceCategory))
            {
                tileViewContainer.Title = tileViewContainer.Parameters.WorkspaceCategory;
            }
            TileViewModel model = this.AddTileView(tileViewContainer);
            TileView view = new TileView(model, this, container.Resolve<IChromeManager>());
            string id = TileViewBarId + Guid.NewGuid().ToString();
            chromeRegistry.RegisterWidgetFactory(id, (container2_, state2_) =>
            {
                container2_.Content = new TileMenuBar(model);
                return true;
            });
            emptyViewContainer_.Header.AddWidget(id, new InitialWidgetParameters());
            emptyViewContainer_.Content = view; 
            return true;
        }

        [FactoryOptions(typeof (TileItemViewContainer))]
        public bool InitializeTileItem(IWindowViewContainer emptyViewContainer_, XDocument state_)
        {
            return true;
        }

        static TileItemCollectionModel()
        {
            FlatModeEnabledProperty = false;
        }

	    public bool IsFlatModeEnabled
	    {
	        get
	        {
	            return FlatModeEnabledProperty;
	        }
	    }

	    protected IHidingLockingManager HidingLockingManager
	    {
	        get
	        {
                return hidingLockingManager;
	        }
	    }
        protected override void OnDispose()
        {
            FlashExecuter.Stop();
        }

        public ObservableCollection<TileItemViewModel> Workspaces
        {
            get
            {
                if (workspaces == null)
                {
                    workspaces = new ObservableCollection<TileItemViewModel>(); 
                }
                return workspaces;
            }
        }

      

        private KeyGesture tileItemRenameGesture;
        public KeyGesture TileItemRenameGesture
        {
            get
            {
                return tileItemRenameGesture;
            }
            set
            {
                tileItemRenameGesture = value;
                OnPropertyChanged("TileItemRenameGesture");
            }
        }

        private bool enforceSizeRestrictions = true;
        public bool EnforceSizeRestrictions
        {
            get
            {
                return enforceSizeRestrictions;
            }
            set
            {
                enforceSizeRestrictions = value;
                OnPropertyChanged("EnforceSizeRestrictions");
            }
        }

        
        RelayCommand closingCommand;
        public ICommand TileItemClosingCommand
        {
            get
            {
                return closingCommand ?? (closingCommand = new RelayCommand(OnClose, CanClose));
            }
        }

        private bool CanClose(object param_)
        {
            if (param_ is TileItemViewModel)
            {
                TileItemViewModel tileItem = (TileItemViewModel)param_;
                return tileItem.TryClosing();
            }
            return true;
        }

        private void OnClose(object param_)
        {
            WindowViewModel tileItem = param_ as WindowViewModel;
            if (tileItem == null) return;
            tileItem.IsBeingClosed = true;
            tileItem.InvokeClosed();
        }

        RelayCommand closedCommand;
        public ICommand TileItemClosedCommand
        {
            get
            {
                return closedCommand ?? (closedCommand = new RelayCommand(OnClosed));
            }
        }

        private void OnClosed(object param_)
        {
            TileItemViewModel tileItem = param_ as TileItemViewModel;
            if (tileItem == null) return;
            var contains = Workspaces.FirstOrDefault((wp) => wp.ID == tileItem.ID);
            if (contains != null)
            {
                Workspaces.Remove(contains);
            }
            tileItem.Parent.RemoveTileItem(tileItem);
            tileItem.InvokeClosed();
        }

        public event RequestSaveEventHandler SaveRequested;

        public virtual XDocument SaveLayout()
        {
            var copy = SaveRequested;
            if (copy != null)
            {
                SaveEventArgs args = new SaveEventArgs();
                copy(args);
                return args.SavedState;
            }
            return null;
        }

        public event RequestLoadEventHandler LoadRequested;

        public virtual void LoadLayout(XDocument state_)
        {
            var copy = LoadRequested;
            if (copy != null)
            {
                copy(new LoadEventArgs(state_));
            }
        }
         
   
        #region Implementation of IWidgetViewContainer
        public InitialWidgetParameters Parameters
        {
            get { return new InitialWidgetParameters(); }
        }

        public IWidgetViewContainer Parent
        {
            get { return null; }
        }

        public IWidgetViewContainer this[string index_]
        {
            get { return null; }
        }

        public bool Contains(string index_)
        {
            return false;
        }

        public object Content
        {
            get
            {
                return null;
            }
            set
            { 
            }
        }

        public event EventHandler ContentReady;

        public string FactoryID
        {
            get { return string.Empty; }
        }

        InitialViewParameters IViewContainerBase.Parameters
        {
            get { return new InitialWidgetParameters(); }
        }
        #endregion 
  
        public List<TileViewModel> TileViews
        {
            get { return tileViews; }
        }

        #region Implementation of ITileItemManager


        public TileViewModel AddTileView(TileViewContainer tileContainer_)
        {
            TileViewModel tileViewModel = tileContainer_.GetViewModel(this.HidingLockingManager) as TileViewModel;
            this.tileViews.Add(tileViewModel);
            EventHandler handler = null;
            handler = (s_, e_) =>
            { 
                tileViewModel.Closed -= handler;
                tileViewModel.ClosedQuietly -= handler;
                for (int i = tileViewModel.TileItems.Count - 1; i >= 0; i--)
                {
                    tileViewModel.TileItems[i].Close();
                }
                this.TileViews.Remove(tileViewModel);

            };
            tileViewModel.Closed += handler;
            tileViewModel.ClosedQuietly += handler;
            return tileViewModel;
        }
         
        public void AddNewItem(TileItemViewContainer viewViewContainer_, TileViewModel hostTile_)
        {
            var wsfound = Workspaces.FirstOrDefault(ws_ => ws_.ID == viewViewContainer_.ID);
            if (wsfound != null) return;
            var hostTile = hostTile_;
            InitialTileItemParameters parameters = viewViewContainer_.Parameters as InitialTileItemParameters;
            if (hostTile == null)
            {
                IWindowViewContainer tile = parameters.InitialLocationTarget;
                if (tile != null)
                {
                    hostTile = TileViews.FirstOrDefault(t_ => t_.ViewContainer == tile);
                }
            }
            if (hostTile == null) throw new ArgumentOutOfRangeException("hostTile must be specified");
            TileItemViewModel tileItemViewModel = new TileItemViewModel(viewViewContainer_, HidingLockingManager, hostTile)
            {
                InitialParameters = parameters,
                WindowRenameGesture = initialSettings.WindowRenameGesture
            };
            hostTile.AddTileItem(tileItemViewModel);
            Workspaces.Add(tileItemViewModel);
            EventHandler handler = null;
            handler = (sender_, args_) =>
                {
                    tileItemViewModel.RequestClose -= handler; 
                    TileItemClosedCommand.Execute(tileItemViewModel);

                };
            tileItemViewModel.RequestClose += handler;
        }
         

        public void Clean()
        {
            foreach (TileItemViewModel tileItemViewModel in new List<TileItemViewModel>(Workspaces))
            {
                tileItemViewModel.Close();
            }
            this.tileViews.Clear();
        }

        public const string NODE_NAME = "TileViews";

        public XDocument SaveState(Func<TileItemViewContainer, XElement> buildIViewXml_)
        {
            XDocument documentWithSubitems = new XDocument();

            XElementWithSubitems rootElement = new XElementWithSubitems(NODE_NAME);
            foreach (var tileViewModel in tileViews)
            {
                XElement tileViewElement = new XElement("TileView");
                rootElement.Add(tileViewElement);
                tileViewElement.SetAttributeValue("ViewId", tileViewModel.ViewContainer.ID);
                tileViewElement.SetAttributeValue("Title", tileViewModel.Title);
                //tileViewElement.SetAttributeValue("Left", tileViewModel.Left);
                //tileViewElement.SetAttributeValue("Top", tileViewModel.Top);
                tileViewElement.SetAttributeValue("Width", tileViewModel.Width);
                tileViewElement.SetAttributeValue("Height", tileViewModel.Height);
                tileViewElement.SetAttributeValue("Zoom", tileViewModel.Zoom);
                XElement itemsElement = new XElement("TileItems");
                tileViewElement.Add(itemsElement);
                foreach (var tileItem in tileViewModel.TileItems)
                {
                    if (tileItem.IsEditing)
                    {
                        tileItem.RecordEdgeMargin();
                    }
                    XElement viewElement = buildIViewXml_(tileItem.ViewContainer as TileItemViewContainer);
                    itemsElement.Add(viewElement);
                    rootElement.Subitems[tileItem.ID] = viewElement;

                }
            }
            documentWithSubitems.Add(rootElement);
            return documentWithSubitems;
        }

        public XElement SaveState(TileViewContainer tileViewContainer_, Func<TileItemViewContainer, XElement> buildIViewXml_)
        {
            TileViewModel tileView = this.tileViews.FirstOrDefault(t_ => t_.ViewContainer == tileViewContainer_);
            if (tileView == null) return null;

            XElement tileViewElement = new XElement("TileView");
            tileViewElement.SetAttributeValue("ViewId", tileView.ViewContainer.ID);
            tileViewElement.SetAttributeValue("Title", tileView.Title);
            //tileViewElement.SetAttributeValue("Left", tileView.Left);
            //tileViewElement.SetAttributeValue("Top", tileView.Top);
            tileViewElement.SetAttributeValue("Width", tileView.Width);
            tileViewElement.SetAttributeValue("Height", tileView.Height);
            tileViewElement.SetAttributeValue("Zoom", tileView.Zoom);
            XElement itemsElement = new XElement("TileItems");
            tileViewElement.Add(itemsElement);
            foreach (var tileItem in tileView.TileItems)
            {
                if (tileItem.IsEditing)
                {
                    tileItem.RecordEdgeMargin();
                }
                XElement viewElement = buildIViewXml_(tileItem.ViewContainer as TileItemViewContainer);
                itemsElement.Add(viewElement); 

            }
            return tileViewElement;
        } 

        public void BeforeSaveState()
        {
        }

        public void AfterSaveState()
        {
        }
        private volatile bool cancelLayoutLoad; 
  
        public void CancelLoad(bool cancelLayoutRestore_)
        {
            cancelLayoutLoad = true; 
        }

        public void LoadState(XDocument layout_, Action<XDocument, TileViewModel> applyNodeAction_)
        {
            if (this.TileViews.Count == 0) return;
            XElement mainElement = layout_.Element(NODE_NAME);
            if (mainElement == null) return;
             
            try
            {
                IEnumerable<XElement> tileViewsElement = mainElement.Elements("TileView");

                foreach (XElement tileViewNode in tileViewsElement)
                {
                    if (cancelLayoutLoad) return;
                    var tileItemsElement = tileViewNode.Element("TileItems");
                    if (tileItemsElement == null) continue;

                    var viewIdAttr = tileViewNode.Attribute("ViewId");
                    if (viewIdAttr != null && !string.IsNullOrEmpty(viewIdAttr.Value))
                    {
                        TileViewModel tileView = tileViews.FirstOrDefault(v_ => v_.ViewContainer.ID == viewIdAttr.Value);
                        if (tileView != null)
                        {
                            var zoomAttr = tileViewNode.Attribute("Zoom");
                            if (zoomAttr != null && !string.IsNullOrEmpty(zoomAttr.Value))
                            {
                                double zoom;
                                if (double.TryParse(zoomAttr.Value, out zoom))
                                {
                                    tileView.Zoom = zoom;
                                }
                            }  
                            if (windowManager is FloatingOnlyDockViewModel)
                            {
                                var heightAttr = tileViewNode.Attribute("Height");
                                if (heightAttr != null && !string.IsNullOrEmpty(heightAttr.Value))
                                {
                                    double height;
                                    if (double.TryParse(heightAttr.Value, out height))
                                    {
                                        tileView.Height = height;
                                    }
                                }

                                var widthAttr = tileViewNode.Attribute("Width");
                                if (widthAttr != null && !string.IsNullOrEmpty(widthAttr.Value))
                                {
                                    double width;
                                    if (double.TryParse(widthAttr.Value, out width))
                                    {
                                        tileView.Width = width;
                                    }
                                }
                            }
                            TileViewContainer tileViewContainer = (TileViewContainer)tileView.ViewContainer;
                            tileViewContainer.ClearChangeFactors();
                            foreach (XElement viewNode in tileItemsElement.Elements("IView"))
                            {
                                applyNodeAction_(new XDocument(viewNode), tileView);
                            }
                            tileView.IsFloating = DockHelper.GetRootOfFloatingPane(tileView.ViewContainer as IWindowViewContainer) != null; 
                            tileView.ResizeEdgeItems();  
                        }
                    }

                } 
            }
            finally
            { 
                cancelLayoutLoad = false;
            } 
        }
        public void LoadState(XElement  tileViewNode_, Action<XDocument, TileViewModel> applyNodeAction_)
        {
            if (this.TileViews.Count == 0) return;
            var tileItemsElement = tileViewNode_.Element("TileItems");
            if (tileItemsElement == null) return;

            var viewIdAttr = tileViewNode_.Attribute("ViewId");
            if (viewIdAttr != null && !string.IsNullOrEmpty(viewIdAttr.Value))
            {
                TileViewModel tileView = tileViews.FirstOrDefault(v_ => v_.ViewContainer.ID == viewIdAttr.Value);
                if (tileView != null)
                {
                    TileViewContainer tileViewContainer = (TileViewContainer)tileView.ViewContainer;
                    tileViewContainer.ClearChangeFactors();
                    var zoomAttr = tileViewNode_.Attribute("Zoom");
                    if (zoomAttr != null && !string.IsNullOrEmpty(zoomAttr.Value))
                    {
                        double zoom;
                        if (double.TryParse(zoomAttr.Value, out zoom))
                        {
                            tileView.Zoom = zoom;
                        }
                    }
                    foreach (XElement viewNode in tileItemsElement.Elements("IView"))
                    {
                        applyNodeAction_(new XDocument(viewNode), tileView);
                    }
                    tileView.IsFloating = DockHelper.GetRootOfFloatingPane(tileView.ViewContainer as IWindowViewContainer) != null;
                    tileView.ResizeEdgeItems();  
                }
            }

        }

        public void BeforeLoadState()
        {
        }

        public void AfterLoadState()
        {
        }

        
        #endregion


    }
}
