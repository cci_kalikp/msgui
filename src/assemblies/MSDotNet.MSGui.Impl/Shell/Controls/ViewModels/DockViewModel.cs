﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Controls/ViewModels/DockViewModel.cs#38 $
// $Change: 902487 $
// $DateTime: 2014/10/27 03:05:30 $
// $Author: caijin $

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Xml.Linq;
using System.Linq; 
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Controls.Controls;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.DockManager;
using MorganStanley.MSDotNet.MSGui.Impl.DockManager.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions; 
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions.MVVMDock;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
    internal class DockViewModel : ViewModelBase, ILayoutSaveLoadRequester, IGlobalDockInfoProvider
	{
	    private readonly IHidingLockingManager m_hidingLockingManager;
        protected readonly IUnityContainer m_container;
	    protected readonly InitialSettings GlobalSettings;
	    internal static bool UseHarmonia = false;
        internal static double? InitialViewSpacingSize { get; set; }
        internal static bool EnableNonQuietCloseOfViews { get; set; }

        internal static bool FlatModeEnabledProperty;

	    internal static bool TrackLayoutLoadStatus;

        internal static bool EnableTopmost;
        internal static bool FireDragDropEvents = false;
        internal static bool RenameDisabled = false; 
        static DockViewModel()
        {
            FlatModeEnabledProperty = false;
        }

	    public Func<string[], string> ToolWindowTitleGenerator
	    {
	        get
	        {
	            return m_container.Resolve<InitialSettings>().CustomToolWindowTitleGenerator;
	        }
	    }

	    public bool IsFlatModeEnabled
	    {
	        get
	        {
	            return FlatModeEnabledProperty;
	        }
	    }

	    public double WindowResizeAreaThickness
	    {
	        get
	        {
	            return FlatModeEnabledProperty ? 3.0 : 8.0;
	        }
	    } 
         
        public static double BaseDefaultFloatingWindowContentVerticalMargin
        {
            get
            {
                if (UseHarmonia)
                {
                    return (PaneContainer.ContainerBorderThickness.Top + PaneContainer.ContainerBorderThickness.Bottom)*
                           3 +
                           (FlatModeEnabledProperty
                                ? (ThemeExtensions.ContentPaneFlatModeMargin.Top +
                                   ThemeExtensions.ContentPaneFlatModeMargin.Bottom +
                                   ThemeExtensions.ContentPaneFlatModeFlashBorderMargin.Top +
                                   ThemeExtensions.ContentPaneFlatModeFlashBorderMargin.Bottom)
                                : (ThemeExtensions.ContentPaneMargin.Top +
                                   ThemeExtensions.ContentPaneMargin.Bottom +
                                   ThemeExtensions.ContentPaneFlashBorderMargin.Top +
                                   ThemeExtensions.ContentPaneFlashBorderMargin.Bottom)) +
                           PaneContainer.PaneHeaderHeightDefault + 
                            ContentPaneWrapperHost.HeaderPanelMargin.Top + ContentPaneWrapperHost.HeaderPanelMargin.Bottom + 
                           ContentPaneWrapperHost.HostBorderThickness.Bottom +
                            ContentPaneWrapperHost.HostPadding.Top + ContentPaneWrapperHost.HostPadding.Bottom +
                            WindowViewModel.StaticContentPaddingResolved.Top + WindowViewModel.StaticContentPaddingResolved.Bottom;   
                }
                else
                {
                    return ThemeExtensions.FloatingWindowBorderBorderThickness.Top +
       ThemeExtensions.FloatingWindowBorderBorderThickness.Bottom +
       ThemeExtensions.FloatingWindowBorderMargin.Top + ThemeExtensions.FloatingWindowBorderMargin.Bottom +
       (FlatModeEnabledProperty
            ? (ThemeExtensions.FloatingWindowFlatModeBorderPadding.Top +
               ThemeExtensions.FloatingWindowFlatModeBorderPadding.Bottom +
               ThemeExtensions.ContentPaneFlatModeMargin.Top +
               ThemeExtensions.ContentPaneFlatModeMargin.Bottom +
               ThemeExtensions.ContentPaneFlatModeFlashBorderMargin.Top +
               ThemeExtensions.ContentPaneFlatModeFlashBorderMargin.Bottom)
            : (ThemeExtensions.FloatingWindowBorderPadding.Top +
               ThemeExtensions.FloatingWindowBorderPadding.Bottom +
               ThemeExtensions.ContentPaneMargin.Top +
               ThemeExtensions.ContentPaneMargin.Bottom +
               ThemeExtensions.ContentPaneFlashBorderMargin.Top +
               ThemeExtensions.ContentPaneFlashBorderMargin.Bottom)) +
                        //ThemeExtensions.FloatingWindowHeaderSeperatorHeight +
        WindowViewModel.StaticContentPaddingResolved.Top + WindowViewModel.StaticContentPaddingResolved.Bottom +
        ThemeExtensions.HeaderHeightFloatingDefault + ThemeExtensions.FloatingWindowCaptionBarMargin.Top + ThemeExtensions.FloatingWindowCaptionBarMargin.Bottom ;

                }
 
            }
        }

        public static double BaseDefaultFloatingWindowContentHorizontalMargin
        {
            get
            {
                if (UseHarmonia)
                {
                    return (PaneContainer.ContainerBorderThickness.Left + PaneContainer.ContainerBorderThickness.Right)* 3 +
                           (FlatModeEnabledProperty
                                ? (ThemeExtensions.ContentPaneFlatModeMargin.Left +
                                   ThemeExtensions.ContentPaneFlatModeMargin.Right +
                                   ThemeExtensions.ContentPaneFlatModeFlashBorderMargin.Left +
                                   ThemeExtensions.ContentPaneFlatModeFlashBorderMargin.Right)
                                : (ThemeExtensions.ContentPaneMargin.Left +
                                   ThemeExtensions.ContentPaneMargin.Right +
                                   ThemeExtensions.ContentPaneFlashBorderMargin.Left +
                                   ThemeExtensions.ContentPaneFlashBorderMargin.Right))  +
                           ContentPaneWrapperHost.HostBorderThickness.Left + ContentPaneWrapperHost.HostBorderThickness.Right +
                           ContentPaneWrapperHost.HostPadding.Left + ContentPaneWrapperHost.HostPadding.Right +
                            WindowViewModel.StaticContentPaddingResolved.Left + WindowViewModel.StaticContentPaddingResolved.Right;  
                }
                else
                {
                    return ThemeExtensions.FloatingWindowBorderBorderThickness.Left +
       ThemeExtensions.FloatingWindowBorderBorderThickness.Right +
       ThemeExtensions.FloatingWindowBorderMargin.Left + ThemeExtensions.FloatingWindowBorderMargin.Right +
       (FlatModeEnabledProperty
            ? (ThemeExtensions.FloatingWindowFlatModeBorderPadding.Left +
               ThemeExtensions.FloatingWindowFlatModeBorderPadding.Right +
               ThemeExtensions.ContentPaneFlatModeMargin.Left +
               ThemeExtensions.ContentPaneFlatModeFlashBorderMargin.Right +
               ThemeExtensions.ContentPaneFlatModeFlashBorderMargin.Left +
               ThemeExtensions.ContentPaneFlatModeFlashBorderMargin.Right)
            : (ThemeExtensions.FloatingWindowBorderPadding.Left +
               ThemeExtensions.FloatingWindowBorderPadding.Right +
               ThemeExtensions.ContentPaneMargin.Left +
               ThemeExtensions.ContentPaneMargin.Right +
               ThemeExtensions.ContentPaneFlashBorderMargin.Left +
               ThemeExtensions.ContentPaneFlashBorderMargin.Right)) +
        WindowViewModel.StaticContentPaddingResolved.Left + WindowViewModel.StaticContentPaddingResolved.Right;
                }

 
            }
        }
	    public TextTrimming TitleTextTrimming
	    {
	        get
	        {
	            return GlobalSettings.ViewTitleTextTrimming;
	        }
	    }

	    internal IHidingLockingManager HidingLockingManager
	    {
	        get
	        {
	            return m_hidingLockingManager;
	        }
	    }

        internal static string DockLayoutRootName
        {
            get
            {
                return DockViewModel.UseHarmonia ? LayoutConverter.HarmoniaDockLayoutRootName
                                   : LayoutConverter.DockLayoutRootName;
            }
        }

	    public DockViewModel(IHidingLockingManager hidingLockingManager, IUnityContainer container)
		{
		    m_hidingLockingManager = hidingLockingManager;  
	        SpacingSize = InitialViewSpacingSize;
	        m_hidingLockingManager.HideUIGranularityChanged += (sender_, args_) =>
	            {
                    SpacingSize = m_hidingLockingManager.Granularity.HasFlag(HideLockUIGranularity.DisableResizingViews) ? 0 : InitialViewSpacingSize;
	            };
	        m_container = container;
	        GlobalSettings = container.Resolve<InitialSettings>();
	        PaneFactory.ActivePaneChanged += OnActivePaneChanged;
		}


        private void OnActivePaneChanged(object sender, RoutedPropertyChangedEventArgs<UIElement> e)
        {
            var copy = ActivePaneChanged;
            if (copy != null)
            {
                copy(sender,
                     new WindowManagerActiveChangedEventArgs(
                         e.OldValue == null ? null : Attached.GetViewContainer(e.OldValue),
                         e.NewValue == null ? null : Attached.GetViewContainer(e.NewValue)));
            }
        } 

        protected void OnActivePaneChanging(object sender, UIElement oldValue, UIElement newValue, out bool cancel)
        {
            var copy = ActivePaneChanging;
            if (copy != null)
            {
                var e =
                    new WindowManagerActiveChangingEventArgs(
                        oldValue == null ? null : Attached.GetViewContainer(oldValue),
                        newValue == null ? null : Attached.GetViewContainer(newValue));
                copy(sender, e);
                cancel = e.Cancel;
            }
            else
            {
                cancel = false;
            }
        }
        public event EventHandler<WindowManagerActiveChangedEventArgs> ActivePaneChanged;
        public event EventHandler<WindowManagerActiveChangingEventArgs> ActivePaneChanging;
	    protected override void OnDispose()
		{
			if (m_workspaces != null)
			{
				m_workspaces.CollectionChanged -= OnWorkspacesChanged;
			} 
		}

		WindowCollection m_workspaces;
		public WindowCollection Workspaces
		{
			get
			{
				if (m_workspaces == null)
				{
					m_workspaces = new WindowCollection();
					m_workspaces.CollectionChanged += OnWorkspacesChanged;
				}
				return m_workspaces;
			}
		}

		private void OnWorkspacesChanged(object sender_, NotifyCollectionChangedEventArgs e_)
		{
			if (e_.NewItems != null && e_.NewItems.Count != 0)
			{
				foreach (WindowViewModel workspace in e_.NewItems)
				{

				}
			}

			if (e_.OldItems != null && e_.OldItems.Count != 0)
			{
				foreach (WindowViewModel workspace in e_.OldItems)
				{

				}
			}
		}

		public void AddNewView(WindowViewContainer viewContainer_)
		{
			Workspaces.Add(viewContainer_.GetViewModel(m_hidingLockingManager));
		}

		private KeyGesture m_windowRenameGesture;
		public KeyGesture WindowRenameGesture
		{
			get
			{
				return m_windowRenameGesture;
			}
			set
			{
				m_windowRenameGesture = value;
				OnPropertyChanged("WindowRenameGesture");
			}
		}

		private bool m_enforceSizeRestrictions = true;
		public bool EnforceSizeRestrictions
		{
			get
			{
				return m_enforceSizeRestrictions;
			}
			set
			{
				m_enforceSizeRestrictions = value;
				OnPropertyChanged("EnforceSizeRestrictions");
			}
		}

		private bool m_useOwnedWindows = true;
		public bool UseOwnedWindows
		{
			get
			{
				return m_useOwnedWindows;
			}
			set
			{
				m_useOwnedWindows = value;
				OnPropertyChanged("UseOwnedWindows");
			}
		}

		RelayCommand m_closingCommand;
		public ICommand WindowClosingCommand
		{
			get
			{
				return m_closingCommand ?? (m_closingCommand = new RelayCommand(OnClose, CanClose));
			}
		}

		private bool CanClose(object param_)
		{
			if (param_ is WindowViewModel)
			{
				WindowViewModel window = (WindowViewModel)param_;
				return window.TryClosing();
			}
			return true;
		}

		private void OnClose(object param_)
		{
			if (param_ is WindowViewModel)
			{
				WindowViewModel window = (WindowViewModel)param_;
			    window.IsBeingClosed = true;
				window.InvokeClosed();
			}
		}

		RelayCommand m_closedCommand;
		public ICommand WindowClosedCommand
		{
			get
			{
				return m_closedCommand ?? (m_closedCommand = new RelayCommand(OnClosed));
			}
		}

		private void OnClosed(object param_)
		{
            WindowViewModel window = param_ as WindowViewModel;
            if (window == null) return;
            var contains = Workspaces.FirstOrDefault((wp) => wp.ID == window.ID);
            if (contains != null)
            {
                Workspaces.Remove(contains);
            }
            window.Dispose();
		}

		public event RequestSaveEventHandler SaveRequested;

		public virtual XDocument SaveLayout()
		{
			var copy = SaveRequested;
			if (copy != null)
			{
				SaveEventArgs args = new SaveEventArgs();
				copy(args);
				return args.SavedState;
			}
			return null;
		}

		public event RequestLoadEventHandler LoadRequested;

        public virtual void LoadLayout(XDocument state_)
		{
			var copy = LoadRequested;
			if (copy != null)
			{
				copy(new LoadEventArgs(state_));
			}
		}

        public event EventHandler<GenericDragOrDropOnWindowEventArgs> DragOrDropOnWindow;
        public void InvokeDragOrDropOnWindow(GenericDragOrDropOnWindowEventArgs e_)
        {
            EventHandler<GenericDragOrDropOnWindowEventArgs> handler = DragOrDropOnWindow;
            if (handler != null) handler(this, e_);
        }

		private Window m_mainWindow;
        private EnvironmentLabelViewModel _environment;
	    private double? _spacingSize;

	    public Window MainWindow
		{
			get
			{
				return m_mainWindow;
			}
			set
			{
				m_mainWindow = value;
				OnPropertyChanged("MainWindow");
			}
		}

		public IWindowViewContainer CreateDialog()
		{
			return new DialogWindow(m_useOwnedWindows ? MainWindow : null);
		}

	    public static Dock TabGroupTabStripPlacement = Dock.Bottom;

        public Dock TabStripPlacement
        {
            get { return TabGroupTabStripPlacement; }
        }

        public double? SpacingSize
        {
            get
            {
                return _spacingSize ?? 4;
            }
            set
            {
                _spacingSize = value;
                OnPropertyChanged("SpacingSize");
            }
        }

	    public event EventHandler<CrossProcessDragDropEventArgs> CrossProcessDragDropAction;

        internal void OnCrossProcessDragDropAction(IFloatingWindow window_, int processId_, CrossProcessDragDropAction action_)
        {
            var handler = CrossProcessDragDropAction;
            if (handler != null) handler(this, new CrossProcessDragDropEventArgs
            {
                ProcessId = processId_,
                Action = action_,
                FloatingWindow = window_
            });
        }

	    public EnvironmentLabelViewModel EnvironmentLabelViewModel
	    {
	        get
	        {
	            return _environment;
	        }
	        set
	        {
	            _environment = value;
                OnPropertyChanged("EnvironmentLabelViewModel");
	        }
	    }

        public bool EnvironmentSpecified
        {
            get
            {
               return EnvironmentLabelViewModel != null &&
                 EnvironmentLabelViewModel.Environment != null &&
                 EnvironmentLabelViewModel.Environment.Environment != Environ.N_A;
            }
        }

	    protected static string GetTitleForStatusUpdate(XElement node)
	    {
	        var titleAttribute = node.Attribute("Title");
	        var idAttribute = node.Attribute("Id");
	        var title = titleAttribute != null && !String.IsNullOrEmpty(titleAttribute.Value)
	                        ? titleAttribute.Value
	                        : idAttribute.Value;
	        return title;
	    }


        public void RegisterDockManagerHookers(Assembly hookerAssembly)
        {
            foreach (var type in hookerAssembly.GetTypes())
            { 
                if (!typeof(IDockManagerHooker).IsAssignableFrom(type)) continue;
                var attrs = type.GetCustomAttributes(typeof(DockManagerHookerAttribute), true);
                if (attrs.Length <= 0) continue;
                if (((DockManagerHookerAttribute)attrs[0]).UseHarmonia != UseHarmonia) continue;
                IDockManagerHooker manager = Activator.CreateInstance(type) as IDockManagerHooker;
                if (manager != null)
                {
                    manager.Hook();
                }
            }
        }

        public IEnumerable<IWindowViewContainerHostRoot> ReadSubState(string subLayoutName_, XElement subLayout_, RehydratePaneDelegate callback_,
            bool reuseExisting_ = false, SubLayoutLocation locationOverride_ = SubLayoutLocation.SavedLocation)
        {
            if (UseHarmonia)
            {
                return DockManager.Services.LayoutManager.ReadSubLayout(subLayoutName_, subLayout_, callback_,
                                                                        reuseExisting_, locationOverride_);
            }
            else
            {
                return XamDockManagerLayoutHelper.ReadSubLayout(subLayoutName_, subLayout_, callback_, reuseExisting_, locationOverride_);
            }
        }

        public XElement WriteSubState(IEnumerable<IWindowViewContainerHostRoot> roots_, IEnumerable<IWindowViewContainer> containers_ = null, bool skipLocationInfo_ = false)
        {
            if (UseHarmonia)
            {
                return DockManager.Services.LayoutManager.SaveSubLayout(roots_, containers_, skipLocationInfo_);
            }
            else
            {
                return XamDockManagerLayoutHelper.WriteSubLayout(roots_, containers_, skipLocationInfo_);
            }
        }
	}
    internal interface IGlobalDockInfoProvider
    {
        EnvironmentLabelViewModel EnvironmentLabelViewModel { get; }
        Func<string[], string> ToolWindowTitleGenerator { get; }
        TextTrimming TitleTextTrimming { get; }
        KeyGesture WindowRenameGesture { get; }
        double? SpacingSize { get; }
    }

	internal interface ILayoutSaveLoadRequester
	{
		event RequestLoadEventHandler LoadRequested;
		event RequestSaveEventHandler SaveRequested;
	}

	internal delegate void RequestSaveEventHandler(SaveEventArgs args_);
	internal class SaveEventArgs : EventArgs
	{
		public XDocument SavedState
		{
			get;
			set;
		}
	}

	internal delegate void RequestLoadEventHandler(LoadEventArgs state_);
	internal class LoadEventArgs : EventArgs
	{
		public LoadEventArgs(XDocument state_)
		{
			State = state_;
		}

		public XDocument State
		{
			get;
			private set;
		}
	}
}