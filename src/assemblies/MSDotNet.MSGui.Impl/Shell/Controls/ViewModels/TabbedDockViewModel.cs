﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Controls/ViewModels/TabbedDockViewModel.cs#54 $
// $Change: 903044 $
// $DateTime: 2014/10/29 21:48:22 $
// $Author: hrechkin $

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using System.Xml;
using System.Xml.Linq; 
using Microsoft.Practices.Unity; 
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence.LayoutLoadingScreen;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.ViewModels;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions; 

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
    internal class InitialShellTabParameters : InitialWidgetParameters, ILockerPassThrough
    {
    }

    internal class InitialShellTabHeaderPrePostParameters : InitialWidgetParameters
    {
    }

    internal class InitialShellTabToolbarParameters : InitialWidgetParameters
    {
    }

    internal class TabbedDockViewModel : DockViewModel, IWindowManager, ITabbedDock, IWidgetViewContainer,
                                         IActivePaneChangingHandler
    {
        //TODO don't allow to have no active tab
        internal const string MAIN_TAB_NAME = "Main";
        private const string NEW_NAME_BASE = "Untitled";
        private readonly ChromeRegistry _chromeRegistry;
        private readonly ObservableCollection<IDockTab> _dockTabs = new ObservableCollection<IDockTab>();
        private ShellTabViewModel _previousTab;
        private OverlayModeSettings _overlaySettings = new OverlayModeSettings { Enable = false };

        private volatile bool _cancelViewLoad;
        private volatile bool _cancelLayoutRestore;
        private readonly string dockLayoutRootName; 

        public TabbedDockViewModel(ChromeRegistry chromeRegistry, IUnityContainer container,
                                   IHidingLockingManager hidingLockingManager)
            : base(hidingLockingManager, container)
        {
            _chromeRegistry = chromeRegistry;
            _chromeRegistry.RegisterWidgetFactoryMapping<InitialShellTabParameters>(ShellTabFactory);
            _chromeRegistry.RegisterWidgetFactoryMapping<InitialShellTabHeaderPrePostParameters>(ShellTabHeaderPrePostFactory);
            _chromeRegistry.RegisterWidgetFactoryMapping<InitialShellTabToolbarParameters>(ShellTabToolbarFactory);
            //todo: choose LayoutConverter.HarmoniaDockLayoutRootName when harmonia is supported and enabled for docked window
            dockLayoutRootName = LayoutConverter.DockLayoutRootName; 

            //AddTab(MAIN_TAB_NAME); --moved to ShellWindow.Initialize
            //TODO this breaks launcher shell mode
            InitializeCommandBindings();
            InitTabSwitching(); 

            this.m_tabs.CollectionChanged += (s, e) =>
                {
                    if (e.NewItems != null)
                    {
                        foreach (IDockTab tab in e.NewItems)
                        {
                            this._dockTabs.Add(tab);
                        }
                    }

                    if (e.OldItems != null)
                    {
                        foreach (IDockTab tab in e.OldItems)
                        {
                            this._dockTabs.Remove(tab);
                        }
                    }
                };

            MainTabwellVisibility = ShellModeExtension.GetVisibilityForElement(HideLockUIGranularity.HideMainTabwell);
            if (MainTabwellVisibility == Visibility.Collapsed)
            {
                RemoveEmptyTabsOnLastChildClosed = RemoveEmptyTabsOnLayoutLoad = false;
            }
        }

        [FactoryOptions(typeof (ShellTabViewModel))]
        private static bool ShellTabFactory(IWidgetViewContainer vc, XDocument state)
        {
            var tab = vc as ShellTabViewModel;
            tab.Title = vc.Parameters.Text;

            return true;
        }

        private static bool ShellTabHeaderPrePostFactory(IWidgetViewContainer vc, XDocument state)
        {
            return true;
        }

        private static bool ShellTabToolbarFactory(IWidgetViewContainer vc, XDocument state)
        {
            return true;
        }

        #region Tabs

        private void InitializeCommandBindings()
        {
            CommandBinding addTabBinding = new CommandBinding(Command.AddTabCommand, HandleAddTabCommand,
                                                              (o, x) => x.CanExecute = true);

            //Register the binding to the class
            CommandManager.RegisterClassCommandBinding(typeof (TabbedDockViewModel), addTabBinding);
            CommandBindings.Add(addTabBinding);
        }

        private void InitTabSwitching()
        {
            Workspaces.CollectionChanged += OnWorkspacesChanged;
            foreach (WindowViewModel workspace in Workspaces)
            {
                if (!workspace.IsHidden && !workspace.IsFloating)
                {
                    workspace.Host = CurrentTab;
                }
            }

            Tabs.CurrentChanging += (s, e) =>
                {
                    bool cancel;
                    if (ActivePaneAtClick != null)
                    {
                        OnActivePaneChanging(this, ActivePaneAtClick, null, out cancel);
                        e.Cancel = cancel;
                    }

                    this.OnActiveTabChanged();
                };
            Tabs.CurrentChanged += OnCurrentTabChanged;
        }

        private readonly ObservableCollection<ShellTabViewModel> m_tabs = new ObservableCollection<ShellTabViewModel>();
        private ICollectionView m_tabsCollectionView;

        public ICollectionView Tabs
        {
            get
            {
                return m_tabsCollectionView ?? (m_tabsCollectionView = CollectionViewSource.GetDefaultView(m_tabs));
            }
        }

        public object CurrentTab
        {
            get
            {
                if (MainTabwellVisibility == Visibility.Collapsed)
                {
                     
                    return m_tabs.Count > 0 ?  m_tabs[0] : DummyTab;
                }
                else
                {
                    return Tabs.CurrentItem;
                }
            }
        }

        private void HandleAddTabCommand(object o_, ExecutedRoutedEventArgs e_)
        {
            int maxSuffix = 0;
            foreach (ShellTabViewModel tab in m_tabs)
            {
                var headerName = tab.Title;
                if (headerName != null && headerName.StartsWith(NEW_NAME_BASE) &&
                    headerName.Length > NEW_NAME_BASE.Length)
                {
                    var suffix = headerName.Substring(NEW_NAME_BASE.Length);
                    int candidate;
                    int.TryParse(suffix, out candidate);
                    if (candidate > maxSuffix)
                    {
                        maxSuffix = candidate;
                    }
                }
            }
            maxSuffix++;
            AddTab(NEW_NAME_BASE + maxSuffix);
        }

        private ShellTabViewModel _dummyTab;
        private ShellTabViewModel DummyTab
        {
            get 
            { 
                if (_dummyTab != null) return _dummyTab;
                var chromeManager = m_container.Resolve<IChromeManager>();
                _dummyTab =
                    (ShellTabViewModel)
                    chromeManager.AddWidget("Dummy", new InitialShellTabParameters { Text = "Dummy" },
                                            chromeManager[ChromeArea.TabWell]);
                return _dummyTab;
            }
        }
        /// <summary>
        /// Add another tab
        /// </summary>
        /// <param name="name_"></param>
        public ShellTabViewModel AddTab(string name_, bool setSelected_=true)
        {
            if (name_ == null)
            {
                return null;
            }

            var chromeManager = m_container.Resolve<IChromeManager>();
            var tabViewModel =
                (ShellTabViewModel)
                chromeManager.AddWidget(name_, new InitialShellTabParameters {Text = name_},
                                        chromeManager[ChromeArea.TabWell]);

            tabViewModel.Title = name_;
            tabViewModel.RequestClosing += OnTabClosing;
            tabViewModel.RequestClose += (x_, y_) => CloseTabWithChildren(tabViewModel);
            tabViewModel.RequestCloseAndDetach += (x_, y_) => CloseTabDetachChildren(tabViewModel);
            tabViewModel.RequestCloseOthers += (x_, y_) => CloseAllTabsButThis(tabViewModel);
            tabViewModel.RequestCloseAll += (x_, y_) => CloseAllTabs(tabViewModel);

            if (SpecialTabView != null)
            {
                m_tabs.Insert(m_tabs.Count - 1, tabViewModel);
            }
            else
            {
                m_tabs.Add(tabViewModel);
            }
            if (setSelected_)
            {
                this.SetSelectedTab(tabViewModel.Title);
            }
            return tabViewModel;
        }

        public ShellTabViewModel AddTab(string name_, object titleData_, bool setSelected_ = true)
        {
            var tab = AddTab(name_, setSelected_);
            if (tab != null)
            {
                tab.TitleData = titleData_;
            }
            return tab;
        }


        private void OnTabClosing(object sender_, CancelEventArgs e_)
        {
            if (m_tabs.Count < 2)
            {
                e_.Cancel = true;
            }
        }

        private void CloseTabWithChildren(ShellTabViewModel tab_)
        {
            foreach (WindowViewModel workspace in new List<WindowViewModel>(Workspaces))
            {
                if (workspace.Host == tab_)
                {
                    workspace.Close(); 
                }
            }
            m_tabs.Remove(tab_);
        }

        private void CloseTabDetachChildren(ShellTabViewModel tab_)
        {
            foreach (WindowViewModel workspace in Workspaces)
            {
                if (workspace.Host == tab_)
                {
                    workspace.IsHidden = false;
                    workspace.IsFloating = true;
                }
            }
            m_tabs.Remove(tab_);
        }

        private void CloseAllTabsButThis(ShellTabViewModel tab_)
        {
            List<ShellTabViewModel> clone = new List<ShellTabViewModel>(m_tabs);
            foreach (ShellTabViewModel tab in clone)
            {
                if (tab != tab_)
                {
                    CloseTabWithChildren(tab);
                }
            }
        }

        private void CloseAllTabs(ShellTabViewModel tab_)
        {
            List<ShellTabViewModel> clone = new List<ShellTabViewModel>(m_tabs);
            foreach (ShellTabViewModel tab in clone)
            {
                CloseTabWithChildren(tab);
            }
            AddTab(MAIN_TAB_NAME);
        }

        private ShellTabViewModel FindTabWithName(string name_)
        {
            foreach (var tab in m_tabs)
            {
                if (tab.Title == name_)
                {
                    return tab;
                }
            }
            return null;
        }

        public ShellTabViewModel SetSelectedTab(string tabName_)
        {
            Tabs.MoveCurrentTo(null);
            ShellTabViewModel newtab = FindTabWithName(tabName_);
            //if tab doesn't exist select the first tab
            if (newtab == null)
            {
                if (m_tabs.Count == 0)
                {
                    //things somehow went really bad
                    AddTab(MAIN_TAB_NAME);
                }
                newtab = m_tabs[0];
            }
            Tabs.MoveCurrentTo(newtab);

            return newtab;
        }

        public string GetSelectedTabName()
        {
            ShellTabViewModel selectedTab = CurrentTab as ShellTabViewModel;
            if (selectedTab == null)
            {
                return string.Empty;
            }
            return selectedTab.Title;
        }

        public ReadOnlyCollection<ShellTabViewModel> GetTabs()
        {
            return new List<ShellTabViewModel>(m_tabs).AsReadOnly();
        }

        private void OnWorkspacesChanged(object sender_, NotifyCollectionChangedEventArgs e_)
        {
            if (e_.NewItems != null && e_.NewItems.Count != 0)
            {
                foreach (WindowViewModel workspace in e_.NewItems)
                {
                    workspace.PropertyChanged += OnWorkspacePropertyChanged;
                }
            }

            if (e_.OldItems != null && e_.OldItems.Count != 0)
            {
                foreach (WindowViewModel workspace in e_.OldItems)
                {
                    workspace.PropertyChanged -= OnWorkspacePropertyChanged;
                }
            }
        }

        private void OnWorkspacePropertyChanged(object sender_, PropertyChangedEventArgs e_)
        {
            if (e_.PropertyName == "IsFloating" && sender_ is WindowViewModel && !m_isLoading)
            {
                WindowViewModel window = (WindowViewModel) sender_;
                if (window.IsBeingClosed) return;

                var hostTab = window.Host as ShellTabViewModel;
                if (hostTab != null && !ChromeManagerBase.TabsOwnFloatingWindows)
                {
                    hostTab.RemoveWindow(window.ViewContainer as IWindowViewContainer);
                }

                if (window.IsFloating)
                {
                    window.Host = null;
                }
                else
                {
                    var host = CurrentTab as ShellTabViewModel; 
                    window.Host = host; 
                    if (host != null)
                        host.AddWindow(window.ViewContainer as IWindowViewContainer);
                }
            }
        }

        private void OnCurrentTabChanged(object sender_, EventArgs e_)
        {
            System.Windows.Application.Current.Dispatcher.Invoke(TabGroupPaneSelectedIndexStore);
            var current = CurrentTab as ShellTabViewModel;
            foreach (WindowViewModel workspace in Workspaces)
            {
                if (!workspace.IsFloating)
                {
                    var host = workspace.Host as ShellTabViewModel;
                    workspace.IsHidden = host != current;
                }
                else if (ChromeManagerBase.TabsOwnFloatingWindows)
                {
                    workspace.IsInActiveWorkspace = workspace.Owner == current;
                }
            }
            if (current != null)
                current.RaiseIsActivePropertyChanged();
            if (_previousTab != null)
                _previousTab.RaiseIsActivePropertyChanged();
            _previousTab = current;
            System.Windows.Application.Current.Dispatcher.Invoke(TabGroupPaneSelectedIndexCleanup);
            OnPropertyChanged("IsSpecialTabSelected");
        }

        internal event EventHandler TabGroupPaneSelectedIndexStoreEvent;
        internal event EventHandler TabGroupPaneSelectedIndexCleanupEvent;

        private void TabGroupPaneSelectedIndexCleanup()
        {
            if (TabGroupPaneSelectedIndexCleanupEvent != null)
                TabGroupPaneSelectedIndexCleanupEvent(this, EventArgs.Empty);
        }

        private void TabGroupPaneSelectedIndexStore()
        {
            if (TabGroupPaneSelectedIndexStoreEvent != null)
                TabGroupPaneSelectedIndexStoreEvent(this, EventArgs.Empty);
        }

        #endregion Tabs

        #region Save/Load

        public const string NODE_NAME = "TabbedDock";

        public XDocument SaveState(Func<WindowViewContainer, XElement> buildIViewXml_)
        {
            XDocument documentWithSubitems = new XDocument();
            // <TabbedDock>
            // | <XamDockManagerLayout>
            XDocument dockLayout = SaveLayout();
            XElementWithSubitems rootElement = new XElementWithSubitems(NODE_NAME,
                                                                        new XElement(dockLayoutRootName, dockLayout.Root));

            // | <Tabs>
            // Minor issue:
            // it's allowed to have more than one tab with the same name
            // if it happens, the first tab with this name will be activated

            XElement tabsElement = new XElement("Tabs");

            //receiving two data structures containing IVews and their tabs
            List<WindowViewModel> tabLessPanes;
            Dictionary<ShellTabViewModel, List<WindowViewModel>> tabbedPanesDict;
            GetPanesInfo(out tabLessPanes, out tabbedPanesDict);

            IEnumerable<ShellTabViewModel> tabs;
            bool dummyTabSelected;
            if (m_tabs.Count > 0)
            {
                tabs = GetTabs();
                dummyTabSelected = false;
            }
            else
            {
                tabs = new[] {DummyTab};
                dummyTabSelected = true;
            }

            foreach (var tab in tabs)
            {
                // | | <Tab Name="Hello" IsActive="true">
                XElement tabElement;
                if (dummyTabSelected)
                {
                    tabElement =
                        new XElement("Tab",
                                     new XAttribute("Name", tab.Title ?? ""),
                                     new XAttribute("DummyTab", true.ToString(CultureInfo.InvariantCulture)));
                }
                else
                {
                    tabElement =
                        new XElement("Tab",
                                     new XAttribute("Name", tab.Title ?? ""));
                }

                if (tab == CurrentTab)
                {
                    tabElement.Add(new XAttribute("IsActive", "true"));
                }
                // docked panes in this tab
                // | | | <Panes>
                var panesElement = new XElement("Panes");

                if (tabbedPanesDict.ContainsKey(tab))
                {
                    foreach (WindowViewModel windowViewModel in tabbedPanesDict[tab])
                    {
                        if (windowViewModel != null && !windowViewModel.Transient)
                        {
                            XElement viewElement = buildIViewXml_((WindowViewContainer) windowViewModel.ViewContainer);
                            panesElement.Add(viewElement);
                            rootElement.Subitems[windowViewModel.ID] = viewElement;
                        }
                    }
                }

                tabElement.Add(panesElement);
                tabsElement.Add(tabElement);
            }

            rootElement.Add(tabsElement);

            // floating panes
            // | | <Panes>
            XElement tablessPanesElement = new XElement("Panes");
            foreach (WindowViewModel windowViewModel in tabLessPanes)
            {
                if (windowViewModel != null && !windowViewModel.Transient)
                {
                    XElement viewElement = buildIViewXml_((WindowViewContainer) windowViewModel.ViewContainer);
                    tablessPanesElement.Add(viewElement);
                    rootElement.Subitems[windowViewModel.ID] = viewElement;
                }
            }
            rootElement.Add(tablessPanesElement);
            documentWithSubitems.Add(rootElement);
            return documentWithSubitems;
        }

        public void BeforeSaveState()
        {
        }

        public void AfterSaveState()
        {
        }

        private void GetPanesInfo(out List<WindowViewModel> tabLess,
                                  out Dictionary<ShellTabViewModel, List<WindowViewModel>> tabbedPanes)
        {
            tabLess = new List<WindowViewModel>();
            tabbedPanes = new Dictionary<ShellTabViewModel, List<WindowViewModel>>();
            foreach (var windowViewModel in Workspaces)
            {
                if (windowViewModel.Host is ShellTabViewModel)
                {
                    var tab = (ShellTabViewModel) windowViewModel.Host;
                    if (!tabbedPanes.ContainsKey(tab) || tabbedPanes[tab] == null)
                    {
                        tabbedPanes[tab] = new List<WindowViewModel>();
                    }
                    tabbedPanes[tab].Add(windowViewModel);
                }
                else
                {
                    tabLess.Add(windowViewModel);
                }
            }
        }

        public void LoadState(XDocument layout_, PersistenceProfileService persistenceProfileService_, Action<XDocument, object> applyNodeAction_)
        {
            persistenceProfileService_.DoDispatchedAction(Clean); 

            var mainElement = layout_.Element(NODE_NAME);
            var layoutLoadingScreen = m_container.Resolve<LayoutLoadingScreenRetriever>().GetScreen(); 
            try
            {

                if (mainElement == null)
                {
                    if ((mainElement = TryLoadForeignLayout(layout_.Root, persistenceProfileService_, applyNodeAction_, layoutLoadingScreen)) == null)
                    {
                        return;
                    }
                }

                var layoutNode = mainElement.Element(dockLayoutRootName);
                var tablessPanesNode = mainElement.Element("Panes");
                var tabsElement = mainElement.Element("Tabs");
                var tabsNodes = tabsElement != null ? tabsElement.Elements("Tab") : new XElement[] {};
                var paneCounter = 0;

                ShellTabViewModel activeTab = null;
                if (TrackLayoutLoadStatus)
                { 
                    layoutLoadingScreen.ShowProgressBar((tablessPanesNode != null ? tablessPanesNode.Elements().Count() : 0) + 
                                          tabsNodes.Count());
                } 

                foreach (var tabNode in tabsNodes)
                {
                    if (_cancelViewLoad) break;

                    var nameAttr = tabNode.Attribute("Name");

                    var dummyTabAttr = tabNode.Attribute("DummyTab");
                    var isDummyTab = false;
                    if (dummyTabAttr != null)
                    {
                        bool isDummyTabLocal;
                        if(bool.TryParse(dummyTabAttr.Value, out isDummyTabLocal))
                        {
                            isDummyTab = isDummyTabLocal;
                        }
                    }

                    if (nameAttr == null)
                    {
                        continue;
                    }
                    ShellTabViewModel tab = null;
                    persistenceProfileService_.DoDispatchedAction(new Action(() =>
                    {
                        if (!isDummyTab)
                        {
                            tab = AddTab(nameAttr.Value);
                        }
                        else
                        {
                            tab = DummyTab;
                        }
                    })); 
                    var activeAttr = tabNode.Attribute("IsActive");
                    if (activeAttr != null && activeAttr.Value.Equals("true", StringComparison.OrdinalIgnoreCase))
                    {
                        activeTab = tab;
                    }
                    if (_cancelViewLoad) break;
                    var panesNode = tabNode.Element("Panes");
                    if (panesNode != null)
                    {
                        foreach (XElement node in panesNode.Elements())
                        {
                            if (_cancelViewLoad) break;
                            if (TrackLayoutLoadStatus)
                            {
                                layoutLoadingScreen.SetCurrentView(GetTitleForStatusUpdate(node));
                                layoutLoadingScreen.SetProgress(paneCounter++);
                            } 
                            applyNodeAction_(new XDocument(node), tab);
                        }
                    }
                }

                if (tablessPanesNode != null && !_cancelViewLoad)
                {
                    var tablessPanes = tablessPanesNode.Elements();
                    foreach (var node in tablessPanes)
                    {
                        if (_cancelViewLoad) break;

                        ShellTabViewModel ownerTab = null; 
                        var ownerTabAttribute = node.Attribute("OwnerTab");
                        if (ownerTabAttribute != null && ownerTabAttribute.Value != null)
                        {
                            ownerTab = this.m_tabs.FirstOrDefault(tab => tab.Title == ownerTabAttribute.Value);
                        }
                         
                        if (TrackLayoutLoadStatus)
                        {
                            layoutLoadingScreen.SetCurrentView(GetTitleForStatusUpdate(node));
                            layoutLoadingScreen.SetProgress(paneCounter++);
                        } 
                        applyNodeAction_(new XDocument(node),
                                        new Tuple<ShellTabViewModel, ShellTabViewModel>(null, ownerTab));
                    }
                }
                if (_cancelLayoutRestore) return;
                
                layoutLoadingScreen.SetProgress(paneCounter);


                persistenceProfileService_.DoDispatchedAction(() =>
                    {
                        if (layoutNode != null && layoutNode.HasElements)
                        {
                            LoadLayout(
                                new XDocument(layoutNode.Elements().First((e) => e.NodeType == XmlNodeType.Element)));
                        }

                        if (_cancelLayoutRestore) return;
                        Tabs.Refresh();
                        if (activeTab == null)
                        {
                            if (!Tabs.IsEmpty)
                                Tabs.MoveCurrentToFirst();
                        }
                        else
                            Tabs.MoveCurrentTo(activeTab);

                        OnCurrentTabChanged(this, null);
                    });


            }
            finally
            { 
                _cancelViewLoad = false;
                _cancelLayoutRestore = false;
            }
        }

        public void CancelLoad(bool cancelLayoutRestore_)
        {
            _cancelViewLoad = true;
            _cancelLayoutRestore = cancelLayoutRestore_;
        }
         

        private XElement TryLoadForeignLayout(XElement layout, PersistenceProfileService persistenceProfileService, Action<XDocument, object> applyNodeAction, ILayoutLoadingScreen splashScreen)
        {
            if (layout.Name == ConcordPersistenceStorage.EXPORTED_NODE_NAME)
            {
                string type = LayoutConverter.TryGetAttribute(layout, "type");
                switch (type)
                {
                    case ConcordPersistenceStorage.CONCORD_WINDOW_MANAGER_NAME:
                        return LayoutConverter.PaneWindowManagerToTabbedDock(new XElement("Layout", layout.Elements()));
                }
            }

            if (layout.Name == FloatingOnlyDockViewModel.NODE_NAME)
            {
                var paneCounter = 0;

                XElement layoutNode = layout.Element(dockLayoutRootName);
                XElement tablessPanesNode = layout.Element("Panes");

                if (tablessPanesNode != null)
                {
                    if (TrackLayoutLoadStatus)
                    {
                        splashScreen.ShowProgressBar(tablessPanesNode.Elements().Count());
                    }

                    IEnumerable<XElement> tablessPanes = tablessPanesNode.Elements();
                    
                    foreach (XElement node in tablessPanes)
                    {
                        if (_cancelViewLoad) break;
                        if (TrackLayoutLoadStatus)
                        {
                            splashScreen.SetCurrentView(GetTitleForStatusUpdate(node));
                            splashScreen.SetProgress(paneCounter++);
                        }
                        var tabNameAttr = node.Attribute("TabName");
                        IDockTab tab = null;
                        if (tabNameAttr != null && !string.IsNullOrEmpty(tabNameAttr.Value))
                        {
                            tab = this[tabNameAttr.Value];
                            if (tab == null)
                            {
                                persistenceProfileService.DoDispatchedAction(new Action(() =>
                                {
                                    tab = AddTab(tabNameAttr.Value);
                                })); 
                            }
                            
                        }
                        applyNodeAction(new XDocument(node), tab);
                    }
                }

                if (!_cancelLayoutRestore && layoutNode != null && layoutNode.HasElements)
                {
                    persistenceProfileService.DoDispatchedAction(() =>
                        {
                            var state =
    new XDocument(layoutNode.Elements().First((e) => e.NodeType == XmlNodeType.Element));
                            if (Bootstrapper.ShellModeUserConfigurable)
                            {
                                var layoutNodeCopied = state.Element(LayoutConverter.DockManagerRootName);
                                if (layoutNodeCopied != null)
                                {
                                    var contentPanes = layoutNodeCopied.Element("contentPanes");
                                    if (contentPanes != null)
                                    {
                                        int count = 0;
                                        foreach (var contentPane in contentPanes.Elements("contentPane"))
                                        {
                                            var locationAttr = contentPane.Attribute("location");
                                            if (locationAttr != null && (locationAttr.Value == "Floating" || locationAttr.Value == "FloatingOnly"))
                                            {
                                                locationAttr.Value = "DockedTop";
                                            }
                                            if (count > 0)
                                            {
                                                contentPane.SetAttributeValue("visibility", "Collapsed");
                                            }
                                            count++;
                                        }
                                    }

                                    var panesNode = layoutNodeCopied.Element("panes");
                                    if (panesNode != null)
                                    {
                                        foreach (var pane in panesNode.Elements("splitPane"))
                                        {
                                            var locationAttr = pane.Attribute("location");
                                            if (locationAttr != null && (locationAttr.Value == "Floating" || locationAttr.Value == "FloatingOnly"))
                                            {
                                                locationAttr.Value = "DockedTop";
                                            }
                                        }
                                    }
                                }  
                            } 
                            LoadLayout(state);
                            if (!Tabs.IsEmpty)
                            {
                                Tabs.MoveCurrentToFirst();
                                OnCurrentTabChanged(this, null);
                            }
                        });
                }
            }

            return null;
        }
 

        public void Clean()
        {
            foreach (WindowViewModel windowViewModel in new List<WindowViewModel>(Workspaces))
            {
                if (EnableNonQuietCloseOfViews)
                {
                    windowViewModel.Close();
                }
                else
                {
                    windowViewModel.CloseQuietly();
                }
                windowViewModel.Dispose();
            }
            m_tabs.Clear();
        }


        private bool m_isLoading;

        public void BeforeLoadState()
        {
            m_isLoading = true;
            //enable offscreen window initialization
            FloatingWindowUtil.HideLoadingWindows = true;
        }

        public void AfterLoadState()
        {
            m_isLoading = false;
            //disable offscreen window initialization
            FloatingWindowUtil.DoShowWindows();
            FixInvisibleWindows();
            if (RemoveEmptyTabsOnLayoutLoad)
            {
                var TabsToBeRemoved = m_tabs.Where((t) => (!Workspaces.Any((w) => (w.Host == t)))).ToArray();
                foreach (var shellTabViewModel in TabsToBeRemoved)
                {
                    m_tabs.Remove(shellTabViewModel);
                }
            }
        }

        internal static bool RemoveEmptyTabsOnLayoutLoad { get; set; }
        internal static bool RemoveEmptyTabsOnLastChildClosed { get; set; }

        private void FixInvisibleWindows()
        {

            foreach (WindowViewModel workspace in Workspaces)
            {
                if (!workspace.IsFloating && workspace.IsHidden && workspace.Host == null)
                {
                    workspace.IsFloating = true;
                    workspace.IsHidden = false;
                }

                var parameters = workspace.ViewContainer.InitialViewSettings.wrappedInitialWindowParameters;

                if (workspace.IsFloating && !parameters.AllowPartiallyOffscreen &&
                    workspace.ViewContainer.Content is DependencyObject)
                {
                    double coverage = 0;
                    var window = Window.GetWindow(workspace.ViewContainer.Content as DependencyObject);

                    Rect windowRect = new Rect(window.Left, window.Top, window.ActualWidth, window.ActualHeight);

                    foreach (var screen in System.Windows.Forms.Screen.AllScreens)
                    {
                        var temp = new Rect(screen.WorkingArea.Left, screen.WorkingArea.Top, screen.WorkingArea.Width,
                                            screen.WorkingArea.Height);
                        temp.Intersect(windowRect);

                        if (!temp.IsEmpty)
                            coverage += (temp.Width*temp.Height)/(windowRect.Width*windowRect.Height);

                        if (coverage > 0.999999999)
                            break;
                    }

                    if (coverage < 0.999999999)
                    {
                        CustomInitialLocationDelegate handler = ((parameters.InitialLocation & InitialLocation.Custom) ==
                                                                 InitialLocation.Custom)
                                                                    ? parameters.InitialLocationCallback
                                                                    : InitialLocationHelper.MainDisplayCenter;

                        var newPosition = handler(window.ActualWidth, window.ActualHeight);
                        window.Top = newPosition.Top;
                        window.Left = newPosition.Left;

                        if (!CoreUtilities.AreClose(window.ActualWidth, newPosition.Width))
                            window.Width = newPosition.Width;
                        if (!CoreUtilities.AreClose(window.ActualHeight,newPosition.Height))
                            window.Height = newPosition.Height;
                    }
                }
            }

        }

        #endregion Save/Load

        public void AddNewView(WindowViewContainer viewContainer_, object additionalData_)
        {
            var wsfound = Workspaces.FirstOrDefault(ws => ws.ID == viewContainer_.ID);
            if (wsfound != null) return;

            var tuple = additionalData_ as Tuple<ShellTabViewModel, ShellTabViewModel>;
            var hostTab = (tuple == null)
                              ? additionalData_ as ShellTabViewModel
                              : tuple.Item1;
            var ownerTab = (tuple == null)
                               ? CurrentTab
                               : tuple.Item2;


            if (additionalData_ == null)
            {
                if (viewContainer_.Parameters.InitialLocation == InitialLocation.DockBottom ||
                    viewContainer_.Parameters.InitialLocation == InitialLocation.DockLeft ||
                    viewContainer_.Parameters.InitialLocation == InitialLocation.DockRight ||
                    viewContainer_.Parameters.InitialLocation == InitialLocation.DockTabbed ||
                    viewContainer_.Parameters.InitialLocation == InitialLocation.DockTop
                    )
                {
                    if (viewContainer_.Parameters.InitialLocationTarget == null)
                    {
                        additionalData_ = this.CurrentTab;
                        hostTab = additionalData_ as ShellTabViewModel;
                    }
                    else
                    {
                        var viewModel =
                            Workspaces.FirstOrDefault(
                                wm => wm.ViewContainer == viewContainer_.Parameters.InitialLocationTarget);
                        if (viewModel != null)
                            additionalData_ = viewModel.Host;
                    }
                }
            }
            WindowViewModel windowViewModel = viewContainer_.GetViewModel(HidingLockingManager);
            windowViewModel.InitialParameters = viewContainer_.Parameters;
            windowViewModel.Host = hostTab;
            windowViewModel.Owner = ownerTab;
            windowViewModel.DragOrDropOnContent +=
                (sender_, args_) => InvokeDragOrDropOnWindow(new GenericDragOrDropOnWindowEventArgs
                    {
                        DragEventArgs = args_.EventArgs,
                        InternalEventType = args_.InternalEventType,
                        Window = windowViewModel.ViewContainer as IWindowViewContainer
                    });


            if (hostTab != null)
            {
                if (hostTab != CurrentTab)
                    windowViewModel.IsHidden = true;
                //tab.AddWindow(viewContainer_);
                hostTab.AddWindow(viewContainer_);
            }

            if (TabbedDockViewModel.RemoveEmptyTabsOnLastChildClosed)
            {
                EventHandler closeHandler = null;
                closeHandler = (s, e) =>
                    {
                        if (Workspaces.Count((w) => { return w.Host == windowViewModel.Host as ShellTabViewModel; }) < 2)
                        {
                            m_tabs.Remove(windowViewModel.Host as ShellTabViewModel);
                        }
                        windowViewModel.Closed -= closeHandler;
                    };
                windowViewModel.Closed += closeHandler;
            }

            Workspaces.Add(windowViewModel);
        }
         

        void IActivePaneChangingHandler.RaiseActivePaneChanging(UIElement oldValue, UIElement newValue,
                                                                out bool cancel)
        {
            OnActivePaneChanging(this, oldValue, newValue, out cancel);
        }

        
        public void MoveTabRight(ShellTabViewModel shellTabViewModel)
        {
            int index = m_tabs.IndexOf(shellTabViewModel);
            if (index < m_tabs.Count - 1)
            {
                m_tabs.RemoveAt(index);
                m_tabs.Insert(index + 1, shellTabViewModel);
                SetSelectedTab(shellTabViewModel.Title);
            }
        }

        internal void MoveTabLeft(ShellTabViewModel shellTabViewModel)
        {
            int index = m_tabs.IndexOf(shellTabViewModel);
            if (index > 0)
            {
                m_tabs.RemoveAt(index);
                m_tabs.Insert(index - 1, shellTabViewModel);
                SetSelectedTab(shellTabViewModel.Title);
            }
        }

        public Visibility MainTabwellVisibility { get; set; }

        #region ITabbedDock Members

        public IDockTab this[string tabName]
        {
            get
            {
                return this.m_tabs.FirstOrDefault(x => x.Title == tabName);
            }
        }

        ReadOnlyObservableCollection<IDockTab> ITabbedDock.Tabs
        {
            get
            {
                return new ReadOnlyObservableCollection<IDockTab>(this._dockTabs);
            }
        }

        public IDockTab ActiveTab
        {
            get
            {
                return this.CurrentTab as IDockTab;
            }
        }

        IDockTab ITabbedDock.AddTab(string tabName)
        {
            return this.AddTab(tabName);
        }

        IDockTab ITabbedDock.AddTab(string tabName, object titleData)
        {
            return this.AddTab(tabName, titleData);
        }

        public IWidgetViewContainer LeftEdge
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public IWidgetViewContainer RightEdge
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public event EventHandler ActiveTabChanged;

        private void OnActiveTabChanged()
        {
            var handler = this.ActiveTabChanged;
            if (handler != null)
            {
                handler(this, new EventArgs());
            }
        }

        #endregion

        #region IWidgetViewContainer Members

        public InitialWidgetParameters Parameters
        {
            get
            {
                return new InitialWidgetParameters();
            }
        }

        public IWidgetViewContainer Parent
        {
            get
            {
                return null;
            }
        }

        IWidgetViewContainer IWidgetViewContainer.this[string index]
        {
            get
            {
                return this.m_tabs.FirstOrDefault(tab => tab.Title == index);
            }
        }

        public bool Contains(string index)
        {
            return this.m_tabs.FirstOrDefault(tab => tab.Title == index) != null;
        }

        public object Content
        {
            get
            {
                return null;
            }
            set
            {
            }
        }

        public event EventHandler ContentReady;

        #endregion

        #region IViewContainerBase Members

        public string FactoryID
        {
            get
            {
                return string.Empty;
            }
        }

        InitialViewParameters IViewContainerBase.Parameters
        {
            get
            {
                return new InitialWidgetParameters();
            }
        }

        #endregion

        internal UIElement ActivePaneAtClick { get; set; }

 
        public event EventHandler<PaneDroppedAtScreenEdgeEventArgs> PaneDroppedAtScreenEdge;
        public event EventHandler<WindowGroupDroppedOnScreenEdgeEventArgs> WindowGroupDroppedAtScreenEdge;

        internal void RaiseWindowGroupDroppedAtScreenEdge(IFloatingWindow window, ScreenEdge edge,
                                                          TabbedDockPaneDragEndedEventArgs originalEventArgs)
        {
            var copy = WindowGroupDroppedAtScreenEdge;
            if (copy != null)
            {
                var eventArgs = new WindowGroupDroppedOnScreenEdgeEventArgs(window, edge);
                copy(this, eventArgs);

                // TODO (hrechkin): Come up with a way to close these window groups properly.
                if (eventArgs.CloseWindowAfterProcessing)
                {
                    originalEventArgs.MarkWindowForClosing();
                }
            }
        }

        internal void RaisePaneDroppedAtScreenEdge(UIElement pane, ScreenEdge edge)
        {
            var copy = PaneDroppedAtScreenEdge;
            if (copy != null)
            {
                PaneDroppedAtScreenEdge(this, new PaneDroppedAtScreenEdgeEventArgs()
                    {
                        ViewContainer = Attached.GetViewContainer(pane) as IWindowViewContainer,
                        Edge = edge
                    });
            }
        }

        public void ToggleOverlayMode(bool enable_, Brush brush_ = null, Func<FrameworkElement> content_ = null)
        {
            OverlaySettings = new OverlayModeSettings
                {
                    Brush = brush_,
                    Enable = enable_,
                    ContentFactory = content_
                };
        }

        public OverlayModeSettings OverlaySettings
        {
            get
            {
                return _overlaySettings;
            }
            set
            {
                _overlaySettings = value;
                OnPropertyChanged("OverlaySettings");
            }
        }

        #region SpecialTab suport for Workspace shell

        internal const string SpecialTabName = "SpecialTab";

        public object SpecialTabView { get; private set; }

        public void SetSpecialTab(object view_)
        {
            var tab = AddTab(SpecialTabName);
            tab.IsSpecial = true;
            SpecialTabView = view_;
        }

        public bool IsSpecialTabSelected
        {
            get
            {
                return SpecialTabView != null && GetSelectedTabName() == SpecialTabName;
            }
        }

        #endregion

 
    }

    internal class OverlayModeSettings
    {
        public bool Enable;
        public Brush Brush;
        public Func<FrameworkElement> ContentFactory;
    }
}
