﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Controls/ViewModels/WindowViewModel.cs#35 $
// $Change: 902487 $
// $DateTime: 2014/10/27 03:05:30 $
// $Author: caijin $

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.View;
using Size = System.Windows.Size;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
	class WindowViewModel : ViewModelBase
	{
	    private readonly IHidingLockingManager m_hidingLockingManager;
	    public static bool IsPinningAllowedSetting = true;
        internal static double? ShadowOffset;
	    public static bool FlatModeEnabledSetting = false;

	    private readonly WindowViewContainer m_viewContainer;

        public WindowViewModel(WindowViewContainer viewContainer, IHidingLockingManager hidingLockingManager)
        {
            m_viewContainer = viewContainer;
            m_viewContainer.PropertyChanged += OnPropertyChanged;
            m_viewContainer.CloseRequested += InvokeRequestClose;
            m_viewContainer.ActivationRequested += OnActivationRequested;
            m_viewContainer.Closed += OnViewContainerClosed;
            m_viewContainer.ClosedQuietly += OnViewContainerClosedQuietly; 
            m_viewContainer.FlashRequested += InvokeRequestFlash;
            m_hidingLockingManager = hidingLockingManager;
            m_hidingLockingManager.HideUIGranularityChanged += ApplyHideUIGranularity;
            ApplyHideUIGranularity(null, null);
        }

	    private void ApplyHideUIGranularity(object sender_, EventArgs args_)
	    {
            AllowDockingFloating = !m_hidingLockingManager.Granularity.HasFlag(HideLockUIGranularity.DisableViewTearOff);
	        TearOffButtonVisibility =
	            !m_hidingLockingManager.Granularity.HasFlag(HideLockUIGranularity.HideViewTearOffButton);
            CloseButtonVisibility = !m_hidingLockingManager.Granularity.HasFlag(HideLockUIGranularity.HideViewCloseButton);
            PinButtonVisibility = !m_hidingLockingManager.Granularity.HasFlag(HideLockUIGranularity.HideViewPinButton);
	        AllowFloatingOnly = !m_hidingLockingManager.Granularity.HasFlag(HideLockUIGranularity.DisableViewFloatingOnly);
	        AllowDocking = !m_hidingLockingManager.Granularity.HasFlag(HideLockUIGranularity.DisableViewDocking);
	    }

	    protected override void OnDispose()
		{
			m_viewContainer.PropertyChanged -= OnPropertyChanged;
			m_viewContainer.CloseRequested -= InvokeRequestClose;
			m_viewContainer.ActivationRequested -= OnActivationRequested;
			m_viewContainer.Closed -= OnViewContainerClosed;
            m_viewContainer.ClosedQuietly -= OnViewContainerClosedQuietly; 
			m_viewContainer.FlashRequested -= InvokeRequestFlash;
	        m_hidingLockingManager.HideUIGranularityChanged -= ApplyHideUIGranularity;
		}

		private void OnActivationRequested(object sender_, EventArgs args_)
		{
			Activate();
		}

		public event EventHandler Closed;
		private void OnViewContainerClosed(object sender_, WindowEventArgs e_)
		{
			var copy = Closed;
			if (copy != null)
			{
				copy(this, e_); //or maybe empty event args
			}
		}

        public event EventHandler ClosedQuietly;
        private void OnViewContainerClosedQuietly(object sender_, WindowEventArgs e_)
        {
            var copy = ClosedQuietly;
            if (copy != null)
            {
                copy(this, e_); //or maybe empty event args
            }
        }

		void OnPropertyChanged(object sender_, System.ComponentModel.PropertyChangedEventArgs e_)
		{
			switch (e_.PropertyName)
			{
				case "Title":
				case "DefaultSize":
				case "Width":
				case "Height":
				case "IsVisible":
                case "IsHidden":
				case "Background":
				case "ID":
				case "Content":
				case "IsActive":
				case "Icon":
				case "AccentColour":
				case "Topmost":
                case "OwnedByContainer":
				case "Transient":
				case "ShowInTaskbar":
                case "Tooltip":
                case "HeaderStructure":
                case "FloatingWindowMaximumHeight":
                case "FloatingWindowMaximumWidth": 
                case "FloatingWindowMinimumHeight":
                case "FloatingWindowMinimumWidth":
                case "CanMaximize":
					OnPropertyChanged(e_.PropertyName);
					break;
			}
		}
         
		/// <summary>
		/// Holds the initial setings.
		/// </summary>
		//public InitialViewSettings InitialViewSettings { get; internal set; }
		public InitialWindowParameters InitialParameters { get; internal set; }

	    public object Tooltip
	    {
	        get
	        {
	            return m_viewContainer.Tooltip;
	        }
            set
            {
                m_viewContainer.Tooltip = value;
            }
	    }

	    public virtual string Title
		{
			get
			{
				return m_viewContainer.Title;
			}
			set
			{
				m_viewContainer.Title = value;
			}
		}

		public Size DefaultSize
		{
			get
			{
				return new Size(m_viewContainer.Parameters.Width, m_viewContainer.Parameters.Height);//m_viewContainer.DefaultSize;
			}
			set
			{
				m_viewContainer.Parameters.Width = value.Width;
				m_viewContainer.Parameters.Height = value.Height;
			}
		}

        public string WorkspaceCategory
        {
            get { return m_viewContainer.Parameters.WorkspaceCategory; }
            set { m_viewContainer.Parameters.WorkspaceCategory = value; }
        }

	    public virtual double? FloatingWindowMaximumWidth
	    {
	        get
	        {
	            return m_viewContainer.FloatingWindowMaximumWidth;
	        }
            set
            {
                m_viewContainer.FloatingWindowMaximumWidth = value;
            }
	    }

        public virtual double? FloatingWindowMaximumHeight
        {
            get
            {
                return m_viewContainer.FloatingWindowMaximumHeight;
            }
            set
            {
                m_viewContainer.FloatingWindowMaximumHeight = value;
            }
        }

        public virtual double FloatingWindowMinimumWidth
        {
            get
            {
                return m_viewContainer.FloatingWindowMinimumWidth;
            }
            set
            {
                m_viewContainer.FloatingWindowMinimumWidth = value;
            }
        }

        public virtual double FloatingWindowMinimumHeight
        {
            get
            {
                return m_viewContainer.FloatingWindowMinimumHeight;
            }
            set
            {
                m_viewContainer.FloatingWindowMinimumHeight = value;
            }
        }

		public virtual double Width
		{
			get
			{
				return m_viewContainer.Width;
			}
			set
			{
				m_viewContainer.Width = value;
			}
		}

		public virtual double Height
		{
			get
			{
				return m_viewContainer.Height;
			}
			set
			{
				m_viewContainer.Height = value;
			}
		}
         

		public IList<object> HeaderItems
		{
			get
			{
				return m_viewContainer.HeaderItems;
			}
		}

		public bool IsVisible
		{
			get
			{
				return m_viewContainer.IsVisible;
			}
			internal set
			{
				m_viewContainer.IsVisible = value;
			}
		}

		public System.Windows.Media.Brush Background
		{
			get
			{
				return m_viewContainer.Background;
			}
			set
			{
				m_viewContainer.Background = value;
			}
		}

		public string ID
		{
			get
			{
				return m_viewContainer.ID;
			}
			set
			{
				m_viewContainer.ID = value;
			}
		}

		public object Content
		{
			get
			{
				return m_viewContainer.Content;
			}
			set
			{
				m_viewContainer.Content = value;
			}
		}

		public virtual  bool IsActive
		{
			get
			{
				return m_viewContainer.IsActive;
			}
			internal set
			{
				m_viewContainer.IsActive = value;
			}
		}

		public Icon Icon
		{
			get
			{
				return m_viewContainer.Icon;
			}
			set
			{
				m_viewContainer.Icon = value;
			}
		}

		public System.Windows.Media.Color AccentColour
		{
			get
			{
				return m_viewContainer.AccentColour;
			}
			set
			{
				m_viewContainer.AccentColour = value;
			}
		}

		//public ViewMetadata ViewMeta
		//{
		//    get
		//    {
		//        return m_viewContainer.ViewMeta;
		//    }
		//    set
		//    {
		//        m_viewContainer.ViewMeta = value;
		//    }
		//}

		public IViewContainer ViewContainer
		{
			get
			{
				return m_viewContainer;
			}
		}

		public ViewContainerType ViewContainerType
		{
			get
			{
				return this.m_viewContainer.ViewContainerType;
			}
		}

		private bool m_isPinned = true;
		public bool IsPinned
		{
			get
			{
				return m_isPinned;
			}
			set
			{
				m_isPinned = value;
				OnPropertyChanged("IsPinned");
			}
		}

		public void Close()
		{
			m_viewContainer.Close();
		}

		public void CloseQuietly()
		{
			InvokeRequestQuietClose();
		}

		public void Activate()
		{
			InvokeRequestActivate(this, EventArgs.Empty);
		}

		public bool TryClosing()
		{
			return !m_viewContainer.InvokeClosing().Cancel;
		}

		public void InvokeClosed()
		{
			m_viewContainer.InvokeClosed();
		}

		public event EventHandler RequestClose;
		private void InvokeRequestClose(object sender_, EventArgs eventArgs_)
		{
			var handler = RequestClose;
			if (handler != null)
			{
				handler(this, EventArgs.Empty);
			}
		}

		public event EventHandler RequestQuietClose;
		private void InvokeRequestQuietClose()
		{
			var handler = RequestQuietClose;
			if (handler != null)
			{
				handler(this, EventArgs.Empty);
			}
			m_viewContainer.InvokeClosedQuietly();
		}

		internal event EventHandler<EventArgs<System.Windows.Media.Color>> RequestFlash;
		private void InvokeRequestFlash(object sender_, EventArgs<System.Windows.Media.Color> eventArgs_)
		{
			EventHandler<EventArgs<System.Windows.Media.Color>> flashRequestedCopy = RequestFlash;
			if (flashRequestedCopy != null)
			{
				flashRequestedCopy(this, eventArgs_);
			}
		}

		internal event EventHandler<EventArgs> RequestActivate;
		private void InvokeRequestActivate(object sender_, EventArgs eventArgs_)
		{
			if (!ChromeManagerBase.StartupCompleted)
			{
				return;
			}

			EventHandler<EventArgs> activateRequestedCopy = RequestActivate;
			if (activateRequestedCopy != null)
			{
				activateRequestedCopy(this, eventArgs_);
			}
		}

		public bool IsHidden
		{
			get
			{
				return m_viewContainer.IsHidden;
			}
			set
			{
				m_viewContainer.IsHidden = value;
				OnPropertyChanged("IsHidden");
			}
		}

        public bool IsPinningAllowed
	    {
            get
            {
                return IsPinningAllowedSetting;
            }
	    }

		private bool m_isFloating;
		public bool IsFloating
		{
			get
			{
				return m_isFloating;
			}
			set
			{
				bool oldvalue = m_isFloating;
				if (oldvalue != value)
				{
					m_isFloating = value; 
					OnPropertyChanged("IsFloating");
				    TileView tileView = this.Content as TileView;
                    if (tileView != null)
                    {
                        TileViewModel model = tileView.DataContext as TileViewModel;
                        if (model != null)
                        {
                            model.IsFloating = value;
                        }
                    }
				}
			}
		}

		private object m_host;
		public virtual object Host
		{
			get
			{
				return m_host;
			}
			set
			{
                if (m_host != value)
                {
                    var old = m_host;
                    m_host = value;
                    OnPropertyChanged("Host");
                    m_viewContainer.InvokeHostChanged(old, value);
                    var copy = HostChangedInternal;
                    if (copy != null)
                    {
                        copy(this, new WindowViewContainer.HostChangedEventArgs() { NewHost = value, OldHost = old, Sender = m_viewContainer });
                    }
                }
			}
		}


	    internal event EventHandler<WindowViewContainer.HostChangedEventArgs> HostChangedInternal;


        private object m_owner;
        public object Owner
        {
            get
            {
                return this.m_owner;
            }
            set
            {
                this.m_owner = value;
                OnPropertyChanged("Owner");
            }
        }

		public bool Topmost
		{
			get
			{
				return this.m_viewContainer.Topmost;
			}
			set
			{
				this.m_viewContainer.Topmost = value;
			}
		}

	    public bool OwnedByContainer
	    {
	        get
	        {
	            return m_viewContainer.OwnedByContainer;
	        }
	        set
	        {
	            m_viewContainer.OwnedByContainer = value;
	        }
	    }

	    public bool Transient
		{
			get
			{
				return this.m_viewContainer.Transient;
			}
			set
			{
				this.m_viewContainer.Transient = value;
			}
		}

		public bool ShowInTaskbar
		{
			get
			{
				return this.m_viewContainer.ShowInTaskbar;
			}
			set
			{
				this.m_viewContainer.ShowInTaskbar = value;
			}
		}

        public HeaderStructure HeaderStructure
		{
			get
			{
                return this.m_viewContainer.HeaderStructure;
			}
			set
			{
                this.m_viewContainer.HeaderStructure = value;
			}
		}


      
	    public bool ShowFlashBorder
	    {
	        get {return m_viewContainer.Parameters.ShowFlashBorder;}
	    }

        public bool IsInActiveWorkspace
        {
            get
            {
                return this.m_viewContainer.IsInActiveWorkspace;
            }
            set
            {
                this.m_viewContainer.IsInActiveWorkspace = value;
                this.OnPropertyChanged("IsInActiveWorkspace");
            }
        }


        public HideLockUIGranularity HideGranularity
        {
            get
            {
                return m_hidingLockingManager.Granularity;
            }
        }

        private bool m_allowDocking;
        public bool AllowDocking
        {
            get
            {
                return m_allowDocking;
            }
            set
            {
                m_allowDocking = value;
                OnPropertyChanged("AllowDocking");
            }
        }

        private bool m_allowFloatingOnly;
        public bool AllowFloatingOnly
        {
            get
            {
                return m_allowFloatingOnly;
            }
            set
            {
                m_allowFloatingOnly = value;
                OnPropertyChanged("AllowFloatingOnly");
            }
        }

        private bool m_pinButtonVisibility;
        public bool PinButtonVisibility
        {
            get
            {
                return m_pinButtonVisibility;
            }
            set
            {
                m_pinButtonVisibility = value;
                OnPropertyChanged("PinButtonVisibility");
            }
        }

        private bool m_closeButtonVisibility;
        public bool CloseButtonVisibility
        {
            get
            {
                return m_closeButtonVisibility;
            }
            set
            {
                m_closeButtonVisibility = value;
                OnPropertyChanged("CloseButtonVisibility");
            }
        }

        private bool m_tearOffButtonVisibility;
        public bool TearOffButtonVisibility
        {
            get
            {
                return m_tearOffButtonVisibility;
            }
            set
            {
                m_tearOffButtonVisibility = value;
                OnPropertyChanged("TearOffButtonVisibility");
            }
        }

        private bool m_allowDockingFloating;

	    public bool AllowDockingFloating
        {
            get
            {
                return m_allowDockingFloating;
            }
            set
            {
                m_allowDockingFloating = value;
                OnPropertyChanged("AllowDockingFloating");
            }
        }

	    public bool CanMaximize
	    {
	        get
	        {
	            return m_viewContainer.CanMaximize;
	        }
	        set
	        {
	            m_viewContainer.CanMaximize = value;
                
	        }
	    }

	    internal bool IsBeingClosed { get; set; }

        public Thickness? ShadowOffsetThickness
        {
            get
            {
                if (ShadowOffset.HasValue)
                {
                    return new Thickness(ShadowOffset.Value, ShadowOffset.Value, 0, 0);
                }
                return null;
            }
        }

        public event EventHandler<DropOnContentEventArgs> DragOrDropOnContent;

	    public void InvokeDragOrDropOnContent(DropOnContentEventArgs e_)
	    {
	        EventHandler<DropOnContentEventArgs> handler = DragOrDropOnContent;
	        if (handler != null) handler(this, e_);
	    }

	    public bool IsFlatModeEnabled
	    {
	        get
	        {
	            return FlatModeEnabledSetting;
	        }
	    }

	    public Thickness ContentPaddingResolved
	    {
            get { return StaticContentPaddingResolved; }
	    }

        public static Thickness StaticContentPaddingResolved
        {
            get { return ContentPadding ?? new Thickness(ThemeExtensions.DefaultDockContentPadding); }
        }

	    public static Thickness? ContentPadding { get; set; }
	}

    public class DropOnContentEventArgs : EventArgs
    {
        public DragEventArgs EventArgs { get; set; }
        public RoutedEvent InternalEventType { get; set; }
    }
}
