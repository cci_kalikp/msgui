﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel; 
using System.Linq;
using System.Windows;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
    internal class TileViewModel : WindowViewModel
    {

        private readonly TileViewContainer viewContainer; 
        private readonly ObservableCollection<TileItemViewModel> tileItems =
            new ObservableCollection<TileItemViewModel>();
        private bool isEditing;
        private double unitSize = 8d;
        private double marginSize = 2d;  

        internal TileViewModel(TileViewContainer viewContainer_, IHidingLockingManager hidingLockingManager_)
            : base(viewContainer_, hidingLockingManager_)
        {
            this.viewContainer = viewContainer_;
            if (viewContainer_.Parameters != null)
            {
                UnitSize = viewContainer_.Parameters.UnitSize;
                MarginSize = viewContainer_.Parameters.MarginSize;
            }
            this.InitialParameters = viewContainer_.Parameters;
            if (InitialParameters != null)
            {
                this.Icon = this.InitialParameters.Icon;
            }
            this.Closed += TileViewModel_Closed;
            this.ClosedQuietly += TileViewModel_Closed;
        }

        void TileViewModel_Closed(object sender, EventArgs e)
        {
           foreach (TileItemViewModel tileItem in new List<TileItemViewModel>(this.TileItems))
           {
               tileItem.Close();
           }
           this.Closed -= new EventHandler(TileViewModel_Closed);
           this.ClosedQuietly -= TileViewModel_Closed;
        }
         
 
        internal void AddTileItem(TileItemViewModel tileItem_)
        {
            TileItemViewContainer tileItemViewContainer = tileItem_.ViewContainer as TileItemViewContainer;
            this.viewContainer.Children.Add(tileItemViewContainer);
            tileItem_.IsEditing = this.isEditing;
            tileItems.Add(tileItem_);
            tileItemViewContainer.Parent = this.viewContainer;
            tileItem_.Host = this.Host;
            tileItemViewContainer.V2vHost = this.viewContainer.V2vHost;
            EventHandler<WindowEventArgs> handler = null;
            handler = (s_, e_) =>
            {
                tileItemViewContainer.Closed -= handler;
                tileItemViewContainer.ClosedQuietly -= handler;
                RemoveTileItem(tileItem_);
                tileItemViewContainer.Parent = null;
                tileItem_.Host = null;

            };
            tileItemViewContainer.Closed += handler;
            tileItemViewContainer.ClosedQuietly += handler;
        }
         
        internal void RemoveTileItem(TileItemViewModel tileItem_)
        {
            this.viewContainer.Children.Remove(tileItem_.ViewContainer as TileItemViewContainer);
            tileItems.Remove(tileItem_);
            tileItem_.Dispose();

        }
        public new InitialTileParameters InitialParameters { get; internal set; }
 
        public override object Host
        {
            get
            {
                return base.Host;
            }
            set
            {
                base.Host = value;
                foreach (var tileItemViewModel in tileItems)
                {
                    tileItemViewModel.Host = value;
                }
            }
        }
        internal bool AutomaticCreated { get; set; }

 

        public ObservableCollection<TileItemViewModel> TileItems
        {
            get { return tileItems; }
        }


        public double UnitSize
        {
            get { return unitSize; }
            set
            {
                unitSize = value;
                OnPropertyChanged("UnitSize");
            }
        }

        public double MarginSize
        {
            get { return marginSize; }
            set
            {
                marginSize = value;
                OnPropertyChanged("MarginSize");
            }
        }
      
        public double Zoom
        {
            get { return viewContainer.Parameters.AllowZoom ? viewContainer.Zoom : 1d; }
            set
            {
                if (viewContainer.Parameters.AllowZoom)
                {
                    viewContainer.Zoom = value;
                    OnPropertyChanged("Zoom"); 
                } 
            }
        }

        public bool AllowZoom
        {
            get { return viewContainer.Parameters.AllowZoom; }
        }

        public bool IsEditing
        {
            get { return isEditing; }
            set 
            { 
                if (isEditing != value)
                {
                    isEditing = value;
                    foreach (var tileItemViewModel in TileItems)
                    {
                        tileItemViewModel.IsEditing = value;
                    } 
                    OnPropertyChanged("IsEditing");
                    OnPropertyChanged("ScaleSliderVisibility");
                    OnPropertyChanged("Title");
                } 
            }
        }

        public override string Title
        {
            get {  return viewContainer.Title + (isEditing ? viewContainer.Parameters.EditModeSuffix : string.Empty); }
            set { base.Title = value; }
        }

        public Visibility ScaleSliderVisibility
        {
            get { return IsEditing && AllowZoom ? Visibility.Visible : Visibility.Collapsed; }
        }
        
        public double GetNearestEdge(double value_)
        {
            var a = Math.Floor(value_ / (unitSize + marginSize)) * (unitSize + marginSize);
            var b = value_ - a;
            return (b * 2 < unitSize + marginSize) ? a : a + unitSize + marginSize;
        }

        public double GetRoundSize(double value_)
        {
            var a = Math.Floor(value_ / (unitSize + marginSize)) * (unitSize + marginSize);
            var b = value_ - a;
            return ((b + marginSize) * 2 < unitSize + marginSize && a > marginSize) ? a - marginSize : a + unitSize;
        }

        public double GetRoundUpSize(double value_)
        {
            var a = Math.Floor(value_ / (unitSize + marginSize)) * (unitSize + marginSize);
            var b = value_ - a;
            return (b < unitSize) ? a + unitSize : a + unitSize + unitSize + marginSize;
        }

        public Tuple<List<TileItemViewModel>, List<TileItemViewModel>> FindHorizontalAlignedItems(double x_, double y_)
        {

            var upItems = new List<TileItemViewModel>();
            var downItems = new List<TileItemViewModel>(); 
            if (tileItems.Count < 2)
            {
                return new Tuple<List<TileItemViewModel>, List<TileItemViewModel>>(upItems, downItems);
            }
            var start = 0d;
            var end = (double.IsNaN(Width) ? double.MaxValue : Width);
            var isPointInsideItem = false;

            foreach (var item in tileItems)
            {
                var clientRect = item.ClientBound;
                var top = clientRect.Top;
                var bottom = clientRect.Bottom;
                if (y_ >= top && y_ <= bottom)
                {
                    var left = clientRect.Left;
                    var right = clientRect.Right;
                    if (left > x_)
                    {
                        end = Math.Min(end, left);
                    }
                    else if (right < x_)
                    {
                        start = Math.Max(start, right);
                    }
                    else
                    {
                        isPointInsideItem = true;
                        break;
                    }
                }
            }

            if (!isPointInsideItem)
            {
                foreach (var item in tileItems)
                {
                    var clientRect = item.ClientBound; 
                    if (IsOverlapped(start, end, item.DisplayLeft, item.DisplayLeft + item.DisplayWidth))
                    {
                        var top = clientRect.Top;
                        var bottom = clientRect.Bottom;
                        if (bottom < y_ && bottom > (y_ - MarginSize - UnitSize - ThemeExtensions.TileItemBorderThickness.Top))
                        {
                            upItems.Add(item);
                        }
                        else if (top > y_ && top < (y_ + MarginSize + UnitSize + ThemeExtensions.TileItemBorderThickness.Left))
                        {
                            downItems.Add(item);
                        } 
                    }
                }
            }

            return new Tuple<List<TileItemViewModel>, List<TileItemViewModel>>(upItems, downItems);
        }

        public Tuple<List<TileItemViewModel>, List<TileItemViewModel>> FindVerticalAlignedItems(double x_, double y_)
        {

            var leftItems = new List<TileItemViewModel>();
            var rightItems = new List<TileItemViewModel>();
            if (tileItems.Count < 2)
            {
                return new Tuple<List<TileItemViewModel>, List<TileItemViewModel>>(leftItems, rightItems);
            }
            var start = 0d;
            var end = (double.IsNaN(Height) ? double.MaxValue : Height);
            var isPointInsideItem = false;

            foreach (var item in tileItems)
            {
                var clientRect = item.ClientBound;
                var left = clientRect.Left;
                var right = clientRect.Right;
                if (x_ >= left && x_ <= right)
                {
                    var top = clientRect.Top;
                    var bottom = clientRect.Bottom;
                    if (top > y_)
                    {
                        end = Math.Min(end, top);
                    }
                    else if (bottom < y_)
                    {
                        start = Math.Max(start, bottom);
                    }
                    else
                    {
                        isPointInsideItem = true;
                        break;
                    }
                }
            }

            if (!isPointInsideItem)
            {
                foreach (var item in tileItems)
                {
                    var clientRect = item.ClientBound; 
                    if (IsOverlapped(start, end, item.DisplayTop, item.DisplayTop + item.DisplayHeight))
                    {
                        var left = clientRect.Left;
                        var right = clientRect.Right;
                        if (right < x_ && right > (x_ - MarginSize - UnitSize - ThemeExtensions.TileItemBorderThickness.Right))
                        {
                            leftItems.Add(item);
                        }
                        else if (left > x_ && left < (x_ + MarginSize + UnitSize + ThemeExtensions.TileItemBorderThickness.Left))
                        {
                            rightItems.Add(item);
                        }
                    }
                }
            }

            return new Tuple<List<TileItemViewModel>, List<TileItemViewModel>>(leftItems, rightItems);
        }

        public void ResizeEdgeItems()
        {
            if (tileItems.Count == 0) return;
            var resizeHorizontalItems = new List<TileItemViewModel>();
            var removeHorizontalItems = new List<TileItemViewModel>();
            var resizeVerticalItems = new List<TileItemViewModel>();
            var removeVerticalItems = new List<TileItemViewModel>();

            foreach (var item in tileItems)
            {
                var clientRect = item.ClientBound;
                if (item.ShouldAlignToBottom)
                { 
                    foreach (var resizeItem in resizeVerticalItems)
                    {
                        var clientRect2 = resizeItem.ClientBound; 
                        //if current item overlapps in horizontal with any of the items to be align to bottom
                        if (IsOverlapped(clientRect.Left, clientRect.Right, clientRect2.Left, clientRect2.Right))
                        {
                            //if it's closer to the edge than other item, don't align that item to bottom
                            if (clientRect.Bottom > clientRect2.Bottom)
                            {
                                removeVerticalItems.Add(resizeItem);
                            } 
                        }
                    }

                    removeVerticalItems.ForEach(a => resizeVerticalItems.Remove(a));
                    removeVerticalItems.Clear(); 
                    resizeVerticalItems.Add(item); 
                }

                if (item.ShouldAlignToRight)
                {  
                    foreach (var resizeItem in resizeHorizontalItems)
                    {
                        var clientRect2 = resizeItem.ClientBound; 
                        //if current item overlapps in vertically with any of the items to be align to right
                        if (IsOverlapped(clientRect.Top, clientRect.Bottom, clientRect2.Top, clientRect2.Bottom))
                        {
                            //if it's closer to the edge than other item, don't align that item to right
                            if (clientRect.Right > clientRect2.Right)
                            {
                                removeHorizontalItems.Add(resizeItem);
                            } 
                        }
                    }

                    removeHorizontalItems.ForEach(a => resizeHorizontalItems.Remove(a));
                    removeHorizontalItems.Clear();
                    resizeHorizontalItems.Add(item);

                }
            }

            foreach (var item in resizeHorizontalItems)
            {
                var width = GetMaxWidth(item.DisplayLeft);
                if (width > 0)
                {
                    item.DisplayWidth = width;
                } 
            }

            foreach (var item in resizeVerticalItems)
            {
                var height = GetMaxHeight(item.DisplayTop);
                 if (height > 0)
                {
                    item.DisplayHeight = height;
                } 
            }
            
            var maximizedItem = tileItems.FirstOrDefault(a => a.IsMaximized);
            if (maximizedItem != null)
            {
                maximizedItem.Width = maximizedItem.Width;
                maximizedItem.Height = maximizedItem.Height;
            }
        }




        private static bool IsOverlapped(double start1_, double end1_, double start2_, double end2_)
        { 
            return (start1_ >= start2_ && start1_ <= end2_) ||
              (end1_ >= start2_ && end1_ <= end2_) ||
              (start2_ >= start1_ && start2_ <= end1_) ||
              (end2_ >= start1_ && end2_ <= end1_);
        }

        public double GetMaxHeight(double top_)
        {
            var height = Height - MarginSize - top_;
            //the width in dock mode on the view model is the width of the customcontentpane, not the real width of the tile view
            if (!IsFloating && !DockViewModel.UseHarmonia)
            {
                TileView content = this.Content as TileView;
                if (content == null) return height;
                double margin = 0;
                if (this.IsVisible)
                {
                    FrameworkElement pane =
                        content.FindVisualParent(o_ => o_ is IWindowViewContainerHost) as FrameworkElement;
                    if (pane != null && pane.IsVisible && pane.ActualHeight > 0)
                    {
                        margin = pane.ActualHeight - content.ActualHeight;  
                        return height - margin;
                    }
                }
                content.AccumulateMargin(ref margin, true, o_ => o_ is IWindowViewContainerHost); 
                margin += !this.viewContainer.Parameters.IsHeaderVisible ?
                       0 : ThemeExtensions.HeaderHeightDefault; 
                return height - margin;
            }

            return height;
        }

        public double GetMaxWidth(double left_)
        {
            var width = Width - MarginSize - left_;
            //the height in dock mode on the view model is the height of the customcontentpane, not the real height of the tile view
            if (!IsFloating)
            { 
                TileView content = this.Content as TileView;
                if (content == null) return width;
                double margin = 0;
                if (this.IsVisible)
                {
                    FrameworkElement pane =
                        content.FindVisualParent(o_ => o_ is IWindowViewContainerHost) as FrameworkElement;
                    if (pane != null && pane.IsVisible && pane.ActualWidth > 0)
                    {
                        margin = pane.ActualWidth - content.ActualWidth;
                        return width - margin;
                    }
                }
                content.AccumulateMargin(ref margin, false,o_=> o_ is IWindowViewContainerHost);
                return width - margin;
            }
            return width;
        }

        public Size GetActualSize()
        {
            TileView content = this.Content as TileView;
            if (!content.IsVisible) return Size.Empty;
            return content.RenderSize;
        }
    }
}
