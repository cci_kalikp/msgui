﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Controls/ViewModels/FloatingOnlyDockViewModel.cs#33 $
// $Change: 902487 $
// $DateTime: 2014/10/27 03:05:30 $
// $Author: caijin $

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Xml;
using System.Xml.Linq; 
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence.LayoutLoadingScreen;  
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager; 

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.ViewModels
{
    internal class FloatingOnlyDockViewModel : DockViewModel, IWindowManager, IActivePaneChangingHandler
    {


        private volatile bool cancelViewLoad;
        private volatile bool cancelLayoutRestore;  
        public FloatingOnlyDockViewModel(IHidingLockingManager hidingLockingManager, IUnityContainer container)
            : base(hidingLockingManager, container)
        {
 
        }

 
        void IActivePaneChangingHandler.RaiseActivePaneChanging(UIElement oldValue, UIElement newValue,
                                                                out bool cancel)
        {
            OnActivePaneChanging(this, oldValue, newValue, out cancel);
        }

  
        public void AddNewView(WindowViewContainer viewContainer_, object additionalData_)
        {
            var wsfound = Workspaces.FirstOrDefault(ws => ws.ID == viewContainer_.ID);
            if (wsfound != null) return;

            if (additionalData_ == null)
            {
                if (viewContainer_.Parameters.InitialLocation == InitialLocation.DockBottom ||
                    viewContainer_.Parameters.InitialLocation == InitialLocation.DockLeft ||
                    viewContainer_.Parameters.InitialLocation == InitialLocation.DockRight ||
                    viewContainer_.Parameters.InitialLocation == InitialLocation.DockTabbed ||
                    viewContainer_.Parameters.InitialLocation == InitialLocation.DockTop
                    )
                {
                    if (viewContainer_.Parameters.InitialLocationTarget == null)
                    {
                        additionalData_ = null;
                    }
                    else
                    {
                        var viewModel =
                            Workspaces.FirstOrDefault(
                                wm => wm.ViewContainer == viewContainer_.Parameters.InitialLocationTarget);
                        if (viewModel != null)
                            additionalData_ = viewModel.Host;
                    }
                }
            }


            WindowViewModel windowViewModel = viewContainer_.GetViewModel(HidingLockingManager);
            windowViewModel.InitialParameters = viewContainer_.Parameters;
            windowViewModel.Host = additionalData_ as ShellTabViewModel;
            windowViewModel.DragOrDropOnContent +=
                (sender_, args_) => InvokeDragOrDropOnWindow(new GenericDragOrDropOnWindowEventArgs
                    {
                        DragEventArgs = args_.EventArgs,
                        InternalEventType = args_.InternalEventType,
                        Window = windowViewModel.ViewContainer as IWindowViewContainer
                    });


            Workspaces.Add(windowViewModel);
        }

        #region Save/Load

        public const string NODE_NAME = "FloatingOnlyDock";

        public void Clean()
        {
            foreach (WindowViewModel windowViewModel in new List<WindowViewModel>(Workspaces))
            {
                if (EnableNonQuietCloseOfViews)
                {
                    windowViewModel.Close();
                }
                else
                {
                    windowViewModel.CloseQuietly();
                }
                windowViewModel.Dispose();
            }
        }


        public XDocument SaveState(Func<WindowViewContainer, XElement> buildIViewXml_)
        {
            XDocument documentWithSubitems = new XDocument();
            // <TabbedDock>
            // | <XamDockManagerLayout>
            XDocument dockLayout = SaveLayout();
            #region special handling for floating tabbed workspaces
            
            #endregion

            XElementWithSubitems rootElement = new XElementWithSubitems(NODE_NAME,
                                                                        new XElement(DockLayoutRootName, dockLayout.Root));

            // floating panes
            // | <Panes>
            XElement tablessPanesElement = new XElement("Panes");
            foreach (WindowViewModel windowViewModel in Workspaces)
            {
                if (windowViewModel != null && !windowViewModel.Transient)
                { 
                    XElement viewElement = buildIViewXml_((WindowViewContainer) windowViewModel.ViewContainer);
                    #region special handling for floating workspaces
                    //floating workspaces would opened as tabs in tabbed mdi
                    if (Bootstrapper.ShellModeUserConfigurable && !String.IsNullOrEmpty(windowViewModel.WorkspaceCategory))
                    {
                        viewElement.SetAttributeValue("TabName", viewElement.Attribute("Title").Value);
                    }
                    #endregion
                    tablessPanesElement.Add(viewElement);
                    rootElement.Subitems[windowViewModel.ID] = viewElement;
                }
            }
            rootElement.Add(tablessPanesElement);
            documentWithSubitems.Add(rootElement);
            return documentWithSubitems;
        }

        public void BeforeSaveState()
        {

        }

        public void AfterSaveState()
        {

        }

        public void LoadState(XDocument layout_, PersistenceProfileService persistenceProfileService_, Action<XDocument, object> applyNodeAction_)
        {
            persistenceProfileService_.DoDispatchedAction(Clean); 

            XElement mainElement = layout_.Element(NODE_NAME); 

            try
            {

                if (mainElement == null)
                {
                    if ((mainElement = TryLoadForeignLayout(layout_.Root, applyNodeAction_)) == null)
                    {
                        return;
                    }
                }

                XElement layoutNode = mainElement.Element(DockLayoutRootName);
                XElement tablessPanesNode = mainElement.Element("Panes");
                var layoutLoadingScreen = m_container.Resolve<LayoutLoadingScreenRetriever>().GetScreen();
                var paneCounter = 0;

                if (TrackLayoutLoadStatus)
                {
                    layoutLoadingScreen.ShowProgressBar((tablessPanesNode != null ? tablessPanesNode.Elements().Count() : 0));
                }

                if (tablessPanesNode != null)
                {
                    IEnumerable<XElement> tablessPanes = tablessPanesNode.Elements();
                    foreach (XElement node in tablessPanes)
                    {
                        if (cancelViewLoad)
                        {
                            break;
                        }
                        if (TrackLayoutLoadStatus)
                        {
                            layoutLoadingScreen.SetCurrentView(GetTitleForStatusUpdate(node));
                            layoutLoadingScreen.SetProgress(paneCounter++);  
                        }
                        applyNodeAction_(new XDocument(node), null);
                    }
                }
                if (cancelLayoutRestore) return;
                persistenceProfileService_.DoDispatchedAction(new Action(() =>
                {
                    if (layoutNode != null && layoutNode.HasElements)
                    {
                        LoadLayout(new XDocument(layoutNode.Elements().First((e) => e.NodeType == XmlNodeType.Element)));
                    }
                }));


            }
            finally
            { 
                cancelViewLoad = false;
                cancelLayoutRestore = false;
            }
        }
 
        public void CancelLoad(bool cancelLayoutRestore_)
        {
            cancelViewLoad = true;
            cancelLayoutRestore = cancelLayoutRestore_;
        }

        private XElement TryLoadForeignLayout(XElement layout_, Action<XDocument, object> applyNodeAction_)
        {
            if (layout_.Name == ConcordPersistenceStorage.EXPORTED_NODE_NAME)
            {
                string type = LayoutConverter.TryGetAttribute(layout_, "type");
                switch (type)
                {
                    case ConcordPersistenceStorage.CONCORD_WINDOW_MANAGER_NAME:
                        layout_ =
                            LayoutConverter.PaneWindowManagerToTabbedDock(new XElement("Layout", layout_.Elements()));
                        break;
                    default:
                        break;
                }
            }
            if (layout_.Name == TabbedDockViewModel.NODE_NAME)
            {


                List<XElement> panesNodes = new List<XElement> {layout_.Element("Panes")};

                foreach (var tabelem in layout_.Element("Tabs").Elements("Tab"))
                {
                    panesNodes.Add(tabelem.Element("Panes"));
                }

                var layoutLoadingScreen = m_container.Resolve<LayoutLoadingScreenRetriever>().GetScreen();
                var paneCounter = 0;

                foreach (XElement panesNode in panesNodes)
                {
                    if (panesNode != null)
                    {
                        IEnumerable<XElement> tablessPanes = panesNode.Elements();
                        if (TrackLayoutLoadStatus)
                        {
                            layoutLoadingScreen.ShowProgressBar(tablessPanes.Count());
                        }
                        foreach (XElement node in tablessPanes)
                        {
                            if (cancelViewLoad) break;
                            if (TrackLayoutLoadStatus)
                            {
                                layoutLoadingScreen.SetCurrentView(String.Format(GetTitleForStatusUpdate(node)));
                                layoutLoadingScreen.SetProgress(paneCounter++);
                            }
                            applyNodeAction_(new XDocument(node), null);
                        }
                    }
                }

                XElement layoutNode = layout_.Element(DockLayoutRootName);
                if (!cancelLayoutRestore && layoutNode != null && layoutNode.HasElements)
                {
                    var persistenceProfileService = m_container.Resolve<PersistenceProfileService>();
                    persistenceProfileService.DoDispatchedAction(() =>
                        {
                            var state =
                                new XDocument(layoutNode.Elements().First((e) => e.NodeType == XmlNodeType.Element));
                            if (!UseHarmonia)
                            {
                                FixHiddenPanes(state);
                            } 
                            LoadLayout(state);
                    }); 
                    foreach (WindowViewModel windowViewModel in Workspaces)
                    {
                        if (!windowViewModel.IsFloating)
                        {
                            windowViewModel.IsFloating = true;
                        }
                    }
                }
            }
            return null;
        }

        private static void FixHiddenPanes(XDocument state)
        {
            var layoutNodeCopied = state.Element(LayoutConverter.DockManagerRootName);
            if (layoutNodeCopied != null)
            {
                var contentPanes = layoutNodeCopied.Element("contentPanes");
                if (contentPanes != null)
                {
                    foreach (var contentPane in contentPanes.Elements("contentPane"))
                    {
                        var visibilityAttr = contentPane.Attribute("visibility");
                        if (visibilityAttr != null)
                        {
                            visibilityAttr.Remove();
                        }
                    }
                }
            }
        }


        private bool m_isLoading;

        public void BeforeLoadState()
        {
            m_isLoading = true;
        }

        public void AfterLoadState()
        {
            m_isLoading = false;
            FixInvisibleWindows();
        }

        private void FixInvisibleWindows()
        { 
            foreach (WindowViewModel workspace in Workspaces)
            {
                if (!workspace.IsFloating && workspace.IsHidden && workspace.Host == null)
                {
                    workspace.IsFloating = true;
                    workspace.IsHidden = false;
                }
            }
        }

        #endregion 
        public event EventHandler<PaneDroppedAtScreenEdgeEventArgs> PaneDroppedAtScreenEdge;
        public event EventHandler<WindowGroupDroppedOnScreenEdgeEventArgs> WindowGroupDroppedAtScreenEdge;


    }
}
