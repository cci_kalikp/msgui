﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using System.Collections.Generic;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using System.Collections.ObjectModel;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.TabWell;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
    class ShellTabViewModel : WidgetViewContainer, IDockTab, IWindowViewContainerHostRoot
    {
        public ShellTabViewModel(string factoryID, InitialWidgetParameters parameters)
            : base(factoryID, parameters)
        {
            this.headerPreControls = this.AddWidget(Guid.NewGuid().ToString(), new InitialShellTabHeaderPrePostParameters());
            this.headerPostControls = this.AddWidget(Guid.NewGuid().ToString(), new InitialShellTabHeaderPrePostParameters());
            this.ToolbarControls = this.AddWidget(Guid.NewGuid().ToString(), new InitialShellTabToolbarParameters());
        }

        public void RaiseIsActivePropertyChanged()
        {
            OnPropertyChanged("IsActive");
        }

        //protected override bool DoAddVisual(IWidgetViewContainer child)
        //{
        //    this.additionalControls.Add(child);
        //    return base.DoAddVisual(child);
        //}

        //protected override void DoRemoveVisual(IWidgetViewContainer child)
        //{
        //    this.additionalControls.Remove(child);
        //    base.DoRemoveVisual(child);
        //}

        private string m_title;
        public string Title
        {
            get
            {
                return m_title;
            }
            set
            {
                m_title = value;
                OnPropertyChanged("Title");
            }
        }

        private object m_titleData;
        public object TitleData
        {
            get { return m_titleData; }
            set { m_titleData = value; OnPropertyChanged("TitleData"); }
        }

        private object m_tooltip;
        public object Tooltip
        {
            get
            {
                return m_tooltip;
            }
            set
            {
                m_tooltip = value;
                OnPropertyChanged("Tooltip");
            }
        }

        private System.Windows.Media.Brush accentBrush;
        public System.Windows.Media.Brush AccentBrush
        {
            get
            {
                return accentBrush;
            }
            set
            {
                accentBrush = value;
                OnPropertyChanged("AccentBrush");
            }
        }

        private IWidgetViewContainer headerPreControls;
        private IWidgetViewContainer headerPostControls;
        private IWidgetViewContainer ToolbarControls;

        private bool m_isInEditMode;
        public bool IsInEditMode
        {
            get
            {
                return m_isInEditMode;
            }
            set
            {
                m_isInEditMode = value;
                OnPropertyChanged("IsInEditMode");
            }
        }

        public event EventHandler<CancelEventArgs> RequestClosing;
        private void InvokeRequestClosing(CancelEventArgs e_)
        {
            var handler = this.RequestClosing;
            if (handler != null)
                handler(this, e_);
        }

        #region CloseCommand
        private ICommand m_closeCommand;
        public ICommand CloseCommand
        {
            get
            {
                return m_closeCommand ?? (
                  m_closeCommand = new RelayCommand
                  (param_ => this.OnRequestClose(),
                   obj_ =>
                   {
                       CancelEventArgs args = new CancelEventArgs();
                       InvokeRequestClosing(args);
                       return !ShellModeExtension.HideUIGranularity.HasFlag(HideLockUIGranularity.DisableTabClose) && !args.Cancel;
                   }));
            }
        }

        public event EventHandler RequestClose;

        void OnRequestClose()
        {
            EventHandler handler = this.RequestClose;
            if (handler != null)
                handler(this, EventArgs.Empty);
        }

        public bool IsSpecial
        {
            get
            {
                return _isSpecial;
            }
            set
            {
                _isSpecial = value;
                OnPropertyChanged("IsSpecial");
            }
        }

        #endregion

        #region CloseAndDetachCommand

        private ICommand m_closeAndDetachCommand;
        public ICommand CloseAndDetachCommand
        {
            get
            {
                return m_closeAndDetachCommand ?? (m_closeAndDetachCommand = new RelayCommand(
                  param_ => this.OnRequestCloseAndDetach(),
                  obj_ =>
                  {
                      CancelEventArgs args = new CancelEventArgs();
                      InvokeRequestClosing(args);
                      return !ShellModeExtension.HideUIGranularity.HasFlag(HideLockUIGranularity.DisableTabClose) && !args.Cancel;
                  }));
            }
        }

        public event EventHandler RequestCloseAndDetach;

        void OnRequestCloseAndDetach()
        {
            EventHandler handler = this.RequestCloseAndDetach;
            if (handler != null)
                handler(this, EventArgs.Empty);
        }
        #endregion

        #region CloseOthersCommand
        private ICommand m_closeOthersCommand;
        public ICommand CloseOthersCommand
        {
            get
            {
                return m_closeOthersCommand ?? (m_closeOthersCommand = new RelayCommand(param => this.OnRequestCloseOthers(),
                    param => !ShellModeExtension.HideUIGranularity.HasFlag(HideLockUIGranularity.DisableTabClose)));
            }
        }

        public event EventHandler RequestCloseOthers;

        void OnRequestCloseOthers()
        {
            EventHandler handler = this.RequestCloseOthers;
            if (handler != null)
                handler(this, EventArgs.Empty);
        }
        #endregion
        #region CloseOthersCommand
        private ICommand m_closeAllCommand;
        public ICommand CloseAllCommand
        {
            get
            {
                return m_closeAllCommand ?? (m_closeAllCommand = new RelayCommand(param => this.OnRequestCloseAll(),
                    param => !ShellModeExtension.HideUIGranularity.HasFlag(HideLockUIGranularity.DisableTabClose)));
            }
        }

        public event EventHandler RequestCloseAll;

        void OnRequestCloseAll()
        {
            EventHandler handler = this.RequestCloseAll;
            if (handler != null)
                handler(this, EventArgs.Empty);
        }
        #endregion

        #region RenameCommand
        private ICommand m_renameCommand;
        public ICommand RenameCommand
        {
            get
            {
                return m_renameCommand ??
                    (m_renameCommand = new RelayCommand(param => this.IsInEditMode = true,
                  param => !ShellModeExtension.HideUIGranularity.HasFlag(HideLockUIGranularity.DisableTabRename)));
            }
        }
        #endregion

        private ICommand m_tabWellRightCommand;
        public ICommand TabWellRightCommand
        {
            get
            {
                return m_tabWellRightCommand ??
                       (m_tabWellRightCommand =
                        new RelayCommand(
                          param =>
                          { if (TabWellRightEvent != null) TabWellRightEvent(this, new EventArgs<ShellTabViewModel>(this)); },
                            param => !ShellModeExtension.HideUIGranularity.HasFlag(HideLockUIGranularity.DisableTabMove)));
            }
        }

        internal static event EventHandler<EventArgs<ShellTabViewModel>> TabWellRightEvent;

        private ICommand m_tabWellLeftCommand;
        public ICommand TabWellLeftCommand
        {
            get
            {
                return m_tabWellLeftCommand ??
                       (m_tabWellLeftCommand =
                        new RelayCommand(
                          param =>
                          { if (TabWellLeftEvent != null) TabWellLeftEvent(this, new EventArgs<ShellTabViewModel>(this)); },
                          param => !ShellModeExtension.HideUIGranularity.HasFlag(HideLockUIGranularity.DisableTabMove)));
            }
        }

        internal static event EventHandler<EventArgs<ShellTabViewModel>> TabWellLeftEvent;

        private ObservableCollection<IWindowViewContainer> windows = new ObservableCollection<IWindowViewContainer>();
        private bool _isSpecial;

        internal void AddWindow(IWindowViewContainer window)
        {
            this.windows.Add(window);
            
            EventHandler<Core.WindowEventArgs> handler = null;
            handler = (s, e) =>
            {
                window.Closed -= handler;
                window.ClosedQuietly -= handler;
                this.windows.Remove(window);
            };
            window.Closed += handler;
            window.ClosedQuietly += handler;
        }

        internal void RemoveWindow(IWindowViewContainer window)
        {
            this.windows.Remove(window);
        }
        #region IDockTab Members

        public ReadOnlyObservableCollection<IWindowViewContainer> Windows
        {
            get
            {
                return new ReadOnlyObservableCollection<IWindowViewContainer>(this.windows);
            }
        }

        
        public void Activate()
        {
            var p = this.Parent as TabWellRootArea;
            p.DockViewModel.SetSelectedTab(this.Title);
        }

        public IWidgetViewContainer HeaderPreItems
        {
            get
            {
                return this.headerPreControls;
            }
        }

        public ReadOnlyObservableCollection<object> headerPreItems
        {
            get
            {
                return (this.headerPreControls as WidgetViewContainer).VisualChildren;
            }
        }

        public IWidgetViewContainer HeaderPostItems
        {
            get
            {
                return this.headerPostControls;
            }
        }

        public ReadOnlyObservableCollection<object> headerPostItems
        {
            get
            {
                return (this.headerPostControls as WidgetViewContainer).VisualChildren;
            }
        }

        public IWidgetViewContainer Toolbar
        {
            get
            {
                return this.ToolbarControls;
            }
            set
            {
                this.ToolbarControls = value;
                OnPropertyChanged("Toolbar");
            }
        }

        public void Close()
        {
            this.OnRequestClose();
        }

        public bool IsActive
        {
            get
            {
                var p = this.Parent as TabWellRootArea;
                return string.Equals(p.DockViewModel.GetSelectedTabName(), Title);
                
            }
        }

        #endregion


        IEnumerable<IWindowViewContainer> IWindowViewContainerHostRoot.Windows
        {
            get { return windows; }
        }
    }
}
