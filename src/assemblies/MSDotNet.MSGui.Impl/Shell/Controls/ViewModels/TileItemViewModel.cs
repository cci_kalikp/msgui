﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Microsoft.Practices.Composite.Presentation.Commands; 
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
    internal class TileItemViewModel : WindowViewModel
    {
        private readonly IHidingLockingManager hidingLockingManager; 
        private bool isEditing; 
        private readonly TileItemViewContainer viewContainer;
        private const int IdleZIndex = 5;
        private const int DefaultZIndex = 10;
        private const int HighlightedZIndex = 20;
        private static readonly ImageSource DefaultMaximizeIcon = new BitmapImage(new Uri("pack://application:,,,/MSDotNet.MSGui.Impl;component/Resources/square.png"));
        private static readonly ImageSource DefaultCloseIcon = new BitmapImage(new Uri("pack://application:,,,/MSDotNet.MSGui.Impl;component/Resources/close.png"));
        private readonly InitialTileItemParameters parameters;
        public TileItemViewModel(TileItemViewContainer viewContainer_, IHidingLockingManager hidingLockingManager_,
                                 TileViewModel parent_):base(viewContainer_, hidingLockingManager_)
        {
            parameters = viewContainer_.Parameters;
            Parent = parent_;
            viewContainer = viewContainer_;
            hidingLockingManager = hidingLockingManager_; 
            ApplyHideUIGranularity();
            ToggleStateCommand = new DelegateCommand<object>(o_ => IsMaximized = !IsMaximized, o_ => AllowMaximize);
            CloseCommand = new DelegateCommand<object>(o_ => Close()); 
             
        }

        private void ApplyHideUIGranularity()
        {

            MaximizeButtonVisibility =
                hidingLockingManager.GetVisibilityForElement(HideLockUIGranularity.HideViewMaximizeButton);
            if (maximizeButtonVisiblity == Visibility.Visible && !parameters.AllowMaximize)
            {
                MaximizeButtonVisibility = Visibility.Collapsed;
            }
            AllowMaximize = !hidingLockingManager.Granularity.HasFlag(HideLockUIGranularity.DisableViewMaximize);
            if (AllowMaximize && !parameters.AllowMaximize)
            {
                AllowMaximize = false;
            }
        }

       

        public TileViewModel Parent { get; private set; } 

        public new InitialTileItemParameters InitialParameters { get; internal set; } 
        
        public override double Width
        {
            get { return base.Width; }
            set
            {
                base.Width = value;
                OnPropertyChanged("Width");
                OnPropertyChanged("DisplayWidth");
            }
        }

        public double DisplayWidth
        {
            get
            {
                return (IsMaximized)
                  ? (!double.IsNaN(Parent.Width) ? Parent.GetMaxWidth(Parent.MarginSize) : Width)
                  : Width;
            }
            set
            {
                if (!IsMaximized)
                {
                    Width = value;
                    OnPropertyChanged("DisplayWidth");
                }
            }
        }

        public override double Height
        {
            get { return base.Height; }
            set
            {
                base.Height = value;
                OnPropertyChanged("Height");
                OnPropertyChanged("DisplayHeight");
            }
        }

        public double DisplayHeight
        {
            get
            {
                return (IsMaximized)
                  ? (!double.IsNaN(Parent.Height) ? Parent.GetMaxHeight(Parent.MarginSize) : Height)
                  : Height;
            }
            set
            {
                if (!IsMaximized)
                {
                    Height = value;
                    OnPropertyChanged("DisplayHeight");
                }
            }
        }

        public double Left
        {
            get { return viewContainer.RelativeLeft; }
            set 
            { 
                viewContainer.RelativeLeft = value;
                OnPropertyChanged("Left");
                OnPropertyChanged("DisplayLeft");
            }
        }

        public double DisplayLeft
        {
            get { return IsMaximized ? Parent.MarginSize : Left ; }
            set
            {
                if (!IsMaximized)
                {
                    Left = value ;
                    OnPropertyChanged("DisplayLeft");
                 }
            }
        }

        public double Top
        {
            get { return viewContainer.RelativeTop; }
            set 
            {
                viewContainer.RelativeTop = value; 
                OnPropertyChanged("Top");
                OnPropertyChanged("DisplayTop");
            }
        }

        public double DisplayTop
        {
            get { return IsMaximized ? Parent.MarginSize  : Top ; }
            set
            {
                if (!IsMaximized)
                {
                    Top = value;
                    OnPropertyChanged("DisplayTop");
                }
            }
        }
         

        public bool IsEditing
        {
            get { return isEditing; }
            set 
            { 
                if (isEditing != value)
                {
                    isEditing = value;
                    if (isEditing)
                    {
                        IsMaximized = false;
                    }
                    else
                    {
                        RecordEdgeMargin();
                    }
                    OnPropertyChanged("IsEditing"); 
                }

            }
        }
         
        public double BorderOpacity
        {
            get { return IsMaximized ? 1 : 0.2; }
        }
        
        public Rect ClientBound
        {
            get
            {
                double innerWidth = DisplayWidth - ThemeExtensions.TileItemBorderThickness.Left -
                               ThemeExtensions.TileItemBorderThickness.Right;
                if (innerWidth < 0)
                {
                    innerWidth = 0;
                }
                double innerHeight = DisplayHeight - ThemeExtensions.TileItemBorderThickness.Top -
                                     ThemeExtensions.TileItemBorderThickness.Bottom;
                if (innerHeight < 0)
                {
                    innerHeight = 0;
                }
                return new Rect(DisplayLeft + ThemeExtensions.TileItemBorderThickness.Left,
                    DisplayTop + ThemeExtensions.TileItemBorderThickness.Top,
                   innerWidth, innerHeight);
            }
        }
        public override bool IsActive
        {
            get { return base.IsActive; }
            internal set
            {
                base.IsActive = value;
                OnPropertyChanged("IsActive");
                OnPropertyChanged("ZIndex");
            }
        }

        public int ZIndex { get { return IsMaximized ? HighlightedZIndex : (IsActive ? DefaultZIndex : IdleZIndex); } } 


        private Visibility maximizeButtonVisiblity;
        public Visibility MaximizeButtonVisibility
        {
            get { return maximizeButtonVisiblity; }
            set 
            { 
                maximizeButtonVisiblity = value;
                OnPropertyChanged("MaximizeButtonVisibility");
                OnPropertyChanged("TitleBarVisiblity");
            }
        }

        public Visibility TitleBarVisiblity
        {
            get
            {
                return (maximizeButtonVisiblity != Visibility.Visible && string.IsNullOrEmpty(Title) && Icon == null && 
                     HeaderItems.Count == 0 && ((HeaderItemsHolderWidgetContainer)viewContainer.Header).Items.Count == 0) ||
                     !InitialParameters.IsHeaderVisible 
                           ? Visibility.Collapsed : Visibility.Visible;
            }
        }

        private bool allowMaximize;
        public bool AllowMaximize
        {
            get { return allowMaximize; }
            set 
            { 
                allowMaximize = value;
                OnPropertyChanged("AllowMaximize");
            }
        }

        public ImageSource MaximizeIcon
        {
            get { return parameters.MaximizeIcon ??  DefaultMaximizeIcon; }
        }

        public ImageSource CloseIcon
        {
            get { return parameters.CloseIcon ?? DefaultCloseIcon; }
        }
       
        private bool isMaximized;
        public bool IsMaximized
        {
            get { return isMaximized; }
            set
            {
                if (value != isMaximized)
                {
                    isMaximized = value;
                    var copy = StateChanged;
                    if (copy != null)
                    {
                        copy(this, EventArgs.Empty);
                    }
                    if (isMaximized)
                    {
                        foreach (var tileItemViewModel in this.Parent.TileItems)
                        {
                            tileItemViewModel.IsMinimized = (tileItemViewModel != this); 
                        }
                    }
                    else
                    {
                        foreach (var tileItemViewModel in this.Parent.TileItems)
                        {
                            tileItemViewModel.IsMinimized = false;
                        }
                    }


                    OnPropertyChanged("DisplayTop");
                    OnPropertyChanged("DisplayLeft");
                    OnPropertyChanged("DisplayWidth");
                    OnPropertyChanged("DisplayHeight");
                    OnPropertyChanged("ZIndex");
                    OnPropertyChanged("BorderOpacity");
                }

            }
        }

        private bool isMinimized;
        public bool IsMinimized
        {
            get { return isMinimized; }
            set
            {
                if (value != isMinimized)
                {
                    isMinimized = value; 
                    var copy = StateChanged;
                    if (copy != null)
                    {
                        copy(this, EventArgs.Empty);
                    }
                }
            }
        }

        public double? LastRightEdgeMargin
        {
            get { return viewContainer.LastRightEdgeMargin; }
            set { viewContainer.LastRightEdgeMargin = value; }
        }

        public double? LastBottomEdgeMargin
        {
            get { return viewContainer.LastBottomEdgeMargin; }
            set { viewContainer.LastBottomEdgeMargin = value; }
        }
        internal void RecordEdgeMargin()
        {
            if (this.Parent == null) return;
            Size parentSize = this.Parent.GetActualSize();
            if (parentSize.IsEmpty || CoreUtilities.AreClose(parentSize.Width,0) || 
                CoreUtilities.AreClose(parentSize.Height, 0)) return;
            LastRightEdgeMargin = parentSize.Width - this.DisplayLeft - this.DisplayWidth;
            LastBottomEdgeMargin = parentSize.Height - this.DisplayTop - this.DisplayHeight;

        }

        public bool ShouldAlignToRight
        {
            get { return LastRightEdgeMargin == null || Math.Abs(LastRightEdgeMargin.Value) < 30; }
        }

        public bool ShouldAlignToBottom
        {
            get { return LastBottomEdgeMargin == null || Math.Abs(LastBottomEdgeMargin.Value) < 30; }
        }

        public EventHandler StateChanged;

        private KeyGesture windowRenameGesture;
        public KeyGesture WindowRenameGesture
        {
            get
            {
                return windowRenameGesture;
            }
            set
            {
                windowRenameGesture = value;
                OnPropertyChanged("WindowRenameGesture");
            }
        }

        public ICommand ToggleStateCommand { get; private set; }

        public ICommand CloseCommand { get; private set; }

        public void RefreshSize()
        {
            OnPropertyChanged("DisplayTop");
            OnPropertyChanged("DisplayLeft");
            OnPropertyChanged("DisplayWidth");
            OnPropertyChanged("DisplayHeight");
        }

    }
}
