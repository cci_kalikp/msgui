﻿ 
using System;
using System.Windows;
using System.Windows.Input;
using MorganStanley.MSDotNet.MSGui.Controls.Controls;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.ViewModels
{
    class DummyWindowViewModel : ViewModelBase, IGlobalDockInfoProvider
    {
        public HeaderStructure HeaderStructure
        {
            get
            {
                return PaneHeaderControlBase.Structure;
            } 
        }

        public double? SpacingSize { get; set; }
        public TextTrimming TitleTextTrimming { get; set; }

        public EnvironmentLabelViewModel EnvironmentLabelViewModel { get; set; }

        public Func<string[], string> ToolWindowTitleGenerator { get; set; }
        
        public KeyGesture WindowRenameGesture { get; set; }

        public bool ShowFlashBorder
        {
            get { return InitialWindowParameters.Defaults == null || InitialWindowParameters.Defaults.ShowFlashBorder; }
        }
   
        public Thickness? ShadowOffsetThickness
        {
            get
            {
                if (WindowViewModel.ShadowOffset.HasValue)
                {
                    return new Thickness(WindowViewModel.ShadowOffset.Value, WindowViewModel.ShadowOffset.Value, 0, 0);
                }
                return null;
            }
        }
 
        public bool IsFlatModeEnabled
        {
            get
            {
                return WindowViewModel.FlatModeEnabledSetting;
            }
        }

        public Thickness ContentPaddingResolved
        {
            get { return StaticContentPaddingResolved; }
        }

        public static Thickness StaticContentPaddingResolved
        {
            get { return ContentPadding ?? new Thickness(ThemeExtensions.DefaultDockContentPadding); }
        }

        public static Thickness? ContentPadding { get; set; }

        public bool AllowFloatingOnly
        {
            get { return !ShellModeExtension.HideUIGranularity.HasFlag(HideLockUIGranularity.DisableViewFloatingOnly); }
        }

        public bool CloseButtonVisibility
        {
            get { return !ShellModeExtension.HideUIGranularity.HasFlag(HideLockUIGranularity.HideViewCloseButton); }
        }

        public bool AllowDockingFloating
        {
            get { return !ShellModeExtension.HideUIGranularity.HasFlag(HideLockUIGranularity.DisableViewTearOff); }
        }


        public bool TearOffButtonVisibility
        {
            get { return !ShellModeExtension.HideUIGranularity.HasFlag(HideLockUIGranularity.HideViewTearOffButton); }
        }
         
    }
}
