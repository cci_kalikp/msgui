﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Controls/CustomPaneHeaderPresenter.cs#7 $
// $Change: 892952 $
// $DateTime: 2014/08/14 10:13:14 $
// $Author: milosp $

using System.Windows;
using Infragistics.Windows.DockManager;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
	public class CustomPaneHeaderPresenter : PaneHeaderPresenter
	{
        public Visibility CloseButtonVisibility
        {
            get { return (Visibility)GetValue(CloseButtonVisibilityProperty); }
            set { SetValue(CloseButtonVisibilityProperty, value); }
        }

        public static readonly DependencyProperty CloseButtonVisibilityProperty =
            DependencyProperty.Register("CloseButtonVisibility", typeof(Visibility), typeof(CustomPaneHeaderPresenter), new UIPropertyMetadata(System.Windows.Visibility.Visible));

		public CustomPaneHeaderPresenter()
        {
		    CloseButtonVisibility = ShellModeExtension.GetVisibilityForElement(HideLockUIGranularity.HideViewCloseButton);
        }
	}
}
