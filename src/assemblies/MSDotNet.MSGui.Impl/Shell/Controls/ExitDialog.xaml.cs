﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Controls/ExitDialog.xaml.cs#20 $
// $Change: 873824 $
// $DateTime: 2014/03/28 10:17:49 $
// $Author: caijin $

using System;
using System.Globalization;
using System.Timers;
using System.Windows;
using System.Windows.Automation.Peers;
using System.Windows.Data;
using MorganStanley.MSDotNet.MSGui.Impl.TestSupport;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
	/// <summary>
	/// Interaction logic for ExitDialog.xaml
	/// </summary>
	public partial class ExitDialog : Window, IExitDialog
	{
		public ExitDialog()
		{
			InitializeComponent();
			
			this.DontAskAgainVisiblity = ExitDialog.ShowDontAskAgainOption;
			this.SaveAndQuitVisiblity = ExitDialog.ShowSaveAndQuitOption;
		}

		public static readonly DependencyProperty ProfileNameProperty =
		   DependencyProperty.Register(
		   "ProfileName",
			typeof(string),
			typeof(ExitDialog), new PropertyMetadata(""));

		#region Timeout
		public static readonly DependencyProperty TimeoutTextProperty =
		   DependencyProperty.Register(
		   "TimeoutText",
			typeof(string),
			typeof(ExitDialog), new PropertyMetadata(""));

		public static readonly DependencyProperty TimeoutTextVisiblityProperty =
		   DependencyProperty.Register(
		   "TimeoutTextVisiblity",
			typeof(Visibility),
			typeof(ExitDialog), new PropertyMetadata(Visibility.Collapsed));

		public string TimeoutText
		{
			get
			{
				return (string)this.GetValue(ExitDialog.TimeoutTextProperty);
			}
			set
			{
				this.SetValue(ExitDialog.TimeoutTextProperty, value);
				TimeoutTextVisiblity = (value == string.Empty) ? Visibility.Collapsed : Visibility.Visible;
				this.OnPropertyChanged(new DependencyPropertyChangedEventArgs(ExitDialog.TimeoutTextProperty, this.GetValue(ExitDialog.TimeoutTextProperty), value));
			}
		}

		internal Visibility TimeoutTextVisiblity
		{
			get
			{
				return (Visibility)this.GetValue(TimeoutTextVisiblityProperty);
			}
			set
			{
				this.SetValue(TimeoutTextVisiblityProperty, value);
			}
		}

		private Timer m_timeoutTimer = new Timer(1000) { Enabled = false };
		private int? m_timeLeft = null;
		internal int? m_timeout = null;

		/// <summary>
		/// Represents the timeout setting in seconds. If null, global timeout settings will be used.
		/// </summary>
		internal int? Timeout 
		{
			get
			{
				return this.m_timeout;
			}
			set
			{
				if (value == null)
				{
					this.m_timeout = this.m_timeLeft = ExitDialog.DefaultTimeout;
				}
				else
				{
					this.m_timeout = this.m_timeLeft = value;
				}
			}
		}

		internal static int? DefaultTimeout = null;
		#endregion

		internal static string WarningBlockText = null;

		#region "Don't ask again" checkbox suppression
		internal static bool ShowDontAskAgainOption = true;

		internal static DependencyProperty DontAskAgainVisibilityProperty = DependencyProperty.Register
		(
			"DontAskAgainVisiblity", 
			typeof(Visibility), 
			typeof(ExitDialog), 
			new PropertyMetadata()
		);

		internal bool DontAskAgainVisiblity
		{
			get
			{
				return (((Visibility)this.GetValue(DontAskAgainVisibilityProperty)) == System.Windows.Visibility.Visible) ? true : false;
			}
			set
			{
				this.SetValue(DontAskAgainVisibilityProperty, value ? Visibility.Visible : Visibility.Collapsed);
			}
		}
		#endregion


		#region "Save and quit" button visibility
		internal static bool ShowSaveAndQuitOption = true;

		internal static DependencyProperty SaveAndQuitVisibilityProperty = DependencyProperty.Register
		(
			"SaveAndQuitVisiblity", 
			typeof(Visibility), 
			typeof(ExitDialog), 
			new PropertyMetadata()
		);

		internal bool SaveAndQuitVisiblity
		{
			get
			{
				return (((Visibility)this.GetValue(SaveAndQuitVisibilityProperty)) == System.Windows.Visibility.Visible) ? true : false;
			}
			set
			{
				this.SetValue(SaveAndQuitVisibilityProperty, value ? Visibility.Visible : Visibility.Collapsed);
			}
		}
		#endregion

		public string ProfileName
		{
			get
			{
				return (string)GetValue(ProfileNameProperty);
			}
			set
			{
				SetValue(ProfileNameProperty, value);
				questionblock.Text =  UsedForSubLayout ? "Save changes in current workspace?" : string.Format("Save changes in the current profile '{0}'?", value);
			}
		}

		public static readonly DependencyProperty AppNameProperty =
		   DependencyProperty.Register(
		   "AppName",
			typeof(string),
			typeof(ExitDialog), new PropertyMetadata(""));

		public string AppName
		{
			get
			{
				return (string)GetValue(AppNameProperty);
			}
			set
			{
				SetValue(AppNameProperty, value);
				Title = string.Format("Quit {0}?", value);
				warningblock.Text = ExitDialog.WarningBlockText ??
				                 (UsedForSubLayout ? 
                                 string.Format(
				                    	"You are about to close workspace '{0}'.", value):
                                 string.Format(
				                    	"You are about to close {0}. The application and all its windows will be closed.", value));
			}
		}

		public static readonly DependencyProperty DontAskProperty =
		   DependencyProperty.Register(
		   "DontAsk",
			typeof(bool),
			typeof(ExitDialog), new PropertyMetadata(false));

		public bool DontAsk
		{
			get
			{
				return (bool)GetValue(DontAskProperty);
			}
			set
			{
				SetValue(DontAskProperty, value);
			}
		}

		public bool SaveProfile
		{
			get;
			private set;
		}

        public bool UsedForSubLayout { get; set; }

		//public int AutomaticSeconds { get; set; }

		//public bool AutomaticAnswer { get; set; }

		//public bool AutomaticClosed { get; set; }

	    private bool closed = false;
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            closed = true;
        }
		private void OnYesClick(object sender, RoutedEventArgs e)
		{
            m_timeoutTimer.Elapsed -= new ElapsedEventHandler(t_Elapsed);
            this.m_timeoutTimer.Enabled = false;
			SaveProfile = true;
			this.DialogResult = true;
		}

		private void OnNoClick(object sender, RoutedEventArgs e)
		{
            m_timeoutTimer.Elapsed -= new ElapsedEventHandler(t_Elapsed);
            this.m_timeoutTimer.Enabled = false;
			SaveProfile = false;
			this.DialogResult = true;
		}

		private void exitDialog_Loaded(object sender, RoutedEventArgs e)
		{
			if (this.Timeout == null)
			{
				this.Timeout = ExitDialog.DefaultTimeout;
			}
			
			if (this.Timeout > 0)
			{
				this.m_timeoutTimer.Elapsed += new ElapsedEventHandler(t_Elapsed);
				this.m_timeoutTimer.Enabled = true;
				this.TimeoutText = UsedForSubLayout ? 
                    String.Format("The workspace will close in {0} seconds without saving the layout.", this.m_timeLeft):
                    String.Format("The application will close in {0} seconds without saving the layout.", this.m_timeLeft);
			}
			else
			{
				this.TimeoutText = "";
			}

			//if (AutomaticSeconds != -1)
			//{
			//  AutomaticClosed = false;
			//  Timer timer = new Timer
			//                  {
			//                    AutoReset = false,
			//                    Enabled = true,
			//                    Interval = AutomaticSeconds*1000
			//                  };
			//  timer.Elapsed += delegate
			//                     {
			//                       AutomaticClosed = true;
			//                       SaveProfile = AutomaticAnswer;
			//                       DialogResult = true;
			//                       Close();
			//                     };
			//  timer.Start();
			//}
		}

		void t_Elapsed(object sender, ElapsedEventArgs e)
		{	
			if (this.m_timeLeft <= 0)
			{
				this.Dispatcher.Invoke((System.Windows.Forms.MethodInvoker)delegate() 
				{
                    this.m_timeoutTimer.Enabled = false;
                    m_timeoutTimer.Elapsed -= new ElapsedEventHandler(t_Elapsed);
                    if (closed) return;
					SaveProfile = false; 
					DialogResult = true; 
					Close(); 
				});
			}
			else
			{
				this.Dispatcher.Invoke((System.Windows.Forms.MethodInvoker)delegate()
				{
					this.m_timeLeft--;
					if (m_timeLeft <= 0)
					{
						this.TimeoutText = String.Format("Closing...", this.m_timeLeft);
					}
					else
					{
						this.TimeoutText = String.Format("The application will close in {0} seconds without saving the profile.", this.m_timeLeft);
					}
				});
			}
		}

		private void OnCancelClick(object sender, RoutedEventArgs e)
		{
            m_timeoutTimer.Elapsed -= new ElapsedEventHandler(t_Elapsed);
            this.m_timeoutTimer.Enabled = false;
        }

        #region For CUIT
        protected override System.Windows.Automation.Peers.AutomationPeer OnCreateAutomationPeer()
        {
            var inner = base.OnCreateAutomationPeer();
            var peer = new RecordImageAutomationPeerWrapper(inner, this);
            return peer;
        }
 
        #endregion
    }
}