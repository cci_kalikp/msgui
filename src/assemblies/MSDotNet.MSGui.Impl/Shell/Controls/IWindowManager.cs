﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Controls/IWindowManager.cs#25 $
// $Change: 896904 $
// $DateTime: 2014/09/15 10:33:52 $
// $Author: caijin $

using System;
using System.Collections.Generic;
using System.Reflection;
using System.Windows;
using System.Xml.Linq; 
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence; 

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
    internal interface IWindowManager
    {
        void AddNewView(WindowViewContainer viewContainer_, object additionalData_);
        void Clean();
        XDocument SaveState(Func<WindowViewContainer, XElement> buildIViewXml_);
        void BeforeSaveState();
        void AfterSaveState();
        void LoadState(XDocument layout_, PersistenceProfileService persistenceProfileService_, Action<XDocument, object> applyNodeAction_);
        void BeforeLoadState();
        void AfterLoadState();
        void CancelLoad(bool cancelLayoutRestore_);
        IWindowViewContainer CreateDialog(); 
        IEnumerable<IWindowViewContainerHostRoot> ReadSubState(string subLayoutName_, XElement subLayout_, RehydratePaneDelegate callback_,
                          bool reuseExisting_ = false,  SubLayoutLocation locationOverride_ = SubLayoutLocation.SavedLocation); 
        XElement WriteSubState(IEnumerable<IWindowViewContainerHostRoot> roots_, IEnumerable<IWindowViewContainer> containers_=null, bool skipLocationInfo_ = false);
        void RegisterDockManagerHookers(Assembly hookerAssembly_);

        event EventHandler<WindowManagerActiveChangedEventArgs> ActivePaneChanged;
        event EventHandler<WindowManagerActiveChangingEventArgs> ActivePaneChanging;
        event EventHandler<GenericDragOrDropOnWindowEventArgs> DragOrDropOnWindow;
        event EventHandler<PaneDroppedAtScreenEdgeEventArgs> PaneDroppedAtScreenEdge;
        event EventHandler<WindowGroupDroppedOnScreenEdgeEventArgs> WindowGroupDroppedAtScreenEdge; 
        event EventHandler<CrossProcessDragDropEventArgs> CrossProcessDragDropAction;

    }

    internal interface IDockManagerHooker
    {
        void Hook(); 
    }

    [AttributeUsage(AttributeTargets.Class)]
    internal class DockManagerHookerAttribute:Attribute
    {
        public DockManagerHookerAttribute(bool useHarmonia_)
        {
            UseHarmonia = useHarmonia_;
        }

        public bool UseHarmonia { get; set; }
    }

    public delegate void RehydratePaneDelegate(string storedId_, string newId_);

    internal class GenericDragOrDropOnWindowEventArgs : EventArgs
    {
        public IWindowViewContainer Window { get; set; }
        public DragEventArgs DragEventArgs { get; set; }
        public RoutedEvent InternalEventType { get; set; }
    }

    internal class CrossProcessDragDropEventArgs : EventArgs
    {
        public IFloatingWindow FloatingWindow { get; set; }
        public int ProcessId { get; set; }
        public CrossProcessDragDropAction Action { get; set; }
    }

    public enum CrossProcessDragDropAction
    {
        Enter, Leave, Drop
    }

    public enum ScreenEdge
    {
        Left,
        Top,
        Right,
        Bottom
    };
}
