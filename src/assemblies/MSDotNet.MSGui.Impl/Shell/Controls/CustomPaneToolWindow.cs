﻿////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Controls/CustomPaneToolWindow.cs#93 $
// $Change: 900785 $
// $DateTime: 2014/10/14 04:41:09 $
// $Author: caijin $

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel; 
using System.Drawing; 
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Automation.Peers;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media; 
using System.Windows.Threading;
using Infragistics.Windows.Controls;
using Infragistics.Windows.DockManager; 
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Core.Win32; 
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Helpers;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.Converters;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.MicrosoftShell.Standard; 
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions; 
using MorganStanley.MSDotNet.MSGui.Impl.TestSupport;
using MorganStanley.MSDotNet.MSGui.Impl.View;
using MorganStanley.MSDotNet.My;
using Brush = System.Windows.Media.Brush;
using NativeMethods = MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.MicrosoftShell.Standard.NativeMethods;
using Point = System.Windows.Point;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
    /// <summary>
    /// This class replaces PaneToolWindow in the "reflection hack" in TabbedDock to change SupportsAllowsTransparency
    /// and allows to create global styles for panetoolwindow without affecting PaneToolWindows in
    /// other instances of XamDockManager that may exist in views and loaded concord applets
    /// </summary>
    public class CustomPaneToolWindow : PaneToolWindow, IFloatingWindow
    {
        private static IMSLogger _logger = MSLoggerFactory.CreateLogger<CustomPaneToolWindow>();

        public Guid GuidID { get; set; }

        public Window HostWindow
        {
            get { return this.Parent as Window; }
        }
        static CustomPaneToolWindow()
        {
            
        }
          #region statics

        public static readonly DependencyProperty HeaderVisibilityProperty = DependencyProperty.Register
        (
            "HeaderVisibility",
            typeof(Visibility),
            typeof(CustomPaneToolWindow),
            new UIPropertyMetadata(System.Windows.Visibility.Visible)
        );

        public static readonly RoutedUICommand ToggleTopmostCommand = new RoutedUICommand
        (
            "Toggle topmost",
            "ToggleTopmostCommand",
            typeof(CustomPaneToolWindow)
        );

        public static readonly DependencyProperty TopmostButtonVisibilityProperty = DependencyProperty.Register
        (
            "TopmostButtonVisibility",
            typeof(Visibility),
            typeof(CustomPaneToolWindow),
            new UIPropertyMetadata(Visibility.Collapsed)
        );

        public static readonly DependencyProperty ContentPaneProperty = DependencyProperty.Register
        (
            "ContentPane",
            typeof(ContentPane),
            typeof(CustomPaneToolWindow),
            new FrameworkPropertyMetadata
            (
                null,
                FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.AffectsParentMeasure,
                OnContentPaneChanged
            )
        );

        public Visibility CloseButtonVisibility
        {
            get { return (Visibility)GetValue(CloseButtonVisibilityProperty); }
            set { SetValue(CloseButtonVisibilityProperty, value); }
        }

        public static readonly DependencyProperty CloseButtonVisibilityProperty =
            DependencyProperty.Register("CloseButtonVisibility", typeof(Visibility), typeof(CustomPaneToolWindow), new UIPropertyMetadata(System.Windows.Visibility.Visible));

        #endregion

        public static readonly DependencyProperty HeaderHeightProperty =
            DependencyProperty.Register("HeaderHeight", typeof(Double), typeof(CustomPaneToolWindow), new PropertyMetadata(22.0));

        public Double HeaderHeight
        {
            get { return (Double)GetValue(HeaderHeightProperty); }
            set { SetValue(HeaderHeightProperty, value); }
        }

        public Double HeaderHeightFloating
        {
            get { return (Double)GetValue(HeaderHeightFloatingProperty); }
            set { SetValue(HeaderHeightFloatingProperty, value); }
        }

        public static readonly DependencyProperty HeaderHeightFloatingProperty =
            DependencyProperty.Register("HeaderHeightFloating", typeof(Double), typeof(CustomPaneToolWindow), new UIPropertyMetadata(22.0));


        #region properties

        public Visibility HeaderVisibility
        {
            get { return (Visibility)GetValue(HeaderVisibilityProperty); }
            set { SetValue(HeaderVisibilityProperty, value); }
        }

        public Visibility TopmostButtonVisibility
        {
            get { return (Visibility)GetValue(TopmostButtonVisibilityProperty); }
            set { SetValue(TopmostButtonVisibilityProperty, value); }
        }

        protected override bool SupportsAllowsTransparency
        {
            get
            { 
                return false;
            }
        }

        public ContentPane ContentPane
        {
            get
            {
                return (ContentPane)GetValue(ContentPaneProperty);
            }
            set
            {
                SetValue(ContentPaneProperty, value);
            }
        }

        public bool ContainsMultiple
        {
            get
            {
                return ReflHelper.PropertyGet(this, "SinglePane") as ContentPane == null;
            }
        }

        public IEnumerable<IWindowViewContainer> Windows
        {
            get { return  XamDockHelper.GetPanes(this).ConvertAll(p_ => ((IWindowViewContainerHost)p_).View); }
        }
        #endregion

        #region fields

        private bool RestrictClosingToHeaderButtonActive = false;
        private System.ComponentModel.CancelEventHandler m_closeHack;
        private Window m_window;
        private IntPtr m_mainWindowHandle;

        #endregion


        #region window placement hack on startup

        #endregion

        #region AccentColour dependency property
        public System.Windows.Media.Color AccentColour
        {
            get { return (System.Windows.Media.Color)GetValue(AccentColourProperty); }
            set { SetValue(AccentColourProperty, value); }
        }

        public static readonly DependencyProperty AccentColourProperty =
            DependencyProperty.Register("AccentColour", typeof(System.Windows.Media.Color), typeof(CustomPaneToolWindow), new UIPropertyMetadata(System.Windows.Media.Colors.Transparent));

        #endregion


        public CustomPaneToolWindow()
        {
            GuidID = Guid.NewGuid();

            Loaded += OnLoaded;
            Unloaded += OnUnloaded;
            GotFocus += OnGotFocus;
            Activated += OnActivated;

            TopmostButtonVisibility = DockViewModel.EnableTopmost ? Visibility.Visible : Visibility.Collapsed;
            MinimizeButtonVisibility = ShellModeExtension.GetVisibilityForElement(HideLockUIGranularity.HideViewMinimizeButton);
            MaximizeButtonVisibility = ShellModeExtension.GetVisibilityForElement(HideLockUIGranularity.HideViewMaximizeButton);
            CloseButtonVisibility = ShellModeExtension.GetVisibilityForElement(HideLockUIGranularity.HideViewCloseButton);

            CommandBindings.Add(new CommandBinding(ToggleTopmostCommand,
                                                        (s, e) => ToggleTopmost(),
                                                        (s, e) => e.CanExecute = true));

            #region Legacy Workspace
            CommandBindings.Add(new CommandBinding(Command.SaveSublayoutCommand, 
                                                        (sender_, args_) => ChromeManager.ChromeManagerBase.Instance.SaveWindow(this),
                                                        (s, e) => e.CanExecute = true));
            #endregion

            ContextMenuOpening += CustomPaneToolWindow_ContextMenuOpening;
            HeaderHeight = ThemeExtensions.HeaderHeightDefault;
            HeaderHeightFloating = ThemeExtensions.HeaderHeightFloatingDefault; 
        }
 

        internal void ToggleTopmost()
        {
            Topmost = !Topmost;
            XamDockHelper.DoForEveryChildContentPane(Pane, pane =>
            {
                var p = pane as CustomContentPane;
                if (p != null)
                {
                    p.Topmost = Topmost;
                }
            });
        }
         

        #region Legacy Workspace
        public string SubLayoutTitle { get; set; }
    
        #endregion

        public void ToggleOverlay(bool enable_, Brush brush_ = null, Func<FrameworkElement> controlFactory_ = null)
        {
            var visual = Template.FindName("windowRoot", this) as Grid;
            if (visual == null) return;
            if (enable_ && brush_ != null)
            {
                var adornerLayer = AdornerLayer.GetAdornerLayer(visual);
                m_overlayAdorner = new OverlayAdorner(visual, HeaderHeight) { OverlayBrush = brush_ };
                if (controlFactory_ != null) m_overlayAdorner.Child = controlFactory_.Invoke();
                adornerLayer.Add(m_overlayAdorner);
            }
            else
            {
                var adornerLayer = AdornerLayer.GetAdornerLayer(visual);
                if (m_overlayAdorner != null)
                {
                    adornerLayer.Remove(m_overlayAdorner);
                    m_overlayAdorner = null;
                }
            }
        }

        void CustomPaneToolWindow_ContextMenuOpening(object sender, System.Windows.Controls.ContextMenuEventArgs e)
        {

        }

        private void OnActivated(object sender_, EventArgs e_)
        {

        }

        private void OnGotFocus(object sender_, RoutedEventArgs e_)
        {

        }

        private void OnUnloaded(object sender_, RoutedEventArgs e_)
        {
            var tdvm = DataContext as TabbedDockViewModel;
            if (tdvm != null)
            {
                ((TabbedDockViewModel)DataContext).PropertyChanged -= OnToggleOverlayRequested;
            }
            if (m_window != null)
            {
                BindingOperations.ClearBinding(m_window, ToolWindow.TitleProperty);
            }
            //Loaded -= OnLoaded; this object can be sometimes reused and it needs to handle load event again
            Unloaded -= OnUnloaded;
            GotFocus -= OnGotFocus;
            Activated -= OnActivated; 
        }

        private void OnToggleOverlayRequested(object sender_, PropertyChangedEventArgs e_)
        {
            if (e_.PropertyName != "OverlaySettings") return;
            var settings = ((TabbedDockViewModel)DataContext).OverlaySettings;
            ToggleOverlay(settings.Enable, settings.Brush, settings.ContentFactory);
        }

        private void OnLoaded(object sender_, RoutedEventArgs e_)
        {
            UpdateWindow();
            var tdvm = DataContext as TabbedDockViewModel;
            if (tdvm != null)
            {
                if (tdvm.OverlaySettings.Enable)
                {
                    var settings = tdvm.OverlaySettings;
                    ToggleOverlay(settings.Enable, settings.Brush, settings.ContentFactory);
                }
                tdvm.PropertyChanged += OnToggleOverlayRequested;
            }
            if (FrameworkExtensions.DefaultStickWindowOptions != StickyWindowOptions.None)
            {
                new WPFStickyWindow(Window.GetWindow(this))
                {
                    StickToScreen = FrameworkExtensions.DefaultStickWindowOptions.HasFlag(StickyWindowOptions.StickToScreen),
                    StickToOther = FrameworkExtensions.DefaultStickWindowOptions.HasFlag(StickyWindowOptions.StickToOther)
                };
            } 
        }


        protected override bool OnDoubleClickCaption(Point pt_, MouseDevice mouse_)
        {
            if (AllowMaximize)
            {
                Command.MaximizeWindowCommand.Execute(null, this);
            }

            return true;
        }

        
        private void UpdateWindow()
        {
            Window oldValue = m_window;
            m_window = Window.GetWindow(this); 
            if (m_window != null)
            {
                //here so that it can be set below and can be accessed by the anon func too
                var tempTaskbarVisibility = true;
                double tempLeft = m_window.Left;
                if (m_window.WindowState == System.Windows.WindowState.Normal && FloatingWindowUtil.HideLoadingWindows)
                { 
                    this.PutOffScreen = true;
                    m_window.Left = -5000; 
                    m_window.ShowInTaskbar = false;
                    //This causes reentrancy issues, so we need to figure out a proper solution!
                    //System.Windows.Forms.Application.DoEvents();

                    EventHandler hack = null;
                    hack = (a, b) =>
                        { 
                            this.PutOffScreen = false;
                            m_window.Left = tempLeft;
                            m_window.ShowInTaskbar = tempTaskbarVisibility;
                            FloatingWindowUtil.ShowWindows -= hack;
                        };
                    FloatingWindowUtil.ShowWindows += hack;
                }

                int paneCount = XamDockHelper.CountPanes(this);
                if (FloatingWindowUtil.RestrictClosingToHeaderButton)
                {
                    this.m_closeHack = (a, b) =>
                    {
                        if (!b.Cancel)
                        {
                            b.Cancel = this.RestrictClosingToHeaderButtonActive;
                        }
                        this.RestrictClosingToHeaderButtonActive = false; 
                    };
                    this.m_window.Closing += this.m_closeHack;
                }


                if (paneCount == 1)
                {
                    //CustomContentPane ccp = (CustomContentPane)(this.Pane as SplitPane).Panes[0];
                    CustomContentPane ccp = null;

                    if (this.Pane != null)
                        ccp = XamDockHelper.FindOnlyPane(this.Pane) as CustomContentPane;

                    if (ccp != null)
                    {
                        //ViewModelBase vm = ccp.DataContext as ViewModelBase;
                        WindowViewModel wvm = ccp.DataContext as WindowViewModel;


                        if (wvm.InitialParameters.SizingMethod == SizingMethod.SizeToContent)
                            m_window.SizeToContent = SizeToContent.WidthAndHeight;
                        else
                            m_window.SizeToContent = SizeToContent.Manual;


                        if (wvm.InitialParameters.EnforceSizeRestrictions == false)
                        {
                            m_window.MaxHeight = double.PositiveInfinity;
                            m_window.MaxWidth = double.PositiveInfinity;
                        }

                        if ((wvm.InitialParameters.InitialLocation & InitialLocation.Custom) == InitialLocation.Custom &&
                            wvm.InitialParameters.InitialLocationCallback != null)
                        {
                            m_window.WindowStartupLocation = System.Windows.WindowStartupLocation.Manual;
                            var location = wvm.InitialParameters.InitialLocationCallback(this.ActualWidth,
                                                                                         this.ActualHeight);
                            this.m_window.Top = location.Top;
                            tempLeft = this.m_window.Left = location.Left;
                            if (!CoreUtilities.AreClose(this.ActualWidth, location.Width))
                                m_window.Width = location.Width;
                            if (!CoreUtilities.AreClose(this.ActualHeight, location.Height))
                                m_window.Height = location.Height;
                        }

                        /* 
                         * This applies the custom theme from the child window to the CPTW chrome. 
                         * TODO: add code to remove the styling when multiple windows are docked together or solve this some other way
                         */
                        if (wvm.InitialParameters.Resources != null)
                        {
                            this.Resources.MergedDictionaries.Add(wvm.InitialParameters.Resources);
                        }

                        ResizeMode = wvm.InitialParameters.ResizeMode;

                        /*
                         * set tempTaskbarVisibility as well so that the "restore from -5k" function does not mess up the setting
                         */
                        this.ShowInTaskbar = tempTaskbarVisibility = wvm.ShowInTaskbar;
                        if (wvm.FloatingWindowMaximumHeight.HasValue)
                        {
                            ccp.SetContentMaximumHeight(wvm.FloatingWindowMaximumHeight.Value);
                        }
                        if (wvm.FloatingWindowMaximumWidth.HasValue)
                        {
                            ccp.SetContentMaximumWidth(wvm.FloatingWindowMaximumWidth.Value);
                        }
                        if (wvm.FloatingWindowMinimumHeight > 0)
                        {
                            ccp.SetContentMinimumHeight(wvm.FloatingWindowMinimumHeight);
                        }
                        if (wvm.FloatingWindowMinimumWidth > 0)
                        {
                            ccp.SetContentMinimumWidth(wvm.FloatingWindowMinimumWidth);
                        }
                     
                    }
                }
                //m_window.SizeToContent = SizeToContent.WidthAndHeight;
                //m_window.Owner = null;
                //win.SizeToContent = SizeToContent.WidthAndHeight;
                //win.ResizeMode = ResizeMode.CanResizeWithGrip;      

                //win.Height = 500;
                //win.Width = 500;
                // win.WindowState = WindowState.Maximized;

                SplitPane val = (SplitPane)this.Content;

                //foreach (FrameworkElement frameworkElement in val.Panes)
                //{
                //  IViewContainer container = Attached.GetViewContainer(frameworkElement);
                //  if (container != null)
                //  {
                //    //if (((ViewContainer)container).FlashRequested == null)
                //    ((ViewContainer) container).RequestFlash(delegate { StartFlash(container.ID); });
                //    ((ViewContainer)container).RequestFlashStop(delegate { StopFlash(container.ID); });
                //  }
                //}

                Binding bind = new Binding
                {
                    Path = new PropertyPath(ToolWindow.TitleProperty),
                    Source = this,
                    Converter = new NullToEmptyStringConverter()
                };
                m_window.SetBinding(ToolWindow.TitleProperty, bind);
                Dispatcher.BeginInvoke(DispatcherPriority.ApplicationIdle, (Action)(() => SetIcon(this, ContentPane)));

                m_mainWindowHandle = (new WindowInteropHelper(m_window)).Handle;
                var hwndSource = HwndSource.FromHwnd(m_mainWindowHandle);
                /*
var c = ReflHelper.FieldGet(m_window, "_content");
if (c is ToolWindow)
{
    (c as ToolWindow).ResizeMode = System.Windows.ResizeMode.CanResize;
}
//hwndSource.RemoveHook(new HwndSourceHook((m_window as Infragistics.Windows.Controls.ToolWindowHostWindow).WndProc));
    */
                hwndSource.AddHook(new HwndSourceHook(MainWinProc));
                NativeMethods.EnableMenuItem(NativeMethods.GetSystemMenu(m_mainWindowHandle, false), SC.MINIMIZE, MF.DISABLED | MF.GRAYED); 
            }
            else
            {
                if (oldValue != null)
                {
                    if (this.m_closeHack != null)
                    {
                        oldValue.Closing -= this.m_closeHack;
                    }

                    BindingOperations.ClearBinding(oldValue, ToolWindow.TitleProperty);

                    var hwndSource = HwndSource.FromHwnd(m_mainWindowHandle);
                    hwndSource.RemoveHook(MainWinProc);
                    hwndSource.Dispose();
                }
            }
        }
 
       
        /// <summary>
        /// Refresh the header and headeritems of the CPTW. This is invoked when a pane drag operation has ended for floating panes.y
        /// </summary>
        /// <param name="trigger">The panes that have been moved, triggering the refresh. May not be in the current CPTW.</param>
        internal void RefreshHeaderBindings(ReadOnlyCollection<ContentPane> trigger)
        {
            var thisWindow = Window.GetWindow(this);
            if ((Window.GetWindow(trigger[0]) == thisWindow))
            // if the triggering panes are in this CPTW, update header to the first of them, and hide every child's header items
            {
                RefreshIsOwnedWindow(this);

                var ap = CustomContentPane.GetActivePane(this);
                if ((XamDockHelper.CountPanes(this) <= 1 && trigger.Contains(ap))||
                   (!ContainsMultiple && this.Pane.Panes[0] is DocumentContentHost)) 
                    return;
                 
                this.Dispatcher.Invoke(() =>
                {
                      
                    if (!FloatingWindowUtil.SeparateHeaderItemsInFloatingWindow)
                    {
                        foreach (var p in XamDockManagerExtensions.Manager.GetPanes(PaneNavigationOrder.VisibleOrder).Where(pane => Window.GetWindow(pane) == thisWindow))
                        {
                            var ccp = p as CustomContentPane;
                            if (ccp == null)
                                continue;

                            var wvm = ccp.DataContext as WindowViewModel;
                            if (wvm == null)
                                continue;

                            var hic = wvm.HeaderItems as HeaderItemsCollection;
                            if (hic == null)
                                continue;

                            hic.ReleaseCurrentParent();
                        }
                    }
                     

                    CustomContentPane.SetActivePane(this, null);
                    CustomContentPane.SetActivePane(this, trigger.Contains(ap) ? ap : trigger[0]);
                }, DispatcherPriority.Loaded);
            }
            else
            // else find a pane to use
            {
                var lastActivePane = CustomContentPane.GetActivePane(this); // get the value - may be not set if just loaded from layout
                if (lastActivePane == null || (Window.GetWindow(lastActivePane) != thisWindow)) // if not set, or it is no longer in this window, pick one
                    lastActivePane = XamDockManagerExtensions.Manager.GetPanes(PaneNavigationOrder.VisibleOrder).FirstOrDefault(pane => Window.GetWindow(pane) == thisWindow);

                if (lastActivePane != null) // if we have a window, use that
                {
                    CustomContentPane.SetActivePane(this, lastActivePane);
                }
            }
        }


        #region WindowProc hook for restore from minimize all
        private WindowState? windowStateTemp = null; //nullable so we know when did we actually set it - and be able to use it only once every time
        private bool needToRestoreWindow = false;
        private DateTime? lastMinimizeTime = null;
        private OverlayAdorner m_overlayAdorner;

        /// <summary>
        /// Add a hook to the window procedure to fix a bug occuring when restoring a minimized 
        /// group of windows from the taskbar when Windows has grouped the buttons into one.
        /// </summary>
        /// <param name="hwnd">See MSDN for WndProc specs.</param>
        /// <param name="msg">See MSDN for WndProc specs.</param>
        /// <param name="wParam">See MSDN for WndProc specs.</param>
        /// <param name="lParam">See MSDN for WndProc specs.</param>
        /// <param name="handled">See MSDN for WndProc specs.</param>
        /// <returns>See MSDN for WndProc specs.</returns>
        private System.IntPtr MainWinProc(System.IntPtr hwnd, int msg, System.IntPtr wParam, System.IntPtr lParam, ref bool handled)
        {
            #region Taskbar grouped button minimize/maximize fix
            /*
						How it works:
														
						When restoring a group of minimized windows, Windows sends the following messages in sequence:
						Message                   wParam            lParam
						WM_SYSCOMMAND    SC_MINIMIZE      
						WM_SHOWWINDOW    1 (true) SW_PARENTOPENING 
														
						When minimizing via the taskbar, we receive the following:                                                                    
						Message                   wParam            lParam
						WM_SHOWWINDOW    0 (false)        SW_PARENTCLOSING 
														
						What the hack is doing: 
														
						As the 2 messages when restoring occur very close to each other, we can filter the
						SC_MINIMIZE call that we receive before the SC_PARENTCLOSING. By doing so, we know 
						if the minimized state of the window was caused by the user, or the system doing a
						group minimize.
														
						When we get the SC_PARENTCLOSING message, we check to see if there was an 
						SC_MINIMIZE just before it. If yes, we store the window state that was before the
						SC_MINIMIZE, so we can restore it on SC_PARENTOPENING.
														
						As we can't set the window state from within the window procedure, we send a message
						via the usual way.
						*/
            if (msg == Win32.WM_SHOWWINDOW)
            {
                if ((int)wParam == 0 && (int)lParam == Win32.SW_PARENTCLOSING && lastMinimizeTime != null) // hide the window because parent is closing
                {
                    if ((DateTime.Now - (DateTime)lastMinimizeTime).TotalMilliseconds < 100)
                    //we only save the restoreTo if it's not older than 100 msec === it's from the WM_SYSCOMMAND-SC_MINIMIZE preceding a taskbar-button/right-click/minimize-all
                    {
                        needToRestoreWindow = windowStateTemp != WindowState.Minimized;
                    }
                }
                else if ((int)wParam == 1 && (int)lParam == Win32.SW_PARENTOPENING) //show the window because parent is opening
                {
                    if (needToRestoreWindow)
                    {
                        Win32.SendMessage(m_mainWindowHandle, Win32.WM_SYSCOMMAND, Win32.SC_RESTORE, 0);
                        needToRestoreWindow = false;
                    }
                }
            }
            else if (msg == Win32.WM_SYSCOMMAND)
            {
                if ((int)wParam == Win32.SC_MINIMIZE)
                {
                    lastMinimizeTime = DateTime.Now;
                    windowStateTemp = m_window.WindowState;
                }
                else if ((int)wParam == Win32.SC_RESTORE)
                {
                    Width = m_window.RestoreBounds.Width;
                    Height = m_window.RestoreBounds.Height;
                }
                else if ((int)wParam == Win32.SC_CLOSE && FloatingWindowUtil.RestrictClosingToHeaderButton)
                {
                    this.RestrictClosingToHeaderButtonActive = true;
                }
            }
            else if (msg == Win32.WM_MOUSEACTIVATE)
            {
                if (IsTitleItemsHolderItemClicked())
                {
                    handled = true;
                    return new IntPtr(Win32.MA_NOACTIVATEANDEAT);
                }
            }
            #endregion

            #region Allow the window to be put off screen 
            //work arround the VerifyIsInView call in ToolWindowHostWindow
            else if (msg == Win32.WM_WINDOWPOSCHANGING && PutOffScreen)
            {
                var windowPos = (Win32.WINDOWPOS)Marshal.PtrToStructure(lParam, typeof(Win32.WINDOWPOS));
                if (windowPos.x < -2000 ||
                    windowPos.y < -2000)
                {
                    return IntPtr.Zero;
                }
                else
                {
                    if (windowPos.x > -2000)
                    {
                        windowPos.x = -5000;
                    }
                    if (windowPos.y > -2000)
                    {
                        windowPos.y = -5000;
                    }
                    Marshal.StructureToPtr(windowPos, lParam, true);
                    handled = false;
                    return hwnd;

                }

            }
            #endregion
            return IntPtr.Zero;
        }

        internal bool PutOffScreen { get; set; }
        private bool IsTitleItemsHolderItemClicked()
        {
            var holderItemClicked = false;
            var clickedPoint = Mouse.GetPosition(m_window);
            VisualTreeHelper.HitTest(m_window,
                                                             null,
                                                             result_ =>
                                                             {
                                                                 var visualHit = result_.VisualHit;
                                                                 var holder = visualHit.FindVisualParentIncludeChild<HeaderItemsHolder>(); // object is in the holder
                                                                 if (holder != null)
                                                                 {
                                                                     holderItemClicked = true;
                                                                     var fe = visualHit as FrameworkElement;
                                                                     if (fe != null)
                                                                     {
                                                                         try
                                                                         {
                                                                             fe.RaiseEvent(
                                                                                 new MouseButtonEventArgs(
                                                                                     Mouse.PrimaryDevice,
                                                                                     1,
                                                                                     MouseButton.Left)
                                                                                     {
                                                                                         RoutedEvent = MouseDownEvent
                                                                                     });
                                                                         }
                                                                         catch (InvalidOperationException)
                                                                         {
                                                                             _logger.Warning("An exception occurred when routing mouse event for a header item.");
                                                                         }
                                                                     }
                                                                     return HitTestResultBehavior.Stop;
                                                                 }
                                                                 return HitTestResultBehavior.Continue;
                                                             },
                                                             new PointHitTestParameters(clickedPoint));
            return holderItemClicked;
        }


        #endregion
         

        private static void OnContentPaneChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            CustomPaneToolWindow cpw = (CustomPaneToolWindow)d;
            //cpw.m_window.Icon = 
            ContentPane cp = e.NewValue as ContentPane;

            SetIcon(cpw, cp);
            if ((cpw != null) && (cp is CustomContentPane)) {
                cpw.Topmost = (cp as CustomContentPane).Topmost;
            }
        }

        internal static void RefreshIsOwnedWindow(CustomPaneToolWindow cpw)
        {
            if (cpw.ContentPane != null && cpw.ContentPane.DataContext != null && cpw.ContentPane.DataContext is WindowViewModel)
            {
                cpw.IsOwnedWindow =
                    ((IWindowViewContainer) ((WindowViewModel) cpw.ContentPane.DataContext).ViewContainer)
                        .OwnedByContainer;
            }
        }

        private static void SetIcon(CustomPaneToolWindow cpw_, ContentPane cp_)
        {
            if (cpw_.m_window != null)
            {
                if (cp_ == null)
                {
                    cpw_.m_window.Icon = null;
                    return;
                }
                IViewContainer viewContainer = Attached.GetViewContainer(cp_);
                if (viewContainer == null)
                {
                    cpw_.m_window.Icon = null;
                    return;
                }

                Icon icon = viewContainer.Icon;
                if (icon != null)
                { 
                    cpw_.m_window.Icon = icon.ToImageSource();
                }
                else
                {
                    cpw_.m_window.Icon = null;
                }
            }
        }



        //public void StartFlash(string id_)
        //{
        //    object o = TryFindResource("FlashWindow");
        //    if (o != null)
        //    {
        //        Storyboard sb = o as Storyboard;
        //        FrameworkElement q = null;
        //        foreach (var z in PrivateExtensions.FindInVisualTree(this, y => y.GetValue(
        //            FrameworkElement.NameProperty)
        //                                                                            .ToString() ==
        //                                                                        "windowInnerBorder"))
        //        {
        //            if (z.GetParentObject().GetParentObject().GetValue(NameProperty).ToString()
        //                == "contentPane" + id_)
        //            q = z.GetParentObject() as FrameworkElement;
        //        }
        //        try{
        //    sb.Begin(q, true); 
        //            }
        //        catch (Exception)
        //        {
        //        }
        //    }
        //}

        //public void StopFlash(string id_)
        //{
        //    object o = TryFindResource("FlashWindow");
        //    if (o != null)
        //    {
        //        Storyboard sb = o as Storyboard;
        //        FrameworkElement q = null;
        //        foreach (var z in PrivateExtensions.FindInVisualTree(this, y => y.GetValue(
        //            FrameworkElement.NameProperty)
        //                                                                            .ToString() ==
        //                                                                        "windowInnerBorder"))
        //        {
        //            if (z.GetParentObject().GetParentObject().GetValue(NameProperty).ToString()
        //                == "contentPane" + id_)
        //                q = z.GetParentObject() as FrameworkElement;
        //        }
        //        try
        //        {
        //            sb.Stop(q);
        //        }
        //        catch (Exception)
        //        {
        //        }
        //    }

        //}
        //public bool Flash
        //{
        //  get { return (bool)GetValue(FlashProperty); }
        //  set
        //  {
        //    //if (value)
        //    //{ SetValue(FlashProperty, false); }
        //    SetValue(FlashProperty, value);
        //  }
        //}

        //public static readonly DependencyProperty FlashProperty =
        //  DependencyProperty.Register("Flash", typeof (bool), typeof (CustomPaneToolWindow),
        //                              new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.AffectsRender));


        #region for CUIT

        

        protected override System.Windows.Automation.Peers.AutomationPeer OnCreateAutomationPeer()
        {
            var inner = base.OnCreateAutomationPeer();
            var peer = new CustomToolWindowAutomationPeer(inner, this);
            return peer;
        }
        
        private class CustomToolWindowCommandsHandler: ImagePathHandler
        {
            public CustomToolWindowCommandsHandler()
            {
                Handlers[ToolWindowSupportedCommands.MoveTo.ToString()] = (parameters) =>
                                                                   {
                                                                       double top, left;
                                                                       if (!double.TryParse(parameters[0], out left) ||
                                                                           !double.TryParse(parameters[1], out top))
                                                                           return "";

                                                                       var peer =
                                                                           this.AutomationPeer as
                                                                           CustomToolWindowAutomationPeer;
                                                                       var tw = peer.Owner as CustomPaneToolWindow;
                                                                       tw.Top = top;
                                                                       tw.Left = left;
                                                                       return "";
                                                                   };
            }
           
        }

        private class CustomToolWindowAutomationPeer : RecordImageAutomationPeerWrapper
        {
            public CustomToolWindowAutomationPeer(AutomationPeer inner, FrameworkElement owner) : base(inner, owner, new CustomToolWindowCommandsHandler())
            {
                RecordImage();
            }

            private CustomPaneToolWindow MyOwner { get { return (CustomPaneToolWindow)Owner; } }

            protected override string GetAutomationIdCore()
            {
                var thisWindow = Window.GetWindow(MyOwner);
                var ids = "";
                foreach (var p in XamDockManagerExtensions.Manager.GetPanes(PaneNavigationOrder.VisibleOrder).Where(pane => Window.GetWindow(pane) == thisWindow))
                {
                    var ccp = p as CustomContentPane;
                    if (ccp == null)
                        continue;

                    var wvm = ccp.DataContext as WindowViewModel;
                    if (wvm == null)
                        continue;



                    var viewId = wvm.ViewContainer.ID;
                    ids += viewId + ";";

                }
                return ids;
            }

            protected override string GetItemStatusCore()
            {
                var thisWindow = Window.GetWindow(MyOwner);
                var ids = "";
                foreach (var p in XamDockManagerExtensions.Manager.GetPanes(PaneNavigationOrder.VisibleOrder).Where(pane => Window.GetWindow(pane) == thisWindow))
                {
                    var ccp = p as CustomContentPane;
                    if (ccp == null)
                        continue;

                    var wvm = ccp.DataContext as WindowViewModel;
                    if (wvm == null)
                        continue;



                    var viewId = wvm.ViewContainer.ID;
                    ids += viewId + ";";

                }
                return ids;
            }

            protected override string GetClassNameCore()
            {
                return typeof(CustomPaneToolWindow).Name;
            }


            public override object GetPattern(PatternInterface patternInterface)
            {

                switch (patternInterface)
                {
                    case PatternInterface.Invoke:
                        return this;


                    default:
                        return base.GetPattern(patternInterface);
                }

            }
        }
        #endregion
         
        
    }
}
