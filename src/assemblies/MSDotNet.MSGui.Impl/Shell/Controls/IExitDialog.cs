﻿using System;
using System.Windows;
namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
	public interface IExitDialog
	{
		/// <summary>
		/// Application name that will be used in dialog's title
		/// </summary>
		string AppName { get; set; }
		/// <summary>
		/// Indicates if the user doesn't want this dialog to display next time.
		/// Set by the dialog.
		/// </summary>
		bool DontAsk { get; set; }
		/// <summary>
		/// Profile's name that will be used in the dialog
		/// </summary>
		string ProfileName { get; set; }
		/// <summary>
		/// Indicates if the user wants to save the profile.
		/// Set by the dialog.
		/// </summary>
		bool SaveProfile { get; }
		/// <summary>
		/// Owning window.
		/// </summary>
		Window Owner { get; set; }
		/// <summary>
		/// Shows the dialog, blocks until the dialog is closed.
		/// </summary>
		/// <returns>True if the application should be closed.</returns>
		bool? ShowDialog();

        bool UsedForSubLayout { get; set; }
	}
}
