﻿/**************************************************************************\
    Copyright Microsoft Corporation. All Rights Reserved.
\**************************************************************************/

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.MicrosoftShell
{
    public class JumpPath : JumpItem
    {
        public JumpPath()
        {}

        public string Path { get; set; }
    }
}
