﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Runtime;
using System.Runtime.Serialization;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.MicrosoftShell.Standard
{
    
    [Serializable]
    internal class HttpValueCollection : NameValueCollection
    {
        internal HttpValueCollection()
            : base(StringComparer.OrdinalIgnoreCase)
        {
        }
        internal HttpValueCollection(HttpValueCollection col)
            : base(StringComparer.OrdinalIgnoreCase)
        {
            for (int i = 0; i < col.Count; i++)
            {
                string name = col.BaseGetKey(i);
                object value = col.BaseGet(i);
                base.BaseAdd(name, value);
            }
            base.IsReadOnly = col.IsReadOnly;
        }
        internal HttpValueCollection(string str)
            : base(StringComparer.OrdinalIgnoreCase)
        {
            if (!string.IsNullOrEmpty(str))
            {
                this.FillFromString(str);
            }
        }
        internal HttpValueCollection(int capacity)
            : base(capacity, StringComparer.OrdinalIgnoreCase)
        {
        }
        [TargetedPatchingOptOut("Performance critical to inline this type of method across NGen image boundaries")]
        protected HttpValueCollection(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
        internal static bool KeyIsCandidateForValidation(string key)
        {
            return key == null || !key.StartsWith("__", StringComparison.Ordinal);
        }
        
        internal void MakeReadOnly()
        {
            base.IsReadOnly = true;
        }
        internal void MakeReadWrite()
        {
            base.IsReadOnly = false;
        }

        internal void FillFromString(string s)
        {
            int num = (s != null) ? s.Length : 0;
            for (int i = 0; i < num; i++)
            {
               
                int num2 = i;
                int num3 = -1;
                while (i < num)
                {
                    char c = s[i];
                    if (c == '=')
                    {
                        if (num3 < 0)
                        {
                            num3 = i;
                        }
                    }
                    else
                    {
                        if (c == '&')
                        {
                            break;
                        }
                    }
                    i++;
                }
                string text = null;
                string text2;
                if (num3 >= 0)
                {
                    text = s.Substring(num2, num3 - num2);
                    text2 = s.Substring(num3 + 1, i - num3 - 1);
                }
                else
                {
                    text2 = s.Substring(num2, i - num2);
                } 
                base.Add(Utility.UrlDecode(text), Utility.UrlDecode(text2));
                if (i == num - 1 && s[i] == '&')
                {
                    base.Add(null, string.Empty);
                }
            }
        }
       
        internal void Reset()
        {
            base.Clear();
        }
       
    }
}
