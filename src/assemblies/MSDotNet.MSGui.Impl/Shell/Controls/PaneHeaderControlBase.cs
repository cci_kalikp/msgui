﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Forms.Integration;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading; 
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;  
using MorganStanley.MSDotNet.MSGui.Impl.Interop; 

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
    [TemplatePart(Name = TitlePartName, Type = typeof(TextBlock))]
    [TemplatePart(Name = HeaderImagePartName, Type = typeof(Image))]
    public class PaneHeaderControlBase : UserControl
    {
        internal static HeaderStructure Structure = HeaderStructure.WindowsClassic;

        private const string TitlePartName = "PART_Title";
        private const string HeaderImagePartName = "PART_Image";
        private const string HeaderItemsLeftPartName = "PART_HeaderItems_Left";
        private const string HeaderItemsRightPartName = "PART_HeaderItems_Right";
         
        public PaneHeaderControlBase()
        {
            this.SizeChanged += PaneHeaderControlBase_SizeChanged;
        }

        void PaneHeaderControlBase_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (this.ShowCustomItems && this.CustomItems != null && this.CustomItems.Count > 0)
            {
                foreach (var item in this.CustomItems)
                {
                    FrameworkElement element = item as FrameworkElement;
                    if (element == null) continue;
                    WindowsFormIntegrationHelper.ClipWindowsFormsHostIfNeeded(element, this);
                     
                } 
            }
        }
        
        public bool ShowEnvironmentLabel
        {
            get { return (bool)GetValue(ShowEnvironmentLabelProperty); }
            set { SetValue(ShowEnvironmentLabelProperty, value); }
        }

        public TextTrimming TitleTextTrimming
        {
            get { return (TextTrimming)GetValue(TitleTextTrimmingProperty); }
            set { SetValue(TitleTextTrimmingProperty, value); }
        }

        public static readonly DependencyProperty TitleTextTrimmingProperty =
            DependencyProperty.Register("TitleTextTrimming", typeof(TextTrimming), typeof(PaneHeaderControlBase), new UIPropertyMetadata(TextTrimming.None));

        public static readonly DependencyProperty ShowEnvironmentLabelProperty =
            DependencyProperty.Register("ShowEnvironmentLabel", typeof(bool), typeof(PaneHeaderControlBase), new UIPropertyMetadata(true));

       
         

        public static readonly DependencyPropertyKey CustomItemsProperty =
            DependencyProperty.RegisterReadOnly("CustomItems", typeof(ObservableCollection<object>), typeof(PaneHeaderControlBase), new UIPropertyMetadata(null));



        public bool ShowCustomItems
        {
            get { return (bool)GetValue(ShowCustomItemsProperty); }
            set { SetValue(ShowCustomItemsProperty, value); }
        }

        public static readonly DependencyProperty ShowCustomItemsProperty =
            DependencyProperty.Register("ShowCustomItems", typeof(bool), typeof(PaneHeaderControlBase), new UIPropertyMetadata(true));

        // TODO FARM this may be unused
        //public static readonly DependencyProperty SystemButtonsProperty =
        //    DependencyProperty.Register("SystemButtons", typeof(FrameworkElement), typeof(PaneHeaderControl), new UIPropertyMetadata(null));

        //public FrameworkElement SystemButtons
        //{
        //    get { return (FrameworkElement)GetValue(SystemButtonsProperty); }
        //    set { SetValue(SystemButtonsProperty, value); }
        //}


        #region System button visibility properties

        public static readonly DependencyProperty ShowHeaderTextProperty =
            DependencyProperty.Register("ShowHeaderText", typeof(bool), typeof(PaneHeaderControlBase), new UIPropertyMetadata(true));
        public static readonly DependencyProperty ShowTearOffButtonProperty =
            DependencyProperty.Register("ShowTearOffButton", typeof(bool), typeof(PaneHeaderControlBase), new UIPropertyMetadata(true));


        public static readonly DependencyProperty ShowMinimizeButtonProperty =
            DependencyProperty.Register("ShowMinimizeButton", typeof(bool), typeof(PaneHeaderControlBase), new UIPropertyMetadata(true));
        public static readonly DependencyProperty ShowMinMaxButtonProperty =
            DependencyProperty.Register("ShowMinMaxButton", typeof(bool), typeof(PaneHeaderControlBase), new UIPropertyMetadata(true));
        public static readonly DependencyProperty ShowTopmostButtonProperty =
            DependencyProperty.Register("ShowTopmostButton", typeof(bool), typeof(PaneHeaderControlBase), new UIPropertyMetadata(true));
        public static readonly DependencyProperty ShowPinButtonProperty =
            DependencyProperty.Register("ShowPinButton", typeof(bool), typeof(PaneHeaderControlBase), new UIPropertyMetadata(true));
        public static readonly DependencyProperty ShowCloseButtonProperty =
            DependencyProperty.Register("ShowCloseButton", typeof(bool), typeof(PaneHeaderControlBase), new UIPropertyMetadata(true));
        public static readonly DependencyProperty PaneHeaderStructureProperty =
            DependencyProperty.Register("PaneHeaderStructure", typeof(HeaderStructure), typeof(PaneHeaderControlBase), new PropertyMetadata(HeaderStructure.WindowsClassic));

        // Using a DependencyProperty as the backing store for ShowIcon.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ShowIconProperty =
            DependencyProperty.Register("ShowIcon", typeof(bool), typeof(PaneHeaderControlBase), new UIPropertyMetadata(true));



        public bool ShowTearOffButton
        {
            get { return (bool)GetValue(ShowTearOffButtonProperty); }
            set { SetValue(ShowTearOffButtonProperty, value); }
        }

        public bool ShowMinimizeButton
        {
            get { return (bool)GetValue(ShowMinimizeButtonProperty); }
            set { SetValue(ShowMinimizeButtonProperty, value); }
        }

        public bool ShowMinMaxButton
        {
            get { return (bool)GetValue(ShowMinMaxButtonProperty); }
            set { SetValue(ShowMinMaxButtonProperty, value); }
        }

        public bool ShowTopmostButton
        {
            get { return (bool)GetValue(ShowTopmostButtonProperty); }
            set { SetValue(ShowTopmostButtonProperty, value); }
        }

        public bool ShowPinButton
        {
            get { return (bool)GetValue(ShowPinButtonProperty); }
            set { SetValue(ShowPinButtonProperty, value); }
        }

        public bool ShowHeaderText
        {
            get { return (bool)GetValue(ShowHeaderTextProperty); }
            set { SetValue(ShowHeaderTextProperty, value); }
        }


        public bool ShowIcon
        {
            get { return (bool)GetValue(ShowIconProperty); }
            set { SetValue(ShowIconProperty, value); }
        }


        public bool ShowCloseButton
        {
            get { return (bool)GetValue(ShowCloseButtonProperty); }
            set { SetValue(ShowCloseButtonProperty, value); }
        }
        public HeaderStructure PaneHeaderStructure
        {
            get { return (HeaderStructure)GetValue(PaneHeaderStructureProperty); }
            set { SetValue(PaneHeaderStructureProperty, value); }
        }
        #endregion

        internal ObservableCollection<object> CustomItems
        {
            get { return (ObservableCollection<object>)GetValue(CustomItemsProperty.DependencyProperty); }
            private set { SetValue(CustomItemsProperty, value); }
        }

        public static readonly DependencyProperty MinMaxCommandProperty =
            DependencyProperty.Register("MinMaxCommand", typeof(RoutedCommand), typeof(PaneHeaderControlBase), new UIPropertyMetadata(null));
        public static readonly DependencyProperty CloseCommandProperty =
            DependencyProperty.Register("CloseCommand", typeof(RoutedCommand), typeof(PaneHeaderControlBase), new UIPropertyMetadata(null));

        public RoutedCommand MinMaxCommand
        {
            get { return (RoutedCommand)GetValue(MinMaxCommandProperty); }
            set { SetValue(MinMaxCommandProperty, value); }
        }
        public RoutedCommand CloseCommand
        {
            get { return (RoutedCommand)GetValue(CloseCommandProperty); }
            set { SetValue(CloseCommandProperty, value); }
        }

        protected EditableTextBlock TitlePart;
        protected Image ImagePart;
        private HeaderItemsHolder headerItemsLeft;

        public static readonly DependencyProperty ContentObjectProperty = DependencyProperty.Register(
   "ContentObject",
   typeof(object),
   typeof(PaneHeaderControlBase),
   new FrameworkPropertyMetadata(
       null,
       FrameworkPropertyMetadataOptions.AffectsRender |
       FrameworkPropertyMetadataOptions.AffectsParentMeasure, OnContentObjectChanged
       )
   );
        public object ContentObject
        {
            get { return GetValue(ContentObjectProperty); }
            set { SetValue(ContentObjectProperty, value); }
        }
         

        internal EditableTextBlock TitleControl { get { return this.TitlePart; } }
        protected virtual bool ResolveShowCustomItems()
        {
            return ShowCustomItems;
        }
        internal protected virtual void BindContentObject()
        {
            this.Clear(); 

            var container = this.Container;
            if (container != null && ResolveShowCustomItems())
            {
                var widgetContainer = container.Header as HeaderItemsHolderWidgetContainer;
                if (widgetContainer != null)
                    this.CustomItems = widgetContainer.Items;
                Binding headerStructureBinding = new Binding()
                {
                    Source = container,
                    Path = new PropertyPath("HeaderStructure")
                };
                this.SetBinding(PaneHeaderStructureProperty, headerStructureBinding);
            }
            DependencyPropertyDescriptor.FromProperty(BackgroundProperty, typeof(PaneHeaderControlBase)).AddValueChanged(this, BackgroundChanged);
            if (container != null)
            {
                container.Closed -= container_Closed;
                container.ClosedQuietly -= container_Closed;
                container.Closed += container_Closed;
                container.ClosedQuietly += container_Closed;
            }
        }
        private static void OnContentObjectChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var paneHeaderControl = d as PaneHeaderControlBase;
            if (paneHeaderControl == null || e.NewValue == e.OldValue)
                return;
            if (e.NewValue == null)
            {
                paneHeaderControl.Clear();
                return;
            }
            paneHeaderControl.BindContentObject();
             
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            this.TitlePart = GetTemplateChild(TitlePartName) as EditableTextBlock;
            this.headerItemsLeft = GetTemplateChild(HeaderItemsLeftPartName) as HeaderItemsHolder;

            this.ImagePart = GetTemplateChild(HeaderImagePartName) as Image;
            
            if (ContentObject == null)
            {
                Clear(); 
            }
            else
            {
                BindContentObject();
            }
            

        }
         
        void container_Closed(object sender, WindowEventArgs e)
        {
            DependencyPropertyDescriptor.FromProperty(BackgroundProperty, typeof(PaneHeaderControlBase))
                                        .RemoveValueChanged(this, BackgroundChanged);
            e.ViewContainer.Closed -= container_Closed;
            e.ViewContainer.ClosedQuietly -= container_Closed;

        }

        private void BackgroundChanged(object sender_, EventArgs eventArgs_)
        {
            Dispatcher.BeginInvoke(new Action(() =>
                {
                    if (this.ShowCustomItems && this.CustomItems != null && this.CustomItems.Count > 0)
                    {
                        foreach (var customItem in this.CustomItems)
                        {
                            DependencyObject dpo = customItem as DependencyObject;
                            if (dpo != null)
                            {
                                WindowsFormIntegrationHelper.NotifyBackgroundChanged(dpo);
                            }
                        }
                    }
                }), DispatcherPriority.Background);

        }

        protected virtual void Clear()
        {
            // clear refs to viewContainer
            DependencyPropertyDescriptor.FromProperty(BackgroundProperty, typeof(PaneHeaderControlBase)).RemoveValueChanged(this, BackgroundChanged);
            this.CustomItems = null;

            if (this.TitlePart != null)
            {
                BindingOperations.ClearBinding(TitlePart, TextBlock.TextProperty);
                BindingOperations.ClearBinding(TitlePart, UIElement.VisibilityProperty);
            }

            if (this.ImagePart != null)
            {
                BindingOperations.ClearBinding(ImagePart, Image.SourceProperty);
                BindingOperations.ClearBinding(ImagePart, FrameworkElement.MarginProperty);
            }  
        }

        internal virtual WindowViewModel ContentModel
        {
            get { return ContentObject as WindowViewModel; }
        }

        internal virtual IWindowViewContainer Container
        {
            get { return ContentModel == null ? null : ContentModel.ViewContainer as IWindowViewContainer; }
        }

    }



    public class CaptionMarginConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter,
                              System.Globalization.CultureInfo culture)
        {
            var column0 = values[0] as ColumnDefinition;
            var column1 = values[1] as ColumnDefinition;
            var column2 = values[2] as ColumnDefinition;
            var column3 = values[3] as ColumnDefinition;
            var phc = values[4] as PaneHeaderControlBase;

            if (column0 != null && column1 != null && phc != null)
            {
                var model = phc.ContentModel;
                var headerStructure = model == null ? PaneHeaderControlBase.Structure : model.HeaderStructure;
                double titleWidth, offsetForCenter;
                switch (headerStructure)
                {
                    case HeaderStructure.WindowsClassic:
                        return new Thickness(column0.ActualWidth + column1.ActualWidth + 4, 0, 0, 0);

                    case HeaderStructure.WindowsModern:
                        titleWidth = 0;

                        if (phc.TitleControl != null)
                        {
                            var uic = CultureInfo.CurrentUICulture;
                            var ft = new FormattedText(phc.TitleControl.Text, uic,
                                                       uic.TextInfo.IsRightToLeft
                                                           ? FlowDirection.RightToLeft
                                                           : FlowDirection.LeftToRight,
                                                       new Typeface(phc.TitleControl.FontFamily, phc.TitleControl.FontStyle,
                                                                    phc.TitleControl.FontWeight, phc.TitleControl.FontStretch),
                                                       phc.TitleControl.FontSize, phc.TitleControl.Foreground);
                            titleWidth = ft.Width;
                        }
                        offsetForCenter = (phc.ActualWidth - titleWidth) / 2.0;
                        var maxOffset = column0.ActualWidth + column1.ActualWidth + column2.ActualWidth +
                                        column3.ActualWidth - titleWidth - 4 /* margin simulated with const, else it would mess up centering */;
                        return new Thickness(Math.Min(offsetForCenter, maxOffset), 0, 0, 0);

                    case HeaderStructure.Office2007:
                        titleWidth = 0;

                        if (phc.TitleControl != null)
                        {
                            var uic = CultureInfo.CurrentUICulture;
                            var ft = new FormattedText(phc.TitleControl.Text, uic,
                                                       uic.TextInfo.IsRightToLeft
                                                           ? FlowDirection.RightToLeft
                                                           : FlowDirection.LeftToRight,
                                                       new Typeface(phc.TitleControl.FontFamily, phc.TitleControl.FontStyle,
                                                                    phc.TitleControl.FontWeight, phc.TitleControl.FontStretch),
                                                       phc.TitleControl.FontSize, phc.TitleControl.Foreground);
                            titleWidth = ft.Width;
                        }
                        offsetForCenter = (phc.ActualWidth - titleWidth) / 2.0;
                        var leftItemsWidth = column0.ActualWidth + column1.ActualWidth + column2.ActualWidth + 4 /* margin simulated with const, else it would mess up centering */;
                        return new Thickness(Math.Max(offsetForCenter, leftItemsWidth), 0, 0, 0);
                }
            }

            return new Thickness(0); // if anything fails, put the caption on the left
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }

    public class ImageMarginConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value != null ? new Thickness(2, 0, 2, 0) : new Thickness(0);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }


    public class HeaderItemsConverter : IMultiValueConverter
    {

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var phc = values[0] as PaneHeaderControlBase;
            var model = phc.ContentModel;
            var headerStructure = model == null ? PaneHeaderControlBase.Structure : model.HeaderStructure;

            if (headerStructure == HeaderStructure.WindowsClassic || headerStructure == HeaderStructure.WindowsModern)
                return (parameter.Equals("right")) ? phc.CustomItems : null;
            else
                return (parameter.Equals("right")) ? null : phc.CustomItems;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }


    public class VisibilityToBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (value is Visibility) && (Visibility)value == Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}