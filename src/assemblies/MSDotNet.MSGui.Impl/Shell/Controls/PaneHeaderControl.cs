﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using Infragistics.Windows.Controls;
using Infragistics.Windows.DockManager;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions; 

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{ 
    public class PaneHeaderControl : PaneHeaderControlBase
    { 
        private MultipaneHeaderValueConverter multipaneHeaderValueConverter;
        private ContentPane boundPane;
        private RoutedEventHandler titleNeedChangingHandler; 

        static PaneHeaderControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(PaneHeaderControl),
                                         new FrameworkPropertyMetadata(typeof(PaneHeaderControl)));

        }
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            var dockViewModel = DataContext as DockViewModel;
            if (dockViewModel != null && dockViewModel.ToolWindowTitleGenerator != null)
            {
                multipaneHeaderValueConverter = new MultipaneHeaderValueConverter(dockViewModel.ToolWindowTitleGenerator);
            } 
        }
 
 
        private bool IsTopHeader
        {
            get { return DataContext as DockViewModel != null; }
        }

        private CustomPaneToolWindow GetAttachedWindow()
        {
            DependencyObject vchcParent = this.Parent;
            while (vchcParent != null)
            {
                var paneParent = vchcParent as CustomContentPane;
                if (paneParent != null)
                {
                    paneParent.HeaderControl = this;
                    return null;
                }
                var window = vchcParent as CustomPaneToolWindow;
                if (window != null)
                {
                    return window;
                }
                vchcParent = VisualTreeHelper.GetParent(vchcParent);
            }
            return null;
        }
        protected override bool ResolveShowCustomItems()
        {
          
            var showCustomItemResolved = this.ShowCustomItems;

            //hide contentpane header in floating windows when separated header items is not enabled
            if (!IsTopHeader && showCustomItemResolved &&
                !FloatingWindowUtil.SeparateHeaderItemsInFloatingWindow)
            {
                var pane = ContentObject as ContentPane;
                showCustomItemResolved = pane != null && !pane.IsFloating();
            }

            return showCustomItemResolved;
        }

        internal protected override void BindContentObject()
        {

            base.BindContentObject();
            this.boundPane = ContentObject as ContentPane;
            DockViewModel model = DataContext as DockViewModel;
            if (model != null && model.EnvironmentSpecified)
            {
                ShowEnvironmentLabel = true;
            }
            else
            {
                ShowEnvironmentLabel = false;
            }
                
            if (this.TitlePart != null)
            {
                var headerBinding = new Binding()
                {
                    Source = this.ContentObject,
                    Path = new PropertyPath("Header"),
                    Mode = BindingMode.TwoWay
                };

                var headerVisibilityBinding = new Binding
                {
                    Source = this,
                    Path = new PropertyPath("ShowHeaderText"),
                    Converter = new BooleanToVisibilityConverter()
                };
                TitlePart.SetBinding(TextBlock.TextProperty, headerBinding);
                TitlePart.SetBinding(UIElement.VisibilityProperty, headerVisibilityBinding);
            }
            if (ImagePart != null && ShowIcon)
            {
                if (ShellModeExtension.HideUIGranularity.HasFlag(HideLockUIGranularity.HideHeaderIcon))
                    ImagePart.Visibility = Visibility.Hidden;
                else
                {
                    Binding imageBinding = new Binding()
                    {
                        Source = ContentObject,
                        Path = new PropertyPath("Image"), 
                    };
                    ImagePart.SetBinding(Image.SourceProperty, imageBinding);


                    Binding imageMarginBinding = new Binding()
                    {
                        Source = ContentObject,
                        Path = new PropertyPath("Image"),
                        Converter = new ImageMarginConverter()
                    };
                    ImagePart.SetBinding(MarginProperty, imageMarginBinding);
                }
            }

            var window = GetAttachedWindow();
            IWindowViewContainer windowViewContainer = this.Container; 
            //do not allow rename a workspace container
            if (windowViewContainer != null && !string.IsNullOrEmpty(windowViewContainer.Parameters.WorkspaceCategory)) return;

            this.titleNeedChangingHandler = (x, y) =>
            {
                if (TitlePart != null && TitlePart.IsVisible)
                {
                    TitlePart.IsInEditMode = true;
                }
            };  
 
            /*
             * This figures out if this ViewContainerHeaderControl is owned by a CustomPaneToolWindow and not a 
             * CustomContentPane. We need to know this so that we don't enable renaming of the floating container 
             * window if it has multiple children.
             */


            if (IsTopHeader && window != null)
            {
                if (XamDockHelper.CountPanes(window) == 1)
                {
                    Attached.AddTitleNeedChangingHandler(boundPane, this.titleNeedChangingHandler);
                }
                else
                {
                    if (TitlePart != null && multipaneHeaderValueConverter != null)
                    {
                        //BindingOperations.ClearBinding(titlePart, TextBlock.TextProperty);
                        //BindingOperations.ClearBinding(window, ToolWindow.TitleProperty);
                        if (ChromeManagerBase.LoadedWorkspaceStickyTitle &&
                            !string.IsNullOrEmpty(window.SubLayoutTitle))
                        {
                            window.Title = window.SubLayoutTitle;
                            return;
                        }

                        var multipaneHeaderBinding = new MultiBinding();
                        foreach (var contentPane in XamDockHelper.GetPanes(window))
                        {
                            multipaneHeaderBinding.Bindings.Add(new Binding("Header") { Source = contentPane });
                        }
                        multipaneHeaderBinding.Converter = multipaneHeaderValueConverter;
                        TitlePart.SetBinding(TextBlock.TextProperty, multipaneHeaderBinding);
                        window.SetBinding(ToolWindow.TitleProperty, multipaneHeaderBinding);
                    }
                }
            }
            else
            {
                Attached.AddTitleNeedChangingHandler(boundPane, this.titleNeedChangingHandler);

            } 
        }

        protected override void Clear()
        {
            base.Clear();
            if (this.boundPane != null)
            {   
                Attached.RemoveTitleNeedChangingHandler(this.boundPane, titleNeedChangingHandler);
                boundPane = null;
            }
        }

        internal override IWindowViewContainer Container
        {
            get { return Attached.GetViewContainer(this.ContentObject as FrameworkElement) as IWindowViewContainer; }
        } 

        internal override WindowViewModel ContentModel
        {
            get 
            { 
                ContentPane p = ContentObject as ContentPane;
                return p == null ? null : p.DataContext as WindowViewModel;
            }
        }

    }


    public class MultipaneHeaderValueConverter : IMultiValueConverter
    {
        private readonly Func<string[], string> toolWindowTitleGenerator;

        public MultipaneHeaderValueConverter(Func<string[], string> toolWindowTitleGenerator)
        {
            this.toolWindowTitleGenerator = toolWindowTitleGenerator;
        }

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            return toolWindowTitleGenerator.Invoke(values.Cast<string>().ToArray());
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return new object[] { };
        }
    }
}
