﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
    class WindowViewContainerHostHelper
    {
        private readonly IWindowViewContainerHost hoster;
        private readonly ContentControl hosterControl;
        public WindowViewContainerHostHelper(IWindowViewContainerHost hoster_)
        {
            hoster = hoster_;
            hosterControl = (ContentControl)hoster_;
            if (DockViewModel.FireDragDropEvents)
            {
                hosterControl.AllowDrop = true;
                hosterControl.Drop += (sender_, args_) => InvokeDragOrDropOnContentOnViewModel(args_, UIElement.DropEvent);
                hosterControl.PreviewDrop += (sender_, args_) => InvokeDragOrDropOnContentOnViewModel(args_, UIElement.PreviewDropEvent);
                hosterControl.DragEnter += (o_, eventArgs_) => InvokeDragOrDropOnContentOnViewModel(eventArgs_, UIElement.DragEnterEvent);
                hosterControl.DragOver += (o_, eventArgs_) => InvokeDragOrDropOnContentOnViewModel(eventArgs_, UIElement.DragOverEvent);
                hosterControl.DragLeave += (o_, eventArgs_) => InvokeDragOrDropOnContentOnViewModel(eventArgs_, UIElement.DragLeaveEvent);
                hosterControl.PreviewDragEnter +=
                    (o_, eventArgs_) => InvokeDragOrDropOnContentOnViewModel(eventArgs_, UIElement.PreviewDragEnterEvent);
                hosterControl.PreviewDragOver +=
                    (o_, eventArgs_) => InvokeDragOrDropOnContentOnViewModel(eventArgs_, UIElement.PreviewDragOverEvent);
                hosterControl.PreviewDragLeave +=
                    (o_, eventArgs_) => InvokeDragOrDropOnContentOnViewModel(eventArgs_, UIElement.PreviewDragLeaveEvent);
            }
        }
        private void InvokeDragOrDropOnContentOnViewModel(DragEventArgs dragEventArgs_, RoutedEvent eventType_)
        {
            WindowViewModel model = hosterControl.DataContext as WindowViewModel;
            if (model != null)
            {
                model.InvokeDragOrDropOnContent(
   new DropOnContentEventArgs
   {
       EventArgs = dragEventArgs_,
       InternalEventType = eventType_,
   });
            }

        }
        private double GetHeightDiff(Window window_)
        {
            if (!double.IsNaN(window_.ActualHeight))
            {
                var content = hosterControl.Content as FrameworkElement;
                double actualHeight = content != null ? content.ActualHeight : hosterControl.ActualHeight;
                if (actualHeight > 0.000001)
                {
                    var heightDelta = window_.ActualHeight - actualHeight;
                    if (heightDelta > 0)
                    {
                        return heightDelta;
                    }
                }
            }
            return ((WindowViewContainer)hoster.View).VerticalContentDiff;
        }

        private double GetWidthDiff(Window window_)
        {
            if (!double.IsNaN(window_.ActualWidth))
            {
                var content = hosterControl.Content as FrameworkElement;
                double actualWidth = content != null ? content.ActualWidth : hosterControl.ActualWidth;
                if (actualWidth > 0.000001)
                {
                    var widthDelta = window_.ActualWidth - actualWidth;
                    if (widthDelta > 0)
                    {
                        return widthDelta;
                    }
                }
            }
            return ((WindowViewContainer)hoster.View).HorizontalContentDiff;
        }

        internal void SetContentWidth(double width_)
        {
            Window w = Window.GetWindow(hosterControl);
            if (w == null)
                return;
            w.SizeToContent = SizeToContent.Manual;
            double width = GetWidthDiff(w) + width_;
            if (!double.IsInfinity(w.MaxWidth) && !double.IsNaN(w.MaxWidth))
            {
                w.Width = width > w.MaxWidth ? w.MaxWidth : width;
            }
            else if (!double.IsInfinity(w.MinWidth) && !double.IsNaN(w.MinWidth))
            {
                w.Width = width < w.MinWidth ? w.MinWidth : width;
            }
        }
        internal void SetContentHeight(double height_)
        {
            Window w = Window.GetWindow(hosterControl);
            if (w == null)
                return;
            w.SizeToContent = SizeToContent.Manual;
            double height = GetHeightDiff(w) + height_;
            if (!double.IsInfinity(w.MaxHeight) && !double.IsNaN(w.MaxHeight))
            {
                w.Height = height > w.MaxHeight ? w.MaxHeight : height;
            }
            else if (!double.IsInfinity(w.MinHeight) && !double.IsNaN(w.MinHeight))
            {
                w.Height = height < w.MinHeight ? w.MinHeight : height;
            }
        }

        internal void SetContentMaximumWidth(double maximumWidth_)
        {
            Window w = Window.GetWindow(hosterControl);
            if (w == null)
                return;
            w.SizeToContent = SizeToContent.Manual;
            double maxWidth = maximumWidth_ + GetWidthDiff(w);
            if (!double.IsInfinity(w.MinWidth) && !double.IsNaN(w.MinWidth) && w.MinWidth > maxWidth)
            {
                w.MinWidth = maxWidth;
            }
            w.MaxWidth = maxWidth;
            if (!double.IsNaN(w.ActualWidth) && w.ActualWidth > maxWidth)
            {
                w.Width = maxWidth;
            }
            else if (!double.IsNaN(w.Width) && w.Width > maxWidth)
            {
                w.Width = maxWidth;
            }

        }
        internal void SetContentMinimumWidth(double minWidth_)
        {
            Window w = Window.GetWindow(hosterControl);
            if (w == null)
                return;
            w.SizeToContent = SizeToContent.Manual;
            double minWidth = minWidth_ + GetWidthDiff(w);
            if (!double.IsInfinity(w.MaxWidth) && !double.IsNaN(w.MaxWidth) && w.MaxWidth < minWidth)
            {
                w.MaxWidth = minWidth;
            }
            w.MinWidth = minWidth;
            if (!double.IsNaN(w.ActualWidth) && w.ActualWidth < minWidth)
            {
                w.Width = minWidth;
            }
            else if (!double.IsNaN(w.Width) && w.Width < minWidth)
            {
                w.Width = minWidth;
            }
        }
        internal void SetContentMaximumHeight(double maxHeight_)
        {
            Window w = Window.GetWindow(hosterControl);
            if (w == null)
                return;
            w.SizeToContent = SizeToContent.Manual;
            double maxHeight = maxHeight_ + GetHeightDiff(w);
            if (!double.IsInfinity(w.MinHeight) && !double.IsNaN(w.MinHeight) && w.MinHeight > maxHeight)
            {
                w.MinHeight = maxHeight;
            }
            w.MaxHeight = maxHeight;
            if (!double.IsNaN(w.ActualHeight) && w.ActualHeight > maxHeight)
            {
                w.Height = maxHeight;
            }
            else if (!double.IsNaN(w.Height) && w.Height > maxHeight)
            {
                w.Height = maxHeight;
            }
        }
        internal void SetContentMinimumHeight(double minHeight_)
        {
            Window w = Window.GetWindow(hosterControl);
            if (w == null)
                return;
            w.SizeToContent = SizeToContent.Manual;
            double minHeight = minHeight_ + GetHeightDiff(w);
            if (!double.IsInfinity(w.MaxHeight) && !double.IsNaN(w.MaxHeight) && w.MaxHeight < minHeight)
            {
                w.MaxHeight = minHeight;
            }
            w.MinHeight = minHeight;
            if (!double.IsNaN(w.ActualHeight) && w.ActualHeight < minHeight)
            {
                w.Height = minHeight;
            }
            else if (!double.IsNaN(w.Height) && w.Height < minHeight)
            {
                w.Height = minHeight;
            }
        }

        internal MenuItem CreateOptionsMenuItem(string header_, RoutedEventHandler click_)
        {
            var menuItem = new MenuItem() { Header = header_ };
            menuItem.Click += new RoutedEventHandler(click_);
            return menuItem;
        }

        internal void FixOptionsMenuItems(ItemCollection items_)
        {
            CleanupSeparators(items_);
        }

        private static void CleanupSeparators(ItemCollection items_)
        {
            var idx = 0;
            while (idx < items_.Count)
            {
                while (idx < items_.Count && !(items_[idx] is Separator)) idx++;
                idx++;
                while (idx < items_.Count && items_[idx] is Separator) items_.RemoveAt(idx);
            }
            if (items_[items_.Count - 1] is Separator) items_.RemoveAt(items_.Count - 1);
        }
         
    }
}
