﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Controls/RelayCommand.cs#4 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.Diagnostics;
using System.Windows.Input;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
    /// <summary>
    /// A command whose sole purpose is to 
    /// relay its functionality to other
    /// objects by invoking delegates. The
    /// default return value for the CanExecute
    /// method is 'true'.
    /// </summary>
    public class RelayCommand : ICommand
    {
        readonly Action<object> m_execute;
        readonly Predicate<object> m_canExecute;        

        /// <summary>
        /// Creates a new command that can always execute.
        /// </summary>
        /// <param name="execute_">The execution logic.</param>
        public RelayCommand(Action<object> execute_)
            : this(execute_, null)
        {
        }

        /// <summary>
        /// Creates a new command.
        /// </summary>
        /// <param name="execute_">The execution logic.</param>
        /// <param name="canExecute_">The execution status logic.</param>
        public RelayCommand(Action<object> execute_, Predicate<object> canExecute_)
        {
            if (execute_ == null)
                throw new ArgumentNullException("execute_");

            m_execute = execute_;
            m_canExecute = canExecute_;           
        }

        [DebuggerStepThrough]
        public bool CanExecute(object parameter_)
        {
            return m_canExecute == null ? true : m_canExecute(parameter_);
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter_)
        {
            m_execute(parameter_);
        }
    }
}