﻿////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Controls/LoginWindow.xaml.cs#13 $
// $Change: 875725 $
// $DateTime: 2014/04/09 05:52:47 $
// $Author: anbuy $

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Automation.Peers;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using MorganStanley.MSDotNet.MSGui.Impl.TestSupport;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
    [Flags]
    public enum LoginDisplay
    {
        None = 0,
        Credentials = 2, 
        Profile = 4,
        All = Credentials | Profile
    }

    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    internal partial class LoginWindow
    {
        #region Declarations

        private static readonly DependencyPropertyKey CurrentDisplayPropertyKey =
            DependencyProperty.RegisterReadOnly("CurrentDisplay", typeof (LoginDisplay), typeof (LoginWindow),
                                                new PropertyMetadata(LoginDisplay.None));

        public static readonly DependencyProperty CurrentDisplayProperty = CurrentDisplayPropertyKey.DependencyProperty;

        public static readonly DependencyProperty PasswordProperty =
            DependencyProperty.Register("Password", typeof (string), typeof (LoginWindow));

        public static readonly DependencyProperty SelectedProfileProperty =
            DependencyProperty.Register("SelectedProfile", typeof (KeyValuePair<string, string>), typeof (LoginWindow));

        public static readonly DependencyProperty UsernameProperty =
            DependencyProperty.Register("Username", typeof (string), typeof (LoginWindow));

        private readonly EventHandler _passwordChangedHandler;
        private readonly EventHandler _usernameChangedHandler;
        private readonly EventHandler _profileChangedHandler;

        #endregion Declarations

        #region Constructors

        public LoginWindow(string applicationName, IDictionary<string, string> profiles)
        {
            ApplicationName = applicationName;
            //System.Diagnostics.Debugger.Break();
            AvailableProfiles = profiles;
            
            InitializeComponent();

            DependencyPropertyDescriptor.FromProperty(PasswordProperty, typeof (LoginWindow))
                                        .AddValueChanged(this,
                                                         _passwordChangedHandler = delegate { InvalidateCommands(); });
            DependencyPropertyDescriptor.FromProperty(UsernameProperty, typeof (LoginWindow))
                                        .AddValueChanged(this,
                                                         _usernameChangedHandler = delegate { InvalidateCommands(); });
            DependencyPropertyDescriptor.FromProperty(SelectedProfileProperty, typeof (LoginWindow))
                                        .AddValueChanged(this,
                                                         _profileChangedHandler = delegate { InvalidateCommands(); });
        }

        #endregion Constructors

        #region Base Class Overrides

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            DependencyPropertyDescriptor.FromProperty(PasswordProperty, typeof (LoginWindow))
                                        .RemoveValueChanged(this, _passwordChangedHandler);
            DependencyPropertyDescriptor.FromProperty(UsernameProperty, typeof (LoginWindow))
                                        .RemoveValueChanged(this, _usernameChangedHandler);
            DependencyPropertyDescriptor.FromProperty(SelectedProfileProperty, typeof (LoginWindow))
                                        .RemoveValueChanged(this, _profileChangedHandler);
        }

        #endregion Base Class Overrides

        #region Public Properties

        public LoginDisplay DisplayFlags { get; set; }

        public IDictionary<string, string> AvailableProfiles { get; private set; }

        public string ApplicationName { get; private set; }

        public LoginDisplay CurrentDisplay
        {
            get { return (LoginDisplay) GetValue(CurrentDisplayProperty); }
            private set { SetValue(CurrentDisplayPropertyKey, value); }
        }

        public string Password
        {
            get { return (string) GetValue(PasswordProperty); }
            set { SetValue(PasswordProperty, value); }
        }

        public KeyValuePair<string, string> SelectedProfile
        {
            get { return (KeyValuePair<string, string>) GetValue(SelectedProfileProperty); }
            set { SetValue(SelectedProfileProperty, value); }
        }

        public string Username
        {
            get { return (string) GetValue(UsernameProperty); }
            set { SetValue(UsernameProperty, value); }
        }

        #endregion Public Properties

        #region Public Methods

        public bool? ShowView()
        {
            if (DisplayFlags == LoginDisplay.Profile)
            {
                if (AvailableProfiles.Count == 1)
                {
                    SelectedProfile = AvailableProfiles.First();
                    return true;
                }
            }

            return ShowDialog();
        }

        public bool? ShowView(Window owner)
        {
            Owner = owner;
            return ShowDialog();
        }

        #endregion Public Methods

        #region Event Handlers

        private void _next_Click(object sender, RoutedEventArgs e)
        {
            ChangeDisplay(CurrentDisplay, DisplayFlags);
        }

        private void _ok_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Activate();
            BringIntoView();
            ChangeDisplay(CurrentDisplay, DisplayFlags);
        }

        private void Window_GotFocus(object sender, RoutedEventArgs e)
        {
            var textBox = e.OriginalSource as TextBox;
            if (textBox != null)
            {
                textBox.SelectAll();
                e.Handled = true;
                return;
            }

            var passwordBox = e.OriginalSource as PasswordBox;
            if (passwordBox != null)
            {
                passwordBox.SelectAll();
                e.Handled = true;
            }
        }

        #endregion Event Handlers

        #region Private Methods

        private void ChangeDisplay(LoginDisplay currentDisplay, LoginDisplay allowedDisplays)
        {
            switch (currentDisplay)
            {
                case LoginDisplay.None:
                    if ((allowedDisplays & LoginDisplay.Credentials) == LoginDisplay.Credentials)
                    {
                        _credentialsPanel.Visibility = Visibility.Visible;
                        _profilePanel.Visibility = Visibility.Collapsed;

                        if ((allowedDisplays & LoginDisplay.Profile) == LoginDisplay.Profile)
                        {
                            _next.Visibility = Visibility.Visible;
                            _ok.Visibility = Visibility.Collapsed;
                        }
                        else
                        {
                            _next.Visibility = Visibility.Collapsed;
                            _ok.Visibility = Visibility.Visible;
                            //_ok.IsEnabled = (this.SelectedProfile != null);
                        }

                        if (String.IsNullOrEmpty(Username))
                        {
                            Keyboard.Focus(_usernameField);
                        }
                        else
                        {
                            Keyboard.Focus(_passwordField);
                        }

                        CurrentDisplay = LoginDisplay.Credentials;
                    }
                    else
                    {
                        ChangeDisplay(LoginDisplay.Credentials, allowedDisplays);
                    }
                    break;

                case LoginDisplay.Credentials:
                    _credentialsPanel.Visibility = Visibility.Collapsed;
                    _profilePanel.Visibility = Visibility.Visible;

                    _next.Visibility = Visibility.Collapsed;
                    _ok.Visibility = Visibility.Visible;
                    //_ok.IsEnabled = SelectedProfile != null;

                    Keyboard.Focus(_profileField);

                    CurrentDisplay = LoginDisplay.Profile;
                    break;
            }
        }

        private void InvalidateCommands()
        {
            _next.IsEnabled = _next.IsVisible &&
                              CurrentDisplay == LoginDisplay.Credentials &&
                              !String.IsNullOrEmpty(Username) &&
                              !String.IsNullOrEmpty(Password);

            if (CurrentDisplay == LoginDisplay.Credentials)
            {
                _ok.IsEnabled = _ok.IsVisible &&
                                !String.IsNullOrEmpty(Username) &&
                                !String.IsNullOrEmpty(Password);
            }
            else
            {
                _ok.IsEnabled = _ok.IsVisible; // &&
                //this.SelectedProfile != null;
            }
        }

        #endregion Private Methods


        #region For CUIT
        protected override System.Windows.Automation.Peers.AutomationPeer OnCreateAutomationPeer()
        {
            var inner =  base.OnCreateAutomationPeer();
            var handler = new LoginWindowCommandsHandler();
            return new LoginWindowAutomationPeer(inner, this, handler);
        }

        class LoginWindowCommandsHandler : ImagePathHandler
        {
            public LoginWindowCommandsHandler()
            {
                Handlers[MainWindowSupportedCommands.GetAppliationTitle.ToString()] =
                     (paras) =>
                     {
                         var peer = this.AutomationPeer as LoginWindowAutomationPeer;
                         var window = peer.Owner as LoginWindow;
                         return window.Title;
                     };
            }
        }

        class LoginWindowAutomationPeer: RecordImageAutomationPeerWrapper
        {
         
            public LoginWindowAutomationPeer(AutomationPeer inner, FrameworkElement owner, ImagePathHandler commandsHandler )
                :base(inner, owner, commandsHandler)
            {
                
            }
        }
        #endregion
    }
}
