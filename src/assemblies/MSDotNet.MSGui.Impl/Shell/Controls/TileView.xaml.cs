﻿using System;
using System.Collections.Generic; 
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives; 
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager; 
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Interop; 

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
    /// <summary>
    /// Interaction logic for TileView.xaml
    /// </summary>
    public partial class TileView : UserControl
    {
        public static bool ShowPreview = true;
        private readonly TileViewModel tileViewModel;
        private readonly IChromeManager chromeManager;
        private bool isResizing = false;
        private bool isResizingHorizontal = false;
        private Point? resizingInitialPosition;
        private bool internalSizing = false;
        private double splitterInitialOffset;
        private Tuple<List<TileItemViewModel>, List<TileItemViewModel>> resizingItems; 
        //we must use a popup/window instead of an adorner to implement the splitter preview due to the windows form control used
        private Window resizePreview;
        private readonly TileItemCollectionModel manager; 
        //public static readonly ResourceKey SplitterPreviewStyleKey = new ComponentResourceKey(typeof(TileView), "SplitterPreviewStyleKey");
        internal TileView(TileViewModel tileViewModel_, TileItemCollectionModel manager_, IChromeManager chromeManager_ )
        {
            InitializeComponent();
            this.chromeManager = chromeManager_; 
            this.DataContext = tileViewModel_;
            tileViewModel = tileViewModel_;
            manager = manager_;
            tileViewModel_.TileItems.CollectionChanged += TileItems_CollectionChanged;
            this.Loaded += TileView_Loaded;
            this.SizeChanged += new SizeChangedEventHandler(TileView_SizeChanged);
        }

        void TileView_SizeChanged(object sender, SizeChangedEventArgs e)
        { 
            foreach (TileItem tileItemControl in TileCanvas.Children)
            {
                tileItemControl.SyncHostClip();
            }

        } 
        void TileView_Loaded(object sender_, RoutedEventArgs e_)
        { 
            WindowViewContainer container = tileViewModel.ViewContainer as WindowViewContainer;
            if (container != null)
            {
                container.PropertyChanged -= container_PropertyChanged;
                container.PropertyChanged += container_PropertyChanged;
            }
        }

 
 
        void container_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        { 
            if (tileViewModel.TileItems.Count == 0) return;
            if (e.PropertyName == "Width" || e.PropertyName == "Height")
            { 
                bool needsToUpdateChild = !tileViewModel.IsEditing &&!internalSizing;
                if (needsToUpdateChild)
                {
                    TileViewContainer container = (TileViewContainer)sender;

                    if (e.PropertyName == "Width" && !CoreUtilities.AreClose(container.WidthChangeFactor, 1d))
                    {
                        foreach (TileItem tileItemControl in TileCanvas.Children)
                        {
                            tileItemControl.RefreshHorizontally(container.WidthChangeFactor);
                        }
                    }
                    else if (e.PropertyName == "Height" && !CoreUtilities.AreClose(container.HeightChangeFactor, 1d))
                    {
                        foreach (TileItem tileItemControl in TileCanvas.Children)
                        {
                            tileItemControl.RefreshVertically(container.HeightChangeFactor);
                        }
                    }
                }

                if (needsToUpdateChild)
                {
                    tileViewModel.ResizeEdgeItems();
                } 
                 
            } 
        }
          
        void TileItems_CollectionChanged(object sender_, System.Collections.Specialized.NotifyCollectionChangedEventArgs e_)
        {
            if (e_.OldItems != null)
            {
                foreach (var tileItemViewModel in e_.OldItems)
                {
                    for (int i = 0; i < TileCanvas.Children.Count; i++)
                    {
                        TileItem tileItemControl = TileCanvas.Children[i] as TileItem;
                        if (tileItemControl != null && tileItemControl.ViewModel == tileItemViewModel)
                        {
                            TileCanvas.Children.RemoveAt(i);
                            break; 
                        }
                    } 
                }
            }
            if (e_.NewItems != null)
            {
                foreach (TileItemViewModel tileItemView in e_.NewItems)
                {
                    CreateItem(tileItemView);
                }
                
            }
        }
        private void CreateItem(TileItemViewModel vm_)
        {
            if (vm_.InitialParameters.SizingMethod == SizingMethod.SizeToContent)
            {
                FrameworkElement control = vm_.Content as FrameworkElement;
                if (control != null)
                {
                    vm_.DisplayWidth = tileViewModel.GetRoundUpSize(control.Width);
                    vm_.DisplayHeight = tileViewModel.GetRoundUpSize(control.Height);         
                }
            }
            var item = new TileItem(vm_, manager); 

            TileCanvas.Children.Add(item);

            if (tileViewModel.AutomaticCreated)
            {
                vm_.DisplayLeft = tileViewModel.MarginSize;
                vm_.DisplayTop = tileViewModel.MarginSize;
                if (tileViewModel.InitialParameters.InitialLocation == InitialLocation.Floating ||
                    tileViewModel.InitialParameters.InitialLocation == InitialLocation.FloatingOnly)
                {
                    internalSizing = true;
                    tileViewModel.FloatingWindowMinimumHeight = 0;
                    tileViewModel.FloatingWindowMinimumWidth = 0;
                    tileViewModel.Width = item.Width + tileViewModel.MarginSize * 2 + 10;
                    tileViewModel.Height = item.Height + tileViewModel.MarginSize * 2 + 10;
                    internalSizing = false;                
                }
                Canvas.SetLeft(item, vm_.DisplayLeft);
                Canvas.SetTop(item, vm_.DisplayTop); 
                tileViewModel.AutomaticCreated = false;
            }
            else
            {
                Canvas.SetLeft(item, vm_.DisplayLeft);
                Canvas.SetTop(item, vm_.DisplayTop);

            }  
        }
         
        private void TileCanvas_Drop(object sender_, DragEventArgs e_)
        {

            InitialTileItemParameters parameter = null;
            var tileItemId = e_.Data.GetData(typeof(string)) as string;
            if (tileItemId == null)
            {
                var tileItemInfo = e_.Data.GetData(typeof(TileItemDragInfo)) as TileItemDragInfo;
                if (tileItemInfo != null)
                {
                    tileItemId = tileItemInfo.FactoryId;
                    parameter = tileItemInfo.Parameters;
                }
            } 
            if (string.IsNullOrEmpty(tileItemId)) return; 
            if (parameter == null)
            {
                parameter = new InitialTileItemParameters();
            }

            var point = WindowsFormIntegrationHelper.GetCurrentPosition(TileCanvas);
            parameter.InitialLocationTarget = tileViewModel.ViewContainer as IWindowViewContainer;
            parameter.Left = tileViewModel.GetNearestEdge(point.X);
            parameter.Top = tileViewModel.GetNearestEdge(point.Y);

            IWindowViewContainer container = chromeManager.CreateWindow(tileItemId, parameter);
            e_.Handled = true; 
        }
         
        private void TileCanvas_MouseMove(object sender_, MouseEventArgs e_)
        { 
              
            if (tileViewModel.IsEditing) return;
             
            var point = WindowsFormIntegrationHelper.GetCurrentPosition(TileCanvas);
            if (!isResizing)
            { 
                var horizontalItems = tileViewModel.FindHorizontalAlignedItems(point.X, point.Y);
                if (horizontalItems.Item1.Count > 0 && horizontalItems.Item2.Count > 0)
                {
                    this.Cursor = Cursors.SizeNS;
                    resizingItems = horizontalItems;
                    isResizingHorizontal = false;
                    resizingInitialPosition = point; 
                }
                else
                {
                    var verticalItems = tileViewModel.FindVerticalAlignedItems(point.X, point.Y);
                    if (verticalItems.Item1.Count > 0 && verticalItems.Item2.Count > 0)
                    {
                        this.Cursor = Cursors.SizeWE;
                        resizingItems = verticalItems;
                        isResizingHorizontal = true;
                        resizingInitialPosition = point;
                         
                    }
                    else
                    {
                        EndResize();
                         
                    }
                }
            }
            else if (resizingItems != null && resizingInitialPosition != null)
            {
                if (ShowPreview && resizePreview != null)
                {
                    UpdateResizePreview(point); 
                }
                else
                {
                    ResizeItems();
                }
            } 
        }


        private void TileCanvas_MouseDown(object sender_, MouseButtonEventArgs e_)
        { 
            if (tileViewModel.IsEditing || isResizing) return;

            if (resizingItems != null && (resizingItems.Item1.Count > 0 && resizingItems.Item2.Count > 0))
            { 
                BeginResize();
            }
        }

        private void TileCanvas_MouseUp(object sender_, MouseButtonEventArgs e_)
        {
            if (tileViewModel.IsEditing || !isResizing) return; 
            EndResize();
        }

        private void ResizeItems()
        {
            var point = WindowsFormIntegrationHelper.GetCurrentPosition(TileCanvas);
            if (isResizingHorizontal)
            {
                var edge = tileViewModel.GetNearestEdge(point.X);
                var maxLeft = resizingItems.Item1.Max(a_ => a_.DisplayLeft); 
                var minRight = resizingItems.Item2.Min(a_ => a_.DisplayLeft + a_.DisplayWidth);
                if ((maxLeft < edge - tileViewModel.MarginSize - tileViewModel.UnitSize) && (minRight > edge + tileViewModel.UnitSize))
                {
                    resizingItems.Item1.ForEach(a_ =>
                    {
                        a_.DisplayWidth = edge - tileViewModel.MarginSize - a_.DisplayLeft;
                    });
                    resizingItems.Item2.ForEach(a_ =>
                    {
                        TileItem control = (TileItem)(((WindowViewContainer)a_.ViewContainer).Visual);
                        control.PerformLayout(delegate
                        {
                            a_.DisplayWidth = a_.DisplayLeft + a_.DisplayWidth - edge;
                            a_.DisplayLeft = edge;
                        });

                    });
                }
            }
            else
            {
                var edge = tileViewModel.GetNearestEdge(point.Y);
                var maxTop = resizingItems.Item1.Max(a_ => a_.DisplayTop);
                var minBottom = resizingItems.Item2.Min(a_ => a_.DisplayTop + a_.DisplayHeight);
                if ((maxTop < edge - tileViewModel.MarginSize - tileViewModel.UnitSize) && (minBottom > edge + tileViewModel.UnitSize))
                {
                    resizingItems.Item1.ForEach(a_ =>
                    {
                        a_.DisplayHeight = edge - tileViewModel.MarginSize - a_.DisplayTop;
                    });
                    resizingItems.Item2.ForEach(a_ =>
                    {
                        TileItem control = (TileItem)(((WindowViewContainer)a_.ViewContainer).Visual);
                        control.PerformLayout(delegate
                        {
                            a_.DisplayHeight = a_.DisplayTop + a_.DisplayHeight - edge;
                            a_.DisplayTop = edge;
                        });

                    });
                }
            } 
        }

        internal static Window CreatePreviewWindow(bool horizontal_)
        {
            return new Window()
            {
                Background = Brushes.Gray,
                Opacity = 90,
                AllowsTransparency = true,
                ResizeMode = ResizeMode.NoResize,
                WindowStyle = WindowStyle.None,
                ShowInTaskbar = false,
                //IsHitTestVisible = false
                Width = horizontal_ ? 0 : 3,
                Height = horizontal_ ? 3 : 0,
                Topmost = true
            };
        }
        private void BeginResize()
        {
            isResizing = true;
            if (!ShowPreview) return;
            if (resizePreview == null)
            {
                resizePreview = CreatePreviewWindow(!isResizingHorizontal);

                resizePreview.MouseMove += TileCanvas_MouseMove;
                resizePreview.MouseLeftButtonUp += TileCanvas_MouseUp;
            } 
            
            resizePreview.Cursor = this.Cursor;
            if (isResizingHorizontal)
            { 
                double right = this.ActualWidth;
                double top = this.ActualHeight;
                double bottom = 0;
                double left = 0; 
                foreach (var leftItem in resizingItems.Item1)
                {
                    if (leftItem.DisplayLeft + leftItem.DisplayWidth > left)
                    {
                        left = leftItem.DisplayLeft + leftItem.DisplayWidth;
                    }
                    if (leftItem.DisplayTop < top)
                    {
                        top = leftItem.DisplayTop;
                    }
                    if (leftItem.DisplayTop + leftItem.DisplayHeight > bottom)
                    {
                        bottom = leftItem.DisplayTop + leftItem.DisplayHeight;
                    }
                }

                foreach (var rightItem in resizingItems.Item2)
                {
                    if (rightItem.DisplayLeft < right)
                    {
                        right = rightItem.DisplayLeft;
                    }
                    if (rightItem.DisplayTop < top)
                    {
                        top = rightItem.DisplayTop;
                    }
                    if (rightItem.DisplayTop + rightItem.DisplayHeight > bottom)
                    {
                        bottom = rightItem.DisplayTop + rightItem.DisplayHeight;
                    }
                }
                if (left < tileViewModel.MarginSize) left = tileViewModel.MarginSize;
                if (top < tileViewModel.MarginSize) top = tileViewModel.MarginSize;
                if (bottom > this.ActualHeight - tileViewModel.MarginSize) bottom = this.ActualHeight - tileViewModel.MarginSize;
                var height = bottom - top;
                if (height < 0) height = 1;

                var point = TileCanvas.PointToScreen(new Point(left, top));
                resizePreview.Left = point.X;
                splitterInitialOffset = point.X;
                resizePreview.Top = point.Y;
                resizePreview.Height = height;
                resizePreview.Width = 3;
            }
            else
            { 
                double right = 0;
                double top = 0;
                double bottom = this.ActualHeight;
                double left = this.ActualWidth;
                foreach (var topItem in resizingItems.Item1)
                {
                    if (topItem.DisplayTop + topItem.DisplayHeight > top)
                    {
                        top = topItem.DisplayTop + topItem.DisplayHeight;
                    }
                    if (topItem.DisplayLeft < left)
                    {
                        left = topItem.DisplayLeft;
                    }
                    if (topItem.DisplayWidth + topItem.DisplayLeft > right)
                    {
                        right = topItem.DisplayWidth + topItem.DisplayLeft;
                    }
                }

                foreach (var bottomItem in resizingItems.Item2)
                {
                    if (bottomItem.DisplayTop < bottom)
                    {
                        bottom = bottomItem.DisplayTop;
                    }
                    if (bottomItem.DisplayLeft < left)
                    {
                        left = bottomItem.Left;
                    }
                    if (bottomItem.DisplayWidth + bottomItem.DisplayLeft > right)
                    {
                        right = bottomItem.DisplayWidth + bottomItem.DisplayLeft;
                    }
                }

                if (left < tileViewModel.MarginSize) left = tileViewModel.MarginSize;
                if (right > this.ActualWidth - tileViewModel.MarginSize) right = this.ActualWidth - tileViewModel.MarginSize;
                if (top < tileViewModel.MarginSize) top = tileViewModel.MarginSize; 
                var width = right - left;
                if (width < 0) width = 1;

                var point = TileCanvas.PointToScreen(new Point(left, top)); 
                resizePreview.Left = point.X;
                splitterInitialOffset = point.Y;
                resizePreview.Top = point.Y;
                resizePreview.Width = width;
                resizePreview.Height = 3;
            }

            resizePreview.Show();
        }
  
        private void EndResize()
        {  
            if (isResizing && ShowPreview)
            {
                ResizeItems();
            }
            isResizing = false;
            if (resizingItems != null)
            {
                if (resizingItems.Item1 != null)
                {
                    //foreach (var item in resizingItems.Item1)
                    //{
                    //    TileItem control = (TileItem)(((WindowViewContainer)item.ViewContainer).Visual);
                    //    control.ClearPreviews();
                    //} 
                    resizingItems.Item1.Clear();
                }
                if (resizingItems.Item2 != null)
                {
                    //foreach (var item in resizingItems.Item2)
                    //{
                    //    TileItem control = (TileItem)(((WindowViewContainer)item.ViewContainer).Visual);
                    //    control.ClearPreviews();
                    //}
                    resizingItems.Item2.Clear();
                }
                resizingItems = null;
            }
             
            if (resizePreview != null)
            {
                resizePreview.Hide(); 
            }

            this.Cursor = Cursors.Arrow;
        }

        private void UpdateResizePreview(Point point)
        {
            if (isResizingHorizontal)
            {
                var diff = point.X - resizingInitialPosition.Value.X;
                resizePreview.Left = splitterInitialOffset + diff;
                //foreach (var left in resizingItems.Item1)
                //{
                //    TileItem control = (TileItem)(((WindowViewContainer)left.ViewContainer).Visual);
                //    control.ShowHorizontalPreviews(0, diff);
                //}
                //foreach (var right in resizingItems.Item2)
                //{
                //    TileItem control = (TileItem)(((WindowViewContainer)right.ViewContainer).Visual);
                //    control.ShowHorizontalPreviews(diff, 0);
                //}
            }
            else
            {
                var diff = point.Y - resizingInitialPosition.Value.Y;
                resizePreview.Top = splitterInitialOffset + diff;
                //foreach (var top in resizingItems.Item1)
                //{
                //    TileItem control = (TileItem)(((WindowViewContainer)top.ViewContainer).Visual);
                //    control.ShowVerticalPreviews(0, diff);
                //}
                //foreach (var down in resizingItems.Item2)
                //{
                //    TileItem control = (TileItem)(((WindowViewContainer)down.ViewContainer).Visual);
                //    control.ShowVerticalPreviews(diff, 0);
                //}
            }
        } 
    }
}
