﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
    /// <summary>
    /// Interaction logic for TileMenuBar.xaml
    /// </summary>
    public partial class TileMenuBar : UserControl
    {
        private readonly TileViewModel tileViewModel;
        private readonly InitialDropdownButtonParameters appStoreButtonParameters;

        internal TileMenuBar(TileViewModel tileViewModel_)
        {
            InitializeComponent();
            appStoreButtonParameters = tileViewModel_.InitialParameters.AppStoreButtonParameters;

            if (appStoreButtonParameters == null)
            {
                menuBar.Children.Remove(widgetButton);
                menuBar.Children.Remove(widgetPopup);
            }
            else
            {
                widgetButton.ToolTip = appStoreButtonParameters.ToolTip;
                widgetButton.VerticalContentAlignment = VerticalAlignment.Center;
                widgetButton.HorizontalContentAlignment = HorizontalAlignment.Center;
                if (appStoreButtonParameters.Image != null)
                {
                    var image = new Image() { Source = appStoreButtonParameters.Image };
                    RenderOptions.SetBitmapScalingMode(image, BitmapScalingMode.HighQuality);
                    widgetButton.Content = image;
                }
                else if (!string.IsNullOrEmpty(appStoreButtonParameters.Text))
                {
                    widgetButton.Content = appStoreButtonParameters.Text;
                }

            }
            tileViewModel = tileViewModel_;
            this.DataContext = tileViewModel_;
        }


        private void _scaleSlider_MouseDoubleClick(object sender_, MouseButtonEventArgs e_)
        {
            tileViewModel.Zoom = 1d;
        }

        
        private void widgetButton_Click(object sender_, RoutedEventArgs e_)
        {
            if (appStoreButtonParameters.Opening != null)
            {
                appStoreButtonParameters.Opening(widgetPopup, new WindowEventArgs(tileViewModel.ViewContainer as IWindowViewContainer));
                if (widgetPopup.Child != null || widgetButton.Content != null)
                {
                    widgetPopup.IsOpen = true;
                    if (appStoreButtonParameters.Opened != null)
                    {
                        appStoreButtonParameters.Opened(widgetPopup, new WindowEventArgs(tileViewModel.ViewContainer as IWindowViewContainer));
                    }
                }

            }
        }
    }

    public class EditModeToToolTipConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (System.Convert.ToBoolean(value))
            {
                return "Click to finish design";
            }
            else
            {
                return "Click to start design";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
 
}
