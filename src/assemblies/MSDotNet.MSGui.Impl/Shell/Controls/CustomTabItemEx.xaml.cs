﻿////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Controls/CustomTabItemEx.xaml.cs#4 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
  /// <summary>
  /// Interaction logic for CustmTabItemEx.xaml
  /// </summary>
  public partial class CustomTabItemEx 
  {
    public CustomTabItemEx()
    {
      InitializeComponent();
    }
  }
}
