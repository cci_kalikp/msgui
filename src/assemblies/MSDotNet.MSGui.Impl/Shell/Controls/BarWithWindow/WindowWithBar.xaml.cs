﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Controls/BarWithWindow/WindowWithBar.xaml.cs#9 $
// $Change: 884823 $
// $DateTime: 2014/06/16 02:44:04 $
// $Author: caijin $

using System;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interop;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Helpers;

//using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.MicrosoftShell.Standard;


namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.BarWithWindow
{
  /// <summary>
  /// Interaction logic for WindowWithBar.xaml
  /// </summary>
  public partial class WindowWithBar : Window
  {
    public WindowWithBar(bool enableSoftwareRendering)
    {
      if (enableSoftwareRendering)
      {
        var hwndSource = PresentationSource.FromVisual(this) as HwndSource;
        if (hwndSource != null)
        {
          hwndSource.CompositionTarget.RenderMode = RenderMode.SoftwareOnly;
        }
      }


      MinimizeCommand = new MinimizeWindowCommand(this);
      MaximizeCommand = new MaximizeWindowCommand(this);
      RestoreCommand = new RestoreWindowSizeCommand(this);
      CloseCommand = new CloseWindowCommand(this);
      DragMoveCommand = new DrageMoveWindowCommand(this);

        ShowSystemMenuCommand = new ShowSystemMenuCommand(this);
      
      InitializeComponent();
      TitleBar = new TitleBarPlaceHolder() { DataContext = this };
      this.Loaded += WindowWithBar_Loaded;
    }

    void WindowWithBar_Loaded(object sender, RoutedEventArgs e)
    {
        if (FrameworkExtensions.DefaultStickWindowOptions != StickyWindowOptions.None)
        {
            new WPFStickyWindow(this)
                {
                    StickToScreen = FrameworkExtensions.DefaultStickWindowOptions.HasFlag(StickyWindowOptions.StickToScreen),
                    StickToOther = FrameworkExtensions.DefaultStickWindowOptions.HasFlag(StickyWindowOptions.StickToOther) 
                };
        }
    }



    public static DependencyProperty TitleBarProperty
          = DependencyProperty.Register("TitleBar", typeof(UIElement), typeof(WindowWithBar));

   


    public UIElement TitleBar
    {
        get { return (UIElement)GetValue(TitleBarProperty); }
        set
        {
            SetValue(TitleBarProperty, value);
        }
    }

      internal TitleBarPlaceHolder TitleBarInternal
      {
          get
          {
              return (TitleBarPlaceHolder) TitleBar;
          }
      }


    public static DependencyProperty LauncherBarProperty
    = DependencyProperty.Register("LauncherBar", typeof(UIElement), typeof(WindowWithBar));

    public UIElement LauncherBar
    {
        get { return (UIElement)GetValue(LauncherBarProperty); }
        set
        {
            SetValue(LauncherBarProperty, value);
        }
    }

    public static DependencyProperty StatusBarProperty
       = DependencyProperty.Register("StatusBar", typeof(UIElement), typeof(WindowWithBar));

    public UIElement StatusBar
    {
        get
        {
            return (UIElement) GetValue(StatusBarProperty);
        }
        set
        {
            SetValue(StatusBarProperty, value);
        }
    }


    public static DependencyProperty TitleBarHeightProperty
        = DependencyProperty.Register("TitleBarHeight", typeof(double), typeof(WindowWithBar));
    public double TitleBarHeight
    {
        get { return (double)GetValue(TitleBarHeightProperty); }
        set { SetValue(TitleBarHeightProperty, value); }
    }

    public ICommand MinimizeCommand { get; private set; }
    public ICommand MaximizeCommand { get; private set; }
    public ICommand RestoreCommand { get; private set; }
    public ICommand CloseCommand { get; private set; }
    public ICommand DragMoveCommand { get; private set; }

    public ICommand ShowSystemMenuCommand { get; private set; }
  }
 
}
