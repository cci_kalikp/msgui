﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interactivity;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.BarWithWindow
{
    abstract class WindowCommand : ICommand
    {
        private readonly Window _mainWindow;
        internal WindowCommand(Window window)
        {
            _mainWindow = window;
        }

        protected Window MainWindow { get { return _mainWindow; } }
        public abstract void Execute(object parameter);

        public abstract bool CanExecute(object parameter);

        public event EventHandler CanExecuteChanged;
    }

    class CloseWindowCommand : WindowCommand
    {
        public CloseWindowCommand(Window window)
            : base(window)
        {
        }

        public override void Execute(object parameter)
        {
            MainWindow.Close();
        }

        public override bool CanExecute(object parameter)
        {
            return true;
        }
    }

    class MinimizeWindowCommand : WindowCommand
    {
        public MinimizeWindowCommand(Window window)
            : base(window)
        {
        }

        public override void Execute(object parameter)
        {
            MainWindow.WindowState = WindowState.Minimized;
        }

        public override bool CanExecute(object parameter)
        {
            var canExcecute = MainWindow.ResizeMode != ResizeMode.NoResize &&
                 MainWindow.WindowState != WindowState.Minimized;
            return canExcecute;
        }
    }

    class MaximizeWindowCommand : WindowCommand
    {
        public MaximizeWindowCommand(Window window)
            : base(window)
        {
        }

        public override void Execute(object parameter)
        {
            //MainWindow.Height = SystemParameters.WorkArea.Height;
            //MainWindow.Width = SystemParameters.WorkArea.Width;

            //MainWindow.BorderThickness = MicrosoftShell.SystemParameters2.Current.WindowResizeBorderThickness;


            /*
           var win = Window.GetWindow(o as DependencyObject);
           var tool = ToolWindow.GetToolWindow(o as DependencyObject);

           win.ShowInTaskbar = true;

           win.Title = tool.Title;

           win.SizeToContent = SizeToContent.Manual;
           win.ResizeMode = ResizeMode.CanResizeWithGrip;

           if (win.Tag is Rect)
           {CustomDockManagerFloatingPanes
               var res = (Rect)win.Tag;
               win.Top = res.Top;
               win.Left = res.Left;
               win.Width = res.Width;
               win.Height = res.Height;
               win.Tag = null;
           }
           else
           {
               Rectangle workingArea = System.Windows.Forms.Screen.FromRectangle(
                 new Rectangle(
                   Convert.ToInt32(win.Left),
                   Convert.ToInt32(win.Top),
                   Convert.ToInt32(win.ActualWidth),
                   Convert.ToInt32(win.ActualHeight))).WorkingArea;
               win.Tag = win.RestoreBounds;
               win.SetValue(Window.WidthProperty, (Double)workingArea.Width);
               win.SetValue(Window.HeightProperty, (Double)workingArea.Height);
               win.SetValue(Window.TopProperty, (Double)workingArea.Top);
               win.SetValue(Window.LeftProperty, (Double)workingArea.Left);
               //win.WindowState = WindowState.Maximized;
           }*/


            MainWindow.WindowState = WindowState.Maximized;
        }

        public override bool CanExecute(object parameter)
        {
            return MainWindow.ResizeMode != ResizeMode.NoResize && MainWindow.WindowState != WindowState.Maximized;
        }
    }

    class RestoreWindowSizeCommand : WindowCommand
    {
        public RestoreWindowSizeCommand(Window window)
            : base(window)
        {
        }

        public override void Execute(object parameter)
        {
            MainWindow.WindowState = WindowState.Normal;
        }

        public override bool CanExecute(object parameter)
        {
            return MainWindow.ResizeMode != ResizeMode.NoResize &&
                MainWindow.WindowState != WindowState.Normal;
        }
    }

    class DrageMoveWindowCommand : WindowCommand
    {
        public DrageMoveWindowCommand(Window window)
            : base(window)
        {
        }

        public override void Execute(object parameter)
        {
            /*var pt = Mouse.GetPosition(MainWindow);
            var absPt = MainWindow.PointToScreen(pt);
            var top = absPt.Y - pt.Y + MicrosoftShell.SystemParameters2.Current.WindowResizeBorderThickness.Top;
            var left = absPt.X - pt.X + MicrosoftShell.SystemParameters2.Current.WindowResizeBorderThickness.Left;

            if (MainWindow.WindowState == WindowState.Maximized)
            {
                var width =
                    System.Windows.Forms.Screen.GetWorkingArea(new System.Drawing.Point((int) absPt.X, (int) absPt.Y)).
                        Width;
                MainWindow.WindowState = WindowState.Normal;
                MainWindow.Left = left + pt.X - (pt.X/width)*MainWindow.Width;
                MainWindow.Top = top;
            }*/
            MainWindow.DragMove();
        }

        public override bool CanExecute(object parameter)
        {
            return true;
        }
    }

    class ShowSystemMenuCommand : WindowCommand
    {
        public ShowSystemMenuCommand(Window window)
            : base(window)
        {
        }

        public override void Execute(object parameter)
        {
            var pt = Mouse.GetPosition(MainWindow);
            MicrosoftShell.SystemCommands.ShowSystemMenu(MainWindow, MainWindow.PointToScreen(pt));
            
        }

        public override bool CanExecute(object parameter)
        {
            return true;
        }
    }

    public  class MainWindowIconBehavior: Behavior<UIElement>
    {

        private bool _isMenuOpened;
       

        protected override void OnAttached()
        {
            base.OnAttached();

            var window =System.Windows.Application.Current.MainWindow;
            AssociatedObject.MouseLeftButtonDown +=
                (s, e) =>
                    {
                       // AssociatedObject.CaptureMouse();
                        
                        if (e.ClickCount > 1)
                        {
                          
                            e.Handled = true;
                            window.Close();
                        }
                        if (e.ClickCount == 1)
                        {
                            var pt = Mouse.GetPosition(window);
                            if (!_isMenuOpened)
                                MicrosoftShell.SystemCommands.ShowSystemMenu(window,
                                                                         window.PointToScreen(new Point(pt.X + 3.0, pt.Y + 3.0)));

                            _isMenuOpened = !_isMenuOpened;
                        }

                    };
        }
    }

    public class CaptionAreaBehavior : Behavior<UIElement>
    {

        protected override void OnAttached()
        {
            base.OnAttached();
            var window = System.Windows.Application.Current.MainWindow;
            AssociatedObject.MouseLeftButtonDown +=
                (s, e) =>
                    {
                        //AssociatedObject.CaptureMouse();
                        if (e.ClickCount > 1)
                        {
                            window.WindowState = window.WindowState == WindowState.Normal
                                                     ? WindowState.Maximized
                                                     : WindowState.Normal;
                            e.Handled = true;
                        }
                    };
          //  AssociatedObject.MouseLeftButtonUp +=
          //      (s, e) => AssociatedObject.ReleaseMouseCapture();

        }

    }
}
