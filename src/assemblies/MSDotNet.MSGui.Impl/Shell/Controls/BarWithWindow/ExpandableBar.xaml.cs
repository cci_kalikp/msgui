﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Controls/BarWithWindow/ExpandableBar.xaml.cs#5 $
// $Change: 855003 $
// $DateTime: 2013/11/19 03:33:57 $
// $Author: caijin $

using System;
using System.Windows;
using System.Windows.Controls;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.BarWithWindow
{
  /// <summary>
  /// Interaction logic for ExpandableBar.xaml
  /// </summary>
  public partial class ExpandableBar : UserControl, IExpandableBar
  {
    public ExpandableBar()
    {
      InitializeComponent();
      Height = HeightShrinked;
      MouseDoubleClick += delegate
                            {
                              BarState = BarState == LauncherBarState.Shrinked ? LauncherBarState.Expanded : LauncherBarState.Shrinked;
                            };
    }

    public void AddContent(UIElement content_)
    {
      grid.Children.Add(content_);
    }

    #region BarState property and event
    public event EventHandler BarStateChanged;
    public static readonly DependencyProperty BarStateProperty =
      DependencyProperty.Register(
        "BarState", typeof(LauncherBarState), typeof(ExpandableBar), new PropertyMetadata(LauncherBarState.Shrinked, BarStateChangedCallback));

    private static void BarStateChangedCallback(DependencyObject d_, DependencyPropertyChangedEventArgs e_)
    {
      ExpandableBar window = (ExpandableBar)d_;

      var copy = window.BarStateChanged;
      if (copy != null)
      {
        copy(window, EventArgs.Empty);
      }

      if ((LauncherBarState)e_.NewValue == LauncherBarState.Shrinked)
      {
        window.Height = HeightShrinked;
      }
      else
      {
        window.Height = HeightExpanded;
      }
    }

    public LauncherBarState BarState
    {
      get { return (LauncherBarState)GetValue(BarStateProperty); }
      set { SetValue(BarStateProperty, value); }
    }
    #endregion

    public string Version
    {
      get;
      set;
    }

    public bool IsTaskbar { get; set; }

    public bool IsTopmost
    {
      get; set;
    }
    
    public string Profile
    {
      get;
      set;
    }

    public static int HeightShrinked = 50;

    public static int HeightExpanded = 150;
    
    public void SetDock(bool b_)
    {
      
    }

    public void ToggleMode()
    {
        switch (BarState)
        {
            case LauncherBarState.Shrinked:
                BarState = LauncherBarState.Expanded;
                break;
            case LauncherBarState.Expanded:
                BarState = LauncherBarState.Shrinked;
                break;
        }
    }


		public string UserName {get; set; }

		public string ConcordProfile {get; set; }

      public string State
      {
          get
          {
              Window w = Window.GetWindow(this);
              return w == null ? string.Empty : w.WindowState.ToString();
          }
          set
          {
              Window w = Window.GetWindow(this);
              if (w == null) return; 
              switch (value.ToLower())
              {
                  case "maximized":
                      w.WindowState = WindowState.Maximized;
                      break;
                  case "minimized":
                      w.WindowState = WindowState.Minimized;
                      break;
                  default:
                      w.WindowState = WindowState.Normal;
                      break;
              }
          }
      }
		
	}
}
