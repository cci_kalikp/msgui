﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Controls/ITileItemManager.cs#7 $
// $Change: 902487 $
// $DateTime: 2014/10/27 03:05:30 $
// $Author: caijin $

using System; 
using System.Xml.Linq; 
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
    internal interface ITileItemManager
    {
        TileViewModel AddTileView(TileViewContainer tileContainer_);
        void AddNewItem(TileItemViewContainer viewViewContainer_, TileViewModel tileView_);
        void Clean();
        XDocument SaveState(Func<TileItemViewContainer, XElement> state_);
        XElement SaveState(TileViewContainer tileViewContainer_, Func<TileItemViewContainer, XElement> buildIViewXml_);
        void BeforeSaveState();
        void AfterSaveState();
        void CancelLoad(bool cancelLayoutRestore_);
        void LoadState(XDocument layout_, Action<XDocument, TileViewModel> applyNodeAction_);
        void LoadState(XElement tileViewNode_, Action<XDocument, TileViewModel> applyNodeAction_);
        void BeforeLoadState();
        void AfterLoadState(); 

    }
     
}
