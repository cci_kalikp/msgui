﻿using System.Windows.Automation.Peers; 
using MorganStanley.MSDotNet.MSGui.Impl.TestSupport;

namespace MorganStanley.MSDotNet.MSGui.Impl
{
  /// <summary>
  /// Interaction logic for TabItemMenu.xaml
  /// </summary>
  public partial class TabItemMenu
  {
      internal static bool RenameDisabled;

    public TabItemMenu()
    {
      InitializeComponent();
        if (RenameDisabled)
            RenameTab.IsEnabled = false; 
    }

      
    internal bool EnableLeft 
    {
      set { MoveLeft.IsEnabled = value; }
    }

    internal bool EnableRight
    {
      set { MoveRight.IsEnabled = value; }
    }

    #region for CUIT
    protected override AutomationPeer OnCreateAutomationPeer()
    {
        var peer = base.OnCreateAutomationPeer();
        return new RecordImageAutomationPeerWrapper(peer, this);
    }
    
    #endregion
    
  }
}
