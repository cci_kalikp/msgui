﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Controls/TabHeaderControl.xaml.cs#8 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
  /// <summary>
  /// Interaction logic for TabHeaderControl.xaml
  /// </summary>
  internal partial class TabHeaderControl : UserControl
  {
    public TabHeaderControl()
    {
      InitializeComponent();      
    }

    public static readonly DependencyProperty TitleProperty =
     DependencyProperty.Register(
       "Title", typeof(string), typeof(TabHeaderControl), new PropertyMetadata("New Tab"));

    public string Title
    {
      get { return (string)GetValue(TitleProperty); }
      set { SetValue(TitleProperty, value); }
    }    

    public override string ToString()
    {
      return Title.ToString();
    }

    private void EditableTextBlock_DoneEdit(object sender, System.EventArgs e)
    {

    }


    private void HandleRenameCommand(object sender_, ExecutedRoutedEventArgs e_)
    {
      textBlock.IsInEditMode = true;      
    }

    private void CommandBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
    {
      e.CanExecute = true;
    }
  }
}
