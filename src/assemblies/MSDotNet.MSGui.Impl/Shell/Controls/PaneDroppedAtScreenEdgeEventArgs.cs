﻿using System;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
    internal class PaneDroppedAtScreenEdgeEventArgs : EventArgs
    {
        public IWindowViewContainer ViewContainer { get; set; }
        public ScreenEdge Edge { get; set; }
    }
}
