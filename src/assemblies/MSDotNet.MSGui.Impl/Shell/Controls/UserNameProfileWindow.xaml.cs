﻿////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Controls/UserNameProfileWindow.xaml.cs#6 $
// $Change: 849973 $
// $DateTime: 2013/10/16 17:23:36 $
// $Author: hrechkin $

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
    internal class OKClickedEventArgs : EventArgs
    {
        public bool CanCloseDialog { get; set; }

        public OKClickedEventArgs()
        {
            CanCloseDialog = true;
        }
    }

    /// <summary>
    /// Interaction logic for UserNameProfileWindow.xaml
    /// </summary>
    internal partial class UserNameProfileWindow
    {
        #region Declarations

        public static readonly DependencyProperty SelectedProfileProperty =
            DependencyProperty.Register("SelectedProfile", typeof (KeyValuePair<string, string>),
                                        typeof (UserNameProfileWindow));

        public static readonly DependencyProperty UsernameProperty =
            DependencyProperty.Register("Username", typeof (string), typeof (UserNameProfileWindow));

        private readonly EventHandler _usernameChangedHandler;
        private readonly EventHandler _profileChangedHandler;

        #endregion Declarations

        #region Constructors

        static UserNameProfileWindow()
        {
            IsImpersonationEnabled = true;
        }

        public UserNameProfileWindow(string applicationName, IDictionary<string, string> profiles)
        {
            ApplicationName = applicationName;
            AvailableProfiles = profiles;

            InitializeComponent();

            _profileField.Focus();

            DependencyPropertyDescriptor.FromProperty(UsernameProperty, typeof (UserNameProfileWindow))
                                        .AddValueChanged(this,
                                                         _usernameChangedHandler =
                                                         delegate { InvalidateCommands(); });
            DependencyPropertyDescriptor.FromProperty(SelectedProfileProperty, typeof (UserNameProfileWindow))
                                        .AddValueChanged(this,
                                                         _profileChangedHandler =
                                                         delegate { InvalidateCommands(); });
        }

        #endregion Constructors

        #region Base Class Overrides

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            DependencyPropertyDescriptor.FromProperty(UsernameProperty, typeof (UserNameProfileWindow))
                                        .RemoveValueChanged(this, _usernameChangedHandler);
            DependencyPropertyDescriptor.FromProperty(SelectedProfileProperty, typeof (UserNameProfileWindow))
                                        .RemoveValueChanged(this, _profileChangedHandler);
        }

        #endregion Base Class Overrides

        #region Public Properties

        public LoginDisplay DisplayFlags { get; set; }

        public event EventHandler<OKClickedEventArgs> OkClicked;

        public IDictionary<string, string> AvailableProfiles { get; private set; }

        public string ApplicationName { get; private set; }

        public KeyValuePair<string, string> SelectedProfile
        {
            get { return (KeyValuePair<string, string>) GetValue(SelectedProfileProperty); }
            set { SetValue(SelectedProfileProperty, value); }
        }

        public string Username
        {
            get { return (string) GetValue(UsernameProperty); }
            set { SetValue(UsernameProperty, value); }
        }

        internal static bool IsImpersonationEnabled { get; set; }

        #endregion Public Properties

        #region Public Methods

        public bool? ShowView()
        {
            return ShowDialog();
        }

        public bool? ShowView(Window owner)
        {
            Owner = owner;
            return ShowDialog();
        }

        #endregion Public Methods

        #region Event Handlers

        private void _ok_Click(object sender, RoutedEventArgs e)
        {
            var args = new OKClickedEventArgs();
            OnOkClicked(args);
            if (args.CanCloseDialog)
            {
                DialogResult = true;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            _usernameField.IsEnabled = IsImpersonationEnabled;
            Activate();
            BringIntoView();
        }

        private void Window_GotFocus(object sender, RoutedEventArgs e)
        {
            var textBox = e.OriginalSource as TextBox;
            if (textBox != null)
            {
                textBox.SelectAll();
                e.Handled = true;
            }
        }

        #endregion Event Handlers

        #region Protected Methods

        protected virtual void OnOkClicked(OKClickedEventArgs args)
        {
            var handler = OkClicked;
            if (handler != null)
            {
                handler(this, args);
            }
        }

        #endregion

        #region Private Methods

        private void InvalidateCommands()
        {
            _ok.IsEnabled = !String.IsNullOrEmpty(Username);
        }

        #endregion Private Methods
    }
}
