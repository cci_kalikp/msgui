﻿using System.Collections.ObjectModel;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
    class ExtraContextMenuWidgetContainer : WidgetViewContainer, IContextMenuExtrasHolder
    {
        private readonly ObservableCollection<IWidgetViewContainer> extraContextMenuItems;

        public ExtraContextMenuWidgetContainer(string ID, InitialWidgetParameters parameters, ObservableCollection<IWidgetViewContainer> extraContextMenuItems) : base(ID, parameters)
        {
            this.extraContextMenuItems = extraContextMenuItems;
        }

        protected override bool DoAddVisual(IWidgetViewContainer child)
        {
            extraContextMenuItems.Add(child);
            return base.DoAddVisual(child);
        }

        protected override void DoRemoveVisual(IWidgetViewContainer child)
        {
            extraContextMenuItems.Remove(child);
            base.DoRemoveVisual(child);
        }

        public void AddWidget(InitialWidgetParameters widget, int index)
        {
            this.AddWidget(widget);
            extraContextMenuItems.Move(extraContextMenuItems.Count - 1, index);
        }

        public int ElementCount
        {
            get
            {
                return extraContextMenuItems.Count;
            }
        }
    }
}
