﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Automation.Peers;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using System.Windows.Threading;
using System.Timers;
using MorganStanley.MSDotNet.MSGui.Impl.TestSupport;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
	public class ExitTaskDialogLauncher : IExitDialog
	{
		public ExitTaskDialogLauncher()
		{
			m_timeoutTimer = new Timer(1000) { Enabled = false };
			m_dispatcher = Dispatcher.CurrentDispatcher;
			m_handle = new TaskDialogHandle();
			m_timeUp = false;
		}

		public bool? ShowDialog()
		{
		    m_options = new TaskDialogOptions
		    {
		        Title = string.Format("Quit {0}?", AppName),
		        MainInstruction = string.Format("You are about to close {0}", AppName),
		        Content = WarningBlockText ??
                            (UsedForSubLayout ?
                            (
                            string.Format("Current workspace will be closed. {0}{1}",
                            Environment.NewLine,
                            ShowSaveAndQuitOption
                                ? "Save changes in current workspace?"
                                : "Changes in current workspace will not be saved.")

                            ) :
                            (
                            string.Format("The application and all its windows will be closed. {0}{1}", 
                            Environment.NewLine, 
                            ShowSaveAndQuitOption
		                        ? string.Format("Save changes in the current profile '{0}'?", ProfileName)
		                        : string.Format("Changes in the current profile '{0}' will not be saved.", ProfileName)))
		                    ),
				AllowDialogCancellation = true,
				MainIcon = VistaTaskDialogIcon.Warning, 
                Owner = this.Owner
			};

			if (ShowDontAskAgainOption)
				m_options.VerificationText = "Do not ask next time";
			var buttons = new List<string>();
            if (ShowSaveAndQuitOption)
                buttons.Add(SaveAndQuitLabel);
			buttons.Add(QuitLabel);
			buttons.Add(CancelLabel);
			m_options.CustomButtons = buttons.ToArray();

			if (Timeout == null)
			{
				Timeout = DefaultTimeout;
			}
			if (Timeout > 0)
			{
				m_timeoutTimer.Elapsed += new ElapsedEventHandler(t_Elapsed);
				m_options.ProgressBarMinimum = 0;
				m_options.ProgressBarMaximum = Timeout;
                m_options.ProgressBarInitialValue = 0;
				m_timeoutTimer.Enabled = true;
			}
			else
			{
				m_options.ProgressBarInitialValue = null;
			}

			TaskDialogResult results = TaskDialog.Show(m_options, m_handle);

			DontAsk = results.VerificationChecked.HasValue && results.VerificationChecked.Value ? true : false;
			if (results.CustomButtonResult.HasValue)
			{
				switch (buttons[results.CustomButtonResult.Value])
				{
					case SaveAndQuitLabel:
						return SaveAndQuit();
					case QuitLabel:
						return Quit();
				}
			}

			if (m_timeUp) return true;

			return false;
		}

		#region Actions

		private bool SaveAndQuit()
		{
            m_timeoutTimer.Elapsed -= new ElapsedEventHandler(t_Elapsed);
            m_timeoutTimer.Enabled = false;
			SaveProfile = true;
			return true;
		}

		private bool Quit()
		{
            m_timeoutTimer.Elapsed -= new ElapsedEventHandler(t_Elapsed);
            m_timeoutTimer.Enabled = false;
			SaveProfile = false;
			return true;
		}

		#endregion

		#region Configuration

		internal int? Timeout
		{
			get
			{
				return this.m_timeout;
			}
			set
			{
				if (value == null)
				{
					this.m_timeout = this.m_timeLeft = DefaultTimeout;
				}
				else
				{
					this.m_timeout = this.m_timeLeft = value;
				}
			}
		}

		internal static bool ShowDontAskAgainOption = true;
		internal static bool ShowSaveAndQuitOption = true;
		internal static string WarningBlockText = null;
		internal static int? DefaultTimeout = null;

		private const string SaveAndQuitLabel = "Save and Quit";
		private const string QuitLabel = "Quit";
		private const string CancelLabel = "Cancel";

		#endregion

		#region Properties

		public string AppName { get; set; }
		public bool DontAsk { get; set; }
		public string ProfileName { get; set; }
		public bool SaveProfile { get; private set; }
		public Window Owner { get; set; }
        public bool UsedForSubLayout { get; set; }
		#endregion

		void t_Elapsed(object sender, ElapsedEventArgs e)
		{
			if (this.m_timeLeft == 0)
			{
				m_dispatcher.Invoke((System.Windows.Forms.MethodInvoker)delegate()
				{
					this.m_timeoutTimer.Enabled = false;
                    m_timeoutTimer.Elapsed -= new ElapsedEventHandler(t_Elapsed);
                    SaveProfile = false;
					m_handle.Close();
					m_timeUp = true;
				});
			}
			else
			{
				m_dispatcher.Invoke((System.Windows.Forms.MethodInvoker)delegate()
				{
					this.m_timeLeft--;
					m_handle.ProgressBarCurrentValue = m_options.ProgressBarMaximum - m_timeLeft;
				});
			}
		}

		private TaskDialogHandle m_handle;
		private TaskDialogOptions m_options;
		private Timer m_timeoutTimer;
		private int? m_timeLeft;
		private int? m_timeout;
		private bool m_timeUp;
		private Dispatcher m_dispatcher;



       
    }
}
