﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel; 
using System.Runtime.InteropServices; 
using System.Windows;
using System.Windows.Forms;
using System.Windows.Interop;
using System.Windows.Threading;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Win32;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.MicrosoftShell.Standard;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
    static class FloatingWindowUtil
    {
        /// <summary>
        /// If true, windows will be placed at -5000 initially. WARNING: Make sure DoShowWindows gets called!
        /// </summary>
        internal static bool HideLoadingWindows = false;

        internal static event EventHandler ShowWindows;

        internal static void DoShowWindows()
        {
            HideLoadingWindows = false;
            if (ShowWindows != null)
                ShowWindows(null, null);
        }

        static List<Window> windows = new List<Window>(); 
  

        static readonly ObservableCollection<IFloatingWindow> floatingWindows = new ObservableCollection<IFloatingWindow>();
        public static ReadOnlyObservableCollection<IFloatingWindow> FloatingWindows
        {
            get { return new ReadOnlyObservableCollection<IFloatingWindow>(floatingWindows); }
        }

        public static void RegisterFloatingWindow(IFloatingWindow window_)
        {
            if (floatingWindows.Contains(window_)) return;
            floatingWindows.Add(window_);
            EventHandler closed = null;
            window_.Closed += closed = (sender_, args_) =>
            {
                window_.Closed -= closed;
                floatingWindows.Remove(window_);
            };

        }

        public static void FixInvisibleWindows()
        {
            Dispatcher.CurrentDispatcher.BeginInvoke(
                DispatcherPriority.ApplicationIdle, (Action)(() =>
                {
                    int count = 0;
                    foreach (Window window in windows)
                    {
                        //the position of main window would be adjusted when restoring shell persistence
                        if (window == System.Windows.Application.Current.MainWindow) continue;
                        if (window.Visibility != Visibility.Visible) continue;
                        
                        if (!WindowOutOfScreen(window)) continue;
                        count++;
                        window.Left = 100 + count * 10;
                        window.Top = 100 + count * 10;
                    }
                }));
        }

        
        internal static bool WindowOutOfScreen(Window window_)
        {
            return ((Double.IsNaN(window_.Left) ? -10000d : window_.Left) +
                    0.995d * (Double.IsNaN(window_.Width) ? 0d : window_.Width) <
                    SystemParameters.VirtualScreenLeft
                    + 20) ||
                   ((Double.IsNaN(window_.Left) ? 10000d : window_.Left) -
                    0.005d * (Double.IsNaN(window_.Width) ? 0d : window_.Width) >
                    SystemParameters.VirtualScreenLeft + SystemParameters.VirtualScreenWidth - 20) ||
                   ((Double.IsNaN(window_.Top) ? -10000d : window_.Top) +
                    0.995d * (Double.IsNaN(window_.Height) ? 0d : window_.Height) <
                    SystemParameters.VirtualScreenTop + 20) ||
                   ((Double.IsNaN(window_.Top) ? 10000d : window_.Top) -
                    0.005d * (Double.IsNaN(window_.Height) ? 0d : window_.Height) >
                    SystemParameters.VirtualScreenTop + SystemParameters.VirtualScreenHeight - 20);
        }

        public static void FixResize(Window win_)
        {
            IntPtr handle = (new WindowInteropHelper(win_)).Handle;
            HwndSource.FromHwnd(handle).AddHook(WindowProc);
            windows.Add(win_);
            EventHandler closed = null;
            win_.Closed += closed = (sender_, args_) =>
            {

                windows.Remove(win_);
                win_.Closed -= closed;
            };
        }

  

        public static void FixRestoreBounds(Window win_)
        {
            SizeChangedEventHandler sizeChanged = null;
            var windowHandle = (new WindowInteropHelper(win_)).Handle;
            var source = HwndSource.FromHwnd(windowHandle);
            win_.SizeChanged += sizeChanged = (sender_, args_) =>
            {
                //this would happen when tries to maximize the window from the taskbar context menu
                if (win_.WindowState == WindowState.Maximized &&
                    (!CoreUtilities.AreClose(win_.ActualWidth, win_.Width) ||
                    !CoreUtilities.AreClose(win_.ActualHeight, win_.Height)))
                {
                    var restoreBounds = win_.RestoreBounds;
                    win_.Dispatcher.BeginInvoke(DispatcherPriority.Background,
                        new Action<HwndSource, Rect>(FixRestoreBounds), source, restoreBounds);
                }
            };
            EventHandler closed = null;
            win_.Closed += closed = (sender_, args_) =>
            {
                win_.SizeChanged -= sizeChanged;
                win_.Closed -= closed;
            };
        }

        private static void FixRestoreBounds(HwndSource source_, Rect restoreBounds_)
        {
            if (source_ == null || source_.IsDisposed || source_.CompositionTarget == null) return;
            Win32.WindowPlacement windowPlacement = new Win32.WindowPlacement();
            Win32.GetWindowPlacement(source_.Handle, ref windowPlacement);
            var point = Win32.POINT.FromPoint(source_.CompositionTarget.TransformToDevice.Transform(new Point(restoreBounds_.Left, restoreBounds_.Top)));
            var point2 = Win32.POINT.FromPoint(source_.CompositionTarget.TransformToDevice.Transform(new Point(restoreBounds_.Right, restoreBounds_.Bottom)));
            windowPlacement.NormalPosition = new Win32.RECT(point.X, point.Y, point2.X, point2.Y);
            Win32.SetWindowPlacement(source_.Handle, ref windowPlacement);
        }

         
        private static readonly List<long> specialResize2 = new List<long>();
        static readonly List<long> specialResize = new List<long>(); 
        private static IntPtr WindowProc(IntPtr hwnd_, int msg_, IntPtr wParam_, IntPtr lParam_, ref bool handled_)
        {
            switch ((uint)msg_)
            {
                case Win32.WM_WINDOWPOSCHANGING:
                    var windowState2 = NativeUiMethodsHelper.GetWindowState(hwnd_);

                    if (windowState2 == WindowState.Maximized)
                    {

                        var val = (Win32.WINDOWPOS)Marshal.PtrToStructure(lParam_, typeof(Win32.WINDOWPOS));

                        if (val.flags ==
                            (Win32.SetWindowsPosFlags.SWP_NOSIZE |
                             Win32.SetWindowsPosFlags.SWP_NOZORDER |
                             Win32.SetWindowsPosFlags.SWP_NOACTIVATE |
                             Win32.SetWindowsPosFlags.SWP_NOOWNERZORDER))
                        {
                            val.flags |= Win32.SetWindowsPosFlags.SWP_NOMOVE;
                            Marshal.StructureToPtr(val, lParam_, true);
                            handled_ = true;

                            return Win32.DefWindowProc(hwnd_, msg_, wParam_, lParam_);
                        }
                        else if (specialResize.Contains(hwnd_.ToInt64()))
                        {
                            specialResize.Remove(hwnd_.ToInt64());
                            specialResize2.Add(hwnd_.ToInt64());
                            //Console.WriteLine("foo => " + hwnd.ToInt64());
                            var val3 = (Win32.WINDOWPOS)Marshal.PtrToStructure(lParam_, typeof(Win32.WINDOWPOS));

                            val3.flags |= Win32.SetWindowsPosFlags.SWP_NOZORDER;

                            var val2 = (Win32.WINDOWPOS)Marshal.PtrToStructure(lParam_, typeof(Win32.WINDOWPOS));

                            Marshal.StructureToPtr(val3, lParam_, true);
                            return Win32.DefWindowProc(hwnd_, msg_, wParam_, lParam_);
                        }
                        else if (specialResize2.Contains(hwnd_.ToInt64()))
                        {
                            specialResize2.Remove(hwnd_.ToInt64());

                            var val3 = (Win32.WINDOWPOS)Marshal.PtrToStructure(lParam_, typeof(Win32.WINDOWPOS));

                            val3.flags |= Win32.SetWindowsPosFlags.SWP_NOZORDER;

                            var val2 = (Win32.WINDOWPOS)Marshal.PtrToStructure(lParam_, typeof(Win32.WINDOWPOS));

                            Marshal.StructureToPtr(val3, lParam_, true);
                            return Win32.DefWindowProc(hwnd_, msg_, wParam_, lParam_);
                        }
                    }
                    break;

                // as a result you can decrease size of a window in the way when its borders and header are partially cut 
                case Win32.WM_GETMINMAXINFO:
                    WmGetMinMaxInfo(hwnd_, lParam_);
                    handled_ = true; 
                    break;

            }
            return IntPtr.Zero;
        }


        private static void WmGetMinMaxInfo(IntPtr hwnd_, IntPtr lParam_)
        {  

            IntPtr lPrimaryScreen = Win32.MonitorFromPoint(new Win32.POINT { X = 0, Y = 0 }, Win32.MonitorOptions.MONITOR_DEFAULTTOPRIMARY);
            Win32.MONITORINFO lPrimaryScreenInfo = new Win32.MONITORINFO();
            if (Win32.GetMonitorInfo(lPrimaryScreen, lPrimaryScreenInfo) == false)
            {
                return;
            }

            IntPtr lCurrentScreen = Win32.MonitorFromWindow(hwnd_, Win32.MonitorOptions.MONITOR_DEFAULTTONEAREST);

            Win32.MINMAXINFO lMmi = (Win32.MINMAXINFO)Marshal.PtrToStructure(lParam_, typeof(Win32.MINMAXINFO)); 

            if (lPrimaryScreen.Equals(lCurrentScreen))
            {
                lMmi.ptMaxPosition.X = lPrimaryScreenInfo.rcWork.Left;
                lMmi.ptMaxPosition.Y = lPrimaryScreenInfo.rcWork.Top;
                lMmi.ptMaxSize.X = lPrimaryScreenInfo.rcWork.Right - lPrimaryScreenInfo.rcWork.Left;
                lMmi.ptMaxSize.Y = lPrimaryScreenInfo.rcWork.Bottom - lPrimaryScreenInfo.rcWork.Top; 
            }
            else
            { 
                lMmi.ptMaxPosition.X = lPrimaryScreenInfo.rcMonitor.Left;
                lMmi.ptMaxPosition.Y = lPrimaryScreenInfo.rcMonitor.Top;
                lMmi.ptMaxSize.X = lPrimaryScreenInfo.rcMonitor.Right - lPrimaryScreenInfo.rcMonitor.Left;
                lMmi.ptMaxSize.Y = lPrimaryScreenInfo.rcMonitor.Bottom - lPrimaryScreenInfo.rcMonitor.Top; 
            }
            int windowMaxWidth = Math.Min(lPrimaryScreenInfo.rcWork.GetLength(), lPrimaryScreenInfo.rcMonitor.GetLength());
            int windwoMaxHeight = Math.Min(lPrimaryScreenInfo.rcWork.GetHeight(), lPrimaryScreenInfo.rcMonitor.GetHeight());
            HwndSource source = HwndSource.FromHwnd(hwnd_);
            if (source != null && source.RootVisual != null)
            {
                FrameworkElement element = source.RootVisual as FrameworkElement;
                if (element != null)
                {
                    if (lMmi.ptMaxPosition.X > element.MaxWidth && element.MaxWidth < windowMaxWidth)
                    {
                        lMmi.ptMaxPosition.X = (int)element.MaxWidth;
                    }
                    if (lMmi.ptMaxPosition.Y > element.MaxHeight && element.MaxHeight < windwoMaxHeight)
                    { 
                        lMmi.ptMaxPosition.Y = (int)element.MaxHeight;
                    }

                }
            }
            Marshal.StructureToPtr(lMmi, lParam_, true);

        }
        public static bool EnableNonOwnedWindows;
        internal static bool SeparateHeaderItemsInFloatingWindow;
        internal static bool RestrictClosingToHeaderButton = false;
    }
}
