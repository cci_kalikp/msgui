﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input; 
using Infragistics.Windows.Controls;
using Infragistics.Windows.DockManager;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager; 
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions; 
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions; 
using MorganStanley.MSDotNet.MSGui.Impl.TestSupport;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
	/// <summary>
	/// Interaction logic for CustomContentPane.xaml
	/// </summary>
    public partial class CustomContentPane : ContentPane, IWindowViewContainerHost
	{
		#region Attached dependency property 

		internal static DependencyPropertyKey ActivePanePropertyKey = DependencyProperty.RegisterAttachedReadOnly
		(
			"ActivePane",
			typeof(ContentPane),
			typeof(CustomContentPane),
			new PropertyMetadata(null)
		);

		public static DependencyProperty ActivePaneProperty
		{
			get
			{
				return ActivePanePropertyKey.DependencyProperty;
			}
		}

		public static void SetActivePane(UIElement element, ContentPane value)
		{
           ;
			element.SetValue(ActivePanePropertyKey, value);
		}

		public static ContentPane GetActivePane(UIElement element)
		{
			return (ContentPane)element.GetValue(ActivePaneProperty);
		}

		#endregion 
	    internal static bool NonuniformDocumentTabsMode = false;

		public Visibility HeaderVisible
		{
			get { return (Visibility)GetValue(HeaderVisibleProperty); }
			set { SetValue(HeaderVisibleProperty, value); }
		}

		// Using a DependencyProperty as the backing store for HeaderVisible.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty HeaderVisibleProperty =
				DependencyProperty.Register("HeaderVisible", typeof(Visibility), typeof(CustomContentPane), new UIPropertyMetadata(System.Windows.Visibility.Visible));

		public static readonly DependencyProperty HeaderHeightProperty =
			DependencyProperty.Register("HeaderHeight", typeof(Double), typeof(CustomContentPane), new PropertyMetadata(22.0));

		public Double HeaderHeight
		{
			get { return (Double) GetValue(HeaderHeightProperty); }
			set { SetValue(HeaderHeightProperty, value); }
		}

		public System.Windows.Media.Color AccentColour
		{
			get { return (System.Windows.Media.Color)GetValue( AccentColourProperty); }
			set { SetValue( AccentColourProperty, value); }
		}

		public static readonly DependencyProperty  AccentColourProperty =
			DependencyProperty.Register("AccentColour", typeof(System.Windows.Media.Color), typeof(CustomContentPane), new UIPropertyMetadata(System.Windows.Media.Colors.Transparent));

		public Visibility MaximizeButtonVisibility
		{
			get { return (Visibility)GetValue(MaximizeButtonVisibilityProperty); }
			set { SetValue(MaximizeButtonVisibilityProperty, value); }
		}

		// Using a DependencyProperty as the backing store for MaximizeButtonVisibility.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty MaximizeButtonVisibilityProperty =
			DependencyProperty.Register("MaximizeButtonVisibility", typeof(Visibility), typeof(CustomContentPane), new UIPropertyMetadata(System.Windows.Visibility.Visible));

	    private readonly WindowViewContainerHostHelper _helper;
		public CustomContentPane()
		{
			InitializeComponent();  
			this.OptionsMenuOpening += new System.EventHandler<Infragistics.Windows.DockManager.Events.PaneOptionsMenuOpeningEventArgs>(CustomContentPane_OptionsMenuOpening);
			this.Loaded += (s, e) => this.UpdateContainer(true); 
            _helper = new WindowViewContainerHostHelper(this);

            this.ExecutingCommand += (CustomContentPane_ExecutingCommand);
            this.ExecutedCommand += (CustomContentPane_ExecutedCommand);
			DependencyPropertyDescriptor.FromProperty(HeaderVisibilityProperty, typeof (ContentPane)).AddValueChanged(this,
																											  OnHeaderVisibilityChanged);
            unpinnedPaneContainerType = typeof (DockManagerPanel).GetNestedType("UnpinnedPaneContainer", BindingFlags.NonPublic);
		    isHiddenUnpinnedProperty = (DependencyProperty) unpinnedPaneContainerType.GetField("IsHiddenUnpinnedProperty").GetValue(null);
            DependencyPropertyDescriptor.FromProperty(isHiddenUnpinnedProperty, unpinnedPaneContainerType).AddValueChanged(this, HandleIsUnpinnedHiddenChanged);

			if (ThemeExtensions.SubTabHeaderTooltipsEnabled)
			{
                FrameworkElementFactory textFactory = new FrameworkElementFactory(typeof(TextBlock));
                textFactory.SetBinding(TextBlock.TextProperty, new Binding("."));
                textFactory.SetBinding(ToolTipProperty, new Binding("DataContext.Tooltip") { Source = this });
                var tabHeaderWithTooltip = new DataTemplate() { VisualTree = textFactory };
                tabHeaderWithTooltip.Seal();
                TabHeaderTemplate = tabHeaderWithTooltip;

			}
			MaximizeButtonVisibility = ShellModeExtension.GetVisibilityForElement(HideLockUIGranularity.HideViewMaximizeButton);

		    this.CommandBindings.Add(new CommandBinding(Command.CloseLastInTabGroupCommand, (sender, args) =>
		        {
                    var tgp = args.Parameter as DependencyObject;
                    while (tgp != null)
                    {
                        tgp = tgp.GetParentObject();
                        if (tgp is TabGroupPane)
                        {
		                    var dm = XamDockManager.GetDockManager(this);
                            foreach (var pane in dm.GetPanes(PaneNavigationOrder.ActivationOrder))
                            {
                                if (pane.Parent != tgp)
                                    continue;

                                (pane.DataContext as WindowViewModel).Close();
                                return;
                            }
                        }
                    }

		        }, (sender, args) => { args.CanExecute = true; }  ));

            DependencyPropertyDescriptor.FromProperty(AccentColourProperty, typeof(CustomContentPane)).AddValueChanged(this, AccentColourChanged);
            DependencyPropertyDescriptor.FromProperty(IsPinnedProperty, typeof(ContentPane)).AddValueChanged(this, IsPinnedChanged);
		    HeaderHeight = ThemeExtensions.HeaderHeightDefault; 
		}

        void CustomContentPane_ExecutingCommand(object sender, Infragistics.Windows.Controls.Events.ExecutingCommandEventArgs e)
        {
            if ((e.Command == ContentPaneCommands.ToggleDockedState || e.Command == ContentPaneCommands.ChangeToFloatingOnly) &&
                !this.IsFloating()) 
            {
                var arg = new CancelPaneEventArgs(this);
                XamDockManagerExtensions.InvokeTearingOffPane(arg);
                if (arg.Cancel) e.Cancel = true;
            }
            else if ((e.Command == ContentPaneCommands.ToggleDockedState || e.Command == ContentPaneCommands.ChangeToDockable) &&
    this.IsFloating())
            {
                var arg = new CancelPaneEventArgs(this);
                XamDockManagerExtensions.InvokeDockingPane(arg);
                if (arg.Cancel) e.Cancel = true;

            }
        }

        void CustomContentPane_ExecutedCommand(object sender, Infragistics.Windows.Controls.Events.ExecutedCommandEventArgs e)
        {
            if ((e.Command == ContentPaneCommands.ToggleDockedState || e.Command == ContentPaneCommands.ChangeToFloatingOnly) &&
                 this.IsFloating())
            {
                 var arg = new PaneEventArgs(this);
                 XamDockManagerExtensions.InvokeToreOffPane(arg);
            }
            else if ((e.Command == ContentPaneCommands.ToggleDockedState || e.Command == ContentPaneCommands.ChangeToDockable) &&
    !this.IsFloating())
            {

                var arg = new PaneEventArgs(this);
                XamDockManagerExtensions.InvokeDockedPane(arg);

            }
        }
        internal PaneHeaderControl HeaderControl { get; set; }

        protected override void OnGotKeyboardFocus(KeyboardFocusChangedEventArgs e)
        { 

            base.OnGotKeyboardFocus(e);
            WindowViewModel model = this.DataContext as WindowViewModel;
            if (model != null)
            {
                model.IsActive = true;
            }
        }

        protected override void OnGotFocus(RoutedEventArgs e)
        {

            base.OnGotFocus(e);
            WindowViewModel model = this.DataContext as WindowViewModel;
            if (model != null)
            {
                model.IsActive = true;
            }
        }

	    private void HandleIsUnpinnedHiddenChanged(object sender, EventArgs eventArgs)
	    {
            if (DataContext as WindowViewModel != null &&
                ((IWindowViewContainer)((WindowViewModel) DataContext).ViewContainer).StayPinnedWhenRevealed &&
                !(bool)GetValue(isHiddenUnpinnedProperty))
            {
                IsPinned = true;
            }
	    }

	    /// <summary>
        /// Bindings do not work unless the window is visible. This leads to issues if the InvisibleMainController is enabled.
        /// </summary>
	    private void IsPinnedChanged(object sender, EventArgs eventArgs)
	    {
            if (!IsVisible)
            {
                var model = DataContext as WindowViewModel;
                if (model != null)
                {
                    model.IsPinned = IsPinned;
                }
            }
	    }
        
	    private void AccentColourChanged(object sender, EventArgs eventArgs)
	    {
            UpdateContainer(true);
	    }


		
		private void OnHeaderVisibilityChanged(object sender, EventArgs e)
		{
		    if (compactModeValue)
		    {
		        HeaderVisible = Visibility.Collapsed;
		    }
		    else
		    {
		        var isDocument = XamDockManager.GetPaneLocation(this) == PaneLocation.Document;
				HeaderVisible = HeaderVisibleLock == Visibility.Collapsed ? HeaderVisibleLock : 
                	(isDocument ? Visibility.Visible : HeaderVisibility);
		    }
		}

		public void ReleaseEventHandlers()
		{
			DependencyPropertyDescriptor.FromProperty(HeaderVisibilityProperty, typeof(ContentPane)).RemoveValueChanged(this, OnHeaderVisibilityChanged);
            DependencyPropertyDescriptor.FromProperty(AccentColourProperty, typeof(CustomContentPane)).RemoveValueChanged(this, AccentColourChanged);
            DependencyPropertyDescriptor.FromProperty(IsPinnedProperty, typeof(CustomContentPane)).RemoveValueChanged(this, IsPinnedChanged);
            DependencyPropertyDescriptor.FromProperty(isHiddenUnpinnedProperty, unpinnedPaneContainerType).RemoveValueChanged(this, HandleIsUnpinnedHiddenChanged); 
        }
         
	    private void CustomContentPane_OptionsMenuOpening(object sender,
														  Infragistics.Windows.DockManager.Events.
															  PaneOptionsMenuOpeningEventArgs e)
		{
			var contentPane = e.Source as ContentPane;
			if (contentPane == null)
				return;
             
            e.Items.Add(new Separator());
	        var model = (WindowViewModel) DataContext;
			if (!ShellModeExtension.HideUIGranularity.HasFlag(HideLockUIGranularity.DisableViewHeaderContextMenuRename) &&
                string.IsNullOrEmpty(model.WorkspaceCategory)) //disable renaming for workspaces
			{
				var item = _helper.CreateOptionsMenuItem("Rename", RenameMenuItem_Click);

				if (DockViewModel.RenameDisabled)
					item.IsEnabled = false;

				e.Items.Add(item);
			}

            var extraItems = ((WindowViewContainer)((WindowViewModel)DataContext).ViewContainer).ExtraContextMenuItems;
		    if (extraItems.Count > 0)
		    {
		        e.Items.Add(new Separator());
		        foreach (var extraItem in extraItems)
		        {
		            var logicalParent = LogicalTreeHelper.GetParent((DependencyObject) extraItem.Content) as ContextMenu;
                    if (logicalParent != null)
                    {
                        logicalParent.Items.Remove(extraItem.Content);
                    }
		            e.Items.Add(extraItem.Content);
		        }
		    }

		    var tgp = this.Parent as TabGroupPane;
			if (tgp != null)
			{
                 
                e.Items.Add(new Separator());

				var i = tgp.Items.IndexOf(this);
				if (!ShellModeExtension.HideUIGranularity.HasFlag(HideLockUIGranularity.DisableSubtabContextMenuMove))
				{
					if (i > 0)
					{
                        e.Items.Add(_helper.CreateOptionsMenuItem("Move left", MoveLeftMenuItem_Click));
					}
					if (i < tgp.Items.Count - 1)
					{
                        e.Items.Add(_helper.CreateOptionsMenuItem("Move right", MoveRightMenuItem_Click));
					}
				}
                if (!model.HideGranularity.HasFlag(HideLockUIGranularity.DisableViewHeaderContextMenuClose))
				{
                    e.Items.Add(_helper.CreateOptionsMenuItem("Close", CloseMenuItem_Click));
                    e.Items.Add(_helper.CreateOptionsMenuItem("Close all", CloseAllMenuItem_Click));
                    e.Items.Add(_helper.CreateOptionsMenuItem("Close others", CloseOthersMenuItem_Click));
				}
			}

		    var pinItem = e.Items.Cast<object>().FirstOrDefault(m => m is MenuItem && (m as MenuItem).Command == ContentPaneCommands.TogglePinnedState);
            if (pinItem != null) e.Items.Remove(pinItem);
            var closeItem = e.Items.Cast<object>().FirstOrDefault(m => m is MenuItem && (m as MenuItem).Command == ContentPaneCommands.Close);
			if (closeItem != null) e.Items.Remove(closeItem);
            
            #region Legacy Workspace
            //use new workspace saving menus in this case
            if (ChromeManagerBase.WorkspaceSaveLoadEnabled && string.IsNullOrEmpty(model.WorkspaceCategory) && 
                Window.GetWindow(this) != System.Windows.Application.Current.MainWindow) 
            {
                e.Items.Add(_helper.CreateOptionsMenuItem("Save workspace",
                                                  (s, ea) => Command.SaveSublayoutCommand.Execute(null, null)));

            }

            #endregion
            _helper.FixOptionsMenuItems(e.Items);

            //clear out the overriding of DefaultStyleKeyProperty to make the menu item created by IG and msdesktop to be all use the themed menu item styles 
		    foreach (var item in e.Items)
		    {
		        MenuItem menuItem = item as MenuItem;
                if (menuItem != null)
                {
                    menuItem.ClearValue(FrameworkElement.DefaultStyleKeyProperty);
                }
		    }
		}

		
		private void CloseOthersMenuItem_Click(object sender, RoutedEventArgs routedEventArgs)
		{
			if (Parent is TabGroupPane)
			{
				ContentPane[] panes = new ContentPane[(Parent as TabGroupPane).Items.Count];
				(Parent as TabGroupPane).Items.CopyTo(panes, 0);
				foreach (ContentPane pane in panes.Where(p => p != this))
				{
					pane.ExecuteCommand(ContentPaneCommands.Close);
				}
			}
		}



		void RenameMenuItem_Click(object sender, RoutedEventArgs e)
		{
			FrameworkElement o = this;
			while (o != null && !(o is SplitPane))
			{
				o = o.Parent as FrameworkElement;
			}

			var sp = o as SplitPane;
			if (sp != null)
			{
				Command.RenameTitleCommand.Execute(null, sp);
			}
		}

		void MoveLeftMenuItem_Click(object sender, RoutedEventArgs e)
		{
			var tgp = this.Parent as TabGroupPane;
			if (tgp != null)
			{
				var i = tgp.Items.IndexOf(this);
				if (i > 0)
				{
					tgp.Items.Remove(this);
					tgp.Items.Insert(i - 1, this);
                    tgp.Items.Refresh();
				    tgp.SelectedItem = this;
				}
			}
		}

		void MoveRightMenuItem_Click(object sender, RoutedEventArgs e)
		{
			var tgp = this.Parent as TabGroupPane;
			if (tgp != null)
			{
				var i = tgp.Items.IndexOf(this);
				if (i < tgp.Items.Count - 1)
				{
					tgp.Items.Remove(this);
					tgp.Items.Insert(i + 1, this);
                    tgp.Items.Refresh();
				    tgp.SelectedItem = this;
				}
			}
		}

		void CloseMenuItem_Click(object sender, RoutedEventArgs e)
		{
			this.ExecuteCommand(ContentPaneCommands.Close);
		}

		void CloseAllMenuItem_Click(object sender, RoutedEventArgs e)
		{
			if (Parent is TabGroupPane)
			{
				ContentPane[] panes = new ContentPane[(Parent as TabGroupPane).Items.Count];
				(Parent as TabGroupPane).Items.CopyTo(panes, 0);
				foreach (ContentPane pane in panes)
				{
					pane.ExecuteCommand(ContentPaneCommands.Close);
				}
			}
		}
         
		public bool CanChangeDimensions
		{
            get
            {
                CustomPaneToolWindow cptw = (CustomPaneToolWindow)ToolWindow.GetToolWindow(this);
                int paneCount = XamDockHelper.CountPanes(cptw);
                var wvm = this.DataContext as WindowViewModel;

                return (paneCount == 1) &&
                    (wvm.InitialParameters.SizingMethod == Core.SizingMethod.Custom) &&
                    this.IsFloating();
            } 
		}
         

        internal void SetContentWidth(double p)
        {
            _helper.SetContentWidth(p);
        }
		internal void SetContentHeight(double p)
		{ 
             _helper.SetContentHeight(p);
		}
        
        internal void SetContentMaximumWidth(double floatingWindowMaximumWidth)
        { 
           _helper.SetContentMaximumWidth(floatingWindowMaximumWidth);

        }
        internal void SetContentMinimumWidth(double floatingWindowMinimumWidth)
        {
            _helper.SetContentMinimumWidth(floatingWindowMinimumWidth);

        }
        internal void SetContentMaximumHeight(double floatingWindowMaximumHeight)
        { 
            _helper.SetContentMaximumHeight(floatingWindowMaximumHeight);
        }

        internal void SetContentMinimumHeight(double floatingWindowMinimumHeight)
        {
            _helper.SetContentMinimumHeight(floatingWindowMinimumHeight);
        }
        
	    public override void OnApplyTemplate()
	    {
	        base.OnApplyTemplate();
            var titlePart = GetTemplateChild("txtCaption") as EditableTextBlock; 
	    }

	    private CustomPaneToolWindow parentWindow = null;
		internal void UpdateContainer(bool setAttached = false)
		{
			if (setAttached)
 			{
                DependencyObject parent = this.Parent;
                while (parent != null)
                {
                    if (parent is CustomPaneToolWindow)
                    {
                        this.parentWindow = parent as CustomPaneToolWindow;
                        CustomContentPane.SetActivePane(this.parentWindow, this);

                        if (((IWindowViewContainerHost)this).View != null)
                        {
                            parentWindow.AllowMaximize = ((IWindowViewContainerHost)this).View.CanMaximize;
                        }

                        var dm = XamDockManager.GetDockManager(this);
                        foreach (var pane in dm.GetPanes(PaneNavigationOrder.ActivationOrder))
                        {
                            if (Window.GetWindow(pane) != Window.GetWindow(this))
                                continue;

                            if (pane == this)
                            {
                                parentWindow.AccentColour = this.AccentColour;
                            }
                            break;
                        }
                        break;
                    }

                    parent = LogicalTreeHelper.GetParent(parent);
                }
            }

		    if (CustomContentPane.CompactTabGroupMode)
		    {
		        //UpdateTabGroupMap(this);
		        this.compactModeValue = (this.Parent is TabGroupPane) && ((TabGroupPane)Parent).IsTabItemAreaVisible;
		        OnHeaderVisibilityChanged(this, EventArgs.Empty);
		    }
		    //if (this.parentWindow != null)
		    //{
		    //    BindingOperations.ClearBinding(this.parentWindow, CustomPaneToolWindow.AccentColourProperty);
		    //}

		    //DependencyObject parent = this.Parent;
		    //while (parent != null)
		    //{
		    //    if (parent is CustomPaneToolWindow)
		    //    {
		    //        this.parentWindow = parent as CustomPaneToolWindow;

		    //        if (setAttached)
		    //        {
		    //            CustomContentPane.SetActivePane(this.parentWindow, this);
		    //        }
		    //        this.parentWindow.Title = (this.Header as string) ?? " ";    

		    //        BindingOperations.SetBinding(this.parentWindow, CustomPaneToolWindow.AccentColourProperty, new Binding() { Source = this, Path = new PropertyPath("AccentColour") });

		    //        break;
		    //    }

		    //    parent = LogicalTreeHelper.GetParent(parent);
		    //}
             
		}
        /*private Dictionary<TabGroupPane, List<CustomContentPane>> tabGroupMap = new Dictionary<TabGroupPane, List<CustomContentPane>>();
	    private void UpdateTabGroupMap(CustomContentPane pane)
	    {
	        var tgp = pane.Parent as TabGroupPane;
            if (tgp != null)
            {
                if (!tabGroupMap.ContainsKey(tgp))
                    tabGroupMap.Add(tgp, new List<CustomContentPane>());

                tabGroupMap[tgp].Add(pane);
            }
	    }*/

	    public static bool CompactTabGroupMode = false;

		private bool m_topmost = false;
		public bool Topmost
		{
			get
			{
				return this.m_topmost;
			}
			set
			{
				if (this.m_topmost != value)
				{
					this.m_topmost = value;
					var vm = this.DataContext as WindowViewModel;
					if (vm != null)
					{
						vm.Topmost = value;
					}
				    var window = ToolWindow.GetToolWindow(this) as CustomPaneToolWindow;
                    if (window != null && window.Topmost != value)
                    {
                        window.ToggleTopmost();
                    }
				}
			}
		}

		private Visibility headerVisibleLock;
		public System.Windows.Visibility HeaderVisibleLock
		{
			get { return headerVisibleLock; }
			set { headerVisibleLock = value;
				OnHeaderVisibilityChanged(this, EventArgs.Empty);
			}
		}

		private bool m_showInTaskbar = true;
		public bool ShowInTaskbar
		{
			get
			{
				return this.m_showInTaskbar;
			}
			set
			{
                this.m_showInTaskbar = value;
			}
		}

		private bool m_allowPartiallyOffscreen = true;

	    private bool compactModeValue;
	    private readonly DependencyProperty isHiddenUnpinnedProperty;
	    private readonly Type unpinnedPaneContainerType;

	    public bool AllowPartiallyOffscreen
		{
			get
			{
				return this.m_allowPartiallyOffscreen;
			}
			set
			{
                this.m_allowPartiallyOffscreen = value;

			}
		}

	    public bool NonuniformDocumentTabs
	    {
	        get
	        {
	            return NonuniformDocumentTabsMode;
	        }
	    }


        IWindowViewContainer IWindowViewContainerHost.View
        {
            get 
            { 
                WindowViewModel model = this.DataContext as WindowViewModel;
                if (model == null) return null;
                return model.ViewContainer as IWindowViewContainer;
            }
        }

        IFloatingWindow IWindowViewContainerHost.FloatingWindow
	    {
	        get
	        {
                var floatingWindow = ToolWindow.GetToolWindow(this) as IFloatingWindow;
	            if (floatingWindow != null) return floatingWindow;
                var pRoot = this.Parent;
                while (pRoot != null)
                {
                    if (pRoot is IFloatingWindow) return pRoot as IFloatingWindow;
                    pRoot = pRoot.GetParentObject();
                }

                return pRoot as IFloatingWindow;
	        }
	    }
        #region  CUIT

	    protected override AutomationPeer OnCreateAutomationPeer()
	    {
	        var inner = base.OnCreateAutomationPeer();
	        var peer = new CustomContentPaneAutomationPeer(inner, this);
	        return peer;
	    }

	    private class CustomContentPaneAutomationPeer: RecordImageAutomationPeerWrapper
        {
            public CustomContentPaneAutomationPeer(AutomationPeer inner, FrameworkElement owner) : base(inner, owner)
            {
                RecordImage();
            }

            private CustomContentPane MyOwner { get { return Owner as CustomContentPane; } }

            protected override string GetClassNameCore()
            {
                return typeof (CustomContentPane).Name;
            }

            protected override string GetAutomationIdCore()
            {
                var dc = MyOwner.DataContext as WindowViewModel;
                if (dc == null)
                    return "";

                return dc.ViewContainer.ID;
            }
            
        }
        #endregion

	    public void SetCanMaximize(bool canMaximize)
	    {
            var cptw = (CustomPaneToolWindow)ToolWindow.GetToolWindow(this);
            if (cptw != null)
            {
                cptw.AllowMaximize = canMaximize;
                if (!canMaximize && cptw.WindowState == WindowState.Maximized)
                {
                    cptw.WindowState = WindowState.Normal;
                }
            }
	    }

	    public void SetOwnedByContainer()
	    {
            var cptw = (CustomPaneToolWindow)ToolWindow.GetToolWindow(this);
	        if (cptw != null)
	        {
	            CustomPaneToolWindow.RefreshIsOwnedWindow(cptw);
	        } 
	    } 
 
    }


   
}
