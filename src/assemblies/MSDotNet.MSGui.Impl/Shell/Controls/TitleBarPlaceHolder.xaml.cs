﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using MorganStanley.MSDotNet.MSGui.Controls.Controls;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
    /// <summary>
    /// Interaction logic for TitleBar.xaml
    /// </summary>
    public partial class TitleBarPlaceHolder : UserControl
    {
        internal static bool ChangeTitlebarColorOnMaximize = true;

        internal TitleBarPlaceHolder()
        {
            InitializeComponent();
            ChangeColorOnMaximize = ChangeTitlebarColorOnMaximize;
        }



        public Visibility RibbonVisibility
        {
            get { return (Visibility)GetValue(RibbonVisibilityProperty); }
            set { SetValue(RibbonVisibilityProperty, value); }
        }

        // Using a DependencyProperty as the backing store for RibbonVisibility.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RibbonVisibilityProperty =
            DependencyProperty.Register("RibbonVisibility", typeof(Visibility), typeof(TitleBarPlaceHolder), new UIPropertyMetadata(Visibility.Collapsed));

        

        
        public Visibility AppIconVisibility { get;set; }
        

        private void HideDefault()
        {
            _titleBlock.Visibility = Visibility.Collapsed;
            AppIconVisibility = Visibility.Collapsed;
            _titleBlock.Visibility = Visibility.Collapsed;
        }

        public void AddToLeft(UIElement child)
        {
            _left.Children.Add(child);
            HideDefault();
        }

        public void AddToMiddle(UIElement child)
        {
            _middle.Children.Add(child);
            HideDefault();
        }

        public void AddToRight(UIElement child)
        {
            _right.Children.Add(child);
            if (child is EnvironmentLabel) return;
            HideDefault();
        }

        public bool ChangeColorOnMaximize
        {
            get { return (bool)GetValue(ChangeColorOnMaximizeProperty); }
            set { SetValue(ChangeColorOnMaximizeProperty, value); }
        }

        public static readonly DependencyProperty ChangeColorOnMaximizeProperty =
            DependencyProperty.Register("ChangeColorOnMaximize", typeof(bool), typeof(TitleBarPlaceHolder), new UIPropertyMetadata(true));

    }


    [ValueConversion(typeof(Visibility), typeof(HorizontalAlignment))]
    internal class AlignmentConverter : IValueConverter
    {
        public AlignmentConverter()
        {
            
        }
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var visibility = (Visibility)value;
            if (visibility == Visibility.Visible)
            {
                return HorizontalAlignment.Center;
            }
            else
            {
                return HorizontalAlignment.Left;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
