﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Controls/ViewContainerHeaderControl.cs#17 $
// $Change: 882285 $
// $DateTime: 2014/05/26 09:32:19 $
// $Author: milosp $

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Infragistics.Windows.DockManager;
using MorganStanley.MSDotNet.MSGui.Core;
using System.Windows.Media;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
	[TemplatePart(Name = ViewContainerHeaderControl.TitlePartName, Type = typeof(TextBox))]
	[TemplatePart(Name = ViewContainerHeaderControl.HeaderItemsPartName, Type = typeof(HeaderItemsHolder))]
	[TemplatePart(Name = ViewContainerHeaderControl.HeaderImagePartName, Type = typeof(Image))]
	public class ViewContainerHeaderControl : Control
	{
	    public ViewContainerHeaderControl()
	    {
            HeaderHeight = ThemeExtensions.HeaderHeightDefault;
	    }

		public static readonly DependencyProperty HeaderHeightProperty =
			DependencyProperty.Register("HeaderHeight", typeof(Double), typeof(ViewContainerHeaderControl), new PropertyMetadata(22.0));

		public Double HeaderHeight
		{
			get { return (Double) GetValue(HeaderHeightProperty); }
			set { SetValue(HeaderHeightProperty, value); }
		}

		public static readonly DependencyProperty TitleColumnProperty =
			DependencyProperty.Register("TitleColumn", typeof (Int32), typeof (ViewContainerHeaderControl),
										new PropertyMetadata(ThemeExtensions.HeaderItemPositionBeforeTitle ? 2 : 1));

		public Int32 TitleColumn
		{
			get { return (Int32) GetValue(TitleColumnProperty); }
			set { SetValue(TitleColumnProperty, value); }
		}

		public static readonly DependencyProperty TitleAlignmentProperty =
            DependencyProperty.Register("TitleAlignment", typeof(HorizontalAlignment), typeof(ViewContainerHeaderControl), new PropertyMetadata(ThemeExtensions.HeaderItemPositionBeforeTitle ? HorizontalAlignment.Center : HorizontalAlignment.Left));

		public HorizontalAlignment TitleAlignment
		{
			get { return (HorizontalAlignment) GetValue(TitleAlignmentProperty); }
			set { SetValue(TitleAlignmentProperty, value); }
		}

		public static readonly DependencyProperty SecondColumnProperty =
		DependencyProperty.Register("SecondColumn", typeof(GridLength), typeof(ViewContainerHeaderControl), new PropertyMetadata(new GridLength(1, ThemeExtensions.HeaderItemPositionBeforeTitle ? GridUnitType.Auto : GridUnitType.Star)));

		public GridLength SecondColumn
		{
			get { return (GridLength)GetValue(SecondColumnProperty); }
			set { SetValue(SecondColumnProperty, value); }
		}

		public static readonly DependencyProperty ThirdColumnProperty =
			DependencyProperty.Register("ThirdColumn", typeof(GridLength), typeof(ViewContainerHeaderControl), new PropertyMetadata(new GridLength(1, ThemeExtensions.HeaderItemPositionBeforeTitle ? GridUnitType.Star : GridUnitType.Auto)));

		public GridLength ThirdColumn
		{
			get { return (GridLength) GetValue(ThirdColumnProperty); }
			set { SetValue(ThirdColumnProperty, value); }
		}

		public static readonly DependencyProperty ItemColumnProperty =
			DependencyProperty.Register("ItemColumn", typeof (Int32), typeof (ViewContainerHeaderControl),
										new PropertyMetadata(ThemeExtensions.HeaderItemPositionBeforeTitle ? 1 : 2));

		public Int32 ItemColumn
		{
			get { return (Int32) GetValue(ItemColumnProperty); }
			set { SetValue(ItemColumnProperty, value); }
		}



        public double TextOffset
        {
            get { return (double)GetValue(TextOffsetProperty); }
            set { SetValue(TextOffsetProperty, value); }
        }

        public static readonly DependencyProperty TextOffsetProperty = DependencyProperty.Register("TextOffset", typeof(double), typeof(ViewContainerHeaderControl), new UIPropertyMetadata(0.0d));



		private const string TitlePartName = "PART_Title";
		private const string HeaderImagePartName = "PART_Image";
		private const string HeaderItemsPartName = "PART_HeaderItems";

		static ViewContainerHeaderControl()
		{
			DefaultStyleKeyProperty.OverrideMetadata(typeof(ViewContainerHeaderControl),
				  new FrameworkPropertyMetadata(typeof(ViewContainerHeaderControl)));
		}

		public static readonly DependencyProperty ContentPaneProperty =
			DependencyProperty.Register("ContentPane", typeof(ContentPane), typeof(ViewContainerHeaderControl),
				new FrameworkPropertyMetadata(null,
					FrameworkPropertyMetadataOptions.AffectsRender |
					FrameworkPropertyMetadataOptions.AffectsParentMeasure, OnContentPaneChanged));

		public static readonly DependencyProperty TitleOnlyProperty =
			DependencyProperty.Register("TitleOnly", typeof(bool), typeof(ViewContainerHeaderControl), new UIPropertyMetadata(false));

		private static void OnContentPaneChanged(DependencyObject d_, DependencyPropertyChangedEventArgs e_)
		{
			if (!(d_ is ViewContainerHeaderControl))
			{
				return;
			}

			ViewContainerHeaderControl control = (ViewContainerHeaderControl)d_;
			if (e_.NewValue == e_.OldValue)
			{
				return;
			}
			if (e_.NewValue != null)
			{
				control.SetBindings((ContentPane)e_.NewValue);
			}
			else
			{
				control.Clear();
			}
		}

		public ContentPane ContentPane
		{
			get
			{
				return (ContentPane)GetValue(ContentPaneProperty);
			}
			set
			{
				SetValue(ContentPaneProperty, value);
			}
		}

		public bool TitleOnly
		{
			get { return (bool)GetValue(TitleOnlyProperty); }
			set { SetValue(TitleOnlyProperty, value); }
		}

		public EditableTextBlock TextControl
		{
			get
			{
				return m_titleTextBlockEditableTextBlock;
			}
		}

		private EditableTextBlock m_titleTextBlockEditableTextBlock;
		private HeaderItemsHolder m_headerItemsHolder;
		private Image m_image;

		public override void OnApplyTemplate()
		{
			base.OnApplyTemplate();

			m_titleTextBlockEditableTextBlock = GetTemplateChild(TitlePartName) as EditableTextBlock;
			m_headerItemsHolder = GetTemplateChild(HeaderItemsPartName) as HeaderItemsHolder;
			m_image = GetTemplateChild(HeaderImagePartName) as Image;

			ContentPane cp = ContentPane;
			if (cp != null)
			{
				SetBindings(cp);
			}
			else
			{
				Clear();
			}
		}

		private void Clear()
		{
			if (m_titleTextBlockEditableTextBlock != null)
			{
				BindingOperations.ClearBinding(m_titleTextBlockEditableTextBlock, TextBlock.TextProperty);
				m_titleTextBlockEditableTextBlock.IsInEditMode = false;
				m_titleTextBlockEditableTextBlock.DoneEdit -= this.doneEdit;
				//BindingOperations.ClearBinding(m_titleTextBlockEditableTextBlock, EditableTextBlock.IsInEditModeProperty);
			}
			if (m_image != null)
			{
				BindingOperations.ClearBinding(m_image, Image.SourceProperty);
			}
			if (m_headerItemsHolder != null)
			{
				m_headerItemsHolder.ItemsSource = null;
			}
			this.Visibility = Visibility.Collapsed;

			if (this.boundPane != null)
			{
				Attached.RemoveTitleNeedChangingHandler(this.boundPane, titleNeedChangingHandler);
				boundPane = null;
			}
		}

		private ContentPane boundPane = null;
		private RoutedEventHandler titleNeedChangingHandler = null;
		private EventHandler doneEdit = null;
		private void SetBindings(ContentPane pane_)
		{
			Clear();
			this.Visibility = Visibility.Visible;
			if (m_titleTextBlockEditableTextBlock != null)
			{
				this.boundPane = pane_;
				Binding headerBinding = new Binding()
				{
					Source = pane_,
					Path = new PropertyPath("Header"),
					Mode = BindingMode.TwoWay
				};

				m_titleTextBlockEditableTextBlock.SetBinding(TextBlock.TextProperty, headerBinding);

				this.titleNeedChangingHandler = (x, y) =>
				{
					if (m_titleTextBlockEditableTextBlock.IsVisible)
					{
						m_titleTextBlockEditableTextBlock.IsInEditMode = true;
					}
					//y.Handled = true;
				};

				/*
				 * This figures out if this ViewContainerHeaderControl is owned by a CustomPaneToolWindow and not a 
				 * CustomContentPane. We need to know this so that we don't enable renaming of the floating container 
				 * window if it has multiple children.
				 */
				bool topLevel = true;
				DependencyObject vchcParent = this.Parent;
				CustomPaneToolWindow window = null;
				while (vchcParent != null)
				{
					if (vchcParent is CustomContentPane)
					{
						topLevel = false;
						break;
					}
					else if (vchcParent is CustomPaneToolWindow)
					{
						window = vchcParent as CustomPaneToolWindow;
						break;
					}

					vchcParent = VisualTreeHelper.GetParent(vchcParent);
				}

				if (window != null && topLevel)
				{
					if (XamDockHelper.CountPanes(window) == 1)
					{
						Attached.AddTitleNeedChangingHandler(pane_, this.titleNeedChangingHandler);
					}
				}
				else
				{
					Attached.AddTitleNeedChangingHandler(pane_, this.titleNeedChangingHandler);
				}



				this.doneEdit = (x, y) =>
				{
					if (ContentPane != null)
					{
						ContentPane.Activate();
					}
				};

				m_titleTextBlockEditableTextBlock.DoneEdit += this.doneEdit;

			}

			IViewContainer viewContainer = Attached.GetViewContainer(pane_);
			if (m_headerItemsHolder != null && viewContainer != null && !this.TitleOnly)
			{
				m_headerItemsHolder.ItemsSource = viewContainer.HeaderItems;
			}

            if ((m_image != null) && (!ShellModeExtension.HideUIGranularity.HasFlag(HideLockUIGranularity.HideHeaderIcon)))
			{
				Binding headerBinding = new Binding()
				{
					Source = pane_,
					Path = new PropertyPath("Image")
				};
				m_image.SetBinding(Image.SourceProperty, headerBinding);
			}
            else
            {
                if (m_image!=null)
                    m_image.Visibility = Visibility.Collapsed;
            }
		}
		static internal bool StickyNow;
//	    private static int count;
	}

    public class LeftMarginConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return new Thickness((value is double) ? (double)value : 0, 0, 0, 0);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return 0;
        }
    }
}