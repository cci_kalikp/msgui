﻿////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Controls/DialogWindow.cs#36 $
// $Change: 900785 $
// $DateTime: 2014/10/14 04:41:09 $
// $Author: caijin $

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Reflection;
using System.Windows;
using System.Windows.Input;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Helpers;
using MorganStanley.MSDotNet.MSGui.Impl.ModernTheme;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.MicrosoftShell;
using MorganStanley.MSDotNet.MSGui.Impl.View;
using Brush = System.Windows.Media.Brush;
using Color = System.Windows.Media.Color;
using Size = System.Windows.Size;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
    [TemplatePart(Name = DialogWindow.HeaderItemsPartName, Type = typeof(HeaderItemsHolder))]
    public class DialogWindow : Window, IDialogWindow, IWindowViewContainerInternal
    {
        private const string HeaderItemsPartName = "PART_HeaderItems";
        private HeaderItemsHolder m_headerItemsHolder;

        private Icon m_icon;

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            m_headerItemsHolder = GetTemplateChild(HeaderItemsPartName) as HeaderItemsHolder;

            if (m_headerItemsHolder != null)
            {
                m_headerItemsHolder.ItemsSource = HeaderItems;
            }
        }

        public DialogWindow(Window owner_)
        {
            if (MorganStanley.MSDotNet.MSGui.Impl.Extensions.ShellModeExtension.InvisibleMainControllerMode == ShellModeExtension.InvisibleMainControllerModeEnum.None)
            {
                Owner = owner_;
            }
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            CommandBinding closeBinding = new CommandBinding(SystemCommands.CloseWindowCommand, OnCloseCommand);
            CommandBindings.Add(closeBinding);
            CommandBinding restoreBinding = new CommandBinding(SystemCommands.RestoreWindowCommand, OnRestoreCommand);
            CommandBindings.Add(restoreBinding);
            CommandBinding maxBinding = new CommandBinding(SystemCommands.MaximizeWindowCommand, OnMaximizeCommand);
            CommandBindings.Add(maxBinding);
            CommandBinding minBinding = new CommandBinding(SystemCommands.MinimizeWindowCommand, OnMinimizeCommand);
            CommandBindings.Add(minBinding);

            (this as Window).Closed += (sender, args) => InvokeClosed(new WindowEventArgs(this));
            (this as Window).Closing += (o, eventArgs) => InvokeClosing(new CancelEventArgs());
            this.SourceInitialized += new EventHandler(DialogWindow_SourceInitialized);

            HeaderHeight = ThemeExtensions.HeaderHeightFloatingDefault;

            if (ModernThemeHelper.IsModernThemeEnabled)
            {
                this.Resources.MergedDictionaries.Add(new ResourceDictionary
                {
                    Source = new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/MSDesktop2014/Controls/TextBlock.xaml")
                });
            }
            this.MinHeight = this.MinWidth = 100;
        }

        void DialogWindow_SourceInitialized(object sender, EventArgs e)
        {
            FloatingWindowUtil.FixResize(this);
            this.EnableMinimizeScreenshotSupport();
            if (FrameworkExtensions.DefaultStickWindowOptions != StickyWindowOptions.None)
            {
                new WPFStickyWindow(this)
                {
                    StickToScreen = FrameworkExtensions.DefaultStickWindowOptions.HasFlag(StickyWindowOptions.StickToScreen),
                    StickToOther = FrameworkExtensions.DefaultStickWindowOptions.HasFlag(StickyWindowOptions.StickToOther)
                };
            }
        }

        protected override void OnContentChanged(object oldContent, object newContent)
        {
            base.OnContentChanged(oldContent, newContent);
            var handler = ContentChanged;
            if (handler != null)
            {
                var eventArgs = new ContentChangedEventArgs(oldContent, newContent);
                handler(this, eventArgs);
            }
        }

        // These are now only accessible via the deprecated interface; however, the new interface
        // allows setting ResizeMode which can hide the buttons.
        #region Buttons visibility

        public static readonly DependencyProperty MinimizeVisibleProperty =
                DependencyProperty.Register("MinimizeVisible", typeof(bool), typeof(DialogWindow), new UIPropertyMetadata(true));
        public static readonly DependencyProperty MaximizeVisibleProperty =
                DependencyProperty.Register("MaximizeVisible", typeof(bool), typeof(DialogWindow), new UIPropertyMetadata(true));
        public static readonly DependencyProperty CloseVisibleProperty =
                DependencyProperty.Register("CloseVisible", typeof(bool), typeof(DialogWindow), new UIPropertyMetadata(true));

        public bool MinimizeVisible
        {
            get { return (bool)GetValue(MinimizeVisibleProperty); }
            set { SetValue(MinimizeVisibleProperty, value); }
        }

        public bool MaximizeVisible
        {
            get { return (bool)GetValue(MaximizeVisibleProperty); }
            set { SetValue(MaximizeVisibleProperty, value); }
        }

        public bool CloseVisible
        {
            get { return (bool)GetValue(CloseVisibleProperty); }
            set { SetValue(CloseVisibleProperty, value); }
        }

        public static readonly DependencyProperty HeaderHeightProperty =
            DependencyProperty.Register("HeaderHeight", typeof(double), typeof(DialogWindow), new PropertyMetadata(22.0));

        #endregion

        public double HeaderHeight
        {
            get { return (double)GetValue(HeaderHeightProperty); }
            set { SetValue(HeaderHeightProperty, value); }
        }

        private void OnRestoreCommand(object sender_, ExecutedRoutedEventArgs e_)
        {
            SystemCommands.RestoreWindow(this);
        }

        private void OnMinimizeCommand(object sender_, ExecutedRoutedEventArgs e_)
        {
            SystemCommands.MinimizeWindow(this);
        }

        private void OnMaximizeCommand(object sender_, ExecutedRoutedEventArgs e_)
        {
            SystemCommands.MaximizeWindow(this);
        }

        private void OnCloseCommand(object sender_, ExecutedRoutedEventArgs e_)
        {
            SystemCommands.CloseWindow(this);
        }

        private readonly IList<object> m_headerItems = new HeaderItemsCollection();

        public double? FloatingWindowMaximumWidth
        {
            get { return !double.IsInfinity(this.MaxWidth) && !double.IsNaN(MaxWidth) ? this.MaxWidth : default(double?); }
            set
            {
                if (value != null)
                {
                    this.MaxWidth = value.Value;
                }
            }
        }

        public double? FloatingWindowMaximumHeight
        {
            get { return !double.IsInfinity(this.MaxHeight) && !double.IsNaN(MaxHeight) ? this.MaxHeight : default(double?); }
            set
            {
                if (value != null)
                {
                    this.MaxHeight = value.Value;
                }
            }
        }
        public double FloatingWindowMinimumWidth
        {
            get { return this.MinWidth; }
            set
            {
                this.MinWidth = value; 
            }
        }

        public double FloatingWindowMinimumHeight
        {
            get { return this.MinHeight; }
            set
            {
                this.MinHeight = value;
            }
        }
        public IList<object> HeaderItems
        {
            get
            {
                return m_headerItems;
            }
        }

        public new event EventHandler<VisibilityEventArgs> IsVisibleChanged;
        public event EventHandler<ContentChangedEventArgs> ContentChanged;

        #region IWindowViewContainer implementation

        public bool IsHidden { get; set; }

        public new Icon Icon
        {
            get
            {
                return m_icon;
            }
            set
            {
                if (value != m_icon)
                {
                    m_icon = value;
                    (this as Window).Icon = value.ToImageSource();
                }
            }
        }

        public string ID { get; set; }

        // TODO honor this property
        public Color AccentColour { get; set; }

        public System.Windows.Media.Brush TabColour
        {
            get
            {
                return System.Windows.Media.Brushes.White;
            }
            set
            {
            }
        }

        public Brush SelectedTabColour
        {
            get
            {
                return null;
            }
            set
            {
            }
        }

        public bool Transient { get; set; }

        public bool OwnedByContainer { get; set; }
        public bool CanMaximize { get; set; }
        public bool UnpinOnHeaderDoubleClick { get; set; }
        public bool StayPinnedWhenRevealed { get; set; }

        public new void Activate()
        {
            (this as Window).Activate();
        }

        // no-op; does not make sense in case of modal dialog
        public void Flash()
        {
        }

        // no-op; does not make sense in case of modal dialog
        public void Flash(Color color)
        {
        }

        public new event EventHandler<CancelEventArgs> Closing;

        public void InvokeClosing(CancelEventArgs e)
        {
            EventHandler<CancelEventArgs> handler = Closing;
            if (handler != null) handler(this, e);
        }

        public new event EventHandler<WindowEventArgs> Closed;

        public void InvokeClosed(WindowEventArgs e)
        {
            EventHandler<WindowEventArgs> handler = Closed;
            if (handler != null) handler(this, e);
        }

        public void InvokeCreated(WindowEventArgs e)
        {
            var handler = Created;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        // never invoked; does not make sense in case of modal dialog
        public event EventHandler<WindowEventArgs> ClosedQuietly;

        public event EventHandler<WindowEventArgs> Created;
        public event EventHandler TopmostChanged;

        // no-op; does not make sense in case of modal dialog
        public PrintWindowHandler PrintHandler { get; set; }

        // no-op; does not make sense in case of modal dialog
        public PreviewPrintWindowHandler PreviewPrintHandler { get; set; }

        // TODO honor this property
        public object Tooltip { get; set; }

        public string FactoryID { get; internal set; }

        public InitialWindowParameters Parameters { get; internal set; }

        InitialViewParameters IViewContainerBase.Parameters
        {
            get
            {
                return Parameters;
            }
        }

        #endregion

        private PropertyInfo windowSizePI = typeof(Window).GetProperty("WindowSize", BindingFlags.NonPublic | BindingFlags.Instance);

        public Size WindowSize
        {
            get
            {
                if (windowSizePI == null)
                {
                    return new Size(100.0, 100.0);
                }
                try
                {
                    return (Size)windowSizePI.GetValue(this, null);
                }
                catch (Exception)
                {

                    return new Size(100.0, 100.0);
                }

            }
        }


        public IWidgetViewContainer Header
        {
            get { return null; }
        }

        [Obsolete("Custom header structures are not supported on dialogs.", true)]
        public HeaderStructure HeaderStructure
        {
            get
            {
                return HeaderStructure.WindowsClassic;
            }
            set
            {

            }
        }

        public IContextMenuExtrasHolder ContextMenuExtras
        {
            get
            {
                return null;
            }
        }

        public XDocument ViewState { get; set; }

        public event EventHandler<WindowDockStateChangedEventArgs> DockStateChanged; 
    }
}
