﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Controls/WindowManagerActiveChangedEventArgs.cs#6 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
  internal class WindowManagerActiveChangedEventArgs : EventArgs
  {
#pragma warning disable 612,618
    public IViewContainer OldValue
#pragma warning restore 612,618
    {
      get; private set;
    }
#pragma warning disable 612,618
    public IViewContainer NewValue
#pragma warning restore 612,618
    {
      get;
      private set;
    }
#pragma warning disable 612,618
    public WindowManagerActiveChangedEventArgs(IViewContainer old_, IViewContainer new_)
#pragma warning restore 612,618
    {
      OldValue = old_;
      NewValue = new_;
    }
  }
}
