﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
	/// <summary>
	/// Interaction logic for WorkspaceChildWindow.xaml
	/// </summary>
	public partial class WorkspaceChildWindow : UserControl
	{
		public WorkspaceChildWindow()
		{
			InitializeComponent();
		}

		public void SetContent(UIElement c)
		{
			this.RootGrid.Children.Add(c);
		}
	}
}
