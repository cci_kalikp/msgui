﻿////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Controls/SaveAsLayoutWindow.xaml.cs#8 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.Timers;
using System.Windows;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
  /// <summary>
  /// Interaction logic for SaveAsLayoutWindow.xaml
  /// </summary>
  public partial class SaveAsLayoutWindow 
  {
    public SaveAsLayoutWindow()
    {
      AutomaticSeconds = -1;
      InitializeComponent();
    }

    public string SelectedText
    {
      get; 
      set;
    }

    public int AutomaticSeconds { get; set; }

    public void OnOk(object sender_, RoutedEventArgs e)
    {
      SelectedText = textBox.Text;
      DialogResult = true;      
    }

    public void OnCancel(object sender_, RoutedEventArgs e)
    {
      DialogResult = false;
    }

    private void Window_Loaded(object sender, RoutedEventArgs e)
    {
      //if (AutomaticSeconds != -1)
      //{
      //  Timer timer = new Timer
      //  {
      //    AutoReset = false,
      //    Enabled = true,
      //    Interval = AutomaticSeconds * 1000
      //  };
      //  timer.Elapsed += delegate
      //                     {
      //                       SelectedText = "Automatic save - " + DateTime.Now.ToString("yyyyMMddHHmmss");
      //                       DialogResult = true;
      //                       Close();
      //                     };
      //  timer.Start();
      //}
    }
  }
}
