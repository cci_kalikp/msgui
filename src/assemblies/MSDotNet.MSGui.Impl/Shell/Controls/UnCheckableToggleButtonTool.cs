﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using Infragistics.Windows;
using Infragistics.Windows.Ribbon;
using Infragistics.Windows.Ribbon.Internal;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
    public class UnCheckableToggleButtonTool : ToggleButtonTool, IRibbonTool 
    {
        //prevent the button to be checked manually when hosted in menutool
        RibbonToolProxy IRibbonTool.ToolProxy
        {
            get { return UnCheckableToggleButtonToolProxy.Instance; }
        }

        //prevent the button to be checked manually when used normally
        protected override void OnToggle()
        {
            //base.OnToggle();
        }


         protected class UnCheckableToggleButtonToolProxy : ToggleButtonToolProxy 
         {
             static UnCheckableToggleButtonToolProxy()
             {
                 Instance = new UnCheckableToggleButtonToolProxy();
             }
             protected override void PrepareToolMenuItem(ToolMenuItem toolMenuItem, ToggleButtonTool tool)
             {
                 base.PrepareToolMenuItem(toolMenuItem, tool);
                 toolMenuItem.IsCheckable = false;
             }

             internal static readonly UnCheckableToggleButtonTool.UnCheckableToggleButtonToolProxy Instance;

         }
    }

   


}
