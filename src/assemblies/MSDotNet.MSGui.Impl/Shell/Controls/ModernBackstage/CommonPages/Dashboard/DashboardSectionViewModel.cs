﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.ModernBackstage.CommonPages.Dashboard
{
    class DashboardSectionViewModel : BackstageSectionViewModel
    {
        private Dashboard view;
        public DashboardSectionViewModel(IUnityContainer container, Application.Application app)
        {
            this.Application = app; 
            this.Text = "About"; 

            this.Content = view = new Dashboard { DataContext = this };
            container.RegisterInstance("DashboardItemsControl", view.DashboardItems);
            if (container.IsRegistered<IMSDesktopEnvironment>())
            {
                this.Environment = container.Resolve<IMSDesktopEnvironment>();
            }
        }

        public Application.Application Application { get; private set; }
        public IMSDesktopEnvironment Environment { get; set; }

        public string AboutText
        {
            get
            {
                return Environment != null
                    ? string.Format("You are running version {0} as {1} in {2}{3}", Application.Version, Environment.User, Environment.Environment, 
                    !string.IsNullOrEmpty(Environment.Subenvironment) ? ("/" + Environment.Subenvironment) : string.Empty)
                    : string.Format("You are running version {0}", Application.Version);
            }
        }
        
    }
     

     
}
