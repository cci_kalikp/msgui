﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.ModernBackstage.CommonPages.Dashboard
{
    /// <summary>
    /// Interaction logic for SupportInformationView.xaml
    /// </summary>
    public partial class SupportInformationView : UserControl
    {
        public SupportInformationView()
        {
            InitializeComponent();
        }
    }
}
