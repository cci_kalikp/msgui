﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.ModernBackstage
{
    public class LayoutSectionViewModel:BackstageSectionViewModel
    {
        private IProfileMonitor profileService;
        private IShell shell;
        private readonly LayoutScreenshotProvider screenshotProvider;

        internal LayoutSectionViewModel(IProfileMonitor profileService_, IShell shellWindow_, LayoutScreenshotProvider screenshotProvider)
        {
            this.Text = "Layouts";
            this.profileService = profileService_;
            this.shell = shellWindow_;
            this.screenshotProvider = screenshotProvider;
            LoadLayouts();
            profileService_.ProfileDeleted +=profileService__ProfileDeleted;
            profileService_.ProfileSaved += ProfileServiceOnProfileSaved;
            profileService_.ProfileRenamed += profileService__ProfileRenamed;
            profileService_.CurrentProfileChanged +=profileService__CurrentProfileChanged;
        }

        private void LoadLayouts()
        {
            this.Items.Clear();

//            this.Items.Add(new LayoutItemViewModel(string.Empty, profileService, shell));
            if (!string.IsNullOrEmpty(profileService.CurrentProfile))
            {
                this.Items.Add(new LayoutItemViewModel(profileService.CurrentProfile, profileService, shell, screenshotProvider));        
            }
            foreach (var profile in profileService.CurrentAvailableProfiles)
            {
                 if (!profile.Equals(profileService.CurrentProfile, StringComparison.InvariantCultureIgnoreCase))
                 {
                     this.Items.Add(new LayoutItemViewModel(profile, profileService, shell, screenshotProvider));
                 }
            }

            this.SelectedItem = this.Items.FirstOrDefault(i => i.IsSelected);
        }

        void profileService__ProfileRenamed(object sender, ProfileRenamedEventArgs e)
        {
            LoadLayouts();
        }

        void profileService__CurrentProfileChanged(object sender, CurrentProfileChangedEventArgs e)
        {
            LoadLayouts();

        }

        private void ProfileServiceOnProfileSaved(object sender_, ProfileSavedEventArgs profileSavedEventArgs_)
        {
            LoadLayouts();
        }

        void profileService__ProfileDeleted(object sender, ProfileDeletedEventArgs e)
        {
            LoadLayouts();

        } 
         
    }
}
