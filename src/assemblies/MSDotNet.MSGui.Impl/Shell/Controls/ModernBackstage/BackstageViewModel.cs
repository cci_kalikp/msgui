﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Threading;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Options; 

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.ModernBackstage
{
    public sealed class BackstageViewModel : ViewModelBase
    {
        private readonly ApplicationOptions applicationOptions;

        public BackstageViewModel(IApplicationOptions applicationOptions)
        {
            this.applicationOptions = applicationOptions as ApplicationOptions;

            this.Sections = new ObservableCollection<BackstageSectionViewModel>(); 
            this.optionPageContainer = new OptionPageContainer();
        } 

        private bool isVisible;

        public bool IsVisible
        {
            get { return isVisible; }
            set
            {
                if (isVisible != value && options != null)
                {
                    options.PropertyChanged -= options_PropertyChanged;
                    options = null;
                    OnPropertyChanged("Options");
                }
                isVisible = value;
            }
        }
        private BackstageOptionsViewModel options;
        public BackstageOptionsViewModel Options
        {
            get
            {
                if (options == null)
                {
                    options = new BackstageOptionsViewModel(applicationOptions);
                    options.PropertyChanged += options_PropertyChanged;
                }

                return options;
            }
        }

        void options_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "SelectedTopLevelItem" && options != null &&
                options.SelectedTopLevelItem != null)
            {
                CurrentSection = null;
            }

            if (e.PropertyName == "CurrentOptionPage")
            {
                this.optionPageContainer.OptionPage = this.options.CurrentOptionPage;
                if (this.options.CurrentOptionPage != null)
                {
                    this.CurrentBackstageItem = this.optionPageContainer;

                }
            }
        }

        public ObservableCollection<BackstageSectionViewModel> Sections { get; private set; }

        private BackstageSectionViewModel currentSection;
        public BackstageSectionViewModel CurrentSection
        {
            get { return currentSection; }
            set
            {
                if (value != null && options != null)
                {
                    options.SelectedTopLevelItem = null;
                }
                
                if (currentSection != null)
                    currentSection.PropertyChanged -= CurrentSection_PropertyChanged;

                currentSection = value;
                OnPropertyChanged("CurrentSection");

                if (currentSection != null)
                {
                    if (currentSection.Content != null)
                        this.CurrentBackstageItem = currentSection.Content;
                    else
                    {
                        this.CurrentBackstageItem = currentSection.SelectedItem != null
                            ? currentSection.SelectedItem.Content
                            : null;
                        currentSection.PropertyChanged += CurrentSection_PropertyChanged;
                    }
                }
            }
        }

        private void CurrentSection_PropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            var section = sender as BackstageSectionViewModel;
            if (section != null)
            {
                this.CurrentBackstageItem = (section.SelectedItem != null) ? section.SelectedItem.Content : null;
            }
        }

        private object currentBackstageItem;
        private OptionPageContainer optionPageContainer;

        public object CurrentBackstageItem
        {
            get { return currentBackstageItem; }
            set
            {
                currentBackstageItem = value;
                Dispatcher.CurrentDispatcher.BeginInvoke((Action) (() => OnPropertyChanged("CurrentBackstageItem")), DispatcherPriority.Background);
            }
        }
    }
}
