﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MorganStanley.MSDotNet.MSGui.Impl.Options;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.ModernBackstage
{
    /// <summary>
    /// Interaction logic for OptionPageContainer.xaml
    /// </summary>
    public partial class OptionPageContainer : UserControl
    {
        public OptionPageContainer()
        {
            InitializeComponent();
        }
          
        public OptionPage OptionPage
        {
            get { return (OptionPage)GetValue(OptionPageProperty); }
            set { SetValue(OptionPageProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty OptionPageProperty =
            DependencyProperty.Register("OptionPage", typeof(OptionPage), typeof(OptionPageContainer), new PropertyMetadata(null, PropertyChangedCallback));

        private static void PropertyChangedCallback(DependencyObject dependencyObject_, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs_)
        {
            OptionPageContainer container = (OptionPageContainer)dependencyObject_;
            OptionPage oldPage = dependencyPropertyChangedEventArgs_.OldValue as OptionPage;
            if (oldPage != null) oldPage.Dispose();
            container.btnOK.Command = null;
            container.btnHelp.Command = null;
            OptionPage newPage = dependencyPropertyChangedEventArgs_.NewValue as OptionPage;
            if (newPage == null)
            {
                container.Visibility = Visibility.Collapsed;
            }
            else
            {
                container.Visibility = Visibility.Visible;
                container.OptionDisplayer.Children.Clear();
                var currentContent = newPage.RenderPage();
                FrameworkElement currentElement = currentContent as FrameworkElement;
                if (currentElement != null)
                {
                    Panel p = currentElement.Parent as Panel;
                    if (p != null && p != container.OptionDisplayer)
                    {
                        p.Children.Remove(currentElement);
                    }
                }
                container.OptionDisplayer.Children.Add(currentContent);
                container.btnOK.Command = newPage.SaveCommand;
                container.btnHelp.Command = newPage.HelpCommand;
            }  
        }
    }
}
