﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.ModernBackstage
{
    public class LayoutItemViewModel : BackstageItemViewModel
    {
        private readonly BackstageItemCommand saveCommand;
        private readonly BackstageItemCommand deleteCommand;
        private readonly BackstageItemCommand loadCommand;

        public bool IsNew { get; set; }
        public bool IsCurrent { get; set; }

        public ImageSource Thumbnail
        {
            get { return thumbnail; }
            set { thumbnail = value; OnPropertyChanged("Thumbnail"); }
        }

        private string profileName;
        private readonly LayoutScreenshotProvider screenshotProvider;
        private ImageSource thumbnail;

        internal LayoutItemViewModel(string profileName_, IProfileMonitor profileService_, IShell shellWindow_, LayoutScreenshotProvider screenshotProvider)
        {
            profileName = profileName_;
            this.screenshotProvider = screenshotProvider;

            if (string.IsNullOrEmpty(profileName_))
            {
                this.Text = "Create New";
                //this.Content = new Button{Content = "OHAI"};
                this.IsNew = true;
            }
            else
            {
                this.Text = profileName_;
                IsCurrent = profileName_.Equals(profileService_.CurrentProfile, StringComparison.InvariantCultureIgnoreCase);
                this.Thumbnail = screenshotProvider.GetLayoutThumbnail(profileName_);
            }

            saveCommand = new BackstageItemCommand(o_ => Dispatcher.CurrentDispatcher.BeginInvoke((Action)(() =>
            {
                var thumb = this.screenshotProvider.CaptureCurrentLayout();
                this.screenshotProvider.UpdateLayoutThumbnail(profileService_.CurrentProfile, thumb);
                this.Thumbnail = this.screenshotProvider.GetLayoutThumbnail(profileService_.CurrentProfile);

                if (IsNew)
                {
                    shellWindow_.SaveProfileAs();
                }
                else
                {
                    shellWindow_.SaveCurrentProfile();
                }
                this.Text = profileService_.CurrentProfile;
            }), DispatcherPriority.Background), null, true);

            loadCommand = new BackstageItemCommand(o_ => shellWindow_.LoadProfile(profileName));

            if (IsNew || IsCurrent)
            {
                this.Commands.Add(saveCommand);
            }
            else
            {
                this.Commands.Add(loadCommand);
            }
            //deleteCommand = new BackstageItemCommand(o_ => shellWindow_.DeleteProfile(profileName));
            //todo re-add layout delete support to backstage

            if (!IsNew)
            {
                this.Commands.Add(deleteCommand);
            }


            var page = new CommonPages.Layout.LayoutPage
            {
                DataContext = this
            };
            this.Content = page;
        }

        public BackstageItemCommand SaveCommand { get { return this.saveCommand; } }
        public BackstageItemCommand LoadCommand { get { return this.loadCommand; } }
        public BackstageItemCommand DeleteCommand { get { return this.deleteCommand; } }

    }
}
