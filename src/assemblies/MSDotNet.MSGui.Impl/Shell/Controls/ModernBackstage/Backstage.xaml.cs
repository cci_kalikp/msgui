﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Options;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.ModernBackstage
{
    /// <summary>
    /// Interaction logic for Backstage.xaml
    /// </summary>
    public partial class Backstage : UserControl
    {
        public Backstage()
        {
            InitializeComponent();
        }

        public ItemsControl ApplicationMenu
        {
            get { return (ItemsControl)GetValue(ApplicationMenuProperty); }
            set { SetValue(ApplicationMenuProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ApplicationMenu.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ApplicationMenuProperty =
            DependencyProperty.Register("ApplicationMenu", typeof(ItemsControl), typeof(Backstage), new PropertyMetadata(null));


 
    }

    public class SettingsPageMock
    {
        public ListMock SideNavigationItems { get; set; }
    }

    public class ListMock : List<String>
    {
        public ListMock()
        {
            this.Add("Lorem");
            this.Add("ipsum");
            this.Add("dolor");
            this.Add("sit");
            this.Add("amet");
        }
    }
}
