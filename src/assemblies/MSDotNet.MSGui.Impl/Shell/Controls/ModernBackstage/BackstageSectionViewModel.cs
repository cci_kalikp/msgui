﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.ModernBackstage
{
    public class BackstageSectionViewModel : ViewModelBase
    {
        private bool isSelected = false;
        private BackstageItemViewModel selectedItem;

        public BackstageSectionViewModel()
        {
            Items = new ObservableCollection<BackstageItemViewModel>();
        }

        public string Text { get; set; }

        public ObservableCollection<BackstageItemViewModel> Items { get; private set; }

        public BackstageItemViewModel SelectedItem
        {
            get { return selectedItem; }
            set
            {
                selectedItem = value; 
                OnPropertyChanged("SelectedItem");
            }
        }

        public object Content { get; protected set; }
        
        public bool IsSelected
        {
            get { return isSelected; }
            set
            {
                isSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }
    }
}
