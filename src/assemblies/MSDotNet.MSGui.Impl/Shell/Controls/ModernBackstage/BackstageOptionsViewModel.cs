﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Options;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.ModernBackstage
{
    public sealed class BackstageOptionsViewModel : OptionPageNavigationViewModel
    {
        private readonly ApplicationOptions applicationOptions;
        public BackstageOptionsViewModel(IApplicationOptions applicationOptions_) :
            base(((ApplicationOptions)applicationOptions_).Pages)
        {
            this.applicationOptions = (ApplicationOptions)applicationOptions_;
            if (this.SearchBox.EnableTypeaheadDropDown)
            {
                this.SearchBox.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(SearchBox_PropertyChanged);
            }
        }
          

        private SideNavigationItem selectedTopLevelItem = null;

        public SideNavigationItem SelectedTopLevelItem
        {
            get { return selectedTopLevelItem; }
            set
            {
                selectedTopLevelItem = value; 
                this.OnPropertyChanged("SelectedTopLevelItem");
                this.OnPropertyChanged("SideNavigationSubItems");
                this.OnPropertyChanged("SelectedSecondLevelItem");
            }
        }

        private SideNavigationItem selectedSecondLevelItem = null;

        public SideNavigationItem SelectedSecondLevelItem
        {
            get
            {
                if (selectedSecondLevelItem != null)
                {
                    CurrentOptionPage = selectedSecondLevelItem.Page;
                }
                else
                {
                    CurrentOptionPage = null;
                }
                return selectedSecondLevelItem;
            }
            set
            {
                selectedSecondLevelItem = value; 
                if (selectedSecondLevelItem == null)
                {
                    CurrentOptionPage = null;
                }
                this.OnPropertyChanged("SelectedSecondLevelItem");
            }
        }


        public IList<SideNavigationItem> SideNavigationSubItems
        {
            get
            {
                var selectedItem = this.SideNavigationItems.FirstOrDefault(i => i.IsSelected);
                return (selectedItem != null) ? selectedItem.Items : new List<SideNavigationItem>();
            }
        }

        void SearchBox_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "SelectedItem" && this.SearchBox.SelectedItem != null)
            {
                NavigateTo(this.SearchBox.SelectedItem as SearchResultItem);
            }
        }

         
        private OptionPage currentOptionPage = null;

        public OptionPage CurrentOptionPage
        {
            get { return this.currentOptionPage; }
            set
            {
                if (value != null)
                {
                    applicationOptions.EnsureStyle();
                }
                this.currentOptionPage = value;
                this.OnPropertyChanged("CurrentOptionPage");
            }
        } 
 
        public void NavigateTo(OptionPage page_)
        {
            foreach (var sideNavigationItem in this.SideNavigationItems)
            {
                foreach (var navigationItem in sideNavigationItem)
                {
                    if (navigationItem.PathInHierarchy == page_.FullPath && navigationItem.ShowNode)
                    {
                        SelectedTopLevelItem = sideNavigationItem;
                        SelectedSecondLevelItem = navigationItem;
                        //this.SideNavigationSelectedItem = navigationItem; 
                        CurrentOptionPage = page_;
                        return;
                    }
                }
            }
        }

        internal void NavigateTo(SearchResultItem resultItem_)
        {
            NavigateTo(resultItem_.Page);
            if (resultItem_.Item != null)
            {
                resultItem_.Page.LocateItem(resultItem_.Item);
            }
        } 
    }
}
