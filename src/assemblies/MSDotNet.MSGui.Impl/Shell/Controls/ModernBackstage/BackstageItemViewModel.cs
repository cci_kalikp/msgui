﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Windows.Media;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.ModernBackstage
{
    public class BackstageItemViewModel : ViewModelBase
    {
        public BackstageItemViewModel()
        {
            this.Commands = new ObservableCollection<BackstageItemCommand>();
        }

        public ImageSource Icon { get; set; }

        private bool isSelected = false;
        public bool IsSelected
        {
            get { return isSelected; }
            set
            {
                isSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }

        private string text;
        private object content;

        public string Text
        {
            get { return text; }
            set
            {
                text = value;
                OnPropertyChanged("Text");
            }
        }

        public object Content
        {
            get { return content; }
            set
            {
                content = value;
                OnPropertyChanged("Content");
            }
        }

        public ObservableCollection<BackstageItemCommand> Commands { get; private set; }

    }

    public class BackstageItemCommand : ViewModelBase, ICommand
    {
        internal static Action CloseBackstageAction = null;
        private readonly bool closeBackstage = false;

        private readonly Predicate<object> canExecute;
        private readonly Action<object> execute;
        event EventHandler ICommand.CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public BackstageItemCommand(Action<object> execute_, Predicate<object> canExecute_, bool closeBackstage)
        {
            this.execute = execute_;
            this.canExecute = canExecute_;
            this.closeBackstage = closeBackstage;
        }
        public BackstageItemCommand(Action<object> execute_, Predicate<object> canExecute_)
            : this(execute_, canExecute_, false)
        {
        }

        public BackstageItemCommand(Action<object> execute_)
            : this(execute_, null)
        {
        }

        bool ICommand.CanExecute(object parameter_)
        {
            return this.canExecute == null || this.canExecute(target);
        }

        void ICommand.Execute(object parameter_)
        {
            if (this.closeBackstage && CloseBackstageAction != null)
            {
                CloseBackstageAction();
            }

            this.execute(target);
        }

        public ImageSource Icon { get; set; }

        public string Text { get; set; }

        private BackstageItemViewModel target;
        public BackstageItemViewModel Target
        {
            get { return target; }
            set
            {
                target = value;
                OnPropertyChanged("Target");
            }
        }

    }
}