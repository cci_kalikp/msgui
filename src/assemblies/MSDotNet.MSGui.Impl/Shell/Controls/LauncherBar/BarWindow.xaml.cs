﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Controls/LauncherBar/BarWindow.xaml.cs#42 $
// $Change: 902903 $
// $DateTime: 2014/10/29 02:23:15 $
// $Author: caijin $

using System;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Drawing;
using System.Windows;
using System.Windows.Automation.Peers;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Threading; 
using Microsoft.Practices.Unity;
using Microsoft.WindowsAPICodePack.Shell.PropertySystem;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.Win32;
using MorganStanley.MSDotNet.MSGui.Impl.DockManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Helpers;
using MorganStanley.MSDotNet.MSGui.Impl.ModernTheme;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.LauncherBar;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.MicrosoftShell;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.ModernBackstage;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.ModernBackstage.CommonPages.Dashboard; 
using MorganStanley.MSDotNet.MSGui.Impl.TestSupport;
using Point = System.Windows.Point;
using Size = System.Windows.Size;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager; 
using System.Windows.Data;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class BarWindow : Window, IExpandableBar
    {
        private bool m_isTaskbar;

        #region BarState property and event
        public event EventHandler BarStateChanged;
        public static readonly DependencyProperty BarStateProperty =
          DependencyProperty.Register(
            "BarState", typeof(LauncherBarState), typeof(BarWindow), new PropertyMetadata(LauncherBarState.Shrinked, BarStateChangedCallback));

        private static void BarStateChangedCallback(DependencyObject d_, DependencyPropertyChangedEventArgs e_)
        {
            BarWindow window = (BarWindow)d_;

            var copy = window.BarStateChanged;
            if (copy != null)
            {
                copy(window, EventArgs.Empty);
            }
        }

        public LauncherBarState BarState
        {
            get { return (LauncherBarState)GetValue(BarStateProperty); }
            set { SetValue(BarStateProperty, value); }
        }
        #endregion



        public Visibility StateToggleButtonVisibility
        {
            get
            {
                return CanToggleState ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        // Using a DependencyProperty as the backing store for UserNamePresentOnLauncherBarProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty UserNamePresentOnLauncherBarProperty =
            DependencyProperty.Register("UserNamePresentOnLauncherBarProperty", typeof(bool), typeof(BarWindow), new PropertyMetadata(UserNameOnLauncherBar));

        public bool UserNamePresentOnLauncherBar
        {
            get { return (bool)GetValue(UserNamePresentOnLauncherBarProperty); }
            set { SetValue(UserNamePresentOnLauncherBarProperty, value); }
        }

        public Visibility MinimizeButtonVisibility
        {
            get
            {
                return CanMinimize ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public static readonly DependencyProperty VersionProperty =
          DependencyProperty.Register(
          "Version",
           typeof(string),
           typeof(BarWindow), new PropertyMetadata(""));

        public string Version
        {
            get
            {
                return (string)GetValue(VersionProperty);
            }
            set
            {
                SetValue(VersionProperty, value);
            }
        }

        public static readonly DependencyProperty ProfileProperty =
          DependencyProperty.Register(
          "Profile",
           typeof(string),
           typeof(BarWindow), new PropertyMetadata(""));

        public string Profile
        {
            get
            {
                return (string)GetValue(ProfileProperty);
            }
            set
            {
                SetValue(ProfileProperty, value);
            }
        }



        public string ConcordProfile
        {
            get { return (string)GetValue(ConcordProfileProperty); }
            set { SetValue(ConcordProfileProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ConcordProfile.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ConcordProfileProperty =
                DependencyProperty.Register("ConcordProfile", typeof(string), typeof(BarWindow), new UIPropertyMetadata(""));



        public string UserName
        {
            get { return (string)GetValue(UserNameProperty); }
            set { SetValue(UserNameProperty, value); }
        }

        // Using a DependencyProperty as the backing store for UserName.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty UserNameProperty =
                DependencyProperty.Register("UserName", typeof(string), typeof(BarWindow), new UIPropertyMetadata(""));



        #region Taskbar property and event
        public event EventHandler TaskbarStateChanged;

        public static readonly DependencyProperty IsTaskbarProperty =
          DependencyProperty.Register(
            "IsTaskbar", typeof(bool), typeof(BarWindow),
            new PropertyMetadata(false, IsTaskbarStateChangedCallback));

        private static void IsTaskbarStateChangedCallback(DependencyObject d_, DependencyPropertyChangedEventArgs e_)
        {
            BarWindow window = (BarWindow)d_;
            window.m_isTaskbar = (bool)e_.NewValue;

            var copy = window.TaskbarStateChanged;
            if (copy != null)
            {
                copy(window, EventArgs.Empty);
            }
        }

        public bool IsTaskbar
        {
            get
            {
                return (bool)GetValue(IsTaskbarProperty);
            }
            set
            {
                SetValue(IsTaskbarProperty, value);
            }
        }
        #endregion

        //not always the same as "Topmost" of a window (see TaskBar mode)
        public static readonly DependencyProperty IsTopmostProperty =
          DependencyProperty.Register("IsTopmost", typeof(bool), typeof(BarWindow),
          new PropertyMetadata(false));

        public bool IsTopmost
        {
            get
            {
                return (bool)GetValue(IsTopmostProperty);
            }
            set
            {
                SetValue(IsTopmostProperty, value);
            }
        }

        private IUnityContainer m_container;
        private IProfileMonitor _profileMonitor;
        internal static bool CanToggleState = true;
        internal static bool CanMinimize;
        internal static bool UserNameOnLauncherBar = true;
        public static bool ShowLayoutNameOnLauncherBar = true;
        public static bool CanResizeHorizontally = false; 
        internal static bool AutoScrollHorizontally = true;
        private WindowState oldState;
        public BarWindow(bool enableSoftwareRendering, IUnityContainer container_)
        {
            m_container = container_; 
 
            //this._profileMonitor = m_container.Resolve<PersistenceProfileService>();
            //this.screenshotProvider = m_container.Resolve<LayoutScreenshotProvider>(); 

            if (enableSoftwareRendering)
            {
                var hwndSource = PresentationSource.FromVisual(this) as HwndSource;
                if (hwndSource != null)
                {
                    hwndSource.CompositionTarget.RenderMode = RenderMode.SoftwareOnly;
                }

            }

            InitializeComponent(); 
            TitleBar = new TitleBarPlaceHolder() { DataContext = this };
            //comment out this to leave dragging only to the right side (change CaptionHeight in the style of the window accordingly)

            UserNamePresentOnLauncherBar = UserNameOnLauncherBar;
 
            HandleWindowDragMove();

            if (ShellModeExtension.InvisibleMainControllerMode.HasFlag(ShellModeExtension.InvisibleMainControllerModeEnum.HideWindow))
            {
                this.Visibility = Visibility.Hidden;
            }
            if (ShellModeExtension.InvisibleMainControllerMode.HasFlag(ShellModeExtension.InvisibleMainControllerModeEnum.MoveWindow))
            {
                this.Top = -5000;
                this.Left = -5000;
            }
            if (CanResizeHorizontally)
            {
                this.ResizeMode = ResizeMode.CanResize;
            }
            else
            {
                this.ResizeMode = ResizeMode.NoResize;
            }
            Loaded += BarWindow_Loaded;


        }

        private Point initialPosistion = new Point(-1000, -1000);

        private void HandleWindowDragMove()
        {
            MouseLeftButtonDown +=
                 (sender_, args_) =>
                    {
                        initialPosistion = WPFHelper.GetMousePosition();
                        if (!IsTaskbar)
                        {
                            this.CaptureMouse();
                            oldState = this.WindowState; 
                        }
                    };
            MouseMove += (sender_, args_) =>
                {
                    if (args_.LeftButton != MouseButtonState.Pressed ||
                        !IsMouseCaptured || initialPosistion.X < -500) return; 
                    Point currentPosition = WPFHelper.GetMousePosition();
                    Vector diff = new Vector(currentPosition.X - initialPosistion.X, currentPosition.Y - initialPosistion.Y);
                    if ((Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance ||
                         Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance))
                    {
                        var rc = new Win32.RECT();
                        var hwnd = new WindowInteropHelper(this).Handle;
                        Win32.GetWindowRect(hwnd, ref rc);
                        if (this.WindowState == WindowState.Maximized && oldState == WindowState)
                        {
                            this.WindowState = WindowState.Normal;
                        }
                         
                        this.Left = rc.Left + diff.X;
                        this.Top = rc.Top + diff.Y; 
                        oldState = this.WindowState;
                        initialPosistion = currentPosition;
                    } 

                };
            MouseLeftButtonUp += (sender_, args_) =>
                {
                    if (IsMouseCaptured)
                        ReleaseMouseCapture(); 
                    if (initialPosistion.X < -500) return;
                    var actualscreen = System.Windows.Forms.Screen.FromRectangle(
                        new Rectangle(
                            Convert.ToInt32(Left),
                            Convert.ToInt32(Top),
                            Convert.ToInt32(ActualWidth),
                            Convert.ToInt32(ActualHeight)));
                    if (!IsTaskbar)
                    {
                        Point currentPosition = WPFHelper.GetMousePosition();

                        if (currentPosition.Y > actualscreen.WorkingArea.Bottom - Convert.ToInt32(ActualHeight) + 5)
                        {
                            SetDock(true);
                        }
                    }
                    initialPosistion = new Point(-1000, -1000);
                };
            MouseLeave += delegate
            {
                if (IsTaskbar && initialPosistion.X > -500)
                {
                    Point currentPosition = WPFHelper.GetMousePosition();
                    var actualscreen = System.Windows.Forms.Screen.FromRectangle(
                             new Rectangle(
                               Convert.ToInt32(Left),
                               Convert.ToInt32(Top),
                               Convert.ToInt32(ActualWidth),
                               Convert.ToInt32(ActualHeight)));
                    if (currentPosition.Y < actualscreen.WorkingArea.Bottom - Convert.ToInt32(ActualHeight) - 5)
                    {
                        SetDock(false);
                    }
                } 
            };
        }


        void BarWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this._profileMonitor = m_container.Resolve<PersistenceProfileService>();
            this.screenshotProvider = m_container.Resolve<LayoutScreenshotProvider>(); 


            if (ShellModeExtension.NoTaskbarPin)
            {
                NoTaskbarPin();
            }
            if (FrameworkExtensions.DefaultStickWindowOptions != StickyWindowOptions.None)
            {
                new WPFStickyWindow(this)
                {
                    StickToScreen = FrameworkExtensions.DefaultStickWindowOptions.HasFlag(StickyWindowOptions.StickToScreen),
                    StickToOther = FrameworkExtensions.DefaultStickWindowOptions.HasFlag(StickyWindowOptions.StickToOther)
                };
            }
            
            if (ModernThemeHelper.IsModernThemeEnabled)
            {
                SetModernTheme();
            } 
        }
         
        private void SetModernTheme()
        {
            this.backstageViewModel = m_container.Resolve<BackstageViewModel>();

            var backstageDashboardSectionViewModel = new DashboardSectionViewModel(m_container, m_container.Resolve<IApplication>() as Application.Application);
            this.backstageViewModel.Sections.Add(backstageDashboardSectionViewModel);

            var backstageLayoutsSection = new LayoutSectionViewModel(this._profileMonitor,
                m_container.Resolve<IShell>(), screenshotProvider);
            this.backstageViewModel.Sections.Add(backstageLayoutsSection);

            this.backstage = m_container.Resolve<Backstage>();
            backstage.DataContext = this.backstageViewModel;
            backstage.HorizontalAlignment = HorizontalAlignment.Stretch;
            backstage.VerticalAlignment = VerticalAlignment.Stretch;

            this.backstageWindow = new Window
            {
                Content =
                    new Border
                    {
                        BorderThickness = new Thickness(1),
                        BorderBrush = new SolidColorBrush(ModernThemeHelper.AccentColor),
                        Child = this.backstage
                    },
                WindowStyle = WindowStyle.None,
                BorderThickness = new Thickness(0),
                ResizeMode = ResizeMode.NoResize,
                ShowInTaskbar = false,
                MinWidth = 720,
                MinHeight = 450,
                Width = 850,
                Height = 550
            }; 
            /*this.Resources.MergedDictionaries.Add(new ResourceDictionary
            {
                Source = new Uri("pack://application:,,,/MSDotNet.MSGui.Themes;component/MSDesktop2014/RibbonOverrides.xaml")
            });
            */

            System.Windows.Application.Current.Deactivated += (o, args) => backstageWindow.Hide();
            backstageWindow.Deactivated += (s, e_) => backstageWindow.Hide();
            this.LocationChanged += (o, args) =>
            {
                if (this.backstageWindow.IsVisible)
                {
                    UpdateBackstagePosition();
                }
            }; 
        }

        private void NoTaskbarPin()
        {
            WindowProperties.SetWindowProperty(this, SystemProperties.System.AppUserModel.PreventPinning, "1");
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);
            SetDock(false);
        }

        public void SetDock(bool isDocked_)
        {
            if (m_isTaskbar != isDocked_)
            {
                IsTaskbar = isDocked_;
                this.Width = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Right -
                             System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Left;
                this.RenderSize = new Size(System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Right -
                                           System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Left, this.RenderSize.Height);
                this.SizeToContent = m_isTaskbar ? System.Windows.SizeToContent.Manual : System.Windows.SizeToContent.Width;
                AppBarFunctions.SetAppBar(this, m_isTaskbar ? ABEdge.Bottom : ABEdge.None,
                                          !m_isTaskbar
                                            ? new Point(System.Windows.Forms.Cursor.Position.X,
                                                        System.Windows.Forms.Cursor.Position.Y)
                                            : new Point(-1, -1));
                this.Width = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Right -
                             System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Left;
                this.RenderSize = new Size(System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Right -
                                           System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Left, this.RenderSize.Height);
                this.SizeToContent = m_isTaskbar ? System.Windows.SizeToContent.Manual : System.Windows.SizeToContent.Width;
                if (m_isTaskbar)
                {
                    Dispatcher.BeginInvoke(DispatcherPriority.ApplicationIdle,
                                           (Action)(
                                                      () =>
                                                      {
                                                          ToggleMode();
                                                          ToggleMode();
                                                      }));
                }
            }
        }

        private void _OnSystemCommandCloseWindow(object sender_, ExecutedRoutedEventArgs e_)
        {
            SystemCommands.CloseWindow((Window)e_.Parameter);
            SetDock(false);
        }

        private void BarWindow_OnPreviewMouseDoubleClick(object sender_, MouseButtonEventArgs e_)
        {
            ToggleMode();
            e_.Handled = true;
        }

        public void ToggleMode()
        {
            if (BarWindow.CanToggleState)
            {
                switch (BarState)
                {
                    case LauncherBarState.Shrinked:
                        BarState = LauncherBarState.Expanded;
                        break;
                    case LauncherBarState.Expanded:
                        BarState = LauncherBarState.Shrinked;
                        break;
                }
                if (IsTaskbar)
                {
                    Dispatcher.BeginInvoke(
                        (Action)
                        (() => AppBarFunctions.ABSetPos(ABEdge.Bottom, this)),
                        DispatcherPriority.ApplicationIdle);
                }
            }
        }

        public void AddContent(UIElement content_)
        {
            Content = content_;
        }

        public void SetUpConcordStrings()
        {
            string profile = m_container.Resolve<string>("CurrentConcordProfile");
            if (!string.IsNullOrWhiteSpace(profile))
                ConcordProfile = profile;

            string user = m_container.Resolve<string>("CurrentConcordUser");
            if (!string.IsNullOrWhiteSpace(user))
                UserName = user;
        }

        //protected override void OnChildDesiredSizeChanged(UIElement child)
        //{
        //  Width = child.DesiredSize.Width;
        //  base.OnChildDesiredSizeChanged(child);
        //}
        private void OnToggleExpandCommand(object sender_, ExecutedRoutedEventArgs e_)
        {
            ToggleMode();
        }


        private ModernBackstage.Backstage backstage = null;
        private ModernBackstage.BackstageViewModel backstageViewModel = null;
        private Popup backstagePopup = null;

        private void OnToggleBackstageCommand(object sender, ExecutedRoutedEventArgs e)
        {
            if (this.backstageWindow != null)
            {
                UpdateBackstagePosition();

                if (backstageWindow.IsVisible)
                    backstageWindow.Hide();
                else
                    backstageWindow.Show();
            }

        }

        private void UpdateBackstagePosition()
        {
            backstageWindow.Left = this.Left;
            backstageWindow.Top = this.Top + this.ActualHeight - 1;
        }

        public static DependencyProperty TitleBarProperty
              = DependencyProperty.Register("TitleBar", typeof(UIElement), typeof(BarWindow));

        private Window backstageWindow;
        private LayoutScreenshotProvider screenshotProvider;


        public UIElement TitleBar
        {
            get { return (UIElement)GetValue(TitleBarProperty); }
            set
            {
                SetValue(TitleBarProperty, value);
            }
        }

        public string State
        {
            get { return this.IsTaskbar.ToString(); }
            set
            {
                switch (value.ToLower())
                {
                    case "true":
                        if (this.IsTaskbar != true)
                            this.SetDock(true);
                        break;
                    case "false":
                        if (this.IsTaskbar != false)
                            this.SetDock(false);
                        break;
                }
            }
        }

        private void OnMinimizeCommand(object sender, ExecutedRoutedEventArgs e)
        {
            OnMinimizeNeeded();
        }

        public event EventHandler MinimizeNeeded;

        protected virtual void OnMinimizeNeeded()
        {
            var handler = MinimizeNeeded;
            if (handler != null) handler(this, EventArgs.Empty);
        }
        #region  For CUIT

        protected override System.Windows.Automation.Peers.AutomationPeer OnCreateAutomationPeer()
        {

            var inner = base.OnCreateAutomationPeer();
            var chromManager = m_container.Resolve<IChromeManager>();
            var peer = new LauncherBarAutomationPeer(inner, this, new BarWindowCommandHandler(chromManager));
            return peer;
        }


        class LauncherBarAutomationPeer : RecordImageAutomationPeerWrapper
        {
            public LauncherBarAutomationPeer(AutomationPeer inner, FrameworkElement owner, ImagePathHandler commandHandler)
                : base(inner, owner, commandHandler)
            {
                
            }

            protected override bool IsEnabledCore()
            {
                RecordImage();
                return base.IsEnabledCore();
            }
        }


        private class BarWindowCommandHandler : ImagePathHandler
        {

            internal BarWindowCommandHandler(IChromeManager chromeManager)
                : base()
            {
                _chromeManager = chromeManager;
                _chromeManager.Windows.CollectionChanged += Windows_CollectionChanged;


                Handlers[MainWindowSupportedCommands.GetLastCreatedView.ToString()] =
                    (paras) => _lastViewId;

                Handlers[MainWindowSupportedCommands.GetAllViews.ToString()] =
                    (paras) => _chromeManager.Windows.Aggregate("", (id, wvc) => wvc.ID + "; " + id); ;

                Handlers[MainWindowSupportedCommands.GetFloatingViews.ToString()] =
                    (paras) => (from view in _chromeManager.Windows
                                where DockHelper.GetRootOfFloatingPane(view) != null
                                select view.ID).Aggregate("", (ids, id) => ids + ";" + id);

                Handlers[MainWindowSupportedCommands.GetDockedViews.ToString()] =
                    (paras) => (from view in _chromeManager.Windows
                                where DockHelper.GetRootOfDockedPane(view) != null
                                select view.ID).Aggregate("", (ids, id) => ids + ";" + id);


                Handlers[MainWindowSupportedCommands.SetToPlaybackMode.ToString()] =
                    (paras) =>
                    {
                        VisualTestHelper.IsRecording =
                            false;
                        return "";
                    };


                Handlers[MainWindowSupportedCommands.MoveTo.ToString()] =
                    (parameters) =>
                    {
                        double top, left;
                        if (!double.TryParse(parameters[0], out left) ||
                            !double.TryParse(parameters[1], out top))
                            return "";

                        var peer = this.AutomationPeer as LauncherBarAutomationPeer;
                        var barWindow = peer.Owner as BarWindow;
                        barWindow.Left = left;
                        barWindow.Top = top;

                        return "";
                    };

                Handlers[MainWindowSupportedCommands.GetAppliationTitle.ToString()] =
                    (paras) =>
                    {
                        var peer = this.AutomationPeer as LauncherBarAutomationPeer;
                        var barWindow = peer.Owner as BarWindow;
                        return barWindow.Title;
                    };

                Handlers[MainWindowSupportedCommands.GetProcessId.ToString()] =
               (paras) =>
               {
                   return Process.GetCurrentProcess().Id.ToString();
               };


            }

            private IChromeManager _chromeManager;
            private string _lastViewId;

            ~BarWindowCommandHandler()
            {
                _chromeManager.Windows.CollectionChanged -= Windows_CollectionChanged;
            }

            void Windows_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
            {
                if (e.NewItems != null)
                {
                    var wvc = e.NewItems[0] as IWindowViewContainer;
                    _lastViewId = wvc.ID;
                }
            }
        }



        #endregion
         
    }

    public static class LauncherBarCommnads
    {
        public static readonly RoutedUICommand ToggleExpandCommand = new RoutedUICommand();
        public static readonly RoutedUICommand ToggleBackstageCommand = new RoutedUICommand(); 
    }

    public class BarStateToHeightConverter : IValueConverter
    {
        public static int HeightShrinked { get; set; }

        public static int HeightExpanded { get; set; }

        static BarStateToHeightConverter()
        {
            HeightExpanded = 200;
            HeightShrinked = 50;
        }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if ((LauncherBarState)value == LauncherBarState.Shrinked)
                return HeightShrinked;
            else
                return HeightExpanded;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }


}
