﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.ViewModels;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions.MVVMDock;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.LauncherBar
{
    static class HarmoniaFloatingOnlyDock
    { 
        public static void Bind(FloatingOnlyDockViewModel model_)
        {
            var factory = new HarmoniaContentPaneFactory(model_);
            model_.LoadRequested += OnLoadRequested;
            model_.SaveRequested += OnSaveRequested;
        }

        private static void OnSaveRequested(SaveEventArgs args_)
        {
            try
            {
                // Tell the XamDockManager to save its current layout to the string
                string layoutXml = DockManager.Services.LayoutManager.SaveLayout();  
                args_.SavedState = XDocument.Parse(layoutXml);
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Exception during Save:" + Environment.NewLine + ex.Message, ex);
            }
        }

        private static void OnLoadRequested(LoadEventArgs state_)
        {
            string layout = state_.State.ToString();
            try
            {
                // Tell XamDockManager to load the layout from the string
                DockManager.Services.LayoutManager.LoadLayout(layout); //we could also use XmlWriter here, might have better performance 
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Exception during Load:" + Environment.NewLine + ex.Message, ex);
            }
        } 

    }
}
