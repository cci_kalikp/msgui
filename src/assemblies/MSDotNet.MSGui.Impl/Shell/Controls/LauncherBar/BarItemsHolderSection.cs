﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.LauncherBar;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
    public class BarItemsHolderSection : Panel
    { 
        public BarItemsHolderSection()
        { 
            Orientation = Orientation.Vertical; // could be set in a static constructor too

        }

        private static bool IsButtonLike(object control_)
        {
            return control_ is LauncherBarButton ||
                   control_ is OptionsButton ||
                   control_ is Button ||
                   (control_ is WidgetPlaceholder && ((WidgetPlaceholder)control_).Content is Button);
        }

        //-----------------
        //Measure and arrange. Buttons like WrapPanel, Widgets like DockPanel
        //-----------------
        private Orientation m_orientation = Orientation.Vertical;
        public static readonly DependencyProperty OrientationProperty =
          StackPanel.OrientationProperty.AddOwner(
          typeof(BarItemsHolderSection),
          new FrameworkPropertyMetadata(
            Orientation.Vertical,
            FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsParentMeasure,
            BarItemsHolderSection.OnOrientationChanged));

        private static void OnOrientationChanged(DependencyObject d_, DependencyPropertyChangedEventArgs e_)
        {
            BarItemsHolderSection panel = (BarItemsHolderSection)d_;
            panel.m_orientation = (Orientation)e_.NewValue;
        }

        public Orientation Orientation
        {
            get
            {
                return m_orientation;
            }
            set
            {
                SetValue(OrientationProperty, value);
            }
        }

        // Override the default Measure method of Panel
        protected override Size MeasureOverride(Size availableSize)
        {
            switch (Orientation)
            {
                case Orientation.Horizontal:
                    return MeasureOverrideHorizontal(availableSize);
                    break;
                case Orientation.Vertical:
                    return MeasureOverrideVertical(availableSize);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private Size MeasureOverrideVertical(Size availableSize_)
        {
            var childCount = InternalChildren.Count;
            double maxHeight = 0;
            double width = 0;
            double oldwidth = 0;
            bool buttonStack = false;
            double buttonStackHeight = 0;
            for (int i = 0; i < childCount; i++)
            {
                var child = InternalChildren[i];
                child.Measure(availableSize_);
                if (IsButtonLike(child))
                {
                    if (buttonStack && availableSize_.Height - buttonStackHeight < child.DesiredSize.Height)
                    {
                        buttonStack = false;
                    }

                    if (buttonStack)
                    {
                        width = Math.Max(width, oldwidth + child.DesiredSize.Width);
                        buttonStackHeight += child.DesiredSize.Height;
                    }
                    else
                    {
                        buttonStack = true;
                        buttonStackHeight = child.DesiredSize.Height;
                        oldwidth = width;
                        width = oldwidth + child.DesiredSize.Width;
                    }
                }
                else
                {
                    buttonStack = false;
                    maxHeight = Math.Max(maxHeight, child.DesiredSize.Height);
                    width += child.DesiredSize.Width;
                }
            }
            return new Size(width, maxHeight);
        }

        private Size MeasureOverrideHorizontal(Size availableSize_)
        {
            var childCount = InternalChildren.Count;
            double maxWidth = 0;
            double height = 0;
            double oldheight = 0;
            bool buttonStack = false;
            double buttonStackWidth = 0;
            for (int i = 0; i < childCount; i++)
            {
                var child = InternalChildren[i];
                child.Measure(availableSize_);
                if (IsButtonLike(child))
                {
                    if (buttonStack && availableSize_.Width - buttonStackWidth < child.DesiredSize.Width)
                    {
                        buttonStack = false;
                    }

                    if (buttonStack)
                    {
                        height = Math.Max(height, oldheight + child.DesiredSize.Height);
                        buttonStackWidth += child.DesiredSize.Width;
                    }
                    else
                    {
                        buttonStack = true;
                        buttonStackWidth = child.DesiredSize.Width;
                        oldheight = height;
                        height = oldheight + child.DesiredSize.Height;
                    }
                }
                else
                {
                    buttonStack = false;
                    maxWidth = Math.Max(maxWidth, child.DesiredSize.Width);
                    height += child.DesiredSize.Height;
                }
            }
            return new Size(maxWidth, height);
        }


        protected override Size ArrangeOverride(Size finalSize)
        {
            switch (Orientation)
            {
                case Orientation.Horizontal:
                    ArrangeHorizontal(finalSize);
                    break;
                case Orientation.Vertical:
                    ArrangeVertical(finalSize);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return finalSize;
        }

        private void ArrangeVertical(Size finalSize_)
        {
            double x = 0;
            double y = 0;
            double buttonx = 0;
            double buttony = 0;
            foreach (UIElement child in InternalChildren)
            {
                Size desired = child.DesiredSize;
                if (IsButtonLike(child))
                {
                    if (buttony != 0 && buttony + desired.Height > finalSize_.Height)
                    {
                        buttonx = x;
                        buttony = 0; // caret return
                    }

                    child.Arrange(new Rect(new Point(buttonx, buttony), new Size(desired.Width, Math.Min(finalSize_.Height - buttony, desired.Height))));
                    buttony += desired.Height;
                    x = Math.Max(x, buttonx + desired.Width);
                }
                else
                {
                    buttony = 0;
                    child.Arrange(new Rect(new Point(x, y), new Size(desired.Width, finalSize_.Height)));
                    x += desired.Width;
                    buttonx = x;
                }
            }
        }

        private void ArrangeHorizontal(Size finalSize_)
        {
            double x = 0;
            double buttonx = 0;
            double buttony = 0;
            double y = 0;
            foreach (UIElement child in InternalChildren)
            {
                Size desired = child.DesiredSize;
                if (IsButtonLike(child))
                {
                    if (buttonx != 0 && buttonx + desired.Width > finalSize_.Width)
                    {
                        buttony = y;
                        buttonx = 0; // caret return
                    }

                    child.Arrange(new Rect(new Point(buttonx, buttony), new Size(Math.Min(finalSize_.Width - buttonx, desired.Width), desired.Height)));
                    buttonx += desired.Width;
                    y = Math.Max(y, buttony + desired.Height);
                }
                else
                {
                    buttonx = 0;
                    child.Arrange(new Rect(new Point(x, y), new Size(finalSize_.Width, desired.Height)));
                    y += desired.Height;
                    buttony = y;
                }
            }
        }
    }
}
