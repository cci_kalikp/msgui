﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media; 

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls 
{
    //we inherit from separator instead of border or any other control, so that when the none msdesktop theme is used, it still looks consistent with the look and feel of a horizontal separator
    public class VerticalSeparator:Separator 
    {
        static VerticalSeparator()
        {
            LayoutTransformProperty.OverrideMetadata(
                   typeof(VerticalSeparator),
                   new FrameworkPropertyMetadata(new RotateTransform(90)));
            MarginProperty.OverrideMetadata(typeof(VerticalSeparator), new FrameworkPropertyMetadata(new Thickness(3, 0, 3, 0)));  
        }

        
    }
} 