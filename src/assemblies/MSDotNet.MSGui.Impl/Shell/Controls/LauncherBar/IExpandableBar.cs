/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Controls/LauncherBar/IExpandableBar.cs#5 $
// $Change: 855003 $
// $DateTime: 2013/11/19 03:33:57 $
// $Author: caijin $

using System;
using System.Windows;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
  public interface IExpandableBar
  {
    LauncherBarState BarState
    { 
      get;
    }
    event EventHandler BarStateChanged;
    string Version { get; set; }    
    bool IsTaskbar { get; set; }
    bool IsTopmost { get; set; }
    string Profile { get; set; }
    void SetDock(bool b_);
    void ToggleMode();
    void AddContent(UIElement content_);
		string UserName { get; set; }
		string ConcordProfile { get; set; }
        string State { get; set; }
  }
}