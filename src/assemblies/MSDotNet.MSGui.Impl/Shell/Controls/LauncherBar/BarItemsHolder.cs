﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Controls/LauncherBar/BarItemsHolder.cs#21 $
// $Change: 902897 $
// $DateTime: 2014/10/29 01:43:59 $
// $Author: caijin $

using System;
using System.Windows;
using System.Windows.Controls;
using MorganStanley.MSDotNet.MSGui.Controls.Controls;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.ControlParadigm;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.LauncherBar;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager; 

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
    public class BarItemsHolder : DockPanel
    {
        private readonly IExpandableBar expandableBar;
        private readonly BarItemsHolderSection scrollableHolder;
        private readonly BarItemsHolderSection freezeHolder;

        public BarItemsHolder(IExpandableBar expandableBar_)
        {
            expandableBar = expandableBar_;
            this.LastChildFill = true;
            freezeHolder = new BarItemsHolderSection {Orientation = Orientation.Vertical}; 
            DockPanel.SetDock(freezeHolder, Dock.Left);
            this.Children.Add(freezeHolder); 
            XamPager pager = new XamPager
            {
                VerticalScrollBarVisibility = ScrollBarVisibility.Disabled,
                HorizontalScrollBarVisibility = BarWindow.AutoScrollHorizontally ?
                ScrollBarVisibility.Auto : ScrollBarVisibility.Disabled
            };
            this.Children.Add(pager);
            scrollableHolder = new BarItemsHolderSection {Orientation = Orientation.Vertical};
            pager.Content = scrollableHolder;
            Orientation = Orientation.Vertical; // could be set in a static constructor too 

        }

        internal BarItemsHolderSection ButtonHolder
        {
            get { return scrollableHolder; }
        }
        public void AddButton(LauncherBarButton button_, bool freeze_)
        {
            button_.MSGuiButton.ButtonSize = SetButtonSize();
            expandableBar.BarStateChanged += delegate
                {
                    button_.MSGuiButton.ButtonSize = SetButtonSize();
                };

            (freeze_ ? freezeHolder : scrollableHolder).Children.Add(button_);
        }

        public void AddWidget(IWidget widget_, bool freeze_)
        {
            ((Widget) widget_).State = SetWidgetState();
            expandableBar.BarStateChanged += delegate
                {
                    ((Widget) widget_).State = SetWidgetState();
                };

            WidgetPlaceholder widgetPlaceholder = CreateWidgetControl(widget_);
            widgetPlaceholder.Margin = new Thickness(3);
            (freeze_ ? freezeHolder : scrollableHolder).Children.Add(widgetPlaceholder);
        }


        private ButtonSize SetButtonSize()
        {
            switch (expandableBar.BarState)
            {
                case LauncherBarState.Expanded:
                    return ButtonSize.Large;
                case LauncherBarState.Shrinked:
                    return ButtonSize.Small;
            }
            return ButtonSize.Large;
        }

        private WidgetState SetWidgetState()
        {
            switch (expandableBar.BarState)
            {
                case LauncherBarState.Expanded:
                    return WidgetState.Maximized;
                case LauncherBarState.Shrinked:
                    return WidgetState.Minimized;
            }
            return WidgetState.Minimized;
        }

        public void AddChild(UIElement child_, bool freeze_)
        { 
            (freeze_ ? freezeHolder : scrollableHolder).Children.Add(child_);
        }

        public void InsertChild(int index_, UIElement child_, bool freeze_)
        {
            (freeze_ ? freezeHolder : scrollableHolder).Children.Insert(index_, child_);
        }
        public void RemoveChild(UIElement child_)
        {
            freezeHolder.Children.Remove(child_);
            scrollableHolder.Children.Remove(child_);
        }
         

        public void SwapChildren(UIElement childA_, UIElement childB_)
        {
            BarItemsHolderSection holderA = freezeHolder;
            var indexA = freezeHolder.Children.IndexOf(childA_);
            if (indexA < 0)
            {
                indexA = scrollableHolder.Children.IndexOf(childA_);
                holderA = scrollableHolder;
            }
            BarItemsHolderSection holderB = freezeHolder;
            var indexB = freezeHolder.Children.IndexOf(childB_);
            if (indexB < 0)
            {
                indexB = scrollableHolder.Children.IndexOf(childB_);
                holderB = scrollableHolder;
            } 
            holderB.Children.RemoveAt(indexB);
            holderB.Children.Insert(indexB, new GroupBox());
            holderA.Children.RemoveAt(indexA);
            holderA.Children.Insert(indexA, childB_);
            holderB.Children.RemoveAt(indexB);
            holderB.Children.Insert(indexB, childA_);
        }

        private WidgetPlaceholder CreateWidgetControl(IWidget widget_)
        {
            WidgetPlaceholder widgetPlaceholder = new WidgetPlaceholder();
            widgetPlaceholder.Content = widget_;
            if (widget_.MaximizedView is OptionsButton)
            {
                widgetPlaceholder.MaxHeight = 44;
            }
            return widgetPlaceholder;
        }


        private Orientation m_orientation = Orientation.Vertical;

        public static readonly DependencyProperty OrientationProperty =
            StackPanel.OrientationProperty.AddOwner(
                typeof (BarItemsHolder),
                new FrameworkPropertyMetadata(
                    Orientation.Vertical,
                    FrameworkPropertyMetadataOptions.AffectsMeasure,
                    BarItemsHolder.OnOrientationChanged));

        private static void OnOrientationChanged(DependencyObject d_, DependencyPropertyChangedEventArgs e_)
        {
            BarItemsHolder panel = (BarItemsHolder) d_;
            panel.m_orientation = (Orientation) e_.NewValue;
            panel.freezeHolder.Orientation =
            panel.scrollableHolder.Orientation = (Orientation) e_.NewValue;
        }

        public Orientation Orientation
        {
            get { return m_orientation; }
            set { SetValue(OrientationProperty, value); }
        } 
    }
}
