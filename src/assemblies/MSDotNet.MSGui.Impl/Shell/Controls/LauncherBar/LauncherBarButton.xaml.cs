﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Controls/LauncherBar/LauncherBarButton.xaml.cs#11 $
// $Change: 865596 $
// $DateTime: 2014/02/06 22:09:00 $
// $Author: caijin $

using System;
using System.Windows;
using System.Windows.Data;
using MorganStanley.MSDotNet.MSGui.Core;
using System.Windows.Input;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
  /// <summary>
  /// Interaction logic for LauncherBarButton.xaml
  /// </summary>
  public partial class LauncherBarButton 
  {
    private readonly MSGuiButton m_msguiButton;

      public new static double MaxHeight = 44d;
    public LauncherBarButton(MSGuiButton msguiButton_)
    {
        base.MaxHeight = MaxHeight;
      m_msguiButton = msguiButton_;
      InitializeComponent();
      DataContext = msguiButton_;

      button.Click += delegate(object sender_, RoutedEventArgs e_)
                 {
                   if (msguiButton_.OnClick != null)
                   {
                     var copy = msguiButton_.OnClick;
                     copy(sender_, e_);
                   }
                 };

      button.IsEnabled = msguiButton_.IsEnabled;
      if (msguiButton_.CommandBinding != null)
      {
        button.CommandBindings.Add(msguiButton_.CommandBinding);
      }
      button.Command = msguiButton_.Command;
    }

    public MSGuiButton MSGuiButton
    {
      get
      {
        return m_msguiButton;
      }
    }
  }
     
}
