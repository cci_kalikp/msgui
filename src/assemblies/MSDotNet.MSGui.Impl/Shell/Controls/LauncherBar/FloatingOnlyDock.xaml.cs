﻿////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Controls/LauncherBar/FloatingOnlyDock.xaml.cs#41 $
// $Change: 896821 $
// $DateTime: 2014/09/15 02:20:20 $
// $Author: caijin $

using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Interop;
using Infragistics.Windows.DockManager.Dragging;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.Win32;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions.MVVMDock;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
	/// <summary>
	/// Interaction logic for FloatingOnlyDock.xaml
	/// </summary>
    internal partial class FloatingOnlyDock
	{
		internal static bool UseImmediatePaneDragging = false;
		internal static bool UseSystemWindowDrag = false;
        internal static bool WindowDeactivationNotificationEnabled = false;

		public FloatingOnlyDock()
		{
			DataContextChanged += OnDataContextChanged;
			InitializeComponent();
             
            dockManager.ActivePaneChanged += (s, e) =>
            { 
                if (e.NewValue != null)
                {
                    if (e.NewValue is CustomContentPane)
                    { 
                        (e.NewValue as CustomContentPane).UpdateContainer(true);
                    }
                }
            };
            dockManager.PaneDragStarting += (s, e) =>
			{
				if (s != dockManager)
					return;

                if (((DockViewModel)DataContext).HidingLockingManager.Granularity.HasFlag(HideLockUIGranularity.LockViewDragDrop))
				{
					e.Cancel = true;
					return;
				}

				var rc = ReflHelper.FieldGet(e, "_rootContainer") as FrameworkElement;
                if (rc is CustomPaneToolWindow)
				{
					if (e.Panes.OfType<CustomContentPane>().Any(cp => cp.Topmost))
					{
						e.Cancel = true;
					}
				}
			};

			dockManager.PaneDragOver += (s, e) =>
			{
				// are we dragging topmost stuff?
				if ((from cp in e.Panes.OfType<CustomContentPane>()
						where cp.Topmost
						select e.DragAction as MoveWindowAction).Any(mwa => mwa == null))
				{
					e.IsValidDragAction = false;
				}

				if (e.DragAction is NewSplitPaneAction)
				{
					var E = e.DragAction as NewSplitPaneAction;
					if (XamDockHelper.IsTopmost(E.Pane))
					{
						e.IsValidDragAction = false;
					}
				}
				else if (e.DragAction is NewTabGroupAction)
				{
					var E = e.DragAction as NewTabGroupAction;
                    if (XamDockHelper.IsTopmost(E.Pane))
					{
						e.IsValidDragAction = false;
					}
				}
				else if (e.DragAction is AddToGroupAction)
				{
					var E = e.DragAction as AddToGroupAction;
                    if (XamDockHelper.IsTopmost(E.Group))
					{
						e.IsValidDragAction = false;
					}
				}
			};

			if (FloatingOnlyDock.UseImmediatePaneDragging)
			{
				dockManager.FloatingWindowDragMode = Infragistics.Windows.DockManager.FloatingWindowDragMode.Immediate;
			}
			if (FloatingOnlyDock.UseSystemWindowDrag)
			{
				dockManager.FloatingWindowDragMode = Infragistics.Windows.DockManager.FloatingWindowDragMode.UseSystemWindowDrag;
			}
		}


		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
			WindowInteropHelper wndHelper = new WindowInteropHelper(this);

			var exStyle = Win32.GetWindowLong(wndHelper.Handle, (-20));

			exStyle |= (int)Win32.WS_EX_TOOLWINDOW;
			Win32.SetWindowLong(wndHelper.Handle, (-20), exStyle);
		}

		private void OnDataContextChanged(object sender_, DependencyPropertyChangedEventArgs e_)
		{
			if (e_.NewValue is ILayoutSaveLoadRequester)
			{
				controller.EventHost = e_.NewValue as ILayoutSaveLoadRequester;
			}
			if (e_.NewValue is DockViewModel)
			{
				DockViewModel vm = (DockViewModel)e_.NewValue;
				factory.ItemsSource = vm.Workspaces;

                if (WindowDeactivationNotificationEnabled)
                {
                    XamDockHelper.HwndSourceHookFactory hookFactory = XamDockHelper.GetCheckForActivePaneChangesHookFactory(Window.GetWindow(this), dockManager, DataContext);
                    dockManager.Loaded += (s, e) =>
                    {
                        var mainWindow = Window.GetWindow(this);
                        var mainWindowHandle = (new WindowInteropHelper(mainWindow)).Handle;
                        var hwndSource = HwndSource.FromHwnd(mainWindowHandle);
                        hwndSource.AddHook(hookFactory(mainWindow));
                    };
                    dockManager.ToolWindowLoaded += (s, e) =>
                        {
                            var paneToolWindow = e.Window;
                            RoutedEventHandler loaded = null;
                            paneToolWindow.Loaded += loaded = (s1, e1) =>
                                {
                                    paneToolWindow.Loaded -= loaded;
                                    var paneWindow = Window.GetWindow(e.Window);
                                    var paneWindowHandle = (new WindowInteropHelper(paneWindow)).Handle;
                                    var hwndSource = HwndSource.FromHwnd(paneWindowHandle);
                                    hwndSource.AddHook(hookFactory(paneWindow));
                                };
                        };
                }
			}
		}
	}
}
