﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Controls/LauncherBar/OptionsButton.xaml.cs#19 $
// $Change: 898126 $
// $DateTime: 2014/09/24 06:50:44 $
// $Author: caijin $

using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers.ShellMenu;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Options;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.Converters;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.LauncherBar
{
  /// <summary>
  /// Interaction logic for OptionsButton.xaml
  /// </summary>
  public partial class OptionsButton : UserControl
  {
      private IChromeManager m_chromeManager;
      private IChromeRegistry m_chromeRegistry;

      public OptionsButton()
      {
          InitializeComponent();
          InitFieldsFromResources();

          this.TopmostMenuItem.Visibility = System.Windows.Visibility.Collapsed;
          this.DockMenuItem.Visibility = System.Windows.Visibility.Collapsed;
          this.UndockMenuItem.Visibility = System.Windows.Visibility.Collapsed;
          this.MinimizeMenuItem.Visibility = System.Windows.Visibility.Collapsed;
      }

      public OptionsButton(BarWindow launcherBarWindow_)
    {
        InitializeComponent();
        InitFieldsFromResources();

        var topmostBinding = new Binding("IsTopmost")
        {
            Source = launcherBarWindow_,
            Mode = BindingMode.TwoWay
        };
        this.TopmostMenuItem.SetBinding(MenuItem.IsCheckedProperty, topmostBinding);

        var taskbarBinding = new Binding("IsTaskbar")
        {
            Source = launcherBarWindow_,
            Mode = BindingMode.OneWay,
            Converter = new OppositeBoolConverter()
        };
        //Topmost menu item does not make sense if the window is taskbar-like
        this.TopmostMenuItem.SetBinding(UIElement.IsEnabledProperty, taskbarBinding);

        launcherBarWindow_.TaskbarStateChanged += delegate
        {
            if (launcherBarWindow_.IsTaskbar)
            {
                this.DockMenuItem.Visibility = Visibility.Collapsed;
                this.UndockMenuItem.Visibility = Visibility.Visible;
            }
            else
            {
                this.DockMenuItem.Visibility = Visibility.Visible;
                this.UndockMenuItem.Visibility = Visibility.Collapsed;
            }
        };

        if (launcherBarWindow_.IsTaskbar)
        {
            this.DockMenuItem.Visibility = Visibility.Collapsed;
            this.UndockMenuItem.Visibility = Visibility.Visible;
        }      
    }


    internal void Initialize(IChromeManager chromeManager_, IChromeRegistry chromeRegistry_, ApplicationOptions applicationOptions_)
    {
        m_chromeManager = chromeManager_;
        m_chromeRegistry = chromeRegistry_;

        AddMenuItemToApplicationMenu(LoadLayoutMenu, ShellMenuExtensions.ApplicationMenuFactories.LoadLayoutMenu);
        AddMenuItemToApplicationMenu(SaveCurrentLayoutMenu, ShellMenuExtensions.ApplicationMenuFactories.SaveLayoutButton);
        AddMenuItemToApplicationMenu(SaveLayoutMenu, ShellMenuExtensions.ApplicationMenuFactories.SaveLayoutMenu);
        AddMenuItemToApplicationMenu(Resources["deleteLayoutMenu"], ShellMenuExtensions.ApplicationMenuFactories.DeleteLayoutButton);
        AddMenuItemToApplicationMenu(new Separator(),
                                     ShellMenuExtensions.ApplicationMenuFactories.AfterLayoutManagementSeparator);
        AddMenuItemToApplicationMenu(ThemesMenu, ShellMenuExtensions.ApplicationMenuFactories.ThemesButton);
        AddButtonToApplicationMenu(ShellMenuExtensions.ApplicationMenuFactories.OptionsButton,
                                   applicationOptions_.GetOptionButtonParameter());
        AddMenuItemToApplicationMenu(Resources["dockingMenu"], ShellMenuExtensions.ApplicationMenuFactories.DockingMenu);
        AddMenuItemToApplicationMenu(TopmostMenuItem, ShellMenuExtensions.ApplicationMenuFactories.TopmostButton);
        
        AddMenuItemToApplicationMenu(MinimizeMenuItem, ShellMenuExtensions.ApplicationMenuFactories.MinimizeButton);
        AddMenuItemToApplicationMenu(Resources["exitMenu"], ShellMenuExtensions.ApplicationMenuFactories.ExitButtonWithSeparator);

        (m_chromeManager as ChromeManagerBase).AfterCreateChrome +=
                        (sender_, args_) =>
                        {
                            var exitButton = Resources["exitMenu"] as MenuItem;
                            if (exitButton != null && optionsContextMenu.Items.Contains(exitButton))
                            {
                                chromeManager_.RemoveWidget(ShellMenuExtensions.ApplicationMenuFactories.ExitButtonWithSeparator);
                                m_chromeManager[ChromeManagerBase.MENU_COMMANDAREA_NAME].AddWidget(ShellMenuExtensions.ApplicationMenuFactories.ExitButtonWithSeparator, new InitialWidgetParameters());
                            }
                        };
    }

    private void AddMenuItemToApplicationMenu(object tool_, string factoryName_)
    {
        m_chromeRegistry.RegisterWidgetFactory(factoryName_, (viewContainer_, state_) =>
        {
            viewContainer_.Content = tool_;
            return true;
        });
        m_chromeManager[ChromeManagerBase.MENU_COMMANDAREA_NAME].AddWidget(factoryName_,
                                                                           new InitialWidgetParameters());
    }

    private void AddButtonToApplicationMenu(string factoryName_, InitialButtonParameters parameter_)
    {
        m_chromeManager[ChromeManagerBase.MENU_COMMANDAREA_NAME].AddWidget(factoryName_, parameter_);

    }

      private void InitFieldsFromResources()
      {
          var dockingMenuItem = (MenuItem)Resources["dockingMenu"];

          TopmostMenuItem = (MenuItem)Resources["topmostMenu"];
          DockMenuItem = (MenuItem)dockingMenuItem.Items[0];
          UndockMenuItem = (MenuItem)dockingMenuItem.Items[1];
          LoadLayoutMenu = (MenuItem)Resources["loadLayoutMenu"];
          SaveCurrentLayoutMenu = (MenuItem)Resources["saveLayoutMenu"];
          SaveLayoutMenu = (MenuItem)Resources["saveLayoutAsMenu"];
          ThemesMenu = (MenuItem)Resources["themesMenu"];
          MinimizeMenuItem = (MenuItem) Resources["minimizeMenuItem"];
      }

    private void Button_Click(object sender, System.Windows.RoutedEventArgs e)
    {
      optionsContextMenu.PlacementTarget = this;
      optionsContextMenu.IsOpen = true;
    }  

    private void DeleteCurrentSelected(object sender_, RoutedEventArgs e_)
    {
      var copy = DeleteCurrentCalled;
      if (copy != null)
      {
        copy(this, EventArgs.Empty);
      }
    }

    private void ExitSelected(object sender_, RoutedEventArgs e_)
    {
      var copy = ExitCalled;
      if (copy != null)
      {
        copy(this, EventArgs.Empty);
      }
    }
    
    private void DockSelected(object sender, RoutedEventArgs e)
    {      
      var copy = DockCalled;
      if (copy != null)
      {
        copy(this, EventArgs.Empty);
      }
    }

    private void UnDockSelected(object sender, RoutedEventArgs e)
    {      
      var copy = UnDockCalled;
      if (copy != null)
      {
        copy(this, EventArgs.Empty);
      }
    }

    private void MinimizeMenuItem_OnClick(object sender, RoutedEventArgs e)
    {
        var copy = MinimizeNeeded;
        if (copy != null)
        {
            copy(this, EventArgs.Empty);
        }
    }

      private void RedrawCall()
      {
          var copy = RedrawNeeded;
          if (copy != null)
          {
              copy(this, EventArgs.Empty);
          }
      }
    private void CustomizeSelected(object sender_, RoutedEventArgs e_)
    {
      
    }

    public EventHandler DeleteCurrentCalled;
    public EventHandler ExitCalled;
    public EventHandler DockCalled;
    public EventHandler UnDockCalled;
      public EventHandler RedrawNeeded;
      public EventHandler MinimizeNeeded;

      internal MenuItem TopmostMenuItem;
      internal MenuItem DockMenuItem;
      internal MenuItem UndockMenuItem;
      internal MenuItem LoadLayoutMenu;
      internal MenuItem SaveCurrentLayoutMenu;
      internal MenuItem SaveLayoutMenu;
      internal MenuItem ThemesMenu;
      internal MenuItem MinimizeMenuItem;

  }
}
