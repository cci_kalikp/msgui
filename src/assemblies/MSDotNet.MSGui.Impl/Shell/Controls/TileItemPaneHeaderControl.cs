﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.Converters;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
    public class TileItemPaneHeaderControl:PaneHeaderControlBase
    {
        private ITitleDisplayer titleDisplayer;
        private RoutedEventHandler titleNeedChangingHandler;

        static TileItemPaneHeaderControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(TileItemPaneHeaderControl), 
                new FrameworkPropertyMetadata(typeof(TileItemPaneHeaderControl)));
            ShowCloseButtonProperty.OverrideMetadata(typeof(TileItemPaneHeaderControl), new UIPropertyMetadata(false));
            ShowEnvironmentLabelProperty.OverrideMetadata(typeof(TileItemPaneHeaderControl), new UIPropertyMetadata(false));
            ShowMinMaxButtonProperty.OverrideMetadata(typeof(TileItemPaneHeaderControl), new UIPropertyMetadata(false));
            ShowMinimizeButtonProperty.OverrideMetadata(typeof(TileItemPaneHeaderControl), new UIPropertyMetadata(false));
            ShowPinButtonProperty.OverrideMetadata(typeof(TileItemPaneHeaderControl), new UIPropertyMetadata(false));
            ShowTearOffButtonProperty.OverrideMetadata(typeof(TileItemPaneHeaderControl), new UIPropertyMetadata(false));
            ShowTopmostButtonProperty.OverrideMetadata(typeof(TileItemPaneHeaderControl), new UIPropertyMetadata(false));
        }

        internal protected override void BindContentObject()
        {

            base.BindContentObject();

            if (this.TitlePart != null)
            {
                var headerBinding = new Binding()
                {
                    Source = this.ContentObject,
                    Path = new PropertyPath("Title"),
                    Mode = BindingMode.TwoWay
                };

                var headerVisibilityBinding = new Binding
                {
                    Source = this,
                    Path = new PropertyPath("ShowHeaderText"),
                    Converter = new BooleanToVisibilityConverter()
                };
                TitlePart.SetBinding(TextBlock.TextProperty, headerBinding);
                TitlePart.SetBinding(UIElement.VisibilityProperty, headerVisibilityBinding);
            } 

            if (ImagePart != null && ShowIcon)
            {
                if (ShellModeExtension.HideUIGranularity.HasFlag(HideLockUIGranularity.HideHeaderIcon))
                    ImagePart.Visibility = Visibility.Hidden;
                else
                {
                    Binding imageBinding = new Binding()
                    {
                        Source = ContentObject,
                        Path = new PropertyPath("Icon"),
                        Converter = new IconImageSourceConverter()
                    };
                    ImagePart.SetBinding(Image.SourceProperty, imageBinding);


                    Binding imageMarginBinding = new Binding()
                    {
                        Source = ContentObject,
                        Path = new PropertyPath("Icon"),
                        Converter = new ImageMarginConverter()
                    };
                    ImagePart.SetBinding(FrameworkElement.MarginProperty, imageMarginBinding);
                }
            }

            this.titleNeedChangingHandler = (x, y) =>
            {
                if (TitlePart != null && TitlePart.IsVisible)
                {
                    TitlePart.IsInEditMode = true;
                }
            };   
            DependencyObject vchcParent = this.Parent;
            while (vchcParent != null)
            {
                if (vchcParent is ITitleDisplayer)
                {
                    titleDisplayer = vchcParent as ITitleDisplayer;
                    Attached.AddTitleNeedChangingHandler(vchcParent, this.titleNeedChangingHandler);
                    break;
                }
                vchcParent = VisualTreeHelper.GetParent(vchcParent);
            }
        }


 

        protected override void Clear()
        {
            base.Clear();
            if (titleDisplayer != null)
            {
                Attached.RemoveTitleNeedChangingHandler(this.titleDisplayer as DependencyObject, titleNeedChangingHandler);
            }
        }
    }
}
