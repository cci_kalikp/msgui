using System.Collections;
using System.Reflection;
using System.Xml;
using MorganStanley.IED.Concord.Configuration;
using MorganStanley.MSDotNet.MSGui.Impl.Concord;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.UserPreference
{
    public class UserPreferenceInitialSettings : Core.ViewModel.ViewModelBase
    {
        internal static ShellMode DefaultShellMode = ShellMode.RibbonWindow;

        internal static ShellMode[] AvailableShellModes = new ShellMode[]
            {
                ShellMode.RibbonWindow, ShellMode.LauncherBarAndFloatingWindows,
                ShellMode.LauncherBarAndWindow, ShellMode.RibbonAndFloatingWindows
            };
        private ShellMode shellMode = DefaultShellMode;
        public ShellMode ShellMode
        {
            get { return shellMode; }
            set
            {
                if (value != shellMode)
                {
                    shellMode = value;
                    OnPropertyChanged("ShellMode");
                }
            }
        }

        public bool ShellModeConfigurable { get; set; }

        internal static string DefaultTheme = "Simple";
        private string theme = DefaultTheme;
        public string Theme
        {
            get { return theme; }
            set
            {
                if (value != theme)
                {
                    theme = value;
                    OnPropertyChanged("Theme");
                }
            }
        }

        public bool ThemeConfigurable { get; set; }

        public void Save()
        {
            if (ShellModeConfigurable)
            {
                SaveConfiguration(ShellModeConfig, shellMode.ToString());
            }

            if (ThemeConfigurable && !string.IsNullOrEmpty(theme))
            {
                SaveConfiguration(ThemeConfig, theme);
            }
        }

        public void Load()
        {
            if (ShellModeConfigurable)
            {
                var shellModeConfig = (Configurator)ConfigurationManager.GetConfig(ShellModeConfig);
                var shellModeConfiguredString = shellModeConfig.GetValue(ShellModeConfig, null);
                if (!string.IsNullOrEmpty(shellModeConfiguredString))
                {
                    MorganStanley.MSDotNet.MSGui.Impl.Extensions.ShellMode shellModeConfigured;
                    if (MorganStanley.MSDotNet.MSGui.Impl.Extensions.ShellMode.TryParse(shellModeConfiguredString,
                                                                                        out shellModeConfigured))
                    {
                        this.ShellMode = shellModeConfigured;
                    }
                }
            }

            if (ThemeConfigurable)
            {
                var themeConfig = (Configurator)ConfigurationManager.GetConfig(ThemeConfig);
                var themeConfigured = themeConfig.GetValue(ThemeConfig, null);
                if (!string.IsNullOrEmpty(themeConfigured))
                {
                    this.Theme = themeConfigured;
                }
            }
        }

        private static void SaveConfiguration(string configName_, string configValue_)
        {
            var config = (Configurator)ConfigurationManager.GetConfig(configName_);
            var xn = new XmlDocument
            {
                InnerXml = string.Format(@"<configuration>
    <{1}>{0}</{1}>
</configuration>", configValue_, configName_)
            };
            config.SetNode(".", null, xn.DocumentElement);
            config.Save();
            ConfigurationManagerHelper.DirtyConfiguration(configName_); 
        }
        public const string ThemeConfig = "MSDesktopTheme";
        public const string ShellModeConfig = "MSDesktopShellMode";
    }
}
