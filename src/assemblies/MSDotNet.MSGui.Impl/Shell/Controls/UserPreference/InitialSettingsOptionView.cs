/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Controls/UserPreference/InitialSettingsOptionView.cs#4 $
// $Change: 888544 $
// $DateTime: 2014/07/16 07:07:22 $
// $Author: caijin $

using System;
using System.Windows;
using System.Windows.Controls;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using System.Collections.Generic;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.UserPreference
{
    internal class InitialSettingsOptionView : IValidatableOptionView
    {
        private readonly InitialSettingsControl content;
        private readonly UserPreferenceInitialSettings data;
        internal InitialSettingsOptionView(bool shellModeConfigurable_, bool themeConfigurable_, IList<Bootstrapper.ThemeInfo> availableThemes_)
        {
            content = new InitialSettingsControl();
            if (shellModeConfigurable_)
            {
                content.cboShellModes.ItemsSource = UserPreferenceInitialSettings.AvailableShellModes;
            }
            if (themeConfigurable_)
            {
                content.cboTheme.ItemsSource = availableThemes_;
            }
            data = new UserPreferenceInitialSettings()
            {
                ShellModeConfigurable = shellModeConfigurable_,
                ThemeConfigurable = themeConfigurable_
            };
            content.DataContext = data;
            data.Load();
        }

        public void OnOK()
        {
            data.Save();
        }
        public bool OnValidate()
        {
            if (data.ThemeConfigurable && string.IsNullOrEmpty(data.Theme))
            {
                TaskDialog.ShowMessage("Please select a theme", "Invalid Settings",
                         TaskDialogCommonButtons.Close, VistaTaskDialogIcon.Warning,
                         Window.GetWindow(content));

                return false;
            }

            return true;
        }

        public void OnInvalid()
        {
            content.cboTheme.Highlight();
        }


        public void OnCancel()
        {
        }

        public void OnDisplay()
        {
        }

        public void OnHelp()
        {
        }

        public UIElement Content
        {
            get { return content; }
        }

    }
}
