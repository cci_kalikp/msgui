﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.ViewModels
{
	internal class WorkspaceViewModel : DockViewModel, IWindowManager
	{
		#region IWindowManager Members

	    public WorkspaceViewModel(IHidingLockingManager hidingLockingManager, IUnityContainer container) : base(hidingLockingManager, container)
	    {
	    }

	    public void AddNewView(WindowViewContainer viewContainer_, object additionalData_)
		{
			var wsfound = Workspaces.FirstOrDefault(ws => ws.ID == viewContainer_.ID);
			if (wsfound != null) return;

	        WindowViewModel windowViewModel = viewContainer_.GetViewModel(HidingLockingManager);
	        windowViewModel.InitialParameters = viewContainer_.Parameters;
	        windowViewModel.Host = additionalData_ as ShellTabViewModel;
            windowViewModel.DragOrDropOnContent +=
                (sender_, args_) => InvokeDragOrDropOnWindow(new GenericDragOrDropOnWindowEventArgs
                {
                    DragEventArgs = args_.EventArgs,
                    InternalEventType = args_.InternalEventType,
                    Window = windowViewModel.ViewContainer as IWindowViewContainer
                });


			Workspaces.Add(windowViewModel);
		}

		public void Clean()
		{
			Workspaces.Clear();
		}

		public XDocument SaveState(Func<WindowViewContainer, XElement> buildIViewXml_)
		{
			return new XDocument();		
		}

		public void BeforeSaveState()
		{

		}

		public void AfterSaveState()
		{

		}

		public void LoadState(XDocument layout_, PersistenceProfileService persistenceProfileService_, Action<XDocument, object> applyNodeAction_)
		{

		}

		public void BeforeLoadState()
		{
	
		}

		public void AfterLoadState()
		{
		
		}

        public void CancelLoad(bool cancelLayoutRestore_)
        {

        }


	    public event EventHandler<PaneDroppedAtScreenEdgeEventArgs> PaneDroppedAtScreenEdge;
	    public event EventHandler<WindowGroupDroppedOnScreenEdgeEventArgs> WindowGroupDroppedAtScreenEdge;

	    #endregion


         
    }
}
