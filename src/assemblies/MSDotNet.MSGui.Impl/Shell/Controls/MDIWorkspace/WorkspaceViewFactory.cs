﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions;
using System.Windows;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.MDIWorkspace
{
	internal class WorkspaceViewFactory : ContainerFactory
	{
		public MDIWorkspaceControl Target { get; internal set; }

		static WorkspaceViewFactory()
		{
			ContainerTypeProperty.OverrideMetadata(typeof(WorkspaceViewFactory), new FrameworkPropertyMetadata(typeof(WorkspaceChildWindow)));
		}

		public WorkspaceViewFactory()
		{

		}

		protected override void OnItemInserted(DependencyObject container_, object item_, int index_)
		{
			Target.Windows.Add(container_ as WorkspaceChildWindow);
		}

		protected override void OnItemMoved(DependencyObject container_, object item_, int oldIndex_, int newIndex_)
		{

		}

		protected override void OnItemRemoved(DependencyObject container_, object oldItem_)
		{

		}
        public override void ShowHideHeaderOnCCPs(Visibility visibility_)
        {
             
        }
	}
}
