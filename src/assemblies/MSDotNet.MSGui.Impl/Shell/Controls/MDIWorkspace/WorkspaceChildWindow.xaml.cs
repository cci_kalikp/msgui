﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.MDIWorkspace
{
	/// <summary>
	/// Interaction logic for WorkspaceChildWindow.xaml
	/// </summary>
	public partial class WorkspaceChildWindow : HeaderedContentControl
	{


		public Visibility HeaderVisibility
		{
			get { return (Visibility)GetValue(HeaderVisibilityProperty); }
			set { SetValue(HeaderVisibilityProperty, value); }
		}

		// Using a DependencyProperty as the backing store for HeaderVisibility.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty HeaderVisibilityProperty =
			DependencyProperty.Register("HeaderVisibility", typeof(Visibility), typeof(WorkspaceChildWindow), new UIPropertyMetadata(Visibility.Visible));

		

		

		public WorkspaceChildWindow()
		{
			InitializeComponent();
			this.DataContextChanged += new DependencyPropertyChangedEventHandler(WorkspaceChildWindow_DataContextChanged);
		}

		void WorkspaceChildWindow_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			if (e.OldValue != null)
			{
				BindingOperations.ClearBinding(this, HeaderedContentControl.HeaderProperty);
			}

			if (e.NewValue != null)
			{
				BindingOperations.SetBinding(this, HeaderedContentControl.HeaderProperty, new Binding() { Source = e.NewValue, Path = new PropertyPath("Header") });
			}
		}
	}
}
