﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows.Media.Media3D;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.MDIWorkspace
{
	/// <summary>
	/// Interaction logic for MDIWorkspace.xaml
	/// </summary>
	public partial class MDIWorkspaceControl : UserControl
	{
		private WorkspaceViewFactory factory;
		private bool isDragInProgress;
		private Point origCursorLocation;
private  double origHorizOffset;
private  double origVertOffset;
private  UIElement elementBeingDragged;
private bool modifyLeftOffset;
private bool modifyTopOffset;	
		public ObservableCollection<WorkspaceChildWindow> Windows { get; private set; }

		public MDIWorkspaceControl()
		{
			InitializeComponent();

			this.factory = new WorkspaceViewFactory() { Target = this };

			this.Windows = new ObservableCollection<WorkspaceChildWindow>();
			this.Windows.CollectionChanged += new NotifyCollectionChangedEventHandler(Windows_CollectionChanged);

			this.DataContextChanged += OnDataContextChanged;
		}

		void Windows_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
		{
		    switch (e.Action)
		    {
		        case NotifyCollectionChangedAction.Add:
					this.WorkspaceCanvas.Children.Add(e.NewItems[0] as UIElement);
		            break;
		    }
		}

		private void OnDataContextChanged(object sender_, DependencyPropertyChangedEventArgs e_)
		{ //Binding happens too late. We need this function to set properties as soon as DataContext has been changed
			//same in Floating only dock
			if (e_.NewValue is ILayoutSaveLoadRequester)
			{
				//controller.EventHost = e_.NewValue as ILayoutSaveLoadRequester;
			}
			if (e_.NewValue is DockViewModel)
			{
				DockViewModel vm = (DockViewModel)e_.NewValue;
				factory.ItemsSource = vm.Workspaces;
			}
		}


	}
}
