﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using Infragistics.Windows.DockManager;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.Converters
{
	public class FloatingWindowTitleMultiConverter : IMultiValueConverter
	{
		public virtual object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			if (values.Length < 2 || values[1] == null)
			{
				return null;
			}
			PaneToolWindow window = values[1] as PaneToolWindow;
			if (window == null)
			{
				return "";
			}
			if (window.Pane == null)
			{
				return window.Title;
			}
            if (ChromeManagerBase.LoadedWorkspaceStickyTitle)
            {
                CustomPaneToolWindow customPaneToolWindow = window as CustomPaneToolWindow;
                if (customPaneToolWindow != null && !string.IsNullOrEmpty(customPaneToolWindow.SubLayoutTitle))
                {
                    return customPaneToolWindow;
                }
            }
            ContentPane singlePane = ReflHelper.PropertyGet(window, "SinglePane") as ContentPane ?? CustomContentPane.GetActivePane(window);
			if (singlePane != null)
			{
				return singlePane.Header ?? "";
			} 

			return "";
		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
