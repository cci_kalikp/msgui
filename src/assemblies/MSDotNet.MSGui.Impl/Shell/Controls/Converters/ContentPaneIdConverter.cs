﻿using System;
using System.Globalization;
using System.Windows.Data;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.Converters
{
    internal class ContentPaneIdConverter : IValueConverter
    {
        private const string prefix = "contentPane";

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string str = value as string;
            return ConvertToPaneId(str);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string str = value as string;
            if (str != null && str.StartsWith(prefix))
            {
                return str.Substring(prefix.Length);
            }

            throw new InvalidOperationException("Unexpected name change");
        }

        public static string ConvertToPaneId(string str_)
        {
            return prefix + str_;
        }

        public static string GeneratePaneId()
        {
            return prefix + WindowViewContainer.GenerateNewID();
        }

        public static string ConverFromPaneId(string str_)
        {
            return str_.StartsWith(prefix) ? str_.Substring(11) : str_;
        }

    }
}
