﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.Converters
{
  internal class VisibilityConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value is bool)
      {
        bool isVisible = (bool) value;
        if (parameter != null)
        {
          return isVisible ? Visibility.Collapsed : Visibility.Visible;
        }
        return isVisible ? Visibility.Visible : Visibility.Collapsed;
      }

      if (value is int)
      {
        if (parameter != null)
        {
          return (int)value != 0 ? Visibility.Collapsed : Visibility.Visible;
        }
        return (int)value != 0 ? Visibility.Visible : Visibility.Collapsed;
      }
      return null;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      return (value is Visibility && (Visibility)value == Visibility.Visible) ^ (parameter != null);
    }
  }
}
