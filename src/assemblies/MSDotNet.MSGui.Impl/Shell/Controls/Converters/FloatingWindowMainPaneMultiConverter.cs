﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Controls/Converters/FloatingWindowMainPaneMultiConverter.cs#20 $
// $Change: 896821 $
// $DateTime: 2014/09/15 02:20:20 $
// $Author: caijin $

using System;
using System.Windows;
using System.Windows.Data;
using Infragistics.Windows.Controls;
using Infragistics.Windows.DockManager;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager; 

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.Converters
{
    /// <summary>
    /// How to test: turn on EnableLoadedWorkspaceStickyTitle, and then load a saved sub layout see if the title is always the layout name, regardless the current selected pane
    /// </summary>
    public class FloatingWindowMainPaneMultiConverter : IMultiValueConverter
    {
        public virtual object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values.Length < 2 || values[1] == null)
            {
                return null;
            }

            PaneToolWindow window = values[1] as PaneToolWindow;

            if (window == null || window.Pane == null)
            {
                return null;
            }

            //create empty ContentPane when ActivePane is null, instead of using SinglePane even if it's not null.
            //this would only occur when XamDockManager.LoadLayout for the last window loaded
            //so later on the ContentPane would be reset ActivePane is set
            //if we return SinglePane this time, later on when ActivePane is set, since SinglePane = ActivePane, the PaneHeaderControl.ContentPane changed event won't be triggered
            var singlePane = CustomContentPane.GetActivePane(window);
            if (singlePane != null)
            {
                if (FloatingWindowUtil.SeparateHeaderItemsInFloatingWindow &&
                    ReflHelper.PropertyGet(window, "SinglePane") == null)
                {
                    singlePane = new ContentPane() { Header = singlePane.Header, Image = singlePane.Image };
                }
            }
            else
            {
                singlePane = new ContentPane();
            }
            if (!ChromeManagerBase.LoadedWorkspaceStickyTitle) return singlePane;

            var toolWindow = ToolWindow.GetToolWindow(window) as CustomPaneToolWindow;
            if (toolWindow == null) return singlePane;

            string title = toolWindow.SubLayoutTitle;
            if (title != null)
            {
                toolWindow.Title = title;
                ContentPane pane = new ContentPane()
                {
                    Header = title,
                    Image = singlePane.Image
                };
                if (!FloatingWindowUtil.SeparateHeaderItemsInFloatingWindow)
                {
                    Attached.SetViewContainer(pane, Attached.GetViewContainer(singlePane));
                }
                return pane;
            }

            return singlePane;

        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
