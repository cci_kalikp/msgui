﻿using System;
using System.Windows.Data;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.Converters
{
  public class NonEmptyStringToBoolConverter : IValueConverter
  {
    #region IValueConverter Members

    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      string str = value as string;
      if (!String.IsNullOrEmpty(str))
      {
        return true;
      }
      return false;
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      throw new NotImplementedException();
    }

    #endregion
  }
}
