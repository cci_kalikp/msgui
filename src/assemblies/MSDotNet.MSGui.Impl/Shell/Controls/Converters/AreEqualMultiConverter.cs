﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.Converters
{
  public class AreEqualMultiConverter : IMultiValueConverter
  {
    public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
    {
      if (values.Length == 0)
      {
        return "true";
      }
      if (values.All(a => a != null && a.Equals(values[0])))
      {
        return "true";
      }
      return "false";
    }

    public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
