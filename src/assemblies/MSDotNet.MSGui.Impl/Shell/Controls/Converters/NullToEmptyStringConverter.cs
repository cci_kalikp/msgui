﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.Converters
{
  internal class NullToEmptyStringConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {      
      return value ?? " ";
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      return value;
    }
  }
}
