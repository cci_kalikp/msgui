/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/ShellWindow.xaml.cs#166 $
// $Change: 902340 $
// $DateTime: 2014/10/24 07:16:48 $
// $Author: caijin $

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using System.Xml.Linq;
using Infragistics.Windows.Controls;
using Infragistics.Windows.DockManager;
using Infragistics.Windows.Ribbon;
using Infragistics.Windows.Ribbon.Events;
using Microsoft.Practices.Unity;
using Microsoft.WindowsAPICodePack.Shell.PropertySystem;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using MorganStanley.MSDotNet.MSGui.Controls.Controls;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers.ShellMenu;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Menu;
using MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Helpers;
using MorganStanley.MSDotNet.MSGui.Impl.ModernTheme;
using MorganStanley.MSDotNet.MSGui.Impl.Options;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.LauncherBar;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.MDIWorkspace;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.ModernBackstage;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.ModernBackstage.CommonPages.Dashboard;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.ViewModels;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions; 
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Themes;
using MorganStanley.MSDotNet.MSGui.Impl.TestSupport;
using MorganStanley.MSDotNet.My;
using CustomXamTabControl = MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.CustomXamTabControl;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell
{
    /// <summary>
    /// Interaction logic for ShellWindow.xaml
    /// </summary>
    internal partial class ShellWindow : INotifyPropertyChanged, IShell, IShellWindowMenuFacade
    {
        private static readonly IMSLogger Logger = MSLoggerFactory.CreateLogger<ShellWindow>();
        private static IUnityContainer _container;
         
        internal static Thickness? StatusBarPadding;

        public static bool StatusBarContextMenuEnabled = false; 

        private readonly object _isLoadingOrSaving = new object();
        private readonly Application.Application _application;
        private readonly MDIWorkspaceControl _mdiWorkspace;
        private readonly TileItemCollectionModel _tileItemCollectionModel;
        private readonly SkipSaveProfileOnExitCondition _skipSaveProfileOnExitCondition;
        private readonly InitialSettings _initialSettings;
        private string _titleBase;
        private Func<bool> _tryCloseConcordMainWindow;
        private PersistenceProfileService _persistenceProfileService;
        private IProfileMonitor _profileMonitor;
        private LayoutLoadStrategy _layoutLoadStrategy;
        private LayoutLoadStrategyCallback _layoutLoadStrategyCallback;
        private RecentItemsManager _recentItemsManager;

        private bool _shown;
        private bool _themesDisabled;

        private Backstage _backstage;
        private BackstageViewModel _backstageViewModel;
        private DashboardSectionViewModel _backstageDashboardSectionViewModel;
        private LayoutSectionViewModel _backstageLayoutsSection;
        private readonly ItemsControl _backstageApplicationMenu;
        private RoutedCommand closeBackstageCommand = new RoutedUICommand("CloseBackstageCommand", "CloseBackstageCommand", 
            typeof(ShellWindow), new InputGestureCollection { new KeyGesture(Key.Escape) });


        // DON'T rename these variables: there are dependencies in reflection in other DLLs

        private readonly IWindowManager windowManager;
        private readonly FloatingOnlyDock floatingOnlyDock;
        private readonly TabbedDock tabbedDock;

        public event Action ShellShown;
        private MenuRootArea menuRootArea;

        public ShellWindow(IUnityContainer container, InitialSettings initialSettings/*, KeyGesture gesture_, bool enableSoftwareRendering_, bool disableEnforcedSizeRestrictions_, bool enableNonOwnedWindows_ = false*/)
        {
            Closing += ShellWindow_Closing;
            _container = container;
            container.RegisterInstance<IShellWindowMenuFacade>(this);
            _application = container.Resolve<IApplication>() as Application.Application;
            if (_application == null)
            {
                throw new InvalidOperationException("Application must be IApplication");
            }
            _initialSettings = initialSettings;
            _skipSaveProfileOnExitCondition = initialSettings.SkipSaveProfileOnExitCondition;
            if (initialSettings.EnableSoftwareRendering)
            {
                var hwndSource = PresentationSource.FromVisual(this) as HwndSource;
                if (hwndSource != null)
                {
                    hwndSource.CompositionTarget.RenderMode = RenderMode.SoftwareOnly;
                }
            }
            if (ShellModeExtension.MenuPopupAnimationDisabled)
            {
                Resources.Add(SystemParameters.MenuPopupAnimationKey, PopupAnimation.None); 
            }

            InitializeComponent(); 
            TitleBar = new TitleBarPlaceHolder() { DataContext = this };
            BindingOperations.SetBinding(TitleBar, TitleBarPlaceHolder.RibbonVisibilityProperty, 
                new Binding() { Path = new PropertyPath("Ribbon.Visibility"), Source = this });


            if (initialSettings.ShellMode == ShellMode.RibbonWindow || initialSettings.ShellMode == ShellMode.External)
            {
                tabbedDock = new TabbedDock();
                MainDockArea.Children.Add(tabbedDock);
                Grid.SetColumn(tabbedDock, 1);
                Grid.SetRow(tabbedDock, 1);

                var model = new TabbedDockViewModel(container.Resolve<IChromeRegistry>() as ChromeRegistry,
                                                                    container,
                                                                    container.Resolve<IHidingLockingManager>())
                {
                    EnforceSizeRestrictions = !initialSettings.DisableEnforcedSizeRestrictions,
                    WindowRenameGesture = initialSettings.WindowRenameGesture,
                    UseOwnedWindows = !initialSettings.EnableNonOwnedWindows,
                    MainWindow = this
                };
                windowManager = model;

                model.TabGroupPaneSelectedIndexStoreEvent += model_TabGroupPaneSelectedIndexStoreEvent;
                model.TabGroupPaneSelectedIndexCleanupEvent += model_TabGroupPaneSelectedIndexCleanupEvent;
                this.tabbedDock.DataContext = model;
            }
            else if (initialSettings.ShellMode == ShellMode.RibbonAndFloatingWindows)
            {
                MainDockArea.Visibility = Visibility.Collapsed;

                const int height = 185;
                MinHeight = height;
                MaxHeight = height;
                Height = height;
                StatusBar.SizeChanged += (s, e) => HandleStatusBarSizeChange(e.NewSize.Height);
                 

                var floatingOnlyDockModel = new FloatingOnlyDockViewModel(container.Resolve<IHidingLockingManager>(), container)
                {
                    WindowRenameGesture = initialSettings.WindowRenameGesture,
                    EnforceSizeRestrictions = !initialSettings.DisableEnforcedSizeRestrictions,
                    UseOwnedWindows = !initialSettings.EnableNonOwnedWindows,
                    MainWindow = this,
                };

                if (DockViewModel.UseHarmonia)
                {
                    HarmoniaFloatingOnlyDock.Bind(floatingOnlyDockModel); 
                }
                else
                {
                    floatingOnlyDock = new FloatingOnlyDock
                    {
                        DataContext = floatingOnlyDockModel
                    };
                    this.StateChanged += (sender_, args_) =>
                        {
                            floatingOnlyDock.WindowState = this.WindowState;
                        };
                }

                windowManager = floatingOnlyDockModel;
            }
            else // if (initialSettings_.ShellMode == ShellMode.RibbonMDI)
            {
                this._mdiWorkspace = new MDIWorkspaceControl();
                this.MainDockArea.Children.Add(_mdiWorkspace);

                var model = new WorkspaceViewModel(container.Resolve<IHidingLockingManager>(), container)
                {

                };
                this._mdiWorkspace.DataContext = model;
                windowManager = model;

            }
            _tileItemCollectionModel =
                new TileItemCollectionModel(container.Resolve<IChromeRegistry>() as ChromeRegistry,
                                            container,
                                            container.Resolve<IHidingLockingManager>(), windowManager, initialSettings);
            _titleBase = string.Format("{0} - (v{1})", _application.Name, _application.Version);
            Title = _titleBase;



            if (_application.Icon != null)
            {
                Icon = _application.Icon;
            }
            Loaded += ShellWindow_Loaded;
            HeaderVisibility = Visibility.Visible;
            this.ShowActivated = ChromeManagerBase.AcquireFocusAfterStartup;

            statusBar.Height = ThemeExtensions.StatusBarHeight;
            XamRibbonWindowExtensions.SetStatusBarPadding(this, StatusBarPadding);

            _container.Resolve<IHidingLockingManager>().HideUIGranularityChanged += OnHideUIGranularityChanged;
            OnHideUIGranularityChanged(null, null);

            if (StatusBarContextMenuEnabled)
            {
                container.Resolve<IPersistenceService>().AddPersistor("StatusBarContextMenuPersistor",
                    state =>
                    {
                        foreach (var item in state.Descendants("Item"))
                        {
                            var name = item.Attribute("Name").Value;
                            var visible = bool.Parse(item.Attribute("Visible").Value);
                            var element = statusBar.ContextMenu.Items.Cast<HidableStatusBarItemBase>().FirstOrDefault(it =>
                                                                                                        it.Name ==
                                                                                                        name);
                            if (element == null) continue;
                            element.Visible = visible;
                        }
                    },
                    () =>
                    {
                        var document = new XDocument();
                        var root = new XElement("Items");
                        foreach (var item in statusBar.ContextMenu.Items)
                        {
                            var el = new XElement("Item");
                            el.SetAttributeValue("Name", ((HidableStatusBarItemBase)item).Name);
                            el.SetAttributeValue("Visible", ((HidableStatusBarItemBase)item).Visible);
                            root.Add(el);
                        }
                        document.Add(root);
                        return document;
                    });
            }
            #region Modern theme 
            this._backstageApplicationMenu = new ItemsControl(); 
            _container.Resolve<IChromeRegistry>().RegisterWidgetFactory(MSDesktopButtonFactoryID, MSDesktopButtonFactory);
            //if (ModernThemeHelper.IsModernThemeEnabled)
            //{
            //    SetModernTheme();
            //}
            _application.ThemeChanged += _application_OnThemeChanged;
            #endregion

            this.EnableMinimizeScreenshotSupport();
        }

        #region Modern theme support
        private ResourceDictionary modernThemeRibbonOverrideResource; 
        private XamTabControl ribbonTabControl;
        private FrameworkElement orbPanel;
        private void SetModernTheme()
        {
            this.ribbon.RibbonTabItemSelected -= RibbonOnRibbonTabItemSelected_BackstageCloseHandler;
            this.ribbon.RibbonTabItemSelected += RibbonOnRibbonTabItemSelected_BackstageCloseHandler;
            this.closeBackstageCommandBinding = new CommandBinding(closeBackstageCommand,
                (sender, args) => HideBackstage());

            if (modernThemeRibbonOverrideResource != null)
            {
                this.Resources.MergedDictionaries.Remove(modernThemeRibbonOverrideResource);
                this.ribbon.Resources.MergedDictionaries.Remove(modernThemeRibbonOverrideResource);
                this._contentHost.Resources.MergedDictionaries.Remove(modernThemeRibbonOverrideResource);
                this.MainDockArea.Resources.MergedDictionaries.Remove(modernThemeRibbonOverrideResource); 
            }
            modernThemeRibbonOverrideResource = new ResourceDictionary
            {
                Source = new Uri( "pack://application:,,,/MSDotNet.MSGui.Themes;component/MSDesktop2014/RibbonOverrides.xaml")
            };

            this.Resources.MergedDictionaries.Add(modernThemeRibbonOverrideResource);
            this.ribbon.Resources.MergedDictionaries.Add(modernThemeRibbonOverrideResource);
            this._contentHost.Resources.MergedDictionaries.Add(modernThemeRibbonOverrideResource);
            this.MainDockArea.Resources.MergedDictionaries.Add(modernThemeRibbonOverrideResource);

            this.ribbon.ApplicationMenuMode = ApplicationMenuMode.Office2010;
            this.ribbon.ApplicationAccentColor = ModernThemeHelper.AccentColor;

            BackstageItemCommand.CloseBackstageAction = this.HideBackstage;

            if (!this.IsLoaded)
            {
                this.Loaded += (s, e) => UpdateControlsForModernTheme(false); 
            }
            else
            {
                UpdateControlsForModernTheme(true);
            }
             
            var tbph = TitleBar as TitleBarPlaceHolder;
            if (tbph != null)
                tbph.SetResourceReference(ForegroundProperty, RibbonBrushKeys.CaptionActiveTextForegroundKey);

            //copy menu items that are not classic specific to modern theme
            if (appmenu != null && appmenu.Items.Count > 0)
            {
                menuRootArea.ObservableChildren.CollectionChanged -= ApplicationMenuItemCollectionChanged;
                List<IWidgetViewContainer> children = new List<IWidgetViewContainer>(menuRootArea.Children);
                foreach (var child in children)
                {
                    child.ClearParent();
                }
                appmenu.Items.Clear();
                menuRootArea.Content = _backstageApplicationMenu;
                foreach (var child in children)
                {
                    if (!classicThemeOnlyItems.Contains(child))
                    {
                        menuRootArea.Add(child);
                    }
                }
                menuRootArea.ObservableChildren.CollectionChanged += ApplicationMenuItemCollectionChanged;

            } 
             
        }
        private void UpdateControlsForModernTheme(bool ribbonInitialized)
        {
            this.screenshotProvider = _container.Resolve<LayoutScreenshotProvider>();
            if (_backstage != null)
            {
                this.BackstageGrid.Children.Remove(_backstage);
                _backstage = null;
            }
            if (_backstageViewModel == null)
            {
                this._backstageViewModel = _container.Resolve<BackstageViewModel>(); 
                this._backstageDashboardSectionViewModel = new DashboardSectionViewModel(_container, this._application);
                this._backstageViewModel.Sections.Add(_backstageDashboardSectionViewModel);
                this._backstageLayoutsSection = new LayoutSectionViewModel(this._profileMonitor, this, screenshotProvider);
                this._backstageViewModel.Sections.Add(this._backstageLayoutsSection); 
            } 


            this._backstage = _container.Resolve<Backstage>(); 
            _backstage.DataContext = this._backstageViewModel;
            _backstage.HorizontalAlignment = HorizontalAlignment.Stretch;
            _backstage.VerticalAlignment = VerticalAlignment.Stretch; 
            this.BackstageGrid.Children.Add(_backstage);
            this._backstage.ApplicationMenu = this._backstageApplicationMenu;
            
            if (ShellModeExtension.HideUIGranularity.HasFlag(HideLockUIGranularity.HideRibbon))
            {
                _container.Resolve<IChromeManager>().Tabwell.LeftEdge.RemoveWidget(MSDesktopButtonFactoryID);
                _container.Resolve<IChromeManager>().Tabwell.LeftEdge.AddWidget(MSDesktopButtonFactoryID, new InitialWidgetParameters());
                TopBackstagePadding.Height = 57;
            }
            else
            {
                if (!ribbonInitialized)
                {
                    FixRibbonForModernTheme();
                }
                else
                {
                    EventHandler layoutUpdated = null;
                    ribbon.LayoutUpdated += layoutUpdated = (sender_, args_) =>
                        {
                            if (FixRibbonForModernTheme())
                            {
                                ribbon.LayoutUpdated -= layoutUpdated; 
                            } 
                        };
                }
            } 
            this.m_chromeManager.LayoutLoaded -= new EventHandler(m_chromeManager_LayoutLoaded);
            this.m_chromeManager.LayoutLoaded += new EventHandler(m_chromeManager_LayoutLoaded);

           

        }
        private bool FixRibbonForModernTheme()
        {
            ribbonTabControl = FindChild(this.ribbon, item => item is XamTabControl) as XamTabControl;
            if (ribbonTabControl == null) return false;

            //hack to hide IG backstage button 
            orbPanel = FindChild(this.ribbon, item => "orbPanel".Equals(item.GetValue(NameProperty))) as FrameworkElement;
            if (orbPanel != null)
            {
                orbPanel.SetValue(VisibilityProperty, Visibility.Collapsed);
            }

            if (ribbonTabControl != null && ribbonTabControl.IsVisible)
            {
                ribbonTabControl.PreTabItemContentTemplate = null;

                if (this.msdesktopButton == null)
                {
                    this.msdesktopButton = this.GetMSDesktopButton();
                }
                //we only do this if we don't create it in any other place (ex: when there is no ribbon, we add the button to the main tabwell)
                ribbonTabControl.PreTabItemContent = msdesktopButton;
            }
             
            this.ribbonContentPanel = FindChild(ribbon, o =>
            {
                var fe = o as FrameworkElement;

                if (fe == null)
                    return false;

                return fe.Name == "ContentPanel" && fe is Border;
            }) as Border;

            this.backstageHelpers_RibbonSize = FindChild(this, o =>
            {
                var fe = o as FrameworkElement;

                if (fe == null)
                    return false;

                return fe.Name == "ribbonSize" && fe is DesiredSizeDecorator;
            }) as DesiredSizeDecorator;

            this.backstageHelpers_TabControl = FindChild(this.tabbedDock, o =>
            {
                var fe = o as FrameworkElement;

                if (fe == null)
                    return false;

                return fe.Name == "tabControl" && fe is CustomXamTabControl;
            }) as CustomXamTabControl; 
            return true;
        }
        void m_chromeManager_LayoutLoaded(object sender, EventArgs e)
        {
            HideBackstage();
        }
        private void SetClassicTheme()
        {
            HideBackstage(); 
            if (this.IsLoaded)
            {
                _container.Resolve<IChromeManager>().Tabwell.LeftEdge.RemoveWidget(MSDesktopButtonFactoryID);         
            }

            if (modernThemeRibbonOverrideResource != null)
            { 
                this.Resources.MergedDictionaries.Remove(modernThemeRibbonOverrideResource);
                this.ribbon.Resources.MergedDictionaries.Remove(modernThemeRibbonOverrideResource);
                this._contentHost.Resources.MergedDictionaries.Remove(modernThemeRibbonOverrideResource);
                this.MainDockArea.Resources.MergedDictionaries.Remove(modernThemeRibbonOverrideResource);
                modernThemeRibbonOverrideResource = null;
            }
            if (_backstageViewModel != null)
            {
                _backstageViewModel.Options.SelectedSecondLevelItem = null;
            }
            ribbon.ClearValue(XamRibbon.ApplicationMenuModeProperty);
            ribbon.ClearValue(XamRibbon.ApplicationAccentColorProperty);
            if (_backstage != null)
            {
                this.BackstageGrid.Children.Remove(_backstage);
                _backstage = null;
            }
            this.m_chromeManager.LayoutLoaded -= new EventHandler(m_chromeManager_LayoutLoaded);
            if (orbPanel != null)
            {
                orbPanel.ClearValue(VisibilityProperty);
                orbPanel = null;
            } 
            if (ribbonTabControl != null)
            {
                ribbonTabControl.ClearValue(XamTabControl.PreTabItemContentTemplateProperty);  
                //we only do this if we don't create it in any other place (ex: when there is no ribbon, we add the button to the main tabwell)
                ribbonTabControl.ClearValue(XamTabControl.PreTabItemContentProperty);
                ribbonTabControl = null;
            }
            var tbph = TitleBar as TitleBarPlaceHolder;
            if (tbph != null) 
                tbph.ClearValue(ForegroundProperty);

            menuRootArea.ObservableChildren.CollectionChanged -= ApplicationMenuItemCollectionChanged; 
            //copy menu items added in modern theme and those classic specific menus to classic theme
            if (_backstageApplicationMenu != null && _backstageApplicationMenu.Items.Count > 0)
            {
                List<IWidgetViewContainer> children = new List<IWidgetViewContainer>(menuRootArea.Children);
                foreach (var child in children)
                {
                    child.ClearParent();
                }
                menuRootArea.Content = appmenu;

                if (classicThemeOnlyItems.Count == 0)
                {
                    CreateClassicThemeSpecificItems();
                }
                else
                {
                    foreach (var classicThemeOnlyItem in classicThemeOnlyItems)
                    {
                        menuRootArea.Add(classicThemeOnlyItem);
                    }
                }
                foreach (var child in children)
                {
                    menuRootArea.Add(child);
                }
                
                if (!menuRootArea.Contains(ShellMenuExtensions.ApplicationMenuFactories.ExitButtonWithSeparator)) return;
                var exitItem = menuRootArea[ShellMenuExtensions.ApplicationMenuFactories.ExitButtonWithSeparator];
                if (exitItem == null) return;
                exitItem.ClearParent();
                if (!menuRootArea.Contains(ShellMenuExtensions.ApplicationMenuFactories.ExitButtonSeparator)) 
                {
                    menuRootArea.AddWidget(ShellMenuExtensions.ApplicationMenuFactories.ExitButtonSeparator, new InitialDropdownSeparatorParameters());
 
                }
                else
                {
                    var exitButtonSeperator = menuRootArea[ShellMenuExtensions.ApplicationMenuFactories.ExitButtonSeparator];
                    exitButtonSeperator.ClearParent();
                    menuRootArea.Add(exitButtonSeperator);  
                }
                menuRootArea.Add(exitItem);  
                _backstageApplicationMenu.Items.Clear();
            }
            menuRootArea.ObservableChildren.CollectionChanged += ApplicationMenuItemCollectionChanged;

        }
         
       
        void _application_OnThemeChanged(object sender, EventArgs e)
        {
            if (msdesktopButton != null) //modern theme
            {
                msdesktopButton.Click -= msdesktopButton_Click; 
            }
            msdesktopButton = null; 
            if (ModernThemeHelper.IsModernThemeEnabled)
            {
                SetModernTheme();
            }
            else
            {
                SetClassicTheme();
            }
        }
        #endregion

        #region MSDesktop button for modern theme

        private ToggleButton msdesktopButton;
        internal const string MSDesktopButtonFactoryID = "MSDesktopButtonFactory_F1068C34D2674C27AD551D7BA7E99BC3";

        public bool MSDesktopButtonFactory(IWidgetViewContainer vc, XDocument state)
        {
            if (msdesktopButton == null)
            {
                this.msdesktopButton = this.GetMSDesktopButton();
            }
            vc.Content = msdesktopButton;
            return true;
        }

        private ToggleButton GetMSDesktopButton()
        {
            var msdesktopButton = new ToggleButton
            {
                Content = "MSDesktop"
            };
            msdesktopButton.SetResourceReference(Control.TemplateProperty, "MSDesktopBrandingButtonTemplate"); 
            msdesktopButton.Background = new SolidColorBrush(ModernThemeHelper.AccentColor);

            msdesktopButton.Click += msdesktopButton_Click;

            return msdesktopButton;
        }

        void msdesktopButton_Click(object sender, RoutedEventArgs e)
        {
            ToggleBackstage();
        }

        private void ToggleBackstage()
        {
            if (BackstageGrid.IsVisible)
                HideBackstage();
            else
                ShowBackstage();
        }

        /// <summary>
        /// Holds the selected tab item while the backstage is active.
        /// </summary>
        private RibbonTabItem backstageRibbonTabItemBuffer = null;

        private void ShowBackstage()
        {
            if (this._backstage != null && this._backstageViewModel != null)
            {
                this._backstageViewModel.IsVisible = true;
                this._backstageViewModel.CurrentSection = this._backstageDashboardSectionViewModel;
                this._backstageLayoutsSection.SelectedItem =
                    this._backstageLayoutsSection.Items.FirstOrDefault(
                        layoutItem => (layoutItem as LayoutItemViewModel).IsCurrent);
            }

            BackstageWrapper.Visibility = Visibility.Visible;

            if (this.tabbedDock != null)
            {
                tabbedDock.dockManager.Visibility = Visibility.Hidden;
            }

            if (this.ribbon.IsVisible)
            {
                backstageRibbonTabItemBuffer = this.ribbon.SelectedTab;
                this.ribbon.RibbonTabItemSelected -= RibbonOnRibbonTabItemSelected_BackstageCloseHandler;
                this.ribbon.SelectedTab = null;
                this.ribbon.RibbonTabItemSelected += RibbonOnRibbonTabItemSelected_BackstageCloseHandler;
                TopBackstagePadding.Height = ribbon.ActualHeight - ribbonContentPanel.ActualHeight + 1;
            }
            else if (!this.ribbon.IsVisible)
            {
                var tC = FindChild(this, item => "tabControl".Equals(item.GetValue(NameProperty))) as CustomXamTabControl;
                if (tC != null)
                {
                    TopBackstagePadding.Height = (int)(this.ActualHeight - MainDockArea.ActualHeight);
                }
            }
            BottomBackstagePadding.Height = this.statusBar.IsVisible ? this.statusBar.ActualHeight : 0;

            this.CommandBindings.Add(closeBackstageCommandBinding);
        }

        private void RibbonOnRibbonTabItemSelected_BackstageCloseHandler(object sender, RibbonTabItemSelectedEventArgs ribbonTabItemSelectedEventArgs)
        {
            backstageRibbonTabItemBuffer = null;
            HideBackstage();
        }

        private void HideBackstage()
        {
            if (this._backstage != null && this._backstageViewModel != null)
            {
                this._backstageViewModel.IsVisible = false;
            }

            BackstageWrapper.Visibility = Visibility.Collapsed;
            TopBackstagePadding.IsHitTestVisible = false;
            BottomBackstagePadding.IsHitTestVisible = false;
            if (msdesktopButton != null)
            { 
                this.msdesktopButton.IsChecked = false;
            }

            if (this.tabbedDock != null)
            {
                tabbedDock.dockManager.Visibility = Visibility.Visible;
            }

            if (this.ribbon.IsVisible && backstageRibbonTabItemBuffer != null)
            {
                this.ribbon.SelectedTab = backstageRibbonTabItemBuffer;
            }
            this.ribbon.RibbonTabItemSelected -= RibbonOnRibbonTabItemSelected_BackstageCloseHandler;
            if (closeBackstageCommandBinding != null)
            {
                this.CommandBindings.Remove(closeBackstageCommandBinding);          
            }
        }

        #endregion

        private DependencyObject FindChild(DependencyObject root, Func<DependencyObject, bool> predicate)
        { 
            if (root == null)
                return null;

            if (predicate(root))
                return root;

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(root); i++)
            {
                var result = FindChild(VisualTreeHelper.GetChild(root, i), predicate);
                if (result != null)
                    return result;
            }

            return null;
        }

        private void HandleStatusBarSizeChange(double newHeight)
        {
            var height = 160 + newHeight;
            MinHeight = height;
            MaxHeight = height;
            Height = height;
        }

        private void NoTaskbarPin()
        {
            this.Dispatcher.Invoke(() =>
                                   WindowProperties.SetWindowProperty(this,
                                                                      SystemProperties.System.AppUserModel
                                                                                      .PreventPinning, "1"));
        }

        void model_TabGroupPaneSelectedIndexCleanupEvent(object sender, EventArgs e)
        {
            if (tabbedDock != null)
            {
                var set = new HashSet<TabGroupPane>();
                foreach (
                    var tgp in
                        tabbedDock.dockManager.GetPanes(PaneNavigationOrder.VisibleOrder)
                                   .Where(contentPane => contentPane.Visibility == Visibility.Visible)
                                   .Select(contentPane => contentPane.TryFindParent<TabGroupPane>())
                                   .Where(tgp => tgp != null))
                {
                    set.Add(tgp);
                }

                foreach (var tabGroupPane in set)
                {
                    if (tgpsi.ContainsKey(tabGroupPane.Name))
                    {
                        try
                        {
                            tabGroupPane.SelectedIndex = tgpsi[tabGroupPane.Name];
                        }
                        catch (Exception exc)
                        {
                            Logger.Warning(
                                string.Format("Invalid tabgrouppane {0} : {1}", tabGroupPane.Name,
                                              tgpsi[tabGroupPane.Name]), exc);
                        }

                    }
                }
            }
        }

        private Dictionary<string, int> tgpsi = new Dictionary<string, int>();

        void model_TabGroupPaneSelectedIndexStoreEvent(object sender, EventArgs e)
        {
            if (tabbedDock != null)
            {
                var set = new HashSet<TabGroupPane>();
                foreach (
                    var tgp in
                        tabbedDock.dockManager.GetPanes(PaneNavigationOrder.VisibleOrder)
                                   .Where(contentPane => contentPane.Visibility == Visibility.Visible)
                                   .Select(contentPane => contentPane.TryFindParent<TabGroupPane>())
                                   .Where(tgp => tgp != null))
                {
                    set.Add(tgp);
                }

                foreach (var tabGroupPane in set)
                {
                    tgpsi[tabGroupPane.Name] = tabGroupPane.SelectedIndex;
                }
            }
        }

        private void OnHideUIGranularityChanged(object sender_, EventArgs eventArgs_)
        {
            if (_container.Resolve<IHidingLockingManager>()
                           .Granularity
                           .HasFlag(HideLockUIGranularity.LockRelativeWindowsPositions))
            {
                _previousLocation = new Point(Left, Top);
                LocationChanged += OnWindowLocationChanged;
            }
            else
            {
                LocationChanged -= OnWindowLocationChanged;
            }
        }

        public void ToggleOverlay(bool enable_, Brush brush_ = null, Func<FrameworkElement> controlFactory_ = null)
        {
            if (enable_ && brush_ != null)
            {
                var adornerLayer = AdornerLayer.GetAdornerLayer(_contentHost);
                var titlebarHeight =
                    ((RibbonCaptionPanel)ribbon.Template.FindName("PART_RibbonCaptionPanel", ribbon)).ActualHeight;
                m_overlayAdorner = new OverlayAdorner(_contentHost, titlebarHeight) { OverlayBrush = brush_ };
                if (controlFactory_ != null) m_overlayAdorner.Child = controlFactory_.Invoke();
                adornerLayer.Add(m_overlayAdorner);
            }
            else
            {
                var adornerLayer = AdornerLayer.GetAdornerLayer(_contentHost);
                if (m_overlayAdorner != null)
                {
                    adornerLayer.Remove(m_overlayAdorner);
                    m_overlayAdorner = null;
                }
            }
        }

        public bool IsShown
        {
            get { return _shown; }
        }

        public string CurrentProfile
        {
            get
            {
                if (_profileMonitor == null || _profileMonitor.CurrentProfile == null)
                {
                    return string.Empty;
                }
                return _profileMonitor.CurrentProfile;
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion INotifyPropertyChanged Members

        #region IPersistable Members

        public void LoadState(XDocument state_)
        {
            try
            {
                lock (_isLoadingOrSaving)
                {
                    XElement mainElement = state_.Root;
                    if (mainElement != null && mainElement.Name == "MainWindow")
                    {
                        XAttribute widthAttr = mainElement.Attribute("Width");
                        double width;
                        if (widthAttr != null && widthAttr.Value != null &&
                            Double.TryParse(widthAttr.Value, NumberStyles.Any, CultureInfo.InvariantCulture, out width))
                        {
                            Width = width;
                        }

                        XAttribute heightAttr = mainElement.Attribute("Height");
                        double height;
                        if (heightAttr != null && heightAttr.Value != null &&
                            Double.TryParse(heightAttr.Value, NumberStyles.Any, CultureInfo.InvariantCulture, out height))
                        {
                            Height = height;
                        }

                        XAttribute topAttr = mainElement.Attribute("Top");
                        double top;
                        if (topAttr != null && topAttr.Value != null &&
                            Double.TryParse(topAttr.Value, NumberStyles.Any, CultureInfo.InvariantCulture, out top))
                        {
                            Top = top;
                        }

                        XAttribute leftAttr = mainElement.Attribute("Left");
                        double left;
                        if (leftAttr != null && leftAttr.Value != null &&
                            Double.TryParse(leftAttr.Value, NumberStyles.Any, CultureInfo.InvariantCulture, out left))
                        {
                            Left = left;
                        }

                        XAttribute themeAttr = mainElement.Attribute("Theme");
                        if (themeAttr != null && themeAttr.Value != null && !_themesDisabled)
                        {
                            SetTheme(themeAttr.Value);
                        }

                        XAttribute minattr = mainElement.Attribute("WindowState");
                        if (minattr != null && minattr.Value != null)
                        {
                            switch (minattr.Value.ToLower())
                            {
                                case "maximized":
                                    WindowState = WindowState.Maximized;
                                    break;
                                case "minimized":
                                    WindowState = WindowState.Minimized;
                                    break;
                                default:
                                    WindowState = WindowState.Normal;
                                    break;
                            }
                            //WindowState windowState = WindowState.Normal;          |
                            ////Enum.TryParse(minattr.Value, true, out windowState); |-- .NET4-specific code
                            //WindowState = windowState;                             |
                        }
                        if (!TabbedDock.DisableHeaderTogglePersistor)
                        {
                            XAttribute headertoggleattr = mainElement.Attribute("HeaderToggle");
                            if (headertoggleattr != null && headertoggleattr.Value != null)
                            {
                                switch (headertoggleattr.Value.ToLower())
                                {
                                    case "visible":
                                        HeaderVisibility = Visibility.Visible;
                                        Dispatcher.BeginInvoke((Action) (() => ShowHideHeaderOnCCPs(HeaderVisibility)));
                                        break;
                                    case "collapsed":
                                        HeaderVisibility = Visibility.Collapsed;
                                        Dispatcher.BeginInvoke((Action) (() => ShowHideHeaderOnCCPs(HeaderVisibility)));
                                        break;
                                    default:
                                        HeaderVisibility = Visibility.Visible;
                                        Dispatcher.BeginInvoke((Action) (() => ShowHideHeaderOnCCPs(HeaderVisibility)));
                                        break;
                                }
                                tgpsi.Clear();
                                if (mainElement.HasElements && mainElement.Element("TGPSI") != null)
                                {
                                    foreach (var kvpElement in mainElement.Element("TGPSI").Elements())
                                    {
                                        tgpsi.Add(kvpElement.Attribute("K").Value,
                                            Int32.Parse(kvpElement.Attribute("V").Value));
                                    }
                                }
                            }
                            else
                            {
                                HeaderVisibility = Visibility.Visible;
                                Dispatcher.BeginInvoke((Action) (() => ShowHideHeaderOnCCPs(HeaderVisibility)));
                            }
                        }
                    }
                    if (FloatingWindowUtil.WindowOutOfScreen(mainShell))
                    {
                        mainShell.Left = 100;
                        mainShell.Top = 100;
                    }

                }
            }
            catch
            {
                return;
            }
        }

        public XDocument SaveState()
        {
            try
            {
                lock (_isLoadingOrSaving)
                {

                    // <MainWindow WindowState="Maximized" Width="300" Height="400"/>
                    var srcTree = new XDocument(
                        new XElement("MainWindow",
                                     new XAttribute("WindowState", WindowState.ToString()),
                                     new XAttribute("Width", Width.ToString(CultureInfo.InvariantCulture)),
                                     new XAttribute("Height", Height.ToString(CultureInfo.InvariantCulture)),
                                     new XAttribute("Top", Top.ToString(CultureInfo.InvariantCulture)),
                                     new XAttribute("Left", Left.ToString(CultureInfo.InvariantCulture)),
                                     new XAttribute("Theme", _application.CurrentThemeName),
                                     new XAttribute("HeaderToggle", HeaderVisibility.ToString())
                        )
                        );
                    model_TabGroupPaneSelectedIndexStoreEvent(null, null);
                    if (tgpsi.Count > 0)
                    {
                        var tgpsiMainElement = new XElement("TGPSI");
                        foreach (var kvp in tgpsi)
                        {
                            tgpsiMainElement.Add(new XElement("KVP", new XAttribute("K", kvp.Key),
                                                              new XAttribute("V", kvp.Value)));
                        }
                        srcTree.Element("MainWindow").Add(tgpsiMainElement);
                    }
                    return srcTree;
                }
            }
            catch
            {
                return null;
            }
        }

        public string PersistorId
        {
            get { return PersistorIdKey; }
        }

        public const string PersistorIdKey = "MSGui.Internal.Shell";
        #endregion IPersistable Members

        #region IThemeChangerMembers

        public void AddTheme(string name_, ImageSource icon_, Theme theme_)
        {
            if (_application.Themes.ContainsKey(name_)) return;
            _application.Themes.Add(name_, theme_);
            var rbt = new RadioButtonTool { Caption = name_, Tag = name_, LargeImage = icon_ };
            rbt.Click += OnThemeSelected;
            rbt.SetValue(MenuToolBase.MenuItemDescriptionProperty, name_);
            themesMenu.Items.Add(rbt);
        }

        public void SetTheme(string name_)
        {
            _application.SetTheme(name_, _initialSettings);
            foreach (var themeMenuItem in themesMenu.Items)
            {
                if (themeMenuItem is RadioButtonTool)
                {
                    RadioButtonTool rbt = (RadioButtonTool)themeMenuItem;
                    if ((string)rbt.Tag == name_)
                    {
                        rbt.IsChecked = true;
                        break;
                    }
                }
            }
        }

        public void SetTheme(Theme theme_)
        {
            if (_application.Themes.ContainsKey(theme_.Name))
            {
                _application.Themes.Remove(theme_.Name); 
                foreach (var item in themesMenu.Items)
                {
                    RadioButtonTool rbt = item as RadioButtonTool;
                    if (rbt != null && (string)rbt.Tag == theme_.Name)
                    {
                        themesMenu.Items.Remove(theme_.Name);
                        break;
                    }
                }
            }
            AddTheme(theme_.Name, theme_.Icon, theme_);
            SetTheme(theme_.Name);
        }

        public void DisableThemeChanger()
        {
            themesMenu.Visibility = Visibility.Collapsed;
            _themesDisabled = true;
        }

        private void OnThemeSelected(object sender, RoutedEventArgs e)
        {
            var tool = e.Source as RadioButtonTool;
            if (tool != null && tool.Tag != null)
            {
                _application.SetTheme(tool.Tag as string, _initialSettings);
                //m_persistenceProfileService.Reload(); -- windows layout fail to load TODO
            }
        }

        #endregion IThemeChangerMembers

        internal void RedrawSaveMenu(object sender_, NotifyCollectionChangedEventArgs e)
        {
            RepopulateSaveMenu(menuSaveLayoutMenu.Items);
            RepopulateSaveMenu(((MenuTool)Resources["ribbonSaveLayoutMenu"]).Items);
        }

        private void RepopulateSaveMenu(ItemCollection items_)
        {
            foreach (var i in items_)
            {
                Infragistics.Windows.Ribbon.ToolMenuItem tmi = i as Infragistics.Windows.Ribbon.ToolMenuItem;
                if (tmi != null)
                {
                    RoutedEventHandler handler = tmi.Tag as RoutedEventHandler;

                    if (handler != null)
                    {
                        tmi.Click -= handler;
                    }
                }
            }

            items_.Clear();
            var createNewItem = new Infragistics.Windows.Ribbon.ToolMenuItem
            {
                Header = "<Create New>",
                Icon = new Image(){Source = new BitmapImage(new Uri(@"pack://application:,,,/MSDotNet.MSGui.Impl;component/Images/Icons/plus.png"))},
            };

            RoutedEventHandler newItemClickHandler = new RoutedEventHandler(CreateNewItemClick);
            createNewItem.Click += newItemClickHandler;
            createNewItem.Tag = newItemClickHandler;

            items_.Add(createNewItem);
            items_.Add(new SeparatorTool());
            foreach (var profile in _profileMonitor.CurrentAvailableProfiles)
            {
                var radioTool = new Infragistics.Windows.Ribbon.ToolMenuItem
                {
                    Header = string.Format("Save as '{0}'", profile),
                    IsChecked = profile == CurrentProfile,
                    DataContext = profile
                };
                if (profile == CurrentProfile)
                {
                    radioTool.Icon = new Image(){Source = new BitmapImage(new Uri(@"pack://application:,,,/MSDotNet.MSGui.Impl;component/Images/Icons/tick.png"))};
                    radioTool.Background = Brushes.LightYellow;
                }
                else
                {
                    radioTool.Icon = new Image()
                        {
                            Source =
                                new BitmapImage(
                                    new Uri(@"pack://application:,,,/MSDotNet.MSGui.Impl;component/Images/Icons/round.png"))
                        };
                }
                var profile2 = profile;
                RoutedEventHandler clickHandler = (x_, y_) =>
                {
                    var result = TaskDialog.ShowMessage
                                (string.Format("Overwrite layout '{0}'?", profile2), "Overwrite?",
                                TaskDialogCommonButtons.YesNo, VistaTaskDialogIcon.Warning);
                    if (result == TaskDialogSimpleResult.Yes)
                    {
                        _persistenceProfileService.SaveCurrentProfileAs(profile2);
                    }
                };
                radioTool.Tag = clickHandler;
                radioTool.Click += clickHandler;
                items_.Add(radioTool);
            }
        }

        private void CreateNewItemClick(object sender_, RoutedEventArgs e_)
        {
            SaveProfileAs();
        }

        internal void RedrawMenus()
        {
            RedrawSaveMenu(null, null);
            ((MenuTool)Resources["ribbonLoadLayoutMenu"]).ItemsSource = _profileMonitor.CurrentAvailableProfiles;
            menuLoadLayout.ItemsSource = _profileMonitor.CurrentAvailableProfiles;

        }

        private void ShowSaveAsDialog(out string profile, out bool save)
        {
            if (_initialSettings.EnableLegacyDialog)
            {
                var profileNameDialog = new SaveAsLayoutWindow { Owner = this };
                bool? profileNameDialogResult = profileNameDialog.ShowDialog();
                profile = profileNameDialog.SelectedText;
                save = profileNameDialogResult.HasValue && profileNameDialogResult.Value;
            }
            else
            {
                TaskDialogOptions options = new TaskDialogOptions()
                {
                    UserInputEnabled = true,
                    MainInstruction = @"Please provide layout name",
                    Title = @"Save layout",
                    CommonButtons = TaskDialogCommonButtons.OKCancel,
                    MainIcon = VistaTaskDialogIcon.Information,
                };
                TaskDialogResult dialogResult = TaskDialog.Show(options);
                profile = dialogResult.UserInput;
                save = dialogResult.Result == TaskDialogSimpleResult.Ok;
                if (save && string.IsNullOrWhiteSpace(profile))
                {
                    TaskDialog.ShowMessage("Please enter a layout name.");
                    save = false;
                }
            }
        }

        #region Closing

        protected override void OnClosing(CancelEventArgs e_)
        {
            if (DisableClose)
            {
                e_.Cancel = true;
            }
            if (e_.Cancel) return;
            _application.InvokeApplicationClosing();
            Dictionary<string, XElement> savedStateDictionary;
            //if (!m_persistenceProfileService.CheckIsUnchanged(out savedStateDictionary))
            if (!ShouldSkipSaveProfile() &&
                (PresentUnsavedQuestion(out savedStateDictionary) || _layoutLoadStrategy == LayoutLoadStrategy.NeverAskButPromptForExit))
            {
                if (_application.Settings.ShowExitDialog)
                {
                    IExitDialog dialog;

                    if (_initialSettings.EnableLegacyDialog)
                        dialog = new ExitDialog();
                    else
                        dialog = new ExitTaskDialogLauncher();

                    dialog.ProfileName = _profileMonitor.CurrentProfile ?? "Untitled profile";
                    dialog.AppName = _application.Name;
                    //AutomaticSeconds = m_quitDialogSeconds,
                    //AutomaticAnswer = m_quitDialogSave
                    try
                    {
                        dialog.Owner = this;
                    }
                    catch
                    {
                        // Means that window was never shown, not a problem
                    }
                    bool? result = dialog.ShowDialog();
                    if (result != true) //Cancel pressed
                    {
                        e_.Cancel = true;
                        base.OnClosing(e_);
                        return;
                    }
                    if (dialog.DontAsk)
                    {
                        _application.Settings.SaveOnExit = dialog.SaveProfile;
                        _application.Settings.ShowExitDialog = false;
                    }

                    if (dialog.SaveProfile)
                    {
                        if (string.IsNullOrEmpty(_profileMonitor.CurrentProfile))
                        {
                            string profile;
                            bool save;
                            ShowSaveAsDialog(out profile, out save);
                            if (save && !String.IsNullOrEmpty(profile))
                            {
                                string existingProfile = _profileMonitor.CurrentAvailableProfiles.FindProfile(profile);
                                if (existingProfile != null)
                                {
                                    profile = existingProfile;
                                    TaskDialogSimpleResult result2 = TaskDialog.ShowMessage
                                                          (string.Format("Overwrite layout '{0}'?", profile), "Overwrite?", TaskDialogCommonButtons.YesNo,
                                                           VistaTaskDialogIcon.Warning);
                                    if (result2 == TaskDialogSimpleResult.No)
                                    {
                                        return;
                                    }
                                }
                                if (savedStateDictionary == null)
                                {
                                    _persistenceProfileService.CheckIsUnchanged(out savedStateDictionary);
                                }
                                _persistenceProfileService.SaveItemsDictionaryAs(savedStateDictionary, profile);
                            }
                        }
                        else
                        {
                            if (savedStateDictionary == null)
                            {
                                _persistenceProfileService.CheckIsUnchanged(out savedStateDictionary);
                            }
                            _persistenceProfileService.SaveItemsDictionary(savedStateDictionary);
                        }
                    }
                }
                else
                {
                    if (_application.Settings.SaveOnExit)
                    {
                        if (string.IsNullOrEmpty(_profileMonitor.CurrentProfile))
                        {
                            CreateNewItemClick(this, null);
                        }
                        else
                        {
                            _persistenceProfileService.SaveCurrentProfile();
                        }
                    }
                }
            }
            var persistenceService = _container.Resolve<IItemsDictionaryService>();
            persistenceService.SaveItemsToDict(ItemScope.GlobalPersistor);

            //give Concord ability to process closing
            bool allowed = true;
            if (_tryCloseConcordMainWindow != null && !e_.Cancel)
            {
                allowed = _tryCloseConcordMainWindow();
            }
            e_.Cancel = !allowed;

            if (!e_.Cancel)
            {
                foreach (Func<bool> action in onClosingActions)
                {
                    try
                    {
                        if (!action())
                        {
                            e_.Cancel = true;
                            break;
                        }
                    }
                    catch (Exception exc)
                    {
                        Logger.Warning("OnClosing action thrown an exception", exc);
                    }
                }
            }

            // when Concord allows to close, turn off closing handler
            if (_tryCloseConcordMainWindow != null && !e_.Cancel)
            {
                var form = this.ConcordMSNetLoopForm as ConcordMSNetLoopForm;
                if (form != null) form.TurnOffFormClosingEvents();
            }

            base.OnClosing(e_);

            // when Main window cancel closing.. restore the event FormClosingEvent handler
            if (e_.Cancel)
            {
                var form = this.ConcordMSNetLoopForm as ConcordMSNetLoopForm;
                if (form != null)
                {
                    form.TurnOnFormClosingEvents();
                }
            }
            else
            {
                foreach (Action action in onClosedActions)
                {
                    try
                    {
                        action();
                    }
                    catch (Exception exc)
                    {
                        Logger.Warning("OnClosed action thrown an exception", exc);
                    }
                }
                // not Canceling, close the form is acceptable.
                if (ConcordMSNetLoopForm != null)
                    ConcordMSNetLoopForm.Close();
            }
        }

        void ShellWindow_Closing(object sender, CancelEventArgs e)
        {
            if (e.Cancel)
            {
                _application.InvokeApplicationClosingCancelled();
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            _container.Resolve<IHidingLockingManager>().HideUIGranularityChanged -= OnHideUIGranularityChanged;
            _application.InvokeApplicationClosed();
            base.OnClosed(e);
            System.Windows.Application.Current.Shutdown();
        }

        #endregion Closing

        private void CurrentProfileChanged(object sender_, CurrentProfileChangedEventArgs e)
        {
            PropertyChangedEventHandler copy = PropertyChanged;
            if (copy != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs("CurrentProfile"));
            }
            RedrawMenus();

            UpdateTitle();

            if (!string.IsNullOrEmpty(e.NewProfile)) //can be null when the current profile is deleted
            {
                RecentItemInfo info = new RecentItemInfo(e.NewProfile, e.NewProfile);
                if (!_recentItemsManager.MoveRecentItem(info, 0))
                {
                    _recentItemsManager.AddRecentItem(info);
                }
            }
        }

        void ProfileDeleted(object sender, ProfileDeletedEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.ProfileDeleted))
            {
                _recentItemsManager.RemoveRecentItem(new RecentItemInfo(e.ProfileDeleted, e.ProfileDeleted));
            }
        }


        void ProfileRenamed(object sender, ProfileRenamedEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.OldProfileName) && !string.IsNullOrEmpty(e.NewProfileName))
            {
                _recentItemsManager.UpdateRecentItem(new RecentItemInfo(e.OldProfileName, e.OldProfileName),
                    new RecentItemInfo(e.NewProfileName, e.NewProfileName));
            }
        }


        private void UpdateTitle()
        {
            if (ShellModeExtension.HideUIGranularity.HasFlag(HideLockUIGranularity.DontAppendProfileToWindowTitle))
                Title = _titleBase;
            else
                Title = _titleBase + " " + CurrentProfile;
        }

        private void ShellWindow_Loaded(object sender_, RoutedEventArgs e)
        {
            if (ShellModeExtension.HideUIGranularity.HasFlag(HideLockUIGranularity.HideRibbon))
            {
                ribbon.Visibility = Visibility.Collapsed;
            }

            if (ShellModeExtension.HideUIGranularity.HasFlag(HideLockUIGranularity.HideStatusBar))
            {
                BottomBackstagePadding.Height = 0;
            }

            if (ShellModeExtension.InvisibleMainControllerMode.HasFlag(ShellModeExtension.InvisibleMainControllerModeEnum.HideWindow))
            {
                this.Visibility = Visibility.Hidden;
            }
            if (ShellModeExtension.InvisibleMainControllerMode.HasFlag(ShellModeExtension.InvisibleMainControllerModeEnum.MoveWindow))
            {
                this.Top = -5000;
                this.Left = -5000;
            }

            _shown = true;
            Action copy = ShellShown;
            if (copy != null)
            {
                copy();
            }
            if (ShellModeExtension.NoTaskbarPin)
            {
                NoTaskbarPin();
            }
            if (FrameworkExtensions.DefaultStickWindowOptions != StickyWindowOptions.None)
            {
                new WPFStickyWindow(this)
                {
                    StickToScreen = FrameworkExtensions.DefaultStickWindowOptions.HasFlag(StickyWindowOptions.StickToScreen),
                    StickToOther = FrameworkExtensions.DefaultStickWindowOptions.HasFlag(StickyWindowOptions.StickToOther)
                };
            }
            if (tabbedDock != null)
            {

                var label = _contentHost.FindVisualChildByName<EnvironmentLabel>("ShellWindowEnvironmentLabel");
                if (label == null) return;
                var dockViewModel = tabbedDock.DataContext as DockViewModel;
                if (dockViewModel != null && dockViewModel.EnvironmentLabelViewModel != null)
                {
                    label.DataContext = dockViewModel.EnvironmentLabelViewModel;
                    label.Visibility = dockViewModel.EnvironmentLabelViewModel.LabelsEnabled
                                           ? Visibility.Visible
                                           : Visibility.Collapsed;
                }
            }
            else if (floatingOnlyDock != null)
            {
                // We need to disable sizeable border and get rid of traditional client area
            }
        }

        private void OnSaveClick(object sender_, RoutedEventArgs e_)
        {
            SaveCurrentProfile();
        }

        private void OnDeleteClick(object sender_, RoutedEventArgs e_)
        {
            DeleteCurrentProfile();
        }

        private bool PresentUnsavedQuestion(out Dictionary<string, XElement> dictionary)
        {
            switch (_layoutLoadStrategy)
            {
                case LayoutLoadStrategy.Default:
                    //Dictionary<string, XElement> dictionary;
                    return (!_persistenceProfileService.CheckIsUnchanged(out dictionary));
                case LayoutLoadStrategy.AlwaysAsk:
                    dictionary = null;
                    return true;
                case LayoutLoadStrategy.NeverAskButPromptForExit:
                case LayoutLoadStrategy.NeverAsk:
                    dictionary = null;
                    return false;
                case LayoutLoadStrategy.UseCallback:
                    dictionary = null;
                    return _layoutLoadStrategyCallback();
                default:
                    dictionary = null;
                    return true;
            }
        }

        private bool ShouldSkipSaveProfile()
        {
            if (_skipSaveProfileOnExitCondition == SkipSaveProfileOnExitCondition.Never) return false;

            return _container.Resolve<IModuleLoadInfos>().Infos.Any(
                m_ =>
                    //condition 1: module is blocked by entitlement
                (m_.Status == ModuleStatus.BlockedByEntitlements ||
                 m_.Status == ModuleStatus.BlockedByNoEntitlementsInfo ||
                 m_.Status == ModuleStatus.DependentModuleBlocked) &&
                ((_skipSaveProfileOnExitCondition & SkipSaveProfileOnExitCondition.ModuleBlockedByEntitlement) == SkipSaveProfileOnExitCondition.ModuleBlockedByEntitlement) ||
                    //condition 2: module type is not resolved
                (m_.Status == ModuleStatus.TypeResolutionFailed &&
                (_skipSaveProfileOnExitCondition & SkipSaveProfileOnExitCondition.ModuleTypeResolutionFailed) == SkipSaveProfileOnExitCondition.ModuleTypeResolutionFailed) ||
                    //condition 3: exception occurred when loading module
                (m_.Status == ModuleStatus.Exceptioned &&
                (_skipSaveProfileOnExitCondition & SkipSaveProfileOnExitCondition.ModuleLoadExceptioned) == SkipSaveProfileOnExitCondition.ModuleLoadExceptioned));
        }

        private void LayoutToLoadSelected(object sender_, RoutedEventArgs e)
        {
            var toolMenuItem = sender_ as Infragistics.Windows.Ribbon.ToolMenuItem;
            if (toolMenuItem != null)
            {
                string profileName = toolMenuItem.Header.ToString();
                LoadProfile(profileName);
            }
        }

        #region IShell Members

        public void Initialize()
        {

            m_chromeRegistry = _container.Resolve<IChromeRegistry>();
            m_chromeManager = _container.Resolve<IChromeManager>();

            _persistenceProfileService = _container.Resolve<PersistenceProfileService>();
            _profileMonitor = _persistenceProfileService;

            _recentItemsManager = new RecentItemsManager(_container.Resolve<IPersistenceService>(), appmenu);
            _recentItemsManager.RegisterHandler(RecentItemUsage.LayoutProfile,
                new Action<Shell.RecentItemInfo>(info => { LoadProfile(info.Parameter); }),
                //do the synchronization when restored MRU list, just in case the profiles have been deleted from the file system outside of the application running
                new Func<RecentItemInfo, bool>(info =>
                {
                    string profileName = _profileMonitor.CurrentAvailableProfiles.FindProfile(info.Parameter);
                    if (profileName == null) return false;
                    info.CopyFrom(new RecentItemInfo(profileName, profileName));
                    return true;
                }));
            _profileMonitor.CurrentProfileChanged += CurrentProfileChanged;
            _profileMonitor.ProfileDeleted += new EventHandler<ProfileDeletedEventArgs>(ProfileDeleted);
            _profileMonitor.ProfileRenamed += new EventHandler<ProfileRenamedEventArgs>(ProfileRenamed);

            Dispatcher.BeginInvoke(RedrawMenus, DispatcherPriority.ApplicationIdle);
            //((INotifyCollectionChanged)m_profileMonitor.AvailableProfiles).CollectionChanged += RedrawSaveMenu;

            if (tabbedDock != null)
            {
                var model = (TabbedDockViewModel)tabbedDock.DataContext;
                if (SpecialTabView != null)
                {
                    model.SetSpecialTab(SpecialTabView);
                }
                else
                {
                    model.AddTab(TabbedDockViewModel.MAIN_TAB_NAME);
                }
            }

            //ChromeManagerBase.Instance.registrations[ChromeManagerBase.Instance].Add(model);
            //ChromeManagerBase.Instance[ChromeArea.TabbedDock] = model;

            menuLoadLayout = (MenuTool)Resources["appMenuLoadLayoutMenu"];
            themesMenu = (MenuTool)Resources["appMenuThemesMenu"];
            menuSaveLayoutMenu = (MenuTool)Resources["appMenuSaveLayoutMenu"];
            menuRootArea = (MenuRootArea)m_chromeManager[ChromeManagerBase.MENU_COMMANDAREA_NAME]; 

            if (!ModernThemeHelper.IsModernThemeEnabled)
            {
                CreateClassicThemeSpecificItems();
            }
            ((ChromeManagerBase)m_chromeManager).AfterCreateChrome += (sender_, args_) =>
            { 
                if (ChromeManagerBase.WillLoadChromeFromState || ModernThemeHelper.IsModernThemeEnabled) return; 

                appmenu.Dispatcher.BeginInvoke(() =>
                {
                        if (!menuRootArea.Contains(ShellMenuExtensions.ApplicationMenuFactories.ExitButtonWithSeparator)) return; 
                        var exitItem = menuRootArea[ShellMenuExtensions.ApplicationMenuFactories.ExitButtonWithSeparator];
                        if (exitItem == null) return;
                        exitItem.ClearParent();
                        if (!menuRootArea.Contains(ShellMenuExtensions.ApplicationMenuFactories.ExitButtonSeparator))
                        {
                            menuRootArea.AddWidget(ShellMenuExtensions.ApplicationMenuFactories.ExitButtonSeparator, new InitialDropdownSeparatorParameters());

                        }
                        else
                        {
                            var exitButtonSeperator = menuRootArea[ShellMenuExtensions.ApplicationMenuFactories.ExitButtonSeparator];
                            exitButtonSeperator.ClearParent();
                            menuRootArea.Add(exitButtonSeperator);
                        }
                        menuRootArea.Add(exitItem);
                    }, DispatcherPriority.ApplicationIdle);
                 
            };
        }

        public void PostChromeCreationInitialize()
        {
            if (!_initialSettings.PreventRibbonCleaning)
            {
                XamRibbonHelper.CleanRibbon(Ribbon, _container.Resolve<IControlParadigmImplementator>());
            }
        }

        private void CreateClassicThemeSpecificItems()
        {
            AddRibbonToolToApplicationMenu(menuLoadLayout, ShellMenuExtensions.ApplicationMenuFactories.LoadLayoutMenu);
            AddRibbonToolToApplicationMenu((IRibbonTool) Resources["appMenuSaveLayoutButton"],
                                           ShellMenuExtensions.ApplicationMenuFactories.SaveLayoutButton);
            AddRibbonToolToApplicationMenu(menuSaveLayoutMenu, ShellMenuExtensions.ApplicationMenuFactories.SaveLayoutMenu);
            AddRibbonToolToApplicationMenu((IRibbonTool) Resources["appMenuDeleteLayoutButton"],
                                           ShellMenuExtensions.ApplicationMenuFactories.DeleteLayoutButton);

            AddRibbonToolToApplicationMenu(new SeparatorTool(),
                                           ShellMenuExtensions.ApplicationMenuFactories.AfterLayoutManagementSeparator);

            AddRibbonToolToApplicationMenu(themesMenu, ShellMenuExtensions.ApplicationMenuFactories.ThemesButton);
            AddButtonToApplicationMenu(ShellMenuExtensions.ApplicationMenuFactories.OptionsButton, _container.Resolve<ApplicationOptions>().GetOptionButtonParameter()); 
            AddRibbonToolToApplicationMenu((IRibbonTool) Resources["appMenuExitButton"],
                                           ShellMenuExtensions.ApplicationMenuFactories.ExitButtonWithSeparator); 
        }

        void ApplicationMenuItemCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (IWidgetViewContainer oldItem in e.OldItems)
                {
                    if (classicThemeOnlyItems.Contains(oldItem))
                    {
                        classicThemeOnlyItems.Remove(oldItem);
                        classicThemeItemsToBeAdded.Remove(oldItem.FactoryID);
                    }
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (IWidgetViewContainer newItem in e.NewItems)
                {
                    if (staticClassicThemeItemsToBeAdded.Contains(newItem.FactoryID))
                    {
                        if (!classicThemeOnlyItems.Contains(newItem))
                        {
                            classicThemeOnlyItems.Add(newItem);
                            classicThemeItemsToBeAdded.Add(newItem.FactoryID);
                        } 
                    }
                }
            }
        }

        List<IWidgetViewContainer> classicThemeOnlyItems = new List<IWidgetViewContainer>();

        private static readonly string[] staticClassicThemeItemsToBeAdded = new string[]
            {
                ShellMenuExtensions.ApplicationMenuFactories.LoadLayoutMenu,
                ShellMenuExtensions.ApplicationMenuFactories.SaveLayoutButton,
                ShellMenuExtensions.ApplicationMenuFactories.SaveLayoutMenu,
                ShellMenuExtensions.ApplicationMenuFactories.DeleteLayoutButton,
                ShellMenuExtensions.ApplicationMenuFactories.AfterLayoutManagementSeparator,
                ShellMenuExtensions.ApplicationMenuFactories.ThemesButton,
                ShellMenuExtensions.ApplicationMenuFactories.OptionsButton,
                ShellMenuExtensions.ApplicationMenuFactories.ExitButtonWithSeparator
            };
        private readonly List<string> classicThemeItemsToBeAdded = new List<string>(staticClassicThemeItemsToBeAdded);
        private void AddRibbonToolToApplicationMenu(IRibbonTool tool_, string factoryName_)
        { 
            if (!classicThemeItemsToBeAdded.Contains(factoryName_)) return;
            m_chromeRegistry.RegisterWidgetFactory(factoryName_, (viewContainer_, state_) =>
                {
                    if (ModernThemeHelper.IsModernThemeEnabled) return false;
                    viewContainer_.Content = tool_;
                    return true;
                });
            string parametersText = null;
            if (tool_ is MenuTool)
            {
                parametersText = ((MenuTool)tool_).Caption;
            }
            else if (tool_ is ButtonTool)
            {
                parametersText = ((ButtonTool)tool_).Caption;
            }
           
            classicThemeOnlyItems.Add(menuRootArea.AddWidget(factoryName_, new InitialWidgetParameters
                                                                               {
                                                                                   Text = parametersText
                                                                               }));
        }

        private void AddButtonToApplicationMenu(string factoryName_, InitialButtonParameters parameter_)
        {
            if (!classicThemeItemsToBeAdded.Contains(factoryName_)) return;
            m_chromeRegistry.RegisterWidgetFactory(factoryName_, (viewContainer_, state_) =>
            {
                return ! ModernThemeHelper.IsModernThemeEnabled; 
            });
            
            classicThemeOnlyItems.Add(menuRootArea.AddWidget(factoryName_, parameter_));

        }
         

        public ItemsControl MainMenu
        {
            get
            {
                return ModernThemeHelper.IsModernThemeEnabled ? _backstageApplicationMenu : appmenu;
            }
        }

        public IWindowManager WindowManager
        {
            get { return windowManager; }
        }

        public ITileItemManager TileItemManager
        {
            get { return _tileItemCollectionModel; }
        }
        public Window MainWindow
        {
            get
            {
                return this;
            }
        }

        public System.Windows.Controls.Primitives.StatusBar StatusBar
        {
            get
            {
                return statusBar;
            }
        }

        public Func<bool> TryCloseConcordMainWindow
        {
            set
            {
                _tryCloseConcordMainWindow = value;
            }
        }

        public System.Windows.Forms.Form ConcordMSNetLoopForm
        {
            get;
            set;
        }

        public void LoadProfile(string profile_, bool calledWhenLoading_ = false)
        {
            if (profile_ != null)
            {
                Dictionary<string, XElement> dictionary;
                if (_shown && PresentUnsavedQuestion(out dictionary))
                {
                    TaskDialogSimpleResult result = TaskDialog.ShowMessage(
                        string.Format(
                            "Do you want to load layout '{0}'? \n All unsaved changes in the current layout will be lost.",
                            profile_),
                        "Load",
                        TaskDialogCommonButtons.YesNo,
                        VistaTaskDialogIcon.Warning);
                    if (result == TaskDialogSimpleResult.No)
                    {
                        return;
                    }
                }
            }

            if (calledWhenLoading_)
            {
                IItemsDictionaryService persistenceService = _container.Resolve<IItemsDictionaryService>();
                persistenceService.RestoreItemsFromDict(new Dictionary<string, XElement>(), ItemScope.GlobalPersistor);
                //we only want to monitor any manual change of profile by end user after the loading of the shell
                _profileMonitor.CurrentProfileChanged -= CurrentProfileChanged;
                RedrawMenus();
            }
            _persistenceProfileService.LoadProfile(profile_, calledWhenLoading_, null,
                () =>
                {
                    if (calledWhenLoading_)
                    {

                        //we only want to monitor any manual change of profile by end user after the loading of the shell
                        _profileMonitor.CurrentProfileChanged += CurrentProfileChanged;
                    }
                });
        }

        public void SaveCurrentProfile()
        {
            if (string.IsNullOrEmpty(_profileMonitor.CurrentProfile))
            {
                return;
            }
            _persistenceProfileService.SaveCurrentProfile();
        }

        public void DeleteCurrentProfile()
        {
            DeleteProfile(_profileMonitor.CurrentProfile);
        }

        public void DeleteProfile(string profileName_)
        {
            if (string.IsNullOrEmpty(profileName_))
            {
                return;
            }
            TaskDialogSimpleResult result = TaskDialog.ShowMessage
                (string.Format("Are you sure you want to delete the profile {0}", profileName_),
                 string.Format("Delete {0}?", _profileMonitor.CurrentProfile),
                 TaskDialogCommonButtons.YesNo,
                 VistaTaskDialogIcon.Warning);
            if (result == TaskDialogSimpleResult.Yes)
            {
                _persistenceProfileService.DeleteProfile(profileName_);
                RedrawMenus();
            }
        }

        public void SaveProfileAs()
        {
            string profile;
            bool save;
            ShowSaveAsDialog(out profile, out save);
            if (save == true && !String.IsNullOrEmpty(profile))
            {
                string existingProfile = _profileMonitor.CurrentAvailableProfiles.FindProfile(profile);
                if (existingProfile != null)
                {
                    profile = existingProfile;
                    TaskDialogSimpleResult result2 = TaskDialog.ShowMessage
    (string.Format("Overwrite layout '{0}'?", profile), "Overwrite?", TaskDialogCommonButtons.YesNo,
    VistaTaskDialogIcon.Warning);
                    if (result2 == TaskDialogSimpleResult.No)
                    {
                        return;
                    }
                }
                _persistenceProfileService.SaveCurrentProfileAs(profile);
                RedrawMenus();
            }
        }

        public void SetLayoutLoadStrategy(LayoutLoadStrategy layoutLoadStrategy_)
        {
            _layoutLoadStrategy = layoutLoadStrategy_;
        }

        public void SetLayoutLoadStrategy(LayoutLoadStrategy layoutLoadStrategy_, LayoutLoadStrategyCallback layoutLoadStrategyCallback_)
        {
            _layoutLoadStrategy = layoutLoadStrategy_;
            _layoutLoadStrategyCallback = layoutLoadStrategyCallback_;
        }

        #endregion IShell Members

        public XamRibbon Ribbon
        {
            get { return ribbon; }
        }

        public ItemsControl QAT
        {
            get { return qat; }
        }

        #region IShell Members

        private IList<Func<bool>> onClosingActions = new List<Func<bool>>();
        private IList<Action> onClosedActions = new List<Action>();

        Func<bool> IShell.OnClosing
        {
            set { onClosingActions.Add(value); }
        }

        Action IShell.OnClosed
        {
            set { onClosedActions.Add(value); }
        }
        //public void SetQuitDialogAutomaticBehaviour(int quitDialogSeconds, bool quitDialogSave)
        //{
        //    m_quitDialogSeconds = quitDialogSeconds;
        //    m_quitDialogSave = quitDialogSave;
        //}

        public bool DisableClose { get; set; }
        #endregion IShell Members

        private void OnExitClick(object sender_, RoutedEventArgs e_)
        {
            this.Close();
        }

        #region Implementation of IShellWindowMenuFacade

        public UIElement LoadLayoutMenu
        {
            get { return (UIElement)Resources["ribbonLoadLayoutMenu"]; }
        }

        public UIElement SaveLayoutButton
        {
            get { return (UIElement)Resources["ribbonSaveLayout"]; }
        }

        public UIElement SaveLayoutAsMenu
        {
            get { return (UIElement)Resources["ribbonSaveLayoutMenu"]; }
        }

        public UIElement DeleteCurrentLayoutButton
        {
            get { return (UIElement)Resources["ribbonDeleteLayout"]; }
        }

        public UIElement ThemesMenu
        {
            get { return (UIElement)Resources["ribbonThemesMenu"]; }
        }

        public UIElement ExitButton
        {
            get { return (UIElement)Resources["ribbonExit"]; }
        }

        public UIElement HeaderVisibilityToggle
        {
            get { return (UIElement)Resources["ribbonHeaderVisibilityToggle"]; }
        }


        public UIElement PrintButton
        {
            get { return (UIElement)Resources["ribbonPrint"]; }
        }

        public UIElement PrintPreviewButton
        {
            get { return (UIElement)Resources["ribbonPrintPreview"]; }
        }

        #endregion Implementation of IShellWindowMenuFacade

        internal void ShowHideHeaderOnCCPs(Visibility visibility)
        {
            if (tabbedDock == null) return;
            tabbedDock.ShowHideHeaderOnCCPs(visibility);
        }

        private Visibility HeaderVisibility { get; set; }

        private void OnRibbonHeaderVisibilityToggleClick(object sender, RoutedEventArgs e)
        {
            HeaderVisibility = HeaderVisibility == Visibility.Visible ? Visibility.Collapsed : Visibility.Visible;
            ShowHideHeaderOnCCPs(HeaderVisibility);
        }

        internal static void SetTitleBase(string titleBase)
        {
            var instance = _container.Resolve<IShellWindowMenuFacade>() as ShellWindow;

            if (titleBase == null)
            {
                instance._titleBase = string.Format("{0} - (v{1})", instance._application.Name, instance._application.Version);
            }
            else
            {
                instance._titleBase = titleBase;
            }

            instance.UpdateTitle();
        }


        /** Hack to add automation Id for ribbon tab list; this is needed to work with CodedUI Test. */
        private void XamRibbonWindow_Loaded(object sender, RoutedEventArgs e)
        {
            XamTabControl tabControl = (XamTabControl)Infragistics.Windows.Utilities.GetDescendantFromType(Ribbon, typeof(XamTabControl), false);
            if (tabControl != null)
                AutomationProperties.SetAutomationId(tabControl, "MSDesktop_Main_RibbonXamTabControl");
            CommandBinding restoreBinding = new CommandBinding(RibbonWindowCommands.RestoreCommand, OnRestoreCommand);
            CommandBindings.Add(restoreBinding);
            CommandBinding maxBinding = new CommandBinding(RibbonWindowCommands.MaximizeCommand, OnMaximizeCommand);
            CommandBindings.Add(maxBinding);
            CommandBinding minBinding = new CommandBinding(RibbonWindowCommands.MinimizeCommand, OnMinimizeCommand);
            CommandBindings.Add(minBinding);

        }

        private void OnMinimizeCommand(object sender, ExecutedRoutedEventArgs e)
        {
            Debug.WriteLine(this.RestoreBounds);
            this.WindowState = WindowState.Minimized;
            e.Handled = true;
        }

        private void OnMaximizeCommand(object sender, ExecutedRoutedEventArgs e)
        {
            Debug.WriteLine(this.RestoreBounds);
            this.WindowState = WindowState.Maximized;
            e.Handled = true;

        }

        private void OnRestoreCommand(object sender, ExecutedRoutedEventArgs e)
        {
            Debug.WriteLine(this.RestoreBounds);
            this.WindowState = WindowState.Normal;
            e.Handled = true;

        }


        public static DependencyProperty TitleBarProperty
         = DependencyProperty.Register("TitleBar", typeof(UIElement), typeof(ShellWindow));

        private MenuTool menuLoadLayout;
        private MenuTool themesMenu
            ;

        private MenuTool menuSaveLayoutMenu;
        private IChromeRegistry m_chromeRegistry;
        private IChromeManager m_chromeManager;

        public UIElement TitleBar
        {
            get { return (UIElement)GetValue(TitleBarProperty); }
            set
            {
                SetValue(TitleBarProperty, value);
            }
        }

        public object SpecialTabView { get; set; }

        internal FloatingOnlyDock FloatingOnlyDock
        {
            get { return floatingOnlyDock; }
        }


        private OverlayAdorner m_overlayAdorner;

        private Point _previousLocation;
        private LayoutScreenshotProvider screenshotProvider;
        private Border ribbonContentPanel;
        private DesiredSizeDecorator backstageHelpers_RibbonSize;
        private CustomXamTabControl backstageHelpers_TabControl;
        private CommandBinding closeBackstageCommandBinding;

        private void OnWindowLocationChanged(object sender_, EventArgs e_)
        {
            if (tabbedDock == null) return;
            var newLocation = new Point(Left, Top);
            var delta = newLocation - _previousLocation;
            tabbedDock.MoveAllWindowsByVector(delta);
            _previousLocation = newLocation;
        }


        private void BackstageWrapper_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            HideBackstage();

            this.RaiseEvent(e);
        }

        #region  For CUIT

        protected override System.Windows.Automation.Peers.AutomationPeer OnCreateAutomationPeer()
        {
            var inner = base.OnCreateAutomationPeer();
            var helper = new ShellWindowCommandHandler(m_chromeManager);
            return new ShellWindowAutomationPeer(inner as FrameworkElementAutomationPeer, this, helper);
        }


        private class ShellWindowCommandHandler : ImagePathHandler
        {

            internal ShellWindowCommandHandler(IChromeManager chromeManager)
                : base()
            {
                _chromeManager = chromeManager;
                _chromeManager.Windows.CollectionChanged += Windows_CollectionChanged;


                Handlers[MainWindowSupportedCommands.GetLastCreatedView.ToString()] =
                    (paras) => _lastViewId;

                Handlers[MainWindowSupportedCommands.GetAllViews.ToString()] =
                    (paras) => _chromeManager.Windows.Aggregate("", (id, wvc) => wvc.ID + "; " + id); ;

                Handlers[MainWindowSupportedCommands.GetFloatingViews.ToString()] =
                    (paras) => (from view in _chromeManager.Windows
                                where DockHelper.GetRootOfFloatingPane(view) != null
                                select view.ID).Aggregate("", (ids, id) => ids + ";" + id);

                Handlers[MainWindowSupportedCommands.GetDockedViews.ToString()] =
                    (paras) => (from view in _chromeManager.Windows
                                where DockHelper.GetRootOfDockedPane(view) != null
                                select view.ID).Aggregate("", (ids, id) => ids + ";" + id);


                Handlers[MainWindowSupportedCommands.SetToPlaybackMode.ToString()] =
                    (paras) =>
                    {
                        VisualTestHelper.IsRecording =
                            false;
                        return "";
                    };


                Handlers[MainWindowSupportedCommands.MoveTo.ToString()] =
                    (parameters) =>
                    {
                        double top, left;
                        if (!double.TryParse(parameters[0], out left) ||
                            !double.TryParse(parameters[1], out top))
                            return "";

                        var peer = this.AutomationPeer as ShellWindowAutomationPeer;
                        var shellWindow = peer.Owner as ShellWindow;
                        shellWindow.Left = left;
                        shellWindow.Top = top;

                        return "";
                    };

                Handlers[MainWindowSupportedCommands.GetAppliationTitle.ToString()] =
                    (paras) =>
                    {
                        var peer = this.AutomationPeer as ShellWindowAutomationPeer;
                        var shellWindow = peer.Owner as ShellWindow;
                        return shellWindow.Title;
                    };

                Handlers[MainWindowSupportedCommands.GetProcessId.ToString()] =
                  (paras) =>
                  {
                      return Process.GetCurrentProcess().Id.ToString();
                  };

                Handlers[MainWindowSupportedCommands.GetTestConfigFile.ToString()] =
                    (paras) =>
                    {
                        var dir = Assembly.GetExecutingAssembly().Location;
                        return Path.GetFullPath(Path.Combine(dir, "Tests", "CUIT.Config"));
                    };
            }

            private IChromeManager _chromeManager;
            private string _lastViewId;

            ~ShellWindowCommandHandler()
            {
                _chromeManager.Windows.CollectionChanged -= Windows_CollectionChanged;
            }

            void Windows_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
            {
                if (e.NewItems != null)
                {
                    var wvc = e.NewItems[0] as IWindowViewContainer;
                    _lastViewId = wvc.ID;
                }
            }
        }


        class ShellWindowAutomationPeer : RecordImageAutomationPeerWrapper
        {
            public ShellWindowAutomationPeer(FrameworkElementAutomationPeer inner, FrameworkElement owner, ShellWindowCommandHandler commandHandler)
                : base(inner, owner, commandHandler)
            {
            }

            protected override string GetAutomationIdCore()
            {
                return (String)Owner.GetValue(AutomationProperties.AutomationIdProperty);
            }

            protected override bool IsEnabledCore()
            {
                RecordImage();
                return base.IsEnabledCore();
            }

        }
        #endregion
    }
}
