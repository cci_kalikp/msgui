﻿using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core.Persistence; 

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell
{
    internal class RecentItemsInfo:IPersistable
    {
        private readonly RecentItemsManager recentItemManager;

        public RecentItemsInfo(RecentItemsManager recentItemManager_)
        {
            recentItemManager = recentItemManager_;
        }

        public void LoadState(XDocument state_)
        {
            recentItemManager.Clear();
            foreach (var recentItemElement in state_.Root.Descendants("RecentItemInfo"))
            {
                var recentItem = RecentItemInfo.LoadFromElement(recentItemElement);
                if (recentItem != null)
                {
                    recentItemManager.AddRecentItem(recentItem);
                }
            }
        }

        public XDocument SaveState()
        {
            XElement rootElement = new XElement(PersistorId);
            foreach (var recentItem in recentItemManager.GetItems())
            {
                XElement recentItemElement = new XElement("RecentItemInfo");
                recentItem.SaveToElement(recentItemElement);
                rootElement.Add(recentItemElement);
            }
            return new XDocument(rootElement);
        }

        public string PersistorId
        {
            get { return "RecentItemsInfo"; }
        }
    }
}
