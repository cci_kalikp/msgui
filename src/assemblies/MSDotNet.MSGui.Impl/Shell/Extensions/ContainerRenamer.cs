﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Extensions/ContainerRenamer.cs#4 $
// $Change: 895203 $
// $DateTime: 2014/09/02 03:46:50 $
// $Author: caijin $ 
using System.Windows;
using System.Windows.Input; 
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions
{
    internal class ContainerRenamer : RenamerBase
    {
 
        private UIElement headerContainer;
        protected override void ReleaseTitleChangeBinding(KeyGesture gesture_)
        {
            this.RemoveTitleChangeBinding(this.headerContainer, gesture_);
        }

        protected override void SetupTitleChangeBinding(DependencyObject associator_)
        {
            if (associator_ is ITitleDisplayer)
            {
                headerContainer = associator_ as UIElement;
            }
            if (headerContainer == null || Gesture == null) return;

            AddTitleChangeBinding(headerContainer);
        } 
      
        protected override void RenameTitle(object sender_, ExecutedRoutedEventArgs e_)
        { 
            if (headerContainer != null)
            {
                headerContainer.RaiseEvent(new RoutedEventArgs(Attached.TitleNeedChangingEvent));
            }
            e_.Handled = true;
        }

    }
}
