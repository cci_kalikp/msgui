/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Extensions/PasswordBoxBindingAssistant.cs#6 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.Windows;
using System.Windows.Controls;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions
{
  public class PasswordBoxBindingAssistant
  {
    public static readonly DependencyProperty BoundPassword =
      DependencyProperty.RegisterAttached("BoundPassword", typeof(string), typeof(PasswordBoxBindingAssistant), new FrameworkPropertyMetadata(string.Empty, OnBoundPasswordChanged));

    public static readonly DependencyProperty BindPassword = DependencyProperty.RegisterAttached(
      "BindPassword", typeof(bool), typeof(PasswordBoxBindingAssistant), new PropertyMetadata(false, OnBindPasswordChanged));

    private static readonly DependencyProperty UpdatingPassword =
      DependencyProperty.RegisterAttached("UpdatingPassword", typeof(bool), typeof(PasswordBoxBindingAssistant));

    private static void OnBoundPasswordChanged(DependencyObject d_, DependencyPropertyChangedEventArgs e_)
    {
      PasswordBox box = d_ as PasswordBox;

      // only handle this event when the property is attached to a PasswordBox   
      // and when the BindPassword attached property has been set to true   
      if (d_ == null && !GetBindPassword(d_))
      {
        return;
      }

      // avoid recursive updating by ignoring the box's changed event   
      box.PasswordChanged -= HandlePasswordChanged;

      string newPassword = (string)e_.NewValue;

      if (!GetUpdatingPassword(box))
      {
        box.Password = newPassword;
      }

      box.PasswordChanged += HandlePasswordChanged;
    }

    private static void OnBindPasswordChanged(DependencyObject dp_, DependencyPropertyChangedEventArgs e_)
    {
      // when the BindPassword attached property is set on a PasswordBox,   
      // start listening to its PasswordChanged event   

      PasswordBox box = dp_ as PasswordBox;

      if (box == null)
      {
        return;
      }

      bool wasBound = (bool)(e_.OldValue);
      bool needToBind = (bool)(e_.NewValue);

      if (wasBound)
      {
        box.PasswordChanged -= HandlePasswordChanged;
      }

      if (needToBind)
      {
        box.PasswordChanged += HandlePasswordChanged;
      }
    }

    private static void HandlePasswordChanged(object sender_, RoutedEventArgs e_)
    {
      PasswordBox box = sender_ as PasswordBox;

      // set a flag to indicate that we're updating the password   
      SetUpdatingPassword(box, true);
      // push the new password into the BoundPassword property   
      SetBoundPassword(box, box.Password);
      SetUpdatingPassword(box, false);
    }

    public static void SetBindPassword(DependencyObject dp_, bool value_)
    {
      dp_.SetValue(BindPassword, value_);
    }

    public static bool GetBindPassword(DependencyObject dp_)
    {
      return (bool)dp_.GetValue(BindPassword);
    }

    public static string GetBoundPassword(DependencyObject dp_)
    {
      return (string)dp_.GetValue(BoundPassword);
    }

    public static void SetBoundPassword(DependencyObject dp_, string value_)
    {
      dp_.SetValue(BoundPassword, value_);
    }

    private static bool GetUpdatingPassword(DependencyObject dp_)
    {
      return (bool)dp_.GetValue(UpdatingPassword);
    }

    private static void SetUpdatingPassword(DependencyObject dp_, bool value_)
    {
      dp_.SetValue(UpdatingPassword, value_);
    }
  }
}