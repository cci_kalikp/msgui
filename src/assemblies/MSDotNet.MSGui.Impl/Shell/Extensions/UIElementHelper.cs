﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Extensions/UIElementHelper.cs#4 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.Windows;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions
{
	/// <summary>
	/// Static class for dealing with UIElement related members.
	/// </summary>
	public static class UIElementHelper
	{
		/// <summary>
		/// Adds a handler for the specified event on the specified element
		/// </summary>
		/// <param name="element_">The element that listens to the event</param>
		/// <param name="routedEvent_">The event to listen for</param>
		/// <param name="handler_">The delegate to add</param>
		public static void AddHandler(DependencyObject element_, RoutedEvent routedEvent_, Delegate handler_)
		{
			if (element_ == null)
				throw new ArgumentNullException("element_");

			IInputElement inputElement = element_ as IInputElement;

			if (null != inputElement)
				inputElement.AddHandler(routedEvent_, handler_);
		}

		/// <summary>
		/// Raises an event for the specified event arguments on the specified element
		/// </summary>
		/// <param name="element_">The element that listens to the event</param>
		/// <param name="e_">The arguments for the routed event to be raised</param>
		public static void RaiseEvent(DependencyObject element_, RoutedEventArgs e_)
		{
			if (element_ == null)
				throw new ArgumentNullException("element_");

			IInputElement inputElement = element_ as IInputElement;

			if (null != inputElement)
				inputElement.RaiseEvent(e_);
		}
		
		/// <summary>
		/// Removes a handler for the specified event on the specified element
		/// </summary>
		/// <param name="element_">The element that listens to the event</param>
		/// <param name="routedEvent_">The event to unhook from</param>
		/// <param name="handler_">The delegate to remove</param>
		public static void RemoveHandler(DependencyObject element_, RoutedEvent routedEvent_, Delegate handler_)
		{
			if (element_ == null)
				throw new ArgumentNullException("element_");

			IInputElement inputElement = element_ as IInputElement;

			if (null != inputElement)
				inputElement.RemoveHandler(routedEvent_, handler_);
		}		
	}
}
