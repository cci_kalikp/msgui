﻿using System;
using System.Windows;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions
{
    public static class XamRibbonWindowExtensions
    {
        public static void SetStatusBarPadding(UIElement element_, Thickness? value_)
        {
            element_.SetValue(StatusBarPaddingProperty, value_);
        }

        public static Thickness? GetStatusBarPadding(UIElement element_)
        {
            return (Thickness?)element_.GetValue(StatusBarPaddingProperty);
        }

        public static readonly DependencyProperty StatusBarPaddingProperty =
            DependencyProperty.RegisterAttached(
                "StatusBarPadding", 
                typeof(Thickness?), 
                typeof(XamRibbonWindowExtensions), 
                new FrameworkPropertyMetadata(null));
    }
}
