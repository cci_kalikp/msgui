﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input; 

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions
{
    internal abstract class RenamerBase: Freezable
    {
        public static readonly DependencyProperty GestureProperty = DependencyProperty.Register("Gesture",
          typeof(KeyGesture), typeof(RenamerBase), new FrameworkPropertyMetadata(OnGestureChanged));
        public KeyGesture Gesture
        {
            get
            {
                return (KeyGesture)GetValue(GestureProperty);
            }
            set
            {
                SetValue(GestureProperty, value);
            }
        }
        private static void OnGestureChanged(DependencyObject d_, DependencyPropertyChangedEventArgs e_)
        {
            if (e_.NewValue == e_.OldValue) return;

            if (d_ is RenamerBase && e_.OldValue is KeyGesture)
            {
                KeyGesture oldGesture = ((KeyGesture)e_.OldValue);
                ((RenamerBase)d_).ReleaseTitleChangeBinding(oldGesture);
            }

            if (d_ is RenamerBase && e_.NewValue is KeyGesture)
            {
               ((RenamerBase)d_).SetupTitleChangeBinding(null);
            }
        }


        public static readonly DependencyProperty RenamerProperty = DependencyProperty.RegisterAttached(
 "Renamer", typeof(RenamerBase), typeof(RenamerBase), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.None, RenamerPropertyChanged));
         
        private static void RenamerPropertyChanged(DependencyObject d_, DependencyPropertyChangedEventArgs e_)
        {
            if (e_.NewValue == e_.OldValue) return;
            if (e_.OldValue is RenamerBase)
            {
                var oldRenamer = ((RenamerBase)e_.OldValue);
                oldRenamer.ReleaseTitleChangeBinding(oldRenamer.Gesture);
            }
            RenamerBase newRenamer = e_.NewValue as RenamerBase;
            if (newRenamer == null) return;
            newRenamer.SetupTitleChangeBinding(d_); 
        }

        public static RenamerBase GetRenamer(DependencyObject d_)
        {
            return (RenamerBase)d_.GetValue(RenamerProperty);
        }

 
        public static void SetRenamer(DependencyObject d_, RenamerBase value_)
        {
            d_.SetValue(RenamerProperty, value_);
        }


        protected void AddTitleChangeBinding(UIElement element_)
        {
            InputBinding ib = new InputBinding(Command.RenameTitleCommand, Gesture);
            element_.InputBindings.Add(ib);
            CommandBinding cb = new CommandBinding(Command.RenameTitleCommand, RenameTitle, (sender_, e_) => e_.CanExecute = true);
            element_.CommandBindings.Add(cb);
        }


        protected override Freezable CreateInstanceCore()
        {
            return  Activator.CreateInstance(this.GetType()) as Freezable;
        }

        protected void RemoveTitleChangeBinding(UIElement element_, KeyGesture oldGesture_)
        {
            foreach (var binding in new ArrayList(element_.InputBindings))
            {
                InputBinding inputBinding = binding as InputBinding;
                if (inputBinding != null && inputBinding.Gesture == oldGesture_)
                {
                    element_.InputBindings.Remove(inputBinding);
                }
            }
            foreach (var binding in new ArrayList(element_.CommandBindings))
            {
                CommandBinding commandBinding = binding as CommandBinding;
                if (commandBinding != null && commandBinding.Command == Command.RenameTitleCommand)
                {
                    element_.CommandBindings.Remove(commandBinding);
                }
            }

            CommandBinding cb = new CommandBinding(Command.RenameTitleCommand, RenameTitle, (sender_, e_) => e_.CanExecute = true);
            element_.CommandBindings.Add(cb);
        }


        protected abstract void RenameTitle(object sender_, ExecutedRoutedEventArgs e_);

        protected abstract void ReleaseTitleChangeBinding(KeyGesture gesture_);

        protected abstract void SetupTitleChangeBinding(DependencyObject associator_);
    }
}
