﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Extensions/NativeUiMethodsHelper.cs#1 $
// $Change: 870935 $
// $DateTime: 2014/03/12 15:00:16 $
// $Author: hrechkin $

using System;
using System.Runtime.InteropServices;
using System.Security;
using System.Windows;
using MorganStanley.MSDotNet.MSGui.Core.Win32;
using Point = System.Drawing.Point;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions
{
	/// <summary>
	/// A class that contains only static methods that expose some
	/// native windows apis that relate to graphics 
	/// </summary>
	[SuppressUnmanagedCodeSecurity]
	[ComVisible(false)]
	internal static class NativeUiMethodsHelper
	{
		#region Apis

		#region WindowFromPoint

		internal static IntPtr WindowFromPointApi(Point point)
		{
			// AS 7/27/04
			// WindowFromPoint does not consider disabled windows but we actually need to
			// consider them - e.g. when the customize dialog is displayed modally.
			//
			//return WindowFromPoint(point);
			// MD 10/31/06 - 64-Bit Support
			//IntPtr hwnd = WindowFromPoint( point );
			var hwnd = Win32.WindowFromPoint(Win32.POINT.FromPoint(point));
			hwnd = GetRealWindowFromPoint(hwnd, point);

			return hwnd;
		}

		#endregion WindowFromPoint

		// AS 7/27/04
		// WindowFromPoint does not consider disabled windows but we actually need to
		// consider them when the customize dialog is displayed modally.
		//
		#region GetRealWindowFromPoint

		/// <summary>
		/// Recursive routine used to find the deepest child window that contains the specified point.
		/// </summary>
		/// <param name="hwndParent">Parent window handle</param>
		/// <param name="screenPt">Screen coordinates</param>
		/// <returns>The actual descendant window at the specified screen coordinates.</returns>
		private static IntPtr GetRealWindowFromPoint(IntPtr hwndParent, Point screenPt)
		{
			// convert the coordinates to client coordinates
			Point clientPt = PointToClientHelper(hwndParent, screenPt);

			// use the api to determine the actual child at that point
			// AS 9/7/06
			// In VS2005, using RealChildWindowFromPoint is returning the overlay window instead of 
			// the form so we'll use ChildWindowFromPointEx instead.
			// 
			//IntPtr childHwnd = NativeWindowMethods.RealChildWindowFromPoint(hwndParent, clientPt.X, clientPt.Y);
            IntPtr childHwnd = ChildWindowFromPointEx(hwndParent, clientPt.X, clientPt.Y, Win32.CWP_SKIPINVISIBLE | Win32.CWP_SKIPTRANSPARENT);

			// if there is none, then return the parent
			if (childHwnd == IntPtr.Zero || childHwnd == hwndParent)
				return hwndParent;

			// otherwise see if that window has any children at the specified screen location
			return GetRealWindowFromPoint(childHwnd, screenPt);
		}

		// MD 10/31/06
		// This didnt seem to be used anywhere
		//[DllImport("user32")]
		//private static extern IntPtr RealChildWindowFromPoint(IntPtr hwnd, int parentX, int parentY);

		// AS 9/7/06
		// In VS2005, using RealChildWindowFromPoint is returning the overlay window instead of 
		// the form so we'll use ChildWindowFromPointEx instead.
		// 
		[DllImport("user32")]
		private static extern IntPtr ChildWindowFromPointEx(IntPtr hwnd, int parentX, int parentY, int flags);

		#endregion //GetRealWindowFromPoint

		#region PointToClientHelper
		/// <summary>
		/// Helper routine for converting screen coordinates into client coordinates.
		/// </summary>
		/// <param name="hwnd">Window whose client coordinates should be returned.</param>
		/// <param name="pt">Point in screen coordinates</param>
		/// <returns>Returns a point in client coordinates of the specified window</returns>
		private static Point PointToClientHelper(IntPtr hwnd, Point pt)
		{
			var newPt = new Win32.POINT {X = pt.X, Y = pt.Y};
		    Win32.MapWindowPoints(IntPtr.Zero, hwnd, ref newPt, 1);

			return new Point(newPt.X, newPt.Y);
		}

		#endregion //PointToClientHelper
		#endregion Apis

		#region Helper Methods

		#region GetXFromLParam
		internal static int GetXFromLParam(IntPtr lParam)
		{
			// explicitly casted to short to maintain sign
			// AS 11/7/08 TFS10274
			//return (short)(lParam.ToInt32() & 0xffff);
			return (short)(lParam.ToInt64() & 0xffff);
		}
		#endregion //GetXFromLParam

		#region GetYFromLParam
		internal static int GetYFromLParam(IntPtr lParam)
		{
			// explicitly casted to short to maintain sign
			// AS 11/7/08 TFS10274
			//return (short)((lParam.ToInt32() >> 16) & 0xffff);
			return (short)((lParam.ToInt64() >> 16) & 0xffff);
		}
		#endregion //GetYFromLParam

		#region GetWindowState
		internal static WindowState GetWindowState(IntPtr hwnd)
		{
			WindowState state;

			var placement = new Win32.WindowPlacement();
			placement.Length = Marshal.SizeOf(placement);
			bool retVal = Win32.GetWindowPlacement(hwnd, ref placement);

			if (placement.ShowCmd == Win32.SW_SHOWMINIMIZED)
				state = WindowState.Minimized;
            else if (placement.ShowCmd == Win32.SW_SHOWMAXIMIZED)
				state = WindowState.Maximized;
			else
			{
				state = WindowState.Normal;
			}

			return state;
		}
		#endregion //GetWindowState
		#endregion // Helper Methods
	}
}