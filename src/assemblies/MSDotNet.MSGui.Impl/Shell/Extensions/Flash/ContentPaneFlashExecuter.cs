﻿using System;
using System.Windows; 
using System.Windows.Media.Animation;
using System.Windows.Threading;
using Infragistics.Windows.DockManager;
using MorganStanley.MSDotNet.MSGui.Impl.Alert;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions.Flash
{
    class ContentPaneFlashExecuter : FlashExecuterBase<ContentPane>
    {
        private static readonly FlashingElements<ContentPane> panesFlashing = new FlashingElements<ContentPane>();
        private static readonly FlashingElements<PaneTabItem> tabItemsFlashing = new FlashingElements<PaneTabItem>(); 

        protected override bool CanFlash(ContentPane pane_)
        {
            return true;
        }
        protected override bool IsFlashing(ContentPane pane_)
        {
            return panesFlashing.Contains(pane_);
        }

        protected override void ExecuteFlash(ContentPane pane_, System.Windows.Media.Color color_)
        {
            FlashTaskBar.Flash(Window.GetWindow(pane_), 15);
            var sb = pane_.TryFindResource("FlashWindow1") as Storyboard;
            var sb2 = pane_.TryFindResource("SaturateWindow1") as Storyboard;
            if (sb != null && sb2 != null)
            {
                sb = sb.Clone();
                sb2 = sb2.Clone();
                ((ColorAnimationUsingKeyFrames)sb.Children[0]).KeyFrames[0].Value =
                ((ColorAnimationUsingKeyFrames)sb2.Children[0]).KeyFrames[0].Value = color_;
                panesFlashing.StartFlash(pane_, "outerGrid", sb, sb2);
            }

            PaneTabItem pti = XamDockHelper.GetTabItem(pane_);
            if (pti == null) return;
            sb = pti.TryFindResource("FlashTab") as Storyboard;
            sb2 = pti.TryFindResource("SaturateTab") as Storyboard;
            if (sb == null || sb2 == null) return;
            sb = sb.Clone();
            sb2 = sb2.Clone();
            ((ColorAnimationUsingKeyFrames)sb.Children[0]).KeyFrames[0].Value =
            ((ColorAnimationUsingKeyFrames)sb2.Children[0]).KeyFrames[0].Value = color_;
            tabItemsFlashing.StartFlash(pti, "Border", sb, sb2); 
        }

        protected override void StopFlash(ContentPane pane_)
        {
            Window lastwindow = Window.GetWindow(pane_);
            FlashTaskBar.Stop(lastwindow);
            lastwindow.ShowInTaskbar = !lastwindow.ShowInTaskbar;
            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.ApplicationIdle,
                (Action<Window>)(w_ => w_.ShowInTaskbar = !w_.ShowInTaskbar), lastwindow);

            var sb = pane_.TryFindResource("DesaturateWindow1") as Storyboard;
            if (sb == null) return;
            panesFlashing.StopFlash(pane_, "outerGrid", sb);

            PaneTabItem pti = XamDockHelper.GetTabItem(pane_);
            if (pti == null) return;
            var sb2 = (pti).TryFindResource("DesaturateTab") as Storyboard;
            tabItemsFlashing.StopFlash(pti, "Border", sb2);  
        }
         
    }
}
