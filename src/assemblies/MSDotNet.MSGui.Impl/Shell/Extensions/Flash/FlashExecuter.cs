﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Threading;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions.Flash
{
    internal abstract class FlashExecuterBase<T>  where T: FrameworkElement
    {
        private DispatcherTimer dispatcherTimer;
        private readonly Queue<Tuple<T, Color>> panesToFlash = new Queue<Tuple<T, Color>>(); 

        public void Start()
        {
            dispatcherTimer = new DispatcherTimer(new TimeSpan(0, 0, 0, 4), DispatcherPriority.Render, DoExecuteFlash,
                                                    Dispatcher.CurrentDispatcher) { IsEnabled = true };
            dispatcherTimer.Start();
        }

        public void Stop()
        {
            if (dispatcherTimer == null) return;
            dispatcherTimer.Stop();
            dispatcherTimer.IsEnabled = false;
            dispatcherTimer = null; 
        }


        public void Flash(T pane_, Color color_)
        {
            lock (panesToFlash)
            {
                if (!panesToFlash.Any(toFlash_ => toFlash_.Item1 == pane_) &&
                    !IsFlashing(pane_))
                {
                    panesToFlash.Enqueue(new Tuple<T, Color>(pane_, color_));
                }
            }
        }

        public void PaneActivated(T pane_)
        {
            if (pane_ == null || !IsFlashing(pane_)) return;
            try
            {
                StopFlash(pane_);
            }
            catch
            {
            }
 
        }


        private void DoExecuteFlash(object sender_, EventArgs args_)
        {
            lock (panesToFlash)
            {
                if (panesToFlash.Count == 0) return;
                var current = panesToFlash.Dequeue();
                if (current != null)
                {
                    try
                    {
                        ExecuteFlash(current.Item1, current.Item2); 
                    }
                    catch  
                    { 
                    }
                }
            }
        }

        protected abstract bool CanFlash(T pane_);

        protected abstract bool IsFlashing(T pane_);

        protected abstract void StopFlash(T pane_);

        protected abstract void ExecuteFlash(T pane_, Color color_);


        protected class FlashingElements<TElement> where TElement : FrameworkElement
        {
            private readonly Dictionary<TElement, Storyboard[]> flashings = new Dictionary<TElement, Storyboard[]>();
            public void StartFlash(TElement element_, string flashingElementName_, Storyboard flashStoryboard_, Storyboard saturateStoryboard_)
            {
                lock (flashings)
                {
                    if (flashings.ContainsKey(element_)) return;
                    foreach (DependencyObject dependencyObject in element_.FindVisualChildren(
                        x_ => x_.GetValue(FrameworkElement.NameProperty).ToString() == flashingElementName_))
                    {
                        EventHandler completedHandler = null;
                        flashStoryboard_.Completed += completedHandler = delegate
                        {
                            lock (flashings)
                            {
                                // The following exception may be thrown when we change themes while tile item blink
                                //'windowInnerBorder' name cannot be found in the name scope of 'System.Windows.Controls.Grid'.
                                // Therefore we refind the DO
                                foreach (DependencyObject dependencyObject2 in element_.FindVisualChildren(
                                    x_ => x_.GetValue(FrameworkElement.NameProperty).ToString() == flashingElementName_))
                                {
                                    if (flashings.ContainsKey(element_))
                                        saturateStoryboard_.Begin((FrameworkElement)dependencyObject2, true);
                                }
                            }
                            flashStoryboard_.Completed -= completedHandler;
                        };
                        flashStoryboard_.Begin((FrameworkElement)dependencyObject, true);
                    }

                    flashings.Add(element_, new Storyboard[2] { flashStoryboard_, saturateStoryboard_ });
                }
            }

            public bool StopFlash(TElement element_, string flashingElementName_, Storyboard desaturateStoryboard_)
            {
                Storyboard[] storyboards;
                if (!flashings.TryGetValue(element_, out storyboards))
                {
                    return false;
                }

                lock (flashings)
                {
                    flashings.Remove(element_);
                }
                foreach (DependencyObject dependencyObject in element_.FindVisualChildren(
              x_ => x_.GetValue(FrameworkElement.NameProperty).ToString() == flashingElementName_))
                {
                    foreach (var storyboard in storyboards)
                    {
                        storyboard.Stop((FrameworkElement)dependencyObject);
                    }
                    if (desaturateStoryboard_ != null)
                    {
                        desaturateStoryboard_.Begin((FrameworkElement)dependencyObject);
                    }
                }
                return true;
            }

            public bool Contains(TElement element_)
            {
                return this.flashings.ContainsKey(element_);
            }
        }

    }
}
