﻿ 
using System.Windows.Media.Animation; 
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions.Flash
{
    class TileItemFlashExecuter : FlashExecuterBase<TileItem>
    {
        private static readonly FlashingElements<TileItem> panesFlashing = new FlashingElements<TileItem>(); 

        protected override bool CanFlash(TileItem pane_)
        {
            return pane_.ViewModel.ShowFlashBorder;
        }
        protected override bool IsFlashing(TileItem pane_)
        {
            return panesFlashing.Contains(pane_);
        }

        protected override void ExecuteFlash(TileItem pane_, System.Windows.Media.Color color_)
        {
            var sb = pane_.TryFindResource("FlashWindow1") as Storyboard;
            var sb2 = pane_.TryFindResource("SaturateWindow1") as Storyboard;
            if (sb == null || sb2 == null) return;
            sb = sb.Clone();
            sb2 = sb2.Clone();
            ((ColorAnimationUsingKeyFrames)sb.Children[0]).KeyFrames[0].Value =
                ((ColorAnimationUsingKeyFrames)sb2.Children[0]).KeyFrames[0].Value = color_;
            panesFlashing.StartFlash(pane_, "outerGrid", sb, sb2);  
        }

        protected override void StopFlash(TileItem pane_)
        {
            var sb = pane_.TryFindResource("DesaturateWindow1") as Storyboard;
            if (sb == null) return;
            panesFlashing.StopFlash(pane_, "outerGrid", sb); 
        }

       
    }
}
