﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Extensions/MaximizeFloatingWindowExtensionAdapter.cs#33 $
// $Change: 896821 $
// $DateTime: 2014/09/15 02:20:20 $
// $Author: caijin $

using System;
using System.Collections.Generic; 
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interop; 
using Infragistics.Windows.Controls;
using Infragistics.Windows.DockManager;
using Infragistics.Windows.DockManager.Events;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Win32;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions
{
	internal enum FullScreenDockedMode
	{
		FullScreenFloating,
		FullScreenFloatingDisableActivate
	}

	internal class MaximizeFloatingWindowExtensionAdapter
	{
		private XamDockManager m_dockManager;
		private FullScreenDockedMode m_mode = FullScreenDockedMode.FullScreenFloatingDisableActivate;
		private CommandBinding m_maximizeCommandBinding;

		public MaximizeFloatingWindowExtensionAdapter(XamDockManager dockManager_)
		{
			m_dockManager = dockManager_;
		}

		public void Attach()
		{
			if (m_dockManager.IsLoaded)
			{
				AttachMainWindow(null, null);
			}
			m_dockManager.Loaded += AttachMainWindow;
			m_dockManager.Unloaded += DetachMainWindow;


			IEnumerable<ToolWindow> allToolWindows = m_dockManager.GetAllToolWindows();
			foreach (var toolWindow in allToolWindows)
			{
				SetToolWindowBindings(toolWindow);
			}
			m_dockManager.ToolWindowLoaded += PaneToolWindowLoaded; 
            m_dockManager.ToolWindowUnloaded += PaneToolWindowUnloaded;
		    if (!FloatingWindowUtil.EnableNonOwnedWindows)
		    {
		        m_dockManager.PaneDragStarting += OnPaneDragStarting;
		        m_dockManager.PaneDragEnded += OnPaneDragEnded;
		    }
		}



	    public void Detach()
		{
			if (m_dockManager == null)
			{
				return;
			}
			m_dockManager.Loaded -= AttachMainWindow;
			m_dockManager.Unloaded -= DetachMainWindow;

			m_dockManager.ToolWindowLoaded -= PaneToolWindowLoaded; 
            m_dockManager.ToolWindowUnloaded -= PaneToolWindowUnloaded;
			m_dockManager.PaneDragStarting -= OnPaneDragStarting;
			m_dockManager.PaneDragEnded -= OnPaneDragEnded;
			//it'd be nice to remove tool window binding too
		}

		//TODO review and rewrite all methods below. maximization is very buggy

		private void AttachMainWindow(object x_, RoutedEventArgs y_)
		{
			//Window win = Window.GetWindow(m_dockManager);

			//win.SourceInitialized += OnMainWindowSourceInitialized;

			//m_maximizeCommandBinding = new CommandBinding(
			//  Command.MaximizeWindowCommand,
			//  HandleMaximizeWindowCommand,
			//  CanExecute);

			//win.CommandBindings.Add(m_maximizeCommandBinding);
		}

		private void DetachMainWindow(object x_, RoutedEventArgs y_)
		{
			//if (m_mainWindow != null)
			//{
			//  m_mainWindow.SourceInitialized -= OnMainWindowSourceInitialized;
			//  if (m_maximizeCommandBinding != null)
			//  {
			//    m_mainWindow.CommandBindings.Remove(m_maximizeCommandBinding);
			//  }        
			//}
		}

		void OnPaneDragEnded(object sender, Infragistics.Windows.DockManager.Events.PaneDragEndedEventArgs e)
		{
			paneDragging = false;
			var win = Window.GetWindow(m_dockManager);

			if (m_windowHidden)
			{
				win.WindowState = WindowState.Minimized;
				//  win.Show();
				//  win.WindowState = WindowState.Minimized;
				m_windowHidden = false;

				win.Width = oldWidth;
				win.Height = oldHeight;
				win.Top = oldTop;
				win.Left = oldLeft;
			}
		}

		bool m_windowHidden = false;
		Visibility oldVisible = Visibility.Collapsed;
		double oldWidth = 0;
		double oldHeight = 0;

		double oldLeft = 0;
		double oldTop = 0;

		void OnPaneDragStarting(object sender, Infragistics.Windows.DockManager.Events.PaneDragStartingEventArgs e)
		{
			paneDragging = true;

			var win = Window.GetWindow(m_dockManager);

			if (win.WindowState == WindowState.Minimized)
			{
				oldWidth = win.Width;
				oldHeight = win.Height;

				oldLeft = win.Left;
				oldTop = win.Top;

				win.Width = 1;
				win.Height = 1;
				win.MinWidth = 0;
				win.Left = -100;
				win.Top = -100;

				win.WindowState = WindowState.Normal;
				//oldVisible = win.Visibility;
				//win.Visibility = Visibility.Hidden;

				m_windowHidden = true;
			}
		}

		private void PaneToolWindowLoaded(object sender, PaneToolWindowEventArgs e)
		{
			ToolWindow window = ToolWindow.GetToolWindow(e.Window);
			SetToolWindowBindings(window);
		}


        //this is needed, so that those toolwindows that are never really loaded,
        //(created when loading a layout but is never loaded since the pane becomes docked later on)
        //would have a chance to dispose
        private void PaneToolWindowUnloaded(object sender_, PaneToolWindowEventArgs e_)
        {
            ToolWindow window = ToolWindow.GetToolWindow(e_.Window);
            if (window != null)
            {
                window.Loaded -= OnToolWindowLoaded;
            } 
        }

		private void SetToolWindowBindings(ToolWindow toolWindow_)
		{
			CommandBinding cb = new CommandBinding(
			  Command.MaximizeWindowCommand,
			  HandleMaximizeWindowCommand,
			  CanExecute);

			toolWindow_.CommandBindings.Add(cb);

			CommandBinding cb2 = new CommandBinding(
			  Command.MinimizeWindowCommand,
			  HandleMinimizeWindowCommand,
			  CanExecute);

			toolWindow_.CommandBindings.Add(cb2);

			CommandBinding cb3 = new CommandBinding(Command.TearBackCommand,
													HandleTearBackCommand,
													CanExecute);

			toolWindow_.CommandBindings.Add(cb3); 
			var loadedWindow = Window.GetWindow(toolWindow_);
			if (loadedWindow != null)
			{
				if (loadedWindow.IsLoaded)
				{
					SetHandle(loadedWindow);
				}
				else
				{
					loadedWindow.Loaded += OnToolWindowsWindowLoaded;
				}
			}
			else
			{
				toolWindow_.Loaded += OnToolWindowLoaded;
			}
		}

		private void HandleTearBackCommand(object o, ExecutedRoutedEventArgs e)
		{
			PaneToolWindow cp = o as PaneToolWindow;

			ContentPane casd = cp.Pane.Panes[0] as ContentPane;
			if (casd != null)
			{
				casd.ExecuteCommand(ContentPaneCommands.ToggleDockedState);
			}
			e.Handled = true;
		}

		private void CanExecute(object o, CanExecuteRoutedEventArgs x)
		{
			x.CanExecute = true;
			x.Handled = true;
		}

		void OnToolWindowLoaded(object sender, RoutedEventArgs e)
		{
			Window win = Window.GetWindow(e.Source as DependencyObject);
			if (win.IsLoaded)
			{
				SetHandle(win);
			}
			else
			{
				win.Loaded += OnToolWindowsWindowLoaded;
			}
		}

		private void OnToolWindowsWindowLoaded(object sender_, RoutedEventArgs routedEventArgs_)
		{
			Window win = routedEventArgs_.Source as Window;
			if (win != null)
			{
				SetHandle(win);
				win.Loaded -= OnToolWindowsWindowLoaded;
			}
		}

		void SetHandle(Window win_)
		{
			if (m_dockManager.IsEnabled)
			{
				//TODO: FARM Why was this here?
				//win_.WindowStartupLocation = WindowStartupLocation.CenterScreen;
				win_.WindowStartupLocation = WindowStartupLocation.Manual;
			}

			win_.StateChanged += new EventHandler(win__StateChanged);
			win_.Closing += new System.ComponentModel.CancelEventHandler(win__Closing);
            FloatingWindowUtil.FixResize(win_);
            FloatingWindowUtil.FixRestoreBounds(win_);
		    IFloatingWindow floatingWindow = win_.Content as IFloatingWindow;
            if (floatingWindow != null)
            {
                FloatingWindowUtil.RegisterFloatingWindow(floatingWindow);
            }
            win_.EnableMinimizeScreenshotSupport();
		}
         

	    void win__Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			((Window)sender).Closing -= win__Closing;
			((Window)sender).StateChanged -= win__StateChanged;
		}

		void win__StateChanged(object sender, EventArgs e)
		{
			Window win = ((Window)sender);
			if (win.WindowState == WindowState.Normal)
				if ((win.Tag != null) && (win.Tag is Rect))
				{
					Rect rect = (Rect)win.Tag;
					win.Left = rect.Left;
					win.Top = rect.Top;
				}
		}


		void HandleMinimizeWindowCommand(object o, ExecutedRoutedEventArgs e)
		{
			var win = Window.GetWindow(o as DependencyObject);
			win.WindowState = WindowState.Minimized;
			/*
			var win = Window.GetWindow(o as DependencyObject);
			var tool = ToolWindow.GetToolWindow(o as DependencyObject);

			win.SizeToContent = SizeToContent.Manual;
			if (win.WindowState == WindowState.Minimized)
			{

				win.WindowState = WindowState.Normal;
			}
			else
			{
				win.Tag = win.RestoreBounds;
				win.WindowState = WindowState.Minimized;
			}
			if (e != null)
			{
				e.Handled = true;
			}*/
		}

		void HandleMaximizeWindowCommand(object o, ExecutedRoutedEventArgs e)
		{
			var win = Window.GetWindow(o as DependencyObject);
			win.WindowState = (win.WindowState == WindowState.Maximized) ? WindowState.Normal : WindowState.Maximized;
			/*
			var win = Window.GetWindow(o as DependencyObject);
			var tool = ToolWindow.GetToolWindow(o as DependencyObject);

			win.ShowInTaskbar = true;

			win.Title = tool.Title;

			win.SizeToContent = SizeToContent.Manual;
			win.ResizeMode = ResizeMode.CanResizeWithGrip;

			if (win.Tag is Rect)
			{
				var res = (Rect)win.Tag;
				win.Top = res.Top;
				win.Left = res.Left;
				win.Width = res.Width;
				win.Height = res.Height;
				win.Tag = null;
			}
			else
			{
				Rectangle workingArea = System.Windows.Forms.Screen.FromRectangle(
				  new Rectangle(
					Convert.ToInt32(win.Left),
					Convert.ToInt32(win.Top),
					Convert.ToInt32(win.ActualWidth),
					Convert.ToInt32(win.ActualHeight))).WorkingArea;
				win.Tag = win.RestoreBounds;
				win.SetValue(Window.WidthProperty, (Double)workingArea.Width);
				win.SetValue(Window.HeightProperty, (Double)workingArea.Height);
				win.SetValue(Window.TopProperty, (Double)workingArea.Top);
				win.SetValue(Window.LeftProperty, (Double)workingArea.Left);
				//win.WindowState = WindowState.Maximized;
			}*/
		}

		IntPtr m_mainWindowHandle;
		Window m_mainWindow;
		bool paneDragging;

	    private void OnMainWindowSourceInitialized(object sender, EventArgs e)
		{
			var window = sender as Window;
			m_mainWindow = window;
			m_mainWindowHandle = (new WindowInteropHelper(window)).Handle;
			//iansau: FIXFIX
			HwndSource.FromHwnd(m_mainWindowHandle).AddHook(new HwndSourceHook(MainWinProc));
		}

		private System.IntPtr MainWinProc(
		  System.IntPtr hwnd,
		  int msg,
		  System.IntPtr wParam,
		  System.IntPtr lParam,
		  ref bool handled)
		{

			if (msg == Win32.WM_WINDOWPOSCHANGING)
			{
				bool disable = true;

				if (FocusManager.GetIsFocusScope(m_mainWindow))
				{
					disable = false;
				}

				if (m_dockManager.ActivePane != null && !FocusManager.GetIsFocusScope(m_dockManager.ActivePane))
				{
					disable = true;
				}
				else
					if (m_dockManager.ActivePane == null && FocusManager.GetIsFocusScope(m_mainWindow))
					{
						disable = false;
					}

				if (paneDragging)
				{
					disable = true;
				}

				if (disable)
				{
					handled = true;

                    var val = (Win32.WINDOWPOS)Marshal.PtrToStructure(lParam, typeof(Win32.WINDOWPOS));

                    val.flags |= Win32.SetWindowsPosFlags.SWP_NOZORDER;

                    var val2 = (Win32.WINDOWPOS)Marshal.PtrToStructure(lParam, typeof(Win32.WINDOWPOS));

					Marshal.StructureToPtr(val, lParam, true);
					return Win32.DefWindowProc(hwnd, msg, wParam, lParam);
				}
			}

			return (System.IntPtr)0;
		}


	}
}