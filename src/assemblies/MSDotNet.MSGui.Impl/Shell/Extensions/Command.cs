﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Extensions/Command.cs#16 $
// $Change: 902932 $
// $DateTime: 2014/10/29 06:32:59 $
// $Author: caijin $

using System.Windows;
using System.Windows.Input;
using System.Windows.Controls;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions
{
    public static class Command
    {
        public static readonly RoutedUICommand MaximizeWindowCommand = new RoutedUICommand("Maximize Window", "MaximizeWindowCommand", typeof(Button));

        public static readonly RoutedUICommand MinimizeWindowCommand = new RoutedUICommand("Minimize Window", "MinimizeWindowCommand", typeof(Button));

        public static readonly RoutedUICommand MaximizeViewCommand = new RoutedUICommand("Maximize View", "MaximizeViewCommand", typeof(Button));

        public static readonly RoutedUICommand TearBackCommand = new RoutedUICommand("Tear Back", "TearBackCommand", typeof(Button));

        public static readonly RoutedUICommand AddTabCommand = new RoutedUICommand("Add Tab", "AddTabCommand", typeof(Button));

        public static readonly RoutedUICommand FocusViewCommand = new RoutedUICommand("Focus View", "FocusViewCommand", typeof(Button));

        public static readonly RoutedCommand RenameTabCommand = new RoutedUICommand();

        public static readonly RoutedCommand RenameTitleCommand = new RoutedCommand(); 

        #region Legacy Workspace
        public static readonly RoutedCommand SaveSublayoutCommand = new RoutedUICommand("Save as sublayout", "SaveSublayoutCommand", typeof(FrameworkElement));
        #endregion
        public static readonly RoutedCommand CloseLastInTabGroupCommand = new RoutedUICommand("close", "CloseLastActiveInTabCommand", typeof(Button));
    }
}
