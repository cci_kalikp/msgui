﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Extensions/MVVMDock/ItemBinding.cs#4 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.Windows.Data;
using System.Windows;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions
{
	/// <summary>
	/// Class that provides information about a binding.
	/// </summary>
	public class ItemBinding
	{
		/// <summary>
		/// Returns or sets the binding to the underlying item that will be set as the binding for the <see cref="TargetProperty"/>
		/// </summary>
		public Binding Binding
		{
			get;
			set;
		}

		/// <summary>
		/// Returns or sets the base type for the item to which the TargetProperty will be bound.
		/// </summary>
		/// <remarks>
		/// <para>This property defaults to null which means that it can be applied to any item. This property 
		/// is intended to be used in a situation where the source collection can items of different types and 
		/// the binding only applies to items of a given source type.</para>
		/// </remarks>
		public Type SourceType
		{
			get;
			set;
		}

		/// <summary>
		/// Returns or sets the base type for the container on which this binding may be applied.
		/// </summary>
		public Type TargetContainerType
		{
			get;
			set;
		}

		/// <summary>
		/// Returns or sets the property that will be set to the specified <see cref="Binding"/>
		/// </summary>
		public DependencyProperty TargetProperty
		{
			get;
			set;
		}

		internal bool CanApply(DependencyObject container_, object item_)
		{
			if (TargetProperty == null || Binding == null)
				return false;

			if (container_ != null && TargetContainerType != null && !TargetContainerType.IsAssignableFrom(container_.GetType()))
				return false;

			if (item_ != null && SourceType != null && !SourceType.IsAssignableFrom(item_.GetType()))
				return false;

			return true;
		}
	}
}
