﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Forms;
using System.Windows.Forms.Integration; 
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.DockManager;
using MorganStanley.MSDotNet.MSGui.Impl.DockManager.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.DockManager.Services;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.Converters;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions.Flash;
using MorganStanley.MSDotNet.My;
using Binding = System.Windows.Data.Binding;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions.MVVMDock
{
    public class HarmoniaContentPaneFactory:PaneFactory
    {
        private static readonly IMSLogger Logger = MSLoggerFactory.CreateLogger<HarmoniaContentPaneFactory>();

        static HarmoniaContentPaneFactory()
        {
            ContainerTypeProperty.OverrideMetadata(typeof(HarmoniaContentPaneFactory), new FrameworkPropertyMetadata(typeof(ContentPane)));

        }

        private readonly DockViewModel manager;
        private readonly HamoniaContentPaneFlashExecuter flashExecuter = new HamoniaContentPaneFlashExecuter();
        internal HarmoniaContentPaneFactory(DockViewModel manager_)
        {
            DockManager.Services.DockManager.FloatingWindowLoaded += OnFloatingWindowLoaded;
            DockManager.Services.DockManager.ActivePaneChanged += OnActivePaneChanged;
            DockManager.Services.DockManager.FloatingWindowCreated += OnFloatingWindowCreated;
            this.manager = manager_;
            ItemsSource = manager_.Workspaces;
            ClosedWorkSpace = manager_.WindowClosedCommand;
            ClosingWorkSpace = manager_.WindowClosingCommand;
            EnforceSizeRestrictions = manager_.EnforceSizeRestrictions; 
            flashExecuter.Start();
            RenamerBase.SetRenamer(this, new HarmoniaWindowRenamer(){Gesture = manager_.WindowRenameGesture});
        }


        private void OnFloatingWindowLoaded(object sender_, FloatingWindowEventArgs floatingWindowEventArgs_)
        {

            if (!EnforceSizeRestrictions) return;
            var w = floatingWindowEventArgs_.Window;
            AddSizeRestrictionsHandler(w);
            EventHandler closed = null;
            w.Closed += closed = (o_, args_) =>
            {
                RemoveSizeRestrictionsHandler(w);
                w.Closed -= closed;
            };
            
        }

        public override void ShowHideHeaderOnCCPs(Visibility visibility_)
        {
            foreach (ContentPane pane in GetElements())
            {
                pane.IsHeaderVisible = visibility_ == Visibility.Visible;
            }
        }
        private void OnActivePaneChanged(object sender_, ActivePaneChangedEventArgs activePaneChangedEventArgs_)
        {
            HandleActivePaneChanged(sender_, activePaneChangedEventArgs_.OldPane, activePaneChangedEventArgs_.NewPane);
        }
         
        protected override void OnItemInserted(DependencyObject container_, object item_, int index_)
        {
            AddPane((ContentPane)container_);
        }


        protected override void OnItemMoved(DependencyObject container_, object item_, int oldIndex_, int newIndex_)
        { 
        }

        protected override void OnItemRemoved(DependencyObject container_, object oldItem_)
        {
            RemovePane((ContentPane)container_);
        }

        protected override void PrepareContainerForItem(DependencyObject container_, object item_)
        {
 
            base.PrepareContainerForItem(container_, item_);

            if (RemoveItemOnClose)
            {
                ContentPane pane = container_ as ContentPane;
                pane.Closed += OnPaneClosed;
                pane.Closing += OnPaneClosing;

                IEditableCollectionView cv = CollectionViewSource.GetDefaultView(ItemsSource) as IEditableCollectionView;

                if (null == cv || !cv.CanRemove)
                {
                    pane.AllowClose = false;
                }
            }
        }

        private readonly HashSet<ContentPane> toCloseQuietly = new HashSet<ContentPane>();
        private void OnPaneClosing(object sender_, ClosingEventArgs e_)
        {
            ContentPane pane = sender_ as ContentPane;
            if (toCloseQuietly.Contains(pane))
            {
                if (!e_.Cancel)
                {
                    pane.Closing -= OnPaneClosing;
                }
                return;
            }
            if (ClosingWorkSpace != null && ClosingWorkSpace.CanExecute(pane.DataContext))
            {
                ClosingWorkSpace.Execute(pane.DataContext);
                if (!e_.Cancel)
                {
                    pane.Closing -= OnPaneClosing;
                }
            }
            else
            {
                e_.Cancel = true;
            }
        }

        private void OnPaneClosed(object sender_, EventArgs e_)
        { 
            ContentPane pane = sender_ as ContentPane;
            pane.Closed -= OnPaneClosed;
            IEditableCollectionView cv = CollectionViewSource.GetDefaultView(this.ItemsSource) as IEditableCollectionView;
            if (null != cv)
            {
                object dataItem = GetItemForContainer(pane);
                cv.Remove(dataItem);
            }
            if (toCloseQuietly.Contains(pane))
            {
                toCloseQuietly.Remove(pane);
                return;
            }
            if (ClosedWorkSpace != null && ClosedWorkSpace.CanExecute(pane.DataContext))
            {
                ClosedWorkSpace.Execute(pane.DataContext);
            }
            BindingOperations.ClearAllBindings(pane);
            pane.DataContext = null;
        }


        /// <summary>
        /// Invoked when the <see cref="ContainerFactory.ContainerType"/> is about to be changed to determine if the specified type is allowed.
        /// </summary>
        /// <param name="elementType_">The new element type</param>
        protected sealed override void ValidateContainerType(Type elementType_)
        {
            if (!typeof(ContentPane).IsAssignableFrom(elementType_))
                throw new ArgumentException("ContainerType must be a ContentPane or a derived class.");

            base.ValidateContainerType(elementType_);
        }
         

        private void RemovePane(ContentPane contentPane_)
        {
            contentPane_.ExecuteCommand(ContentPaneCommands.Close);
        }


        private void OnFloatingWindowCreated(object sender_, FloatingWindowEventArgs floatingWindowEventArgs_)
        {
            floatingWindowEventArgs_.Window.SetBinding(FrameworkElement.DataContextProperty, new Binding() { Source = manager, Mode = BindingMode.OneWay });
            floatingWindowEventArgs_.Window.MainWindow = manager.MainWindow;
            floatingWindowEventArgs_.Window.IsOwnedByMainWindow = manager.UseOwnedWindows;
        }

        private void AddPane(ContentPane pane_)
        {
            pane_.SetBinding(ContentPane.IconProperty, new Binding("Icon") { Converter = new IconImageSourceConverter(), Mode = BindingMode.OneWay });
            pane_.SetBinding(ContentPane.CaptionProperty, new Binding("Title"){Mode = BindingMode.TwoWay}); 
            pane_.SetBinding(ContentControl.ContentProperty, new Binding("Content") { Mode = BindingMode.OneWay });
            pane_.SetBinding(UIElement.VisibilityProperty,
                             new Binding("IsHidden")
                                 {
                                     Mode = BindingMode.TwoWay,
                                     Converter = new VisibilityConverter(),
                                     ConverterParameter = "foo"
                                 });
            pane_.SetBinding(ContentPane.HeaderTooltipProperty, new Binding("Tooltip") {Mode = BindingMode.OneWay});
            SetUnbindable(pane_);
            WindowViewModel wvm = pane_.DataContext as WindowViewModel;
            if (wvm == null)
            {
                new PaneContainer(pane_).Show();
                return;
            }
            var hostViewContainer = wvm.InitialParameters.InitialLocationTarget;
            var hostPane = hostViewContainer == null ? null : DockManager.Services.DockManager.GetAllPanes(true).
                FirstOrDefault(pane2_ => (((WindowViewModel) pane2_.DataContext).ViewContainer == hostViewContainer));

            switch (wvm.InitialParameters.InitialLocation & ~Core.InitialLocation.Custom)
            {
                case InitialLocation.Floating:
                    AddFloatingPane(pane_);
                    break; 
                case InitialLocation.FloatingOnly:
                    pane_.AllowDock = false;
                    AddFloatingPane(pane_);
                    break; 
                case InitialLocation.DockInNewTab:
                    AddFloatingPane(pane_);
                    break; 
                case InitialLocation.DockTabbed:
                    if (hostPane == null)
                    {
                        Logger.WarningWithFormat(
                            "The pane \"{0}\" with InitialLocation \"{1}\" with a target \"{2}\" couldn't be placed as the target wasn't found. Docking into new tab instead."
                            , (pane_.DataContext as WindowViewModel).Title ?? "Empty title", wvm.InitialParameters.InitialLocation,
                            wvm.InitialParameters.InitialLocationTarget != null ? wvm.InitialParameters.InitialLocationTarget.Title ?? "No title" : "No target");
                        AddFloatingPane(pane_);
                    }
                    else
                    {
                        this.AddDockTabbedPane(hostPane, pane_);
                    }
                    break; 
                case InitialLocation.DockLeft:
                    if (hostPane == null)
                    {
                        Logger.WarningWithFormat(
                            "The pane \"{0}\" with InitialLocation \"{1}\" with a target \"{2}\" couldn't be placed as the target wasn't found. Docking into new tab instead."
                            , (pane_.DataContext as WindowViewModel).Title ?? "Empty title",
                            wvm.InitialParameters.InitialLocation,
                            wvm.InitialParameters.InitialLocationTarget != null
                                ? wvm.InitialParameters.InitialLocationTarget.Title ?? "No title"
                                : "No target");
                        AddFloatingPane(pane_);
                    }
                    else
                    {
                        AddDockedPane(hostPane, pane_, Dock.Left);
                    } 
                    break;

                case InitialLocation.DockTop:
                    if (hostPane == null)
                    {
                        Logger.WarningWithFormat(
                            "The pane \"{0}\" with InitialLocation \"{1}\" with a target \"{2}\" couldn't be placed as the target wasn't found. Docking into new tab instead."
                            , (pane_.DataContext as WindowViewModel).Title ?? "Empty title", wvm.InitialParameters.InitialLocation,
                            wvm.InitialParameters.InitialLocationTarget != null ? wvm.InitialParameters.InitialLocationTarget.Title ?? "No title" : "No target");
                        AddFloatingPane(pane_);
                    }
                    else
                    {
                        AddDockedPane(hostPane, pane_, Dock.Top);
                    } 
                    break; 
                case InitialLocation.DockRight:
                    if (hostPane == null)
                    {
                        Logger.WarningWithFormat(
                            "The pane \"{0}\" with InitialLocation \"{1}\" with a target \"{2}\" couldn't be placed as the target wasn't found. Docking into new tab instead."
                            , (pane_.DataContext as WindowViewModel).Title ?? "Empty title", wvm.InitialParameters.InitialLocation,
                            wvm.InitialParameters.InitialLocationTarget != null ? wvm.InitialParameters.InitialLocationTarget.Title ?? "No title" : "No target");
                        AddFloatingPane(pane_);
                    }
                    else
                    {
                        AddDockedPane(hostPane, pane_, Dock.Right);
                    } 
                    break; 
                case InitialLocation.DockBottom:
                    if (hostPane == null)
                    {
                        Logger.WarningWithFormat(
                            "The pane \"{0}\" with InitialLocation \"{1}\" with a target \"{2}\" couldn't be placed as the target wasn't found. Docking into new tab instead."
                            , (pane_.DataContext as WindowViewModel).Title ?? "Empty title", wvm.InitialParameters.InitialLocation,
                            wvm.InitialParameters.InitialLocationTarget != null ? wvm.InitialParameters.InitialLocationTarget.Title ?? "No title" : "No target");
                        AddFloatingPane(pane_);
                    }
                    else
                    {
                        AddDockedPane(hostPane, pane_, Dock.Bottom);
                    } 
                    break;  
            }
            if (wvm.InitialParameters.InitialLocation.HasFlag(InitialLocation.DockInActiveTab))
            {
                switch (wvm.InitialParameters.InitialLocation & ~InitialLocation.DockInActiveTab)
                {
                    case InitialLocation.DockLeft:
                        AddFloatingPane(pane_);
                        break;
                    case InitialLocation.DockRight:
                        AddFloatingPane(pane_);
                        break;
                    case InitialLocation.DockTop:
                        AddFloatingPane(pane_);
                        break;
                    case InitialLocation.DockBottom:
                        AddFloatingPane(pane_);
                        break;
                    case InitialLocation.DockTabbed:
                        hostPane = DockManager.Services.DockManager.ActivePane;
                        AddDockTabbedPane(hostPane, pane_);
                        break;
                    default:
                        Logger.Warning("Initial location not handled. Adding as floating.");
                        AddFloatingPane(pane_);
                        break;
                }
            }
        }

        private void AddFloatingPane(ContentPane pane_)
        {
            new PaneContainer(pane_).Show(); 
        }

        private void AddDockTabbedPane(ContentPane hostPane_, ContentPane pane_)
        { 
            hostPane_.ParentContainer.AddPane(pane_);
        }

        private void AddDockedPane(ContentPane hostPane_, ContentPane pane_, Dock dock_)
        {
             hostPane_.Wrapper.DockingGrid.Add(new PaneContainer(pane_),dock_, false);
        }
        private void SetUnbindable(ContentPane pane_)
        {
            WindowViewModel vm = pane_.DataContext as WindowViewModel;
            if (vm == null) return;
            pane_.Name = vm.ID;
            pane_.TitleTextTrimming = manager.TitleTextTrimming;
            WindowViewContainer viewContainer = (WindowViewContainer)vm.ViewContainer;
            pane_.SizeToContent = vm.InitialParameters.SizingMethod;
            pane_.IsOwnedByMainWindow = viewContainer.OwnedByContainer;
            if (pane_.SizeToContent == SizingMethod.Custom)
            {
                pane_.PaneHeight = vm.InitialParameters.Height;
                pane_.PaneWidth = vm.InitialParameters.Width;
                pane_.ClearValue(FrameworkElement.HeightProperty);
                pane_.ClearValue(FrameworkElement.WidthProperty);
            }
            RestrictWindowSize(pane_, viewContainer);


            viewContainer.Visual = pane_;
            pane_.Name = ContentPaneIdConverter.ConvertToPaneId(vm.ID);
            pane_.Topmost = vm.Topmost;
            vm.ShowInTaskbar = viewContainer.Parameters.ShowInTaskbar;
            pane_.ShowInTaskbar = vm.ShowInTaskbar;
            vm.Transient = viewContainer.Parameters.Transient; 
            pane_.SaveInLayout = !vm.Transient;
            //pane_.AllowPartiallyOffscreen = viewContainer.Parameters.AllowPartiallyOffscreen;
            if (viewContainer.Parameters.Resources != null)
            {
                pane_.Resources.MergedDictionaries.Add(viewContainer.Parameters.Resources);
            } 
            if (!viewContainer.Parameters.IsHeaderVisible)
            {
                pane_.IsHeaderVisible = false;
            }

            DependencyPropertyDescriptor dpLocation =
                DependencyPropertyDescriptor.FromProperty(ContentPane.PaneLocationProperty, typeof (ContentPane));
            EventHandler onLocationChanged = null;
            dpLocation.AddValueChanged(pane_, onLocationChanged = (sender_, args_) =>
                {
                    ContentPane contentPane = sender_ as ContentPane;
                    if (contentPane == null) return; 
                    var viewContainerLocal = (WindowViewContainer)((IWindowViewContainerHost)pane_).View;
                    viewContainerLocal.V2vHost = contentPane;
                     
                    if (contentPane.PaneLocation != PaneLocation.Unknown)
                    {
                        viewContainerLocal.Root = DockHelper.GetRootObject(viewContainerLocal);
                    }
                    
                });
            DependencyPropertyDescriptor dpdWidth =
    DependencyPropertyDescriptor.FromProperty(FrameworkElement.ActualWidthProperty, typeof(ContentPane));
            EventHandler onWidthChanged = null;
            if (dpdWidth != null)
            {
                dpdWidth.AddValueChanged(pane_, onWidthChanged =
                                                delegate(object sender_, EventArgs e_)
                                                {
                                                    ContentPane contentPane = sender_ as ContentPane;
                                                    if (contentPane != null)
                                                    {
                                                        //actual size of the ContentPane would changed if minimized
                                                        Window w = Window.GetWindow(contentPane);
                                                        if (w == null || w.WindowState == WindowState.Minimized) return;

                                                        var content = contentPane.Content as FrameworkElement;
                                                        if (content != null && content.ActualWidth <= 0) return;
                                                        double actualWidth = ((content != null) ? content.ActualWidth : contentPane.ActualWidth);
                                                        ((WindowViewModel)contentPane.DataContext).Width = Core.CoreUtilities.AreClose(actualWidth, 0) ? 100 : actualWidth; 
                                                    }
                                                });
            }

            DependencyPropertyDescriptor dpdHeight =
            DependencyPropertyDescriptor.FromProperty(FrameworkElement.ActualHeightProperty, typeof(ContentPane));
            EventHandler onHeightChanged = null;
            if (dpdHeight != null)
            {
                dpdHeight.AddValueChanged(pane_, onHeightChanged =
                                                delegate(object sender_, EventArgs e_)
                                                {
                                                    ContentPane contentPane = sender_ as ContentPane;
                                                    if (contentPane != null)
                                                    {
                                                        //actual size of the ContentPane would changed if minimized
                                                        Window w = Window.GetWindow(contentPane);
                                                        if (w == null || w.WindowState == WindowState.Minimized) return;

                                                        var content = contentPane.Content as FrameworkElement;
                                                        if (content != null && content.ActualHeight <= 0) return;
                                                        double actualHeight = ((content != null) ? content.ActualHeight : contentPane.ActualHeight);

                                                        ((WindowViewModel)contentPane.DataContext).Height = Core.CoreUtilities.AreClose(actualHeight, 0) ? 100 : actualHeight;
                                                    }
                                                });
            }
            if (vm.Background != null)
            {
                pane_.Background = vm.Background;
            }
            DependencyPropertyChangedEventHandler onVisibleChanged = null;
            pane_.IsVisibleChanged += onVisibleChanged =
                delegate(object sender_, DependencyPropertyChangedEventArgs e_)
                {
                    if (e_.NewValue != null)
                    {
                        ((WindowViewModel)pane_.DataContext).IsVisible = (bool)e_.NewValue;
                    }
                };
             
            vm.RequestClose += (sender_, e_) => pane_.ExecuteCommand(ContentPaneCommands.Close);
            vm.RequestQuietClose += (sender_, e_) =>
            {
                toCloseQuietly.Add(pane_);
                pane_.ExecuteCommand(ContentPaneCommands.Close);
            };

            vm.RequestFlash += (sender_, e_) => flashExecuter.Flash(pane_, e_.Value1);

            vm.RequestActivate += (sender_, e_) => pane_.Activate();

            PropertyChangedEventHandler onPropertyChanged;
            vm.PropertyChanged += onPropertyChanged = (sender_, e_) =>
                {
                    WindowViewModel wvm = sender_ as WindowViewModel;
                    if (wvm == null) return;
                    var paneCopied = pane_;
                    switch (e_.PropertyName)
                    {
                        case "Background":
                            paneCopied.Background = wvm.Background;
                            //and it's impossible to "unset" the background and get back to theme-set values
                            break; 
                        case "IsActive":
                            if (wvm.IsActive)
                            {
                                flashExecuter.PaneActivated(paneCopied);
                            }
                            break;
                        case "ID":
                            paneCopied.Name = ContentPaneIdConverter.ConvertToPaneId(wvm.ID);
                            break;
                        case "Width":
                            if (paneCopied.CanChangeDimensions)
                            {
                                paneCopied.SetContentWidth(wvm.Width);
                            }
                            break;
                        case "Height":
                            if (paneCopied.CanChangeDimensions)
                            {
                                paneCopied.SetContentHeight(wvm.Height); 
                            }
                            break;
                        case "FloatingWindowMaximumWidth":
                            if (wvm.FloatingWindowMaximumWidth.HasValue && paneCopied.CanChangeDimensions)
                            {
                                paneCopied.SetContentMaximumWidth(wvm.FloatingWindowMaximumWidth.Value);
                            }
                            break;
                        case "FloatingWindowMinimumWidth":
                            if (paneCopied.CanChangeDimensions)
                            {
                                paneCopied.SetContentMinimumWidth(wvm.FloatingWindowMinimumWidth);
                            }
                            break;
                        case "CanMaximize":
                            paneCopied.AllowMaxmize = wvm.CanMaximize;
                            break;
                        case "FloatingWindowMaximumHeight":
                            if (wvm.FloatingWindowMaximumHeight.HasValue && paneCopied.CanChangeDimensions)
                            {
                                paneCopied.SetContentMaximumHeight(wvm.FloatingWindowMaximumHeight.Value);
                            }
                            break;
                        case "FloatingWindowMinimumHeight":
                            if (paneCopied.CanChangeDimensions)
                            {
                                paneCopied.SetContentMinimumHeight(wvm.FloatingWindowMinimumHeight);
                            }
                            break; 
                        case "Topmost":
                            paneCopied.Topmost = wvm.Topmost;
                            break;
                        case "OwnedByContainer":
                            paneCopied.IsOwnedByMainWindow = wvm.OwnedByContainer;
                            break;
                        case "Transient":
                            paneCopied.SaveInLayout = !wvm.Transient;
                            break;
                        case "IsInActiveWorkspace":
                            if (ChromeManagerBase.TabsOwnFloatingWindows && (wvm.IsHidden == wvm.IsInActiveWorkspace))
                            { 
                                wvm.IsHidden = !wvm.IsInActiveWorkspace; 
                            }
                            break; 
                    }
                };
            EventHandler handler = null;
            handler =
                (sender_, e_) =>
                {
                    if (pane_ == null) return;

                    if (dpdWidth != null && onWidthChanged != null)
                    {
                        dpdWidth.RemoveValueChanged(pane_, onWidthChanged);
                    }
                    if (dpdHeight != null && onHeightChanged != null)
                    {
                        dpdHeight.RemoveValueChanged(pane_, onHeightChanged);
                    }
                    dpLocation.RemoveValueChanged(pane_, onLocationChanged);
                    pane_.IsVisibleChanged -= onVisibleChanged;
                    vm.PropertyChanged -= onPropertyChanged;
                    pane_.ClearValue(ContentControl.ContentProperty);
                    pane_.Closed -= handler; 
                    pane_ = null;
                    vm = null;
                    dpdWidth = null;
                    dpdHeight = null;
                };
            pane_.Closed += handler;

        }
         

        private void RestrictWindowSize(ContentPane pane_, IWindowViewContainer container_)
        {
            DependencyObject content = container_.Content as DependencyObject;
            if (content != null)
            {
                foreach (var host in content.FindLogicalChildren<WindowsFormsHost>(true))
                {
                    var hostCopied = host;
                    Form f = host.Child as Form;
                    if (f != null && (!f.MaximumSize.IsEmpty || !f.MinimumSize.IsEmpty))
                    {
                        DependencyPropertyDescriptor dpdWidth =
    DependencyPropertyDescriptor.FromProperty(FrameworkElement.ActualWidthProperty, typeof(ContentPane));
                        if (dpdWidth != null)
                        {
                            SizeChangedEventHandler handler = null;
                            handler = (sender_, args_) =>
                            {
                                Window w = Window.GetWindow(pane_);
                                if (w == null) return;
                                ContentPane contentPane = sender_ as ContentPane;
                                if (contentPane == null) return;
                                if (!contentPane.IsSingleRootPane)
                                { 
                                    w.ClearValue(FrameworkElement.MaxWidthProperty);
                                    w.ClearValue(FrameworkElement.MinWidthProperty);
                                    w.ClearValue(FrameworkElement.MaxHeightProperty);
                                    w.ClearValue(FrameworkElement.MinHeightProperty);
                                    return;
                                }

                                if (f.MaximumSize.Width > 0)
                                {
                                    double margin = 0;
                                    hostCopied.AccumulateMargin<Window>(ref margin, false);
                                    w.MaxWidth = Math.Min(f.MaximumSize.Width + margin, w.MaxWidth);
                                }
                                if (f.MinimumSize.Width > 0)
                                {
                                    double margin = 0;
                                    hostCopied.AccumulateMargin<Window>(ref margin, false);
                                    w.MinWidth = Math.Max(f.MinimumSize.Width + margin, w.MinWidth);
                                }
                                if (f.MaximumSize.Height > 0)
                                {
                                    double margin = 0;
                                    hostCopied.AccumulateMargin<Window>(ref margin, true);
                                    w.MaxHeight = Math.Min(f.MaximumSize.Height + margin, w.MaxHeight);
                                }
                                if (f.MinimumSize.Height > 0)
                                {
                                    double margin = 0;
                                    hostCopied.AccumulateMargin<Window>(ref margin, true);
                                    w.MinHeight = Math.Max(f.MinimumSize.Height + margin, w.MinHeight);
                                } 

                            };
                            pane_.SizeChanged += handler;
                            EventHandler handler2 = null;
                            handler2 = (sender_, e_) =>
                            {
                                pane_.Closed -= handler2;
                                pane_.SizeChanged -= handler;
                            };
                            pane_.Closed += handler2;
                        } 
                    }
                    break;

                }
            }
        }
    }
}
