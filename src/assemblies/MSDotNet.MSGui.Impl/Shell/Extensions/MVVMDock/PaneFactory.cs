﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Threading;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions.MVVMDock
{
    public abstract class PaneFactory: ContainerFactory
    {
        internal static event RoutedPropertyChangedEventHandler<UIElement> ActivePaneChanged;
        protected void HandleActivePaneChanged<TContentPane>(object sender_, TContentPane oldValue_, TContentPane newValue_) where TContentPane:FrameworkElement
        {
            if (oldValue_ != null && oldValue_.DataContext is WindowViewModel)
            {
                if (LinkControlFocusHackEnabled &&
                    (oldValue_.DataContext as WindowViewModel).HeaderItems.FirstOrDefault((o) => (o is Image) && (o as Image).Tag.ToString() == "LinkControl") != null)
                {
                    #region LinkControlFocusHack

                    if (FocusBounceBackNeeded && BounceBackTo == null)
                        BounceBackTo = oldValue_;

                    if (FocusBounceBackNeeded && oldValue_ == BounceBackTo && newValue_ != null)
                    {
                        var vm = ((WindowViewModel)newValue_.DataContext);
                        PropertyChangedEventHandler hack = null;
                        hack = new PropertyChangedEventHandler(
                            (a, b) =>
                            {
                                if (b.PropertyName != "IsActive") return;
                                /*ContentPaneFactory.FocusBounceBackNeeded = false; e_.OldValue.Activate();*/
                                oldValue_.Focus();
                                vm.PropertyChanged -= hack;
                            }
                            );
                        vm.PropertyChanged += hack;
                        ((WindowViewModel)oldValue_.DataContext).IsActive = false;
                    }
                    else
                    {
                        //var focusedChild = FindFocusedChild(e_.OldValue);

                        var holderItemClicked = false;
                        var window = Window.GetWindow(oldValue_);
                        if (window == null) return;
                        var clickedPoint = Mouse.GetPosition(window);
                        if (clickedPoint.X < 0 && clickedPoint.Y < 0) return;
                        VisualTreeHelper.HitTest
                                                        (
                                                            window,
                                                            null,
                                                            result_ =>
                                                            {
                                                                var visualHit = result_.VisualHit;
                                                                var holder = visualHit.FindVisualParentIncludeChild<HeaderItemsHolder>(); // object is in the holder
                                                                if (holder != null && holder.FindVisualParent<TContentPane>() == newValue_)
                                                                {
                                                                    holderItemClicked = true;
                                                                }
                                                                return HitTestResultBehavior.Continue;
                                                            },
                                                            new PointHitTestParameters(clickedPoint)
                                                        );

                        if ((holderItemClicked) && (newValue_ != null) && newValue_.DataContext is WindowViewModel)
                        {
                            var vm = ((WindowViewModel)newValue_.DataContext);
                            PropertyChangedEventHandler hack = null;
                            hack = new PropertyChangedEventHandler((a, b) => { /*e_.OldValue.Activate();*/ oldValue_.Focus(); vm.PropertyChanged -= hack; });
                            vm.PropertyChanged += hack;
                        }
                        else
                        {
                            ((WindowViewModel)oldValue_.DataContext).IsActive = false;
                        }
                    }

                    #endregion LinkControlFocusHack
                }
                else
                {

                    ((WindowViewModel)oldValue_.DataContext).IsActive = false;
                }
            }
            if (newValue_ != null && newValue_.DataContext is WindowViewModel)
            {
                foreach (WindowViewModel model in this.ItemsSource)
                {
                    if (model != newValue_.DataContext)
                    {
                        model.IsActive = false;
                    }
                }
                ((WindowViewModel)newValue_.DataContext).IsActive = true;
            }
            var copy = ActivePaneChanged;
            if (copy != null)
            {
                copy(sender_, new RoutedPropertyChangedEventArgs<UIElement>(oldValue_, newValue_));
            }
        }



        public static readonly DependencyProperty ClosingWorkSpaceProperty = DependencyProperty.Register("ClosingWorkSpace",
            typeof(ICommand), typeof(PaneFactory), new FrameworkPropertyMetadata(null));

        public ICommand ClosingWorkSpace
        {
            get
            {
                return (ICommand)GetValue(ClosingWorkSpaceProperty);
            }
            set
            {
                SetValue(ClosingWorkSpaceProperty, value);
            }
        }

        public static readonly DependencyProperty ClosedWorkSpaceProperty = DependencyProperty.Register("ClosedWorkSpace",
             typeof(ICommand), typeof(PaneFactory), new FrameworkPropertyMetadata(null));

        public ICommand ClosedWorkSpace
        {
            get
            {
                return (ICommand)GetValue(ClosedWorkSpaceProperty);
            }
            set
            {
                SetValue(ClosedWorkSpaceProperty, value);
            }
        }
        /// <summary>
        /// Identifies the <see cref="RemoveItemOnClose"/> dependency property
        /// </summary>
        public static readonly DependencyProperty RemoveItemOnCloseProperty = DependencyProperty.Register("RemoveItemOnClose",
            typeof(bool), typeof(PaneFactory), new FrameworkPropertyMetadata(true));

        /// <summary>
        /// Returns or sets a boolean indicating whether to remove the item when the pane was closed.
        /// </summary>
        /// <seealso cref="RemoveItemOnCloseProperty"/>
        [Description("Returns or sets a boolean indicating whether to remove the item when the pane was closed.")]
        [Category("Behavior")]
        [Bindable(true)]
        public bool RemoveItemOnClose
        {
            get
            {
                return (bool)this.GetValue(RemoveItemOnCloseProperty);
            }
            set
            {
                this.SetValue(RemoveItemOnCloseProperty, value);
            }
        }
         

        public static readonly DependencyProperty EnforceSizeRestrictionsProperty = DependencyProperty.Register("EnforceSizeRestrictions",
            typeof(bool), typeof(PaneFactory), new FrameworkPropertyMetadata(true));

        public bool EnforceSizeRestrictions
        {
            get
            {
                return (bool)GetValue(EnforceSizeRestrictionsProperty);
            }
            set
            {
                SetValue(EnforceSizeRestrictionsProperty, value);
            }
        }

        private void SizeRestrictionHandler(object sender_, EventArgs args_)
        {
            WindowInteropHelper windowInteropHelper = null;
            Screen screen = null;
            var tw = (FrameworkElement)sender_;
            var window = Window.GetWindow(tw);
            if (window == null)
            {
                Dispatcher.BeginInvoke((Action)(() =>
                {
                    window = Window.GetWindow(tw);
                    if (window == null) return;
                    windowInteropHelper = new WindowInteropHelper(window);
                    screen = Screen.FromHandle(windowInteropHelper.Handle);
                    if (tw.MaxWidth > screen.WorkingArea.Width)
                    {
                        tw.MaxWidth = screen.WorkingArea.Width;
                    }
                    if (tw.MaxHeight > screen.WorkingArea.Height)
                    {
                        tw.MaxHeight = screen.WorkingArea.Height;
                    }
                }), DispatcherPriority.ApplicationIdle);
                return;
            }
            windowInteropHelper = new WindowInteropHelper(window);
            screen = Screen.FromHandle(windowInteropHelper.Handle);
            if (tw.MaxWidth > screen.WorkingArea.Width)
            {
                tw.MaxWidth = screen.WorkingArea.Width;
            }
            if (tw.MaxHeight > screen.WorkingArea.Height)
            {
                tw.MaxHeight = screen.WorkingArea.Height;
            }
        }


        protected void AddSizeRestrictionsHandler(FrameworkElement window_)
        {
            DependencyPropertyDescriptor.FromProperty(Window.TopProperty, window_.GetType()).AddValueChanged(
                                window_, SizeRestrictionHandler);
            DependencyPropertyDescriptor.FromProperty(Window.LeftProperty, window_.GetType()).AddValueChanged(
                window_, SizeRestrictionHandler); 
        }

        protected void RemoveSizeRestrictionsHandler(FrameworkElement window_)
        {

            DependencyPropertyDescriptor.FromProperty(Window.TopProperty, window_.GetType()).RemoveValueChanged(
                                window_, SizeRestrictionHandler);
            DependencyPropertyDescriptor.FromProperty(Window.LeftProperty, window_.GetType()).RemoveValueChanged(
                window_, SizeRestrictionHandler); 
        }

        internal static bool FocusBounceBackNeeded = false;
        internal static bool LinkControlFocusHackEnabled = false;
        internal static object BounceBackTo = null;
    }
}
