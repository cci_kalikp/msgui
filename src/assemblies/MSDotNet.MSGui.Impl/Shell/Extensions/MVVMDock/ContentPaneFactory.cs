/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Extensions/MVVMDock/ContentPaneFactory.cs#65 $
// $Change: 900785 $
// $DateTime: 2014/10/14 04:41:09 $
// $Author: caijin $

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq; 
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Forms;
using System.Windows.Forms.Integration; 
using System.Windows.Interop; 
using Infragistics.Windows.Controls;
using Infragistics.Windows.Controls.Events;
using Infragistics.Windows.DockManager;
using Infragistics.Windows.DockManager.Events; 
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Win32;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions; 
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.Converters;
using MorganStanley.MSDotNet.MSGui.Core; 
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions.Flash;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions.MVVMDock;
using MorganStanley.MSDotNet.My;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;

//using System.Linq;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions
{
	/// <summary>
	/// Class used to generate ContentPane instances based on a given source collection of items (<see cref="ContainerFactoryBase.ItemsSource"/>).
	/// </summary>
	/// TODO we have additional unused code in this class for situations when we attach this thing not to XamDockManager, but to smth else. It could be removed.
    public class ContentPaneFactory : PaneFactory
	{
		private static readonly IMSLogger m_logger = MSLoggerFactory.CreateLogger<ContentPaneFactory>();
        private readonly Flash.ContentPaneFlashExecuter flashExecuter = new ContentPaneFlashExecuter();
		public override void ShowHideHeaderOnCCPs(Visibility visibility_)
		{
			foreach (CustomContentPane pane in GetElements())
			{
				pane.HeaderVisibleLock = visibility_;
			}
		}
         
		private DependencyObject m_target;

		static ContentPaneFactory()
		{
			ContainerTypeProperty.OverrideMetadata(typeof(ContentPaneFactory), new FrameworkPropertyMetadata(typeof(CustomContentPane)));
		}
         
		/// <summary>
		/// Invoked when an element for an item has been generated.
		/// </summary>
		/// <param name="item_">The underlying item for which the element has been generated</param>
		/// <param name="container_">The element that was generated</param>
		/// <param name="index_">The index at which the item existed</param>
		protected sealed override void OnItemInserted(DependencyObject container_, object item_, int index_)
		{
			AddPane((ContentPane)container_);
		}

		/// <summary>
		/// Invoked when an item has been moved in the source collection.
		/// </summary>
		/// <param name="item_">The item that was moved</param>
		/// <param name="container_">The associated element</param>
		/// <param name="oldIndex_">The old index</param>
		/// <param name="newIndex_">The new index</param>
		protected sealed override void OnItemMoved(DependencyObject container_, object item_, int oldIndex_, int newIndex_)
		{
		}

		/// <summary>
		/// Invoked when an element created for an item has been removed
		/// </summary>
		/// <param name="oldItem_">The item associated with the element that was removed</param>
		/// <param name="container_">The element that has been removed</param>
		protected sealed override void OnItemRemoved(DependencyObject container_, object oldItem_)
		{
			((CustomContentPane)container_).ReleaseEventHandlers();
			RemovePane((ContentPane)container_);
		}

		/// <summary>
		/// Used to initialize a container for a given item.
		/// </summary>
		/// <param name="container_">The container element </param>
		/// <param name="item_">The item from the source collection</param>
		protected override void PrepareContainerForItem(DependencyObject container_, object item_)
		{
			//BindingHelper.BindPath(container_, item_, HeaderPath, HeaderedContentControl.HeaderProperty);
			//BindingHelper.BindPath(container_, item_, ContentPath, ContentControl.ContentProperty);
			//BindingHelper.BindPath(container_, item_, TabHeaderPath, ContentPane.TabHeaderProperty);

			base.PrepareContainerForItem(container_, item_);

			if (RemoveItemOnClose)
			{
				ContentPane pane = container_ as ContentPane;
				pane.Closed += OnPaneClosed;
				pane.Closing += OnPaneClosing;

				IEditableCollectionView cv = CollectionViewSource.GetDefaultView(ItemsSource) as IEditableCollectionView;

				if (null == cv || !cv.CanRemove)
				{
					pane.AllowClose = false;
				}
			}
		}

		/// <summary>
		/// Invoked when the <see cref="ContainerFactory.ContainerType"/> is about to be changed to determine if the specified type is allowed.
		/// </summary>
		/// <param name="elementType_">The new element type</param>
		protected sealed override void ValidateContainerType(Type elementType_)
		{
			if (!typeof(ContentPane).IsAssignableFrom(elementType_))
				throw new ArgumentException("ContainerType must be a ContentPane or a derived class.");

			base.ValidateContainerType(elementType_);
		}
         

        public bool FloatingOnly
        {
            get { return (bool)GetValue(FloatingOnlyProperty); }
            set { SetValue(FloatingOnlyProperty, value); }
        }

        // Using a DependencyProperty as the backing store for FloatingOnly.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FloatingOnlyProperty =
            DependencyProperty.Register("FloatingOnly", typeof(bool), typeof(ContentPaneFactory), new PropertyMetadata(false));

        
		
		/// <summary>
		/// PaneFactory Attached Dependency Property
		/// </summary>
		public static readonly DependencyProperty PaneFactoryProperty =
			DependencyProperty.RegisterAttached("PaneFactory", typeof(ContentPaneFactory), typeof(ContentPaneFactory),
			new FrameworkPropertyMetadata((ContentPaneFactory)null,
				new PropertyChangedCallback(OnPaneFactoryChanged)));

		/// <summary>
		/// Returns the object that creates ContentPane instances based on the items in the associated <see cref="ContainerFactoryBase.ItemsSource"/>
		/// </summary>
		[AttachedPropertyBrowsableForType(typeof(DocumentContentHost))]
		[AttachedPropertyBrowsableForType(typeof(TabGroupPane))]
		[AttachedPropertyBrowsableForType(typeof(SplitPane))]
		[AttachedPropertyBrowsableForType(typeof(XamDockManager))]
		public static ContentPaneFactory GetPaneFactory(DependencyObject d_)
		{
			return (ContentPaneFactory)d_.GetValue(PaneFactoryProperty);
		}

		/// <summary>
		/// Sets the object that will create ContentPane instances based on the items in the associate <see cref="ContainerFactoryBase.ItemsSource"/>
		/// </summary>
		public static void SetPaneFactory(DependencyObject d_, ContentPaneFactory value_)
		{
			d_.SetValue(PaneFactoryProperty, value_);
		}

		private static void OnPaneFactoryChanged(DependencyObject d_, DependencyPropertyChangedEventArgs e_)
		{
			if (d_ is DocumentContentHost || d_ is TabGroupPane || d_ is SplitPane || d_ is XamDockManager)
			{
				ContentPaneFactory oldFactory = (ContentPaneFactory)e_.OldValue;
				ContentPaneFactory newFactory = (ContentPaneFactory)e_.NewValue;

				if (oldFactory == newFactory)
					return;

				if (oldFactory != null)
				{
				    XamDockManager manager = oldFactory.m_target as XamDockManager;
                    if (manager != null)
					{
                        manager.InitializePaneContent -= OnInitializePaneContent; 
                        manager.ActivePaneChanged -= oldFactory.OnActivePaneChanged;
                        manager.ToolWindowLoaded -= oldFactory.OnToolWindowLoaded;
                        manager.ToolWindowUnloaded -= oldFactory.OnToolWindowUnloaded;
					}
                    oldFactory.m_target = null; 

					foreach (ContentPane cp in oldFactory.GetElements())
					{
						oldFactory.RemovePane(cp);
					}
                    oldFactory.flashExecuter.Stop();
				}

				if (newFactory != null)
				{
					newFactory.m_target = d_;
				    XamDockManager manager = newFactory.m_target as XamDockManager;
                    if (manager != null)
					{
                        manager.InitializePaneContent += OnInitializePaneContent;
                        manager.ActivePaneChanged += newFactory.OnActivePaneChanged;
                        manager.ToolWindowLoaded += newFactory.OnToolWindowLoaded;
                        manager.ToolWindowUnloaded += newFactory.OnToolWindowUnloaded;
                    }

					foreach (ContentPane cp in newFactory.GetElements())
					{
						newFactory.AddPane(cp);
					}
                    newFactory.flashExecuter.Start();
				}
			}
		}

        //this is needed, so that those toolwindows that are never really loaded,
        //(created when loading a layout but is never loaded since the pane becomes docked later on)
        //would have a chance to dispose
        private void OnToolWindowUnloaded(object sender_, PaneToolWindowEventArgs e_)
	    { 
            ToolWindow window = ToolWindow.GetToolWindow(e_.Window) as CustomPaneToolWindow;
            if (window == null) return;
            if (EnforceSizeRestrictions)
            {
                RemoveSizeRestrictionsHandler(window);
                window.Closed -= new EventHandler(ContentPaneFactory_Closed);
            } 
        }


	    private void OnToolWindowLoaded(object sender_, PaneToolWindowEventArgs e_)
		{
			PaneToolWindow paneToolWindow = e_.Window;
            ToolWindow window = ToolWindow.GetToolWindow(e_.Window);

			if (window == null) return;
 
			if (EnforceSizeRestrictions)
			{
                AddSizeRestrictionsHandler(window);
                window.Closed += new EventHandler(ContentPaneFactory_Closed); 
			} 

            //window.MinHeight = 100;
            //window.MinWidth = 100;

            //since this event is triggered when IsWindowLoaded is set, this is not needed
            //var field = typeof(PaneToolWindow).GetField("_isWindowLoaded", BindingFlags.Instance | BindingFlags.NonPublic);
            //var t = field.GetValue(paneToolWindow);
            //field.SetValue(paneToolWindow, true);

			window.UseOSNonClientArea = false;

			ContentPane child = XamDockHelper.FindOnlyContentPaneChild(paneToolWindow.Pane);
            //todo: implement in harmonia
			XamDockHelper.SetMetadata(child, paneToolWindow);
		}

		private void ContentPaneFactory_Closed(object sender, EventArgs e)
		{
			((CustomPaneToolWindow)sender).Closed -= ContentPaneFactory_Closed;
            RemoveSizeRestrictionsHandler((CustomPaneToolWindow)sender); 
		}

		private void OnActivePaneChanged(object sender_, RoutedPropertyChangedEventArgs<ContentPane> e_)
		{
			if (e_.OldValue == e_.NewValue)
				return;
            HandleActivePaneChanged(sender_, e_.OldValue, e_.NewValue); 
		}



		//private static DependencyObject FindFocusedChild(DependencyObject o)
		//{
		//    var e = o as FrameworkElement;

		//    if (e != null && e.IsFocused)
		//    {
		//        return e;
		//    }
		//    else
		//    {
		//        for (int i = 0; i < System.Windows.Media.VisualTreeHelper.GetChildrenCount(o); i++)
		//        {
		//            var O = FindFocusedChild(System.Windows.Media.VisualTreeHelper.GetChild(o, i));
		//            if (O != null)
		//                return O;
		//        }
		//    }
		//    return null;
		//}

		private static void OnInitializePaneContent(object sender_, InitializePaneContentEventArgs e_)
		{
		}


		/// <summary>
		/// Identifies the <see cref="TabHeaderPath"/> dependency property
		/// </summary>
		public static readonly DependencyProperty TabHeaderPathProperty = DependencyProperty.Register("TabHeaderPath",
			typeof(string), typeof(ContentPaneFactory), new FrameworkPropertyMetadata(null));

		/// <summary>
		/// Returns or sets the path to the property on the underlying item that should be used to provide the TabHeader for the ContentPane.
		/// </summary>
		/// <seealso cref="TabHeaderPathProperty"/>
		[Description("Returns or sets the path to the property on the underlying item that should be used to provide the TabHeader for the ContentPane.")]
		[Category("Behavior")]
		[Bindable(true)]
		public string TabHeaderPath
		{
			get
			{
				return (string)GetValue(TabHeaderPathProperty);
			}
			set
			{
				SetValue(TabHeaderPathProperty, value);
			}
		}

	    /// <summary>
		/// Invoked when a new <see cref="ContentPane"/> has been created and needs to be added to the appropriate target collection.
		/// </summary>
		/// <param name="pane_">The pane that was created and needs to be added to the appropriate collection</param>
		protected virtual void AddPane(ContentPane pane_)
		{
			if (m_target is XamDockManager)
			{
				var dockManager = ((XamDockManager)m_target);

				pane_.AllowInDocumentHost = ShellModeExtension.ContentHostModeEnabled;
				pane_.CloseAction = PaneCloseAction.RemovePane;

				if (!((XamDockManager)m_target).IsEnabled)
				{
					pane_.Visibility = Visibility.Collapsed;
				}
				SetUnbindable(pane_);

				object item = pane_.GetValue(ItemForContainerProperty);
				if (item != null)
				{
					var wvm = (item as WindowViewModel);
					if (wvm != null)
					{
                        OnAddingPane(pane_, wvm.ViewContainer);

						var hostViewContainer = wvm.InitialParameters.InitialLocationTarget;
                        var hostPane = hostViewContainer == null ? null : dockManager.GetPanes(PaneNavigationOrder.ActivationOrder).FirstOrDefault(pane => ((pane.DataContext as WindowViewModel).ViewContainer == hostViewContainer));

						switch (wvm.InitialParameters.InitialLocation & ~Core.InitialLocation.Custom)
						{
							case Core.InitialLocation.Floating:
								AddFloatingPane(dockManager, InitialPaneLocation.DockableFloating, pane_);
								break;

							case Core.InitialLocation.FloatingOnly:
								AddFloatingPane(dockManager, InitialPaneLocation.FloatingOnly, pane_);
								break;

							case Core.InitialLocation.DockInNewTab:
								AddFloatingPane(dockManager, InitialPaneLocation.DockedTop, pane_);
								break;

							case Core.InitialLocation.DockTabbed:
								this.AddDockTabbedPane(dockManager, hostPane, pane_);
								break;

							case Core.InitialLocation.DockLeft:
								if (hostPane == null)
								{
									m_logger.WarningWithFormat(
										"The pane \"{0}\" with InitialLocation \"{1}\" with a target \"{2}\" couldn't be placed as the target wasn't found. Docking into new tab instead."
										, (pane_.DataContext as WindowViewModel).Title ?? "Empty title", wvm.InitialParameters.InitialLocation,
										wvm.InitialParameters.InitialLocationTarget != null ? wvm.InitialParameters.InitialLocationTarget.Title ?? "No title" : "No target");
									AddFloatingPane(dockManager, InitialPaneLocation.DockedLeft, pane_);
								}
								else
									this.AddDockedPane(dockManager, hostPane, pane_, true, System.Windows.Controls.Orientation.Vertical);
								break;

							case Core.InitialLocation.DockTop:
								if (hostPane == null)
								{
									m_logger.WarningWithFormat(
										"The pane \"{0}\" with InitialLocation \"{1}\" with a target \"{2}\" couldn't be placed as the target wasn't found. Docking into new tab instead."
										, (pane_.DataContext as WindowViewModel).Title ?? "Empty title", wvm.InitialParameters.InitialLocation,
										wvm.InitialParameters.InitialLocationTarget != null ? wvm.InitialParameters.InitialLocationTarget.Title ?? "No title" : "No target");
									AddFloatingPane(dockManager, InitialPaneLocation.DockedTop, pane_);
								}
								else
									this.AddDockedPane(dockManager, hostPane, pane_, true, System.Windows.Controls.Orientation.Horizontal);
								break;

							case Core.InitialLocation.DockRight:
								if (hostPane == null)
								{
									m_logger.WarningWithFormat(
										"The pane \"{0}\" with InitialLocation \"{1}\" with a target \"{2}\" couldn't be placed as the target wasn't found. Docking into new tab instead."
										, (pane_.DataContext as WindowViewModel).Title ?? "Empty title", wvm.InitialParameters.InitialLocation,
										wvm.InitialParameters.InitialLocationTarget != null ? wvm.InitialParameters.InitialLocationTarget.Title ?? "No title" : "No target");
									AddFloatingPane(dockManager, InitialPaneLocation.DockedRight, pane_);
								}
								else
									this.AddDockedPane(dockManager, hostPane, pane_, false, System.Windows.Controls.Orientation.Vertical);
								break;

							case Core.InitialLocation.DockBottom:
								if (hostPane == null)
								{
									m_logger.WarningWithFormat(
										"The pane \"{0}\" with InitialLocation \"{1}\" with a target \"{2}\" couldn't be placed as the target wasn't found. Docking into new tab instead."
										, (pane_.DataContext as WindowViewModel).Title ?? "Empty title", wvm.InitialParameters.InitialLocation,
										wvm.InitialParameters.InitialLocationTarget != null ? wvm.InitialParameters.InitialLocationTarget.Title ?? "No title" : "No target");
									AddFloatingPane(dockManager, InitialPaneLocation.DockedBottom, pane_);
								}
								else
									this.AddDockedPane(dockManager, hostPane, pane_, false, System.Windows.Controls.Orientation.Horizontal);
								break;
						}
                        if (wvm.InitialParameters.InitialLocation.HasFlag(InitialLocation.DockInActiveTab))
                        {
                            switch (wvm.InitialParameters.InitialLocation & ~Core.InitialLocation.DockInActiveTab)
                            {
                                case InitialLocation.DockLeft:
                                    AddFloatingPane(dockManager, InitialPaneLocation.DockedLeft, pane_);
                                    break;
                                case InitialLocation.DockRight:
                                    AddFloatingPane(dockManager, InitialPaneLocation.DockedRight, pane_);
                                    break;
                                case InitialLocation.DockTop:
                                    AddFloatingPane(dockManager, InitialPaneLocation.DockedTop, pane_);
                                    break;
                                case InitialLocation.DockBottom:
                                    AddFloatingPane(dockManager, InitialPaneLocation.DockedBottom, pane_);
                                    break;
                                case InitialLocation.DockTabbed:
                                    hostPane = dockManager.GetPanes(PaneNavigationOrder.ActivationOrder).FirstOrDefault(
                                        (pane) => pane.IsActivePane);
                                    AddDockTabbedPane(dockManager, hostPane, pane_);
                                    break;
                                default:
                                    m_logger.Warning("Initial location not handled. Adding as floating.");
                                    AddFloatingPane(dockManager, InitialPaneLocation.DockableFloating, pane_);
                                    break;
                            }
                        }
					}
					if ((item as WindowViewModel) != null &&
						(item as WindowViewModel).InitialParameters.IsHeaderVisible == false)
					{
						var window = ToolWindow.GetToolWindow(pane_) as CustomPaneToolWindow;
						if (window != null)
						{
							window.HeaderVisibility = Visibility.Collapsed;
						}
					}
				}
			}
			else if (m_target is DocumentContentHost)
			{
				ContentPane sibling = this.GetSiblingDocument();
				TabGroupPane tgp = null;

				if (sibling != null)
				{
					tgp = LogicalTreeHelper.GetParent(sibling) as TabGroupPane;
					Debug.Assert(null != tgp, "Expected all documents to be within a tab group pane.");
				}

				if (null == tgp)
				{
					SplitPane sp = new SplitPane();
					tgp = new TabGroupPane();
					tgp.Name = "Z" + Guid.NewGuid().ToString("N");
					sp.Panes.Add(tgp);
					DocumentContentHost dch = (DocumentContentHost)m_target;
					dch.Panes.Add(sp);
				}

				tgp.Items.Add(pane_);

				RaiseInitializeContentPane(pane_);
			}
			else
			{
				IList targetCollection = null;

				Debug.Assert(m_target == null || !string.IsNullOrEmpty((string)m_target.GetValue(FrameworkElement.NameProperty)),
					"The Name should be set so the container will not be removed when all the panes have been moved elsewhere. Otherwise new panes may not be displayed.");

				if (m_target is SplitPane)
				{
					targetCollection = ((SplitPane)m_target).Panes;
				}
				else if (m_target is TabGroupPane)
				{
					targetCollection = ((TabGroupPane)m_target).Items;
				}

				if (null != targetCollection)
				{
					targetCollection.Add(pane_);
					RaiseInitializeContentPane(pane_);
				}
			}
		}
        
	    private void AddDockTabbedPane(XamDockManager dockManager, ContentPane host, ContentPane pane)
		{
			if (host != null)
			{
				var sp = host.Parent as SplitPane;
				if (sp != null)
				{
					var i = sp.Panes.IndexOf(host);
					sp.Panes.Remove(host);
					var tgp = new TabGroupPane();
					tgp.Items.Add(host);
					tgp.Items.Add(pane);
					sp.Panes.Insert(i, tgp);
				}
				else
				{
					var tgp = host.Parent as TabGroupPane;
					if (tgp != null)
					{
						tgp.Items.Add(pane);
					}
				}
			}
			else if (ShellModeExtension.ContentHostModeEnabled)
			{
			    var dch = (DocumentContentHost) dockManager.Content;
			    var tgp = FindTabGroupPane(dch.Panes);
                if (tgp == null)
                {
                    var sp = new SplitPane();
                    tgp = new TabGroupPane();
                    sp.Panes.Add(tgp);
                    dch.Panes.Add(sp);
                }
			    tgp.Items.Add(pane);
			}
			else
			{   
                AddFloatingPane(dockManager, InitialPaneLocation.DockableFloating, pane);
			}
		}

        private System.Windows.Controls.TabControl FindTabGroupPane(IList list_)
        {
            foreach (var item in list_)
            {
                if (item is System.Windows.Controls.TabControl)
                {
                    return item as System.Windows.Controls.TabControl;
                }
                if (item is SplitPane)
                {
                    var splitPane = item as SplitPane;
                    var tgp = FindTabGroupPane(splitPane.Panes);
                    if (tgp != null)
                    {
                        return tgp;
                    }
                }
            }
            return null;
        }

		private void AddDockedPane(XamDockManager dockManager, ContentPane host, ContentPane pane, bool first, System.Windows.Controls.Orientation orientation)
		{
			if (host != null)
			{
				var sp = host.Parent as SplitPane;
				if (sp != null)
				{
					InternalAddDockedPane(host, pane, sp, orientation, first);
				}
				else
				{
					var tgp = host.Parent as TabGroupPane;
					if (tgp != null)
					{
						var tgpParent = tgp.Parent as SplitPane;
						if (tgpParent != null)
						{
							InternalAddDockedPane(tgp, pane, tgpParent, orientation, first);
						}
					}
				}
			}
			else
			{
				AddFloatingPane(dockManager, InitialPaneLocation.DockableFloating, pane);
			}
		}

		private static void InternalAddDockedPane(FrameworkElement host, ContentPane pane, SplitPane sp, System.Windows.Controls.Orientation orientation, bool addAsFirst)
		{
			if (host.Visibility == Visibility.Collapsed)
				pane.Visibility = Visibility.Collapsed;

			var i = sp.Panes.IndexOf(host);

			if (sp.SplitterOrientation == orientation)
			{
				sp.Panes.Insert(addAsFirst ? i : i + 1, pane);
			}
			else
			{
				sp.Panes.Remove(host);
				var newSp = new SplitPane() { SplitterOrientation = orientation };
				newSp.Panes.Add(addAsFirst ? pane : host);
				newSp.Panes.Add(addAsFirst ? host : pane);
				sp.Panes.Insert(i, newSp);
			}
		}

		private void AddFloatingPane(XamDockManager dockManager, InitialPaneLocation location, ContentPane pane_)
		{
 
			SplitPane splitPane = new SplitPane();
			InitSplitPane(pane_, splitPane);
            if (FloatingOnly && location != InitialPaneLocation.DockableFloating && location != InitialPaneLocation.FloatingOnly)
            {
                location = InitialPaneLocation.DockableFloating;
            }
            XamDockManager.SetInitialLocation(splitPane, location);
			splitPane.Panes.Add(pane_);
			((XamDockManager)m_target).Panes.Add(splitPane);
		}

		private static void InitSplitPane(ContentPane pane_, SplitPane splitPane)
        {
            WindowViewModel vm = pane_.DataContext as WindowViewModel;
            if (vm == null) return;
            if (!vm.DefaultSize.IsEmpty && vm.InitialParameters.SizingMethod == Core.SizingMethod.Custom)
            {
                splitPane.SetValue(XamDockManager.FloatingSizeProperty, vm.DefaultSize);
            } 
		}
 
  
        private void RestrictWindowSize(ContentPane pane_, IWindowViewContainer container_)
        {
            DependencyObject content = container_.Content as DependencyObject;
            if (content == null) return;
            foreach (var host in content.FindLogicalChildren<WindowsFormsHost>(true))
            {
                Form f = host.Child as Form;
                if (f != null && (!f.MaximumSize.IsEmpty || !f.MinimumSize.IsEmpty))
                {
                    var hostCopied = host;
                    DependencyPropertyDescriptor dpdWidth =
DependencyPropertyDescriptor.FromProperty(FrameworkElement.ActualWidthProperty, typeof(ContentPane));
                    if (dpdWidth != null)
                    {
                        SizeChangedEventHandler handler = null;
                        handler = (sender_, args_) =>
                        {
                            Window w = Window.GetWindow(pane_);
                            if (w == null) return;
                            ContentPane contentPane = sender_ as ContentPane;
                            if (contentPane == null) return;
                            if (pane_.IsFloating())
                            {
                                CustomPaneToolWindow hostWindow = DockHelper.GetRootOfFloatingPane(container_) as CustomPaneToolWindow;
                                if (hostWindow != null && !hostWindow.ContainsMultiple)
                                {
                                    if (f.MaximumSize.Width > 0)
                                    {
                                        double margin = 0;
                                        hostCopied.AccumulateMargin<Window>(ref margin, false);
                                        w.MaxWidth = Math.Min(f.MaximumSize.Width + margin,
                                                              container_.FloatingWindowMaximumWidth ??
                                                              int.MaxValue);
                                    }
                                    if (f.MinimumSize.Width > 0)
                                    {
                                        double margin = 0;
                                        hostCopied.AccumulateMargin<Window>(ref margin, false);
                                        w.MinWidth = Math.Max(f.MinimumSize.Width + margin,
                                                              container_.FloatingWindowMinimumWidth);
                                    }
                                    if (f.MaximumSize.Height > 0)
                                    {
                                        double margin = 0;
                                        hostCopied.AccumulateMargin<Window>(ref margin, true);
                                        w.MaxHeight = Math.Min(f.MaximumSize.Height + margin,
                                                              container_.FloatingWindowMaximumHeight ??
                                                              int.MaxValue);
                                    }
                                    if (f.MinimumSize.Height > 0)
                                    {
                                        double margin = 0;
                                        hostCopied.AccumulateMargin<Window>(ref margin, true);
                                        w.MinHeight = Math.Max(f.MinimumSize.Height + margin,
                                                          container_.FloatingWindowMinimumHeight);
                                    }
                                    return;
                                }
                            }
                            w.ClearValue(FrameworkElement.MaxWidthProperty);
                            w.ClearValue(FrameworkElement.MinWidthProperty);
                            w.ClearValue(FrameworkElement.MaxHeightProperty);
                            w.ClearValue(FrameworkElement.MinHeightProperty);
                        };
                        pane_.SizeChanged += handler;
                        EventHandler<PaneClosedEventArgs> handler2 = null;
                        handler2 = (sender_, e_) =>
                        {
                            pane_.Closed -= handler2;
                            pane_.SizeChanged -= handler;
                        };
                        pane_.Closed += handler2;
                    }


                }
                break;

            }
        }
         

		private void SetUnbindable(ContentPane pane_)
		{
            WindowViewModel vm = pane_.DataContext as WindowViewModel;
            if (vm == null) return;
            CustomContentPane pane = (CustomContentPane)pane_; //a copy for closures
		    IWindowViewContainer viewContainer = (IWindowViewContainer)vm.ViewContainer;
            vm.Width = pane_.ActualWidth;
            vm.Height = pane_.ActualHeight;
            RestrictWindowSize(pane_, viewContainer);

            ((WindowViewContainer)vm.ViewContainer).Visual = pane;
            pane.Name = ContentPaneIdConverter.ConvertToPaneId(vm.ID);
            pane.AccentColour = vm.AccentColour;
            pane.Topmost = vm.Topmost;
            pane.SaveInLayout = !viewContainer.Parameters.Transient;
            vm.Transient = viewContainer.Parameters.Transient;
            vm.ShowInTaskbar = viewContainer.Parameters.ShowInTaskbar;
            pane.AllowPartiallyOffscreen = viewContainer.Parameters.AllowPartiallyOffscreen;
            if (viewContainer.Parameters.Resources != null)
            {
                pane_.Resources.MergedDictionaries.Add(viewContainer.Parameters.Resources);
            }
            if (!viewContainer.Parameters.IsHeaderVisible)
            {
                pane.HeaderVisibleLock = Visibility.Collapsed;
            }

            DependencyPropertyDescriptor dpdWidth =
                DependencyPropertyDescriptor.FromProperty(FrameworkElement.ActualWidthProperty, typeof(ContentPane));
            EventHandler onWidthChanged = null;
            if (dpdWidth != null)
            {
                dpdWidth.AddValueChanged(pane_, onWidthChanged =
                                                delegate(object sender_, EventArgs e_)
                                                {
                                                    ContentPane contentPane = sender_ as ContentPane;
                                                    if (contentPane != null)
                                                    {
                                                        //actual size of the contentpane would changed if minimized
                                                        Window w = Window.GetWindow(contentPane); 
                                                        if (w != null && w.WindowState == WindowState.Minimized) return;

                                                        if (contentPane.IsFloating())
                                                        {
                                                            if (w != null)
                                                            {
                                                                var content = contentPane.Content as FrameworkElement;
                                                                if (content != null && content.ActualWidth <= 0) return;
                                                                double actualWidth = ((content != null) ? content.ActualWidth : contentPane.ActualWidth);

                                                                ((WindowViewModel)pane.DataContext).Width = Core.CoreUtilities.AreClose(actualWidth, 0) ? 100 : actualWidth;
                                                            }
                                                            else
                                                                ((WindowViewModel)pane.DataContext).Width = 100; 
                                                        }
                                                        else
                                                        {
                                                            ((WindowViewModel)pane.DataContext).Width = contentPane.ActualWidth;
                                                        }
                                                    }
                                                });
            }

            DependencyPropertyDescriptor dpdHeight =
                DependencyPropertyDescriptor.FromProperty(FrameworkElement.ActualHeightProperty, typeof(ContentPane));
            EventHandler onHeightChanged = null;
            if (dpdHeight != null)
            {
                dpdHeight.AddValueChanged(pane_, onHeightChanged =
                                                delegate(object sender_, EventArgs e_)
                                                {
                                                    CustomContentPane contentPane = sender_ as CustomContentPane;
                                                    if (contentPane != null)
                                                    {
                                                        //actual size of the contentpane would changed if minimized
                                                        Window w = Window.GetWindow(contentPane);
                                                        if (w != null && w.WindowState == WindowState.Minimized) return;


                                                        if (pane.IsFloating())
                                                        { 
                                                            if (w != null)
                                                            {
                                                                var content = contentPane.Content as FrameworkElement;
                                                                if (content != null && content.ActualHeight <= 0) return;
                                                                double actualHeight = ((content != null) ? content.ActualHeight : contentPane.ActualHeight);

                                                                ((WindowViewModel)pane.DataContext).Height = Core.CoreUtilities.AreClose(actualHeight, 0) ? 100 : actualHeight;
                                                            }
                                                            else
                                                                ((WindowViewModel)pane.DataContext).Height = 100;
                                                        }
                                                        else
                                                        {
                                                            ((WindowViewModel)pane.DataContext).Height = contentPane.ActualHeight;
                                                        }
                                                    }
                                                });
            }

            if (vm.Background != null)
            {
                pane_.Background = vm.Background;
            }

            DependencyPropertyChangedEventHandler onVisibleChanged = null;
            pane_.IsVisibleChanged += onVisibleChanged =
                delegate(object sender_, DependencyPropertyChangedEventArgs e_)
                {
                    if (e_.NewValue != null)
                    {
                        ((WindowViewModel)pane.DataContext).IsVisible = (bool)e_.NewValue;
                    }
                };
            pane.ExecutingCommand += OnExecutingCommand;
            vm.RequestClose += (sender_, e_) => pane.ExecuteCommand(ContentPaneCommands.Close);
            vm.RequestQuietClose += (sender_, e_) =>
            {
                m_toCloseQuietly.Add(pane);
                pane.ExecuteCommand(ContentPaneCommands.Close);
            };

            vm.RequestFlash += (sender_, e_) =>
            {
                //if (m_application.CurrentTheme != null && m_application.CurrentTheme.Flash != null)
                //  m_application.CurrentTheme.Flash(pane_, e_.Value1);
                //flashing dependent on Themes and on IApplication was a terrible idea!
                flashExecuter.Flash(pane, e_.Value1);
            };

            vm.RequestActivate += (sender, e) =>
            {
                Window w = Window.GetWindow(pane);
                if (w != null)
                {
                    if (w.WindowState == WindowState.Minimized)
                    {
                        var hwndSource = (HwndSource)PresentationSource.FromVisual(w);
                        Win32.ShowWindow(hwndSource.Handle, Win32.ShowWindowCommands.SW_RESTORE);
                    }
                    if (((WindowViewModel)pane.DataContext).Host != null)
                    {
                        var tab = ((ShellTabViewModel)((WindowViewModel)pane.DataContext).Host);
                        foreach (var xamTabControl in w.FindVisualChildren((dependencyObject) => dependencyObject is XamTabControl).Select(
                                o => ((XamTabControl)o)))
                        {
                            xamTabControl.SelectedIndex =
                                xamTabControl.Items.Cast<ShellTabViewModel>().TakeWhile(shellTabViewModel => shellTabViewModel != tab).
                                    Count();
                            break;
                        }
                    }
                }
                pane.ExecuteCommand(ContentPaneCommands.ActivatePane);
                if (!pane.IsPinned)
                    pane.ExecuteCommand(ContentPaneCommands.Flyout);
                if (w != null)
                    w.Activate();
                pane.Activate();
            };

            DependencyPropertyDescriptor dpdPaneLocation =
                DependencyPropertyDescriptor.FromProperty(ContentPane.PaneLocationProperty, typeof(ContentPane));
            EventHandler onLocationChanged = null;
            if (dpdPaneLocation != null)
            {
                dpdPaneLocation.AddValueChanged(pane_, onLocationChanged = delegate(object sender_, EventArgs e_)
                {
                    CustomContentPane contentPane = sender_ as CustomContentPane;
                    if(contentPane == null) return;
                    
                    var viewModel = ((WindowViewModel)pane.DataContext);
                    WindowViewContainer viewContainerLocal = (WindowViewContainer) viewModel.ViewContainer;
                    viewContainerLocal.V2vHost = pane;
                    ((WindowViewModel)pane.DataContext).IsFloating = contentPane.IsFloating();
                    if (contentPane.PaneLocation != PaneLocation.Unknown && contentPane.PaneLocation != PaneLocation.Unpinned)
                    {
                        viewContainerLocal.Root = DockHelper.GetRootObject(viewContainerLocal);
                    }
                    if (!FloatingWindowUtil.SeparateHeaderItemsInFloatingWindow && contentPane.HeaderControl != null)
                    {
                        contentPane.HeaderControl.BindContentObject();
                    }
                    /*if (viewModel.Host as ShellTabViewModel == null)
                        viewModel.Host = pane;*/

                });
            }

            PropertyChangedEventHandler onPropertyChanged;
            vm.PropertyChanged += onPropertyChanged = delegate(object sender_, PropertyChangedEventArgs e_)
            {
                WindowViewModel wvm = sender_ as WindowViewModel;
                if (wvm == null) return;
                var _pane = pane;
                switch (e_.PropertyName)
                {
                    case "Background":
                        _pane.Background = wvm.Background;
                        //and it's impossible to "unset" the background and get back to theme-set values
                        break;
                    case "IsFloating":
                        if (wvm.IsFloating != _pane.IsFloating())
                        {
                            _pane.ExecuteCommand(ContentPaneCommands.ToggleDockedState);
                        }
                        break;
                    case "IsActive":
                        if (wvm.IsActive)
                        {
                            flashExecuter.PaneActivated(_pane);
                        }
                        break;
                    case "ID":
                        _pane.Name = ContentPaneIdConverter.ConvertToPaneId(wvm.ID);
                        break;
                    case "Width":
                        if (_pane.CanChangeDimensions)
                        {
                            _pane.SetContentWidth(wvm.Width); 
                        }
                        break;
                    case "Height":
                        if (_pane.CanChangeDimensions)
                        {
                            _pane.SetContentHeight(wvm.Height); 
                        } 
                        break;
                    case "FloatingWindowMaximumWidth":
                        if (wvm.FloatingWindowMaximumWidth.HasValue && _pane.CanChangeDimensions)
                        {
                            _pane.SetContentMaximumWidth(wvm.FloatingWindowMaximumWidth.Value);
                        }
                        break;
                    case "FloatingWindowMinimumWidth":
                        if (_pane.CanChangeDimensions)
                        {
                            _pane.SetContentMinimumWidth(wvm.FloatingWindowMinimumWidth);
                        }
                        break;
                    case "CanMaximize":
                        _pane.SetCanMaximize(wvm.CanMaximize);
                        break;
                    case "FloatingWindowMaximumHeight":
                        if (wvm.FloatingWindowMaximumHeight.HasValue && _pane.CanChangeDimensions)
                        {
                            _pane.SetContentMaximumHeight(wvm.FloatingWindowMaximumHeight.Value);
                        }
                        break;
                    case "FloatingWindowMinimumHeight":
                        if (_pane.CanChangeDimensions)
                        {
                            _pane.SetContentMinimumHeight(wvm.FloatingWindowMinimumHeight);
                        }
                        break;
                    case "AccentColour":
                        _pane.AccentColour = wvm.AccentColour;
                        break;
                    case "Topmost":
                        _pane.Topmost = wvm.Topmost;
                        break;
                    case "OwnedByContainer":
                        _pane.SetOwnedByContainer();
                        break;
                    case "Transient":
                        _pane.SaveInLayout = !wvm.Transient;
                        break;
                    case "IsInActiveWorkspace":
                        if (ChromeManagerBase.TabsOwnFloatingWindows && (wvm.IsHidden == wvm.IsInActiveWorkspace))
                        {
                            wvm.IsHidden = !wvm.IsInActiveWorkspace;


                            //var window = Window.GetWindow(pane_);
                            //if (wvm.IsInActiveWorkspace)
                            //{
                            //    var info = window.Tag as Tuple<bool, double, double>;
                            //    if (info != null)
                            //    {
                            //        window.ShowInTaskbar = info.Item1;
                            //        window.Left = info.Item2;
                            //        window.Top = info.Item3;
                            //        window.Tag = null;
                            //    }
                            //    else if (window.Left != -5000)
                            //    {
                            //        info = new Tuple<bool, double, double>(window.ShowInTaskbar, window.Left, window.Top);
                            //        window.Tag = info;
                            //    }
                            //}
                            //else if (window.Tag == null)
                            //{
                            //    var info = new Tuple<bool, double, double>(window.ShowInTaskbar, window.Left, window.Top);
                            //    window.Left = -5000;
                            //    window.ShowInTaskbar = false;
                            //    window.Tag = info;
                            //}
                        }
                        break;
                    //case "ShowInTaskbar":
                    //    if (_pane != null)
                    //    {
                    //        _pane.ShowInTaskbar = !wvm.ShowInTaskbar;
                    //    }
                    //    break;
                }
            };
            EventHandler<PaneClosedEventArgs> handler = null;
            handler =
                (sender_, e_) =>
                {
                    if (pane_ == null) return;

                    if (dpdWidth != null && onWidthChanged != null)
                    {
                        dpdWidth.RemoveValueChanged(pane_, onWidthChanged);
                    }
                    if (dpdHeight != null && onHeightChanged != null)
                    {
                        dpdHeight.RemoveValueChanged(pane_, onHeightChanged);
                    }
                    if (dpdPaneLocation != null && onLocationChanged != null)
                    {
                        dpdPaneLocation.RemoveValueChanged(pane_, onLocationChanged);
                    }
                    pane_.IsVisibleChanged -= onVisibleChanged;
                    vm.PropertyChanged -= onPropertyChanged;
                    pane_.ClearValue(ContentControl.ContentProperty);
                    pane_.Closed -= handler;
                    pane_.ExecutingCommand -= OnExecutingCommand;
                    pane_ = null;
                    vm = null;
                    dpdWidth = null;
                    dpdHeight = null;
                };
            pane_.Closed += handler;
		}
        
        private void OnExecutingCommand(object sender, ExecutingCommandEventArgs args)
        {
            if (args.Command == ContentPaneCommands.ChangeToDocument)
            {
                var pane = sender as CustomContentPane;
                if (pane != null)
                {
                    if (pane.Topmost)
                    {
                        pane.Topmost = false;
                    }
                }
            }
        }
         

		private ContentPane GetSiblingDocument()
		{
			DocumentContentHost dch = m_target as DocumentContentHost;

			if (dch == null)
				return null;

			if (null != dch.ActiveDocument)
				return dch.ActiveDocument;

			XamDockManager dm = XamDockManager.GetDockManager(dch);

			if (dm == null)
				return null;

			ContentPane firstDocument = null;

			foreach (ContentPane cp in dm.GetPanes(PaneNavigationOrder.VisibleOrder))
			{
				if (cp.PaneLocation != PaneLocation.Document)
					continue;

				if (firstDocument == null)
					firstDocument = cp;

				if (cp.Visibility != Visibility.Visible)
					continue;

				return cp;
			}

			return firstDocument;
		}

		private readonly HashSet<ContentPane> m_toCloseQuietly = new HashSet<ContentPane>();

		private void OnPaneClosing(object sender_, PaneClosingEventArgs e_)
		{
			ContentPane pane = sender_ as ContentPane;
			if (m_toCloseQuietly.Contains(pane))
            {
                if (!e_.Cancel)
                {
                    pane.Closing -= OnPaneClosing;     
                }
				return;
			}
			if (ClosingWorkSpace != null && ClosingWorkSpace.CanExecute(pane.DataContext))
			{
				ClosingWorkSpace.Execute(pane.DataContext);
                if (!e_.Cancel)
                {
                    pane.Closing -= OnPaneClosing;
                }
            }
			else
			{
				e_.Cancel = true;
			}
		}

		private void OnPaneClosed(object sender_, PaneClosedEventArgs e_)
		{
            if (e_.OriginalSource != sender_) return;
			ContentPane pane = sender_ as ContentPane;
		    pane.Closed -= OnPaneClosed;
			IEditableCollectionView cv = CollectionViewSource.GetDefaultView(this.ItemsSource) as IEditableCollectionView;
			if (null != cv)
			{
				object dataItem = GetItemForContainer(pane);
				cv.Remove(dataItem);
			}
			if (m_toCloseQuietly.Contains(pane))
			{
				m_toCloseQuietly.Remove(pane);
				return;
			}
			if (ClosedWorkSpace != null && ClosedWorkSpace.CanExecute(pane.DataContext))
			{
				ClosedWorkSpace.Execute(pane.DataContext);
			}
			BindingOperations.ClearAllBindings(pane);
			pane.DataContext = null;
		}

		private void RaiseInitializeContentPane(ContentPane pane_)
		{
			if (null != m_target)
			{
				InitializeContentPaneEventArgs args = new InitializeContentPaneEventArgs(pane_);
				args.RoutedEvent = InitializeContentPaneEvent;
				UIElementHelper.RaiseEvent(m_target, args);
			}
		}

		/// <summary>
		/// Invoked when a ContentPane for a given item is being removed.
		/// </summary>
		/// <param name="cp_">The pane being removed</param>
		protected virtual void RemovePane(ContentPane cp_)
		{
			cp_.CloseAction = PaneCloseAction.RemovePane;
			cp_.ExecuteCommand(ContentPaneCommands.Close);

			Debug.Assert(cp_.Parent == null, "Pane was not removed.");
		}

		/// <summary>
		/// InitializeContentPane Attached Routed Event
		/// </summary>
		public static readonly RoutedEvent InitializeContentPaneEvent = EventManager.RegisterRoutedEvent("InitializeContentPane",
			RoutingStrategy.Direct, typeof(EventHandler<InitializeContentPaneEventArgs>), typeof(ContentPaneFactory));

	    /// <summary>
		/// Adds a handler for the InitializeContentPane attached event
		/// </summary>
		/// <param name="element_">UIElement or ContentElement that listens to the event</param>
		/// <param name="handler_">Event handler to be added</param>
		public static void AddInitializeContentPaneHandler(DependencyObject element_, EventHandler<InitializeContentPaneEventArgs> handler_)
		{
			UIElementHelper.AddHandler(element_, InitializeContentPaneEvent, handler_);
		}

		/// <summary>
		/// Removes a handler for the InitializeContentPane attached event
		/// </summary>
		/// <param name="element_">UIElement or ContentElement that listens to the event</param>
		/// <param name="handler_">Event handler to be removed</param>
		public static void RemoveInitializeContentPaneHandler(DependencyObject element_, EventHandler<InitializeContentPaneEventArgs> handler_)
		{
			UIElementHelper.RemoveHandler(element_, InitializeContentPaneEvent, handler_);
		}
        
        /// <summary>
        /// This hook is used by CafGUI.Toolkit integration
        /// </summary>
        public static event Action<object, object> AddingPane;

        protected virtual void OnAddingPane(object pane, object viewContainer)
        {
            var handler = AddingPane;
            if (handler != null) handler(pane, viewContainer);
        }
	}

	/// <summary>
	/// Event arguments for the <see cref="ContentPaneFactory.InitializeContentPaneEvent"/>
	/// </summary>
	public class InitializeContentPaneEventArgs : RoutedEventArgs
	{
		private readonly ContentPane m_pane;

		/// <summary>
		/// Initializes a new <see cref="InitializeContentPaneEventArgs"/>
		/// </summary>
		/// <param name="pane_">The pane being initialized</param>
		public InitializeContentPaneEventArgs(ContentPane pane_)
		{
			if (pane_ == null)
				throw new ArgumentNullException("pane_");

			m_pane = pane_;
		}

		/// <summary>
		/// Returns the pane being initialized
		/// </summary>
		public ContentPane Pane
		{
			get { return m_pane; }
		}
	}
}
