﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Extensions/MVVMDock/BindingHelper.cs#4 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System.Windows.Data;
using System.Windows;
using System.Runtime.CompilerServices;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions
{
	/// <summary>
	/// Static class with methods relating to binding
	/// </summary>
	public static class BindingHelper
	{
		#region Member Variables
		
		private static bool m_xmlLoaded = false;

		#endregion //Member Variables

		#region Methods

		#region BindPath
		/// <summary>
		/// Binds the specified target property to the specified path property.
		/// </summary>
		/// <param name="item">The underlying data item</param>
		/// <param name="container">The element associated with the item</param>
		/// <param name="path">The path property to provide the value for the specified <paramref name="targetProperty"/></param>
		/// <param name="targetProperty">The property whose value is to come from the specified path of the underlying data item.</param>
		public static void BindPath(DependencyObject container, object item, string path, DependencyProperty targetProperty)
		{
			if (string.IsNullOrEmpty(path))
				return;

			Binding b = new Binding();

			if (IsXmlNode(item))
				b.XPath = path;
			else
				b.Path = new PropertyPath(path);

			BindingOperations.SetBinding(container, targetProperty, b);
		}
		#endregion //BindPath

		#region IsXmlNode
		/// <summary>
		/// Returns a boolean indicating if the specified object is an xml node object without forcing the Xml assembly to be loaded
		/// </summary>
		/// <param name="item">The object to evaluate</param>
		/// <returns>True if the object is or derives from System.Xml.XmlNode</returns>
		public static bool IsXmlNode(object item)
		{
			if (null != item && (m_xmlLoaded || item.GetType().FullName.StartsWith("System.Xml")))
			{
				return IsXmlNodeHelper(item);
			}

			return false;
		}

		[MethodImpl(MethodImplOptions.NoInlining)]
		private static bool IsXmlNodeHelper(object item)
		{
			m_xmlLoaded = true;
			return item is System.Xml.XmlNode;
		}
		#endregion //IsXmlNode

		#endregion //Methods
	}
}
