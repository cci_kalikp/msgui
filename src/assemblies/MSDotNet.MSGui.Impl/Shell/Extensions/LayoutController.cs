﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Extensions/LayoutController.cs#6 $
// $Change: 896453 $
// $DateTime: 2014/09/11 00:04:12 $
// $Author: caijin $

using System;
using System.Windows;
using System.Xml.Linq;
using Infragistics.Windows.DockManager;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls; 

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions
{
	internal class DockManagerLayoutController : Freezable
	{
		public DockManagerLayoutController()
		{
		}

		public static readonly DependencyProperty ControllerProperty = DependencyProperty.RegisterAttached(
		 "Controller",
		 typeof(DockManagerLayoutController),
		 typeof(DockManagerLayoutController),
		 new FrameworkPropertyMetadata(
			 null,
			 FrameworkPropertyMetadataOptions.AffectsMeasure,
			 ControllerPropertyChanged));

		private XamDockManager m_dockManager;
		private static void ControllerPropertyChanged(DependencyObject d_, DependencyPropertyChangedEventArgs e_)
		{
			if (d_ is XamDockManager && e_.NewValue is DockManagerLayoutController && e_.NewValue != e_.OldValue) //TODO remove from old
			{
				DockManagerLayoutController controller = ((DockManagerLayoutController)e_.NewValue);
				XamDockManager dockManager = (XamDockManager)d_;
				controller.m_dockManager = dockManager;
			}
		}

		public static DockManagerLayoutController GetController(DependencyObject d_)
		{
			return (DockManagerLayoutController)d_.GetValue(ControllerProperty);
		}

		/// <summary>
		/// Sets the object that will create ContentPane instances based on the items in the associate <see cref="ContainerFactoryBase.ItemsSource"/>
		/// </summary>
		public static void SetController(DependencyObject d_, DockManagerLayoutController value_)
		{
			d_.SetValue(ControllerProperty, value_);
		}

		public static readonly DependencyProperty EventHostProperty = DependencyProperty.Register("EventHost",
			typeof(ILayoutSaveLoadRequester), typeof(DockManagerLayoutController), new FrameworkPropertyMetadata(OnEventHostChanged));

		private static void OnEventHostChanged(DependencyObject d_, DependencyPropertyChangedEventArgs e_)
		{
			if (d_ is DockManagerLayoutController && e_.NewValue is ILayoutSaveLoadRequester)
			{
				ILayoutSaveLoadRequester requester = (ILayoutSaveLoadRequester)e_.NewValue;
				DockManagerLayoutController controller = (DockManagerLayoutController)d_;
				if (e_.OldValue is ILayoutSaveLoadRequester)
				{
					var oldrequester = (ILayoutSaveLoadRequester)e_.OldValue;
					oldrequester.LoadRequested -= controller.OnLoadRequested;
					oldrequester.SaveRequested -= controller.OnSaveRequested;
				}
				requester.SaveRequested += controller.OnSaveRequested;
				requester.LoadRequested += controller.OnLoadRequested;
			}
		}

		private void OnSaveRequested(SaveEventArgs args_)
		{
			try
			{
				// Tell the XamDockManager to save its current layout to the string
				string layoutXml = m_dockManager.SaveLayout();
				// Ugly workaround. TODO
				layoutXml = layoutXml.Replace("\"NaN\"", "\"100\"");

				args_.SavedState = XDocument.Parse(layoutXml);
			}
			catch (Exception ex)
			{
				throw new InvalidOperationException("Exception during Save:" + Environment.NewLine + ex.Message, ex);
			}
		}

		private void OnLoadRequested(LoadEventArgs state_)
		{
			string layout = state_.State.ToString();
			try
			{
				// Tell XamDockManager to load the layout from the string
				m_dockManager.LoadLayout(layout); //we could also use XmlWriter here, might have better performance  
			}
			catch (Exception ex)
			{
				throw new InvalidOperationException("Exception during Load:" + Environment.NewLine + ex.Message, ex);
			}
		}

		public ILayoutSaveLoadRequester EventHost
		{
			get
			{
				return (ILayoutSaveLoadRequester)GetValue(EventHostProperty);
			}
			set
			{
				SetValue(EventHostProperty, value);
			}
		}

		protected override Freezable CreateInstanceCore()
		{
			return (WindowRenamer)Activator.CreateInstance(this.GetType());
		}
	}
}