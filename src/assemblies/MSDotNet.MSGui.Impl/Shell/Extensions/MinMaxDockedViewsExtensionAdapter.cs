﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Extensions/MinMaxDockedViewsExtensionAdapter.cs#17 $
// $Change: 898794 $
// $DateTime: 2014/09/30 05:29:21 $
// $Author: caijin $


using System;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows;
using Infragistics.Windows.DockManager;
using System.Windows.Media; 
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager; 
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions
{
  internal class MinMaxDockedViewsExtensionAdapter
  {
    private readonly XamDockManager m_dockManager;
    private readonly TabbedDockViewModel m_tabbedDockModel;
    private CommandBinding m_maximizeWindowCommandBinding;

    public MinMaxDockedViewsExtensionAdapter(XamDockManager dockManager_, TabbedDockViewModel tdm_)
    {
      m_tabbedDockModel = tdm_;
      m_dockManager = dockManager_;
    }
    
    public void Attach()
    {      
       if (m_dockManager != null)
       {
         m_dockManager.PreviewMouseDoubleClick += OnPreviewMouseDoubleClick;
         m_maximizeWindowCommandBinding = new CommandBinding(Command.MaximizeViewCommand, HandleMaximizeViewCommand,
                                                             (o, x) =>
                                                               {
                                                                 x.CanExecute = true;
                                                                 x.Handled = true;
                                                               });
         m_dockManager.CommandBindings.Add(m_maximizeWindowCommandBinding);
       }
    }

    public void Detach()
    {
       if (m_dockManager != null)
       {
         m_dockManager.PreviewMouseDoubleClick -= OnPreviewMouseDoubleClick;
         m_dockManager.CommandBindings.Remove(m_maximizeWindowCommandBinding);
       }
    }
    
    private void HandleMaximizeViewCommand(object o, ExecutedRoutedEventArgs e)
    {
      var cp = e.Parameter as ContentPane;            
      ToggleMaxMin(cp);
      e.Handled = true;
    }
    
    private void OnPreviewMouseDoubleClick(object sender_, MouseButtonEventArgs e_)
    {
      DependencyObject source = e_.OriginalSource as DependencyObject;

      while (!(source == null || source is PaneHeaderPresenter || source is HeaderItemsHolder || source is ButtonBase) && source is Visual)
      {
        source = VisualTreeHelper.GetParent(source);
      }

      if (source is HeaderItemsHolder || source is ButtonBase)
      {
        return;
      }

      var px = source as PaneHeaderPresenter;
      if (px == null)
      {
        return;
      }      
      
      var wm = px.Pane.DataContext as WindowViewModel;
      if (wm != null && ((IWindowViewContainer) wm.ViewContainer).UnpinOnHeaderDoubleClick)
      {
          px.Pane.IsPinned = false;
      }
      else
      {
          ToggleMaxMin(px.Pane);
      }

      e_.Handled = true;
    }

		//private readonly HashSet<WindowViewModel> m_maximized = new HashSet<WindowViewModel>();

    private void ToggleMaxMin(ContentPane pane_)
    {
        if (m_tabbedDockModel.HidingLockingManager.Granularity.HasFlag(HideLockUIGranularity.DisableViewMaximize))
        {
            return;
        }

      WindowViewModel windowModel = pane_.DataContext as WindowViewModel;
      if (windowModel == null)
      {
        return;
      }

      ShellTabViewModel currentTabViewModel = m_tabbedDockModel.CurrentTab as ShellTabViewModel;
      if (currentTabViewModel == null)
      {
        return;
      }

      if (windowModel.Host != currentTabViewModel)
      {
        return; //may be a floating window. ignore.
      }

			if (AdditionalLayoutInfo.MaximizedViews.Contains(windowModel.ID))
      {
        Demaximize(windowModel, currentTabViewModel);
      }
      else
      {
        Maximize(windowModel, currentTabViewModel);
      }      
    }

		//private readonly List<WindowViewModel> hackUnpinnedWindows = new List<WindowViewModel>();

      private void Demaximize(WindowViewModel windowModel_, ShellTabViewModel currentTabViewModel_)
      {
          foreach (WindowViewModel window in m_tabbedDockModel.Workspaces)
          {
              if (window.Host == currentTabViewModel_)
              {
                  if (AdditionalLayoutInfo.TemporarilyHiddenViews.Contains(window.ID))
                  {
                      window.IsPinned = true;
                      AdditionalLayoutInfo.TemporarilyHiddenViews.Remove(window.ID);
                  } /*
					else
					{
          window.IsPinned = true;
					}*/
              }
          }
          AdditionalLayoutInfo.MaximizedViews.Remove(windowModel_.ID);
          windowModel_.HostChangedInternal -= windowModel__HostChangedInternal;
      }

      private void Maximize(WindowViewModel windowModel_, ShellTabViewModel currentTabViewModel_)
      {
          foreach (WindowViewModel window in m_tabbedDockModel.Workspaces)
          {
              if (window.Host == currentTabViewModel_)
              {
                  if (window.IsPinned && window != windowModel_)
                  {
                      AdditionalLayoutInfo.TemporarilyHiddenViews.Add(window.ID);
                  }

                  window.IsPinned = window == windowModel_;
                  if (AdditionalLayoutInfo.MaximizedViews.Contains(window.ID) && window != windowModel_)
                  {
                      AdditionalLayoutInfo.MaximizedViews.Remove(window.ID);
                  }
              }
          }
          AdditionalLayoutInfo.MaximizedViews.Add(windowModel_.ID);
          windowModel_.HostChangedInternal -= windowModel__HostChangedInternal;
          windowModel_.HostChangedInternal += windowModel__HostChangedInternal;
          EventHandler closedHandler = null;
          closedHandler = (sender_, args_) =>
              {
                  windowModel_.HostChangedInternal -= windowModel__HostChangedInternal;
                  windowModel_.Closed -= closedHandler;
                  windowModel_.ClosedQuietly -= closedHandler;
              }; 
          windowModel_.Closed += closedHandler;
          windowModel_.ClosedQuietly += closedHandler;
      }
       
      private void windowModel__HostChangedInternal(object sender, WindowViewContainer.HostChangedEventArgs e)
      {
          WindowViewModel windowModel = sender as WindowViewModel;
          if (windowModel != null)
          {
              windowModel.HostChangedInternal -= windowModel__HostChangedInternal;
              ShellTabViewModel tabViewModel = e.OldHost as ShellTabViewModel;
              if (tabViewModel != null)
              {
                  if (AdditionalLayoutInfo.MaximizedViews.Contains(windowModel.ID))
                  {
                      Demaximize(windowModel, tabViewModel);
                  }
              } 
          } 
      } 
  }
}
