﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Extensions/WindowRenamer.cs#7 $
// $Change: 895203 $
// $DateTime: 2014/09/02 03:46:50 $
// $Author: caijin $
 
using System.Windows;
using System.Windows.Input;
using Infragistics.Windows.Controls;
using Infragistics.Windows.DockManager;
using Infragistics.Windows.DockManager.Events;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions
{
    internal class WindowRenamer : RenamerBase
    { 
        private XamDockManager dockManager;
         
        protected override void SetupTitleChangeBinding(DependencyObject associator_)
        {
            if (associator_ != null)
            {
                this.dockManager = associator_ as XamDockManager;
            }
            if (dockManager == null || Gesture == null) return;
            AddTitleChangeBinding(dockManager);
            foreach (ToolWindow toolWindow in dockManager.GetAllToolWindows())
            {
                AddTitleChangeBinding(toolWindow);
            }
            dockManager.ToolWindowLoaded += OnToolWindowLoaded;
        }

        private void OnToolWindowLoaded(object sender_, PaneToolWindowEventArgs e_)
        {
            PaneToolWindow paneToolWindow = e_.Window;
            AddTitleChangeBinding(paneToolWindow);
        }

        protected override void ReleaseTitleChangeBinding(KeyGesture gesture_)
        {
            RemoveTitleChangeBinding(dockManager, gesture_);
            foreach (ToolWindow toolWindow in dockManager.GetAllToolWindows())
            {
                RemoveTitleChangeBinding(toolWindow, gesture_);
            }
            dockManager.ToolWindowLoaded -= OnToolWindowLoaded;
        }
         
        protected override void RenameTitle(object sender_, ExecutedRoutedEventArgs e_)
        {
            ContentPane pane = null;
            FrameworkElement element = e_.OriginalSource as FrameworkElement;
            if (element != null)
            {
                var window = ToolWindow.GetToolWindow(element) as CustomPaneToolWindow;
                if (window != null)
                {
                    pane = CustomContentPane.GetActivePane(window);
                }
            }
            if (pane == null)
            {
                pane = dockManager.ActivePane;
            }
            if (pane != null)
            {
                pane.RaiseEvent(new RoutedEventArgs(Attached.TitleNeedChangingEvent));
                e_.Handled = true;
            }
        }

    }
}
