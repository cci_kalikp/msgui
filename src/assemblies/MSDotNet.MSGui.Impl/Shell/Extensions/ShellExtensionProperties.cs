﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Extensions/ShellExtensionProperties.cs#9 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System.Windows;
using Infragistics.Windows.DockManager;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions
{
  //TODO rewrite me
  internal static class ShellExtensionProperties
  {
    public static readonly DependencyProperty MaximizeInDockingAreaControllerProperty = DependencyProperty.RegisterAttached(
      "MaximizeInDockingAreaController",
      typeof(TabbedDockViewModel),
      typeof(ShellExtensionProperties),
      new FrameworkPropertyMetadata(
        null,
        FrameworkPropertyMetadataOptions.AffectsMeasure,
        MaximizeInDockingAreaControllerPropertyChanged));

    public static void SetMaximizeInDockingAreaController(DependencyObject dependencyObject, TabbedDockViewModel value)
    {
      dependencyObject.SetValue(MaximizeInDockingAreaControllerProperty, value);
    }

    public static TabbedDockViewModel GetMaximizeInDockingAreaController(DependencyObject dependencyObject)
    {
      return (TabbedDockViewModel)dependencyObject.GetValue(MaximizeInDockingAreaControllerProperty);
    }

    public static void MaximizeInDockingAreaControllerPropertyChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs ar)
    { 
      TabbedDockViewModel tabbedDockViewModel = ar.NewValue as TabbedDockViewModel;
      XamDockManager dockManager = dependencyObject as XamDockManager;
      if (dockManager != null && tabbedDockViewModel != null)
      {
		  MinMaxDockedViewsExtensionAdapter maxMinController =
			new MinMaxDockedViewsExtensionAdapter(dockManager, tabbedDockViewModel);
		  maxMinController.Attach(); //we never detach it. this behaviour is for one time binding only        
      }
    }    

    public static readonly DependencyProperty CustomDockManagerFloatingPanesProperty = 
      DependencyProperty.RegisterAttached(
        "CustomDockManagerFloatingPanes",
        typeof(bool),
        typeof(ShellExtensionProperties),
        new FrameworkPropertyMetadata(
          false,
          FrameworkPropertyMetadataOptions.AffectsMeasure,
          CustomDockManagerFloatingPanesChanged));

    public static void SetCustomDockManagerFloatingPanes(DependencyObject dependencyObject_, bool value_)
    {
      dependencyObject_.SetValue(CustomDockManagerFloatingPanesProperty, value_);
    }

    public static bool GetCustomDockManagerFloatingPanes(DependencyObject dependencyObject_)
    {
      return (bool)dependencyObject_.GetValue(CustomDockManagerFloatingPanesProperty);
    }

    public static void CustomDockManagerFloatingPanesChanged(DependencyObject dpo_, DependencyPropertyChangedEventArgs ar_)
    {
      XamDockManager dockWindow = dpo_ as XamDockManager;

      if (dockWindow != null)
      {
        MaximizeFloatingWindowExtensionAdapter maximizeAdapter = new MaximizeFloatingWindowExtensionAdapter(dockWindow);
        maximizeAdapter.Attach();
      }
    }    
  }
}