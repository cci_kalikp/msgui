﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Extensions/XamDockManagerExtensions.cs#17 $
// $Change: 896821 $
// $DateTime: 2014/09/15 02:20:20 $
// $Author: caijin $

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel; 
using System.Windows;
using System.Windows.Data;
using Infragistics.Windows;
using Infragistics.Windows.Controls;
using Infragistics.Windows.DockManager;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls; 

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions
{
    internal static class XamDockManagerExtensions
    {

        public static bool FocusHackEnabled = false;
        #region reflection hack
        public static readonly DependencyProperty ReflectionHackProperty = DependencyProperty.RegisterAttached(
         "ReflectionHack",
         typeof(bool),
         typeof(XamDockManagerExtensions),
         new FrameworkPropertyMetadata(
           false,
           FrameworkPropertyMetadataOptions.AffectsMeasure,
           ReflectionHackPropertyChanged));

        //this hack here is to create an instance of CustomPaneToolWindow instead of PaneToolWindow when floating panes are added
        //so when upgrade ig, you should make sure all other logics Infragistics.Windows.DockManager.XamDockManager.PanesCollection.OnFloatingPanesChanged 
        //and Infragistics.Windows.DockManager.XamDockManager.OnFloatingPaneAdded are copied 
        private static void ReflectionHackPropertyChanged(DependencyObject d_, DependencyPropertyChangedEventArgs param_)
        {
            if (d_ is XamDockManager) //should be set only once for each xdm 
            {
                XamDockManager dockManager = (XamDockManager)d_;
                manager = dockManager;
                //lots of reflection below
                //fixes invisible popups bug  MSDOTNETMAIN-1000
                //be careful here when update IG libraries 
                ObservableCollection<SplitPane> floPane = ReflHelper.FieldGet(dockManager.Panes, "_floatingPanes") as ObservableCollection<SplitPane>;
                ReflHelper.RemoveInstanceEventHandler(floPane, "CollectionChanged", "OnFloatingPanesChanged", dockManager.Panes,
                                                       dockManager.Panes.GetType());

                floPane.CollectionChanged += (sender, e) => OnFloatingPanesChanged(sender, e, dockManager);

                if (XamDockManagerExtensions.FocusHackEnabled)
                {
                    dockManager.ActivePaneChanged += (o, e) =>
                    {
                        var d = (XamDockManager)o;

                        if (d.CurrentFlyoutPane != null && d.CurrentFlyoutPane != d.ActivePane)
                            d.CurrentFlyoutPane.ExecuteCommand(ContentPaneCommands.FlyIn);
                    };
                }
                dockManager.PaneDragEnded += (sender, args) =>
                    { 
                        var panesPerWindow = new List<CustomPaneToolWindow>();
                        foreach (var sp in dockManager.GetPanes(PaneNavigationOrder.ActivationOrder))
                        {
                            var toolWindow = ToolWindow.GetToolWindow(sp) as CustomPaneToolWindow;
                            if (toolWindow != null && !panesPerWindow.Contains(toolWindow))
                            {
                                toolWindow.RefreshHeaderBindings(args.Panes);
                                panesPerWindow.Add(toolWindow); 
                            }
                        } 
                    };
  
            }
        }

        public static void SetReflectionHack(DependencyObject dependencyObject, bool value)
        {
            dependencyObject.SetValue(ReflectionHackProperty, value);
        }

        public static bool GetReflectionHack(DependencyObject dependencyObject)
        {
            return (bool)dependencyObject.GetValue(ReflectionHackProperty);
        }


        #region Functions copied from Infragistics classes to replace PaneToolWindow creation with our own class
        //!!! be careful when updating to the next version of Infragistics. These functions might be chenged.

        private static void OnFloatingPanesChanged(object sender_, NotifyCollectionChangedEventArgs e_, XamDockManager dockManager)
        {
            var added = (IList<SplitPane>)ReflHelper.PropertyGet(e_, "ItemsAdded");
            var removed = (IList<SplitPane>)ReflHelper.PropertyGet(e_, "ItemsRemoved");
            // deal with items added/removed
            for (int i = 0, count = added.Count; i < count; i++)
            {
                SplitPane pane = added[i];

                if (null != pane)
                {
                    OnFloatingPaneAdded(pane, dockManager);
                }
            }

            for (int i = 0, count = removed.Count; i < count; i++)
            {
                SplitPane pane = removed[i];

                if (null != pane)
                {
                    ReflHelper.MethodInvoke(dockManager, "OnFloatingPaneRemoved", new[] { typeof(SplitPane) },
                                            new object[] { pane });
                }
            }
        }

        private static void OnFloatingPaneAdded(SplitPane pane, XamDockManager dockManager)
        {
            bool isDesignMode = DesignerProperties.GetIsInDesignMode(pane);
            var dmuType = typeof(XamDockManager).Assembly.GetType("Infragistics.Windows.DockManager.DockManagerUtilities");
            var paneLocation = (PaneLocation)ReflHelper.StaticMethodInvoke(dmuType, "ToPaneLocation",
                                                                      new object[] { XamDockManager.GetInitialLocation(pane) }); 
            var dmbType = typeof(XamDockManager).Assembly.GetType("Infragistics.Windows.DockManager.DockManagerKnownBoxes");
            if (isDesignMode)
            {
                // AS 5/20/08 BR31679
                // Just keep the element as a logical child of the xdm so that we
                // can still have the attached properties exposed for the elements.
                //
                pane.SetValue(
                  (DependencyPropertyKey)ReflHelper.StaticFieldGet(typeof(XamDockManager), "PaneLocationPropertyKey"),
                  ReflHelper.StaticMethodInvoke(dmbType, "FromValue", new object[] { paneLocation }));
                ReflHelper.MethodInvoke(dockManager, "AddLogicalChildInternal", new object[] { pane });
            }
            else
            {
                PaneToolWindow window = new CustomPaneToolWindow();

                window.SetValue((DependencyPropertyKey)ReflHelper.StaticFieldGet(typeof(XamDockManager), "DockManagerPropertyKey"), dockManager);
                window.SetValue((DependencyPropertyKey)ReflHelper.StaticFieldGet(typeof(XamDockManager), "PaneLocationPropertyKey"),
                  ReflHelper.StaticMethodInvoke(dmbType, "FromValue", new Type[] { typeof(PaneLocation) }, new object[] { paneLocation }));

                // AS 10/15/08 TFS8068
                // The floating windows are not part of the logical/visual tree of the 
                // dockmanager but they should reflect the same datacontext/right to left/etc.
                //
                window.SetBinding(FrameworkElement.DataContextProperty, Utilities.CreateBindingObject(FrameworkElement.DataContextProperty, BindingMode.OneWay, dockManager));
                window.SetBinding(FrameworkElement.FlowDirectionProperty, Utilities.CreateBindingObject(FrameworkElement.FlowDirectionProperty, BindingMode.OneWay, dockManager));

                // AS 1/24/11 NA 2011 Vol 1 - Min/Max/Taskbar
                window.SetBinding(Infragistics.Windows.Controls.ToolWindow.AllowMaximizeProperty, Utilities.CreateBindingObject(XamDockManager.AllowMaximizeFloatingWindowsProperty, BindingMode.OneWay, dockManager));
                window.SetBinding(Infragistics.Windows.Controls.ToolWindow.AllowMinimizeProperty, Utilities.CreateBindingObject(XamDockManager.AllowMinimizeFloatingWindowsProperty, BindingMode.OneWay, dockManager));
                window.SetBinding(Infragistics.Windows.Controls.ToolWindow.ShowInTaskbarProperty, Utilities.CreateBindingObject(XamDockManager.ShowFloatingWindowsInTaskbarProperty, BindingMode.OneWay, dockManager));
                window.SetBinding(Infragistics.Windows.Controls.ToolWindow.IsOwnedWindowProperty, Utilities.CreateBindingObject(XamDockManager.UseOwnedFloatingWindowsProperty, BindingMode.OneWay, dockManager));

                //skip this, we could handle the floating window caption in MSDesktop
                //paneToolWindow.SetBinding(PaneToolWindow.FloatingWindowCaptionSourceProperty, Utilities.CreateBindingObject(XamDockManager.FloatingWindowCaptionSourceProperty, BindingMode.OneWay, this));
	
                window.Content = pane;
                ObservableCollection<PaneToolWindow> toolWindows = ReflHelper.FieldGet(dockManager, "_toolWindows") as ObservableCollection<PaneToolWindow>;
                toolWindows.Add(window);

                // AS 4/30/08
                // Instead of raising the tool window event when the pane is first added, we're
                // going to manage it around when the dm is loaded/unloaded. This helps in 2 cases.
                // First, if the dm is on a page/tab and you select another page/tab, this 
                // will give the programmer a chance to unhook any events on the pane tool window.
                // Second, just being on the window and closing the window, the events will now
                // fire where as previously they would not have.
                //
                if ((bool)ReflHelper.PropertyGet(dockManager, "AllowToolWindowLoaded"))
                {
                    //paneToolWindow.IsWindowLoaded = true; is called in IG 10.3, but change to this.SetIsWindowLoaded(paneToolWindow, true); from IG 12
                    ReflHelper.MethodInvoke(dockManager, "SetIsWindowLoaded", new Type[]{typeof(PaneToolWindow), typeof(bool)}, new object[] { window, true });
                    //ReflHelper.PropertySet(window, "IsWindowLoaded", true);
                }
            }
        }
        #endregion
        #endregion

        private static XamDockManager manager;
        internal static XamDockManager Manager
        {
            get { return manager; }
        }

        public static event EventHandler<CancelPaneEventArgs> TearingOffPane;
        public static event EventHandler<PaneEventArgs> ToreOffPane;

        internal static void InvokeTearingOffPane(CancelPaneEventArgs arg_)
        {
            var copy = TearingOffPane;
            if (copy != null)
            {
                copy(arg_.Pane, arg_);
            }
        }

        internal static void InvokeToreOffPane(PaneEventArgs arg_)
        {
            var copy = ToreOffPane;
            if (copy != null)
            {
                copy(arg_.Pane, arg_);
            }
        }

        public static event EventHandler<CancelPaneEventArgs> DockingPane;
        public static event EventHandler<PaneEventArgs> DockedPane;

        internal static void InvokeDockingPane(CancelPaneEventArgs arg_)
        {
            var copy = DockingPane;
            if (copy != null)
            {
                copy(arg_.Pane, arg_);
            }
        }

        internal static void InvokeDockedPane(PaneEventArgs arg_)
        {
            var copy = DockedPane;
            if (copy != null)
            {
                copy(arg_.Pane, arg_);
            }
        }
    }


    public class PaneEventArgs : EventArgs
    {
        public PaneEventArgs(ContentPane pane_)
        {
            Pane = pane_;
        }

        public ContentPane Pane { get; private set; }
    }

    public class CancelPaneEventArgs : CancelEventArgs
    {
        public CancelPaneEventArgs(ContentPane pane_)
        {
            Pane = pane_;
        }

        public ContentPane Pane { get; private set; }
    }

}
