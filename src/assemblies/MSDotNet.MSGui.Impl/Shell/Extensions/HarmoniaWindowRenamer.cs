﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.DockManager; 
using MorganStanley.MSDotNet.MSGui.Impl.DockManager.Services;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions
{
    class HarmoniaWindowRenamer:RenamerBase
    {
        protected override void RenameTitle(object sender_, System.Windows.Input.ExecutedRoutedEventArgs e_)
        {
            ContentPane pane = null;
            FrameworkElement element = e_.OriginalSource as FrameworkElement;
            if (element != null)
            {
                var container = element.FindVisualParent<PaneContainer>();
                if (container != null)
                {
                    pane = container.ActivePane;
                }
            }
            if (pane == null)
            {
                pane = DockManager.Services.DockManager.ActivePane;
            } 
            if (pane == null) return;
            var arg = new RoutedEventArgs(Attached.TitleNeedChangingEvent);
            pane.RaiseEvent(arg);
            e_.Handled = arg.Handled;
        }

        protected override void ReleaseTitleChangeBinding(System.Windows.Input.KeyGesture gesture_)
        {

            foreach (var window in DockManager.Services.DockManager.GetAllFloatingWindows())
            {
                RemoveTitleChangeBinding(window, gesture_);
            }
            DockManager.Services.DockManager.FloatingWindowLoaded -= FloatingWindowLoaded;
        }

        protected override void SetupTitleChangeBinding(System.Windows.DependencyObject associator_)
        {
           if(Gesture == null) return;
             
            foreach (var window in DockManager.Services.DockManager.GetAllFloatingWindows())
            {
                AddTitleChangeBinding(window);
            }
            DockManager.Services.DockManager.FloatingWindowLoaded += FloatingWindowLoaded;  
        }

        private void FloatingWindowLoaded(object sender_, FloatingWindowEventArgs floatingWindowEventArgs_)
        {
            AddTitleChangeBinding(floatingWindowEventArgs_.Window); 
        }
    }
}
