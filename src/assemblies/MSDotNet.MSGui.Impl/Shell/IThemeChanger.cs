﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/IThemeChanger.cs#6 $
// $Change: 894688 $
// $DateTime: 2014/08/28 03:37:39 $
// $Author: caijin $

using System.Windows.Media;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Themes;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell
{
    internal interface IThemeChanger
    {
        void AddTheme(string name_, ImageSource icon_, Theme theme_);
        void SetTheme(string name_);
        void SetTheme(Theme theme_);
        void DisableThemeChanger();
    }
}
