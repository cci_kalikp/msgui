﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/IShell.cs#25 $
// $Change: 898451 $
// $DateTime: 2014/09/26 05:09:31 $
// $Author: caijin $

using System;
using System.Windows;
using System.Windows.Controls; 
using MorganStanley.MSDotNet.MSGui.Core.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions; 
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using System.Windows.Forms;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell
{
    internal interface IShell : IThemeChanger, IPersistable
    {
        IWindowManager WindowManager
        {
            get;
        }

        ITileItemManager TileItemManager
        {
            get;
        }

        Window MainWindow
        {
            get;
        }

        System.Windows.Controls.Primitives.StatusBar StatusBar
        {
            get;
        }

        UIElement TitleBar { get; }

        Func<bool> TryCloseConcordMainWindow
        {
            set;
        }

        void Show();

        void Close();

        void LoadProfile(string profile_, bool calledWhenLoading_=false);
        void SaveCurrentProfile();
        void DeleteCurrentProfile();
        void SaveProfileAs();

        void SetLayoutLoadStrategy(LayoutLoadStrategy layoutLoadStrategy_, LayoutLoadStrategyCallback layoutLoadStrategyCallback_);
        void SetLayoutLoadStrategy(LayoutLoadStrategy layoutLoadStrategy_);

        Func<bool> OnClosing
        {
            set;
        }

        Action OnClosed
        {
            set;
        }
        void Initialize();
        void PostChromeCreationInitialize();
        //void SetQuitDialogAutomaticBehaviour(int quitDialogSeconds, bool quitDialogSave);

        ItemsControl MainMenu { get; }


        Form ConcordMSNetLoopForm
        {
            get;
            set;
        } 

        bool DisableClose { get; set; }

    }
}
