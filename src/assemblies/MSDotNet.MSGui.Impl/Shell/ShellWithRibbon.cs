﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/ShellWithRibbon.cs#36 $
// $Change: 898451 $
// $DateTime: 2014/09/26 05:09:31 $
// $Author: caijin $

using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Xml.Linq; 
using Infragistics.Windows.Ribbon;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core; 
using MorganStanley.MSDotNet.MSGui.Core.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Themes;
using System.Windows.Forms;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell
{
  internal class ShellWithRibbon : IShell, IPersistable
  {
	private readonly ShellWindow m_shellWindow;
	private readonly Application.Application m_application;

	public ShellWithRibbon(IUnityContainer container_, InitialSettings initialSettings_)
	{      
	  m_application = container_.Resolve<IApplication>() as Application.Application;
	  if (m_application == null)
	  {
		throw new InvalidOperationException("Application must be IApplication");
	  }
	  m_shellWindow = new ShellWindow(container_, initialSettings_);
	  container_.RegisterInstance(WindowManager);

	  var persistenceService = container_.Resolve<IPersistenceProfileServiceProxy>();
	  persistenceService.Shell = this;
        if (ChromeManagerBase.MainWindowFocusGrabAtStartupSuppressed)
        {
            m_shellWindow.ShowActivated = false;
            if (m_shellWindow.FloatingOnlyDock != null)
            { 
                m_shellWindow.FloatingOnlyDock.ShowActivated = false;
            }
            EventHandler applicationLoaded = null;
            m_application.ApplicationLoaded += applicationLoaded = (sender_, args_) =>
                {
                    m_shellWindow.ShowActivated = true;
                    if (m_shellWindow.FloatingOnlyDock != null)
                    {
                        m_shellWindow.FloatingOnlyDock.ShowActivated = true;
                    }
                    m_application.ApplicationLoaded -= applicationLoaded;
                };
        }
	}

    

	public void AddTheme(string name_, ImageSource icon_, Theme theme_)
	{
	  m_shellWindow.AddTheme(name_, icon_, theme_);
	}

	public void SetTheme(string name_)
	{
	  m_shellWindow.SetTheme(name_);
	}
    public void SetTheme(Theme theme_)
    {
        m_shellWindow.SetTheme(theme_);
    }
	public void DisableThemeChanger()
	{
	  m_shellWindow.DisableThemeChanger();
	}

	public IWindowManager WindowManager
	{
	  get
	  {
		return m_shellWindow.WindowManager;
	  }
	}
    public ITileItemManager TileItemManager
    {
        get { return m_shellWindow.TileItemManager; }
    }
	public Window MainWindow
	{
	  get 
	  {
		return m_shellWindow.MainWindow;
	  }
	}

	public System.Windows.Controls.Primitives.StatusBar StatusBar
	{
	  get
	  {
		return m_shellWindow.StatusBar;
	  }
	}

      UIElement IShell.TitleBar
      {
          get { return m_shellWindow.TitleBar; }
      }

      public Func<bool> TryCloseConcordMainWindow
	{
	  set 
	  {
		m_shellWindow.TryCloseConcordMainWindow = value;
	  }
	}

	public void Show()
	{
        if (!ShellModeExtension.InvisibleMainControllerMode.HasFlag(ShellModeExtension.InvisibleMainControllerModeEnum.DontShowWindow))
        {
            m_shellWindow.Show();
        }

        // Show the floating window manager, if we have one
        if (m_shellWindow.FloatingOnlyDock != null)
        {
            m_shellWindow.FloatingOnlyDock.Show();
        }
	}

      public void Close()
      {
          ((IShell)m_shellWindow).Close();
      }

      public void LoadProfile(string profile_, bool calledWhenLoading_ = false)
	{
        m_shellWindow.LoadProfile(profile_, calledWhenLoading_);
	}

      public void SaveCurrentProfile()
      {
          m_shellWindow.SaveCurrentProfile();
      }

      public void DeleteCurrentProfile()
      {
          m_shellWindow.DeleteCurrentProfile();
      }

      public void SaveProfileAs()
      {
          m_shellWindow.SaveProfileAs();
      }

      public void SetLayoutLoadStrategy(LayoutLoadStrategy layoutLoadStrategy_, LayoutLoadStrategyCallback layoutLoadStrategyCallback_)
	{
	  m_shellWindow.SetLayoutLoadStrategy(layoutLoadStrategy_, layoutLoadStrategyCallback_);
	}

	public void SetLayoutLoadStrategy(LayoutLoadStrategy layoutLoadStrategy_)
	{
	  m_shellWindow.SetLayoutLoadStrategy(layoutLoadStrategy_);
	}

	public void Initialize()
	{
	  m_shellWindow.Initialize();
	}

      public void PostChromeCreationInitialize()
      {
          m_shellWindow.PostChromeCreationInitialize();
      }

      public Func<bool> OnClosing
	{
	  set 
	  {
		((IShell)m_shellWindow).OnClosing = value;
	  }
	}    

      public Action OnClosed
      {
          set
          {
              ((IShell)m_shellWindow).OnClosed = value;
          }
      }

	public XamRibbon Ribbon
	{
	  get
	  {
		return m_shellWindow.Ribbon;
	  }
	}

	  public ItemsControl QAT
	  {
		  get { return Ribbon.QuickAccessToolbar; }
	  }

	  public ItemsControl MainMenu
	{
	  get
	  {
		return m_shellWindow.MainMenu;
	  }
	}

	public void LoadState(XDocument state_)
	{
	  m_shellWindow.LoadState(state_);
	}

	public XDocument SaveState()
	{
	  return m_shellWindow.SaveState();
	}

	public string PersistorId
	{
	  get { return m_shellWindow.PersistorId; }
	}

	public ShellWindow ShellWindow
	{
	  get { return m_shellWindow; }
	}

	public Form ConcordMSNetLoopForm
	{
	  get
	  {
		return m_shellWindow.ConcordMSNetLoopForm;
	  }
	  set
	  {
		m_shellWindow.ConcordMSNetLoopForm = value;
	  }
	} 
      public bool DisableClose
      {
          get { return m_shellWindow.DisableClose; }
          set { m_shellWindow.DisableClose = value; }
      }
  }
}
