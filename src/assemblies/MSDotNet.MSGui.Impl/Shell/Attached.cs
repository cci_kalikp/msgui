﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Shell/Attached.cs#12 $
// $Change: 898066 $
// $DateTime: 2014/09/23 21:47:39 $
// $Author: caijin $

using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.View;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls
{
  public class Attached : UserControl
  {
    #region ViewContainer
    public static readonly DependencyProperty ViewContainerProperty =
      DependencyProperty.RegisterAttached(
        "ViewContainer",
        typeof(IViewContainer),
        typeof(Attached),
        new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsRender));

    public static void SetViewContainer(UIElement element_, IViewContainer value_)
    {
      element_.SetValue(ViewContainerProperty, value_);
    }

    public static IViewContainer GetViewContainer(UIElement element_)
    {
      if (element_ == null)
        return null;
      return (IViewContainer)element_.GetValue(ViewContainerProperty);
    }
    #endregion

    #region ViewMetadata
	internal static readonly DependencyProperty ViewContainerTypeProperty =
      DependencyProperty.RegisterAttached(
        "ViewContainerType",
        typeof(ViewContainerType),
        typeof(Attached),
        new FrameworkPropertyMetadata(ViewContainerType.WithCreator, FrameworkPropertyMetadataOptions.AffectsRender));

	internal static void SetViewContainerType(UIElement element_, ViewContainerType value_)
    {
		element_.SetValue(ViewContainerTypeProperty, value_);
    }

	internal static ViewContainerType GetViewContainerType(UIElement element_)
    {
		return (ViewContainerType)element_.GetValue(ViewContainerTypeProperty);
    }
    #endregion ViewMetadata

    #region IsTitleBeingChanged
    public static readonly DependencyProperty IsTitleBeingChangedProperty =
      DependencyProperty.RegisterAttached(
        "IsTitleBeingChanged",
        typeof(bool),
        typeof(Attached),
        new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.AffectsRender));
    public static void SetIsTitleBeingChanged(UIElement element_, bool value_)
    {
      element_.SetValue(IsTitleBeingChangedProperty, value_);
    }
    public static bool GetIsTitleBeingChanged(UIElement element_)
    {
      return (bool)element_.GetValue(IsTitleBeingChangedProperty);
    }
    #endregion


    public static readonly RoutedEvent TitleNeedChangingEvent = EventManager.RegisterRoutedEvent("TitleNeedChanging", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(Attached));
    public static void AddTitleNeedChangingHandler(DependencyObject d, RoutedEventHandler handler)
    {
      UIElement uie = d as UIElement;
      if (uie != null)
      {
        uie.AddHandler(TitleNeedChangingEvent, handler);
      }
    }
    public static void RemoveTitleNeedChangingHandler(DependencyObject d, RoutedEventHandler handler)
    {
        if (handler == null) return;
      UIElement uie = d as UIElement;
      if (uie != null)
      {
        uie.RemoveHandler(TitleNeedChangingEvent, handler);
      }
    }

    #region - RegisterCommandBindings Attached Property -
    public static DependencyProperty RegisterCommandBindingsProperty =
        DependencyProperty.RegisterAttached("RegisterCommandBindings", typeof(CommandBindingCollection), typeof(Attached),
        new PropertyMetadata(null, OnRegisterCommandBindingChanged));

    public static void SetRegisterCommandBindings(UIElement element, CommandBindingCollection value)
    {
      if (element != null)
        element.SetValue(RegisterCommandBindingsProperty, value);
    }
    public static CommandBindingCollection GetRegisterCommandBindings(UIElement element)
    {
      return (element != null ? (CommandBindingCollection)element.GetValue(RegisterCommandBindingsProperty) : null);
    }
    private static void OnRegisterCommandBindingChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
    {
      UIElement element = sender as UIElement;
      if (element == null) return;

      var bindings = (e.NewValue as CommandBindingCollection);
      if (bindings != null)
      {
        element.CommandBindings.Clear();
        element.CommandBindings.AddRange(bindings);
      }
    }
    #endregion

 
  }
}
