﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.Factories;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell
{
    interface IExternalShellProvider
    {
        IShell GetShellInstance(IUnityContainer container_, InitialSettings initialSettings_);

        ChromeManagerBase GetChromeManagerInstance(ChromeRegistry chromeRegistry_, IShell shell_,
                                                PersistenceProfileService persistenceProfileService_,
                                                PersistenceService persistenceService_, IUnityContainer container_,
                                                IWindowFactory windowFactory_);

        ICommonFactories GetFactoriesInstance(IChromeRegistry registry_);
    }
}
