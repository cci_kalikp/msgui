﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace MorganStanley.MSDotNet.MSGui.Impl.Shell
{
    internal class OverlayAdorner : Adorner
    {
        private readonly double m_titlebarHeight;

        private FrameworkElement m_child;
        private Brush m_brush;

        public OverlayAdorner(UIElement adornedElement_, double titlebarHeight_) : base(adornedElement_)
        {
            m_titlebarHeight = titlebarHeight_;
            IsHitTestVisible = false;
        }

        public Brush OverlayBrush
        {
            get
            {
                return m_brush;
            }
            set
            {
                m_brush = value;
            }
        } 

        protected override int VisualChildrenCount
        {
            get
            {
                return m_child == null ? 0 : 1;
            }
        }

        protected override Visual GetVisualChild(int index_)
        {
            if (index_ != 0) throw new ArgumentOutOfRangeException();
            return m_child;
        }

        public FrameworkElement Child
        {
            get { return m_child; }
            set
            {
                if (m_child != null)
                {
                    RemoveVisualChild(m_child);
                }
                m_child = value;
                if (m_child != null)
                {
                    AddVisualChild(m_child);
                }
            }
        }

        protected override Size MeasureOverride(Size constraint)
        {
            if (Child != null) Child.Measure(AdornedElement.DesiredSize);
            return AdornedElement.DesiredSize;
        }

        protected override Size ArrangeOverride(Size finalSize)
        {
            if (Child != null) Child.Arrange(new Rect(new Point(0, 0), AdornedElement.RenderSize));
            return AdornedElement.RenderSize;
        }

        protected override void OnRender(DrawingContext drawingContext_)
        {
            var elementSize = new Rect(AdornedElement.RenderSize);
            var rectangle = new Rect(0, m_titlebarHeight, elementSize.Width, elementSize.Height);
            drawingContext_.DrawRectangle(m_brush, null, rectangle);
            base.OnRender(drawingContext_);
        }
    }
}
