﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using System.Windows.Input;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.FusionMessagingImpl;

namespace MorganStanley.MSDotNet.MSGui.Impl.Interop
{
    class MouseEventMessageFilter:IMessageFilter 
    {

        private static readonly IMessageFilter MessageFilter = new MouseEventMessageFilter();
        public static void RegisterHost(WindowsFormsHost host_)
        { 
            host_.HandleUnloaded(e_=>UnHookMouseEventRedirect(host_));
            if (host_.IsLoaded)
            {
                HookMouseEventRedirect(host_);
            }
            else
            {
                RoutedEventHandler loaded = null;
                host_.Loaded += loaded = (sender_, args_) =>
                    {
                        HookMouseEventRedirect(host_);
                        host_.Loaded -= loaded;
                    };
            } 
        }


        private static void HookMouseEventRedirect(WindowsFormsHost host_)
        { 
            if (HostsToTrack.Contains(host_))
            {
               return;
            }
            HostsToTrack.Add(host_);
            if (HostsToTrack.Count == 1)
            {
                System.Windows.Forms.Application.AddMessageFilter(MessageFilter);
            }
        }
        private static void UnHookMouseEventRedirect(WindowsFormsHost host_)
        { 
            if (!HostsToTrack.Remove(host_))
            { 
                return;
            }
            if (HostsToTrack.Count == 0)
            {
                System.Windows.Forms.Application.RemoveMessageFilter(MessageFilter);
            }
        }

        
        internal static List<WindowsFormsHost> HostsToTrack = new List<WindowsFormsHost>(); 
        private static bool ShouldRedirect(WindowsFormsHost host_)
        {
            var control = host_.Child; 
            return control != null && !control.IsDisposed && control.IsHandleCreated && 
                control.Visible && !control.Focused;
        }
        

        bool IMessageFilter.PreFilterMessage(ref Message m_)
        {
            //handle mouse move
            if (m_.Msg == (int) FSNWin32.Msgs.WM_MOUSEMOVE)
            {
                var host = FindHostToHandle(m_);
                if (host == null) return false;

                var args = new System.Windows.Input.MouseEventArgs(InputManager.Current.PrimaryMouseDevice,
                                                                   Environment.TickCount);
                args.RoutedEvent = UIElement.MouseMoveEvent;
                host.RaiseEvent(args);
                return false;
            }

            //handle mouse down
            if (m_.Msg == (int) FSNWin32.Msgs.WM_LBUTTONDOWN || m_.Msg == (int) FSNWin32.Msgs.WM_MBUTTONDOWN ||
                m_.Msg == (int) FSNWin32.Msgs.WM_RBUTTONDOWN)
            {
                var host = FindHostToHandle(m_);
                if (host == null) return false;

                //wpf event 
                MouseButton mouseButton = MouseButton.Left;
                if (m_.Msg == (int) FSNWin32.Msgs.WM_RBUTTONDOWN)
                {
                    mouseButton = MouseButton.Right;
                }
                else if (m_.Msg == (int) FSNWin32.Msgs.WM_MBUTTONDOWN)
                {
                    mouseButton = MouseButton.Middle;
                }
                var args = new MouseButtonEventArgs(InputManager.Current.PrimaryMouseDevice, Environment.TickCount,
                                                    mouseButton);
                args.RoutedEvent = UIElement.MouseDownEvent;
                host.RaiseEvent(args);
                return false;
            }

            //handle mouse up
            if (m_.Msg == (int) FSNWin32.Msgs.WM_LBUTTONUP || m_.Msg == (int) FSNWin32.Msgs.WM_MBUTTONUP ||
                m_.Msg == (int) FSNWin32.Msgs.WM_RBUTTONUP)
            {
                var host = FindHostToHandle(m_);
                if (host == null) return false;

                //wpf event 
                MouseButton mouseButton = MouseButton.Left;
                if (m_.Msg == (int) FSNWin32.Msgs.WM_RBUTTONUP)
                {
                    mouseButton = MouseButton.Right;
                }
                else if (m_.Msg == (int) FSNWin32.Msgs.WM_MBUTTONUP)
                {
                    mouseButton = MouseButton.Middle;
                }
                var args = new MouseButtonEventArgs(InputManager.Current.PrimaryMouseDevice, Environment.TickCount,
                                                    mouseButton);
                args.RoutedEvent = UIElement.MouseUpEvent;
                host.RaiseEvent(args);
                return false;
            }
            return false;
        }

        private static WindowsFormsHost FindHostToHandle(Message message_)
        {
            Control c = Control.FromChildHandle(message_.HWnd);
            if (c == null) return null;
            var host = HostsToTrack.FirstOrDefault(h_ => IsParentOrSelf(c, h_.Child));
            if (host == null || !ShouldRedirect(host)) return null;
            return host;
        }
        private static bool IsParentOrSelf(Control child_, Control parent_)
        {
            while (child_ != null)
            {
                if (child_ == parent_) return true;
                child_ = child_.Parent;
            }
            return false;
        }
    }
}
