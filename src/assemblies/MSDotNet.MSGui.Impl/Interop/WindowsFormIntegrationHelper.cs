﻿using System;
using System.Windows;
using System.Windows.Forms.Integration;
using System.Windows.Media;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.Win32;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.MicrosoftShell.Standard;

namespace MorganStanley.MSDotNet.MSGui.Impl.Interop
{
    class WindowsFormIntegrationHelper
    {

        public static void ClipWindowsFormsHostIfNeeded(FrameworkElement element_, FrameworkElement parent_)
        {
            foreach (var host in element_.FindVisualChildrenIncludeParent<WindowsFormsHost>())
            {
                Point location = host.TranslatePoint(new Point(0, 0), parent_);
                Rect rectChild = new Rect(location.X, location.Y, host.ActualWidth, host.ActualHeight);
                Rect rectParent = new Rect(parent_.RenderSize);
                rectParent.Intersect(rectChild);
                // no clip
                if (CoreUtilities.AreClose(rectChild.Width, rectParent.Width) &&
                    CoreUtilities.AreClose(rectChild.Height, rectParent.Height))
                {
                    UnclipWindowsFormsHost(host);

                }
                else if (!rectParent.Size.IsEmpty) // the child is out of screen
                {
                    rectParent.Offset(-location.X, -location.Y);
                    ClipWindowsFormsHost(host, rectParent);
                }

                if (IsTransparent(host.Background) && host.Child.BackColor == System.Drawing.Color.Transparent)
                {
                    host.PropertyMap.Apply("Background");
                }
            }
        }

        public static void NotifyBackgroundChanged(DependencyObject element_)
        {
            foreach (var child in element_.FindVisualChildrenIncludeParent<WindowsFormsHost>())
            {
                if (IsTransparent(child.Background) && child.Child.BackColor == System.Drawing.Color.Transparent)
                {
                    child.PropertyMap.Apply("Background");
                }
            } 
        }
         
        public static bool IsTransparent(Brush b_)
        {
            return b_ == null || b_ == Brushes.Transparent ||
                (b_ is SolidColorBrush && ((SolidColorBrush)b_).Color.R == 0 &&
                ((SolidColorBrush)b_).Color.G == 0 && ((SolidColorBrush)b_).Color.B == 0);
        }

        public static void ClipWindowsFormsHost(WindowsFormsHost host_, Rect rect_)
        {
            if (host_.Handle == IntPtr.Zero) return;
            RECT rect = new RECT();
            if (!rect_.IsEmpty)
            {
                rect.Left = (int)rect_.X;
                rect.Top = (int)rect_.Y;
                rect.Right = (int)(rect_.X + rect_.Width);
                rect.Bottom = (int)(rect_.Y + rect_.Height); 
            }
            var hrgn = Shell.Controls.MicrosoftShell.Standard.NativeMethods.CreateRectRgnIndirect(rect);
            Shell.Controls.MicrosoftShell.Standard.NativeMethods.SetWindowRgn(host_.Handle, hrgn, true); 
        }

        public static void UnclipWindowsFormsHost(WindowsFormsHost host_)
        {
            ClipWindowsFormsHost(host_, new Rect(0,0, host_.ActualWidth, host_.ActualHeight));
        }

        public static void ClipWindowsFormsHostAll(WindowsFormsHost host_)
        {
            ClipWindowsFormsHost(host_, new Rect());
        }

        public static Point GetCurrentPosition(FrameworkElement relativeTo_)
        {
            Win32.POINT cursor;
            Win32.GetCursorPos(out cursor);
            return relativeTo_.PointFromScreen(new Point(cursor.X, cursor.Y)); 
        }
         
    }
}
