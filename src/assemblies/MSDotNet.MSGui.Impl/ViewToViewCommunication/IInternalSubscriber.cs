﻿namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication
{
    internal interface IInternalSubscriber<in T>
    {
        void Receive(T message);
    }
}