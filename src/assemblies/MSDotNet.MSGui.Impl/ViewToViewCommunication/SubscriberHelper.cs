﻿using System;
using System.Linq;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Core.SmartApi;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication.Prism;

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication
{
    internal static class SubscriberHelper
    {
        private static bool _initialized;
        private static readonly object SyncRoot = new object();
        public static ISubscriber<TMessage> CreateSubscriber<TMessage>(EventRouter eventRouter_, bool receiveLastSentMessage_)
        {
            lock (SyncRoot)
            {
                if (!_initialized)
                {
                    ExtensionsUtils.RequireAssembly(ExternalAssembly.ReactiveInterfaces, ExternalAssembly.ReactiveCore, ExternalAssembly.ReactiveLinq);
                    _initialized = true;
                }
            }

            if (BootstrapperBase.UsePrismAsInfrasturcture)
            {
                return new PrismSubscriber<TMessage>(eventRouter_, receiveLastSentMessage_);
            }
            else
            {
                return new Subscriber<TMessage>(eventRouter_, receiveLastSentMessage_);
            }
        }

        public static object GetSubscriber(IEventRegistrator eventRegistrator, Type messageType, IWindowViewContainer container, object handler)
        {
            var subscriber = typeof(IEventRegistrator).GetMethod("RegisterSubscriber",
                                                      new[] { typeof(IViewContainerBase) })
                                           .MakeGenericMethod(messageType)
                                           .Invoke(eventRegistrator, new object[] { container });

            var observable = subscriber.GetType().GetMethod("GetObservable").Invoke(subscriber, new object[] { });
            var subscribeMethod = typeof(ObservableExtensions).GetMethods()
                                                                     .First(m => m.Name.Equals("Subscribe") && m.GetParameters().Count() == 2);
            subscribeMethod = subscribeMethod.MakeGenericMethod(messageType);

            return subscribeMethod.Invoke(observable, new[] { observable, handler });
        }

        public static void Subscribe(object subscriber, Type messageType, Action<object> handler)
        {
            var subscribeMethod = subscriber.GetType().GetMethod("Subscribe").MakeGenericMethod(messageType);

            subscribeMethod.Invoke(subscriber, new object[] {handler});
        }
    }
}
