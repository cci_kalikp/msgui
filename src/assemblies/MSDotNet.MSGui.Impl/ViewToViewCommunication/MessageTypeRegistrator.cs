﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication
{
    internal sealed class MessageTypeRegistrator : IMessageTypeRegistrator
    {
        private readonly IDictionary<string, Type> _namedTypes = new Dictionary<string, Type>();
        private readonly HashSet<Type> _unNamedTypes = new HashSet<Type>();

        public void RegisterType<T>()
        {
            var meta =
                typeof (T).GetCustomAttributes(typeof (Core.Messaging.MessageAttribute), false).FirstOrDefault() as
                Core.Messaging.MessageAttribute;

            if (IsRegisterred(typeof (T)))
            {
                return;
            }


            if (meta != null)
            {
                if (_namedTypes.ContainsKey(meta.Id) && _namedTypes[meta.Id] != typeof (T))
                {
                    throw new ApplicationException("Register different message type with the same Id");
                }
                _namedTypes[meta.Id] = typeof (T);
            }
            else
            {
                _unNamedTypes.Add(typeof (T));
            }
        }

        public Type ResolveType(string id)
        {
            Type type;
            if (_namedTypes.TryGetValue(id, out type))
            {
                return type;
            }

            throw new ApplicationException("Type is not registered, id=" + id);
        }

        public bool IsRegisterred(Type msgType)
        {
            return _namedTypes.Values.Contains(msgType) || _unNamedTypes.Contains(msgType);
        }

        public string GetRegistartionId(Type msgType)
        {
            return (from entry in _namedTypes
                    where entry.Value == msgType
                    select entry.Key).FirstOrDefault();
        }
    }
}
