﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2011 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/ViewToViewCommunication/EventRegistrator.cs#25 $
// $Change: 897041 $
// $DateTime: 2014/09/16 05:44:27 $
// $Author: milosp $

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Threading;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication.Prism;
using MSDesktop.Isolation.Interfaces;


namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication
{
    public sealed class EventRegistrator : IEventRegistrator
    {
        private readonly IDictionary<Type, Dictionary<string, object>> _registeredSubscribers =
            new Dictionary<Type, Dictionary<string, object>>();

        private readonly IDictionary<Type, Dictionary<string, object>> _registeredPublishers =
            new Dictionary<Type, Dictionary<string, object>>();

        private readonly IList<Tuple<Type, Type, string, string>> _missingConnections =
            new List<Tuple<Type, Type, string, string>>();


        private readonly EventRouter _eventRouter;
        private readonly IMSDesktopEventAggregator _eventAggregator;
        private readonly IMessageTypeRegistrator _typeRegistrator;

        public EventRegistrator(IAdapterService adapterService, [Dependency("InnerMSDesktopEventAggregator")] IMSDesktopEventAggregator eventAggregator,
                                IMessageTypeRegistrator typeRegistrator)
            : this(adapterService, eventAggregator, typeRegistrator, Dispatcher.CurrentDispatcher)
        {
        }

        internal EventRegistrator(IAdapterService adapterService, [Dependency("InnerMSDesktopEventAggregator")] IMSDesktopEventAggregator eventAggregator,
                                  IMessageTypeRegistrator typeRegistrator, Dispatcher dispatcher)
        {
            _eventRouter = new EventRouter(adapterService as ViewMessageAdapterService, dispatcher);
            _eventAggregator = eventAggregator;
            _typeRegistrator = typeRegistrator;
        }


        public IAdapterService AdapterService
        {
            get { return _eventRouter.ViewMessageAdapterService; }
            internal set { _eventRouter.ViewMessageAdapterService = (ViewMessageAdapterService)value; }
        }

        #region  ID based functions

        internal ISubscriber<TMessage> RegisterSubscriber<TMessage>(string viewContainer)
        {
            return RegisterSubscriber(viewContainer, false, b => SubscriberHelper.CreateSubscriber<TMessage>(_eventRouter, b));
              
        }

        private ISubscriber<TMessage> RegisterSubscriber<TMessage>(
            string viewContainer, 
            bool receiveLastSentMessage, 
            Func<bool, ISubscriber<TMessage>> subscriberCreator)
        {
            _typeRegistrator.RegisterType<TMessage>();

            Dictionary<string, object> viewSubscriptions;
            if (!_registeredSubscribers.TryGetValue(typeof (TMessage), out viewSubscriptions))
            {
                viewSubscriptions = new Dictionary<string, object>();
                _registeredSubscribers.Add(typeof (TMessage), viewSubscriptions);
            }

            object subscriber;
            if (viewSubscriptions.TryGetValue(viewContainer, out subscriber))
            {
                var disposableSubscriber = subscriber as IDisposable;
                if (disposableSubscriber!=null)
                {
                    var args = new RemovingExistingComponentEventArgs {ViewContainerId = viewContainer};
                    OnRemovingExistingComponent(args);
                    if (!args.Cancel)
                    {
                        disposableSubscriber.Dispose();
                    }
                    else
                    {
                        return (ISubscriber<TMessage>) subscriber;
                    }
                }
            }

            var newSubscriber = subscriberCreator(receiveLastSentMessage);
            viewSubscriptions[viewContainer] = newSubscriber;

            if (_eventAggregator != null)
            {
                newSubscriber.GetObservable()
                             .Subscribe(msg =>
                                 {
                                     var eventToPublish =
                                         _eventAggregator.GetViewToViewEvent<TMessage>();

                                     eventToPublish
                                         .PublishToExternal(new ViewToViewAggregateMessage<TMessage>
                                             {
                                                 SentByRegistrator = true,
                                                 ViewContainerId = viewContainer,
                                                 Message = msg
                                             });
                                 });
            }
            ReconnectMissingConnections();
            return newSubscriber;
        }

        private IPublisher<TMessage> RegisterPublisher<TMessage>(string viewContainer,
                                                                 Func<IPublisher<TMessage>> publisherCreator)
        {
            _typeRegistrator.RegisterType<TMessage>();

            Dictionary<string, object> viewPublishers;
            if (!_registeredPublishers.TryGetValue(typeof (TMessage), out viewPublishers))
            {
                viewPublishers = new Dictionary<string, object>();
                _registeredPublishers.Add(typeof (TMessage), viewPublishers);
            }

            object publisherObject;
            if (viewPublishers.TryGetValue(viewContainer, out publisherObject))
            {
                var disposablePublisher = publisherObject as IDisposable;
                if (disposablePublisher != null)
                {
                    var args = new RemovingExistingComponentEventArgs {ViewContainerId = viewContainer};
                    OnRemovingExistingComponent(args);
                    if (!args.Cancel)
                    {
                        disposablePublisher.Dispose();
                    }
                    else
                    {
                        return (IPublisher<TMessage>)publisherObject;
                    }
                }
            }

            var publisher = publisherCreator();
            viewPublishers[viewContainer] = publisher;
            if (_eventAggregator != null)
            {
                _eventAggregator.GetViewToViewEvent<TMessage>().InternalSubscribe(msg =>
                    {
                        if (!msg.SentByRegistrator && msg.ViewContainerId == viewContainer)
                        {
                            publisher.Publish(msg.Message);
                        }
                    }, MSDesktopThreadOption.BackgroundThread, true);
            }

            ReconnectMissingConnections();
            return publisher;
        }

        internal IPublisher<TMessage> RegisterPublisher<TMessage>(string viewContainer)
        {
            return RegisterPublisher(viewContainer, () => new Publisher<TMessage>(_eventRouter));
        }

        internal IEnumerable<Type> GetPublishedTypes(String viewContainer)
        {
            return from x in _registeredPublishers
                   where x.Value.ContainsKey(viewContainer)
                   select x.Key;
        }

        internal IEnumerable<Type> GetSubscribedTypes(String viewContainer)
        {
            return from x in _registeredSubscribers
                   where x.Value.ContainsKey(viewContainer)
                   select x.Key;
        }


        private void ReconnectMissingConnections()
        {
            foreach (Tuple<Type, Type, string, string> missingConnection in _missingConnections.ToArray())
            {
                try
                {
                    var methodInfo = typeof (EventRegistrator).GetMethod("Connect", new Type[] {typeof (string), typeof (string)});
                    var method = methodInfo.MakeGenericMethod(new [] { missingConnection.Item1, missingConnection.Item2 });
                    method.Invoke(this, new object[] { missingConnection.Item3, missingConnection.Item4 });

                }
                catch (Exception)
                {
                    continue;
                }
                _missingConnections.Remove(missingConnection);
            }
        }

        public void Connect<TMessage1, TMessage2>(string publishContainer, string subscribeContainer)
        {
            var publisher = GetRegisteredPublisher<TMessage1>(publishContainer);

            var tuple = new Tuple<Type, Type, string, string>(
                typeof (TMessage1),
                typeof (TMessage2),
                publishContainer,
                subscribeContainer);

            if (publisher == null)
            {
                if (!_missingConnections.Contains(tuple))
                {
                    _missingConnections.Add(tuple);
                }

                throw new ApplicationException(
                    String.Format("The view container {0} is not registered to publish type {1}", publishContainer,
                                  typeof (TMessage1).FullName));
            }

            var subscriber = GetRegisteredSubscriber<TMessage2>(subscribeContainer);
            if (subscriber == null)
            {
                if (!_missingConnections.Contains(tuple))
                {
                    _missingConnections.Add(tuple);
                }

                throw new ApplicationException(
                    String.Format("The view container {0} is not registered to subscribe type {1}", subscribeContainer,
                                  typeof (TMessage2).FullName));
            }

            _eventRouter.Connect(publisher, subscriber);
        }

        public void Disconnect<TMessage1, TMessage2>(string publishViewId, string subscribeViewId)
        {
            var publisher = GetRegisteredPublisher<TMessage1>(publishViewId);
            var subscriber = GetRegisteredSubscriber<TMessage2>(subscribeViewId);
            _eventRouter.Disconnect(publisher, subscriber);
        }

        #endregion


        internal IEnumerable<object> GetAllRegisterredPublishers(IViewContainerBase view)
        {
            return from pair in _registeredPublishers
                   from pub in pair.Value
                   where pub.Key == view.GetId()
                   select pub.Value;
        }

        internal IEnumerable<object> GetAllRegisterredSubscribers(IViewContainerBase view)
        {
            return from pair in _registeredSubscribers
                   from sub in pair.Value
                   where sub.Key == view.GetId()
                   select sub.Value;
        }



        #region Implementation of IEventRegistrator

        public bool UnregisterSubscriber<TMessage>(ISubscriber<TMessage> subscriber)
        {
            Dictionary<string, object> viewSubscriptions;
            if (_registeredSubscribers.TryGetValue(typeof (TMessage), out viewSubscriptions)
                && viewSubscriptions.ContainsValue(subscriber) &&
                viewSubscriptions.Remove(viewSubscriptions.First(el => el.Value == subscriber).Key))
            {
                var disposableSubscriber = subscriber as IDisposable;
                if (disposableSubscriber != null)
                {
                    disposableSubscriber.Dispose();
                }
                return true;
            }
            return false;

        }

        public bool UnregisterSubscriber<TMessage>(IViewContainerBase viewContainer)
        {
            var view = viewContainer as ViewContainerBase;
            if (view != null)
                view.ReleaseResources -= UnregisterSubscriberHandler<TMessage>;

            var subscriber = GetRegisteredSubscriber<TMessage>(viewContainer);
            return UnregisterSubscriber(subscriber);
        }


        public bool UnregisterPublisher<TMessage>(IPublisher<TMessage> publisher)
        {
            Dictionary<string, object> viewPublishers;
            if (_registeredPublishers.TryGetValue(typeof (TMessage), out viewPublishers)
                && viewPublishers.ContainsValue(publisher)
                && viewPublishers.Remove(viewPublishers.First(elem_ => elem_.Value == publisher).Key))
            {
                var disposablePublisher = publisher as IDisposable;
                if (disposablePublisher != null)
                {
                    disposablePublisher.Dispose();
                }

                return true;
            }
            return false;
        }

        public bool UnregisterPublisher<TMessage>(IViewContainerBase viewContainer)
        {
            var view = viewContainer as ViewContainerBase;
            if (view != null)
                view.ReleaseResources -= UnregisterPublisherHandler<TMessage>;

            var publisher = GetRegisteredPublisher<TMessage>(viewContainer);
            return UnregisterPublisher(publisher);
        }


        public IEnumerable<string> GetCompatiblePublishers<TMessage>()
        {
            Dictionary<String, object> pubs;
            return AdapterService != null
                       ? from publishers in _registeredPublishers
                         where AdapterService.IsAdaptationPossible(publishers.Key, typeof (TMessage))
                         from publisher in publishers.Value
                         select publisher.Key
                       : _registeredPublishers.TryGetValue(typeof (TMessage), out pubs)
                             ? pubs.Keys
                             : Enumerable.Empty<String>();
        }

        public IEnumerable<string> GetCompatibleSubscribers<TMessage>()
        {
            Dictionary<String, object> subs;

            return AdapterService != null
                       ? from subscribers in _registeredSubscribers
                         where AdapterService.IsAdaptationPossible(typeof (TMessage), subscribers.Key)
                         from subscriber in subscribers.Value
                         select subscriber.Key
                       : _registeredSubscribers.TryGetValue(typeof (TMessage), out subs)
                             ? subs.Keys
                             : Enumerable.Empty<String>();
        }

        public ICollection<Type> GetSubscribedTypes()
        {
            return _registeredSubscribers.Keys;
        }

        public ICollection<Type> GetPublishedTypes()
        {
            return _registeredPublishers.Keys;
        }

        public IPublisher<TMessage> GetRegisteredPublisher<TMessage>(string viewContainer)
        {
            Dictionary<string, object> publishers;
            object publisher;
            return _registeredPublishers.TryGetValue(typeof (TMessage), out publishers)
                   && publishers.TryGetValue(viewContainer, out publisher)
                   && publisher is IPublisher<TMessage>
                       ? (IPublisher<TMessage>) publisher
                       : null;
        }

        public ISubscriber<TMessage> GetRegisteredSubscriber<TMessage>(string viewContainer)
        {
            Dictionary<string, object> subscribers;
            object subscriber;
            return _registeredSubscribers.TryGetValue(typeof (TMessage), out subscribers)
                   && subscribers.TryGetValue(viewContainer, out subscriber)
                   && subscriber is ISubscriber<TMessage>
                       ? (ISubscriber<TMessage>) subscriber
                       : null;
        }

        public IPublisher<TMessage> RegisterMarshalingPublisher<TMessage>(IViewContainerBase viewContainer, object addinHost)
        {
            return RegisterPublisher(viewContainer, () => new MarshalingPublisher<TMessage>(_eventRouter, (IXProcAddInHost)addinHost));
        }

        #region  newly created for v2v visualization


        private String GetPublishOwner(Type type, object obj)
        {
            if (!_registeredPublishers.ContainsKey(type))
                return null;

            var col = _registeredPublishers[type];
            var names = from n in col
                        where n.Value.Equals(obj)
                        select n.Key;

            return names.FirstOrDefault();
        }

        private String GetSubscriberOwner(Type type, object subsriber)
        {
            if (!_registeredSubscribers.ContainsKey(type))
                return null;

            var col = _registeredSubscribers[type];
            var names = from n in col
                        where n.Value.Equals(subsriber)
                        select n.Key;

            return names.FirstOrDefault();
        }

        internal Dictionary<KeyValuePair<Type, String>, HashSet<KeyValuePair<Type, String>>> GetConnections()
        {

            var result = new Dictionary<KeyValuePair<Type, string>, HashSet<KeyValuePair<Type, string>>>();
            foreach (var pair in _eventRouter.GetConnections())
            {
                var source = pair.Key;
                var pub = GetPublishOwner(source.Key, source.Value);
                if (string.IsNullOrEmpty(pub))
                    continue;

                var subs = new HashSet<KeyValuePair<Type, string>>();
                foreach (var v in pair.Value)
                {

                    String sub = this.GetSubscriberOwner(v.Key, v.Value);
                    if (string.IsNullOrEmpty(sub))
                        continue;
                    subs.Add(new KeyValuePair<Type, string>(v.Key, sub));
                }

                result[new KeyValuePair<Type, String>(source.Key, pub)] = subs;

            }

            return result;


            /**

        Dictionary<KeyValuePair<Type, String>, HashSet<KeyValuePair<Type, String>>> res = new Dictionary<KeyValuePair<Type, string>, HashSet<KeyValuePair<Type, string>>>();
        foreach (var con in m_connections)
        {
            var source = con.Key;
            HashSet<KeyValuePair<Type, String>> targetSet = new HashSet<KeyValuePair<Type, string>>();
            foreach (var target in con.Value)
            {
                targetSet.Add(target);
            }

            res[source] = targetSet;

        }
        return res;
        */
        }



        #endregion

        #endregion


        #region  Restrict to IViewContainerBase

        public IPublisher<TMessage> RegisterPublisher<TMessage>(IViewContainerBase viewContainer)
        {
            return RegisterPublisher(viewContainer, () => new Publisher<TMessage>(_eventRouter));
        }

        private IPublisher<TMessage> RegisterPublisher<TMessage>(IViewContainerBase viewContainer,
                                                                 Func<IPublisher<TMessage>> publisherCreator)
        {
            var id = viewContainer.GetId();

            var publisher = RegisterPublisher(id, publisherCreator);
            var vc = viewContainer as ViewContainerBase;

            Debug.Assert(vc != null);

            var eventArgs = new InstallingReleaseResourcesHandlerEventArgs {ViewContainer = viewContainer};
            InvokeInstallingReleaseResourcesHandler(eventArgs);
            if (eventArgs.Cancel == false)
                vc.ReleaseResources += UnregisterPublisherHandler<TMessage>;

            return publisher;
        }

        internal event EventHandler<InstallingReleaseResourcesHandlerEventArgs> InstallingReleaseResourcesHandler;

        internal void InvokeInstallingReleaseResourcesHandler(InstallingReleaseResourcesHandlerEventArgs e)
        {
            var handler = InstallingReleaseResourcesHandler;
            if (handler != null) handler(this, e);
        }

        internal event EventHandler<UnregisteringComponentEventArgs> UnregisteringComponent;

        private void OnUnregisteringComponent(UnregisteringComponentEventArgs e_)
        {
            var handler = UnregisteringComponent;
            if (handler != null) handler(this, e_);
        }

        private void UnregisterPublisherHandler<TMessage>(object sender, EventArgs args)
        {
            var viewContainer = sender as ViewContainerBase;
            if (viewContainer == null)
                return;
            var e = new UnregisteringComponentEventArgs {ViewContainer = viewContainer};
            OnUnregisteringComponent(e);
            if (e.Cancel) return;
            UnregisterPublisher<TMessage>(viewContainer);
        }

        internal event EventHandler<RemovingExistingComponentEventArgs> RemovingExistingComponent;

        private void OnRemovingExistingComponent(RemovingExistingComponentEventArgs e_)
        {
            var handler = RemovingExistingComponent;
            if (handler != null) handler(this, e_);
        }

        private void UnregisterSubscriberHandler<TMessage>(object sender, EventArgs args)
        {
            var viewContainer = sender as ViewContainerBase;
            if (viewContainer != null)
            {
                var e = new UnregisteringComponentEventArgs { ViewContainer = viewContainer };
                OnUnregisteringComponent(e);
                if (e.Cancel) return;
                UnregisterSubscriber<TMessage>(viewContainer);
            }
        }

        public bool UnregisterPublisher<TMessage>(string id)
        {
            var publisher = GetRegisteredPublisher<TMessage>(id);
            if (publisher == null)
                return false;
            return UnregisterPublisher(publisher);
        }

        public ISubscriber<TMessage> RegisterMarshalingSubscriber<TMessage>(IViewContainerBase viewContainer, object addinHost)
        {
            return RegisterSubscriberWithReleaseHandler(
                viewContainer, 
                false,
                b => new MarshalingSubscriber<TMessage>(_eventRouter, b, (IXProcAddInHost)addinHost));
        }

        public ISubscriber<TMessage> RegisterSubscriber<TMessage>(IViewContainerBase viewContainer)
        {
            return RegisterSubscriber<TMessage>(viewContainer, false);
        }

        public ISubscriber<TMessage> RegisterSubscriber<TMessage>(IViewContainerBase viewContainer,
                                                                  bool receiveLastMessage)
        {
            if (BootstrapperBase.UsePrismAsInfrasturcture)
            {
                return RegisterSubscriberWithReleaseHandler(viewContainer, false, b => new PrismSubscriber<TMessage>(_eventRouter, b));
            }
            else
            {
                return RegisterSubscriberWithReleaseHandler(viewContainer, false, b => new Subscriber<TMessage>(_eventRouter, b));
            }
            
        }

        private ISubscriber<TMessage> RegisterSubscriberWithReleaseHandler<TMessage>(IViewContainerBase viewContainer,
                                                                           bool reciveLastMessage, Func<bool, ISubscriber<TMessage>> subscriberCreator)
        {
            var id = viewContainer.GetId();

            var subscriber = RegisterSubscriber(id, reciveLastMessage, subscriberCreator);
            var vc = viewContainer as ViewContainerBase;
            Debug.Assert(vc != null);

            var eventArgs = new InstallingReleaseResourcesHandlerEventArgs { ViewContainer = viewContainer };
            InvokeInstallingReleaseResourcesHandler(eventArgs);
            if (eventArgs.Cancel == false)
                vc.ReleaseResources += UnregisterSubscriberHandler<TMessage>;

            return subscriber;
        }


        public bool UnregisterSubscriber<TMessage>(string id)
        {
            var subscriber = GetRegisteredSubscriber<TMessage>(id);
            if (subscriber == null)
                return false;
            return UnregisterSubscriber(subscriber);
        }


        public IPublisher<TMessage> GetRegisteredPublisher<TMessage>(IViewContainerBase viewContainer)
        {
            var id = viewContainer.GetId();

            return GetRegisteredPublisher<TMessage>(id);

        }

        public ISubscriber<TMessage> GetRegisteredSubscriber<TMessage>(IViewContainerBase viewContainer)
        {
            var id = viewContainer.GetId();

            return GetRegisteredSubscriber<TMessage>(id);
        }

        public void Connect<TMessage1, TMessage2>(IViewContainerBase publishContainer,
                                                  IViewContainerBase subscribeContainer)
        {
            Guard.ArgumentNotNull(publishContainer, "publishContainer");
            Guard.ArgumentNotNull(subscribeContainer, "publishContainer");


            var pubId = publishContainer.GetId();
            var subId = subscribeContainer.GetId();
            this.Connect<TMessage1, TMessage2>(pubId, subId);


        }

        public void Disconnect<TMessage1, TMessage2>(IViewContainerBase publishContainer,
                                                     IViewContainerBase subscribeContainer)
        {
            Guard.ArgumentNotNull(publishContainer, "publishContainer");
            Guard.ArgumentNotNull(subscribeContainer, "publishContainer");
            var pubId = publishContainer.GetId();
            var subId = subscribeContainer.GetId();

            this.Disconnect<TMessage1, TMessage2>(pubId, subId);
        }


        public IEnumerable<Type> GetPublishedTypes(IViewContainerBase viewContainer)
        {

            var id = viewContainer.GetId();

            return from x in _registeredPublishers
                   where x.Value.ContainsKey(id)
                   select x.Key;
        }

        public IEnumerable<Type> GetSubscribedTypes(IViewContainerBase viewContainer)
        {
            var id = viewContainer.GetId();
            return from x in _registeredSubscribers
                   where x.Value.ContainsKey(id)
                   select x.Key;
        }

        #endregion
    }

    internal class RemovingExistingComponentEventArgs : EventArgs
    {
        public string ViewContainerId { get; set; }
        public bool Cancel { get; set; }
    }

    internal class UnregisteringComponentEventArgs : EventArgs
    {
        public bool Cancel { get; set; }
        public IViewContainerBase ViewContainer { get; set; }
    }

    internal class InstallingReleaseResourcesHandlerEventArgs : EventArgs
    {
        public bool Cancel { get; set; }
        public IViewContainerBase ViewContainer { get; set; }
    }
}
