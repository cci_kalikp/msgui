﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Core.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging; 

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication
{
    /// <summary>
    /// The persistence by messaget type id attribute is not implemented in this class
    /// </summary>

    
    internal class ExternalConnectionManager: IConnectionState, IPersistable, IDisposable
    {
        private readonly InterProcessMessenger _ipcMessenger;



        private readonly HashSet<Tuple<string, string, Type, string, string, Type>> _publishs = new HashSet<Tuple<string, string, Type, string, string, Type>>();
        private readonly HashSet<Tuple<string, string, Type, string, string, Type>> _subscribes = new HashSet<Tuple<string, string, Type, string, string, Type>>();

        private readonly HashSet<Tuple<ViewMetaInfo, Type, ViewMetaInfo, Type>> _activePublishs = new HashSet<Tuple<ViewMetaInfo, Type, ViewMetaInfo, Type>>();
        private readonly HashSet<Tuple<ViewMetaInfo, Type, ViewMetaInfo, Type>> _activeSubscribes = new HashSet<Tuple<ViewMetaInfo, Type, ViewMetaInfo, Type>>();

        private readonly ExternalViewsProxy _externalProxy;
        private readonly ExternalViewManger _externalViewManger;

        private readonly object _connectionLock = new object();

        internal ExternalConnectionManager(InterProcessMessenger ipcMessenger)
        {
            _ipcMessenger = ipcMessenger;
            _externalProxy = _ipcMessenger.ExternalViewsProxy;
            _ipcMessenger.ExternalViewRegisterred += OnExternalViewRegisterred;
            _ipcMessenger.ExternalViewUnregisterred += OnExternalViewUnregisterred;
            _ipcMessenger.ExternalApplicationClosed += OnExternalApplicationRemoved;

            _ipcMessenger.ExternalViewConnected += OnExternalViewConnected;
            _ipcMessenger.ExternalViewDisconnected += OnExternalViewDisconnected;
        }

        
        private void OnExternalViewConnected(object sender, InterProcessMessenger.ViewConnectionEventArgs args)
        {
            if ((this as IConnectionState).IsConnected(args.LocalView, args.ExternalView))
                return;
            if (args.ExternalAsPublisher)
            {
               
                    _activeSubscribes.Add(new Tuple<ViewMetaInfo, Type, ViewMetaInfo, Type>(args.LocalView, args.LocalType,
                                                                                      args.ExternalView,
                                                                                      args.ExternalType));
            }
            else
            {

                _activePublishs.Add(new Tuple<ViewMetaInfo, Type, ViewMetaInfo, Type>(args.LocalView, args.LocalType,
                                                                                         args.ExternalView,
                                                                                         args.ExternalType));
                
            }
            
            
        }

        private void OnExternalViewDisconnected(object sender, InterProcessMessenger.ViewConnectionEventArgs args)
        {
            if (args.ExternalAsPublisher)
            {
                _activeSubscribes.RemoveWhere(
                    (tp) => tp.Item1.LocalId == args.LocalView.LocalId && tp.Item3.FullId == args.ExternalView.FullId);
            }
            else
            {
                _activePublishs.RemoveWhere(
                    (tp) => tp.Item1.LocalId == args.LocalView.LocalId && tp.Item3.FullId == args.ExternalView.FullId);

            }
        }

        void IDisposable.Dispose()
        {
            _ipcMessenger.ExternalViewRegisterred -= OnExternalViewRegisterred;
            _ipcMessenger.ExternalViewUnregisterred -= OnExternalViewUnregisterred;
            _ipcMessenger.ExternalApplicationClosed -= OnExternalApplicationRemoved;
        }
        

        

        private void Active(ViewMetaInfo externalView, Type externalType, bool externalAsPublisher)
        {
            if (externalAsPublisher)
            {
                var subConn = from sub in _subscribes
                              where sub.Item4 == externalView.ApplicationName && sub.Item5 == externalView.LocalId
                              select sub;

                foreach (var sub in subConn)
                {
                    var local = new ViewMetaInfo(_ipcMessenger.Application, sub.Item2, null);
                    var s = new Tuple<ViewMetaInfo, Type, ViewMetaInfo, Type>(local, sub.Item3, externalView, sub.Item6);
                    _activeSubscribes.Add(s);
                }
            }
            else
            {
                var conn = from pub in _publishs
                           where
                               pub.Item4 == externalView.ApplicationName && pub.Item5 == externalView.LocalId && pub.Item6 == externalType
                           select pub;

                foreach (var pub in conn)
                {
                    var local = new ViewMetaInfo(_ipcMessenger.Application, pub.Item2, null);

                    var ap = new Tuple<ViewMetaInfo, Type, ViewMetaInfo, Type>(local, pub.Item3, externalView, pub.Item6);
                    _activePublishs.Add(ap);
                }
            }
           
        }


        private void DeactiveAll(ApplicationInfo application)
        {
            _activeSubscribes.RemoveWhere(
                (sub) => sub.Item3.Host == application.Host && sub.Item3.ApplicationName == application.Name && sub.Item3.Instance.ToString() == application.Instance);

            _activePublishs.RemoveWhere((pub) => pub.Item3.Host == application.Host && pub.Item3.ApplicationName == application.Name && pub.Item3.Instance.ToString() == application.Instance);
        }

        private void Deactive(ViewMetaInfo externalView, Type externalType, bool externalAsPublisher)
        {
            if (externalAsPublisher)
            {
                    _activeSubscribes.RemoveWhere(s => s.Item3.FullId == externalView.FullId && s.Item4 == externalType);
                
            }
            else
            {  
                    _activePublishs.RemoveWhere(s => s.Item3.FullId == externalView.FullId && s.Item4 == externalType);
            }

        }


        private void ApplyDisconnections()
        {
            var conn = from c in _ipcMessenger.GetConnections()
                       where _ipcMessenger.GetLocalId(c.Key.Value) != null
                       from sub in c.Value
                       where _externalProxy.IsActiveExternalView(sub.Value)
                       select new Tuple<string, Type, string, Type>(c.Key.Value, c.Key.Key, sub.Value, sub.Key);

            var methodInfo = _ipcMessenger.GetType().GetMethod("DisconnectFromExternalSubscriber", new Type[] { typeof(string), typeof(string) });

            var pubs = from ap in _activePublishs
                       select new Tuple<string, Type, string, Type>(ap.Item1.LocalId, ap.Item2, ap.Item3.FullId, ap.Item4);

            foreach (var dis in conn.ToList().Except(pubs))
            {
                var method = methodInfo.MakeGenericMethod(new Type[] {dis.Item2, dis.Item4});
                method.Invoke(_ipcMessenger, new object[] {dis.Item1, dis.Item3});
            }


            conn = from c in _ipcMessenger.GetConnections()
                       where _externalProxy.IsActiveExternalView(c.Key.Value)
                       from sub in c.Value
                       where _ipcMessenger.GetLocalId(sub.Value) != null
                       select new Tuple<string, Type, string, Type>(sub.Value, sub.Key, c.Key.Value, c.Key.Key);

            var subs = from s in _activeSubscribes
                       select new Tuple<string, Type, string, Type>(s.Item1.LocalId, s.Item2, s.Item3.FullId, s.Item4);

            methodInfo = _ipcMessenger.GetType().GetMethod("DisconnectFromExternalPublisher", new Type[] { typeof(string), typeof(string) });
            foreach (var dis in conn.ToList().Except(subs))
            {
                var method = methodInfo.MakeGenericMethod(new Type[] { dis.Item2, dis.Item4 });
                method.Invoke(_ipcMessenger, new object[] { dis.Item1, dis.Item3 });
            }
        }


        private void ApplyConnections()
        {
            var conn = from c in _ipcMessenger.GetConnections()
                       where _ipcMessenger.GetLocalId(c.Key.Value) != null
                       from sub in c.Value
                       where _externalProxy.IsActiveExternalView(sub.Value)
                       select new Tuple<string, Type, string, Type>(c.Key.Value, c.Key.Key, sub.Value, sub.Key);

            var publish = from pub in _activePublishs
                          select new Tuple<string, Type, string, Type>(pub.Item1.LocalId, pub.Item2, pub.Item3.FullId, pub.Item4);

            var methodInfo = _ipcMessenger.GetType().GetMethod("ConnectToExternalSubscriber");
            foreach (var sub in publish.ToList().Except(conn.ToList()))
            {
                if(!_ipcMessenger.GetPublishedTypes(sub.Item1).Contains(sub.Item2))
                    continue;

                var method = methodInfo.MakeGenericMethod(new Type[] { sub.Item2, sub.Item4 });
                method.Invoke(_ipcMessenger, new string[] { sub.Item1, sub.Item3 });
            }


            conn = from c in _ipcMessenger.GetConnections()
                   where _externalProxy.IsActiveExternalView(c.Key.Value)
                   from sub in c.Value
                   where _ipcMessenger.GetLocalId(sub.Value) != null
                   select new Tuple<string, Type, string, Type>(sub.Value, sub.Key, c.Key.Value, c.Key.Key);

            var subs = from s in _activeSubscribes
                       select new Tuple<string, Type, string, Type>(s.Item1.LocalId, s.Item2, s.Item3.FullId, s.Item4);

            methodInfo = _ipcMessenger.GetType().GetMethod("ConnectToExternalPublisher");
            foreach (var sub in subs.ToList().Except(conn.ToList()))
            {
                if(!_ipcMessenger.GetSubscribedTypes(sub.Item1).Contains(sub.Item2))
                    continue;

                var method = methodInfo.MakeGenericMethod(new Type[] { sub.Item2, sub.Item4 });
                method.Invoke(_ipcMessenger, new string[] { sub.Item1, sub.Item3 });
            }
        }

        public void ApplyChanges()
        {
            ApplyDisconnections();
            ApplyConnections();
        }

        public void RegisterPublish<TPubMessage, TSubMessage>(ViewMetaInfo publishView, ViewMetaInfo externalSubscriber)
        {
            _activePublishs.Add(new Tuple<ViewMetaInfo, Type, ViewMetaInfo, Type>(publishView, typeof(TPubMessage),
                                                                                    externalSubscriber,
                                                                                    typeof (TSubMessage)));
        }

        public void RegisterPublish(ViewMetaInfo publisher, ViewMetaInfo externalSubscriber)
        {
            var pubs = from pt in _ipcMessenger.GetPublishedTypes(publisher.LocalId)
                     from st in _ipcMessenger.GetSubscribedTypes(externalSubscriber.FullId)
                     where _ipcMessenger.AdapterService.IsAdaptationPossible(pt, st)
                     select
                         new Tuple<ViewMetaInfo, Type, ViewMetaInfo, Type>(publisher, pt, externalSubscriber, st);

            foreach (var pub in pubs)
            {
                _activePublishs.Add(pub);
                /*var p = new Tuple<string, string, Type, string, string, Type>(pub.Item1.ApplicationName, pub.Item1.LocalId,
                                                                              pub.Item2, pub.Item3.ApplicationName,
                                                                              pub.Item3.LocalId, pub.Item4);
                _publishs.Add(p);*/
            }
        }

        public void UnregisterPublish(ViewMetaInfo publisher, ViewMetaInfo externalSubscriber)
        {
            var pubs = from pt in _ipcMessenger.GetPublishedTypes(publisher.LocalId)
                       from st in _ipcMessenger.GetSubscribedTypes(externalSubscriber.FullId)
                       where _ipcMessenger.AdapterService.IsAdaptationPossible(pt, st)
                       select
                           new Tuple<ViewMetaInfo, Type, ViewMetaInfo, Type>(publisher, pt, externalSubscriber, st);

            foreach (var pub in pubs)
            {
                
                _activePublishs.RemoveWhere((act) => act.Item1.FullId == pub.Item1.FullId
                    && act.Item2 == pub.Item2 && act.Item3.FullId == pub.Item3.FullId && act.Item4 == pub.Item4);
               /* var p = new Tuple<string, string, Type, string, string, Type>(pub.Item1.ApplicationName, pub.Item1.LocalId,
                                                                             pub.Item2, pub.Item3.ApplicationName,
                                                                             pub.Item3.LocalId, pub.Item4);
                _publishs.Remove(p);*/
            }
        }

        public void RegisterSubscription(ViewMetaInfo subscriber, ViewMetaInfo externalPublisher)
        {
            var subs = from pt in _ipcMessenger.GetPublishedTypes(externalPublisher.FullId)
                       from st in _ipcMessenger.GetSubscribedTypes(subscriber.LocalId)
                       where _ipcMessenger.AdapterService.IsAdaptationPossible(pt, st)
                       select
                           new Tuple<ViewMetaInfo, Type, ViewMetaInfo, Type>(subscriber, st, externalPublisher, pt);

            foreach (var sub in subs)
            {
                _activeSubscribes.Add(sub);
            }
        }

        public void UnregisterSubsription(ViewMetaInfo subscriber, ViewMetaInfo externalPublisher)
        {
            var subs = from pt in _ipcMessenger.GetPublishedTypes(externalPublisher.FullId)
                       from st in _ipcMessenger.GetSubscribedTypes(subscriber.LocalId)
                       where _ipcMessenger.AdapterService.IsAdaptationPossible(pt, st)
                       select
                           new Tuple<ViewMetaInfo, Type, ViewMetaInfo, Type>(subscriber, st, externalPublisher, pt);
            

            foreach (var sub in subs)
            {
                _activeSubscribes.RemoveWhere((act) => act.Item1.FullId == sub.Item1.FullId
                    && act.Item2 == sub.Item2 && act.Item3.FullId == sub.Item3.FullId && act.Item4 == sub.Item4);
            }
        }

        public void RegisterConnection(ViewMetaInfo localView, ViewMetaInfo externalView)
        {
            RegisterPublish(localView, externalView);
            RegisterSubscription(localView, externalView);
        }

        public void UnregisterConnection(ViewMetaInfo localView, ViewMetaInfo externalView)
        {
            UnregisterPublish(localView, externalView);
            UnregisterSubsription(localView,externalView);
        }

        public void RegisterSubscription<TPubMessage, TSubMessage>(ViewMetaInfo externalPublisher, ViewMetaInfo subscriberView)
        {
            _activeSubscribes.Add(new Tuple<ViewMetaInfo, Type, ViewMetaInfo, Type>(subscriberView,
                                                                                      typeof (TSubMessage),
                                                                                      externalPublisher,
                                                                                      typeof (TPubMessage)));
        }


        private IEnumerable<string> GetCompatibleExternalSubscribers(string localViewId)
        {
            if (_ipcMessenger.IsEncodedExternal(localViewId))
                return Enumerable.Empty<string>();

            return (from pt in _ipcMessenger.GetPublishedTypes(localViewId)
                          from extSub in _externalProxy.GetCompatibleSubscribers(pt)
                          select extSub).Distinct();
        }

        private IEnumerable<string> GetCompatibleExternalPublishers(string localViewId)
        {
            if (_ipcMessenger.IsEncodedExternal(localViewId))
                return Enumerable.Empty<string>();

            return (from st in _ipcMessenger.GetSubscribedTypes(localViewId)
                    from extSub in _externalProxy.GetCompatiblePublihsers(st)
                    select extSub).Distinct();
        }

        bool IConnectionState.IsConnected(ViewMetaInfo view1, ViewMetaInfo view2)
        {
            var cs = this as IConnectionState;
            return cs.HasPublishedTo(view1, view2) || cs.HasSubscribedFrom(view1, view2);

        }

        bool IConnectionState.IsDisconnected(ViewMetaInfo view1, ViewMetaInfo view2)
        {
            return !(this as IConnectionState).IsConnected(view1, view2) && !(this as IConnectionState).IsImplicitConnected(view1, view2);
        }

        bool IConnectionState.CanPublishTo(ViewMetaInfo publisher, ViewMetaInfo subscriber)
        {
            return this.GetCompatibleExternalSubscribers(publisher.LocalId).Contains(subscriber.FullId) ||
                   this.GetCompatibleExternalPublishers(subscriber.LocalId).Contains(publisher.FullId);
        }

        bool IConnectionState.HasPublishedTo(ViewMetaInfo publisher, ViewMetaInfo subscriber)
        {
            var pubs = from pub in _activePublishs
                       where publisher.FullId.Equals(pub.Item1.FullId) && subscriber.FullId.Equals(pub.Item3.FullId)
                       select pub;

            return pubs.Count() > 0;
        }

        bool IConnectionState.HasSubscribedFrom(ViewMetaInfo subscriber, ViewMetaInfo publisher)
        {
          
            var subs = from sub in _activeSubscribes
                       where subscriber.FullId.Equals(sub.Item1.FullId) && publisher.FullId.Equals(sub.Item3.FullId)
                       select sub;

            return subs.Count() > 0;
        }

        bool IConnectionState.IsImplicitConnected(ViewMetaInfo view1, ViewMetaInfo view2)
        {
            var publishs = from con in _ipcMessenger.GetConnections()
                            where con.Key.Value == view1.LocalId
                            from rec in con.Value
                            where rec.Value == view2.FullId
                            select rec.Value;

            var subscribes = from con in _ipcMessenger.GetConnections()
                             where con.Key.Value == view2.FullId
                             from sender in con.Value
                             where sender.Value == view1.LocalId
                             select con.Key.Value;

            return publishs.FirstOrDefault() != null || subscribes.FirstOrDefault() != null;
        }

        private void OnExternalViewRegisterred(object sender, InterProcessMessenger.ViewRegistrationEventArgs args)
        {
            Active(args.View, args.MessageType, args.AsPublisher);
            ApplyConnections();
        }

        private void OnExternalViewUnregisterred(object sender, InterProcessMessenger.ViewRegistrationEventArgs args)
        {
            Deactive(args.View, args.MessageType, args.AsPublisher);
            ApplyDisconnections();
        }

        private void OnExternalApplicationRemoved(object  sender, InterProcessMessenger.ExternalApplicationEventArgs args)
        {
            DeactiveAll(args.Application);
            ApplyDisconnections();
        }

       

        #region Persistence


        private void BuildConnectionXML(XElement root)
        {
            foreach (var con in _activePublishs)
            {
                var source = con.Item1;
                var st = con.Item2;
                var target = con.Item3;
                var tt = con.Item4;
                
                var connection = new XElement("Publishs",
                    new XAttribute("LocalType", st.FullName),
                    new XAttribute("LcoalAssembly", st.Assembly.GetName().Name),
                    new XAttribute("LocalId", source.LocalId),
                    new XAttribute("ExternalType", tt.FullName),
                    new XAttribute("ExternalAssembly", tt.Assembly.GetName().Name),
                    new XAttribute("ExternalApplication", con.Item3.ApplicationName),
                    new XAttribute("ExternalID", target.LocalId));
                root.Add(connection);
            }


            foreach (var con in _activeSubscribes)
            {
                var source = con.Item1;
                var st = con.Item2;
                var target = con.Item3;
                var tt = con.Item4;

                var connection = new XElement("Subscriptions",
                    new XAttribute("LocalType", st.FullName),
                    new XAttribute("LcoalAssembly", st.Assembly.GetName().Name),
                    new XAttribute("LocalId", source.LocalId),
                    new XAttribute("ExternalType", tt.FullName),
                    new XAttribute("ExternalAssembly", tt.Assembly.GetName().Name),
                     new XAttribute("ExternalApplication", con.Item3.ApplicationName),
                    new XAttribute("ExternalID",  target.LocalId));
                root.Add(connection);
            }


           
        }

        private void RestoreConnections(XElement root)
        {

            _publishs.Clear();
            _activePublishs.Clear();
            _activeSubscribes.Clear();
            

            foreach (var con in root.Elements("Publishs"))
            {
                var nm = con.Attribute("LocalType").Value;
                var asmName = con.Attribute("LcoalAssembly").Value;
                var asm = Assembly.Load(asmName);
                var lt = asm.GetType(nm);
                
                string lid = con.Attribute("LocalId").Value;
                var ttNm = con.Attribute("ExternalType").Value;
                var tAsmNam = con.Attribute("ExternalAssembly").Value;
                var tAsm = Assembly.Load(tAsmNam);
                var et = tAsm.GetType(ttNm);
                var extApp = con.Attribute("ExternalApplication").Value;
                var eid = con.Attribute("ExternalID").Value;

                
                
                _publishs.Add(new Tuple<string, string, Type, string, string, Type>(_ipcMessenger.ApplicationName, lid, lt, extApp, eid, et));
            }


            foreach (var con in root.Elements("Subscriptions"))
            {
                var nm = con.Attribute("LocalType").Value;
                var asmName = con.Attribute("LcoalAssembly").Value;
                var asm = Assembly.Load(asmName);
                var lt = asm.GetType(nm);

                string lid = con.Attribute("LocalId").Value;
                var ttNm = con.Attribute("ExternalType").Value;
                var tAsmNam = con.Attribute("ExternalAssembly").Value;
                var tAsm = Assembly.Load(tAsmNam);
                var et = tAsm.GetType(ttNm);
                var extApp = con.Attribute("ExternalApplication").Value;
                var eid = con.Attribute("ExternalID").Value;

                _subscribes.Add(new Tuple<string, string, Type, string, string, Type>(_ipcMessenger.ApplicationName, lid, lt, extApp, eid, et));
            }

            _ipcMessenger.RequestAllViews().Wait();

            ApplyChanges();}

        void IPersistable.LoadState(XDocument state_)
        {
            XElement root = state_.Element("InterProcessConnecton");
            RestoreConnections(root);

            
        }

        XDocument IPersistable.SaveState()
        {
            XDocument doc = new XDocument();
            XElement root = new XElement("InterProcessConnecton");

            BuildConnectionXML(root);
            doc.Add(root);
            return doc;
        }

        string IPersistable.PersistorId
        {
            get { return GetType().FullName; }
        }
        #endregion
    }
}
