﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Composite.Presentation.Events;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication
{
    public class ViewToViewAggregateMessage<TMessage>
    {
        public ViewToViewAggregateMessage()
        {
        }

        internal bool SentByRegistrator;
        public string ViewContainerId { get; set; }
        public TMessage Message { get; set; }
    }
}
