﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2011 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/ViewToViewCommunication/EventRouter.cs#18 $
// $Change: 877117 $
// $DateTime: 2014/04/16 11:25:13 $
// $Author: hrechkin $

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Threading;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.My;

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication
{
    public class EventRouter : IEventRouter
    {

        private readonly Dictionary<Type, HashSet<object>> _subscriptions = new Dictionary<Type, HashSet<object>>();
        
        private readonly Dictionary<KeyValuePair<Type, object>, HashSet<KeyValuePair<Type, object>>> _connections =
            new Dictionary<KeyValuePair<Type, object>, HashSet<KeyValuePair<Type, object>>>();

        private readonly Dictionary<Type, object> _lastSentMessages = new Dictionary<Type, object>();
        private readonly Dispatcher _dispatcher;
        private static readonly IMSLogger _logger = MSLoggerFactory.CreateLogger<EventRouter>();
       
        public ViewMessageAdapterService ViewMessageAdapterService { get; internal set; }

        #region Implementation of IEventRouter

        public EventRouter(ViewMessageAdapterService adpaterService)
            : this(adpaterService, Dispatcher.CurrentDispatcher)
        {
        }

        internal EventRouter(ViewMessageAdapterService adpaterService, Dispatcher dispatcher)
        {
            Guard.ArgumentNotNull(adpaterService, "ViewMessageAdapterService");
            ViewMessageAdapterService = adpaterService;
            _dispatcher = dispatcher;
        }


        public void Connect<TMessage1, TMessage2>(IPublisher<TMessage1> publisher, ISubscriber<TMessage2> subscriber)
        {
            if (publisher == null)
                throw new ArgumentNullException("publisher");
            if (subscriber == null)
                throw new ArgumentNullException("subscriber");

            HashSet<KeyValuePair<Type, object>> subscribers;
            var publisherKvp = new KeyValuePair<Type, object>(typeof (TMessage1), publisher);
            lock (_connections)
            {
                if (!_connections.TryGetValue(publisherKvp, out subscribers))
                {
                    subscribers = new HashSet<KeyValuePair<Type, object>>();
                    _connections.Add(publisherKvp, subscribers);
                }
            } 
            subscribers.Add(new KeyValuePair<Type, object>(typeof (TMessage2), subscriber));

            object lastMessage;
            if (subscriber is Subscriber<TMessage2>
                && ((Subscriber<TMessage2>) subscriber).ReceiveLastSentMessage
                && _lastSentMessages.TryGetValue(typeof (TMessage1), out lastMessage)
                && lastMessage is TMessage1)
            {
                if (lastMessage.GetType().IsAssignableFrom(typeof (TMessage2)))
                {
                    // done on UI thread to prevent later messages arriving earlier than this one
                    // remember that Publish should also be called on UI thread
// ReSharper disable PossibleInvalidCastException
                    ((IInternalSubscriber<TMessage1>) subscriber).Receive((TMessage1) lastMessage);
// ReSharper restore PossibleInvalidCastException
                }
                else if (ViewMessageAdapterService != null
                         && ViewMessageAdapterService.IsAdaptationPossible(typeof (TMessage1), typeof (TMessage2)))
                {
                    ((IInternalSubscriber<TMessage2>) subscriber).Receive(
                        ViewMessageAdapterService.ApplyAdapterChain<TMessage1, TMessage2>((TMessage1) lastMessage));
                }
            }
        }





        public void Disconnect<TMessage1, TMessage2>(IPublisher<TMessage1> publisher, ISubscriber<TMessage2> subscriber)
        {
            if (publisher == null)
                throw new ArgumentNullException("publisher");
            if (subscriber == null)
                throw new ArgumentNullException("subscriber");

            HashSet<KeyValuePair<Type, object>> subscribers;
            var publisherKvp = new KeyValuePair<Type, object>(typeof (TMessage1), publisher);
            lock (_connections)
            {
                if (_connections.TryGetValue(publisherKvp, out subscribers))
                {
                    subscribers.Remove(new KeyValuePair<Type, object>(typeof(TMessage2), subscriber));
                }    
            }
        }

        #endregion

        internal void Publish<TMessage>(IPublisher<TMessage> publisher, TMessage message,
                                        CommunicationTargetFilter targetFilterLocation, params string[] location)
        {
            List<object> subscribers = new List<object>();
            var result = GetDirectlyConnectedSubscribers(publisher);
            lock (result)
            {
                foreach (var subscriber in result)
                {
                    subscribers.Add(subscriber);
                }
            }

            if (ShellModeExtension.enableParallelBasedEventing)
                Task.Factory.StartNew(() =>
                                      Parallel.ForEach(subscribers
                                              .Where(
                                                  sub =>
                                                  sub is ILocationHolder &&
                                                  ((ILocationHolder) sub).BelongsToLocation(targetFilterLocation,
                                                                                            location)),
                                          subscriber_ =>
                                              {
                                                  try
                                                  {
                                                      if (DoPublish(message, subscriber_)) return;
                                                  }
                                                  catch (Exception exc)
                                                  {
                                                      _logger.Error(
                                                          string.Format(
                                                              "Message delivery failed [type: {0}, filter: {1}, location: {2}"
                                                              , typeof (TMessage).FullName, targetFilterLocation,
                                                              location == null ? "none" : string.Join("/", location)
                                                              ), exc);

                                                  }
                                              }));
            else
                (_dispatcher ?? Dispatcher.CurrentDispatcher).BeginInvoke(
                    () =>
                        {
                            if (subscribers.Where(
                                sub =>
                                sub is ILocationHolder &&
                                ((ILocationHolder) sub).BelongsToLocation(targetFilterLocation, location))
                                                                          .Any(s =>
                                                                              {

                                                                                  try
                                                                                  {
                                                                                      return DoPublish(message, s);
                                                                                  }
                                                                                  catch (Exception exc)
                                                                                  {
                                                                                      _logger.Error(
                                                                                          string.Format(
                                                                                              "Message delivery failed [type: {0}, filter: {1}, location: {2}"
                                                                                              ,
                                                                                              typeof (TMessage).FullName,
                                                                                              targetFilterLocation,
                                                                                              location == null
                                                                                                  ? "none"
                                                                                                  : string.Join("/",
                                                                                                                location)
                                                                                              ), exc);
                                                                                      return false;
                                                                                  }
                                                                              }
                                ))
                            {
                            }
                        });

            _lastSentMessages[typeof (TMessage)] = message;
        }

        private bool DoPublish<TMessage>(TMessage message, object subscriber)
        {
            var objSubscriber = subscriber as IObjectSubscriber;
            if (objSubscriber == null)
            {
                return true;
            }

            if (objSubscriber.GetMessageType() == typeof (TMessage))
            {
                ((IInternalSubscriber<TMessage>) subscriber).Receive(message);
            }
            else
            {
                if (ViewMessageAdapterService != null &&
                    ViewMessageAdapterService.IsAdaptationPossible(typeof (TMessage),
                                                                   objSubscriber.GetMessageType()))
                {
                    var adaptedMessage = ViewMessageAdapterService.ApplyAdapterChain(
                        typeof (TMessage),
                        objSubscriber.GetMessageType(),
                        message);

                    objSubscriber.Receive(adaptedMessage);
                }
            }

            return false;
        }


        private IEnumerable<object> GetDirectlyConnectedSubscribers<TMessage>(IPublisher<TMessage> publisher)
        {
            HashSet<KeyValuePair<Type, object>> subscribers;
            lock (_connections)
            {
                if (
               !_connections.TryGetValue(new KeyValuePair<Type, object>(typeof(TMessage), publisher),
                                         out subscribers))
                {
                    return Enumerable.Empty<object>();
                }
            }

            subscribers = new HashSet<KeyValuePair<Type, object>>(subscribers);
            lock (_subscriptions)
            {
                var subs = from sub in subscribers
                           join t in _subscriptions on sub.Key equals t.Key
                           select sub.Value;
                return subs.ToList();
            }
        }

        internal void Subscribe<TMessage>(IInternalSubscriber<TMessage> subscriber)
        {

            lock (_subscriptions)
            {
                HashSet<object> subscribers;
                if (_subscriptions.ContainsKey(typeof (TMessage)))
                {
                    subscribers = _subscriptions[typeof (TMessage)];
                }
                else
                {
                    subscribers = new HashSet<object>();
                    _subscriptions[typeof (TMessage)] = subscribers;
                }
                subscribers.Add(subscriber);
            }
        }

        internal bool Unsubscribe<TMessage>(IInternalSubscriber<TMessage> subscriber)
        {
            lock (_subscriptions)
            {
                HashSet<object> subscribers;
                return _subscriptions.TryGetValue(typeof(TMessage), out subscribers)
                       && subscribers.Remove(subscriber);    
            }
        }

        internal void RemoveSubscriber<TMessage>(IInternalSubscriber<TMessage> subscriber)
        {
            

            lock (_connections)
            {
                Unsubscribe(subscriber);
                var subscriberValue = new KeyValuePair<Type, object>(typeof(TMessage), subscriber);
                var publishers = from connection in _connections
                                 where connection.Value.Contains(subscriberValue)
                                 select connection.Key;

                foreach (var publisher in publishers)
                {
                    _connections[publisher].Remove(subscriberValue);
                }    
            }
            
        }

        internal void RemovePublisher<TMessage>(IPublisher<TMessage> publisher)
        {
            lock (_connections)
            {
                _connections.Remove(new KeyValuePair<Type, object>(typeof(TMessage), publisher));    
            }
            
        }

        internal IDictionary<KeyValuePair<Type, object>, HashSet<KeyValuePair<Type, object>>> GetConnections()
        {
            lock (_connections)
            {
                var copy = new Dictionary<KeyValuePair<Type, object>, HashSet<KeyValuePair<Type, object>>>();
                foreach (var connection in _connections)
                {
                    copy[connection.Key]=connection.Value;
                }
                return copy;
            }
        }

    }
}
