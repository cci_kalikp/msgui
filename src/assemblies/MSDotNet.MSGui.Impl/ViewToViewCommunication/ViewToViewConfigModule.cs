﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Infragistics.Windows.Ribbon;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.Shell;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication
{
    public class ViewToViewConfigModule : IModule
    {
        private readonly IWindowManager _windowManager;
        private readonly ViewCollection _viewCollection;
        private readonly ConfigModel _config;
        private readonly IControlParadigmRegistrator _controlParadigmRegistrator;
        private readonly IControlParadigmImplementator _controlParadigmImplementator;
        private  ToolMenuItem _menuItem;
        internal ViewToViewConfigModule(IShell shell, ViewCollection viewCollection, IEventRegistrator eventRegistrator_, IControlParadigmRegistrator controlParadigmRegistrator_, IControlParadigmImplementator controlParadigmImplementator_)
        {
            _controlParadigmRegistrator = controlParadigmRegistrator_;
            _controlParadigmImplementator = controlParadigmImplementator_;
            _windowManager = shell.WindowManager;
            _viewCollection = viewCollection;
            _config = new ConfigModel(eventRegistrator_ as EventRegistrator, viewCollection, _windowManager);
        }

        public void Initialize()
        {
            var menuItem = new ToolMenuItem()
                                        {
                                            Header = "View to View Communication",
                                            Icon = new Image
                                                       {
                                                           Source =
                                                               BitmapFrame.Create(
                                                                   new Uri(
                                                                       @"pack://application:,,,/MSDotNet.MSGui.Impl;component/ViewToViewCommunication/GUI/Img/communication_icon.png",
                                                                       UriKind.RelativeOrAbsolute))
                                                       }
                                        };
            menuItem.Click += CreateWindow;

            _menuItem = menuItem;

            _controlParadigmRegistrator.Register("ViewToViewCommunicationUI", menuItem);
            _controlParadigmImplementator["Menu"].AddItem("ViewToViewCommunicationUI");

        }

        private void CreateWindow(object  sender, EventArgs eventArgs)
        {
            
           //var dialog =  _windowManager.CreateDialog();
            var dialog = new Window();
            dialog.Title = "View to View Communication Configuration";
            dialog.Width = 600;
            dialog.Height = 400;
            dialog.Icon =
                BitmapFrame.Create(
                    new Uri(
                        @"pack://application:,,,/MSDotNet.MSGui.Impl;component/ViewToViewCommunication/GUI/Img/communication_icon.png",
                        UriKind.RelativeOrAbsolute));
                          
            var v2vConfigUi = new V2VConfigUI();
            v2vConfigUi.Initialize(_windowManager,_viewCollection, _config);
            dialog.Content = v2vConfigUi;
            dialog.Owner = Window.GetWindow(_menuItem);
            dialog.ShowDialog();
        }
    }
}
