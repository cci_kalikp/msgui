﻿using System;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MSDesktop.Isolation.Interfaces;

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication
{
    internal sealed class MarshalingPublisher<TMessage> : IDisposable, IPublisher<TMessage>, ILocationHolder
    {
        private readonly IXProcAddInHost _addinHost;
        private bool _disposed;
        private readonly EventRouter _eventRouter;

        internal MarshalingPublisher(EventRouter eventRouter, IXProcAddInHost addinHost)
        {
            _eventRouter = eventRouter;
            _addinHost = addinHost;
            _addinHost.RegisterMessageHandler(typeof(TMessage), PublishInternal);
        }

        public void Dispose()
        {
            if (!_disposed)
            {
                // TODO (hrechkin(: Provide a story for unregistering subscribers.
                // _addinHost.MessageArrived -= AddinHost_MessageArrived;
                _disposed = true;
            }
        }

        public void Publish(TMessage message, CommunicationTargetFilter locationFilter = CommunicationTargetFilter.All,
                            params string[] location)
        {
            _eventRouter.Publish(this, message, locationFilter, location);
        }

        private void PublishInternal(object message)
        {
            Publish((TMessage)message);
        }

        public CafGuiApplicationInfoProxy Application { get; set; }
        public string Tab { get; set; }
    }
}
