﻿using System;

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication
{
    public interface IMessageTypeRegistrator
    {
        void RegisterType<T>();
        Type ResolveType(string id);
        bool IsRegisterred(Type msgType);
        string GetRegistartionId(Type msgType);
    }
}
