﻿using System;
using System.Collections.Generic;

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication
{
    internal sealed class ViewMessageActionsCollection
    {
        private readonly IDictionary<Type, IList<Action<object>>>  _actionsByType =
            new Dictionary<Type, IList<Action<object>>>();

        internal ViewMessageActionsCollection()
        {
            // Want this to be internal to indicate no usage through Unity.
        }

        internal void RegisterAction(Type type, Action<object> action)
        {
            IList<Action<object>> actions;
            if (!_actionsByType.TryGetValue(type, out actions))
            {
                actions = new List<Action<object>>();
                _actionsByType.Add(type, actions);
            }

            actions.Add(action);
        }

        internal void ExecuteActions(object message)
        {
            var type = message.GetType();
            IList<Action<object>> actions;

            if (_actionsByType.TryGetValue(type, out actions))
            {
                foreach (var action in actions)
                {
                    action(message);
                }
            }
        }
    }
}
