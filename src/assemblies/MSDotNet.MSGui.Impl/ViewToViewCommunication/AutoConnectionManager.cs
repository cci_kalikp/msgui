﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Core.ScreenGroup;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging;
using MorganStanley.MSDotNet.MSGui.Impl.ScreenGroup;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication
{
    internal sealed class AutoConnectionManager : IV2VConnectionManager
    {
        private readonly IWindowManager _windowManager;
        private readonly IEnumerable<IWindowViewContainer> _viewCollection;
        private readonly IpcLocalConnectionManager _localConnectionManager;

        private readonly Dictionary<IWindowViewContainer, object> _viewHost = new Dictionary<IWindowViewContainer, object>();
        private readonly Dictionary<IWindowViewContainer, Action<IEnumerable<IWindowViewContainer>>> _hookupCallback = new Dictionary<IWindowViewContainer, Action<IEnumerable<IWindowViewContainer>>>();
        private readonly Dictionary<IWindowViewContainer, Action<IEnumerable<IWindowViewContainer>>> _unhookedCallback = new Dictionary<IWindowViewContainer, Action<IEnumerable<IWindowViewContainer>>>();

        private readonly Dictionary<IWindowViewContainer, HashSet<IWindowViewContainer>> _hookupViews = new Dictionary<IWindowViewContainer, HashSet<IWindowViewContainer>>();
        private readonly Dictionary<IWindowViewContainer, HashSet<IWindowViewContainer>> _unhookViews = new Dictionary<IWindowViewContainer, HashSet<IWindowViewContainer>>();
        private readonly IScreenGroupManager _screenGroupManager;

        private AutoHookMode _autoHookMode;

        public AutoConnectionManager(IWindowManager windowManager, 
            IEnumerable<IWindowViewContainer> viewCollection,
            IpcLocalConnectionManager localConnection)
        {
            _windowManager = windowManager;
            _viewCollection = viewCollection;
            _localConnectionManager = localConnection;

            if (ScreenGroupManager.IsScreenGroupSupportEnabled)
            {
                _screenGroupManager = ServiceLocator.Current.GetInstance<IUnityContainer>().Resolve<IScreenGroupManager>();
                _screenGroupManager.GroupMemberChanged += this.ScreenGroupChangeHandlerInDisp;
            } 

        }

        public AutoHookMode AutoHookMode
        {
            set { _autoHookMode = value; }
        }

        internal void Reset()
        {
            _hookupViews.Clear();
            _unhookViews.Clear();
        }

        public void SignUpAutoHook(IWindowViewContainer viewContainer, Action<IEnumerable<IWindowViewContainer>> hookupCallback, Action<IEnumerable<IWindowViewContainer>> unhookedCallback)
        {
           
            var view = viewContainer as WindowViewContainer;
            Debug.Assert(view != null);

            _hookupCallback[view] = hookupCallback;
            _unhookedCallback[view] = unhookedCallback;

           
            view.HostChanged += HostChangeHandler;
            view.V2vHostChanged += HostChangeHandler;
            view.ReleaseResources += AutoSignOffHandler;
        }

        private void AutoSignOffHandler(object sender, EventArgs args)
        {   
            SignOffAutoHook(sender as IWindowViewContainer);
        }

        public void SignOffAutoHook(IWindowViewContainer viewContainer)
        {
            var view = viewContainer as WindowViewContainer;
            Debug.Assert(view != null);


            _viewHost.Remove(viewContainer);
            _hookupCallback.Remove(view);
            _unhookedCallback.Remove(view);

            view.HostChanged -= HostChangeHandler;
            view.V2vHostChanged -= HostChangeHandler;
            view.ReleaseResources -= AutoSignOffHandler;
        }



        private void UnHook(IWindowViewContainer view, IEnumerable<IWindowViewContainer> views)
        {
            _localConnectionManager.AutoUnhook(view, views);
            if (_unhookedCallback.ContainsKey(view) && _unhookedCallback[view] != null)
                _unhookedCallback[view](views);
        }

        private void Hook(IWindowViewContainer view, IEnumerable<IWindowViewContainer> views)
        {
            _localConnectionManager.AutoHook(view, views);

            if (_hookupCallback.ContainsKey(view) && _hookupCallback[view] != null)
                _hookupCallback[view](views);
        }


        private ShellTabViewModel GetTab(WindowViewContainer viewContainer, DockViewModel tabbedDockViewModel)
        {
            foreach (var ws in tabbedDockViewModel.Workspaces)
            {
                var child = viewContainer as IChildViewContainer;

                IWindowViewContainer window; 
                if (child != null)
                {
                    window = child.Parent;
                }
                else
                {
                    window = viewContainer;
                }

                if (ws.ViewContainer == window)
                {
                    return ws.Host as ShellTabViewModel;
                }
            }

            return null;
        }

        private void  ScreenGroupChangeHandlerInDisp(object sender, GroupChangedEventArgs args)
        {
            if (args.OldGroup != null)
            {
                if (args.OldRole.HasFlag(V2VRole.AsPublisher))
                {
                    if (_screenGroupManager.IsViewSelfAutoHook(args.Window))
                    {
                        _localConnectionManager.AutoUnhookPublishing(args.Window, args.OldGroup.Windows.Union(Enumerable.Repeat(args.Window, 1)));
                    }
                    else
                    {
                        _localConnectionManager.AutoUnhookPublishing(args.Window, args.OldGroup.Windows.Except(Enumerable.Repeat(args.Window, 1)));     
                    }
                    
                }
                if (args.OldRole.HasFlag(V2VRole.AsSubscriber))
                {
                    if (_screenGroupManager.IsViewSelfAutoHook(args.Window))
                    {
                        _localConnectionManager.AutoUnhookSubscribing(args.Window, args.OldGroup.Windows.Union(Enumerable.Repeat(args.Window, 1)));
                    }
                    else
                    {
                        _localConnectionManager.AutoUnhookSubscribing(args.Window, args.OldGroup.Windows.Except(Enumerable.Repeat(args.Window, 1)));
                    }
                }
            }

            if (args.NewGroup != null)
            {
                if (args.NewRole.HasFlag(V2VRole.AsPublisher))
                {
                    if (_screenGroupManager.IsViewSelfAutoHook(args.Window))
                    {
                        _localConnectionManager.AutoHookPublishing(args.Window, args.NewGroup.Windows.Union(Enumerable.Repeat(args.Window, 1)));
                    }
                    else
                    {
                        _localConnectionManager.AutoHookPublishing(args.Window, args.NewGroup.Windows.Except(Enumerable.Repeat(args.Window, 1)));        
                    }
                
                }
                if (args.NewRole.HasFlag(V2VRole.AsSubscriber))
                {
                    if (_screenGroupManager.IsViewSelfAutoHook(args.Window))
                    {
                        _localConnectionManager.AutoHookSubscribing(args.Window, args.NewGroup.Windows.Union(Enumerable.Repeat(args.Window, 1)));
                    }
                    else
                    {
                        _localConnectionManager.AutoHookSubscribing(args.Window, args.NewGroup.Windows.Except(Enumerable.Repeat(args.Window, 1)));
                    }
                
                }
            }
        }

        private void  HostChangeHandlerInDisp(WindowViewContainer.HostChangedEventArgs args)
        {
            var view = args.Sender;
            if (!_hookupCallback.ContainsKey(view)) // not signed up
                return;

            object host;
            _viewHost.TryGetValue(args.Sender, out host);
            if (host != args.OldHost)
            {
                Trace.WriteLine("Old host is inconsistent");
                host = args.OldHost;
            }

            var oldTab = host;// as ShellTabViewModel;
            var newTab = args.NewHost;// as ShellTabViewModel;
            _viewHost[view] = args.NewHost;

            Tuple<IEnumerable<IWindowViewContainer>, IEnumerable<IWindowViewContainer>> views;
            switch (_autoHookMode)
            {
                case AutoHookMode.IslandMode:

                    views = GetIslandViews(view, oldTab as ShellTabViewModel, newTab as ShellTabViewModel);
                    break;
                case AutoHookMode.TabAndFloatingMode:
                    if (!(_windowManager is TabbedDockViewModel))
                    {
                        throw new ApplicationException("V2V Auto-hook: TabAndFloatingMode is not supported on Non-tabbed application");
                    }

                    var nt = GetTab(view, _windowManager as TabbedDockViewModel);
                  

                    views = GetTabbedViews(view, oldTab as ShellTabViewModel, nt);
                    break;
                default:
                    throw new ApplicationException("Unsupported auto hook mode " + _autoHookMode.ToString());
            }
            
            _localConnectionManager.DiscardOutdatedDisconnections(view, views.Item1);

            UnHook(view, views.Item1);
            Hook(view, views.Item2);
        }

        private void HostChangeHandler(object sender, WindowViewContainer.HostChangedEventArgs args)
        {
            //Dispatcher.CurrentDispatcher.Invoke(() => HostChangeHandlerInDisp(sender, args));
            HostChangeHandlerInDisp(args);
        }


        private object GetTopHost(IWindowViewContainer view, object host = null)
        {
           //var chromeManager = ServiceLocator.Current.GetInstance<IChromeManager>(); 
            var tab = host as ShellTabViewModel;
            if (tab != null)
                return tab;

            tab = GetTab(view as WindowViewContainer, _windowManager as DockViewModel);
            if (tab != null)
                return tab;

            var tw = DockHelper.GetRootOfFloatingPane(view);
           
            if (tw != null)
                return tw;

          

         /*   throw new ApplicationException("Cannot find the Tool window or tab for the view " + view.Title + "  " +
                                           view.ID);*/
            return null;
        }

        private Tuple<IEnumerable<IWindowViewContainer>, IEnumerable<IWindowViewContainer>> GetIslandViews(IWindowViewContainer view, ShellTabViewModel oldTab, ShellTabViewModel newTab)
        {

            if (!AllowHookup(view))
            {
                return new Tuple<IEnumerable<IWindowViewContainer>, IEnumerable<IWindowViewContainer>>(Enumerable.Empty<IWindowViewContainer>(), Enumerable.Empty<IWindowViewContainer>());
            }
            var host = GetTopHost(view, newTab);


            IEnumerable<IWindowViewContainer> unhookViews = from uv in _viewHost
                                                     where uv.Key != view && GetTopHost(uv.Key) != host
                                                     select uv.Key;

            IEnumerable<IWindowViewContainer> hookViews = from uv in _viewHost
                                                    where uv.Key != view && GetTopHost(uv.Key) == host
                                                    select uv.Key;
            return new Tuple<IEnumerable<IWindowViewContainer>, IEnumerable<IWindowViewContainer>>(unhookViews,hookViews);
        }


        private Tuple<IEnumerable<IWindowViewContainer>, IEnumerable<IWindowViewContainer>> GetTabbedViews(IWindowViewContainer view, ShellTabViewModel oldTab, ShellTabViewModel newTab)
        {

            if (!AllowHookup(view))
            {
                return new Tuple<IEnumerable<IWindowViewContainer>, IEnumerable<IWindowViewContainer>>(Enumerable.Empty<IWindowViewContainer>(), Enumerable.Empty<IWindowViewContainer>());
            }

            IEnumerable<IWindowViewContainer> unhookViwes;
            var manager = _windowManager as TabbedDockViewModel;
            if (manager == null)
                Trace.WriteLine("Why tab is not in TabbedDockViewModel?");
           

            IEnumerable<IWindowViewContainer> hookViews;
            
            if (newTab != null)
            {
                unhookViwes = from ws in manager.Workspaces
                              where (ws.Host != null && ws.Host != newTab) && ws.ViewContainer is IWindowViewContainer
                              select ws.ViewContainer as IWindowViewContainer;

                hookViews = from ws in manager.Workspaces
                            where (ws.Host == null || ws.Host == newTab) && ws.ViewContainer is IWindowViewContainer && AllowHookup(ws.ViewContainer as IWindowViewContainer) 
                        select ws.ViewContainer as IWindowViewContainer;
            }
            else
            {
                unhookViwes = Enumerable.Empty<IWindowViewContainer>();
                hookViews = _viewCollection.Where(v => v != view);
            }

            var uvs = unhookViwes.Except(hookViews);

            return new Tuple<IEnumerable<IWindowViewContainer>, IEnumerable<IWindowViewContainer>>(uvs, hookViews);

        }

        private void RegisterUnhookView(IWindowViewContainer source, IWindowViewContainer target)
        {
            HashSet<IWindowViewContainer> views;
            _unhookViews.TryGetValue(source, out views);
            if (views == null)
            {
                _unhookViews[source] = new HashSet<IWindowViewContainer>();
            }

            _unhookViews[source].Add(target);
        }

        private void UnregisterUnhookView(IWindowViewContainer source, IWindowViewContainer target)
        {
            if (!_unhookViews.ContainsKey(source))
                return;

            _unhookViews[source].Remove(target);

            if (_unhookViews[source].Count == 0)
                _unhookViews.Remove(source);    
            
        }

       
        private void RegisterHookupView(IWindowViewContainer source, IWindowViewContainer target)
        {
            HashSet<IWindowViewContainer> views;
            _hookupViews.TryGetValue(source, out views);
            if (views == null)
            {
                _hookupViews[source] = new HashSet<IWindowViewContainer>();
            }

            _hookupViews[source].Add(target);
        }

        private void UnregisterHookupView(IWindowViewContainer source, IWindowViewContainer target)
        {
            if (!_hookupViews.ContainsKey(source))
                return;

            _hookupViews[source].Remove(target);
            if (_hookupViews[source].Count == 0)
                _hookupViews.Remove(source);

        }

        internal void RegisterHookup(IWindowViewContainer source, string sTab, IWindowViewContainer target, string tTab)
        {
            if (CanHookTo(source, sTab, tTab))
            {
                RegisterHookupView(source, target);
            }
            else
            {
                RegisterUnhookView(source, target);
            }
        }

        internal void UnregisterHookup(IWindowViewContainer source, string sTab, IWindowViewContainer target, string tTab)
        {
            if (CanHookTo(source, sTab, tTab))
            {
                UnregisterHookupView(source, target);
            }
            else
            {
                UnregisterUnhookView(source, target);
            }
        }


        internal bool CanHookTo(ViewMetaInfo source, ViewMetaInfo target)
        {
            return CanHookTo(source.ViewContainer as IWindowViewContainer, source.TabName, target.TabName) && AllowHookup(source.ViewContainer as IWindowViewContainer) && AllowHookup(target.ViewContainer as IWindowViewContainer);
        }

       

        internal bool CanHookTo(IWindowViewContainer source, string sTab, string tTab)
        {
            if (!_hookupCallback.ContainsKey(source))
                return false;
            if(!AllowHookup(source))
                return false;
            return sTab == null || tTab == null || sTab == tTab;
        }

        internal bool AllowHookup(IWindowViewContainer view)
        {
            return ScreenGroupManager.IsScreenGroupSupportEnabled ||  _hookupCallback.ContainsKey(view);
        }

        internal void UpdateHooks()
        {
            foreach (var pair in _unhookViews)
            {
                UnHook(pair.Key, pair.Value);
            }
            foreach (var pair in _hookupViews)
            {   
                Hook(pair.Key, pair.Value);
            }
            _hookupViews.Clear();
        }


    }
}
