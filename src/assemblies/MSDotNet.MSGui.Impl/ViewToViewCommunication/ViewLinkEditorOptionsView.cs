﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2011 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/ViewToViewCommunication/ViewLinkEditorOptionsView.cs#6 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication
{
  internal class ViewLinkEditorOptionsView : IOptionView
  {
    //internal readonly IViewManager m_viewManager;
      private readonly ViewCollection m_viewCollection;
      private readonly IWindowManager m_windowManager;
    private readonly EventRegistrator m_eventRegistrator;
    
    //private readonly ViewCommunicationConfig m_publisherView;
      private readonly V2VConfigUI m_view2viewUI;

    private readonly ConfigModel m_config;

    internal ViewLinkEditorOptionsView(IChromeManager chromeManager_, EventRegistrator eventRegistrator_)
    {
      //m_viewManager = viewManager_;
      m_eventRegistrator = eventRegistrator_;
        m_viewCollection = viewManager_.Views;
        m_windowManager = windowManager_;
      m_config = new ConfigModel(eventRegistrator_,  viewManager_.Views, windowManager_);      
      //m_publisherView = new ViewCommunicationConfig(m_config);
        
        m_view2viewUI = new V2VConfigUI();
    }

    #region Implementation of IOptionView

    public void OnOK()
    {      
        m_config.ApplyChanges();
    }

    public void OnCancel()
    {
    }

    public void OnDisplay()
    {
        m_config.InitConfigInfo();
    }

    public void OnHelp()
    {
    }

    public UIElement Content
    {
      get { return m_view2viewUI; }
    }

    #endregion
  }
}
