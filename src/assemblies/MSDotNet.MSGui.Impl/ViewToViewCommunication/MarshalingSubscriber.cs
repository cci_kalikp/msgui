﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MSDesktop.Isolation.Interfaces;

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication
{
    internal sealed class MarshalingSubscriber<TMessage> : ISubscriber<TMessage>, IDisposable, IObjectSubscriber,
                                                           ILocationHolder, IInternalSubscriber<TMessage>
    {
        private readonly ISubscriber<TMessage> _localSubscriber;
        private bool _disposed;
        private readonly IXProcAddInHost _host;
        private readonly IFormatter _formatter;

        internal MarshalingSubscriber(EventRouter eventRouter, bool receiveLastSentMessage, IXProcAddInHost host)
        {
            _host = host;
            _localSubscriber = SubscriberHelper.CreateSubscriber<TMessage>(eventRouter, receiveLastSentMessage);
            _formatter = new BinaryFormatter();
        }

        public CafGuiApplicationInfoProxy Application
        {
            get { return ((ILocationHolder)_localSubscriber).Application; }
            set { ((ILocationHolder)_localSubscriber).Application = value; }
        }

        public string Tab
        {
            get { return ((ILocationHolder)_localSubscriber).Tab; }
            set { ((ILocationHolder)_localSubscriber).Tab = value; }
        }

        public Type GetMessageType()
        {
            return ((IObjectSubscriber) _localSubscriber).GetMessageType();
        }

        public void Receive(TMessage message)
        {
            Receive((object) message);
        }

        public void Receive(object message)
        {
            if (_host.AddIn != null)
            {
                byte[] payloadBytes;
                using (var stream = new MemoryStream())
                {
                    _formatter.Serialize(stream, message);
                    payloadBytes = stream.GetBuffer();
                }

                _host.AddIn.Receive(payloadBytes);
            }
        }

        public void Dispose()
        {
            if (!_disposed)
            {
                ((IDisposable)_localSubscriber).Dispose();
                _disposed = true;
            }
        }

        public void Subscribe(Action<TMessage> messageHandler)
        {
            _localSubscriber.Subscribe(messageHandler);
        }

        public IObservable<TMessage> GetObservable()
        {
            return _localSubscriber.GetObservable();
        }
    }
}
