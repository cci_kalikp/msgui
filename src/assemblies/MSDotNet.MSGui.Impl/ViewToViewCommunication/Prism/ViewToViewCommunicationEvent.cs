﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.Events;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication;

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication.Prism
{
    class ViewToViewCommunicationEvent<TMessage> : CompositePresentationEvent<ViewToViewAggregateMessage<TMessage>>, IViewToViewCommunicationEvent<TMessage>
    {
        private readonly CompositePresentationEvent<ViewToViewAggregateMessage<TMessage>> _internal;

        public ViewToViewCommunicationEvent()
        {
            _internal = Activator.CreateInstance<CompositePresentationEvent<ViewToViewAggregateMessage<TMessage>>>();

        }

        internal void PublishToInternal(ViewToViewAggregateMessage<TMessage> payload)
        {
            _internal.Publish(payload);
        }

        void IViewToViewCommunicationEvent<TMessage>.PublishToExternal(ViewToViewAggregateMessage<TMessage> payload)
        {
            base.Publish(payload);
        }

        public override void Publish(ViewToViewAggregateMessage<TMessage> payload)
        {
            //PublishToExternal(payload);
            PublishToInternal(payload);
        }

        #region Implementation of IViewToViewCommunicationEvent<TMessage>

        void IViewToViewCommunicationEvent<TMessage>.InternalSubscribe(Action<ViewToViewAggregateMessage<TMessage>> action, MSDesktopThreadOption threadOption, bool keepSubscriberReferenceAlive)
        {
            ThreadOption to;
            Enum.TryParse(threadOption.ToString(), out to);
            _internal.Subscribe(action, to, keepSubscriberReferenceAlive);
        }

        #endregion
    }
}
