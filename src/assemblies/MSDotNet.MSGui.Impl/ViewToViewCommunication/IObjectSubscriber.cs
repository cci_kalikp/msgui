﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2011 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/ViewToViewCommunication/IObjectSubscriber.cs#4 $
// $Change: 842602 $
// $DateTime: 2013/08/20 14:36:37 $
// $Author: hrechkin $

using System;

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication
{
  internal interface IObjectSubscriber
  {
    /// <summary>
    /// Gets the type of the message.
    /// </summary>
    /// <returns>The consumed message type</returns>
    Type GetMessageType();

    /// <summary>
    /// Receives the specified message.
    /// </summary>
    /// <param name="message">The message.</param>
    void Receive(object message);
  }
}
