﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2011 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/ViewToViewCommunication/Publisher.cs#6 $
// $Change: 856187 $
// $DateTime: 2013/11/26 06:08:06 $
// $Author: milosp $

using System;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication
{
    public class Publisher<TMessage> : IDisposable, IPublisher<TMessage>, ILocationHolder
    {
        private EventRouter _eventRouter;

        internal Publisher(EventRouter eventRouter)
        {
            _eventRouter = eventRouter;
        }

        #region Implementation of IPublisher<in TMessage>

        public void Publish(TMessage message, CommunicationTargetFilter targetFilterLocation = CommunicationTargetFilter.All, params string[] location)
        {
            if (_eventRouter == null)
            {
                throw new InvalidOperationException("This publisher has been deregistered");
            }

            var args = new PublisherPublishingEventArgs { Message = message };
            OnPublisherPublishing(args);
            if (args.Cancel) return;

            _eventRouter.Publish(this, message, targetFilterLocation, location);
        }


        #endregion

        #region Implementation of IDisposable

        public void Dispose()
        {
            if (_eventRouter == null)
            {
                throw new InvalidOperationException("This publisher has already been deregistered");
            }
            _eventRouter.RemovePublisher(this);
            _eventRouter = null;
        }

        #endregion



        public CafGuiApplicationInfoProxy Application { get; set; }

        public string Tab { get; set; }

        public event EventHandler<PublisherPublishingEventArgs> PublisherPublishing;

        private void OnPublisherPublishing(PublisherPublishingEventArgs e_)
        {
            var handler = PublisherPublishing;
            if (handler != null) handler(this, e_);
        }
    }

    public class PublisherPublishingEventArgs : EventArgs
    {
        public object Message { get; set; }
        public bool Cancel { get; set; }
    }
}
