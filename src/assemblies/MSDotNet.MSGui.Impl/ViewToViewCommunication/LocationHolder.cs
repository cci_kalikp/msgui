﻿using MorganStanley.MSDotNet.MSGui.Core.Messaging;

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication
{
    internal class LocationHolder :ILocationHolder
    {
        public CafGuiApplicationInfoProxy Application { get; set; }
        public string Tab { get; set; }
    }

}
