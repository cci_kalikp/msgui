﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication
{
    /// <summary>
    /// Interaction logic for V2VConfigUI.xaml
    /// </summary>
    [Obsolete]
    public partial class V2VConfigUI : UserControl
    {
      

        private  CommunicationViewModel m_communicationViewModel;
        internal V2VConfigUI()
        {
            InitializeComponent();
            //Initialize(windowManage,viewContainers);
        }

        internal void Initialize(IWindowManager windowManager, IEnumerable<IWindowViewContainer> viewCollection, ConfigModel configModel, CommunicationViewModel communicationViewModel)
        {
            //var configModel = new ConfigModel(null, viewCollection, windowManager);
            configModel.InitConfigInfo();
            m_communicationViewModel = communicationViewModel;
            this.ViewSelector1.ItemsSource = m_communicationViewModel.Tabs;
            this.ViewSelector2.ItemsSource = m_communicationViewModel.Tabs;
            m_communicationViewModel.HookManager.Reset();
        }

        private void CanSetView1(object sender, CanExecuteRoutedEventArgs e)
        {
            var viewInfo = e.Parameter as ViewInfoModel;
            e.CanExecute= viewInfo != null;
        }

        private void SetView1(object sender, ExecutedRoutedEventArgs e)
        {
            var viewInfo = (ViewInfoModel) e.Parameter;
            viewInfo.CommunicationModel.View1 = viewInfo.View;
            viewInfo.CommunicationModel.Tab1 = viewInfo.HostTab;
        }


        private void AllowHook(object sender,RoutedEventArgs args)
        {
            var targetInfo = ((MenuItem) args.Source).CommandParameter as ViewInfoModel;
            if(targetInfo==null)
                throw new ApplicationException("Cannot get target view");
            var sourceView = m_communicationViewModel.View1;
            var sourceTab = m_communicationViewModel.Tab1;
            var targetView = targetInfo.View;
            var targetTab = targetInfo.HostTab;

            m_communicationViewModel.HookManager.RegisterHookup(sourceView, sourceTab, targetView, targetTab);
            m_communicationViewModel.ConfigModel.UnreigsterExplicitConnection(sourceView, targetView);
            m_communicationViewModel.ConfigModel.UnreigsterExplicitDisconnection(sourceView, targetView);


            m_communicationViewModel.View1 = targetInfo.CommunicationModel.View1;
        }

        private void ExplicitConnect(object sender, RoutedEventArgs args)
        {
            var targetInfo = ((MenuItem)args.Source).CommandParameter as ViewInfoModel;
            if (targetInfo == null)
                throw new ApplicationException("Cannot get target view");

            var sourceView = m_communicationViewModel.View1;
            var sourceTab = m_communicationViewModel.Tab1;
            var targetView = targetInfo.View;
            var targetTab = targetInfo.HostTab;
            m_communicationViewModel.ConfigModel.ReigsterExplicitConnection(sourceView, targetView);
            m_communicationViewModel.ConfigModel.UnreigsterExplicitDisconnection(sourceView, targetView);
            m_communicationViewModel.HookManager.UnregisterHookup(sourceView, sourceTab, targetView, targetTab);
            m_communicationViewModel.View1 = sourceView;
        }

        private void ExplicitDisconnect(object sender, RoutedEventArgs args)
        {
            var targetInfo = ((MenuItem)args.Source).CommandParameter as ViewInfoModel;
            if (targetInfo == null)
                throw new ApplicationException("Cannot get target view");
            var sourceView = m_communicationViewModel.View1;
            var sourceTab = m_communicationViewModel.Tab1;
            var targetView = targetInfo.View;
            var targetTab = targetInfo.HostTab;
            m_communicationViewModel.ConfigModel.ReigsterExplicitDisconnection(sourceView, targetView);
            m_communicationViewModel.ConfigModel.UnreigsterExplicitConnection(sourceView, targetView);
            m_communicationViewModel.HookManager.UnregisterHookup(sourceView, sourceTab, targetView, targetTab);
            m_communicationViewModel.View1 = sourceView;
        }

        private void CanConnect(object sender, CanExecuteRoutedEventArgs e)
        {
            var viewInfo = e.Parameter as ViewInfoModel;
            if (viewInfo == null)
            {
                e.CanExecute = false;
                return;
            }

            if (m_communicationViewModel.View1 == null)
            {
                e.CanExecute = false;
                return;
            }

            if (m_communicationViewModel.ConfigModel.GetCompatiblePublishers(viewInfo.View).Contains(m_communicationViewModel.View1)
                || m_communicationViewModel.ConfigModel.GetCompatibleSubscribers(viewInfo.View).Contains(m_communicationViewModel.View1))
            {
                e.CanExecute = true;
                return;
            }
            else
            {
                e.CanExecute = false;
                return;
            }
                
        }

        private void Connect(object sender, ExecutedRoutedEventArgs e)
        {
            var viewInfo = (ViewInfoModel) e.Parameter;
            var view1 = m_communicationViewModel.View1;
            var view2 = viewInfo.View;
            m_communicationViewModel.ConfigModel.ReigsterExplicitConnection(view1, view2);
            m_communicationViewModel.View1 = view1; //force UI change
        }

        private void DisConnect(object sender, ExecutedRoutedEventArgs e)
        {
            var viewInfo = (ViewInfoModel)e.Parameter;
            var view1 = m_communicationViewModel.View1;
            var view2 = viewInfo.View;
            m_communicationViewModel.ConfigModel.UnreigsterExplicitConnection(view1, view2);
            m_communicationViewModel.View1 = view1; //force UI change
        }

        private void ApplyChanges(object sender, EventArgs eventArgs)
        {
            m_communicationViewModel.ConfigModel.ApplyChanges();
            m_communicationViewModel.HookManager.UpdateHooks();
            var window = Window.GetWindow(this);
            if (window != null)
                window.Close();
        }

        private void Cancel(object sender, EventArgs eventArgs)
        {
            var window = Window.GetWindow(this);
            if(window!=null)
                window.Close();
        }
    }

    [ValueConversion(typeof(object), typeof(int))]
    internal class HeightConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            var ui = value as UIElement;
            if (ui != null && ui.RenderSize != null && !ui.RenderSize.IsEmpty)
            {
                return ui.RenderSize.Height;
            }

            if (ui != null && ui.DesiredSize != null && !ui.DesiredSize.IsEmpty)
            {
                return ui.DesiredSize.Height;
            }

            return 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }

    }

    [ValueConversion(typeof(object), typeof(int))]
    internal class WidthConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var ui = value as UIElement;
            if (ui != null && ui.RenderSize != null && !ui.RenderSize.IsEmpty)
            {
                return ui.RenderSize.Width;
            }

            if (ui != null && ui.DesiredSize != null && !ui.DesiredSize.IsEmpty)
            {
                return ui.DesiredSize.Width;
            }

            return 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }

    }
}
