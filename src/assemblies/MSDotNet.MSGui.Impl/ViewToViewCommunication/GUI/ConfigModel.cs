﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core;
using System.Reflection;
using MorganStanley.MSDotNet.MSGui.Core.Persistence; 
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication
{
    internal class ConfigModel : IPersistable
    {
        internal readonly Dictionary<IWindowViewContainer, HashSet<Type>> m_publishs;
		internal readonly Dictionary<IWindowViewContainer, HashSet<Type>> m_subscriptions;
        internal readonly EventRegistrator m_eventRegistrator;


        private readonly Dictionary<KeyValuePair<Type, String>, HashSet<KeyValuePair<Type, String>>> m_explicitConnections;
        private readonly Dictionary<KeyValuePair<Type, String>, HashSet<KeyValuePair<Type, String>>> m_explicitDisconnections;
        private readonly ViewCollection m_viewCollectoin;
        private readonly IWindowManager m_windowManager;
        private readonly IEnumerable<IWindowViewContainer> m_windowCollection;

        //private readonly Dictionary<KeyValuePair<Type, String>, HashSet<KeyValuePair<Type, String>>> m_implicitConnections;

       
        internal ConfigModel(EventRegistrator eventRegistrator_, IEnumerable<IWindowViewContainer> windowCollection_, IWindowManager windowManager_)
        {
            m_eventRegistrator = eventRegistrator_;

			m_publishs = new Dictionary<IWindowViewContainer, HashSet<Type>>();
			m_subscriptions = new Dictionary<IWindowViewContainer, HashSet<Type>>();
            m_explicitConnections = new Dictionary<KeyValuePair<Type, string>, HashSet<KeyValuePair<Type, string>>>();
            m_explicitDisconnections = new Dictionary<KeyValuePair<Type, string>, HashSet<KeyValuePair<Type, string>>>();
            //m_implicitConnections = new Dictionary<KeyValuePair<Type, string>, HashSet<KeyValuePair<Type, string>>>();
            
            m_windowManager = windowManager_;
			m_windowCollection = windowCollection_;
        }


       


        internal void InitConfigInfo()
        {
            m_publishs.Clear();
            m_subscriptions.Clear();
            //m_implicitConnections.Clear();
         
            foreach (var view in m_windowCollection)
            {
                m_publishs[view] = new HashSet<Type>();
                foreach (var t in m_eventRegistrator.GetPublishedTypes(view.ID))
                {
                    m_publishs[view].Add(t);
                }

                m_subscriptions[view] = new HashSet<Type>();
                foreach (var t in m_eventRegistrator.GetSubscribedTypes(view.ID))
                {
                    m_subscriptions[view].Add(t);
                }
            }


           /* var connection = from conn in m_eventRegistrator.GetConnections()
                             where GetViewFromID(conn.Key.Value) != null
                             from sub in conn.Value
                             where GetViewFromID(sub.Value) != null
                             select new Tuple<Type, string, Type, string>(conn.Key.Key, conn.Key.Value, sub.Key, sub.Value);
            var exp = from c in m_explicitConnections
                      from sub in c.Value
                      select new Tuple<Type, string, Type, string>(c.Key.Key, c.Key.Value, sub.Key, sub.Value);

            foreach (var conn in connection.Except(exp))
            {
                HashSet<KeyValuePair<Type, String>> subs;
                var pub = new KeyValuePair<Type, string>(conn.Item1, conn.Item2);
                m_implicitConnections.TryGetValue(pub, out subs);
                if (subs == null)
                {
                    subs = new HashSet<KeyValuePair<Type, string>>();
                    m_implicitConnections[pub] = subs;
                }
                m_implicitConnections[pub].Add(new KeyValuePair<Type, string>(conn.Item3, conn.Item4));
            }*/
            
        }

        private Dictionary<KeyValuePair<Type, String>, HashSet<KeyValuePair<Type, String>>> GetImplicitConnections()
        {
            var implicitConnections =
                new Dictionary<KeyValuePair<Type, String>, HashSet<KeyValuePair<Type, String>>>();
            var connection = from conn in m_eventRegistrator.GetConnections()
                             where GetViewFromID(conn.Key.Value) != null
                             from sub in conn.Value
                             where GetViewFromID(sub.Value) != null
                             select new Tuple<Type, string, Type, string>(conn.Key.Key, conn.Key.Value, sub.Key, sub.Value);

            var exp = from c in m_explicitConnections
                      from sub in c.Value
                      select new Tuple<Type, string, Type, string>(c.Key.Key, c.Key.Value, sub.Key, sub.Value);

            var dis = from c in m_explicitDisconnections
                      from sub in c.Value
                      select new Tuple<Type, string, Type, string>(c.Key.Key, c.Key.Value, sub.Key, sub.Value);

            foreach (var conn in connection.Except(exp).Except(dis))
            {
                HashSet<KeyValuePair<Type, String>> subs;
                var pub = new KeyValuePair<Type, string>(conn.Item1, conn.Item2);
                implicitConnections.TryGetValue(pub, out subs);
                if (subs == null)
                {
                    subs = new HashSet<KeyValuePair<Type, string>>();
                    implicitConnections[pub] = subs;
                }
                implicitConnections[pub].Add(new KeyValuePair<Type, string>(conn.Item3, conn.Item4));
            }
            return implicitConnections;
        }

        internal bool IsExplicitConnected(IWindowViewContainer view1, IWindowViewContainer view2)
        {
            return ExplicitPublishTo(view1).Contains(view2) || ExplicitSubscribeFrom(view1).Contains(view2);
        }

        internal bool IsExplicitlyDisconnected(IWindowViewContainer view1, IWindowViewContainer view2)
        {
            var dis = (from d in m_explicitDisconnections
                       where d.Key.Value == view1.ID
                       from sub in d.Value
                       where sub.Value == view2.ID
                       select true).Count() > 0;
            if (dis)
                return true;


            dis = (from d in m_explicitDisconnections
                   where d.Key.Value == view2.ID
                   from sub in d.Value
                   where sub.Value == view1.ID
                   select true).Count() > 0;

            return dis;
        }


        internal bool IsImplicitConnected(IWindowViewContainer view1, IWindowViewContainer view2)
        {
            return ImplicitPublishTo(view1).Contains(view2) || ImplicitSubscribedFrom(view1).Contains(view2);
        }

       /* internal bool IsExplicitConnected(Type sourceType_, String sourceView_, Type targetType_, String targetView_)
        {
            var key = new KeyValuePair<Type, string>(sourceType_, sourceView_);
            if (!m_explicitConnections.ContainsKey(key))
            {
                return false;            
            }
            else
            {
                return m_explicitConnections[key].Contains(new KeyValuePair<Type, string>(targetType_, targetView_));
            }            
        }
*/
        

        internal IEnumerable<IWindowViewContainer> ExplicitPublishTo(IWindowViewContainer subscriber_)
        {

            var source = from conn in m_explicitConnections
                         from tp in conn.Value
                         where tp.Value == subscriber_.ID
                         select conn.Key.Value;

            return from id in source.Distinct()
                   select GetViewFromID(id);
        }

        internal IEnumerable<IWindowViewContainer> ImplicitPublishTo(IWindowViewContainer subscriber_)
        {
            var source = from conn in GetImplicitConnections()
                         from tp in conn.Value
                         where tp.Value == subscriber_.ID
                         select conn.Key.Value;

            return from id in source.Distinct()
                   select GetViewFromID(id);
        }

        internal IEnumerable<IWindowViewContainer> ExplicitSubscribeFrom(IWindowViewContainer publisher_)
        {
            var target = from conn in m_explicitConnections
                         where conn.Key.Value == publisher_.ID
                         from tp in conn.Value
                         select tp.Value;

            return from id in target.Distinct()
                   select GetViewFromID(id);
        }

        internal IEnumerable<IWindowViewContainer> ImplicitSubscribedFrom(IWindowViewContainer publisher_)
        {
            var target = from conn in GetImplicitConnections()
                         where conn.Key.Value == publisher_.ID
                         from tp in conn.Value
                         select tp.Value;

            return from id in target.Distinct()
                   select GetViewFromID(id);
        }
       

        internal IEnumerable<IWindowViewContainer> GetViewCollection()
        {
            return m_windowCollection;
        }

        internal IEnumerable<KeyValuePair<Type, String>> GetCompatibleSubscribers(Type subType_)
        {
            
            MethodInfo method = m_eventRegistrator.GetType().GetMethod("GetCompatibleSubscribersWithType");
            method = method.MakeGenericMethod(new Type[] { subType_ });
            return (IEnumerable<KeyValuePair<Type, String>>)method.Invoke(m_eventRegistrator, new object[] { });
        }

        internal IEnumerable<IWindowViewContainer> GetCompatibleSubscribers(IWindowViewContainer publisher_)
        {
            HashSet<Type> ts;
            if (!m_publishs.TryGetValue(publisher_, out ts))
                return Enumerable.Empty<IWindowViewContainer>();

            var res = from pt in ts
                      from sub in m_subscriptions
                      from st in sub.Value
                      where m_eventRegistrator.AdapterService.IsAdaptationPossible(pt, st)
                      select sub.Key;

            return res.Distinct();
        }

     

        internal IEnumerable<KeyValuePair<Type, String>> GetCompatiblePublishers(Type pubType_)
        {
            MethodInfo method = m_eventRegistrator.GetType().GetMethod("GetCompatiblePublishersWithType");
            method = method.MakeGenericMethod(new Type[] { pubType_ });
            return (IEnumerable<KeyValuePair<Type, String>>)method.Invoke(m_eventRegistrator, new object[] { });
        }

        internal IEnumerable<IWindowViewContainer> GetCompatiblePublishers(IWindowViewContainer subscriber_)
        {
            HashSet<Type> ts;
            if (!m_subscriptions.TryGetValue(subscriber_, out ts))
            {
                return Enumerable.Empty<IWindowViewContainer>();
            }

            var res = from st in ts
                      from pub in m_publishs
                      from pt in pub.Value
                      where m_eventRegistrator.AdapterService.IsAdaptationPossible(pt, st)
                      select pub.Key;

            return res.Distinct();
        }

       
        private void ReigsterExplicitPublish(Type sourceType_, String sourceView_, Type targetType_, String targetView_)
        {            
            KeyValuePair<Type, String> s = new KeyValuePair<Type, string>(sourceType_, sourceView_);

            if (!m_explicitConnections.ContainsKey(s)) 
            {
                m_explicitConnections[s] = new HashSet<KeyValuePair<Type, string>>();
            }
            m_explicitConnections[s].Add(new KeyValuePair<Type, string>(targetType_, targetView_));
        }

        private void ReigsterExplicitNonPublish(Type sourceType_, String sourceView_, Type targetType_, String targetView_)
        {
            KeyValuePair<Type, String> s = new KeyValuePair<Type, string>(sourceType_, sourceView_);

            if (!m_explicitDisconnections.ContainsKey(s))
            {
                m_explicitDisconnections[s] = new HashSet<KeyValuePair<Type, string>>();
            }
            m_explicitDisconnections[s].Add(new KeyValuePair<Type, string>(targetType_, targetView_));
        }
/*
        internal void ReigsterImplicitConnection(Type sourceType_, String sourceView_, Type targetType_, String targetView_)
        {
            KeyValuePair<Type, String> s = new KeyValuePair<Type, string>(sourceType_, sourceView_);

            if (!m_implicitConnections.ContainsKey(s))
            {
                m_explicitDisconnections[s] = new HashSet<KeyValuePair<Type, string>>();
            }
            m_implicitConnections[s].Add(new KeyValuePair<Type, string>(targetType_, targetView_));
        }

        internal void ReigsterImplicitConnection(IWindowViewContainer view1, IWindowViewContainer view2)
        {
            var ts = from pt in m_eventRegistrator.GetPublishedTypes(view1.ID)
                     from st in m_eventRegistrator.GetSubscribedTypes(view2.ID)
                     where m_eventRegistrator.AdapterService.IsAdaptationPossible(pt, st)
                     select new Tuple<Type, string, Type, string>(pt, view1.ID, st, view2.ID);

            foreach (var conn in ts)
            {
                this.ReigsterImplicitConnection(conn.Item1, conn.Item2, conn.Item3, conn.Item4);
            }

            ts = from pt in m_eventRegistrator.GetPublishedTypes(view2.ID)
                 from st in m_eventRegistrator.GetSubscribedTypes(view1.ID)
                 where m_eventRegistrator.AdapterService.IsAdaptationPossible(pt, st)
                 select new Tuple<Type, string, Type, string>(pt, view2.ID, st, view1.ID);

            foreach (var conn in ts)
            {
                this.ReigsterImplicitConnection(conn.Item1, conn.Item2, conn.Item3, conn.Item4);
            }
        }*/

        internal void ReigsterExplicitConnection(IWindowViewContainer view1, IWindowViewContainer view2)
        {
            var ts = from pt in m_eventRegistrator.GetPublishedTypes(view1.ID)
                     from st in m_eventRegistrator.GetSubscribedTypes(view2.ID)
                     where m_eventRegistrator.AdapterService.IsAdaptationPossible(pt, st)
                     select new Tuple<Type, string, Type, string>(pt, view1.ID, st,view2.ID);

            foreach (var conn in ts)
            {
                this.ReigsterExplicitPublish(conn.Item1, conn.Item2, conn.Item3, conn.Item4);
            }

            ts = from pt in m_eventRegistrator.GetPublishedTypes(view2.ID)
                     from st in m_eventRegistrator.GetSubscribedTypes(view1.ID)
                     where m_eventRegistrator.AdapterService.IsAdaptationPossible(pt, st)
                     select new Tuple<Type, string, Type, string>(pt, view2.ID, st, view1.ID);

            foreach (var conn in ts)
            {
                this.ReigsterExplicitPublish(conn.Item1, conn.Item2, conn.Item3, conn.Item4);
            }
        }

        internal void ReigsterExplicitDisconnection(IWindowViewContainer view1, IWindowViewContainer view2)
        {
            var conn = from pubType in m_eventRegistrator.GetPublishedTypes(view1.ID)
                       from subType in m_eventRegistrator.GetSubscribedTypes(view2.ID)
                       where m_eventRegistrator.AdapterService.IsAdaptationPossible(pubType, subType)
                       select new Tuple<Type, Type>(pubType, subType);

            foreach (var connection in conn)
            {
                this.ReigsterExplicitNonPublish(connection.Item1, view1.ID, connection.Item2, view2.ID);
            }

            conn = from pubType in m_eventRegistrator.GetPublishedTypes(view2)
                   from subType in m_eventRegistrator.GetSubscribedTypes(view1)
                   where m_eventRegistrator.AdapterService.IsAdaptationPossible(pubType, subType)
                   select new Tuple<Type, Type>(pubType, subType);

            foreach (var connection in conn)
            {
                this.ReigsterExplicitNonPublish(connection.Item1, view2.ID, connection.Item2, view1.ID);
            }
        }

        internal void UnreigsterExplicitDisconnection(IWindowViewContainer view1, IWindowViewContainer view2)
        {
            var conn = from pubType in m_eventRegistrator.GetPublishedTypes(view1)
                       from subType in m_eventRegistrator.GetSubscribedTypes(view2)
                       where m_eventRegistrator.AdapterService.IsAdaptationPossible(pubType, subType)
                       select new Tuple<Type, Type>(pubType, subType);

            foreach (var connection in conn)
            {
                this.UnreigsterExplicitNonPublish(connection.Item1, view1.ID, connection.Item2, view2.ID);
            }

            conn = from pubType in m_eventRegistrator.GetPublishedTypes(view2)
                   from subType in m_eventRegistrator.GetSubscribedTypes(view1)
                   where m_eventRegistrator.AdapterService.IsAdaptationPossible(pubType, subType)
                   select new Tuple<Type, Type>(pubType, subType);

            foreach (var connection in conn)
            {
                this.UnreigsterExplicitNonPublish(connection.Item1, view2.ID, connection.Item2, view1.ID);
            }
        }

        internal void UnreigsterExplicitConnection(IWindowViewContainer view1, IWindowViewContainer view2)
        {
            var ts = from pt in m_eventRegistrator.GetPublishedTypes(view1.ID)
                     from st in m_eventRegistrator.GetSubscribedTypes(view2.ID)
                     where m_eventRegistrator.AdapterService.IsAdaptationPossible(pt, st)
                     select new Tuple<Type, string, Type, string>(pt, view1.ID, st, view2.ID);

            foreach (var conn in ts)
            {
                this.UnreigsterExplicitPublish(conn.Item1, conn.Item2, conn.Item3, conn.Item4);
            }

            ts = from pt in m_eventRegistrator.GetPublishedTypes(view2.ID)
                 from st in m_eventRegistrator.GetSubscribedTypes(view1.ID)
                 where m_eventRegistrator.AdapterService.IsAdaptationPossible(pt, st)
                 select new Tuple<Type, string, Type, string>(pt, view2.ID, st, view1.ID);

            foreach (var conn in ts)
            {
                this.UnreigsterExplicitPublish(conn.Item1, conn.Item2, conn.Item3, conn.Item4);
            }
        }

        private void UnreigsterExplicitPublish(Type sourceType_, String sourceView_, Type targetType_, String targetView_)
        {
            KeyValuePair<Type, String> s = new KeyValuePair<Type, string>(sourceType_, sourceView_);

            if (!m_explicitConnections.ContainsKey(s))
            {
                return;
            }
            m_explicitConnections[s].Remove(new KeyValuePair<Type, string>(targetType_, targetView_));
        }


        internal void UnreigsterExplicitNonPublish(Type sourceType_, String sourceView_, Type targetType_, String targetView_)
        {
            KeyValuePair<Type, String> s = new KeyValuePair<Type, string>(sourceType_, sourceView_);

            if (!m_explicitDisconnections.ContainsKey(s))
            {
                return;
            }
            m_explicitDisconnections[s].Remove(new KeyValuePair<Type, string>(targetType_, targetView_));
        }
     

        
        public IWindowViewContainer GetViewFromID(String id_)
        {
            foreach (var view in m_windowCollection)
            {
                if (view.ID.Equals(id_))
                {
                    return view;
                }
            }
            return null;
        }


        

        internal void ApplyChanges()
        {

           
            /*
            foreach (var con in m_eventRegistrator.GetConnections())
            {
                var source = con.Key;

                foreach (var target in con.Value)
                {  
                    var method = m_eventRegistrator.GetType().GetMethod("Disconnect", new Type[] { typeof(IViewContainerBase), typeof(IViewContainerBase) });
                    method = method.MakeGenericMethod(new Type[] { source.Key, target.Key });
                    var sourceView = this.GetViewFromID(source.Value);
                    var targetView = this.GetViewFromID(target.Value);
                    if(sourceView != null && targetView != null)  // config model should only work on views, not modules; so, skip the modules
                        method.Invoke(m_eventRegistrator, new IViewContainerBase[] { sourceView, targetView });
                }
            }*/

            foreach (var con in m_explicitConnections)
            {
                var source = con.Key;
                foreach (var target in con.Value)
                {
                    var method = m_eventRegistrator.GetType().GetMethod("Connect", new Type[] { typeof(IViewContainerBase), typeof(IViewContainerBase) });
                    method = method.MakeGenericMethod(new Type[] { source.Key, target.Key });
                    var sourceView = this.GetViewFromID(source.Value);
                    var targetView = this.GetViewFromID(target.Value);
                    Debug.Assert(sourceView != null && targetView != null);  // config model should only work on views, not modules
                    method.Invoke(m_eventRegistrator, new IViewContainerBase[] { sourceView, targetView });
                }
            }

          /*  foreach (var con in m_implicitConnections)
            {
                var source = con.Key;
                foreach (var target in con.Value)
                {
                    var method = m_eventRegistrator.GetType().GetMethod("Connect", new Type[] { typeof(IViewContainerBase), typeof(IViewContainerBase) });
                    method = method.MakeGenericMethod(new Type[] { source.Key, target.Key });
                    var sourceView = this.GetViewFromID(source.Value);
                    var targetView = this.GetViewFromID(target.Value);
                    Debug.Assert(sourceView != null && targetView != null);  // config model should only work on views, not modules
                    method.Invoke(m_eventRegistrator, new IViewContainerBase[] { sourceView, targetView });
                }
            }*/

            foreach (var con in m_explicitDisconnections)
            {
                var source = con.Key;

                foreach (var target in con.Value)
                {
                    var method = m_eventRegistrator.GetType().GetMethod("Disconnect", new Type[] { typeof(IViewContainerBase), typeof(IViewContainerBase) });
                    method = method.MakeGenericMethod(new Type[] { source.Key, target.Key });
                    var sourceView = this.GetViewFromID(source.Value);
                    var targetView = this.GetViewFromID(target.Value);
                    if (sourceView != null && targetView != null)  // config model should only work on views, not modules; so, skip the modules
                        method.Invoke(m_eventRegistrator, new IViewContainerBase[] { sourceView, targetView });
                }
            }
        }



        #region Autohook

        
      


        internal void AutoHook(IWindowViewContainer view, IEnumerable<IWindowViewContainer> others)
        {
            AutoHookPublishing(view, others);
            AutoHookSubscribing(view, others);
        }




        private void AutoHookPublishing(IWindowViewContainer publisher, IEnumerable<IWindowViewContainer> others)
        {
            var hookedViews = new HashSet<IWindowViewContainer>();
            var pubTypes = m_eventRegistrator.GetPublishedTypes(publisher.ID);
            foreach (var subscriber in others)
            {
                var subTypes = m_eventRegistrator.GetSubscribedTypes(subscriber.ID);
                var conn = from pt in pubTypes
                           from st in subTypes
                           where m_eventRegistrator.AdapterService.IsAdaptationPossible(pt, st)
                           select new Tuple<Type, Type>(pt, st);

                foreach (var typePaire in conn)
                {
                    var methodInfo = m_eventRegistrator.GetType().GetMethod("Connect", new Type[]{typeof(string), typeof(string)});
                    HashSet<KeyValuePair<Type, string>> conns;
                    m_explicitDisconnections.TryGetValue(new KeyValuePair<Type, string>(typePaire.Item1, publisher.ID),
                                                         out conns);
                    if (conns==null || !conns.Contains(new KeyValuePair<Type, string>(typePaire.Item2, subscriber.ID)))
                    {
                       // RegisterImplicitConnection(typePaire.Item1, publisher.ID, typePaire.Item2, subscriber.ID);
                        var method = methodInfo.MakeGenericMethod(new Type[] {typePaire.Item1, typePaire.Item2});
                        method.Invoke(m_eventRegistrator, new string[] {publisher.ID, subscriber.ID});
                    }
                    
                }
            }
        }

        private void AutoHookSubscribing(IWindowViewContainer subscriber, IEnumerable<IWindowViewContainer> others)
        {
            var subTypes = m_eventRegistrator.GetSubscribedTypes(subscriber.ID);


            foreach (var publisher in others)
            {
                var pubTypes = m_eventRegistrator.GetPublishedTypes(publisher);
                var conn = from pt in pubTypes
                           from st in subTypes
                           where m_eventRegistrator.AdapterService.IsAdaptationPossible(pt, st)
                           select new Tuple<Type, Type>(pt, st);

                foreach (var typePaire in conn)
                {
                    var methodInfo = m_eventRegistrator.GetType().GetMethod("Connect", new Type[] { typeof(string), typeof(string) });
                    HashSet<KeyValuePair<Type, string>> conns;
                    m_explicitDisconnections.TryGetValue(new KeyValuePair<Type, string>(typePaire.Item1, publisher.ID),
                                                         out conns);

                    if (conns==null || !conns.Contains(new KeyValuePair<Type, string>(typePaire.Item2, subscriber.ID)))
                    {
                       // RegisterImplicitConnection(typePaire.Item1, publisher.ID, typePaire.Item2, subscriber.ID);
                        var method = methodInfo.MakeGenericMethod(new Type[] { typePaire.Item1, typePaire.Item2 });
                        method.Invoke(m_eventRegistrator, new string[] { publisher.ID, subscriber.ID });
                    }
                }
            }
        }

        internal void AutoUnhook(IWindowViewContainer view, IEnumerable<IWindowViewContainer> others)
        {
            AutoUnhookPublishing(view, others);
            AutoUnhookSubscribing(view, others);
        }

        private void AutoUnhookPublishing(IWindowViewContainer publisher, IEnumerable<IWindowViewContainer> others)
        {

            var connections = from conn in m_eventRegistrator.GetConnections()
                              where conn.Key.Value == publisher.ID
                              from sub in conn.Value
                              where others.Contains(GetViewFromID(sub.Value))
                              select 
                                  new Tuple<Type, string, Type, string>(conn.Key.Key, conn.Key.Value, sub.Key, sub.Value);

            var expconnections = from exp in m_explicitConnections
                                 where exp.Key.Value == publisher.ID
                                 from es in exp.Value
                                 where others.Contains(GetViewFromID(es.Value))
                                 select
                                     new Tuple<Type, string, Type, string>(exp.Key.Key, exp.Key.Value, es.Key, es.Value);

            var methodInfo = m_eventRegistrator.GetType().GetMethod("Disconnect",
                                                                    new Type[] {typeof (string), typeof (string)});
            foreach (var conn in connections.Except(expconnections))
            {
                var method = methodInfo.MakeGenericMethod(new Type[] {conn.Item1, conn.Item3});
                method.Invoke(m_eventRegistrator, new string[] {conn.Item2, conn.Item4});
            }

        }

        private void AutoUnhookSubscribing(IWindowViewContainer subscriber, IEnumerable<IWindowViewContainer> others)
        {
            var connections = from conn in m_eventRegistrator.GetConnections()
                              where others.Contains(GetViewFromID(conn.Key.Value))
                              from sub in conn.Value
                              where subscriber.ID == sub.Value
                              select
                                  new Tuple<Type, string, Type, string>(conn.Key.Key, conn.Key.Value, sub.Key, sub.Value);

            var expconnections = from exp in m_explicitConnections
                                 where others.Contains(GetViewFromID(exp.Key.Value))
                                 from es in exp.Value
                                 where es.Value == subscriber.ID
                                 select
                                     new Tuple<Type, string, Type, string>(exp.Key.Key, exp.Key.Value, es.Key, es.Value);


            var methodInfo = m_eventRegistrator.GetType().GetMethod("Disconnect",
                                                                    new Type[] { typeof(string), typeof(string) });
            foreach (var conn in connections.Except(expconnections))
            {
                var method = methodInfo.MakeGenericMethod(new Type[] { conn.Item1, conn.Item3 });
                method.Invoke(m_eventRegistrator, new string[] { conn.Item2, conn.Item4 });
            }
        }


        internal void DiscardOutdatedDisconnections(IWindowViewContainer source, IEnumerable<IWindowViewContainer> targets)
        {
            var discard = new HashSet<Tuple<Type, string, Type, string>>();
            foreach (var disconnection in m_explicitDisconnections)
            {
                var sid = disconnection.Key.Value;
                if(sid != source.ID)
                    continue;

                foreach (var sub in disconnection.Value)
                {
                    if (targets.Contains(GetViewFromID(sub.Value)))
                        discard.Add(new Tuple<Type, string, Type, string>(disconnection.Key.Key, disconnection.Key.Value,
                                                                          sub.Key, sub.Value));
                }
            }

            foreach (var disconnection in m_explicitDisconnections)
            {
                var tid = disconnection.Key.Value;
                if(!targets.Contains(GetViewFromID(tid)))
                    continue;

                foreach (var pub in disconnection.Value)
                {
                    if (pub.Value == source.ID)
                        discard.Add(new Tuple<Type, string, Type, string>(disconnection.Key.Key, disconnection.Key.Value,
                                                                          pub.Key, pub.Value));
                }
            }

            foreach (var tuple in discard)
            {
                var key = new KeyValuePair<Type, string>(tuple.Item1, tuple.Item2);
                var value = new KeyValuePair<Type, string>(tuple.Item3, tuple.Item4);
                m_explicitDisconnections[key].Remove(value);
            }
            
        }
       

       /* private void RegisterImplicitConnection(Type pubType, string pubId, Type subType, string subId)
        {
            var s = new KeyValuePair<Type, string>(pubType, pubId);

            if (!m_implicitConnections.ContainsKey(s))
            {
                m_implicitConnections[s] = new HashSet<KeyValuePair<Type, string>>();
            }
            m_implicitConnections[s].Add(new KeyValuePair<Type, string>(subType, subId));
        }*/

        #endregion


        #region persistence


        private void BuildConnectionXML(XElement root)
        {
            foreach (var con in m_explicitConnections)
            {
                var source = con.Key;
                var connection = new XElement("Connection",
                    new XAttribute("SourceType", source.Key.FullName),
                    new XAttribute("SourceAssembly", source.Key.Assembly.GetName().Name),
                    new XAttribute("SourceID", source.Value));

                foreach (var rec in con.Value)
                {
                    var receiver = new XElement("Receiver",
                        new XAttribute("TargetType", rec.Key.FullName),
                        new XAttribute("TargetAssembly", rec.Key.Assembly.GetName().Name),
                        new XAttribute("TargetID", rec.Value));
                    connection.Add(receiver);
                }
                root.Add(connection);
            }

            foreach (var discon in m_explicitDisconnections)
            {
                var source = discon.Key;
                var disconnection = new XElement("Disconnection",
                    new XAttribute("SourceType", source.Key.FullName),
                    new XAttribute("SourceAssembly", source.Key.Assembly.GetName().Name),
                    new XAttribute("SourceID", source.Value));

                foreach (var rec in discon.Value)
                {
                    var receiver = new XElement("Receiver",
                        new XAttribute("TargetType", rec.Key.FullName),
                        new XAttribute("TargetAssembly", rec.Key.Assembly.GetName().Name),
                        new XAttribute("TargetID", rec.Value));
                    disconnection.Add(receiver);
                }
                root.Add(disconnection);
                
            }
        }

        private void RestoreConnections(XElement root)
        {
           
            m_publishs.Clear();
            m_subscriptions.Clear();
            //m_implicitConnections.Clear();
            m_explicitConnections.Clear();
            m_explicitDisconnections.Clear();

            foreach (var con in root.Elements("Connection"))
            {
                var nm = con.Attribute("SourceType").Value;
                var asmName = con.Attribute("SourceAssembly").Value;
                var asm = Assembly.Load(asmName);
                var st = asm.GetType(nm);

                string sid = con.Attribute("SourceID").Value;
                foreach (var rec in con.Elements("Receiver"))
                {
                    asm = Assembly.Load(rec.Attribute("TargetAssembly").Value);
                    Type targetType = asm.GetType(rec.Attribute("TargetType").Value);
                    string tid = rec.Attribute("TargetID").Value;

                    this.ReigsterExplicitPublish(st, sid, targetType, tid);
                }
            }

            foreach (var con in root.Elements("Disconnection"))
            {
                var nm = con.Attribute("SourceType").Value;
                var asmName = con.Attribute("SourceAssembly").Value;
                var asm = Assembly.Load(asmName);
                var st = asm.GetType(nm);

                string sid = con.Attribute("SourceID").Value;
                foreach (var rec in con.Elements("Receiver"))
                {
                    asm = Assembly.Load(rec.Attribute("TargetAssembly").Value);
                    Type targetType = asm.GetType(rec.Attribute("TargetType").Value);
                    string tid = rec.Attribute("TargetID").Value;

                    this.ReigsterExplicitNonPublish(st, sid, targetType, tid);
                }
            }

            ApplyChanges();
        }
      
        void IPersistable.LoadState(System.Xml.Linq.XDocument state_)
        {
            XElement root = state_.Element("View2ViewConnection");
            RestoreConnections(root);
        }

        System.Xml.Linq.XDocument IPersistable.SaveState()
        {

            XDocument doc = new XDocument();
            XElement root = new XElement("View2ViewConnection");

            BuildConnectionXML(root);
            doc.Add(root);
            return doc;
        }


        string IPersistable.PersistorId
        {
            get { return typeof(ConfigModel).FullName; }
        }


        #endregion
       

       
    }
}
