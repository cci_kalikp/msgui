﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication
{
    /// <summary>
    /// Interaction logic for IPCViewToViewConfigUI.xaml
    /// </summary>
    public partial class IPCViewToViewConfigUI : UserControl
    {
        private IpcViewModel _ipcViewModel;
        public IPCViewToViewConfigUI()
        {
            InitializeComponent();
        }

         internal void Initialize(IWindowManager windowManager, IEnumerable<ViewMetaInfo> viewCollection, IpcLocalConnectionManager localConnectionManagerModel, IpcViewModel ipcViewModel)
        {
            //var localConnectionManagerModel = new localConnectionManagerModel(null, viewCollection, windowManager);
            localConnectionManagerModel.InitConfigInfo(viewCollection);
             _ipcViewModel = ipcViewModel;
            this.ViewSelector1.ItemsSource = _ipcViewModel.Applications;
            this.ViewSelector2.ItemsSource = _ipcViewModel.Applications;
            ipcViewModel.HookManager.Reset();
        }

        private void CanSetView1(object sender, CanExecuteRoutedEventArgs e)
        {
            var viewInfo = e.Parameter as IpcViewInfoModel;
            e.CanExecute= viewInfo != null;
        }

        private void SetView1(object sender, ExecutedRoutedEventArgs e)
        {
            var viewInfo = (IpcViewInfoModel)e.Parameter;
            _ipcViewModel.View1 = viewInfo.ViewInfo;
            _ipcViewModel.Application1 = IpcViewModel.GetAppInstanceName(viewInfo.ViewInfo.Application);
            _ipcViewModel.Tab1 = viewInfo.ViewInfo.TabName;
        }

/*
        private void AllowHook(object sender,RoutedEventArgs args)
        {
            var targetInfo = ((MenuItem) args.Source).CommandParameter as ViewInfoModel;
            if(targetInfo==null)
                throw new ApplicationException("Cannot get target view");
            var sourceView = _ipcViewModel.View1;
            var sourceTab = sourceView.TabName;
            var targetView = targetInfo.View;
            var targetTab = targetInfo.HostTab;

            _ipcViewModel.HookManager.RegisterHookup(sourceView, sourceTab, targetView, targetTab);
            _ipcViewModel.localConnectionManagerModel.UnreigsterExplicitConnection(sourceView, targetView);
            _ipcViewModel.localConnectionManagerModel.UnreigsterExplicitDisconnection(sourceView, targetView);


            _ipcViewModel.View1 = targetInfo.CommunicationModel.View1;
        }*/

        private void ExplicitConnect(object sender, RoutedEventArgs args)
        {
            var targetInfo = ((MenuItem)args.Source).CommandParameter as IpcViewInfoModel;
            if (targetInfo == null)
                throw new ApplicationException("Cannot get target view");

            var sourceView = _ipcViewModel.View1;
            var sourceTab = sourceView.TabName;
            var targetView = targetInfo.ViewInfo;
            var targetTab = targetInfo.ViewInfo.TabName;
            if (sourceView.IsInSameAppInstance(targetView))
            {
                _ipcViewModel.LocalConnectionManager.ReigsterExplicitConnection(sourceView, targetView);
                _ipcViewModel.LocalConnectionManager.UnreigsterExplicitDisconnection(sourceView, targetView);
                //_ipcViewModel.HookManager.UnregisterHookup(sourceView, sourceTab, targetView, targetTab);    
            }
            else
            {
                _ipcViewModel.ExternalConnectionManager.RegisterConnection(sourceView, targetView);
            }
            
            _ipcViewModel.View1 = sourceView;
        }

        private void ExplicitDisconnect(object sender, RoutedEventArgs args)
        {
            var targetViewInfo = ((MenuItem)args.Source).CommandParameter as IpcViewInfoModel;

            if (targetViewInfo == null)
                throw new ApplicationException("Cannot get target view");

            var targetInfo = targetViewInfo.ViewInfo;
            var sourceView = _ipcViewModel.View1;
            var sourceTab = sourceView.TabName;
            var targetView = targetInfo;
            var targetTab = targetInfo.TabName;
            if (sourceView.IsInSameAppInstance(targetView))
            {
                _ipcViewModel.LocalConnectionManager.ReigsterExplicitDisconnection(sourceView, targetView);
                _ipcViewModel.LocalConnectionManager.UnreigsterExplicitConnection(sourceView, targetView);    
            }
            else
            {
                _ipcViewModel.ExternalConnectionManager.UnregisterConnection(sourceView, targetView);
            }
            
            //_ipcViewModel.HookManager.UnregisterHookup(sourceView, sourceTab, targetView, targetTab);
            _ipcViewModel.View1 = sourceView;
        }


        private void AllowHook(object sender, RoutedEventArgs args)
        {
            var targetInfo = ((MenuItem)args.Source).CommandParameter as IpcViewInfoModel;
            if (targetInfo == null)
                throw new ApplicationException("Cannot get target view");
            var sourceView = _ipcViewModel.View1;
            var sourceTab = _ipcViewModel.Tab1;
            var targetView = targetInfo.ViewInfo;
            var targetTab = targetInfo.ViewInfo.TabName;

            _ipcViewModel.HookManager.RegisterHookup(sourceView.ViewContainer as IWindowViewContainer, sourceTab, 
                targetView.ViewContainer as IWindowViewContainer, targetTab);
            _ipcViewModel.LocalConnectionManager.UnreigsterExplicitConnection(sourceView, targetView);
            _ipcViewModel.LocalConnectionManager.UnreigsterExplicitDisconnection(sourceView, targetView);


            _ipcViewModel.View1 = targetInfo.IpcViewModel.View1;
        }

        private void CanConnect(object sender, CanExecuteRoutedEventArgs e)
        {
            var viewInfo = e.Parameter as ViewMetaInfo;
            if (viewInfo == null)
            {
                e.CanExecute = false;
                return;
            }

            if (_ipcViewModel.View1 == null)
            {
                e.CanExecute = false;
                return;
            }

            
            if (_ipcViewModel.LocalConnectionManager.GetCompatiblePublishers(viewInfo).Contains(_ipcViewModel.View1)
                || _ipcViewModel.LocalConnectionManager.GetCompatibleSubscribers(viewInfo).Contains(_ipcViewModel.View1))
            {
                e.CanExecute = true;
                return;
            }
            else
            {
                e.CanExecute = false;
                return;
            }

            //TODO
            e.CanExecute = true;

        }
      
        private void ApplyChanges(object sender, EventArgs eventArgs)
        {
            _ipcViewModel.LocalConnectionManager.ApplyChanges();
            _ipcViewModel.HookManager.UpdateHooks();
            _ipcViewModel.ExternalConnectionManager.ApplyChanges();
            var window = Window.GetWindow(this);
            if (window != null)
                window.Close();
        }

        private void Cancel(object sender, EventArgs eventArgs)
        {
            var window = Window.GetWindow(this);
            if(window!=null)
                window.Close();
        }
    }


   
}
