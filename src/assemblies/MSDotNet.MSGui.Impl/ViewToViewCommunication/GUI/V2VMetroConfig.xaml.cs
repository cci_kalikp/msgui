﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MorganStanley.MSDotNet.MSGui.Controls;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.MicrosoftShell.Standard;

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication.GUI
{
	/// <summary>
	/// Interaction logic for V2VMetroConfig.xaml
	/// </summary>
	public partial class V2VMetroConfig : UserControl
	{
		internal static V2VMetroConfig Instance = null;
		IChromeManager m_chromeManager;

	    private ApplicationInfo _thisApp;

        internal ApplicationInfo Application { get { return _thisApp; } }
		
		internal IpcViewModel Model;


		public V2VMetroConfig(IChromeManager chromeManager_, ApplicationInfo application)
		{
			Instance = this;
			this.m_chromeManager = chromeManager_;

			InitializeComponent();

			//this.Tiles.ItemsSource = this.m_tileList;
			this.TilesGrouped.ItemsSource = this.m_groupedTileList;
			this.SearchFilter = "";

		    _thisApp = application;
		}

		internal void Initialize(IWindowManager windowManager, IEnumerable<ViewMetaInfo> viewCollection,  IpcViewModel communicationViewModel)
		{
            communicationViewModel.LocalConnectionManager.InitConfigInfo(viewCollection);
            
			this.Model = communicationViewModel;
			foreach (var application in Model.Tabs)
			{
				var applicationTiles = new TabTilesDictionary(); 

			    var localViewState = communicationViewModel.LocalConnectionManager;
                var extApp = application.Key;
                var connectionState = _thisApp.Equals(extApp)
			                          ? communicationViewModel.LocalConnectionManager
			                          : communicationViewModel.ExternalConnectionManager as IConnectionState;


                foreach (var tab in application.Value)
				{
					var tabTiles = new List<IpcViewInfoModel>();

				    foreach (var window in tab.Views)
				    {
                        var view = window.ViewInfo;
                        window.WinForms = false;
                        if (view.ViewContainer!=null )//localViewState.GetCompatiblePublishers(view).Count() + localViewState.GetCompatibleSubscribers(view).Count() > 0)
                        {
                          
                            var viewContainer = view.ViewContainer as IWindowViewContainer;
                            if(viewContainer == null)
                                continue;

                            var dp = viewContainer.Content as DockPanel;
                            if (dp != null && dp.Children.Count > 0)
                            {
                                var afh = dp.Children[0] as MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl.AppletFormHost;
                                if (afh != null)
                                {
                                    window.WinForms = true;
                                    System.Drawing.Bitmap b = new System.Drawing.Bitmap(afh.Child.Width, afh.Child.Height);
                                    afh.Child.DrawToBitmap(b, new System.Drawing.Rectangle(0, 0, afh.Child.Width, afh.Child.Height));
                                    var brush = new ImageBrush(b.ToImageSource(true));
                                    window.Brush = brush; 
                                }
                            }

                        }

                        if (!window.WinForms && view.ScreenShot != null)
                        {

                            var bitmap = new System.Drawing.Bitmap(view.ScreenShot);
                            var bitmapSource = bitmap.ToImageSource(true);
                            window.Brush = new ImageBrush(bitmapSource);    
                        }

                        this.m_tileList.Add(window);
						tabTiles.Add(window);
				    }

					if (tabTiles.Count > 0)
					{
						applicationTiles[tab.Tab?? ""]=tabTiles;
					}
                    
				}

				this.m_groupedTileList.Add(application.Key, applicationTiles);
			}
		}

		private ApplicationTilesDictionary m_groupedTileList = new ApplicationTilesDictionary();

        private ObservableCollection<IpcViewInfoModel> m_tileList = new ObservableCollection<IpcViewInfoModel>();
        internal ObservableCollection<IpcViewInfoModel> TileList
		{
			get
			{
				return this.m_tileList;
			}
		}

		internal IpcViewInfoModel CurrentTile
		{
			get 
			{
				return (IpcViewInfoModel)GetValue(CurrentTileProperty); 
			}
			set 
			{
				var currentValue = GetValue(CurrentTileProperty) as IpcViewInfoModel;
				if (currentValue == value)
				{
					if (currentValue != null)
					{
						currentValue.SetValue(IsCurrentTileProperty, false);
					}

					SetValue(CurrentTileProperty, null);
				}
				else
				{
					if (currentValue != null)
					{
						currentValue.SetValue(IsCurrentTileProperty, false);
					}

					SetValue(CurrentTileProperty, value);

					if (value != null)
					{
						value.SetValue(IsCurrentTileProperty, true);
					}
				}
			}
		}

		internal static readonly DependencyProperty CurrentTileProperty = DependencyProperty.Register
		(
			"CurrentTile",
			typeof(IpcViewInfoModel), 
			typeof(V2VMetroConfig), 
			new UIPropertyMetadata(null)
		);

		public static readonly DependencyProperty IsCurrentTileProperty = DependencyProperty.RegisterAttached
		(
			"IsCurrentTile", 
			typeof(bool), 
			typeof(V2VMetroConfig), 
			new PropertyMetadata(false)
		);

		public static bool GetIsCurrentTile(UIElement element)
		{
			return (bool)element.GetValue(IsCurrentTileProperty);
		}

		public static void SetIsCurrentTile(UIElement element, bool value)
		{
			element.SetValue(IsCurrentTileProperty, value);
		}

		public string SearchFilter
		{
			get { return (string)GetValue(SearchFilterProperty); }
			set { SetValue(SearchFilterProperty, value); }
		}

		public static readonly DependencyProperty SearchFilterProperty =
			DependencyProperty.Register("SearchFilter", typeof(string), typeof(V2VMetroConfig), new UIPropertyMetadata(""));

		



		private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
		{
			var rect = sender as Rectangle;
			if (rect != null)
			{
				var tile = rect.Tag as IpcViewInfoModel;
				if (tile != null)
				{
					this.CurrentTile = tile;
				}
			}
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			var v = (e.Source as Button).Tag as IpcViewInfoModel;
           

            IConnectionState  connectionState;

            if (v.ViewInfo.IsInSameAppInstance(CurrentTile.ViewInfo))
            {
                connectionState = Model.LocalConnectionManager;

                if (connectionState.IsDisconnected(this.CurrentTile.ViewInfo, v.ViewInfo))
                {
                    Connect(this.CurrentTile, v);
                }
                else if (connectionState.IsConnected(v.ViewInfo, this.CurrentTile.ViewInfo))
                {
                    if (Model.HookManager.AllowHookup(v.ViewInfo.ViewContainer as IWindowViewContainer) &&
                        Model.HookManager.AllowHookup(this.CurrentTile.ViewInfo.ViewContainer as IWindowViewContainer))
                    {
                        Autoconnect(this.CurrentTile, v);
                    }
                    else
                    {
                        Disconnect(this.CurrentTile, v);
                    }

                }
                else
                {
                    Disconnect(this.CurrentTile, v);
                }
            }
            else
            {
                connectionState = Model.ExternalConnectionManager;

                if (connectionState.IsConnected(this.CurrentTile.ViewInfo, v.ViewInfo))
                {
                    Disconnect(CurrentTile, v);
                }
                else
                {
                    Connect(CurrentTile, v);
                }

            }

		    var t = this.CurrentTile;
			this.CurrentTile = null;
			this.CurrentTile = t;
			e.Handled = true;
		}

		private bool IsAutoConnectEnabled()
		{
			//TODO
			return true;
		}

		private bool CanConnect(IpcViewInfoModel v1, IpcViewInfoModel v2)
		{
		    var connectionState = v1.ViewInfo.IsInSameAppInstance(v2.ViewInfo)
		                              ? Model.LocalConnectionManager
		                              : Model.ExternalConnectionManager as IConnectionState;
		    return connectionState.CanPublishTo(v1.ViewInfo, v2.ViewInfo) ||
		           connectionState.CanPublishTo(v2.ViewInfo, v1.ViewInfo);
		}

		private void Connect(IpcViewInfoModel v1, IpcViewInfoModel v2)
		{
            if (v1.ViewInfo.IsInSameAppInstance(v2.ViewInfo))
            {
                this.Model.LocalConnectionManager.ReigsterExplicitConnection(v1.ViewInfo, v2.ViewInfo);
                this.Model.LocalConnectionManager.UnreigsterExplicitDisconnection(v1.ViewInfo, v2.ViewInfo);
                this.Model.HookManager.UnregisterHookup(v1.ViewInfo.ViewContainer as IWindowViewContainer, v1.ViewInfo.TabName,
                    v2.ViewInfo.ViewContainer as IWindowViewContainer, v2.ViewInfo.TabName);
            }
            else
            {   
                Model.ExternalConnectionManager.RegisterConnection(v1.ViewInfo, v2.ViewInfo);
            }
			
		}

        private void Disconnect(IpcViewInfoModel v1, IpcViewInfoModel v2)
		{
            if (v1.ViewInfo.IsInSameAppInstance(v2.ViewInfo))
            {

                this.Model.LocalConnectionManager.ReigsterExplicitDisconnection(v1.ViewInfo, v2.ViewInfo);
                this.Model.LocalConnectionManager.UnreigsterExplicitConnection(v1.ViewInfo, v2.ViewInfo);
                this.Model.HookManager.UnregisterHookup(v1.ViewInfo.ViewContainer as IWindowViewContainer, v1.ViewInfo.TabName,
                   v2.ViewInfo.ViewContainer as IWindowViewContainer, v2.ViewInfo.TabName);
            }
            else
            {
                Model.ExternalConnectionManager.UnregisterConnection(v1.ViewInfo, v2.ViewInfo);
            }
		}

        private void Autoconnect(IpcViewInfoModel v1, IpcViewInfoModel v2)
		{
            if (v1.ViewInfo.IsInSameAppInstance(v2.ViewInfo))
            {
                this.Model.LocalConnectionManager.UnreigsterExplicitDisconnection(v1.ViewInfo, v2.ViewInfo);
                this.Model.LocalConnectionManager.UnreigsterExplicitConnection(v1.ViewInfo, v2.ViewInfo);
                this.Model.HookManager.RegisterHookup(v1.ViewInfo.ViewContainer as IWindowViewContainer, v1.ViewInfo.TabName, 
                    v2.ViewInfo.ViewContainer as IWindowViewContainer, v2.ViewInfo.TabName);
            }
		}

		private void SaveButton_Click(object sender, RoutedEventArgs e)
		{
			this.Model.LocalConnectionManager.ApplyChanges();
            this.Model.ExternalConnectionManager.ApplyChanges();
			this.Model.HookManager.UpdateHooks();
			var window = Window.GetWindow(this);
			if (window != null)
				window.Close();
		}

		private void CancelButton_Click(object sender, RoutedEventArgs e)
		{
			var window = Window.GetWindow(this);
			if (window != null)
				window.Close();
		}
	}


	public class TileConverter : IMultiValueConverter
	{
		#region IMultiValueConverter Members

		public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			if ((parameter as string) == "TileVisibility")
			{
				var filter = V2VMetroConfig.Instance.SearchFilter.ToLower();
				var tabName = (values[1] as IpcViewInfoModel).ViewInfo.TabName ?? "";

				if (V2VMetroConfig.Instance.SearchFilter.Trim() == string.Empty ||
					(values[1] as IpcViewInfoModel).ViewInfo.Title.ToLower().Contains(filter) ||
					tabName.ToLower().Contains(filter) || 
					(values[1] as IpcViewInfoModel).ViewInfo.ApplicationName.ToLower().Contains(filter) || 
					(values[1] as IpcViewInfoModel).ViewInfo.Host.ToLower().Contains(filter))
				{
					if (values[0] == null)
					{
						return Visibility.Visible;
					}
					else if (values[1] == values[0])
					{
						return Visibility.Collapsed;
					}
					else if (values[1] != null)
					{
					    var view1 = (values[0] as IpcViewInfoModel).ViewInfo;

                        if (!view1.IsInAppliction(V2VMetroConfig.Instance.Application))
                            return Visibility.Collapsed;

                        var view2 = (values[1] as IpcViewInfoModel).ViewInfo;
					    var connectionState = view1.IsInSameAppInstance(view2)
					                              ? V2VMetroConfig.Instance.Model.LocalConnectionManager
					                              : V2VMetroConfig.Instance.Model.ExternalConnectionManager as IConnectionState;

                        if (connectionState.CanPublishTo(view1, view2) || connectionState.CanPublishTo(view2,view1))
						{
							return Visibility.Visible;
						}
					}
				}

				return Visibility.Collapsed;
			}
			else if ((parameter as string) == "TileColor")
			{
				if (values[0] == null)
				{
					return Colors.Transparent;
				}
				else if (values[1] == values[0])
				{
					return Colors.Transparent;
				}
				else if (values[1] != null)
				{
					var view1 = (values[0] as IpcViewInfoModel).ViewInfo;
                    
                    var view2 = (values[1] as IpcViewInfoModel).ViewInfo;

				    var connectionState = V2VMetroConfig.Instance.Model.LocalConnectionManager as IConnectionState;

                    if (connectionState.IsConnected(view1, view2))
					{
						return V2VMetroConfig.Instance.TilesGrouped.FindResource(MSGuiColors.V2VExplicitConnectedColorKey);// Color.FromRgb(0x86, 0x9A, 0x87); //green - explicit connected
					}
                    else if (connectionState.IsDisconnected(view1, view2))
					{
						return V2VMetroConfig.Instance.TilesGrouped.FindResource(MSGuiColors.V2VExplicitDisconnectedColorKey);// Color.FromRgb(0xFA, 0x5E, 0x41); //red - explicit disconnected
					}
                    else if (connectionState.IsImplicitConnected(view1, view2))
					{
						return V2VMetroConfig.Instance.TilesGrouped.FindResource(MSGuiColors.V2VAutoConnectedColorKey);// Color.FromRgb(0xA8, 0xB9, 0xD0); //light blue - auto, connected
					}
					else
					{
						return V2VMetroConfig.Instance.TilesGrouped.FindResource(MSGuiColors.V2VAutoNonConnectedColorKey);// Color.FromRgb(0x3D, 0x55, 0x73); //dark blue - auto, disconnected
					}
				}

				return Colors.White;
			}
			else if ((parameter as string) == "ConnectButtonContent")
			{
				if (values[0] == null)
				{
					return "n/a";
				}
				else if (values[1] == values[0])
				{
					return "n/a";
				}
				else if (values[1] != null)
				{
					var view1 = (values[0] as IpcViewInfoModel).ViewInfo;
					var view2 = (values[1] as IpcViewInfoModel).ViewInfo;
				    var connectionState = view1.IsInSameAppInstance(view2)
				                              ? V2VMetroConfig.Instance.Model.LocalConnectionManager
				                              : V2VMetroConfig.Instance.Model.ExternalConnectionManager as IConnectionState;
				    
                    if (connectionState.IsConnected(view1, view2))
					{
						return "Connected";
					}
                    else if (connectionState.IsDisconnected(view1, view2))
					{
						return "Disconnected";
					}
                    else if (view1.IsInSameAppInstance(view2))
					{
						return "Automatic";
					}
                    else if (connectionState.IsImplicitConnected(view1, view2))
                    {
                        return "Passive";
                    }
				}

				return Visibility.Collapsed;
			}
			else if ((parameter as string) == "ConnectButtonVisibility")
			{
				if (values[0] == null)
				{
					return Visibility.Collapsed;
				}
				else if (values[1] == values[0])
				{
					return Visibility.Collapsed;
				}
				else if (values[1] != null)
				{
                    var view1 = (values[0] as IpcViewInfoModel).ViewInfo;
                    var view2 = (values[1] as IpcViewInfoModel).ViewInfo;
                    var connectionState = view1.IsInSameAppInstance(view2)
                                              ? V2VMetroConfig.Instance.Model.LocalConnectionManager
                                              : V2VMetroConfig.Instance.Model.ExternalConnectionManager as IConnectionState;

                    if (connectionState.CanPublishTo(view1, view2) ||
                        connectionState.CanPublishTo(view2,view1))
					{
						return Visibility.Visible;
					}
				}

				return Visibility.Collapsed;
			}

			return null;
		}

		public object[] ConvertBack(object values, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
		{
			throw new NotImplementedException();
		}

		#endregion
	}

	public class TitleConverter : IValueConverter
	{

		#region IValueConverter Members

		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			switch (parameter as string)
			{
				case "AppName":
				{
					var app = value as ApplicationInfo;
					return app.Name.ToUpper();
				}
				case "MachineName":
				{
					var app = value as ApplicationInfo;
					return app.Host.ToUpper();
				}
			}

			return value;
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			throw new NotImplementedException();
		}

		#endregion
	}

	public class SearchBoxVisibilityConverter : IValueConverter
	{

		#region IValueConverter Members

		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			if (parameter as string == "SearchBox")
			{
				return V2VMetroConfig.Instance.SearchFilter != "" ? Visibility.Hidden : Visibility.Visible;
			}
			else if (parameter as string == "Watermark")
			{
				return V2VMetroConfig.Instance.SearchFilter == "" ? Visibility.Visible : Visibility.Hidden;
			}

			return false;
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			throw new NotImplementedException();
		}

		#endregion
	}

	/// <summary>
	/// Key: tab name
	/// Value: View metadata
	/// </summary>
	class TabTilesDictionary : Dictionary<string, List<IpcViewInfoModel>>
	{

	}

	/// <summary>
	/// Key: Application metadata
	/// Value: tiles per each tab in the application
	/// </summary>
	class ApplicationTilesDictionary : Dictionary<ApplicationInfo, TabTilesDictionary>
	{

	}
}
