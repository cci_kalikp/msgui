﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using MorganStanley.MSDotNet.MSGui.Core;
using System.ComponentModel;
using System.Linq.Expressions;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication
{
    public class TreeModel : CommunicationViewModelBase
    {
        public ObservableCollection<TreeModel> Views { get; internal set; }
		public IWindowViewContainer Content { get; private set; }

        public ObservableCollection<TypeInfoModel> PublishedTypes { get; private set; }
        public ObservableCollection<TypeInfoModel> SubscribedTypes { get; private set; }
        
        public bool IsRegistered 
		{ 
            get
            {   
                //return PublishedTypes.Count() > 0;
                return true;
            }
        }

        public TreeModel(IWindowViewContainer content_)
        {            
            Content = content_;

            PublishedTypes = new ObservableCollection<TypeInfoModel>();
            SubscribedTypes = new ObservableCollection<TypeInfoModel>();

            Views = new ObservableCollection<TreeModel>();
        }
    }
}
