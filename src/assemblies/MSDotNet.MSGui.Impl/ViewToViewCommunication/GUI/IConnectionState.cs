﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging;

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication
{
    internal interface IConnectionState
    {
        bool CanPublishTo(ViewMetaInfo publisher, ViewMetaInfo subscriber);
        bool IsConnected(ViewMetaInfo view1, ViewMetaInfo view2);
        bool IsImplicitConnected(ViewMetaInfo view1, ViewMetaInfo view2);
        bool IsDisconnected(ViewMetaInfo view1, ViewMetaInfo view2);
        bool HasPublishedTo(ViewMetaInfo publisher, ViewMetaInfo subscriber);
        bool HasSubscribedFrom(ViewMetaInfo subscriber, ViewMetaInfo publisher);
    }
}
