﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication
{

    public enum Direction { ThisAsSubscriber, ThisAsPublisher };

    public class ConnectionInfoModel : CommunicationViewModelBase
    {
        private IWindowViewContainer m_otherView;
        private readonly ConfigModel m_config;
        private readonly Type m_otherType;
        

        public ConnectionInfoModel(ConfigModel config_, IWindowViewContainer otherView_, Type otherType_, Direction direction_)
        {
            Guard.ArgumentNotNull(otherView_, "sourceView_");
            m_otherView = otherView_;
            m_config = config_;
            m_otherType = otherType_;
            m_direction = direction_;
        }


        private readonly Direction m_direction;
        public Direction Direction
        {
            get { return m_direction; }            
        }

        private bool _isConnected;
        public bool IsConnected
        {
            get
            {
                return _isConnected;
            }
            set
            {
                _isConnected = value;
                UpdateConnection(value);
                RaisePropertyChanged(() => IsConnected);
            }
        }

        private Type _type;
        public Type CompatibleType
        {
            get
            {
                return _type;
            }
            set
            {
                _type = value;
                RaisePropertyChanged(() => CompatibleType);
            }
        }

		private IWindowViewContainer _view;
		public IWindowViewContainer View
        {
            get
            {
                return _view;
            }
            set
            {
                _view = value;
                RaisePropertyChanged(() => View);
            }
        }

        private void UpdateConnection(bool connect)
        {
            switch (m_direction)
            {
                case Direction.ThisAsSubscriber:
                    if (connect)
                    {
                        m_config.ReigsterConnection(m_otherType, m_otherView.ID, CompatibleType, View.ID);
                    }
                    else
                    {
                        m_config.UnreigsterConnection(m_otherType, m_otherView.ID, CompatibleType, View.ID);
                    }
                    
                    break;

                case Direction.ThisAsPublisher:
                    if (connect)
                    {
                        m_config.ReigsterConnection(CompatibleType, View.ID, m_otherType, m_otherView.ID);
                    }
                    else
                    {
                        m_config.UnreigsterConnection(CompatibleType, View.ID, m_otherType, m_otherView.ID);
                    }
                    
                    break;
            }

        }
    }
}
