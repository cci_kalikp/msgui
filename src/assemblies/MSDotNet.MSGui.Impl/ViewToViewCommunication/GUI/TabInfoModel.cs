﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.View;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication
{
    internal class TabInfoModel 
    {
        private readonly ShellTabViewModel m_tabModel;
        private readonly ObservableCollection<ViewInfoModel> m_viewCollection = new ObservableCollection<ViewInfoModel>();
        

        private static readonly string FLOATINGWINDOWTITLE = "Floating Windows";

        /**
         * tab == null means it is a floating window.
         */
        internal TabInfoModel(ShellTabViewModel tab)
        {
            m_tabModel = tab;
        }

        internal bool isFloatingWindow
        {
            get { return m_tabModel == null; }
        }

        public  ObservableCollection<ViewInfoModel> Views
        {
            get { return m_viewCollection; }
        }

        internal ShellTabViewModel Tab {get { return m_tabModel; }}

        

        public string Title
        {
            get
            {
                return m_tabModel != null ? m_tabModel.Title : FLOATINGWINDOWTITLE;
            }
        }

    }
}
