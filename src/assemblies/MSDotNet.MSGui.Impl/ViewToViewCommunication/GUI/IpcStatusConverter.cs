﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Data;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication.GUI;

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication
{
    class IpcStatusConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var viewInfoModel = values[0] as IpcViewInfoModel;
            if (viewInfoModel == null)
                return null;
            var viewInfo = viewInfoModel.ViewInfo;
            var commVM = viewInfoModel.IpcViewModel as IpcViewModel;
            if (commVM == null)
            {
                return null;
            }

            var localConnection  = commVM.LocalConnectionManager as IConnectionState;
            var localViewState = commVM.LocalConnectionManager;
            var externalConnection = commVM.ExternalConnectionManager as IConnectionState;
            var autoConnection = commVM.HookManager;


            if (parameter.Equals("IsConnectable"))
            {
                return localViewState.GetCompatiblePublishers(viewInfo).Count() > 0 ||
                localViewState.GetCompatibleSubscribers(viewInfo).Count() > 0;
            }

            if (parameter.Equals("IsConnected"))
            {
                return localViewState.ExplicitPublishTo(viewInfo).Count() > 0 ||
                       localViewState.ExplicitSubscribeFrom(viewInfo).Count() > 0;
            }

            if (parameter.Equals("CanPublish"))
            {
                return localViewState.GetCompatibleSubscribers(viewInfo).Count() > 0;
            }

            if (parameter.Equals("HasPublished"))
            {
                return localViewState.ExplicitSubscribeFrom(viewInfo).Count() > 0;
            }

            if (parameter.Equals("CanSubscribe"))
            {
                return localViewState.GetCompatiblePublishers(viewInfo).Count() > 0;
            }


            if (parameter.Equals("HasSubscribed"))
            {
                return localViewState.ExplicitSubscribeFrom(viewInfo).Count() > 0;
            }

            if (values.Length < 2)
                return false;

            var targetView = viewInfo;
            
            var sourceView = values[1] as ViewMetaInfo;
            if (sourceView == null)
                return false;

            var connectionState = sourceView.IsInSameAppInstance(targetView)
                                      ? localConnection
                                      : externalConnection;

            
            if (parameter.Equals("IsConnectedToView1"))
            {
                return connectionState.IsConnected(sourceView, targetView);
            }

            if (parameter.Equals("IsNotExplicitlyConnectedToView1"))
            {
                return !connectionState.IsConnected(sourceView, targetView);
            }

            if (parameter.Equals("IsNotExplicitlyDisconnectedToView1"))
            {
                return !connectionState.IsDisconnected(sourceView, targetView);
            }


            if (parameter.Equals("IsImplicitlyConnectedToView1"))
            {
               // return connectionState.CanPublishTo(sourceView, targetView) ||
               //        connectionState.CanPublishTo(targetView, sourceView);

                if (!localViewState.GetCompatiblePublishers(sourceView).Contains(targetView) && !localViewState.GetCompatibleSubscribers(targetView).Contains(sourceView))
                    return false;

                return !connectionState.IsDisconnected(sourceView, targetView);

                //return config.IsImplicitConnected(viewInfo.View, view1);

                //return !config.IsExplicitlyDisconnected(viewInfo.View, view1) &&
                //      commVM.HookManager.CanHookTo(viewInfo.View, viewInfo.HostTab, commVM.Tab1);
            }

            if (parameter.Equals("MaySubscribeFromView1"))
            {
                return connectionState.CanPublishTo(sourceView, targetView);
            }

            if (parameter.Equals("MayPublishToView1"))
            {
                return connectionState.CanPublishTo(targetView, sourceView);
            }

            if (parameter.Equals("MayConnectToView1"))
            {
                return connectionState.CanPublishTo(sourceView, targetView) ||
                       connectionState.CanPublishTo(targetView, sourceView);
                        
            }

            if (parameter.Equals("HasPublishedToView1"))
            {

                return connectionState.HasSubscribedFrom(sourceView, targetView);
            }

            if (parameter.Equals("HasSubscribedFromView1"))
            {
                return connectionState.HasPublishedTo(sourceView, targetView);
            }

            if (parameter.Equals("CanHookToView1"))
            {
                if(sourceView.IsInSameAppInstance(targetView))
                    return autoConnection.CanHookTo(sourceView,targetView);
                return false;
            }

            return null;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

       
    }
}
