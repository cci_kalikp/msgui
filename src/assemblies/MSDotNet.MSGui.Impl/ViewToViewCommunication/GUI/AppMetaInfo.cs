﻿using System.Collections.Generic;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication
{
    internal class AppMetaInfo
    {
        internal AppMetaInfo(ApplicationInfo application)
        {
            Application = application;
        }
        public ApplicationInfo Application { get; private set; }
        public List<TabMetaInfo> Tabs { get; set; }
    }
}
