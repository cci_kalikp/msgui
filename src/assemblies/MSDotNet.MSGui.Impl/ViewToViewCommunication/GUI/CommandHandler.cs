﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Controls;

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication
{
    class CommandHandler
    {
        public static RoutedUICommand SetView1 = new RoutedUICommand("Set View1", "SetView1", typeof(CommandHandler));
        public static RoutedUICommand Connect = new RoutedUICommand("Connect Views", "Connect", typeof(CommandHandler));
        public static RoutedUICommand DisConnect = new RoutedUICommand("DisConnect Views", "DisConnect", typeof(CommandHandler));
        

        public static DependencyProperty CommandProperty = 
            DependencyProperty.RegisterAttached("Command", 
            typeof(ICommand),
            typeof(CommandHandler), 
            new UIPropertyMetadata(CommandChanged)); 

        public static DependencyProperty CommandParameterProperty = 
            DependencyProperty.RegisterAttached("CommandParameter",
            typeof(object), typeof(CommandHandler), 
            new UIPropertyMetadata(null)); 
        
        public static void SetCommand(DependencyObject target, ICommand value) { target.SetValue(CommandProperty, value); }      
        
        public static void SetCommandParameter(DependencyObject target, object value) { target.SetValue(CommandParameterProperty, value); }     
        
        public static object GetCommandParameter(DependencyObject target) { return target.GetValue(CommandParameterProperty); }      
        
        private static void CommandChanged(DependencyObject target, DependencyPropertyChangedEventArgs e) {
            Control control = target as Control;
            if (control != null) 
            {
                if ((e.NewValue != null) && (e.OldValue == null))
                {
                    control.PreviewMouseLeftButtonDown += OnPreviewMouseLeftDown;
                    
                    //control.MouseDoubleClick += OnPreviewMouseLeftDown;                    
                } 
                else if ((e.NewValue == null) && (e.OldValue != null))
                {
                    control.PreviewMouseLeftButtonDown -= OnPreviewMouseLeftDown;

                    //control.MouseDoubleClick -= OnPreviewMouseLeftDown;
                } 
            } 
        }      
        
        private static void OnPreviewMouseLeftDown(object sender, RoutedEventArgs e) 
        { 
            Control control = sender as Control; 
            ICommand command = (ICommand)control.GetValue(CommandProperty);
            object commandParameter = control.GetValue(CommandParameterProperty); 
            command.Execute(commandParameter); 
        } 
    }
}
