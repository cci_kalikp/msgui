﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication
{
    internal class IpcViewModel : CommunicationViewModelBase
    {
        //private readonly ConfigModel m_configModel;
        private readonly Dictionary<ApplicationInfo, List<TabMetaInfo>> _tabs = new Dictionary<ApplicationInfo, List<TabMetaInfo>>();
        private readonly IpcLocalConnectionManager _localConnectionManager;
        private readonly ExternalConnectionManager _externalConnectionManager;
        private readonly AutoConnectionManager _hookManager;


        

        private readonly ApplicationInfo _application;
        
        
        internal IpcViewModel(ApplicationInfo applicationInfo, IWindowManager windowManger,
            IEnumerable<IWindowViewContainer> viewCollection, IEnumerable<ViewMetaInfo> externalViews,
            IpcLocalConnectionManager localConnectionManager, AutoConnectionManager hookManager,
            ExternalConnectionManager externalConnectionManager)
        {
            _localConnectionManager = localConnectionManager;
            _hookManager = hookManager;
            _application = applicationInfo;

            
            _tabs[applicationInfo] = new List<TabMetaInfo>();

            
            LoadTabbedViews(windowManger);
            LoadFloatingViews(viewCollection);
            LoadExternalViews(externalViews);
            _externalConnectionManager = externalConnectionManager;
        }

        //internal List<TabInfoModel> Tabs { get { return _tabs; } }
        
        internal Dictionary<ApplicationInfo,List<TabMetaInfo>> Tabs {get { return _tabs; }}
        internal IEnumerable<AppMetaInfo> Applications
        {
            get
            {
                 return from app in _tabs.Keys
                          select new AppMetaInfo(app) { Tabs = _tabs[app] };
            }
        }


        internal static string GetAppInstanceName(ApplicationInfo applicationInfo)
        {
            return applicationInfo.Name + ": " + applicationInfo.Instance;
        }

       

        private void LoadTabbedViews(IWindowManager windowManager)
        {
            var manager = windowManager as TabbedDockViewModel;
            if (manager == null)
                return;

            
            foreach (var tab in manager.GetTabs())
            {

                var tabInfo = new TabMetaInfo(){Tab = tab.Title};
                _tabs[_application].Add(tabInfo);
                
            }

            foreach (var ws in manager.Workspaces)
            {

                var tab = ws.Host as ShellTabViewModel;
                if (tab == null)
                    continue;

                var tabInfo = (from t in _tabs[_application]
                               where t.Tab == tab.Title
                               select t).FirstOrDefault();

                if (tabInfo == null)
                {
                    throw new ApplicationException("Tab not in manager.Tabs collection");
                    //tabInfo = new TabInfoModel(tab, this);
                    //_tabs.Add(tabInfo);
                }
                var wvc = ws.ViewContainer as IWindowViewContainer;

                if (wvc == null)
                    return;
                var viewMeta = new ViewMetaInfo(_application,wvc.ID, wvc)
                                   {
                                       TabName = tabInfo.Tab,
                                       Title = wvc.Title
                                   };
                tabInfo.Views.Add(new IpcViewInfoModel(viewMeta, this));
            }

            
        }

        private void LoadFloatingViews(IEnumerable<IWindowViewContainer> viewCollection)
        {
            var thisApp = GetAppInstanceName(_application);
            var views = from view in viewCollection
                        where (from tab in _tabs[_application]
                                from vinfo in tab.Views
                                where vinfo.ViewInfo.LocalId == view.ID
                            select tab).FirstOrDefault() == null
                        select view;

            var floatingTab = (from tab in _tabs[_application]
                                  where tab.Tab == null
                                  select tab).FirstOrDefault();

            if(views.FirstOrDefault()==null)
                return;

            if (floatingTab == null)
            {
               floatingTab = new TabMetaInfo(){Tab = null};
               _tabs[_application].Add(floatingTab);
            }
            
            foreach (var view in views)
            {
                IViewCollectionContainer container2 = view as IViewCollectionContainer;
                if (container2 != null)
                {
                    foreach (var windowViewContainer in container2.Children)
                    {
                        var viewMeta = new ViewMetaInfo(_application, windowViewContainer.ID, windowViewContainer) { Title = windowViewContainer.Title };
                        floatingTab.Views.Add(new IpcViewInfoModel(viewMeta, this));
                    }
                }
                else
                {
                    var viewMeta = new ViewMetaInfo(_application, view.ID, view) { Title = view.Title };
                    floatingTab.Views.Add(new IpcViewInfoModel(viewMeta, this));
                }

            }
         
        }


        private void LoadExternalViews(IEnumerable<ViewMetaInfo> externalViews)
        {
            foreach (var view in externalViews)
            {
                 var viewApp = view.Application;
                if(!_tabs.ContainsKey(viewApp))
                    _tabs[viewApp] = new List<TabMetaInfo>();
                
                var tabName = view.TabName;
                var tabInfo = _tabs[viewApp].FirstOrDefault((tab) => tab.Tab == tabName);
                if (tabInfo == null)
                {
                    tabInfo = new TabMetaInfo() {Tab = view.TabName};
                    _tabs[viewApp].Add(tabInfo);
                }
                tabInfo.Views.Add(new IpcViewInfoModel(view, this));
            }
        }

        private readonly ObservableCollection<Type> _messageTypes1 = new ObservableCollection<Type>();
        internal ObservableCollection<Type> MessageTypes1 { get { return _messageTypes1; } }



        private readonly ObservableCollection<Type> _messageTypes2 = new ObservableCollection<Type>();
        internal ObservableCollection<Type> MessageTypes2 { get { return _messageTypes2; } }
        

        private ViewMetaInfo _view1;
        public ViewMetaInfo View1 
        {
            get { return _view1; }
            set 
            { 
                _view1 = value;
                RaisePropertyChanged(() =>View1);
            }
        }


        private string _tab1;
        public string Tab1
        {
            get { return _tab1; }
            set { _tab1 = value; RaisePropertyChanged(()=>Tab1); }
        }

        private string _aplication1;
        public string Application1
        {
            get { return _aplication1; }
            set { _aplication1 = value; RaisePropertyChanged(()=>Application1); }
        }


        internal int NumConnection(ViewMetaInfo view)
        {
            int pubTo = _localConnectionManager.ExplicitPublishTo(view).Count();
            int subFrom = _localConnectionManager.ExplicitSubscribeFrom(view).Count();

            return pubTo + subFrom;
        }

       



        internal enum ConnectionType
        {
            ImplicitConnected, ExplicitConnected, ExplicitDisconnected, Disconnected
        }

        internal IpcLocalConnectionManager LocalConnectionManager 
        {
            get { return _localConnectionManager; }
        }

        internal ExternalConnectionManager ExternalConnectionManager
        {
            get { return _externalConnectionManager; }
        }

        internal AutoConnectionManager HookManager { get { return _hookManager; } }

    }
}
