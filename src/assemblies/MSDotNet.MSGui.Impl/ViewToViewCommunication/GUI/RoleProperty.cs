﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication
{
    public class RoleProperty:DependencyObject
    {
        //public Boolean IsDrivingSelector
        //{
        //    get { return (Boolean)GetValue(IsDrivingSelectorProperty); }
        //    set { SetValue(IsDrivingSelectorProperty, value); }
        //}

        public static void SetIsDrivingSelector(UIElement element, Boolean value)
        {
            element.SetValue(IsDrivingSelectorProperty, value);
        }

        public static Boolean GetIsDrivingSelector(UIElement element)
        {
            return (Boolean) element.GetValue(IsDrivingSelectorProperty);
        }

        // Using a DependencyProperty as the backing store for IsDrivingSelector.  This enables animation, styling, binding, etc...
        public static DependencyProperty IsDrivingSelectorProperty =
            DependencyProperty.RegisterAttached("IsDrivingSelector", typeof(Boolean), typeof(RoleProperty));
    }
}
