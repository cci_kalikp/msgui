﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Data;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication
{
    internal class TabStatusConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
          
            var tab = values[0] as TabInfoModel;
          
            if (tab == null)
            {
                return null;
            }

            var config = tab.CommunicationModel.ConfigModel;

  
            if ("HasPubOrSub".Equals(parameter))
            {
                foreach (var view in tab.Views)
                {   
                    if (config.GetCompatibleSubscribers(view.View).FirstOrDefault() != null)
                    {
                        return true;
                    }
                    if (config.GetCompatiblePublishers(view.View).FirstOrDefault() != null)
                    {
                        return true;
                    }
                }
                return false;
            }
            
            if(values.Length<2)
                return null;

            var view1 = values[1] as IWindowViewContainer;
            if (view1 == null)
                return false;

            if ("HasCompatiblePubOrSub".Equals(parameter))
            {
                foreach (var view in tab.Views)
                {
                    if (config.GetCompatiblePublishers(view.View).Contains(view1))
                    {
                        return true;
                    }
                    if (config.GetCompatibleSubscribers(view.View).Contains(view1))
                    {
                        return true;
                    }
                }
                return false;
            }
            return null;

        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
