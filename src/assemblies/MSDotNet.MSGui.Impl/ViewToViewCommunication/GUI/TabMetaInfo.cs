﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication.GUI;

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication
{
    internal class TabMetaInfo 
    {
        private string _tab;
        public string Tab { get { return _tab; } set { _tab = value; }  }

        private readonly ObservableCollection<IpcViewInfoModel> _views = new ObservableCollection<IpcViewInfoModel>();
        public ObservableCollection<IpcViewInfoModel> Views { get { return _views; } }
    }
}
