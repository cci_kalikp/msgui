﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MorganStanley.MSDotNet.MSGui.Core;
using System.Collections;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication
{
    /// <summary>
    /// Interaction logic for ViewCommunicationConfig.xaml
    /// </summary>
    public partial class ViewCommunicationConfig : UserControl
    {
         public TreeModel Tree { get; private set; }
        ObservableWindowCollection m_windowCollection;
        ConfigModel m_config;
        public ViewCommunicationConfig(ConfigModel config_)
        {
            m_config = config_;
            m_windowCollection = config_.GetViewCollection();
            
            Tree = new TreeModel(null);
            
            InitializeComponent();
            //GenerateLogicalTree(null, null);
            this.logicTree.DataContext = Tree;
            
        }



		private IWindowViewContainer GetContainer(object view)
        {
            foreach (var c in m_windowCollection)
            {
                if (c.Content == view)
                {
                    return c;
                }
            }
            return null;
        }


        private List<TreeModel> BuildFromManageViews()
        {

            List<TreeModel> subtrees = new List<TreeModel>();

            foreach (var vc in m_windowCollection)
            {
                var child = new TreeModel(vc);
                if (m_config.m_publishs.ContainsKey(vc))
                {
                    foreach (var item in m_config.m_publishs[vc])
                    {
                        TypeInfoModel info = new TypeInfoModel(child);
                        info.ContainedType = item;
                        child.PublishedTypes.Add(info);
                    }
                }
               

                if (m_config.m_subscriptions.ContainsKey(vc))
                {
                    foreach (var item in m_config.m_subscriptions[vc])
                    {
                        TypeInfoModel info = new TypeInfoModel(child);
                        info.ContainedType = item;
                        child.SubscribedTypes.Add(info);
                    }

                }

                subtrees.Add(child);
            }

            return subtrees;
        }


        public void GenerateLogicalTree()
        {
            this.Tree.Views.Clear();

            foreach(var sub in BuildFromManageViews()) 
            {
                Tree.Views.Add(sub);
            }           
        }



        private void CanLoad(object sender, CanExecuteRoutedEventArgs e)
        {
            if (e.Parameter is TypeInfoModel)
            {
                e.CanExecute = true;
            }
            else
            {
                e.CanExecute = false;
            }
                
        }

        private void OnLoadSubscribers(object sender, ExecutedRoutedEventArgs e)
        {

            TypeInfoModel info = (TypeInfoModel)e.Parameter;
			IWindowViewContainer sourceView = info.m_parent.Content;
            Type sourceType = info.ContainedType;

            HashSet<ConnectionInfoModel> connectionInfo = new HashSet<ConnectionInfoModel>();

            /**
             var res = from subscriber in m_config.m_subscriptions                       
                       where subscriber.Key != null
                       from targetType in subscriber.Value
                       where this.m_config.m_eventRegistrator.AdapterService.IsAdaptationPossible(sourceType, targetType)
                       select new Tuple<IViewContainer, Type>(subscriber.Key, targetType);

             foreach (var sub in res)
             {
                 ConnectionInfoModel ci = new ConnectionInfoModel(m_config, sourceView, sourceType, Direction.ThisAsSubscriber);
                 ci.CompatibleType = sub.Item2;
                 ci.View = sub.Item1;
                 ci.IsConnected = m_config.IsConnected(info.ContainedType, sourceView.ID, ci.CompatibleType, ci.View.ID);
                 connectionInfo.Add(ci);
             }
            */
                       
                       
           

            foreach (var pair in m_config.m_subscriptions)
            {
                IWindowViewContainer cur = pair.Key;

                foreach (var t in pair.Value)
                {
                    if (m_config.m_eventRegistrator.AdapterService.IsAdaptationPossible(sourceType,t))
                    {
                        ConnectionInfoModel ci = new ConnectionInfoModel(m_config, sourceView, sourceType, Direction.ThisAsSubscriber);
                        ci.CompatibleType = t;
                        ci.View = cur;
                        ci.IsConnected = m_config.IsConnected(info.ContainedType, sourceView.ID, t, cur.ID);
                        connectionInfo.Add(ci);
                    }
                }
            }
            
            this.ConnectionList.DataContext = connectionInfo;
            //this.ListDescription.DataContext = targetView.Title;
        }



        private void OnLoadPublishers(object sender, ExecutedRoutedEventArgs e)
        {

            TypeInfoModel info = (TypeInfoModel)e.Parameter;
			IWindowViewContainer targetView = info.m_parent.Content;
            Type targetType = info.ContainedType;

            List<ConnectionInfoModel> connectionInfo = new List<ConnectionInfoModel>();

            var res = from publisher in m_config.m_publishs
                      from pt in publisher.Value
                      where m_config.m_eventRegistrator.AdapterService.IsAdaptationPossible(pt, targetType)
                      select new KeyValuePair<Type, IWindowViewContainer>(pt, publisher.Key);

            foreach(var pair in res)
            {
                ConnectionInfoModel ci = new ConnectionInfoModel(m_config, targetView, targetType, Direction.ThisAsPublisher);
                ci.CompatibleType = pair.Key;
                ci.View = pair.Value;
                ci.IsConnected = m_config.IsConnected(pair.Key, pair.Value.ID, targetType, targetView.ID);
                connectionInfo.Add(ci);
            }


            

            this.ConnectionList.DataContext = connectionInfo;
            
            //this.ListDescription.DataContext = targetView.Title;
        }

        public void ClearView()
        {            
            this.ConnectionList.DataContext = null;
        }

    }


    [ValueConversion(typeof(object), typeof(int))]
    internal class HeightConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            var ui = value as UIElement;
            if (ui != null && ui.RenderSize!=null && !ui.RenderSize.IsEmpty)
            {
                return ui.RenderSize.Height;
            }

            if (ui != null && ui.DesiredSize != null && !ui.DesiredSize.IsEmpty)
            {
                return ui.DesiredSize.Height;
            }

            return 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }

    }

    [ValueConversion(typeof(object), typeof(int))]
    internal class WidthConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var ui = value as UIElement;
            if (ui != null && ui.RenderSize != null && !ui.RenderSize.IsEmpty)
            {
                return ui.RenderSize.Width;
            }

            if (ui != null && ui.DesiredSize != null && !ui.DesiredSize.IsEmpty)
            {
                return ui.DesiredSize.Width;
            }

            return 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }

    }
}
