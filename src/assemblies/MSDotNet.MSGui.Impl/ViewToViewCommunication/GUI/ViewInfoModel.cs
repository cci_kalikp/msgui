﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using System.Windows;
using System.Windows.Media;


namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication
{
    internal class ViewInfoModel : DependencyObject
    {   
        private readonly TabInfoModel _parentTab;

        internal ViewInfoModel(IWindowViewContainer view_, TabInfoModel parentTab)
        {
            View = view_;
            _parentTab = parentTab;
        }

        internal ShellTabViewModel HostTab 
        { 
            get
            {
                return _parentTab != null ? _parentTab.Tab : null;
            } 
        }

        public IWindowViewContainer View
        {
            get; 
            private set;
        }
       
		public bool WinForms { get; set; }
		public Brush Brush { get; set; }
    }
}
