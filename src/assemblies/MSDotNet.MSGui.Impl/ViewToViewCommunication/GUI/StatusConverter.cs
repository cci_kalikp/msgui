﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Data;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication
{
    [Obsolete]
    internal class StatusConverter : IMultiValueConverter
    {
      
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var viewInfo = values[0] as ViewInfoModel;
            if (viewInfo == null)
                return null;

            var commVM = viewInfo.CommunicationModel as CommunicationViewModel;
            if (commVM == null)
            {
                return null;
            }

            var config = commVM.ConfigModel;


            if (parameter.Equals("IsConnectable"))
            {
                return config.GetCompatiblePublishers(viewInfo.View).Count() > 0 || config.GetCompatibleSubscribers(viewInfo.View).Count() > 0;
            }

            if (parameter.Equals("IsConnected"))
            {
                return config.ExplicitPublishTo(viewInfo.View).Count() > 0 || config.ExplicitSubscribeFrom(viewInfo.View).Count() > 0;
            }

            if (parameter.Equals("CanPublish"))
            {
                return config.GetCompatibleSubscribers(viewInfo.View).FirstOrDefault() != null;
            }

            if (parameter.Equals("HasPublished"))
            {
                return config.ExplicitSubscribeFrom(viewInfo.View).Count() > 0;
            }

            if (parameter.Equals("CanSubscribe"))
            {
                return config.GetCompatiblePublishers(viewInfo.View).FirstOrDefault() != null;
            }


            if (parameter.Equals("HasSubscribed"))
            {
                return config.ExplicitPublishTo(viewInfo.View).Count() > 0;
            }

            if (values.Length < 2)
                return false;

            var view1 = values[1] as IWindowViewContainer;
            if (view1 == null)
                return false;
            if (parameter.Equals("IsConnectedToView1"))
            {
                
                return config.ExplicitPublishTo(viewInfo.View).Contains(view1) ||
                       config.ExplicitSubscribeFrom(viewInfo.View).Contains(view1);
            }

            if (parameter.Equals("IsNotExplicitlyConnectedToView1"))
            {
                return !config.IsExplicitConnected(viewInfo.View, view1);
            }
            if (parameter.Equals("IsNotExplicitlyDisconnectedToView1"))
            {
                return !config.IsExplicitlyDisconnected(viewInfo.View, view1);
            }
          

            if (parameter.Equals("IsImplicitlyConnectedToView1"))
            {

                if (!config.GetCompatiblePublishers(viewInfo.View).Contains(view1) && !config.GetCompatibleSubscribers(viewInfo.View).Contains(view1))
                    return false;

                return !config.IsExplicitlyDisconnected(viewInfo.View, view1) &&
                       commVM.HookManager.CanHookTo(viewInfo.View, viewInfo.HostTab, commVM.Tab1);

                //return config.IsImplicitConnected(viewInfo.View, view1);

                //return !config.IsExplicitlyDisconnected(viewInfo.View, view1) &&
                 //      commVM.HookManager.CanHookTo(viewInfo.View, viewInfo.HostTab, commVM.Tab1);
            }

            if (parameter.Equals("MaySubscribeFromView1"))
            {
                return config.GetCompatiblePublishers(viewInfo.View).Contains(view1);
            }

            if (parameter.Equals("MayPublishToView1"))
            {
                return config.GetCompatibleSubscribers(viewInfo.View).Contains(view1);
            }

            if (parameter.Equals("MayConnectToView1"))
            {
                return config.GetCompatiblePublishers(viewInfo.View).Contains(view1) ||
                       config.GetCompatibleSubscribers(viewInfo.View).Contains(view1);
            }

            if (parameter.Equals("HasPublishedToView1"))
            {
                return config.ExplicitPublishTo(view1).Contains(viewInfo.View);
            }

            if (parameter.Equals("HasSubscribedFromView1"))
            {
                return config.ExplicitSubscribeFrom(view1).Contains(viewInfo.View);
            }


            if (parameter.Equals("CanHookToView1"))
            {
                if(viewInfo.View.ID == commVM.View1.ID)
                    return false;

                if (!config.GetCompatiblePublishers(viewInfo.View).Contains(view1) && !config.GetCompatibleSubscribers(viewInfo.View).Contains(view1))
                    return false;

                return commVM.HookManager.CanHookTo(viewInfo.View, viewInfo.HostTab, commVM.Tab1);
                //return commVM.Tab1 == null || viewInfo.HostTab == null || commVM.Tab1 == viewInfo.HostTab;
            }

            return null;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

}
