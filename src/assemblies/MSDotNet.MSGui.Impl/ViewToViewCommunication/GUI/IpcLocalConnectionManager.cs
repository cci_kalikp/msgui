﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml.Linq; 
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Core.Persistence;
using MorganStanley.MSDotNet.MSGui.Core.Persistence.DelayedLoading;
using MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging; 
using MorganStanley.MSDotNet.My;

// ReSharper disable InconsistentNaming
namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication
{
    internal class IpcLocalConnectionManager: IPersistable, IConnectionState
    {
        private static readonly IMSLogger Logger = MSLoggerFactory.CreateLogger<IpcLocalConnectionManager>();

        private readonly Dictionary<ViewMetaInfo, HashSet<Type>> m_localPublishs = new Dictionary<ViewMetaInfo, HashSet<Type>>();
        private readonly Dictionary<ViewMetaInfo, HashSet<Type>> m_localSubscriptions = new Dictionary<ViewMetaInfo, HashSet<Type>>();
        private readonly InterProcessMessenger m_eventRegistrator;

        private readonly Dictionary<KeyValuePair<Type, String>, HashSet<KeyValuePair<Type, String>>> m_explicitConnections = new Dictionary<KeyValuePair<Type, string>, HashSet<KeyValuePair<Type, string>>>();
        private readonly Dictionary<KeyValuePair<Type, String>, HashSet<KeyValuePair<Type, String>>> m_explicitDisconnections = new Dictionary<KeyValuePair<Type, string>, HashSet<KeyValuePair<Type, string>>>();
        
        private  IEnumerable<ViewMetaInfo> m_localWindowCollection;
        
        
        //private readonly Dictionary<KeyValuePair<Type, String>, HashSet<KeyValuePair<Type, String>>> m_implicitConnections;
        private readonly IMessageTypeRegistrator m_typeRegistrator;

        internal IpcLocalConnectionManager(InterProcessMessenger eventRegistrator, IMessageTypeRegistrator typeRegistrator)
        {
            m_eventRegistrator = eventRegistrator;
            m_typeRegistrator = typeRegistrator;
        }
        

        internal IEnumerable<ViewMetaInfo> InitConfigInfo(IEnumerable<ViewMetaInfo> localViews)
        {
            m_localWindowCollection = localViews;
            m_localPublishs.Clear();
            m_localSubscriptions.Clear();
            //m_implicitConnections.Clear();
         
            HashSet<ViewMetaInfo> activeLocalViews = new HashSet<ViewMetaInfo>();
            foreach (var view in m_localWindowCollection)
            {
                m_localPublishs[view] = new HashSet<Type>();
                foreach (var t in m_eventRegistrator.GetPublishedTypes(view.LocalId))
                {
                    m_localPublishs[view].Add(t);
                }

                m_localSubscriptions[view] = new HashSet<Type>();
                foreach (var t in m_eventRegistrator.GetSubscribedTypes(view.LocalId))
                {
                    m_localSubscriptions[view].Add(t);
                }

                if (m_localPublishs.ContainsKey(view) || m_localSubscriptions.ContainsKey(view))
                    activeLocalViews.Add(view);
            }

            return activeLocalViews;

        }

        private Dictionary<KeyValuePair<Type, String>, HashSet<KeyValuePair<Type, String>>> GetImplicitConnections()
        {
            var implicitConnections =
                new Dictionary<KeyValuePair<Type, String>, HashSet<KeyValuePair<Type, String>>>();
            var connection = from conn in m_eventRegistrator.GetConnections()
                             where GetLocalViewFromID(conn.Key.Value) != null
                             from sub in conn.Value
                             where GetLocalViewFromID(sub.Value) != null
                             select new Tuple<Type, string, Type, string>(conn.Key.Key, conn.Key.Value, sub.Key, sub.Value);

            var exp = from c in m_explicitConnections
                      from sub in c.Value
                      select new Tuple<Type, string, Type, string>(c.Key.Key, c.Key.Value, sub.Key, sub.Value);

            var dis = from c in m_explicitDisconnections
                      from sub in c.Value
                      select new Tuple<Type, string, Type, string>(c.Key.Key, c.Key.Value, sub.Key, sub.Value);

            foreach (var conn in connection.Except(exp).Except(dis))
            {
                HashSet<KeyValuePair<Type, String>> subs;
                var pub = new KeyValuePair<Type, string>(conn.Item1, conn.Item2);
                implicitConnections.TryGetValue(pub, out subs);
                if (subs == null)
                {
                    subs = new HashSet<KeyValuePair<Type, string>>();
                    implicitConnections[pub] = subs;
                }
                implicitConnections[pub].Add(new KeyValuePair<Type, string>(conn.Item3, conn.Item4));
            }
            return implicitConnections;
        }

      

        internal bool IsExplicitlyDisconnected(ViewMetaInfo view1, ViewMetaInfo view2)
        {
            var dis = (from d in m_explicitDisconnections
                       where d.Key.Value == view1.LocalId
                       from sub in d.Value
                       where sub.Value == view2.LocalId
                       select true).Count() > 0;
            if (dis)
                return true;


            dis = (from d in m_explicitDisconnections
                   where d.Key.Value == view2.LocalId
                   from sub in d.Value
                   where sub.Value == view1.LocalId
                   select true).Count() > 0;

            return dis;
        }


        internal bool IsImplicitConnected(ViewMetaInfo view1, ViewMetaInfo view2)
        {
            return ImplicitPublishTo(view1).Contains(view2) || ImplicitSubscribedFrom(view1).Contains(view2);
        }

       /* internal bool IsExplicitConnected(Type sourceType_, String sourceView_, Type targetType_, String targetView_)
        {
            var key = new KeyValuePair<Type, string>(sourceType_, sourceView_);
            if (!m_explicitConnections.ContainsKey(key))
            {
                return false;            
            }
            else
            {
                return m_explicitConnections[key].Contains(new KeyValuePair<Type, string>(targetType_, targetView_));
            }            
        }
*/
        

        internal IEnumerable<ViewMetaInfo> ExplicitPublishTo(ViewMetaInfo subscriber_)
        {

            var source = from conn in m_explicitConnections
                         from tp in conn.Value
                         where tp.Value == subscriber_.LocalId
                         select conn.Key.Value;

            return from id in source.Distinct()
                   select GetLocalViewFromID(id);
        }

        internal IEnumerable<ViewMetaInfo> ImplicitPublishTo(ViewMetaInfo subscriber_)
        {
            var source = from conn in GetImplicitConnections()
                         from tp in conn.Value
                         where tp.Value == subscriber_.LocalId
                         select conn.Key.Value;

            return from id in source.Distinct()
                   select GetLocalViewFromID(id);
        }

        internal IEnumerable<ViewMetaInfo> ExplicitSubscribeFrom(ViewMetaInfo publisher_)
        {
            var target = from conn in m_explicitConnections
                         where conn.Key.Value == publisher_.LocalId
                         from tp in conn.Value
                         select tp.Value;

            return from id in target.Distinct()
                   select GetLocalViewFromID(id);
        }

        internal IEnumerable<ViewMetaInfo> ImplicitSubscribedFrom(ViewMetaInfo publisher_)
        {
            var target = from conn in GetImplicitConnections()
                         where conn.Key.Value == publisher_.LocalId
                         from tp in conn.Value
                         select tp.Value;

            return from id in target.Distinct()
                   select GetLocalViewFromID(id);
        }
       

        internal IEnumerable<ViewMetaInfo> GetLocalViewCollection()
        {
            return m_localWindowCollection;
        }

        internal IEnumerable<KeyValuePair<Type, String>> GetCompatibleSubscribers(Type subType_)
        {
            
            MethodInfo method = m_eventRegistrator.GetType().GetMethod("GetCompatibleSubscribersWithType");
            method = method.MakeGenericMethod(new Type[] { subType_ });
            return (IEnumerable<KeyValuePair<Type, String>>)method.Invoke(m_eventRegistrator, new object[] { });
        }

        internal IEnumerable<ViewMetaInfo> GetCompatibleSubscribers(ViewMetaInfo publisher_)
        {
            HashSet<Type> ts;
            if (!m_localPublishs.TryGetValue(publisher_, out ts))
                return Enumerable.Empty<ViewMetaInfo>();

            var res = from pt in ts
                      from sub in m_localSubscriptions
                      from st in sub.Value
                      where m_eventRegistrator.AdapterService.IsAdaptationPossible(pt, st)
                      select sub.Key;

            return res.Distinct();
        }

     

        internal IEnumerable<KeyValuePair<Type, String>> GetCompatiblePublishers(Type pubType_)
        {
            MethodInfo method = m_eventRegistrator.GetType().GetMethod("GetCompatiblePublishersWithType");
            method = method.MakeGenericMethod(new Type[] { pubType_ });
            return (IEnumerable<KeyValuePair<Type, String>>)method.Invoke(m_eventRegistrator, new object[] { });
        }

        internal IEnumerable<ViewMetaInfo> GetCompatiblePublishers(ViewMetaInfo subscriber_)
        {
            HashSet<Type> ts;
            
            if (!m_localSubscriptions.TryGetValue(subscriber_, out ts))
            {
                return Enumerable.Empty<ViewMetaInfo>();
            }

            var res = from st in ts
                      from pub in m_localPublishs
                      from pt in pub.Value
                      where m_eventRegistrator.AdapterService.IsAdaptationPossible(pt, st)
                      select pub.Key;

            return res.Distinct();
        }

       
        private void ReigsterExplicitPublish(Type sourceType_, String sourceView_, Type targetType_, String targetView_)
        {            
            KeyValuePair<Type, String> s = new KeyValuePair<Type, string>(sourceType_, sourceView_);

            if (!m_explicitConnections.ContainsKey(s)) 
            {
                m_explicitConnections[s] = new HashSet<KeyValuePair<Type, string>>();
            }
            m_explicitConnections[s].Add(new KeyValuePair<Type, string>(targetType_, targetView_));
        }

        private void ReigsterExplicitNonPublish(Type sourceType_, String sourceView_, Type targetType_, String targetView_)
        {
            KeyValuePair<Type, String> s = new KeyValuePair<Type, string>(sourceType_, sourceView_);

            if (!m_explicitDisconnections.ContainsKey(s))
            {
                m_explicitDisconnections[s] = new HashSet<KeyValuePair<Type, string>>();
            }
            m_explicitDisconnections[s].Add(new KeyValuePair<Type, string>(targetType_, targetView_));
        }
/*
        internal void ReigsterImplicitConnection(Type sourceType_, String sourceView_, Type targetType_, String targetView_)
        {
            KeyValuePair<Type, String> s = new KeyValuePair<Type, string>(sourceType_, sourceView_);

            if (!m_implicitConnections.ContainsKey(s))
            {
                m_explicitDisconnections[s] = new HashSet<KeyValuePair<Type, string>>();
            }
            m_implicitConnections[s].Add(new KeyValuePair<Type, string>(targetType_, targetView_));
        }

        internal void ReigsterImplicitConnection(IWindowViewContainer view1, IWindowViewContainer view2)
        {
            var ts = from pt in m_eventRegistrator.GetPublishedTypes(view1.ID)
                     from st in m_eventRegistrator.GetSubscribedTypes(view2.ID)
                     where m_eventRegistrator.AdapterService.IsAdaptationPossible(pt, st)
                     select new Tuple<Type, string, Type, string>(pt, view1.ID, st, view2.ID);

            foreach (var conn in ts)
            {
                this.ReigsterImplicitConnection(conn.Item1, conn.Item2, conn.Item3, conn.Item4);
            }

            ts = from pt in m_eventRegistrator.GetPublishedTypes(view2.ID)
                 from st in m_eventRegistrator.GetSubscribedTypes(view1.ID)
                 where m_eventRegistrator.AdapterService.IsAdaptationPossible(pt, st)
                 select new Tuple<Type, string, Type, string>(pt, view2.ID, st, view1.ID);

            foreach (var conn in ts)
            {
                this.ReigsterImplicitConnection(conn.Item1, conn.Item2, conn.Item3, conn.Item4);
            }
        }*/

        internal void ReigsterExplicitConnection(ViewMetaInfo view1, ViewMetaInfo view2)
        {
            var ts = from pt in m_eventRegistrator.GetPublishedTypes(view1.LocalId)
                     from st in m_eventRegistrator.GetSubscribedTypes(view2.LocalId)
                     where m_eventRegistrator.AdapterService.IsAdaptationPossible(pt, st)
                     select new Tuple<Type, string, Type, string>(pt, view1.LocalId, st,view2.LocalId);

            foreach (var conn in ts)
            {
                this.ReigsterExplicitPublish(conn.Item1, conn.Item2, conn.Item3, conn.Item4);
            }

            ts = from pt in m_eventRegistrator.GetPublishedTypes(view2.LocalId)
                     from st in m_eventRegistrator.GetSubscribedTypes(view1.LocalId)
                     where m_eventRegistrator.AdapterService.IsAdaptationPossible(pt, st)
                     select new Tuple<Type, string, Type, string>(pt, view2.LocalId, st, view1.LocalId);

            foreach (var conn in ts)
            {
                this.ReigsterExplicitPublish(conn.Item1, conn.Item2, conn.Item3, conn.Item4);
            }
        }

        internal void ReigsterExplicitDisconnection(ViewMetaInfo view1, ViewMetaInfo view2)
        {
            var conn = from pubType in m_eventRegistrator.GetPublishedTypes(view1.LocalId)
                       from subType in m_eventRegistrator.GetSubscribedTypes(view2.LocalId)
                       where m_eventRegistrator.AdapterService.IsAdaptationPossible(pubType, subType)
                       select new Tuple<Type, Type>(pubType, subType);

            foreach (var connection in conn)
            {
                this.ReigsterExplicitNonPublish(connection.Item1, view1.LocalId, connection.Item2, view2.LocalId);
            }

            conn = from pubType in m_eventRegistrator.GetPublishedTypes(view2.LocalId)
                   from subType in m_eventRegistrator.GetSubscribedTypes(view1.LocalId)
                   where m_eventRegistrator.AdapterService.IsAdaptationPossible(pubType, subType)
                   select new Tuple<Type, Type>(pubType, subType);

            foreach (var connection in conn)
            {
                this.ReigsterExplicitNonPublish(connection.Item1, view2.LocalId, connection.Item2, view1.LocalId);
            }
        }

        internal void UnreigsterExplicitDisconnection(ViewMetaInfo view1, ViewMetaInfo view2)
        {
            var conn = from pubType in m_eventRegistrator.GetPublishedTypes(view1.LocalId)
                       from subType in m_eventRegistrator.GetSubscribedTypes(view2.LocalId)
                       where m_eventRegistrator.AdapterService.IsAdaptationPossible(pubType, subType)
                       select new Tuple<Type, Type>(pubType, subType);

            foreach (var connection in conn)
            {
                this.UnreigsterExplicitNonPublish(connection.Item1, view1.LocalId, connection.Item2, view2.LocalId);
            }

            conn = from pubType in m_eventRegistrator.GetPublishedTypes(view2.LocalId)
                   from subType in m_eventRegistrator.GetSubscribedTypes(view1.LocalId)
                   where m_eventRegistrator.AdapterService.IsAdaptationPossible(pubType, subType)
                   select new Tuple<Type, Type>(pubType, subType);

            foreach (var connection in conn)
            {
                this.UnreigsterExplicitNonPublish(connection.Item1, view2.LocalId, connection.Item2, view1.LocalId);
            }
        }

        internal void UnreigsterExplicitConnection(ViewMetaInfo view1, ViewMetaInfo view2)
        {
            var ts = from pt in m_eventRegistrator.GetPublishedTypes(view1.LocalId)
                     from st in m_eventRegistrator.GetSubscribedTypes(view2.LocalId)
                     where m_eventRegistrator.AdapterService.IsAdaptationPossible(pt, st)
                     select new Tuple<Type, string, Type, string>(pt, view1.LocalId, st, view2.LocalId);

            foreach (var conn in ts)
            {
                this.UnreigsterExplicitPublish(conn.Item1, conn.Item2, conn.Item3, conn.Item4);
            }

            ts = from pt in m_eventRegistrator.GetPublishedTypes(view2.LocalId)
                 from st in m_eventRegistrator.GetSubscribedTypes(view1.LocalId)
                 where m_eventRegistrator.AdapterService.IsAdaptationPossible(pt, st)
                 select new Tuple<Type, string, Type, string>(pt, view2.LocalId, st, view1.LocalId);

            foreach (var conn in ts)
            {
                this.UnreigsterExplicitPublish(conn.Item1, conn.Item2, conn.Item3, conn.Item4);
            }
        }

        private void UnreigsterExplicitPublish(Type sourceType_, String sourceView_, Type targetType_, String targetView_)
        {
            KeyValuePair<Type, String> s = new KeyValuePair<Type, string>(sourceType_, sourceView_);

            if (!m_explicitConnections.ContainsKey(s))
            {
                return;
            }
            m_explicitConnections[s].Remove(new KeyValuePair<Type, string>(targetType_, targetView_));
        }


        internal void UnreigsterExplicitNonPublish(Type sourceType_, String sourceView_, Type targetType_, String targetView_)
        {
            KeyValuePair<Type, String> s = new KeyValuePair<Type, string>(sourceType_, sourceView_);

            if (!m_explicitDisconnections.ContainsKey(s))
            {
                return;
            }
            m_explicitDisconnections[s].Remove(new KeyValuePair<Type, string>(targetType_, targetView_));
        }
     

        
        public ViewMetaInfo GetLocalViewFromID(String id_)
        {
            var id = m_eventRegistrator.GetLocalId(id_);
            return m_localWindowCollection.FirstOrDefault(v => v.LocalId == id);
        }

      
        

        internal void ApplyChanges()
        {

           
            /*
            foreach (var con in m_eventRegistrator.GetConnections())
            {
                var source = con.Key;

                foreach (var target in con.Value)
                {  
                    var method = m_eventRegistrator.GetType().GetMethod("Disconnect", new Type[] { typeof(IViewContainerBase), typeof(IViewContainerBase) });
                    method = method.MakeGenericMethod(new Type[] { source.Key, target.Key });
                    var sourceView = this.GetViewFromID(source.Value);
                    var targetView = this.GetViewFromID(target.Value);
                    if(sourceView != null && targetView != null)  // config model should only work on views, not modules; so, skip the modules
                        method.Invoke(m_eventRegistrator, new IViewContainerBase[] { sourceView, targetView });
                }
            }*/

            foreach (var con in m_explicitConnections)
            {
                var source = con.Key;
                foreach (var target in con.Value)
                {
                    var method = m_eventRegistrator.GetType().GetMethod("Connect", BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(ViewMetaInfo), typeof(ViewMetaInfo) }, null);
                    method = method.MakeGenericMethod(new Type[] { source.Key, target.Key });
                    var sourceView = this.GetLocalViewFromID(source.Value) ;
                    var targetView = this.GetLocalViewFromID(target.Value) ;
                    if(sourceView == null || targetView == null)
                        continue;
                    Debug.Assert(sourceView != null && targetView != null);  // config model should only work on views, not modules
                    try
                    {
                        method.Invoke(m_eventRegistrator, new ViewMetaInfo[] {sourceView, targetView});
                    }
                    catch (Exception exc)
                    {
                        Logger.Error(
                            string.Format("Failed to apply change for explicit connection {0} with value {1} on views {2} and {3}", con.Key, con.Value, sourceView, targetView), exc);
                    }
                }
            }

          /*  foreach (var con in m_implicitConnections)
            {
                var source = con.Key;
                foreach (var target in con.Value)
                {
                    var method = m_eventRegistrator.GetType().GetMethod("Connect", new Type[] { typeof(IViewContainerBase), typeof(IViewContainerBase) });
                    method = method.MakeGenericMethod(new Type[] { source.Key, target.Key });
                    var sourceView = this.GetViewFromID(source.Value);
                    var targetView = this.GetViewFromID(target.Value);
                    Debug.Assert(sourceView != null && targetView != null);  // config model should only work on views, not modules
                    method.Invoke(m_eventRegistrator, new IViewContainerBase[] { sourceView, targetView });
                }
            }*/

            foreach (var con in m_explicitDisconnections)
            {
                var source = con.Key;

                foreach (var target in con.Value)
                {
                    var method = m_eventRegistrator.GetType().GetMethod("Disconnect", BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { typeof(ViewMetaInfo), typeof(ViewMetaInfo) }, null);
                    method = method.MakeGenericMethod(new Type[] { source.Key, target.Key });
                    var sourceView = this.GetLocalViewFromID(source.Value);
                    var targetView = this.GetLocalViewFromID(target.Value);
                    if(sourceView == null || targetView == null)
                        continue;
                    try
                    {

                        method.Invoke(m_eventRegistrator, new ViewMetaInfo[] { sourceView, targetView });
                    }
                    catch (Exception exc)
                    {
                        Logger.Error(
                            string.Format("Failed to apply change for explicit disconnection {0} with value {1} on views {2} and {3}", con.Key, con.Value, sourceView, targetView), exc);
                    }
                }
            }
        }



        #region Autohook

        
      


        internal void AutoHook(IWindowViewContainer view, IEnumerable<IWindowViewContainer> others)
        {
            AutoHookPublishing(view, others);
            AutoHookSubscribing(view, others);
        }




        internal void AutoHookPublishing(IWindowViewContainer publisher, IEnumerable<IWindowViewContainer> others)
        {
            var hookedViews = new HashSet<IWindowViewContainer>();
            var pubTypes = m_eventRegistrator.GetPublishedTypes(publisher.ID);
            foreach (var subscriber in others)
            {
                var subTypes = m_eventRegistrator.GetSubscribedTypes(subscriber.ID);
                var conn = from pt in pubTypes
                           from st in subTypes
                           where m_eventRegistrator.AdapterService.IsAdaptationPossible(pt, st)
                           select new Tuple<Type, Type>(pt, st);
                 

                foreach (var typePair in conn)
                {
                    var methodInfo = m_eventRegistrator.GetType().GetMethod("Connect", new Type[] { typeof(IWindowViewContainer), typeof(IWindowViewContainer) });
                    HashSet<KeyValuePair<Type, string>> conns;
                    m_explicitDisconnections.TryGetValue(new KeyValuePair<Type, string>(typePair.Item1, publisher.ID),
                                                         out conns);
                    if (conns==null || !conns.Contains(new KeyValuePair<Type, string>(typePair.Item2, subscriber.ID)))
                    {
                       // RegisterImplicitConnection(typePaire.Item1, publisher.ID, typePaire.Item2, subscriber.ID);
                        var method = methodInfo.MakeGenericMethod(new Type[] {typePair.Item1, typePair.Item2});
                        method.Invoke(m_eventRegistrator, new IWindowViewContainer[] { publisher, subscriber });
                    }
                    
                }
            }
        }

        internal void AutoHookSubscribing(IWindowViewContainer subscriber, IEnumerable<IWindowViewContainer> others)
        {
            var subTypes = m_eventRegistrator.GetSubscribedTypes(subscriber.ID);

            foreach (var publisher in others)
            {
                var pubTypes = m_eventRegistrator.GetPublishedTypes(publisher.ID);
                var conn = from pt in pubTypes
                           from st in subTypes
                           where m_eventRegistrator.AdapterService.IsAdaptationPossible(pt, st)
                           select new Tuple<Type, Type>(pt, st);

                foreach (var typePaire in conn)
                {
                    var methodInfo = m_eventRegistrator.GetType().GetMethod("Connect", new Type[] { typeof(IWindowViewContainer), typeof(IWindowViewContainer) });
                    HashSet<KeyValuePair<Type, string>> conns;
                    m_explicitDisconnections.TryGetValue(new KeyValuePair<Type, string>(typePaire.Item1, publisher.ID),
                                                         out conns);

                    if (conns==null || !conns.Contains(new KeyValuePair<Type, string>(typePaire.Item2, subscriber.ID)))
                    {
                       // RegisterImplicitConnection(typePaire.Item1, publisher.ID, typePaire.Item2, subscriber.ID);
                        var method = methodInfo.MakeGenericMethod(new Type[] { typePaire.Item1, typePaire.Item2 });
                        method.Invoke(m_eventRegistrator, new IWindowViewContainer[] { publisher, subscriber });
                    }
                }
            }
        }

        internal void AutoUnhook(IWindowViewContainer view, IEnumerable<IWindowViewContainer> others)
        {
            AutoUnhookPublishing(view, others);
            AutoUnhookSubscribing(view, others);
        }

        internal void AutoUnhookPublishing(IWindowViewContainer publisher, IEnumerable<IWindowViewContainer> others)
        {

            var connections = from conn in m_eventRegistrator.GetConnections()
                              where conn.Key.Value == publisher.ID
                              from sub in conn.Value
                              where others.FirstOrDefault((o) => o.ID == sub.Value) != null 
                              select 
                                  new Tuple<Type, string, Type, string>(conn.Key.Key, conn.Key.Value, sub.Key, sub.Value);

            var expconnections = from exp in m_explicitConnections
                                 where exp.Key.Value == publisher.ID
                                 from es in exp.Value
                                 where others.FirstOrDefault((o) => o.ID == es.Value) != null 
                                 select
                                     new Tuple<Type, string, Type, string>(exp.Key.Key, exp.Key.Value, es.Key, es.Value);

            var methodInfo = m_eventRegistrator.GetType().GetMethod("Disconnect",
                                                                    new Type[] { typeof(IWindowViewContainer), typeof(IWindowViewContainer) });
            foreach (var conn in connections.Except(expconnections))
            {
                var method = methodInfo.MakeGenericMethod(new Type[] {conn.Item1, conn.Item3});
                var subscriber = others.Where((o) => o.ID == conn.Item4).FirstOrDefault();
                method.Invoke(m_eventRegistrator, new IWindowViewContainer[] { publisher, subscriber });
            }

        }

        internal void AutoUnhookSubscribing(IWindowViewContainer subscriber, IEnumerable<IWindowViewContainer> others)
        {
            var connections = from conn in m_eventRegistrator.GetConnections()
                              where others.FirstOrDefault((o) => o.ID == conn.Key.Value) != null 
                              from sub in conn.Value
                              where subscriber.ID == sub.Value
                              select
                                  new Tuple<Type, string, Type, string>(conn.Key.Key, conn.Key.Value, sub.Key, sub.Value);

            var expconnections = from exp in m_explicitConnections
                                 where others.FirstOrDefault((o) => o.ID == exp.Key.Value) != null 
                                 from es in exp.Value
                                 where es.Value == subscriber.ID
                                 select
                                     new Tuple<Type, string, Type, string>(exp.Key.Key, exp.Key.Value, es.Key, es.Value);


            var methodInfo = m_eventRegistrator.GetType().GetMethod("Disconnect",
                                                                    new Type[] { typeof(IWindowViewContainer), typeof(IWindowViewContainer) });
            foreach (var conn in connections.Except(expconnections))
            {
                var method = methodInfo.MakeGenericMethod(new Type[] { conn.Item1, conn.Item3 });
                var publisher = others.Where((o) => o.ID == conn.Item2).FirstOrDefault();
                method.Invoke(m_eventRegistrator, new IWindowViewContainer[] { publisher, subscriber });
            }
        }


        internal void DiscardOutdatedDisconnections(IWindowViewContainer source, IEnumerable<IWindowViewContainer> targets)
        {
            var discard = new HashSet<Tuple<Type, string, Type, string>>();
            foreach (var disconnection in m_explicitDisconnections)
            {
                var sid = disconnection.Key.Value;
                if(sid != source.ID)
                    continue;

                foreach (var sub in disconnection.Value)
                {
                    if (targets.FirstOrDefault((o) => o.ID == sub.Value) != null)
                        discard.Add(new Tuple<Type, string, Type, string>(disconnection.Key.Key, disconnection.Key.Value,
                                                                          sub.Key, sub.Value));
                }
            }

            foreach (var disconnection in m_explicitDisconnections)
            {
                var tid = disconnection.Key.Value;
                if (targets.FirstOrDefault((o) => o.ID == tid) == null)
                    continue;

                foreach (var pub in disconnection.Value)
                {
                    if (pub.Value == source.ID)
                        discard.Add(new Tuple<Type, string, Type, string>(disconnection.Key.Key, disconnection.Key.Value,
                                                                          pub.Key, pub.Value));
                }
            }

            foreach (var tuple in discard)
            {
                var key = new KeyValuePair<Type, string>(tuple.Item1, tuple.Item2);
                var value = new KeyValuePair<Type, string>(tuple.Item3, tuple.Item4);
                m_explicitDisconnections[key].Remove(value);
            }
            
        }

        #endregion

        #region persistence


        private void BuildConnectionXML(XElement root)
        {
            foreach (var con in m_explicitConnections)
            {
                var source = con.Key;

                var srcTypeId = m_typeRegistrator.GetRegistartionId(source.Key);


                XElement connection;
                if (srcTypeId != null)
                {
                    connection = new XElement("Connection",
                        new XAttribute("SourceID", source.Value),
                        new XAttribute("SourceTypeId", srcTypeId)
                       );    
                }
                else
                {
                    connection = new XElement("Connection",
                    new XAttribute("SourceType", source.Key.FullName),
                    new XAttribute("SourceAssembly", source.Key.Assembly.GetName().Name),
                    new XAttribute("SourceID", source.Value));
                }

                foreach (var rec in con.Value)
                {

                    var targetTypeId = m_typeRegistrator.GetRegistartionId(rec.Key);
                    XElement receiver;
                    if (targetTypeId != null)
                    {
                        receiver = new XElement("Receiver",
                        new XAttribute("TargetID", rec.Value),
                        new XAttribute("TargetTypeId", targetTypeId));
                    }
                    else
                    {
                        receiver = new XElement("Receiver",
                        new XAttribute("TargetType", rec.Key.FullName),
                        new XAttribute("TargetAssembly", rec.Key.Assembly.GetName().Name),
                        new XAttribute("TargetID", rec.Value));
                    }

                    connection.Add(receiver);
                }
                root.Add(connection);
            }

            foreach (var discon in m_explicitDisconnections)
            {
                var source = discon.Key;
                

                XElement disconnection;

                var srcTypeId = m_typeRegistrator.GetRegistartionId(source.Key);

                if (srcTypeId != null)
                {
                    disconnection = new XElement("Disconnection",
                        new XAttribute("SourceID", source.Value),
                        new XAttribute("SourceTypeId", srcTypeId)
                       );
                }
                else
                {
                    disconnection = new XElement("Disconnection",
                    new XAttribute("SourceType", source.Key.FullName),
                    new XAttribute("SourceAssembly", source.Key.Assembly.GetName().Name),
                    new XAttribute("SourceID", source.Value));
                }



                foreach (var rec in discon.Value)
                {
                    var targetTypeId = m_typeRegistrator.GetRegistartionId(rec.Key);
                    XElement receiver;
                    if (targetTypeId != null)
                    {
                        receiver = new XElement("Receiver",
                        new XAttribute("TargetID", rec.Value),
                        new XAttribute("TargetTypeId", targetTypeId));
                    }
                    else
                    {
                        receiver = new XElement("Receiver",
                        new XAttribute("TargetType", rec.Key.FullName),
                        new XAttribute("TargetAssembly", rec.Key.Assembly.GetName().Name),
                        new XAttribute("TargetID", rec.Value));
                    }

                    disconnection.Add(receiver);
                }
                root.Add(disconnection);
            }
        }

        internal EventHandler OnRestoringFromPersistence;

        private void RestoreConnections(XElement root)
        {  
            m_localPublishs.Clear();
            m_localSubscriptions.Clear();
            //m_implicitConnections.Clear();
            m_explicitConnections.Clear();
            m_explicitDisconnections.Clear();

            foreach (var con in root.Elements("Connection"))
            {
                var srcTypeIdAttr = con.Attribute("SourceTypeId");
                Type srcType;
                if (srcTypeIdAttr != null)
                {
                    srcType = m_typeRegistrator.ResolveType(srcTypeIdAttr.Value);
                }
                else
                {
                    var nm = con.Attribute("SourceType").Value;
                    var asmName = con.Attribute("SourceAssembly").Value;
                    var asm = Assembly.Load(asmName);
                    srcType = asm.GetType(nm);
                }

                var srcId = con.Attribute("SourceID").Value;


                foreach (var rec in con.Elements("Receiver"))
                {
                    var targetTypeIdAttr = rec.Attribute("TargetTypeId");
                    Type targetType;

                    if (targetTypeIdAttr != null)
                    {
                        targetType = m_typeRegistrator.ResolveType(targetTypeIdAttr.Value);
                    }
                    else
                    {
                        var asm = Assembly.Load(rec.Attribute("TargetAssembly").Value);
                        targetType = asm.GetType(rec.Attribute("TargetType").Value);
                    }

                    var targetId = rec.Attribute("TargetID").Value;

                    this.ReigsterExplicitPublish(srcType, srcId, targetType, targetId);
                }
            }

            foreach (var con in root.Elements("Disconnection"))
            {
                var srcTypeIdAttr = con.Attribute("SourceTypeId");
                Type srcType;
                if (srcTypeIdAttr != null)
                {
                    srcType = m_typeRegistrator.ResolveType(srcTypeIdAttr.Value);
                }
                else
                {
                    var nm = con.Attribute("SourceType").Value;
                    var asmName = con.Attribute("SourceAssembly").Value;
                    var asm = Assembly.Load(asmName);
                    srcType = asm.GetType(nm);
                }

                var srcId = con.Attribute("SourceID").Value;


                foreach (var rec in con.Elements("Receiver"))
                {
                    var targetTypeIdAttr = rec.Attribute("TargetTypeId");
                    Type targetType;

                    if (targetTypeIdAttr != null)
                    {
                        targetType = m_typeRegistrator.ResolveType(targetTypeIdAttr.Value);
                    }
                    else
                    {
                        var asm = Assembly.Load(rec.Attribute("TargetAssembly").Value);
                        targetType = asm.GetType(rec.Attribute("TargetType").Value);
                    }

                    var targetId = rec.Attribute("TargetID").Value;
                    this.ReigsterExplicitNonPublish(srcType, srcId, targetType, targetId);
                }
            }

            var copy = OnRestoringFromPersistence;
            if (copy != null)
            {
                copy(this, new EventArgs());
            }

            ApplyChanges();
        }
      
        void IPersistable.LoadState(System.Xml.Linq.XDocument state_)
        {
            XElement root = state_.Element("View2ViewConnection");
            var eventArgs = new BeforeLoadView2ViewConnectionEventArgs
                                {
                                    Connection = state_
                                };
            DelayedProfileLoadingExtensionPoints.InvokeBeforeLoadView2ViewConnection(eventArgs);
            if (!eventArgs.Cancel)
            {
                
                RestoreConnections(root);
            }
        }

        System.Xml.Linq.XDocument IPersistable.SaveState()
        {

            XDocument doc = new XDocument();
            XElement root = new XElement("View2ViewConnection");

            BuildConnectionXML(root);
            doc.Add(root);
            return doc;
        }


        string IPersistable.PersistorId
        {
            get { return typeof(IpcLocalConnectionManager).FullName; }
        }


        #endregion


        bool IConnectionState.IsConnected(ViewMetaInfo view1, ViewMetaInfo view2)
        {
            return (this as IConnectionState).HasPublishedTo(view1, view2) ||
                   (this as IConnectionState).HasSubscribedFrom(view1, view2);
        }

        bool IConnectionState.IsDisconnected(ViewMetaInfo view1, ViewMetaInfo view2)
        {
            return IsExplicitlyDisconnected(view1, view2);
        }

        bool IConnectionState.CanPublishTo(ViewMetaInfo publishView, ViewMetaInfo subscribeView)
        {
            return this.GetCompatibleSubscribers(publishView).Contains(subscribeView);
        }

       
        bool IConnectionState.HasPublishedTo(ViewMetaInfo publisher, ViewMetaInfo subscriber)
        {
            return this.ExplicitPublishTo(subscriber).Contains(publisher);
        }

        bool IConnectionState.IsImplicitConnected(ViewMetaInfo view1, ViewMetaInfo view2)
        {
            return this.IsImplicitConnected(view1, view2);
        }

        public bool HasSubscribedFrom(ViewMetaInfo subscriber, ViewMetaInfo publisher)
        {
            return this.ExplicitSubscribeFrom(publisher).Contains(subscriber);
        }



    }
}
// ReSharper restore InconsistentNaming