﻿using System;
using System.Collections.Generic;

using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging;

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication
{
    internal class IpcViewInfoModel : DependencyObject
    {
        internal IpcViewInfoModel(ViewMetaInfo viewInfo, IpcViewModel ipcViewModel)
        {
            ViewInfo = viewInfo;
            IpcViewModel = ipcViewModel;
        }
        public ViewMetaInfo ViewInfo { get; private set; }
        public IpcViewModel IpcViewModel { get; private set; }
        public IWindowViewContainer View {get { return ViewInfo.ViewContainer as IWindowViewContainer; }}

        public bool WinForms { get; set; }
        public Brush Brush { get; set; }
    }
}
