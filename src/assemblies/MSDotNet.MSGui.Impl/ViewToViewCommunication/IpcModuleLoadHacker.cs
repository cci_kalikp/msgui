﻿using MorganStanley.MSDotNet.MSGui.Impl.Shell;

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication
{
    public sealed class IpcModuleLoadHacker
    {
        private readonly IShell _shell;

        internal IpcModuleLoadHacker(IShell shell)
        {
            _shell = shell;
        }

        internal IShell Shell
        {
            get { return _shell; }
        }
    }
}
