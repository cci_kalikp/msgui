﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2011 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/ViewToViewCommunication/LinkEditor.xaml.cs#5 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $


using MorganStanley.MSDotNet.MSGui.Core;
using System.Collections.ObjectModel;

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication
{
	public partial class LinkEditor
	{
		private readonly ObservableCollection<IViewContainer> m_views;

		public LinkEditor(ObservableCollection<IViewContainer> views_)
		{
			m_views = views_;
			InitializeComponent();
		}
	}
}