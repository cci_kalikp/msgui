﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication
{
    [Obsolete]
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct)]
    public class MessageAttribute: MSGui.Core.Messaging.MessageAttribute
    {
       
        public MessageAttribute(string id):base(id)
        {
        }
    }
}
