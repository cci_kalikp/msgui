﻿using System;
using System.Collections.Generic;
using System.Linq;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Core.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging; 
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication.GUI;

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication
{
    public class V2VConfigModule : IMSDesktopModule, IV2VConnectionManager
    {
        private readonly IWindowManager _windowManager;
        private readonly IEnumerable<IWindowViewContainer> _viewCollection;
        private readonly IpcLocalConnectionManager _localConnectionManager;
        private readonly IChromeManager _chromeManager;
        private readonly IChromeRegistry _chromeRegistry;
        private AutoConnectionManager _autoHookManager;
        private readonly InterProcessMessenger _ipc;

        private AutoHookMode _autoHookMode;
        public AutoHookMode AutoHookMode
        {
            get
            {
                return _autoHookMode;
            }
            set
            {   
                _autoHookMode = value;
                if(_autoHookManager != null)
                    _autoHookManager.AutoHookMode = value;
            }
        }

        private readonly ExternalConnectionManager _externalConnectionManager;

        public V2VConfigModule(IpcModuleLoadHacker loader, IChromeManager chromeManager, IChromeRegistry chromeRegistry,
                               IEventRegistrator ipc, IPersistenceService persistenceService,
                               IMessageTypeRegistrator typeRegistrator)
        {
            _chromeManager = chromeManager;
            _chromeRegistry = chromeRegistry;

            // TODO (hrechkin): This may be null in Isolated scenarios
            var shell = loader.Shell;
            shell.MainWindow.Closed += (s, args) => _ipc.NotifyApplicationClosed();
            _windowManager = shell.WindowManager;

            _viewCollection = chromeManager.GetViews();

            _ipc = (InterProcessMessenger) ipc;
            _localConnectionManager = new IpcLocalConnectionManager(_ipc, typeRegistrator);
            _externalConnectionManager = new ExternalConnectionManager(_ipc);


            var localPersistor = _localConnectionManager as IPersistable;
            persistenceService.AddPersistor(localPersistor.PersistorId, localPersistor.LoadState,
                                            localPersistor.SaveState);

            var extPersistor = _externalConnectionManager as IPersistable;
            persistenceService.AddPersistor(extPersistor.PersistorId, extPersistor.LoadState, extPersistor.SaveState);
        }

        public void EnableIpcImc()
        {
            _ipc.EnableIpcImc();
        }


        public void DisableIpcImc()
        {
            _ipc.DisableIpcImc();
        }

        private IEnumerable<ViewMetaInfo> GetLocalViews()
        {
            var ipcViewModel = new IpcViewModel(_ipc.Application, _windowManager, _chromeManager.Windows, _ipc.ExternalViewManager.ExternalViews, _localConnectionManager, _autoHookManager, _externalConnectionManager);

            return new List<ViewMetaInfo>(from app in ipcViewModel.Applications
                                          where app.Application.Equals(_ipc.Application)
                                          from tab in app.Tabs
                                          from view in tab.Views
                                          select view.ViewInfo); 
        }



        public void Initialize()
        {
            _autoHookManager = new AutoConnectionManager(_windowManager, _viewCollection, _localConnectionManager)
                {
                    AutoHookMode = _autoHookMode
                };

            _localConnectionManager.OnRestoringFromPersistence +=
                (s, e) => _localConnectionManager.InitConfigInfo(GetLocalViews());


            _ipc.RequestAllViews().Wait();


            _chromeRegistry.RegisterDialogFactory("TileMockup", ConfigDialogFactory);

            _chromeManager.AddButton("TileMockup", ChromeManagerBase.MENU_COMMANDAREA_NAME, "",
                                     new InitialButtonParameters
                                         {
                                             Text = "View to View Communication",
                                             Click = HandleTileMockupClick,
                                         });
        }

        public bool ConfigDialogFactory(IDialogWindow dialog)
        {
            var future = _ipc.RequestAllViews();
            future.Wait();

            Console.WriteLine("Request all views sent");

            var ipcViewModel = new IpcViewModel(_ipc.Application, _windowManager, _chromeManager.Windows, _ipc.ExternalViewManager.ExternalViews, _localConnectionManager, _autoHookManager, _externalConnectionManager);


            var localViews = from app in ipcViewModel.Applications
                             where app.Application.Equals(_ipc.Application)
                             from tab in app.Tabs
                             from view in tab.Views
                             select view.ViewInfo;
            var window = new V2VMetroConfig(_chromeManager, _ipc.Application)
                {
                    Model = ipcViewModel
                };

            window.Initialize(_windowManager, localViews, ipcViewModel);

            dialog.Title = "View to View Communication Configuration";
            dialog.Content = window;
            dialog.Width = 1000;
            dialog.Height = 700;

            return true;
        }

        public void SignUpAutoHook(IWindowViewContainer viewContainer, Action<IEnumerable<IWindowViewContainer>> hookupCallback, Action<IEnumerable<IWindowViewContainer>> unhookedCallback)
        {
            _autoHookManager.SignUpAutoHook(viewContainer, hookupCallback, unhookedCallback);
        }

        public void SignOffAutoHook(IWindowViewContainer viewContainer)
        {
            _autoHookManager.SignOffAutoHook(viewContainer);
        }

        private void HandleTileMockupClick(object sender, EventArgs args)
        {
            var dialog = _chromeManager.CreateDialog("TileMockup");
            if (dialog != null)
            {
                dialog.ShowDialog();
            }
        }
    }
}
