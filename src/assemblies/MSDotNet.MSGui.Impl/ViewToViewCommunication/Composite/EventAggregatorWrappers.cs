﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication.Composite
{
    /// <summary>
    /// To unify the underlying event aggregator instances
    /// </summary>
    internal class EventAggregatorWrapper : IMSDesktopEventAggregator
    {
        private IEventAggregator _eventAggregator;
        public EventAggregatorWrapper(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
        }


        #region Implementation of IMSDesktopEventAggregator

        TEvent IMSDesktopEventAggregator.GetEvent<TEvent>()
        {
            var methodInfo = typeof(IEventAggregator).GetMethod("GetEvent");
            methodInfo = methodInfo.MakeGenericMethod(new Type[] { typeof(TEvent) });
            return (TEvent)(methodInfo.Invoke(_eventAggregator, null));
        }

        #endregion


    }

    /// <summary>
    /// To Expose MSDesktop Event Aggregator as IEventAggregator
    /// </summary>
    internal class MSDesktopEventAggregatorWrapper : IEventAggregator
    {

        private IMSDesktopEventAggregator _msDesktopEventAggregator;
        public MSDesktopEventAggregatorWrapper([Dependency("InnerMSDesktopEventAggregator")] IMSDesktopEventAggregator msDesktopEventAggregator)
        {
            _msDesktopEventAggregator = msDesktopEventAggregator;
        }

        #region Implementation of IEventAggregator

        /// <summary>
        /// Gets an instance of an event type.
        /// </summary>
        /// <typeparam name="TEventType">The type of event to get.</typeparam>
        /// <returns>
        /// An instance of an event object of type <typeparamref name="TEventType"/>.
        /// </returns>
        TEventType IEventAggregator.GetEvent<TEventType>()
        {
            return _msDesktopEventAggregator.GetEvent<TEventType>();
        }

        #endregion
    }
}
