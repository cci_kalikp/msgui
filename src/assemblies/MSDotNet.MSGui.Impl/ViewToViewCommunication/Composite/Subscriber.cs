﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2011 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/ViewToViewCommunication/Composite/Subscriber.cs#2 $
// $Change: 896777 $
// $DateTime: 2014/09/12 17:18:03 $
// $Author: hrechkin $

using System;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Presentation.Events;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.My;

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication
{
    internal class Subscriber<TMessage> : IDisposable, ISubscriber<TMessage>, IObjectSubscriber, ILocationHolder, IInternalSubscriber<TMessage>
    {
        private static readonly IMSLogger _log = MSLoggerFactory.CreateLogger<Subscriber<TMessage>>();
        private readonly IEventAggregator _eventAggregator;
        private readonly CompositePresentationEvent<TMessage> _msgArriveEvent;
        private readonly bool _receiveLastSentMessage;
        private EventRouter _eventRouter;

        internal Subscriber(EventRouter eventRouter, bool receiveLastSentMessage)
        {
            _eventRouter = eventRouter;
            _receiveLastSentMessage = receiveLastSentMessage;
            _eventAggregator = new EventAggregator();

            _msgArriveEvent = _eventAggregator.GetEvent<CompositePresentationEvent<TMessage>>();
            _eventRouter.Subscribe(this);
        }

        internal bool ReceiveLastSentMessage
        {
            get { return _receiveLastSentMessage; }
        }

        #region Implementation of ISubscriber<out TMessage>

        public void Subscribe(Action<TMessage> messageHandler)
        {
            if (_eventRouter == null)
            {
                throw new InvalidOperationException("This subscriber has been deregistered");
            }
            if (messageHandler == null)
            {
                throw new ArgumentNullException("messageHandler");
            }

            var observable = GetObservable();
            observable.Subscribe(messageHandler);
        }

        private Action GetUnsubscribeAction(IObserver<TMessage> observer)
        {
            SubscriptionToken token;
            lock (_msgArriveEvent)
            {
                //TODO: authentication & authorization
                token = _msgArriveEvent.Subscribe(observer.OnNext,
                                                   ShellModeExtension.enableParallelBasedEventing
                                                       ? ThreadOption.BackgroundThread
                                                       : ThreadOption.UIThread, true);
            }

            return () => _msgArriveEvent.Unsubscribe(token);
        }

        public IObservable<TMessage> GetObservable()
        {

            if (_eventRouter == null)
            {
                throw new InvalidOperationException("This subscriber has been deregistered");
            }

            return Async(Observable.Create((Func<IObserver<TMessage>, Action>)GetUnsubscribeAction).Publish().RefCount());
        }

        private static IObservable<TMessage> Async(IObservable<TMessage> observable)
        {
            return Observable.Create<TMessage>(observer =>
                {
                    IDisposable subscription;
                    if (ShellModeExtension.enableParallelBasedEventing)
                    {
                        subscription =
                            observable.Subscribe(value => QueueAsyncNext(observer, value));
                    }
                    else
                    {
                        subscription = observable.Subscribe(value => AsyncNext(observer, value));
                    }

                    return subscription.Dispose;
                });
        }

        private static bool QueueAsyncNext(IObserver<TMessage> observer, TMessage value)
        {
            return ThreadPool.QueueUserWorkItem(o => AsyncNext(observer, value));
        }

        private static void AsyncNext(IObserver<TMessage> observer, TMessage value)
        {
            try
            {
                observer.OnNext(value);
            }
            catch (Exception ex)
            {
                _log.Error("Error while asynchronously publishing event", ex, "Async");
            }
        }

        #endregion

        void IInternalSubscriber<TMessage>.Receive(TMessage message)
        {
            _msgArriveEvent.Publish(message);
        }

        #region Implementation of IDisposable

        public void Dispose()
        {
            if (_eventRouter == null)
            {
                throw new InvalidOperationException("This subscriber has already been deregistered");
            }

            _eventRouter.RemoveSubscriber(this);
            _eventRouter = null;
        }

        #endregion

        #region Implementation of IObjectSubscriber

        Type IObjectSubscriber.GetMessageType()
        {
            return typeof (TMessage);
        }

        void IObjectSubscriber.Receive(object message)
        {
            if (message is TMessage)
            {
                ((IInternalSubscriber<TMessage>)this).Receive((TMessage) message);
            }
        }

        #endregion


        public CafGuiApplicationInfoProxy Application { get; set; }
        public string Tab { get; set; }
    }
}
