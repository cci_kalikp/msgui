﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Composite.Presentation.Events;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication;

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication
{
    internal interface IViewToViewCommunicationEvent<TMessage>
    {
        bool Contains(Action<ViewToViewAggregateMessage<TMessage>> subscriber);
        void Publish(ViewToViewAggregateMessage<TMessage> message);
        void InternalSubscribe(Action<ViewToViewAggregateMessage<TMessage>> action, MSDesktopThreadOption threadOption,
                                      bool keepSubscriberReferenceAlive);

        void PublishToExternal(ViewToViewAggregateMessage<TMessage> payload);
    }

    public class ViewToViewCommunicationEvent<TMessage> : CompositePresentationEvent<ViewToViewAggregateMessage<TMessage>>, IViewToViewCommunicationEvent<TMessage>
    {
        private readonly CompositePresentationEvent<ViewToViewAggregateMessage<TMessage>> _internal;

         public ViewToViewCommunicationEvent()
         {
             _internal = Activator.CreateInstance<CompositePresentationEvent<ViewToViewAggregateMessage<TMessage>>>();
             
         }

         internal void PublishToInternal(ViewToViewAggregateMessage<TMessage> payload)
         {
             _internal.Publish(payload);
         }

         void IViewToViewCommunicationEvent<TMessage>.PublishToExternal(ViewToViewAggregateMessage<TMessage> payload)
         {
             base.Publish(payload);
         }

         public override void Publish(ViewToViewAggregateMessage<TMessage> payload)
         {
             //PublishToExternal(payload);
             PublishToInternal(payload);
         }

        #region Implementation of IViewToViewCommunicationEvent<TMessage>

        void IViewToViewCommunicationEvent<TMessage>.InternalSubscribe(Action<ViewToViewAggregateMessage<TMessage>> action, MSDesktopThreadOption threadOption, bool keepSubscriberReferenceAlive)
        {
            Microsoft.Practices.Composite.Presentation.Events.ThreadOption to;
            Enum.TryParse(threadOption.ToString(), out to);
            _internal.Subscribe(action, to, keepSubscriberReferenceAlive);
        }

        #endregion
    }

    
}
