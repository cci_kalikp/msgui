﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging;

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication.Composite
{
    public class CompositeV2VConfigModule: IModule
    {
         private V2VConfig _v2vConfig;

         public CompositeV2VConfigModule(IpcModuleLoadHacker loader, IChromeManager chromeManager, IChromeRegistry chromeRegistry,
            InterProcessMessenger ipc, IPersistenceService persistenceService, IMessageTypeRegistrator typeRegistrator)
        {
            _v2vConfig = new V2VConfig(loader, chromeManager, chromeRegistry,ipc,persistenceService, typeRegistrator);
        }

        #region Implementation of IModule

        /// <summary>
        /// Notifies the module that it has be initialized.
        /// </summary>
        public void Initialize()
        {
            _v2vConfig.Initialize();
        }

        #endregion

        public bool ConfigDialogFactory(IDialogWindow dialog_)
        {
            return _v2vConfig.ConfigDialogFactory(dialog_);
        }
        public void SignUpAutoHook(IWindowViewContainer viewContainer, Action<IEnumerable<IWindowViewContainer>> hookupCallback, Action<IEnumerable<IWindowViewContainer>> unhookedCallback)
        {
            _v2vConfig.SignUpAutoHook(viewContainer, hookupCallback, unhookedCallback);
        }


        public void SignOffAutoHook(IWindowViewContainer viewContainer)
        {
            _v2vConfig.SignOffAutoHook(viewContainer);
        }

        public AutoHookMode AutoHookMode
        {
            get
            {
                return _v2vConfig.AutoHookMode;
            }
            set { _v2vConfig.AutoHookMode = value; }
        }
    }
}
