﻿using System;
using System.Collections.Generic;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication
{
    internal class EventAggregatorRegistrator: IMSDesktopEventAggregator, IIPCEventAggregator
    {
        private readonly IMSDesktopEventAggregator _eventAggregator;

        private readonly Dictionary<Type, object> _viewToViewEvents = new Dictionary<Type, object>();
        private readonly Dictionary<Type, object> _moduleToModuleEvents = new Dictionary<Type, object>();

        public EventAggregatorRegistrator(IMSDesktopEventAggregator msDesktopEventAggregator)
        {
            _eventAggregator = msDesktopEventAggregator;
        }

        private TEventType GetViewToViewEvent<TEventType>()
        {
            var msgType = typeof (TEventType).GetGenericArguments()[0];
            if (!_viewToViewEvents.ContainsKey(msgType))
                _viewToViewEvents[msgType] =
                    Activator.CreateInstance<TEventType>();

            return (TEventType)_viewToViewEvents[msgType];
        }

        private TEventType GetModuleToModuleEvent<TEventType>()
        {
            var msgType = typeof(TEventType).GetGenericArguments()[0];
            if (!_moduleToModuleEvents.ContainsKey(msgType))
                _moduleToModuleEvents[msgType] =
                    Activator.CreateInstance<TEventType>();

            return (TEventType)_moduleToModuleEvents[msgType];
        }

        public TEventType GetEvent<TEventType>()
        {
            if(!typeof(TEventType).IsGenericType)
                return _eventAggregator.GetEvent<TEventType>();
           
            if (typeof(TEventType).GetGenericTypeDefinition().Name == "ViewToViewCommunicationEvent`1")
                return GetViewToViewEvent<TEventType>();

            if (typeof(TEventType).GetGenericTypeDefinition().Name == "ModuleToModuleMessageEvent`1")
                return GetModuleToModuleEvent<TEventType>();

            return _eventAggregator.GetEvent<TEventType>();
        }


    }
}
