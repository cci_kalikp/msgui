﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Impl.Application;


namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication
{
    static class  V2VMessageEventHelper
    {
        private static IViewToViewCommunicationEvent<TMessage> GetPrismEvent<TMessage>(this IMSDesktopEventAggregator msDesktopEventAggregator)
        {
            return msDesktopEventAggregator.GetEvent<MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication.Prism.ViewToViewCommunicationEvent<TMessage>>();
        }

        private static IViewToViewCommunicationEvent<TMessage> GetCompositeEvent<TMessage>(this IMSDesktopEventAggregator msDesktopEventAggregator)
        {
            return msDesktopEventAggregator.GetEvent<MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication.ViewToViewCommunicationEvent<TMessage>>();
            
        }

        public static IViewToViewCommunicationEvent<TMessage> GetViewToViewEvent<TMessage>(this IMSDesktopEventAggregator msDesktopEventAggregator)
        {
            if (BootstrapperBase.UsePrismAsInfrasturcture)
            {
                return msDesktopEventAggregator.GetPrismEvent<TMessage>();
            }
            else
            {
                return msDesktopEventAggregator.GetCompositeEvent<TMessage>();

            }
        }
    }
}
