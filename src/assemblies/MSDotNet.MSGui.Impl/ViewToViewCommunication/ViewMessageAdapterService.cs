﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2011 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/ViewToViewCommunication/ViewMessageAdapterService.cs#5 $
// $Change: 848872 $
// $DateTime: 2013/10/09 12:54:27 $
// $Author: hrechkin $

using System;
using System.Collections.Generic;
using System.Linq;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication
{
    /// <summary>
    /// Manages adapters that can convert between message types
    /// </summary>
    public class ViewMessageAdapterService : IAdapterService
    {
        /// <summary>
        /// Map of source types to collection of corresponding destination types
        /// </summary>
        private readonly IDictionary<Type, IDictionary<Type, Delegate>> _conversions =
            new Dictionary<Type, IDictionary<Type, Delegate>>();

        #region Implementation of IAdapterService

        public void AddAdapter<TSource, TDestination>(Func<TSource, TDestination> adapter)
        {
            IDictionary<Type, Delegate> destinations;
            if (!_conversions.TryGetValue(typeof (TSource), out destinations))
            {
                destinations = new Dictionary<Type, Delegate>();
                _conversions.Add(typeof (TSource), destinations);
            }

            destinations[typeof (TDestination)] = adapter;
        }

        public bool RemoveAdapter<TSource, TDestination>(Func<TSource, TDestination> adapter)
        {
            IDictionary<Type, Delegate> destinations;
            return _conversions.TryGetValue(typeof (TSource), out destinations)
                   && destinations.Remove(typeof (TDestination));
        }

        public TDestination ApplyAdapterChain<TSource, TDestination>(TSource sourceMessage)
        {
            return (TDestination) ApplyAdapterChain(typeof (TSource), typeof (TDestination), sourceMessage);
        }


        public object ApplyAdapterChain(Type sourceType, Type destinationType, object message)
        {


            if (sourceType == destinationType)
            {
                return message;
            }

            var path = GetAdaptionPath(sourceType, destinationType);

            foreach (var pair in path)
            {
                message = pair.Value.DynamicInvoke(message);
            }

            return message;
        }


        #endregion


        private IEnumerable<KeyValuePair<Type, Delegate>> GetAdaptionPath(Type sourceType, Type destinationType)
        {
            if (sourceType == destinationType)
            {
                return new KeyValuePair<Type, Delegate>[] {};
            }

            IDictionary<Type, Delegate> adapters;
            if (!_conversions.TryGetValue(sourceType, out adapters))
            {
                return null;
            }

            var level = new Queue<List<KeyValuePair<Type, Delegate>>>();
            var start = new List<KeyValuePair<Type, Delegate>>();

            start.Insert(0, new KeyValuePair<Type, Delegate>(sourceType, null));
            level.Enqueue(start);

            var allTypes = new HashSet<Type>();

            while (level.Count > 0)
            {
                var path = level.Dequeue();
                var node = path[path.Count - 1];
                if (node.Key == destinationType)
                {
                    if (path.Count > 0)
                    {
                        path.RemoveAt(0);
                    }

                    return path;
                }

                if (allTypes.Contains(node.Key))
                {
                    continue;
                }
                allTypes.Add(sourceType);

                if (!_conversions.TryGetValue(node.Key, out adapters))
                {
                    continue;
                }

                foreach (var next in adapters)
                {
                    var n = new KeyValuePair<Type, Delegate>(next.Key, next.Value);
                    var np = path.ToList();
                    np.Add(n);
                    level.Enqueue(np);
                }

            }

            return null;
        }

        /// <summary>
        /// Determines whether adaptation is possible between types.
        /// </summary>
        /// <param name="sourceType">The source type.</param>
        /// <param name="destinationType">The destination type.</param>
        /// <returns>
        ///   <c>true</c> if adaptation is possible between types; otherwise, <c>false</c>.
        /// </returns>
        public bool IsAdaptationPossible(Type sourceType, Type destinationType)
        {
            //TODO: make auto-composition work like with M2M2
            /**
      IDictionary<Type, Delegate> destinations;
      return _conversions.TryGetValue(sourceType_, out destinations)
             && destinations.ContainsKey(destinationType_);
         */

            bool result;
            if (sourceType == destinationType)
            {
                result = true;
            }
            else
            {
                result = (GetAdaptionPath(sourceType, destinationType) != null);
            }

            return result;
        }

        public TDestination ApplyAdapterChain<TSource, TDestination>(TSource sourceMessage, TDestination destinationDummy)
        {
            return ApplyAdapterChain<TSource, TDestination>(sourceMessage);
        }
    }
}

