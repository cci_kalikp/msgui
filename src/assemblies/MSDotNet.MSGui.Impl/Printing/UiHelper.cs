﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows;

namespace MorganStanley.MSDotNet.MSGui.Impl.Printing
{
  public class UiHelper
  {
    // @ Summary
    //   Stretch/Tile/Center 
    // @ Comment 
    //   Stretch?
    public static void UpdateSize(FrameworkElement element, double avaiableWidth)
    {
      var vbox = new Viewbox { Child = element };
      vbox.Measure(new Size(avaiableWidth, 20000));
      vbox.Arrange(new Rect(0, 0, avaiableWidth, 2000));
      vbox.UpdateLayout();
    }


    // @ Summary
    //   Stretch/Tile/Center 
    // @ Comment 
    //   Center
    // @TODO
    //   test
    public static void Center(FrameworkElement element, double availableWidth)
    {
      var vbox = new Viewbox { Child = element };
      vbox.Measure(new Size(element.ActualWidth, element.ActualWidth));
      vbox.Arrange(new Rect(Math.Max(Math.Min(availableWidth, 20000) - element.ActualWidth, 0), 0, availableWidth, 20000));
      vbox.UpdateLayout();
    }

    public static void Title(FrameworkElement element, double availableWidth)
    {
      throw new NotImplementedException();
    }
  }
}
