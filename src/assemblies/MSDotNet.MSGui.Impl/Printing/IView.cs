﻿using MorganStanley.MSDotNet.MSGui.Core.Printing;

namespace MorganStanley.MSDotNet.MSGui.Impl.Printing
{
    internal interface IView
    {
        IViewModel ViewModel { set; }
    }
}
