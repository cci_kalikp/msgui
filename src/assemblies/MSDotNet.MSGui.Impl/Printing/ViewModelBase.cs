﻿using System;
using System.Text;
using System.Windows;

namespace MorganStanley.MSDotNet.MSGui.Impl.Printing
{
  // @ViewModel base of views from Printing namespace.
  internal abstract class ViewModelBase : DependencyObject, IViewModel
  {

    protected ViewModelBase(IView view)
    {
      View = view;
      view.ViewModel = this;
    }

    public IView View
    {
      get { return (IView)GetValue(ViewProperty); }
      set { SetValue(ViewProperty, value); }
    }

    // Using a DependencyProperty as the backing store for View.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty ViewProperty =
        DependencyProperty.Register("View", typeof(IView), typeof(ViewModelBase), new UIPropertyMetadata(null));



  }
}
