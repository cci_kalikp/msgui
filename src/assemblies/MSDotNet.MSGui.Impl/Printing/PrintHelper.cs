﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using System.Printing;
using System.Drawing.Printing;
using System.Xml;
using System.Windows.Media;
using System.Windows;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.My;
using MorganStanley.MSDotNet.MSGui.Core;
using System.Windows.Media.Imaging;
using System.ComponentModel;
using System.Reflection;

namespace MorganStanley.MSDotNet.MSGui.Impl.Printing
{
  public static class PrintHelper
  {
    // Cannot use MSLoggerFactory.CreateLogger<PrintHelper>();
    // static type can not be used in generic type parameters
    private static readonly IMSLogger m_logger = MSLoggerFactory.CreateLogger();

    public static void Print(IPrintImage image)
    {
      throw new NotImplementedException();
    }

    internal static IPrintImage FromVisual()
    {
      throw new NotImplementedException();
    }

    internal static IPrintImage FromGeneral(object obj)
    {
      throw new NotImplementedException();
    }


    /// <summary>
    /// Get Printers
    /// </summary>
    /// <returns><see cref="PrintQueueCollection"/></returns>
    internal static PrintQueueCollection GetPrinters()
    {
      return GetPrinters(null);
    }

    /// <summary>
    /// Get Default Printer
    /// </summary>
    /// <returns><see cref="PrintQueue"/></returns>
    internal static PrintQueue GetDefaultPrinter()
    {
      return LocalPrintServer.GetDefaultPrintQueue();
    }

    /// <summary>
    /// Get the Printers
    /// </summary>
    /// <param name="enumeratedTypes">[opptinal] lists of Print Queue types </param>
    /// <returns><see cref="PrintQueueCollection"/></returns>
    internal static PrintQueueCollection GetPrinters(params EnumeratedPrintQueueTypes[] enumeratedTypes)
    {
      if (enumeratedTypes == null || enumeratedTypes.Length == 0)
      {
        enumeratedTypes = new[] { EnumeratedPrintQueueTypes.Connections, EnumeratedPrintQueueTypes.Local };
      }

      var printServer = new PrintServer();
      var printers = printServer.GetPrintQueues(enumeratedTypes);
      return printers;

    }

    internal static PrintQueueCollection m_printers;

    public static PrintQueueCollection Printers
    {
      get
      {
        if (m_printers == null)
        {
          m_printers = GetPrinters();
        }
        return m_printers;
      }
    }

    // @summary
    //   this is a in-memory cache of the printer settings which is indexed by the Printers name
    // @Comment
    //   may not be directly useful in version@1
    private static Dictionary<string, PrinterSettings> m_printerSettings = new Dictionary<string, PrinterSettings>();
    internal static PrinterSettings GetPrintSettings(string currentPrinterName)
    {
      if (string.IsNullOrEmpty(currentPrinterName)) throw new ArgumentException("currentPrinterName");

      if (m_printerSettings.ContainsKey(currentPrinterName))
      {
        return m_printerSettings[currentPrinterName];
      }
      return null;
    }

    internal static Dictionary<string, PrinterSettings> PrinterSettings
    {
      get { return m_printerSettings; }
    }

    // @Comment:Get the size of the printing, based on arguments of 1. PaperSize 2. PageOrientation
    // 
    public static Size GetPrintSize(PaperSize paperSize, PageOrientation pageOrientation)
    {
      var printSize = new Size(paperSize.Width, paperSize.Height);
      if (pageOrientation == PageOrientation.Landscape)
      {
        printSize = new Size(paperSize.Height, paperSize.Width);
      }
      return printSize;
    }

    // @Comment: Scale the &lt;paramref name="drawing&gt; 
    public static DrawingVisual GetScaledVisual(DrawingVisual drawingVisual, double scale)
    {
      if (drawingVisual == null) throw new ArgumentNullException("drawingVisual");

      if (scale == 1) return drawingVisual;

      var visual = new DrawingVisual();
      using (var dc = visual.RenderOpen())
      {
        dc.PushTransform(new ScaleTransform(scale, scale));
        dc.DrawDrawing(drawingVisual.Drawing);
      }
      return visual;
    }

    // @Comment: Scale the &lt;paramref name="drawingVisuals&gt; 
    public static IEnumerable<DrawingVisual> GetScaledVisuals(IEnumerable<DrawingVisual> drawingVisuals, double scale)
    {
      if (drawingVisuals == null) throw new ArgumentNullException("drawingVisuals");

      foreach (var visual in drawingVisuals)
      {
        yield return PrintHelper.GetScaledVisual(visual, scale);
      }
    }


    // @Comment: Get the Printer's Margin settings
    // @TODO: Cache the settings
    public static Thickness GetPageMargin(string currentPrinterName)
    {
      // TODO:
      if (string.IsNullOrEmpty(currentPrinterName)) throw new ArgumentNullException("currentPrinterName");
      if (Printers != null && Printers.FirstOrDefault(printer => printer.FullName == currentPrinterName) == null) throw new ArgumentException("Printer's fullname is expected");

      float hardMarginX;
      float hardMarginY;


      var printerSetting = new PrinterSettings() { PrinterName = currentPrinterName };

      try
      {
        hardMarginX = printerSetting.DefaultPageSettings.HardMarginX;
        hardMarginY = printerSetting.DefaultPageSettings.HardMarginY;
      }
      catch (Exception ex)
      {
        hardMarginX = 0;
        hardMarginY = 0;
        m_logger.Warning("DefaultPageSettings failed. ", ex);
      }

      return new Thickness(hardMarginX + 5, hardMarginY + 5, printerSetting.DefaultPageSettings.Margins.Right, printerSetting.DefaultPageSettings.Margins.Bottom + 50);

    }

    //@Comment: find a matching PageSize given a list of PageSize and one System.Drawing.Print.PageMediaSize
    //PrintQueue.UserTicket.PageMediaSize --> PaperSize -- consumed by --> VisualPaginator
    //@param rotated: this is set to true if the PaperSize returned is a rotated one of the param pagemediaSize;
    public static PaperSize FindMatchingPageMediaSize(PageMediaSize pageMediaSize, IList<PaperSize> paperSizes, out bool rotated)
    {
      if (pageMediaSize == null) throw new ArgumentNullException("pageMediaSize");
      if (paperSizes == null) throw new ArgumentNullException("paperSizes");

      rotated = false;
      
      var widthInInch = Math.Round(pageMediaSize.Width.Value / 96 * 100);
      var heightInInch = Math.Round(pageMediaSize.Height.Value / 96 * 100);

      var paperSize = paperSizes.FirstOrDefault(p => p.Width == widthInInch && p.Height == heightInInch);
      if (paperSize == null)
      {
        paperSize = paperSizes.FirstOrDefault(p => p.Width == heightInInch && p.Height == widthInInch);
        if (paperSize != null) rotated = true;
      }

      if (paperSize != null)
        return paperSize;

      return null;
    }

    //@Comment: Given a printer name, find the paper sizes that it supports
    //@TODO: cache
    public static IList<PaperSize> GetPaperSizes(string printerName)
    {
      var paperSizes = new List<PaperSize>();

      var printerSettings = new PrinterSettings { PrinterName = printerName };

      var sizes = printerSettings.PaperSizes;
      foreach (var ps in sizes)
      {
        if (((PaperSize)ps).PaperName != "Custom Size")
        {
          paperSizes.Add(((PaperSize)ps));
        }
      }

      return paperSizes;
    }


    public static bool IsPageMediaSizeSupported(PrintQueue printer, PageMediaSize pageMediaSize)
    {
      if (printer == null) throw new ArgumentNullException("printer");
      if (pageMediaSize == null) throw new ArgumentNullException("pageMediaSize");

      var capabilities = printer.GetPrintCapabilities();
      if (capabilities.PageMediaSizeCapability.FirstOrDefault(media => media.PageMediaSizeName == pageMediaSize.PageMediaSizeName) != null)
      {
        return true;
      }
      return false;
    }


    public static PageMediaSize GetPageMediaSizeByName(PrintQueue printer, PageMediaSizeName? pageMediaSizename)
    {
      if (printer == null) throw new ArgumentNullException("printer");

      var capabiltiies = printer.GetPrintCapabilities();
      if (capabiltiies != null)
      {
        var pageMediasize = capabiltiies.PageMediaSizeCapability.FirstOrDefault(cap => cap.PageMediaSizeName == pageMediaSizename);
        return pageMediasize;
      }

      return null; 
    }


    // @Summary
    //   Xps Printer Helper
    public static class XpsPrinterHelper
    {
      // @Summary
      //  Get the Input Bin options 
      // @Comment
      //   TODO:
      //      1. Cache the Input Bing associated with each PrintQueue 
      public static Dictionary<string, string> GetInputBins(PrintQueue printQueue)
      {
        var inputBins = new Dictionary<string, string>();

        var printerCapXmlStream = printQueue.GetPrintCapabilitiesAsXml();

        var xmlDoc = new XmlDocument();
        xmlDoc.Load(printerCapXmlStream);

        var manager = new XmlNamespaceManager(xmlDoc.NameTable);
        manager.AddNamespace(xmlDoc.DocumentElement.Prefix, xmlDoc.DocumentElement.NamespaceURI);

        var nodeList = xmlDoc.SelectNodes("//psf:Feature[@name='psk:JobInputBin']/psf:Option", manager);

        foreach (XmlNode node in nodeList)
        {
          inputBins.Add(node.LastChild.InnerText, node.Attributes["name"].Value);
        }

        return inputBins;
      }


    }
  }

  // @Summary
  //   Factory to print a control
  // @Comment
  //   the intpu to PrintControlFactory
  // @Note
  //  ReachFramework.dll
  public class PrintControlFactory
  {
    public static DrawingVisual BuildGraphVisual(PageMediaSize pageSize, Visual visual)
    {
      var drawingVisual = new DrawingVisual();
      using (var drawingContext = drawingVisual.RenderOpen())
      {
        var visualContent = visual;
        var rect = new Rect
        {
          X = 0,
          Y = 0,
          Width = pageSize.Width.Value,
          Height = pageSize.Height.Value
        };

        var strech = Stretch.None;
        var visualBrush = new VisualBrush(visualContent) { Stretch = strech };
        drawingContext.DrawRectangle(visualBrush, null, rect);
        // see http://msdn.microsoft.com/en-us/library/ms743320(v=vs.90).aspx 
        // for the Opacity mask 
        drawingContext.PushOpacityMask(Brushes.White);
      }

      return drawingVisual;
    }

    // @return a instance of IPrintImage
    public static IPrintImage CreatePrintImage(IWindowViewContainer container_, InitialPrintParameters printParameters_)
    {
      if (container_ == null) throw new ArgumentNullException("container_");

      if (container_.Content != null)
      {
        var hwndHost = container_.Content as System.Windows.Interop.HwndHost;
        if (hwndHost != null)
        {
          var drawingVisual = PrintControlFactory.CreateDrawingVisualViaImageSource(hwndHost, hwndHost.ActualWidth, hwndHost.ActualHeight);
          return new PrintImage(hwndHost) { DrawingVisual = drawingVisual };
        }
        var content = container_.Content as FrameworkElement;
        if (content != null)
        {
          var drawingVisual = PrintControlFactory.CreateDrawingVisual(content, content.ActualWidth, content.ActualHeight);
          return new PrintImage(content) { DrawingVisual = drawingVisual };
        }

        //TODO: Test
        var control = container_.Content as System.Windows.Forms.Control;
        if (control != null)
        {
          var drawingVisual =  PrintControlFactory.CreateDrawingVisual(control, control.Width, control.Height);
          return new PrintImage(control) { DrawingVisual = drawingVisual };
        }
      }
      return null;
    }



    /// <summary>
    /// Create <see cref="System.Windows.Media.DrawingVisual"/> from <see cref="System.Windows.FrameworkElement"/>
    /// </summary>
    /// <param name="visual"></param>
    /// <param name="width"></param>
    /// <param name="height"></param>
    /// <returns>The <see cref="System.Windows.Media.DrawingVisual"/> </returns>
    /// <remarks>
    /// if the background is not specified, the created DrawingVisual render without the background,
    /// though you can see background is inherited from the theme settings;
    /// </remarks>
    public static DrawingVisual CreateDrawingVisual(FrameworkElement visual, double width, double height)
    {
      var drawingVisual = new DrawingVisual();
      var background = GetEffectiveDependencyPropertyValue<Brush>("Background", visual);

      // open the Render of the DrawingVisual
      using (var dc = drawingVisual.RenderOpen())
      {
        var vb = new VisualBrush(visual) { Stretch = Stretch.None };
        var rectangle = new Rect
        {
          X = 0,
          Y = 0,
          Width = width,
          Height = height,
        };

        // draw the white background if no background is present
        if (background == null)
          dc.DrawRectangle(Brushes.White, null, rectangle);
        else
          dc.DrawRectangle(background, null, rectangle);

        // draw the visual
        dc.DrawRectangle(vb, null, rectangle);
      }
      return drawingVisual;
    }

    /// <summary>
    /// Get the effective property from the Dependency Property and the Metadata system
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="propertyName"></param>
    /// <param name="element"></param>
    /// <returns></returns>
    public static T GetEffectiveDependencyPropertyValue<T>(string propertyName, FrameworkElement element)
    {
      if (element == null) throw new ArgumentNullException("element");
      if (propertyName == null) throw new ArgumentNullException("propertyName");

      var visual = (DependencyObject)element;
      while (visual != null)
      {
        var propertyDescriptor = DependencyPropertyDescriptor.FromName(propertyName, visual.GetType(), visual.GetType(), true);
        if (propertyDescriptor != null)
        {
          var propertyValue = propertyDescriptor.GetValue(visual);
          if (propertyValue != null)
          {
            return (T)propertyValue;
          }
        }
        visual = LogicalTreeHelper.GetParent(visual);
      }

      return default(T); 
    }

     
    public static DrawingVisual CreateDrawingVisualViaImageSource(Visual visual, double width, double height)
    {
      return ImageSourceToDrawingVisual(new DrawingImage(VisualTreeHelper.GetDrawing(visual)), width, height);
    }

    public static DrawingVisual CreateDrawingVisual(System.Windows.Forms.Control control, double width, double height)
    {
      return ImageSourceToDrawingVisual(CreateImageSource(control, width, height), width, height);
    }

      public static ImageSource CreateImageSource(System.Windows.Forms.Control control, double width, double height)
      {
          System.Drawing.Bitmap b = new System.Drawing.Bitmap((int) width, (int) height);
          control.DrawToBitmap(b, new System.Drawing.Rectangle(0, 0, (int) width, (int) height));
          return b.ToImageSource(true);
      }

      public static DrawingVisual ImageSourceToDrawingVisual(System.Windows.Media.ImageSource imageSource, double width, double height)
    {
      var drawingVisual = new DrawingVisual();
      // you can create the ImageBrush
      //var imageBrush = new ImageBrush(imageSource);
      using (var dc = drawingVisual.RenderOpen())
      {
        var rectangle = new Rect
        {
          X = 0,
          Y = 0,
          Width = width,
          Height = height,
        };

        // draw the white background
        dc.DrawRectangle(Brushes.White, null, rectangle);
        // if you create the ImageBrush, you can pain with the brush 
        //dc.DrawRectangle(imageBrush, null, rectangle);
        dc.DrawImage(imageSource, rectangle);
      }

      return drawingVisual;
    }

  }

}
