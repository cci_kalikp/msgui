﻿using System.Windows;
using System.Windows.Input;
using MorganStanley.MSDotNet.MSGui.Impl.Printing;

namespace MorganStanley.MSDotNet.MSGui.Impl.Printing.Controls.ProgressDialog
{
    internal interface IProgressDialog : IWaitScreen
    {
        string DialogTitle { get; set; }
        string CancelButtonCaption { get; set; }

        double MaxProgressValue { get; set; }
        double CurrentProgressValue { get; set; }
        
        Visibility CancelButtonVisibility { get; set; }
        ICommand CancelCommand { get; set; }
        void Initialize(ICommand cancelCommand, int maxProgressValue);
    }
}
