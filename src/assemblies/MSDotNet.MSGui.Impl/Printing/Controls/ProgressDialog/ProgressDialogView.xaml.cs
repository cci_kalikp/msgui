﻿using System.Windows.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.Printing;

namespace MorganStanley.MSDotNet.MSGui.Impl.Printing.Controls.ProgressDialog
{
    /// <summary>
    /// Interaction logic for ProgressDialogView.xaml
    /// </summary>
    internal partial class ProgressDialogView : UserControl, IProgressDialogView
    {
        public ProgressDialogView()
        {
            InitializeComponent();
        }

        private IViewModel m_presenter;
        public IViewModel ViewModel
        {
            set
            {
                m_presenter = value as IViewModel;
                DataContext = m_presenter;
            }
        }
    }
}