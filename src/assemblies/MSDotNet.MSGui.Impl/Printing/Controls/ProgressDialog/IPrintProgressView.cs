﻿using System.Windows;
using System.Windows.Input;

namespace MorganStanley.MSDotNet.MSGui.Impl.Printing
{
  /// <summary>
  /// Interface that show the progress of the printing..
  /// </summary>
  internal interface IPrintProgressView : IWaitScreenView
  {
  }
}
