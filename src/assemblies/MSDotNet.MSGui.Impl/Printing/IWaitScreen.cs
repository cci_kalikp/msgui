﻿namespace MorganStanley.MSDotNet.MSGui.Impl.Printing
{
  /// <summary>
  /// Wait screen Interface
  /// </summary>
  /// <remarks>
  /// General in other scenario?</remarks>
  internal interface IWaitScreen : IViewModel
  {
    bool Hide();
    bool Show();
    bool Show(string message);
    bool Show(string message, bool disableParent);
    string Message { get; set; }
    void HideNow();
  }
}
