﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Impl.Printing
{
  /// <summary>
  /// MSDestkop Print Exception
  /// </summary>
  public class PrintException : Exception
  {
    public PrintException(string description) : this(description, null) { }
    public PrintException(string description, Exception innerException) : base(description, innerException) { }
  }
}
