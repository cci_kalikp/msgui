﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core.Printing;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using System.Drawing;
using MorganStanley.MSDotNet.MSGui.Impl.Printing.Previews;
using System.Windows.Input;
using Microsoft.Practices.Composite.Presentation.Commands;
using System.Windows.Documents;
using MorganStanley.MSDotNet.MSGui.Impl.Printing.Controls.ProgressDialog;
using System.Windows;
using System.Printing;
using System.Drawing.Printing;
using MorganStanley.MSDotNet.MSGui.Impl.Printing;
using System.Windows.Media;
using MorganStanley.MSDotNet.MSGui.Impl.Paginators;
using System.Diagnostics;
using System.Windows.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.Printing.Utils.Extensions;
using Rectangle = System.Windows.Shapes.Rectangle;
using Brushes = System.Windows.Media.Brushes;
using Brush = System.Windows.Media.Brush;
using MorganStanley.MSDotNet.MSGui.Impl.Printing.Controls.WaitScreen;
using Size = System.Windows.Size;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeManager
{
  // TODO
  //  extract the functionality of ChromeManager as IPrintManager and move it to a separate class
  public partial class ChromeManagerBase : IPrintManager, IPrintController, IViewModel
  {

    #region IPrintManager
    public void Print(IWindowViewContainer windowContainer_, InitialPrintParameters printParameter_ = null)
    {
      var printImage = ChecksPrintImageAndThrows(InternalPrint(windowContainer_, printParameter_));
      DrawingVisuals = new List<DrawingVisual>(new DrawingVisual[] { GetDrawingVisual(printImage) });
      ExecutePrint(null);
    }

    public void PrintAll(InitialPrintParameters printParameter_ = null)
    {
      DrawingVisuals = WindowsToDrawingVisuals(printParameter_, false);
      ExecutePrint(null);
    }

    private List<DrawingVisual> WindowsToDrawingVisuals(InitialPrintParameters printParameter_, bool preview)
    {
      var drawingVisuals = new List<DrawingVisual>();
      //var predicate = ((Func<IWindowViewContainer, bool>) (w => {
      //    if (preview)
      //    {
      //      return w.PrintHandler != null || w.PreviewPrintHandler != null;
      //    }
      //    return w.PrintHandler != null;
      //}));

      var imageFactory = ((Func<IWindowViewContainer, IPrintImage>)(w =>
      {
        if (preview)
          return InternalPreviewPrint(w, printParameter_);
        else
          return InternalPrint(w, printParameter_);
      }));
      var printImages = ChecksPrintImagesAndThrows(from w in GetViews() where IsWindowPrintable(w) select imageFactory(w));
      drawingVisuals.AddRange(from p in printImages select GetDrawingVisual(p));
      return drawingVisuals;
    }



    public void PreviewPrint(IWindowViewContainer windowContainer_, InitialPrintParameters printParameter_ = null)
    {
      var printImage = ChecksPrintImageAndThrows(InternalPreviewPrint(windowContainer_, printParameter_));
      DrawingVisuals = new List<DrawingVisual>(new DrawingVisual[] { GetDrawingVisual(printImage) });
      ShowPreview();
    }

    private DrawingVisual GetDrawingVisual(PrintImage printImage)
    {
      if (printImage == null) throw new ArgumentNullException("printImage");
      if (printImage.IsVisualOrImageSource) return printImage.DrawingVisual;
      else return PrintControlFactory.ImageSourceToDrawingVisual(printImage.ImageSource, printImage.ImageSource.Width, printImage.ImageSource.Height);
    }


    public void PreviewPrintAll(InitialPrintParameters printParameter_ = null)
    {
      DrawingVisuals = WindowsToDrawingVisuals(printParameter_, true);
      ShowPreview();
    }

    // Throws conditions including:
    //  1. the IPrintImage is not PrintImage or subclass of PrintImage
    //  2. Only the Visual properties of PrintImage is supported as of the time.
    private PrintImage ChecksPrintImageAndThrows(IPrintImage printImage)
    {
      if (printImage == null) throw new ArgumentNullException("printImage");
      var image = printImage as PrintImage;
      if (image == null)
      {
        // TODO: support any impl class to IPrintImage
        throw new NotSupportedException("Only class PrintImage and its derived are supported");
      }
      if (image.IsVisualOrImageSource && image.DrawingVisual == null) throw new NotSupportedException("DrawingImage not specified");
      // TODO: Support ImageSource types
      if (!image.IsVisualOrImageSource && image.ImageSource == null) throw new NotSupportedException("ImageSource Not specified");

      return image;
    }

    private IEnumerable<PrintImage> ChecksPrintImagesAndThrows(IEnumerable<IPrintImage> printImages)
    {
      if (printImages == null) throw new ArgumentNullException("printImages");

      foreach (var printImage in printImages)
      {
        yield return ChecksPrintImageAndThrows(printImage);
      }
    }

    protected virtual IPrintImage InternalPrint(IWindowViewContainer windowContainer_, InitialPrintParameters printParameter_ = null)
    {
      if (windowContainer_ == null) throw new ArgumentNullException("windowContainer_");
      if (!IsWindowPrintable(windowContainer_)) throw new PrintException("Window is not printable, the window is not visible");

      m_requestedPrintParameters = printParameter_;

      return GetPrintImageOrDefault(windowContainer_, printParameter_);
    }

    protected virtual IPrintImage InternalPreviewPrint(IWindowViewContainer windowContainer_, InitialPrintParameters printParameter_ = null)
    {
      if (windowContainer_ == null) throw new ArgumentNullException("windowContainer_");
      if (!IsWindowPrintable(windowContainer_)) throw new PrintException("Window is not printable, the window is not visible");

      m_requestedPrintParameters = printParameter_;

      return GetPreviewPrintImageOrDefault(windowContainer_, printParameter_);

    }

    #endregion IPrintManager
    
    protected virtual IPrintImage GetPrintImageOrDefault(IWindowViewContainer windowContainer_, InitialPrintParameters printParameter_ = null)
    {
      if (windowContainer_ == null) throw new ArgumentNullException("windowContainer_");
      if (!IsWindowPrintable(windowContainer_)) throw new PrintException("Window is not printable, the window is not visible");

      var print = windowContainer_.PrintHandler;
      if (windowContainer_.PrintHandler != null)
      {
        return print(windowContainer_, printParameter_);
      }
      return PrintControlFactory.CreatePrintImage(windowContainer_, printParameter_);
    }

    protected virtual IPrintImage GetPreviewPrintImageOrDefault(IWindowViewContainer windowContainer_, InitialPrintParameters printParameter_ = null)
    {
      if (windowContainer_ == null) throw new ArgumentNullException("windowContainer_");
      if (!IsWindowPrintable(windowContainer_)) throw new PrintException("Window is not printable, the window is not visible");

      var preview = windowContainer_.PreviewPrintHandler;
      if (preview == null)
      {
        if (windowContainer_.PrintHandler != null)
        {
          preview = (w, p) => windowContainer_.PrintHandler(w, p);
        }
        else
        {
          preview = null;
        }
      }

      if (preview == null)
      {
        var print = windowContainer_.PrintHandler;
        preview = print != null ? (PreviewPrintWindowHandler)((w, p) => print(w, p)) : null;
      }

      if (preview != null)
      {
        return preview(windowContainer_, printParameter_);
      }

      return PrintControlFactory.CreatePrintImage(windowContainer_, printParameter_);
    }

    protected virtual bool IsWindowPrintable(IWindowViewContainer windowContainer_)
    {
      //TODO:
      // we have to check if the window is minized
      if (windowContainer_ != null && windowContainer_.IsVisible && windowContainer_.Content != null) return true;
      return false;
    }

    #region Print InitialParameters
    /// <summary>
    /// Initial Print Parameters
    /// </summary>
    /// <remarks>
    /// A better name would be Default print Settings?</remarks>
    public InitialPrintParameters PrintParameters
    {
      get;
      set;
    }


    private InitialPrintParameters m_requestedPrintParameters; 

    private void InitDefaultPrintParameters()
    {
      PrintParameters = new InitialPrintParameters
      {
      };
    }


    internal virtual void InitPrintParameters(InitialPrintParameters printParameter)
    {
        if (PrintParameters == null || (printParameter != null && PrintParameters != printParameter))
        {
            if (printParameter != null)
                PrintParameters = new InitialPrintParameters(printParameter);
            if (PrintParameters == null) InitDefaultPrintParameters();
            InitializePrintProperties();
        }

        Contract.Ensures(ActivePrinter != null, "Internal Error, ActivePrinter not initialized Properly");
        Contract.Ensures(UserTicket != null, "Internal Error, UserTicket is not initialize properly");
        Contract.Ensures(DrawingVisuals != null, "Internal Error, DrawingVisuals is not initalized");
        Contract.Ensures(WaitScreen != null, "Internal Error, WaitSCreen is not initialized");
        string message;
        Contract.Ensures(ValidatePrintParameters(PrintParameters, out message), String.Format("validation error, {0}", message));
    }

    protected virtual void InitializePrintProperties()
    {
      if (PrintParameters == null) throw new Exception("Internal Error! PrintParameter not initialized");

      m_printers = PrintHelper.Printers;
      m_defaultPrinter = PrintHelper.GetDefaultPrinter();
      SetActivePrinter();
      PrintParameters.PrinterName = m_activePrinter.FullName;

      AddDefaultPrinterIfNotExists();

      if (m_userTicket == null)
        m_userTicket = m_defaultPrinter.DefaultPrintTicket.Clone();

      m_paperSizes = PrintHelper.GetPaperSizes(m_activePrinter.FullName);
      if (PrintParameters.PageOrientation != null && PrintParameters.PageOrientation != PageOrientation.Unknown)
      {
        this.PageOrientation = PrintParameters.PageOrientation ?? m_userTicket.PageOrientation ?? PageOrientation.Portrait; // default Orientation is Portrait
        if (m_userTicket.PageOrientation != this.PageOrientation)
          m_userTicket.PageOrientation = this.PageOrientation;
      }
      else
      {
        PrintParameters.PageOrientation = m_userTicket.PageOrientation;
        PageOrientation = PrintParameters.PageOrientation ?? PageOrientation.Unknown;
      }

      PageMediaSize pageMediaSize = null;
      if (PrintParameters.PageMediaSizeName != PageMediaSizeName.Unknown)
        pageMediaSize = PrintHelper.GetPageMediaSizeByName(m_activePrinter, PrintParameters.PageMediaSizeName);
      else
        pageMediaSize = m_userTicket.PageMediaSize;

      PrintParameters.PageMediaSizeName = pageMediaSize.PageMediaSizeName ?? m_userTicket.PageMediaSize.PageMediaSizeName;

      // TODO:
      // What User has set the PageMediaSize which is different from the PageMediaSize from the PrintTicket?
      bool rotated;
      if (pageMediaSize != null)
      {
        PaperSize = PrintHelper.FindMatchingPageMediaSize(pageMediaSize, m_paperSizes, out rotated);
        if (rotated)
        {
          Logger.Warning("No exactly matched Paper found, a rotated paper is found!");
        }
      }
      else
      {
        // A4 will be selected as the default PaperSize
        PaperSize = m_paperSizes.FirstOrDefault(paper => paper.Kind == PaperKind.A4);
        if (PaperSize == null && m_paperSizes.Count > 0) PaperSize = m_paperSizes.First();
        else throw new NotSupportedException(String.Format("PageMediaSizeName \"{0}\" not supported", PrintParameters.PageMediaSizeName));
      }

      if (PrintParameters.Scale == 0.0) PrintParameters.Scale = 1.0; // 0.0 is the reserved value, while 1.0 has real world sense
      m_scale = PrintParameters.Scale;

      // TODO: 
      // initialize the WaitScreen Dialog
      // initialize the Progress Dialog

      if (m_progressDialogView == null)
        m_progressDialogView = new ProgressDialogView();
      if (m_waitScreenView == null)
        m_waitScreenView = new WaitScreenView();
      if (m_progressDialog == null)
        m_progressDialog = new ProgressDialogViewModel(m_progressDialogView);
      if (m_waitScreen == null)
        m_waitScreen = new WaitScreenViewModel(m_waitScreenView);

    }

    // Initialize Print Controller, including the WaitScreen and ProgressDialog
    protected virtual void InitializePrintController()
    {
      // @Comment: Better rename the xxViewModel as xxxController
      if (m_waitScreen == null)
        m_waitScreen = new WaitScreenViewModel(new WaitScreenView());
      if (m_progressDialog == null)
        m_progressDialog = new ProgressDialogViewModel(new ProgressDialogView());
    }

    private void SetActivePrinter()
    {
      Debug.Assert(PrintParameters != null, "PrintParameter is not Initialized");
      Debug.Assert(m_defaultPrinter != null, "Default Printer is unknown");

      if (PrintParameters.PrinterName != null)
      {
        //var printer = m_printers.FirstOrDefault(p => p.FullName == PrintParameters.PrintQueue.FullName);
        var printer = m_printers.FirstOrDefault(p => p.FullName == PrintParameters.PrinterName || p.Name == PrintParameters.PrinterName);
        if (printer == null)
          ActivePrinter = printer;
        else
          ActivePrinter = m_defaultPrinter;
      }
      else
      {
        ActivePrinter = m_defaultPrinter;
      }
    }

    #endregion Print InitialParameters

    private IPrintControlView CreateAndSetupPrintPreviewView()
    {
      var printPreviewView = new PrintPreviewView();
      printPreviewView.ViewModel = this;
      printPreviewView.Loaded += new RoutedEventHandler(OnPrintPreviewViewLoaded);
      return printPreviewView;
    }

    void OnPrintPreviewViewLoaded(object sender, RoutedEventArgs e)
    {
      // Call initialize Properties

      if (m_printControlView == null) throw new Exception("Internal Error! m_printControlView is not Initialized");
      //TODO
      // enable SetPrintingOptinosWaitcurtainVisibility(Visibility.Collapsed) below
      //
      //((PrintPreviewView)m_printControlView).SetPrintingOptionsWaitCurtainVisibility(Visibility.Collapsed);
      SetPrintPreviewInputBindings(m_printPreviewWindow);
      InitPrintParameters(m_requestedPrintParameters);
      ReloadPreview();
    }

    private void SetPrintPreviewInputBindings(Window printwindow_)
    {
      if (printwindow_ == null) throw new ArgumentNullException("view_");

      var inputKeyBinding = new KeyBinding(PrintDocumentCommand, Key.P, ModifierKeys.Alt);
      printwindow_.InputBindings.Add(inputKeyBinding);

      var cancelPrintDocumentGesture = new KeyGesture(Key.C, ModifierKeys.Alt);
      var cancelPrintDocumentBinding = new KeyBinding(CancelPrintCommand, cancelPrintDocumentGesture);
      printwindow_.InputBindings.Add(cancelPrintDocumentBinding);

    }

    public bool ShowPreview()
    {
      //if (m_printControlView == null) throw new ArgumentException("Internal Error! m_printControlPreview is not initialized");
      if (m_printControlView == null)
      {
        m_printControlView = CreateAndSetupPrintPreviewView();
      }
      
      if (m_printPreviewWindow != null)
      {
        m_printPreviewWindow.Content = null;
      }
      CreatePrintPreviewWindow();
      m_printPreviewWindowLoading = true;
      if (m_printPreviewWindow != null) m_printPreviewWindow.ShowDialog();
      // now clear the ApplicationExtension's idea of who is the current top level window
      ApplicationExtention.MainWindow = null;

      return true;
    }

    // @TODO:
    //  move the precondition to the InitPrintParameters so there is no need to check in very function that relies on InitPrintParameters
    //  TIP: use ensures, or postconditions
    public virtual void ExecutePrint(object parameter)
    {
      InitPrintParameters(m_requestedPrintParameters);

      var reloadPaginatorCalled = false;
      try
      {
        var printDialog = new PrintDialog();
        printDialog.PrintQueue = ActivePrinter;
        printDialog.PrintTicket = UserTicket;
        if (!m_reloadingPreview)
        {
          m_reloadingPreview = true;
          reloadPaginatorCalled = true;
          ReloadPaginator(m_scale, PaperSize, PageOrientation);
        }

        ShowProgressDialog();
        // todo: We have to put the VisualPaginator creation aside in the background.
        ((VisualPaginator)Paginator).PageCreated += new EventHandler<PageEventArgs>(PrintControlPageCreated);
        printDialog.PrintDocument(Paginator, "");

      }
      catch (Exception ex)
      {
        Logger.Error("Print failed", ex);
      }
      finally
      {
        ((VisualPaginator)Paginator).PageCreated -= PrintControlPageCreated;
        // mutual exclusive on ReloadPreview
        if (reloadPaginatorCalled) m_reloadingPreview = false;
        ProgressDialog.Hide();
      }
    }

    

    public virtual void ExecuteCancelPrint(object parameter)
    {
      if (m_printPreviewWindow != null)
      {
        m_printPreviewWindow.Close();
      }
    }

    public virtual void ExecutePageOrientation(object parameter)
    {
      throw new NotImplementedException();
    }

    public virtual void ExecuteMarkPageNumbers(object parameter)
    {
      throw new NotImplementedException();
    }

    public virtual void ExecuteAllPages(object parameters)
    {
      throw new NotImplementedException();
    }

    public virtual void ExecuteActualPageSizeCommand(object paramters)
    {
      throw new NotImplementedException();
    }

    public virtual void ExecuteChangePaper(object parameters)
    {
      throw new NotImplementedException();
    }

    public virtual void ExecuteSetPrintingOptions(object parameters)
    {
      throw new NotImplementedException();
    }


    public virtual void ExecuteCancelAsyncPrint(object parameters)
    {
      throw new NotImplementedException();
    }


    // @Comment : Print document
    public ICommand PrintDocumentCommand { get; set; }
    public ICommand PrintSetupCommand { get; set; }
    public ICommand CancelPrintCommand { get; set; }
    public ICommand PageOrientationCommand { get; set; }
    public ICommand SetPrintingOptionsCommand { get; set; }
    public ICommand CancelPrintingOptionsCommand { get; set; }
    public ICommand MarkPageNumbersCommand { get; set; }
    public ICommand AllPagesCommand { get; set; }
    public ICommand ActualPageSizeCommand { get; set; }
    public ICommand ChangePaperCommand { get; set; }


    // @Comment: Initialize teh Print Commands 
    protected virtual void InitializePrintCommands()
    {

        
        ICommandHeler helper = CommandsHelper.CommandHeler;
        PrintDocumentCommand = helper.CreateDelegateCommand<object>(ExecutePrint);

        CancelPrintCommand = helper.CreateDelegateCommand<object>(ExecuteCancelPrint);
        PageOrientationCommand = helper.CreateDelegateCommand<object>(ExecutePageOrientation);
        SetPrintingOptionsCommand = helper.CreateDelegateCommand<object>(ExecuteSetPrintingOptions);
        AllPagesCommand = helper.CreateDelegateCommand<object>(ExecuteAllPages);
        MarkPageNumbersCommand = helper.CreateDelegateCommand<object>(ExecuteMarkPageNumbers);
        ActualPageSizeCommand = helper.CreateDelegateCommand<object>(ExecuteActualPageSizeCommand);
        ChangePaperCommand = helper.CreateDelegateCommand<object>(ExecuteChangePaper);
    }
    
    // Show the printing progress window
    private void ShowProgressDialog()
    {
      if (ProgressDialog == null) throw new Exception("Internal Error, Progress Dialog is not initialized");

      var cancelAsyncPrintCommand = new DelegateCommand<object>(ExecuteCancelAsyncPrint);
      ProgressDialog.CancelCommand = cancelAsyncPrintCommand;
      ProgressDialog.MaxProgressValue = ApproximateNumberOfPages;
      ProgressDialog.CurrentProgressValue = 0;
      ProgressDialog.Message = GetStatusMessage();
      ProgressDialog.DialogTitle = "Printing...";
      ProgressDialog.CancelButtonCaption = "Cancel";
      SetProgressDialogCancelButtonVisibility();
      ProgressDialog.Show();
    }

    private string GetStatusMessage()
    {
      return String.Format("Printing pages {0}/{1}", ProgressDialog.CurrentProgressValue, ProgressDialog.MaxProgressValue);
    }

    internal virtual void SetProgressDialogCancelButtonVisibility()
    {
      if (PrintParameters == null) throw new Exception("Internal Error, PrintParameter is not initialized");
      //if (PrintParameters.PrintQueue == null) throw new Exception("Internal Error, PrintParameter is not initialized");
      if (ActivePrinter == null) throw new Exception("Internal Error, PrintParameter is not initialized");

      var currentPrinter = ActivePrinter;
      ProgressDialog.CancelButtonVisibility = currentPrinter.IsXpsDevice ? Visibility.Visible : Visibility.Hidden;
    }

    // @Comment: Reload the preview so that Paginator and etc.. is ready to print
    // @TODO: rename
    internal void ReloadPreview(double scale, Thickness margin, PageOrientation pageOrientation, PaperSize paperSize)
    {
      //if (DrawingVisual == null) throw new Exception("Internal Error, DrawingVisual is not initialized");
      if (DrawingVisuals == null) throw new Exception("Internal Error, DrawingVisuals i not initialized");
      if (WaitScreen == null) throw new Exception("Internal Error, WaitScreen is not initialized");
      string message;
      if (!ValidatePrintParameters(PrintParameters, out message)) throw new Exception(String.Format("validation error, {0}", message));

      try
      {
        m_reloadingPreview = true;
        ShowWaitScreen();
        var visualPaginator = ReloadPaginator(scale, paperSize, pageOrientation);

        // Display the preview of the pages
        DisplayPagePreviewsAll(visualPaginator);
        m_reloadingPreview = false;
      }
      catch (Exception ex)
      {
        Logger.Error("ReloadPreview Failed", ex);
      }
      finally
      {
        WaitScreen.Hide();
      }
    }




    internal void ReloadPreview()
    {
      //if (PrintParameters.
      if (PrintParameters == null) throw new Exception("Internal Error! PrintParameter is not initialized");
      if (PaperSize == null) throw new Exception("Internal Error! PaperSize is not initialized");

      ReloadPreview(m_scale, new Thickness(), PageOrientation, PaperSize);
    }

    // Create the Print Preview Window, the top window that shall host the PreviewControl
    internal void CreatePrintPreviewWindow()
    {
      CreateWaitScreenWindow(PrintControlView);
    }

    private void CreateWaitScreenWindow(object view)
    {
      if (view == null) throw new ArgumentNullException("view");
      // TODO:
      // check the type of the view is settable to ApplicationExtension.Mainwindow.Content;

      if (m_printPreviewWindow != null)
      {
          m_printPreviewWindow.Activated -= PrintPreviewWindowActivated;
      }
      m_printPreviewWindow = new Window();
      m_printPreviewWindow.Activated += PrintPreviewWindowActivated;
      //m_printPreviewWindow.Closing += PrintPreviewWindowClosing;
      m_printPreviewWindow.Title = "Print Preview";
      m_printPreviewWindow.MinWidth = 600;
      m_printPreviewWindow.MinHeight = 600;
      m_printPreviewWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
      m_printPreviewWindow.ShowInTaskbar = false;
      m_printPreviewWindow.WindowStyle = WindowStyle.SingleBorderWindow;
      m_printPreviewWindow.WindowState = WindowState.Maximized;
      m_printPreviewWindow.Owner = System.Windows.Application.Current.MainWindow;
      m_printPreviewWindow.Content = view;
      ApplicationExtention.MainWindow = m_printPreviewWindow;

    }


    // @Comment: Display the WaitScreen, reloading preview and etc. may take some time
    private void ShowWaitScreen()
    {
      if (WaitScreen == null) throw new Exception("Internal Error! WaitScreen is not Initialized");

      if (PrintPreviewWindow != null)
      {
        WaitScreen.Show("Loading... ");
      }
    }


    #region Protected

    protected virtual void CreatePaginator(DrawingVisual visual, Size printSize)
    {
      m_paginator = new VisualPaginator(visual, printSize, new Thickness(), PrintHelper.GetPageMargin(ActivePrinter.FullName));
    }

    // @TODO:
    //  hoard/stash the settings of the Margings a mean of optimization
    protected virtual void CreatePaginator(IEnumerable<DrawingVisual> visuals, Size printSize)
    {
      m_paginator = new VisualPaginator(visuals, printSize, new Thickness(), PrintHelper.GetPageMargin(ActivePrinter.FullName));
    }

    //@Description: ReloadPaginator
    //@Comment: Reload Paginator and properties that depends on Paginator
    //@TODO: return type changed to abstract/interface
    private VisualPaginator ReloadPaginator(double scale, PaperSize paperSize, PageOrientation pageOrientation)
    {
      if (DrawingVisuals == null) throw new Exception("Internal Error, DrawingVisuals is not initialized");
      if (WaitScreen == null) throw new Exception("Internal Error, WaitScreen is not initialized");
      
      // TODO:
      // check WaitScreen is displayed
      //

      var printSize = PrintHelper.GetPrintSize(paperSize, pageOrientation);
      var visuals = PrintHelper.GetScaledVisuals(DrawingVisuals, scale);
      CreatePaginator(visuals, printSize);
      var visualPaginator = ((VisualPaginator)m_paginator);
      visualPaginator.Initialize(PrintParameters.UsePaginator);
      m_pageAcross = visualPaginator.HorizontalPageCount;
      ApproximateNumberOfPages = m_maxCopies = m_paginator.PageCount;

      return visualPaginator;
    }


    //protected int ApproximateNumberOfPages { get { return m_numberOfPages; } set { m_numberOfPages = value; } }


    public int ApproximateNumberOfPages
    {
      get { return (int)GetValue(ApproximateNumberOfPagesProperty); }
      set { 
        SetAndRaisePropertyChanged(ApproximateNumberOfPagesProperty, value);
      }
    }

    // Using a DependencyProperty as the backing store for ApproximateNumberOfPages.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty ApproximateNumberOfPagesProperty =
        DependencyProperty.Register("ApproximateNumberOfPages", typeof(int), typeof(ChromeManagerBase), new UIPropertyMetadata(0));



    protected virtual void DisplayPagePreviewsAll(DocumentPaginator paginator)
    {
      if (m_printControlView == null) throw new Exception("Internal Error, m_printControlView is not Initialized");

      double scale;
      var rowCount = GetRowCount(paginator);
      var container = ((IPrintControlView)PrintControlView).GetPagePreviewContainer();
      container.Children.Clear();
      for (var i = 0; i < rowCount; i++)
      {

        container.Children.Add(new StackPanel
        {
          Orientation = Orientation.Horizontal,
          HorizontalAlignment = HorizontalAlignment.Stretch,
          VerticalAlignment = VerticalAlignment.Center
        });
        // yield the control temporarily
        System.Windows.Application.Current.DoEvents();
      }

      var totalWidth = PagesAcross * (paginator.PageSize.Width + 40);
      var totalHeight = rowCount * (paginator.PageSize.Height + 40);
      if (totalWidth > totalHeight)
      {
        scale = ((ScrollViewer)((Border)container.Parent).Parent).ActualWidth / totalWidth;
      }
      else
      {
        scale = ((ScrollViewer)((Border)container.Parent).Parent).ActualHeight / totalHeight;
      }

      scale = ShowAllPages ? scale : 1;

      for (var i = 0; i < paginator.PageCount; i++)
      {
        System.Windows.Application.Current.DoEvents();
        var pageElement = GetPageUiElement(i, paginator, scale);
        pageElement.HorizontalAlignment = HorizontalAlignment.Center;
        pageElement.VerticalAlignment = VerticalAlignment.Center;
        // Row number is determined by the RowSpan of each visual...
        var rowIndex = i / PagesAcross;
        // REMOVE THE PREVIEW container
        InsertPageToPreviewContainer(rowIndex, pageElement, container);

      }
    }

    #endregion Protected


    #region Internals
    internal IProgressDialog ProgressDialog
    {
      get { return m_progressDialog; }
    }

    //@Comment: Is the preview still reloading.
    internal bool ReloadingPreview
    {
      get { return m_reloadingPreview; }
    }

    internal PrintQueueCollection Printers
    {
      get { return m_printers; }
      set { m_printers = value; }
    }

    internal PrintQueue DefaultPrinter
    {
      get { return m_defaultPrinter; }
      set { m_defaultPrinter = value; }
    }

    internal int PagesAcross
    {
      get { return m_pageAcross; }
      set { m_pageAcross = value; }
    }

    // reserved for allow user to preview in different size (actual size or all pages that scaled to fit in one page)
    internal bool ShowAllPages
    {
      get { return m_showAllPages; }
      set { m_showAllPages = value; } 
    }


    internal IPrintControlView PrintControlView { get { return m_printControlView; } set { m_printControlView = value; } }
    private Window m_printPreviewWindow;

    public Window PrintPreviewWindow
    {
      get { return m_printPreviewWindow; }
      set { m_printPreviewWindow = value; }
    }

    // Replace the DrawingVisual with DrawingVisuals
    // 
    internal List<DrawingVisual> DrawingVisuals { get; set; } 

    public string ActivePrinterName
    {
      get { return (string)GetValue(ActivePrinterNameProperty); }
      set { SetAndRaisePropertyChanged(ActivePrinterNameProperty, value);  }
    }

    // Using a DependencyProperty as the backing store for ActivePrinterName.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty ActivePrinterNameProperty =
        DependencyProperty.Register("ActivePrinterName", typeof(string), typeof(ChromeManagerBase), new UIPropertyMetadata(String.Empty));

    public PaperSize PaperSize
    {
      get { return (PaperSize)GetValue(PaperSizeProperty); }
      set {
        SetAndRaisePropertyChanged(PaperSizeProperty, value);
      }
    }

    // Using a DependencyProperty as the backing store for PaperSize.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty PaperSizeProperty =
        DependencyProperty.Register("PaperSize", typeof(PaperSize), typeof(ChromeManagerBase), new UIPropertyMetadata(null));


    internal IList<PaperSize> PaperSizes
    {
      get { return m_paperSizes; }
      set { m_paperSizes = value; } 
    }

    internal PrintQueue ActivePrinter
    {
      get { return m_activePrinter; }
      set { 
        m_activePrinter = value;
        ActivePrinterName = m_activePrinter.Name;
      }
    }

    internal double Scale
    {
      get { return m_scale; }
      set { m_scale = value; }
    }


    internal PrintTicket UserTicket
    {
      get
      {
        return m_userTicket;
      }
      private set { m_userTicket = value; }
    }

    internal PageOrientation PageOrientation
    {
      get { return m_pageOrientation; }
      set 
      {
        m_pageOrientation = value;
        PageOrientationString = value.ToString();
      }
    }

    internal bool PrintPreviewWindowLoading
    {
      get { return m_printPreviewWindowLoading; }
      set { m_printPreviewWindowLoading = value; }
    }

    // Whether or not you can cancel the print
    // @NOTE: reserved
    internal bool IsPrintCancelable
    {
      get { return m_isPrintCancelable; }
      set { m_isPrintCancelable = value; }
    }

    // Whether it is allow to adjust the print settings
    // @NOTE: reserved
    internal bool IsPrintOptionsSettable
    {
      get { return m_isPrintOptionsSettable; }
      set { m_isPrintOptionsSettable = value; }
    }

    internal IWaitScreen WaitScreen
    {
      get { return m_waitScreen; }
      set { m_waitScreen = value; }
    }


    internal DocumentPaginator Paginator
    {
      get { return m_paginator; }
      private set { m_paginator = value; } 
    }

    #endregion Internals


    #region Privates
    // @TODO: initialize paginator nad ProgressDialog
    private DocumentPaginator m_paginator;
    // Progress Dialog controller
    private IProgressDialog m_progressDialog;

    private PrintQueueCollection m_printers;
    // TODO:
    //  remove the commented m_localPrinter
    //private PrintQueue m_localPrinter;
    private PrintQueue m_defaultPrinter;
    private PrintQueue m_activePrinter;


    private bool m_reloadingPreview = false;

    // how many pages a hole image may occupy
    private int m_pageAcross; 
    private PrintTicket m_userTicket;
    private int m_maxCopies;
    //private int m_numberOfPages;

    // contains the preview and Control UI
    private IPrintControlView m_printControlView;
    private bool m_showAllPages = false;

    private IList<PaperSize> m_paperSizes;

    private double m_scale;

    private PageOrientation m_pageOrientation;

    private bool m_printPreviewWindowLoading;

    private bool m_isPrintCancelable;

    private bool m_isPrintOptionsSettable;

    // wait screen controller
    private IWaitScreen m_waitScreen;

    private IWaitScreenView m_waitScreenView;
    private IProgressDialogView m_progressDialogView;



    internal string PageOrientationString
    {
      get { return (string)GetValue(PageOrientationStringProperty); }
      set {
        SetAndRaisePropertyChanged(PageOrientationStringProperty, value);
      }
    }

    private void SetAndRaisePropertyChanged(DependencyProperty dp, object newvalue)
    {
      var oldvalue = GetValue(dp);
      SetValue(dp, newvalue);
      OnPropertyChanged(new DependencyPropertyChangedEventArgs(dp, oldvalue, newvalue));
    }


    // Using a DependencyProperty as the backing store for PageOrientationString.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty PageOrientationStringProperty =
        DependencyProperty.Register("PageOrientationString", typeof(string), typeof(ChromeManagerBase), new UIPropertyMetadata(String.Empty));


    // @TODO: Code contract to ensure post conditions
    private void AddDefaultPrinterIfNotExists()
    {
      Debug.Assert(m_printers != null, "Printers collection not initialized");
      Debug.Assert(m_defaultPrinter != null, "DefaultPrinter not initialized");
      var defaultExists = false;
      foreach (var printer in m_printers)
      {
        if (printer.Name == m_defaultPrinter.Name)
        {
          defaultExists = true;
        }
      }

      if (!defaultExists)
      {
        m_printers.Add(m_defaultPrinter);
      }
    }

    private int GetRowCount(DocumentPaginator paginator)
    {
      return (int)(Math.Ceiling((double)paginator.PageCount / m_pageAcross));
    }

    // get the visual element that represent the DocumentPaginator at page i
    private Border GetPageUiElement(int i, DocumentPaginator paginator, double scale)
    {
      var source = paginator.GetPage(i);
      // TODO: Reorganize the using directive
      var border = new Border() { Background = Brushes.White };
      border.Margin = new Thickness(10 * scale);
      border.BorderBrush = Brushes.DarkGray;
      border.BorderThickness = new Thickness(1);
      //var margin = PrintUtility.GetPageMargin(CurrentPrinterName);
      var margin = new Thickness();
      var rectangle = new Rectangle();
      rectangle.Width = ((source.Size.Width * 0.96 - (margin.Left + margin.Right)) * scale);
      rectangle.Height = ((source.Size.Height * 0.96 - (margin.Top + margin.Bottom)) * scale);
      rectangle.Margin = new Thickness(margin.Left * scale, margin.Top * scale, margin.Right * scale, margin.Bottom * scale);
      rectangle.Fill = Brushes.White;
      var vb = new VisualBrush(source.Visual);
      vb.Opacity = 1;
      vb.Stretch = Stretch.Uniform;
      rectangle.Fill = vb;
      border.Child = rectangle;
      return border;
    }



    private bool ValidatePrintParameters(InitialPrintParameters printParameters, out string message)
    {
      message = String.Empty;
      var errors = new List<string>();

      if (printParameters == null) errors.Add("printParameters is null");
      if (printParameters.PageOrientation == PageOrientation.Unknown  || printParameters.PageOrientation == null) errors.Add("PageOrienation unitialized");
      if (String.IsNullOrEmpty(printParameters.PrinterName)) errors.Add("PrintName is unitialized");
      if (printParameters.PageMediaSizeName == PageMediaSizeName.Unknown) errors.Add("PageMediaSizeName is unitialized");
      if (printParameters.Scale == 0.0) errors.Add("Scale is not initialized");

      if (errors.Count > 0) message = String.Join("\r\t", errors);
      return message == String.Empty;
    }

    private void InsertPageToPreviewContainer(int rowIndex, Border pageElement, StackPanel container)
    {
      ((StackPanel)container.Children[rowIndex]).Children.Add(pageElement);
    }

    private void PrintPreviewWindowActivated(object sender, EventArgs e)
    {
      if (m_printPreviewWindowLoading)
      {
          PrintPreviewWindow.Activated -= PrintPreviewWindowActivated;
        // subsequent Application Main from ApplicationExtension will have this window as the main window.
        ApplicationExtention.MainWindow = PrintPreviewWindow;
      }
    }

    // @Comment, fired when the PageCreated event is fired
    private void PrintControlPageCreated(object sender, PageEventArgs e)
    {
      if (ProgressDialog == null) throw new Exception("Internal Error, ProgressDialog not initialized");

      ProgressDialog.CurrentProgressValue = e.PageNumber;
      ProgressDialog.Message = GetStatusMessage();
      System.Windows.Application.Current.DoEvents();
    }

    #endregion Privates
  }
}
