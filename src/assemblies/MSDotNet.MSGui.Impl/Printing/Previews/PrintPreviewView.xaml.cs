using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Input;
using System;

namespace MorganStanley.MSDotNet.MSGui.Impl.Printing.Previews
{
    /// <summary>
    /// Interaction logic for AdvancedColorPicker.xaml
    /// </summary>
    internal partial class PrintPreviewView : IPrintControlView
    {
        public PrintPreviewView()
        {
            InitializeComponent();
        }

        #region IPrintControlView Members

        //@Comment: The document viewer is the Visual Control which allows viewing FixedDocument
        // more details can be found here: 
        // "Documents in Windows Presentation Foundation" - http://msdn.microsoft.com/en-us/library/windows/desktop/ms748388(v=vs.85).aspx
        //
        DocumentViewer IPrintControlView.DocumentViewer
        {
            get
            {
                return null;// DocViewer;
            }
        }

        public StackPanel GetPagePreviewContainer()
        {
            return PagePreviewContainer;
        }

        public ScrollViewer GetSv()
        {
            return null;//sv;
        }

        #endregion

        public void ScalePreviewPaneVisibility(bool isVisible)
        {
        }

        public void ResizeButtonVisibility(bool isVisible)
        {
        }

      // Temporarily disable the options
      //  to be implemented later based on user's feedback and project vision
      // 
      /*
        public void PrintingOptionsWaitCurtainVisibility(bool isVisible)
        {
            if (isVisible)
                PrintingOptionsWaitCurtain.Visibility = Visibility.Visible;
            else
                PrintingOptionsWaitCurtain.Visibility = Visibility.Collapsed;
        }
        public void ScalePreviewNode(ScaleTransform scaleTransform)
        {
            PreviewNode.LayoutTransform = scaleTransform;
        }

        internal void EnablePrintingOptionsSet(bool isEnabled)
        {
            if (isEnabled)
                SetPanel.Visibility = Visibility.Visible;
            else
                SetPanel.Visibility = Visibility.Collapsed;
        }

        internal void SetPrintingOptionsWaitCurtainVisibility(Visibility visibility)
        {
          PrintingOptionsWaitCurtain.Visibility = visibility;
        }
       */

        public void ScalePreviewNode(ScaleTransform scaleTransform)
        {
          throw new NotImplementedException();
        }

        public void PrintingOptionsWaitCurtainVisibility(bool isEnabled)
        {
          throw new NotImplementedException();
        }

      #region IView Members

        public IViewModel ViewModel
        {
          get { return m_viewModel; }
          set
          {
            m_viewModel = value;
            this.DataContext = m_viewModel;
          }
        }
      #endregion IView Members


      #region Private Fields
      private IViewModel m_viewModel;

      #endregion Private Fields 

    }
}