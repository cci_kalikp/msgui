﻿using System.Windows.Controls;
using System.Windows.Media;

namespace MorganStanley.MSDotNet.MSGui.Impl.Printing.Previews
{
    //public interface IPrintControlView : IView
    // @Summary
    //    Interface for IPrintPreviewView
    // @Note:
    // @TODO:
    //   renaming 
    internal interface IPrintControlView : IView
    {
        DocumentViewer DocumentViewer { get; }
        void PrintingOptionsWaitCurtainVisibility(bool b);
        StackPanel GetPagePreviewContainer();
        void ScalePreviewNode(ScaleTransform scaleTransform);
    }
}