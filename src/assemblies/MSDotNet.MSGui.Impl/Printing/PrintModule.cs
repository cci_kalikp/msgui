﻿using System;
using System.Windows.Controls.Primitives;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager; 
using System.Windows.Media.Imaging;
using System.Diagnostics; 
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Printing;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Shell;
using Microsoft.Practices.Unity;

namespace MorganStanley.MSDotNet.MSGui.Impl.Printing
{
  public class PrintModule : IMSDesktopModule
  {
    public PrintModule(IChromeManager manager_, IChromeRegistry registry_, IUnityContainer container_)
    {
      this.m_manager = manager_;
      this.m_registry = registry_;
      if (this.m_manager is IPrintManager)
      {
        this.m_print = (IPrintManager)this.m_manager;
      }
      this.m_container = container_;
    }

    public void Initialize()
    {
      CreatePrintWidgets();
    }


    #region Print Chromes
    /**
     * @Summary Print Chromes
     *   The chromes related to the Print functionalities
     * @Comment
     *    MSDesktop provides print command on the MSDesktop's Chrome areas
     *    and depends on the different type of Shell Mode, it should place different type to one or more areas on the MSDesttop's chrome areas;
     *    e.g. on RibbonShell, it should ae 
     *  @Remark
     *    Normally the menu shall looks like this: 
     *    Load Layout    |>...
     *    Save Layout
     *    Save Layout As |>... 
     *    Delete Current Layout 
     *    --- separator ---
     *    Print
     *    Print Preview
     *    --- Separator ---
     *    Options 
     *    View To View Communication
     *    .. other widget that is placed on by the ChromeManager
     *    --- Separator ---
     *    Exit 
     *    
     *  similar impl details see ApplicationOptions
     */

    internal const string PRINT_MENU_COMMANDAREA_KEY = "MSGuiPrintButton";
    internal const string PRINTPREVIEW_MENU_COMMANDAREA_KEY = "MSGuiPrintPreviewButton";
    //internal const string PRINT_QAT_COMMANDAREA_NAME = @"PRINT_QAT_COMMANDAREA_NAME";
    //internal const string PRINTPREVIEW_QAT_COMMANDAREA_NAME = @"PRINTPREVIEW_QAT_COMMANDAREA_NAME";

    private MorganStanley.MSDotNet.MSGui.Impl.Application.InitialSettings m_initialSettings = null;

      private bool isRibbon;
    private void EnsureDependenciesInitialized()
    {
      Debug.Assert(m_container != null);

      if (m_initialSettings == null)
      {
        m_initialSettings = UnityContainerExtensions.Resolve<Application.InitialSettings>(m_container);
        if (m_initialSettings == null)
          throw new NotSupportedException("Resolve way of getting InitialSettings is not supported");
          isRibbon = m_initialSettings.ShellMode == ShellMode.RibbonAndFloatingWindows ||
                     m_initialSettings.ShellMode == ShellMode.RibbonMDI ||
                     m_initialSettings.ShellMode == ShellMode.RibbonWindow;
      }
      if (m_shell == null)
      {
        m_shell = UnityContainerExtensions.Resolve<IShell>(m_container);
        if (m_shell == null)
          throw new NotSupportedException("Resolve getting IShell is not supported");
      }

    }

    internal void CreatePrintWidgets()
    {
      Debug.Assert(this.m_registry != null);
      Debug.Assert(this.m_manager != null);
      Debug.Assert(this.m_print != null);
      EnsureDependenciesInitialized();

        m_manager[ChromeManagerBase.MENU_COMMANDAREA_NAME].AddWidget(PRINT_MENU_COMMANDAREA_KEY,
                                                                     GetPrintButtonParameters());

        m_manager[ChromeManagerBase.MENU_COMMANDAREA_NAME].AddWidget(PRINTPREVIEW_MENU_COMMANDAREA_KEY,
                                                             GetPrintPreviewButtonParameters());
        if (isRibbon)
        {
            BindPrintButton();
            BindPrintPreviewButton();
        }
    }

    internal InitialButtonParameters GetPrintButtonParameters()
    { 
        return new InitialButtonParameters()
            {
                Text = "Print",
                Image = new BitmapImage(new Uri(@"/MSDotNet.MSGui.Impl;component/Images/icons/Print_Large.png", UriKind.RelativeOrAbsolute)),
                Click = OnPrintAll
            };
          
    }

     internal void BindPrintButton()
     { 
         var ribbonShell = m_shell as ShellWithRibbon; 
         if (ribbonShell == null) return;
         var printButton = ribbonShell.ShellWindow.PrintButton;
         ((ButtonBase)printButton).Click += OnPrintAll;
     }

    internal InitialButtonParameters GetPrintPreviewButtonParameters()
    {
        return new InitialButtonParameters()
        {
            Text = "Print Preview",
            Image = new BitmapImage(new Uri(@"/MSDotNet.MSGui.Impl;component/Images/icons/PrintPreview_Large.png", UriKind.RelativeOrAbsolute)),
            Click = OnPreviewPrintAll
        };
 
    }

      internal void BindPrintPreviewButton()
      {
          var ribbonShell = m_shell as ShellWithRibbon;
          if (ribbonShell == null) return;
          var printPreviewButton = ribbonShell.ShellWindow.PrintPreviewButton;
          ((ButtonBase)printPreviewButton).Click += OnPreviewPrintAll;
      }

    #region Chrome Command Handlers
    internal void OnPrintAll(object sender, EventArgs e)
    {
      m_print.PrintAll();
    }

    internal void OnPreviewPrintAll(object sender, EventArgs e)
    {
      m_print.PreviewPrintAll();
    }


    #endregion Chrome Command Handlers
    #endregion Print Chromes

    #region Private Fields
    private readonly IChromeManager m_manager;
    private readonly IChromeRegistry m_registry;
    private readonly IPrintManager m_print;
    private readonly IUnityContainer m_container;
    private IShell m_shell;

    #endregion Private Fields
  }
}
