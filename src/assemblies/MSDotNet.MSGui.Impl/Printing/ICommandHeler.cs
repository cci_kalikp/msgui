﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace MorganStanley.MSDotNet.MSGui.Impl.Printing
{
    interface ICommandHeler
    {
        ICommand CreateDelegateCommand<T>(Action<T> execute);
    }
}
