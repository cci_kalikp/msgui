using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Shapes;
//using SUT.PrintEngine.Utils;
using MorganStanley.MSDotNet.MSGui.Impl.Printing.Utils;

namespace MorganStanley.MSDotNet.MSGui.Impl.Paginators
{
    public class VisualPaginator : DocumentPaginator
    {
        protected const string DrawingVisualNullMessage = "Drawing Visual Source is null";
        public int HorizontalPageCount;

        private readonly Size _printSize;
        protected Thickness PageMargins;
        private readonly Thickness _originalMargin;
        protected Size ContentSize;
        protected Pen FramePen;
        protected readonly List<double> AdjustedPageWidths = new List<double>();
        protected readonly List<double> AdjustedPageHeights = new List<double>();
        private int _verticalPageCount;
        protected Rect FrameRect;
        public List<DrawingVisual> DrawingVisuals;
        private IDocumentPaginatorSource _document;
        protected double PrintablePageWidth;
        protected double PrintablePageHeight;

        private List<DrawingVisual> m_drawingVisualSources;
        private List<Size> m_printSizes;
        private int m_totalPage;

        private class DrawingVisualPageMarker
        {
          public int StartPage;
          public int HorPageCount;
          public int VerPageCount;
        }

        private Dictionary<DrawingVisual, DrawingVisualPageMarker> m_drawingPageMarkers;

      /// <summary>
      /// Whether or not to show Page Mark (indicative of page/page count)
      /// </summary>
        public bool ShowPageMarkers;
        protected double HeaderHeight;
        public event EventHandler<PageEventArgs> PageCreated;

        public void OnPageCreated(PageEventArgs e)
        {
            EventHandler<PageEventArgs> handler = PageCreated;
            if (handler != null) handler(this, e);
        }


      // how can we define the printSize, why not 
        public VisualPaginator(List<DrawingVisual> sources, Size printSize, Thickness pageMargins, Thickness originalMargin) : this((IEnumerable<DrawingVisual>)sources, printSize, pageMargins, originalMargin)
        {
        }

        //@TODO:
        // Remove if no usage can be foretold
        public VisualPaginator(List<DrawingVisual> sources, List<Size> printSizes, Thickness pageMargins, Thickness originalMargin) 
        {
          if (sources == null) throw new ArgumentNullException("sources");
          if (printSizes == null) throw new ArgumentNullException("printSizes");
          if (pageMargins == null) throw new ArgumentNullException("pageMargins");
          if (originalMargin == null) throw new ArgumentNullException("orginalMargin");

          m_drawingVisualSources = sources;
          m_printSizes = printSizes;
          PageMargins = pageMargins;
          _originalMargin = originalMargin;
        }

        public VisualPaginator(IEnumerable<DrawingVisual> sources, Size printSize, Thickness pageMargins, Thickness originalMargin)
        {
          if (sources == null) throw new ArgumentNullException("sources");
          if (printSize == null) throw new ArgumentNullException("printSizes");
          if (pageMargins == null) throw new ArgumentNullException("pageMargins");
          if (originalMargin == null) throw new ArgumentNullException("orginalMargin");

          m_drawingVisualSources = new List<DrawingVisual>(sources);
          _printSize = printSize;
          PageMargins = pageMargins;
          _originalMargin = originalMargin;
        }


        public VisualPaginator(DrawingVisual source, Size printSize, Thickness pageMargins, Thickness originalMargin) : 
          this(new List<DrawingVisual>(new [] { source}), printSize, pageMargins, originalMargin)
        {
        }

        public void Initialize(bool isMarkPageNumbers)
        {
            m_drawingPageMarkers = new Dictionary<DrawingVisual, DrawingVisualPageMarker>();

            ShowPageMarkers = isMarkPageNumbers;
            var totalHorizontalMargin = PageMargins.Left + PageMargins.Right;
            var toltalVerticalMargin = PageMargins.Top + PageMargins.Bottom;

            PrintablePageWidth = PageSize.Width - totalHorizontalMargin;
            PrintablePageHeight = PageSize.Height - toltalVerticalMargin;

            ContentSize = new Size(_printSize.Width - totalHorizontalMargin, _printSize.Height - toltalVerticalMargin);
            FrameRect = new Rect(new Point(PageMargins.Left, PageMargins.Top), new Size(_printSize.Width - totalHorizontalMargin, _printSize.Height - toltalVerticalMargin));
            FramePen = new Pen(Brushes.Black, 0);

            HorizontalPageCount = GetMaxHorizalPageCount();
            _verticalPageCount = GetTotalVerticalPageCount();
             m_totalPage = GetTotalPageCount();

            CreateAllPageVisuals();
        }


        private void CreateAllPageVisuals()
        {
          DrawingVisuals = new List<DrawingVisual>();

          foreach (var visual in m_drawingVisualSources)
          {
            for (var verticalPageNumber = 0; verticalPageNumber < m_drawingPageMarkers[visual].VerPageCount; verticalPageNumber++)
            {
              for (var horizontalPageNumber = 0; horizontalPageNumber < m_drawingPageMarkers[visual].HorPageCount; horizontalPageNumber++)
              {
                const float horizontalOffset = 0;
                var verticalOffset = (float)(verticalPageNumber * PrintablePageHeight);
                var pageBounds = GetPageBounds(horizontalPageNumber, verticalPageNumber, horizontalOffset, verticalOffset);
                var drawingVisual = new DrawingVisual();
                using (var dc = drawingVisual.RenderOpen())
                {
                  CreatePageVisual(pageBounds, visual,
                                   IsFooterPage(horizontalPageNumber), dc);
                }
                DrawingVisuals.Add(drawingVisual);
              }
            }
          }
        }

        protected virtual Rect GetPageBounds(int horizontalPageNumber, int verticalPageNumber, float horizontalOffset, float verticalOffset)
        {
            var x = (float)((horizontalPageNumber * PrintablePageWidth));
            return new Rect { X = x, Y = verticalOffset, Size = ContentSize };
        }

        private static bool IsFooterPage(int horizontalPageNumber)
        {
            return horizontalPageNumber == 0;
        }

        protected virtual int GetTotalVerticalPageCount()
        {
          int count = 0;
          if (IsDrawingsNotNull())
          {
            foreach (var drawing in m_drawingVisualSources)
            {
              var visualPageCountByHeight = (int)Math.Ceiling(GetDrawingBounds(drawing).Height / (PrintablePageHeight));
              // TODO:
              // extract the piece of code to method (make virtual as well)
              if (m_drawingPageMarkers.ContainsKey(drawing))
              {
                m_drawingPageMarkers[drawing].VerPageCount = visualPageCountByHeight;
              }
              else
              {
                m_drawingPageMarkers[drawing] = new DrawingVisualPageMarker { VerPageCount = visualPageCountByHeight };
              }
              count += visualPageCountByHeight;
            }
          }
          else
          {
            // TODO:
            throw new NullReferenceException("One of DrawingVisuals is null");
          }
          return count;
        }

        protected virtual int GetTotalPageCount()
        {
          CheckPageMarkersAndThrow();

          var currPage = 0;
          foreach (var visual in m_drawingVisualSources)
          {
            m_drawingPageMarkers[visual].StartPage = currPage;
            currPage += m_drawingPageMarkers[visual].HorPageCount * m_drawingPageMarkers[visual].VerPageCount;
          }
          return currPage;
        }


        private void CheckPageMarkersAndThrow()
        {
          foreach (var visual in m_drawingVisualSources)
          {
            if (!m_drawingPageMarkers.ContainsKey(visual) || m_drawingPageMarkers[visual].VerPageCount == 0 || m_drawingPageMarkers[visual].HorPageCount == 0)
              throw new InvalidOperationException("Run GetTotalVericalPageCount() and GetMaxHorizontalPageCount() first");
          }
        }


        protected virtual int GetVerticalPageCount(DrawingVisual drawingVisual)
        {
          if (drawingVisual == null) throw new ArgumentNullException("drawingVisual");

          return (int)Math.Ceiling(GetDrawingBounds(drawingVisual).Height / (PrintablePageHeight));
        }
        
        
        public override DocumentPage GetPage(int pageNumber)
        {
            DrawingVisual pageVisual = GetPageVisual(pageNumber);
            var documentPage = new DocumentPage(pageVisual, PageSize, FrameRect, FrameRect);
            if (ShowPageMarkers)
                InsertPageMarkers(pageNumber + 1, documentPage);
            // Notify when one page is created, useful when notifying observer, such as the progress reporter.
            OnPageCreated(new PageEventArgs(pageNumber));
            return documentPage;
        }

        private DrawingVisual GetPageVisual(int pageNumber)
        {
            var totalHorizontalMargin = _originalMargin.Left + _originalMargin.Right;
            var toltalVerticalMargin = _originalMargin.Top + _originalMargin.Bottom;
            var printablePageWidth = PageSize.Width - totalHorizontalMargin;
            var printablePageHeight = PageSize.Height - toltalVerticalMargin - 10;

            var xFactor = printablePageWidth / PageSize.Width;
            var yFactor = printablePageHeight / PageSize.Height;
            var scaleFactor = Math.Max(xFactor, yFactor);
            var pageVisual = DrawingVisuals[pageNumber];
            var transformGroup = new TransformGroup();
            var scaleTransform = new ScaleTransform(scaleFactor, scaleFactor);
            var translateTransform = new TranslateTransform(_originalMargin.Left, _originalMargin.Top);
            transformGroup.Children.Add(translateTransform);
            transformGroup.Children.Add(scaleTransform);
            pageVisual.Transform = transformGroup;
            return pageVisual;
        }

        protected virtual void InsertPageMarkers(int pageNumber, DocumentPage documentPage)
        {
            var labelDrawingVisual = new DrawingVisual();
            using (var drawingContext = labelDrawingVisual.RenderOpen())
            {
                var pageNumberContent = pageNumber + "/" + PageCount;
                var ft = new FormattedText(pageNumberContent,
                                           Thread.CurrentThread.CurrentCulture,
                                           FlowDirection.LeftToRight,
                                           new Typeface("Verdana"),
                                           15, Brushes.Black);

                drawingContext.DrawText(ft, new Point(PageMargins.Left, PageMargins.Top));
            }

            var drawingGroup = new DrawingGroup();
            drawingGroup.Children.Add(((DrawingVisual)documentPage.Visual).Drawing);
            drawingGroup.Children.Add(labelDrawingVisual.Drawing);

            var currentDrawingVisual = (DrawingVisual)documentPage.Visual;
            using (var currentDrawingContext = currentDrawingVisual.RenderOpen())
            {
                currentDrawingContext.DrawDrawing(drawingGroup);
                currentDrawingContext.PushOpacityMask(Brushes.White);
            }
        }

        public override bool IsPageCountValid
        {
            get
            {
                return true;
            }
        }

        public override int PageCount
        {
          get
          {
            return m_totalPage;
          }
        }

        public override sealed Size PageSize
        {
            get { return _printSize; }
            set { }
        }

        public override IDocumentPaginatorSource Source
        {
            get
            {
                return _document;
            }
        }

        public void UpdatePageMarkers(bool showPageMarkers)
        {
            ShowPageMarkers = showPageMarkers;
        }

      // @Summary
      //    create the DocumentPaginatorSource
      // @Note:
      //    USAGE NOT FOUND!
        public IDocumentPaginatorSource CreateDocumentPaginatorSource()
        {
            var document = new FixedDocument();
            for (var i = 0; i < PageCount; i++)
            {
                var page = GetPage(i);
                var fp = new FixedPage { ContentBox = FrameRect, BleedBox = FrameRect, Width = page.Size.Width, Height = page.Size.Height };

                var vb = new DrawingBrush(DrawingVisuals[i].Drawing)
                             {
                                 Stretch = Stretch.Uniform,
                                 ViewboxUnits = BrushMappingMode.Absolute,
                                 Viewbox = new Rect(page.Size)
                             };
                var totalHorizontalMargin = _originalMargin.Left + _originalMargin.Right;
                var toltalVerticalMargin = _originalMargin.Top + _originalMargin.Bottom;
                var printablePageWidth = PageSize.Width - totalHorizontalMargin;
                var printablePageHeight = PageSize.Height - toltalVerticalMargin - 10;

                var rect = new Rect(_originalMargin.Left, _originalMargin.Top, printablePageWidth, printablePageHeight);
                fp.Children.Add(CreateContentRectangle(vb, rect));
                var pageContent = new PageContent();
                ((IAddChild)pageContent).AddChild(fp);
                document.Pages.Add(pageContent);
            }
            _document = document;
            _document.DocumentPaginator.PageSize = new Size(PageSize.Width, PageSize.Height);
            return _document;
        }

        // @Summary
        //    create the DocumentPaginatorSource
        // @Note:
        //    USAGE NOT FOUND!
        public List<FixedDocument> CreateFixedDocumentsForEachPage()
        {
            var documents = new List<FixedDocument>();
            for (var i = 0; i < PageCount; i++)
            {
                var document = new FixedDocument();
                var page = GetPage(i);
                var fp = new FixedPage { ContentBox = FrameRect, BleedBox = FrameRect, Width = page.Size.Width, Height = page.Size.Height };
                var vb = new DrawingBrush(DrawingVisuals[i].Drawing) { Stretch = Stretch.Uniform, ViewboxUnits = BrushMappingMode.Absolute, Viewbox = new Rect(page.Size) };

                var totalHorizontalMargin = _originalMargin.Left + _originalMargin.Right;
                var toltalVerticalMargin = _originalMargin.Top + _originalMargin.Bottom;
                var printablePageWidth = PageSize.Width - totalHorizontalMargin;
                var printablePageHeight = PageSize.Height - toltalVerticalMargin - 10;

                var rect = new Rect(_originalMargin.Left, _originalMargin.Top, printablePageWidth, printablePageHeight);
                fp.Children.Add(CreateContentRectangle(vb, rect));
                var pageContent = new PageContent();
                ((IAddChild)pageContent).AddChild(fp);
                document.Pages.Add(pageContent);
                documents.Add(document);
            }
            return documents;
        }

        // @Summary
        //    create the DocumentPaginatorSource
        // @Note:
        //    USAGE NOT FOUND!
        public List<FixedDocument> CreateFixedDocumentsForEachPageWithPageNumber(int startPageNumber, double height, string slideName)
        {
            slideName = GetSlideNameForEntityChartHeader(slideName);
            var documents = new List<FixedDocument>();
            for (var i = 0; i < PageCount; i++)
            {
                var document = new FixedDocument();
                var page = GetPage(i);
                var fp = new FixedPage { ContentBox = FrameRect, BleedBox = FrameRect, Width = page.Size.Width, Height = page.Size.Height };
                var vb = new DrawingBrush(DrawingVisuals[i].Drawing) { Stretch = Stretch.Uniform, ViewboxUnits = BrushMappingMode.Absolute, Viewbox = new Rect(page.Size) };

                var totalHorizontalMargin = _originalMargin.Left + _originalMargin.Right;
                var toltalVerticalMargin = _originalMargin.Top + _originalMargin.Bottom;
                var printablePageWidth = PageSize.Width - totalHorizontalMargin;
                var printablePageHeight = height - toltalVerticalMargin - 10;

                var rect = new Rect(_originalMargin.Left, _originalMargin.Top + Constants.CsBook.EntityChartPageHeaderSize, printablePageWidth, printablePageHeight);
                fp.Children.Add(CreateContentRectangle(vb, rect));

                InsertEntityChartPageHeader(fp, startPageNumber++, slideName, i + 1);

                var pageContent = new PageContent();
                ((IAddChild)pageContent).AddChild(fp);
                document.Pages.Add(pageContent);
                documents.Add(document);
            }
            return documents;
        }

        private string GetSlideNameForEntityChartHeader(string slideName)
        {
            var entityChartSlideNameMaxSize = PageSize.Width - Constants.CsBook.PageNumberTextLength - 10;
            if (slideName.Length > entityChartSlideNameMaxSize)
            {
                return string.Format("{0}...", slideName.Substring(0, (int)entityChartSlideNameMaxSize));
            }
            return slideName;
        }

        private void InsertEntityChartPageHeader(FixedPage fp, int pageNumber, string slideName, int pageIndex)
        {
            var slideNameAvailableWidth = PageSize.Width - 2 * Constants.CsBook.PageNumberTextLength;

            var pageNumberLabel = new TextBlock { Text = string.Format("Page {0}", pageNumber) };
            FixedPage.SetLeft(pageNumberLabel, PageSize.Width - Constants.CsBook.PageNumberTextLength);
            FixedPage.SetTop(pageNumberLabel, 10);
            fp.Children.Add(pageNumberLabel);

            var noOfTotal = string.Format("({0} of {1})", pageIndex, PageCount);
            const int noOfTotalFontSize = 10;
            var ft1 = new FormattedText(noOfTotal, Thread.CurrentThread.CurrentCulture,
                                       FlowDirection.LeftToRight, new Typeface("Verdana"), noOfTotalFontSize, Brushes.Black);
            var pageNumberTextWidth = ft1.Width;
            var availableSlideNameWidth = slideNameAvailableWidth - pageNumberTextWidth;
            ft1 = new FormattedText(slideName, Thread.CurrentThread.CurrentCulture,
                                       FlowDirection.LeftToRight, new Typeface("Verdana"), 20, Brushes.Black);
            var slideNameTextWidth = ft1.Width;
            var txtBlockSlideNameWidth = (slideNameTextWidth > availableSlideNameWidth)
                                             ? availableSlideNameWidth
                                             : slideNameTextWidth;
            var stackPanel = new StackPanel { Orientation = Orientation.Horizontal};
            var txtBlockSlideName = new TextBlock
                                        {
                                            Text = slideName,
                                            Width = Math.Max(0, txtBlockSlideNameWidth),
                                            FontFamily = new FontFamily("Verdana"),
                                            FontSize = 20,
                                            TextTrimming = TextTrimming.CharacterEllipsis,
                                            Padding = new Thickness(0),
                                            Margin = new Thickness(0),
                                            VerticalAlignment = VerticalAlignment.Bottom
                                        };
            var txtBlockPageNo = new TextBlock
                                     {
                                         Text = noOfTotal,
                                         Width = pageNumberTextWidth,
                                         FontFamily = new FontFamily("Verdana"),
                                         FontSize = noOfTotalFontSize,
                                         TextTrimming = TextTrimming.None,
                                         VerticalAlignment = VerticalAlignment.Bottom,
                                         Padding = new Thickness(0,0,0,2),
                                         Margin = new Thickness(0)
                                     };
            stackPanel.Children.Add(txtBlockSlideName);
            stackPanel.Children.Add(txtBlockPageNo);
            var label = new Label
                            {
                                Width = slideNameAvailableWidth,
                                Content = stackPanel,
                                HorizontalContentAlignment = HorizontalAlignment.Center,
                                Padding = new Thickness(0),
                                Margin = new Thickness(0)
                            };


            FixedPage.SetLeft(label, Constants.CsBook.PageNumberTextLength);
            FixedPage.SetTop(label, 10);
            fp.Children.Add(label);
        }

        private static Rectangle CreateContentRectangle(Brush vb, Rect rect)
        {
            var rc = new Rectangle { Width = rect.Width, Height = rect.Height, Fill = vb };
            FixedPage.SetLeft(rc, rect.X);
            FixedPage.SetTop(rc, rect.Y);
            FixedPage.SetRight(rc, rect.Width);
            FixedPage.SetBottom(rc, rect.Height);
            return rc;
        }

        protected virtual void CreatePageVisual(Rect pageBounds, DrawingVisual source, bool isFooterPage, DrawingContext drawingContext)
        {
            drawingContext.DrawRectangle(null, FramePen, new Rect { X = FrameRect.X, Y = FrameRect.Y, Width = FrameRect.Width, Height = FrameRect.Height });
            var offsetX = PageMargins.Left - pageBounds.X - 1;
            var offsetY = PageMargins.Top - pageBounds.Y;
            drawingContext.PushTransform(new TranslateTransform(offsetX, offsetY + HeaderHeight));
            var pg = new Rect(new Point(pageBounds.X, pageBounds.Y), new Size(pageBounds.Width, pageBounds.Height));
            drawingContext.PushClip(new RectangleGeometry(pg));
            drawingContext.PushOpacityMask(Brushes.White);
            drawingContext.DrawDrawing(source.Drawing);
        }

        protected virtual int GetHorizontalPageCount(DrawingVisual drawingVisual)
        {
          if (drawingVisual == null)
            throw new ArgumentNullException("drawingVisual");

          return (int)Math.Ceiling(GetDrawingBounds(drawingVisual).Width / PrintablePageWidth);
        }


        protected virtual int GetMaxHorizalPageCount()
        {
          if (IsDrawingsNotNull())
          {
            var maxHorizontalPages = 0;
            foreach (var drawing in m_drawingVisualSources)
            {
              var pageCount = (int)Math.Ceiling(GetDrawingBounds(drawing).Width / PrintablePageWidth);
              // TODO:
              // extract the piece of code to method (make virtual as well)
              if (m_drawingPageMarkers.ContainsKey(drawing))
              {
                m_drawingPageMarkers[drawing].HorPageCount = pageCount;
              }
              else
              {
                m_drawingPageMarkers[drawing] = new DrawingVisualPageMarker { HorPageCount = pageCount };
              }
              if (pageCount > maxHorizontalPages) maxHorizontalPages = pageCount;
            }
            return maxHorizontalPages;
          }
          else
          {
            // todo: 
            // is the ArgumetnNullException possible good here? 
            throw new NullReferenceException("One of DrawingVisuals is null");
          }
        }


        private bool IsDrawingsNotNull()
        {
          foreach (var drawingVisual in m_drawingVisualSources)
          {
            if (drawingVisual.Drawing == null) return false;
          }
          return true;
        }

        protected virtual Rect GetDrawingBounds(DrawingVisual drawingVisual)
        {
          System.Diagnostics.Debug.Assert(drawingVisual != null, "GetDrawingBounds should happen after IsDrawingsNotNull");
          return drawingVisual.Drawing.Bounds;
        }

        public FixedDocument GetDocument(int startIndex, int endIndex)
        {
            var document = new FixedDocument();
            for (var i = startIndex; i < endIndex; i++)
            {
                var fp = new FixedPage { ContentBox = FrameRect, BleedBox = FrameRect };
                var page = GetPage(i);
                var vb = new DrawingBrush(DrawingVisuals[i].Drawing) { Stretch = Stretch.Uniform, ViewboxUnits = BrushMappingMode.Absolute, Viewbox = new Rect(page.Size) };
                fp.Children.Add(CreateContentRectangle(vb, FrameRect));
                var pageContent = new PageContent();
                ((IAddChild)pageContent).AddChild(fp);
                document.Pages.Add(pageContent);
            }
            document.DocumentPaginator.PageSize = new Size(PageSize.Width, PageSize.Height);
            return document;
        }

      #region Factory
      // @Summary
      //  Create a Visual Paginator from current Settings.
      // @TODO:
      //   impl
        public static VisualPaginator CreateFrom(DrawingVisual visual, Size printSize)
        {
          throw new NotImplementedException();
        }
      #endregion Factory
    }

    public class PageEventArgs : EventArgs
    {
        private readonly int _pageNumber;
        public int PageNumber { get { return _pageNumber; }}

        public PageEventArgs(int pageNumber)
        {
            _pageNumber = pageNumber;
        }
    }
}
