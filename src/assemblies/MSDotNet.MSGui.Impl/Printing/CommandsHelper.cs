﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using MorganStanley.MSDotNet.MSGui.Impl.Application;


namespace MorganStanley.MSDotNet.MSGui.Impl.Printing
{
    class CommandsHelper
    {
        public static bool UsePrism { get; set; }
        private static ICommandHeler _compositeCommandsHelper;
        private static ICommandHeler _prismCommandsHelper;
        public static  ICommandHeler CommandHeler
        {
            get
            {
                if (BootstrapperBase.UsePrismAsInfrasturcture)
                {
                    if (_prismCommandsHelper == null)
                        _prismCommandsHelper = new PrismCommandsHelper();
                    return _prismCommandsHelper;
                }
                else
                {
                    if (_compositeCommandsHelper == null)
                        _compositeCommandsHelper = new CompositeCommandsHelper();
                    return _compositeCommandsHelper;
                }
            }
        }

        private class CompositeCommandsHelper : ICommandHeler
        {

            public ICommand CreateDelegateCommand<T>(Action<T> execute)
            {
                return new Microsoft.Practices.Composite.Presentation.Commands.DelegateCommand<T>(execute);
            }
        }

        private class PrismCommandsHelper : ICommandHeler
        {
            public ICommand CreateDelegateCommand<T>(Action<T> execute)
            {
                return new Microsoft.Practices.Prism.Commands.DelegateCommand<T>(execute);
            }
        }
    
    }

    
}
