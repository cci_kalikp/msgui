﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;

namespace MorganStanley.MSDotNet.MSGui.Impl
{
    public static class DockHelper
    {
        internal static IEnumerable<IWindowViewContainerHost> GetPanes(IEnumerable<IWindowViewContainer> views_)
        {
            var result = new List<IWindowViewContainerHost>();

            foreach (WindowViewContainer windowViewContainer in views_)
            {
                if (windowViewContainer == null) continue;
                var pane = windowViewContainer.Visual as IWindowViewContainerHost;
                if (pane == null) continue;
                result.Add(pane);
            }
            return result;
        }

        public static IFloatingWindow GetRootOfFloatingPane(IWindowViewContainer view_)
        {
            IChildViewContainer childView = view_ as IChildViewContainer;
            if (childView != null)
            {
                view_ = childView.Parent;
            }
            var contentPanes = GetPanes(new[] { view_ });
            var contentPane = contentPanes.FirstOrDefault();
            if (contentPane == null) return null;
            return contentPane.FloatingWindow;
        }

        internal static ShellTabViewModel GetRootOfDockedPane(IWindowViewContainer view_)
        {
            IChildViewContainer childView = view_ as IChildViewContainer;
            if (childView != null)
            {
                view_ = childView.Parent;
            }
            var pane = ((WindowViewContainer)view_).Visual as FrameworkElement;
            if (pane == null) return null;
            WindowViewModel model = pane.DataContext as WindowViewModel;
            if (model == null) return null;
            var shellTab = model.Host as ShellTabViewModel;
            if (shellTab == null && !model.IsFloating && ChromeManagerBase.Instance.WindowManager is TabbedDockViewModel)
            {
                shellTab = ChromeManagerBase.Instance.Tabwell.ActiveTab as ShellTabViewModel;
            }
            return shellTab;
        }

        public static IWindowViewContainerHostRoot GetRootObject(IWindowViewContainer view_)
        {
            return GetRootOfFloatingPane(view_) as IWindowViewContainerHostRoot ?? GetRootOfDockedPane(view_);
        }

        public static void SetRootTitle(IWindowViewContainer view_, string title_)
        {
            var window = GetRootOfFloatingPane(view_);
            if (window != null)
            {
                window.Title = window.SubLayoutTitle = title_;
            }
            else
            {
                IDockTab tab = GetRootOfDockedPane(view_);
                if (tab != null)
                {
                    tab.Title = title_;
                }
            }
            if (view_ is IViewCollectionContainer)
            {
                view_.Title = title_;
            }
        }

        public static string GetRootTitle(IWindowViewContainer view_)
        {
            if (view_ is IViewCollectionContainer)
            {
                return view_.Title;
            }
            var window = GetRootOfFloatingPane(view_);
            if (window != null)
            {
                return window.SubLayoutTitle ?? window.Title ?? view_.Title;
            }
            else
            {
                IDockTab tab = GetRootOfDockedPane(view_);
                if (tab != null)
                {
                    return tab.Title ?? view_.Title;
                }
            }
            return view_.Title;
        }
        public static List<IWindowViewContainer> GetViewsInvolved(IWindowViewContainer rootView_)
        {
            if (rootView_ is TileViewContainer)
            {
                return new List<IWindowViewContainer>(new[] { rootView_ });
            }

            var rootPane = GetRootOfFloatingPane(rootView_) as IWindowViewContainerHostRoot ?? GetRootOfDockedPane(rootView_); 

            List<IWindowViewContainer> containers = new List<IWindowViewContainer>();
            containers.Add(rootView_);
            if (rootPane != null)
            {
                foreach (var window in rootPane.Windows)
                {
                    if (!containers.Contains(window))
                    {
                        containers.Add(window);
                    }
                }
            }
            return containers;
        }


        /// <summary>
        /// Checks if a given set of panes is a valid sublayout and returns the root panes.
        /// </summary>
        /// <param name="views">The sublayout candidate views</param>
        /// <returns>Root panes of the sublayout or null if <paramref name="views"/> contains an invalid set.</returns>
        internal static IEnumerable<IWindowViewContainerHostRoot> GetNormalizedSubLayout(IEnumerable<IWindowViewContainer> views)
        {
            var panes = GetPanes(views);
            var rootPanes = new List<IWindowViewContainerHostRoot>();

            if (panes == null)
                return null;

            // find all root panes
            foreach (var pane in panes)
            {
                var rootPane = GetRootOfFloatingPane(pane.View) as IWindowViewContainerHostRoot ?? GetRootOfDockedPane(pane.View);
                if (rootPane != null && !rootPanes.Contains(rootPane))
                {
                    rootPanes.Add(rootPane);
                } 

            }
             
            return rootPanes;
        }
 
    }
}
