/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/ControlParadigm/ControlCreator.cs#5 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Infragistics.Windows.Controls;
using Infragistics.Windows.Ribbon;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;

namespace MorganStanley.MSDotNet.MSGui.Impl.ControlParadigm
{/*
  internal class ControlCreator
  {
    private readonly CreateControlHandler m_createHandler;
    private readonly MSGuiButton m_button;
    private readonly UIElement m_control;
    
    public ControlCreator(MSGuiButton button_)
    {
      m_button = button_;
    }
    public ControlCreator(CreateControlHandler handler_)
    {
      m_createHandler = handler_;
    }
    public ControlCreator(UIElement control_)
    {
      m_control = control_;
    }    

    public object CreateControl(ShellMode shellMode_)
    {
      object item = null;
      if (m_button != null)
      {
        item = shellMode_ == ShellMode.RibbonWindow ? (object)GetButtonToolFromButton(m_button) : new LauncherBarButton(m_button);
      }
      else if (m_createHandler != null)
      {
        IWidget widget = new Widget();
        m_createHandler(widget);
        item = widget.MaximizedView ?? widget.MinimizedView;
      }
      else if (m_control != null)
      {
        item = m_control;
      }
      return item;
    }

    public bool IsPlaced
    {
      get; set;
    }

    private static ButtonTool GetButtonToolFromButton(MSGuiButton button_)
    {
      ButtonTool bt = new ButtonTool();
      if (button_.OnClick != null) bt.Click += new RoutedEventHandler(button_.OnClick);

      Binding textBinding =
        new Binding("Text")
          {
            Source = button_,
            Mode = BindingMode.TwoWay
          };
      bt.SetBinding(ButtonTool.CaptionProperty, textBinding);

      Binding imageBinding =
        new Binding("Image")
          {
            Source = button_,
            Mode = BindingMode.TwoWay
          };
      bt.SetBinding(ButtonTool.SmallImageProperty, imageBinding);
      bt.SetBinding(ButtonTool.LargeImageProperty, imageBinding);

      if (button_.ButtonSize == ButtonSize.Large)
      {
        RibbonGroup.SetMinimumSize(bt, RibbonToolSizingMode.ImageAndTextLarge);
        RibbonGroup.SetMaximumSize(bt, RibbonToolSizingMode.ImageAndTextLarge);
      }
      else
      {
        RibbonGroup.SetMinimumSize(bt, RibbonToolSizingMode.ImageAndTextNormal);
        RibbonGroup.SetMaximumSize(bt, RibbonToolSizingMode.ImageAndTextNormal);
      }

      bt.IsEnabled = button_.IsEnabled;

      Binding enabledBinding =
        new Binding("IsEnabled")
          {
            Source = button_,
            Mode = BindingMode.TwoWay
          };
      bt.SetBinding(UIElement.IsEnabledProperty, enabledBinding);

      //TODO if tooltip is not set straight away, it won't be shown 
      if (button_.ToolTip != null)
      {
        XamRibbonScreenTip tip = new XamRibbonScreenTip
                                   {
                                     Header = bt.Caption,
                                     FooterSeparatorVisibility = Visibility.Collapsed,
                                   };

        Binding tooltipHeaderBinding = new Binding("Caption")
                                         {
                                           Source = bt,
                                           Mode = BindingMode.TwoWay
                                         };
        tip.SetBinding(XamScreenTip.HeaderProperty, tooltipHeaderBinding);

        Binding tooltipBinding = new Binding("ToolTip")
                                   {
                                     Source = button_,
                                     Mode = BindingMode.TwoWay
                                   };
        tip.SetBinding(ContentControl.ContentProperty, tooltipBinding);

        bt.ToolTip = tip;
      }
      return bt;
    }
  }*/
}