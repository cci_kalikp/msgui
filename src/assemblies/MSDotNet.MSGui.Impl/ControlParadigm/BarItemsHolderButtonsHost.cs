﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/ControlParadigm/BarItemsHolderButtonsHost.cs#5 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System.Windows;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;

namespace MorganStanley.MSDotNet.MSGui.Impl.ControlParadigm
{/*
  internal class BarItemsHolderButtonsHost : IButtonsHost
  {
    private readonly BarItemsHolder m_holder;

    public BarItemsHolderButtonsHost(BarItemsHolder holder_)
    {
      m_holder = holder_;
      //TODO add Itemshost to holder
    }

    public void AddItem(object item_)
    {
      IWidget widget = item_ as IWidget;
      if (widget != null)
      {
        m_holder.AddWidget(widget);
        return;
      }

      LauncherBarButton button = item_ as LauncherBarButton;
      if (button != null)
      {
        m_holder.AddButton(button);
        return;
      }

      UIElement element = item_ as UIElement;
      if (element != null)
      {
        m_holder.AddChild(element);
        return;
      }
    }

    public void AddItem(object item_, int index_)
    {
      AddItem(item_);
    }

    public void RemoveItem(object item_)
    {
      //TODO
    }

    public void Clear()
    {
      //TODO
    }
  }*/
}
