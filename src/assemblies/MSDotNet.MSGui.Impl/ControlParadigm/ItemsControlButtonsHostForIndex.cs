﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/ControlParadigm/ItemsControlButtonsHostForIndex.cs#4 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System.Windows.Controls;

namespace MorganStanley.MSDotNet.MSGui.Impl.ControlParadigm
{
  internal class ItemsControlButtonsHostForIndex : IButtonsHost 
  {
    private readonly ItemsControl m_control;
    private readonly int m_first;
    private int m_last;

    //adding items to some existing ItemsControl that already has elements, so ItemsSource could not be used. starting from the given index 
    public ItemsControlButtonsHostForIndex(ItemsControl control_, int index_)
    {
      m_control = control_;
      m_first = index_;
      m_last = index_;
    }

    public void AddItem(object item_)
    {
      m_control.Items.Insert(m_last+1, item_);
      m_last++;
    }

    public void AddItem(object item_, int index_)
    {
      int index = m_first + index_;
      if (index > m_last + 1) 
      {
        index = m_last + 1;
      }
      m_control.Items.Insert(index, item_);
      m_last++;
    }

    public void RemoveItem(object item_)
    {
      for (int i = m_first; i <= m_last; i++)
      {
        if (m_control.Items[i] == item_)
        {
          m_control.Items.RemoveAt(i);
          m_last--;
          return;
        }
      }
    }

    public void Clear()
    {
      for (int i = m_first; i <= m_last; i++)
      {
        m_control.Items.RemoveAt(m_first);       
      }
      m_last = m_first;
    }
  }
}
