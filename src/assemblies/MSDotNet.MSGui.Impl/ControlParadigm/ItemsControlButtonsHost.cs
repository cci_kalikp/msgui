﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/ControlParadigm/ItemsControlButtonsHost.cs#6 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System.Collections.ObjectModel;
using System.Windows.Controls;

namespace MorganStanley.MSDotNet.MSGui.Impl.ControlParadigm
{
  internal class ItemsControlButtonsHost : IButtonsHost 
  {    
    private readonly ObservableCollection<object> m_items = new ObservableCollection<object>();
    private ItemsControl m_control;

    public ItemsControlButtonsHost(ItemsControl control_)
    {
      m_control = control_;
      control_.ItemsSource = m_items;
    }

    public ItemsControlButtonsHost()
    {      
    }

    public void SetControl(ItemsControl control_)
    {
      if (m_control != null)
      {
        control_.ItemsSource = null;
      }
      m_control = control_;
      control_.ItemsSource = m_items;
    }

    public void AddItem(object item_)
    {
      m_items.Add(item_);
    }

    public void AddItem(object item_, int index_)
    {
      m_items.Insert(index_, item_);
    }

    public void RemoveItem(object item_)
    {
      m_items.Remove(item_);
    }

    public void Clear()
    {
      m_items.Clear();
    }
  }
}
