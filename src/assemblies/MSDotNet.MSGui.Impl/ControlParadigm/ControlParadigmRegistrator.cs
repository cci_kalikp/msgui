﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/ControlParadigm/ControlParadigmRegistrator.cs#13 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.Collections.Generic;
using System.Windows;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.Ribbon;

namespace MorganStanley.MSDotNet.MSGui.Impl.ControlParadigm
{/*
  internal class ControlParadigmRegistrator : IControlParadigmRegistrator
  {
    public IList<ControlParadigmRegistration> Registrations = new List<ControlParadigmRegistration>();
    #region IControlParadigmRegistrator Members

    public void AddRibbonItem(string tab, string group, RibbonItem item)
    {
      ControlParadigmRegistration registration = new ControlParadigmRegistration() {tab = tab, group = group, RibbonItem = item};
      Registrations.Add(registration);
      InvokeObsoleteRibbonControlRegistered(registration);
    }

    public void AddMenuItem(RibbonButton ribbonButton)
    {
      var registration = new ControlParadigmRegistration() {ControlKey = "menu", RibbonItem = ribbonButton};
      Registrations.Add(registration);
      InvokeObsoleteRibbonControlRegistered(registration);
    }

    //--------------

    private readonly Dictionary<string, ControlCreator> m_creators = new Dictionary<string, ControlCreator>();

    internal Dictionary<string, ControlCreator> Creators
    {
      get
      {
        return m_creators;
      }
    }

    public void Register(
      string key_, 
      CreateControlHandler createControlHandler_)
    {
      if (m_creators.ContainsKey(key_))
      {
        throw new InvalidOperationException(
          string.Format("Key collision error. Creator with id {0} is already registered", key_));
      }
      m_creators[key_] = new ControlCreator(createControlHandler_);
      InvokeControlRegistered(key_);
    }

    public void Register(string key_, MSGuiButton button_)
    {
      if (m_creators.ContainsKey(key_))
      {
        throw new InvalidOperationException(
          string.Format("Key collision error. An item with id {0} is already registered", key_));
      }
      m_creators[key_] = new ControlCreator(button_);
      InvokeControlRegistered(key_);
    }

    public void Register(string key_, UIElement control_)
    {
      if (m_creators.ContainsKey(key_))
      {
        throw new InvalidOperationException(
          string.Format("Key collision error. An item with id {0} is already registered", key_));
      }
      m_creators[key_] = new ControlCreator(control_);
      InvokeControlRegistered(key_);
    }

    public bool IsRegistered(string key_)
    {
      return m_creators.ContainsKey(key_);
    }

    #endregion

    internal delegate void ControlRegisteredDelegate(string key_);
    internal event ControlRegisteredDelegate ControlRegistered;

    internal delegate void ObsoleteRibbonControlRegisteredDelegate(ControlParadigmRegistration registration_);
    internal event ObsoleteRibbonControlRegisteredDelegate ObsoleteRibbonControlRegistered;

    internal void InvokeControlRegistered(string key_)
    {
      ControlRegisteredDelegate handler = ControlRegistered;
      if (handler != null)
      {
        handler(key_);
      }
    }

    internal void InvokeObsoleteRibbonControlRegistered(ControlParadigmRegistration registration_)
    {
      var handler = ObsoleteRibbonControlRegistered;
      if (handler != null)
      {
        handler(registration_);
      }
    }    
  }

  internal struct ControlParadigmRegistration
  {
    internal string ControlKey { get; set;}
    internal string tab { get; set;}
    internal string group { get; set;}
    internal RibbonItem RibbonItem { get; set;}
    internal object ToolTip { get; set;}
  }*/
}
