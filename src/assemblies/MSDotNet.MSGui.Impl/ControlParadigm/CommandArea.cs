﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/ControlParadigm/CommandArea.cs#12 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System.Linq;
using System.Windows;
using Infragistics.Windows.Ribbon;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.My;
using MorganStanley.MSDotNet.MSGui.Core.Ribbon;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.Converters;
using System.ComponentModel;

namespace MorganStanley.MSDotNet.MSGui.Impl.ControlParadigm
{/*
	public class CommandArea : ICommandArea
	{
		internal static bool NoPlacedCheck { get; set; }
		private static readonly IMSLogger m_logger = MSLoggerFactory.CreateLogger<CommandArea>();
		private readonly IButtonsHost m_buttonsHost;
		private readonly ControlParadigmRegistrator m_registrator;
		private readonly ShellMode m_shellMode;
		internal string Name { get; set; }

		internal CommandArea(IButtonsHost buttonsHost_, ControlParadigmRegistrator registrator_, ShellMode shellMode_)
		{
			m_buttonsHost = buttonsHost_;
			m_registrator = registrator_;
			m_shellMode = shellMode_;
		}

		public void AddItem(string itemId_)
		{
			AddItem(itemId_, -1);
		}

		public void AddItem(string itemId_, InitialViewSettings ivs_)
		{
			AddItem(itemId_, -1, ivs_);
		}

		public void AddItem(string itemId_, int index_)
		{
			AddItem(itemId_, index_, new InitialViewSettings());
		}

		public void AddItem(string itemId_, int index_, InitialViewSettings ivs_)
		{
			int removeCandidateIndex = -1;
			foreach (var cpr in m_registrator.Registrations)
			{
				var ri = cpr.RibbonItem as RibbonButton;
				if (ri != null && ri.Text != null && ("RibbonItem" + ri.Text == itemId_))
				{
					var btn = new MSGuiButton()
					{
						Text = ri.Text,
						ToolTip = ri.ToolTip.ToString(),
						ButtonSize = ri.Size == RibbonButtonSize.Small ? ButtonSize.Small : ButtonSize.Large,
						Image = IconImageSourceConverter.IconToImageSource(ri.Icon),
						IsEnabled = ri.IsEnabled
					};


					btn.PropertyChanged += delegate(object sender_, PropertyChangedEventArgs e_)
					{
						if (e_.PropertyName == "IsEnabled")
							btn.IsEnabled = ri.IsEnabled;
					};

					if (!m_registrator.Creators.ContainsKey(itemId_))
					{
						m_registrator.Creators.Add(itemId_, new ControlCreator(btn));
						removeCandidateIndex = m_registrator.Registrations.IndexOf(cpr);
					}
					break;
				}
			}

			if (removeCandidateIndex >= 0)
				m_registrator.Registrations.RemoveAt(removeCandidateIndex);


			if (m_registrator.Creators.ContainsKey(itemId_))
			{
				ControlCreator creator = m_registrator.Creators[itemId_];
				if (creator.IsPlaced)
				{
					if (!NoPlacedCheck)
						m_logger.DebugWithFormat("Item {0} was tried to put at index {1} on {2}, but it was already placed", itemId_, index_, Name);
					return;
				}
				object item = creator.CreateControl(m_shellMode);
				if (item != null)
				{
					var depObj = item as DependencyObject;
					if (depObj != null && RibbonToolHelper.GetId(depObj) == null)
					{
						RibbonToolHelper.SetId(depObj, itemId_);
					}

					if (index_ < 0)
						m_buttonsHost.AddItem(item);
					else
						m_buttonsHost.AddItem(item, index_);

					ConcordExtensionStorage.ConcordInitialViewSettings.Add(this.Name + "->" + itemId_, ivs_);
				}
				creator.IsPlaced = true;
			}
		}


		public void RemoveItem(object itemId_)
		{
			if (itemId_ != null)
				RemoveItem(itemId_.ToString());
		}

		public void RemoveItem(string itemId_)
		{
			if (!string.IsNullOrWhiteSpace(itemId_))
			{
				if (m_registrator.Creators.ContainsKey(itemId_))
				{
					ControlCreator creator = m_registrator.Creators[itemId_];
					if (creator.IsPlaced)
					{
						m_logger.DebugWithFormat("Item {0} was tried to removed from {1}, but it was already placed", itemId_, Name);
						return;
					}
					m_registrator.Creators.Remove(itemId_);
				}
			}
		}

		public void Clear()
		{
			m_buttonsHost.Clear();
		}
	}*/
}