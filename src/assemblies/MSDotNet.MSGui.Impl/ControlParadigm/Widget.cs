﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/ControlParadigm/Widget.cs#8 $
// $Change: 901409 $
// $DateTime: 2014/10/17 05:24:50 $
// $Author: caijin $

using System;
using System.ComponentModel;
using System.Windows;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;

namespace MorganStanley.MSDotNet.MSGui.Impl.ControlParadigm
{
  internal class Widget : ViewModelBase, IWidget
  {
    private string m_title;
    private UIElement m_maximizedView;
    private UIElement m_minimizedView;
    private WidgetState m_state;
    private IWidgetViewContainer parent;
      public Widget(IWidgetViewContainer parent_)
      {
          this.parent = parent_;
      }
    public string Title
    {
      get 
      {
        return m_title;
      }
      set 
      {
        m_title = value;
        OnPropertyChanged("Title");
      }
    }

    public UIElement MaximizedView
    {
      get
      {
        return m_maximizedView;
      }
      set
      {
        m_maximizedView = value;
        OnPropertyChanged("MaximizedView");
      }
    }

    public UIElement MinimizedView
    {
      get
      {
        return m_minimizedView;
      }
      set
      {
        m_minimizedView = value;
        OnPropertyChanged("MinimizedView");
      }
    }

    public WidgetState State
    {
      get 
      { 
        return m_state; 
      }
      set
      {
        m_state = value;
        OnPropertyChanged("State");
      }
    }
       
      public IWidgetViewContainer Parent
      {
          get { return parent; }
      }
  }
}
