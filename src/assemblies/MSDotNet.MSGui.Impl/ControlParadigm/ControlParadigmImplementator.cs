﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/ControlParadigm/ControlParadigmImplementator.cs#39 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows;
using System.Windows.Controls;
using Infragistics.Windows.Ribbon;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.Ribbon;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl.BarAdapters;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Options;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.Ribbon;
using MorganStanley.MSDotNet.MSGui.Impl.Shell;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.Converters;
using MorganStanley.MSDotNet.My;
using System.Linq;
using System.Windows.Input;
using System.Windows.Forms;

namespace MorganStanley.MSDotNet.MSGui.Impl.ControlParadigm
{/*
	internal class ControlParadigmImplementator : IControlParadigmImplementator, IHostRegistry
	{
		private static readonly IMSLogger m_logger = MSLoggerFactory.CreateLogger<ControlParadigmImplementator>();

		private readonly ShellMode m_shellMode;
		private readonly ControlParadigmRegistrator m_registrator;
		private readonly IShell m_shell;
		private readonly RibbonInfragisticsImpl m_obsoleteRibbonImplementation;
		private readonly XamRibbon m_xamRibbon;
		private readonly BarItemsHolder m_barItemsHolder;
		private readonly BarControlsCreator m_barControlsCreator;
		private const string DEFAULT_TAB_NAME = "Main";
		private const string DEFAULT_GROUP_NAME = "Tools";
		private string m_defaultCommandAreaId;
		private string m_rootHostId;
		private bool m_placeAllCalled = false;

		public ControlParadigmImplementator(
			InitialSettings initialSettings_,
			ControlParadigmRegistrator registrator_,
			ApplicationOptions applicationOptions_,
			IShell shell_,
			PersistenceService persistenceService_)
		{
			m_logger.Debug("Creating ControlParadigmImplementator");
			m_shellMode = initialSettings_.ShellMode;
			m_registrator = registrator_;
			m_shell = shell_;
			m_barControlsCreator = new BarControlsCreator(m_shellMode);

			switch (m_shellMode)
			{
				case ShellMode.LauncherBarAndFloatingWindows:
				case ShellMode.LauncherBarAndWindow:
					m_logger.Debug("Initializing for one of shell modes with LauncherBar");
					m_rootHostId = "Root";
					m_defaultCommandAreaId = "Root";
					m_barItemsHolder = ((ShellWithLauncher)m_shell).Holder;
					m_commandAreas[m_rootHostId] = new CommandArea(new BarItemsHolderButtonsHost(m_barItemsHolder), m_registrator, m_shellMode) { Name = m_rootHostId };
					break;
				case ShellMode.RibbonWindow:
					m_logger.Debug("Initializing for shell mode with Ribbon");
					m_xamRibbon = ((IRibbonShell)m_shell).Ribbon;
					m_rootHostId = string.Format("Ribbon/{0}", DEFAULT_TAB_NAME);
					m_defaultCommandAreaId = string.Format("Ribbon/{0}/{1}", DEFAULT_TAB_NAME, DEFAULT_GROUP_NAME);
					m_obsoleteRibbonImplementation = new RibbonInfragisticsImpl(m_xamRibbon);
					m_commandAreas["QAT"] = new QatCommandArea(new ItemsControlButtonsHostForIndex(((IRibbonShell)m_shell).QAT, -1), m_xamRibbon);
					persistenceService_.AddPersistor(
						m_obsoleteRibbonImplementation.PersistorId,
						m_obsoleteRibbonImplementation.LoadState,
						m_obsoleteRibbonImplementation.SaveState);
					break;
			}
			m_commandAreas["Menu"] = new CommandArea(new ItemsControlButtonsHostForIndex(m_shell.MainMenu, 4), m_registrator, m_shellMode) { Name = "Menu" };
			const string optionsButtonKey = "MSGuiOptionsButton";
			m_registrator.Register(optionsButtonKey, applicationOptions_.CreateApplicationOptionsButton());
			this["Menu"].AddItem(optionsButtonKey); //we are adding it directly, not as a recommendation, which means that it can't be moved to another place
		}

		#region IControlParadigmImplementator Members

		public void PlaceAllControls()
		{
			CommandArea.NoPlacedCheck = true;
			m_logger.Debug("PlaceAllControls called");
			if (m_placeAllCalled)
			{
				m_logger.Error("PlaceAllControls was called multiple times");
				return;
			}
			m_placeAllCalled = true; //this function can effectively be called only once

			AddRegistrations();

			List<string> keys;
			List<string> processedKeys = new List<string>();

			keys = new List<string>(m_registrator.Creators.Keys);

			while (keys.Count() > 0)
			{
				PlaceControls(keys);
				processedKeys.AddRange(keys);
				keys = new List<string>(m_registrator.Creators.Keys.Except(processedKeys));
			}

			m_registrator.ControlRegistered += PlaceControlForCreator;


			#region Register the shortcuts on the window.
			//if (ConcordExtensionStorage.KeyBindings.Count > 0 && this.m_xamRibbon != null)
			//{
			//    Window window = Window.GetWindow(this.m_xamRibbon);
			//    foreach (object tool in ConcordExtensionStorage.KeyBindings.Keys)
			//    {
			//        ConcordExtensionStorage.ShortcutMetadata shortcutMetadata = ConcordExtensionStorage.KeyBindings[tool];

			//        if (shortcutMetadata.Shortcut != System.Windows.Forms.Shortcut.None)
			//        {
			//            var k = Key.MediaStop;
			//            var mk = ModifierKeys.None;

			//            #region Shortcut mapping...
			//            switch (shortcutMetadata.Shortcut)
			//            {
			//                case Shortcut.None:
			//                    k = Key.None;
			//                    mk = ModifierKeys.None;
			//                    break;
			//                case Shortcut.Ins:
			//                    k = Key.Insert;
			//                    mk = ModifierKeys.None;
			//                    break;
			//                case Shortcut.Del:
			//                    k = Key.Delete;
			//                    mk = ModifierKeys.None;
			//                    break;
			//                case Shortcut.F1:
			//                    k = Key.F1;
			//                    mk = ModifierKeys.None;
			//                    break;
			//                case Shortcut.F2:
			//                    k = Key.F2;
			//                    mk = ModifierKeys.None;
			//                    break;
			//                case Shortcut.F3:
			//                    k = Key.F3;
			//                    mk = ModifierKeys.None;
			//                    break;
			//                case Shortcut.F4:
			//                    k = Key.F4;
			//                    mk = ModifierKeys.None;
			//                    break;
			//                case Shortcut.F5:
			//                    k = Key.F5;
			//                    mk = ModifierKeys.None;
			//                    break;
			//                case Shortcut.F6:
			//                    k = Key.F6;
			//                    mk = ModifierKeys.None;
			//                    break;
			//                case Shortcut.F7:
			//                    k = Key.F7;
			//                    mk = ModifierKeys.None;
			//                    break;
			//                case Shortcut.F8:
			//                    k = Key.F8;
			//                    mk = ModifierKeys.None;
			//                    break;
			//                case Shortcut.F9:
			//                    k = Key.F9;
			//                    mk = ModifierKeys.None;
			//                    break;
			//                case Shortcut.F10:
			//                    k = Key.F10;
			//                    mk = ModifierKeys.None;
			//                    break;
			//                case Shortcut.F11:
			//                    k = Key.F11;
			//                    mk = ModifierKeys.None;
			//                    break;
			//                case Shortcut.F12:
			//                    k = Key.F12;
			//                    mk = ModifierKeys.None;
			//                    break;
			//                case Shortcut.ShiftIns:
			//                    k = Key.Insert;
			//                    mk = ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.ShiftDel:
			//                    k = Key.Delete;
			//                    mk = ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.ShiftF1:
			//                    k = Key.F1;
			//                    mk = ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.ShiftF2:
			//                    k = Key.F2;
			//                    mk = ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.ShiftF3:
			//                    k = Key.F3;
			//                    mk = ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.ShiftF4:
			//                    k = Key.F4;
			//                    mk = ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.ShiftF5:
			//                    k = Key.F5;
			//                    mk = ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.ShiftF6:
			//                    k = Key.F6;
			//                    mk = ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.ShiftF7:
			//                    k = Key.F7;
			//                    mk = ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.ShiftF8:
			//                    k = Key.F8;
			//                    mk = ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.ShiftF9:
			//                    k = Key.F9;
			//                    mk = ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.ShiftF10:
			//                    k = Key.F10;
			//                    mk = ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.ShiftF11:
			//                    k = Key.F11;
			//                    mk = ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.ShiftF12:
			//                    k = Key.F12;
			//                    mk = ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.CtrlIns:
			//                    k = Key.Insert;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.CtrlDel:
			//                    k = Key.Delete;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.Ctrl0:
			//                    k = Key.D0;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.Ctrl1:
			//                    k = Key.D1;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.Ctrl2:
			//                    k = Key.D2;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.Ctrl3:
			//                    k = Key.D3;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.Ctrl4:
			//                    k = Key.D4;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.Ctrl5:
			//                    k = Key.D5;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.Ctrl6:
			//                    k = Key.D6;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.Ctrl7:
			//                    k = Key.D7;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.Ctrl8:
			//                    k = Key.D8;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.Ctrl9:
			//                    k = Key.D9;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.CtrlA:
			//                    k = Key.A;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.CtrlB:
			//                    k = Key.B;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.CtrlC:
			//                    k = Key.C;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.CtrlD:
			//                    k = Key.D;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.CtrlE:
			//                    k = Key.E;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.CtrlF:
			//                    k = Key.F;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.CtrlG:
			//                    k = Key.G;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.CtrlH:
			//                    k = Key.H;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.CtrlI:
			//                    k = Key.I;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.CtrlJ:
			//                    k = Key.J;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.CtrlK:
			//                    k = Key.K;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.CtrlL:
			//                    k = Key.L;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.CtrlM:
			//                    k = Key.M;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.CtrlN:
			//                    k = Key.N;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.CtrlO:
			//                    k = Key.O;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.CtrlP:
			//                    k = Key.P;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.CtrlQ:
			//                    k = Key.Q;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.CtrlR:
			//                    k = Key.R;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.CtrlS:
			//                    k = Key.S;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.CtrlT:
			//                    k = Key.T;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.CtrlU:
			//                    k = Key.U;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.CtrlV:
			//                    k = Key.V;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.CtrlW:
			//                    k = Key.W;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.CtrlX:
			//                    k = Key.X;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.CtrlY:
			//                    k = Key.Y;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.CtrlZ:
			//                    k = Key.Z;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.CtrlF1:
			//                    k = Key.F1;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.CtrlF2:
			//                    k = Key.F2;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.CtrlF3:
			//                    k = Key.F3;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.CtrlF4:
			//                    k = Key.F4;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.CtrlF5:
			//                    k = Key.F5;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.CtrlF6:
			//                    k = Key.F6;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.CtrlF7:
			//                    k = Key.F7;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.CtrlF8:
			//                    k = Key.F8;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.CtrlF9:
			//                    k = Key.F9;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.CtrlF10:
			//                    k = Key.F10;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.CtrlF11:
			//                    k = Key.F11;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.CtrlF12:
			//                    k = Key.F12;
			//                    mk = ModifierKeys.Control;
			//                    break;
			//                case Shortcut.CtrlShift0:
			//                    k = Key.D0;
			//                    mk = ModifierKeys.Control | ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.CtrlShift1:
			//                    k = Key.D1;
			//                    mk = ModifierKeys.Control | ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.CtrlShift2:
			//                    k = Key.D2;
			//                    mk = ModifierKeys.Control | ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.CtrlShift3:
			//                    k = Key.D3;
			//                    mk = ModifierKeys.Control | ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.CtrlShift4:
			//                    k = Key.D4;
			//                    mk = ModifierKeys.Control | ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.CtrlShift5:
			//                    k = Key.D5;
			//                    mk = ModifierKeys.Control | ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.CtrlShift6:
			//                    k = Key.D6;
			//                    mk = ModifierKeys.Control | ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.CtrlShift7:
			//                    k = Key.D7;
			//                    mk = ModifierKeys.Control | ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.CtrlShift8:
			//                    k = Key.D8;
			//                    mk = ModifierKeys.Control | ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.CtrlShift9:
			//                    k = Key.D9;
			//                    mk = ModifierKeys.Control | ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.CtrlShiftA:
			//                    k = Key.A;
			//                    mk = ModifierKeys.Control | ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.CtrlShiftB:
			//                    k = Key.B;
			//                    mk = ModifierKeys.Control | ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.CtrlShiftC:
			//                    k = Key.C;
			//                    mk = ModifierKeys.Control | ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.CtrlShiftD:
			//                    k = Key.D;
			//                    mk = ModifierKeys.Control | ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.CtrlShiftE:
			//                    k = Key.E;
			//                    mk = ModifierKeys.Control | ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.CtrlShiftF:
			//                    k = Key.F;
			//                    mk = ModifierKeys.Control | ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.CtrlShiftG:
			//                    k = Key.G;
			//                    mk = ModifierKeys.Control | ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.CtrlShiftH:
			//                    k = Key.H;
			//                    mk = ModifierKeys.Control | ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.CtrlShiftI:
			//                    k = Key.I;
			//                    mk = ModifierKeys.Control | ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.CtrlShiftJ:
			//                    k = Key.J;
			//                    mk = ModifierKeys.Control | ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.CtrlShiftK:
			//                    k = Key.K;
			//                    mk = ModifierKeys.Control | ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.CtrlShiftL:
			//                    k = Key.L;
			//                    mk = ModifierKeys.Control | ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.CtrlShiftM:
			//                    k = Key.M;
			//                    mk = ModifierKeys.Control | ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.CtrlShiftN:
			//                    k = Key.N;
			//                    mk = ModifierKeys.Control | ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.CtrlShiftO:
			//                    k = Key.O;
			//                    mk = ModifierKeys.Control | ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.CtrlShiftP:
			//                    k = Key.P;
			//                    mk = ModifierKeys.Control | ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.CtrlShiftQ:
			//                    k = Key.Q;
			//                    mk = ModifierKeys.Control | ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.CtrlShiftR:
			//                    k = Key.R;
			//                    mk = ModifierKeys.Control | ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.CtrlShiftS:
			//                    k = Key.S;
			//                    mk = ModifierKeys.Control | ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.CtrlShiftT:
			//                    k = Key.T;
			//                    mk = ModifierKeys.Control | ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.CtrlShiftU:
			//                    k = Key.U;
			//                    mk = ModifierKeys.Control | ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.CtrlShiftV:
			//                    k = Key.V;
			//                    mk = ModifierKeys.Control | ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.CtrlShiftW:
			//                    k = Key.W;
			//                    mk = ModifierKeys.Control | ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.CtrlShiftX:
			//                    k = Key.X;
			//                    mk = ModifierKeys.Control | ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.CtrlShiftY:
			//                    k = Key.Y;
			//                    mk = ModifierKeys.Control | ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.CtrlShiftZ:
			//                    k = Key.Z;
			//                    mk = ModifierKeys.Control | ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.CtrlShiftF1:
			//                    k = Key.F1;
			//                    mk = ModifierKeys.Control | ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.CtrlShiftF2:
			//                    k = Key.F2;
			//                    mk = ModifierKeys.Control | ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.CtrlShiftF3:
			//                    k = Key.F3;
			//                    mk = ModifierKeys.Control | ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.CtrlShiftF4:
			//                    k = Key.F4;
			//                    mk = ModifierKeys.Control | ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.CtrlShiftF5:
			//                    k = Key.F5;
			//                    mk = ModifierKeys.Control | ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.CtrlShiftF6:
			//                    k = Key.F6;
			//                    mk = ModifierKeys.Control | ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.CtrlShiftF7:
			//                    k = Key.F7;
			//                    mk = ModifierKeys.Control | ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.CtrlShiftF8:
			//                    k = Key.F8;
			//                    mk = ModifierKeys.Control | ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.CtrlShiftF9:
			//                    k = Key.F9;
			//                    mk = ModifierKeys.Control | ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.CtrlShiftF10:
			//                    k = Key.F10;
			//                    mk = ModifierKeys.Control | ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.CtrlShiftF11:
			//                    k = Key.F11;
			//                    mk = ModifierKeys.Control | ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.CtrlShiftF12:
			//                    k = Key.F12;
			//                    mk = ModifierKeys.Control | ModifierKeys.Shift;
			//                    break;
			//                case Shortcut.AltBksp:
			//                    k = Key.Back;
			//                    mk = ModifierKeys.Alt;
			//                    break;
			//                case Shortcut.AltLeftArrow:
			//                    k = Key.Left;
			//                    mk = ModifierKeys.Alt;
			//                    break;
			//                case Shortcut.AltUpArrow:
			//                    k = Key.Up;
			//                    mk = ModifierKeys.Alt;
			//                    break;
			//                case Shortcut.AltRightArrow:
			//                    k = Key.Right;
			//                    mk = ModifierKeys.Alt;
			//                    break;
			//                case Shortcut.AltDownArrow:
			//                    k = Key.Down;
			//                    mk = ModifierKeys.Alt;
			//                    break;
			//                case Shortcut.Alt0:
			//                    k = Key.D0;
			//                    mk = ModifierKeys.Alt;
			//                    break;
			//                case Shortcut.Alt1:
			//                    k = Key.D1;
			//                    mk = ModifierKeys.Alt;
			//                    break;
			//                case Shortcut.Alt2:
			//                    k = Key.D2;
			//                    mk = ModifierKeys.Alt;
			//                    break;
			//                case Shortcut.Alt3:
			//                    k = Key.D3;
			//                    mk = ModifierKeys.Alt;
			//                    break;
			//                case Shortcut.Alt4:
			//                    k = Key.D4;
			//                    mk = ModifierKeys.Alt;
			//                    break;
			//                case Shortcut.Alt5:
			//                    k = Key.D5;
			//                    mk = ModifierKeys.Alt;
			//                    break;
			//                case Shortcut.Alt6:
			//                    k = Key.D6;
			//                    mk = ModifierKeys.Alt;
			//                    break;
			//                case Shortcut.Alt7:
			//                    k = Key.D7;
			//                    mk = ModifierKeys.Alt;
			//                    break;
			//                case Shortcut.Alt8:
			//                    k = Key.D8;
			//                    mk = ModifierKeys.Alt;
			//                    break;
			//                case Shortcut.Alt9:
			//                    k = Key.D9;
			//                    mk = ModifierKeys.Alt;
			//                    break;
			//                case Shortcut.AltF1:
			//                    k = Key.F1;
			//                    mk = ModifierKeys.Alt;
			//                    break;
			//                case Shortcut.AltF2:
			//                    k = Key.F2;
			//                    mk = ModifierKeys.Alt;
			//                    break;
			//                case Shortcut.AltF3:
			//                    k = Key.F3;
			//                    mk = ModifierKeys.Alt;
			//                    break;
			//                case Shortcut.AltF4:
			//                    k = Key.F4;
			//                    mk = ModifierKeys.Alt;
			//                    break;
			//                case Shortcut.AltF5:
			//                    k = Key.F5;
			//                    mk = ModifierKeys.Alt;
			//                    break;
			//                case Shortcut.AltF6:
			//                    k = Key.F6;
			//                    mk = ModifierKeys.Alt;
			//                    break;
			//                case Shortcut.AltF7:
			//                    k = Key.F7;
			//                    mk = ModifierKeys.Alt;
			//                    break;
			//                case Shortcut.AltF8:
			//                    k = Key.F8;
			//                    mk = ModifierKeys.Alt;
			//                    break;
			//                case Shortcut.AltF9:
			//                    k = Key.F9;
			//                    mk = ModifierKeys.Alt;
			//                    break;
			//                case Shortcut.AltF10:
			//                    k = Key.F10;
			//                    mk = ModifierKeys.Alt;
			//                    break;
			//                case Shortcut.AltF11:
			//                    k = Key.F11;
			//                    mk = ModifierKeys.Alt;
			//                    break;
			//                case Shortcut.AltF12:
			//                    k = Key.F12;
			//                    mk = ModifierKeys.Alt;
			//                    break;
			//            }
			//            #endregion

			//            if (shortcutMetadata.Command != null)
			//            {
			//                var keyBinding = new KeyBinding(shortcutMetadata.Command, k, mk);
			//                window.InputBindings.Add(keyBinding);
			//            }
			//        }
			//    }
			//}
			#endregion

			CommandArea.NoPlacedCheck = false;
		}

		private void PlaceControls(IEnumerable<string> keys_)
		{
			m_logger.DebugWithFormat("PlaceControls called for {0} keys", keys_.Count());
			ICollection<string> waitingListKeys = new LinkedList<string>();
			foreach (string key in keys_)
			{
				if (m_recommendations.ContainsKey(key))
				{
					this[m_recommendations[key]].AddItem(key);
				}
				else
				{
					waitingListKeys.Add(key);
				}
			}
			if (waitingListKeys.Count == keys_.Count()) //nothing changed adding the rest to default
			{
				foreach (var waitingListKey in waitingListKeys)
				{
					this[m_defaultCommandAreaId].AddItem(waitingListKey);
				}
			}
			else
			{
				PlaceControls(waitingListKeys);
			}
		}

		private void PlaceControlForCreator(string key_)
		{
			m_logger.DebugWithFormat("PlaceControlForCreator '{0}'", key_);
			if (m_recommendations.ContainsKey(key_))
			{
				string hostPath = m_recommendations[key_];
				this[hostPath].AddItem(key_);
			}
			//TODO or default
		}

		public void SetOnClosingAction(Action OnClosing)
		{
			m_logger.Debug("SetOnClosingAction");
			m_shell.OnClosing = OnClosing;
		}

		[Obsolete]
		public void SetRibbonIcon(Icon icon_)
		{
			m_logger.Debug("Setting ribbon icon");
			if (m_obsoleteRibbonImplementation != null)
			{
				m_obsoleteRibbonImplementation.Icon = icon_;
			}
		}
		#endregion

		private static IEnumerable<MSGuiButton> RibbonItemToButtons(RibbonItem item_)
		{
			List<MSGuiButton> result = new List<MSGuiButton>();

			if (item_ is RibbonButton)
			{
				RibbonButton ribbonButton = (RibbonButton)item_;
				var msguiButton = new MSGuiButton
											{
												Text = ribbonButton.Text,
												Image = IconImageSourceConverter.IconToImageSource(ribbonButton.Icon),
												IsEnabled = ribbonButton.IsEnabled,
												ToolTip = ribbonButton.ToolTip.ToString(), //TODO shall we use objects for toolTips too?
												OnClick = ribbonButton.Clicked,
												ButtonSize = ribbonButton.Size == RibbonButtonSize.Large ? ButtonSize.Large : ButtonSize.Small
											};

				ribbonButton.PropertyChanged += delegate(object sender_, PropertyChangedEventArgs e_)
													{
														switch (e_.PropertyName)
														{
															case "IsEnabled":
																msguiButton.IsEnabled = ribbonButton.IsEnabled;
																break;
														}
													};
				result.Add(msguiButton);
				return result;
			}

			if (item_ is RibbonButtonGroup)
			{
				RibbonButtonGroup itemGroup = item_ as RibbonButtonGroup;
				ButtonGroup btnGroup = new ButtonGroup();

				foreach (RibbonButton rbtn in itemGroup.Buttons)
				{
					result.AddRange(RibbonItemToButtons(rbtn));
				}
				return result;
			}

			if (item_ is WrapperPannelRibbonItem)
			{
				WrapperPannelRibbonItem wp = item_ as WrapperPannelRibbonItem;
				System.Windows.Controls.Panel igPanel;

				foreach (RibbonItem rbItem in wp.Items)
				{
					result.AddRange(RibbonItemToButtons(rbItem));
				}
				return result;
			}
			return result;
		}

		#region recommendations
		private readonly Dictionary<string, string> m_recommendations = new Dictionary<string, string>();
		public void AddRecommendation(string childId_, string hostId_)
		{
			m_logger.DebugWithFormat("Adding recommendation '{0}' '{1}'", childId_, hostId_);
			m_recommendations[childId_] = hostId_;
			if (m_registrator.Creators.ContainsKey(childId_) && m_commandAreas.ContainsKey(hostId_))
			{
				//TODO we currently don't do anything here, but in theory, if we are in the middle of placing controls and one of newly placed items has added a recommendation, related to some other control that is wainting for it in the waiting list, some actions might be taken here
			}
		}

		public void RemoveRecommendation(string childId_)
		{
			m_logger.DebugWithFormat("Removing recommendation '{0}'", childId_);
			m_recommendations.Remove(childId_);
		}

		public void ClearRecommendationsForHost(string hostId_)
		{
			m_logger.DebugWithFormat("Removing all recommendations for host '{0}'", hostId_);
			foreach (KeyValuePair<string, string> keyValuePair in new Dictionary<string, string>(m_recommendations))
			{
				if (keyValuePair.Value == hostId_)
				{
					m_recommendations.Remove(keyValuePair.Key);
				}
			}
		}
		#endregion

		public string AddHost(IButtonsHost host_, string suggestedId_)
		{
			m_logger.DebugWithFormat("Adding new host with suggested id '{0}'", suggestedId_);
			string key = GetUnusedId(suggestedId_, x => m_commandAreas.ContainsKey(x));
			m_commandAreas[key] = new CommandArea(host_, m_registrator, m_shellMode) { Name = key };
			return key;
		}

		public string AddRootHost(string name_)
		{
			m_logger.DebugWithFormat("Adding new root host with name '{0}'", name_);
			string key = string.Format("{0}/{1}", m_rootHostId, name_);
			key = GetUnusedId(key, x => m_commandAreas.ContainsKey(x));
			switch (m_shellMode)
			{
				case ShellMode.RibbonWindow:
					return CreateRibbonGroupHost(name_, DEFAULT_TAB_NAME, key);
				case ShellMode.LauncherBarAndFloatingWindows:
				case ShellMode.LauncherBarAndWindow:
					return CreateBarGroupHost(name_, key);
				default:
					throw new ArgumentOutOfRangeException();
			}
			throw new ArgumentOutOfRangeException();
		}

		public string AddMenuRootHost(string name_)
		{
			m_logger.DebugWithFormat("Adding new root menu host with name '{0}'", name_);
			string key = string.Format("{0}/{1}", m_defaultCommandAreaId, name_);
			key = GetUnusedId(key, x => m_commandAreas.ContainsKey(x));
			switch (m_shellMode)
			{
				case ShellMode.RibbonWindow:
				case ShellMode.LauncherBarAndFloatingWindows:
				case ShellMode.LauncherBarAndWindow: //currently nothing specific to the shell mode, but it could be changed
					return CreateMenuHost(name_, this[m_defaultCommandAreaId], key);
				default:
					throw new ArgumentOutOfRangeException();
			}
			throw new ArgumentOutOfRangeException();
		}

		#region command areas
		private readonly Dictionary<string, ICommandArea> m_commandAreas = new Dictionary<string, ICommandArea>(); //try not to add items using to dictionary's items directly. if you can't guarantee that the area for the key does exist, use this[key] instead
		public ICommandArea this[string id_]
		{
			get
			{
				m_logger.DebugWithFormat("Getting command area with id_ '{0}'", id_);
				if (m_commandAreas.ContainsKey(id_))
				{
					return m_commandAreas[id_];
				}
				m_logger.DebugWithFormat("Command area with id_ '{0}' not found. Creating one.", id_);

				var ca = CreateCommandAreaByName(id_);
				if (ca as CommandArea != null)
					(ca as CommandArea).Name = id_;

				return ca;
			}
		}

		private ICommandArea CreateCommandAreaByName(string id_)
		{
			m_logger.DebugWithFormat("Creating command area with id_ '{0}'.", id_);
			string[] parts = id_.Split('/');
			switch (m_shellMode)
			{
				case ShellMode.RibbonWindow:
					if (id_.StartsWith("Ribbon/") && parts.Length == 3)
					{
						string hostId = CreateRibbonGroupHost(parts[2], parts[1], id_);
						return m_commandAreas[hostId];
					}
					return m_commandAreas[CreateRibbonGroupHost(id_, DEFAULT_TAB_NAME, id_)];
				case ShellMode.LauncherBarAndFloatingWindows:
				case ShellMode.LauncherBarAndWindow:
					if (id_.StartsWith("Ribbon/") && parts.Length == 3)
					{
						string hostId = CreateBarGroupHost(parts[2], id_);
						return m_commandAreas[hostId];
					}
					return m_commandAreas[CreateBarGroupHost(id_, id_)];
				default:
					throw new ArgumentOutOfRangeException();
			}

		}

		#endregion

		public static string CreateCommandBarKey(string controlRegKey_, string key_)
		{
			return string.Format("{{{0}}}/{1}", controlRegKey_, key_);
		}

		private void AddRegistrations()
		{
			m_logger.Debug("Adding registrations");
			switch (m_shellMode)
			{
				case ShellMode.LauncherBarAndFloatingWindows:
				case ShellMode.LauncherBarAndWindow:
				case ShellMode.RibbonWindow:
					foreach (ControlParadigmRegistration controlParadigmRegistration in m_registrator.Registrations)
					{
						AddObsoleteRegistration(controlParadigmRegistration);
					}
					m_registrator.ObsoleteRibbonControlRegistered += AddObsoleteRegistration;
					break;
				//case ShellMode.RibbonWindow: // TODO small buttons like "Show" and "+/-" in ExampleFrameworkApp are not small after this change. You can uncomment the block below to roll it back or rewrite RibbonItemToButtons to return smth other than msguibuttons
				//  foreach (ControlParadigmRegistration controlParadigmRegistration in m_registrator.Registrations)
				//  {
				//    if (controlParadigmRegistration.ControlKey == "menu")
				//    {
				//      m_obsoleteRibbonImplementation.AddMenuItem(controlParadigmRegistration.RibbonItem);
				//    }
				//    else
				//    {
				//      m_obsoleteRibbonImplementation.AddRibbonItem(controlParadigmRegistration.tab, controlParadigmRegistration.group, controlParadigmRegistration.RibbonItem);
				//    }
				//  }
			}
		}

		private void AddObsoleteRegistration(ControlParadigmRegistration controlParadigmRegistration)
		{
			foreach (MSGuiButton msguiButton in RibbonItemToButtons(controlParadigmRegistration.RibbonItem))
			{
				string areaKey = controlParadigmRegistration.ControlKey == "menu"
									 ? "Menu" //TODO if it is a button, a button (not a menuitem) is being added to the menu.
									 : string.Format("Ribbon/{0}/{1}", controlParadigmRegistration.tab,
													 controlParadigmRegistration.group);
				string controlKey = GetUnusedId("RibbonItem" + msguiButton.Text ?? string.Empty, x => m_registrator.IsRegistered(x));
				m_registrator.Register(controlKey, msguiButton);
				m_recommendations.Add(controlKey, areaKey); //this is only a recommendation - this item can be moved to another place and it won't be shown if you don't enable legacy controls placement
				//this[areaKey].AddItem(controlKey);        //if you want it to be unmovabale and always added, comment out the line above and uncomment this line
			}
		}

		private static string GetUnusedId(string suggestedId_, Predicate<string> predicate_)
		{
			string id = suggestedId_;
			while (predicate_(id))
			{
				id += "_";
			}
			return id;
		}

		private string CreateRibbonGroupHost(string groupName_, string tabName_, string suggestedId_)
		{
			m_logger.DebugWithFormat("CreateRibbonGroupHost groupName_ = '{0}' id_ = '{1}'", groupName_, suggestedId_);
			RibbonGroup ribbonGroup = Concord.ConcordImpl.BarAdapters.BarControlsCreator.CreateRibbonGroup(groupName_);
			RibbonTabItem ribbonTab = XamRibbonHelper.CreateOrFindRibbonTab(m_xamRibbon, tabName_);
			ribbonTab.RibbonGroups.Add(ribbonGroup);
			ItemsControlButtonsHost host = new ItemsControlButtonsHost(ribbonGroup);
			return AddHost(host, suggestedId_);
		}

		private string CreateBarGroupHost(string name_, string suggestedId_)
		{
			m_logger.DebugWithFormat("CreateBarGroupHost groupName_ = '{0}' id_ = '{1}'", name_, suggestedId_);

			FrameworkElementFactory factoryPanel = new FrameworkElementFactory(typeof(WrapPanel));
			factoryPanel.SetValue(WrapPanel.OrientationProperty, System.Windows.Controls.Orientation.Vertical);
			ItemsPanelTemplate template = new ItemsPanelTemplate { VisualTree = factoryPanel };

			ItemsControl listbox = new ItemsControl() { ItemsPanel = template };
			System.Windows.Controls.GroupBox box = new System.Windows.Controls.GroupBox() { Header = name_, Content = listbox, Margin = new Thickness(2) };
			string controlKey = GetUnusedId(suggestedId_, x => m_registrator.IsRegistered(x));
			m_registrator.Register(controlKey, box);
			m_commandAreas[m_rootHostId].AddItem(controlKey);
			ItemsControlButtonsHost host = new ItemsControlButtonsHost(listbox);
			return AddHost(host, suggestedId_);
		}

		private string CreateMenuHost(string menuName_, ICommandArea parentArea_, string suggestedId_)
		{
			m_logger.DebugWithFormat("CreateMenuHost menuName_ = '{0}' suggestedId_ = '{1}'", menuName_, suggestedId_);
			ItemsControl itemsControl;
			var menuBar = m_barControlsCreator.CreateMenuBar(menuName_, out itemsControl);
			string controlKey = GetUnusedId(suggestedId_, x => m_registrator.IsRegistered(x));
			m_registrator.Register(controlKey, menuBar);
			parentArea_.AddItem(controlKey);
			ItemsControlButtonsHost host = new ItemsControlButtonsHost(itemsControl);
			return AddHost(host, controlKey);
		}
	}*/
}
