﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/ControlParadigm/IHostRegistry.cs#5 $
// $Change: 817043 $
// $DateTime: 2013/02/19 09:28:20 $
// $Author: milosp $

using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.ControlParadigm
{
  internal interface IHostRegistry
  {
    string AddHost(IButtonsHost host_, string suggestedId_);
    string AddRootHost(string name_);
    string AddMenuRootHost(string name_);
    //bool TryGetHost(string id_, out IButtonsHost host_);
    void AddRecommendation(string childId_, string hostId_, InitialWidgetParameters parameters);
    void RemoveRecommendation(string childId_);
    void ClearRecommendationsForHost(string hostId_);
  }
}
