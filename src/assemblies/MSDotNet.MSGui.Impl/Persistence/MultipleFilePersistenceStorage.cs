/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Persistence/MultipleFilePersistenceStorage.cs#26 $
// $Change: 886109 $
// $DateTime: 2014/06/25 16:49:32 $
// $Author: hrechkin $

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using MorganStanley.MSDotNet.My;

namespace MorganStanley.MSDotNet.MSGui.Impl.Persistence
{
	public class MultipleFilePersistenceStorage : BaseFilesystemPersistenceStorage, IPartialPersistenceStorage
	{
		private const string DIRNAME_DOS_PATTERN = "*_state";
		private const string DIRNAME_PATTERN = "{0}_state";
		private const string STATE_EXTENSION = ".msguistate";
		private const string SUBSTATE_EXTENSION = ".msguisubstate";
		private const string INFO_FILENAME = "msgui.stateinfo";
		private const string SAVE_TIME_ATTR_NAME = "SaveTime";
		private static readonly IMSLogger m_logger = MSLoggerFactory.CreateLogger<MultipleFilePersistenceStorage>();

		private static readonly Regex m_dirnameRegexp = new Regex("^(.*)_state$", RegexOptions.Compiled);

		public MultipleFilePersistenceStorage(Framework framework)
			: this(framework, null)
		{
		}

		public MultipleFilePersistenceStorage(Framework framework, string presetProfilesLocation)
			: base(framework.Bootstrapper.Application.Name, presetProfilesLocation)
		{
		}

		public MultipleFilePersistenceStorage(string applicationName) : base(applicationName)
		{
		}

		public MultipleFilePersistenceStorage(string applicationName, string presetDirectory)
			: base(applicationName, presetDirectory)
		{
		}

		#region IPartialPersistenceStorage Members

		public void SaveState(string profile_, Dictionary<string, XElement> items_, Dictionary<string, XElement> subitems_,
		                      bool clean_)
		{
			DateTime now = DateTime.Now;
			try
			{
				if (clean_)
				{
					CleanState(profile_);
				}

				SetDateFor(profile_, now);

				if (items_ != null)
				{
					foreach (string key in items_.Keys)
					{
						SaveItem(items_[key], key, profile_);
					}
				}
				if (subitems_ != null)
				{
					foreach (string key in subitems_.Keys)
					{
						SaveSubItem(subitems_[key], key, profile_);
					}
				}
			}
			finally
			{
				RenewProfileNamesList();
			}
		}

		public void LoadState(string name, out Dictionary<string, XElement> items_,
		                      out Dictionary<string, XElement> subitems_)
		{
			IEnumerable<string> folders = OrderToLoad(name);
			foreach (string folder in folders)
			{
				if (TryLoadState(name, out items_, out subitems_, folder))
				{
					return;
				}
			}
			throw new InvalidOperationException(
				string.Format("Unable to load profile '{0}'. It is not available.", name));
		}

		#endregion

		protected override IEnumerable<string> GetProfilesListFromDirectory(string folder)
		{
			var result = new List<string>();
            if (!System.IO.Directory.Exists(folder))
			{
				return result;
			}

			var baseDirInfo = new DirectoryInfo(folder);
			DirectoryInfo[] directoryInfos = baseDirInfo.GetDirectories(DIRNAME_DOS_PATTERN);

			foreach (DirectoryInfo directoryInfo in directoryInfos)
			{
				string profileName = GetProfileNameFromDirectoryName(directoryInfo.Name);
				if (profileName != null)
				{
					result.Add(profileName);
				}
			}
			return result;
		}

		protected override DateTime? GetProfileDateTime(string profile_, string folder_)
		{
			try
			{
			    if (folder_ == null)
			        return null;
				string dirpath = Path.Combine(folder_, BuildDirName(profile_));
				string filepath = Path.Combine(dirpath, MakeValidFileName(INFO_FILENAME));

				if (File.Exists(filepath))
				{
					XDocument inforDoc = XDocument.Load(filepath);
					if (inforDoc.Root != null)
					{
						XAttribute attr = inforDoc.Root.Attribute(SAVE_TIME_ATTR_NAME);
						if (attr != null)
						{
							DateTime result;
							if (DateTime.TryParseExact(attr.Value, "o", CultureInfo.InvariantCulture, DateTimeStyles.None, out result))
							{
								return result;
							}
						}
					}
				}
			}
			catch (Exception)
			{
				m_logger.ErrorWithFormat("Could not load last save time for profile '{0}' from {1}", profile_, folder_);
			}
			return null;
		}

		protected void SetProfileDateTime(DateTime time_, string profileFolder_)
		{
			try
			{
				string nowString = time_.ToString("o", CultureInfo.InvariantCulture);
				string filepath = Path.Combine(profileFolder_, MakeValidFileName(INFO_FILENAME));
				new XDocument(new XElement("State", new XAttribute(SAVE_TIME_ATTR_NAME, nowString))).Save(filepath);
			}
			catch (Exception)
			{
				m_logger.ErrorWithFormat("Could not set last save time to {0}", profileFolder_);
			}
		}

		protected override void DeleteState(string profile, string folder)
		{
			string path = Path.Combine(folder, BuildDirName(profile));
            if (System.IO.Directory.Exists(path))
			{
                System.IO.Directory.Delete(path, true);
			}
		}

		protected override void RenameState(string oldName, string newName, string folder)
		{
			string oldpath = Path.Combine(folder, BuildDirName(oldName));
			string newpath = Path.Combine(folder, BuildDirName(newName));
            if (System.IO.Directory.Exists(newpath))
			{
                System.IO.Directory.Delete(newpath, true);
			}

            System.IO.Directory.Move(oldpath, newpath);
		}

		//Not used. If we ever want to make IPartialPersistenceStorage public, we should create 
		// a parent interface for them IPersistenceStorageCommon that doesn't have the following 2
		// methods and derive from it

		private void SaveItem(XElement state_, string persistorId_, string profile_)
		{
			bool baseResult = TrySaveItemTo(state_, persistorId_, profile_, BaseFolder);
			bool altResult = TrySaveItemTo(state_, persistorId_, profile_, AlternativeFolder);

			if (!baseResult && !altResult)
			{
				throw new InvalidOperationException(string.Format("Failed to Save item {0} from profile {1} to any location",
				                                                  persistorId_, profile_));
			}
		}

		private void SaveSubItem(XElement state_, string subitemId_, string profile_)
		{
			bool baseResult = TrySaveSubitemTo(state_, subitemId_, profile_, BaseFolder);
			bool altResult = TrySaveSubitemTo(state_, subitemId_, profile_, AlternativeFolder);

			if (!baseResult && !altResult)
			{
				throw new InvalidOperationException(string.Format("Failed to Save subitem '{0}' for profile '{1}' to any location",
				                                                  subitemId_, profile_));
			}
		}

		private void SetDateFor(string profile_, DateTime dateTime_)
		{
			TrySetDateFor(profile_, dateTime_, BaseFolder);
			TrySetDateFor(profile_, dateTime_, AlternativeFolder);
		}

		private bool TrySetDateFor(string profile_, DateTime dateTime_, string folder_)
		{
			try
			{
				string profileDir = Path.Combine(folder_, BuildDirName(profile_));
                if (!System.IO.Directory.Exists(profileDir))
				{
                    System.IO.Directory.CreateDirectory(profileDir);
				}
				SetProfileDateTime(dateTime_, profileDir);
				return true;
			}
			catch (Exception exc)
			{
				m_logger.ErrorWithFormat("Set date error: {0}", exc.Message);
			}
			return false;
		}

		private static bool TrySaveSubitemTo(XElement state_, string subitemId_, string profile_, string folder_)
		{
			try
			{
				string profileDir = Path.Combine(folder_, BuildDirName(profile_));
                if (!System.IO.Directory.Exists(profileDir))
				{
                    System.IO.Directory.CreateDirectory(profileDir);
				}
				return TrySaveElementToFile(state_, Path.Combine(profileDir, MakeValidFileName(subitemId_ + SUBSTATE_EXTENSION)));
			}
			catch (Exception exc)
			{
				m_logger.ErrorWithFormat("Save error: {0}", exc.Message);
			}
			return false;
		}

		private static bool TrySaveItemTo(XElement state_, string persistorId_, string profile_, string folder_)
		{
			try
			{
				string profileDir = Path.Combine(folder_, BuildDirName(profile_));
                if (!System.IO.Directory.Exists(profileDir))
				{
                    System.IO.Directory.CreateDirectory(profileDir);
				}
				return TrySaveElementToFile(state_, Path.Combine(profileDir, MakeValidFileName(persistorId_ + STATE_EXTENSION)));
			}
			catch (Exception exc)
			{
				m_logger.ErrorWithFormat("Save error: {0}", exc.Message);
				return false;
			}
		}

		private static bool TrySaveElementToFile(XElement state_, string file_)
		{
			if (file_ != null)
			{
				try
				{
					state_.Save(file_);
					return true;
				}
				catch (Exception exc)
				{
					m_logger.ErrorWithFormat("Couldn't save to '{0}'. Error: {1}", file_, exc.Message);
				}
			}
			return false;
		}


		private void CleanState(string profile_)
		{
			DateTime now = DateTime.Now;

			bool baseResult = TryCleanFolder(profile_, BaseFolder, now);
			bool altResult = TryCleanFolder(profile_, AlternativeFolder, now);

			if (!baseResult && !altResult)
			{
				throw new InvalidOperationException(
					string.Format("Unable to clean profile {0}", profile_));
			}
		}

		private static bool TryCleanFolder(string profile_, string folder_, DateTime now_)
		{
			try
			{
				string profileDir = Path.Combine(folder_, BuildDirName(profile_));
                if (System.IO.Directory.Exists(profileDir))
				{
                    string[] files = System.IO.Directory.GetFiles(profileDir);
					foreach (string file in files)
					{
						File.Delete(file);
					}
				}
				return true;
			}
			catch (Exception exception)
			{
				m_logger.ErrorWithFormat(
					"Unable to clean state {0} from the file storage: {1}. Error {2}", profile_, exception.Message, exception.Message);
			}
			return false;
		}

		private XDocument LoadPartial(string profile_)
		{
			Dictionary<string, XElement> items;
			Dictionary<string, XElement> subitems;

			//called from LoadProfile from a thread pool thread
			LoadState(profile_, out items, out subitems);

			var unpackeditems = new Dictionary<string, XElement>();
			if (items != null)
			{
				foreach (string itemKey in items.Keys)
				{
					XElement item;
					try
					{
						item = PersistenceProfileService.UnpackItem(items[itemKey]).Value;
					}
					catch (Exception exception)
					{
						m_logger.ErrorWithFormat("Failed to load state item {0} for profile {1}. Error: {2}", itemKey, profile_,
						                         exception.Message);
						//m_statusBar.MainItem.UpdateStatus(StatusLevel.Critical,
						//                                  string.Format(
						//                                    "Failed to load state item {0} for profile {1}. Error: {2}", itemKey,
						//                                    profile_, exception.Message));
						continue;
					}

					if (item is XElementWithSubitems)
					{
						var xews = ((XElementWithSubitems) item);
						foreach (string key in new List<string>(xews.Subitems.Keys))
						{
							if (subitems != null && subitems.ContainsKey(key))
							{
								PersistenceProfileService.ReplaceSubitem(xews, key, subitems[key]);
							}
							else
							{
								//TODO we've lost one subitem state. what to do?
							}
							subitems.Remove(key);
						}
						//item = new XElement(item);
					}
					unpackeditems.Add(itemKey, item);
					//TODO load the rest of subitems
				}
			}
			return PersistenceProfileService.DictToXDocument(unpackeditems);
		}


		private static bool TryLoadState(string profile_, out Dictionary<string, XElement> items_,
		                                 out Dictionary<string, XElement> subitems_, string folder_)
		{
			items_ = new Dictionary<string, XElement>();
			subitems_ = new Dictionary<string, XElement>();
			try
			{
				string path = Path.Combine(folder_, BuildDirName(profile_));
                if (System.IO.Directory.Exists(path))
				{
                    foreach (string filename in System.IO.Directory.GetFiles(path, "*" + STATE_EXTENSION))
					{
						XDocument currentFile = XDocument.Load(filename);
						string id = Path.GetFileName(filename);
						id = id.Substring(0, id.Length - STATE_EXTENSION.Length);
						items_[id] = currentFile.Root;
						//rootElement.Add(new XElement("Item", new XAttribute("Id", id), currentFile.Root));
					}

                    foreach (string filename in System.IO.Directory.GetFiles(path, "*" + SUBSTATE_EXTENSION))
					{
						XDocument currentFile = XDocument.Load(filename);
						string id = Path.GetFileName(filename);
						id = id.Substring(0, id.Length - SUBSTATE_EXTENSION.Length);
						subitems_[id] = currentFile.Root;
					}
					return true;
				}
			}
			catch (Exception exception)
			{
				// state exists and couldn't be loaded for some reason                  
				m_logger.ErrorWithFormat(
					"Unable to load state {0} from the directory {1}. Error: {1}", profile_, folder_, exception.Message);
			}
			return false;
		}

		private static string BuildDirName(string profileName_)
		{
			return string.Format(DIRNAME_PATTERN, MakeValidFileName(profileName_));
		}

		private static string GetProfileNameFromDirectoryName(string dirname_)
		{
			Match m = m_dirnameRegexp.Match(dirname_);
			if (m.Success)
			{
				return m.Groups[1].ToString();
			}
			return null;
		}

		#region Unused

		[EditorBrowsable(EditorBrowsableState.Never)]
		public void SaveState(XDocument state, string name)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public XDocument LoadState(string name)
		{
			return LoadPartial(name);
		}


		[EditorBrowsable(EditorBrowsableState.Never)]
		public void InitializeStorage()
		{
			// TODO:
			//  do nothing
		}


		[EditorBrowsable(EditorBrowsableState.Never)]
		public void Apply()
		{
			// TODO:
			//  do nothing
		}

		#endregion
	}
}