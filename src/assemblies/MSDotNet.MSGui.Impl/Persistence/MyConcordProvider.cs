﻿////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Persistence/MyConcordProvider.cs#6 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using MorganStanley.IED.Concord.Configuration;

namespace MorganStanley.MSDotNet.MSGui.Impl.Persistence
{
  //This class is for testing only.
  //For some reason NUnit doen't allow to implememnt interfaces from libraries other than tested.
  //I don't understand why, but NUnit wants to see this Concord assembly loaded before SetUp method for
  //any namespace is called or assembly from the current dir is loaded, if this class lives in the etsting assembly.
  // Update: TODO we are not using NUnit anymore. Try to move it back to the testing project.
  class MyConcordProvider : IConfigurationStorageReader, IConfigurationStorageWriter
  {
    internal static Dictionary<string, XmlNode> m_configs = new Dictionary<string, XmlNode>();
    public void Initialize(string settings_)
    {
      
    }

    public void Initialize(string settings_, XmlNode config_)
    {
      
    }

    public ConfigurationSection ReadConfiguration(string name_, string version_)
    {
      if (m_configs.Keys.Contains(name_))
      {
        return new ConfigurationSection(name_, version_, new XmlNode[] {}, m_configs[name_]);
      }
      XmlDocument document = new XmlDocument();
      return new ConfigurationSection(name_, version_, new XmlNode[] {}, document.CreateElement(name_));
    }

    public ConfigurationSection[] ReadConfigurations(ConfigurationIdentityCollection configEntries_)
    {
      throw new NotImplementedException();
    }

    public void WriteConfiguration(string name_, string version_, XmlNode node_)
    {
      m_configs[name_] = node_;
    }
  }
}
