﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Persistence/BaseFilesystemPersistenceStorage.cs#12 $
// $Change: 886109 $
// $DateTime: 2014/06/25 16:49:32 $
// $Author: hrechkin $

using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.My;

namespace MorganStanley.MSDotNet.MSGui.Impl.Persistence
{
    /// <summary>
    /// Base class for persistece storages that are saving to some filesystem location
    /// </summary>
    public abstract class BaseFilesystemPersistenceStorage
    {
        private static IMSLogger m_logger = MSLoggerFactory.CreateLogger<BaseFilesystemPersistenceStorage>();
        private string m_baseFolderPath;
        private string m_alternativeFolderPath;
        private string m_presetDirectory;
        private readonly ProfilesCollection m_profiles;
        protected readonly ReadOnlyProfilesCollection m_roProfiles;

        public const string DEFAULT_DEFAULT_LAYOUT_NAME = "Default";
        public const string CONFIG_FILENAME = "msgui.config";

        protected BaseFilesystemPersistenceStorage(string applicationName)
            : this(applicationName, null)
        {
        }

        public string BaseFolder
        {
            get { return m_baseFolderPath; }
            set
            {
                m_baseFolderPath = value;
                RenewProfileNamesList();
            }
        }

        public string AlternativeFolder
        {
            get { return m_alternativeFolderPath; }
            set
            {
                m_alternativeFolderPath = value;
                RenewProfileNamesList();
            }
        }

        public string PresetFolder
        {
            get { return m_presetDirectory; }
            set
            {
                m_presetDirectory = value;
                RenewProfileNamesList();
            }
        }

        protected BaseFilesystemPersistenceStorage(string applicationName, string presetDirectory)
        {
            m_presetDirectory = presetDirectory;
            m_profiles = new ProfilesCollection();
            m_roProfiles = new ReadOnlyProfilesCollection(m_profiles);
            m_baseFolderPath = BuildBaseFolderPath(applicationName);
            m_alternativeFolderPath = BuildAlternativeFolderPath(applicationName);

            RenewProfileNamesList();
        }

        private static string BuildBaseFolderPath(string appName_)
        {
            if (string.IsNullOrEmpty(appName_))
            {
                throw new InvalidOperationException("Application name must be not empty");
            }

            try
            {
                string result = Path.Combine(@"U:\Application Data\Morgan Stanley", MakeValidFileName(appName_));
                if (!System.IO.Directory.Exists(result))
                {
                    System.IO.Directory.CreateDirectory(result);
                }
                return result;
            }
            catch (Exception exc)
            {
                m_logger.ErrorWithFormat("Could not build main folder path on U drive. Error: {0}", exc.Message);
            }
            return null;
        }

        private static string BuildAlternativeFolderPath(string appName_)
        {
            if (string.IsNullOrEmpty(appName_))
            {
                throw new InvalidOperationException("Application name must be not empty");
            }
            try
            {
                string appDataDir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
                string result = Path.Combine(appDataDir, "Morgan Stanley");
                result = Path.Combine(result, MakeValidFileName(appName_));
                if (!System.IO.Directory.Exists(result))
                {
                    System.IO.Directory.CreateDirectory(result);
                }
                return result;
            }
            catch (Exception exc)
            {
                m_logger.ErrorWithFormat("Could not build alternative folder path on C drive. Error: {0}", exc.Message);
            }
            return null;
        }

        protected static string MakeValidFileName(string name_)
        {
            string invalidChars = Regex.Escape(new string(Path.GetInvalidFileNameChars()));
            string invalidReStr = string.Format(@"([{0}]|\.\.)", invalidChars);
            return Regex.Replace(name_, invalidReStr, "_");
        }

        public ReadOnlyProfilesCollection AvailableProfiles
        {
            get { return m_roProfiles; }
        }

        /// <summary>
        /// Initialise profiles names
        /// </summary>    
        protected void RenewProfileNamesList()
        {
            m_profiles.Clear();

            if (m_presetDirectory != null)
            {
                foreach (string profile in GetProfilesListFromDirectory(m_presetDirectory))
                {
                    m_profiles.Add(profile);
                }
            }

            List<string> baseList = new List<string>(GetProfilesListFromDirectory(m_baseFolderPath));
            List<string> altList = new List<string>(GetProfilesListFromDirectory(m_alternativeFolderPath));

            foreach (var profile in baseList)
            {
                if (!m_profiles.Contains(profile))
                {
                    m_profiles.Add(profile);
                }
            }

            foreach (var profile in altList)
            {
                if (!m_profiles.Contains(profile))
                {
                    m_profiles.Add(profile);
                }
            }
        }

        public void DeleteState(string profile)
        {
            bool altResult = TryDeleteState(profile, m_alternativeFolderPath);
            bool baseResult = TryDeleteState(profile, m_baseFolderPath);
            RenewProfileNamesList();
            //todo what if one failed and another not?
            if (!altResult && !baseResult)
            {
                throw new InvalidOperationException(string.Format("Unable to delete state {0}", profile));
            }
        }

        private bool TryDeleteState(string profile_, string workDirectory_)
        {
            try
            {
                m_logger.DebugWithFormat("Deleting profile '{0}' from location '{1}'", profile_, workDirectory_);
                DeleteState(profile_, workDirectory_);
            }
            catch (Exception exception)
            {
                m_logger.ErrorWithFormat("Could not delete profile '{0}' from {1}. Error message: {2}", profile_,
                                         workDirectory_, exception.Message);
                return false;
            }
            return true;
        }

        private bool TryRenameState(string oldName_, string newName_, string workDirectory_)
        {
            try
            {
                m_logger.DebugWithFormat("Renaming profile '{0}' (new name '{1}') from location '{2}'", oldName_,
                                         newName_, workDirectory_);
                RenameState(oldName_, newName_, workDirectory_);
            }
            catch (Exception exception)
            {
                m_logger.ErrorWithFormat("Could not rename profile '{0}' to '{1}' from {2}. Error message: {3}",
                                         oldName_, newName_, workDirectory_, exception.Message);
                return false;
            }
            return true;
        }

        public void RenameState(string oldName, string newName)
        {
            //TODO reset date????
            bool altResult = TryRenameState(oldName, newName, m_alternativeFolderPath);
            bool baseResult = TryRenameState(oldName, newName, m_baseFolderPath);
            RenewProfileNamesList();
            if (!altResult && !baseResult)
            {
                throw new InvalidOperationException(string.Format("Unable to rename state {0} to {1}", oldName,
                                                                  newName));
            }
        }

        private string m_defaultName;
        private bool m_defaultLayoutLoaded = false;

        public string DefaultName
        {
            get
            {
                if (!m_defaultLayoutLoaded)
                {
                    LoadDefaultName();
                    m_defaultLayoutLoaded = true;
                }
                m_defaultName = m_defaultName ?? DEFAULT_DEFAULT_LAYOUT_NAME;
                return m_profiles.Contains(m_defaultName) ? m_defaultName : null;
            }
            set
            {
                m_defaultName = value;
                SaveDefaultName();
            }
        }

        private void LoadDefaultName()
        {
            //TODO we do not try to load&save of the default layout in a special thread, maybe we should
            List<string> paths = new List<string>();
            if (m_baseFolderPath != null)
            {
                paths.Add(Path.Combine(m_baseFolderPath, CONFIG_FILENAME));
            }
            if (m_alternativeFolderPath != null)
            {
                paths.Add(Path.Combine(m_alternativeFolderPath, CONFIG_FILENAME));
            }

            foreach (var path in paths)
            {
                if (File.Exists(path))
                {
                    try
                    {
                        XElement element = XElement.Load(path);
                        if (element != null)
                        {
                            XElement defaultElement = element.Element("Default");
                            if (defaultElement != null)
                            {
                                m_defaultName = defaultElement.Value;
                                return;
                            }
                        }
                    }
                    catch (Exception exc)
                    {
                        m_logger.DebugWithFormat("file {0} is not available. error message: {1}", path, exc.Message);
                    }
                }
            }

            m_defaultName = DEFAULT_DEFAULT_LAYOUT_NAME;
        }

        private void SaveDefaultName()
        {
            if (!string.IsNullOrEmpty(DefaultName))
            {
                List<string> paths = new List<string>();
                if (m_baseFolderPath != null)
                {
                    paths.Add(Path.Combine(m_baseFolderPath, CONFIG_FILENAME));
                }
                if (m_alternativeFolderPath != null)
                {
                    paths.Add(Path.Combine(m_alternativeFolderPath, CONFIG_FILENAME));
                }

                foreach (var path in paths)
                {
                    try
                    {
                        XElement elementToSave = new XElement("Config", new XElement("Default", DefaultName));
                        elementToSave.Save(path);
                    }
                    catch (Exception exc)
                    {
                        m_logger.DebugWithFormat("Unable to save to the file {0}. error message: {1}", path, exc.Message);
                    }
                }
            }
        }

        protected IEnumerable<string> OrderToLoad(string profile_)
        {
            DateTime? baseTime = GetProfileDateTime(profile_, m_baseFolderPath);
            DateTime? altTime = GetProfileDateTime(profile_, m_alternativeFolderPath);
            List<string> result = new List<string>();
            if (altTime == null ||
                (baseTime != null && baseTime.Value >= altTime.Value))
            {
                if (m_baseFolderPath != null)
                {
                    result.Add(m_baseFolderPath);
                }
                if (m_alternativeFolderPath != null)
                {
                    result.Add(m_alternativeFolderPath);
                }
            }
            else
            {
                if (m_alternativeFolderPath != null)
                {
                    result.Add(m_alternativeFolderPath);
                }
                if (m_baseFolderPath != null)
                {
                    result.Add(m_baseFolderPath);
                }
            }
            if (m_presetDirectory != null)
            {
                result.Add(m_presetDirectory);
            }
            return result;
        }

        protected abstract IEnumerable<string> GetProfilesListFromDirectory(string folder);

        protected abstract DateTime? GetProfileDateTime(string profile_, string folder_);

        protected abstract void DeleteState(string profile, string folder);

        protected abstract void RenameState(string oldName, string newName, string folder);
    }
}
