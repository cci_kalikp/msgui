﻿using System.Threading;
using System.Xml.Linq;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.Persistence.DelayedLoading;

namespace MorganStanley.MSDotNet.MSGui.Impl.Persistence.DelayedLoading
{
    internal sealed class DelayedProfileLoader : IDelayedProfileLoader
    {
        private readonly PersistenceProfileService _persistenceProfileService;
        private readonly PersistenceService _persistenceService;
        private readonly ManualResetEvent _persistanceInitializedEvent;

        public DelayedProfileLoader(IUnityContainer container)
        {
            _persistenceProfileService = container.Resolve<PersistenceProfileService>();
            _persistenceService = container.Resolve<IPersistenceService>() as PersistenceService;
            _persistanceInitializedEvent = new ManualResetEvent(false);
        }

        public void ReloadCurrentProfile()
        {
            ThreadPool.QueueUserWorkItem(ReloadCurrentProfileProc);
        }

        public ManualResetEvent PersistanceInitialized
        {
            get { return _persistanceInitializedEvent; }
        }

        private void ReloadCurrentProfileProc(object o)
        {
            if (PersistanceInitialized.WaitOne())
            {
                _persistenceProfileService.LoadProfile(_persistenceProfileService.CurrentProfile, false);
                DelayedProfileLoadingExtensionPoints.DelayLoadProfile = false;
            }
        }

        public void LoadConnections(XDocument element)
        {
            if (element != null)
            {
                _persistenceService.RestoreItem(element.Root,
                                                 "MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication.IpcLocalConnectionManager");
            }
        }
    }
}
