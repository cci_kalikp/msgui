﻿using System;
using System.Windows.Threading;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Persistence.DelayedLoading;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity;

namespace MorganStanley.MSDotNet.MSGui.Impl.Persistence.DelayedLoading
{

    public abstract class DelayedProfileLoadCoordinatingModule : IMSDesktopModule, IDelayedLoadModule
    {
        private readonly DispatcherTimer _timeoutTimer;
        private readonly IChromeManager _chromeManager;
        private readonly IModuleGroup _moduleGroup;

        protected DelayedProfileLoadCoordinatingModule(IUnityContainer container)
        {
            _chromeManager = container.Resolve<IChromeManager>();
            _moduleGroup = container.Resolve<IModuleGroup>();

            _timeoutTimer = new DispatcherTimer(TimeSpan.FromSeconds(DelayedLoadingExtensions.Timeout),
                                               DispatcherPriority.ApplicationIdle,
                                               InitializationTimedOut, System.Windows.Application.Current.Dispatcher);

            _moduleGroup.RegisterDelayedLoadModule(this);
        }

        protected abstract void CloseInitializer();

        protected IChromeManager ChromeManager 
        {
            get { return _chromeManager; }
        }

        protected virtual int TimeoutInSeconds { get { return -1; } }

        public virtual void Initialize()
        {
            if (TimeoutInSeconds > 0)
            {
                _timeoutTimer.Interval = TimeSpan.FromSeconds(TimeoutInSeconds);
            }
            _timeoutTimer.Start();
        }

        protected void AddContentType(Type contentType)
        {
            _moduleGroup.ContentTypes.Add(contentType);
        }

        protected void ViewLoadedCompletely()
        {
            if (_moduleGroup.ViewLoadedCompletely(DelayedLoadingExtensions.Connection))
            {
                DelayedLoadingExtensions.Connection = null;
            }
        }

        public virtual void OnModuleInitialized()
        {
            if (_timeoutTimer != null)
            {
                _timeoutTimer.Stop();
            }

            CloseInitializer();
            _moduleGroup.DelayedLoadModuleInitialized(this);
        }

        private void InitializationTimedOut(object sender, EventArgs eventArgs)
        {
            if (_timeoutTimer != null)
            {
                _timeoutTimer.Stop();
            }

            CloseInitializer();
            _moduleGroup.DelayedLoadModuleInitializationFailed(this);
        }
    }
}
