﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core.Persistence.DelayedLoading;

namespace MorganStanley.MSDotNet.MSGui.Impl.Persistence.DelayedLoading
{
    public class DelayedLoadingExtensions
    {
        internal static XDocument Connection { get; set; }
#if DEBUG
        internal static int Timeout = 150;
#else
        internal static int Timeout = 15;
#endif
        public static void EnableDelayedLoading()
        {
            DelayedProfileLoadingExtensionPoints.AllModulesLoaded -= WebSupportExtensionPointsOnAllModulesLoaded;
            DelayedProfileLoadingExtensionPoints.BeforeLoadView2ViewConnection -= OnBeforeLoadView2ViewConnection;

            DelayedProfileLoadingExtensionPoints.AllModulesLoaded += WebSupportExtensionPointsOnAllModulesLoaded;
            DelayedProfileLoadingExtensionPoints.BeforeLoadView2ViewConnection += OnBeforeLoadView2ViewConnection;
        }
        
        private static void WebSupportExtensionPointsOnAllModulesLoaded(object sender, AllModulesLoadedEventArgs allModulesLoadedEventArgs)
        {
            DelayedProfileLoadingExtensionPoints.DelayLoadProfile =
                allModulesLoadedEventArgs.Modules.Infos.Any(m => typeof(DelayedProfileLoadCoordinatingModule).IsAssignableFrom(Type.GetType(m.FullTypeName)));
        }

        private static void OnBeforeLoadView2ViewConnection(object sender, BeforeLoadView2ViewConnectionEventArgs e)
        {
            if (Connection == null)
            {
                Connection = e.Connection;
                e.Cancel = true;
            }
        }
    }
}
