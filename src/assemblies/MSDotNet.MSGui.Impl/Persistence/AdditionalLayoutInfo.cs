﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core.Persistence;

namespace MorganStanley.MSDotNet.MSGui.Impl.Persistence
{
	internal class AdditionalLayoutInfo : IPersistable
	{
		#region Static members
		public static List<string> TemporarilyHiddenViews = new List<string>();
		public static readonly HashSet<string> MaximizedViews = new HashSet<string>(); 
		#endregion


		private IUnityContainer Container;

		public AdditionalLayoutInfo(IUnityContainer Container)
		{
			this.Container = Container;
		}

		#region IPersistable Members

		public void LoadState(System.Xml.Linq.XDocument state_)
		{
			try
			{
				var viewManager = (IViewManager)this.Container.Resolve<IViewManager>();
				var root = state_.Elements("AdditionalLayoutInfo").FirstOrDefault();

				var temporarilyHiddenViewsElement = root.Elements("TemporarilyHiddenViews").FirstOrDefault();
				var maximizedViewsElement = root.Elements("MaximizedViews").FirstOrDefault();

				foreach (var element in temporarilyHiddenViewsElement.Elements("IView"))
				{
					AdditionalLayoutInfo.TemporarilyHiddenViews.Add(element.FirstAttribute.Value);
				}

				foreach (var element in maximizedViewsElement.Elements("IView"))
				{
					AdditionalLayoutInfo.MaximizedViews.Add(element.FirstAttribute.Value);
				}
			}
			catch
			{

			}
		}

		public System.Xml.Linq.XDocument SaveState()
		{
			var xDocument = new XDocument();
			var root = new XElement("AdditionalLayoutInfo");
			var temporarilyHiddenViewsElement = new XElement("TemporarilyHiddenViews");
			var maximizedViewsElement = new XElement("MaximizedViews");

			foreach (var viewID in AdditionalLayoutInfo.TemporarilyHiddenViews)
			{
				temporarilyHiddenViewsElement.Add(new XElement("IView", new XAttribute("Id", viewID)));
			}

			foreach (var viewID in AdditionalLayoutInfo.MaximizedViews)
			{
				maximizedViewsElement.Add(new XElement("IView", new XAttribute("Id", viewID)));
			}

			root.Add(temporarilyHiddenViewsElement);
			root.Add(maximizedViewsElement);
			xDocument.Add(root);
			return xDocument;
		}

		public string PersistorId
		{
			get { return "AdditionalLayoutInfo"; }
		}

		#endregion
	}
}
