﻿using System;
using System.Collections.Generic;
using System.ComponentModel; 
using System.Windows;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Persistence; 

namespace MorganStanley.MSDotNet.MSGui.Impl.Persistence
{
    public enum StatePersistenceMode
    {
        Global = 0,
        CurrentProfile = 1,
        OtherProfile = 2
    }

    public class PersistenceStorage
    {
        public StatePersistenceMode Mode {get;set;}
        public string ProfileName { get; set; }

        public static readonly PersistenceStorage Global = new PersistenceStorage()
            {
                Mode = StatePersistenceMode.Global
            };

        public static readonly PersistenceStorage CurrentProfile = new PersistenceStorage()
            {
                Mode = StatePersistenceMode.CurrentProfile
            };
        public static PersistenceStorage Profile(string profileName_)
        {
            return new PersistenceStorage() {ProfileName = profileName_, Mode = StatePersistenceMode.OtherProfile};
        }
    }

    public static class ComponentPersistenceExtensions
    {
        internal static PersistenceService PersistenceService;

        #region Profile Associated View Container Persistence Related
        public static void LoadSubState(this IWindowViewContainer container_, DependencyObject component_, bool loadManualItems_ = true, string subStateId_ = null)
        {
            LoadSubState<LogicalTreeElementPersistorEnumerator>(container_, component_, loadManualItems_, subStateId_);
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public static void LoadSubState<TEnumerator>(this IWindowViewContainer container_, DependencyObject component_, bool loadManualItems_ = true, string subStateId_ = null) where TEnumerator : IComponentPersistorEnumerator, new()
        {
            var persistor = GetPersistor<TEnumerator>(container_) as ViewContainerPersistor<TEnumerator>;
            persistor.LoadSubState(component_, persistor.SavedState ?? persistor.LoadedState, loadManualItems_, subStateId_);
        }

        public static void SaveSubState(this IWindowViewContainer container_, DependencyObject component_, string subStateId_ = null)
        {
            SaveSubState<LogicalTreeElementPersistorEnumerator>(container_, component_, subStateId_);
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public static void SaveSubState<TEnumerator>(this IWindowViewContainer container_, DependencyObject component_, string subStateId_ = null) where TEnumerator : IComponentPersistorEnumerator, new()
        {
            var persistor = GetPersistor<TEnumerator>(container_) as ViewContainerPersistor<TEnumerator>;
            persistor.SaveSubState(component_, persistor.SavedState ?? persistor.LoadedState ?? new XDocument(new XElement(persistor.RootElementName)), subStateId_);
        }


        public static void RemoveSubState(this IWindowViewContainer container_, DependencyObject component_, string subStateId_ = null)
        {
            RemoveSubState<LogicalTreeElementPersistorEnumerator>(container_, component_, subStateId_);
        }


        [EditorBrowsable(EditorBrowsableState.Never)]
        public static void RemoveSubState<TEnumerator>(this IWindowViewContainer container_, DependencyObject component_, string subStateId_ = null) where TEnumerator : IComponentPersistorEnumerator, new()
        {
            var persistor = GetPersistor<TEnumerator>(container_) as ViewContainerPersistor<TEnumerator>;
            persistor.RemoveSubState(component_, null, subStateId_);
        }
         
        public static IPersistable GetPersistor(this IWindowViewContainer container_)
        {
            if (container_.Parameters == null || container_.Parameters.PersistorType == null)
            {
                return GetPersistor<LogicalTreeElementPersistorEnumerator>(container_);
            }

            object persistor = container_.Parameters.PersistorType.GetConstructor(new Type[] { typeof(IWindowViewContainer), typeof(string), typeof(XDocument) })
                                             .Invoke(new object[] { container_, null, null });
            if (persistor != null && container_.Parameters.DynamicComponentPersistors)
            {
                ReflHelper.TryPropertySet(persistor, "CachePersistors", false);
            }
            return persistor as IPersistable;
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public static IPersistable GetPersistor<TEnumerator>(this IWindowViewContainer container_) where TEnumerator : IComponentPersistorEnumerator, new()
        {
            var persistor = new ViewContainerPersistor<TEnumerator>(container_, null, null);
            if (container_.Parameters != null && container_.Parameters.DynamicComponentPersistors)
            {
                persistor.CachePersistors = false;
            }
            return persistor;
        }
        #endregion

        #region Standalone View Container Persistence Related

        public static void LoadStandaloneState(this IWindowViewContainer container_, PersistenceStorage storage_, string stateId_, bool loadManualItems_ = false)
        {
            LoadStandaloneState<LogicalTreeElementPersistorEnumerator>(container_, storage_, stateId_, loadManualItems_);
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public static void LoadStandaloneState<TEnumerator>(this IWindowViewContainer container_, PersistenceStorage storage_, string stateId_, bool loadManualItems_ = false) where TEnumerator : IComponentPersistorEnumerator, new()
        {
            var persistor = GetStandalonePersistor<TEnumerator>(container_, stateId_) as ViewContainerPersistor<TEnumerator>;
            ExecutePersistor(persistor.PersistorId, stateId_, null, s => persistor.LoadState(s, loadManualItems_), persistor.SaveState, storage_, true);   
        }

        public static void SaveStandaloneState(this IWindowViewContainer container_, PersistenceStorage storage_, string stateId_)
        {
            SaveStandaloneState<LogicalTreeElementPersistorEnumerator>(container_, storage_, stateId_);
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public static void SaveStandaloneState<TEnumerator>(this IWindowViewContainer container_, PersistenceStorage storage_, string stateId_) where TEnumerator : IComponentPersistorEnumerator, new()
        {
            var persistor = GetStandalonePersistor<TEnumerator>(container_, stateId_);
            ExecutePersistor(persistor.PersistorId, stateId_, null, persistor.LoadState, persistor.SaveState, storage_, false);   
            
        }

        public static void LoadStandaloneSubState(this IWindowViewContainer container_, PersistenceStorage storage_, string stateId_, DependencyObject component_, bool loadManualItems_ = true, string subStateId_ = null)
        {
            LoadStandaloneSubState<LogicalTreeElementPersistorEnumerator>(container_, storage_, stateId_, component_, loadManualItems_, subStateId_);
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public static void LoadStandaloneSubState<TEnumerator>(this IWindowViewContainer container_, PersistenceStorage storage_, string stateId_, DependencyObject component_, bool loadManualItems_ = true, string subStateId_ = null) where TEnumerator : IComponentPersistorEnumerator, new()
        {
            var persistor = GetStandalonePersistor<TEnumerator>(container_, stateId_) as ViewContainerPersistor<TEnumerator>;
            ExecutePersistor(persistor.PersistorId,stateId_, subStateId_, s_ => persistor.LoadSubState(component_, s_, loadManualItems_, subStateId_), persistor.SaveState, storage_, true);   
        }

        public static void SaveStandaloneSubState(this IWindowViewContainer container_, PersistenceStorage storage_, string stateId_, DependencyObject component_, string subStateId_ = null) 
        {
            SaveStandaloneSubState<LogicalTreeElementPersistorEnumerator>(container_, storage_, stateId_, component_, subStateId_);
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public static void SaveStandaloneSubState<TEnumerator>(this IWindowViewContainer container_, PersistenceStorage storage_, string stateId_, DependencyObject component_, string subStateId_ = null) where TEnumerator : IComponentPersistorEnumerator, new()
        {
            var persistor = GetStandalonePersistor<TEnumerator>(container_, stateId_) as ViewContainerPersistor<TEnumerator>;
            ExecutePersistor(persistor.PersistorId, stateId_, subStateId_, persistor.LoadState, 
                (() =>
                    {
                        XDocument state = null;
                        if (storage_ == PersistenceStorage.Global)
                        {
                            state = PersistenceService.GetGlobalItem(persistor.PersistorId);
                        }
                        else if (storage_ == PersistenceStorage.CurrentProfile)
                        {
                            state = PersistenceService.GetCurrentProfileItem(persistor.PersistorId);
                        }
                        else
                        {
                            state = PersistenceService.GetProfileItem(storage_.ProfileName, persistor.PersistorId);
                        }
                        if (state == null)
                        {
                            state = new XDocument(new XElement(persistor.RootElementName));
                        }
                        return persistor.SaveSubState(component_, state, subStateId_);
                    }), 
                storage_, false);    
        }

        public static void RemoveStandaloneSubState(this IWindowViewContainer container_, PersistenceStorage storage_, string stateId_, DependencyObject component_, string subStateId_ = null)
        {
            RemoveStandaloneSubState<LogicalTreeElementPersistorEnumerator>(container_, storage_, stateId_, component_, subStateId_);
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public static void RemoveStandaloneSubState<TEnumerator>(this IWindowViewContainer container_, PersistenceStorage storage_, string stateId_, DependencyObject component_, string subStateId_ = null) where TEnumerator : IComponentPersistorEnumerator, new()
        {
            var persistor = GetStandalonePersistor<TEnumerator>(container_, stateId_) as ViewContainerPersistor<TEnumerator>;
            ExecutePersistor(persistor.PersistorId, stateId_, subStateId_, persistor.LoadState,
               (() =>
               {
                   XDocument state = null;
                   if (storage_ == PersistenceStorage.Global)
                   {
                       state = PersistenceService.GetGlobalItem(persistor.PersistorId);
                   }
                   else if (storage_ == PersistenceStorage.CurrentProfile)
                   {
                       state = PersistenceService.GetCurrentProfileItem(persistor.PersistorId);
                   }
                   else
                   {
                       state = PersistenceService.GetProfileItem(storage_.ProfileName, persistor.PersistorId);
                   }
                   if (state == null)
                   {
                       state = new XDocument(new XElement(persistor.RootElementName));
                   }
                   return persistor.RemoveSubState(component_, state, subStateId_);
               }),
               storage_, false);     
        }

        public static IPersistable GetStandalonePersistor(this IWindowViewContainer container_, string stateId_)
        {
            if (container_.Parameters == null || container_.Parameters.PersistorType == null)
            {
                return GetStandalonePersistor<LogicalTreeElementPersistorEnumerator>(container_, null);
            }

            object persistor = container_.Parameters.PersistorType.GetConstructor(new Type[] { typeof(IWindowViewContainer), typeof(string), typeof(XDocument) })
                                             .Invoke(new object[] { container_, stateId_, null });
            if (persistor != null && container_.Parameters.DynamicComponentPersistors)
            {
                ReflHelper.TryPropertySet(persistor, "CachePersistors", false);
            }
            return persistor as IPersistable;
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public static IPersistable GetStandalonePersistor<TEnumerator>(this IWindowViewContainer container_, string stateId_) where TEnumerator : IComponentPersistorEnumerator, new()
        {
            var persistor = new ViewContainerPersistor<TEnumerator>(container_, stateId_, null);
            if (container_.Parameters != null && container_.Parameters.DynamicComponentPersistors)
            {
                persistor.CachePersistors = false;
            }
            return persistor;
        }
        #endregion

        #region Standalone Component Persistence Related
        public static void LoadStandaloneComponentState(this DependencyObject component_, PersistenceStorage storage_, string stateId_)
        {
            LoadStandaloneComponentState<LogicalTreeElementPersistorEnumerator>(component_, storage_, stateId_);
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public static void LoadStandaloneComponentState<TEnumerator>(this DependencyObject component_, PersistenceStorage storage_, string stateId_) where TEnumerator : IComponentPersistorEnumerator, new()
        {
            var persistor = GetStandaloneComponentPersistor<TEnumerator>(component_, stateId_);
            ExecutePersistor(persistor.PersistorId, stateId_, null, persistor.LoadState, persistor.SaveState, storage_, true);  
        }

        public static void SaveStandaloneComponentState(this DependencyObject component_, PersistenceStorage storage_, string stateId_)
        {
            SaveStandaloneComponentState<LogicalTreeElementPersistorEnumerator>(component_, storage_, stateId_);
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public static void SaveStandaloneComponentState<TEnumerator>(this DependencyObject component_, PersistenceStorage storage_, string stateId_) where TEnumerator : IComponentPersistorEnumerator, new()
        {
            var persistor = GetStandaloneComponentPersistor<TEnumerator>(component_, stateId_);
            ExecutePersistor(persistor.PersistorId, stateId_, null, persistor.LoadState, persistor.SaveState, storage_, false); 

        }

        public static IPersistable GetStandaloneComponentPersistor(this DependencyObject component_, string stateId_ = null)
        {
            return GetStandaloneComponentPersistor<LogicalTreeElementPersistorEnumerator>(component_, stateId_);
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public static IPersistable GetStandaloneComponentPersistor<TEnumerator>(this DependencyObject component_, string stateId_ = null) where TEnumerator : IComponentPersistorEnumerator, new()
        {
            return new SubViewPersistor<TEnumerator>(component_, stateId_);
        }

        #endregion

        public class PersistenceStateEventArgs:EventArgs
        {
            public PersistenceStateEventArgs(string persistorId_, string stateId_, string subStateId_, PersistenceStorage storage_)
            {
                PersistorId = persistorId_;
                StateId = stateId_;
                SubStateId = subStateId_;
                Storage = storage_;
            }
            public string PersistorId { get; private set; }
            public string StateId { get; private set; }
            public string SubStateId { get; private set; }
            public PersistenceStorage Storage { get; private set; }
        }

        public static event EventHandler<PersistenceStateEventArgs> LoadingState;
        public static event EventHandler<PersistenceStateEventArgs> LoadedState;
        public static event EventHandler<PersistenceStateEventArgs> SavingState;
        public static event EventHandler<PersistenceStateEventArgs> SavedState; 

        #region Helper Methods
         
        private static readonly List<string> globalPersistorIds = new List<string>();
        private static readonly List<string> profilePersistorIds = new List<string>(); 
        private static void ExecutePersistor(string persistorId_, string stateId_, string subStateId_,
                                    RestoreStateHandler restoreStateHandler_,
                                    SaveStateHandler saveStateHandler_, PersistenceStorage storage_, bool load_)
        {
            var args = new PersistenceStateEventArgs(persistorId_, stateId_, subStateId_, storage_);

            if (storage_ == PersistenceStorage.Global)
            {
                if (!globalPersistorIds.Contains(persistorId_))
                {
                    if (load_)
                    {
                        var loading = LoadingState;
                        if (loading != null)
                        {
                            loading(null, args);
                        }
                    }
                    else
                    {
                        var saving = SavingState;
                        if (saving != null)
                        {
                            saving(null, args);
                        }
                    }

                    globalPersistorIds.Add(persistorId_);
                    try
                    {
                        PersistenceService.AddGlobalPersistor(persistorId_, restoreStateHandler_, saveStateHandler_);
                        if (load_)
                        {
                            PersistenceService.LoadGlobalItems(new string[] { persistorId_ });
                        }
                        else
                        {
                            PersistenceService.PersistGlobalItems(new string[] { persistorId_ });
                        }
                    }
                    finally
                    {
                        PersistenceService.RemoveGlobalPersistor(persistorId_);
                        globalPersistorIds.Remove(persistorId_);
                        if (load_)
                        {
                            var loaded = LoadedState;
                            if (loaded != null)
                            {
                                loaded(null, args);
                            }
                        }
                        else
                        {
                            var saved = SavedState;
                            if (saved != null)
                            {
                                saved(null, args);
                            }
                        }

                    } 
                }

            }
            else
            {
                if (!profilePersistorIds.Contains(persistorId_))
                {
                    try
                    {
                        if (load_)
                        {
                            var loading = LoadingState;
                            if (loading != null)
                            {
                                loading(null, args);
                            }
                        }
                        else
                        {
                            var saving = SavingState;
                            if (saving != null)
                            {
                                saving(null, args);
                            }
                        }
                        profilePersistorIds.Add(persistorId_);
                        PersistenceService.AddPersistor(persistorId_, restoreStateHandler_, saveStateHandler_);
                        if (load_)
                        {
                            if (storage_ == PersistenceStorage.CurrentProfile)
                            {
                                PersistenceService.LoadCurrentProfileItems(new string[] { persistorId_ });
                            }
                            else
                            {
                                PersistenceService.LoadProfileItems(storage_.ProfileName, new string[] { persistorId_ });
                            }
                        }
                        else
                        {
                            if (storage_ == PersistenceStorage.CurrentProfile)
                            {
                                PersistenceService.PersistCurrentProfileItems(new string[]{persistorId_}); 
                            }
                            else
                            {
                                PersistenceService.PersistProfileItems(storage_.ProfileName, new string[] { persistorId_ });
                            }
                        }

                    }
                    finally
                    {
                        PersistenceService.RemovePersistor(persistorId_);
                        profilePersistorIds.Remove(persistorId_); 
                        if (load_)
                        {
                            var loaded = LoadedState;
                            if (loaded != null)
                            {
                                loaded(null, args);
                            }
                        }
                        else
                        {
                            var saved = SavedState;
                            if (saved != null)
                            {
                                saved(null, args);
                            }
                        }
                    } 
                } 
            }
        }
        #endregion
    }
}
