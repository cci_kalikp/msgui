﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;

namespace MorganStanley.MSDotNet.MSGui.Impl.Persistence
{
    public class VisualTreeElementPersistorEnumerator : ComponentPersistorEnumeratorBase
    {
        protected override IEnumerable GetChildren(DependencyObject source_)
        {
            int childrenCount = VisualTreeHelper.GetChildrenCount(source_);
            for (int i = 0; i < childrenCount; i++)
            {
                yield return VisualTreeHelper.GetChild(source_, i);
            }
        }

        protected override DependencyObject GetParent(DependencyObject source_)
        {
            return VisualTreeHelper.GetParent(source_);
        }
    }
}
