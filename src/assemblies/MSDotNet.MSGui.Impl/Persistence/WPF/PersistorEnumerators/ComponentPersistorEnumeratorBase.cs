﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using MorganStanley.MSDotNet.MSGui.Core.Persistence;

namespace MorganStanley.MSDotNet.MSGui.Impl.Persistence
{
    public abstract class ComponentPersistorEnumeratorBase:IComponentPersistorEnumerator
    {
        protected void GetPersistorsInternal(object target_, List<IComponentPersistor> existingPersistors_, 
                                             Stack<string> namespaces_, Configuration parentConfiguration_)
        {

            bool newNamepsaceAdded = false;
            DependencyObject dependencyObject = target_ as DependencyObject;
            if (dependencyObject != null)
            {
                FrameworkElement frameworkElementTarget = target_ as FrameworkElement;
                string name = frameworkElementTarget == null ? null : frameworkElementTarget.Name;
                Configuration configuration = PersistenceConfigurator.ResolveConfiguration(dependencyObject, parentConfiguration_);
                if (configuration != null)
                {
                    if (!string.IsNullOrEmpty(configuration.PersistenceName))
                    {
                        namespaces_ = new Stack<string>();
                        name = configuration.PersistenceName;
                    }

                }
                if (!string.IsNullOrEmpty(name))
                { 
                    namespaces_.Push(name);
                    newNamepsaceAdded = true;
                }

                if (configuration != null && configuration.Persistors.Count > 0)
                {
                    var namespacesReversed = new List<string>(namespaces_.ToArray().Reverse());
                    foreach (IComponentPersistor persistor in configuration.Persistors)
                    {   
                        existingPersistors_.Add(persistor.WrapComponent(dependencyObject, namespacesReversed, configuration));
                    }
                }
                
                if (configuration == null || !configuration.SuppressChildrenPersistence)
                {
                    foreach (object child in GetChildren(dependencyObject))
                    {
                        GetPersistorsInternal(child, existingPersistors_, namespaces_, configuration);
                    }
                } 
            }  
            if (newNamepsaceAdded)
            {
                namespaces_.Pop();
            }
    
        }


        protected void GetNameSpaces(object rootObject_, object componentObject_, Stack<string> namespaces_, bool includeComponentObject_)
        { 
            DependencyObject dependencyObject = componentObject_ as DependencyObject;
            if (dependencyObject != null)
            {
                if (includeComponentObject_)
                {
                    FrameworkElement frameworkElementTarget = componentObject_ as FrameworkElement;
                    string name = frameworkElementTarget == null ? null : frameworkElementTarget.Name;
                    Configuration configuration = PersistenceConfigurator.GetConfiguration(dependencyObject);
                    if (configuration != null)
                    {
                        if (!string.IsNullOrEmpty(configuration.PersistenceName))
                        {
                            namespaces_ = new Stack<string>();
                            name = configuration.PersistenceName;
                        }
                    }
                    if (!string.IsNullOrEmpty(name))
                    {
                        namespaces_.Push(name);
                    }
                }

                if (componentObject_ == rootObject_) return; 
                GetNameSpaces(rootObject_, GetParent(dependencyObject), namespaces_, true);
            }   
        }

          
        #region IComponentPersistorEnumerator Members

        public virtual IEnumerable<IComponentPersistor> GetComponentPersistors(object rootObject_, object componentObject_)
        {
            if (rootObject_ == null || componentObject_ == null) return null;
            if (rootObject_ == componentObject_)
            {
                return GetPersistors(rootObject_);
            }
            List<IComponentPersistor> persistors = new List<IComponentPersistor>();
            Stack<string> namespaces = new Stack<string>();
            GetNameSpaces(rootObject_, componentObject_, namespaces, false);
            GetPersistorsInternal(componentObject_, persistors, new Stack<string>(namespaces), null);
            return persistors;
        }
        public virtual IEnumerable<IComponentPersistor> GetPersistors(object rootObject_)
        {
            if (rootObject_ == null) return null;
            List<IComponentPersistor> persistors = new List<IComponentPersistor>();
            GetPersistorsInternal(rootObject_, persistors, new Stack<string>(), null);
            return persistors;
        }


        #endregion

        protected abstract IEnumerable GetChildren(DependencyObject source_);

        protected abstract DependencyObject GetParent(DependencyObject source_);

    }
}
