﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Windows; 

namespace MorganStanley.MSDotNet.MSGui.Impl.Persistence
{
    public delegate IEnumerable GetChildren(DependencyObject source_);
    public delegate DependencyObject GetParent(DependencyObject source_);
    public class LogicalTreeElementPersistorEnumerator : ComponentPersistorEnumeratorBase
    {
 
        private IList<GetChildren> childrenEnumerators;
        private IList<GetParent> parentEnumerators;

        protected virtual void AddChildrenEnumerators(IList<GetChildren> enumerators_)
        {
            childrenEnumerators.Add(LogicalTreeHelper.GetChildren);
        }

        protected virtual void AddParentEnumerators(IList<GetParent> enumerators_)
        {
            parentEnumerators.Add(LogicalTreeHelper.GetParent);
        }

        protected override IEnumerable GetChildren(DependencyObject source_)
        {
            if (childrenEnumerators == null)
            {
                childrenEnumerators = new List<GetChildren>();
                AddChildrenEnumerators(childrenEnumerators); 
            }
            return childrenEnumerators.Select(e_ => e_(source_)).FirstOrDefault(children_ => children_ != null);
        }

        protected override DependencyObject GetParent(DependencyObject source_)
        {
            if (parentEnumerators == null)
            {
                parentEnumerators = new List<GetParent>();
                AddParentEnumerators(parentEnumerators);
            }
            return parentEnumerators.Select(e_ => e_(source_)).FirstOrDefault(parent_ => parent_ != null);
            
        } 

    }
}