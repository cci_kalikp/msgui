﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Markup;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.Persistence;

namespace MorganStanley.MSDotNet.MSGui.Impl.Persistence
{
    public class DefaultConfigurationExtension:MarkupExtension
    {
        public override object ProvideValue(IServiceProvider serviceProvider_)
        {

            var target = serviceProvider_.GetService(typeof(IProvideValueTarget))
                    as IProvideValueTarget;
            var host = target.TargetObject as DependencyObject;
            if (host != null)
            {
                Configuration configuration = new Configuration(); 
                configuration.PersistenceName = PersistenceName; 
                configuration.AutoLoadStage = AutoLoadStage;
                configuration.AutoSave = AutoSave;
                configuration.ExceptionPolicy = ExceptionPolicy;
                IEnumerable<IComponentPersistor> defaultPersistors = PersistenceConfigurator.GetDefaultPersistors(host);
                if (defaultPersistors == null)
                {
                    if (host.GetType().FullName == "Infragistics.Windows.DataPresenter.XamDataGrid")
                    {
                        defaultPersistors = new[] { new XamDataGridLayoutPersistor() }; 
                    }
                    else
                    {
                       var persistable = new DependencyObjectPersistor()
                        {
                            PropertyNamesToPersist = PropertyNamesToPersist,
                            PropertyNamesToIgnore = PropertyNamesToIgnore, 
                            Mode = Mode
                        };
                        if (string.IsNullOrEmpty(PropertyNamesToPersist) && string.IsNullOrEmpty(PropertyNamesToIgnore))
                        {
                            var selector = host as Selector;
                            if (selector != null)
                            {
                                persistable.PropertyNamesToPersist = Selector.SelectedIndexProperty.Name;
                            }
                            else
                            {
                                var txt = host as TextBox;
                                if (txt != null)
                                {
                                    persistable.PropertyNamesToPersist = TextBox.TextProperty.Name;
                                }
                                else
                                {
                                    var toggler = host as ToggleButton;
                                    if (toggler != null)
                                    {
                                        persistable.PropertyNamesToPersist = ToggleButton.IsCheckedProperty.Name;
                                    }
                                    else
                                    {
                                        if (CoreUtilities.IsSubclassOf(host.GetType(), "Infragistics.Windows.Editors.ValueEditor"))
                                        {
                                            persistable.PropertyNamesToPersist = "Value";
                                        }
                                    }
                                }
                            }
                        }
                        defaultPersistors = new[] {persistable};
                    }

                }

                foreach (var defaultPersistor in defaultPersistors)
                {
                    if (defaultPersistor != null) configuration.Persistors.Add(defaultPersistor);
                }
                return configuration;
            }
            return null;
        }

        public string PropertyNamesToPersist { get; set; }
        public string PropertyNamesToIgnore { get; set; }
        public PersistenceExceptionPolicy ExceptionPolicy { get; set; }
        public string PersistenceName { get; set; }
        public LoadStage AutoLoadStage { get; set; }
        public AutoSaveMode AutoSave { get; set; }
        public PropertyMode Mode { get; set; }

    }
}
