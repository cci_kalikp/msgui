﻿using System; 
using System.Windows; 
using System.Windows.Markup; 
using MorganStanley.MSDotNet.MSGui.Core.Persistence;

namespace MorganStanley.MSDotNet.MSGui.Impl.Persistence
{
    public class SuppressChildrenPersistenceExtension:MarkupExtension
    {
        public override object ProvideValue(IServiceProvider serviceProvider_)
        {

            var target = serviceProvider_.GetService(typeof(IProvideValueTarget))
                    as IProvideValueTarget;
            var host = target.TargetObject as DependencyObject;
            if (host != null)
            {
                Configuration configuration = new Configuration();
                configuration.SuppressChildrenPersistence = true;
                return configuration;
            }
            return null;
        }
         
    }
}
