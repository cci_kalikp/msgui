﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core.Persistence; 

namespace MorganStanley.MSDotNet.MSGui.Impl.Persistence
{
    public class PersistenceConfigurator:DependencyObject 
    { 
        public static readonly DependencyProperty ConfigurationProperty = DependencyProperty.RegisterAttached(
    "Configuration",
    typeof(Configuration),
    typeof(Configuration),
    new FrameworkPropertyMetadata(null));

        public static void SetConfiguration(DependencyObject object_, Configuration configuration_)
        {
            object_.SetValue(ConfigurationProperty, configuration_);
        }

        public static Configuration GetConfiguration(DependencyObject object_)
        {
            return (Configuration) object_.GetValue(ConfigurationProperty);
        } 

        public static Configuration ResolveConfiguration(DependencyObject object_,
                                                                Configuration parentConfiguration_)
        {
            var configuration = (Configuration)object_.GetValue(ConfigurationProperty);
            if (parentConfiguration_ != null)
            {
                if (configuration == null) //inherit configuration(except the persistors）from parent 
                {
                    configuration = new Configuration();
                    configuration.PersistenceName = parentConfiguration_.PersistenceName;
                    configuration.ExceptionPolicy = parentConfiguration_.ExceptionPolicy;
                    configuration.AutoLoadStage = parentConfiguration_.AutoLoadStage;
                    configuration.AutoSave = parentConfiguration_.AutoSave;
                }
                else
                {
                    if (configuration.ExceptionPolicy == PersistenceExceptionPolicy.Default)
                    {
                        configuration.ExceptionPolicy = parentConfiguration_.ExceptionPolicy;
                    }
                    if (configuration.AutoLoadStage == LoadStage.Default)
                    {
                        configuration.AutoLoadStage = parentConfiguration_.AutoLoadStage;
                    }
                    if (configuration.AutoSave == AutoSaveMode.Default)
                    {
                        configuration.AutoSave = parentConfiguration_.AutoSave;
                    } 
                }
            }
            return configuration;
        }
         
        static readonly Dictionary<Type,List<IComponentPersistor>> defaultPersistors = new Dictionary<Type, List<IComponentPersistor>>();
        internal static IEnumerable<IComponentPersistor> GetDefaultPersistors(DependencyObject component_)
        {
            List<IComponentPersistor> persistors;
            defaultPersistors.TryGetValue(component_.GetType(), out persistors);
            return persistors;

        }

        public static void RegisterDefaultPersistor<TComponent, TPersistor>() where TComponent : DependencyObject
            where TPersistor : IComponentPersistor, new()
        {
            List<IComponentPersistor> persistors;
            if (!defaultPersistors.TryGetValue(typeof (TComponent), out persistors))
            {
                persistors = new List<IComponentPersistor>();
                defaultPersistors.Add(typeof(TComponent), persistors);
            }
            
            persistors.Add(new TPersistor());
        }

        public static void RegisterDefaultPersistors<TComponent, TPersistor1, TPersistor2>()
            where TComponent : DependencyObject
            where TPersistor1 : IComponentPersistor, new()
            where TPersistor2 : IComponentPersistor, new()
        {
            List<IComponentPersistor> persistors;
            if (!defaultPersistors.TryGetValue(typeof (TComponent), out persistors))
            {
                persistors = new List<IComponentPersistor>();
                defaultPersistors.Add(typeof (TComponent), persistors);
            }

            persistors.Add(new TPersistor1());
            persistors.Add(new TPersistor2());
        }

        public static void RegisterDefaultPersistors<TComponent, TPersistor1, TPersistor2, TPersistor3>()
            where TComponent : DependencyObject
            where TPersistor1 : IComponentPersistor, new()
            where TPersistor2 : IComponentPersistor, new()
            where TPersistor3 : IComponentPersistor, new()
        {
            List<IComponentPersistor> persistors;
            if (!defaultPersistors.TryGetValue(typeof (TComponent), out persistors))
            {
                persistors = new List<IComponentPersistor>();
                defaultPersistors.Add(typeof (TComponent), persistors);
            }

            persistors.Add(new TPersistor1());
            persistors.Add(new TPersistor2());
            persistors.Add(new TPersistor3());
        }

        public static void RegisterDefaultPersistors<TComponent, TPersistor1, TPersistor2, TPersistor3, TPersistor4>()
            where TComponent : DependencyObject
            where TPersistor1 : IComponentPersistor, new()
            where TPersistor2 : IComponentPersistor, new()
            where TPersistor3 : IComponentPersistor, new()
            where TPersistor4 : IComponentPersistor, new()
        {
            List<IComponentPersistor> persistors;
            if (!defaultPersistors.TryGetValue(typeof (TComponent), out persistors))
            {
                persistors = new List<IComponentPersistor>();
                defaultPersistors.Add(typeof (TComponent), persistors);
            }

            persistors.Add(new TPersistor1());
            persistors.Add(new TPersistor2());
            persistors.Add(new TPersistor3());
            persistors.Add(new TPersistor4());
        }

        public static void RegisterDefaultPersistors<TComponent>(params IComponentPersistor[] persistors_)
            where TComponent : DependencyObject
        {
            if (persistors_ != null && persistors_.Length > 0)
            {
                List<IComponentPersistor> persistors;
                if (!defaultPersistors.TryGetValue(typeof (TComponent), out persistors))
                {
                    persistors = new List<IComponentPersistor>();
                    defaultPersistors.Add(typeof (TComponent), persistors);
                }
                persistors.AddRange(persistors_);
            }

        }

        public class ComponentPersistenceStateEventArgs:EventArgs
        {
            public ComponentPersistenceStateEventArgs(object component_, XDocument state_, IComponentPersistor persistor_, Exception error_)
            {
                Component = component_;
                State = state_;
                Persistor = persistor_;
                Error = error_;
            }

            public object Component { get; private set; }
            public XDocument State { get; private set; }
            public IComponentPersistor Persistor { get; private set; }
            public Exception Error { get; private set; }
        }

        public static event EventHandler<ComponentPersistenceStateEventArgs> StateSaved;
        public static event EventHandler<ComponentPersistenceStateEventArgs> StateLoaded;
        protected internal static void OnStateSaved(IComponentPersistor persistor_, object component_, XDocument state_,
                                                    Exception error_)
        {
            var copy = StateSaved;
            if (copy != null)
            {
                ComponentPersistenceStateEventArgs arg = new ComponentPersistenceStateEventArgs(component_, state_, persistor_, error_);
                copy(component_, arg);
            }
        }

        protected internal static void OnStateLoaded(IComponentPersistor persistor_, object component_, XDocument state_,
                                            Exception error_)
        {
            var copy = StateLoaded;
            if (copy != null)
            {
                ComponentPersistenceStateEventArgs arg = new ComponentPersistenceStateEventArgs(component_, state_, persistor_, error_);
                copy(component_, arg);
            }
        }
    }
}
