﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace MorganStanley.MSDotNet.MSGui.Impl.Persistence
{
    interface IComponentPersistorHelper
    {
        XDocument TrySaveToXml(object component_);
        bool TryLoadFromXml(object component_, XDocument state_);
    }
     
}
