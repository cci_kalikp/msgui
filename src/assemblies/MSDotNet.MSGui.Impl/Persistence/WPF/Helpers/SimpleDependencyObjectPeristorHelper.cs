﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Windows; 
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Xml.Linq;
using Infragistics.Persistence.Primitives;

namespace MorganStanley.MSDotNet.MSGui.Impl.Persistence
{
    internal class SimpleDependencyObjectPeristorHelper:IComponentPersistorHelper
    {
        private readonly string[] propertiesToInclude;
        private readonly string[] propertiesToExclude;
        private readonly PropertyMode mode;
        private static readonly Dictionary<Type, TypeConverter> predefinedTypeConverters = new Dictionary<Type, TypeConverter>();
        static SimpleDependencyObjectPeristorHelper()
        {
            predefinedTypeConverters.Add(typeof(string), new StringConverter());
            predefinedTypeConverters.Add(typeof(int), new Int32Converter());
            predefinedTypeConverters.Add(typeof(short), new Int16Converter());
            predefinedTypeConverters.Add(typeof(long), new Int64Converter());
            predefinedTypeConverters.Add(typeof(uint), new UInt32Converter());
            predefinedTypeConverters.Add(typeof(ushort), new UInt16Converter());
            predefinedTypeConverters.Add(typeof(ulong), new UInt64Converter());
            predefinedTypeConverters.Add(typeof(double), new DoubleConverter());
            predefinedTypeConverters.Add(typeof(float), new SingleConverter());
            predefinedTypeConverters.Add(typeof(bool), new BooleanConverter());
            predefinedTypeConverters.Add(typeof(byte), new ByteConverter());
            predefinedTypeConverters.Add(typeof(sbyte), new SByteConverter());
            predefinedTypeConverters.Add(typeof(char), new CharConverter());
            predefinedTypeConverters.Add(typeof(decimal), new DecimalConverter());
            predefinedTypeConverters.Add(typeof(TimeSpan), new TimeSpanTypeConverter());
            predefinedTypeConverters.Add(typeof(Duration), new DurationTypeConverter());
            predefinedTypeConverters.Add(typeof(RepeatBehavior), new RepeatBehaviorTypeConverter());
            predefinedTypeConverters.Add(typeof(CultureInfo), new CultureInfoConverter());
            predefinedTypeConverters.Add(typeof(Type), new TypeTypeConverter());
            predefinedTypeConverters.Add(typeof(DateTime), new DateTimeConverter());
            predefinedTypeConverters.Add(typeof(Color), new ColorConverter());
        }



        public SimpleDependencyObjectPeristorHelper(string propertyNamesToPersist_, string propertyNamesToIgnore_, PropertyMode mode_)
        {
            this.propertiesToInclude = string.IsNullOrWhiteSpace(propertyNamesToPersist_) ? new string[0] : propertyNamesToPersist_.Split(',', ';');
            this.propertiesToExclude = string.IsNullOrWhiteSpace(propertyNamesToIgnore_) ? new string[0] : propertyNamesToIgnore_.Split(',', ';');
            this.mode = mode_;
        }

        private const string RootElementName = "SimplePersistence";
        public XDocument TrySaveToXml(object component_)
        {
            var propertyData = GetPropertyData(component_);
            if (propertyData == null) return null;
            XElement root = new XElement(RootElementName);
            foreach (var data in propertyData.Values)
            {
                var xml = data.ToXml();
                if (xml != null)
                {
                    root.Add(xml); 
                }
            }
            return new XDocument(root);
        }

        public bool TryLoadFromXml(object component_, XDocument state_)
        {
            if (state_ == null || state_.Root == null ) return true;
            if (state_.Root.Name != RootElementName)
            {
                return false;
            }
            foreach (var propertyData in state_.Root.Descendants())
            {
                PropertyData.FromXml(component_, propertyData);
            }
            
            return true;
        }

        private Dictionary<PropertyInfo, PropertyData> GetPropertyData(object component_)
        {

            var  result = new Dictionary<PropertyInfo, PropertyData>();
            if (propertiesToInclude.Length > 0 && mode == PropertyMode.PropertyName)
            { 
                foreach (var propertyName in propertiesToInclude)
                {

                   PropertyInfo property = component_.GetType().GetProperty(propertyName);
                   if (property != null && !result.ContainsKey(property))
                   {
                       if (SkipProperty(component_, property, false)) continue; 
                       var propertyData = GetPropertyData(component_, property);
                       if (propertyData == null)
                       {
                           return null;
                       }
                       result.Add(property, propertyData);
                    }
                }


            }
            else
            { 
                var propertyArray = component_.GetType().GetProperties();
                foreach (var propertyInfo in propertyArray)
                {
                    if (SkipProperty(component_, propertyInfo, true)) continue;
                    var propertyData = GetPropertyData(component_, propertyInfo);
                    if (propertyData == null)
                    {
                        return null;
                    }
                    result.Add(propertyInfo, propertyData);
                }
            }
            return result;
        }
         

        private bool SkipProperty(object component_, PropertyInfo propertyInfo_, bool checkSetting_)
        {
            if (!propertyInfo_.CanRead) return true;
            if (checkSetting_)
            {
                if (mode == PropertyMode.PropertyName)
                {
                    if (propertiesToInclude.Length > 0)
                    {
                        if (!propertiesToInclude.Contains(propertyInfo_.Name)) return true;
                    }
                    if (propertiesToExclude.Contains(propertyInfo_.Name)) return true;
                }
                else if (mode == PropertyMode.Contains)
                {
                    if (propertiesToInclude.Length > 0)
                    {
                        if (propertiesToInclude.FirstOrDefault(p_ => propertyInfo_.Name.Contains(p_)) == null)
                            return true;
                    }
                    if (propertiesToExclude.FirstOrDefault(p_ => propertyInfo_.Name.Contains(p_)) != null)
                    {
                        return true;
                    }
                }
            }
            if (!propertyInfo_.CanWrite)
            {
                object value = propertyInfo_.GetValue(component_, null);
                if (value == null) return true;
                bool canSetPropertySubValues = (from property in
                                                    propertyInfo_.PropertyType.GetProperties(BindingFlags.Instance |
                                                                                             BindingFlags.Public)
                                                where property.CanWrite
                                                select property).Any();
                if ((!canSetPropertySubValues && !(value is IEnumerable)) || value is FrameworkElement)
                {
                    return true;
                }
            }
            return false;
        }

        private PropertyData GetPropertyData(object component_, PropertyInfo propertyInfo_)
        {
            if (!propertyInfo_.CanRead || !propertyInfo_.CanWrite) return null;
            object value = propertyInfo_.GetValue(component_, null);
            if (value == null) return new PropertyData() {Property = propertyInfo_};
            if (value.GetType().IsEnum)
            {
                return new PropertyData() {Property = propertyInfo_, Value = value};
            }
            TypeConverter converter;
            object[] converters = propertyInfo_.GetCustomAttributes(typeof (TypeConverterAttribute), true);
            if (converters.Length > 0)
            {
                try
                {
                    converter = Type.GetType(((TypeConverterAttribute) converters[0]).ConverterTypeName)
                                     .GetConstructor(new Type[0])
                                     .Invoke(new object[0]) as TypeConverter;
                    if (converter != null && converter.CanConvertFrom(typeof(string)) &&
                        converter.CanConvertTo(typeof(string)))
                    {
                        string text = converter.ConvertToString(value);
                         converter.ConvertFromString(text);
                        return new PropertyData() {Property = propertyInfo_, Value = value, Converter = converter};
                    }
                }
                catch
                {

                }

            }
            converters = propertyInfo_.PropertyType.GetCustomAttributes(typeof (TypeConverterAttribute), true);
            if (converters.Length > 0)
            {
                try
                {
                    converter = Type.GetType(((TypeConverterAttribute)converters[0]).ConverterTypeName)
                                     .GetConstructor(new Type[0])
                                     .Invoke(new object[0]) as TypeConverter;
                    if (converter != null && converter.CanConvertFrom(typeof(string)) &&
                        converter.CanConvertTo(typeof(string)))
                    {
                        string text = converter.ConvertToString(value);
                        converter.ConvertFromString(text);
                        return new PropertyData() { Property = propertyInfo_, Value = value, Converter = converter };
                    }
                }
                catch
                {

                }

            }
            if (predefinedTypeConverters.TryGetValue(propertyInfo_.PropertyType, out converter))
            {
                return new PropertyData() { Converter = converter, Property = propertyInfo_, Value = value };
            }
            return null; 
        }

        private class PropertyData
        {
            public TypeConverter Converter { get; set; }
            public object Value { get; set; }
            public PropertyInfo Property { get; set; }

            public XElement ToXml()
            {
                if (Value == null) return null;
                XElement element = new XElement("Property");
                element.SetAttributeValue("Name", Property.Name);
                if (Converter != null)
                {
                    element.SetAttributeValue("Converter", Converter.GetType().AssemblyQualifiedName); 
                }
                if (Value != null)
                { 
                    element.SetAttributeValue("Value", Converter == null ? Value.ToString() : Converter.ConvertToString(Value));
                }
                return element;
            }

            public static void FromXml(object component_, XElement xml_)
            {
                var nameAttr = xml_.Attribute("Name");
                if (nameAttr == null || string.IsNullOrEmpty(nameAttr.Value)) return;
                PropertyInfo propertyInfo = component_.GetType().GetProperty(nameAttr.Value);
                if (propertyInfo == null)
                {
                    return;
                }

                object value = null;
                var valueAttr = xml_.Attribute("Value");
                if (valueAttr != null)
                {
                    var converterTypeAttr = xml_.Attribute("Converter");
                    if (converterTypeAttr != null && !string.IsNullOrEmpty(converterTypeAttr.Value))
                    {
                        TypeConverter converter = Type.GetType(converterTypeAttr.Value).GetConstructor(new Type[0])
                                                      .Invoke(null) as TypeConverter;
                        if (converter != null)
                        {
                            value = converter.ConvertFromString(valueAttr.Value);
                        }
                    }
                    else if (propertyInfo.PropertyType == typeof(string))
                    {
                        value = valueAttr.Value;
                    } 
                    else if (propertyInfo.PropertyType.IsEnum)
                    {
                        value = Enum.Parse(propertyInfo.PropertyType, valueAttr.Value, false);
                    }
                }
                
                propertyInfo.SetValue(component_, value, null);
            }
        }
        
    } 
   
}
