﻿ using System;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input; 
using Infragistics.Windows.Controls;
using Infragistics.Windows.DataPresenter;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea; 

namespace MorganStanley.MSDotNet.MSGui.Impl
{
    /// <summary>
    /// Extensions methods for XamDataGrid class.
    /// </summary> 
    public static class XamDataGridExtensions
    {
         
        /// <summary>
        /// Enables the re-ordering of columns in the data grid.
        /// </summary>
        /// <param name="dataGrid_">The data grid.</param>
        [MethodImpl(MethodImplOptions.NoInlining)]
        public static void EnableShowHideFields(this XamDataGrid dataGrid_)
        {
            dataGrid_.PreviewMouseRightButtonDown += OnXamDataGridPreviewMouseRightButtonDown; 
        }
         
        /// <summary>
        /// Disables the re-ordering of columns in the data grid.
        /// </summary>
        /// <param name="dataGrid_">The data grid.</param>
        [MethodImpl(MethodImplOptions.NoInlining)]
        public static void DisableShowHideFields(this XamDataGrid dataGrid_)
        {
            dataGrid_.PreviewMouseRightButtonDown -= OnXamDataGridPreviewMouseRightButtonDown;

        }

        /// <summary>
        /// Handle Right Mouse Click on data grid and show <see cref="ContextMenu"/> of
        /// available columns.
        /// </summary>
        /// <param name="sender">The data grid.</param>
        /// <param name="e">Event Parameters.</param>
        private static void OnXamDataGridPreviewMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            // Check if the left button is pressed.
            if (e.RightButton == MouseButtonState.Pressed)
            {
                XamDataGrid grid = sender as XamDataGrid;
                if (grid == null) return;
                DependencyObject source = e.OriginalSource as DependencyObject;
                if (source == null) return;
                ContextMenu contextMenu = null;
                HeaderLabelArea labelArea = source.FindVisualParent<HeaderLabelArea>();
                if (labelArea != null)
                {
                    if (labelArea.ContextMenu == null)
                    {
                        labelArea.ContextMenu = new ContextMenu();
                    }
                    contextMenu = labelArea.ContextMenu;
                }
                else
                {
                    CardPanel cp = source.FindVisualParent<CardPanel>();
                    if (cp != null)
                    {
                        if (cp.ContextMenu == null)
                        {
                            cp.ContextMenu = new ContextMenu();
                        }
                        contextMenu = cp.ContextMenu;

                    }
                    else
                    {
                        GroupByArea groupByArea = ((XamDataGrid) sender).GroupByArea;

                        if (groupByArea.ContextMenu == null)
                        {
                            groupByArea.ContextMenu = new ContextMenu();
                        }
                        contextMenu = groupByArea.ContextMenu;
                    }
                }
                if (contextMenu != null)
                { 
                    contextMenu.Items.Clear();

                    foreach (Field field in grid.DefaultFieldLayout.Fields)
                    {
                        Field fieldCopied = field;
                        var item = new MenuItem
                            {
                                Header = fieldCopied.Name,
                                IsCheckable = true,
                                IsChecked = field.Visibility == Visibility.Visible, 
                            };
                        item.Click += new Action(() =>
                            {
                                fieldCopied.Visibility = item.IsChecked ? Visibility.Visible : Visibility.Collapsed;
                            }).ConvertToRouted();
                        contextMenu.Items.Add(item);
                    }
                }
            }
        }
         

    }
}
