﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Xml.Linq;
using System.Xml.Serialization;
using Infragistics.Persistence;
using Infragistics.Persistence.Primitives;

namespace MorganStanley.MSDotNet.MSGui.Impl.Persistence
{
    class ComplexDependencyObjectPersistorHelper : IComponentPersistorHelper
    {
        private class PropertyPersistenceInfo : PropertyPersistenceInfoBase
        {

            private readonly string propertyName;
            public PropertyPersistenceInfo(string propertyName_)
            {
                propertyName = propertyName_;
            }
            public override bool DoesPropertyMeetCriteria(System.Reflection.PropertyInfo pi_, string propertyPath_)
            {
                return pi_ != null && this.propertyName == pi_.Name && !propertyPath_.Contains(".");
            }

        }

        private readonly PropertyMode mode;
        private readonly string[] propertiesToInclude;
        private readonly string[] propertiesToExclude;
        public PersistenceSettings Settings { get; private set; }

        public ComplexDependencyObjectPersistorHelper(string propertyNamesToPersist_, string propertyNamesToIgnore_, PropertyMode mode_)
        {
            this.propertiesToInclude = string.IsNullOrWhiteSpace(propertyNamesToPersist_) ? new string[0] : propertyNamesToPersist_.Split(',', ';');
            this.propertiesToExclude = string.IsNullOrWhiteSpace(propertyNamesToIgnore_) ? new string[0] : propertyNamesToIgnore_.Split(',', ';');
            this.mode = mode_;
        }

        private void EnsureSettings()
        {
            if (Settings != null) return;
            Settings = new PersistenceSettings();
            if (propertiesToInclude.Length == 0)
            {
                Settings.LoadPersistenceOptions = Settings.SavePersistenceOptions = PersistenceOption.AllButIgnored;
            }
            else
            {
                Settings.LoadPersistenceOptions = Settings.SavePersistenceOptions = PersistenceOption.OnlySpecified;
                foreach (var propertyName in propertiesToInclude)
                {
                    if (mode == PropertyMode.PropertyName)
                    {
                        Settings.PropertySettings.Add(new PropertyPersistenceInfo(propertyName));
                    }
                    else if (mode == PropertyMode.Contains || mode == PropertyMode.PropertyPath)
                    {
                        Settings.PropertySettings.Add(new PropertyNamePersistenceInfo(){PropertyName = propertyName, Options = mode == PropertyMode.Contains ? 
                            PropertyNamePersistenceOptions.Contains :  PropertyNamePersistenceOptions.PropertyPath});
                    }
                    else
                    {
                        Settings.PropertySettings.Add(new PropertyTypePersistenceInfo(){ PropertyType = Type.GetType(propertyName)});
                    }
                }
            }
            foreach (var propertyName in propertiesToExclude)
            {
                if (mode == PropertyMode.PropertyName)
                {
                    Settings.IgnorePropertySettings.Add(new PropertyPersistenceInfo(propertyName));
                }
                else if (mode == PropertyMode.Contains || mode == PropertyMode.PropertyPath)
                {
                    Settings.IgnorePropertySettings.Add(new PropertyNamePersistenceInfo()
                    {
                        PropertyName = propertyName,
                        Options = mode == PropertyMode.Contains ?
                            PropertyNamePersistenceOptions.Contains : PropertyNamePersistenceOptions.PropertyPath
                    });
                }
                else
                {
                    Settings.IgnorePropertySettings.Add(new PropertyTypePersistenceInfo() { PropertyType = Type.GetType(propertyName) });
                }
            }
        }

        private const string RootElementName = "ComplexPersistence";
        public XDocument TrySaveToXml(object component_)
        {
            EnsureSettings();
            using (MemoryStream s = PersistenceManager.Save(component_ as DependencyObject, Settings))
            {
                var xmlSerializer = new XmlSerializer(typeof(PersistenceObject));
                PersistenceObject persistenceObject = xmlSerializer.Deserialize(s) as PersistenceObject;

                XElement root = new XElement(RootElementName);
                root.Add(new XElement("Types", string.Join(";", persistenceObject.Types)));
                root.Add(XDocument.Parse(persistenceObject.Xml).Root);
                return new XDocument(root);
            }

        }

        public bool TryLoadFromXml(object component_, XDocument state_)
        {
            if (state_ == null || state_.Root == null) return true;
            if (state_.Root.Name != RootElementName)
            {
                return false;
            } 
            EnsureSettings();
            PersistenceObject persistenceObject = new PersistenceObject();
            XElement types = state_.Root.Element("Types");
            if (types != null && !string.IsNullOrEmpty(types.Value))
            {
                persistenceObject.Types = new Collection<string>(types.Value.Split(';'));

            }
            XElement groupData = state_.Root.Element("GroupData");
            if (groupData == null || string.IsNullOrEmpty(groupData.Value)) return true;
            persistenceObject.Xml = groupData.ToString();

            using (MemoryStream s = new MemoryStream())
            {
                new XmlSerializer(typeof(PersistenceObject)).Serialize(s, persistenceObject);
                s.Position = 0;
                PersistenceManager.Load(component_ as DependencyObject, s, Settings);
            }
            return true;
        }
    }
}
