﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Media.Animation;

namespace MorganStanley.MSDotNet.MSGui.Impl.Persistence
{
    /// <summary>
    /// A <see cref="T:System.ComponentModel.TypeConverter" /> that converts from RepeatBehavior to string, and string to RepeatBehavior.
    /// </summary>
    public class RepeatBehaviorTypeConverter : TypeConverter
    {
        /// <summary>
        /// Returns true if the sourceType is of type string.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="sourceType"></param>
        /// <returns></returns>
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return sourceType == typeof(string);
        }
        /// <summary>
        /// Returns true if the destinationType is of type string.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="destinationType"></param>
        /// <returns></returns>
        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            return destinationType == typeof(string);
        }
        /// <summary>
        /// Converts a string into a RepeatBehavior.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="culture"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            string text = (string)value;
            if (text == "Forever")
            {
                return RepeatBehavior.Forever;
            }
            string[] array = text.Split(new char[]
			{
				':'
			});
            if (array.Length == 2)
            {
                if (array[0] == "D")
                {
                    return new RepeatBehavior(TimeSpan.Parse(array[1]));
                }
                if (array[0] == "C")
                {
                    return new RepeatBehavior(double.Parse(array[1]));
                }
            }
            return null;
        }
        /// <summary>
        /// Converts a RepeatBehavior into a string.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="culture"></param>
        /// <param name="value"></param>
        /// <param name="destinationType"></param>
        /// <returns></returns>
        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            if (value == null)
            {
                return null;
            }
            RepeatBehavior repeatBehavior = (RepeatBehavior)value;
            if (repeatBehavior.HasDuration)
            {
                return "D:" + repeatBehavior.Duration.ToString();
            }
            if (repeatBehavior.HasCount)
            {
                return "C:" + repeatBehavior.Count.ToString();
            }
            return "Forever";
        }
    }
}
