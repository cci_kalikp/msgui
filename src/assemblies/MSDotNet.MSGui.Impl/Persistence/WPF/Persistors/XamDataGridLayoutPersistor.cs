﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Infragistics.Windows.DataPresenter;
using MorganStanley.MSDotNet.MSGui.Core.Persistence;

namespace MorganStanley.MSDotNet.MSGui.Impl.Persistence
{
    [Serializable]
    public class XamDataGridLayoutPersistor : ComponentPersistorBase
    {
        public override LoadStage AutoLoadStageResolved
        {
            get
            {
                return AutoLoadStage == LoadStage.Default ? LoadStage.AfterShown :  AutoLoadStage;
            }
        }

        protected override IComponentPersistor CloneCore()
        {
            return new XamDataGridLayoutPersistor();
        }

        protected override void LoadStateCore(XDocument state_)
        { 
            XamDataGrid grid = component as XamDataGrid; 
            if (grid.GroupByArea != null)
            {
                var isGroupByAreaExpandedAttr = state_.Root.Attribute("IsGroupByAreaExpanded");
                if (isGroupByAreaExpandedAttr != null)
                {
                    grid.GroupByArea.IsExpanded = isGroupByAreaExpandedAttr.Value.ToLower() == "true";
                }
            }
            XElement layoutElement = state_.Root.Descendants("xamDataPresenter").FirstOrDefault();
            if (layoutElement != null)
            {
                grid.LoadCustomizations(layoutElement.ToString());
            }

        }

        protected override XDocument SaveStateCore()
        {
            XElement rootElement = new XElement("XamDataGridLayout");
            XamDataGrid grid = component as XamDataGrid; 
            if (grid.GroupByArea != null)
            {
                rootElement.SetAttributeValue("IsGroupByAreaExpanded", grid.GroupByArea.IsExpanded);
            }
            string xml = grid.SaveCustomizations();
            rootElement.Add(XElement.Parse(xml));
            return new XDocument(rootElement);
        }

        protected override string PersistorSuffix
        {
            get { return typeof (XamDataGrid).FullName + "." + "Layout"; }
        }

        protected override bool Validate(object component_)
        {
            return component_ is XamDataGrid;
        }
    }
}
