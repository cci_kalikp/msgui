﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq; 
using System.Windows;
using System.Xml.Linq; 
using MorganStanley.MSDotNet.MSGui.Core.Persistence; 

namespace MorganStanley.MSDotNet.MSGui.Impl.Persistence
{
    [Serializable]
    public class SubViewPersistor<TEnumerator> : ComponentPersistorBase, IRootComponentPersistor<TEnumerator> where TEnumerator:IComponentPersistorEnumerator, new()
    {  
        #region Constructor  
         
        public SubViewPersistor(DependencyObject component_, string id_ = null, XDocument stateSaveTo_ = null)
        {
            WrapComponent(component_, null, null);
            AutoLoadStage = LoadStage.Later;
            id = id_;
            savedState = stateSaveTo_;
            CachePersistors = true;
        }
          
        #endregion 
        [NonSerialized]
        private IEnumerable<IComponentPersistor> persistors;
        private readonly string id;
        public string RootElementName { get { return "ViewData"; } }
        public bool CachePersistors { get; set; }
        public override LoadStage AutoLoadStageResolved
        {
            get { return AutoLoadStage == LoadStage.Default ? LoadStage.Later : AutoLoadStage; }
        }

        public override AutoSaveMode AutoSaveResolved
        {
            get { return AutoSave == AutoSaveMode.Default ? AutoSaveMode.False : AutoSave; }
        }
         
        public override PersistenceExceptionPolicy ExceptionPolicyResolved
        {
            get { return ExceptionPolicy == PersistenceExceptionPolicy.Default ? PersistenceExceptionPolicy.Ignore : ExceptionPolicy; }
        }

        protected override string PersistorSuffix
        {
            get { return component.GetType().FullName + ".State"; }
        }
        [NonSerialized]
        private readonly TEnumerator persistorEnumerator = new TEnumerator();
        public TEnumerator PersistorEnumerator
        {
            get { return persistorEnumerator; }
        }
        protected override IComponentPersistor CloneCore()
        {
            return new SubViewPersistor<TEnumerator>(this.component as DependencyObject, id, null);  
        }

        protected override void LoadStateCore(XDocument state_)
        {
            if (persistors == null || !CachePersistors)
            {
                persistors = PersistorEnumerator.GetPersistors(component);
            }
            foreach (IComponentPersistor persistor in persistors)
            {
                if (String.IsNullOrEmpty(persistor.PersistorId))
                {
                    throw new InvalidOperationException(
                      String.Format(
                        "Component Persistor '{0}' returned an empty key.",
                        persistor.GetType()));
                }
                string key = persistor.PersistorId;
                if (!string.IsNullOrEmpty(id))
                {
                    key = id + "." + key;
                }

                XElement value = System.Xml.XPath.Extensions.XPathSelectElement(state_,
                                                                                string.Format(
                                                                                    "//Item[@Id='{0}']", key));
                if (value != null)
                {
                    XElement content = value.Descendants().FirstOrDefault();
                    if (content != null)
                    {
                        persistor.LoadState(new XDocument(content));

                    }
                }
            }
        }

        protected override XDocument SaveStateCore()
        {
            if (persistors == null || !CachePersistors)
            {
                persistors = PersistorEnumerator.GetPersistors(component);
            }
            XDocument existingState = SavedState ?? LoadedState;
            XDocument state = existingState; 
            foreach (IComponentPersistor persistor in persistors)
            { 
                if (String.IsNullOrEmpty(persistor.PersistorId))
                {
                    throw new InvalidOperationException(
                      String.Format(
                        "Component Persistor '{0}' returned an empty key.",
                        persistor.GetType()));
                }
                string key = persistor.PersistorId;
                if (!string.IsNullOrEmpty(id))
                {
                    key = id + "." + key;
                }
                XDocument value = persistor.SaveState();
                if (value != null && value.Root != null)
                {
                    if (state == null)
                    {
                        state = new XDocument(new XElement(RootElementName));
                    }
                    if (existingState != null)
                    {
                        XElement existingElement = System.Xml.XPath.Extensions.XPathSelectElement(state,
                                            string.Format(
                                                "//Item[@Id='{0}']", key));
                        if (existingElement != null)
                        {
                            existingElement.Remove();
                        }
                    }
                    XElement element = new XElement("Item");
                    element.SetAttributeValue("Id", key);
                    element.Add(value.Root);
                    state.Root.Add(element);
                }
            }
            return state;
        }

    }
}
