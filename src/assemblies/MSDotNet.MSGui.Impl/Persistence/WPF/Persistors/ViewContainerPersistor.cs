﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq; 
using System.Windows;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager; 

namespace MorganStanley.MSDotNet.MSGui.Impl.Persistence
{ 
    public class ViewContainerPersistor<TEnumerator> : IRootComponentPersistor<TEnumerator> where TEnumerator : IComponentPersistorEnumerator, new()
    {  
        #region Constructor  

        public ViewContainerPersistor(IWindowViewContainer viewContainer_, string id_ = null, XDocument stateSaveTo_ = null)
        {
            WrapComponent(viewContainer_, null, null);
            this.id = id_;
            if (stateSaveTo_ != null)
            {
                this.SavedState = stateSaveTo_;              
            }
            CachePersistors = true;
        }
         
        private readonly string id;
        public string RootElementName { get { return "ViewData"; } }
         
        public XDocument LoadedState { get; private set; }  
        public XDocument SavedState
        {
            get { return ViewContainer.ViewState; }
            set { ViewContainer.ViewState = value; }
        }
        public AutoSaveMode AutoSave { get; set; }

        public LoadStage AutoLoadStage { get; set; }


        public LoadStage AutoLoadStageResolved
        {
            get { return AutoLoadStage == LoadStage.Default ? LoadStage.Later : AutoLoadStage; }
        }

        public AutoSaveMode AutoSaveResolved
        {
            get { return AutoSave == AutoSaveMode.Default ? AutoSaveMode.False : AutoSave; }
        }

        public PersistenceExceptionPolicy ExceptionPolicy { get; set; }

        public PersistenceExceptionPolicy ExceptionPolicyResolved
        {
            get { return ExceptionPolicy == PersistenceExceptionPolicy.Default ? PersistenceExceptionPolicy.Ignore : ExceptionPolicy; }
        }

        public bool CachePersistors { get; set; }
        #endregion
        internal IWindowViewContainerInternal ViewContainer { get; private set; } 

        private LoadStage CurrentStage
        {
            get
            {
                WindowViewContainer window = ViewContainer as WindowViewContainer;
                return (window == null) ? LoadStage.Later : window.CurrentStage; 
            }  
        }

        private XDocument stateLoading; 
        private IEnumerable<IComponentPersistor> persistors;
        private readonly TEnumerator persistorEnumerator = new TEnumerator();
        public TEnumerator PersistorEnumerator 
        {  
            get { return persistorEnumerator; }
        }
        public string PersistorId
        {
            get { return (string.IsNullOrEmpty(id) ? "" : id + ".") + ViewContainer.GetType().FullName + ".State"; }
        }

        public void LoadState(XDocument state_)
        {
            LoadState(state_, false);
        }

        public void LoadState(XDocument state_, bool loadManualItems_)
        { 
            if (ViewContainer == null) return;
            WindowViewContainer window = ViewContainer as WindowViewContainer;
            if (window != null)
            {
                window.ViewState = state_;
            }  
            StartLoadState(ViewContainer.Content, state_, false, loadManualItems_,null); 
        }
         
        public XDocument SaveState()
        {
            if (ViewContainer.Content == null) return null;
            if (persistors == null || !CachePersistors) 
            {
                persistors = PersistorEnumerator.GetPersistors(ViewContainer.Content);
            }
            XDocument state = SaveStateCore(this.persistors, false,null);
            PersistenceConfigurator.OnStateSaved(this, ViewContainer.Content, state, null);
            return state;

        }

        public void LoadSubState(object element_, XDocument state_, bool loadManualItems_ = false, string subStateId_ = null)
        {
            if (element_ == null)
            {
                throw new ArgumentNullException("element_");
            }
            if (stateLoading != null) return;
            if (element_ == ViewContainer.Content)
            {
                LoadState(state_);
                return;
            }
            StartLoadState(element_, state_, true, loadManualItems_,subStateId_);  
        }

        public XDocument SaveSubState(object element_, XDocument stateSavedTo_=null, string subStateId_=null)
        {
            if (element_ == null) return null;
            if (stateSavedTo_ != null)
            {
                SavedState = stateSavedTo_;
            }
            XDocument state = SaveStateCore(PersistorEnumerator.GetComponentPersistors(ViewContainer.Content, element_), true, subStateId_);
            PersistenceConfigurator.OnStateSaved(this, element_, state, null);
            return state;
        }

        public XDocument RemoveSubState(object element_, XDocument stateToDeleteFrom_ = null, string subStateId_ = null)
        {
            if (stateToDeleteFrom_ != null)
            {
                SavedState = stateToDeleteFrom_;
            }

            XDocument existingState = SavedState ?? LoadedState;
            if (existingState == null || existingState.Root == null)
            {
                return existingState;
            } 
            foreach (IComponentPersistor persistor in PersistorEnumerator.GetComponentPersistors(ViewContainer.Content, element_))
            { 
                if (String.IsNullOrEmpty(persistor.PersistorId))
                {
                    throw new InvalidOperationException(
                      String.Format(
                        "Component Persistor '{0}' returned an empty key.",
                        persistor.GetType()));
                }
                string key = persistor.PersistorId;
                if (!string.IsNullOrEmpty(subStateId_))
                {
                    key = subStateId_ + "." + key;
                }
                if (!string.IsNullOrEmpty(id))
                {
                    key = id + "." + key;
                }
                XElement existingElement = System.Xml.XPath.Extensions.XPathSelectElement(existingState,
                                    string.Format(
                                        "//Item[@Id='{0}']", key));
                if (existingElement != null)
                {
                    existingElement.Remove();
                }
            }
            return existingState;
        }

        private void StartLoadState(object component_, XDocument state_, bool subState_,bool loadManualItems_,string subStateId_)
        {
            if (stateLoading != null) return;
 
            if (!subState_)
            {
                LoadedState = state_;    
            }
            stateLoading = state_;  
            if (CurrentStage < LoadStage.AfterShown)
            {
 
                LoadStateCore(component_, state_, subState_, false, CurrentStage, subStateId_);
                WindowViewContainer windowViewContainer = ViewContainer as WindowViewContainer;
                if (windowViewContainer != null)
                {
                    PropertyChangedEventHandler changedHandler = null;
                    windowViewContainer.PropertyChanged += changedHandler = (sender_, args_) =>
                    {
                        if (args_.PropertyName == "CurrentStage")
                        {
                            if (windowViewContainer.CurrentStage == LoadStage.AfterShown)
                            {
                                windowViewContainer.PropertyChanged -= changedHandler;
                                stateLoading = null;
                                if (loadManualItems_)
                                {
                                    LoadStateCore(component_, state_, subState_, false, LoadStage.Later, subStateId_); 
                                }

                                PersistenceConfigurator.OnStateLoaded(this, component_, state_, null);
                            }
                            LoadStateCore(component_, state_, subState_, false, CurrentStage, subStateId_);

                        }
                    };
                }
            }
            else
            {
                LoadStateCore(component_, state_, subState_, true, loadManualItems_ ? LoadStage.Later : CurrentStage, subStateId_); 
                PersistenceConfigurator.OnStateLoaded(this, component_, state_, null);
                stateLoading = null;
            }
        }

        private void LoadStateCore(object component_, XDocument state_, bool subState_, bool overrideLoaded_,LoadStage statge_, string subStateId_)
        {
            if (state_ == null) return;
            if (!subState_ && component_ == null)
            {
                component_ = ViewContainer.Content;
            }
            IEnumerable<IComponentPersistor> currentPersistors;
            if (!subState_ && CachePersistors)
            {
                 if (persistors == null)
                 {
                     persistors = PersistorEnumerator.GetPersistors(component_);
                 }
                currentPersistors = persistors;
            }
            else 
            {
                currentPersistors =  !subState_ ? PersistorEnumerator.GetPersistors(component_): 
                    PersistorEnumerator.GetComponentPersistors(ViewContainer.Content, component_);
            }
             if (currentPersistors == null) return;

            foreach (IComponentPersistor persistor in currentPersistors)
            {
                if (persistor.AutoLoadStageResolved <= statge_ && (persistor.LoadedState == null || overrideLoaded_))
                {
                    if (String.IsNullOrEmpty(persistor.PersistorId))
                    {
                        throw new InvalidOperationException(
                          String.Format(
                            "Component Persistor '{0}' returned an empty key.",
                            persistor.GetType()));
                    }
                    string key = persistor.PersistorId; 
                    if (!string.IsNullOrEmpty(subStateId_))
                    {
                        key = subStateId_ + "." + key;
                    }
                    if (!string.IsNullOrEmpty(id))
                    {
                        key = id + "." + key;
                    }
                    XElement value = System.Xml.XPath.Extensions.XPathSelectElement(state_,
                                                                                    string.Format(
                                                                                        "//Item[@Id='{0}']", key));
                    if (value != null)
                    {
                        XElement content = value.Descendants().FirstOrDefault();
                        if (content != null)
                        {
                            persistor.LoadState(new XDocument(content));
                  
                        }
                    }

                }
            }

        }

        private XDocument SaveStateCore(IEnumerable<IComponentPersistor> persistors_, bool subState_, string subStateId_)
        { 
            if (persistors_ == null) return null;

            XDocument existingState = SavedState ?? LoadedState;
            if (existingState != null && existingState.Root == null)
            {
                existingState.Add(new XElement(RootElementName));
            }
            XDocument state = existingState; 
            foreach (IComponentPersistor persistor in persistors_)
            {
                if (persistor.AutoSaveResolved == AutoSaveMode.False && !subState_) continue; 
                if (String.IsNullOrEmpty(persistor.PersistorId))
                {
                    throw new InvalidOperationException(
                      String.Format(
                        "Component Persistor '{0}' returned an empty key.",
                        persistor.GetType()));
                }
                string key = persistor.PersistorId;
                if (!string.IsNullOrEmpty(subStateId_))
                {
                    key = subStateId_ + "." + key;
                }
                if (!string.IsNullOrEmpty(id))
                {
                    key = id + "." + key;
                }
                XDocument value = persistor.SaveState();
                if (value != null && value.Root != null)
                {
                    if (state == null)
                    {
                        state = new XDocument(new XElement(RootElementName)); 
                    }
                    if (existingState != null)
                    {
                        XElement existingElement = System.Xml.XPath.Extensions.XPathSelectElement(state,
                                            string.Format(
                                                "//Item[@Id='{0}']", key));
                        if (existingElement != null)
                        {
                            existingElement.Remove();
                        }
                    }

                    XElement element = new XElement("Item");
                    element.SetAttributeValue("Id", key);
                    element.Add(value.Root);
                    state.Root.Add(element);
                }
            }
            if (!subState_ || existingState != null)
            { 
                SavedState = state;
            }
            return state;
        }


        public IComponentPersistor WrapComponent(object component_, IEnumerable<string> namespaces_, Configuration configuration_)
        {
            if (!(component_ is IWindowViewContainerInternal)) 
                throw new ArgumentOutOfRangeException("component_ must be an instance of IWindowViewContainer");
            ViewContainer = component_ as IWindowViewContainerInternal;
            return this;
        }


    }
}
