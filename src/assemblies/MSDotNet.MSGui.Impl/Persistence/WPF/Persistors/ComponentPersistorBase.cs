﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core.Persistence; 
using MorganStanley.MSDotNet.My;

namespace MorganStanley.MSDotNet.MSGui.Impl.Persistence
{
    public abstract class ComponentPersistorBase : IComponentPersistor, ICloneable
    {
        [NonSerialized]
        private string persistorId;
        public virtual string PersistorId
        {
            get { return persistorId; }
        }
        protected object component; 
        private readonly IMSLogger logger = MSLoggerFactory.CreateLogger<ComponentPersistorBase>(); 

        public virtual IComponentPersistor WrapComponent(object component_, IEnumerable<string> namespaces_,
                                         Configuration configuration_)
        {
            if (!Validate(component_))
            {
                throw new ArgumentOutOfRangeException("component_ is not of valid instance type");
            } 
            if (component_ != component && component != null)
            {
                return Clone().WrapComponent(component_, namespaces_, configuration_);
            }
            
            component = component_;
            if (configuration_ != null)
            {
                if (ExceptionPolicy == PersistenceExceptionPolicy.Default)
                {
                    ExceptionPolicy = configuration_.ExceptionPolicy;
                }
                if (AutoLoadStage == LoadStage.Default)
                {
                    AutoLoadStage = configuration_.AutoLoadStage;
                }
                if (AutoSave == AutoSaveMode.Default)
                {
                    AutoSave = configuration_.AutoSave;
                }
            }


            StringBuilder namespaceBuilder = new StringBuilder();
            if (namespaces_ != null)
            {
                foreach (string namespaceItem in namespaces_)
                {
                    namespaceBuilder.Append(namespaceItem);
                    namespaceBuilder.Append('.');
                }
            }

            persistorId = String.Format(CultureInfo.InvariantCulture, "{0}{1}", namespaceBuilder, PersistorSuffix);
            return this;

        }

        protected virtual bool Validate(object component_)
        {
            return component_ is DependencyObject;
        }

        public AutoSaveMode AutoSave { get; set; } 
        public LoadStage AutoLoadStage { get; set; }
        public PersistenceExceptionPolicy ExceptionPolicy { get; set; }

        [NonSerialized] protected XDocument loadedState;

        public XDocument LoadedState
        {
            get { return loadedState; } 
        }

        [NonSerialized] protected XDocument savedState;
        public XDocument SavedState
        {
            get { return savedState; }
        }

        public void LoadState(XDocument state_)
        {
            if (state_ == null || state_.Root == null || component == null) return;
             loadedState = state_;
            try
            {
                LoadStateCore(state_);
                PersistenceConfigurator.OnStateLoaded(this, component, state_, null);
            }
            catch (Exception ex)
            { 
                PersistenceConfigurator.OnStateLoaded(this, component, state_, ex);
                switch (ExceptionPolicyResolved)
                {
                    case PersistenceExceptionPolicy.Rethrow:
                        throw;

                    case PersistenceExceptionPolicy.Ignore:
                    default:
                        // logging all details about the exception 
                        logger.Warning(String.Format(
                          "Failed to restore the value of '{0}' for object '{1}'.",
                          this.GetType(),
                          component), ex);
                        logger.Warning(String.Format("Persisted value: '{0}'", state_.ToString(SaveOptions.None)));
                        break;
                }
            }
        }

        public XDocument SaveState()
        {
            if (component == null) return null; 
            try
            {
                savedState = SaveStateCore();
                PersistenceConfigurator.OnStateSaved(this, component, savedState, null);

                return savedState;
            }
            catch (Exception ex)
            { 
                PersistenceConfigurator.OnStateSaved(this, component, savedState, ex);
                switch (ExceptionPolicyResolved)
                {
                    case PersistenceExceptionPolicy.Rethrow:
                        throw;

                    case PersistenceExceptionPolicy.Ignore:
                    default:
                        // logging all details about the exception 
                        logger.Warning(String.Format(
                          "Failed to save the value of '{0}' for object '{1}'.",
                         this.GetType(),
                          component), ex);
                        return null; 
                }

            }
        } 
        protected abstract string PersistorSuffix { get; } 
        protected abstract void LoadStateCore(XDocument state_); 
        protected abstract XDocument SaveStateCore();
        protected abstract IComponentPersistor CloneCore();
        public IComponentPersistor Clone()
        {
            var clone = CloneCore();
            clone.AutoSave = this.AutoSave;
            clone.AutoLoadStage = this.AutoLoadStage;
            clone.ExceptionPolicy = this.ExceptionPolicy;
            return clone;
        } 

        object ICloneable.Clone()
        {
            return this.Clone();
        }


        public virtual LoadStage AutoLoadStageResolved
        {
            get { return AutoLoadStage == LoadStage.Default ? LoadStage.AfterInitialized : AutoLoadStage; }
        }

        public virtual AutoSaveMode AutoSaveResolved
        {
            get { return AutoSave == AutoSaveMode.Default ? AutoSaveMode.True :  AutoSave; }
        }

        public virtual PersistenceExceptionPolicy ExceptionPolicyResolved
        {
            get
            {
                return ExceptionPolicy == PersistenceExceptionPolicy.Default
                           ? PersistenceExceptionPolicy.Ignore
                           : ExceptionPolicy;
            }
        }
    }
}
