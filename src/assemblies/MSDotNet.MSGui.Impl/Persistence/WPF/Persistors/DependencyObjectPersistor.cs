﻿using System; 
using System.Xml.Linq; 
using MorganStanley.MSDotNet.MSGui.Core.Persistence;
using MorganStanley.MSDotNet.MSGui.Core.SmartApi;

namespace MorganStanley.MSDotNet.MSGui.Impl.Persistence
{

    public enum PropertyMode
    {
        PropertyName,
        Contains,
        PropertyPath,
        PropertyType
    }
    [Serializable]
    public class DependencyObjectPersistor : ComponentPersistorBase
    {


        private static bool igPersistenceAssemblyLoaded = false;
        private static readonly object SyncRoot = new object();

        public LoadStage LoadStrategy { get; set; }
        public PropertyMode Mode { get; set; }

        [NonSerialized]
        private IComponentPersistorHelper helper;

        protected override void LoadStateCore(XDocument state_)
        {
            if (helper != null)
            {
                if (helper.TryLoadFromXml(component, state_))
                {
                    return;
                }

            }
            if (Mode == PropertyMode.PropertyName ||
                Mode == PropertyMode.Contains)
            {
                helper = new SimpleDependencyObjectPeristorHelper(PropertyNamesToPersist, PropertyNamesToIgnore, Mode);
                if (helper.TryLoadFromXml(component, state_))
                {
                    return;
                }
            }
            lock (SyncRoot)
            {
                if (!igPersistenceAssemblyLoaded)
                {
                    ExtensionsUtils.RequireAssembly(ExternalAssembly.Infragistics, ExternalAssembly.InfragisticsPersistence);
                    igPersistenceAssemblyLoaded = true;
                }
            }
            helper = new ComplexDependencyObjectPersistorHelper(PropertyNamesToPersist, PropertyNamesToIgnore, Mode);
            helper.TryLoadFromXml(component, state_);

        }

        protected override XDocument SaveStateCore()
        {
            XDocument state = null;
            if (helper != null)
            {
                state = helper.TrySaveToXml(component);
                if (state != null) return state;  
            }
            if (Mode == PropertyMode.PropertyName ||
                Mode == PropertyMode.Contains)
            {
                helper = new SimpleDependencyObjectPeristorHelper(PropertyNamesToPersist, PropertyNamesToIgnore, Mode);
                state = helper.TrySaveToXml(component);
                if (state != null) return state;  
            }

            lock (SyncRoot)
            {
                if (!igPersistenceAssemblyLoaded)
                {
                    ExtensionsUtils.RequireAssembly(ExternalAssembly.Infragistics, ExternalAssembly.InfragisticsPersistence);
                    igPersistenceAssemblyLoaded = true;
                }
            }
            helper = new ComplexDependencyObjectPersistorHelper(PropertyNamesToPersist, PropertyNamesToIgnore, Mode);
            state = helper.TrySaveToXml(component); 
            return state; 
        }

        protected override string PersistorSuffix
        {
            get { return component.GetType().FullName + ".Persistence"; }
        }

        public override LoadStage AutoLoadStageResolved
        {
            get
            {
                return AutoLoadStage == LoadStage.Default ? LoadStage.AfterLoaded :  AutoLoadStage;
            }
        }

        public string PropertyNamesToPersist { get; set; }
        public string PropertyNamesToIgnore { get; set; }

        protected override IComponentPersistor CloneCore()
        {
            var cloned = new DependencyObjectPersistor
                {
                    PropertyNamesToPersist = this.PropertyNamesToPersist,
                    PropertyNamesToIgnore = this.PropertyNamesToIgnore, 
                    Mode = this.Mode,
                    helper = helper
                };
            return cloned;
        }
         
    }
}
