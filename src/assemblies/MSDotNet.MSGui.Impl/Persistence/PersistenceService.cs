﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Persistence/PersistenceService.cs#41 $
// $Change: 901195 $
// $DateTime: 2014/10/16 05:40:06 $
// $Author: caijin $

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Data;
using System.Xml;
using System.Xml.Linq;
using MorganStanley.IED.Concord.Configuration;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.Options;
using MorganStanley.MSDotNet.MSGui.Impl.Shell;

namespace MorganStanley.MSDotNet.MSGui.Impl.Persistence
{
    internal class PersistenceService : IItemsDictionaryService, IPersistenceService, IPersistenceProfileServiceProxy
    {
        internal static bool CallNullPersistence = false;

        private readonly Dictionary<string, StoreRestorePair> _persistableItems =
            new Dictionary<string, StoreRestorePair>();

        private readonly Dictionary<string, StoreRestorePair> _globalPersistableItems =
            new Dictionary<string, StoreRestorePair>();

        private readonly Dictionary<string, XElement> _tempProfileStates = new Dictionary<string, XElement>();

        private PersistenceProfileService _profileService;

        public IShell Shell { get; set; }

        #region IPersistenceService Members

        public void AddPersistor(
            string regeneratorId,
            RestoreStateHandler restoreStateHandler,
            SaveStateHandler saveStateHandler)
        {
            AddPersistor(regeneratorId, restoreStateHandler, saveStateHandler, false);
        }


        internal void AddPersistor(
    string regeneratorId,
    RestoreStateHandler restoreStateHandler,
    SaveStateHandler saveStateHandler,
            bool allowBackgroundThreadInvoke)
        {
            if (_persistableItems.ContainsKey(regeneratorId))
            {
                throw new InvalidOperationException(
                    string.Format("Key collision! Store&Restore functions have already been generated for the key {0}",
                                  regeneratorId));
            }

            _persistableItems.Add(
                regeneratorId,
                new StoreRestorePair
                {
                    Save = saveStateHandler,
                    Restore = restoreStateHandler,
                    AllowBackgroundThreadInvoke = allowBackgroundThreadInvoke
                });
        }

        public void RemovePersistor(string persistorId)
        {
            _persistableItems.Remove(persistorId);
        }

        public void AddGlobalPersistor(
            string persistorId,
            RestoreStateHandler restoreStateHandler,
            SaveStateHandler saveStateHandler)
        {
            if (_globalPersistableItems.ContainsKey(persistorId))
            {
                throw new InvalidOperationException(
                    string.Format("Key collision! Store&Restore functions have already been generated for the key {0}",
                                  persistorId));
            }

            _globalPersistableItems.Add(
                persistorId,
                new StoreRestorePair
                    {
                        Save = saveStateHandler, 
                        Restore = restoreStateHandler
                    });
        }

        public void RemoveGlobalPersistor(string persistorId)
        {
            _globalPersistableItems.Remove(persistorId);
        }

        public void LoadProfile(string name)
        {
            Shell.LoadProfile(name);
        }

        public void SaveProfileAs()
        {
            Shell.SaveProfileAs();
        }

        public void SaveProfileAs(string name)
        {
            ProfileService.SaveCurrentProfileAs(name);
        }

        public void SaveCurrentProfile()
        {
            Shell.SaveCurrentProfile();
        }

        public ObservableCollection<string> GetProfileNames()
        {
            return ProfileService.CurrentAvailableProfiles;
        }

        public void DeleteCurrentProfile()
        {
            Shell.DeleteCurrentProfile();
        }

        public event EventHandler<ProfileSavedEventArgs> ProfileSaved;
        public string GetCurrentProfileName()
        {
            return ProfileService.CurrentProfile;
        }

        public void MakeProfileDefault(string name)
        {
            if (GetProfileNames().Contains(name))
            {
                ProfileService.Storage.DefaultName = name;
            }
        }

        public void PersistGlobalItems(IEnumerable<string> keys = null)
        {
            if (keys == null)
            {
                SaveItemsToDict(ItemScope.GlobalPersistor);
            }
            else
            {
                SaveItemsToDict(keys, ItemScope.GlobalPersistor);
            }
        }

        public void PersistCurrentProfileItems(IEnumerable<string> keys)
        {
            Dictionary<string, XElement> tempState = SaveItemsToDict(keys, ItemScope.Normal);
            foreach (var pair in tempState)
            {
                _tempProfileStates[pair.Key] = pair.Value;
            }
        }

        public void PersistProfileItems(string profileName, IEnumerable<string> keys)
        {
            Dictionary<string, XElement> result;
            if (keys == null)
            {
                result = SaveItemsToDict(ItemScope.Normal);
            }
            else
            {
                result = SaveItemsToDict(keys, ItemScope.Normal);
            }
            if (result != null && result.Count > 0)
            {
                ProfileService.SaveItemsDictionaryAs(result, profileName, keys);
            }
        }

        public void LoadGlobalItems(IEnumerable<string> keys = null)
        {
            RestoreItemsFromDict(null, ItemScope.GlobalPersistor, keys);
        }


        internal XDocument GetGlobalItem(string key)
        {
            var config =
                ConfigurationManager.GetConfig("MSDesktopGlobalApplicationSettings") as Configurator;

            if (config != null)
            {
                var node = config.GetNode(".", null);
                if (node != null)
                {
                    foreach (XmlNode childNode in node.ChildNodes)
                    {
                        if ((childNode.Attributes != null) &&
                            (childNode.Attributes["Id"] != null) &&
                            (childNode.Attributes["Id"].Value != null))
                        {
                            var child = childNode.FirstChild as XmlElement;
                            if (childNode.Attributes["Id"].Value == key)
                            {
                                return new XDocument(XmlHelper.XmlElementToXElement(child));
                            }
                        }
                    }
                }
            }
            return null;
        }

        public void LoadProfileItems(string profileName, IEnumerable<string> keys)
        {
            ProfileService.LoadProfile(profileName, keys);
        }

        public void LoadCurrentProfileItems(IEnumerable<string> keys)
        {
            var states = new Dictionary<string, XElement>();
            var keysCopied = new List<string>(keys);

            foreach (var key in keys)
            {
                XElement state;
                if (_tempProfileStates.TryGetValue(key, out state))
                {
                    states[key] = state;
                    keysCopied.Remove(key);
                }
            }
            if (states.Count > 0)
            {
                RestoreItemsFromDict(states, ItemScope.Normal, states.Keys);
            }

            if (keysCopied.Count > 0)
            {
                ProfileService.LoadProfile(GetCurrentProfileName(), keysCopied);
            }
        }

        internal XDocument GetCurrentProfileItem(string key)
        {
            if (_tempProfileStates.ContainsKey(key))
            {
                return new XDocument(_tempProfileStates[key]);
            }

            return ProfileService.GetProfileItem(GetCurrentProfileName(), key);
        }

        internal XDocument GetProfileItem(string profileName, string key)
        {
            return ProfileService.GetProfileItem(profileName, key);
        }

        #endregion

        public event EventHandler ProfileProssessingComplete;
        public event EventHandler BeginProfileProcessing;

        #region ItemsDictionaryService Members

        /// <summary>
        /// Calls a save function for all registered persistors.
        /// </summary>
        /// <returns>
        /// A dictionary of saved persistors where a key is persistor's ID
        /// </returns>
        public Dictionary<string, XElement> SaveItemsToDict(ItemScope scope = ItemScope.All)
        {
            //return m_persistableItems.Keys.ToDictionary(itemKey_ => itemKey_, SaveItem);
            //TO FIX TeamCity
            var result = new Dictionary<string, XElement>();
            if (scope == ItemScope.All || scope == ItemScope.Normal)
            {
                foreach (var tempProfileState in _tempProfileStates)
                {
                    result[tempProfileState.Key] = tempProfileState.Value;
                }
                foreach (var itemKey in _persistableItems.Keys)
                {
                    XElement savedItem = SaveItem(itemKey);
                    if (savedItem != null)
                    {
                        result[itemKey] = savedItem;
                    }
                }
            }

            if (scope == ItemScope.All || scope == ItemScope.GlobalPersistor)
            {
                var config =
                    ConfigurationManager.GetConfig("MSDesktopGlobalApplicationSettings") as Configurator;
                if (config != null)
                {
                    foreach (var itemKey in _globalPersistableItems.Keys)
                    {
                        XElement savedItem = SaveItem(itemKey);
                        if (savedItem != null)
                        {
                            if (config.GetNode(string.Format("./Item[@Id='{0}']", itemKey), null) == null)
                                //config.AddNode(string.Format("./Item[@Id='{0}']", itemKey), null,
                                //               XmlHelper.XElementToXmlElement(savedItem));
                                config.AddNode(".", null, XmlHelper.XElementToXmlElement(
                                    new XElement("Item", new XAttribute("Id", itemKey),
                                                 savedItem
                                        )));
                            else
                            {
                                //config.DeleteNode(string.Format("./Item[@Id='{0}']", itemKey), null);
                                //config.SetNode(string.Format("./Item[@Id='{0}']", itemKey), null,
                                //               XmlHelper.XElementToXmlElement(savedItem));
                                config.SetNode(string.Format("./Item[@Id='{0}']", itemKey), null,
                                               XmlHelper.XElementToXmlElement(
                                                   new XElement("Item", new XAttribute("Id", itemKey),
                                                                savedItem
                                                       )));
                            }
                        }
                    }
                    config.Save();
                }
            }
            return result;
        }

        /// <summary>
        /// Calls saving functions for registered persistors with keys from the given set.
        /// </summary>
        /// <param name="keys">keys of persistors to save</param>
        /// <param name="scope"></param>
        /// <returns>A dictionary of saved persistors where a key is persistor's ID</returns>
        public Dictionary<string, XElement> SaveItemsToDict(IEnumerable<string> keys, ItemScope scope = ItemScope.All)
        {
            //return keys_.ToDictionary(itemKey_ => itemKey_, SaveItem);
            //TO FIX TeamCity
            var result = new Dictionary<string, XElement>();
            if (scope == ItemScope.All || scope == ItemScope.Normal)
            {
                foreach (var tempProfileState in _tempProfileStates)
                {
                    result[tempProfileState.Key] = tempProfileState.Value;
                }
                foreach (var itemKey in keys)
                {
                    if (_persistableItems.ContainsKey(itemKey))
                    {
                        if (_globalPersistableItems.ContainsKey(itemKey))
                            continue;

                        var savedItem = SaveItem(itemKey);
                        if (savedItem != null)
                        {
                            result[itemKey] = savedItem;
                        }
                    }
                }
            }

            if (scope == ItemScope.All || scope == ItemScope.GlobalPersistor)
            {
                var config =
                    ConfigurationManager.GetConfig("MSDesktopGlobalApplicationSettings") as Configurator;
                if (config != null)
                {
                    foreach (var itemKey in keys)
                    {
                        if (_globalPersistableItems.ContainsKey(itemKey))
                        {

                            XElement savedItem = SaveItem(itemKey);
                            if (savedItem != null)
                            {
                                if (config.GetNode(string.Format("./Item[@Id='{0}']", itemKey), null) == null)
                                    config.AddNode(".", null, XmlHelper.XElementToXmlElement(
                                        new XElement("Item", new XAttribute("Id", itemKey),
                                                     savedItem
                                            )));
                                else
                                {
                                    //config.DeleteNode(string.Format("./Item[@Id='{0}']", itemKey), null);
                                    config.SetNode(string.Format("./Item[@Id='{0}']", itemKey), null,
                                                   XmlHelper.XElementToXmlElement(
                                                       new XElement("Item", new XAttribute("Id", itemKey),
                                                                    savedItem
                                                           )));
                                }

                            }
                        }
                    }

                    config.Save();
                }
            }
            return result;
        }


        public void RestoreItemsFromDict(IEnumerable<KeyValuePair<string, XElement>> itemsDict, ItemScope scope = ItemScope.All,
                                         IEnumerable<string> keys = null)
        {
            var newApplicationSettingsPersistor = ApplicationOptions.PersistorPrefix + typeof (ApplicationSettings);
            XElement staleappitems = null;

            if (scope == ItemScope.All || scope == ItemScope.Normal)
            {
                foreach (var kvp in itemsDict)
                {
                    var key = kvp.Key;

                    if (keys != null && !keys.Contains(key))
                    {
                        continue;
                    }

                    if (_globalPersistableItems.ContainsKey(key))
                    {
                        continue;
                    }
                    
                    if (key == "MSGui.Internal.ApplicationSettings")
                    {
                        staleappitems = kvp.Value;
                        continue;
                    }

                    RestoreItem(kvp.Value, key);
                }
            }

            if (scope == ItemScope.All || scope == ItemScope.GlobalPersistor)
            {
                var config =
                    ConfigurationManager.GetConfig("MSDesktopGlobalApplicationSettings") as Configurator;

                if (config != null)
                {
                    var node = config.GetNode(".", null);
                    if (node != null)
                    {
                        foreach (XmlNode childNode in node.ChildNodes)
                        {
                            if ((childNode.Attributes != null) &&
                                (childNode.Attributes["Id"] != null) &&
                                (childNode.Attributes["Id"].Value != null))
                            {
                                var child = childNode.FirstChild as XmlElement;
                                var key = childNode.Attributes["Id"].Value;

                                if (keys != null && !keys.Contains(key)) continue;

                                if (_globalPersistableItems.ContainsKey(key))
                                {
                                    RestoreItem(XmlHelper.XmlElementToXElement(child), childNode.Attributes["Id"].Value);
                                }
                                else if (key == "MSGui.Internal.ApplicationSettings")
                                {
                                    RestoreItem(XmlHelper.XmlElementToXElement(child), newApplicationSettingsPersistor);
                                    staleappitems = null;
                                }
                            }
                        }
                    }
                }
            }

            if (staleappitems != null)
            {
                RestoreItem(staleappitems, newApplicationSettingsPersistor);
            }
        }

        #endregion

        private XElement SaveItem(string itemKey)
        {
            XDocument itemState;
            StoreRestorePair pair;
            if (_persistableItems.TryGetValue(itemKey, out pair))
            {
                itemState = pair.Save();
            }
            else
            {
                itemState = _globalPersistableItems[itemKey].Save();
            }

            if (itemState != null)
            {
                return itemState.Root;
            }

            return null;
        }

        internal void RestoreItem(XElement itemElement, string itemKey)
        {
            
            if (itemElement == null && !CallNullPersistence) return;
            StoreRestorePair storagePair;
            if (!_persistableItems.TryGetValue(itemKey, out storagePair))
            {
                if (!_globalPersistableItems.TryGetValue(itemKey, out storagePair))
                {
                    return;
                }
            }

            var doc = itemElement == null ? null : new XDocument(itemElement);
            if (storagePair.AllowBackgroundThreadInvoke)
            {
                storagePair.Restore(doc);
            }
            else
            {
                ProfileService.DoDispatchedAction(() => storagePair.Restore(doc));
            }
        }

        private void InvokeBeginProfileProcessing(object sender, EventArgs args)
        {
            var handler = BeginProfileProcessing;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        private void InvokeProfileProcessingComplete(object sender, EventArgs args)
        {
            var handler = ProfileProssessingComplete;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        private void InvokeProfileSaved(object sender, ProfileSavedEventArgs args)
        {
            var handler = ProfileSaved;
            if (handler != null)
            {
                handler(this, args);
            }
        }

        private struct StoreRestorePair
        {
            public SaveStateHandler Save;
            public RestoreStateHandler Restore;
            public bool AllowBackgroundThreadInvoke;
        }

        public PersistenceProfileService ProfileService
        {
            get { return _profileService; }
            set
            {
                if (_profileService != value)
                {
                    if (_profileService != null)
                    {
                        _profileService.CurrentProfileChanged -= CurrentProfileChanged;
                        _profileService.ProfileProssessingComplete -= InvokeProfileProcessingComplete;
                        _profileService.BeginProfileProcessing -= InvokeBeginProfileProcessing;
                        _profileService.ProfileSaved -= InvokeProfileSaved;
                    }

                    _profileService = value;

                    if (value != null)
                    {
                        value.BeginProfileProcessing += InvokeBeginProfileProcessing;
                        value.ProfileProssessingComplete += InvokeProfileProcessingComplete;
                        value.CurrentProfileChanged += CurrentProfileChanged;
                        value.ProfileSaved += InvokeProfileSaved;
                    }
                }
            }
        }

        private void CurrentProfileChanged(object sender, CurrentProfileChangedEventArgs e)
        {
            _tempProfileStates.Clear();
        }
    }
}
