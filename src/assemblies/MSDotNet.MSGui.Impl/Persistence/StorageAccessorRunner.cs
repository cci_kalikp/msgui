﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Persistence/StorageAccessorRunner.cs#11 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.Collections;
using System.Threading;

namespace MorganStanley.MSDotNet.MSGui.Impl.Persistence
{
  internal class StorageAccessorRunner : IDisposable
  {
    public event EventHandler<ExecutionExceptionEventArgs> ExecutionExceptionHappened;
    private readonly Queue m_workQueue;
    private readonly Queue m_workQueueWrapper;
    private readonly Thread m_thread;
    private volatile bool m_stopping;
    private volatile bool m_async = false;
    
    public StorageAccessorRunner()
    {
      m_workQueue = new Queue();
      m_workQueueWrapper = Queue.Synchronized(m_workQueue);
      m_thread = new Thread(Start)
      {
        Name = "Storage Accessor Thread",
        IsBackground = true
      };
      m_thread.Start();
    }

    internal bool Async
    {
      get
      {
        return m_async;
      }
      set
      {
        m_async = value;
      }
    }

    public void Run(Action action_)
    {
      if (action_ == null)
        throw new ArgumentNullException();
      if (m_async)
      {
        lock (this)
        {
          m_workQueueWrapper.Enqueue(action_);
          Monitor.Pulse(this);
        }
      }
      else
      {
        Invoke(action_);
      }
    }

    private void Start()
    {
      try
      {
        while (!m_stopping)
        {
          Action action = null;

          lock (this)
          {
            if (!m_stopping && m_workQueueWrapper.Count == 0)
            {
              Monitor.Wait(this);
            }

            if (!m_stopping && m_workQueueWrapper.Count > 0)
            {
              action = (Action) m_workQueueWrapper.Dequeue();
            }
          }
          
          if (action != null)
          {
            Invoke(action);
          }

          Thread.Sleep(1);
        }
      }
      catch (ThreadAbortException)
      {
        Stop();
        Thread.ResetAbort();
      }
    }

    private void Invoke(Action action_)
    {
      try
      {
        action_.Invoke();
      }
      catch (Exception exception)
      {
        InvokeExceptionEvent(exception);
      }
    }

    public void Stop()
    {
      m_stopping = true;
    }


    #region IDisposable Members

    ~StorageAccessorRunner()
    {
      InternalDispose();
    }

    public void Dispose()
    {
      InternalDispose();
      GC.SuppressFinalize(this);
    }

    private void InternalDispose()
    {
      lock (this)
      {
        Stop();
        Monitor.PulseAll(this);
      }

      if (m_thread != null)
      {
        m_thread.Join();
      }
    }

    internal void InvokeExceptionEvent(Exception exception_)
    {
      var copy = ExecutionExceptionHappened;
      if (copy != null)
      {
        copy.Invoke(this, new ExecutionExceptionEventArgs(exception_));        
      }
    }
    #endregion    
  
    internal void WaitForEmptyQueue()
    {
        bool empty = false;
        while (!empty)
        {
            lock (this)
            {
                if (m_workQueueWrapper.Count == 0)
                    empty = true;
            }
            Thread.Sleep(100);
        }
    }
  }

  internal class ExecutionExceptionEventArgs : EventArgs
  {
    private Exception m_exception;
    public ExecutionExceptionEventArgs(Exception exception_)
    {
      m_exception = exception_;
    }

    public Exception Exception
    {
      get
      {
        return m_exception;
      }
    }
  }
}
