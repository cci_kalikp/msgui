﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Persistence/CurrentProfileChangedEventArgs.cs#10 $
// $Change: 901195 $
// $DateTime: 2014/10/16 05:40:06 $
// $Author: caijin $

using System;
using System.ComponentModel;

namespace MorganStanley.MSDotNet.MSGui.Impl.Persistence
{
    /// <summary>
    /// Notification that state has loaded.
    /// </summary>
    internal class CurrentProfileChangedEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CurrentProfileChangedEventArgs"/> class
        /// </summary>    
        public CurrentProfileChangedEventArgs(string newProfile_, string oldProfile_)
        {
            NewProfile = newProfile_;
            OldProfile = oldProfile_;
        }

        /// <summary>
        /// New profile.
        /// </summary>
        public string NewProfile { get; private set; }

        /// <summary>
        /// Old profile.
        /// </summary>
        public string OldProfile { get; private set; }
    }
     
    internal class ProfileLoadingEventArgs:CancelEventArgs
    {
        public ProfileLoadingEventArgs(string profileLoaded_, string profileToLoad_)
        {
            ProfileLoaded = profileLoaded_;
            ProfileToLoad = profileToLoad_;
        }

        public string ProfileLoaded { get; private set; }

        public string ProfileToLoad { get; private set; }
    }
    internal class ProfileDeletedEventArgs : EventArgs
    {
        public ProfileDeletedEventArgs(string profileDeleted_)
        {
            ProfileDeleted = profileDeleted_;
        }

        public string ProfileDeleted { get; private set; }
    }

    internal class ProfileRenamedEventArgs : EventArgs
    {
        public ProfileRenamedEventArgs(string oldProfileName_, string newProfileName_)
        {
            OldProfileName = oldProfileName_;
            NewProfileName = newProfileName_;
        }

        public string OldProfileName { get; private set; }

        public string NewProfileName { get; private set; }
    }
}