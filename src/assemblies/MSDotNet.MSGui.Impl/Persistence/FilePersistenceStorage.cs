/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Persistence/FilePersistenceStorage.cs#33 $
// $Change: 887154 $
// $DateTime: 2014/07/03 18:18:27 $
// $Author: hrechkin $

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.My;

namespace MorganStanley.MSDotNet.MSGui.Impl.Persistence
{
    public class FilePersistenceStorage : BaseFilesystemPersistenceStorage, IPersistenceStorage
    {
        private static readonly IMSLogger Logger = MSLoggerFactory.CreateLogger<FilePersistenceStorage>();
        private readonly Regex _filenameRegexp = new Regex("^(.*)\\.state$");
        private const string SAVE_TIME_ATTR_NAME = "SaveTime";
        private const string FILENAME_DOS_PATTERN = "*.state";
        private const string FILENAME_PATTERN = "{0}.state";

        public FilePersistenceStorage(string applicationName) : base(applicationName)
        {
        }

        public FilePersistenceStorage(string applicationName, string presetDirectory)
            : base(applicationName, presetDirectory)
        {
        }

        protected override IEnumerable<string> GetProfilesListFromDirectory(string folder)
        {
            var result = new List<string>();

            if (!Directory.Exists(folder))
            {
                return result;
            }

            var baseDirInfo = new DirectoryInfo(folder);
            FileInfo[] files = baseDirInfo.GetFiles(FILENAME_DOS_PATTERN);

            foreach (FileInfo file in files)
            {
                string profileName = GetProfileNameFromFilename(file.Name);
                if (profileName != null)
                {
                    result.Add(profileName);
                }
            }
            return result;
        }

        protected override DateTime? GetProfileDateTime(string profile_, string folder_)
        {
            var doc = TryLoadState(profile_, folder_);
            if (doc != null && doc.Root != null)
            {
                var attr = doc.Root.Attribute(SAVE_TIME_ATTR_NAME);
                if (attr != null)
                {
                    DateTime result;
                    if (DateTime.TryParseExact(attr.Value, "o", CultureInfo.InvariantCulture, DateTimeStyles.None,
                                               out result))
                    {
                        return result;
                    }
                }
            }
            return null;
        }

        protected override void DeleteState(string profile, string folder)
        {
            File.Delete(Path.Combine(folder, BuildFileName(profile)));
        }

        protected override void RenameState(string oldName, string newName, string folder)
        {
            string oldPath = Path.Combine(folder, BuildFileName(oldName));
            string newPath = Path.Combine(folder, BuildFileName(newName));
            if (File.Exists(newPath))
            {
                File.Delete(newPath);
            }
            File.Move(oldPath, newPath);
        }

        public void SaveState(XDocument state, string name)
        {
            var now = DateTime.Now;
            var nowString = now.ToString("o", CultureInfo.InvariantCulture);
            var root = (new XDocument(state)).Root;
                //creating a new instance of XDocument to make sure that we don't change the original
            if (root != null)
            {
                root.Add(new XAttribute(SAVE_TIME_ATTR_NAME, nowString));
            }

            var baseResult = TrySaveTo(state, name, BaseFolder);
            var altResult = TrySaveTo(state, name, AlternativeFolder);

            RenewProfileNamesList();

            if (!baseResult && !altResult)
            {
                throw new InvalidOperationException(string.Format("Failed to Save profile {0} to any location", name));
            }
        }

        private static bool TrySaveTo(XDocument state, string profile, string folder)
        {
            if (folder != null)
            {
                try
                {
                    state.Save(Path.Combine(folder, BuildFileName(profile)));
                    return true;
                }
                catch (Exception exc)
                {
                    Logger.ErrorWithFormat("Couldn't save profile '{0}' to '{1}'. Error: {2}", profile, folder,
                                             exc.Message);
                }
            }
            return false;
        }

        public XDocument LoadState(string name)
        {
            var folders = OrderToLoad(name);
            foreach (var folder in folders)
            {
                var doc = TryLoadState(name, folder);
                if (doc != null)
                {
                    return doc;
                }
            }
            throw new InvalidOperationException(
                string.Format("Unable to load profile '{0}'. It is not available.", name));
        }

        private static XDocument TryLoadState(string profile, string folder)
        {
            try
            {
                var path = Path.Combine(folder, BuildFileName(profile));
                if (File.Exists(path))
                {
                    var document = XDocument.Load(path);
                    return document;
                }
            }
            catch (Exception exception)
            {
                // state exists and couldn't be loaded for some reason                  
                Logger.ErrorWithFormat(
                    "Unable to load state {0} from the directory {1}. Error: {2}", profile, folder, exception.Message);
            }
            return null;
        }


        #region Unused

        [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
        public void InitializeStorage()
        {
            // TODO:
            //  do nothing
        }


        [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
        public void Apply()
        {
            // TODO:
            //  do nothing
        }

        #endregion

        private string GetProfileNameFromFilename(string filename)
        {
            var m = _filenameRegexp.Match(filename);
            if (m.Success)
            {
                return m.Groups[1].ToString();
            }
            return null;
        }

        private static string BuildFileName(string profileName)
        {
            return string.Format(FILENAME_PATTERN, MakeValidFileName(profileName));
        }
    }
}
