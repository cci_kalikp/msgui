/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Persistence/IProfileMonitor.cs#8 $
// $Change: 898389 $
// $DateTime: 2014/09/25 18:40:14 $
// $Author: hrechkin $

using System;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MorganStanley.MSDotNet.MSGui.Impl.Persistence
{
    internal interface IProfileMonitor
    {
        string CurrentProfile { get; }
        ReadOnlyProfilesCollection AvailableProfiles { get; }
        event EventHandler<CurrentProfileChangedEventArgs> CurrentProfileChanged;
        event EventHandler<ProfileDeletedEventArgs> ProfileDeleted;
        event EventHandler<ProfileRenamedEventArgs> ProfileRenamed;
        event EventHandler<ProfileSavedEventArgs> ProfileSaved;
        event EventHandler<ProfileLoadingEventArgs> ProfileLoading;
        event EventHandler<CurrentProfileChangedEventArgs> ProfileLoaded;
            
       ProfilesCollection CurrentAvailableProfiles { get; }
    }
}