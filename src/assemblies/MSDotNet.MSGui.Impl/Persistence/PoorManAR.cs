﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml.Linq;
using System.Xml;
using System.Linq;
using MorganStanley.MSDotNet.My;

namespace MorganStanley.MSDotNet.MSGui.Impl.Persistence
{
    [Obsolete]
	internal static class PoorManAR
	{
		private static readonly IMSLogger m_logger = MSLoggerFactory.CreateLogger("AssemblyLoading");

		public static void Enable()
		{
			AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(CurrentDomain_AssemblyResolve);
			m_logger.Debug("Assemblyloading enabled");
			try
			{
				XmlDocument doc = new XmlDocument();
				doc.Load(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);
				var node = doc.SelectSingleNode("//loadFromRemoteSources");
				if ((node == null)||(node.Attributes["enabled"] == null)||(node.Attributes["enabled"].Value != "true"))
					m_logger.Critical("loadFromRemoteSources is missing from app.config; might cause assembly loading issues");
			}
			catch(Exception exc)
			{
				m_logger.Debug("Couldn't check loadFromRemoteSources existence", exc);
			}
		}

		private static StringDictionary dict = new StringDictionary
		                                       	{
		                                       		{"Concord.Runtime, Version=2.1.1.0, Culture=neutral, PublicKeyToken=354cc34ba0b81117",
													@"\\san01b\DevAppsGML\dist\concord\PROJ\runtime\2.1.1\assemblies\Concord.Runtime.dll"},
													{"MSDotNet.MSNet, Version=2011.150.1.10, Culture=neutral, PublicKeyToken=d0e341ccaeb444f7",
													@"\\san01b\DevAppsGML\dist\msdotnet\PROJ\dnparts\2014.05.23.1\assemblies\MSDotNet.MSNet.dll"},
													{"Concord.Logger, Version=2.1.0.0, Culture=neutral, PublicKeyToken=354cc34ba0b81117",
													@"\\san01b\DevAppsGML\dist\concord\PROJ\logger\2.1.0\assemblies\Concord.Logger.dll"},
													{"Concord.Logger, Version=2.0.0.29705, Culture=neutral, PublicKeyToken=354cc34ba0b81117",
													@"\\san01b\DevAppsGML\dist\concord\PROJ\logger\2.1.0\assemblies\Concord.Logger.dll"},
													{"MSDotNet.MSLog, Version=2011.150.1.9, Culture=neutral, PublicKeyToken=d0e341ccaeb444f7",
													@"\\san01b\DevAppsGML\dist\msdotnet\PROJ\dnparts\2014.05.23.1\assemblies\MSDotNet.MSLog.dll"},
													{"MSDotNet.MSXml, Version=2014.5.23.1, Culture=neutral, PublicKeyToken=d0e341ccaeb444f7",
													@"\\san01b\DevAppsGML\dist\msdotnet\PROJ\dnparts\2014.05.23.1\assemblies\MSDotNet.MSXml.dll"},
		                                       	};

		private static Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
		{
			if (args.Name.Contains(".resources,"))
				return null;
			if (dict.ContainsKey(args.Name))
			{
				string pathtoload = dict[args.Name];
				return Assembly.LoadFrom(pathtoload);
			}
			return null;
		}
	}
}
