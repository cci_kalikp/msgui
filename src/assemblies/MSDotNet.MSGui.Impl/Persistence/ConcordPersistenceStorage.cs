﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Persistence/ConcordPersistenceStorage.cs#58 $
// $Change: 887154 $
// $DateTime: 2014/07/03 18:18:27 $
// $Author: hrechkin $

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using Microsoft.Practices.Unity;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using MorganStanley.IED.Concord.Configuration;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl;
using MorganStanley.MSDotNet.MSGui.Impl.Shell;
using MorganStanley.MSDotNet.MSNet;
using MorganStanley.MSDotNet.My;
using ConfigurationManager = MorganStanley.IED.Concord.Configuration.ConfigurationManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.Persistence
{
#pragma warning disable 612,618
    public class ConcordPersistenceStorage : IPersistenceStorage, IMultiformatPersistenceStorage
#pragma warning restore 612,618
    {
        private static readonly IMSLogger Logger = MSLoggerFactory.CreateLogger<ConcordPersistenceStorage>();

        #region Constants

// ReSharper disable InconsistentNaming
        private const string LAYOUT_NODE_NAME = "Layout";
        private const string LAYOUT_NAME_ATTRIBUTE_NAME = "name";
        private const string LAYOUT_NODE_FORMAT = LAYOUT_NODE_NAME + "[@" + LAYOUT_NAME_ATTRIBUTE_NAME + "='{0}']";
        private const string WINDOW_MANAGER_NODE_NAME = "WindowManager";
        private const string WINDOW_MANAGER_LAYOUTS_NODE_NAME = "Layouts";

        public const string MSDESKTOP_WINDOW_MANAGER_NAME =
            "MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl.ConcordWindowManager, MSDotNet.MSGui.Impl";

        public const string CONCORD_WINDOW_MANAGER_NAME =
            "MorganStanley.IED.Concord.WindowManager.Pane.PaneWindowManager, Concord.PaneWindowManager";

        public const string DEFAULT_LAYOUT_NODE_NAME = "DefaultLayout";
        public const string EXPORTED_NODE_NAME = "Exported";
        public const string DEFAULT_DEFAULT_LAYOUT_NAME = "Default";
// ReSharper restore InconsistentNaming

        #endregion

        #region Members

        internal struct LayoutMetadata
        {
            internal string Name { get; set; }
            internal string WindowManager { get; set; }
            internal string ConfigName { get; set; }
        }

        internal struct WindowManagerMetadata
        {
            internal string Name { get; set; }
            internal string ConfigName { get; set; }
        }

        /// <summary>
        /// List of supported Rambo ConfigNames in descending order of preference.
        /// </summary>
        private readonly List<string> _configNames = new List<string>
            {
                // This contains layouts in MSDesktop format
                "MSDesktopLayouts", 

                // This contains layouts in the old Concord format
                "Layouts"
            };

        private readonly Dictionary<string, LayoutMetadata> _layouts = new Dictionary<string, LayoutMetadata>();

        //TODO: supportedWindowManagers should not be hardcoded.
        /// <summary>
        /// List of supported window managers and the corresponding Rambo ConfigNames.
        /// </summary>
        //private readonly List<WindowManagerMetadata> supportedWindowManagers = new List<WindowManagerMetadata>()
        //{
        //    new WindowManagerMetadata() { Name = MSDESKTOP_WINDOW_MANAGER_NAME, ConfigName = "MSDesktopLayouts"},
        //    new WindowManagerMetadata() { Name = CONCORD_WINDOW_MANAGER_NAME, ConfigName = "Layouts"},
        //};
        private readonly Dictionary<string, string> _supportedWindowManagers = new Dictionary<string, string>
            {
                {MSDESKTOP_WINDOW_MANAGER_NAME, "MSDesktopLayouts"},
                {CONCORD_WINDOW_MANAGER_NAME, "Layouts"}
            };

        private readonly ProfilesCollection _profiles = new ProfilesCollection();
        private ReadOnlyProfilesCollection _profilesReadOnly;
        private readonly string _defaultProfile;

        internal string ApplicationName { get; set; }
        internal string ApplicationVersion { get; set; }

        #endregion

        private string _defaultName;

        #region utilities

        private static string WindowManagerToXPath(LayoutMetadata layoutMetadata)
        {
            return string.Format(WINDOW_MANAGER_NODE_NAME + "[@type='" + layoutMetadata.WindowManager + "']/" +
                                 WINDOW_MANAGER_LAYOUTS_NODE_NAME + "/" + LAYOUT_NODE_FORMAT, layoutMetadata.Name);
        }

        /// <summary>
        /// Functions that call LoadLayout expect to receive State node. 
        /// This method wraps layouts in State & Item & Exported nodes.
        /// </summary>
        /// <param name="layout"></param>
        /// <param name="kindOfLayout"></param>
        /// <returns></returns>
        private static XElement GenerateState(XElement layout, string kindOfLayout)
        {
            return new XElement("State",
                                new XElement("Item",
                                             new XAttribute("Id", "MSGui.Internal.ViewManager"),
                                             new XElement(EXPORTED_NODE_NAME,
                                                          new XAttribute("type", kindOfLayout),
                                                          layout.Elements())),
                                new XElement("Item",
                                             new XAttribute("Id", "MSGui.Internal.Ribbon"),
                                             new XElement("Ribbon",
                                                          new XAttribute("Minimized", "true"))
                                    ));
        }

        private static string BuildXPath(XmlNode node)
        {
            string result = string.Empty;
            result += node.Name;
            if (node.Attributes != null)
            {
                if (node.Attributes.Count > 0)
                {
                    result += "[";
                }
                return node.Attributes.OfType<XmlAttribute>()
                           .Aggregate(result,
                                      (current, attr) => current + string.Format("@{0}='{1}']", attr.Name, attr.Value));
            }

            return result;
        }

        #endregion

        #region Ctor

        public ConcordPersistenceStorage()
            : this(null)
        {

        }

        public ConcordPersistenceStorage(string concordDefaultProfile)
        {
            // to initialize Readonly Profiles, since we cannot do the initialization 
            // in member declaration as profiels is not static field
            _profilesReadOnly = new ReadOnlyProfilesCollection(_profiles);

            _defaultProfile = concordDefaultProfile;
        }

        internal ConcordPersistenceStorage(bool dummy) //for testing
        {
        }

        #endregion

        internal void InitLayouts()
        {
            var defaultLayouts = new Dictionary<string, string>();
            //initializing all to null to avoid later key contain checks
            foreach (var wm in _supportedWindowManagers.Keys)
            {
                defaultLayouts[wm] = null;
            }

            for (var i = _configNames.Count - 1; i >= 0; i--)
            {
                var config = (Configurator) ConfigurationManager.GetConfig(_configNames[i]);

                var fullXml = config.GetNode(".", null);

                if (fullXml == null)
                    continue;

                foreach (XmlNode xmlNode in fullXml.ChildNodes)
                {
                    if (xmlNode != null && xmlNode.Name == WINDOW_MANAGER_NODE_NAME && xmlNode.Attributes != null &&
                        xmlNode.Attributes["type"] != null)
                    {
                        var windowManagerName = xmlNode.Attributes["type"].Value;

                        if (_supportedWindowManagers.ContainsKey(windowManagerName))
                        {
                            string supposedDefaultLayout = InitLayoutsForWindowManager(xmlNode, windowManagerName,
                                                                                       _configNames[i]);
                            if (defaultLayouts[windowManagerName] == null)
                                defaultLayouts[windowManagerName] = supposedDefaultLayout ??
                                                                    defaultLayouts[windowManagerName];
                        }
                        else
                        {
                            Logger.WarningWithFormat("Not supported window manager: {0}", windowManagerName);
                        }
                    }
                    else
                    {
                        if (xmlNode != null)
                            Logger.WarningWithFormat("Not supported node: {0}", xmlNode.OuterXml);
                    }
                }
            }

            _defaultName = defaultLayouts[MSDESKTOP_WINDOW_MANAGER_NAME] ??
                           defaultLayouts[CONCORD_WINDOW_MANAGER_NAME] ?? DEFAULT_DEFAULT_LAYOUT_NAME;
            _profilesReadOnly = new ReadOnlyProfilesCollection(_profiles);
        }

        private string InitLayoutsForWindowManager(XmlNode xmlNode, string windowManagerName, string configName)
        {
            string defaultName = null;

            var node = xmlNode as XmlElement;
            if (node != null)
            {
                var wmElement = XmlHelper.XmlElementToXElement(node);
                var layoutsElement = wmElement.Element(WINDOW_MANAGER_LAYOUTS_NODE_NAME);
                var defaultLayoutElement = wmElement.Element(DEFAULT_LAYOUT_NODE_NAME);
                if (defaultLayoutElement != null && !string.IsNullOrEmpty(defaultLayoutElement.Value))
                {
                    defaultName = defaultLayoutElement.Value; //extracted a name but haven't added a prefix yet
                }

                if (layoutsElement != null)
                {
                    foreach (var layoutElement in layoutsElement.Elements(LAYOUT_NODE_NAME))
                    {
                        XAttribute nameAttr = layoutElement.Attribute(LAYOUT_NAME_ATTRIBUTE_NAME);
                        if (nameAttr != null && !string.IsNullOrEmpty(nameAttr.Value))
                        {
                            var layoutName = nameAttr.Value;
                            var displayLayoutName = layoutName;

                            //to prevent collisions
                            while (_layouts.ContainsKey(displayLayoutName) &&
                                   _layouts[displayLayoutName].ConfigName == configName &&
                                   _layouts[displayLayoutName].WindowManager == windowManagerName)
                            {
                                displayLayoutName += "New";
                            }

                            if (layoutName == defaultName)
                            {
                                defaultName = displayLayoutName;
                            }

                            var layoutMetadata = new LayoutMetadata
                                {
                                    Name = layoutName,
                                    WindowManager = windowManagerName,
                                    ConfigName = configName
                                };

                            if (_layouts.ContainsKey(displayLayoutName))
                                _layouts[displayLayoutName] = layoutMetadata;
                            else
                            {
                                _layouts.Add(displayLayoutName, layoutMetadata);
                                _profiles.Add(displayLayoutName);
                            }
                        }
                    }
                }
            }
            return defaultName;
        }

        public void DeleteState(string name)
        {
            if (_layouts.ContainsKey(name))
            {
                var layoutMetadata = _layouts[name];

                if (_profiles.Contains(name))
                {
                    _profiles.Remove(name);
                }

                if (_layouts.ContainsKey(name))
                {
                    _layouts.Remove(name);
                }

                var config = (Configurator) ConfigurationManager.GetConfig(layoutMetadata.ConfigName);
                config.DeleteNode(WindowManagerToXPath(layoutMetadata), null);
                config.Save();
            }
        }


        public XDocument LoadState(string name)
        {
            if (_layouts.ContainsKey(name))
            {
                Extensions.ConcordExtensionStorage.ViewContainerByWindowGuidStore.Clear();
                var layoutMetadata = _layouts[name];

                var config = (Configurator) ConfigurationManager.GetConfig(layoutMetadata.ConfigName);
                var node = config.GetNode(WindowManagerToXPath(layoutMetadata), null);
                if (node == null || node.ChildNodes.Count < 1)
                {
                    return null;
                }

                var element = node as XmlElement;
                if (element != null)
                {
                    var layoutElement = XmlHelper.XmlElementToXElement(element);

                    switch (layoutMetadata.WindowManager)
                    {
                        case MSDESKTOP_WINDOW_MANAGER_NAME:
                            foreach (XElement child in layoutElement.Elements())
                            {
                                //getting first child
                                return new XDocument(child);
                            }
                            break;
                        case CONCORD_WINDOW_MANAGER_NAME:
                            return new XDocument(GenerateState(layoutElement, CONCORD_WINDOW_MANAGER_NAME));
                    }
                }
            }
            return null;
        }

        public void SaveState(XDocument state, string name)
        {
            const string windowManager = MSDESKTOP_WINDOW_MANAGER_NAME;
            var stateElement = state.Root;
            string nodePath;

            if (_layouts.ContainsKey(name))
            {
                if (_defaultName == name)
                {
                    SetDefaultLayoutForWindowManager(new LayoutMetadata
                        {
                            Name = name,
                            WindowManager = MSDESKTOP_WINDOW_MANAGER_NAME,
                            ConfigName = _supportedWindowManagers[MSDESKTOP_WINDOW_MANAGER_NAME]
                        }, false);
                }

                _layouts[name] = new LayoutMetadata
                    {
                        Name = _layouts[name].Name,
                        WindowManager = MSDESKTOP_WINDOW_MANAGER_NAME,
                        ConfigName = _supportedWindowManagers[MSDESKTOP_WINDOW_MANAGER_NAME]
                    };

                nodePath = WindowManagerToXPath(_layouts[name]);
            }
            else
            {
                nodePath = WindowManagerToXPath(new LayoutMetadata
                    {
                        Name = name,
                        WindowManager = MSDESKTOP_WINDOW_MANAGER_NAME,
                        ConfigName = _supportedWindowManagers[MSDESKTOP_WINDOW_MANAGER_NAME]
                    });
            }

            Save(name, stateElement, nodePath, name, windowManager);
        }

        public void SaveStateInFormat(XDocument state, string name, string format, string displayName)
        {
            if (!SupportedWindowManagers.Contains(format))
            {
                throw new UnsupportedLayoutFormatException(string.Format("format {0} is not supported", format));
            }

            var stateElement = state.Root;
            var nodePath =
                WindowManagerToXPath(new LayoutMetadata
                    {
                        Name = name,
                        WindowManager = format,
                        ConfigName = _supportedWindowManagers[format]
                    });

            switch (format)
            {
                case MSDESKTOP_WINDOW_MANAGER_NAME:
                    //do nothing
                    break;
                case CONCORD_WINDOW_MANAGER_NAME:
                    stateElement = LayoutConverter.StateToPaneWindowManagerLayout(stateElement);
                    stateElement.Add(new XAttribute(LAYOUT_NAME_ATTRIBUTE_NAME, name));
                    break;
            }

            Save(name, stateElement, nodePath, displayName, format);
        }


        private void Save(string name, XElement stateElement, string nodePath, string displayName, string windowManager)
        {
            var config = (Configurator) ConfigurationManager.GetConfig(_supportedWindowManagers[windowManager]);

            XmlNode node = config.GetNode(nodePath, null);
            if (node != null)
            {
                try
                {
                    config.DeleteNode(nodePath, null);
                }
// ReSharper disable EmptyGeneralCatchClause
                catch (Exception)
// ReSharper restore EmptyGeneralCatchClause
                {
                    // To fix the empty node issue in msxml
                }
            }

            var state = XmlHelper.XElementToXmlElement(stateElement);
            if (stateElement.Name == LAYOUT_NODE_NAME)
            {
                config.SetNode(nodePath, null, state);
            }
            else
            {
                config.SetNode(nodePath + "/" + BuildXPath(state), null, state);
            }

            config.Save();
            if (!_profiles.Contains(displayName))
            {
                _profiles.Add(displayName);
            }

            //TODO collisions
            _layouts[displayName] = new LayoutMetadata
                {
                    Name = name,
                    WindowManager = windowManager,
                    ConfigName = _supportedWindowManagers[windowManager]
                };

            _profilesReadOnly = new ReadOnlyProfilesCollection(_profiles);
        }

        public void RenameState(string oldName, string newName)
        {
            if (_layouts.ContainsKey(oldName))
            {
                var layout = _layouts[oldName];
                var config = (Configurator) ConfigurationManager.GetConfig(layout.ConfigName);
                var oldNodePath = WindowManagerToXPath(_layouts[oldName]);
                var newMetadata = new LayoutMetadata
                    {
                        Name = newName,
                        WindowManager = _layouts[oldName].WindowManager,
                        ConfigName = _layouts[oldName].ConfigName
                    };

                var newNodePath = WindowManagerToXPath(newMetadata);


                var node = config.GetNode(oldNodePath, null);
                if (node == null || node.ChildNodes.Count == 0)
                {
                    //no such state to rename
                    return;
                }

                config.DeleteNode(oldNodePath, null);
                config.SetNode(string.Format(newNodePath + "/" + BuildXPath(node.FirstChild)), null, node.FirstChild);
                config.Save();

                if (_profiles.Contains(oldName))
                {
                    _profiles.Remove(oldName);
                }

                _profiles.Add(newName);

                if (_layouts.ContainsKey(oldName))
                {
                    _layouts[newName] = newMetadata;
                    _layouts.Remove(oldName);
                }
                _profilesReadOnly = new ReadOnlyProfilesCollection(_profiles);
            }
        }

        public static bool ConcordConfigurationManagerInitialized { get; internal set; }

        public void InitializeStorage()
        {
            if (Container != null)
            {
                var loop = MSNetLoopWithAffinityBase.GetLoop() ??
                           new MSNetLoopDispatcherImpl(Container.Resolve<IShell>().MainWindow);
            }
            if (!ConcordConfigurationManagerInitialized)
            {
                // TODO shall we allow to create ConcordPersistenceStorage with a different set of arguments?
                // if we are enabling concord applets support, another instance of Arguments is created in IConcordApplication.Run
                if (ApplicationName != null)
                {
                    ConcordConfigurationManager.ApplicationName = ApplicationName;
                }

                ConcordConfigurationManager.Initialize(new Arguments(Environment.GetCommandLineArgs()), _defaultProfile);
                CurrentConcordProfile = ConcordConfigurationManager.CurrentProfile;
            }
        }

        public void Apply()
        {
            if (ConcordConfigurationManagerInitialized)
            {
                InitLayouts();
            }
        }

        public string DefaultName
        {
            get { return _defaultName; }
            set
            {
                _defaultName = value;

                LayoutMetadata layoutMetadata;
                if (!_layouts.TryGetValue(_defaultName, out layoutMetadata))
                {
                    layoutMetadata = new LayoutMetadata
                        {
                            Name = _defaultName,
                            WindowManager = MSDESKTOP_WINDOW_MANAGER_NAME,
                            ConfigName = _supportedWindowManagers[MSDESKTOP_WINDOW_MANAGER_NAME]
                        };
                }

                SetDefaultLayoutForWindowManager(layoutMetadata);
            }
        }

        private void SetDefaultLayoutForWindowManager(LayoutMetadata meta, bool save = true)
        {
            if (meta.WindowManager != MSDESKTOP_WINDOW_MANAGER_NAME)
            {
                if (TaskDialog.ShowMessage("The layout you selected is not in the new MSDesktop format. " +
                                           "Setting it as default will result in automatic conversion that can not be reverted. " +
                                           "Do you want to proceed?", "Layout conversion", TaskDialogCommonButtons.YesNo)
                    == TaskDialogSimpleResult.Yes)
                {
                    SaveState(LoadState(meta.Name), meta.Name);
                    meta.WindowManager = MSDESKTOP_WINDOW_MANAGER_NAME;
                    meta.ConfigName = "MSDesktopLayouts";

                    Configurator config = (Configurator) ConfigurationManager.GetConfig(meta.ConfigName);

                    string xpath = WINDOW_MANAGER_NODE_NAME + "[@type='" + meta.WindowManager + "']/" +
                                   DEFAULT_LAYOUT_NODE_NAME;
                    config.SetNode(xpath, null,
                                   XmlHelper.XElementToXmlElement(new XElement(DEFAULT_LAYOUT_NODE_NAME, meta.Name)));
                    if (save)
                    {
                        config.Save();
                    }
                }
            }
            else
            {
                Configurator config = (Configurator) ConfigurationManager.GetConfig(meta.ConfigName);

                string xpath = WINDOW_MANAGER_NODE_NAME + "[@type='" + meta.WindowManager + "']/" +
                               DEFAULT_LAYOUT_NODE_NAME;
                config.SetNode(xpath, null,
                               XmlHelper.XElementToXmlElement(new XElement(DEFAULT_LAYOUT_NODE_NAME, meta.Name)));
                if (save)
                {
                    config.Save();
                }
            }
        }

        public ReadOnlyProfilesCollection AvailableProfiles
        {
            get { return _profilesReadOnly; }
        }

        public string GetWindowManager(string displayName)
        {
            if (displayName != null && _layouts.ContainsKey(displayName))
            {
                return _layouts[displayName].WindowManager;
            }
            return MSDESKTOP_WINDOW_MANAGER_NAME;
        }

        public string GetSuffix(string windowManager)
        {
            return string.Empty;
        }

        /// <summary>
        /// Get human-readable name of the format for the current window manager
        /// </summary>
        /// <param name="windowManager"></param>
        /// <returns></returns>
        public string GetFormatName(string windowManager)
        {
            switch (windowManager)
            {
                case MSDESKTOP_WINDOW_MANAGER_NAME:
                    return "Default Format";
                case CONCORD_WINDOW_MANAGER_NAME:
                    return "Concord.PaneWindowManager";
                default:
                    return string.Empty;
            }
        }

        public ICollection<string> SupportedWindowManagers
        {
            get { return _supportedWindowManagers.Keys; }
        }

        protected internal static IUnityContainer Container { get; set; }
        protected string CurrentConcordProfile { get; private set; }

    }
}
