﻿////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Persistence/LayoutConverter.cs#24 $
// $Change: 896821 $
// $DateTime: 2014/09/15 02:20:20 $
// $Author: caijin $

using System;
using System.Collections.Generic;
using System.Linq; 
using System.Text.RegularExpressions;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;

namespace MorganStanley.MSDotNet.MSGui.Impl.Persistence
{
    internal static class LayoutConverter
    {

        public const string DockLayoutRootName = "XamDockManagerLayout";
        public const string HarmoniaDockLayoutRootName = "HarmoniaDockManagerLayout";
        public const string DockManagerRootName = "xamDockManager";
        public const string DockManagerVersion = "14.1.20141.2062";

        public static XElement PaneWindowManagerToTabbedDock(XElement paneWindowManagerLayout_)
        {
            Layout oldlayout = Layout.FromXml(paneWindowManagerLayout_);
            TabbedDockRepr tdr = TabbedDockRepr.FromLayout(oldlayout); 
            return TabbedDockRepr.ToXml(tdr);
        }

        public static XElement TabbedDockToPaneWindowManager(XElement tabbedDockLayout_)
        {
            TabbedDockRepr tdr = TabbedDockRepr.FromXml(tabbedDockLayout_);
            Layout layout = Layout.FromTabbedDock(tdr);
            return Layout.ToXml(layout);
        }

        public static XElement StateToPaneWindowManagerLayout(XElement state_)
        {
            XElement viewManagerItem =
                state_.Elements("Item")
                      .First(itemElement_ => TryGetAttribute(itemElement_, "Id") == "MSGui.Internal.ViewManager");
            XElement tdElement = viewManagerItem.Element(TabbedDockViewModel.NODE_NAME);
            if (tdElement != null)
            {
                return TabbedDockToPaneWindowManager(tdElement);
            }
            return null;
        }

        public static string TryGetAttribute(XElement elem_, string name_)
        {
            XAttribute attr = elem_.Attribute(name_);
            return attr == null ? null : attr.Value;
        }

        public static void TrySetAttribute(XElement elem_, string name_, string content_)
        {
            if (content_ != null)
            {
                elem_.Add(new XAttribute(name_, content_));
            }
        }

        internal static class LayoutStringConstants
        {
            public const string ContentPanePrefix = "contentPane";
        }

        //-------------------------------------
        internal class Layout
        {
            public string Name { get; set; }
            public TabSplitter Splitter { get; set; }
            private readonly List<XElement> m_windows = new List<XElement>();

            public List<XElement> Windows
            {
                get { return m_windows; }
            }

            public static Layout FromXml(XElement layout_)
            {
                if (layout_.Name != "Layout")
                {
                    throw new UnsupportedLayoutFormatException("Root name must be Layout");
                }
                Layout result = new Layout();
                result.Name = TryGetAttribute(layout_, "name");

                XElement tsElement = layout_.Element("TabSplitter");
                if (tsElement == null)
                {
                    throw new UnsupportedLayoutFormatException("TabSplitter must exist");
                }

                TabSplitter ts = new TabSplitter();
                result.Splitter = ts;
                ts.SelectedTab = TryGetAttribute(tsElement, "selectedTab");

                IEnumerable<XElement> tabElements = tsElement.Elements("Tab");
                if (tabElements != null)
                {
                    foreach (XElement tabElement in tabElements)
                    {
                        ts.Tabs.Add(Tab.FromXml(tabElement));
                    }
                }
                XElement windowsElement = layout_.Element("Windows");
                if (windowsElement != null)
                {
                    foreach (var element in windowsElement.Elements("Window"))
                    {
                        result.Windows.Add(element);
                    }

                }
                return result;
            }

            public static XElement ToXml(Layout layout_)
            {
                var result = new XElement("Layout");
                TrySetAttribute(result, "name", layout_.Name);
                if (layout_.Splitter != null)
                {
                    result.Add(TabSplitter.ToXml(layout_.Splitter));
                }
                if (layout_.Windows != null && layout_.Windows.Count > 0)
                {
                    var windowsElement = new XElement("Windows");
                    result.Add(windowsElement);
                    foreach (XElement windowElement in layout_.Windows)
                    {
                        windowsElement.Add(windowElement);
                    }
                }
                return result;
            }

            public static Layout FromTabbedDock(TabbedDockRepr tdr_)
            {
                Layout result = new Layout();
                //TODO name
                //TODO tabSplitter
                foreach (ViewRepr pane in tdr_.Panes)
                {
                    if (pane.ViewContainerType == "Concord") //old format does not support non-concord windows
                    {
                        XElement windowElement = ViewRepr.ToWindow(pane);
                        if (windowElement != null)
                        {
                            result.Windows.Add(windowElement);
                        }
                    }
                }

                if (tdr_.Tabs != null && tdr_.Tabs.Count > 0)
                {
                    result.Splitter = new TabSplitter();
                    int tabcounter = 1;
                    foreach (TabRepr tab in tdr_.Tabs)
                    {
                        if (tab.IsActive == "true")
                        {
                            result.Splitter.SelectedTab = tab.Name;
                        }
                        Tab newtab = new Tab() {Caption = tab.Name, Index = tabcounter};
                        result.Splitter.Tabs.Add(newtab);
                        tabcounter++;

                        Dictionary<string, ViewRepr> paneDict = tab.Panes.ToDictionary(pane_ => pane_.Id);

                        if (tdr_.Xdm != null)
                        {
                            List<XdmSplitPane> nonFloatingSplitPanes =
                                tdr_.Xdm.Panes.Where(splitPane => splitPane.Location != "Floating").ToList();

                            if (nonFloatingSplitPanes.Count == 1)
                            {
                                newtab.Child = ChildPane.FromXdmChild(nonFloatingSplitPanes[0], paneDict);
                            }
                            else if (nonFloatingSplitPanes.Count > 1)
                            {
                                SyncSplitPane splitPane = new SyncSplitPane {MainPaneArea = "0.5"};
                                newtab.Child = splitPane;
                                int i = nonFloatingSplitPanes.Count;
                                foreach (XdmSplitPane sp in nonFloatingSplitPanes)
                                {
                                    ChildPane childPane = ChildPane.FromXdmChild(sp, paneDict);
                                    if (splitPane.SplitDirection == null)
                                    {
                                        switch (sp.Location)
                                        {
                                            case "DockedTop":
                                            case "DockedBottom":
                                                splitPane.SplitDirection = "Top";
                                                break;
                                            case "DockedLeft":
                                            case "DockedRight":
                                                splitPane.SplitDirection = "Left";
                                                break;
                                            default:
                                                splitPane.SplitDirection = "Left";
                                                break;
                                        }
                                    }

                                    if (splitPane.Child1 == null)
                                    {
                                        splitPane.Child1 = childPane;
                                    }
                                    else
                                    {
                                        if (i == 1)
                                        {
                                            splitPane.Child2 = childPane;
                                        }
                                        else
                                        {
                                            SyncSplitPane newSp = new SyncSplitPane
                                                {
                                                    MainPaneArea = "0.5",
                                                    Child1 = childPane
                                                };
                                            splitPane.Child2 = newSp;
                                            splitPane = newSp;
                                        }
                                    }
                                    i--;
                                }
                            }

                        }
                        else
                        {
                            foreach (var viewReprPair in paneDict)
                            {
                                var windowElem = ViewRepr.ToWindow(viewReprPair.Value);
                                if (windowElem != null)
                                {
                                    result.Windows.Add(windowElem);
                                }
                            }
                        }
                    }
                }

                return result;
            }
        }

        internal class TabSplitter
        {
            private readonly List<Tab> m_tabs = new List<Tab>();
            public string SelectedTab { get; set; }

            public List<Tab> Tabs
            {
                get { return m_tabs; }
            }

            public static XElement ToXml(TabSplitter tabSplitter_)
            {
                var result = new XElement("TabSplitter");
                TrySetAttribute(result, "selectedTab", tabSplitter_.SelectedTab);
                foreach (Tab tab in tabSplitter_.Tabs)
                {
                    result.Add(Tab.ToXml(tab));
                }
                return result;
            }
        }

        internal abstract class ChildPane
        {
            internal abstract int Width { get; }
            internal abstract int Height { get; }
            internal double RelativeWidth { get; set; }
            internal double RelativeHeight { get; set; }



            protected ChildPane()
            {
                RelativeWidth = 100.0;
                RelativeHeight = 100.0;
            }

            public static ChildPane FromXml(XElement element_)
            {
                if (element_.Name == "Pane")
                {
                    return Pane.FromXml(element_);
                }
                if (element_.Name == "SplitPane")
                {
                    SyncSplitPane result = new SyncSplitPane()
                        {
                            MainPaneArea = LayoutConverter.TryGetAttribute(element_, "MainPaneArea"),
                            SplitDirection = LayoutConverter.TryGetAttribute(element_, "SplitDirection"),
                        };
                    int counter = 1;
                    foreach (var paneChildElement in element_.Elements())
                    {
                        if (counter == 1)
                        {
                            result.Child1 = ChildPane.FromXml(paneChildElement);
                        }
                        else if (counter == 2)
                        {
                            result.Child2 = ChildPane.FromXml(paneChildElement);

                            switch (result.SplitDirection)
                            {
                                case "Left":
                                case "Right":
                                    result.Child1.RelativeWidth = result.Child1.Width;
                                    result.Child2.RelativeWidth = result.Child2.Width;

                                    break;
                                case "Top":
                                case "Bottom":
                                    result.Child1.RelativeHeight = result.Child1.Height;
                                    result.Child2.RelativeHeight = result.Child2.Height;
                                    break;
                            }
                        }
                        else
                        {
                            break;
                        }
                        counter++;
                    }
                    return result;
                }
                throw new UnsupportedLayoutFormatException();
            }

            public static XElement ToXml(ChildPane child_)
            {
                if (child_ is Pane)
                {
                    return Pane.ToXml((Pane) child_);
                }
                if (child_ is SyncSplitPane)
                {
                    return SyncSplitPane.ToXml((SyncSplitPane) child_);
                }
                throw new UnsupportedLayoutFormatException();
            }

            public static ChildPane FromXdmChild(XdmChildPane xdmChildPane_, Dictionary<string, ViewRepr> paneDict_)
            {
                if (xdmChildPane_ is XdmContentPane)
                {
                    return Pane.FromXdmContentPane((XdmContentPane) xdmChildPane_, paneDict_);
                }
                if (xdmChildPane_ is XdmSplitPane && ((XdmSplitPane) xdmChildPane_).Location != "Floating")
                {
                    var spane = SyncSplitPane.FromXdmSplitPane((XdmSplitPane) xdmChildPane_, paneDict_);
                    if (spane.Child1 == null ^ spane.Child2 == null)
                    {
                        return spane.Child1 ?? spane.Child2;
                    }
                    return spane;
                }
                throw new UnsupportedLayoutFormatException();
            }
        }

        internal class Pane : ChildPane
        {
            public XElement Content { get; set; }
            public string Name { get; set; }
            public string Title { get; set; }


            internal int PaneWidth { private get; set; }

            internal int PaneHeight { private get; set; }

            public new static Pane FromXml(XElement element_)
            {
                if (element_.Name != "Pane")
                {
                    throw new UnsupportedLayoutFormatException();
                }
                Pane result = new Pane()
                    {
                        Title = TryGetAttribute(element_, "Title"),
                        Name = TryGetAttribute(element_, "Name")
                    };

                foreach (var paneChildElement in element_.Elements())
                {
                    result.Content = paneChildElement;
                    result.PaneWidth = (from we in paneChildElement.Elements("Bounds")
                                        from val in we.Attributes("width")
                                        select int.Parse(val.Value)).FirstOrDefault();

                    result.PaneHeight = (from we in paneChildElement.Elements("Bounds")
                                         from val in we.Attributes("height")
                                         select int.Parse(val.Value)).FirstOrDefault();
                    break;
                }

                return result;
            }


            public new static XElement ToXml(Pane pane_)
            {
                var result = new XElement("Pane");
                TrySetAttribute(result, "Title", pane_.Title);
                TrySetAttribute(result, "Name", pane_.Name);
                if (pane_.Content != null)
                {
                    result.Add(pane_.Content);
                }
                return result;
            }

            public static Pane FromXdmContentPane(XdmContentPane xdmChildPane_, Dictionary<string, ViewRepr> paneDict_)
            {
                var result = new Pane();
                string name = xdmChildPane_.Name;
                if (!string.IsNullOrEmpty(xdmChildPane_.Name) && xdmChildPane_.Name.StartsWith(LayoutStringConstants.ContentPanePrefix))
                {
                    name = xdmChildPane_.Name.Substring(LayoutStringConstants.ContentPanePrefix.Length);
                }
                ViewRepr view = null;
                paneDict_.TryGetValue(name, out view);
                if (view != null)
                {
                    result.Name = view.Id;
                    result.Title = view.Title;
                    if (view.Content != null && view.Content.Name == "ConcordWindow")
                    {
                        result.Content = view.Content.Element("ConcordForm");
                    }
                    return result;
                }
                return null;
            }

            #region Overrides of ChildPane

            internal override int Width
            {
                get { return PaneWidth; }
            }

            internal override int Height
            {
                get { return PaneHeight; }
            }

            #endregion
        }

        internal class SyncSplitPane : ChildPane
        {
            public ChildPane Child1 { get; set; }
            public ChildPane Child2 { get; set; }
            public string SplitDirection { get; set; }
            public string MainPaneArea { get; set; }



            public new static XElement ToXml(SyncSplitPane splitPane_)
            {
                var result = new XElement("SplitPane");
                TrySetAttribute(result, "SplitDirection", splitPane_.SplitDirection);
                TrySetAttribute(result, "MainPaneArea", splitPane_.MainPaneArea);
                if (splitPane_.Child1 != null)
                {
                    result.Add(ChildPane.ToXml(splitPane_.Child1));
                }
                if (splitPane_.Child2 != null)
                {
                    result.Add(ChildPane.ToXml(splitPane_.Child2));
                }
                return result;
            }

            public static SyncSplitPane FromXdmSplitPane(XdmSplitPane splitPane_, Dictionary<string, ViewRepr> paneDict_)
            {
                SyncSplitPane splitPane = new SyncSplitPane() {MainPaneArea = "0.5"};
                SyncSplitPane result = splitPane;

                string splitDirection = splitPane_.SplitterOrientation == "Vertical" ? "Left" : "Top";
                result.SplitDirection = splitDirection;

                int i = splitPane_.Children.Count;
                foreach (XdmChildPane child in splitPane_.Children)
                {
                    if (child != null)
                    {
                        ChildPane childPane = ChildPane.FromXdmChild(child, paneDict_);
                        if (splitPane.Child1 == null)
                        {
                            splitPane.Child1 = childPane;
                        }
                        else
                        {
                            if (i == 1)
                            {
                                splitPane.Child2 = childPane;
                            }
                            else
                            {
                                SyncSplitPane newSp = new SyncSplitPane
                                    {
                                        MainPaneArea = "0.5",
                                        SplitDirection = splitDirection,
                                        Child1 = childPane
                                    };
                                splitPane.Child2 = newSp;
                                splitPane = newSp;
                            }
                        }
                    }
                    i--;
                }

                return result;
            }

            #region Overrides of ChildPane

            internal override int Width
            {
                get
                {
                    switch (this.SplitDirection)
                    {
                        case "Left":
                        case "Right":
                            return Child1.Width + Child2.Width;
                        case "Top":
                        case "Bottom":
                            return Math.Max(Child1.Width, Child2.Width);
                    }
                    throw new Exception("Not recognized direction " + SplitDirection);
                }
            }

            internal override int Height
            {
                get
                {
                    switch (this.SplitDirection)
                    {
                        case "Left":
                        case "Right":
                            return Math.Max(Child1.Height, Child2.Height);
                        case "Top":
                        case "Bottom":
                            return Child1.Height + Child2.Height;

                    }
                    throw new Exception("Not recognized direction" + SplitDirection);
                }
            }

            #endregion
        }

        internal class Tab
        {
            public ChildPane Child { get; set; }
            public string Caption { get; set; }
            public int Index { get; set; }

            public static Tab FromXml(XElement element_)
            {
                int index;
                int.TryParse(TryGetAttribute(element_, "index"), out index);
                Tab tab = new Tab
                    {
                        Caption = TryGetAttribute(element_, "caption"),
                        Index = index
                    };
                foreach (var tabChildElement in element_.Elements())
                {
                    tab.Child = ChildPane.FromXml(tabChildElement);
                    break;
                }
                return tab;
            }


            public static XElement ToXml(Tab tab_)
            {
                var result = new XElement("Tab");
                TrySetAttribute(result, "caption", tab_.Caption);
                TrySetAttribute(result, "index", tab_.Index.ToString());
                if (tab_.Child != null)
                {
                    result.Add(ChildPane.ToXml(tab_.Child));
                }
                return result;
            }
        }

        //---------------------------
        internal class TabbedDockRepr
        {
            public Xdm Xdm { get; set; }

            private TabsCollectionRepr m_tabs;

            public TabsCollectionRepr Tabs
            {
                get { return m_tabs ?? (m_tabs = new TabsCollectionRepr()); }
                set { m_tabs = value; }
            }

            private PaneCollectionRepr m_panes;

            public PaneCollectionRepr Panes
            {
                get { return m_panes ?? (m_panes = new PaneCollectionRepr()); }
                set { m_panes = value; }
            }

            public static TabbedDockRepr FromXml(XElement element_)
            {
                TabbedDockRepr result = new TabbedDockRepr();
                var xdmLayoutElement = element_.Element(DockLayoutRootName);
                if (xdmLayoutElement != null)
                {
                    result.Xdm = Xdm.FromXml(xdmLayoutElement.Element(DockManagerRootName));
                }
                var panesElement = element_.Element("Panes");
                if (panesElement != null)
                {
                    result.Panes = PaneCollectionRepr.FromXml(panesElement);
                }
                var tabsElement = element_.Element("Tabs");
                if (tabsElement != null)
                {
                    result.Tabs = TabsCollectionRepr.FromXml(tabsElement);
                }
                return result;
            }

            public static XElement ToXml(TabbedDockRepr tdr_)
            {
                XElement result = new XElement(TabbedDockViewModel.NODE_NAME);
                if (tdr_.Xdm != null)
                {
                    result.Add(new XElement(DockLayoutRootName, Xdm.ToXml(tdr_.Xdm)));
                }
                result.Add(TabsCollectionRepr.ToXml(tdr_.Tabs));
                result.Add(PaneCollectionRepr.ToXml(tdr_.Panes));
                return result;
            }

            public static TabbedDockRepr FromLayout(Layout layout_)
            {
                TabbedDockRepr result = new TabbedDockRepr();

                result.Xdm = new Xdm()
                    {
                        Version = DockManagerVersion
                    };

                bool activeFound = false;
                foreach (Tab tab in layout_.Splitter.Tabs)
                {
                    if (tab.Caption == layout_.Splitter.SelectedTab && !activeFound)
                    {
                        activeFound = true;
                    }
                }
                if (!activeFound)
                {
                    layout_.Splitter.SelectedTab = layout_.Splitter.Tabs[0].Caption;
                }
                activeFound = false;
                foreach (Tab tab in layout_.Splitter.Tabs)
                {
                    bool isActive = false;
                    if (tab.Caption == layout_.Splitter.SelectedTab && !activeFound)
                    {
                        activeFound = true;
                        isActive = true;
                    }

                    TabRepr tabRepr = TabRepr.FromTab(tab, result.Xdm, isActive);

                    result.Tabs.Add(tabRepr);
                }
                foreach (XElement windowElement in layout_.Windows)
                {
                    ViewRepr view = ViewRepr.FromWindow(windowElement, result.Xdm);
                    XdmChildPane cp = XdmChildPane.FromViewRepr(view, result.Xdm, true, true);

                    XdmSplitPane sp = new XdmSplitPane()
                        {
                            Location = "Floating",

                        };
                    try
                    {
                        var boundsEle = windowElement.Element("Bounds");

                        sp.FloatingLocation =
                            boundsEle.Attribute("left").Value + ", " +
                            boundsEle.Attribute("top").Value;

                        sp.FloatingSize =
                            boundsEle.Attribute("width").Value + ", " +
                            boundsEle.Attribute("height").Value;
                    }
                    catch
                    {

                    }

                    sp.Children.Add(cp);
                    result.Xdm.Panes.Add(sp);
                    result.Panes.Add(view);
                }
                return result;
            }
        }

        internal class Xdm
        {
            public string Version { get; set; }

            private readonly Dictionary<string, XdmContentPane> m_contentPanes =
                new Dictionary<string, XdmContentPane>();

            public Dictionary<string, XdmContentPane> ContentPanes
            {
                get { return m_contentPanes; }
            }

            private readonly List<XdmSplitPane> m_panes = new List<XdmSplitPane>();

            public List<XdmSplitPane> Panes
            {
                get { return m_panes; }
            }

            public static Xdm FromXml(XElement element_)
            {
                if (element_.Name != DockManagerRootName)
                {
                    throw new UnsupportedLayoutFormatException();
                }
                Xdm result = new Xdm()
                    {
                        Version = TryGetAttribute(element_, "version")
                    };

                XElement contentPanesElement = element_.Element("contentPanes");
                if (contentPanesElement == null)
                {
                    throw new UnsupportedLayoutFormatException();
                }

                foreach (XElement cpElement in contentPanesElement.Elements(LayoutStringConstants.ContentPanePrefix))
                {
                    XdmContentPane cp = XdmContentPane.FromXml(cpElement, result);
                    result.ContentPanes[cp.Name] = cp;
                }

                XElement panesElement = element_.Element("panes");
                if (panesElement != null)
                {
                    foreach (XElement splitPaneElement in panesElement.Elements("splitPane"))
                    {
                        result.Panes.Add(XdmChildPane.FromXml(splitPaneElement, result) as XdmSplitPane);
                    }
                }

                return result;
            }

            public static XElement ToXml(Xdm xdm_)
            {
                var result = new XElement(DockManagerRootName);
                TrySetAttribute(result, "version", xdm_.Version);
                var contentPanes = new XElement("contentPanes");
                var panes = new XElement("panes");
                result.Add(contentPanes);
                result.Add(panes);

                foreach (KeyValuePair<string, XdmContentPane> contentPanePair in xdm_.ContentPanes)
                {
                    contentPanes.Add(XdmContentPane.ToXml(contentPanePair.Value));
                }

                foreach (XdmSplitPane pane in xdm_.Panes)
                {
                    panes.Add(XdmSplitPane.ToXml(pane, true));
                }

                return result;
            }
        }

        internal class XdmChildPane
        {
            public string Name { get; set; }
            public virtual double RelativeWidth { get; set; }
            public virtual double RelativeHeight { get; set; }
            public virtual double Height { get; set; }
            public virtual double Width { get; set; }

            public static XElement ToXml(XdmChildPane childPane_)
            {
                if (childPane_ is XdmSplitPane)
                {
                    return XdmSplitPane.ToXml((XdmSplitPane) childPane_);
                }
                if (childPane_ is XdmContentPane)
                {
                    return XdmContentPane.ToXmlBrief((XdmContentPane) childPane_);
                }
                throw new UnsupportedLayoutFormatException();
            }

            public static XdmChildPane FromXml(XElement element_, Xdm xdm_)
            {
                if (element_.Name == "splitPane")
                {
                    return XdmSplitPane.FromXml(element_, xdm_);
                }
                if (element_.Name == LayoutStringConstants.ContentPanePrefix)
                {
                    return xdm_.ContentPanes[TryGetAttribute(element_, "name")];
                }
                if (element_.Name == "tabGroup")
                {
                    return XdmSplitPane.FromXml(element_, xdm_);
                }
                throw new UnsupportedLayoutFormatException();
            }

            public static XdmChildPane FromChildPane(
                ChildPane childPane_,
                PaneCollectionRepr collectionRepr_,
                Xdm xdm_,
                bool visible_
                )
            {
                if (childPane_ is Pane)
                {
                    Pane pane = (Pane) childPane_;
                    ViewRepr view = ViewRepr.FromPane(pane);
                    if (view != null)
                    {
                        collectionRepr_.Add(view);
                        return FromViewRepr(view, xdm_, false, visible_);
                    }
                    return null;
                }
                if (childPane_ is SyncSplitPane)
                {
                    SyncSplitPane sspane = (SyncSplitPane) childPane_;
                    XdmSplitPane result = new XdmSplitPane();
                    switch (sspane.SplitDirection)
                    {
                        case "Left":
                            result.SplitterOrientation = "Vertical";
                            result.Location = "DockedTop";
                            break;
                        case "Top":
                            result.SplitterOrientation = "Horizontal";
                            result.Location = "DockedLeft";
                            break;
                        case "Bottom":
                            result.SplitterOrientation = "Horizontal";
                            result.Location = "DockedRight";
                            break;
                        case "Right":
                            result.SplitterOrientation = "Vertical";
                            result.Location = "DockedBottom";
                            break;
                        default:
                            result.SplitterOrientation = "Vertical";
                            result.Location = "DockedTop";
                            break;
                    }
                    if (sspane.Child1 != null)
                    {
                        XdmChildPane child1 = FromChildPane(sspane.Child1, collectionRepr_, xdm_, visible_);
                        result.Children.Add(child1);
                    }
                    if (sspane.Child2 != null)
                    {
                        XdmChildPane child2 = FromChildPane(sspane.Child2, collectionRepr_, xdm_, visible_);
                        result.Children.Add(child2);
                    }
                    result.RelativeHeight = sspane.RelativeHeight;
                    result.RelativeWidth = sspane.RelativeWidth;
                    result.Height = sspane.Height;
                    result.Width = sspane.Width;
                    return result;
                }
                throw new UnsupportedLayoutFormatException();
            }

            public static XdmChildPane FromViewRepr(
                ViewRepr view_,
                Xdm xdm_,
                bool floating_,
                bool visible_
                )
            {
                if (view_ != null)
                {
                    XdmContentPane result = new XdmContentPane()
                        {
                            LastDockableState = "Floating",
                            LastFloatingSize = "954,702",
                            LastFloatingWindowRect = "110,110,960,728",
                            LastFloatingLocation = "113,133",
                            LastActivatedTime = "2011-03-28T15:53:12.0525551Z"
                        };
                    result.Location = floating_ ? "Floating" : "DockedTop";
                    result.Visibility = visible_ ? "Visible" : "Collapsed";
                    result.Name = LayoutStringConstants.ContentPanePrefix + view_.Id;

                    xdm_.ContentPanes.Add(result.Name, result);

                    result.RelativeHeight = view_.RelativeHeight;
                    result.RelativeWidth = view_.RelativeWidth;
                    result.Height = view_.RelativeHeight;
                    result.Width = view_.RelativeWidth;
                    return result;
                }
                return null;
            }
        }

        internal class XdmSplitPane : XdmChildPane
        {
            //public XdmChildPane Child1 { get; set; }
            //public XdmChildPane Child2 { get; set; }



            private readonly List<XdmChildPane> m_children = new List<XdmChildPane>();

            public List<XdmChildPane> Children
            {
                get { return m_children; }
            }

            public string Location { get; set; }
            public string SplitterOrientation { get; set; }
            public string FloatingLocation { get; set; }
            public string FloatingSize { get; set; }
            //extent

            public override double Height
            {
                get
                {
                    if (this.SplitterOrientation == "Vertical")
                        return Children.Sum(x => x == null ? 0 : x.Height);
                    else
                    {
                        return Children.Max(x => x == null ? 0 : x.Height);
                    }
                }
                set { base.Height = value; }
            }

            public override double Width
            {
                get
                {
                    if (this.SplitterOrientation == "Vertical")
                    {
                        return Children.Max(x => x == null ? 0 : x.Width);
                    }
                    return Children.Sum(x => x == null ? 0 : x.Width);
                }
                set { base.Width = value; }
            }

            public new static XdmSplitPane FromXml(XElement element_, Xdm xdm_)
            {
                var result = new XdmSplitPane()
                    {
                        Name = TryGetAttribute(element_, "name"),
                        Location = TryGetAttribute(element_, "location"),
                        FloatingLocation = TryGetAttribute(element_, "floatingLocation"),
                        FloatingSize = TryGetAttribute(element_, "floatingSize"),
                        SplitterOrientation = TryGetAttribute(element_, "splitterOrientation")
                    };
                foreach (var spChildElement in element_.Elements())
                {
                    XdmChildPane child = XdmChildPane.FromXml(spChildElement, xdm_);
                    if (child != null)
                    {
                        result.Children.Add(child);
                    }
                }
                return result;
            }

            public static XElement ToXml(XdmSplitPane xdmSplitPane_, bool isRoot = false)
            {
                var result = new XElement("splitPane");
                TrySetAttribute(result, "name", xdmSplitPane_.Name);
                TrySetAttribute(result, "location", xdmSplitPane_.Location);
                TrySetAttribute(result, "splitterOrientation", xdmSplitPane_.SplitterOrientation);
                TrySetAttribute(result, "floatingLocation", xdmSplitPane_.FloatingLocation);
                TrySetAttribute(result, "floatingSize", xdmSplitPane_.FloatingSize);

                if (isRoot)
                {
                    if (!xdmSplitPane_.Children.All(x => x == null))
                    {
                        if (xdmSplitPane_.SplitterOrientation == "Horizontal")
                        {
                            TrySetAttribute(result, "extent", xdmSplitPane_.Height.ToString());
                        }
                        else
                        {
                            TrySetAttribute(result, "extent", xdmSplitPane_.Width.ToString());
                        }
                    }

                }
                else
                {
                    TrySetAttribute(result, "relativeSize",
                                    (xdmSplitPane_.RelativeWidth <= 0 ? 1 : xdmSplitPane_.RelativeWidth) + ", " +
                                    (xdmSplitPane_.RelativeHeight <= 0 ? 1 : xdmSplitPane_.RelativeHeight));
                }


                foreach (XdmChildPane child in xdmSplitPane_.Children)
                {
                    if (child != null)
                    {
                        result.Add(new XElement(XdmChildPane.ToXml(child)));
                    }
                }
                return result;
            }
        }

        internal class XdmContentPane : XdmChildPane
        {
       
            public string Location { get; set; }
            public string LastDockableState { get; set; }
            public string LastFloatingSize { get; set; }
            public string LastFloatingWindowRect { get; set; }
            public string LastFloatingLocation { get; set; }
            public string LastActivatedTime { get; set; }
            public string Visibility { get; set; }

            public new static XdmContentPane FromXml(XElement cpElement_, Xdm xdm_)
            {
                XdmContentPane cp = new XdmContentPane()
                    {
                        Name = SanitizeContentPaneName(LayoutConverter.TryGetAttribute(cpElement_, "name")),
                        LastActivatedTime = LayoutConverter.TryGetAttribute(cpElement_, "lastActivatedTime"),
                        LastFloatingLocation = LayoutConverter.TryGetAttribute(cpElement_, "lastFloatingLocation"),
                        LastFloatingSize = LayoutConverter.TryGetAttribute(cpElement_, "lastFloatingSize"),
                        LastDockableState = LayoutConverter.TryGetAttribute(cpElement_, "lastDockableState"),
                        LastFloatingWindowRect = LayoutConverter.TryGetAttribute(cpElement_, "lastFloatingWindowRect"),
                        Location = LayoutConverter.TryGetAttribute(cpElement_, "location"),
                        Visibility = LayoutConverter.TryGetAttribute(cpElement_, "visibility"),
                    };
                if (cp.Name == null)
                {
                    throw new UnsupportedLayoutFormatException("contentPane elements must have names");
                }
                return cp;
            }

            public static XdmContentPane FromPane(Pane pane_)
            {
                XdmContentPane result = new XdmContentPane()
                    {
                        Location = "DockedTop",
                        Visibility = "Visible",
                        LastDockableState = "Floating",
                        LastFloatingSize = "954,702",
                        LastFloatingWindowRect = "110,110,960,728",
                        LastFloatingLocation = "113,133",
                        LastActivatedTime = "2011-03-28T15:53:12.0525551Z"

                    };

                return result;
            }

            public static string SanitizeContentPaneName(string name_)
            {
                if (string.IsNullOrEmpty(name_))
                {
                    return LayoutStringConstants.ContentPanePrefix + Guid.NewGuid().ToString("N");
                }
                string result = Regex.Replace(name_, @"[\W]", "X");
                return result;
            }

            public static XElement ToXml(XdmContentPane xdmContentPane_)
            {
                XElement result = new XElement(LayoutStringConstants.ContentPanePrefix);
                TrySetAttribute(result, "name", xdmContentPane_.Name);
                TrySetAttribute(result, "location", xdmContentPane_.Location);
                TrySetAttribute(result, "lastFloatingSize", xdmContentPane_.LastFloatingSize);
                TrySetAttribute(result, "lastDockableState", xdmContentPane_.LastDockableState);
                TrySetAttribute(result, "lastFloatingWindowRect", xdmContentPane_.LastFloatingWindowRect);
                TrySetAttribute(result, "lastFloatingLocation", xdmContentPane_.LastFloatingLocation);
                TrySetAttribute(result, "lastActivatedTime", xdmContentPane_.LastActivatedTime);
                TrySetAttribute(result, "visibility", xdmContentPane_.Visibility);
                return result;
            }

            public static XElement ToXmlBrief(XdmContentPane xdmContentPane_)
            {
                XElement result = new XElement(LayoutStringConstants.ContentPanePrefix);
                TrySetAttribute(result, "name", xdmContentPane_.Name);
                TrySetAttribute(result, "relativeSize",
                                xdmContentPane_.RelativeWidth + ", " + xdmContentPane_.RelativeHeight);

                return result;
            }
        }

        internal class TabsCollectionRepr : List<TabRepr>
        {
            public static TabsCollectionRepr FromXml(XElement element_)
            {
                var result = new TabsCollectionRepr();
                foreach (var tabElement in element_.Elements("Tab"))
                {
                    TabRepr tab = TabRepr.FromXml(tabElement);
                    result.Add(tab);
                }
                return result;
            }

            public static XElement ToXml(TabsCollectionRepr tabs_)
            {
                var result = new XElement("Tabs");
                foreach (TabRepr tab in tabs_)
                {
                    XElement child = TabRepr.ToXml(tab);
                    if (child != null)
                    {
                        result.Add(new XElement(child));
                    }
                }
                return result;
            }
        }

        internal class PaneCollectionRepr : List<ViewRepr>
        {
            public static PaneCollectionRepr FromXml(XElement element_)
            {
                var result = new PaneCollectionRepr();
                foreach (var viewElement in element_.Elements("IView"))
                {
                    ViewRepr view = ViewRepr.FromXml(viewElement);
                    if (string.IsNullOrEmpty(view.Id))
                    {
                        throw new UnsupportedLayoutFormatException("IView must have an id");
                    }
                    result.Add(view);
                }
                return result;
            }

            public static XElement ToXml(PaneCollectionRepr panes_)
            {
                var result = new XElement("Panes");
                foreach (ViewRepr pane in panes_)
                {
                    XElement child = ViewRepr.ToXml(pane);
                    if (child != null)
                    {
                        result.Add(new XElement(child));
                    }
                }
                return result;
            }
        }

        internal class TabRepr
        {
            public string Name { get; set; }
            public string IsActive { get; set; }
            private PaneCollectionRepr m_panes;

            public PaneCollectionRepr Panes
            {
                get { return m_panes ?? (m_panes = new PaneCollectionRepr()); }
                set { m_panes = value; }
            }

            public static TabRepr FromXml(XElement element_)
            {
                TabRepr result = new TabRepr();
                result.Name = TryGetAttribute(element_, "Name");
                result.IsActive = TryGetAttribute(element_, "IsActive");
                result.Panes = PaneCollectionRepr.FromXml(element_.Element("Panes"));
                return result;
            }

            public static XElement ToXml(TabRepr tabRepr_)
            {
                XElement result = new XElement("Tab");
                TrySetAttribute(result, "Name", tabRepr_.Name);
                TrySetAttribute(result, "IsActive", tabRepr_.IsActive);
                result.Add(PaneCollectionRepr.ToXml(tabRepr_.Panes));
                return result;
            }

            public static TabRepr FromTab(Tab tab_, Xdm xdm_, bool isActive_)
            {
                var result = new TabRepr();
                if (isActive_)
                {
                    result.IsActive = "true";
                }
                result.Name = tab_.Caption;

                if (tab_.Child is Pane)
                {
                    XdmContentPane contentPane =
                        XdmChildPane.FromChildPane(tab_.Child, result.Panes, xdm_, isActive_) as XdmContentPane;
                    if (contentPane != null)
                    {
                        XdmSplitPane splitPane = new XdmSplitPane()
                            {

                                Location = "DockedTop",
                                SplitterOrientation = "Vertical"
                            };
                        splitPane.Children.Add(contentPane);
                        xdm_.Panes.Add(splitPane);
                    }
                }
                if (tab_.Child is SyncSplitPane)
                {
                    XdmSplitPane splitPane =
                        XdmChildPane.FromChildPane(tab_.Child, result.Panes, xdm_, isActive_) as XdmSplitPane;
                    if (splitPane != null)
                    {
                        xdm_.Panes.Add(splitPane);
                    }
                }
                return result;
            }



        }

        internal class ViewRepr
        {
            public string Id { get; set; }
            public string ViewContainerType { get; set; }
            public string Title { get; set; }
            public string CreatorId { get; set; }
            public XElement Content { get; set; }
            public double RelativeWidth { get; set; }
            public double RelativeHeight { get; set; }

            public static ViewRepr FromXml(XElement element_)
            {
                ViewRepr result = new ViewRepr();
                result.Id = TryGetAttribute(element_, "Id");
                result.ViewContainerType = TryGetAttribute(element_, "ViewContainerType");
                result.Title = TryGetAttribute(element_, "Title");
                result.CreatorId = TryGetAttribute(element_, "CreatorID");
                if (result.ViewContainerType == "Concord")
                {
                    result.Content = element_.Element("ConcordWindow");
                }
                else if (result.ViewContainerType == "WithCreator")
                {
                    result.Content = element_.Element("State");
                }

                return result;
            }

            public static XElement ToXml(ViewRepr viewRepr_)
            {
                var result = new XElement("IView");
                TrySetAttribute(result, "Id", viewRepr_.Id);
                TrySetAttribute(result, "ViewContainerType", viewRepr_.ViewContainerType);
                TrySetAttribute(result, "Title", viewRepr_.Title);
                TrySetAttribute(result, "CreatorID", viewRepr_.CreatorId);
                if (viewRepr_.Content != null)
                {
                    XElement concordwindow = new XElement("ConcordWindow", viewRepr_.Content);
                    TrySetAttribute(concordwindow, "Title", viewRepr_.Title);
                    var guid = TryGetAttribute(viewRepr_.Content, "guid");
                    if (!string.IsNullOrEmpty(guid))
                    {
                        TrySetAttribute(concordwindow, "Guid", guid);
                    }
                    result.Add(concordwindow);
                }
                return result;
            }

            public static ViewRepr FromPane(Pane pane_)
            {
                ViewRepr result = new ViewRepr();
                result.Title = pane_.Title;
                result.Id = WindowViewContainer.GenerateNewID();
                result.ViewContainerType = "Concord";

                result.RelativeWidth = pane_.Width;
                result.RelativeHeight = pane_.Height;
                if (pane_.Content != null)
                {
                    result.Content = new XElement(pane_.Content);
                    return result;
                }
                return null;
            }

            public static ViewRepr FromWindow(XElement element_, Xdm xdm_)
            {
                if (element_.Name != "Window")
                {
                    throw new UnsupportedLayoutFormatException("This function should be called only for Window elements");
                }
                ViewRepr result = new ViewRepr
                    {
                        Id = WindowViewContainer.GenerateNewID(),
                        ViewContainerType = "Concord"
                    };

                try
                {
                    var widthAttr = element_.Element("Bounds").Attribute("width").Value;
                    var heightAttr = element_.Element("Bounds").Attribute("height").Value;
                    double width, height;
                    if (double.TryParse(widthAttr, out width) && double.TryParse(heightAttr, out height))
                    {
                        result.RelativeHeight = height;
                        result.RelativeWidth = width;
                    }
                }
                catch
                {

                }


                if (element_.Element("Caption") != null)
                {
                    result.Title = element_.Element("Caption").Value;
                }
                var content = new XElement(element_) {Name = "ConcordForm"};
                result.Content = content;

                return result;
            }

            public static XElement ToWindow(ViewRepr viewRepr_)
            {
                if (viewRepr_.Content != null)
                {
                    if (viewRepr_.Content.Name == "ConcordWindow")
                    {
                        string title = TryGetAttribute(viewRepr_.Content, "Title");
                        var formElement = viewRepr_.Content.Element("ConcordForm");
                        if (formElement != null)
                        {
                            XElement result = new XElement(formElement);
                            result.Name = "Window";
                            result.Add(new XElement("Caption", title));
                            result.Add(new XElement("WindowState", "Normal"));
                            result.Add(new XElement("TopMost", "False"));
                            return result;
                        }
                    }
                }
                return null;
            }
        }

    }

    internal class UnsupportedLayoutFormatException : Exception
    {
        public UnsupportedLayoutFormatException()
        {
        }

        public UnsupportedLayoutFormatException(string message_)
            : base(message_)
        {
        }
    }

    //-----------------------------


}
