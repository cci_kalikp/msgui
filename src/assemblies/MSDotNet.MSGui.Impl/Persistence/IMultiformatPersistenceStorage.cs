﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Persistence/IMultiformatPersistenceStorage.cs#6 $
// $Change: 885090 $
// $DateTime: 2014/06/17 10:55:45 $
// $Author: hrechkin $

using System.Collections.Generic;
using System.Xml.Linq;

namespace MorganStanley.MSDotNet.MSGui.Impl.Persistence
{
    internal interface IMultiformatPersistenceStorage
    {
        ICollection<string> SupportedWindowManagers { get; }
        void SaveStateInFormat(XDocument state, string name, string format, string displayName);

        /// <summary>
        /// Return window manager id for a layout by layout's display name
        /// </summary>
        /// <param name="displayName"></param>
        /// <returns></returns>   
        string GetWindowManager(string displayName);

        /// <summary>
        /// Gets suffix for this window manager
        /// </summary>
        /// <param name="windowManager">window manager's string Id</param>
        /// <returns></returns>

        string GetSuffix(string windowManager);

        /// <summary>
        /// Get human-readable name of the format for the current window manager
        /// </summary>
        /// <param name="windowManager"></param>
        /// <returns></returns>
        string GetFormatName(string windowManager);
    }
}
