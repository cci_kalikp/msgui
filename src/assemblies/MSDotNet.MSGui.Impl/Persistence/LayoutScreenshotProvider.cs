﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml.Linq;
using CPS.GPB;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.MicrosoftShell.Standard;
using Color = System.Drawing.Color;

namespace MorganStanley.MSDotNet.MSGui.Impl.Persistence
{
    public class LayoutScreenshotProvider : IPersistable
    {
        private readonly IChromeManager chromeManager;
        private readonly Dictionary<string, byte[]> thumbnails = new Dictionary<string, byte[]>();

        public LayoutScreenshotProvider(IChromeManager chromeManager)
        {
            this.chromeManager = chromeManager;
        }

        /// <summary>
        /// Takes a screenshot of the current layout.
        /// </summary>
        /// <returns>A JPEG stream containing the resulting image. Null if failed.</returns>
        public byte[] CaptureCurrentLayout()
        {
            try
            {
                var windows = new List<Window>();

                // make sure to add the main shell, we may not have panes docked into it
                if (System.Windows.Application.Current.MainWindow != null)
                {
                    windows.Add(System.Windows.Application.Current.MainWindow);
                }

                // find all the top-level msdesktop windows
                foreach (var view in this.chromeManager.Windows)
                {
                    // todo farm Add screenshot support for datatemplated views
                    var content = view.Content as DependencyObject;
                    if (content == null)
                        continue;

                    var window = Window.GetWindow(content);
                    if (!windows.Contains(window))
                        windows.Add(window);
                }

                // find the overall bounds
                var left = int.MaxValue;
                var top = int.MaxValue;
                var right = int.MinValue;
                var bottom = int.MinValue;
                foreach (var screen in Screen.AllScreens)
                {
                    if (screen.Bounds.Left < left)
                        left = screen.Bounds.Left;
                    if (screen.Bounds.Top < top)
                        top = screen.Bounds.Top;
                    if (screen.Bounds.Right > right)
                        right = screen.Bounds.Right;
                    if (screen.Bounds.Bottom > bottom)
                        bottom = screen.Bounds.Bottom;
                }

                // the top left corner may not be at (0,0)
                var translateX = -left;
                var translateY = -top;

                // create a big ass buffer
                var buffer = new Bitmap(right - left, bottom - top);
                var graphic = Graphics.FromImage(buffer);

                // capture each screen onto that big ass buffer
                foreach (var screen in Screen.AllScreens)
                {
                    graphic.CopyFromScreen(screen.Bounds.X, screen.Bounds.Y, screen.Bounds.X + translateX,
                        screen.Bounds.Y + translateY, new System.Drawing.Size(screen.Bounds.Width, screen.Bounds.Height),
                        CopyPixelOperation.SourceCopy);
                }

                // create the masking region
                var region = NativeMethods.CreateRectRgn(0, 0, buffer.Width, buffer.Height);

                foreach (var window in windows)
                {
                    var windowRegion = NativeMethods.CreateRectRgn(
                        (int) window.Left + translateX,
                        (int) window.Top + translateY,
                        (int) (window.Left + window.Width) + translateX,
                        (int) (window.Top + window.Height) + translateY);

                    var destRegion = NativeMethods.CreateRectRgn(0, 0, 0, 0);

                    NativeMethods.CombineRgn(destRegion, region, windowRegion, RGN.DIFF);
                    region = destRegion;
                }

                // darken everything outside the msdesktop windows
                var b = new SolidBrush(Color.FromArgb(220, 0, 0, 0));
                var r = Region.FromHrgn(region);
                graphic.FillRegion(b, r);

                // resize it to fit into 2000x2000 pixels
                var ratio = Math.Min(2000f/buffer.Width, 2000f/buffer.Height);
                var thumb = buffer.GetThumbnailImage((int) (buffer.Width*ratio), (int) (buffer.Height*ratio), null,
                    IntPtr.Zero);

                // grab a bytearray
                byte[] imageBytes;
                using (var stream = new MemoryStream())
                {
                    thumb.Save(stream, ImageFormat.Jpeg);
                    imageBytes = stream.GetBuffer();
                }

                return imageBytes;
            }
            catch
            {
                return null;
            }
        }

        public void LoadState(XDocument state)
        {
            this.thumbnails.Clear();

            if (state == null || state.Root == null) 
                return;

            foreach (var element in state.Root.Elements())
            {
                var keyElement = element.Element("LayoutName");
                var imageElement = element.Element("Image");

                if (keyElement == null || imageElement == null) 
                    continue;

                this.UpdateLayoutThumbnail(keyElement.Value, Convert.FromBase64String(imageElement.Value));
            }
        }

        public XDocument SaveState()
        {
            var result = new XDocument();
            var root = new XElement("LayoutThumbnails");

            foreach (var thumbnail in thumbnails)
            {
                root.Add(new XElement("Thumbnail", new XElement("LayoutName", thumbnail.Key), new XElement("Image", Convert.ToBase64String(thumbnail.Value))));
            }

            result.Add(root);
            return result;
        }

        public string PersistorId
        {
            get { return "LayoutScreenshots"; }
        }

        public void UpdateLayoutThumbnail(string profileName, byte[] thumbnailBytes)
        {
            if (thumbnails.ContainsKey(profileName))
            {
                thumbnails[profileName] = thumbnailBytes;
            }
            else
            {
                thumbnails.Add(profileName, thumbnailBytes);
            }
        }

        public ImageSource GetLayoutThumbnail(string profileName)
        {
            if (!this.thumbnails.ContainsKey(profileName))
                return null;

            var bytes = this.thumbnails[profileName];
            if (bytes == null) return null;
            BitmapSource source = null;
            using (var stm = new MemoryStream(bytes))
            {
                var decoder = BitmapDecoder.Create(stm, BitmapCreateOptions.None, BitmapCacheOption.OnLoad);
                source = decoder.Frames[0];
            }

            return source;
        }
    }
}
