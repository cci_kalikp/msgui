﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.Persistence
{
    public class WidgetStatesInfo : IPersistable
    {
        internal static string LocalWidgetStatesInfoInstanceId = "LocalWidgetStatesInfo";
        internal static string GlobalWidgetStatesInfoInstanceId = "GlobalWidgetStatesInfo";

        private readonly string m_id;
        private readonly ChromeManagerBase m_chromeManager;

        public WidgetStatesInfo(string id, IUnityContainer container)
        {
            m_id = id;
            m_chromeManager = container.Resolve<IChromeManager>() as ChromeManagerBase;
        }

        public void LoadState(XDocument state)
        {
            if (state == null || state.Root == null) return;
            var persistableWidgets = new HashSet<string>(m_chromeManager.GetPersistableWidgets(m_id == GlobalWidgetStatesInfoInstanceId));
            foreach (var widgetStateEl in state.Root.Descendants("WidgetState"))
            {
                var widgetIdAttribute = widgetStateEl.Attribute("Id");
                if (widgetIdAttribute == null) return;
                if (!persistableWidgets.Contains(widgetIdAttribute.Value)) continue;
                m_chromeManager.LoadWidgetWithState(widgetIdAttribute.Value, new XDocument(widgetStateEl.Elements().First()));
            }
        }

        public XDocument SaveState()
        {
            var widgets = m_chromeManager.GetPersistableWidgets(m_id == GlobalWidgetStatesInfoInstanceId);
            var stateEls = widgets.Select(id => new { Id = id, Element = m_chromeManager.SaveWidgetState(id)});
            var rootEl = new XElement(m_id);
            foreach (var stateInfo in stateEls)
            {
                if (stateInfo.Element == null) continue; 
                var stateEl = new XElement("WidgetState");
                stateEl.SetAttributeValue("Id", stateInfo.Id);
                stateEl.Add(stateInfo.Element.Root);
                rootEl.Add(stateEl);
            }
            return new XDocument(rootEl);
        }

        public string PersistorId
        {
            get { return m_id; }
        }
    }
}
