﻿////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Persistence/IItemsDictionaryService.cs#11 $
// $Change: 889454 $
// $DateTime: 2014/07/22 18:57:04 $
// $Author: hrechkin $

using System.Collections.Generic;
using System.Xml.Linq;

namespace MorganStanley.MSDotNet.MSGui.Impl.Persistence
{
    internal interface IItemsDictionaryService
    {
        /// <summary>
        /// Calls save function for all registered persistors.
        /// </summary>
        /// <returns>
        /// A dictionary of saved persistors where a key is persistor's ID
        /// </returns>
        Dictionary<string, XElement> SaveItemsToDict(ItemScope scope=ItemScope.All);

        /// <summary>
        /// Calls saving functions for registered persistors with keys from the given set.
        /// </summary>
        /// <param name="keys">keys of persistors to save</param>
        /// <returns>A dictionary of saved persistors where a key is persistor's ID</returns>
        Dictionary<string, XElement> SaveItemsToDict(IEnumerable<string> keys, ItemScope scope=ItemScope.All);

        void RestoreItemsFromDict(IEnumerable<KeyValuePair<string, XElement>> itemsDict, ItemScope scope = ItemScope.All, IEnumerable<string> keys = null);
    }

    public enum ItemScope
    {
        Normal,
        GlobalPersistor,
        All
    }
}
