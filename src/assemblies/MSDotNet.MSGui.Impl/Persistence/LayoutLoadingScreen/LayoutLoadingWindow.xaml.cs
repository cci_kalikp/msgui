﻿using System;
using System.ComponentModel;
using System.Windows;

namespace MorganStanley.MSDotNet.MSGui.Impl.Persistence.LayoutLoadingScreen
{
    /// <summary>
    /// Interaction logic for LayoutLoadingWindow.xaml
    /// </summary>
    public partial class LayoutLoadingWindow : Window
    {
        public LayoutLoadingWindow()
        {
            InitializeComponent();
        }

        public bool CancelClose { get; set; }

        private void LayoutLoadingWindow_OnClosing(object sender, CancelEventArgs e)
        {
            if (!CancelClose)
            {
                CancelClose = true;
                e.Cancel = true;
                ((LayoutLoadingViewModel) DataContext).OnCancelRequested();
            }
        }
    }
}
