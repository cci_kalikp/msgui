﻿using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Application;

namespace MorganStanley.MSDotNet.MSGui.Impl.Persistence.LayoutLoadingScreen
{
    class LayoutLoadingScreenRetriever
    {
        private readonly IUnityContainer container;

        public LayoutLoadingScreenRetriever(IUnityContainer container)
        {
            this.container = container;
        }

        public ILayoutLoadingScreen GetScreen()
        {
            if (SplashLauncher<ModernSplashWindow>.SplashWindowInstance != null)
            {
                return (ILayoutLoadingScreen) container.Resolve<ISplashScreen>();
            }
            return container.Resolve<LayoutLoadingScreenLauncher>().Instance;
        }

        public bool IsSplashScreenVisible()
        {
            return SplashLauncher<ModernSplashWindow>.SplashWindowInstance != null;
        }
    }
}
