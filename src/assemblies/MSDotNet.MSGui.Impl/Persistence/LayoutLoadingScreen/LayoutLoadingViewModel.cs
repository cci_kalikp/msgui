﻿using System; 
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;

namespace MorganStanley.MSDotNet.MSGui.Impl.Persistence.LayoutLoadingScreen
{
    class LayoutLoadingViewModel : ViewModelBase, ILayoutLoadingScreen
    {
        private string title;
        private string statusText;
        private int progressBarMax;
        private int progressBarValue;

        public string Title
        {
            get
            {
                return title;
            }
            set
            {
                title = value;
                OnPropertyChanged("Title");
            }
        }

        public string StatusText
        {
            get
            {
                return statusText;
            }
            set
            {
                statusText = value;
                OnPropertyChanged("StatusText");
            }
        }

        public int ProgressBarMax
        {
            get
            {
                return progressBarMax;
            }
            set
            {
                progressBarMax = value;
                OnPropertyChanged("ProgressBarMax");
            }
        }

        public int ProgressBarValue
        {
            get
            {
                return progressBarValue;
            }
            set
            {
                progressBarValue = value;
                OnPropertyChanged("ProgressBarValue");
            }
        }

        public void SetCurrentView(string text)
        {
            StatusText = string.Format("Loading view {0}", text);
        }

        public void SetStatus(string text)
        {
            this.StatusText = text;
        }
        public void ShowProgressBar(int maxSize)
        {
            ProgressBarMax = maxSize;
        }

        public void SetProgress(int value)
        {
            ProgressBarValue = value;
        }

        public event EventHandler CancelRequested;

        internal void OnCancelRequested()
        {
            StatusText = "Cancelling layout load";
            var handler = CancelRequested;
            if (handler != null) handler(this, EventArgs.Empty);
        }
 

        public void SetProfileName(string profile)
        {
            Title = string.Format("Loading layout '{0}'", profile);
        }
    }
}
