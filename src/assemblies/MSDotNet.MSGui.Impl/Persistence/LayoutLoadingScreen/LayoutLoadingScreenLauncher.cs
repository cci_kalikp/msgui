﻿using System;
using System.Threading;
using System.Windows.Threading;

namespace MorganStanley.MSDotNet.MSGui.Impl.Persistence.LayoutLoadingScreen
{
    sealed class LayoutLoadingScreenLauncher
    {
        public LayoutLoadingViewModel Instance { get; private set; }

        private LayoutLoadingWindow window;
        private Thread screenThread;
        private readonly object mutex;
        private bool screenThreadReady;

        public LayoutLoadingScreenLauncher()
        {
            Instance = new LayoutLoadingViewModel();
            mutex = new object();
        }

        public void Show(string profile)
        {
            lock (mutex)
            {
                screenThreadReady = false;
                screenThread = new Thread(() =>
                {
                    Dispatcher.CurrentDispatcher.BeginInvoke((Action)(() =>
                    {
                        lock (mutex)
                        {
                            window = new LayoutLoadingWindow();
                            Instance.SetProfileName(profile); 
                            Instance.SetProgress(0);
                            Instance.StatusText = string.Empty;
                            window.DataContext = Instance;
                        }
                        if (window != null)
                        { 
                            window.Show();
                        }
                        if (window != null)
                        {
                            window.Activate();
                        }
                    }));
                    Dispatcher.Run();
                });

                screenThread.IsBackground = true;
                screenThread.SetApartmentState(ApartmentState.STA);
                screenThread.Start();
                screenThreadReady = true;
            }
        }

        public void Close()
        {
            lock (mutex)
            {
                if (screenThreadReady)
                {
                    if (window != null)
                    {
                        window.CancelClose = false;
                        window.Dispatcher.BeginInvokeShutdown(DispatcherPriority.Normal);
                    }

                    screenThread = null;
                    window = null;
                }
            }
        }
        
    }
}
