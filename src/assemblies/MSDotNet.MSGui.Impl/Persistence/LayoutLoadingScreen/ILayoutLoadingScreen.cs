﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Impl.Persistence.LayoutLoadingScreen
{
    interface ILayoutLoadingScreen
    {
        void SetCurrentView(string text);

        void ShowProgressBar(int maxSize);

        void SetProgress(int value);

        void SetStatus(string text);

        event EventHandler CancelRequested;
    }
}
