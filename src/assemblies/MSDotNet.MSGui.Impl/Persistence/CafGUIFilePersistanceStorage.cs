﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.IED.Concord.Configuration;

namespace MorganStanley.MSDotNet.MSGui.Impl.Persistence
{
    public class CafGuiFilePersistanceStorage : ConcordPersistenceStorage, IConfigurationInterceptingReader
    {
        private readonly string _concordDefaultProfile;

        public CafGuiFilePersistanceStorage(string applicationName) : base(applicationName)
        {
            _concordDefaultProfile = applicationName;
        }

        public ConfigurationSection InterceptRead(ConfigurationSection original)
        {
            // Here, read the CafGUI profile. We are interested only in window layout.

            //           PersistableWindowStateOwner windowStateOwner = new PersistableWindowStateOwner(window, 0, true);

            return original;
        }
    }
}
