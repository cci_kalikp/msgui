/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Persistence/IPartialPersistenceStorage.cs#8 $
// $Change: 862682 $
// $DateTime: 2014/01/17 16:11:04 $
// $Author: hrechkin $

using System.Collections.Generic;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MorganStanley.MSDotNet.MSGui.Impl.Persistence
{
  internal interface IPartialPersistenceStorage : IPersistenceStorage
  {    
    void SaveState(string profile_, Dictionary<string, XElement> items_, Dictionary<string, XElement> subitems_, bool clean_);   
    void LoadState(string name, out Dictionary<string, XElement> items_, out Dictionary<string, XElement> subitems_);
  }
}
