﻿////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Persistence/XElementWithSubitems.cs#5 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System.Collections.Generic;
using System.Xml.Linq;

namespace MorganStanley.MSDotNet.MSGui.Impl.Persistence
{
  internal class XElementWithSubitems : XElement
  {
    public XElementWithSubitems(XName name_)
      : base(name_)
    { }
    public XElementWithSubitems(XName name_, object content_)
      : base(name_, content_)
    { }
    public XElementWithSubitems(XName name_, params object[] content_)
      : base(name_, content_)
    { }
    
    public XElementWithSubitems(XElement other_)
      : base(other_)
    { }
    public XElementWithSubitems(XStreamingElement other_)
      : base(other_)
    { }

    private readonly Dictionary<string, XElement> m_subitemsDict = new Dictionary<string, XElement>();
    public Dictionary<string, XElement> Subitems
    {
      get
      {
        return m_subitemsDict;
      }
    }
  }
}
