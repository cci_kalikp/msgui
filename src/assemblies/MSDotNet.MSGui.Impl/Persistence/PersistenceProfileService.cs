﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Persistence/PersistenceProfileService.cs#65 $
// $Change: 902903 $
// $DateTime: 2014/10/29 02:23:15 $
// $Author: caijin $

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq; 
using System.Windows.Threading;
using System.Xml.Linq;
using System.Xml.XPath; 
using Microsoft.Practices.Unity; 
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.Persistence.DelayedLoading;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Concord;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence.LayoutLoadingScreen;
using MorganStanley.MSDotNet.MSGui.Impl.Ribbon;
using MorganStanley.MSDotNet.MSGui.Impl.Shell;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.My;

namespace MorganStanley.MSDotNet.MSGui.Impl.Persistence
{
    // Internal layer between IPersistenceService (that knows what to save/load) 
    //                    and IPersistenceStorage (that knows where&how to save/load)
    // Keeps and updates current profile's name.
    //
    // ! we don't want to throw any unhandled exceptions from this class
    internal sealed class PersistenceProfileService : IProfileMonitor
    {
        private static readonly IMSLogger Logger = MSLoggerFactory.CreateLogger<PersistenceProfileService>();
        internal static LayoutLoadStrategy LayoutLoadStrategy;
        private readonly Dispatcher _currentDispatcher;

        private readonly IItemsDictionaryService _itemsDictionaryService;
        private readonly IStatusBar _statusBar;
        private readonly LayoutLoadingScreenRetriever layoutLoadingScreenRetriever;
        private readonly LayoutLoadingScreenLauncher layoutLoadingScreenLauncher;
        private readonly IPersistenceStorage _storage;
        private readonly StorageAccessorRunner _storageAccessorRunner;
        private readonly IPersistenceProfileServiceProxy _profileServiceProxy;
        private readonly IUnityContainer _container;

        private string _currentProfile;
        private bool _isLayoutLoading;
        private Dictionary<string, XElement> _lastSavedStateOfCurrentProfile = new Dictionary<string, XElement>();

        public event EventHandler ProfileProssessingComplete;
        public event EventHandler BeginProfileProcessing;

        public PersistenceProfileService(
// ReSharper disable CSharpWarnings::CS0612
            IPersistenceStorage storage,
// ReSharper restore CSharpWarnings::CS0612
            IItemsDictionaryService itemsDictionaryService,
            IStatusBar statusBar,
            LayoutLoadingScreenRetriever layoutLoadingScreenRetriever,
            LayoutLoadingScreenLauncher layoutLoadingScreenLauncher,
            IUnityContainer container)
        {
            _storage = storage;
            _itemsDictionaryService = itemsDictionaryService; 
            _statusBar = statusBar;
            this.layoutLoadingScreenRetriever = layoutLoadingScreenRetriever;
            this.layoutLoadingScreenLauncher = layoutLoadingScreenLauncher;
            _currentDispatcher = Dispatcher.CurrentDispatcher;
            _storageAccessorRunner = new StorageAccessorRunner();
            _storageAccessorRunner.ExecutionExceptionHappened += OnExecutionExceptionHappened;

            _profileServiceProxy = _itemsDictionaryService as IPersistenceProfileServiceProxy;
            if (_profileServiceProxy != null)
            {
                _profileServiceProxy.ProfileService = this;
            }
            _container = container;
        }

        public IPersistenceStorage Storage
        {
            get { return _storage; }
        }

        internal StorageAccessorRunner StorageAccessorRunner
        {
            get { return _storageAccessorRunner; }
        }

        #region IProfileMonitor Members

        public string CurrentProfile
        {
            get { return _currentProfile; }
            private set
            {
                var oldvalue = _currentProfile;
                _currentProfile = value;

                var copy = CurrentProfileChanged;
                if (copy != null)
                {
                    DoDispatchedAction(() => copy(this, new CurrentProfileChangedEventArgs(value, oldvalue)));
                }
            }
        }

        public ProfilesCollection CurrentAvailableProfiles
        {
            get
            {
                if (!Bootstrapper.StorageAvailableListStale)
                    _availableProfiles = null;
                // .Count == 0 added to solve some early cache issues with TD
                if (_availableProfiles == null || _availableProfiles.Count == 0)
                {
                    _availableProfiles = new ProfilesCollection();

                    _storage.AvailableProfiles.Aggregate(_availableProfiles, (col, profile) =>
                        {
                            col.Add(profile);
                            return col;
                        });
                }
                return _availableProfiles;
            }
        }


        public ReadOnlyProfilesCollection AvailableProfiles
        {
            get { return _storage.AvailableProfiles; }
        }

        public event EventHandler<CurrentProfileChangedEventArgs> CurrentProfileChanged;
        public event EventHandler<ProfileDeletedEventArgs> ProfileDeleted;
        public event EventHandler<ProfileRenamedEventArgs> ProfileRenamed;
        public event EventHandler<ProfileSavedEventArgs> ProfileSaved;
        public event EventHandler<ProfileLoadingEventArgs> ProfileLoading;
        public event EventHandler<CurrentProfileChangedEventArgs> ProfileLoaded;


        private ProfilesCollection _availableProfiles;

        #endregion

        private bool CheckProfileValidity(string profile)
        {
            if (_storage == null)
            {
                _statusBar.MainItem.UpdateStatus(StatusLevel.Critical, "ProfileStorage is not set!");
                return false;
            }
            if (string.IsNullOrEmpty(profile))
            {
                _statusBar.MainItem.UpdateStatus(StatusLevel.Critical, "Empty profile!");
                return false;
            }
            return true;
        }

        public void Reload()
        {
            InvokeBeginProfileProcessing();

            var stateDict = _itemsDictionaryService.SaveItemsToDict();
            _itemsDictionaryService.RestoreItemsFromDict(stateDict);

            InvokeProfileProcessingComplete();
        }

        public bool CheckIsUnchanged(out Dictionary<string, XElement> dictionary)
        {
            Dictionary<string, XElement> stateDict = _itemsDictionaryService.SaveItemsToDict(ItemScope.Normal);
            dictionary = stateDict;
            if (_lastSavedStateOfCurrentProfile == null)
            {
                return false;
            }
             
            if (stateDict.Keys.Any(key =>
                {
                    if (Bootstrapper.ShellModeUserConfigurable &&
                        (key == ShellWindow.PersistorIdKey || key == ShellWithLauncher.PersistorIdKey || key == RibbonInfragisticsImpl.PersistorIdKey))
                        return false;
                    return !_lastSavedStateOfCurrentProfile.ContainsKey(key);
                }))
            {
                return false;
            }

            if (_lastSavedStateOfCurrentProfile.Keys.Any(key =>
                {
                    if (Bootstrapper.ShellModeUserConfigurable &&
                        (key == ShellWindow.PersistorIdKey || key == ShellWithLauncher.PersistorIdKey || key == RibbonInfragisticsImpl.PersistorIdKey))
                        return false;
                    return !stateDict.ContainsKey(key);
                }))
            {
                return false;
            }

            return stateDict.Keys.All(key =>
            {
                if (Bootstrapper.ThemeUserConfigurable)
                {
                    if (key == ShellWindow.PersistorIdKey|| key == ShellWithLauncher.PersistorIdKey)
                    {
                        if (!_lastSavedStateOfCurrentProfile.ContainsKey(key)) return true;
                        XElement savedState = new XElement(_lastSavedStateOfCurrentProfile[key]);
                        savedState.Attribute("Theme").Remove();
                        XElement currentState = new XElement(stateDict[key]);
                        currentState.Attribute("Theme").Remove();
                        return XNode.DeepEquals(savedState, currentState); 
                    } 
                } 
                //todo: the view state saved needs to be abstracted enough
                return XNode.DeepEquals(_lastSavedStateOfCurrentProfile[key], stateDict[key]); 
            });
        }

        public void DeleteProfile(string profile)
        {
            if (_storage == null)
            {
                _statusBar.MainItem.UpdateStatus(StatusLevel.Critical, "ProfileStorage is not set!");
                return;
            }
            _storageAccessorRunner.Run(
                delegate
                    {
                        try
                        {
                            _statusBar.MainItem.UpdateStatus(StatusLevel.Information, "Deleting layout '" + profile + "'");
                            _storage.DeleteState(profile);
                            if (profile == CurrentProfile)
                            {
                                //???????????? TODO 
                                CurrentProfile = null;
                                _lastSavedStateOfCurrentProfile.Clear();
                            }
                            if (_availableProfiles.Contains(profile))
                                _availableProfiles.Remove(profile);
                            EventHandler<ProfileDeletedEventArgs> copy = ProfileDeleted;
                            if (copy != null)
                            {
                                DoDispatchedAction(() => copy(this, new ProfileDeletedEventArgs(profile)));
                            }
                        }
                        catch (Exception exception)
                        {
                            Logger.ErrorWithFormat("Failed to delete state " + profile + " " + exception.Message);
                            _statusBar.MainItem.UpdateStatus(
                                StatusLevel.Critical,
                                "Failed to delete state " + profile + " " + exception.Message);
                            return;
                        }
                        _statusBar.MainItem.UpdateStatus(StatusLevel.Information,
                                                         string.Format("Layout '{0}' deleted.", profile));
                    });
        }

        public bool RenameProfile(string profile, string newName, bool? async = null)
        {
            if (_storage == null)
            {
                _statusBar.MainItem.UpdateStatus(StatusLevel.Critical, "ProfileStorage is not set!");
                return false;
            }

            bool? oldAsyncToRestore = null;

            var succeeded = true;
            try
            {
                if (async != null && _storageAccessorRunner.Async != async.Value)
                {
                    oldAsyncToRestore = _storageAccessorRunner.Async;
                }

                _storageAccessorRunner.Run(
                    delegate
                        {
                            try
                            {
                                if (MSDesktopLayoutsAdapter.Fps is IPartialPersistenceStorage)
                                {
                                    MSDesktopLayoutsAdapter.Fps.RenameState(profile, newName);
                                }
                                else
                                {
                                    _storage.RenameState(profile, newName);
                                }
                                if (profile == CurrentProfile)
                                {
                                    CurrentProfile = newName;
                                }
                                _availableProfiles.Remove(profile);
                                _availableProfiles.Add(newName);
                                EventHandler<ProfileRenamedEventArgs> copy = ProfileRenamed;
                                if (copy != null)
                                {
                                    DoDispatchedAction(() => copy(this, new ProfileRenamedEventArgs(profile, newName)));
                                }
                            }
                            catch (Exception exception)
                            {
                                Logger.ErrorWithFormat("Failed to rename state " + profile + " " + exception.Message);
                                _statusBar.MainItem.UpdateStatus(
                                    StatusLevel.Critical,
                                    "Failed to rename state " + profile + " " + exception.Message);
                                succeeded = false;
                            }
                        });
            }
            finally
            {
                if (oldAsyncToRestore != null)
                {
                    _storageAccessorRunner.Async = oldAsyncToRestore.Value;
                }
            }
            return succeeded;
        }

        internal void DoDispatchedAction(Action action, bool synchronous=true)
        {
            if (_currentDispatcher.CheckAccess())
            {
                action.Invoke();
            }
            else
            {
                var result = _currentDispatcher.BeginInvoke(DispatcherPriority.Normal, action);
                if (synchronous)
                {
                    result.Wait();
                }
            }
        }
         
        private void OnExecutionExceptionHappened(object sender, ExecutionExceptionEventArgs e)
        {
            Logger.ErrorWithFormat(e.Exception.Message);
            _statusBar.MainItem.UpdateStatus(StatusLevel.Critical,
                                             string.Format(
                                                 "Error: {0}",
                                                 e.Exception.Message));
        }

        internal void WaitForEmptyQueue()
        {
            _storageAccessorRunner.WaitForEmptyQueue();
        }

        private bool needToResaveProfile = false;
        //this hack here is to fix the fact the ConcordForm would resized to be different than the saving in the layout file
        //so if we want to test if the layout is changed (in the case LayoutLoadStrategy == LayoutLoadStrategy.Default), it would always use the real one
        //In the case of Synchronous layout loading, this ConcordFormSizeChanged event would be received after the layout of the windows is loaded, so we could invoke a layout saving instantly here;
        //In the case of Asynchronous layout loading, this ConcordFormSizeChanged event would happen during the layout loading of windows, so we postponed it until the layout loading finishes
        internal void ConcordFormSizeChanged(object sender, EventArgs e)
        {
            if (_isLayoutLoading && (LayoutLoadStrategy == LayoutLoadStrategy.Default))
            {
                if (!ChromeManagerBase.AsynchronousLayoutLoading)
                {
                    _lastSavedStateOfCurrentProfile = _itemsDictionaryService.SaveItemsToDict(ItemScope.Normal);
                    _isLayoutLoading = false;               
                }
                else
                {
                    needToResaveProfile = true;
                    
                }
            }
        }

        #region static

        //Calls: 
        //1) XDocumentToDict --> UnpackItem
        //2) DictToXDocument --> PackItem -> BuildXPath --> BuildParentPath --> FindOrder

        internal static XElement PackItem(string itemKey, XElement element, bool removeSubitems)
        {
            var elementToAdd = new XElement(element);
            var itemElement = new XElement("Item",
                                           new XAttribute("Id", itemKey));
            itemElement.Add(elementToAdd);
            if (element is XElementWithSubitems)
            {
                Dictionary<string, XElement> subitems = ((XElementWithSubitems) element).Subitems;
                var subitemsElement = new XElement("Subitems");
                foreach (string key in subitems.Keys)
                {
                    string xpath = BuildXPath(element, subitems[key]);
                    subitemsElement.Add(
                        new XElement("Subitem",
                                     new XAttribute("Id", key),
                                     new XAttribute("XPath", xpath)));
                    if (removeSubitems)
                    {
                        XElement subitemElement = elementToAdd.XPathSelectElement(xpath);
                        subitemElement.ReplaceWith(new XElement(subitemElement.Name.LocalName));
                    }
                }
                itemElement.Add(subitemsElement);
            }
            return itemElement;
        }

        internal static XDocument DictToXDocument(Dictionary<string, XElement> dict, XDocument exisitngState = null)
        {
            XElement rootElement = null;
            if (exisitngState != null)
            {
                rootElement = exisitngState.Root;
            }
            if (rootElement == null)
            {
                rootElement = new XElement("State");
            }
            foreach (string itemKey in dict.Keys)
            {
                try
                {
                    rootElement.Add(PackItem(itemKey, dict[itemKey], false));
                }
                catch (Exception exception)
                {
                    Logger.ErrorWithFormat("Failed to process item {0}. Error: {1}", itemKey, exception.Message);
                }
            }
            return new XDocument(rootElement);
        }

        internal static KeyValuePair<string, XElement> UnpackItem(XElement itemXElement)
        {
            string id = itemXElement.Attribute("Id").Value;
            var elements = new List<XElement>(itemXElement.Elements());
            switch (elements.Count)
            {
                case 1:
                    return new KeyValuePair<string, XElement>(id, elements[0]);
                case 2:
                    var result = new XElementWithSubitems(elements[0]);
                    foreach (XElement subelementNode in elements[1].Elements("Subitem"))
                    {
                        result.Subitems[subelementNode.Attribute("Id").Value] =
                            result.XPathSelectElement(subelementNode.Attribute("XPath").Value);
                    }
                    return new KeyValuePair<string, XElement>(id, result);
            }
            throw new InvalidOperationException("Unxepected Item xml format");
        }

        internal static Dictionary<string, XElement> XDocumentToDict(XDocument document)
        {
            if (document.Root == null)
            {
                return new Dictionary<string, XElement>();
            }

            //return document_.Root.Elements("Item").
            //  Where(node_ => (string) node_.Attribute("Id") != null).
            //  Select(UnpackItem).
            //  ToDictionary(pair_ => pair_.Key, pair_ => pair_.Value);
            var result = new Dictionary<string, XElement>();
            foreach (var element in document.Root.Elements("Item").Where(node => (string) node.Attribute("Id") != null))
            {
                KeyValuePair<string, XElement> unpacked;
                try
                {
                    unpacked = UnpackItem(element);
                }
                catch (Exception exception)
                {
                    Logger.ErrorWithFormat("Failed to unpack state item {0}. Error: {1}", element.ToString(), exception);
                    continue;
                }
                result[unpacked.Key] = unpacked.Value;
            }
            return result;
        }

        private static string BuildXPath(XElement parentNode, XElement childNode)
        {
            if (parentNode == childNode)
            {
                return ".";
            }

            if (parentNode.Descendants().Contains(childNode))
            {
                return BuildParentPath(parentNode, childNode);
            }
            throw new Exception("BuildXPath error");
        }

        private static string BuildParentPath(XElement parentNode, XElement childNode)
        {
            if (childNode == parentNode)
            {
                return string.Empty;
            }
            string prefix = BuildParentPath(parentNode, childNode.Parent);
            if (!string.IsNullOrEmpty(prefix))
            {
                prefix = prefix + "/";
            }

            return string.Format("{0}{1}[{2}]", prefix, childNode.Name.LocalName, FindOrder(childNode));
        }

        private static int FindOrder(XElement childNode)
        {
            return childNode.ElementsBeforeSelf(childNode.Name.LocalName).Count() + 1;
        }

        #endregion

        #region Saving

        public void SaveCurrentProfile()
        {
            if (string.IsNullOrEmpty(CurrentProfile))
            {
                //we don't throw any exceptions here, presuming that emptiness of the current profile is checked before the call        
                //we don't want to throw any unhandled exceptions from this class
                _statusBar.MainItem.UpdateStatus(StatusLevel.Critical, "Current profile's name is not specified");
                return;
            }

            SaveCurrentProfileAs(CurrentProfile);
        }

        public void SetCurrentProfile(string profile)
        {
            CurrentProfile = profile;
            _lastSavedStateOfCurrentProfile = new Dictionary<string, XElement>();
        }

        public void SaveCurrentProfileAs(string profile)
        {
            if (!CheckProfileValidity(profile))
                return;

            Dictionary<string, XElement> savedStateDict = _itemsDictionaryService.SaveItemsToDict(ItemScope.Normal);
            _statusBar.MainItem.UpdateStatus(StatusLevel.Information, "Saving layout '" + profile + "'");

            SaveItemsDictionaryAs(savedStateDict, profile);

            CurrentProfile = profile;

            //left for historical reasons
            if (profile == CurrentProfile)
            {
                _lastSavedStateOfCurrentProfile = savedStateDict;
            }
            _statusBar.MainItem.UpdateStatus(StatusLevel.Information, string.Format("Layout '{0}' saved.", profile));
            if (!_availableProfiles.Contains(profile))
                _availableProfiles.Add(profile);
        }

        public void SaveCurrentProfileAs(string name, string format)
        {
            if (!(_storage is IMultiformatPersistenceStorage))
            {
                const string message = "Current storage does not support multiple formats!";
                Logger.ErrorWithFormat(message);
                _statusBar.MainItem.UpdateStatus(StatusLevel.Critical, message);
            }
            var multiStorage = ((IMultiformatPersistenceStorage) _storage);

            var fullname = string.Format("{0}{1}", name, multiStorage.GetSuffix(format));

            if (!CheckProfileValidity(fullname))
                return;

            var savedStateDict = _itemsDictionaryService.SaveItemsToDict(ItemScope.Normal);
            _statusBar.MainItem.UpdateStatus(StatusLevel.Information, "Saving profile " + fullname);

            var stateDoc = DictToXDocument(savedStateDict);
            _storageAccessorRunner.Run(
                delegate
                    {
                        try
                        {
                            multiStorage.SaveStateInFormat(stateDoc, name, format, fullname);
                            if (!_availableProfiles.Contains(name))
                                _availableProfiles.Add(name);
                        }
                        catch (Exception exception)
                        {
                            var message = "Failed to save state " + fullname + " " + exception.Message;
                            Logger.ErrorWithFormat(message);
                            _statusBar.MainItem.UpdateStatus(StatusLevel.Critical, message);
                        }
                    });
            CurrentProfile = CurrentProfile; //trigger refresh
        }

        public void SaveCurrentProfileForKeys(IEnumerable<string> keys)
        {
            SaveCurrentProfileAsForKeys(CurrentProfile, keys);
        }

        public void SaveCurrentProfileAsForKeys(string profile, IEnumerable<string> keys)
        {
            //TODO i'm not sure what is better here. Maybe we should check if there
            //exists a profile with such name and use it's last saved state instead of the current      
            SaveItemsDictionaryAndRestItemsFromPrevious(_itemsDictionaryService.SaveItemsToDict(keys), profile);
            //Should we change the current profile here?
        }

        private void ResaveLastSaved(string profile)
        {
            SaveItemsDictionaryAs(_lastSavedStateOfCurrentProfile, profile);
        }

        public void SaveSubitem(
            string itemId,
            string subitemId,
            XElement subitemXml,
            Action<XElementWithSubitems, XElement> addNewSubItem,
            XElement cleanItemXml)
        {
            SaveSubitemAs(itemId, subitemId, subitemXml, addNewSubItem, cleanItemXml, CurrentProfile);
        }

        public void SaveSubitemAs(
            string itemId,
            string subitemId,
            XElement subitemXml,
            Action<XElementWithSubitems, XElement> addNewSubItem,
            XElement cleanItemXml,
            string profile)
        {
            if (!_lastSavedStateOfCurrentProfile.ContainsKey(itemId))
            {
                var cleanWithSubitemsXml = new XElementWithSubitems(cleanItemXml);
                addNewSubItem(cleanWithSubitemsXml, subitemXml);
                cleanWithSubitemsXml.Subitems[subitemId] = subitemXml;
                _lastSavedStateOfCurrentProfile.Add(itemId, cleanWithSubitemsXml);
            }
            else
            {
                if (!(_lastSavedStateOfCurrentProfile[itemId] is XElementWithSubitems))
                {
                    //TODO we are in trouble. throw an exception?
                    return;
                }
                var itemElement = _lastSavedStateOfCurrentProfile[itemId] as XElementWithSubitems;
                if (itemElement.Subitems.ContainsKey(subitemId))
                {
                    ReplaceSubitem(itemElement, subitemId, subitemXml);
                }
                else
                {
                    addNewSubItem(itemElement, subitemXml);
                    itemElement.Subitems[subitemId] = subitemXml;
                }
            }
            ResaveLastSaved(profile);
        }

        internal static void ReplaceSubitem(XElementWithSubitems xelement, string subitemId, XElement replacement)
        {
            replacement = new XElement(replacement);
            xelement.Subitems[subitemId].ReplaceWith(replacement);
            xelement.Subitems[subitemId] = replacement;
        }

        private void SaveDictionaryForPartialStorage(Dictionary<string, XElement> dict, string profile, bool clean)
        {
            _storageAccessorRunner.Run(
                delegate
                    {
                        try
                        {
                            var partStorage = _storage as IPartialPersistenceStorage ??
                                              MSDesktopLayoutsAdapter.Fps as IPartialPersistenceStorage;
                            if (partStorage == null)
                                return;
                            var itemsToSave = new Dictionary<string, XElement>();
                            var subitemsToSave = new Dictionary<string, XElement>();
                            foreach (string key in dict.Keys)
                            {
                                XElement itemXelement;
                                try
                                {
                                    itemXelement = PackItem(key, dict[key], true);
                                }
                                catch (Exception exception)
                                {
                                    Logger.ErrorWithFormat("Failed to save item {0} for profile {1}. Error: {2}", key,
                                                            profile,
                                                            exception.Message);
                                    _statusBar.MainItem.UpdateStatus(StatusLevel.Critical,
                                                                     string.Format(
                                                                         "Failed to save item {0} for profile {1}. Error: {2}",
                                                                         key, profile, exception.Message));
                                    continue;
                                }
                                if (dict[key] is XElementWithSubitems)
                                {
                                    var withSub = (XElementWithSubitems) dict[key];
                                    foreach (string subitemKey in withSub.Subitems.Keys)
                                    {
                                        subitemsToSave[subitemKey] = withSub.Subitems[subitemKey];
                                    }
                                }
                                itemsToSave[key] = itemXelement;
                            }
                            partStorage.SaveState(profile, itemsToSave, subitemsToSave, clean);
                            if (!_availableProfiles.Contains(profile))
                                _availableProfiles.Add(profile);
                            if (MSDesktopLayoutsAdapter.Fps != null)
                            {
                                ConfigurationManagerHelper.DirtyConfiguration("MSDesktopLayouts"); 
                            }
                        }
                        catch (Exception exception)
                        {
                            Logger.ErrorWithFormat("Failed to save state " + profile + " " + exception.Message);
                            _statusBar.MainItem.UpdateStatus(
                                StatusLevel.Critical,
                                "Failed to save state " + profile + " " + exception.Message);
                            return;
                        }
                    });
        }

        private void SaveDictionaryForNonPartialStorage(Dictionary<string, XElement> dict_, string profile_,
                                                        IEnumerable<string> keys_ = null)
        {

            _storageAccessorRunner.Run(
                delegate
                    {
                        try
                        {
                            XDocument stateDoc = DictToXDocument(dict_,
                                                                 keys_ == null ? null : _storage.LoadState(profile_));
                            _storage.SaveState(stateDoc, profile_);
                            if (!_availableProfiles.Contains(profile_))
                                _availableProfiles.Add(profile_);
                        }
                        catch (Exception exception)
                        {
                            Logger.ErrorWithFormat("Failed to save state " + profile_ + " " +
                                                    exception.Message);
                            _statusBar.MainItem.UpdateStatus(
                                StatusLevel.Critical,
                                "Failed to save state " + profile_ + " " + exception.Message);
                            return;
                        }
                    });
        }

        private void SaveItemsDictionaryAndRestItemsFromPrevious(Dictionary<string, XElement> savedStateDict,
                                                                 string profile)
        {
            if (!CheckProfileValidity(profile))
                return;

            var combinedDict = new Dictionary<string, XElement>();
            foreach (string key in _lastSavedStateOfCurrentProfile.Keys)
            {
                combinedDict[key] = _lastSavedStateOfCurrentProfile[key];
            }

            foreach (string key in savedStateDict.Keys)
            {
                combinedDict[key] = savedStateDict[key];
            }

            if (_storage is IPartialPersistenceStorage || MSDesktopLayoutsAdapter.Fps is IPartialPersistenceStorage)
            {
                SaveDictionaryForPartialStorage(savedStateDict, profile, false);
            }
            else
            {
                SaveDictionaryForNonPartialStorage(combinedDict, profile);
            }

            if (profile == CurrentProfile)
            {
                _lastSavedStateOfCurrentProfile = combinedDict;
            }
        }

        internal void SaveItemsDictionaryAs(Dictionary<string, XElement> dictionary, string profile, IEnumerable<string> keys = null)
        {
            //!!!we don't change the current profile here
            if (_storage is IPartialPersistenceStorage || MSDesktopLayoutsAdapter.Fps is IPartialPersistenceStorage)
            {
                SaveDictionaryForPartialStorage(dictionary, profile, keys == null);
            }
            else
            {
                SaveDictionaryForNonPartialStorage(dictionary, profile, keys);
            }
             

            //!!!we don't change the current profile here 
            var copy = ProfileSaved;
            if (copy != null)
            {
                DoDispatchedAction(() => copy(this, new ProfileSavedEventArgs(profile)));
            }
        }

        internal void SaveItemsDictionary(Dictionary<string, XElement> dictionary)
        {
            SaveItemsDictionaryAs(dictionary, CurrentProfile);
        }

        #endregion

        #region Loading

        internal XDocument GetProfileItem(string profile, string key)
        {
            XDocument state;
            try
            {
                var storage = MSDesktopLayoutsAdapter.Fps as IPartialPersistenceStorage;
                if (storage != null)
                {
                    state = storage.LoadState(profile);
                }
                else
                {
                    state = _storage.LoadState(profile);
                }
            }
            catch (Exception exception)
            {
                Logger.ErrorWithFormat("Failed to load state " + profile + " " + exception.Message);
                _statusBar.MainItem.UpdateStatus(
                    StatusLevel.Critical,
                    "Failed to load state " + profile + " " + exception.Message);
                return null;
            }

            if (state != null)
            {
                var dict = XDocumentToDict(state);
                XElement result;
                if (dict.TryGetValue(key, out result))
                {
                    return new XDocument(result);
                }
            }
            return null;
        }

        
        public void LoadProfile(string profile, bool startUp, Action beforeAction=null, Action afterAction=null)
        {
            Action beforeLoadLayoutAction = null;
            var application = _container.Resolve<IApplication>();
            if (this._profileServiceProxy != null && this._profileServiceProxy.Shell != null)
            {
                //hide loading windows offscreen
                beforeLoadLayoutAction = () =>
                    {
                        if (ChromeManagerBase.AsynchronousLayoutLoading)
                        {
                            _profileServiceProxy.Shell.MainMenu.IsEnabled = false;
                            _profileServiceProxy.Shell.DisableClose = true;
                        }
                        FloatingWindowUtil.HideLoadingWindows = true;
                        if (beforeAction != null)
                        {
                            beforeAction();
                        }
                         
                        if (startUp)
                        {
                            if (!Framework.SkipSplashScreen)
                            {
                                //_container.Resolve<ISplashScreen>().BringToFront();
                                _container.Resolve<ISplashScreen>().SetText("Loading layout" + (string.IsNullOrEmpty(profile) ? "" : (" '" + profile + "'")));
  
                            } 
                        } 

                    };
            }
            else
            {
                beforeLoadLayoutAction = beforeAction;
            }
            Action afterLoadLayoutAction = null;
            if (this._profileServiceProxy != null && this._profileServiceProxy.Shell != null)
            {
                //this is needed for the windows to show up onscreen - to avoid flickering they are placed left at -5000 when created.
                afterLoadLayoutAction = () =>
                    {
                        try
                        {
                            if (ChromeManagerBase.AsynchronousLayoutLoading)
                            { 
                                //foreach (var pane in _profileServiceProxy.Shell.DockManager.GetPanes(PaneNavigationOrder.ActivationOrder))
                                //{
                                //    pane.Activate();
                                //    break;
                                //}
                            }
                            if (startUp)
                            {
                                if (!DelayedProfileLoadingExtensionPoints.DelayLoadProfile)
                                {
                                    _container.Resolve<IStatusBar>().MainItem.UpdateStatus(StatusLevel.Information, "Ready");
                                }
                                ((Application.Application)application).InvokeApplicationLoaded();
                                if (!Framework.SkipSplashScreen)
                                {
                                     _container.Resolve<ISplashScreen>().Close(); 
                                    if (ChromeManagerBase.AcquireFocusAfterStartup)
                                    {
                                        _profileServiceProxy.Shell.MainWindow.Activate();
                                    }
                                }
                                else
                                {
                                    if (InitialSplashWindowFrameworkWrapper.InitialWindow != null)
                                    {
                                        InitialSplashWindowFrameworkWrapper.InitialWindow.Stop();
                                    }
                                }
                            }
                            //this is needed for the windows to show up onscreen - to avoid flickering they are placed left at -5000 when created.
                            FloatingWindowUtil.DoShowWindows();
                            FloatingWindowUtil.FixInvisibleWindows(); 

                        }
                        finally
                        {
                            if (afterAction != null)
                            {
                                afterAction();
                            }
                            if (ChromeManagerBase.AsynchronousLayoutLoading)
                            {
                                _profileServiceProxy.Shell.MainMenu.IsEnabled = true;
                            }


                            _profileServiceProxy.Shell.DisableClose = false;
                             

                           
                        }
                    };
            }
            else
            {
                afterLoadLayoutAction = afterAction;
            }
            LoadProfileCore(profile, beforeLoadLayoutAction, afterLoadLayoutAction);
        }

        public void LoadProfile(string profile, IEnumerable<string> keys = null)
        {
             LoadProfileCore(profile, null, null, keys);
        }

        private void LoadProfileCore(string profile, Action beforeLoadLayoutAction, Action afterLoadLayoutAction,
                                     IEnumerable<string> keys = null)
        {
            _currentDispatcher.Invoke(InvokeBeginProfileProcessing);

            if (keys == null)
            { 
                var copy = ProfileLoading;
                if (copy != null)
                {
                    var args = new ProfileLoadingEventArgs(CurrentProfile, profile);
                    copy(this, args);
                    if (args.Cancel) return;
                } 
                DoDispatchedAction(() =>
                {
                    if (beforeLoadLayoutAction != null)
                    {
                        beforeLoadLayoutAction();
                    }
                });  
            }

            _isLayoutLoading = true;
            needToResaveProfile = false;
            var oldAsynchronous = _storageAccessorRunner.Async;
            
            if (keys == null)
            {
                _storageAccessorRunner.Async = ChromeManagerBase.AsynchronousLayoutLoading;
            }

            _storageAccessorRunner.Run(
                delegate
                {
                    try
                    {
                        if (profile == null)
                        {
                            try
                            {
                                _itemsDictionaryService.RestoreItemsFromDict(new Dictionary<string, XElement>(),
                                                                             ItemScope.Normal, keys);
                            }
                            catch (Exception exception)
                            {
                                _storageAccessorRunner.InvokeExceptionEvent(exception);
                            }
                            return;
                        }
                        XDocument state; 
                        try
                        {
                            if (keys == null)
                            {
                                _statusBar.MainItem.UpdateStatus(StatusLevel.Information, "Loading layout" + (string.IsNullOrEmpty(profile) ? " 'Default'" : (" '" + profile + "'")));
                                if (ShowLayoutLoadingScreen && !layoutLoadingScreenRetriever.IsSplashScreenVisible())
                                {
                                    layoutLoadingScreenLauncher.Show(profile);
                                }
                            }
                            var storage = MSDesktopLayoutsAdapter.Fps as IPartialPersistenceStorage;
                            if (storage != null)
                            {
                                state = storage.LoadState(profile);
                            }
                            else
                            {
                                state = _storage.LoadState(profile);
                            }
                        }
                        catch (Exception exception)
                        {
                            Logger.ErrorWithFormat("Failed to load state " + profile + " " + exception.Message);
                            _statusBar.MainItem.UpdateStatus(
                                StatusLevel.Critical,
                                "Failed to load state " + profile + " " + exception.Message);
                            return;
                        }

                        if (state != null)
                        {
                            var dict = XDocumentToDict(state);
                            var orderedItems = OrderItemsForRestore(dict);
                            try
                            {
                                _itemsDictionaryService.RestoreItemsFromDict(orderedItems, ItemScope.Normal, keys);
                            }
                            catch (Exception exception)
                            {
                                _storageAccessorRunner.InvokeExceptionEvent(exception);
                            }
                            if (keys == null)
                            {
                                _lastSavedStateOfCurrentProfile = dict;

                            }
                        }

                        if (keys == null)
                        {
                            var oldProfile = CurrentProfile;
                            CurrentProfile = profile;

                            var copy = ProfileLoaded;
                            if (copy != null)
                            {
                                var args = new CurrentProfileChangedEventArgs(CurrentProfile, oldProfile);
                                copy(this, args); 
                            } 

                            _statusBar.MainItem.UpdateStatus(StatusLevel.Information,
                                                             string.Format("Layout '{0}' loaded.", profile));
                            if (ShowLayoutLoadingScreen && !layoutLoadingScreenRetriever.IsSplashScreenVisible())
                            {
                                layoutLoadingScreenLauncher.Close();
                            }

                        }

                    }
                    finally
                    {
                        _storageAccessorRunner.Async = oldAsynchronous; 

                        if (keys == null)
                        {
                            DoDispatchedAction(() =>
                            {
                                if (afterLoadLayoutAction != null)
                                {
                                    afterLoadLayoutAction();
                                }
                            });
                        }
                        _isLayoutLoading = false;
                        //resave the profile in the case of asynchronous loading as a fix for concord forms
                        DoDispatchedAction(() =>
                            {
                                if (needToResaveProfile && ChromeManagerBase.AsynchronousLayoutLoading)
                                {
                                    _lastSavedStateOfCurrentProfile =
                                        _itemsDictionaryService.SaveItemsToDict(ItemScope.Normal);
                                }
                            });

                    }
                });

            // Fire the ProcessingComplete event
            _currentDispatcher.Invoke(InvokeProfileProcessingComplete);
        }

        internal static bool ShowLayoutLoadingScreen { get; set; }

        private void InvokeProfileProcessingComplete()
        {
            var handler = ProfileProssessingComplete;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        private void InvokeBeginProfileProcessing()
        {
            var handler = BeginProfileProcessing;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        private static IEnumerable<KeyValuePair<string, XElement>> OrderItemsForRestore(IDictionary<string, XElement> items)
        {
            string[] separateOrderItems = {"MSGui.Internal.ShellWindow", "MSGui.Internal.ViewManager", "MSGui.Internal.Ribbon"};

            var specialKeyValuePairs = new KeyValuePair<string, XElement>?[separateOrderItems.Length];
            for (var k = 0; k < separateOrderItems.Length; k++)
            {
                XElement itemContent;
                if (items.TryGetValue(separateOrderItems[k], out itemContent))
                {
                    specialKeyValuePairs[k] = new KeyValuePair<string, XElement>(separateOrderItems[k], itemContent);
                }
            }

            foreach (var internalItem in separateOrderItems)
            {
                if (items.ContainsKey(internalItem))
                {
                    items.Remove(internalItem);
                }
            }

            foreach (var specialKeyValuePair in specialKeyValuePairs)
            {
                if (specialKeyValuePair != null)
                {
                    yield return specialKeyValuePair.Value;
                }
            }

            foreach (var kvp in items)
            {
                yield return kvp;
            }
        }

        #endregion
    }

    internal interface IPersistenceProfileServiceProxy
    {
        PersistenceProfileService ProfileService { get; set; }
        IShell Shell { get; set; }
    }
}
