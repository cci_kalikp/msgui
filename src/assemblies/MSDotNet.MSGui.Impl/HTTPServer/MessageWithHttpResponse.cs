﻿using System;
using System.ComponentModel;
using MorganStanley.MSDotNet.MSGui.Core.Messaging; 

namespace MorganStanley.MSDotNet.MSGui.Impl.HTTPServer
{
    [Serializable]
    public abstract class MessageWithHttpResponse<TResponse> : IMessageWithHttpResponse
        where TResponse : IMessageResponse, new()
    {

        public virtual TResponse GetResponse(string content_)
        {
            return new TResponse() { Content = content_, Sequence = this.Sequence };
        }
         
        [DefaultValue(null)]
        public string Sequence { get; set; }


        string IMessageWithHttpResponse.NewSequence(string publisherId_)
        {
            Sequence = publisherId_ + "_" + Guid.NewGuid().ToString();
            return Sequence;
        }

        Type IMessageWithHttpResponse.ResponseType
        {
            get { return typeof(TResponse); }
        }
    }
}
