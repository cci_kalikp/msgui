﻿using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Text;
using MSDesktop.HTTPServer;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.MicrosoftShell.Standard;
using System.Xml.Serialization;
using System.IO;
using System.Xml.Linq;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MorganStanley.MSDotNet.MSGui.Impl.HTTPServer
{
    public class HttpRequestBuilder
    {
        internal const string FactoryParameter = "factory";
        internal const string StateParameter = "state";
        internal const string SessionIdParameter = "id";
        internal const string MessageTypeParameter = "type";
        internal const string MessageParameter = "data";
        internal const string ViewIdParameter = "viewid";
        internal const string ModuleNameParameter = "module";
        internal const string CallbackParameter = "callback";
        internal const string ZippedParameter = "zip";
        private const int CompressThreshold = 1500;
        public const string Localhost = "localhost";
        private readonly int port = 8080;
        private readonly string host;
        
        private readonly List<IUrlProtector> protectors = new List<IUrlProtector>();
        private readonly List<IUrlProtector> protectorsReversed = new List<IUrlProtector>();
        public HttpRequestBuilder(string host_, int port_, bool enableHash_, string hashSeed_)
        {
            host = host_;
            port = port_;
            //this would protect the URL Request from being Tampered 
            if (enableHash_)
            { 
                RegisterUrlProtector(new UrlHashProcessor(hashSeed_));
            } 
        }

        private void RegisterUrlProtector(IUrlProtector protector_)
        {
            protectors.Add(protector_);
            protectorsReversed.Insert(0, protector_);
        }

        public string BaseUrl
        {
            get
            {
                return "http://" + host + ":" + port + "/";
            }
        }
         

        #region Http Server Request Url Builders
        public string BuildCreateViewUrl(string factory_, XDocument state_)
        {
            if (string.IsNullOrWhiteSpace(factory_))
            {
                throw new ArgumentNullException("factory_");
            }
            StringBuilder url = new StringBuilder(BaseUrl);
            url.Append(ConvertToActionAPI(ActionType.CreateView));
            url.Append("?");
            url.AppendFormat("{0}={1}", FactoryParameter, Utility.UrlEncode(factory_));
            if (state_ != null)
            {
                url.AppendFormat("&{0}={1}", StateParameter, Utility.UrlEncode(state_.ToString()));
            }
             
            return ProtectUrl(url.ToString());
        }

        #region M2M Communication Request Url Builders
        public string BuildRegisterPulisherUrl(Type messageType_, ref string publisherId)
        {
            if (messageType_ == null)
            {
                throw new ArgumentNullException("messageType_");
            }
            StringBuilder url = new StringBuilder(BaseUrl);
            url.Append(ConvertToActionAPI(ActionType.RegisterPublisher));
            url.Append("?");
            url.AppendFormat("{0}={1}", MessageTypeParameter,  Utility.UrlEncode(messageType_.AssemblyQualifiedName));
            publisherId = string.IsNullOrEmpty(publisherId) ? Guid.NewGuid().ToString("N"): publisherId;
            url.AppendFormat("&{0}={1}", SessionIdParameter, publisherId);
            return ProtectUrl(url.ToString()); 
        }

        public string BuildRegisterSubscriberUrl(string moduleName_, string callbackMethod_, Type messageType_, ref string subscriberId_)
        {
            if (string.IsNullOrWhiteSpace(moduleName_))
            {
                throw new ArgumentNullException("moduleName_");
            } 
            if (messageType_ == null)
            {
                throw new ArgumentNullException("messageType_");
            }
            StringBuilder url = new StringBuilder(BaseUrl);
            url.Append(ConvertToActionAPI(ActionType.RegisterSubscriber));
            url.Append("?");
            url.AppendFormat("{0}={1}", ModuleNameParameter, moduleName_);
            if (!string.IsNullOrWhiteSpace(callbackMethod_))
            {
                url.AppendFormat("&{0}={1}", CallbackParameter, callbackMethod_);
            }
            url.AppendFormat("&{0}={1}", MessageTypeParameter, Utility.UrlEncode(messageType_.AssemblyQualifiedName));
            subscriberId_ = string.IsNullOrEmpty(subscriberId_) ? Guid.NewGuid().ToString("N") : subscriberId_;
            url.AppendFormat("&{0}={1}", SessionIdParameter, subscriberId_);
            return ProtectUrl(url.ToString());
        }


        public string BuildPulishUrl(object message_, DataFormat format_, string publisherId_)
        {
            if (message_ == null)
            {
                throw new ArgumentNullException("message_");
            }
            if (string.IsNullOrWhiteSpace(publisherId_))
            {
                throw new ArgumentNullException("publisherId_");
            }

            StringBuilder url = new StringBuilder(BaseUrl);
            url.Append(ConvertToActionAPI(format_ == DataFormat.Xml ? ActionType.PublishXml : ActionType.PublishJson));
            url.Append("?");
            string message = SerializeToString(message_, format_);
            bool zipped = false;
            if (message.Length > CompressThreshold) //needs to be zipped
            {
                message = Zip(message);
                zipped = true;
            }
            url.AppendFormat("{0}={1}", MessageParameter, Utility.UrlEncode(message));
            if (zipped)
            {
                url.AppendFormat("&{0}=1", ZippedParameter);
            }
            url.AppendFormat("&{0}={1}", SessionIdParameter, publisherId_);
            return ProtectUrl(url.ToString());
        }
        #endregion

        #region V2V Communication Request Url Builders
        public string BuildCreatePulisherViewUrl(string factory_, XDocument state_, Type messageType_, out string publisherId_)
        {
            if (string.IsNullOrWhiteSpace(factory_))
            {
                throw new ArgumentNullException("factory_");
            }
            if (messageType_ == null)
            {
                throw new ArgumentNullException("messageType_");
            }
            if (state_ == null)
            {
                state_ = new XDocument(); 
                XElement root = new XElement("state");
                state_.Add(root);
            }
            publisherId_ = Guid.NewGuid().ToString("N");
            XElement publisherIdElement = new XElement(SessionIdParameter, publisherId_);
            state_.Root.Add(publisherIdElement);

            StringBuilder url = new StringBuilder(BaseUrl);
            url.Append(ConvertToActionAPI(ActionType.CreatePulisherView));
            url.Append("?");
            url.AppendFormat("{0}={1}", FactoryParameter, Utility.UrlEncode(factory_));
            url.AppendFormat("&{0}={1}", StateParameter, Utility.UrlEncode(state_.ToString())); 
            url.AppendFormat("&{0}={1}", MessageTypeParameter, Utility.UrlEncode(messageType_.AssemblyQualifiedName)); 
            url.AppendFormat("&{0}={1}", SessionIdParameter, publisherId_); 
            return ProtectUrl(url.ToString()); 
        }

        public string BuildCreateSubscriberViewUrl(string factory_, XDocument state_, string callbackMethod_, Type messageType_, out string subscriberId_)
        {
            if (string.IsNullOrWhiteSpace(factory_))
            {
                throw new ArgumentNullException("factory_");
            }
            if (string.IsNullOrWhiteSpace(callbackMethod_))
            {
                throw new ArgumentNullException("callbackMethod_");
            }
            if (messageType_ == null)
            {
                throw new ArgumentNullException("messageType_");
            }

            if (state_ == null)
            {
                state_ = new XDocument();
                XElement root = new XElement("state");
                state_.Add(root);
            }

            subscriberId_ = Guid.NewGuid().ToString("N");
            XElement publisherIdElement = new XElement(SessionIdParameter, subscriberId_);
            state_.Root.Add(publisherIdElement);

            StringBuilder url = new StringBuilder(BaseUrl);
            url.Append(ConvertToActionAPI(ActionType.CreateSubscriberView));
            url.Append("?");
            url.AppendFormat("{0}={1}", FactoryParameter, Utility.UrlEncode(factory_));
            url.AppendFormat("&{0}={1}", StateParameter, Utility.UrlEncode(state_.ToString())); 
            url.AppendFormat("&{0}={1}", CallbackParameter, callbackMethod_);
            url.AppendFormat("&{0}={1}", MessageTypeParameter, Utility.UrlEncode(messageType_.AssemblyQualifiedName)); 
            url.AppendFormat("&{0}={1}", SessionIdParameter, subscriberId_); 
            return ProtectUrl(url.ToString()); 
        }


        public string BuildRegisterV2VPulisherUrl(string viewId_, Type messageType_, out string publisherId_)
        {
            if (string.IsNullOrWhiteSpace(viewId_))
            {
                throw new ArgumentNullException("viewId_");
            }
            if (messageType_ == null)
            {
                throw new ArgumentNullException("messageType_");
            }
            StringBuilder url = new StringBuilder(BaseUrl);
            url.Append(ConvertToActionAPI(ActionType.RegisterV2VPublisher));
            url.Append("?");
            url.AppendFormat("{0}={1}", ViewIdParameter, viewId_);
            url.AppendFormat("&{0}={1}", MessageTypeParameter, Utility.UrlEncode(messageType_.AssemblyQualifiedName));
            publisherId_ = Guid.NewGuid().ToString("N");
            url.AppendFormat("&{0}={1}", SessionIdParameter, publisherId_);  
            return ProtectUrl(url.ToString());  
        }

        public string BuildRegisterV2VSubscriberUrl(string viewId_, string callbackMethod_, Type messageType_, out string subscriberId_)
        {
            if (string.IsNullOrWhiteSpace(viewId_))
            {
                throw new ArgumentNullException("viewId_");
            }
            if (string.IsNullOrWhiteSpace(callbackMethod_))
            {
                throw new ArgumentNullException("callbackMethod_");
            }
            if (messageType_ == null)
            {
                throw new ArgumentNullException("messageType_");
            }
            StringBuilder url = new StringBuilder(BaseUrl);
            url.Append(ConvertToActionAPI(ActionType.RegisterV2VSubscriber));
            url.Append("?");
            url.AppendFormat("{0}={1}", ViewIdParameter, viewId_);
            url.AppendFormat("&{0}={1}", CallbackParameter, callbackMethod_); 
            url.AppendFormat("&{0}={1}", MessageTypeParameter, Utility.UrlEncode(messageType_.AssemblyQualifiedName));
            subscriberId_ = Guid.NewGuid().ToString("N");
            url.AppendFormat("&{0}={1}", SessionIdParameter, subscriberId_); 
            return ProtectUrl(url.ToString());  
        }


        public string BuildV2VPulishUrl(object message_, DataFormat format_, string publisherId_)
        {
            if (message_ == null)
            {
                throw new ArgumentNullException("message_");
            }
            if (string.IsNullOrWhiteSpace(publisherId_))
            {
                throw new ArgumentNullException("publisherId_");
            }

            StringBuilder url = new StringBuilder(BaseUrl);
            url.Append(ConvertToActionAPI(format_ == DataFormat.Xml ? ActionType.V2VPublishXml : ActionType.V2VPulishJson));
            url.Append("?");
            string message = SerializeToString(message_, format_);
            bool zipped = false;
            if (message.Length > CompressThreshold) //needs to be zipped
            {
                message = Zip(message);
                zipped = true;
            }
            url.AppendFormat("{0}={1}", MessageParameter, Utility.UrlEncode(message));
            if (zipped)
            {
                url.AppendFormat("&{0}=1", ZippedParameter, true);
            }
            url.AppendFormat("&{0}={1}", SessionIdParameter, publisherId_);  
            return ProtectUrl(url.ToString());  
        }
        #endregion

        #endregion

        #region Utility Methods
        public static string GenerateHashSeed()
        {
            return Convert.ToBase64String(Guid.NewGuid().ToByteArray()).Substring(0, 10);
        }
        public static string ConvertToActionAPI(ActionType action_)
        {
            switch (action_)
            {
                case ActionType.RegisterPublisher:
                    return "regpub";
                case ActionType.RegisterSubscriber:
                    return "regsub";
                case ActionType.PublishXml:
                    return "pub.xml";
                case ActionType.PublishJson:
                    return "pub.json";
                case ActionType.CreateView:
                    return "view";
                case ActionType.CreatePulisherView:
                    return "pubview";
                case ActionType.CreateSubscriberView:
                    return "subview";
                case ActionType.RegisterV2VPublisher:
                    return "regv2vpub";
                case ActionType.RegisterV2VSubscriber:
                    return "regv2vsub";
                case ActionType.V2VPublishXml:
                    return "v2vpub.xml";
                case ActionType.V2VPulishJson:
                    return "v2vpub.json"; 
                default:
                    return null;
            }
        }

        public static ActionType ParseActionAPI(Uri url_)
        {
            if (url_ == null) return ActionType.Unknown;
            string actionApi = url_.AbsolutePath;
            if (string.IsNullOrWhiteSpace(actionApi)) return ActionType.Unknown;
            actionApi = actionApi.ToLower().TrimStart('/');
            if (string.IsNullOrWhiteSpace(actionApi)) return ActionType.Unknown;
            switch (actionApi)
            {
                case "regpub":
                    return ActionType.RegisterPublisher;
                case "regsub":
                    return ActionType.RegisterSubscriber;
                case "pub.xml":
                    return ActionType.PublishXml;
                case "pub.json":
                    return ActionType.PublishJson;
                case "view":
                    return ActionType.CreateView;
                case "pubview":
                    return ActionType.CreatePulisherView;
                case "subview":
                    return ActionType.CreateSubscriberView;
                case "regv2vpub":
                    return ActionType.RegisterV2VPublisher;
                case "regv2vsub":
                    return ActionType.RegisterV2VSubscriber;
                case "v2vpub.xml":
                    return ActionType.V2VPublishXml;
                case "v2vpub.json":
                    return ActionType.V2VPulishJson; 
                default:
                    return ActionType.Unknown;
            }
        }

        public string ProtectUrl(string url_)
        {
            if (protectors.Count == 0) return url_; 
            foreach (IUrlProtector protector in protectors)
            {
                url_ = protector.ProtectUrl(url_);
                if (url_ == null) return null;
            }
            return url_;
        }

        
        internal Uri UnprotectUrl(HttpListenerRequest request_)
        { 
            Uri uri = request_.Url;
            if (protectors.Count == 0) return uri;
            string url = uri.OriginalString;
            foreach (IUrlProtector protector in protectorsReversed)
            {
                url = protector.UnprotectUrl(url);
                if (url == null) return null;
            }
            return new Uri(url);
        } 


        public static string SerializeToString(object data_, DataFormat format_)
        {
            if (data_ == null)
            {
                return null;
            }
            if (data_ is string) return data_ as string;
            if (format_ == DataFormat.Xml)
            {
                XmlSerializer xmlSerializer = new XmlSerializer(data_.GetType());
                StringWriter textWriter = new StringWriter();

                xmlSerializer.Serialize(textWriter, data_);
                return textWriter.ToString();
            }
            else
            {
                return JsonConvert.SerializeObject(data_);
            }
        } 

        public static object DeserializeFromString(string text_, Type dataType_, DataFormat format_)
        {
            if (string.IsNullOrWhiteSpace(text_)) 
                return null;
            if (format_ == DataFormat.Xml)
            {
                if (dataType_ == null)
                {
                    throw new ArgumentNullException("dataType_");
                }
                return new XmlSerializer(dataType_).Deserialize(new StringReader(text_));
            }
            else
            {
                return JsonConvert.DeserializeObject(text_, dataType_);
            }
        }

        public static void CopyTo(Stream src, Stream dest)
        {
            byte[] bytes = new byte[4096];

            int cnt;

            while ((cnt = src.Read(bytes, 0, bytes.Length)) != 0)
            {
                dest.Write(bytes, 0, cnt);
            }
        }

        public static string Zip(string str_)
        {
            byte[] byteArray = new byte[str_.Length];
            int indexBA = 0;
            foreach (char item in str_)
            {
                byteArray[indexBA++] = (byte)item;
            }

            using (var msi = new MemoryStream(byteArray))
            using (var mso = new MemoryStream())
            {
                using (var gs = new GZipStream(mso, CompressionMode.Compress))
                {
                    msi.CopyTo(gs); 
                }

                return Convert.ToBase64String(mso.ToArray());
            }
        }

        public static string UnZip(string str_)
        {
            var bytes = Convert.FromBase64String(str_);
            using (var msi = new MemoryStream(bytes))
            using (var mso = new MemoryStream())
            {
                using (var gs = new GZipStream(msi, CompressionMode.Decompress))
                {
                    gs.CopyTo(mso); 
                }

                return Encoding.UTF8.GetString(mso.ToArray());
            }
        }

        #endregion 
    }

    public enum DataFormat
    {
        Xml = 0,
        Json = 1
    }

    public enum ActionType
    {
        Unknown = 0, 
        RegisterPublisher = 1,
        RegisterSubscriber = 2,
        PublishXml = 3,
        PublishJson = 4,
        CreateView = 5,
        CreatePulisherView = 6,
        CreateSubscriberView = 7,
        RegisterV2VPublisher = 8,
        RegisterV2VSubscriber = 9,
        V2VPublishXml = 10,
        V2VPulishJson = 11 
    } 
     
}
