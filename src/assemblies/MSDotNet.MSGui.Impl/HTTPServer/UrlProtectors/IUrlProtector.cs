﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace MSDesktop.HTTPServer
{
    interface IUrlProtector
    {
        string ProtectUrl(string url_);
        string UnprotectUrl(string url_);
    }
}
