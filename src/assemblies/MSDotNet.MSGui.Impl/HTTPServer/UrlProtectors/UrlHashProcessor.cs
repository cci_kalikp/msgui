﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Collections.Specialized;
using System.Net;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.MicrosoftShell.Standard;

namespace MSDesktop.HTTPServer
{
    /// <summary>
    /// Append a hash key calculated based on the HashSeed and the Url
    /// This can be used to protect the Url being tampered
    /// </summary>
    class UrlHashProcessor:IUrlProtector
    {
        private string seed;
        private System.Security.Cryptography.SHA256 s256 = new System.Security.Cryptography.SHA256Managed();
        private const string HashParameter = "hash";
        public UrlHashProcessor(string seed_)
        {
            this.seed = seed_;
        }
        public string ProtectUrl(string url_)
        {
            string hashResult = SHA256Hash(url_);
            if (url_.Contains("?"))
            {
                url_ += string.Format("&{0}={1}", HashParameter, hashResult);
            }
            else
            {
                url_ += string.Format("?{0}={1}", HashParameter, hashResult);
            }
            return url_;
        }

        public string UnprotectUrl(string url_)
        {
            Uri uri = new Uri(url_);
            NameValueCollection values = Utility.ParseQueryString(uri.Query);
            string hashTransferred = values[HashParameter];
            if (hashTransferred == null) return null;
            int hashQueryLength = HashParameter.Length + hashTransferred.Length + 2; 
            string rawUrl = url_.Remove(url_.Length - hashQueryLength);
            string hashCalculated = SHA256Hash(rawUrl);
            if (hashTransferred != hashCalculated)
            {
                return null;
            }

            return rawUrl;
        }
          

        private string SHA256Hash(string plainText)
        {
            Byte[] hashedBytes = s256.ComputeHash(Encoding.UTF8.GetBytes(seed + plainText));
            StringBuilder hashString = new StringBuilder();
            foreach (Byte hashedByte in hashedBytes)
            {
                hashString.Append(hashedByte.ToString("x2"));
            }
            return hashString.ToString();
        }
    }
}
