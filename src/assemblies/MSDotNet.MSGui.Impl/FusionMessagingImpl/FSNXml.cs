﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace MorganStanley.MSDotNet.MSGui.Impl.FusionMessagingImpl
{
    internal static class FSNXml
    {

        #region Public API

        public static T Deserialize<T>(string file_)
        {
            FSNGuard.ArgumentNotNullOrEmptyString(file_, "file_");
            return (T)Deserialize(typeof(T), file_);
        }

        public static T Deserialize<T>(Stream from_)
        {
            FSNGuard.ArgumentNotNull(from_, "from_");
            return (T)Deserialize(typeof(T), from_);
        }

        public static T Deserialize<T>(TextReader from_)
        {
            FSNGuard.ArgumentNotNull(from_, "from_");
            return (T)Deserialize(typeof(T), from_);
        }

        public static T Deserialize<T>(XmlNode from_)
        {
            FSNGuard.ArgumentNotNull(from_, "from_");
            return (T)Deserialize(typeof(T), from_);
        }

        public static object Deserialize(Type type_, string file_)
        {
            FSNGuard.ArgumentNotNull(type_, "type_");
            FSNGuard.ArgumentNotNullOrEmptyString(file_, "file");
            XmlDocument doc = new XmlDocument();
            doc.Load(file_);
            return Deserialize(type_, doc.DocumentElement);
        }

        public static object Deserialize(Type type_, Stream from_)
        {
            FSNGuard.ArgumentNotNull(type_, "type_");
            FSNGuard.ArgumentNotNull(from_, "from_");
            XmlSerializer xs = SerializerForType(type_);
            return xs.Deserialize(from_);
        }

        public static object Deserialize(Type type_, TextReader from_)
        {
            FSNGuard.ArgumentNotNull(type_, "type_");
            FSNGuard.ArgumentNotNull(from_, "from_");
            XmlSerializer xs = SerializerForType(type_);
            return xs.Deserialize(from_);
        }

        public static object Deserialize(Type type_, XmlNode from_)
        {
            FSNGuard.ArgumentNotNull(type_, "type_");
            FSNGuard.ArgumentNotNull(from_, "from_");
            XmlSerializer xs = SerializerForType(type_);
            XmlNodeReader nr = new XmlNodeReader(from_);
            return xs.Deserialize(nr);
        }

        public static Stream Serialize(object object_, Stream to_)
        {
            FSNGuard.ArgumentNotNull(object_, "object_");
            FSNGuard.ArgumentNotNull(to_, "to_");
            XmlSerializer xs = SerializerForType(object_.GetType());
            xs.Serialize(to_, object_);
            return to_;
        }

        public static TextWriter Serialize(object object_, TextWriter to_)
        {
            FSNGuard.ArgumentNotNull(object_, "object_");
            FSNGuard.ArgumentNotNull(to_, "to_");
            XmlSerializer xs = SerializerForType(object_.GetType());
            xs.Serialize(to_, object_);
            return to_;
        }

        public static XmlSerializer SerializerForType(Type type_)
        {
            FSNGuard.ArgumentNotNull(type_, "type_");
            lock (_sync)
            {
                XmlSerializer serializer;
                if (!_serializers.TryGetValue(type_, out serializer))
                {
                    serializer = new XmlSerializer(type_);
                    _serializers.Add(type_, serializer);
                }
                return serializer;
            }
        }

        #endregion

        #region Private Data

        private static object _sync = new object();
        private static readonly IDictionary<Type, XmlSerializer> _serializers = new Dictionary<Type, XmlSerializer>();

        #endregion
    }
}
