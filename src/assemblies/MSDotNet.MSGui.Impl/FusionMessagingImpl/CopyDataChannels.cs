﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MorganStanley.MSDotNet.MSGui.Impl.FusionMessagingImpl
{
    internal class CopyDataChannels<TMessage> : DictionaryBase
    {
        private NativeWindow owner = null;

        public event EventHandler OnChannelsUpdated;

        /// <summary>
        /// Returns an enumerator for each of the CopyDataChannel objects
        /// within this collection.
        /// </summary>
        /// <returns>An enumerator for each of the CopyDataChannel objects
        /// within this collection.</returns>
        public new System.Collections.IEnumerator GetEnumerator()
        {
            return this.Dictionary.Values.GetEnumerator();
        }

        public ArrayList Channels
        {
            get { return new ArrayList(this.Dictionary.Keys); }
        }

        /// <summary>
        /// Returns the CopyDataChannel at the specified 0-based index.
        /// </summary>
        internal CopyDataChannel<TMessage> this[int index]
        {
            get
            {
                CopyDataChannel<TMessage> ret = null;
                int i = 0;
                foreach (CopyDataChannel<TMessage> cdc in this.Dictionary.Values)
                {
                    i++;
                    if (i == index)
                    {
                        ret = cdc;
                        break;
                    }
                }
                return ret;
            }
        }
        /// <summary>
        /// Returns the CopyDataChannel for the specified channelName
        /// </summary>
        internal CopyDataChannel<TMessage> this[string channelName]
        {
            get
            {
                return (CopyDataChannel<TMessage>)this.Dictionary[channelName];
            }
        }
        /// <summary>
        /// Adds a new channel on which this application can send and
        /// receive messages.
        /// </summary>
        public void Add(string channelName, object state)
        {
            CopyDataChannel<TMessage> cdc = new CopyDataChannel<TMessage>(owner, channelName, state);
            this.Dictionary.Add(channelName, cdc);
            DispatchOnChannelsUpdated();
        }

        /// <summary>
        /// Adds a new channel on which this application can send and
        /// receive messages and a delegate for when messages arrive
        /// on that channel
        /// </summary>
        public void Add(string channelName, FSNCopyDataReceivedEventHandler<TMessage> handler, object state)
        {
            CopyDataChannel<TMessage> cdc = new CopyDataChannel<TMessage>(owner, channelName, state, handler);
            this.Dictionary.Add(channelName, cdc);
            DispatchOnChannelsUpdated();
        }

        /// <summary>
        /// Removes an existing channel.
        /// </summary>
        /// <param name="channelName">The channel to remove</param>
        public void Remove(string channelName)
        {
            this.Dictionary.Remove(channelName);
            DispatchOnChannelsUpdated();
        }
        /// <summary>
        /// Gets/sets whether this channel contains a CopyDataChannel
        /// for the specified channelName.
        /// </summary>
        public bool Contains(string channelName)
        {
            return this.Dictionary.Contains(channelName);
        }

        /// <summary>
        /// Ensures the resources associated with a CopyDataChannel
        /// object collected by this class are cleared up.
        /// </summary>
        protected override void OnClear()
        {
            foreach (CopyDataChannel<TMessage> cdc in this.Dictionary.Values)
            {
                cdc.Dispose();
            }
            base.OnClear();
        }

        /// <summary>
        /// Ensures any resoures associated with the CopyDataChannel object
        /// which has been removed are cleared up.
        /// </summary>
        /// <param name="key">The channelName</param>
        /// <param name="data">The CopyDataChannel object which has
        /// just been removed</param>
        protected override void OnRemoveComplete(Object key, System.Object data)
        {
            ((CopyDataChannel<TMessage>)data).Dispose();
            base.OnRemove(key, data);
        }

        /// <summary>
        /// If the form's handle changes, the properties associated
        /// with the window need to be cleared up. This override ensures
        /// that it is done.  Note that the CopyData class will then
        /// stop responding to events and it should be recreated once
        /// the new handle has been assigned.
        /// </summary>
        public void OnHandleChange()
        {
            foreach (CopyDataChannel<TMessage> cdc in this.Dictionary.Values)
            {
                cdc.OnHandleChange();
            }
        }

        internal void OnHandleCreated()
        {
            foreach (CopyDataChannel<TMessage> cdc in this.Dictionary.Values)
            {
                cdc.OnHandleCreated();
            }
        }

        /// <summary>
        /// Constructs a new instance of the CopyDataChannels collection.
        /// Automatically managed by the CopyData class.
        /// </summary>
        /// <param name="owner">The NativeWindow this collection
        /// will be associated with</param>
        internal CopyDataChannels(NativeWindow owner)
        {
            this.owner = owner;
        }

        private void DispatchOnChannelsUpdated()
        {
            EventHandler temp = OnChannelsUpdated;
            if (temp != null)
            {
                temp(this, EventArgs.Empty);
            }
        }
    }
}
