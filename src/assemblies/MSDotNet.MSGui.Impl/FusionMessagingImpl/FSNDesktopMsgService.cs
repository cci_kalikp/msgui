﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Impl.FusionMessagingImpl
{
    internal delegate void FSNDesktopMsgHandler(object sender_, FSNDesktopMsgEventArgs e_);
    internal delegate void FSNDesktopPromiscousMsgHandler(object sender_, FSNDesktopMsgPromiscousEventArgs e_);
    internal delegate bool RxMsgFilter<TMessage>(FSNCopyDataSink<TMessage> sink_, IntPtr src_, string channel_, TMessage msg_);

    internal sealed class FSNDesktopMsgService : IDisposable
    {
        #region Events

        /// <summary>
        /// This event is raised when the set of desktop subscriptions is updated
        /// </summary>
        //NOTE [EventPublication(FDSEventTopicNames.SubscriptionsChanged, PublicationScope.Descendants)]
        public event EventHandler OnSubscriptionsChanged;

        #endregion

        //NOTE [ServiceDependency]
        public IFSNLog Log
        {
            get { return _log; }
            set { _log = value; }
        }

        #region Construction

        public FSNDesktopMsgService()
        {
            if (FSNAppContext.LocalApp == null)
            {
                throw new FSNDesktopException("FSNAppContext.LocalApp not initialized");
            }
            // setup desktop id channel
            _desktopIdChannel = MakeDesktopChannelID(FSNAppContext.LocalApp);
            // setup copy data sink
            _marshaller = new FSNDesktopMsgMarshaller();
            _sink = new FSNCopyDataSink<FSNDesktopMsg>(_marshaller);
            _sink.OnChannelsUpdated += OnChannelsUpdated;
            _sink.OnInit += OnSinkInit;
        }

        #endregion

        #region Public API

        /// <summary>
        /// Start message service.
        /// </summary>
        public void Init()
        {
            // wire into init method 
            _sink.OnInit += OnSinkInit;
            // initialize sink
            _sink.Init();
        }

        /// <summary>
        /// Publish empty message to all desktop peers subscribed to channel 'channel_'
        /// </summary>
        /// <param name="channel_">Channel to publish message to</param>
        public void Publish(string channel_)
        {
            FSNGuard.ArgumentNotNull(channel_, "channel_");
            Publish(channel_, new FSNDesktopMsgData());
        }

        /// <summary>
        /// Publish empty message to all desktop peers subscribed to channel 'channel_'
        /// </summary>
        /// <param name="channel_">Channel to publish message to</param>
        /// <param name="timeoutMs_">
        /// If target process has been marked as 'not responding' then block for 'timeoutMs_'
        /// </param>
        public void Publish(string channel_, uint timeoutMs_)
        {
            FSNGuard.ArgumentNotNull(channel_, "channel_");
            Publish(channel_, new FSNDesktopMsgData(), timeoutMs_);
        }

        /// <summary>
        /// Publish data 'data_' to all desktop peers subscribed to channel 'channel_'
        /// </summary>
        /// <param name="channel_">Channel to publish message to</param>
        /// <param name="data_">Data to publish</param>
        public void Publish(string channel_, FSNDesktopMsgData data_)
        {
            FSNGuard.ArgumentNotNull(channel_, "channel_");
            FSNGuard.ArgumentNotNull(data_, "data_");
            Publish(channel_, data_, 0);
        }

        /// <summary>
        /// Publish data 'data_' to all desktop peers subscribed to channel 'channel_'
        /// </summary>
        /// <param name="channel_">Channel to publish message to</param>
        /// <param name="data_">Data to publish</param>
        /// <param name="timeoutMs_">
        /// If target process has been marked as 'not responding' then block for 'timeoutMs_'
        /// </param>
        public void Publish(string channel_, FSNDesktopMsgData data_, uint timeoutMs_)
        {
            FSNGuard.ArgumentNotNull(channel_, "channel_");
            FSNGuard.ArgumentNotNull(data_, "data_");
            Send(FSNAppContext.LocalApp, FSNAppContext.All, channel_, data_, timeoutMs_);
        }

        /// <summary>
        /// Send empty message to desktop peer 'destination_' over channel 'channel_'        
        /// </summary>
        /// <param name="destination_">Address to send data message to, FSNAppContext.All to send 
        /// message to all subscribers</param>
        /// <param name="channel_">Channel to send data on</param>
        public void Send(FSNAppContext destination_, string channel_)
        {
            FSNGuard.ArgumentNotNull(channel_, "channel_");
            Send(destination_, channel_, new FSNDesktopMsgData(), 0);
        }

        /// <summary>
        /// Send empty message to desktop peer 'destination_' over channel 'channel_'        
        /// </summary>
        /// <param name="destination_">Address to send data message to, FSNAppContext.All to send 
        /// message to all subscribers</param>
        /// <param name="channel_">Channel to send data on</param>
        /// <param name="timeoutMs_">
        /// If target process has been marked as 'not responding' then block for 'timeoutMs_'
        /// </param>
        public void Send(FSNAppContext destination_, string channel_, uint timeoutMs_)
        {
            FSNGuard.ArgumentNotNull(channel_, "channel_");
            Send(destination_, channel_, new FSNDesktopMsgData(), timeoutMs_);
        }

        /// <summary>
        /// Send data 'data_' to desktop peer 'destination_' over channel 'channel_'
        /// </summary>
        /// <param name="destination_">Address to send data to (FSNAppContext.All for all peers)</param>
        /// <param name="channel_">Channel to send data on</param>
        /// <param name="data_">Data to send to peer</param>
        public void Send(FSNAppContext destination_, string channel_, FSNDesktopMsgData data_)
        {
            FSNGuard.ArgumentNotNull(channel_, "channel_");
            FSNGuard.ArgumentNotNull(data_, "data_");
            Send(FSNAppContext.LocalApp, destination_, channel_, data_, 0);
        }

        /// <summary>
        /// Send data 'data_' to desktop peer 'destination_' over channel 'channel_'
        /// </summary>
        /// <param name="destination_">Address to send data to (FSNAppContext.All for all peers)</param>
        /// <param name="channel_">Channel to send data on</param>
        /// <param name="data_">Data to send to peer</param>
        /// <param name="timeoutMs_">
        /// If target process has been marked as 'not responding' then block for 'timeoutMs_'
        /// </param>
        public void Send(FSNAppContext destination_, string channel_, FSNDesktopMsgData data_, uint timeoutMs_)
        {
            FSNGuard.ArgumentNotNull(channel_, "channel_");
            FSNGuard.ArgumentNotNull(data_, "data_");
            Send(FSNAppContext.LocalApp, destination_, channel_, data_, timeoutMs_);
        }

        /// <summary>
        /// Retrieve current set of desktop subscriptions
        /// </summary>
        public IEnumerable<string> Subscriptions
        {
            get { return _sink.ChannelSet; }
        }

        /// <summary>
        /// Subcribe to desktop channel channel_
        /// </summary>
        /// <param name="channel_">channel to subscribe to</param>
        public void Subscribe(string channel_)
        {
            FSNGuard.ArgumentNotNull(channel_, "channel_");
            _sink.Subscribe(channel_, null);
        }

        /// <summary>
        /// Subscribe to desktop channel channel_
        /// </summary>
        /// <param name="channel_">channel to subscribe to</param>
        /// <param name="handler_">handler to invoke on receopt of message</param>
        public void Subscribe(string channel_, FSNDesktopMsgHandler handler_)
        {
            FSNGuard.ArgumentNotNull(channel_, "channel_");
            DoSubscribe(channel_, handler_);
        }

        /// <summary>
        /// Unsubscribe from named channel
        /// </summary>
        /// <param name="channel_">channel to unsubscribe from</param>
        public void Unsubscribe(string channel_)
        {
            FSNGuard.ArgumentNotNull(channel_, "channel_");
            _sink.Unsubscribe(channel_);
        }

        /// <summary>
        /// Add delegate to call on receipt of messages over desktop promiscous channel.
        /// The channel is opened on first call to this method - the channel is closed
        /// when all registered handlers are cleared
        /// </summary>
        /// <param name="handler_">invoked on receipt of desktop message over the promiscous channel</param>
        public void AddPromiscousHandler(FSNDesktopPromiscousMsgHandler handler_)
        {
            FSNGuard.ArgumentNotNull(handler_, "handler_");
            lock (_psync)
            {
                if (_promiscousHandlers == null)
                {
                    DoSubscribe(CHANNEL_FUSION_DESKTOP_PROMISCOUS, OnPromiscousMSG);
                }
                _promiscousHandlers += handler_;
            }
        }

        /// <summary>
        /// Rmove promiscous handler
        /// The promiscous channel is closed when no further handlers are registered
        /// </summary>
        /// <param name="handler_">Handler to remov</param>
        public void RemovePromiscousHandler(FSNDesktopPromiscousMsgHandler handler_)
        {
            FSNGuard.ArgumentNotNull(handler_, "handler_");
            lock (_psync)
            {
                _promiscousHandlers -= handler_;
                if (_promiscousHandlers == null)
                {
                    Unsubscribe(CHANNEL_FUSION_DESKTOP_PROMISCOUS);
                }
            }
        }

        /// <summary>
        /// Filter message received queue
        /// </summary>
        public RxMsgFilter<FSNDesktopMsg> MsgFilter
        {
            get { return _sink.MsgFilter; }
            set { _sink.MsgFilter = value; }
        }

        /// <summary>
        /// Inject message into desktop message local rx queue.
        /// </summary>
        /// <param name="src_">The source hwnd.</param>
        /// <param name="channel_">Channel to upcall message on.</param>
        /// <param name="msg_">Message to inject.</param>
        public void Inject(IntPtr src_, string channel_, FSNDesktopMsg msg_)
        {
            _sink.Inject(new FSNDesktopMsgInjector(src_, channel_, msg_));
        }

        #endregion

        #region Diagnostics

        //NOTE [FSNDiagnostics("Fusion Message Service")]
        //public void Diagnose(FSNDiagnosticsContext context_)
        //{
        //    Guard.ArgumentNotNull(context_, "context_");
        //    StringBuilder body = new StringBuilder();
        //    body.Append("Subscriptions : ").Append(Environment.NewLine);
        //    body.Append(Environment.NewLine);
        //    List<string> subs = new List<string>(this.Subscriptions);
        //    subs.Sort(delegate(string a_, string b_) { return a_.CompareTo(b_); });
        //    foreach (string sub in subs)
        //    {
        //        body.Append(sub).Append(Environment.NewLine);
        //    }
        //    context_.Diagnostics = body.ToString();
        //}

        #endregion

        #region Private Implementation

        internal int TTLDefault
        {
            get { return _defTTL; }
            set { _defTTL = value; }
        }

        internal void Send(FSNAppContext source_, FSNAppContext destination_, string channel_)
        {
            Send(source_, destination_, channel_, new FSNDesktopMsgData(), 0);
        }

        internal void Send(FSNAppContext source_, FSNAppContext destination_, string channel_, FSNDesktopMsgData data_, uint timeoutMs_)
        {
            FSNDesktopMsg msg = new FSNDesktopMsg();
            msg.Channel = channel_;
            msg.Source = source_;
            msg.Destination = destination_;
            msg.TimeoutMS = timeoutMs_;
            msg.TTL = _defTTL;
            msg.Data = data_;
            Send(channel_, msg);
        }

        internal void Send(string channel_, FSNDesktopMsg msg_)
        {
            // locate all desktop windows
            IEnumerable<FSNCopyDataTarget<FSNDesktopMsg>> dtws = GetMsgTargets();
            // always send over promiscous channel first
            foreach (FSNCopyDataTarget<FSNDesktopMsg> w in dtws)
            {
                if (w.HasProp(CHANNEL_FUSION_DESKTOP_PROMISCOUS))
                {
                    DoPublish(w, _sink.Handle, CHANNEL_FUSION_DESKTOP_PROMISCOUS, msg_);
                }
            }
            // cook address into channel if publishing to named target
            string acid = msg_.Destination == FSNAppContext.All ? null : MakeAppContextChannel(msg_.Destination);
            // now publish to target(s)
            foreach (FSNCopyDataTarget<FSNDesktopMsg> w in dtws)
            {
                // publishing to named target
                if (acid != null)
                {
                    if (w.HasProp(acid))
                    {
                        if (w.HasProp(msg_.Channel))
                        {
                            DoPublish(w, _sink.Handle, msg_.Channel, msg_);
                        }
                        return;
                    }
                }
                // publishing to all channel listeners                    
                else
                {
                    if (w.HasProp(msg_.Channel))
                    {
                        DoPublish(w, _sink.Handle, msg_.Channel, msg_);
                    }
                }
            }
        }


        private void DoPublish(FSNCopyDataTarget<FSNDesktopMsg> window_, IntPtr src_, string channel_, FSNDesktopMsg msg_)
        {
            if (!window_.Publish(src_, channel_, msg_, null, msg_.TimeoutMS) && _log != null)
            {
                _log.Error(string.Format("Failed to send desktop msg, channel := {0} msg := {1} ", channel_, msg_));
            }
        }

        /// <summary>
        ///  Retrieve handles of windows running on this desktop
        /// </summary>
        /// <returns>Set of all windows running on this desktop - including *this* process</returns>
        private IEnumerable<FSNCopyDataTarget<FSNDesktopMsg>> GetMsgTargets()
        {
            IList<FSNCopyDataTarget<FSNDesktopMsg>> ret = new List<FSNCopyDataTarget<FSNDesktopMsg>>();
            foreach (FSNWindow window in (new FSNEnumCopyDataTargets()).GetWindowsWithProp(_desktopIdChannel))
            {
                ret.Add(new FSNCopyDataTarget<FSNDesktopMsg>(window.Hwnd, _marshaller));
            }
            return ret;
        }

        /// <summary>
        /// Retrieve handle of named desktop window
        /// </summary>
        /// <param name="ctx_">Desktop window identifier</param>
        /// <returns>Handle of named window - or null if not located</returns>
        internal FSNCopyDataTarget<FSNDesktopMsg> GetMsgTarget(FSNAppContext ctx_)
        {
            return GetMsgTarget(ctx_, GetMsgTargets());
        }

        /// <summary>
        /// Locate handle of named window in window handle set
        /// </summary>
        /// <param name="ctx_">Desktop Window Identifier</param>
        /// <param name="dtws_">Set of desktop window handles</param>
        /// <returns>Handle of named window - null if not found</returns>
        private FSNCopyDataTarget<FSNDesktopMsg> GetMsgTarget(FSNAppContext ctx_, IEnumerable<FSNCopyDataTarget<FSNDesktopMsg>> dtws_)
        {
            string acid = MakeAppContextChannel(ctx_);
            // enumerate desktop windows
            foreach (FSNCopyDataTarget<FSNDesktopMsg> target in dtws_)
            {
                if (target.HasProp(acid))
                {
                    return target;
                }
            }
            return null;
        }

        private void DoSubscribe(string channel_, FSNDesktopMsgHandler handler_)
        {
            _sink.Subscribe
              (
              channel_,
              delegate(object sender_, FSNCopyDataReceivedEventArgs<FSNDesktopMsg> e_)
              {
                  handler_(this, new FSNDesktopMsgEventArgs(e_.Message.Source, e_.Message.Data));
              },
              null
              );
        }

        private void DoSubscribe(string channel_, FSNDesktopPromiscousMsgHandler handler_)
        {
            _sink.Subscribe
              (
              channel_,
              delegate(object sender_, FSNCopyDataReceivedEventArgs<FSNDesktopMsg> e_)
              {
                  handler_(this, new FSNDesktopMsgPromiscousEventArgs(e_.Message.Channel, e_.Message));
              },
              null
              );
        }

        private class FSNDesktopMsgInjector : IFSNMsgBuilder<FSNDesktopMsg>
        {
            public FSNDesktopMsgInjector(IntPtr src_, string channel_, FSNDesktopMsg msg_)
            {
                _src = src_;
                _msg = msg_;
                _channel = channel_;
            }

            #region IFSNMsgBuilder<FSNDesktopMsg> Members

            public IntPtr Src
            {
                get { return _src; }
            }

            public string Channel
            {
                get { return _channel; }
            }

            public FSNDesktopMsg Build(object state_)
            {
                return _msg;
            }

            #endregion

            private readonly IntPtr _src;
            private readonly string _channel;
            private readonly FSNDesktopMsg _msg;
        }

        #endregion

        #region Event Handlers

        // callback on sink initialized
        private void OnSinkInit(object sender_, EventArgs e_)
        {
            // don't care at the moment
            if (_log != null) { _log.Debug("Received sink init."); }
        }

        // callback on receipt of a message over the promiscous channel
        private void OnPromiscousMSG(object sender_, FSNDesktopMsgPromiscousEventArgs e_)
        {
            FSNDesktopPromiscousMsgHandler handlers = _promiscousHandlers;
            if (handlers != null)
            {
                handlers(sender_, e_);
            }
        }

        // callback from copy data sink to indicate that channel set has changed
        // upcall this notification to interested parties
        private void OnChannelsUpdated(object sender_, EventArgs e_)
        {
            DispatchOnSubscriptionsChanged();
        }

        private void DispatchOnSubscriptionsChanged()
        {
            EventHandler temp = OnSubscriptionsChanged;
            if (temp != null)
            {
                temp(this, EventArgs.Empty);
            }
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing_)
        {
            if (_disposed) { return; }
            if (_sink != null)
            {
                _sink.Dispose();
            }
            _disposed = true;
        }

        ~FSNDesktopMsgService()
        {
            Dispose(false);
        }

        #endregion

        #region Utility Functions

        internal static string MakeDesktopChannelID(FSNAppContext context_)
        {
            return "ipc://Fusion/Desktop/Id/" + context_.User + "/" + context_.Desktop.Name;
        }

        internal static string MakeAppContextChannel(FSNAppContext context_)
        {
            return "ipc://Fusion/Desktop/AppContext/" + context_.ToString();
        }

        #endregion

        #region Private Data

        private IFSNLog _log;

        // promiscous subscription channel id
        internal const string CHANNEL_FUSION_DESKTOP_PROMISCOUS = "ipc://Fusion/Desktop/Promiscous";

        private volatile bool _disposed = false;
        private volatile int _defTTL = 1;

        // user space handlers to upcall promiscous messages to
        private event FSNDesktopPromiscousMsgHandler _promiscousHandlers;

        // lock for rc counted (un)subscribe of promiscous channel
        private readonly object _psync = new object();

        private readonly FSNDesktopMsgMarshaller _marshaller;
        private readonly FSNCopyDataSink<FSNDesktopMsg> _sink;

        private readonly string _desktopIdChannel;

        // handle cache
        private readonly object _hlock = new object();
        private readonly IDictionary<IntPtr, FSNAppContext> _hcache = new Dictionary<IntPtr, FSNAppContext>();

        #endregion
    }
}
