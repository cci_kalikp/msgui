﻿using System;

namespace MorganStanley.MSDotNet.MSGui.Impl.FusionMessagingImpl
{
    internal class DataEventArgs<TData> : EventArgs
    {
        private readonly TData m_data;

        public DataEventArgs(TData data_)
        {
            m_data = data_;
        }

        public TData Data
        {
            get
            {
                return m_data;
            }
        }

        public override string ToString()
        {
            return m_data.ToString();
        }
    }
}
