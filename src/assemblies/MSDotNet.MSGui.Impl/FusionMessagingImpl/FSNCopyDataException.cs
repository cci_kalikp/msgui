﻿using System;
using System.Runtime.Serialization;

namespace MorganStanley.MSDotNet.MSGui.Impl.FusionMessagingImpl
{
    [Serializable]
    internal class FSNCopyDataException : FSNException
    {
        public FSNCopyDataException() { }
        public FSNCopyDataException(string msg_) : base(msg_) { }
        public FSNCopyDataException(string msg_, Exception inner_) : base(msg_, inner_) { }

        protected FSNCopyDataException(SerializationInfo info_, StreamingContext context_)
            : base(info_, context_)
        {
        }
    }
}
