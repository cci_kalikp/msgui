﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Impl.FusionMessagingImpl
{
    internal static class FSNWin32
    {
        [StructLayout(LayoutKind.Sequential)]
        internal struct RECT
        {
            public int Left;
            public int Top;
            public int Right;
            public int Bottom;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct POINT
        {
            public int x;
            public int y;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct SIZE
        {
            public int cx;
            public int cy;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct WINDOWINFO
        {
            public int size;
            public RECT window;
            public RECT client;
            public int style;
            public int exStyle;
            public int windowStatus;
            public uint xWindowBorders;
            public uint yWindowBorders;
            public short atomWindowtype;
            public short creatorVersion;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct WINDOWPLACEMENT
        {
            public int Length;
            public int flags;
            public int showCmd;
            public POINT ptMinPosition;
            public POINT ptMaxPosition;
            public RECT rcNormalPosition;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct COPYDATASTRUCT
        {
            public IntPtr dwData;
            public int cbData;
            public IntPtr lpData;
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct SHFILEINFO
        {
            public IntPtr hIcon;
            public IntPtr iIcon;
            public uint dwAttributes;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)]
            public string szDisplayName;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 80)]
            public string szTypeName;
        };

        [StructLayout(LayoutKind.Sequential)]
        internal struct FLASHWINFO
        {
            public UInt32 cbSize;
            public IntPtr hwnd;
            public UInt32 dwFlags;
            public UInt32 uCount;
            public UInt32 dwTimeout;
        }

        /// <summary>
        /// Helper class containing Gdi32 API functions
        /// </summary>
        internal class GDI
        {

            public const int SRCCOPY = 0x00CC0020; // BitBlt dwRop parameter
            [DllImport("gdi32.dll")]
            public static extern bool BitBlt(
              IntPtr hObject, int nXDest, int nYDest,
              int nWidth, int nHeight, IntPtr hObjectSource,
              int nXSrc, int nYSrc, int dwRop
              );
            [DllImport("gdi32.dll")]
            public static extern IntPtr CreateCompatibleBitmap(IntPtr hDC, int nWidth, int nHeight);
            [DllImport("gdi32.dll")]
            public static extern IntPtr CreateCompatibleDC(IntPtr hDC);
            [DllImport("gdi32.dll")]
            public static extern bool DeleteDC(IntPtr hDC);
            [DllImport("gdi32.dll")]
            public static extern bool DeleteObject(IntPtr hObject);
            [DllImport("gdi32.dll")]
            public static extern IntPtr SelectObject(IntPtr hDC, IntPtr hObject);
        }

        internal enum Modifiers
        {
            MOD_ALT = 0x0001,
            MOD_CONTROL = 0x0002,
            MOD_SHIFT = 0x0004,
            MOD_WIN = 0x0008
        }

        internal enum HitTestValues
        {
            HTERROR = -2,
            HTTRANSPARENT = -1,
            HTNOWHERE = 0,
            HTCLIENT = 1,
            HTCAPTION = 2,
            HTSYSMENU = 3,
            HTGROWBOX = 4,
            HTMENU = 5,
            HTHSCROLL = 6,
            HTVSCROLL = 7,
            HTMINBUTTON = 8,
            HTMAXBUTTON = 9,
            HTLEFT = 10,
            HTRIGHT = 11,
            HTTOP = 12,
            HTTOPLEFT = 13,
            HTTOPRIGHT = 14,
            HTBOTTOM = 15,
            HTBOTTOMLEFT = 16,
            HTBOTTOMRIGHT = 17,
            HTBORDER = 18,
            HTOBJECT = 19,
            HTCLOSE = 20,
            HTHELP = 21
        }

        internal enum Msgs
        {
            WM_NULL = 0x0000,
            WM_CREATE = 0x0001,
            WM_DESTROY = 0x0002,
            WM_MOVE = 0x0003,
            WM_SIZE = 0x0005,
            WM_ACTIVATE = 0x0006,
            WM_SETFOCUS = 0x0007,
            WM_KILLFOCUS = 0x0008,
            WM_ENABLE = 0x000A,
            WM_SETREDRAW = 0x000B,
            WM_SETTEXT = 0x000C,
            WM_GETTEXT = 0x000D,
            WM_GETTEXTLENGTH = 0x000E,
            WM_PAINT = 0x000F,
            WM_CLOSE = 0x0010,
            WM_QUERYENDSESSION = 0x0011,
            WM_QUIT = 0x0012,
            WM_QUERYOPEN = 0x0013,
            WM_ERASEBKGND = 0x0014,
            WM_SYSCOLORCHANGE = 0x0015,
            WM_ENDSESSION = 0x0016,
            WM_SHOWWINDOW = 0x0018,
            WM_WININICHANGE = 0x001A,
            WM_SETTINGCHANGE = 0x001A,
            WM_DEVMODECHANGE = 0x001B,
            WM_ACTIVATEAPP = 0x001C,
            WM_FONTCHANGE = 0x001D,
            WM_TIMECHANGE = 0x001E,
            WM_CANCELMODE = 0x001F,
            WM_SETCURSOR = 0x0020,
            WM_MOUSEACTIVATE = 0x0021,
            WM_CHILDACTIVATE = 0x0022,
            WM_QUEUESYNC = 0x0023,
            WM_GETMINMAXINFO = 0x0024,
            WM_PAINTICON = 0x0026,
            WM_ICONERASEBKGND = 0x0027,
            WM_NEXTDLGCTL = 0x0028,
            WM_SPOOLERSTATUS = 0x002A,
            WM_DRAWITEM = 0x002B,
            WM_MEASUREITEM = 0x002C,
            WM_DELETEITEM = 0x002D,
            WM_VKEYTOITEM = 0x002E,
            WM_CHARTOITEM = 0x002F,
            WM_SETFONT = 0x0030,
            WM_GETFONT = 0x0031,
            WM_SETHOTKEY = 0x0032,
            WM_GETHOTKEY = 0x0033,
            WM_QUERYDRAGICON = 0x0037,
            WM_COMPAREITEM = 0x0039,
            WM_GETOBJECT = 0x003D,
            WM_COMPACTING = 0x0041,
            WM_COMMNOTIFY = 0x0044,
            WM_WINDOWPOSCHANGING = 0x0046,
            WM_WINDOWPOSCHANGED = 0x0047,
            WM_POWER = 0x0048,
            WM_COPYDATA = 0x004A,
            WM_CANCELJOURNAL = 0x004B,
            WM_NOTIFY = 0x004E,
            WM_INPUTLANGCHANGEREQUEST = 0x0050,
            WM_INPUTLANGCHANGE = 0x0051,
            WM_TCARD = 0x0052,
            WM_HELP = 0x0053,
            WM_USERCHANGED = 0x0054,
            WM_NOTIFYFORMAT = 0x0055,
            WM_CONTEXTMENU = 0x007B,
            WM_STYLECHANGING = 0x007C,
            WM_STYLECHANGED = 0x007D,
            WM_DISPLAYCHANGE = 0x007E,
            WM_GETICON = 0x007F,
            WM_SETICON = 0x0080,
            WM_NCCREATE = 0x0081,
            WM_NCDESTROY = 0x0082,
            WM_NCCALCSIZE = 0x0083,
            WM_NCHITTEST = 0x0084,
            WM_NCPAINT = 0x0085,
            WM_NCACTIVATE = 0x0086,
            WM_GETDLGCODE = 0x0087,
            WM_SYNCPAINT = 0x0088,
            WM_NCMOUSEMOVE = 0x00A0,
            WM_NCLBUTTONDOWN = 0x00A1,
            WM_NCLBUTTONUP = 0x00A2,
            WM_NCLBUTTONDBLCLK = 0x00A3,
            WM_NCRBUTTONDOWN = 0x00A4,
            WM_NCRBUTTONUP = 0x00A5,
            WM_NCRBUTTONDBLCLK = 0x00A6,
            WM_NCMBUTTONDOWN = 0x00A7,
            WM_NCMBUTTONUP = 0x00A8,
            WM_NCMBUTTONDBLCLK = 0x00A9,
            WM_KEYDOWN = 0x0100,
            WM_KEYUP = 0x0101,
            WM_CHAR = 0x0102,
            WM_DEADCHAR = 0x0103,
            WM_SYSKEYDOWN = 0x0104,
            WM_SYSKEYUP = 0x0105,
            WM_SYSCHAR = 0x0106,
            WM_SYSDEADCHAR = 0x0107,
            WM_KEYLAST = 0x0108,
            WM_IME_STARTCOMPOSITION = 0x010D,
            WM_IME_ENDCOMPOSITION = 0x010E,
            WM_IME_COMPOSITION = 0x010F,
            WM_IME_KEYLAST = 0x010F,
            WM_INITDIALOG = 0x0110,
            WM_COMMAND = 0x0111,
            WM_SYSCOMMAND = 0x0112,
            WM_TIMER = 0x0113,
            WM_HSCROLL = 0x0114,
            WM_VSCROLL = 0x0115,
            WM_INITMENU = 0x0116,
            WM_INITMENUPOPUP = 0x0117,
            WM_MENUSELECT = 0x011F,
            WM_MENUCHAR = 0x0120,
            WM_ENTERIDLE = 0x0121,
            WM_MENURBUTTONUP = 0x0122,
            WM_MENUDRAG = 0x0123,
            WM_MENUGETOBJECT = 0x0124,
            WM_UNINITMENUPOPUP = 0x0125,
            WM_MENUCOMMAND = 0x0126,
            WM_CTLCOLORMSGBOX = 0x0132,
            WM_CTLCOLOREDIT = 0x0133,
            WM_CTLCOLORLISTBOX = 0x0134,
            WM_CTLCOLORBTN = 0x0135,
            WM_CTLCOLORDLG = 0x0136,
            WM_CTLCOLORSCROLLBAR = 0x0137,
            WM_CTLCOLORSTATIC = 0x0138,
            WM_MOUSEMOVE = 0x0200,
            WM_LBUTTONDOWN = 0x0201,
            WM_LBUTTONUP = 0x0202,
            WM_LBUTTONDBLCLK = 0x0203,
            WM_RBUTTONDOWN = 0x0204,
            WM_RBUTTONUP = 0x0205,
            WM_RBUTTONDBLCLK = 0x0206,
            WM_MBUTTONDOWN = 0x0207,
            WM_MBUTTONUP = 0x0208,
            WM_MBUTTONDBLCLK = 0x0209,
            WM_MOUSEWHEEL = 0x020A,
            WM_PARENTNOTIFY = 0x0210,
            WM_ENTERMENULOOP = 0x0211,
            WM_EXITMENULOOP = 0x0212,
            WM_NEXTMENU = 0x0213,
            WM_SIZING = 0x0214,
            WM_CAPTURECHANGED = 0x0215,
            WM_MOVING = 0x0216,
            WM_DEVICECHANGE = 0x0219,
            WM_MDICREATE = 0x0220,
            WM_MDIDESTROY = 0x0221,
            WM_MDIACTIVATE = 0x0222,
            WM_MDIRESTORE = 0x0223,
            WM_MDINEXT = 0x0224,
            WM_MDIMAXIMIZE = 0x0225,
            WM_MDITILE = 0x0226,
            WM_MDICASCADE = 0x0227,
            WM_MDIICONARRANGE = 0x0228,
            WM_MDIGETACTIVE = 0x0229,
            WM_MDISETMENU = 0x0230,
            WM_ENTERSIZEMOVE = 0x0231,
            WM_EXITSIZEMOVE = 0x0232,
            WM_DROPFILES = 0x0233,
            WM_MDIREFRESHMENU = 0x0234,
            WM_IME_SETCONTEXT = 0x0281,
            WM_IME_NOTIFY = 0x0282,
            WM_IME_CONTROL = 0x0283,
            WM_IME_COMPOSITIONFULL = 0x0284,
            WM_IME_SELECT = 0x0285,
            WM_IME_CHAR = 0x0286,
            WM_IME_REQUEST = 0x0288,
            WM_IME_KEYDOWN = 0x0290,
            WM_IME_KEYUP = 0x0291,
            WM_MOUSEHOVER = 0x02A1,
            WM_MOUSELEAVE = 0x02A3,
            WM_CUT = 0x0300,
            WM_COPY = 0x0301,
            WM_PASTE = 0x0302,
            WM_CLEAR = 0x0303,
            WM_UNDO = 0x0304,
            WM_RENDERFORMAT = 0x0305,
            WM_RENDERALLFORMATS = 0x0306,
            WM_DESTROYCLIPBOARD = 0x0307,
            WM_DRAWCLIPBOARD = 0x0308,
            WM_PAINTCLIPBOARD = 0x0309,
            WM_VSCROLLCLIPBOARD = 0x030A,
            WM_SIZECLIPBOARD = 0x030B,
            WM_ASKCBFORMATNAME = 0x030C,
            WM_CHANGECBCHAIN = 0x030D,
            WM_HSCROLLCLIPBOARD = 0x030E,
            WM_QUERYNEWPALETTE = 0x030F,
            WM_PALETTEISCHANGING = 0x0310,
            WM_PALETTECHANGED = 0x0311,
            WM_HOTKEY = 0x0312,
            WM_PRINT = 0x0317,
            WM_PRINTCLIENT = 0x0318,
            WM_HANDHELDFIRST = 0x0358,
            WM_HANDHELDLAST = 0x035F,
            WM_AFXFIRST = 0x0360,
            WM_AFXLAST = 0x037F,
            WM_PENWINFIRST = 0x0380,
            WM_PENWINLAST = 0x038F,
            WM_APP = 0x8000,
            WM_USER = 0x0400,
            WM_DDE_INITIATE = 0x03E0,
            WM_DDE_TERMINATE,
            WM_DDE_ADVISE,
            WM_DDE_UNADVISE,
            WM_DDE_ACK,
            WM_DDE_DATA,
            WM_DDE_REQUEST,
            WM_DDE_POKE,
            WM_DDE_EXECUTE
        }


        internal class User
        {
            public const int SW_MAX = 10;
            public const int SW_MAXIMIZE = 3;
            public const int SW_MINIMIZE = 6;
            public const int SW_NORMAL = 1;
            public const int SW_SHOWDEFAULT = 10;
            public const int SW_SHOWMAXIMIZED = 3;
            public const int SW_SHOWMINIMIZED = 2;
            public const int SW_SHOWMINNOACTIVE = 7;

            /// <summary>
            /// Win32 API Constants for ShowWindowAsync()
            /// </summary>
            public const int SW_HIDE = 0;
            public const int SW_SHOWNORMAL = 1;
            public const int SW_SHOWNOACTIVATE = 4;
            public const int SW_RESTORE = 9;

            public const int SC_RESTORE = 0xF120;
            public const int SC_CLOSE = 0xF060;
            public const int SC_MAXIMIZE = 0xF030;
            public const int SC_MINIMIZE = 0xF020;
            public const int SC_MOUSEMENU = 0xF090;
            public const int SC_KEYMENU = 0xF100;
            public const int SC_MOVE = 0xF010;
            public const int SC_SIZE = 0xF000;

            public const int GWL_STYLE = (-16);
            public const int GWL_EXSTYLE = (-20);

            internal delegate int EnumThreadProc(IntPtr hwnd, int lParam);

            public static readonly IntPtr HWND_TOPMOST = new IntPtr(-1);
            public static readonly IntPtr HWND_NOTOPMOST = new IntPtr(-2);
            public static readonly IntPtr HWND_MESSAGE = new IntPtr(-3);
            public static readonly IntPtr HWND_TOP = new IntPtr(0);
            public static readonly IntPtr HWND_BOTTOM = new IntPtr(1);

            [DllImport("user32")]
            public static extern int EnumThreadWindows(int dwThreadId, EnumThreadProc pfnEnum, IntPtr lParam);
            [DllImport("user32")]
            public static extern int EnumWindows(EnumThreadProc lpEnumFunc, int lParam);
            [DllImport("user32")]
            public static extern IntPtr FindWindowEx(IntPtr parentHandle, IntPtr childAfter, string className, IntPtr windowTitle);
            [DllImport("user32")]
            public static extern int GetDesktopWindow();
            [DllImport("user32")]
            public static extern int GetWindowPlacement(IntPtr hwnd, ref WINDOWPLACEMENT lpwndpl);
            [DllImport("user32")]
            public static extern int GetWindowRect(IntPtr hwnd, ref RECT lpRect);
            [DllImport("user32")]
            public static extern int GetWindowText(IntPtr hwnd, StringBuilder lpString, int cch);
            [DllImport("user32")]
            public static extern int GetWindowThreadProcessId(IntPtr hwnd, ref int lpdwProcessId);
            [DllImport("user32")]
            public static extern int IsWindowVisible(IntPtr hwnd);
            [DllImport("user32")]
            public static extern int SetWindowText(IntPtr hwnd, string lpString);
            [DllImport("user32")]
            public static extern int ShowWindow(IntPtr hwnd, int nCmdShow);
            [DllImport("user32")]
            public static extern int ShowWindowAsync(IntPtr hwnd, int nCmdShow);
            [DllImport("user32")]
            public static extern int SetForegroundWindow(IntPtr hwnd);
            [DllImport("user32.dll")]
            public static extern IntPtr GetForegroundWindow();
            [DllImport("user32")]
            public static extern bool GetWindowInfo(IntPtr hwnd, out WINDOWINFO pwi);
            [DllImport("user32")]
            public static extern int SetWindowPlacement(IntPtr hwnd, ref WINDOWPLACEMENT lpwndpl);
            [DllImport("user32")]
            public static extern int SetWindowPos(IntPtr hwnd, IntPtr hwndInsertAfter, int x, int y, int cx, int cy, int wFlags);
            [DllImport("user32")]
            public extern static int IsIconic(IntPtr hWnd);
            [DllImport("user32")]
            public extern static int BringWindowToTop(IntPtr hWnd);
            [DllImport("user32.dll")]
            public static extern IntPtr GetWindowThreadProcessId(IntPtr hWnd, IntPtr ProcessId);
            [DllImport("user32.dll")]
            public static extern IntPtr AttachThreadInput(IntPtr idAttach, IntPtr idAttachTo, int fAttach);
            [DllImport("user32")]
            public static extern IntPtr SetParent(IntPtr hWndChild, IntPtr hWndNewParent);

            //Stop flashing. The system restores the window to its original state.
            public const UInt32 FLASHW_STOP = 0;
            //Flash the window caption.
            public const UInt32 FLASHW_CAPTION = 1;
            //Flash the taskbar button.
            public const UInt32 FLASHW_TRAY = 2;
            //Flash both the window caption and taskbar button.
            //This is equivalent to setting the FLASHW_CAPTION | FLASHW_TRAY flags.
            public const UInt32 FLASHW_ALL = 3;
            //Flash continuously, until the FLASHW_STOP flag is set.
            public const UInt32 FLASHW_TIMER = 4;
            //Flash continuously until the window comes to the foreground.
            public const UInt32 FLASHW_TIMERNOFG = 12;

            [DllImport("user32.dll")]
            public static extern Int32 FlashWindowEx(ref FLASHWINFO pwfi);

            [DllImport("user32", CharSet = CharSet.Auto)]
            public extern static int GetProp(
              IntPtr hwnd,
              string lpString);

            [DllImport("user32", CharSet = CharSet.Auto)]
            public extern static int SetProp(
              IntPtr hwnd,
              string lpString,
              int hData);

            [DllImport("user32", CharSet = CharSet.Auto)]
            public extern static int RemoveProp(
              IntPtr hwnd,
              string lpString);


            [DllImport("user32")]
            public static extern int PostMessage(IntPtr hwnd, int wMsg, int wParam, int lParam);

            [DllImport("user32", CharSet = CharSet.Auto)]
            public extern static int SendMessage(
              IntPtr hWnd,
              int wMsg,
              IntPtr wParam,
              IntPtr lParam);

            [DllImport("user32.dll")]
            [return: MarshalAs(UnmanagedType.Bool)]
            public static extern bool RegisterHotKey(IntPtr hWnd, int id, uint fsModifiers, uint vk);

            [DllImport("user32.dll")]
            [return: MarshalAs(UnmanagedType.Bool)]
            public static extern bool UnregisterHotKey(IntPtr hWnd, int id);

            [DllImportAttribute("user32.dll")]
            public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
            [DllImportAttribute("user32.dll")]
            public static extern bool ReleaseCapture();

            [DllImport("user32.dll")]
            public static extern IntPtr GetWindowDC(IntPtr hWnd);
            [DllImport("user32.dll")]
            public static extern IntPtr ReleaseDC(IntPtr hWnd, IntPtr hDC);
            /// <summary>
            /// SendMessage for WM_COPYDATA
            /// </summary>
            /// <param name="hwnd"></param>
            /// <param name="wMsg">Should pass WM_COPYDATA</param>
            /// <param name="wParam">Pass source hWnd or IntPtr.Zero</param>
            /// <param name="lParam"></param>
            /// <returns>true if target handles message, false otherwise</returns>
            [DllImport("user32", CharSet = CharSet.Auto)]
            public extern static int SendMessage(IntPtr hwnd, int wMsg, IntPtr wParam, ref COPYDATASTRUCT lParam);

            [Flags]
            internal enum SendMessageTimeoutFlags : uint
            {
                SMTO_NORMAL = 0x0000,
                SMTO_BLOCK = 0x0001,
                SMTO_ABORTIFHUNG = 0x0002,
                SMTO_NOTIMEOUTIFNOTHUNG = 0x0008
            }

            [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
            public static extern int SendMessageTimeout(IntPtr windowHandle,
                                                        uint Msg,
                                                        IntPtr wParam,
                                                        ref COPYDATASTRUCT lParam,
                                                        SendMessageTimeoutFlags flags,
                                                        uint timeout,
                                                        out IntPtr result);

            [DllImport("user32.dll", EntryPoint = "GetSystemMenu", SetLastError = true,
              CharSet = CharSet.Auto, ExactSpelling = true,
              CallingConvention = CallingConvention.Winapi)]
            public static extern IntPtr GetSystemMenu(IntPtr WindowHandle, int bReset);

            [DllImport("user32.dll")]
            public static extern bool TrackPopupMenu(IntPtr hMenu, uint uFlags, int x, int y,
                                                     int nReserved, IntPtr hWnd, IntPtr prcRect);

            [StructLayout(LayoutKind.Sequential)]
            internal struct MENUITEMINFO
            {
                public uint cbSize;
                public uint fMask;
                public uint fType;
                public uint fState;
                public uint wID;
                public IntPtr hSubMenu;
                public IntPtr hbmpChecked;
                public IntPtr hbmpUnchecked;
                public IntPtr dwItemData;
                public string dwTypeData;
                public uint cch;
                public IntPtr hbmpItem;

                // return the size of the structure
                internal static uint sizeOf
                {
                    get { return (uint)Marshal.SizeOf(typeof(MENUITEMINFO)); }
                }

                public void reset()
                {
                    cbSize = sizeOf;
                    fMask = 0;
                    fType = 0;
                    fState = 0;
                    wID = 0;
                    hSubMenu = IntPtr.Zero;
                    hbmpChecked = IntPtr.Zero;
                    hbmpUnchecked = IntPtr.Zero;
                    dwItemData = IntPtr.Zero;
                    dwTypeData = null;
                    cch = 0;
                    hbmpItem = IntPtr.Zero;
                }
            }
            public const uint MIIM_STATE = 0x00000001;
            public const uint MFS_GRAYED = 0x00000003;

            [DllImport("user32.dll")]
            public static extern bool SetMenuItemInfo(IntPtr hMenu, uint uItem, bool fByPosition, [In] ref MENUITEMINFO lpmii);

            public static IntPtr IntPtr_TOPMOST = (IntPtr)(-2);
            public static IntPtr IntPtr_NOTOPMOST = (IntPtr)(-2);

            public const int WS_BORDER = 0x800000;
            public const int WS_CAPTION = 0xC00000;
            public const int WS_CHILD = 0x40000000;
            public const int WS_CHILDWINDOW = (WS_CHILD);
            public const int WS_CLIPCHILDREN = 0x2000000;
            public const int WS_CLIPSIBLINGS = 0x4000000;
            public const int WS_DISABLED = 0x8000000;
            public const int WS_DLGFRAME = 0x400000;
            public const int WS_EX_ACCEPTFILES = 0x10;
            public const int WS_EX_DLGMODALFRAME = 0x1;
            public const int WS_EX_NOPARENTNOTIFY = 0x4;
            public const int WS_EX_TOPMOST = 0x8;
            public const int WS_EX_TRANSPARENT = 0x20;
            public const int WS_GROUP = 0x20000;
            public const int WS_HSCROLL = 0x100000;
            public const int WS_ICONIC = WS_MINIMIZE;
            public const int WS_MAXIMIZE = 0x1000000;
            public const int WS_MAXIMIZEBOX = 0x10000;
            public const int WS_MINIMIZE = 0x20000000;
            public const int WS_MINIMIZEBOX = 0x20000;
            public const int WS_OVERLAPPED = 0x0;
            public const int WS_OVERLAPPEDWINDOW = (WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX);
            public const int WS_POPUP = unchecked((int)0x80000000);
            public const int WS_POPUPWINDOW = (WS_POPUP | WS_BORDER | WS_SYSMENU);
            public const int WS_SIZEBOX = WS_THICKFRAME;
            public const int WS_SYSMENU = 0x80000;
            public const int WS_TABSTOP = 0x10000;
            public const int WS_THICKFRAME = 0x40000;
            public const int WS_TILED = WS_OVERLAPPED;
            public const int WS_TILEDWINDOW = WS_OVERLAPPEDWINDOW;
            public const int WS_VISIBLE = 0x10000000;
            public const int WS_VSCROLL = 0x200000;
        }

        /// <summary>
        /// Exposes methods from the Win32API Shell
        /// </summary>
        internal class Shell
        {
            [DllImport("shell32.dll", CharSet = CharSet.Auto, SetLastError = true)]
            static extern IntPtr CommandLineToArgvW(string lpCmdLine, out int pNumArgs);

            public const uint SHGFI_ICON = 0x100;
            public const uint SHGFI_LARGEICON = 0x0; // 'Large icon
            public const uint SHGFI_SMALLICON = 0x1; // 'Small icon

            [DllImport("shell32.dll")]
            public static extern IntPtr SHGetFileInfo(string pszPath, uint dwFileAttributes, ref SHFILEINFO psfi, uint cbSizeFileInfo, uint uFlags);
            /// <summary>
            /// Splits a command line string into its argument pieces
            /// </summary>
            /// <param name="commandLine"></param>
            /// <returns></returns>
            public static string[] CommandLineToArgs(string commandLine)
            {
                int argCount;
                IntPtr result = CommandLineToArgvW(commandLine, out argCount);

                if (result == IntPtr.Zero)
                {
                    throw new System.ComponentModel.Win32Exception();
                }
                try
                {
                    string[] args = new string[argCount];
                    for (int i = 0; i < args.Length; i++)
                    {
                        IntPtr pStr = Marshal.ReadIntPtr(result, i * IntPtr.Size);
                        string arg = Marshal.PtrToStringUni(pStr);
                        args[i] = arg;
                    }

                    return args;
                }
                finally
                {
                    Marshal.FreeHGlobal(result);
                }
            }
        }

        internal class Kernel
        {
            [DllImport("kernel32.dll", SetLastError = true)]
            public static extern int GlobalFree(IntPtr hMem);
        }
    }
}
