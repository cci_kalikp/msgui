﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;

namespace MorganStanley.MSDotNet.MSGui.Impl.FusionMessagingImpl
{
    internal sealed class FusionReceivedMessage
    {
        private string m_channel;
        private ApplicationInfo m_senderAppInfo;
        private FusionMessageData m_messageData;

        /// <summary>
        /// Initializes a new instance of the <see cref="FusionReceivedMessage"/> class.
        /// </summary>
        /// <param name="channel_">The channel from where the message has arrived.</param>
        /// <param name="senderAppInfo_">The sender application's info.</param>
        /// <param name="messageData_">The message data. Can be null.</param>
        public FusionReceivedMessage(string channel_, ApplicationInfo senderAppInfo_, FusionMessageData messageData_)
        {
            m_channel = channel_;
            m_senderAppInfo = senderAppInfo_;
            m_messageData = messageData_;
        }

        /// <summary>
        /// Gets the channel from where the message has arrived.
        /// </summary>
        public string Channel
        {
            get
            {
                return m_channel;
            }
        }

        /// <summary>
        /// Gets the sender application's info.
        /// </summary>
        public ApplicationInfo SenderAppInfo
        {
            get
            {
                return m_senderAppInfo;
            }
        }

        /// <summary>
        /// Gets the message data. Can be null.
        /// </summary>
        public FusionMessageData MessageData
        {
            get
            {
                return m_messageData;
            }
        }
    }
}
