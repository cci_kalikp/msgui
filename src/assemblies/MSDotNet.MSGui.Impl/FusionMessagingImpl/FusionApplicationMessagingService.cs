﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;

namespace MorganStanley.MSDotNet.MSGui.Impl.FusionMessagingImpl
{
    internal delegate void OnMessageCallback<TPayload>(ReceivedMessage<TPayload> message);
    internal delegate void OnChannelMessageCallback<TPayload>(string channel, ReceivedMessage<TPayload> message);
    internal delegate void OnFusionMessageCallback(FusionReceivedMessage message);

    internal class FusionApplicationMessagingService
    {
        private const string MessageDataKey = "##BUILTIN_MSGDATA##";

        private readonly string _hostName;
        private readonly string _userName;
        private readonly string _desktop;
        private readonly string _application;
        private readonly string _instance;
        private readonly object _sync = new object();
        private readonly Dictionary<string, object> _subscriptions = new Dictionary<string, object>();

        private bool _isInitialized;
        private FSNDesktopService _desktopService;

        /// <summary>
        /// Initializes a new instance of the <see cref="FusionApplicationMessagingService"/> class.
        /// </summary>
        /// <param name="hostName">The host name where this application is running.</param>
        /// <param name="userName">The user name who is running this application instance.</param>
        /// <param name="desktop">The desktop that this application belongs to.</param>
        /// <param name="application">The application name.</param>
        /// <param name="instance">The application instance identifier.</param>
        public FusionApplicationMessagingService(string hostName, string userName, string desktop, string application, string instance)
        {
            _hostName = hostName;
            _userName = userName;
            _desktop = desktop;
            _application = application;
            _instance = instance;

            ApplicationInfo = new ApplicationInfo(_hostName, _userName, _desktop, _application, _instance);
            FSNAppContext.LocalApp = new FSNAppContext(_hostName, _userName, new FSNDesktopID(_desktop), new FSNAppID(_application), _instance);
            _desktopService = new FSNDesktopService(FSNAppContext.LocalApp, new FSNDesktopMsgService());

            Init();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FusionApplicationMessagingService"/> class.
        /// </summary>
        /// <param name="hostName">The host name where this application is running.</param>
        /// <param name="userName">The user name who is running this application instance.</param>
        /// <param name="desktop">The desktop that this application belongs to.</param>
        /// <param name="application">The application name.</param>
        /// <param name="instance">The application instance identifier.</param>
        /// <param name="desktopService">The desktop service.</param>
        internal FusionApplicationMessagingService(string hostName, string userName, string desktop, string application, string instance, FSNDesktopService desktopService)
        {
            _hostName = hostName;
            _userName = userName;
            _desktop = desktop;
            _application = application;
            _instance = instance;

            _desktopService = desktopService;

            ApplicationInfo = new ApplicationInfo(_hostName, _userName, _desktop, _application, _instance);
            Init();
        }

        public ApplicationInfo ApplicationInfo { get; protected set; }

        /// <summary>
        /// Initializes the messaging service with the specified application information.
        /// </summary>
        internal void Init()
        {
            lock (_sync)
            {
                if (_isInitialized)
                {
                    throw new InvalidOperationException(
                      "Messaging service has already been initialized. To re-initialize it, call Finish() first.");
                }

                Guard.ArgumentNotNull(ApplicationInfo, "appInfo_");

                // BUG bendel - due to fusion's limited implementation we can only use one ApplicationInfo for all messaging services - not a problem now
                if (FSNAppContext.LocalApp == null)
                {
                    FSNAppContext.LocalApp = CreateAppContext(ApplicationInfo);
                }

                if (_desktopService == null)
                {
                    // creating message service
                    var desktopMsgService = new FSNDesktopMsgService();
                    desktopMsgService.Init();

                    // creating desktop service
                    _desktopService = new FSNDesktopService(desktopMsgService);
                }
                else
                {
                    _desktopService.MsgService.Init();
                }

                _isInitialized = true;
            }
        }

        /// <summary>
        /// Gets an instance of the <see cref="FSNDesktopService"/>.
        /// </summary>
        internal FSNDesktopService DesktopService
        {
            get
            {
                return _desktopService;
            }
        }

        #region Internal static methods
        /// <summary>
        /// Converts the specifed application information to fusion's application context.
        /// </summary>
        /// <param name="appInfo">The application information to convert.</param>
        /// <returns>The fusion application context.</returns>
        internal static FSNAppContext CreateAppContext(ApplicationInfo appInfo)
        {
            return new FSNAppContext(
              appInfo.Host,
              appInfo.User,
              new FSNDesktopID(appInfo.Desktop),
              new FSNAppID(appInfo.Name),
              appInfo.Instance);
        }

        /// <summary>
        /// Converts the specified fusion application context to application information.
        /// </summary>
        /// <param name="appContext">The fusion application context to convert.</param>
        /// <returns>The application information.</returns>
        internal static ApplicationInfo CreateApplicationInfo(FSNAppContext appContext)
        {
            return new ApplicationInfo(
              appContext.Host,
              appContext.User,
              appContext.Desktop.Name,
              appContext.AppID.Id,
              appContext.Instance);
        }

        /// <summary>
        /// Creates fusion desktop msg data from the specified object.
        /// </summary>
        /// <param name="messageData_">The object to put into the fusion message data.</param>
        /// <returns>The fusion message data that contains the specified object</returns>
        internal static FSNDesktopMsgData CreateDesktopMsgData(object messageData_)
        {
            var desktopMsgData = new FSNDesktopMsgData();
            using (MemoryStream memStream = new MemoryStream())
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(memStream, messageData_);

                desktopMsgData.Add(
                  MessageDataKey,
                  Convert.ToBase64String(memStream.ToArray()));
            }

            return desktopMsgData;
        }

        /// <summary>
        /// Extracts the message data from the fusion desktop msg data.
        /// </summary>
        /// <param name="fsnMessageData_">The fusion desktop msg data that holds our message data.</param>
        /// <returns>The extracted message data.</returns>
        internal static object ExtractMessageData(FSNDesktopMsgData fsnMessageData_)
        {
            object messageData = null;
            if (fsnMessageData_ != null)
            {
                // extracting message data
                string messageDataString;
                if (fsnMessageData_.TryGetValue(MessageDataKey, out messageDataString))
                {
                    using (MemoryStream memStream = new MemoryStream(Convert.FromBase64String(messageDataString)))
                    {
                        BinaryFormatter formatter = new BinaryFormatter();
                        messageData = formatter.Deserialize(memStream);
                    }
                }
            }

            return messageData;
        }

        /// <summary>
        /// Creates fusion desktop msg data from the specified parameter.
        /// </summary>
        /// <param name="messageData_">The message data.</param>
        /// <returns>The fusion desktop msg data.</returns>
        internal static FSNDesktopMsgData CreateDesktopMsgData(FusionMessageData messageData_)
        {
            var fsnMessageData = new FSNDesktopMsgData();
            foreach (var keyValuePair in messageData_)
            {
                fsnMessageData.Add(keyValuePair.Key, keyValuePair.Value);
            }
            return fsnMessageData;
        }
        #endregion

        #region IApplicationMessagingService Members
        /// <summary>
        /// Starts the messaging service.
        /// </summary>
        /// <remarks>
        /// <para>
        /// This method joins the desktop, so after it is called, it is possible to publish messages to the desktop,
        /// and also subscribed callbacks will be called, if message is published to the desktop by another application.
        /// </para>
        /// <para>
        /// It is possible to call <see cref="Subscribe{TPayload}(OnMessageCallback{TPayload})"/> or
        /// <see cref="SubscribeChannel{TPayload}(string, OnChannelMessageCallback{TPayload})"/> before calling <see cref="Start"/>.
        /// However it is not possible to publish a message.
        /// </para>
        /// <para>
        /// After this method is called the application must be prepared for receiving messages.
        /// It means the application should <see cref="Subscribe{TPayload}"/> for everything it might need,
        /// and then call <see cref="Start"/>.
        /// </para>
        /// <para>
        /// Typically this method should be executed right after creating an instance of the implementing class.
        /// However in case of application activation, it might be a good idea to call <see cref="Start"/>
        /// later, because after <see cref="Start"/> has been called, the application might receive
        /// those retained messages that are re-sent to it by the activation service (after it has started the application).
        /// </para>
        /// </remarks>
        public virtual void Start()
        {
            _desktopService.Join();
        }

        /// <summary>
        /// Stops the messaging service.
        /// </summary>
        /// <remarks>
        /// <para>
        /// It leaves the desktop, which means, the service will not get any message published to this desktop.
        /// Also, it is not possible to publish messages.
        /// </para>
        /// <para>
        /// If necessary, <see cref="Start"/> can be called again, then messaging service should function properly.
        /// </para>
        /// </remarks>
        public virtual void Stop()
        {
            _desktopService.Leave();
        }

        /// <summary>
        /// Publishes the specified payload to the current desktop
        /// </summary>
        /// <typeparam name="TPayload">The type of the payload to publish.</typeparam>
        /// <param name="payload_">The payload to publish.</param>
        /// <remarks>
        /// <para>
        /// The payload will be published to the current desktop and to all applications.
        /// </para>
        /// </remarks>
        public virtual void Publish<TPayload>(TPayload payload_)
        {
            Publish(
              typeof(TPayload).FullName,
              new FusionMessageData(CreateDesktopMsgData(payload_)));
        }

        /// <summary>
        /// Publishes the specified payload to the specified target application.
        /// </summary>
        /// <typeparam name="TPayload">The type of the payload to publish.</typeparam>
        /// <param name="application_">The target application.</param>
        /// <param name="payload_">The payload to publish.</param>
        /// <remarks>
        /// <para>
        /// The payload will be published only to the specified desktop to the specified application.
        /// </para>
        /// </remarks>
        public virtual void Publish<TPayload>(string application_, TPayload payload_)
        {
            Publish(application_, "default", payload_);
        }

        /// <summary>
        /// Publishes the specified payload to the specified target application identified by the specified instance identifier.
        /// </summary>
        /// <typeparam name="TPayload">The type of the payload to publish.</typeparam>
        /// <param name="application_">The target application.</param>
        /// <param name="instance_">The target application instance identifier.</param>
        /// <param name="payload_">The payload to publish.</param>
        /// <remarks>
        /// <para>
        /// The payload will be published only to the specified desktop to the specified application identified by the specified instance identifier.
        /// </para>
        /// </remarks>
        public virtual void Publish<TPayload>(string application_, string instance_, TPayload payload_)
        {
            string channel = typeof(TPayload).FullName;
            Send(
              CreateAppInfoAppContext(application_, instance_),
              channel,
              new FusionMessageData(CreateDesktopMsgData(payload_)));
        }

        /// <summary>
        /// Publishes the specified payload on the given channel.
        /// </summary>
        /// <typeparam name="TPayload">The type of the payload to publish.</typeparam>
        /// <param name="channel_">The channel to be used for publishing.</param>
        /// <param name="payload_">The payload to publish.</param>
        /// <remarks>
        /// <para>
        /// The payload will be published to all desktops and to all applications.
        /// </para>
        /// </remarks>
        public virtual void PublishChannel<TPayload>(string channel_, TPayload payload_)
        {
            Publish(
              channel_,
              new FusionMessageData(CreateDesktopMsgData(payload_)));
        }

        /// <summary>
        /// Publishes the specified payload on the given channel to the specified target application.
        /// </summary>
        /// <typeparam name="TPayload">The type of the payload to publish.</typeparam>
        /// <param name="application_">The target application.</param>
        /// <param name="channel_">The channel to be used for publishing.</param>
        /// <param name="payload_">The payload to publish.</param>
        /// <remarks>
        /// <para>
        /// The payload will be published only to the specified desktop to the specified application.
        /// </para> 
        /// </remarks>
        public virtual void PublishChannel<TPayload>(string application_, string channel_, TPayload payload_)
        {
            PublishChannel(application_, "default", channel_, payload_);
        }

        /// <summary>
        /// Publishes the specified payload on the given channel to the specified target application identified by the specified instance identifier.
        /// </summary>
        /// <typeparam name="TPayload">The type of the payload to publish.</typeparam>
        /// <param name="application_">The target application.</param>
        /// <param name="instance_">The target application instance identifier.</param>
        /// <param name="channel_">The channel to be used for publishing.</param>
        /// <param name="payload_">The payload to publish.</param>
        /// <remarks>
        /// <para>
        /// The payload will be published only to the specified desktop to the specified application identified by the specified instance identifier.
        /// </para>
        /// </remarks>
        public virtual void PublishChannel<TPayload>(string application_, string instance_, string channel_, TPayload payload_)
        {
            Send(
              CreateAppInfoAppContext(application_, instance_),
              channel_,
              new FusionMessageData(CreateDesktopMsgData(payload_)));
        }

        /// <summary>
        /// Subscribes to the specified payload type.
        /// </summary>
        /// <typeparam name="TPayload">The type of the payload to subscribe to.</typeparam>
        /// <param name="callback_">The callback function that will be called whenever a message arrives with the specified payload type.</param>
        /// <remarks>
        /// <para>
        /// It is possible to subscribe to a particular payload type with multiple subscriber callbacks.
        /// </para>
        /// </remarks>
        public virtual void Subscribe<TPayload>(OnMessageCallback<TPayload> callback_)
        {
            
            lock (_sync)
            {
                string channel = typeof(TPayload).FullName;
                if (_subscriptions.ContainsKey(channel))
                {
                    throw new ArgumentException(String.Format("Already subscribed to channel '{0}'.", channel));
                }

                _subscriptions.Add(channel, null);
                _desktopService.MsgService.Subscribe(
                  channel,
                  delegate(object sender_, FSNDesktopMsgEventArgs e_)
                  {
                      object payload = ExtractMessageData(e_.Data);
                      if (payload is TPayload)
                      {
                          callback_(new ReceivedMessage<TPayload>(
                            CreateApplicationInfo(e_.Source),
                            (TPayload)payload));
                      }
                  });
            }
        }

        /// <summary>
        /// Subscribes to the specified channel with the given payload type.
        /// </summary>
        /// <typeparam name="TPayload">The type of the payload to subscribe to.</typeparam>
        /// <param name="channel_">The channel to be used for subscribing.</param>
        /// <param name="callback_">The callback function that will be called whenever a message arrives on the specified channel with the specified payload type.</param>
        /// <para>
        /// It is possible to subscribe to a particular payload type with multiple subscriber callbacks.
        /// </para>
        public virtual void SubscribeChannel<TPayload>(string channel_, OnChannelMessageCallback<TPayload> callback_)
        {
            
            lock (_sync)
            {
                if (_subscriptions.ContainsKey(channel_))
                {
                    throw new ArgumentException(String.Format("Already subscribed to channel '{0}'.", channel_));
                }

                _subscriptions.Add(channel_, null);
                _desktopService.MsgService.Subscribe(
                  channel_,
                  delegate(object sender_, FSNDesktopMsgEventArgs e_)
                  {
                      object payload = ExtractMessageData(e_.Data);
                      if (payload is TPayload)
                      {
                          callback_(
                                    channel_,
                                    new ReceivedMessage<TPayload>(
                                      CreateApplicationInfo(e_.Source),
                                      (TPayload)payload));
                      }
                  });
            }
        }

        /// <summary>
        /// Unsubscribes the callback from the specified payload type.
        /// </summary>
        /// <typeparam name="TPayload">The type of the payload to unsubscribe from.</typeparam>
        /// <param name="callback_">Callback delegate.</param>
        public virtual void Unsubscribe<TPayload>(OnMessageCallback<TPayload> callback_)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Unsubscribes the callback from the specified channel
        /// </summary>
        /// <param name="channel_">The channel to be used for subscribing.</param>
        /// <param name="callback_">The callback function that will be called whenever a message arrives on the specified channel with the specified payload type.</param>
        /// <typeparam name="TPayload">The type of the payload to unsubscribe from.</typeparam>
        public virtual void UnsubscribeChannel<TPayload>(string channel_, OnChannelMessageCallback<TPayload> callback_)
        {
            Guard.ArgumentNotNullOrEmptyString(channel_, "channel_");

            lock (_sync)
            {
                if (!_subscriptions.ContainsKey(channel_))
                {
                    throw new ArgumentException(String.Format("Has not subscribed to channel '{0}'.", channel_));
                }

                _desktopService.MsgService.Unsubscribe(channel_);
                _subscriptions.Remove(channel_);
            }
        }

        /// <summary>
        /// Returns true if there is a subscription to the specified event.
        /// </summary>
        /// <param name="channel_">The channel name.</param>
        /// <returns>True if there is a subscription to the specified event.</returns>
        public bool IsSubscribed(string channel_)
        {
            Guard.ArgumentNotNullOrEmptyString(channel_, "channel_");

            lock (_sync)
            {
                return _subscriptions.ContainsKey(channel_);
            }
        }
        #endregion

        #region Fusion specific API
        /// <summary>
        /// Publishes the specified message data object on the given channel.
        /// </summary>
        /// <param name="channel_">The channel where the message has to be published.</param>
        /// <param name="messageData_">The message data. Can be null.</param>
        /// <remarks>
        /// If <paramref name="messageData_"/> is not null, it must be serializable.
        /// </remarks>
        public void Publish(string channel_, FusionMessageData messageData_)
        {
            if (messageData_ == null)
            {
                _desktopService.MsgService.Publish(channel_);
            }
            else
            {
                _desktopService.MsgService.Publish(channel_, CreateDesktopMsgData(messageData_));
            }
        }

        /// <summary>
        /// Sends the specified event with the payload object to the specified application.
        /// </summary>
        /// <typeparam name="TPayload">The payload type.</typeparam>
        /// <param name="destinationApp">The destination application's info.</param>
        /// <param name="channel">The channel name.</param>
        /// <param name="payload_">The payload. Can be null.</param>
        /// <remarks>
        /// If <paramref name="payload_"/> is not null, it must be serializable.
        /// </remarks>
        public void Send<TPayload>(ApplicationInfo destinationApp, string channel, TPayload payload_)
        {
         
            Send(
              destinationApp,
              channel,
              new FusionMessageData(CreateDesktopMsgData(payload_)));
        }

        /// <summary>
        /// Sends the specified message data object on the given channel to the specified application.
        /// </summary>
        /// <param name="destinationApp">The destination application's info.</param>
        /// <param name="channel">The channel where the message has to be published.</param>
        /// <param name="messageData">The message data. Can be null.</param>
        /// <remarks>
        /// If <paramref name="messageData"/> is not null, it must be serializable.
        /// </remarks>
        public void Send(ApplicationInfo destinationApp, string channel, FusionMessageData messageData)
        {
            
            if (messageData == null)
            {
                _desktopService.MsgService.Send(
                                                 CreateFSNAppContext(destinationApp.Name, destinationApp.Instance),
                                                 channel);
            }
            else
            {
                _desktopService.MsgService.Send(
                                            CreateFSNAppContext(destinationApp.Name, destinationApp.Instance),
                                            channel,
                                            CreateDesktopMsgData(messageData));
            }
        }

        /// <summary>
        /// Subscribes a callback to the specified channel.
        /// </summary>
        /// <param name="channel_">The channel where the callback has to be subscribed.</param>
        /// <param name="callback_">The callback that has to be subscribed to the channel.</param>
        /// <remarks>
        /// Note that the callback might be called in a different thread!
        /// </remarks>  
        public virtual void Subscribe(string channel_, OnFusionMessageCallback callback_)
        {   
            Guard.ArgumentNotNullOrEmptyString(channel_, "channel_");
            Guard.ArgumentNotNull(callback_, "callback_");

            lock (_sync)
            {
                if (_subscriptions.ContainsKey(channel_))
                {
                    throw new ArgumentException(String.Format("Already subscribed to channel '{0}'.", channel_));
                }

                _subscriptions.Add(channel_, null);
                _desktopService.MsgService.Subscribe(
                  channel_,
                  delegate(object sender_, FSNDesktopMsgEventArgs e_)
                  {
                      FusionMessageData messageData = null;
                      if (e_.Data != null)
                      {
                          messageData = new FusionMessageData(e_.Data);
                      }

                      callback_(new FusionReceivedMessage(
                        channel_,
                        CreateApplicationInfo(e_.Source),
                        messageData));
                  });
            }
        }

        #endregion

        #region Helper methods
        /// <summary>
        /// Creates an <see cref="ApplicationInfo"/> based on given arguments and 
        /// values in Internal.Identifiers.FSNAppContext.LocalApp.
        /// </summary>
        /// <param name="name">Application Name.</param>
        /// <param name="instance">Instance Name.</param>
        /// <returns><see cref="ApplicationInfo"/> created.</returns>
        private static ApplicationInfo CreateAppInfoAppContext(string name, string instance)
        {
            var context = new ApplicationInfo(
                                              FSNAppContext.LocalApp.Host,
                                              FSNAppContext.LocalApp.User,
                                              FSNAppContext.LocalApp.Desktop.Name,
                                              name,
                                              instance);
            return context;
        }

        /// <summary>
        /// Creates an <see cref="Internal.Identifiers.FSNAppContext"/> based on given arguments and 
        /// values in Internal.Identifiers.FSNAppContext.LocalApp.
        /// </summary>
        /// <param name="name">The Application Name.</param>
        /// <param name="instance">The Instance Name.</param>
        /// <returns>The Application Context.</returns>
        private static FSNAppContext CreateFSNAppContext(string name, string instance)
        {
            var context = new FSNAppContext(
                                            FSNAppContext.LocalApp.Host,
                                            FSNAppContext.LocalApp.User,
                                            FSNAppContext.LocalApp.Desktop,
                                            new FSNAppID(name),
                                            instance);
            return context;
        }

        #endregion
    }
}
