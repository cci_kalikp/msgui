﻿using System;

namespace MorganStanley.MSDotNet.MSGui.Impl.FusionMessagingImpl
{
    internal class FSNDesktopMsgEventArgs : EventArgs
    {
        #region Construction

        public FSNDesktopMsgEventArgs(FSNAppContext source_, FSNDesktopMsgData data_)
        {
            _source = source_;
            _data = data_;
        }

        #endregion

        #region Public API

        /// <summary>
        /// Identifier of application that posted this message
        /// </summary>
        public FSNAppContext Source
        {
            get { return _source; }
        }

        /// <summary>
        /// Data associated with this message
        /// </summary>
        public FSNDesktopMsgData Data
        {
            get { return _data; }
        }

        #endregion

        #region Private Data

        private readonly FSNAppContext _source;
        private readonly FSNDesktopMsgData _data;

        #endregion
    }
}
