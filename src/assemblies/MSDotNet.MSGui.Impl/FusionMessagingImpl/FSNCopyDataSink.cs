﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace MorganStanley.MSDotNet.MSGui.Impl.FusionMessagingImpl
{
    internal class FSNCopyDataSink<TMessage> : NativeWindow, IDisposable
    {
        private Form _form;

        public event EventHandler OnInit;
        public event EventHandler OnChannelsUpdated;
        public event EventHandler OnCloseRequest;

        private readonly IFSNStreamMarshaller<TMessage> _marshaller;

        public FSNCopyDataSink(IFSNStreamMarshaller<TMessage> marshaller_)
        {
            FSNGuard.ArgumentNotNull(marshaller_, "marshaller_");
            channels = new CopyDataChannels<TMessage>(this);
            channels.OnChannelsUpdated += new EventHandler(ChannelsUpdated);
            _marshaller = marshaller_;
            _msgThread = new Thread(new ThreadStart(MsgLoop));
            _msgThread.IsBackground = true;
        }

        public void Init()
        {
            if (_form != null)
            {
                throw new InvalidOperationException("FSNCopyDataSink already inited.");
            }
            _form = new FSNCopyDataForm();
            _form.HandleCreated += new EventHandler(_form_HandleCreated);
            _form.HandleDestroyed += new EventHandler(_form_HandleDestroyed);
            _msgThread.Start();
            RegisterHandle();
        }

        public void Stop()
        {
            lock (_sync)
            {
                _quit = true;
                Monitor.Pulse(_sync);
            }
        }

        public IEnumerable<string> ChannelSet
        {
            get
            {
                IList<string> ret = new List<string>();
                foreach (string channel in Channels.Channels)
                {
                    ret.Add(channel);
                }
                return ret;
            }
        }

        public void Subscribe(string channel_, object state_)
        {
            Channels.Add(channel_, state_);
        }

        public void Subscribe(string channel_, FSNCopyDataReceivedEventHandler<TMessage> handler_, object state_)
        {
            Channels.Add(channel_, handler_, state_);
        }

        public void Unsubscribe(string channel_)
        { 
            Channels.Remove(channel_);
        }

        public RxMsgFilter<TMessage> MsgFilter
        {
            get { return _filter; }
            set { _filter = value; }
        }

        private void _form_HandleDestroyed(object sender, EventArgs e)
        {
        }

        private void _form_HandleCreated(object sender, EventArgs e)
        {
            RegisterHandle();
        }

        private void ChannelsUpdated(object sender_, EventArgs e_)
        {
            EventHandler temp = OnChannelsUpdated;
            if (temp != null)
            {
                temp(this, EventArgs.Empty);
            }
        }

        private void RegisterHandle()
        {
            if (!_form.Handle.Equals(IntPtr.Zero))
            {
                if (!base.Handle.Equals(IntPtr.Zero))
                {
                    base.ReleaseHandle();
                }
                AssignHandle(_form.Handle);
                channels.OnHandleCreated();
                EventHandler temp = OnInit;
                if (temp != null)
                {
                    temp(this, EventArgs.Empty);
                }
            }
        }

        #region Member Variables
        private CopyDataChannels<TMessage> channels = null;
        private bool disposed = false;
        #endregion

        /// <summary>
        /// Override for a form's Window Procedure to handle WM_COPYDATA
        /// messages sent by other instances of this class.
        /// </summary>
        /// <param name="m">The Windows Message information.</param>
        protected override void WndProc(ref Message m)
        {
            if (m.Msg == (int)FSNWin32.Msgs.WM_COPYDATA)
            {
                try
                {
                    FSNWin32.COPYDATASTRUCT cds = new FSNWin32.COPYDATASTRUCT();
                    cds = (FSNWin32.COPYDATASTRUCT)Marshal.PtrToStructure(m.LParam, typeof(FSNWin32.COPYDATASTRUCT));
                    if (cds.cbData > 0)
                    {
                        byte[] data = new byte[cds.cbData];
                        Marshal.Copy(cds.lpData, data, 0, cds.cbData);
                        if (FSNCopyDataMessage.IsCopyDataMsg(data))
                        {
                            // process message
                            Process(m.WParam, data);
                            // Mark as handled (not that seems to do anything)
                            m.Result = (IntPtr)1;
                            // Don't forward to the base class - it was OURS!
                            return;
                        }
                    }
                }
                catch (Exception e1)
                {
                    // Usually caused by invalid message
                    System.Diagnostics.Debug.WriteLine(e1.ToString());
                    // Return 0 since we didn't handle it (not that seems to do anything)
                    m.Result = (IntPtr)0;
                }
            }
            else if (m.Msg == (int)FSNWin32.Msgs.WM_QUERYENDSESSION)
            {
                //NOTE bendel - ORIGINAL Fusion issue: upcall quit for AppStyle = Tray
                EventHandler temp = OnCloseRequest;
                if (temp != null)
                {
                    temp(this, EventArgs.Empty);
                }
            }
            else if (m.Msg == (int)FSNWin32.Msgs.WM_DESTROY)
            {
                // WM_DESTROY Dispatches before OnHandleChanged and is
                // a better place to ensure that we've cleared 
                // everything up.
                channels.OnHandleChange();
                base.OnHandleChange();
            }
            base.WndProc(ref m);
        }

        private void Process(IntPtr src_, byte[] raw_)
        {
            Inject(new FSNCopyDataMsgBuilder<TMessage>(_marshaller, src_, raw_));
        }

        /// <summary>
        /// Inject message proxy object into sinks rx queue.
        /// </summary>
        /// <param name="builder_"></param>
        public void Inject(IFSNMsgBuilder<TMessage> builder_)
        {
            lock (_sync)
            {
                _msgQueue.Add(builder_);
                Monitor.Pulse(_sync);
            }
        }

        private void MsgLoop()
        {
            while (!_quit && !disposed)
            {
                IList<IFSNMsgBuilder<TMessage>> builders = null;
                lock (_sync)
                {
                    while (_msgQueue.Count == 0 && !_quit && !disposed)
                    {
                        Monitor.Wait(_sync);
                    }
                    if (_quit) { return; }
                    // adopt builders
                    builders = _msgQueue;
                    _msgQueue = new List<IFSNMsgBuilder<TMessage>>();
                }
                foreach (IFSNMsgBuilder<TMessage> builder in builders)
                {
                    HandleMessage(builder);
                }
            }
        }

        private void HandleMessage(IFSNMsgBuilder<TMessage> builder_)
        {
            try
            {
                if (channels != null && channels.Contains(builder_.Channel))
                {
                    RxMsgFilter<TMessage> filter = _filter;
                    // retrieve per channel state
                    object state = channels[builder_.Channel].State;
                    // extract channel
                    string channel = builder_.Channel;
                    // extract message
                    TMessage msg = builder_.Build(state);
                    // maybe trace message
                    if (filter != null)
                    {
                        // call message filter
                        if (!filter(this, builder_.Src, channel, msg))
                        {
                            // filter says no
                            return;
                        }
                    }
                    // construct event args
                    FSNCopyDataReceivedEventArgs<TMessage> args = new FSNCopyDataReceivedEventArgs<TMessage>(
                      channel,
                      msg,
                      state
                      );
                    // upcall
                    Dispatch(args);
                }
            }
            catch (Exception)
            {
                //NOTE bendel - ORIGINAL Fusion issue: take appropriate action
            }
        }

        private void Dispatch(FSNCopyDataReceivedEventArgs<TMessage> e)
        {
            try
            {
                if (channels != null)
                {
                    CopyDataChannel<TMessage> ch = channels[e.Channel];
                    if (ch != null)
                    {
                        FSNCopyDataReceivedEventHandler<TMessage> handler = ch.Handler;
                        if (handler != null)
                        {
                            handler(this, e);
                        }
                    }
                }
            }
            catch (Exception)
            {
                //NOTE bendel - ORIGINAL Fusion issue: take appropriate action 
            }
        }

        /// <summary>
        /// If the form's handle changes, the properties associated
        /// with the window need to be cleared up. This override ensures
        /// that it is done.  Note that the CopyData class will then
        /// stop responding to events and it should be recreated once
        /// the new handle has been assigned.
        /// </summary>
        protected override void OnHandleChange()
        {
            // need to clear up everything we had set.
            channels.OnHandleChange();
            base.OnHandleChange();
        }

        /// <summary>
        /// Gets the collection of channels.
        /// </summary>
        internal CopyDataChannels<TMessage> Channels
        {
            get
            {
                return this.channels;
            }
        }

        /// <summary>
        /// Clears up any resources associated with this object.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing_)
        {
            if (!disposed)
            {
                channels.Clear(); 
                disposed = true;
                _quit = true;
            }
        }

        /// <summary>
        /// Finalizes a CopyDataSink class which has not been disposed.
        /// There may be a minor resource leak if this class is finalized
        /// after the form it is associated with.
        /// </summary>
        ~FSNCopyDataSink()
        {
            Dispose(false);
        }

        private bool _quit = false;

        private volatile RxMsgFilter<TMessage> _filter;
        private IList<IFSNMsgBuilder<TMessage>> _msgQueue = new List<IFSNMsgBuilder<TMessage>>();

        private readonly object _sync = new object();
        private readonly Thread _msgThread;
    }
}
