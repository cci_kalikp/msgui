﻿using System;
using System.IO;

namespace MorganStanley.MSDotNet.MSGui.Impl.FusionMessagingImpl
{
    internal class FSNCopyDataMsgBuilder<TMessage> : IFSNMsgBuilder<TMessage>
    {
        public FSNCopyDataMsgBuilder(IFSNStreamMarshaller<TMessage> marshaller_, IntPtr src_, byte[] data_)
        {
            _marshaller = marshaller_;
            _src = src_;
            _data = data_;
        }

        #region IMSGBuilder<TMessage> Members

        public string Channel
        {
            get
            {
                return CopyDataMessage.Channel;
            }
        }

        public IntPtr Src
        {
            get { return _src; }
        }

        public TMessage Build(object state_)
        {
            using (MemoryStream ms = new MemoryStream(CopyDataMessage.Data))
            {
                return _marshaller.Unmarshal(ms, state_);
            }
        }

        private FSNCopyDataMessage CopyDataMessage
        {
            get
            {
                if (_copyDataMessage == null)
                {
                    _copyDataMessage = new FSNCopyDataMessage(_data);
                }
                return _copyDataMessage;
            }
        }

        #endregion

        private FSNCopyDataMessage _copyDataMessage;
        private readonly IFSNStreamMarshaller<TMessage> _marshaller;
        private readonly IntPtr _src;
        private readonly byte[] _data;
    }
}
