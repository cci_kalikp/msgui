﻿using System.Collections.Generic;
using System.IO;

namespace MorganStanley.MSDotNet.MSGui.Impl.FusionMessagingImpl
{
    internal class FSNDesktopMsgMarshaller : IFSNStreamMarshaller<FSNDesktopMsg>
    {
        #region IFSNStreamMarshaller<FSNDesktopMsg> Members

        public void Marshal(FSNDesktopMsg msg_, Stream to_, object state_)
        {
            using (BinaryWriter bw = new BinaryWriter(to_))
            {
                // write channel
                FSNMarshal.WriteString(msg_.Channel, bw);
                // write source address
                FSNMarshal.WriteAppContext(msg_.Source, bw);
                // write destination address
                FSNMarshal.WriteAppContext(msg_.Destination, bw);
                // serialize timeout
                FSNMarshal.WriteInt((int)msg_.TimeoutMS, bw);
                // serialize ttl
                FSNMarshal.WriteInt(msg_.TTL, bw);
                // parameter count
                FSNMarshal.WriteInt(msg_.Data.Count, bw);
                // parameters
                foreach (KeyValuePair<string, string> kvp in msg_.Data)
                {
                    FSNMarshal.WriteString(kvp.Key, bw);
                    FSNMarshal.WriteString(kvp.Value, bw);
                }
            }
        }

        public FSNDesktopMsg Unmarshal(Stream from_, object state_)
        {
            using (BinaryReader br = new BinaryReader(from_))
            {
                // read channel
                string channel = FSNMarshal.ReadString(br);
                // read source address
                FSNAppContext source = FSNMarshal.ReadAppContext(br);
                // read destination address
                FSNAppContext dest = FSNMarshal.ReadAppContext(br);
                // read timeout
                uint timeout = (uint)FSNMarshal.ReadInt(br);
                // read ttl
                int ttl = FSNMarshal.ReadInt(br);
                // read data
                FSNDesktopMsgData data = new FSNDesktopMsgData();
                // parameter count
                int pc = FSNMarshal.ReadInt(br);
                // parameter data
                for (int i = 0; i < pc; ++i)
                {
                    data.Add(FSNMarshal.ReadString(br), FSNMarshal.ReadString(br));
                }
                // construct message
                FSNDesktopMsg msg = new FSNDesktopMsg();
                msg.Channel = channel;
                msg.Source = source;
                msg.Destination = dest;
                msg.TimeoutMS = timeout;
                msg.TTL = ttl;
                msg.Data = data;
                return msg;
            }
        }

        #endregion
    }
}
