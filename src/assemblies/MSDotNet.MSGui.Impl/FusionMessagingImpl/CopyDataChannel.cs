﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MorganStanley.MSDotNet.MSGui.Impl.FusionMessagingImpl
{
    internal delegate void FSNCopyDataReceivedEventHandler<TMessage>(object sender, FSNCopyDataReceivedEventArgs<TMessage> e);

    internal class CopyDataChannel<TMessage> : IDisposable
    {

        #region Member Variables

        private string channelName = "";
        private object state = null;
        private bool disposed = false;
        private NativeWindow owner = null;
        private FSNCopyDataReceivedEventHandler<TMessage> handler = null;

        #endregion

        /// <summary>
        /// Gets the name associated with this channel.
        /// </summary>
        public string ChannelName
        {
            get { return this.channelName; }
        }

        /// <summary>
        /// Opaque user state
        /// </summary>
        public object State
        {
            get { return this.state; }
        }

        /// <summary>
        /// Delegate for handling messages on the channel
        /// </summary>
        public FSNCopyDataReceivedEventHandler<TMessage> Handler
        {
            get { return handler; }
        }

        private void addChannel()
        {
            // Tag this window with property "channelName"
            if (owner.Handle != IntPtr.Zero)
            {
                FSNWin32.User.SetProp(owner.Handle, this.channelName, (int)owner.Handle);
            }

        }
        private void removeChannel()
        {
            // Remove the "channelName" property from this window
            if (owner.Handle != IntPtr.Zero)
            {
                FSNWin32.User.RemoveProp(owner.Handle, this.channelName);
            }
        }

        /// <summary>
        /// If the form's handle changes, the properties associated
        /// with the window need to be cleared up. This method ensures
        /// that it is done.  Note that the CopyData class will then
        /// stop responding to events and it should be recreated once
        /// the new handle has been assigned.
        /// </summary>
        internal void OnHandleChange()
        {
            removeChannel();
        }

        internal void OnHandleCreated()
        {
            addChannel();
        }

        /// <summary>
        /// Clears up any resources associated with this channel.
        /// </summary>
        public void Dispose()
        {
            if (!disposed)
            {
                if (channelName.Length > 0)
                {
                    removeChannel();
                }
                channelName = "";
                disposed = true;
                GC.SuppressFinalize(this);
            }
        }

        /// <summary>
        /// Constructs a new instance of a CopyData channel.  Called
        /// automatically by the CopyDataChannels collection.
        /// </summary>
        /// <param name="owner">The owning native window</param>
        /// <param name="channelName">The name of the channel to
        /// send messages on</param>
        /// <param name="state">The state.</param>
        internal CopyDataChannel(NativeWindow owner, string channelName, object state)
        {
            this.owner = owner;
            this.channelName = channelName;
            this.state = state;
            addChannel();
        }


        /// <summary>
        /// Constructs a new instance of a CopyData channel.  Called
        /// automatically by the CopyDataChannels collection.
        /// </summary>
        /// <param name="owner">The owning native window</param>
        /// <param name="channelName">The name of the channel to
        /// send messages on</param>
        /// <param name="state">The state.</param>
        /// <param name="handler">Handler for messages on this channel</param>
        internal CopyDataChannel(NativeWindow owner, string channelName, object state, FSNCopyDataReceivedEventHandler<TMessage> handler)
        {
            this.owner = owner;
            this.channelName = channelName;
            this.handler = handler;
            this.state = state;
            addChannel();
        }

        /// <summary>
        /// Dispose the channel
        /// </summary>
        ~CopyDataChannel()
        {
            Dispose();
        }
    }
}
