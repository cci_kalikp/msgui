﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Impl.FusionMessagingImpl
{
    internal sealed class FSNAppID
    {
        public FSNAppID(string id_)
        {
            _id = id_;
        }

        public string Id
        {
            get { return _id; }
        }

        public override int GetHashCode()
        {
            return _id.GetHashCode();
        }

        public override bool Equals(object rhs_)
        {
            if (ReferenceEquals(rhs_, this)) { return true; }
            FSNAppID rhs = rhs_ as FSNAppID;
            return rhs != null && Id == rhs.Id;
        }

        public override string ToString()
        {
            return _id;
        }

        private readonly string _id;
    }
}
