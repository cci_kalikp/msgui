﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Impl.FusionMessagingImpl
{
    [Flags]
    internal enum FSNDesktopPeerStatus
    {
        None = 0,
        HostRouterDesktop = 1, // can route desktop traffic between hosts
        HostRouterPrivate = 2, // can route it's own desktop traffic between hosts
        Hidden = 4,       // application is hidden
    }

    internal sealed class FSNDesktopPeerInfo
    {
        #region Construction

        public FSNDesktopPeerInfo(FSNAppContext context_)
            : this(context_, FSNDesktopPeerStatus.None)
        {
        }

        public FSNDesktopPeerInfo(FSNAppContext context_, FSNDesktopPeerStatus capabilities_)
        {
            _context = context_;
            _status = capabilities_;
        }

        public FSNDesktopPeerInfo(FSNDesktopPeerInfo rhs_)
        {
            _context = rhs_._context;
            _status = rhs_._status;
        }

        #endregion

        #region Public API

        public FSNAppContext Context
        {
            get { return _context; }
        }

        public FSNDesktopPeerStatus Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("FSNDesktopPeerInfo {");
            sb.Append(_context).Append(',');
            sb.Append(_status);
            sb.Append("}");
            return sb.ToString();
        }

        #endregion

        #region Private Data

        private FSNDesktopPeerStatus _status;
        private readonly FSNAppContext _context;

        #endregion
    }
}
