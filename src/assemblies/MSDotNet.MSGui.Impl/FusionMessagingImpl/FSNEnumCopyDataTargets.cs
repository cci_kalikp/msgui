﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Impl.FusionMessagingImpl
{
    internal class FSNEnumCopyDataTargets
    {
        #region Delegates

        private delegate int EnumWindowsProc(IntPtr hwnd, int lParam);

        #endregion

        private IEnumerable<FSNWindow> Items
        {
            get
            {
                return this._items;
            }
        }

        public IEnumerable<FSNWindow> GetWindowsWithProp(string prop_)
        {
            IntPtr after = IntPtr.Zero;
            _prop = prop_;
            _items = new List<FSNWindow>();
            // iterate over all message windows
            while ((after = FSNWin32.User.FindWindowEx(FSNWin32.User.HWND_MESSAGE, after, null, IntPtr.Zero)) != IntPtr.Zero)
            {
                WindowPropEnum(after, 0);
            }
            FSNWin32.User.EnumWindows(new FSNWin32.User.EnumThreadProc(this.WindowPropEnum), 0);
            return this.Items;
        }

        private int WindowPropEnum(IntPtr hWnd, int lParam)
        {
            FSNWindow wnd = new FSNWindow(hWnd);
            if (wnd.HasProp(_prop))
            {
                _items.Add(wnd);
            }
            return 1;
        }

        #region Member Variables

        private IList<FSNWindow> _items = null;
        private string _prop = "";

        #endregion
    }
}
