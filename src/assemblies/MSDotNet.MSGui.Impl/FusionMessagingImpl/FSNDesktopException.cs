﻿using System;
using System.Runtime.Serialization;

namespace MorganStanley.MSDotNet.MSGui.Impl.FusionMessagingImpl
{
    [Serializable]
    internal class FSNDesktopException : FSNException
    {
        public FSNDesktopException() { }

        public FSNDesktopException(string msg_)
            : base(msg_)
        {
        }

        public FSNDesktopException(string msg_, Exception inner_)
            : base(msg_, inner_)
        {
        }

        protected FSNDesktopException(SerializationInfo info_, StreamingContext context_)
            : base(info_, context_)
        {
        }
    }
}
