﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Impl.FusionMessagingImpl
{
    internal interface IFSNStreamMarshaller<T>
    {
        /// <summary>
        /// Marsjal object 'object_' of type T to stream 'to_'
        /// </summary>
        /// <param name="object_">object to serialize</param>
        /// <param name="to_">stream to marshal to</param>
        /// <param name="state_">user context</param>
        void Marshal(T object_, Stream to_, object state_);

        /// <summary>
        /// Construct an object of type T from stream 'from_'
        /// </summary>
        /// <param name="from_">serialized representation of an object of type T</param>
        /// <returns>object of type T</returns>
        /// <param name="state_">user context</param>
        T Unmarshal(Stream from_, object state_);
    }
}
