﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;

namespace MorganStanley.MSDotNet.MSGui.Impl.FusionMessagingImpl
{
    internal sealed class ReceivedMessage<TPayload> : IReceivedMessage
    {
        public ReceivedMessage(ApplicationInfo appinfo_, TPayload payload_)
        {
            SenderApplicationInfo = appinfo_;
            Payload = payload_;
            ((IReceivedMessage)this).Payload = (object)payload_;
        }

        #region IReceivedMessage Members

        public ApplicationInfo SenderApplicationInfo { get; set; }

        object IReceivedMessage.Payload { get; set; }

        public TPayload Payload { get; set; }

        #endregion
    }
}
