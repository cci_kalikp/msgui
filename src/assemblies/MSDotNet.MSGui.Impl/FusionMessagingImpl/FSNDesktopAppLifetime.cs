﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Impl.FusionMessagingImpl
{
    internal delegate void OnAppExitedDelegate(object sender_, DataEventArgs<FSNAppContext> e_);

    internal class FSNDesktopAppLifetime : IDisposable
    {
        public static void Monitor(FSNAppContext context_, IntPtr hwnd_, OnAppExitedDelegate handler_)
        {
            FSNGuard.ArgumentNotNull(context_, "context_");
            FSNGuard.ArgumentNotNull(hwnd_, "hwnd_");
            FSNGuard.ArgumentNotNull(handler_, "handler_");
            // retrieve pid for handle 
            Monitor(context_, GetPid(hwnd_), handler_);
        }

        public static void Monitor(FSNAppContext context_, int pid_, OnAppExitedDelegate handler_)
        {
            FSNGuard.ArgumentNotNull(context_, "context_");
            FSNGuard.ArgumentNotNull(handler_, "handler_");
            // find process that owns window hwnd_
            Process process = Process.GetProcessById(pid_);
            // process could have exited
            if (process == null) { return; }
            // got process object monitor it
            Monitor(context_, process, handler_);
        }

        public static void Monitor(FSNAppContext context_, Process process_, OnAppExitedDelegate handler_)
        {
            FSNGuard.ArgumentNotNull(context_, "context_");
            FSNGuard.ArgumentNotNull(process_, "process_");
            FSNGuard.ArgumentNotNull(handler_, "handler_");
            lock (_sync)
            {
                // cleanup old process entry
                FSNDesktopAppLifetime lifetime;
                if (_procTable.TryGetValue(context_, out lifetime))
                {
                    lifetime.Dispose();
                    _procTable.Remove(context_);
                }
                // create new entry
                _procTable.Add(context_, new FSNDesktopAppLifetime(context_, process_, handler_));
            }
        }

        public static FSNDesktopAppLifetime FromContext(FSNAppContext context_)
        {
            FSNDesktopAppLifetime lifetime;
            if (_procTable.TryGetValue(context_, out lifetime))
            {
                return lifetime;
            }
            return null;
        }

        public FSNAppContext Context
        {
            get { return _context; }
        }

        public System.Diagnostics.Process Process
        {
            get { return _process; }
        }

        private FSNDesktopAppLifetime(FSNAppContext context_, Process process_, OnAppExitedDelegate handler_)
        {
            _context = context_;
            _process = process_;
            _handler = handler_;
            Process.EnableRaisingEvents = true;
            Process.Exited += new EventHandler(OnExited);
        }

        private void OnExited(object sender_, EventArgs e_)
        {
            _handler(this, new DataEventArgs<FSNAppContext>(Context));
            // Remove the dead entry
            _procTable.Remove(_context);
            Dispose();
        }

        private static int GetPid(IntPtr hwnd_)
        {
            int pid = -1;
            FSNWin32.User.GetWindowThreadProcessId(hwnd_, ref pid);
            return pid;
        }

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing_)
        {
            if (_disposed) { return; }
            if (Process != null)
            {
                Process.Exited -= new EventHandler(OnExited);
                Process.Dispose();
            }
            _disposed = true;
        }

        ~FSNDesktopAppLifetime()
        {
            Dispose(false);
        }

        #endregion

        private bool _disposed = false;

        private readonly FSNAppContext _context;
        private readonly Process _process;
        private readonly OnAppExitedDelegate _handler;

        private static readonly object _sync = new object();

        private static readonly IDictionary<FSNAppContext, FSNDesktopAppLifetime> _procTable = new Dictionary<FSNAppContext, FSNDesktopAppLifetime>();

    }
}
