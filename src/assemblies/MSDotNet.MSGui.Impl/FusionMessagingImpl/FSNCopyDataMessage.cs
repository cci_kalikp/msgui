﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Impl.FusionMessagingImpl
{
    internal class FSNCopyDataMessage
    {
        public FSNCopyDataMessage(string channel_, byte[] data_)
        {
            FSNGuard.ArgumentNotNull(channel_, "channel_");
            FSNGuard.ArgumentNotNull(data_, "data_");
            _channel = channel_;
            _data = data_;
        }

        public static bool IsCopyDataMsg(byte[] raw_)
        {
            if (raw_.Length >= RawSizeMin)
            {
                for (int i = 0; i < MSG_HEADER.Length; ++i)
                {
                    if (raw_[i] != MSG_HEADER[i]) { return false; }
                }
                return true;
            }
            return false;
        }

        public FSNCopyDataMessage(byte[] data_)
        {
            FSNGuard.ArgumentNotNull(data_, "data_");
            using (MemoryStream ms = new MemoryStream(data_))
            {
                BinaryReader br = new BinaryReader(ms);
                if (data_.Length < RawSizeMin)
                {
                    throw new FSNCopyDataException(
                      string.Format("Raw length {0} smaller than min message size {1}",
                                    data_.Length,
                                    RawSizeMin)
                      );
                }
                // read header
                byte[] header = br.ReadBytes(MSG_HEADER.Length);
                for (int i = 0; i < MSG_HEADER.Length; ++i)
                {
                    if (header[i] != MSG_HEADER[i])
                    { throw new FSNCopyDataException("Invalid message header"); }
                }
                // read channel            
                int cl = br.ReadInt32();
                _channel = Encoding.Unicode.GetString(br.ReadBytes(cl));
                // read data
                int dl = br.ReadInt32();
                _data = br.ReadBytes(dl);
            }
        }

        public string Channel
        {
            get { return _channel; }
        }

        public byte[] Data
        {
            get { return _data; }
        }

        public byte[] ToArray()
        {
            using (MemoryStream ms = new MemoryStream(RawSize))
            {
                BinaryWriter bw = new BinaryWriter(ms);
                // Encode msg header
                for (int i = 0; i < MSG_HEADER.Length; ++i)
                {
                    bw.Write(MSG_HEADER[i]);
                }
                // Encode channel
                byte[] cb = Encoding.Unicode.GetBytes(_channel);
                bw.Write(cb.Length);
                bw.Write(cb);
                // encode data
                bw.Write(_data.Length);
                bw.Write(_data);
                // return byte array
                return ms.ToArray();
            }
        }

        private static int RawSizeMin
        {
            get { return MSG_HEADER.Length + 4; }
        }

        private int RawSize
        {
            get
            {
                return
                  MSG_HEADER.Length +
                  4 + Encoding.Unicode.GetByteCount(_channel) +
                  4 + _data.Length;
            }
        }

        private readonly string _channel;
        private readonly byte[] _data;
        private static readonly byte[] MSG_HEADER = new byte[] { 0xb, 0xa, 0xb, 0xe };
    }
}
