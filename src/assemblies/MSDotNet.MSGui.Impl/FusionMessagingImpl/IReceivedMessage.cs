﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;

namespace MorganStanley.MSDotNet.MSGui.Impl.FusionMessagingImpl
{
    internal interface IReceivedMessage
    {
        /// <summary>
        /// Gets or sets the sender application information.
        /// </summary>
        ApplicationInfo SenderApplicationInfo
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the payload object.
        /// </summary>
        object Payload
        {
            get;
            set;
        }
    }
}
