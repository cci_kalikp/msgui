﻿using System.IO;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Impl.FusionMessagingImpl
{
    internal static class FSNMarshal
    {
        #region Utility Methods

        public static int WriteInt(int int_, BinaryWriter to_)
        {
            to_.Write(int_);
            return sizeof(int);
        }

        public static int ReadInt(BinaryReader from_)
        {
            return from_.ReadInt32();
        }

        public static int WriteString(string str_, BinaryWriter to_)
        {
            byte[] raw = Encoding.Unicode.GetBytes(str_);
            to_.Write(raw.Length);
            to_.Write(raw);
            return sizeof(int) + raw.Length;
        }

        public static string ReadString(BinaryReader from_)
        {
            int len = from_.ReadInt32();
            byte[] raw = from_.ReadBytes(len);
            return Encoding.Unicode.GetString(raw);
        }

        public static void WriteAppContext(FSNAppContext ctx_, BinaryWriter to_)
        {
            if (ctx_ == FSNAppContext.All)
            {
                WriteString("", to_);
                return;
            }
            WriteString(ctx_.Host, to_);
            WriteString(ctx_.User, to_);
            WriteString(ctx_.Desktop.Name, to_);
            WriteString(ctx_.AppID.ToString(), to_);
            WriteString(ctx_.Instance, to_);
        }

        public static FSNAppContext ReadAppContext(BinaryReader from_)
        {
            string str = ReadString(from_);
            if (string.Empty.Equals(str))
            {
                return FSNAppContext.All;
            }
            return new FSNAppContext(
              str,
              ReadString(from_),
              new FSNDesktopID(ReadString(from_)),
              new FSNAppID(ReadString(from_)),
              ReadString(from_)
              );
        }

        #endregion
    }
}
