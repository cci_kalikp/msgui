﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Impl.FusionMessagingImpl
{
    internal class FSNWindow
    {
        public FSNWindow(IntPtr hwnd_)
        {
            _hwnd = hwnd_;
        }

        #region IWindow Members

        public int ProcessID
        {
            get
            {
                int ret = 0;
                int threadid = FSNWin32.User.GetWindowThreadProcessId(_hwnd, ref ret);
                return ret;
            }
        }

        public void Raise()
        {
            Activate();
        }

        public void Hide()
        {
            ShowWindowAsync(Hwnd, FSNWin32.User.SW_HIDE);
        }

        public bool Identify()
        {
            return Identify(false);
        }

        public bool Identify(bool stop_)
        {
            FSNWin32.FLASHWINFO fInfo = new FSNWin32.FLASHWINFO();
            fInfo.cbSize = Convert.ToUInt32(Marshal.SizeOf(typeof(FSNWin32.FLASHWINFO)));
            fInfo.hwnd = Hwnd;
            if (stop_)
            {
                fInfo.dwFlags = FSNWin32.User.FLASHW_STOP;
            }
            else
            {
                fInfo.dwFlags = FSNWin32.User.FLASHW_TIMERNOFG | FSNWin32.User.FLASHW_ALL;
            }
            fInfo.uCount = 0;
            fInfo.dwTimeout = 0;
            return (FSNWin32.User.FlashWindowEx(ref fInfo) == 0);
        }

        public void ShowWindowAsync(IntPtr hwnd_, int show_)
        {
            FSNWin32.User.ShowWindowAsync(hwnd_, show_);
        }

        /// <summary>
        /// Close the window
        /// </summary>
        public void Close()
        {
            FSNWin32.User.PostMessage(_hwnd, (int)FSNWin32.Msgs.WM_CLOSE, 0, 0);
        }

        /// <summary>
        /// Gets/Sets whether the window is iconic (mimimised) or not.
        /// </summary>
        public bool Iconic
        {
            get
            {
                return ((FSNWin32.User.IsIconic(_hwnd) == 0) ? false : true);
            }
            set
            {
                FSNWin32.User.SendMessage(
                  _hwnd,
                  (int)FSNWin32.Msgs.WM_SYSCOMMAND,
                  (IntPtr)FSNWin32.User.SW_MINIMIZE,
                  IntPtr.Zero);
            }
        }

        /// <summary>
        /// Brings the window to the foreground
        /// </summary>
        /// 
        public void BringToFront()
        {
            Activate();
        }

        public void Activate()
        {
            if (!Visible) { ShowWindowAsync(Hwnd, FSNWin32.User.SW_SHOWNORMAL); }
            IntPtr fwnd = FSNWin32.User.GetForegroundWindow();
            if (_hwnd == fwnd) { return; }

            IntPtr t1 = FSNWin32.User.GetWindowThreadProcessId(fwnd, IntPtr.Zero);
            IntPtr t2 = FSNWin32.User.GetWindowThreadProcessId(_hwnd, IntPtr.Zero);

            if (t1 != t2)
            {
                int i = (int)FSNWin32.User.AttachThreadInput(t1, t2, 1);
                FSNWin32.User.SetForegroundWindow(_hwnd);
                int j = (int)FSNWin32.User.AttachThreadInput(t1, t2, 0);
            }
            DoBringToFront();
        }


        /// <summary>
        /// Gets/Sets the caption of the window
        /// </summary>
        public string Text
        {
            get
            {
                StringBuilder sb = new StringBuilder(255);
                int len = FSNWin32.User.GetWindowText(_hwnd, sb, 255);
                return sb.ToString().Substring(0, len);
            }
            set { FSNWin32.User.SetWindowText(_hwnd, value); }
        }

        /// <summary>
        /// Gets the visibility of the window
        /// </summary>
        public bool Visible
        {
            get { return FSNWin32.User.IsWindowVisible(_hwnd) != 0; }
        }

        //// What is this?
        //private Rectangle RestorePlacement
        //{
        //  get 
        //  {
        //    FSNWin32.WINDOWPLACEMENT p = Placement();
        //    FSNWin32.RECT r = p.rcNormalPosition;
        //    return new Rectangle(r.Left, r.Top, r.Right-r.Left, r.Bottom - r.Top);
        //  }
        //}

        ///// <summary>
        ///// Gets the window state of the window
        ///// </summary>
        //public FormWindowState WindowState
        //{
        //  get 
        //  {
        //    switch (Placement().showCmd)
        //    {
        //      case FSNWin32.User.SW_SHOWMAXIMIZED: return FormWindowState.Maximized;
        //      case FSNWin32.User.SW_SHOWMINIMIZED: return FormWindowState.Minimized;
        //      default:
        //        return FormWindowState.Normal;
        //    }
        //  }
        //}

        ///// <summary>
        ///// Gets/Sets the location of the window
        ///// </summary>
        public Point Location
        {
            get
            {
                FSNWin32.RECT rect = GetWindowPos();
                return new Point(rect.Left, rect.Top);
            }
            set
            {
                FSNWin32.RECT curr = this.GetWindowPos();
                FSNWin32.User.SetWindowPos(_hwnd, FSNWin32.User.HWND_TOP, value.X, value.Y, curr.Right - curr.Left, curr.Bottom - curr.Top, 0);
            }
        }

        private void DoBringToFront()
        {
            if (Iconic)
            {
                FSNWin32.User.SendMessage(
                  _hwnd,
                  (int)FSNWin32.Msgs.WM_SYSCOMMAND,
                  (IntPtr)FSNWin32.User.SC_RESTORE,
                  IntPtr.Zero);
            }
            FSNWin32.User.BringWindowToTop(_hwnd);
            FSNWin32.User.SetForegroundWindow(_hwnd);
        }


        private FSNWin32.RECT GetWindowPos()
        {
            FSNWin32.RECT rect = new FSNWin32.RECT();
            FSNWin32.User.GetWindowRect(_hwnd, ref rect);
            return rect;
        }

        private FSNWin32.WINDOWPLACEMENT Placement()
        {
            FSNWin32.WINDOWPLACEMENT placement = new FSNWin32.WINDOWPLACEMENT();
            placement.Length = System.Runtime.InteropServices.Marshal.SizeOf(placement);
            FSNWin32.User.GetWindowPlacement(_hwnd, ref placement);
            return placement;
        }

        public int SetProp(string prop)
        {
            return FSNWin32.User.SetProp(_hwnd, prop, (int)_hwnd);
        }

        public bool HasProp(string prop)
        {
            return FSNWin32.User.GetProp(_hwnd, prop) != 0;
        }

        public int RemoveProp(string prop)
        {
            return FSNWin32.User.RemoveProp(_hwnd, prop);
        }

        public bool SendCopyData(IntPtr shell_, byte[] data_)
        {
            return SendCopyData(shell_, data_, 0);
        }

        public bool SendCopyData(IntPtr shell_, byte[] data_, uint timeoutMS_)
        {
            int before = Marshal.GetLastWin32Error();
            before = Marshal.GetLastWin32Error();
            GCHandle gcData = GCHandle.Alloc(data_, GCHandleType.Pinned);
            try
            {
                FSNWin32.COPYDATASTRUCT cds;
                cds.cbData = data_.Length;
                cds.dwData = shell_;
                cds.lpData = gcData.AddrOfPinnedObject();
                IntPtr result;
                int ret;
                if (timeoutMS_ != 0)
                {
                    ret = FSNWin32.User.SendMessageTimeout(
                      _hwnd,
                      (int)FSNWin32.Msgs.WM_COPYDATA,
                      shell_,
                      ref cds,
                      FSNWin32.User.SendMessageTimeoutFlags.SMTO_NOTIMEOUTIFNOTHUNG | FSNWin32.User.SendMessageTimeoutFlags.SMTO_ABORTIFHUNG,
                      timeoutMS_,
                      out result
                      );
                }
                else
                {
                    ret = FSNWin32.User.SendMessage(
                      _hwnd,
                      (int)FSNWin32.Msgs.WM_COPYDATA,
                      shell_,
                      ref cds
                      );
                }
                return (ret != 0);
            }
            finally
            {
                gcData.Free();
            }
        }

        /// <summary>
        /// The handle to the window
        /// </summary>
        public IntPtr Hwnd
        {
            get { return _hwnd; }
        }

        #endregion

        private IntPtr _hwnd;
    }
}
