﻿using System;

namespace MorganStanley.MSDotNet.MSGui.Impl.FusionMessagingImpl
{
    internal class FSNCopyDataReceivedEventArgs<TMessage> : EventArgs
    {
        public FSNCopyDataReceivedEventArgs(string channel_, TMessage message_, object state_)
        {
            _channel = channel_;
            _message = message_;
            _state = state_;
        }

        public string Channel
        {
            get { return _channel; }
        }

        public TMessage Message
        {
            get { return _message; }
        }

        public object State
        {
            get { return _state; }
        }

        private readonly string _channel;
        private readonly TMessage _message;
        private readonly object _state;
    }
}
