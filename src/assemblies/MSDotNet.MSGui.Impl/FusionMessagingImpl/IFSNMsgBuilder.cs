﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Impl.FusionMessagingImpl
{
    internal interface IFSNMsgBuilder<TMessage>
    {
        /// <summary>
        /// Source handle, IntPtr.Zero for non local apps
        /// </summary>
        IntPtr Src { get; }

        /// <summary>
        /// Channel message was transmitted on
        /// </summary>
        string Channel { get; }

        /// <summary>
        /// Unmarshalled message
        /// </summary>
        /// <param name="state_">channel marshall state</param>
        /// <returns>unmarshalled message</returns>
        TMessage Build(object state_);
    }
}
