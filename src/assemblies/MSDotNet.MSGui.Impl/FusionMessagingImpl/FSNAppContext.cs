﻿using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Impl.FusionMessagingImpl
{
    internal sealed class FSNAppContext
    {
        private static volatile FSNAppContext _localContext;
        private readonly string _host;
        private readonly string _user;
        private readonly FSNDesktopID _desktop;
        private readonly FSNAppID _appId;
        private readonly string _instance;

        #region Construction

        public FSNAppContext(string host, string user, FSNDesktopID desktop, FSNAppID appId, string instance)
        {
            FSNGuard.ArgumentNotNullOrEmptyString(host, "host");
            FSNGuard.ArgumentNotNullOrEmptyString(user, "user");
            FSNGuard.ArgumentNotNull(desktop, "desktop");
            FSNGuard.ArgumentNotNull(appId, "appId");
            FSNGuard.ArgumentNotNull(instance, "instance");
            _host = host;
            _user = user;
            _desktop = desktop;
            _appId = appId;
            _instance = instance;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Fusion identifier for this application
        /// </summary>
        public static FSNAppContext LocalApp
        {
            get { return _localContext; }
            set { _localContext = value; }
        }

        public static FSNAppContext All
        {
            get { return null; }
        }

        public string Host
        {
            get { return _host; }
        }

        public string User
        {
            get { return _user; }
        }

        public FSNDesktopID Desktop
        {
            get { return _desktop; }
        }

        public FSNAppID AppID
        {
            get { return _appId; }
        }

        public string Instance
        {
            get { return _instance; }
        }

        #endregion

        #region Public API

        public override int GetHashCode()
        {
            return (_host + _user + _desktop.ToString() + _appId.ToString()).GetHashCode();
        }

        public override bool Equals(object rhs_)
        {
            if (rhs_ == null)
            {
                return false;
            }
            else if (rhs_ == this)
            {
                return true;
            }
            else if (rhs_.GetType() == typeof(FSNAppContext))
            {
                FSNAppContext rhs = rhs_ as FSNAppContext;
                return
                  Host.Equals(rhs.Host) &&
                  User.Equals(rhs.User) &&
                  Desktop.Equals(rhs.Desktop) &&
                  AppID.Equals(rhs.AppID) &&
                  Instance.Equals(rhs.Instance);
            }
            else
            {
                return false;
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("/");
            sb.Append(_host).Append('/');
            sb.Append(_user).Append('/');
            sb.Append(_desktop).Append('/');
            sb.Append(_appId).Append('/');
            sb.Append(_instance);
            return sb.ToString();
        }

        #endregion
    }
}
