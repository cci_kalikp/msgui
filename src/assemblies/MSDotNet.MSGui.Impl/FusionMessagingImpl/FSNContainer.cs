﻿using System.Collections.Generic;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Impl.FusionMessagingImpl
{
    internal static class FSNContainer
    {
        public static bool Equals<T>(IEnumerable<T> lhs_, IEnumerable<T> rhs_)
        {
            IEnumerator<T> li = lhs_.GetEnumerator();
            IEnumerator<T> ri = rhs_.GetEnumerator();
            try
            {
                while (true)
                {
                    bool lir = li.MoveNext();
                    bool rir = ri.MoveNext();
                    if (!lir || !rir)
                    {
                        return lir == rir;
                    }
                    if (!li.Current.Equals(ri.Current))
                    {
                        return false;
                    }
                }
            }
            finally
            {
                if (li != null) { li.Dispose(); }
                if (ri != null) { ri.Dispose(); }
            }
        }

        public static string ToString<T>(IEnumerable<T> container_)
        {
            StringBuilder sb = new StringBuilder();
            foreach (T t in container_)
            {
                sb.Append(t).Append(' ');
            }
            return sb.ToString();
        }
    }
}
