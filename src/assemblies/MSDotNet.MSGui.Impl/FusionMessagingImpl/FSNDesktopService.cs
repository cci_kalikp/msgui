﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.Composite.Events;

namespace MorganStanley.MSDotNet.MSGui.Impl.FusionMessagingImpl
{
    internal sealed class FSNDesktopService
    {
        #region Events

        /// <summary>
        /// This event is raised when 'this' fusion shell joins the desktop
        /// </summary>
        //NOTE [EventPublication(FDSEventTopicNames.DesktopJoin, PublicationScope.Descendants)]
        public event EventHandler OnDesktopJoin;

        /// <summary>
        /// This event is raised when an application joins this desktop
        /// </summary>
        //NOTE [EventPublication(FDSEventTopicNames.DesktopPeerJoin, PublicationScope.Descendants)]
        public event EventHandler<DataEventArgs<FSNAppContext>> OnDesktopPeerJoin;

        /// <summary>
        /// This event is raised when a peer updates it's status mask.
        /// </summary>
        //NOTE [EventPublication(FDSEventTopicNames.DesktopPeerStatus, PublicationScope.Descendants)]
        public event EventHandler<DataEventArgs<FSNAppContext>> OnDesktopPeerStatus;

        /// <summary>
        /// This event is raised when 'this' fusion shell leaves the desktop
        /// </summary>
        //NOTE [EventPublication(FDSEventTopicNames.DesktopLeave, PublicationScope.Descendants)]
        public event EventHandler OnDesktopLeave;

        /// <summary>
        /// This event is raised when an application leaves this desktop
        /// </summary>
        //NOTE [EventPublication(FDSEventTopicNames.DesktopPeerLeave, PublicationScope.Descendants)]
        public event EventHandler<DataEventArgs<FSNAppContext>> OnDesktopPeerLeave;

        #endregion

        #region Dependency Injection

        //NOTE [ServiceDependency]
        public IFSNLog Log
        {
            get { return _log; }
            set { _log = value; }
        }

        #endregion

        #region Construction

        //NOTE [InjectionConstructor]
        public FSNDesktopService
          (
            //NOTE [ServiceDependency]WorkItem workItem_,
            /*NOTE [ServiceDependency]*/FSNDesktopMsgService msgService_
            //NOTE [ServiceDependency]FSNDesktopWindowManager windowManager_,
            //NOTE [ServiceDependency]FSNDesktopLauncher launcher_
          )
            : this(/*workItem_, */FSNAppContext.LocalApp, msgService_/*, windowManager_, launcher_*/)
        {
        }

        public FSNDesktopService
          (
            //WorkItem workItem_,
          FSNAppContext localContext_,
          FSNDesktopMsgService msgService_
            //FSNDesktopWindowManager windowManager_,
            //FSNDesktopLauncher launcher_
          )
        {
            //FSNGuard.ArgumentNotNull(workItem_, "workItem_");
            FSNGuard.ArgumentNotNull(localContext_, "localContext_");
            FSNGuard.ArgumentNotNull(msgService_, "msgService");
            //FSNGuard.ArgumentNotNull(windowManager_, "windowManager_");
            //FSNGuard.ArgumentNotNull(launcher_, "launcher_");
            //_workItem         = workItem_;
            _localContext = localContext_;
            _msgService = msgService_;
            //_windowManager    = windowManager_;
            //_launcher         = launcher_;
            _appIdChannel = FSNDesktopMsgService.MakeAppContextChannel(localContext_);
            _desktopIdChannel = FSNDesktopMsgService.MakeDesktopChannelID(localContext_);
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Local application identifier
        /// </summary>
        public FSNAppContext LocalContext
        {
            get { return _localContext; }
        }

        /// <summary>
        /// Name of this desktop
        /// </summary>
        public FSNDesktopID Name
        {
            get { return LocalContext.Desktop; }
        }

        #endregion

        #region Convenience Methods

        /// <summary>
        /// Obtain reference to desktop messaging service
        /// </summary>
        public FSNDesktopMsgService MsgService
        {
            get { return _msgService; }
        }

        ///// <summary>
        ///// Obtain reference to desktop application launcher service
        ///// </summary>
        //public FSNDesktopLauncher Launcher
        //{
        //    get { return _launcher; }
        //}

        ///// <summary>
        ///// Obtain reference to desktop window manager service
        ///// </summary>
        //public FSNDesktopWindowManager WindowManager
        //{
        //    get { return _windowManager; }
        //}

        #endregion

        #region Desktop Registration

        /// <summary>
        /// True if we're joined to the desktop
        /// </summary>
        public bool Joined
        {
            get { return _joined; }
        }

        /// <summary>
        /// Check if specified app is joined to this desktop
        /// </summary>
        /// <param name="id_">Desktop Application Idetifier</param>
        /// <returns>true if peer is joined to desktop</returns>
        public bool IsJoined(FSNAppContext id_)
        {
            FSNGuard.ArgumentNotNull(id_, "id_");
            lock (_dsync)
            {
                return _peers.ContainsKey(id_);
            }
        }


        /// <summary>
        /// Return status mask for this peer.
        /// </summary>
        /// <returns>Current status for this peer</returns>
        public FSNDesktopPeerStatus GetStatus()
        {
            return _status;
        }

        /// <summary>
        /// Add flags to status mask.
        /// </summary>
        /// <param name="caps_">status to merge with current mask.</param>
        public void MergeStatus(FSNDesktopPeerStatus caps_)
        {
            lock (_dsync)
            {
                FSNDesktopPeerStatus oc = _status;
                _status |= caps_;
                if (_log != null) { _log.Debug(string.Format("Updating status from {0:x} -> {1:x}", oc, _status)); }
                if (oc == _status) { return; }
                if (_joined)
                {
                    _msgService.Publish(
                      CHANNEL_FUSION_DESKTOP_STATUS,
                      AddStatus(_status, new FSNDesktopMsgData()),
                      0
                      );
                }
            }
        }

        /// <summary>
        /// Clear flags from status mask.
        /// </summary>
        /// <param name="caps_">status to merge with current mask.</param>
        public void ClearStatus(FSNDesktopPeerStatus caps_)
        {
            lock (_dsync)
            {
                FSNDesktopPeerStatus oc = _status;
                _status &= ~caps_;
                if (_log != null) { _log.Debug(string.Format("Updating status from {0:x} -> {1:x}", oc, _status)); }
                if (oc == _status) { return; }
                if (_joined)
                {
                    _msgService.Publish(
                      CHANNEL_FUSION_DESKTOP_STATUS,
                      AddStatus(_status, new FSNDesktopMsgData()),
                      0
                      );
                }
            }
        }

        /// <summary>
        /// Retrieve set of currently joined peers
        /// </summary>
        public IEnumerable<FSNAppContext> Peers
        {
            get
            {
                lock (_dsync)
                {
                    return new List<FSNAppContext>(_peers.Keys);
                }
            }
        }

        /// <summary>
        /// Retrieve copy of desktop peer info object.
        /// </summary>
        /// <param name="context_"></param>
        /// <returns>Copy of peer info obejct or null if peer isn't joined to the desktop</returns>
        public FSNDesktopPeerInfo GetPeerInfo(FSNAppContext context_)
        {
            lock (_dsync)
            {
                FSNDesktopPeerInfo info = null;
                return _peers.TryGetValue(context_, out info) ? new FSNDesktopPeerInfo(info) : null;
            }
        }

        /// <summary>
        /// Call this method to join desktop.       
        /// </summary>
        public void Join()
        {
            if (Joined) { return; }
            // flag as belonging to this desktop
            _msgService.Subscribe(_desktopIdChannel);
            // flag app channel
            _msgService.Subscribe(_appIdChannel);
            // add announce channel
            _msgService.Subscribe(CHANNEL_FUSION_DESKTOP_JOIN, OnJoin);
            // add hello channel
            _msgService.Subscribe(CHANNEL_FUSION_DESKTOP_HELLO, OnHello);
            // add status channel
            _msgService.Subscribe(CHANNEL_FUSION_DESKTOP_STATUS, OnStatus);
            // add leave channel
            _msgService.Subscribe(CHANNEL_FUSION_DESKTOP_LEAVE, OnLeave);
            // add rebind channel
            _msgService.Subscribe(CHANNEL_FUSION_DESKTOP_REBIND, OnRebind);
            // add remote peer reset channel
            _msgService.Subscribe(CHANNEL_FUSION_DESKTOP_REMOTE_RESET, OnRemoteReset);
            // add echo service
            _msgService.Subscribe(FDSIPCTopicNames.DESKTOP_ECHO_REQUEST, OnEcho);
            // send desktop join message
            SendJoin();
            // remember that we're joined
            _joined = true;
            // notify event subsribers of join
            DispatchOnDesktopJoin();
        }

        /// <summary>
        /// Call this method to leave desktop.
        /// </summary>
        public void Leave()
        {
            if (!Joined) { return; }
            // remove flag as belonging to this desktop
            _msgService.Unsubscribe(_desktopIdChannel);
            // remove flag app channel
            _msgService.Unsubscribe(_appIdChannel);
            // remove announce channel
            _msgService.Unsubscribe(CHANNEL_FUSION_DESKTOP_JOIN);
            // remove hello channel
            _msgService.Unsubscribe(CHANNEL_FUSION_DESKTOP_HELLO);
            // remove status channel
            _msgService.Unsubscribe(CHANNEL_FUSION_DESKTOP_STATUS);
            // remove leave channel
            _msgService.Unsubscribe(CHANNEL_FUSION_DESKTOP_LEAVE);
            // remove rebind channel
            _msgService.Unsubscribe(CHANNEL_FUSION_DESKTOP_REBIND);
            // remove remote peer reset channel
            _msgService.Unsubscribe(CHANNEL_FUSION_DESKTOP_REMOTE_RESET);
            // remove echo service
            _msgService.Unsubscribe(FDSIPCTopicNames.DESKTOP_ECHO_REQUEST);
            // send desktop leave message
            _msgService.Publish(CHANNEL_FUSION_DESKTOP_LEAVE, 0);
            // remember that we've left
            _joined = false;
            // notify event subsribers of leave
            DispatchOnDesktopLeave();
        }

        /// <summary>
        /// Offline all remote peers
        /// </summary>
        public void RemoteReset()
        {
            // publish reset event to global desktop 
            _msgService.Publish(FSNDesktopService.CHANNEL_FUSION_DESKTOP_REMOTE_RESET, 0);
        }

        /// <summary>
        /// Cause entire desktop to rebind
        /// </summary>
        public void Rebind()
        {
            // request desktop rebind
            _msgService.Publish(FSNDesktopService.CHANNEL_FUSION_DESKTOP_REBIND, 0);
        }

        #endregion

        #region Utility Methods
        /// <summary>
        /// Create desktop uid
        /// </summary>
        /// <returns>desktop unique string</returns>
        public string MakeUniqueName()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(LocalContext.Desktop.Name);
            sb.Append(LocalContext);
            return FSNName.MakeUniqueName(sb.ToString());
        }

        #endregion

        #region Diagnostics

        //NOTE [FSNDiagnostics("Fusion Desktop")]
        //public void Diagnose(FSNDiagnosticsContext context_)
        //{
        //  FSNGuard.ArgumentNotNull(context_, "context_");
        //    StringBuilder body = new StringBuilder();
        //    body.Append("Persistence base directory : ").Append(FSNDesktopPath.PersistenceBaseDir).Append(Environment.NewLine);
        //    body.Append("Logging base directory : ").Append(FSNDesktopPath.LoggingBaseDir).Append(Environment.NewLine);
        //    body.Append("Temp base directory : ").Append(FSNDesktopPath.TempBaseDir).Append(Environment.NewLine);           
        //    body.Append("Peers : ").Append(Environment.NewLine);
        //    body.Append(Environment.NewLine);
        //    foreach (FSNAppContext peer in this.Peers)
        //    {
        //        body.Append(peer).Append(Environment.NewLine);
        //    }
        //    body.Append(Environment.NewLine);
        //    context_.Diagnostics = body.ToString();
        //}

        #endregion

        #region Private Implementation

        private void SendJoin()
        {
            if (_log != null)
            {
                _log.Debug("Sending join.");
            }
            FSNDesktopMsgData data = new FSNDesktopMsgData();
            AddStatus(_status, data);
            _msgService.Publish(CHANNEL_FUSION_DESKTOP_JOIN, data, 0);
        }

        // callback on receipt of desktop join message
        private void OnJoin(object sender_, FSNDesktopMsgEventArgs e_)
        {
            // suppress announcements from us
            if (e_.Source.Equals(LocalContext)) { return; }
            // send hello message to newly joined app
            _msgService.Send(
              e_.Source, CHANNEL_FUSION_DESKTOP_HELLO,
              AddStatus(_status, new FSNDesktopMsgData()),
              0
              );
            // bind to app
            DesktopJoin(e_.Source, GetStatus(e_.Data));
        }

        // callback on receipt of hello message
        private void OnHello(object sender_, FSNDesktopMsgEventArgs e_)
        {
            // supress hello from us (should never happen)
            if (e_.Source.Equals(LocalContext)) { return; }
            // bind to app
            DesktopJoin(e_.Source, GetStatus(e_.Data));
        }

        // callback on receipt of peers status message
        private void OnStatus(object sender_, FSNDesktopMsgEventArgs e_)
        {
            if (e_.Source.Equals(LocalContext)) { return; }
            // update status 
            UpdateStatus(e_.Source, GetStatus(e_.Data));
        }

        // callback on receipt of rebind message
        private void OnRebind(object sender_, FSNDesktopMsgEventArgs e_)
        {
            // republish join message to desktop peers
            _msgService.Publish(CHANNEL_FUSION_DESKTOP_JOIN, 0);
        }

        // callback on receipt of leave message
        private void OnLeave(object sender_, FSNDesktopMsgEventArgs e_)
        {
            DesktopLeave(e_.Source);
        }

        // callback on process exit
        private void OnAppExited(object sender_, DataEventArgs<FSNAppContext> e_)
        {
            DesktopLeave(e_.Data);
        }

        // callback on receipt of remote reset event
        private void OnRemoteReset(object sender_, FSNDesktopMsgEventArgs e_)
        {
            // build list of currently known remote peers
            IList<FSNAppContext> rpl = new List<FSNAppContext>();
            lock (_dsync)
            {
                foreach (FSNAppContext ctx in _peers.Keys)
                {
                    if (!(FSNAppContext.LocalApp.Host.Equals(ctx.Host)))
                    {
                        rpl.Add(ctx);
                    }
                }
            }
            // offline all remote peers
            foreach (FSNAppContext rp in rpl)
            {
                DesktopLeave(rp);
            }
        }

        // peer echo request
        private void OnEcho(object sender_, FSNDesktopMsgEventArgs e_)
        {
            // right back atcha
            _msgService.Send(e_.Source, FDSIPCTopicNames.DESKTOP_ECHO_RESPONSE, e_.Data);
        }

        // monitor lifetime of newly joined app & inform client code of join
        private void DesktopJoin(FSNAppContext context_, FSNDesktopPeerStatus caps_)
        {
            if (context_.Host.Equals(FSNAppContext.LocalApp.Host))
            {
                // locate app window
                FSNCopyDataTarget<FSNDesktopMsg> target = _msgService.GetMsgTarget(context_);
                // monitor app lifetime for local peers
                if (context_.Host.Equals(FSNAppContext.LocalApp.Host))
                {
                    if (target != null)
                    {
                        FSNDesktopAppLifetime.Monitor(context_, target.Hwnd, new OnAppExitedDelegate(OnAppExited));              
                    }
                }
            }
            lock (_dsync)
            {
                // add peer info
                FSNDesktopPeerInfo info = null;
                _peers.TryGetValue(context_, out info);
                if (info != null)
                {
                    IDisposable disp = ((object)info) as IDisposable;
                    if (disp != null) { disp.Dispose(); }
                    _peers.Remove(context_);
                }
                _peers.Add(context_, new FSNDesktopPeerInfo(context_, caps_));
            }
            // Dispatch desktop join
            DispatchOnDesktopPeerJoin(context_);
        }

        private void UpdateStatus(FSNAppContext context_, FSNDesktopPeerStatus caps_)
        {
            lock (_dsync)
            {
                FSNDesktopPeerInfo peer;
                if (_peers.TryGetValue(context_, out peer))
                {
                    peer.Status = caps_;
                }
            }
            DispatchOnDesktopPeerStatus(context_);
        }

        private void DesktopLeave(FSNAppContext context_)
        {
            lock (_dsync)
            {
                _peers.Remove(context_);
            }
            DispatchOnDesktopPeerLeave(context_);
        }

        private void DispatchOnDesktopJoin()
        {
            EventHandler temp = OnDesktopJoin;
            if (temp != null)
            {
                temp(this, EventArgs.Empty);
            }
        }

        private void DispatchOnDesktopPeerJoin(FSNAppContext context_)
        {
            EventHandler<DataEventArgs<FSNAppContext>> temp = OnDesktopPeerJoin;
            if (temp != null)
            {
                temp(this, new DataEventArgs<FSNAppContext>(context_));
            }
        }

        private void DispatchOnDesktopLeave()
        {
            EventHandler temp = OnDesktopLeave;
            if (temp != null)
            {
                temp(this, EventArgs.Empty);
            }
        }

        private void DispatchOnDesktopPeerLeave(FSNAppContext context_)
        {
            EventHandler<DataEventArgs<FSNAppContext>> temp = OnDesktopPeerLeave;
            if (temp != null)
            {
                temp(this, new DataEventArgs<FSNAppContext>(context_));
            }
        }

        private void DispatchOnDesktopPeerStatus(FSNAppContext context_)
        {
            EventHandler<DataEventArgs<FSNAppContext>> temp = OnDesktopPeerStatus;
            if (temp != null)
            {
                temp(this, new DataEventArgs<FSNAppContext>(context_));
            }
        }

        #endregion

        #region Utility Methods

        private FSNDesktopMsgData AddStatus(FSNDesktopPeerStatus caps_, FSNDesktopMsgData data_)
        {
            data_["caps"] = "" + (int)caps_;
            return data_;
        }

        private FSNDesktopPeerStatus GetStatus(FSNDesktopMsgData data_)
        {
            FSNDesktopPeerStatus ret = FSNDesktopPeerStatus.None;
            int capi = 0;
            if (!data_.GetInteger("caps", out capi)) { return ret; }
            // add caps we understand
            foreach (FSNDesktopPeerStatus cap in Enum.GetValues(typeof(FSNDesktopPeerStatus)))
            {
                if ((capi & (int)cap) != 0) { ret |= cap; }
            }
            return ret;
        }

        private FSNDesktopMsgData AddHandle(IntPtr hwnd_, FSNDesktopMsgData data_)
        {
            data_["handle"] = "" + (int)hwnd_;
            return data_;
        }

        private IntPtr GetHandle(FSNDesktopMsgData data_)
        {
            int hwndi = 0;
            if (!data_.GetInteger("handle", out hwndi)) { return IntPtr.Zero; }
            return new IntPtr(hwndi);
        }

        #endregion

        #region Private Data Members

        private IFSNLog _log;

        private volatile bool _joined = false;

        private volatile FSNDesktopPeerStatus _status = FSNDesktopPeerStatus.None;

        // app publishes message to announce desktop join
        internal const string CHANNEL_FUSION_DESKTOP_JOIN = "ipc://Fusion/Desktop/Join";
        // published by desktop apps in response to receipt of CHANNEL_FUSION_DESKTOP_JOIN
        internal const string CHANNEL_FUSION_DESKTOP_HELLO = "ipc://Fusion/Desktop/Hello";
        // published when peer updates status mask
        internal const string CHANNEL_FUSION_DESKTOP_STATUS = "ipc://Fusion/Desktop/Status";
        // app publishes message to announce desktop leave
        internal const string CHANNEL_FUSION_DESKTOP_LEAVE = "ipc://Fusion/Desktop/Leave";
        // published by warp proxy node to encourage a desktop rebind
        internal const string CHANNEL_FUSION_DESKTOP_REBIND = "ipc://Fusion/Desktop/Rebind";
        // published by warp proxy node to instruct local desktop peers to offline all remote apps
        internal const string CHANNEL_FUSION_DESKTOP_REMOTE_RESET = "ipc://Fusion/Desktop/RemoteReset";

        // map of peer info structs
        private IDictionary<FSNAppContext, FSNDesktopPeerInfo> _peers = new Dictionary<FSNAppContext, FSNDesktopPeerInfo>();
        // lock for peer data structure
        private readonly object _dsync = new object();

        //private readonly WorkItem _workItem;
        private readonly FSNDesktopMsgService _msgService;
        //private readonly FSNDesktopWindowManager _windowManager;
        //private readonly FSNDesktopLauncher _launcher;   
        private readonly FSNAppContext _localContext;

        private readonly string _appIdChannel;
        private readonly string _desktopIdChannel;

        #endregion
    }
}
