﻿using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Impl.FusionMessagingImpl
{
    public class FusionMessageData : Dictionary<string, string>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FusionMessageData"/> class.
        /// </summary>
        public FusionMessageData()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FusionMessageData"/> class.
        /// </summary>
        /// <param name="dictionary_">The dictionary.</param>
        public FusionMessageData(IDictionary<string, string> dictionary_)
            : base(dictionary_)
        {
        }

        /// <summary>
        /// Serializes the specified object and adds that into this dictionary.
        /// </summary>
        /// <param name="name_">The name of the object to add.</param>
        /// <param name="object_">The object to add.</param>
        public void AddObject(string name_, object object_)
        {
            using (MemoryStream memStream = new MemoryStream())
            {
                FSNXml.Serialize(object_, memStream);
                Add(name_, DefaultEncoding.GetString(memStream.ToArray()));
            }
        }

        /// <summary>
        /// Retrieves the specified object from the dictionary.
        /// </summary>
        /// <typeparam name="T">The type of the object to retrieve.</typeparam>
        /// <param name="name_">The name of the object to retrieve.</param>
        /// <returns>The object with the specified name.</returns>
        public T GetObject<T>(string name_) where T : class
        {
            using (MemoryStream memStream = new MemoryStream(DefaultEncoding.GetBytes(this[name_])))
            {
                return FSNXml.Deserialize<T>(memStream);
            }
        }

        /// <summary>
        /// Returns an integer from the dictionary.
        /// </summary>
        /// <param name="name_">The name of the integer.</param>
        /// <param name="int_">The retrieved integer.</param>
        /// <returns>True in case of success.</returns>
        public bool GetInteger(string name_, out int int_)
        {
            string ints = null;
            if (!this.TryGetValue(name_, out ints))
            {
                int_ = 0;
                return false;
            }
            return int.TryParse(ints, out int_);
        }

        /// <summary>
        /// Returns a double from the dictionary.
        /// </summary>
        /// <param name="name_">The name of the double.</param>
        /// <param name="double_">The retrieved double.</param>
        /// <returns>True in case of success.</returns>
        public bool GetDouble(string name_, out double double_)
        {
            string dbls = null;
            if (!this.TryGetValue(name_, out dbls))
            {
                double_ = 0.0;
                return false;
            }
            return double.TryParse(dbls, out double_);
        }

        /// <summary>
        /// Returns a string from the dictionary.
        /// </summary>
        /// <param name="name_">The name of the string.</param>
        /// <param name="string_">The retrieved string.</param>
        /// <returns>True in case of success.</returns>
        public bool GetString(string name_, out string string_)
        {
            return this.TryGetValue(name_, out string_);
        }

        /// <summary>
        /// Returns a <see cref="T:System.String" /> that represents the current <see cref="T:System.Object" />.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String" /> that represents the current <see cref="T:System.Object" />.
        /// </returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("FSNDesktopMsgData {");
            foreach (KeyValuePair<string, string> kvp in this)
            {
                sb.Append("{'").Append(kvp.Key).Append("' => '");
                sb.Append(kvp.Value).Append("'}");
            }
            sb.Append("}");
            return sb.ToString();
        }

        /// <summary>
        /// Gets the default encoding.
        /// </summary>
        private static Encoding DefaultEncoding
        {
            get
            {
                return Encoding.UTF8;
            }
        }
    }
}
