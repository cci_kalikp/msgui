﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Impl.FusionMessagingImpl
{
    internal enum LogLevel
    {
        Debug = 0,
        Info = 1,
        Warn = 2,
        Error = 3,
        Fatal = 4
    }

    internal interface IFSNLog
    {
        void Debug(string msg_);
        void Debug(string msg_, Exception e_);

        void Info(string msg_);
        void Info(string msg_, Exception e_);

        void Warn(string msg_);
        void Warn(string msg_, Exception e_);

        void Error(string msg_);
        void Error(string msg_, Exception e_);

        void Fatal(string msg_);
        void Fatal(string msg_, Exception e_);

        bool DebugEnabled { get; set; }
        bool InfoEnabled { get; set; }
        bool WarnEnabled { get; set; }
        bool ErrorEnabled { get; set; }
        bool FatalEnabled { get; set; }

        void Enable(LogLevel lvl_, bool enabled_);
        bool Enabled(LogLevel lvl_);
    }
}
