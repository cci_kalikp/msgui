﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Impl.FusionMessagingImpl
{
    internal static class FDSIPCTopicNames
    {
        // desktop topics
        public const string DESKTOP_ECHO_REQUEST = "ipc://Fusion/Desktop/Echo/Request";
        public const string DESKTOP_ECHO_RESPONSE = "ipc://Fusion/Desktop/Echo/Response";
        // remote shell IPC topics
        public const string REMOTE_SHELL_OPEN = "ipc://Fusion/Desktop/RemoteShell/Open";
        public const string REMOTE_SHELL_OPENED = "ipc://Fusion/Desktop/RemoteShell/Opened";
        public const string REMOTE_SHELL_INPUT = "ipc://Fusion/Desktop/RemoteShell/Input";
        public const string REMOTE_SHELL_OUTPUT = "ipc://Fusion/Desktop/RemoteShell/Output";
        public const string REMOTE_SHELL_CLEAR = "ipc://Fusion/Desktop/RemoteShell/Clear";
        public const string REMOTE_SHELL_CLOSE = "ipc://Fusion/Desktop/RemoteShell/Close";
        public const string REMOTE_SHELL_CLOSED = "ipc://Fusion/Desktop/RemoteShell/Closed";
    }
}
