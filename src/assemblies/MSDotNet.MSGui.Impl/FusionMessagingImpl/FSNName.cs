﻿using System;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Impl.FusionMessagingImpl
{
    internal static class FSNName
    {
        static FSNName()
        {
            _boxName = System.Environment.MachineName;
            _epoch = FSNDate.ToEpoch(DateTime.UtcNow);
        }

        public static long Epoch
        {
            get { return _epoch; }
        }

        public static string Release
        {
            set { _release = value; }
            get { return _release; }
        }

        public static string Version
        {
            set { _version = value; }
            get { return _version; }
        }

        public static string HostName
        {
            get { return _boxName; }
        }

        public static string UserName
        {
            get
            {
                return _suUser ?? Environment.UserName;
            }
            set
            {
                _suUser = value;
            }
        }

        public static string MakeUniqueName(string appcontext_)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(UserName).Append('_');
            sb.Append(HostName).Append('_');
            sb.Append(appcontext_).Append('_');
            lock (_sync)
            {
                sb.Append(_count++);
            }
            return sb.ToString();
        }

        private static int _count = 0;

        private static volatile string _suUser;
        private static volatile string _release;
        private static volatile string _version;

        private static readonly string _boxName;
        private static readonly long _epoch;

        private static readonly object _sync = new object();
    }
}
