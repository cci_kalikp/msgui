﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Impl.FusionMessagingImpl
{
    internal class FSNDesktopMsgPromiscousEventArgs : EventArgs
    {
        #region Construction

        public FSNDesktopMsgPromiscousEventArgs(string channel_, FSNDesktopMsg message_)
        {
            _channel = channel_;
            _message = message_;
        }

        #endregion

        #region Public API

        /// <summary>
        /// Desktop Messaging Channel this message was posted to.
        /// </summary>
        public string Channel
        {
            get { return _channel; }
        }

        /// <summary>
        /// Copy of intercepted message.
        /// </summary>
        public FSNDesktopMsg Message
        {
            get { return _message; }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("FSNDesktopMsgPromiscousEventArgs {");
            sb.Append(Channel).Append(", ");
            sb.Append(Message);
            sb.Append("}");
            return sb.ToString();
        }

        #endregion

        #region Private Data

        private readonly string _channel;
        private readonly FSNDesktopMsg _message;

        #endregion
    }
}
