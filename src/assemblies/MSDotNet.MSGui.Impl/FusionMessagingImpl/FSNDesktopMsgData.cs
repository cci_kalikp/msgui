﻿using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Impl.FusionMessagingImpl
{
    internal class FSNDesktopMsgData : Dictionary<string, string>
    {
        //NOTE bendel - ORIGINAL Fusion issue: Rethink the shape of a desktop message

        #region Public API

        public void AddObject(string name_, object obj_)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                FSNXml.Serialize(obj_, ms);
                this.Add(name_, DefaultEncoding.GetString(ms.ToArray()));
            }
        }

        public T GetObject<T>(string name_) where T : class
        {
            using (MemoryStream ms = new MemoryStream(DefaultEncoding.GetBytes(this[name_])))
            {
                return FSNXml.Deserialize<T>(ms);
            }
        }

        public bool GetInteger(string name_, out int int_)
        {
            string ints = null;
            if (!this.TryGetValue(name_, out ints)) { int_ = 0; return false; }
            return int.TryParse(ints, out int_);
        }

        public bool GetDouble(string name_, out double dbl_)
        {
            string dbls = null;
            if (!this.TryGetValue(name_, out dbls)) { dbl_ = 0; return false; }
            return double.TryParse(dbls, out dbl_);
        }

        public bool GetString(string name_, out string str_)
        {
            return this.TryGetValue(name_, out str_);
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("FSNDesktopMsgData {");
            foreach (KeyValuePair<string, string> kvp in this)
            {
                sb.Append("{'").Append(kvp.Key).Append("' => '");
                sb.Append(kvp.Value).Append("'}");
            }
            sb.Append("}");
            return sb.ToString();
        }

        #endregion

        #region Private Implementation

        private static Encoding DefaultEncoding
        {
            get { return Encoding.UTF8; }
        }

        #endregion
    }
}
