﻿using System;

namespace MorganStanley.MSDotNet.MSGui.Impl.FusionMessagingImpl
{
    internal static class FSNDate
    {
        public static DateTime FromEpoch(long epoch_)
        {
            DateTime ts = _epoch.AddSeconds(epoch_);
            return ts.ToLocalTime();
        }

        public static long ToEpoch(DateTime timestamp_)
        {
            TimeSpan ts = timestamp_.Subtract(_epoch);
            return ts.Ticks / 10000000;
        }

        public static string ShortFormat(DateTime timestamp_)
        {
            return timestamp_.ToLongTimeString() + " " + timestamp_.ToShortDateString();
        }

        public static string ShortFormatTime(DateTime timestamp_)
        {
            return timestamp_.ToLongTimeString();
        }

        public static string ShortFormat(long epoch_)
        {
            return ShortFormat(FromEpoch(epoch_));
        }

        public static string ShortFormatTime(long epoch_)
        {
            return ShortFormatTime(FromEpoch(epoch_));
        }

        private static DateTime _epoch = new DateTime(1970, 1, 1, 0, 0, 0, 0);
    }
}
