﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Impl.FusionMessagingImpl
{
    internal sealed class FSNDesktopID
    {
        #region Construction

        public FSNDesktopID(string name_)
        {
            // FSNGuard.ArgumentNotNullOrEmptyString(name_, "name_");
            _name = name_;
        }

        #endregion

        #region Public API

        public string Name
        {
            get { return _name; }
        }

        public override int GetHashCode()
        {
            return _name.GetHashCode();
        }

        public override bool Equals(object rhs_)
        {
            if (ReferenceEquals(rhs_, this)) { return true; }
            var rhs = rhs_ as FSNDesktopID;
            return rhs != null && rhs.Name == Name;
        }

        public override string ToString()
        {
            return Name;
        }

        #endregion

        #region Private Data

        private readonly string _name;

        #endregion
    }
}
