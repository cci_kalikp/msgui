﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Impl.FusionMessagingImpl
{
    internal class FSNCopyDataTarget<TMessage> : FSNWindow
    {
        public FSNCopyDataTarget(IntPtr hwnd_, IFSNStreamMarshaller<TMessage> marshaller_)
            : base(hwnd_)
        {
            FSNGuard.ArgumentNotNull(hwnd_, "hwnd_");
            FSNGuard.ArgumentNotNull(marshaller_, "marshaller_");
            _marshaller = marshaller_;
        }

        public bool Publish(IntPtr shell_, string channel_, TMessage msg_, object state_, uint timeoutMs_)
        {
            FSNGuard.ArgumentNotNull(shell_, "shell_");
            FSNGuard.ArgumentNotNull(channel_, "channel_");
            FSNGuard.ArgumentNotNull(msg_, "msg_");
            return Publish(shell_, channel_, Marshal(msg_, state_), timeoutMs_);
        }

        private bool Publish(IntPtr shell_, string channel_, byte[] msg_, uint timeoutMs_)
        {
            FSNGuard.ArgumentNotNull(shell_, "shell_");
            FSNGuard.ArgumentNotNull(channel_, "channel_");
            FSNGuard.ArgumentNotNull(msg_, "msg_");
            FSNCopyDataMessage msg = new FSNCopyDataMessage(channel_, msg_);
            return SendCopyData(shell_, msg.ToArray(), timeoutMs_);
        }

        private byte[] Marshal(TMessage msg_, object state_)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                _marshaller.Marshal(msg_, ms, state_);
                return ms.ToArray();
            }
        }

        private readonly IFSNStreamMarshaller<TMessage> _marshaller;
    }
}
