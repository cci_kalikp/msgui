﻿using System;
using System.Runtime.Serialization;

namespace MorganStanley.MSDotNet.MSGui.Impl.FusionMessagingImpl
{
    [Serializable]
    internal class FSNException : Exception
    {
        public FSNException() { }
        public FSNException(string msg_) : base(msg_) { }
        public FSNException(string msg_, Exception inner_) : base(msg_, inner_) { }
        protected FSNException(SerializationInfo info_, StreamingContext context_)
            : base(info_, context_)
        {
        }
    }
}
