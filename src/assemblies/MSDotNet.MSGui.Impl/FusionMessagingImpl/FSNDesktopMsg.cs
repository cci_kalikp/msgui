﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Impl.FusionMessagingImpl
{
    internal sealed class FSNDesktopMsg
    {

        #region Public API

        /// <summary>
        /// Channel this message was sent over - this is *usually* identical to the 
        /// underlying IPC transport channel except for promiscous messages.
        /// </summary>
        public string Channel
        {
            get
            {
                return _channel;
            }
            set
            {
                _channel = value;
            }
        }

        /// <summary>
        /// Identity of desktop application that posted this message.
        /// </summary>
        public FSNAppContext Source
        {
            get
            {
                return _source;
            }
            set
            {
                _source = value;
            }
        }

        /// <summary>
        /// The desktop target application this message was sent to - null for broadcast
        /// </summary>
        public FSNAppContext Destination
        {
            get
            {
                return _destination;
            }
            set
            {
                _destination = value;
            }
        }

        /// <summary>
        /// Timeout message was transmitted with - 0 for no timeout
        /// </summary>
        public uint TimeoutMS
        {
            get
            {
                return _timeoutMs;
            }
            set
            {
                _timeoutMs = value;
            }
        }

        /// <summary>
        /// Optional message identifier
        /// </summary>
        public string Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }

        /// <summary>
        /// Time to live - used in desktop warping
        /// </summary>
        public int TTL
        {
            get
            {
                return _ttl;
            }
            set
            {
                _ttl = value;
            }
        }

        /// <summary>
        /// Data associated with this message
        /// </summary>
        public FSNDesktopMsgData Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value;
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("FSNDesktopMsg {");
            sb.Append(Channel).Append(", ");
            sb.Append(Source).Append(", ");
            sb.Append(Destination).Append(", ");
            sb.Append(TTL).Append(", ");
            sb.Append(FSNContainer.ToString(Data));
            sb.Append("}");
            return sb.ToString();
        }

        #endregion

        #region Private Data

        private string _channel;
        private FSNAppContext _source;
        private FSNAppContext _destination;
        private string _id;
        private int _ttl = 1;
        private uint _timeoutMs = 0;
        private FSNDesktopMsgData _data;

        #endregion
    }
}
