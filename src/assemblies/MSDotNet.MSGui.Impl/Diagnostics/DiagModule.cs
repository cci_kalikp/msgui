﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Diagnostics/DiagModule.cs#5 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.Diagnostics
{
	public class DiagModule : IModule //move in to some external assembly later (it will be picked up by reflection module loader)
	{
		private readonly IApplicationOptions m_options;
		private readonly IChromeRegistry m_chromeRegistry;
		private readonly IChromeManager m_chromeManager;

		public DiagModule(IChromeRegistry chromeRegistry_, IChromeManager chromeManager_, IApplicationOptions options_)
		{
			this.m_chromeRegistry = chromeRegistry_;
			this.m_chromeManager = chromeManager_;
			m_options = options_;
		}

		public void Initialize()
		{
			m_options.AddOptionPage("MSDesktop", "Command Bars", new OptionsPage(m_chromeRegistry, m_chromeManager));

			//Ways to use:
			//m_implementator["Ribbon/Main/Tools"].AddItem("hello1!");
			//m_implementator["Ribbon/Main/Tools2"].AddItem("{Tools}/Menu");
			//m_implementator["{hello3!}/CommandBar"].AddItem("hello!");
			//m_implementator["{hello3!}/CommandBar"].AddItem("hello!");
			//m_implementator["{hello3!}/CommandBar"].AddItem("hello2!");
			//m_implementator["{hello3!}/CommandBar"].AddItem("Test Applet",1);
			//m_implementator["{hello3!}/CommandBar"].AddItem("hello3!_", 2);
			//m_implementator["{hello3!}/CommandBar"].AddItem("MSGuiInternal/Diagnostics");

		}
	}
}