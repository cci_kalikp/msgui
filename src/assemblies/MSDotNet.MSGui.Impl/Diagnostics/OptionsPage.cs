﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Diagnostics/OptionsPage.cs#11 $
// $Change: 848584 $
// $DateTime: 2013/10/07 23:19:23 $
// $Author: caijin $

using System.Linq;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.Ribbon;
using MorganStanley.MSDotNet.MSGui.Impl.ControlParadigm;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.Diagnostics
{
	internal class OptionsPage : IOptionView
	{
		//private readonly IControlParadigmImplementator m_implementator;
		//private readonly IControlParadigmRegistrator m_registrator;

		private readonly IChromeRegistry m_chromeRegistry;
		private readonly IChromeManager m_chromeManager;

		public OptionsPage(IChromeRegistry chromeRegistry_, IChromeManager chromeManager_)
		{
			this.m_chromeRegistry = chromeRegistry_;
			this.m_chromeManager = chromeManager_;
		}
         
		public void OnOK()
		{

		}

		public void OnCancel()
		{

		}

		public void OnDisplay()
		{
			m_textBlock.Text = CreateDiagnosticsMessage();
		}

		public void OnHelp()
		{

		}

		private readonly TextBox m_textBlock = new TextBox { IsReadOnly = true, VerticalScrollBarVisibility = ScrollBarVisibility.Visible };
		public UIElement Content
		{
			get
			{
				return m_textBlock;
			}
		}

		private string CreateDiagnosticsMessage()
		{

            //TODO: FARM fix the diag module
            return "";

            //var factories = ((ChromeManager.ChromeRegistry)m_chromeRegistry).WidgetFactories.Keys;
            //string msg = string.Empty;
            //msg += "====Available keys of commandbar controls====" + System.Environment.NewLine;
            //foreach (string key in factories)
            //{
            //    msg += key + System.Environment.NewLine;
            //}

            //msg += "=====Host Keys====" + System.Environment.NewLine;
            ////var hosts = (Dictionary<string, ICommandArea>)ReflHelper.FieldGet(m_implementator, "m_commandAreas");
            //var rootAreas = ((ChromeManagerBase)m_chromeManager).w;
            //var chromeAreas = new List<string>();
            //foreach (string key in rootAreas.Keys)
            //{
            //    chromeAreas.AddRange(EnumerateChromeAreas(key, rootAreas[key]));
            //}

            //foreach (string s in chromeAreas)
            //{
            //    msg += s + System.Environment.NewLine;
            //}

            //msg += "====Recommendations====" + System.Environment.NewLine;


            //var recommendations = ((ChromeManagerBase)m_chromeManager).widgetRegistrations;

            //foreach (var recommendation in recommendations)
            //{
            //    msg += string.Format("{0}->{1}", recommendation.FactoryID, recommendation.Root + "/" + recommendation.Location) + System.Environment.NewLine;
            //}
            //msg += "====Obsolete registrations====" + System.Environment.NewLine;
            ///*
            //foreach (ControlParadigmRegistration reg in ((ControlParadigmRegistrator)m_registrator).Registrations)
            //{
            //    if (reg.RibbonItem is RibbonButton)
            //    {
            //        msg +=
            //          string.Format("text: {3}, controlkey: {0}, tab: {1}, group: {2}", reg.ControlKey, reg.tab, reg.group,
            //                        ((RibbonButton)reg.RibbonItem).Text) +
            //          System.Environment.NewLine;
            //    }
            //    else
            //    {
            //        msg += string.Format("controlkey: {0}, tab: {1}, group: {2}", reg.ControlKey, reg.tab, reg.group) +
            //               System.Environment.NewLine;
            //    }
            //}*/
            //msg += "====Warnings====" + System.Environment.NewLine;

            //foreach (string warning in XamRibbonHelper.disabledMenuToolList)
            //{
            //    msg += warning + System.Environment.NewLine;
            //}
            //return msg;
		}

        public List<string> EnumerateChromeAreas(string path, WidgetViewContainer area)
        {
            var result = new List<string>();

            if (area.Children.Any())
            {
                result.Add(path);
            }

            foreach (var child in area.Children)
            {
                result.AddRange(EnumerateChromeAreas(path + "/" + child.FactoryID, child as WidgetViewContainer));
            }

            return result;
        }
	}
}