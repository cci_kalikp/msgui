﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media; 
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.My;

namespace MorganStanley.MSDotNet.MSGui.Impl.Diagnostics
{
  public class RenderCapabilityLogger : IMSDesktopModule
  {
    private static readonly IMSLogger logger = MSLoggerFactory.CreateLogger<RenderCapabilityLogger>();

    public RenderCapabilityLogger()
    {
      RenderCapability.TierChanged += HandleTierChanged;
    }

    public void Initialize()
    {
      LogRenderingTier();
    }

    private static void HandleTierChanged(object sender_, EventArgs e_)
    {
      LogRenderingTier();
    }

    private static void LogRenderingTier()
    {
      int renderingTier = RenderCapability.Tier >> 16;
      logger.Info("WPF rendering tier: " + renderingTier, "HandleTierChanged");
    }
  }

}
