﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewHistory
{
    sealed class BoundedObservableQueue<T> : ObservableCollection<T>
    {
        private readonly int _limit;

        public BoundedObservableQueue(int limit_)
        {
            _limit = limit_;
        }

        public void AddWithCheck(T element_)
        {
            Add(element_);
            if (Count > _limit)
            {
                RemoveAt(0);
            }
        }
    }


    //sealed class BoundedObservableQueue<T> : INotifyCollectionChanged, ICollection<T>
    //{
    //    private readonly int _limit;
    //    private readonly LinkedList<T> _storage;

    //    public BoundedObservableQueue(int limit_)
    //    {
    //        _limit = limit_;
    //        _storage = new LinkedList<T>();
    //    }

    //    public event NotifyCollectionChangedEventHandler CollectionChanged;

    //    private void OnCollectionChanged(NotifyCollectionChangedEventArgs e_)
    //    {
    //        var handler = CollectionChanged;
    //        if (handler != null) handler(this, e_);
    //    }

    //    public IEnumerator<T> GetEnumerator()
    //    {
    //        return _storage.GetEnumerator();
    //    }

    //    IEnumerator IEnumerable.GetEnumerator()
    //    {
    //        return ((IEnumerable) _storage).GetEnumerator();
    //    }

    //    public void Add(T item)
    //    {
    //        _storage.AddLast(item);
    //        if (_storage.Count > _limit)
    //        {
    //            var first = _storage.First;
    //            _storage.RemoveFirst();
    //            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, new ArrayList { first }, 0));
    //        }
    //        OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, new ArrayList { item }, _storage.ToList().IndexOf(item)));
    //    }

    //    public void Clear()
    //    {
    //        _storage.Clear();
    //        OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
    //    }

    //    public bool Contains(T item)
    //    {
    //        return _storage.Contains(item);
    //    }

    //    public void CopyTo(T[] array, int arrayIndex)
    //    {
    //        _storage.CopyTo(array, arrayIndex);
    //    }

    //    public bool Remove(T item)
    //    {
    //        var position = _storage.ToList().IndexOf(item);
    //        if (!_storage.Remove(item)) return false;
    //        OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, new ArrayList { item }, position));
    //        return true;
    //    }

    //    public int Count
    //    {
    //        get
    //        {
    //            return _storage.Count;
    //        }
    //    }

    //    public bool IsReadOnly
    //    {
    //        get
    //        {
    //            return false;
    //        }
    //    }
    //}
}
