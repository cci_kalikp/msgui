﻿ 
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Unity; 
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewHistory
{
    internal class ViewHistoryModule : IModule
    {
        internal static int HistorySize { private get; set; }

        private readonly IUnityContainer _container;
        private readonly IChromeManager _chromeManager;
        private readonly IChromeRegistry _chromeRegistry;
        private RecentlyClosedService _service;

        private const string ViewHistoryWidgetFactory = "ViewHistoryWidgetFactoryId";
        
        public ViewHistoryModule(IUnityContainer container_, IChromeManager chromeManager_, IChromeRegistry chromeRegistry_)
        {
            _container = container_;
            _chromeManager = chromeManager_;
            _chromeRegistry = chromeRegistry_;
        }

        readonly Dictionary<ViewRestorationDescriptor, IWidgetViewContainer> widgetsByView = new Dictionary<ViewRestorationDescriptor, IWidgetViewContainer>();
        private IWidgetViewContainer dropDownWidget;
        public void Initialize()
        {
            _container.RegisterInstance(new RecentlyClosedService(_chromeRegistry, _chromeManager, HistorySize));
            _service = _container.Resolve<RecentlyClosedService>();

            dropDownWidget = _chromeManager[ChromeArea.ApplicationMenu].AddWidget(ViewHistoryWidgetFactory, new InitialDropdownButtonParameters()
                {
                    Text = "Recently closed"
                });
            foreach (var descriptor in _service.RecentlyClosedViews)
            {
                var widget = dropDownWidget.AddWidget(new InitialButtonParameters()
                    {
                        Text = descriptor.Title,
                        Image = descriptor.Icon,
                        Click = ViewClicked,  
                    });
                 
                widgetsByView.Add(descriptor, widget); 
            }
            _service.RecentlyClosedViews.CollectionChanged += RecentlyClosedViews_CollectionChanged;
        }


        void RecentlyClosedViews_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (ViewRestorationDescriptor newItem in e.NewItems)
                {
                    var widget = dropDownWidget.AddWidget(new InitialButtonParameters()
                    {
                        Text = newItem.Title,
                        Click = ViewClicked,
                        Image = newItem.Icon,
                    });

                    widgetsByView.Add(newItem, widget); 
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (ViewRestorationDescriptor oldItem in e.OldItems)
                {
                    IWidgetViewContainer widget;
                    if (widgetsByView.TryGetValue(oldItem, out widget))
                    {
                        widgetsByView.Remove(oldItem); 
                        widget.ClearParent();
                    }

                }
            }
        }

        private void ViewClicked(object sender_, EventArgs eventArgs_)
        {
            foreach (var widgetViewContainer in widgetsByView)
            {
                if (widgetViewContainer.Value.Content == sender_)
                {
                    _service.RestoreView(widgetViewContainer.Key);
                    break;
                }
            }
        }
    }
}
