﻿using System.Windows.Media;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewHistory
{
    internal class ViewRestorationDescriptor
    {
        public string FactoryId { get; set; }
        public XDocument State { get; set; }
        public InitialWindowParameters Parameters { get; set; }
        public string Title { get; set; }
        public ImageSource Icon { get; set; }
    }
}