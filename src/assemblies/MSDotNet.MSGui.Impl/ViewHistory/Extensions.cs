﻿ 

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewHistory
{
    public static class Extensions
    {
        public static void EnableViewHistory(this Framework framework_, int historySize_)
        {
            ViewHistoryModule.HistorySize = historySize_;
            framework_.AddModule<ViewHistoryModule>();
        }
    }
}
