﻿using System.Collections.Specialized;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.Factories;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.Converters;

namespace MorganStanley.MSDotNet.MSGui.Impl.ViewHistory
{
    internal class RecentlyClosedService
    {
        private readonly IChromeRegistry _chromeRegistry;
        private readonly IChromeManager _chromeManager;

        public BoundedObservableQueue<ViewRestorationDescriptor> RecentlyClosedViews { get; private set; }

        public RecentlyClosedService(IChromeRegistry chromeRegistry_, IChromeManager chromeManager_, int limit_)
        {
            _chromeRegistry = chromeRegistry_;
            _chromeManager = chromeManager_;
            _chromeManager.Windows.CollectionChanged += WindowsOnCollectionChanged;

            RecentlyClosedViews = new BoundedObservableQueue<ViewRestorationDescriptor>(limit_);

        }

        public void RestoreView(ViewRestorationDescriptor descriptor_)
        {
            RecentlyClosedViews.Remove(descriptor_);
            descriptor_.Parameters.InitialLocation = InitialLocation.PlaceAtCursor | InitialLocation.Floating;
            ((ChromeManagerBase) _chromeManager).CreateWindow(descriptor_.FactoryId, descriptor_.Parameters,
                                                              descriptor_.State);
        }

        private void WindowsOnCollectionChanged(object sender_,
                                                NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs_)
        {
            if (notifyCollectionChangedEventArgs_.Action != NotifyCollectionChangedAction.Remove) return;
            foreach (IWindowViewContainer oldItem in notifyCollectionChangedEventArgs_.OldItems)
            {
                var descriptor = new ViewRestorationDescriptor();
                WindowFactoryHolder creator;
                var creatorId = oldItem.FactoryID;
                ((IChromeElementInitializerRegistry) _chromeRegistry).TryGetWindowFactoryHolder(creatorId, out creator);
                if (creator == null) return;
                if (creator.DehydrateHandler != null)
                {
                    descriptor.State = creator.DehydrateHandler(oldItem);
                }
                descriptor.FactoryId = creatorId;
                descriptor.Parameters = oldItem.Parameters;
                descriptor.Title = oldItem.Title;
                if (oldItem.Icon != null)
                {
                    descriptor.Icon = IconImageSourceConverter.IconToImageSource(oldItem.Icon);
                } 
                RecentlyClosedViews.AddWithCheck(descriptor);
            }
        }


    }
}
