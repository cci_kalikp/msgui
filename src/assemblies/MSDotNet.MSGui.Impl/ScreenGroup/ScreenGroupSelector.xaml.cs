﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ScreenGroup;

namespace MorganStanley.MSDotNet.MSGui.Impl.ScreenGroup
{
    /// <summary>
    /// Interaction logic for ScreenGroupSelector.xaml
    /// </summary>
    public partial class ScreenGroupSelector : UserControl, INotifyPropertyChanged
    {
        private readonly IScreenGroupManager _screenGroupManager;
        private readonly IWindowViewContainer _windowViewContainer;

        private readonly IUnityContainer _unityContainer;

        static internal readonly IScreenGroup NoneGroup = new ScreenGroup("Clear Screen Group");  //hack --- to allow combobox handle None value and to open create new value
        static internal readonly IScreenGroup CreateGroup = new ScreenGroup("Create New Group");

        static internal bool IsNoneGroup(IScreenGroup group)
        {
            return group == NoneGroup;
        }

       
        public ScreenGroupSelector(IWindowViewContainer owner)
        {
            _windowViewContainer = owner;
            _unityContainer = ServiceLocator.Current.GetInstance<IUnityContainer>();
            _screenGroupManager = _unityContainer.Resolve<IScreenGroupManager>();
            this.DataContext = this;
            InitializeComponent();
            this.GroupCombobox.SelectedItem = NoneGroup;

            _screenGroupManager.GroupCollectionChanged += (s, e) => this.NotifyPropertyChanged("ScreenGroups");
           
        }

        public IWindowViewContainer Owner { get { return _windowViewContainer; } }

        public IEnumerable<IScreenGroup> ScreenGroups
        {
            get
            {
                foreach (var screenGroup in _screenGroupManager.ScreenGroups)
                {
                    yield return screenGroup;
                }
                yield return NoneGroup;
                yield return CreateGroup;


            }
        }

       
        public IScreenGroup SelectedScreenGroup
        {
            get
            {   
                var group = _screenGroupManager.GetScreenGroup(Owner);
                if(group == null)
                    return NoneGroup;

                return group;
            }
            set
            {
                
                var current = _screenGroupManager.GetScreenGroup(Owner);
                if (current != null)
                {
                    current.RemoveWindow(Owner);
                }
                if (value == NoneGroup)
                    return;
                if(value != null)
                {
                    value.AddWindow(Owner);
                }

                NotifyPropertyChanged("SelectedScreenGroup");
            }
        }

        private void NotifyPropertyChanged(string name)
        {
            var copy = this.PropertyChanged;
            if (copy != null)
            {
                copy(this, new PropertyChangedEventArgs(name));
            }
        }

        private static System.Collections.Generic.IDictionary<string,Func<string>> actions = new Dictionary<string, Func<string>>();

       

      

        #region Implementation of INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        private void SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Contains(CreateGroup))
            {
                var res = TaskDialog.Show(new TaskDialogOptions
                                    {
                                        AllowDialogCancellation = true,
                                        UserInputEnabled = true
                                    });

                if (res.Result != TaskDialogSimpleResult.Cancel)
                {
                    TaskDialog.ShowMessage(res.UserInput);

                    var group = _screenGroupManager.CreateGroup("New");

                    this.GroupCombobox.SelectedValue = group;

                    NotifyPropertyChanged("ScreenGroups");
                }

            }
        }
    }


    [ValueConversion(typeof(IScreenGroup), typeof(Brush))]
    class ScreenGroupColorConverter: IValueConverter
    {
        private readonly IScreenGroupManager _screenGroupManager;
        public ScreenGroupColorConverter()
        {
            _screenGroupManager = ServiceLocator.Current.GetInstance<IUnityContainer>().Resolve<IScreenGroupManager>();
        }
        #region Implementation of IValueConverter

        /// <summary>
        /// Converts a value. 
        /// </summary>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        /// <param name="value">The value produced by the binding source.</param><param name="targetType">The type of the binding target property.</param><param name="parameter">The converter parameter to use.</param><param name="culture">The culture to use in the converter.</param>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return _screenGroupManager.GetScreenGroupBrush((IScreenGroup) value);
        }

        /// <summary>
        /// Converts a value. 
        /// </summary>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        /// <param name="value">The value that is produced by the binding target.</param><param name="targetType">The type to convert to.</param><param name="parameter">The converter parameter to use.</param><param name="culture">The culture to use in the converter.</param>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    class GroupItemTemplateSelector : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            var element = container as FrameworkElement;
            if(element == null)
                return null;

            if (item is IScreenGroup)
            {
                if (ScreenGroupSelector.NoneGroup == item)
                {
                    return element.FindResource("NoneScreenGroupTemplate") as DataTemplate; 
                }

                if (ScreenGroupSelector.CreateGroup == item)
                {
                    return element.FindResource("NewScreenGroupTemplate") as DataTemplate; ;
                }

                return element.FindResource("ScreenGroupTemplate") as DataTemplate;
            }

            return null;
        }
    }
}
