﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Media;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ScreenGroup;

namespace MorganStanley.MSDotNet.MSGui.Impl.ScreenGroup
{
    internal class ScreenGroupManager : IScreenGroupManager
    {
        public static bool IsScreenGroupSupportEnabled { get; set; }

        private readonly Dictionary<IScreenGroup, Brush> _screenGroup = new Dictionary<IScreenGroup, Brush>();
        private readonly HashSet<IWindowViewContainer> _selfAutoHookViews = new HashSet<IWindowViewContainer>();

        private void OnGroupMemeberChanged(object sender, GroupMemberChangedEventArgs args)
        {
            if (args.AddedWindow != null)
            {
                InvokeGroupMemberChanged(args.AddedWindow, null, V2VRole.None,  sender as IScreenGroup, args.Role);
            }
            if (args.RemovedWindow != null)
            {
                InvokeGroupMemberChanged(args.RemovedWindow, sender as IScreenGroup, args.Role, null, V2VRole.None);
            }
        }

      



        public IEnumerable<IScreenGroup> GetScreenGroupsByName(string name)
        {
            return _screenGroup.Keys.Where(g => g.Name == name);
        }

        public IScreenGroup CreateGroup(string name, Brush color=null)
        {
            var group = new ScreenGroup(name);
            _screenGroup[group] = color;
            group.GroupMemberChanged += OnGroupMemeberChanged;
            InvokeGroupAdded(group);
            return group;
        }

        public void RemoveGroup(IScreenGroup group)
        {
            ((ScreenGroup) group).GroupMemberChanged -= OnGroupMemeberChanged;
            _screenGroup.Remove(group);
            InvokeGroupRemoved(group);
        }

        public IScreenGroup GetScreenGroup(IWindowViewContainer view)
        {
            return _screenGroup.Keys.Where((g) => g.Windows.Contains(view)).FirstOrDefault();
        }


        public Brush GetScreenGroupBrush(IScreenGroup screenGroup)
        {
            return _screenGroup[screenGroup];
        }

        public event EventHandler<GroupChangedEventArgs> GroupMemberChanged;
        public event EventHandler<GroupCollectionChagnedArgs> GroupCollectionChanged;

        public IEnumerable<IScreenGroup> ScreenGroups
        {
            get { return _screenGroup.Keys; }
        }


        public void EnableSelfAutoHook(IWindowViewContainer windowViewContainer)
        {
            _selfAutoHookViews.Add(windowViewContainer);
        }

        public void DisableSelfAutoHook(IWindowViewContainer windowViewContainer)
        {
            _selfAutoHookViews.Remove(windowViewContainer);
        }

        public bool IsViewSelfAutoHook(IWindowViewContainer windowViewContainer)
        {
            return _selfAutoHookViews.Contains(windowViewContainer);
        }

        private void InvokeGroupMemberChanged(IWindowViewContainer source, IScreenGroup oldGroup,V2VRole oldType,  IScreenGroup newGroup, V2VRole newType)
        {
            var copy = GroupMemberChanged;
            if (copy != null)
            {
                copy(this, new GroupChangedEventArgs()
                               {
                                   Window = source,
                                   NewGroup = newGroup,
                                   NewRole = newType,
                                   OldGroup = oldGroup,
                                   OldRole = oldType
                               });
            }
        }

        private void InvokeGroupAdded(IScreenGroup group)
        {
            var copy = GroupCollectionChanged;
            if (copy != null)
            {
                copy(this, new GroupCollectionChagnedArgs() {AddedGroup = group});
            }
        }

        private void InvokeGroupRemoved(IScreenGroup group)
        {
            var copy = GroupCollectionChanged;
            if (copy != null)
            {
                copy(this, new GroupCollectionChagnedArgs() { RemovedGroup = group });
            }
        }
    }


}
