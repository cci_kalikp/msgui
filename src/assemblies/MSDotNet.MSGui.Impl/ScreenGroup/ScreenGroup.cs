﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Fluent;
using MorganStanley.MSDotNet.MSGui.Core.ScreenGroup;

namespace MorganStanley.MSDotNet.MSGui.Impl.ScreenGroup
{
    class ScreenGroup: IScreenGroup
    {
        private readonly HashSet<IWindowViewContainer> _subscribingWindows = new HashSet<IWindowViewContainer>();
        private readonly HashSet<IWindowViewContainer> _publishingWindows = new HashSet<IWindowViewContainer>();

        public ScreenGroup(string name)
        {
            this.Name = name;
        }

        

        #region Implementation of IScreenGroup

        public void AddWindow(IWindowViewContainer windowViewContainer)
        {

            AddWindow(windowViewContainer, V2VRole.Both);
        }


        public void AddWindow(IWindowViewContainer windowViewContainer, V2VRole role)
        {
            if (role.HasFlag(V2VRole.AsSubscriber))
            {
                _subscribingWindows.Add(windowViewContainer);
            }
            if (role.HasFlag(V2VRole.AsPublisher))
            {
                _publishingWindows.Add(windowViewContainer);
            }
            InvokeGroupMemberChanged(windowViewContainer, true, role);
        }

        public void RemoveWindow(IWindowViewContainer windowViewContainer)
        {
            RemoveWindow(windowViewContainer, V2VRole.Both);
        }

        public void RemoveWindow(IWindowViewContainer windowViewContainer, V2VRole role)
        {
            
            if (role.HasFlag(V2VRole.AsSubscriber))
            {
                _subscribingWindows.Remove(windowViewContainer);
            }
            if (role.HasFlag(V2VRole.AsPublisher))
            {
                _publishingWindows.Remove(windowViewContainer);
            }
            InvokeGroupMemberChanged(windowViewContainer, false, role);
            
        }

        public IEnumerable<IWindowViewContainer> Windows
        {
            get { return _subscribingWindows.Union(_publishingWindows); }
        }

        public IEnumerable<IWindowViewContainer> PublishingWindows
        {
            get { return _publishingWindows; }
        }

        public IEnumerable<IWindowViewContainer> SubscribingWindows
        {
            get { return _subscribingWindows; }
        }

        public string Name { get; set; }

        public event EventHandler<GroupMemberChangedEventArgs> GroupMemberChanged;


        private void InvokeGroupMemberChanged(IWindowViewContainer window, bool isAdded, V2VRole v2VRole)
        {
            var copy = GroupMemberChanged;
            if(copy == null)
                return;

            if (isAdded)
            {
                copy(this, new GroupMemberChangedEventArgs() { AddedWindow = window, Role = v2VRole});
            }
            else
            {
                copy(this, new GroupMemberChangedEventArgs() { RemovedWindow = window, Role = v2VRole });
            }
        }

        #endregion
    }



    internal class GroupMemberChangedEventArgs : EventArgs
    {
        public IWindowViewContainer AddedWindow { get; internal set; }
        public V2VRole Role { get; internal set; }
        public IWindowViewContainer RemovedWindow { get; internal set; }
    }
    
}
