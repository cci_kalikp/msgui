﻿using System;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using MorganStanley.IED.Concord.Configuration;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Helpers;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MorganStanley.MSDotNet.My;

namespace MorganStanley.MSDotNet.MSGui.Impl.Concord
{
	public class MSDesktopLayoutsAdapter : IConfigurationInterceptingWriter
	{
        private static readonly IMSLogger _logger = MSLoggerFactory.CreateLogger<MSDesktopLayoutsAdapter>();
		private static IPersistenceStorage _fps;

		private void EnsureFPS()
		{
			if (Fps == null)
			{

				Fps = new FilePersistenceStorage(EntryAssemblyHelper.GetEntryAssembly().GetName().Name);
				//fps = new MultipleFilePersistenceStorage("ExampleFrameworkApp");
                _logger.Info("Initializing default FPS");
			}
            try
            {
                _fps.InitializeStorage();
            }
            catch (Exception exc)
            {
                _logger.Warning(string.Format("FPS could not be initialized (type {0})", _fps.GetType().FullName), exc);
            }

		}

	    internal static IPersistenceStorage Fps
	    {
            get
            {
                return _fps;
            }
            set
            {
                _fps = value;
            }
	    }

	    public void InterceptWrite(string name, string version, XmlNode node,
	                               InterceptingConfigurationStore.DecoratedWriteConfiguration decoratedWriteConfiguration)
	    {

	        var prof = _fps.AvailableProfiles.ToArray().ToList();
            _logger.Debug("Intercepting FPS write");

	        foreach (
	            XmlElement element in node.OwnerDocument.SelectNodes("MSDesktopLayouts/WindowManager/Layouts/Layout"))
	        {
	            var xDocument = new XDocument();
	            xDocument.Add(XmlHelper.XmlElementToXElement((XmlElement) element.FirstChild));
	            _fps.SaveState(xDocument, element.Attributes["name"].Value);
	            prof.Remove(element.Attributes["name"].Value);
	        }
	        foreach (string v in prof)
	        {
	            _fps.DeleteState(v);
	        }

	        var defaultName =
	            node.OwnerDocument.SelectSingleNode("MSDesktopLayouts/WindowManager/DefaultLayout").InnerText;

	        if (!string.Equals(_fps.DefaultName, defaultName, StringComparison.OrdinalIgnoreCase))
	        {
	            //should always be saved if changed, even for partial storage, saving this would fallback to a central file msgui.config
	            _fps.DefaultName = defaultName;
	        }
	    }

	    public ConfigurationSection InterceptRead(ConfigurationSection original)
		{
            _logger.Debug("Intercepting FPS read");
			EnsureFPS();
			var node = new XmlDocument
			{
				InnerXml =
					string.Format(@"<MSDesktopLayouts><WindowManager type=""{0}""><Layouts></Layouts><DefaultLayout></DefaultLayout></WindowManager></MSDesktopLayouts>", ConcordPersistenceStorage.MSDESKTOP_WINDOW_MANAGER_NAME)
			};
			//<Layout name="Default">"
			foreach (var availableProfile in _fps.AvailableProfiles)
			{
				var state = _fps.LoadState(availableProfile);
				var attr = node.CreateAttribute("name");
				attr.InnerText = availableProfile;
				var element = node.CreateElement("Layout");
				element.Attributes.Append(attr);
				element.InnerXml = state.ToString();
				node.DocumentElement["WindowManager"]["Layouts"].AppendChild(element);
			    if (_fps.DefaultName != null)
			    {
			        node.DocumentElement["WindowManager"]["DefaultLayout"].InnerText = _fps.DefaultName;
			    }
			}

			return new ConfigurationSection(original.Name, original.Version, new XmlNode[] { }, node.DocumentElement);
		}
	}
}