﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.Shell;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using IWindowManager = MorganStanley.IED.Concord.Application.IWindowManager;
using Microsoft.Practices.Unity;

namespace MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl
{
    public static class WindowManagerExtensions
    {
        public static void SubscribeToSelectedTabIndexChanged(this IWindowManager windowManager_,
            Action handler_)
        {
            if (!(windowManager_ is ConcordWindowManager)) return;
            if (handler_ == null) return;
            var container = (windowManager_ as ConcordWindowManager).Container;
            var tabbedDockViewModel = container.Resolve<IShell>().WindowManager as TabbedDockViewModel;
            if (tabbedDockViewModel == null) return;
            tabbedDockViewModel.Tabs.CurrentChanged += (sender_, args_) => handler_();
        }

        public static void SelectTabContainingForm(this IWindowManager windowManager_, Form form_)
        {
            if (!(windowManager_ is ConcordWindowManager)) return;
            var chromeManager = (windowManager_ as ConcordWindowManager).ChromeManager;
            var tab = chromeManager.Tabwell.Tabs
                .Cast<ShellTabViewModel>()
                .FirstOrDefault(t_ => 
                                t_.Windows.Any(
                                    w_ => ((AppletFormHost)((DockPanel) w_.Content).Children[0]).Child as Form == form_)
                );
            tab.Activate();
        }

        public static List<Form> GetFormsOnCurrentTab(this IWindowManager windowManager_)
        {
            if (!(windowManager_ is ConcordWindowManager)) return null;
            var chromeManager = (windowManager_ as ConcordWindowManager).ChromeManager;
            var activeTab =
                chromeManager.Tabwell.Tabs.FirstOrDefault(t_ => ((ShellTabViewModel) t_).IsActive);
            if (activeTab == null) return new List<Form>();
            return (activeTab)
                .Windows
                .Select(w_ => ((AppletFormHost)((DockPanel)w_.Content).Children[0]).Child as Form)
                .ToList();
        }

        public static void AddNewTab(this IWindowManager windowManager_, string name_)
        {
            if (!(windowManager_ is ConcordWindowManager)) return;
            var chromeManager = (windowManager_ as ConcordWindowManager).ChromeManager;
            chromeManager.Tabwell.AddTab(name_);
        }
    }
}
