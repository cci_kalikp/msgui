﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.View;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl
{
	internal class ConcordTypeToViewCreator : IConcordTypeToViewCreator
	{
		#region normal windows
		public void Map(string concordAppletFQN, string creatorID)
		{
			ChromeManagerBase.AppletCreatorMappings.Add(concordAppletFQN.ToLower(), creatorID);
		}

		public void Map(string concordAppletFQN, string creatorID, Func<XElement, XElement> converter)
		{
			ChromeManagerBase.AppletCreatorMappings.Add(concordAppletFQN.ToLower(), creatorID);
			ChromeManagerBase.AppletConverterMappings.Add(creatorID, converter);
		}

		public void Map(string concordAppletFQN, Func<string, XElement, string> getCreatorId)
		{
			ChromeManagerBase.AppletCreatorFunctionMappings.Add(concordAppletFQN, getCreatorId);
		}
		#endregion


		#region singleton windows
		public void MapSingleton(string concordAppletFQN, string creatorID)
		{
			Map(concordAppletFQN, creatorID);
			ChromeManagerBase.AppletCreatorMappedSingletons.Add(creatorID);
		}

		public void MapSingleton(string concordAppletFQN, string creatorID, Func<XElement, XElement> converter)
		{
			Map(concordAppletFQN, creatorID, converter);
			ChromeManagerBase.AppletCreatorMappedSingletons.Add(creatorID);
		}
		
		public void MapSingleton(string concordAppletFQN, Func<string, XElement, Tuple<string, bool>> getCreatorId)
		{
			ChromeManagerBase.AppletCreatorSingletonFunctionMappings.Add(concordAppletFQN, getCreatorId);
		}
		
		#endregion
	}
}