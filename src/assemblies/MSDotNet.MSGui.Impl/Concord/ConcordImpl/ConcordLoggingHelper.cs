﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using MorganStanley.IED.Concord.Logging;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSLog;
using MorganStanley.MSDotNet.My;

namespace MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl
{
    public class ConcordLoggingHelper
    {
        internal const LogLevel DefaultConcordLoglevel = LogLevel.Info;
        internal const string DefaultLogThreshold = "*/*/*:All";
        internal const string Mslogconfig = "msLogConfig";
        internal const string MslogconfigCorrectCase = "MSLogConfig";

        private static readonly IMSLogger m_logger = MSLoggerFactory.CreateLogger<ConcordLoggingHelper>();

        /// <summary>
        /// Sets up the log file.
        /// </summary>
        /// <param name="loggingEnabled"></param>
        /// <returns>The log file.</returns>
        internal static MSLogFileDestination SetupLogging(bool loggingEnabled)
        {
            MSLogFileDestination logFile = null;
            try
            {
                Log.LogLevel = DefaultConcordLoglevel;
                Log.EnableRequestTracing = false;
                MSLog.MSLog.Mode = MSLog.MSLog.MSLogMode.Async;
                MSLog.MSLog.AssignThreadId = true;

                if (!loggingEnabled)
                {
                    logFile = new MSLogFileDestination(GetLogFileName());
                    MSLog.MSLog.AddDestination("/default/fileDestination", logFile);
                }

                AddTraceDestinationIfNeeded();

                var logConfig = TryGetMsLogConfig();
                if (null != logConfig)
                {
                    ApplyMsLogConfig(logConfig);
                    return null;
                }

                SetConcordLoggerLogLevel();
                SetThresholdPolicyForDefaultTarget();

                return logFile;
            }
            catch (Exception e)
            {
                if (MSLogConfig.WasInitialized())
                {
                    m_logger.Error("Could not setup login for Concord", e, "SetupLogging");
                }
                return null;
            }
        }

        internal static void AddTraceDestinationIfNeeded()
        {
            string traceLogOutput = ConfigurationSettings.AppSettings["TraceLogOutput"];
            if (traceLogOutput != null && Boolean.Parse(traceLogOutput))
            {
                MSLog.MSLog.AddDestination("/default/traceDestination", new TraceDestination());
            }
        }

        internal static void SetThresholdPolicyForDefaultTarget()
        {
            string thresholdPolicy = ConfigurationSettings.AppSettings["LogThresholdPolicy"];// ?? DefaultLogThreshold;
            if (thresholdPolicy == null) return;
            MSLog.MSLog.SetThresholdPolicy("/default", thresholdPolicy); //All,IED/Concord.Configuration/*:None,ied/concordviewmgmt/*:None");      

            if (Log.IsLogLevelEnabled(LogLevel.Info))
            {
                Log.Info.SetLayer("msdotnet", "msdesktop").SetLocation("MSDesktop.ConcordLoader", "StartLog()").
                    WriteFormat("Application started: " + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString());
            }
        }

        internal static void SetConcordLoggerLogLevel()
        {
            string configLogLevel = ConfigurationManager.AppSettings["ConcordLogLevel"];
            if (!String.IsNullOrEmpty(configLogLevel))
            {
                try
                {
                    Log.LogLevel = (LogLevel)Enum.Parse(typeof(LogLevel), configLogLevel.Trim(), true);
                }
                catch (Exception ex_)
                {
                    // TODO
                    m_logger.Error(
                        String.Format("Parse of concord LogLevel failed, cannot convert {0} to LogLevel enum", configLogLevel),
                        ex_);
                }

                // set log level to enable loggin from logLevel and above
                LogLevel tmpLogLevel = Log.LogLevel;
                do
                {
                    Log.LogLevel |= tmpLogLevel;
                    tmpLogLevel = (LogLevel)((int)tmpLogLevel / 2);
                } while ((int)tmpLogLevel > 1);
            }
            else
            {
                Log.LogLevel = LogLevel.All;
            }
        }

        internal static void ApplyMsLogConfig(XmlDocument logConfig)
        {
            try
            {
                if (!MSLogConfig.WasInitialized())
                    MSLogConfig.Init(logConfig);
            }
            catch (Exception exc)
            {
                Log.Error.SetLayer("msdotnet", "msdesktop").SetLocation("MSDesktop.ConcordLoader", "SetupLog").WriteFormat(
                    "Failed to set up logging with \"{0}\"", exc, logConfig.ToString());
            }
					
            SetLogLevel(logConfig);

            if (Log.IsLogLevelEnabled(LogLevel.Info))
            {
                Log.Info.SetLayer("msdotnet", "msdesktop").SetLocation("MSDesktop.ConcordLoader", "StartLog()").
                    WriteFormat("Application started: " + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString());
            }
        }

        internal static XmlDocument TryGetMsLogConfig()
        {
            XmlDocument logConfig = null;
            var exeConfiguration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var configurationSection = exeConfiguration.GetSection(Mslogconfig);
            if (configurationSection != null)
            {
                var rawXML = configurationSection.SectionInformation.GetRawXml();
                if (!String.IsNullOrEmpty(rawXML))
                {
                    logConfig = new XmlDocument();
                    logConfig.LoadXml(rawXML);
                    if (logConfig.DocumentElement.NodeType == XmlNodeType.Element)
                    {
                        XmlElement oldElement = (XmlElement)logConfig.DocumentElement;
                        XmlElement newElement = logConfig.CreateElement(MslogconfigCorrectCase);
                        while (oldElement.HasAttributes)
                        {
                            newElement.SetAttributeNode(oldElement.RemoveAttributeNode(oldElement.Attributes[0]));
                        }
                        while (oldElement.HasChildNodes)
                        {
                            newElement.AppendChild(oldElement.FirstChild);
                        }
                        logConfig.RemoveAll();
                        logConfig.LoadXml(newElement.OuterXml);
                    }
                }
            }
            else
            {
                configurationSection = exeConfiguration.GetSection(MslogconfigCorrectCase);
                if (configurationSection != null)
                {
                    var rawXML = configurationSection.SectionInformation.GetRawXml();
                    if (!String.IsNullOrEmpty(rawXML))
                    {
                        logConfig = new XmlDocument();
                        logConfig.LoadXml(rawXML);
                    }
                }
            }
            return logConfig;
        }

        /// <summary>
        /// Purges the old log files.
        /// </summary>
        /// <param name="olderThanDays">Files older than this many days will be purged.</param>
        internal static void PurgeOldLogFiles(int olderThanDays)
        {
            if (olderThanDays > 0 && System.IO.Directory.Exists(GetLogFileDirectory()))
            {
                DateTime cutOffDate = DateTime.Now.Subtract(new TimeSpan(olderThanDays, 0, 0, 0));
                if (Log.IsLogLevelEnabled(LogLevel.Debug))
                {
                    Log.Debug.SetLayer("msdotnet", "msdesktop").SetLocation("MSDesktop.ConcordLoader", "PurgeOldLogFiles").
                        WriteFormat("Deleting files dated older than " + cutOffDate.ToString("yyyy-MM-dd"));
                }

                DirectoryInfo logDirInfo = new DirectoryInfo(GetLogFileDirectory());

                foreach (FileInfo logFile in logDirInfo.GetFiles("*.log"))
                {
                    if (logFile.CreationTime.Date <= cutOffDate.Date)
                    {
                        try
                        {
                            logFile.Delete();
                        }
                        catch (Exception ex_)
                        {
                            if (Log.IsLogLevelEnabled(LogLevel.Debug))
                            {
                                Log.Info.SetLayer("msdotnet", "msdesktop").SetLocation("MSDesktop.ConcordLoader", "PurgeOldLogFiles").
                                    WriteFormat("Cannot delete " + logFile + " file", ex_);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets the log file directory.
        /// </summary>
        /// <returns>The log file directory.</returns>
        internal static string GetLogFileDirectory()
        {
            string directory = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            var logFolderName = ConcordExtensionStorage.LogFolderName ?? ConcordApplication.m_msguiApplication.Name;
            if (!(System.IO.Directory.Exists(directory + @"\Morgan Stanley\" + logFolderName + @"\Log")))
            {
                System.IO.Directory.CreateDirectory(directory + @"\Morgan Stanley\" + logFolderName + @"\Log");
            }
            directory += @"\Morgan Stanley\" + logFolderName + @"\Log\";
            return directory;
        }

        /// <summary>
        /// Gets the name of the log file.
        /// </summary>
        /// <returns>The name of the log file.</returns>
        internal static string GetLogFileName()
        {
            //create the logfile
            string file = GetLogFileDirectory() + ConcordApplication.m_msguiApplication.Name + Process.GetCurrentProcess().Id + @".log";
            // if a log file with the same name exists, let's remove it first
            try
            {
                if (File.Exists(file))
                {
                    File.Delete(file);
                }
            }
            catch (Exception ex_)
            {
                Trace.WriteLine("Error removing the older log file: " + ex_);
            }

            return file;
        }

        /// <summary>
        /// Sets the log level.
        /// Based on threshold policies in config, determine the matching ConcordLogger.LogLevel
        /// </summary>
        protected static void SetLogLevel(XmlDocument config)
        {
            try
            {
                int highestLevel = 0;

                //XmlNode config = ConfigurationSettings.GetConfig(MSLOGCONFIG) as XmlNode;

                if (config != null)
                {
                    var nsm = new XmlNamespaceManager(config.NameTable);
                    nsm.AddNamespace("mslog", "http://xml.msdw.com/ns/msdotnet/mslog/1.0");


                    highestLevel = (from XmlNode rule in
                                        (config.SelectNodes("//mslog:Rule", nsm).Cast<XmlNode>().Union(
                                            config.SelectNodes("//Rule").Cast<XmlNode>()))
                                    where rule.Attributes["priority"] != null
                                    where Enum.IsDefined(typeof(LogLevel), rule.Attributes["priority"].Value)
                                    select (LogLevel)Enum.Parse(typeof(LogLevel), rule.Attributes["priority"].Value, true)).
                        Aggregate(highestLevel, (current, ruleLevel) => Math.Max((int)ruleLevel, current));

                    switch ((LogLevel)highestLevel)
                    {
                        case LogLevel.Debug:
                            Log.LogLevel = Log.LogLevel | LogLevel.Debug;
                            goto case LogLevel.Info;

                        case LogLevel.Info:
                            Log.LogLevel = Log.LogLevel | LogLevel.Info;
                            goto case LogLevel.Notice;

                        case LogLevel.Notice:
                            Log.LogLevel = Log.LogLevel | LogLevel.Notice;
                            goto case LogLevel.Warning;

                        case LogLevel.Warning:
                            Log.LogLevel = Log.LogLevel | LogLevel.Warning;
                            goto case LogLevel.Error;

                        case LogLevel.Error:
                            Log.LogLevel = Log.LogLevel | LogLevel.Error;
                            goto case LogLevel.Critical;

                        case LogLevel.Critical:
                            Log.LogLevel = Log.LogLevel | LogLevel.Critical;
                            goto case LogLevel.Alert;

                        case LogLevel.Alert:
                            Log.LogLevel = Log.LogLevel | LogLevel.Alert;
                            goto case LogLevel.Emergency;

                        case LogLevel.Emergency:
                            Log.LogLevel = Log.LogLevel | LogLevel.Emergency;
                            break;

                        case LogLevel.All:
                            Log.LogLevel = LogLevel.All;
                            break;

                        case LogLevel.None:
                            Log.LogLevel = LogLevel.None;
                            break;
                    }

                }
            }
            catch (Exception ex_)
            {
                if (Log.IsLogLevelEnabled(LogLevel.Error))
                {
                    Log.Error.SetLayer("msdotnet", "msdesktop").SetLocation("MSDesktop.ConcordLoader", "SetLogLevel")
                        .Write("Error creating dynamic log level", ex_);
                }
            }
        }
    }
}
