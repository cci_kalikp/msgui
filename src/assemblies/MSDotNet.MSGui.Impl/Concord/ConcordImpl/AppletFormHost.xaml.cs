/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Concord/ConcordImpl/AppletFormHost.xaml.cs#16 $
// $Change: 877979 $
// $DateTime: 2014/04/23 06:16:41 $
// $Author: milosp $

using System;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using System.Windows.Media;
using MorganStanley.IED.Concord.Application;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.My;
using System.Reflection;

namespace MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl
{

    /// <summary>
    /// Interaction logic for AppletFormHost.xaml
    /// </summary>
    public partial class AppletFormHost : WindowsFormsHost
    {
        private static readonly IMSLogger m_logger = MSLoggerFactory.CreateLogger<AppletFormHost>();
    	private static readonly FontFamily FontFamilyToSet = new FontFamily("Microsoft Sans Serif");

        public AppletFormHost()
        {
            InitializeComponent();
            //     Margin="0" FontSize="11.33" FontFamily="Microsoft Sans Serif">
            if (SetMargin)
                wfh.Margin = new Thickness(0);
            if (SetFontSize)
                wfh.FontSize = DefaultFontSize ?? 11.33;
            if (SetFontFamily)
                wfh.FontFamily = FontFamilyToSet;
        }

        internal static bool SetMargin { get; set; }
        internal static bool SetFontSize { get; set; }
        internal static bool SetFontFamily { get; set; }
        internal static double? DefaultFontSize { get; set; }

        public IWindow Window
        {
            get;
            set;
        }


        protected override void Dispose(bool disposing_)
        {
            var form = Child as Form;
            if (form != null)
            {
                try
                {
                    form.Close();
                    form.Visible = false;

					// reflection hack to fix a leak in the .net fw itself
					object winFormsAdapter = typeof(WindowsFormsHost).GetProperty("HostContainerInternal", BindingFlags.GetProperty | BindingFlags.NonPublic | BindingFlags.Instance).GetValue(this, null);

					//System.Diagnostics.Debugger.Launch();
					ReflHelper.FieldSet(winFormsAdapter, "_host", null);


					if (winFormsAdapter is IDisposable)
					{
						((IDisposable)winFormsAdapter).Dispose();
					} 
					
					base.Dispose(disposing_);
                }
                catch (Exception e)
                {
                    m_logger.Error("Error occured when disposing child form", e);
                }
            }
        }
    }
}
