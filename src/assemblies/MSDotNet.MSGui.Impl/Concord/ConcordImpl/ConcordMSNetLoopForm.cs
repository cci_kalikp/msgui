﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Windows.Forms;
using MorganStanley.MSDotNet.MSGui.Core.Win32;

namespace MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl
{
  public class ConcordMSNetLoopForm : Form
  {

    #region Overrides
		
    #endregion

		protected override CreateParams CreateParams
		{
			get
			{
				var parms = base.CreateParams;
                parms.Style &= ~(int)Win32.WS_CAPTION;
				parms.Style &= ~(int)Win32.WS_BORDER;
                parms.Style &= ~(int)Win32.WS_MINIMIZEBOX;
                parms.Style &= ~(int)Win32.WS_MAXIMIZEBOX;
                parms.Style |= (int)Win32.WS_EX_TOOLWINDOW;
				return parms;
			}
		}


    #region Extendsion Points
    protected internal void RaiseOnClosing(object sender_, FormClosingEventArgs arg_)
    {
      //var cancelEventHandler = base.FormClosing;
      var closing = GetFormClosingEvent() as CancelEventHandler;
      if (closing != null)
      {
        closing(sender_, arg_);
      }


      var formClosing = GetFormClosingEvent() as FormClosingEventHandler;
      if (formClosing != null)
      {
        formClosing(sender_, arg_);
      }

    }

    protected internal void RaiseOnClosed()
    {
      throw new NotImplementedException("Not Supported");
    }

    #endregion

    #region Private Functions
    public Delegate GetFormClosingEvent()
    {
      return GetClosingEvent(EVENT_FORMCLOSING);
    }

    public Delegate GetClosingEvent()
    {
      return GetClosingEvent(EVENT_CLOSING);
    }


    public Delegate GetClosingEvent(string eventType_)
    {
      /* -- toggable group 1
      * this may work on .NET 3.5 era, but no longer worked with the .NET 4.0
      foreach (var e in typeof(Form).GetEvents())
      {
        FieldInfo fi = this.GetType().GetField(e.Name, BindingFlags.NonPublic | BindingFlags.Instance);
        object value = fi.GetValue(this);

        if (value != null)
        {
          var frmClosingEventHandler = (FormClosingEventHandler)value;

          if (value != null)
          {
          }
        }
      }
    /*/
      var fi = typeof(Form).GetField(eventType_, BindingFlags.Static |  // Static
                                                        BindingFlags.NonPublic // Non Public Method
                                    );
      if (fi != null)
      {
        object eventFormClosing = fi.GetValue(null);                           // Static method ignore object argument
        if (eventFormClosing != null)
        {
          return this.Events[eventFormClosing] as Delegate;
        }
      }
      return null;
      //*/

    }

    public void TurnOnFormClosingEvents()
    {
    
      if (m_closingEvtStatus == ConcordClosingEventStatus.Off)
      {
        // restore the Closing Invocation List 
        if (m_closingInvocationList != null)
        {
          foreach (var invoke in m_closingInvocationList)
          {
            var i = invoke as CancelEventHandler;
            if (i != null)
              this.Closing += i;
          }
        }

        // restore the FormClosing Invocation List 
        if (m_formClosingInvocationList != null)
        {
          foreach (var invoke in m_formClosingInvocationList)
          {
            var i = invoke as FormClosingEventHandler;
            if (i != null)
            {
              this.FormClosing += i;
            }
          }
        }

        m_closingEvtStatus = ConcordClosingEventStatus.On;
      }

    }

    public void TurnOffFormClosingEvents()
    {

      var closing = GetClosingEvent();
      var formClosing = GetFormClosingEvent();

      if (m_closingEvtStatus == ConcordClosingEventStatus.On)
      {
        if (closing != null)
          m_closingInvocationList = closing.GetInvocationList();
        if (formClosing != null)
          m_formClosingInvocationList = formClosing.GetInvocationList();

        if (m_closingInvocationList != null)
        {
          foreach (var invoke in m_closingInvocationList)
          {
            var i = invoke as CancelEventHandler;
            if (i != null)
              this.Closing -= i;

          }

        }

        if (m_formClosingInvocationList != null)
        {
          foreach (var invoke in m_formClosingInvocationList)
          {
            var i = invoke as FormClosingEventHandler;
            if (i != null)
              this.FormClosing -= i;
          }
        }
        m_closingEvtStatus = ConcordClosingEventStatus.Off;
      }
    }

    #region Inner Class
    private enum ConcordClosingEventStatus
    {
      On,                  // On
      Off                  // Off
    }
    #endregion

    #region Fields

    private const string EVENT_FORMCLOSING = "EVENT_FORMCLOSING";
    private const string EVENT_CLOSING = "EVENT_CLOSING";

    private Delegate[] m_formClosingInvocationList = null;

    private Delegate[] m_closingInvocationList = null;

    private ConcordClosingEventStatus m_closingEvtStatus = ConcordClosingEventStatus.On; // default on
    #endregion

    #endregion
  }
}
