﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Concord/ConcordImpl/OptionPageAdapter.cs#10 $
// $Change: 848584 $
// $DateTime: 2013/10/07 23:19:23 $
// $Author: caijin $

using System.Windows.Forms.Integration;
using MorganStanley.IED.Concord.Application;
using System.Windows;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl
{
  internal class OptionPageAdapter : IOptionView
  {
    private readonly OptionPageBase m_concordOptionPage;
    private readonly WindowsFormsHost m_wfh;
    internal OptionPageAdapter(OptionPageBase concordOptionPage_)
    {
      m_concordOptionPage = concordOptionPage_;
      m_wfh = new WindowsFormsHost {Child = m_concordOptionPage};
    }
       
    public void OnOK()
    {
      m_concordOptionPage.OnOK();
    }

    public void OnCancel()
    {
      m_concordOptionPage.OnCancel();
    }

    public void OnDisplay()
    {
      m_concordOptionPage.OnDisplay();
    }

    public void OnHelp()
    {
      m_concordOptionPage.OnHelp();
    }

    public UIElement Content
    {
      get
      {
        return m_wfh;
      }
    }
  }
}
