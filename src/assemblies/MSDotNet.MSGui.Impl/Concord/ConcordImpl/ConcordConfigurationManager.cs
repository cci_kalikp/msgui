﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Concord/ConcordImpl/ConcordConfigurationManager.cs#49 $
// $Change: 888544 $
// $DateTime: 2014/07/16 07:07:22 $
// $Author: caijin $

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading;
using System.Windows.Media;
using System.Xml;
using MorganStanley.IED.Concord.Configuration;
using MorganStanley.IED.Concord.Runtime;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Helpers;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.My;
using MSDesktop.AuthenticationService.BasicLogin;
using MSDesktop.AuthenticationService.Configuration;
using MSDesktop.AuthenticationService.ImpersonationLogin;
using LoginDisplay = MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.LoginDisplay;
using LoginWindow = MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.LoginWindow;


namespace MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl
{
    internal static class ConcordConfigurationManager
    {
        private static readonly IMSLogger _logger = MSLoggerFactory.CreateLogger("ConcordConfigurationManager");
        private static readonly IDictionary<string, string> _concordProfiles = new Dictionary<string, string>();
        private static int _loginComplete;
        internal static bool AddDefaultProfile = true;

        public static void Initialize(string concordDefaultProfile)
        {
            _logger.Debug("Initializing Concord configuration");
            Initialize(null, concordDefaultProfile);
        }

        public static void Initialize(Arguments arguments, string concordDefaultProfile)
        {
            _logger.Debug("Initializing Concord configuration for custom arguments");
            //IMSNetLoop loop = MSNetLoopWithAffinityBase.GetLoop() ?? new MSNetLoopDispatcherImpl(container_.Resolve<IShell>().MainWindow);
            GetConcordProfiles();
            string profile = ConcordLogin(arguments, concordDefaultProfile);
            if (profile == null)
            {
                System.Environment.Exit(0);
            }
            else
            {
                _logger.DebugWithFormat("ConcordLogin: InitializeConfiguration for profile '{0}'", profile);
                InitializeConfiguration(GetConcordProfileString(profile));
                CurrentProfile = profile;
                ConcordPersistenceStorage.ConcordConfigurationManagerInitialized = true;
            }

        }

        internal static bool PreInitialize(Arguments arguments)
        {
            GetConcordProfiles();
            string profile = arguments["profile"] ?? "Default";
            string profileString;
            if (!_concordProfiles.TryGetValue(profile, out profileString))
            {
                return false;
            }
            InitializeConfiguration(profileString);
            return true;
        }
         
        public static string CurrentProfile { get; private set; }
        internal static string ApplicationName { get; set; }
        internal static string ApplicationVersion { get; set; }
        internal static string ApplicationDisplayName
        {
            get
            {
               return (ApplicationName != null && ApplicationVersion != null)
                         ? string.Format("{0} - (v{1})", ApplicationName, ApplicationVersion)
                         : EntryAssemblyHelper.GetEntryAssembly().GetName().Name;
            }
        }
        internal static ImageSource ApplicationIcon { get; set; }

        private static string ConcordLogin(Arguments arguments, string concordDefaultProfile)
        {
            _logger.Debug("Executing ConcordLogin");
            //returns null if login unsuccessful
            bool loginPrompt = false;
            bool userNamePrompt = false;
            bool profilePrompt = false;

            string username = null;
            string profile = null;
            string password = null;
            string forceProfile = null;
            string forceLogin = null;
            string forceUserProfile = null; 

            if (arguments != null)
            {
                _logger.Debug("ConcordLogin: arguments are not null. Reading...");
                username = arguments["username"];
                profile = string.IsNullOrEmpty(concordDefaultProfile) ? arguments["profile"] : concordDefaultProfile;
                password = arguments["password"];
                forceProfile = arguments["forceprofile"];
                forceLogin = arguments["forcelogin"];
                forceUserProfile = arguments["forceuserprofile"]; 

            }

            string result;

            if (forceUserProfile != null)
            {
                userNamePrompt = true;
                profilePrompt = true;
            } 
        // TODO I copied this logic from concord.host but it doesn't seem very secure to me
            // further reviewing needed
            if (null != forceLogin)
            {
                _logger.Debug("ConcordLogin: logging prompt has been forced");
                loginPrompt = true;
            }
            else if (!userNamePrompt && !string.IsNullOrEmpty(username))
            {
                var cui = new ConcordUserInfo(null, username, password, null);
                ConcordRuntime.Current.SetUserInfo(cui);
                ConcordRuntime.Current.Services.SecureExternal = false;
            }
            else
            {
                loginPrompt = Convert.ToBoolean(ConcordRuntime.Current.Context["LoginRequired"]);

                if (!loginPrompt && !userNamePrompt)
                {
                    ConcordRuntime.Current.SetUserInfo(new ConcordInternalUserInfo());
                    ConcordRuntime.Current.Services.SecureExternal = false;
                }
            }

            if (forceProfile != null && !profilePrompt)
            {
                result = null;
                profilePrompt = true;
            }
            else if (!string.IsNullOrEmpty(profile) && _concordProfiles.ContainsKey(profile))
            {
                result = profile;
            }
            else
            {
                result = ConcordRuntime.Current.Context["DefaultProfile"] as string;
                profilePrompt |= (result == null);
            }

            bool prompt = loginPrompt || userNamePrompt || profilePrompt;
            var initialWindow = InitialSplashWindowFrameworkWrapper.InitialWindow; 
            if (prompt)
            { 
                // there will be a dialog so we need to hide the splashes
                if (SplashLauncher<SplashWindow>.SplashWindowInstance != null)
                {
                    //TODO potential multithreading problems (as in any other place where we use .SplashWindowInstance property)
                    SplashLauncher<SplashWindow>.SplashWindowInstance.Dispatcher.Invoke(
                        (Action) (() => SplashLauncher<SplashWindow>.SplashWindowInstance.Hide()));
                }
                else if (SplashLauncher<ModernSplashWindow>.SplashWindowInstance != null)
                {
                    //TODO potential multithreading problems (as in any other place where we use .SplashWindowInstance property)
                    SplashLauncher<ModernSplashWindow>.SplashWindowInstance.Dispatcher.Invoke(
                        (Action) (() => SplashLauncher<ModernSplashWindow>.SplashWindowInstance.Hide()));
                }
                if (initialWindow != null)
                {
                    initialWindow.IsHidden = true;
                }
            }

            //login prompt means we should use an external user, which needs username password
             if (loginPrompt || (!userNamePrompt && profilePrompt))
            {
                // Simple login window
                // TODO (hrechkin): Validate this will work with new AuthenticationService DLL.
                /*
                ILoginService loginService = new LoginService(null, ConcordRuntime.Current, new ArgumentsService(),
                                                              new AppConfigEnvironmentProfileService(null, true,
                                                                                                     ApplicationName));


                var loginResult = loginService.Login();
                */

                //TODO custom name 
                 var loginWindow = new LoginWindow(ApplicationDisplayName, _concordProfiles);
                 if (ApplicationIcon != null)
                 {
                     loginWindow.Icon = ApplicationIcon;
                 }
                var displayflags = LoginDisplay.None;
                if (loginPrompt)
                {
                    displayflags |= LoginDisplay.Credentials;
                }

                if (profilePrompt)
                {
                    displayflags |= LoginDisplay.Profile;
                }

                loginWindow.DisplayFlags = displayflags;

                bool? dialogResult = loginWindow.ShowView();

                if (dialogResult.HasValue && dialogResult.Value)
                {
                    Interlocked.Exchange(ref _loginComplete, 1);
                    if (loginPrompt)
                    {
                        ConcordRuntime.Current.SetUserInfo(new ConcordUserInfo(LoginPrompt, loginWindow.Username,
                                                                               loginWindow.Password, null));
                    }
                    result = loginWindow.SelectedProfile.Key;
                }
                else
                {
                    result = null;
                }
            }
             else if (userNamePrompt && profilePrompt)
             {
                 result = PerformImpersonatedLogon();
             }
            if (prompt)
            {
                // reshowing the splashes
                if (initialWindow != null)
                {
                    initialWindow.IsHidden = false;
                }
                if (SplashLauncher<SplashWindow>.SplashWindowInstance != null)
                {
                    SplashLauncher<SplashWindow>.SplashWindowInstance.Dispatcher.Invoke(
                        (Action) (() => SplashLauncher<SplashWindow>.SplashWindowInstance.Show()));
                }
                else if (SplashLauncher<ModernSplashWindow>.SplashWindowInstance != null)
                {
                    SplashLauncher<ModernSplashWindow>.SplashWindowInstance.Dispatcher.Invoke(
                        (Action) (() => SplashLauncher<ModernSplashWindow>.SplashWindowInstance.Show()));
                }
            }

            return result;
        }

        private static string PerformImpersonatedLogon()
        {
            string result;
            var profileService = new AppConfigEnvironmentProfileService(null, AddDefaultProfile, ApplicationName);
            var controller = new ImpersonationLoginController(
                new ApplicationDetails()
                {
                    Name = ApplicationName ?? EntryAssemblyHelper.GetEntryAssembly().GetName().Name,
                    Version = ApplicationVersion,
                    DisplayName = ApplicationDisplayName,
                    Image = ApplicationIcon
                },
            profileService);
            controller.AllowImpersonation = UserNameProfileWindow.IsImpersonationEnabled;
            ILoginService loginService = new ImpersonationLoginService(() => controller, ConcordRuntime.Current,
                                                                       new ArgumentsService(),
                                                                       profileService);

            var loginResult = loginService.Login();

            if (loginResult.Success)
            {
                Interlocked.Exchange(ref _loginComplete, 1);

                // impersonation
                var cui = new ConcordUserInfo(null, loginResult.UserInformation.Username, null, null);
                ConcordRuntime.Current.SetUserInfo(cui);
                ConcordRuntime.Current.Services.SecureExternal = false;
                result = loginResult.Profile.Name;
            }
            else
            {
                result = null;
            }
            return result;
        }

        private static void InitializeConfiguration(string profileString)
        {
            //XmlElement configSection = System.Configuration.ConfigurationManager.GetSection("concordConfiguration") as XmlElement;
            var configSection = XmlConfigurationHandler.GetConfigNode("concordConfiguration") as XmlElement;

            var defaultStore = typeof (FileConfigurationStore);

            if (configSection != null)
            {
                var configNode = XmlHelper.XmlElementToXElement(configSection);

                var defaultProvider = XmlHelper.TryGetAttribute(configNode, "defaultProvider");

                if (!string.IsNullOrEmpty(defaultProvider))
                {
                    Type type = Type.GetType(defaultProvider);
                    if (type == null)
                    {
                        throw new Exception(String.Format("Type '{0}' is not available", defaultProvider));
                    }
                    defaultStore = type;
                }
                IED.Concord.Configuration.ConfigurationManager.Initialize(profileString, defaultStore);
            }
            else if (!PersistenceStorageSetup.SuppressExtraAR)
            {
                var doc = new XmlDocument();
                doc.LoadXml(@"
<concordConfiguration defaultProvider=""MorganStanley.IED.Concord.Configuration.InterceptingConfigurationStore, Concord.Configuration"">
		<storageProviders>
			<storageProvider type=""MorganStanley.IED.Concord.Configuration.InterceptingConfigurationStore, Concord.Configuration"">
				<DecoratedStorageProvider type=""MorganStanley.IED.Concord.Configuration.FileConfigurationStoreEx, Concord.Configuration"">
				</DecoratedStorageProvider>
				<ConfigurationInterceptors>
					<ConfigurationInterceptor name=""MSDesktopLayouts"" type=""MorganStanley.MSDotNet.MSGui.Impl.Concord.MSDesktopLayoutsAdapter, MSDotNet.MSGui.Impl"" />
                    <ConfigurationInterceptor name=""Workspaces"" type=""MorganStanley.MSDotNet.MSGui.Impl.Concord.WorkspacesAdapter, MSDotNet.MSGui.Impl"" />
				</ConfigurationInterceptors>
			</storageProvider>
		</storageProviders>
	</concordConfiguration>
");
                IED.Concord.Configuration.ConfigurationManager.Initialize(profileString, typeof (InterceptingConfigurationStore), doc.DocumentElement);
            }
        }

        internal static string GetConcordProfileString(string profileName)
        {
            return _concordProfiles
                .Where(kvp => kvp.Key.Equals(profileName, StringComparison.OrdinalIgnoreCase))
                .Select(p => p.Value)
                .FirstOrDefault();
        }

        private static void GetConcordProfiles()
        {
            _concordProfiles.Clear();
            try
            {
                try
                {
                    var profileConfig =
                        (NameValueCollection) System.Configuration.ConfigurationManager.GetSection("profiles");
                    if (profileConfig == null)
                    {
                        return;
                    }

                    foreach (string name in profileConfig.AllKeys)
                    {
                        _concordProfiles.Add(name, profileConfig[name]);
                    }

                }
                catch (Exception exception)
                {
                    _logger.Info("Switching to xml based config instead of configurationmanager based", exception);
                    _concordProfiles.Clear();
                    var profileConfigNode = XmlConfigurationHandler.GetConfigNode("profiles");
                    if (profileConfigNode == null)
                    {
                        return;
                    }

                    foreach (XmlNode childNode in profileConfigNode.ChildNodes)
                    {
                        if (childNode.NodeType == XmlNodeType.Element)
                            _concordProfiles.Add(childNode.Attributes["key"].Value, childNode.Attributes["value"].Value);
                    }

                }
            }
            finally
            {
                if (_concordProfiles.Count == 0)
                {
                    _concordProfiles.Add("Default", string.Format("app={0}", ApplicationName));
                }
            }
        }


        private static bool LoginPrompt(ref string userName, ref string password, ref byte[] authToken)
        {
            _logger.Debug("Opening login prompt");
            if (_loginComplete == 1)
            {
                var loginWindow =
                    new LoginWindow(ApplicationDisplayName,
                                    _concordProfiles);
                if (ApplicationIcon != null)
                {
                    loginWindow.Icon = ApplicationIcon;             
                }
                loginWindow.DisplayFlags = LoginDisplay.Credentials;
                loginWindow.Username = userName;
                loginWindow.Password = password;

                bool? dialogResult = loginWindow.ShowView();

                if (dialogResult.HasValue && dialogResult.Value)
                {
                    ConcordRuntime.Current.SetUserInfo(new ConcordUserInfo(LoginPrompt, loginWindow.Username,
                                                                           loginWindow.Password, null));
                    //TODO where to save these values? They were saved in ConcordApplication originally. Do we need to do it?
                    //userName_ = login.Username;
                    //password_ = login.Password;
                    return true;
                }
            }
            return false;

        }

        #region Internal class EclipseConfigInfo

        internal class EclipseConfigInfo
        {
            private const int DEFAULT_TIMEOUT = 120000;

            public string Connection = null;
            public int Timeout = DEFAULT_TIMEOUT;
            public string Domain = null;
            public string ResourceName = null;
            public string Action = null;
            public ResourceAttribute[] Attributes = null;

            public EclipseConfigInfo()
            {
            }

            public EclipseConfigInfo(XmlNode eclipseNode)
            {
                try
                {
                    var tempNode = eclipseNode.SelectSingleNode("timeout");
                    if (tempNode != null)
                    {
                        double temp;
                        if (Double.TryParse(tempNode.InnerText, System.Globalization.NumberStyles.Any,
                                            System.Globalization.CultureInfo.InvariantCulture, out temp))
                        {
                            Timeout = (int) temp;
                        }
                    }
                    tempNode = eclipseNode.SelectSingleNode("connection");
                    if (tempNode != null)
                    {
                        Connection = tempNode.InnerText;
                    }
                    tempNode = eclipseNode.SelectSingleNode("resource");
                    if (tempNode != null)
                    {
                        ResourceName = tempNode.Attributes["name"].Value;
                        Domain = tempNode.Attributes["domain"].Value;
                        Action = tempNode.Attributes["action"].Value;
                        
                        var attribNodes = tempNode.SelectNodes("attrib");
                        if (attribNodes != null && attribNodes.Count > 0)
                        {
                            Attributes = new ResourceAttribute[attribNodes.Count];
                            for (int i = 0; i < attribNodes.Count; i++)
                            {
                                Attributes[i] = new ResourceAttribute
                                    (
                                    attribNodes[i].Attributes["name"].Value,
                                    attribNodes[i].Attributes["value"].Value
                                    );
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger.ErrorWithFormat("Error loading eclipse connection config: {0}", ex.Message);
                }
            }
        }

        #endregion Internal class EclipseConfigInfo

        #region Internal class ResourceAttribute

        internal class ResourceAttribute
        {
            public string AttribName = null;
            public string AttribValue = null;

            public ResourceAttribute()
            {
            }

            public ResourceAttribute(string name, string value)
            {
                AttribName = name;
                AttribValue = value;
            }
        }

        #endregion Internal class ResourceAttribute
    }
}
