﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Concord/ConcordImpl/AddMenuItemModule.cs#7 $
// $Change: 869092 $
// $DateTime: 2014/03/03 03:21:59 $
// $Author: caijin $

using System;
using System.Collections.Generic;
using System.Drawing;
using Microsoft.Practices.Composite.Modularity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.Ribbon;

namespace MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl
{

  public class AddMenuItemModule
  {
    internal static readonly IList<Registration> registrations = new List<Registration>();

    public static void AddMenuItem(string menuname, string itemname, Icon icon, Action onclick)
    {
      AddMenuItem(menuname, itemname, icon, onclick, RibbonButtonSize.Large, "Additional");
    }

    public static void AddMenuItem(string menuname, string itemname, Icon icon, Action onclick, RibbonButtonSize ribbonButtonSize, string tabname)
    {
      registrations.Add(new Registration
                          {
                            menuname = menuname,
                            itemname = itemname,
                            icon = icon,
                            onclick = onclick,
                            ribbonbuttonsize = ribbonButtonSize,
                            tabname = tabname
                          });
    }


    #region Nested type: Registration

    internal struct Registration
    {
      internal Icon icon;
      internal string itemname;
      internal string menuname;
      internal Action onclick;
      internal RibbonButtonSize ribbonbuttonsize;
      internal string tabname;
    }

    #endregion
  }
  internal class AddMenuItemModuleImpl : IMSDesktopModule
  {
    private readonly IControlParadigmRegistrator m_ribbon;

    public AddMenuItemModuleImpl(IControlParadigmRegistrator ribbon)
    {
      m_ribbon = ribbon;
    }

    #region IModule Members

    public void Initialize()
    {
      foreach (AddMenuItemModule.Registration registration in AddMenuItemModule.registrations)
      {
        AddMenuItemModule.Registration registration_copy = registration;
        var rb = new RibbonButton(
          registration_copy.icon,
          registration_copy.itemname,
          registration_copy.ribbonbuttonsize,
          registration_copy.itemname,
          (x_, y_) => registration_copy.onclick());
        m_ribbon.AddRibbonItem(registration_copy.tabname, registration_copy.menuname, rb);
      }
    }

    #endregion
  }
}
