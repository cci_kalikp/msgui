﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Concord/ConcordImpl/Arguments.cs#5 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $


// Borrowed from Concord.Host
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Text.RegularExpressions;

namespace MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl
{
  /// <summary>
  /// Arguments class
  /// </summary>
  internal class Arguments
  {
    // Variables
    internal StringDictionary Parameters;

    // Constructor
    public Arguments(IEnumerable<string> args_)
    {
      Parameters = new StringDictionary();
      Regex spliter = new Regex(@"^-{1,2}|^/|=|:", RegexOptions.IgnoreCase | RegexOptions.Compiled);
      Regex remover = new Regex(@"^['""]?(.*?)['""]?$", RegexOptions.IgnoreCase | RegexOptions.Compiled);
      string parameter = null;
      string[] parts;

      // Valid parameters forms:
      // {-,/,--}param{ ,=,:}((",')value(",'))
      // Examples: -param1 value1 --param2 /param3:"Test-:-work" /param4=happy -param5 '--=nice=--'
      foreach (string txt in args_)
      {
        // Look for new parameters (-,/ or --) and a possible enclosed value (=,:)
        parts = spliter.Split(txt, 3);
        switch (parts.Length)
        {
          // Found a value (for the last parameter found (space separator))
          case 1:
            if (parameter != null)
            {
              if (!Parameters.ContainsKey(parameter))
              {
                parts[0] = remover.Replace(parts[0], "$1");
                Parameters.Add(parameter, parts[0]);
              }
              parameter = null;
            }
            // else Error: no parameter waiting for a value (skipped)
            break;
          // Found just a parameter
          case 2:
            // The last parameter is still waiting. With no value, set it to true.
            if (parameter != null)
            {
              if (!Parameters.ContainsKey(parameter)) Parameters.Add(parameter, "true");
            }
            parameter = parts[1];
            break;
          // Parameter with enclosed value
          case 3:
            // The last parameter is still waiting. With no value, set it to true.
            if (parameter != null)
            {
              if (!Parameters.ContainsKey(parameter)) Parameters.Add(parameter, "true");
            }
            parameter = parts[1];
            // Remove possible enclosing characters (",')
            if (!Parameters.ContainsKey(parameter))
            {
              parts[2] = remover.Replace(parts[2], "$1");
              Parameters.Add(parameter, parts[2]);
            }
            parameter = null;
            break;
        }
      }
      // In case a parameter is still waiting
      if (parameter != null)
      {
        if (!Parameters.ContainsKey(parameter)) Parameters.Add(parameter, "true");
      }
    }

    // Retrieve a parameter value if it exists
    public string this[string param_]
    {
      get
      {
        return (Parameters[param_]);
      }
    }

    public override string ToString()
    {
        StringBuilder argList = new StringBuilder("Arguments");
        if ((Parameters != null) && (Parameters.Count > 0))
        {
          foreach (string key in Parameters.Keys)
          {
            argList.AppendFormat(" {0}='{1}'", key, Parameters[key]);
          }
        }
        else
        {
          argList.Append(" None");
        }
      return argList.ToString();
    }
  }
}


