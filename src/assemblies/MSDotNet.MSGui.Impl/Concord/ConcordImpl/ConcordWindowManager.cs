﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Concord/ConcordImpl/ConcordWindowManager.cs#38 $
// $Change: 896821 $
// $DateTime: 2014/09/15 02:20:20 $
// $Author: caijin $

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using Microsoft.Practices.Unity;
using MorganStanley.IED.Concord.Application;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.View;
using MorganStanley.MSDotNet.MSNet;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.My;

namespace MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl
{
	internal class ConcordWindowManager : WindowManagerBase
	{
		private static readonly IMSLogger m_logger = MSLoggerFactory.CreateLogger<ConcordWindowManager>();
	    internal static bool PreventDoubleWindowCreation = false;
        
		private readonly IConcordApplication m_concordApplication;
	    private readonly IUnityContainer m_container;
	    private readonly ChromeManagerBase m_viewManager;
		private readonly IStatusBar m_statusBar;
		private readonly IItemsDictionaryService m_persistenceService;
		private readonly IMSNetLoop m_loop;


		public ConcordWindowManager(
		  IConcordApplication concordApplication_,
		  IUnityContainer container_)
			: base(concordApplication_)
		{
			m_concordApplication = concordApplication_;
		    m_container = container_;
		    m_viewManager = container_.Resolve<IViewManager>() as ChromeManagerBase;
			if (m_viewManager == null)
			{
				throw new InvalidOperationException("IViewManager must be ViewManager");
			}
			m_statusBar = container_.Resolve<IStatusBar>();
			m_persistenceService = container_.Resolve<IPersistenceService>() as IItemsDictionaryService;
			if (m_viewManager == null)
			{
				throw new InvalidOperationException("IPersistenceService must be IItemsDictionaryService");
			}
			m_loop = container_.Resolve<IMSNetLoop>();
		}

		public override ConcordAppletBase CreateApplet(string name_, Type type_)
		{
			try
			{
				ConcordAppletBase applet = AppletDomain.Host.CreateApplet(m_concordApplication, type_, name_);
				return applet;
			}
			catch (MSNetInvocationException invocationException)
			{
				if (invocationException.InnerException != null)
				{
					throw invocationException.InnerException;
				}
				throw;
			}
		}

		public override IWindow CreateWindow(string caption_, Form form_)
		{
			return CreateWindow(caption_, form_, ConcordFormStyles.Default, Guid.NewGuid(), null, true, null);
		}

		public override IWindow CreateWindow(string caption_, Form form_, ConcordFormStyles styles_)
		{
			return CreateWindow(caption_, form_, styles_, Guid.NewGuid(), null, true, null);
		}

		public override void LoadLayout(XmlElement layout_)
		{
			if (layout_ != null)
			{

				XDocument layoutXDocument = new XDocument(XmlHelper.XmlElementToXElement(layout_));
				//m_persistenceServiceRestoreItemsFromDict.(PersistenceProfileService.XDocumentToDict(layoutXDocument));          
				var dict = new Dictionary<string, XElement>();
				dict["MSGui.Internal.ViewManager"] = LayoutConverter.PaneWindowManagerToTabbedDock(layoutXDocument.Root);
				m_persistenceService.RestoreItemsFromDict(dict,ItemScope.Normal);

			}
		}

		//for historical reasons this function is supposed to put the current layout to the given XmlElement, not actually save it
		public override void SaveLayout(XmlElement layout_)
		{
			Dictionary<string, XElement> itemsDict = m_persistenceService.SaveItemsToDict(ItemScope.Normal);
			XElement layoutElement;
			itemsDict.TryGetValue("MSGui.Internal.ViewManager", out layoutElement);

			//XDocument itemsXDoc = PersistenceProfileService.DictToXDocument(itemsDict);

			if (layoutElement == null)
			{
				//TODO log or show error message
				return;
			}

			XElement convertedLayoutElement = LayoutConverter.TabbedDockToPaneWindowManager(layoutElement);

			if (convertedLayoutElement != null)
			{
				foreach (XElement child in convertedLayoutElement.Elements())
				{
					layout_.AppendChild(XmlHelper.XElementToXmlElement(child, layout_.OwnerDocument));
				}
			}
		}

		public override void Startup()
		{
			throw new NotImplementedException();
		}

		public override void Shutdown()
		{
			throw new NotImplementedException();
		}

		protected override WindowCollection GetWindowCollection()
		{
			return new WindowCollectionAdapter();
		}

		public override IWindow ActiveWindow
		{
			get
			{
				WindowViewContainer viewContainer = m_viewManager.ActiveViewContainer as WindowViewContainer;
				if (viewContainer != null && viewContainer.ViewContainerType == ViewContainerType.Concord && viewContainer.Content is IWindow)
				{
					return (IWindow)viewContainer.Content;
				}
				return null;
			}
		}

		//public new event WindowCreatedEventHandler WindowCreated;
        private List<Form> loadedWindows = new List<Form>();
	    internal static bool ActivateConcordWindowWhenOpenedFromMenu = false;
		internal WindowAdapter CreateWindow(string caption_, Form form_, ConcordFormStyles styles_, Guid guid_, string id_, bool visible_, object tab_, bool forceId=false)
		{
            if (ConcordWindowManager.PreventDoubleWindowCreation)
            {
                foreach (WindowAdapter window in _windows)
                {
                    if (window.Form == form_)
                    {
                        if (forceId)
                        {
                            window.ViewContainer.ID = id_;
                            ConcordExtensionStorage.ViewContainerByWindowGuidStore[window.Guid.ToString()] = id_;
                            var store = form_.Controls.Find("WindowGuidStore", true).OfType<Label>().FirstOrDefault();
                            if (store != null)
                            {
                                store.Tag = window.Guid.ToString();
                            }
                        }
                        return window;
                    }
                } 
            }

            //there is a chance that the CreateWindow method is called when DeserializeForm using window factory(this is not recommended)
            //so we need to set the IsLayoutLoadActive to true before the form is loaded
            if (applyingPaneNode)
            {
                IFeedbackWindow feedbackWindow = form_ as IFeedbackWindow;
                if (feedbackWindow != null)
                {
                    feedbackWindow.IsLayoutLoadActive = true;
                    if (ChromeManagerBase.SequentialViewLoad)
                    {
                        EventHandler<EventArgs> fullyLoaded = null;
                        feedbackWindow.FullyLoaded += fullyLoaded = (sender_, args_) =>
                        {
                            loadedWindows.Add(form_);
                            feedbackWindow.FullyLoaded -= fullyLoaded;
                        };
                    } 
                } 

            }
			var adapter = new WindowAdapter(caption_, form_, guid_, visible_,
			                                          id_ ?? WindowViewContainer.GenerateNewID());
            if (ChromeManagerBase.SequentialViewLoad && applyingPaneNode && !(form_ is IFeedbackWindow))
            {
                loadedWindows.Add(form_);
            }
			//we need this lookup to see if the form was loaded by DeserializeConcordForm
			Label guidStore = form_.Controls.Find("WindowGuidStore", true).OfType<System.Windows.Forms.Label>().FirstOrDefault();

			if (guidStore == null)
			{
				guidStore = new System.Windows.Forms.Label()
				{
					Tag = adapter.Guid,
					Name = "WindowGuidStore"
				};
				form_.Controls.Add(guidStore);
				guidStore.Visible = false;
			}

			if (form_ is ILinkableWindow)
			{
				adapter.LinkControl = LinkManager.GetLinkControl(((ILinkableWindow)form_).Colleague);
			}
			if (!string.IsNullOrEmpty(id_))
			{
				adapter.ViewContainer.ID = id_;
			}
			var viewContainer = m_viewManager.AddView(adapter.ViewContainer, tab_);
			viewContainer.Icon = form_.Icon;

		    EventHandler<WindowEventArgs> closedHandler = null;
			adapter.ViewContainer.Closed += closedHandler = (a, b) =>
			{
				if (((WindowCollectionAdapter)this._windows).Contains(adapter))
				{
					((WindowCollectionAdapter)this._windows).Remove(adapter);
				}
			    adapter.ViewContainer.Closed -= closedHandler;
                adapter.ViewContainer.ClosedQuietly -= closedHandler;
            };

		    adapter.ViewContainer.ClosedQuietly += closedHandler;

			((WindowCollectionAdapter)_windows).Add(adapter);

            try
            {
                //TODO: Make this more threadsafe
                OnWindowCreated(new WindowCreatedEventArgs(adapter, m_loop));
            }
            finally
            {
                var initializerStore = form_.Controls.Find("WindowInitializerGuidStore", true).OfType<Label>().FirstOrDefault();
                if (initializerStore != null && ConcordExtensionStorage.ViewContainerInitializers.ContainsKey(initializerStore.Tag.ToString()))
                {
                    ConcordExtensionStorage.ViewContainerInitializers[initializerStore.Tag.ToString()].WindowViewContainer =
                        viewContainer;
                    ConcordExtensionStorage.ViewContainerInitializers.Remove(initializerStore.Tag.ToString());
                }
            }

            if (!applyingPaneNode && ActivateConcordWindowWhenOpenedFromMenu)
            {
                 viewContainer.Activate();
            }

			return adapter;
		}

		private XmlDocument SerializeConcordForm(IWindow window_)
		{
		    Form form = window_.Form;
			XmlDocument window = new XmlDocument();

            string[] typeFullName = form.GetType().AssemblyQualifiedName.Split(',');
			string windowType = String.Format("{0}, {1}", typeFullName[0].Trim(), typeFullName[1].Trim());
 
			using (System.IO.StringWriter stringWriter = new System.IO.StringWriter())
			{
				XmlTextWriter xmlWriter = new XmlTextWriter(stringWriter);
				xmlWriter.WriteStartElement("ConcordForm");
				xmlWriter.WriteAttributeString("type", windowType);
                xmlWriter.WriteAttributeString("guid", window_.Guid.ToString("N"));
                xmlWriter.WriteElementString("Caption", form.Text);
				xmlWriter.WriteStartElement("Bounds");
                xmlWriter.WriteAttributeString("left", form.Left.ToString());
                xmlWriter.WriteAttributeString("top", form.Top.ToString());
                xmlWriter.WriteAttributeString("width", form.Width.ToString());
                xmlWriter.WriteAttributeString("height", form.Height.ToString());
				xmlWriter.WriteEndElement();
				xmlWriter.WriteEndElement();

				window.LoadXml(stringWriter.ToString());
			}

			XmlElement state = window.CreateElement("State");
			bool cancel = false;

			// Save state if the window supports it
            if (form is IPersistentWindow)
			{
                ((IPersistentWindow)form).SaveState(state, ref cancel);
			}

			// save any specified linking state too, if we can
            if (IsLinkingEnabled && window_.LinkControl != null)
			{
				window_.LinkControl.Colleague.SaveState(state, ref cancel);
                LinkManager.SaveState(state, ref cancel, window_.LinkControl.Colleague);
			}

			if (!cancel)
			{
				window.DocumentElement.AppendChild(state);
			}

			return window;
		}

		//State Load&Save

		internal XmlElement BuildWindowXml(IWindow win_, XmlDocument doc_)
		{
			string guid = win_.Guid.ToString("N");
			XmlElement paneElement = doc_.CreateElement("ConcordWindow");
			paneElement.Attributes.Append(doc_.CreateAttribute("Guid"));
			paneElement.Attributes["Guid"].Value = guid;
			paneElement.Attributes.Append(doc_.CreateAttribute("Title"));
			paneElement.Attributes["Title"].Value = win_.Caption;

			XmlDocument xd = SerializeConcordForm(win_);
			paneElement.AppendChild(doc_.ImportNode(xd.DocumentElement, true));

			return paneElement;
		}

	    private bool applyingPaneNode = false;
		internal void ApplyPaneNode(XmlNode paneNode_, string id_, object tab_, bool topmost_)
		{
		    PersistenceProfileService persistenceProfileService = Container.Resolve<PersistenceProfileService>();
			string guid = XmlHelper.ReadAttribute(paneNode_, "Guid", string.Empty);
            string title = XmlHelper.ReadAttribute(paneNode_, "Title", string.Empty);
		    if (!ChromeManagerBase.SequentialViewLoad)
		    {
		        persistenceProfileService.DoDispatchedAction(() =>
		            {
		                applyingPaneNode = true;
		                try
		                {
                            Form form = DeserializeConcordForm(paneNode_.SelectSingleNode("/ConcordForm"));
                            if (form != null)
                            {
                                WindowAdapter window = CreateWindow(title, form, ConcordFormStyles.Default, new Guid(guid), id_,
                                                                    false, tab_, true);
                                if (window != null)
                                {
                                    window.ViewContainer.Topmost = topmost_;
                                }
                            }
		                }
		                finally
		                { 
                            applyingPaneNode = false;
		                }

		            });
		    }
		    else
		    {
		        AutoResetEvent loadedEvent = new AutoResetEvent(false);
		        Form form = null;
		        persistenceProfileService.DoDispatchedAction(() =>
		            {
		                applyingPaneNode = true;
                        EventHandler<EventArgs> fullyLoaded = null;
                        IFeedbackWindow feedbackWindow = null;
                        EventHandler loaded = null;
                        try
                        {
                            form = DeserializeConcordForm(paneNode_.SelectSingleNode("/ConcordForm"));
                            if (form == null)
                            {
                                loadedEvent.Set();
                                return;
                            }
                            if (loadedWindows.Contains(form))
                            {
                                loadedWindows.Remove(form);
                                loadedEvent.Set(); 
                            }
                            else
                            {
                                feedbackWindow = form as IFeedbackWindow;
                                if (feedbackWindow != null)
                                {
                                    feedbackWindow.FullyLoaded += fullyLoaded = (sender_, args_) =>
                                    {
                                        loadedEvent.Set();
                                        feedbackWindow.FullyLoaded -= fullyLoaded;
                                        fullyLoaded = null;
                                    };
                                }
                                else
                                {
                                    if (form.IsHandleCreated) loadedEvent.Set();
                                    else
                                    {
                                        form.Load += loaded = (sender_, args_) =>
                                        {
                                            loadedEvent.Set();
                                            form.Load -= loaded;
                                            loaded = null;
                                        };
                                    }
                                }
                            } 
                            WindowAdapter window = CreateWindow(title, form, ConcordFormStyles.Default, new Guid(guid),
                                                                id_,
                                                                false, tab_, true);
                            if (window == null)
                            {
                                if (fullyLoaded != null && feedbackWindow != null)
                                {
                                    feedbackWindow.FullyLoaded -= fullyLoaded;
                                }
                                else if (loaded != null && form != null)
                                {
                                    form.Load -= loaded;
                                }
                                loadedEvent.Set();
                            }
                            else
                            {
                                window.ViewContainer.Topmost = topmost_; 
                            } 

                        }
                        catch
                        {
                            if (fullyLoaded != null && feedbackWindow != null)
                            {
                                feedbackWindow.FullyLoaded -= fullyLoaded;
                            }
                            else if (loaded != null && form != null)
                            {
                                form.Load -= loaded;
                            }
                            loadedEvent.Set();
                            throw;
                        }
                        finally
                        {
                            applyingPaneNode = false;
                        }
                        

		            }, false);

                if (WaitHandle.WaitAny(new WaitHandle[] { loadedEvent, ChromeManagerBase.CancelLayoutLoading }, ChromeManagerBase.ViewLoadingTimeout) == 0)
                {
                    //if (loadedEvent.WaitOne(ChromeManagerBase.ViewLoadingTimeout))  
                    Thread.Sleep(ChromeManagerBase.ViewLoadingWaitingTime);
                }
		    }
		}

		private Form DeserializeConcordForm(XmlNode deserializedState_)
		{
		    if (deserializedState_ == null) return null;
			string typeName = deserializedState_.Attributes["type"].Value;

			if (typeName == null)
			{
				// TODO: Ask to remove window from layout?
				m_logger.Warning("Failed to deserialized view 1 " + deserializedState_.ToString());
				return null;
			}

			Type type = Type.GetType(typeName);
			if (type == null)
			{
				// TODO: Ask to remove window from layout?
				m_logger.Warning("Failed to deserialized view 2 " + deserializedState_.ToString());
				return null;
			}

			System.Guid guid = System.Guid.Empty;
			if (deserializedState_.Attributes["guid"] != null && deserializedState_.Attributes["guid"].Value != null)
			{
				string guidString = deserializedState_.Attributes["guid"].Value;

				if (guidString != null && guidString.Length > 0)
				{
					guid = new System.Guid(guidString);
				}
			}

			if (guid == Guid.Empty)
			{
				guid = System.Guid.NewGuid();
			}

			Form instance = null;
			XmlNode state = deserializedState_.SelectSingleNode("State");

			object[] attribs = type.GetCustomAttributes(typeof(WindowFactoryAttribute), true);
			if (attribs != null && attribs.Length > 0)
			{
				// Use factory specified in attribute
				WindowFactoryAttribute factory = attribs[0] as WindowFactoryAttribute;

				if (factory != null)
				{
					if (factory.Applet != null)
					{
						if (_application.ConcordApplets[factory.Applet] != null)
						{
							instance = (Form)((IWindowFactory)_application.ConcordApplets[factory.Applet]).GetForm(typeName, deserializedState_);
						}
						else
						{
							//LogLayer.Info.SetLocation(CLASS_NAME, "DeserializeConcordForm").WriteFormat("Could not find factory.Applet {0} in the ConcordApplets Collection.", factory.Applet);
						}
					}
					else if (factory.Type != null)
					{
						m_logger.Warning("Failed to deserialized view 3 " + deserializedState_.ToString());
						//LogLayer.Info.SetLocation(CLASS_NAME, "DeserializeConcordForm").WriteFormat("factory.Applet was null for factory. Type was {0}.", factory.Type);
					}
				}
			}
			else if (type.GetInterface("IWindowFactory") != null)
			{
				// Instantiate and use this class as a factory
				IWindowFactory factory = Activator.CreateInstance(type) as IWindowFactory;
				if (factory != null)
				{
					instance = (Form)factory.GetForm(typeName, deserializedState_);
				}
			}
			else
			{
				// Just create it and show it
				instance = (Form)AppDomain.CurrentDomain.CreateInstanceAndUnwrap(type.Assembly.FullName, type.FullName);
			}

			if (instance != null)
			{

				try
                {
                    IFeedbackWindow feedbackWindow = instance as IFeedbackWindow;
                    if (feedbackWindow != null)
                    {
                        feedbackWindow.IsLayoutLoadActive = true;
                    } 
					//Can't use tag so need to add a special control
					System.Windows.Forms.Label guidStore =
					  new System.Windows.Forms.Label { Tag = guid, Name = "WindowGuidStore" };
					instance.Controls.Add(guidStore);
					guidStore.Visible = false;

					// Load state if the window supports it and it exists
					if (instance is IPersistentWindow)
					{
						if (state != null)
						{
							bool cancel = false;
							((IPersistentWindow)instance).LoadState(state, ref cancel);
						}
					}

					// Set caption
					XmlNode captionNode = deserializedState_.SelectSingleNode("Caption/text()");
					if (captionNode != null)
					{
						instance.Text = captionNode.Value;
					}

					// restore linking if possible
					if (instance is ILinkableWindow)
					{
						bool cancel = false;
						LinkManager.LoadState(state, ref cancel, ((ILinkableWindow)instance).Colleague);
					}

					return instance;
				}
				catch (Exception ex_)
				{
					m_logger.Warning("Failed to deserialized view 4 " + deserializedState_.ToString());
					//if (Log.IsLogLevelEnabled(LogLevel.Error))
					//{
					//  LogLayer.Error.SetLocation(CLASS_NAME, "DeserializeConcordForm").WriteFormat("Error loading state of control", ex_);
					//}
				}
			}
			return null;
		}

        internal ChromeManagerBase ChromeManager
        {
            get
            {
                return m_viewManager;
            }
        }

        internal IUnityContainer Container
        {
            get
            {
                return m_container;
            }
        }
	}
}