/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Concord/ConcordImpl/LinkManager/LinkManager.cs#9 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.Collections;
using System.Xml;

using MorganStanley.IED.Concord.Configuration;
using MorganStanley.IED.Concord.TopicMediator;
using MorganStanley.IED.Concord.Application;

namespace MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl.LinkManager
{

  /// <summary>
  /// Contains the application level linking information.
  /// 
  /// There is one LinkManager per application.
  /// 
  /// There a many LinkedTopics for the one LinkManager, one for each 
  /// TopicHandler that has linking enabled in the TopicMediator config, 
  /// or that has been created explicitly programatically.
  /// 
  /// There may be many LinkedSets for each LinkedTopic, each with its
  /// own unique publisher and a common set of subscribers
  /// 
  /// There are many LinkControls - one for each subscribing and/or 
  /// publishing colleague. These controls can be part of many LinkedSets.
  /// </summary>
  public class LinkManager : ILinkManager
  {
    #region Constants

    private static readonly string CLASS_NAME = "LinkManager";

    private const string CONFIG_XPATH_RESTRICT_LINKING_TO_ONE_PANE = "Linking/DefaultRestrictLinkingToOnePane";
    private const string CONFIG_XPATH_TOPICMEDIATOR_CONFIGNAME = "TopicMediator";
    private const string CONFIG_XPATH_ENABLED_LINKING = "Linking/EnableWindowLinking";
    private const string CONFIG_XPATH_TOPICHANLDERS = "TopicHandlers";
    private const string CONFIG_XPATH_TOPICHANLDER_LINKABLEATTRIBUTE = @"TopicHandler[@linkable]";
    private const string CONFIG_ATTRIBUTE_LINKABLESTATE = "linkable";
    private const string CONFIG_ATTRIBUTE_DEFAULTLINKING = "defaultlinking";
    private const string CONFIG_ATTRIBUTE_RESTRICT_LINKING_TO_ONE_PANE = "restrictlinkingtoonepane";
    private const string CONFIG_ATTRIBUTE_NAME = "name";
    private const string CONFIG_ATTRIBUTE_DESCRIPTION = "description";
    private const string CONFIG_ATTRIBUTE_PERSIST = "persist";

    #endregion Constants

    #region Declarations
    
    private IConcordApplication m_application;
    private readonly Hashtable m_linkableTopics = new Hashtable();

    // used for the GUI side of linking

    // Lock synchronization object
    private static readonly object m_instanceSyncLock = new object();
    private static LinkManager m_current;

    #endregion Declarations

    #region Constructors - singleton pattern

    protected LinkManager()
    {   
      //TODO replace all commented log lines in this file with mslog calls
      //if (Log.IsLogLevelEnabled(LogLevel.Debug))
      //{
      //  LogLayer.Debug.SetLocation(CLASS_NAME, ".ctor()").
      //    WriteFormat("Entering Constructor of LinkManager");
      //}      

      TopicMediator.Current.BeginPublishTopic += TopicMediator_BeginPublishTopic;
      TopicMediator.Current.RegisteredListener += TopicMediator_RegisteredListener;

      //if (Log.IsLogLevelEnabled(LogLevel.Debug))
      //{
      //  LogLayer.Debug.SetLocation(CLASS_NAME, ".ctor()").
      //    WriteFormat("Leaving Constructor of LinkManager");
      //}
    }

    #endregion Constructors

    #region Public Properties    

    /// <summary>
    /// Provide a reference to the application if required.
    /// </summary>
    internal IConcordApplication Application
    {
      [System.Diagnostics.DebuggerStepThrough()]
      get
      {
        return m_application;
      }
    }

    /// <summary>
    /// Used on the GUI side of Linking - the currently selected Colleague
    /// </summary>
    internal IColleague ActiveColleague { get; set; }

    /// <summary>
    /// Get the singleton instance of this class
    /// </summary>
    internal static LinkManager Current 
    {
      [System.Diagnostics.DebuggerStepThrough()]
      get
      {
        if (m_current == null)
        {
          lock (m_instanceSyncLock)
          {
            if (m_current == null)
            {
              m_current = new LinkManager();
            }
          }
        }
        return m_current;
      }
    }

    /// <summary>
    /// Ensure that linking only occurs within one tab.
    /// Can be set in Config using the Linking/RestrictLinkingToOnePane element
    /// </summary>
    internal bool RestrictLinkingToOneTab { get; set; }

    /// <summary>
    /// Switch off or on all linking in this application.
    /// Can also be set in Config using the Linking/EnableWindowLinking element
    /// </summary>
    internal bool Enabled { get; set; }

    #endregion Public Properties

    #region event handlers and private methods

    /// <summary>
    /// Called by TOpicMediator whenever its about to publish a set of messages on a single 
    /// topic from a single publisher. Note that the series of events for TopicMediator
    /// is BeginPublish, PrePublish (one for each subscriber ), EndPublish.
    /// </summary>
    private void TopicMediator_BeginPublishTopic(object sender_, PublishTopicEventArgs e_)
    {
      // cehck for anonymous sender and linked topics
      if (e_.Sender.ColleagueId != Guid.Empty && m_linkableTopics.ContainsKey(e_.CommonTopicName))
      {
        LinkedTopic topic = (LinkedTopic)m_linkableTopics[e_.CommonTopicName];
        // ensure that a linked set exists for this sender
        topic.BeginPublishTopic(e_.Sender);
      }
    }

    /// <summary>
    /// Listen to TopicMediator for any new registered subscribers.
    /// </summary>
    private void TopicMediator_RegisteredListener(object sender_, RegisteredListenerEventArgs e_)
    {
      // check that we have registered all the available topics for publishing and subscribing on this 
      // particular listener.
      ((ILinkManager)Current).AddTopicPublisher(e_.Listener, e_.Listener.PublishedTopics);
    }

    #endregion

    #region Internal Methods

    /// <summary>
    /// Each published topic may be used in config several times, and some of those
    /// topics may be linked. This method gives a list of currently linked topics that
    /// will be triggered by the given name.
    /// </summary>
    /// <param name="publishedTopicName_"></param>
    /// <returns></returns>
    internal string[] GetLinkedTopics(string publishedTopicName_)
    {
      string[] tmTopics = TopicMediator.Current.GetTopicHandlerNames(publishedTopicName_);
      if (tmTopics.Length > 0)
      {
        // establish which of these have LinkedTopics
        ArrayList al = new ArrayList();
        foreach (string name in tmTopics)
        {
          if (this.m_linkableTopics.ContainsKey(name))
          {
            al.Add(name);
          }
        }
        string[] rtn = new string[al.Count];
        al.CopyTo(rtn);
        return rtn;
      }
      // otherwise return the (empty) tmTopics. This step saves
      // us from having to create a new empty string array.
      return tmTopics;
    }

    /// <summary>
    /// Link the given subscriber to the given publisher on the given topic.
    /// 
    /// Note that this is the published topic name, which may, in config, 
    /// actually be repeated in multiple topic handlers. 
    /// 
    /// So this particular method will create a new topic dedicated to this subscriber 
    /// and this publisher, and then link them - no other colleagues will be affected
    /// by this linkage.
    /// 
    /// You will have to register the subscriber with TOpicMediator to listen on the 
    /// given topic too.
    /// </summary>
    /// <returns>
    /// true if the topic link is created, false otherwise.
    /// </returns>
    internal bool AddLinkSubscription(IColleague subscriber_, string publishedTopicName_, IColleague publisher_)
    {
      string spoofNameThatWillBeUnique = Guid.NewGuid().ToString();
      string[] uniqueHandlerNames = TopicMediator.Current.RegisterTopic(spoofNameThatWillBeUnique);
      System.Diagnostics.Debug.Assert(uniqueHandlerNames.Length == 1, "Unexpected result from RegisterTopic.");
      TopicMediator.Current.AddTopicAsPublisher(publishedTopicName_, uniqueHandlerNames[0]);
      TopicMediator.Current.AddTopicAsSubscriber(publishedTopicName_, uniqueHandlerNames[0]);
      this.AddTopicPublisher(publisher_, uniqueHandlerNames[0], false);

      bool ok = this.CreateLinkedTopic(uniqueHandlerNames[0], "AUTO GENERATED PRIVATE LINK", LinkableStateEnum.NonUserLinkable, DefaultLinkingEnum.None, false, false);
      if (ok)
      {
        LinkedTopic topic = (LinkedTopic)m_linkableTopics[uniqueHandlerNames[0]];
        // make the LinkControl available
        topic.AddSubscriptionToPublisherSet(subscriber_, publisher_);
        return true;
      }
      // failed to create the linked topic.
      return false;
    }

    /// <summary>
    /// Set the linking style for an existing LinkedSet.
    /// </summary>
    /// <returns>false if no linked set was found</returns>
    internal bool SetLinked(string topicName_, IColleague publisher_, DefaultLinkingEnum style_, bool usingPublishedTopicName_)
    {
      #region rationalise the topicName into the topicHandlerNames

      if (usingPublishedTopicName_)
      {
        string[] topics = GetLinkedTopics(topicName_);
        if (topics == null || topics.Length == 0)
        {
          return false; // no topics found for this published topic name
        }
        else
        {
          bool ok = true;
          foreach (string s in topics)
          {
            ok &= SetLinked(s, publisher_, style_, false);
          }
          return ok;
        }
      }

      #endregion

      if (m_linkableTopics.ContainsKey(topicName_))
      {
        LinkedTopic lTopic = (LinkedTopic)m_linkableTopics[topicName_];
        lTopic.SetLinked(publisher_, style_);
        return true;
      }
      // no linked set was found
      return false;
    }

    /// <summary>
    /// Set the linking style for an existing topic for the given LinkedSet
    /// back to the Default setting for this LinkedTopic.
    /// </summary>
    internal bool SetLinked(string topicName_, IColleague publisher_, bool usingPublishedTopicName_)
    {
      #region rationalise the topicName into the topicHandlerName

      if (usingPublishedTopicName_)
      {
        string[] topics = GetLinkedTopics(topicName_);
        if (topics == null || topics.Length == 0)
        {
          return false; // no topics found for this published topic name
        }
        else
        {
          bool ok = true;
          foreach (string s in topics)
          {
            ok &= SetLinked(s, publisher_, false);
          }
          return ok;
        }
      }

      #endregion

      if (m_linkableTopics.ContainsKey(topicName_))
      {
        LinkedTopic lTopic = (LinkedTopic)m_linkableTopics[topicName_];
        lTopic.SetLinked(publisher_, lTopic.DefaultLinking);
        return true;
      }
      return false;
    }

    /// <summary>
    /// Creates a linked topic for the given topicName: if usingPublishedTopicName is true, it will get any 
    /// existing topic handler topics from TopicMediator. If there are no existing published topic handler topics
    /// it will create one. If there are more than one, it will throw an ArgumentException.
    /// 
    /// Once it has a single TopicHandler name to publish on (or usingPublishedTopicName is false), it will 
    /// check to see if the linked topic exists, and if not, to create one.
    /// 
    /// This function returns true if the topic was successfully created. False otherwise.
    /// </summary>
    internal bool CreateLinkedTopic(string topicName_, string description_, LinkableStateEnum linkState_, DefaultLinkingEnum defLinking_, bool persist_, bool usingPublishedTopicName_)
    {
      return CreateLinkedTopic(topicName_, description_, linkState_, defLinking_, persist_, usingPublishedTopicName_, RestrictLinkingToOneTab);
    }

    /// <summary>
    /// Creates a linked topic for the given topicName: if usingPublishedTopicName is true, it will get any 
    /// existing topic handler topics from TopicMediator. If there are no existing published topic handler topics
    /// it will create one. If there are more than one, it will throw an ArgumentException.
    /// 
    /// Once it has a single TopicHandler name to publish on (or usingPublishedTopicName is false), it will 
    /// check to see if the linked topic exists, and if not, to create one.
    /// 
    /// This function returns true if the topic was successfully created. False otherwise.
    /// </summary>
    internal bool CreateLinkedTopic(string topicName_, string description_, LinkableStateEnum linkState_, DefaultLinkingEnum defLinking_, bool persist_, bool usingPublishedTopicName_, bool restrictLinkingToOnePane_)
    {
      #region check parameters

      if (topicName_ == null || topicName_.Trim().Length == 0)
      {
        //if (Log.IsLogLevelEnabled(LogLevel.Error))
        //{
        //  Log.Error.SetLocation(CLASS_NAME, "CreateLinkedTopic").Write("Failed to add new topic - topic name was empty." );
        //}
        return false;
      }

      #endregion

      #region rationalise the topicName into the topicHandlerName

      if (usingPublishedTopicName_)
      {
        string[] topics = GetLinkedTopics(topicName_);
        if (topics == null || topics.Length == 0)
        {
          // no linked topics found - need to establish if there is an existing topic handler we can use
          string[] existingHandlerNames = TopicMediator.Current.GetTopicHandlerNames(topicName_);
          if (existingHandlerNames.Length == 0)
          {
            TopicMediator.Current.RegisterTopic(topicName_);
            // now that we've registered a topic, we will find one next time around.
            return CreateLinkedTopic(topicName_, description_, linkState_, defLinking_, persist_, true);
          }
          else
          {
            bool ok = true;
            foreach (string s in existingHandlerNames)
            {
              // now that we've registered a topic, we will find at least one.
              ok &= CreateLinkedTopic(s, description_, linkState_, defLinking_, persist_, false);
            }
            return ok;
          }
        }
        else
        {
          bool ok = true;
          foreach (string s in topics)
          {
            ok &= CreateLinkedTopic(s, description_, linkState_, defLinking_, persist_, false);
          }
          return ok;
        }
      }

      #endregion

      // now we have one unique topic handler name, check that it has been registered
      if (TopicMediator.Current.TopicHandlerNameExists(topicName_))
      {
        // check that this topic handler name is not already in the list
        if (!this.m_linkableTopics.ContainsKey(topicName_))
        {
          // create the linked topic with this topic name
          LinkedTopic lTopic = new LinkedTopic(topicName_, linkState_, defLinking_, persist_, description_, restrictLinkingToOnePane_);
          this.m_linkableTopics.Add(topicName_, lTopic);
          return true;
        }
        else
        {
          //if (Log.IsLogLevelEnabled(LogLevel.Error))
          //{
          //  Log.Error.SetLocation(CLASS_NAME, "CreateLinkedTopic").Write("Failed to add new topic - A linked topic with this name already exists." );
          //}
        }
      }
      else
      {
        //if (Log.IsLogLevelEnabled(LogLevel.Error))
        //{
        //  Log.Error.SetLocation(CLASS_NAME, "CreateLinkedTopic").Write("Failed to add new topic - topic name not found in the TopicMediator enricher set." );
        //}
      }
      return false;
    }


    /// <summary>
    /// Clear out a topic togther with all it's links
    /// </summary>
    internal bool RemoveLinkedTopic(string topicName_, bool usingPublishedTopicName_)
    {
      #region check parameters

      if (topicName_ == null || topicName_.Trim().Length == 0)
      {
        //if (Log.IsLogLevelEnabled(LogLevel.Error))
        //{
        //  Log.Error.SetLocation(CLASS_NAME, "CreateLinkedTopic").Write("Failed to add new topic - topic name was empty." );
        //}
        return false;
      }

      #endregion

      #region rationalise the topicName into the topicHandlerName

      if (usingPublishedTopicName_)
      {
        string[] topics = GetLinkedTopics(topicName_);
        if (topics == null || topics.Length == 0)
        {
          return false; // none found
        }
        else
        {
          bool ok = true;
          foreach (string s in topics)
          {
            ok &= RemoveLinkedTopic(s, false);
          }
          return ok;
        }
      }

      #endregion

      LinkedTopic lTopic = (LinkedTopic)this.m_linkableTopics[topicName_];
      if (lTopic != null)
      {
        lTopic.Clear();
        m_linkableTopics.Remove(topicName_);
        return true;
      }
      // log error
      //if (Log.IsLogLevelEnabled(LogLevel.Error))
      //{
      //  Log.Error.SetLocation(CLASS_NAME, "RemoveLinkedTopic").WriteFormat("Failed to remove existing linked topic - no topic could be found. {0}", topicName_);
      //}
      return false;
    }

    /// <summary>
    /// Add a publisher to an existing topic.
    /// </summary>
    internal bool AddTopicPublisher(IColleague publisher_, string topicName_, bool usingPublishedTopicName_)
    {
      #region check parameters

      if (topicName_ == null || topicName_.Trim().Length == 0)
      {
        //if (Log.IsLogLevelEnabled(LogLevel.Error))
        //{
        //  Log.Error.SetLocation(CLASS_NAME, "AddTopicPublisher").Write("Provided topic name was empty." );
        //}
        return false;
      }

      if (publisher_ == null || publisher_.ColleagueId == Guid.Empty)
      {
        //if (Log.IsLogLevelEnabled(LogLevel.Error))
        //{
        //  Log.Error.SetLocation(CLASS_NAME, "AddTopicPublisher").Write("Provided colleague was empty" );
        //}
        return false;
      }

      #endregion

      #region rationalise the topicName into the topicHandlerName

      if (usingPublishedTopicName_)
      {
        string[] topics = GetLinkedTopics(topicName_);
        if (topics == null || topics.Length == 0)
        {
          return false; // no linked topic handler found for this published name
        }
        else
        {
          bool ok = true;
          foreach (string s in topics)
          {
            ok &= AddTopicPublisher(publisher_, s, false);
          }
          return ok;
        }
      }

      #endregion

      if (this.m_linkableTopics.ContainsKey(topicName_))
      {
        LinkedTopic topic = (LinkedTopic)m_linkableTopics[topicName_];
        topic.AddNewLinkedSet(publisher_);
        return true;
      }

      // else - log error
      //if (Log.IsLogLevelEnabled(LogLevel.Error))
      //{
      //  Log.Error.SetLocation(CLASS_NAME, "AddTopicPublisher").WriteFormat("Failed to add new topic publisher - no topic could be found. {0}", topicName_);
      //}
      return false;
    }

    /// <summary>
    /// Remove the given publisher for the given topic handler name
    /// </summary>
    internal bool RemoveTopicPublisher(IColleague publisher_, string topicName_, bool usingPublishedTopicName_)
    {
      #region check parameters

      if (topicName_ == null || topicName_.Trim().Length == 0)
      {
        //if (Log.IsLogLevelEnabled(LogLevel.Error))
        //{
        //  Log.Error.SetLocation(CLASS_NAME, "RemoveTopicPublisher").Write("Provided topic name was empty." );
        //}
        return false;
      }

      if (publisher_ == null || publisher_.ColleagueId == Guid.Empty)
      {
        //if (Log.IsLogLevelEnabled(LogLevel.Error))
        //{
        //  Log.Error.SetLocation(CLASS_NAME, "RemoveTopicPublisher").Write("Provided colleague was empty" );
        //}
        return false;
      }

      #endregion

      #region rationalise the topicName into the topicHandlerName

      if (usingPublishedTopicName_)
      {
        string[] topics = GetLinkedTopics(topicName_);
        if (topics == null || topics.Length == 0)
        {
          return false; // none found
        }
        else
        {
          bool ok = true;
          foreach (string s in topics)
          {
            ok &= RemoveTopicPublisher(publisher_, s, false);
          }
          return ok;
        }
      }

      #endregion

      if (this.m_linkableTopics.ContainsKey(topicName_))
      {
        LinkedTopic topic = (LinkedTopic)m_linkableTopics[topicName_];
        topic.RemovePublisher(publisher_);
        return true;
      }
      // log error
      //if (Log.IsLogLevelEnabled(LogLevel.Error))
      //{
      //  Log.Error.SetLocation(CLASS_NAME, "RemoveTopicPublisher").WriteFormat("Failed to remove new topic publisher - not topic could be found. {0}", topicName_);
      //}
      return false;
    }

    #region public methods required for GUI stuff

    /// <summary>
    /// Called by the window manager when the given form becomes active
    /// </summary>
    internal void SetActive(IColleague colleague_, bool activate_)
    {
      // if this colleague relates to a LinkControl, assign the form to the linkControl
      if (!LinkControl.InstanceExists(colleague_))
      {
        // this link control has not been linked, or the colleague is null, ie the form 
        // itself is not linked
        this.ActiveColleague = null;
        return;
      }

      LinkControl activeControl = LinkControl.GetInstance(colleague_);

      if (activate_)
      {
        // we can link to this form.
        this.ActiveColleague = colleague_;
        activeControl.SelectLinkedSetForGui();
      }
      else if (!activate_)
      {
        if (colleague_ == this.ActiveColleague)
        {
          this.ActiveColleague = null;
        }
        activeControl.ResetLinkedSetForGui();
      }
    }

    /// <summary>
    /// Add any link conditions here: currently covers linking
    /// on the same tab, but could cover a multitude of things.
    /// </summary>
    internal bool AllowedToSubscribe(LinkedSet set_, LinkControl proposedLinkControl_)
    {
      if (set_.DefaultLinking != DefaultLinkingEnum.SameForm && set_.LinkedTopic.RestrictLinkingToOneTab)
      {
        // if we are restricting linking to the same tab, check it.
        return ((ILinkManager)this).AllowedToSubscribe(set_.Publisher.Colleague, proposedLinkControl_.Colleague);
      }
      return true;
    }

    /// <summary>
    /// Get the window handle of the form which has the current Active (yellow) control.
    /// Note that if there is no active control or if the active control doesn't have a
    /// form (ie, userlinking is disabled) then it returns nothing
    /// </summary>
    internal System.IntPtr GetActiveWindowHandle(bool isLaunched_)
    {
      //this method is called to get around a focus problem where the source window
      //is waiting for an activate message
      LinkControl linkControl = LinkControl.GetInstance(ActiveColleague);

      if (linkControl != null && linkControl.ClientForm != null)
      {
        if (isLaunched_ == true)
        {
          return linkControl.ClientForm.Handle;
        }
        else if (linkControl.IsFloating)
        {
          return linkControl.ClientForm.Parent.Handle;
        }
      }
      return System.IntPtr.Zero;
    }

    #endregion

    #endregion internal Methods

    #region ILinkManager implementation

    /// <summary>
    /// Gets the currently active LinkControl
    /// </summary>
    ILinkControl ILinkManager.ActiveControl
    {
      get
      {
        if (this.ActiveColleague == null)
        {
          return null;
        }
        return LinkControl.GetInstance(this.ActiveColleague);
      }
    }

    /// <summary>
    /// Switch off or on all linking in this application.
    /// Can also be set in Config using the Linking/EnableWindowLinking element
    /// </summary>
    bool ILinkManager.Enabled
    {
      get { return this.Enabled; }
      set { this.Enabled = value; }
    }

    /// <summary>
    /// Clear out a topic together with all it's links. NOte that this will affect all the 
    /// topic handlers raised by the given published topic name, however if a topic is raised
    /// by other published topics, it will not affect that linkage.
    /// </summary>
    bool ILinkManager.RemoveLinkedTopic(string topicName_)
    {
      return this.RemoveLinkedTopic(topicName_, true);
    }

    /// <summary>
    /// Clear out a topic together with all it's links. NOte that this will affect all the 
    /// topic handlers raised by the given published topic name, however if a topic is raised
    /// by other published topics, it will not affect that linkage.
    /// </summary>
    bool ILinkManager.RemoveLinkedTopic(string[] topicNames_)
    {
      if (topicNames_ != null)
      {
        bool ok = true;
        foreach (string topicName in topicNames_)
        {
          ok &= this.RemoveLinkedTopic(topicName, true);
        }
        return ok;
      }
      return false;
    }

    /// <summary>
    /// Each published topic may be used in config several times, and some of those
    /// topics may be linked. This method gives a list of currently linked topics that
    /// will be triggered by the given name.
    /// </summary>
    /// <param name="_publishedTopicName"></param>
    /// <returns></returns>
    string[] ILinkManager.GetLinkedTopics(string _publishedTopicName)
    {
      return this.GetLinkedTopics(_publishedTopicName);
    }


    /// <summary>
    /// Link the given subscriber to the given publisher on the given topic.
    /// 
    /// Note that this is the published topic name, which may, in config, 
    /// actually be repeated in multiple topic handlers. 
    /// 
    /// So this particular method will create a new topic dedicated to this subscriber 
    /// and this publisher, and then link them - no other colleagues will be affected
    /// by this linkage.
    /// </summary>
    /// <returns>
    /// true if the topic link is created, false otherwise.
    /// </returns>
    bool ILinkManager.AddLinkSubscription(IColleague subscriber_, string topicName_, IColleague publisher_)
    {
      return this.AddLinkSubscription(subscriber_, topicName_, publisher_);
    }

    /// <summary>
    /// Creates a linked topic for the given topicName: 
    /// it will use any existing topic handler topics from TopicMediator, but If there are none 
    /// existing, it will create one a new one - the topic handler name will then be a guid, but you 
    /// will never see it, so it shouldn't matter.
    /// 
    /// Note that if a published topic refers to <b>more</b> than one topic then all the 
    /// handlers for that topic will become linked as per the description and persist as per
    /// the persist flag. 
    /// 
    /// This function returns true if the topic was successfully created. False otherwise.
    /// 
    /// Note that you can check how many topics you will be linking on with the GetLinkedTopics
    /// method.
    /// </summary>
    bool ILinkManager.CreateLinkedTopic(string topicName_, string description_, bool persist_)
    {
      return CreateLinkedTopic(topicName_, description_, LinkableStateEnum.UserLinkable, DefaultLinkingEnum.All, persist_, true);
    }

    /// <summary>
    /// Remove the given publisher for the given published topic. If multiple topic handlers
    /// are triggered by this published topic name, all will lose this colleague.
    /// </summary>
    bool ILinkManager.RemoveTopicPublisher(IColleague publisher_, string topicName_)
    {
      return this.RemoveTopicPublisher(publisher_, topicName_, true);
    }

    /// <summary>
    /// Remove the given publisher for the given published topic. If multiple topic handlers
    /// are triggered by this published topic name, all will lose this colleague.
    /// </summary>
    bool ILinkManager.RemoveTopicPublisher(IColleague publisher_, string[] topicNames_)
    {
      if (topicNames_ != null)
      {
        bool ok = true;
        foreach (string topicName in topicNames_)
        {
          ok &= this.RemoveTopicPublisher(publisher_, topicName, true);
        }
        return ok;
      }
      return false;
    }

    /// <summary>
    /// Add a publisher to an existing topic - this will add the given colleague as a publisher
    /// to every topic handler group that is triggered using this publishedTopicName. This step is
    /// an optional part of linking - when a message is published by a named colleague the first time
    /// this step is done anyway - but here you can jump the gun and pre-initialise if required (which
    /// would mean the GUI shows up early).
    /// </summary>
    bool ILinkManager.AddTopicPublisher(IColleague publisher_, string topicName_)
    {
      return this.AddTopicPublisher(publisher_, topicName_, true);
    }

    /// <summary>
    /// Add a publisher to an existing topic - this will add the given colleague as a publisher
    /// to every topic handler group that is triggered using this publishedTopicName. This step is
    /// an optional part of linking - when a message is published by a named colleague the first time
    /// this step is done anyway - but here you can jump the gun and pre-initialise if required (which
    /// would mean the GUI shows up early).
    /// </summary>
    bool ILinkManager.AddTopicPublisher(IColleague publisher_, string[] topicNames_)
    {
      if (topicNames_ != null)
      {
        bool ok = true;
        foreach (string topicName in topicNames_)
        {
          ok &= this.AddTopicPublisher(publisher_, topicName, true);
        }
        return ok;
      }
      return false;
    }

    /// <summary>
    /// Read the config from the TopicMediator config to set up this link manager
    /// </summary>
    void ILinkManager.Initialize(IConcordApplication application_)
    {
      if (m_application == application_) 
        return;

      m_application = application_;

      Configurator config = (Configurator) ConfigurationManager.GetConfig(CONFIG_XPATH_TOPICMEDIATOR_CONFIGNAME);
      XmlNode handlers = null;

      if (config != null)
      {
        // get the list of TopicHandlers where "linkable" is valid
        handlers = config.GetNode(CONFIG_XPATH_TOPICHANLDERS, null);
      }

      if (handlers != null)
      {
        // set up the basics
        if (config.GetNode(CONFIG_XPATH_RESTRICT_LINKING_TO_ONE_PANE, null) != null)
        {
          string restrict = config.GetValue(CONFIG_XPATH_RESTRICT_LINKING_TO_ONE_PANE, null);

          if (!string.IsNullOrEmpty(restrict))
          {
            RestrictLinkingToOneTab = Convert.ToBoolean(restrict);
          }
        }

        if (config.GetNode(CONFIG_XPATH_ENABLED_LINKING, null) != null)
        {
          string enable = config.GetValue(CONFIG_XPATH_ENABLED_LINKING, null);
          if (!string.IsNullOrEmpty(enable))
          {
            Enabled = Convert.ToBoolean(enable);
          }
        }
        else
        {
          Enabled = true;
        }

        System.Xml.XPath.XPathNavigator xnav = handlers.CreateNavigator();
        System.Xml.XPath.XPathNodeIterator iter = xnav.Select(CONFIG_XPATH_TOPICHANLDER_LINKABLEATTRIBUTE);
        while (iter.MoveNext())
        {
          string linkableValue = iter.Current.GetAttribute(CONFIG_ATTRIBUTE_LINKABLESTATE, string.Empty);
          LinkableStateEnum linkState = LinkableStateEnum.None;
          if (linkableValue != null && linkableValue.Trim().Length > 0)
          {
            try
            {
              linkState = (LinkableStateEnum) Enum.Parse(typeof (LinkableStateEnum), linkableValue, false);
            }
            catch (Exception e)
            {
              //if (Log.IsLogLevelEnabled(LogLevel.Error))
              //{
              //  LogLayer.Debug.SetLocation(CLASS_NAME, "LinkManager.Initialize()").
              //    WriteFormat("Failed to parse linkable attribute {0}", e, linkableValue);
              //}
            }
          }

          // no point proceding if the linkState is none
          if (linkState != LinkableStateEnum.None)
          {
            string defaultLinkableValue = iter.Current.GetAttribute(CONFIG_ATTRIBUTE_DEFAULTLINKING, string.Empty);
            DefaultLinkingEnum defState = DefaultLinkingEnum.None;
            if (defaultLinkableValue != null && defaultLinkableValue.Trim().Length > 0)
            {
              try
              {
                defState = (DefaultLinkingEnum) Enum.Parse(typeof (DefaultLinkingEnum), defaultLinkableValue, false);
              }
              catch (Exception e)
              {
                //if (Log.IsLogLevelEnabled(LogLevel.Error))
                //{
                //  LogLayer.Debug.SetLocation(CLASS_NAME, "LinkManager.Initialize()").
                //    WriteFormat("Failed to parse defaultlinking attribute {0}", e, defaultLinkableValue);
                //}
              }
            }

            string restrictedLinkingString = iter.Current.GetAttribute(CONFIG_ATTRIBUTE_RESTRICT_LINKING_TO_ONE_PANE,
                                                                       string.Empty);
            bool restrictLinking = RestrictLinkingToOneTab;
            if (restrictedLinkingString != null && restrictedLinkingString.Trim().Length > 0)
            {
              try
              {
                restrictLinking = bool.Parse(restrictedLinkingString);
              }
              catch (Exception e)
              {
                //if (Log.IsLogLevelEnabled(LogLevel.Error))
                //{
                //  LogLayer.Debug.SetLocation(CLASS_NAME, "LinkManager.Initialize()").
                //    WriteFormat("Failed to parse restrictLinkingToOnePane attribute {0}", e, restrictedLinkingString);
                //}
              }
            }

            string topicName = iter.Current.GetAttribute(CONFIG_ATTRIBUTE_NAME, string.Empty);
            string topicDescription = iter.Current.GetAttribute(CONFIG_ATTRIBUTE_DESCRIPTION, string.Empty);

            string strPersistPreference = iter.Current.GetAttribute(CONFIG_ATTRIBUTE_PERSIST, string.Empty);
            bool persistPreference = (linkState == LinkableStateEnum.UserLinkable);
              // in config assume persistance is desired unless not user linkable

            if (strPersistPreference != null && strPersistPreference.Trim().Length > 0)
            {
              persistPreference = Convert.ToBoolean(strPersistPreference);
            }

            CreateLinkedTopic(topicName, topicDescription, linkState, defState, persistPreference, false,
                              restrictLinking);
          }
        }
      }
      else
      {
        #region Logging

        //if (Log.IsLogLevelEnabled(LogLevel.Info))
        //{
        //  LogLayer.Info.SetLocation(CLASS_NAME, "LinkManager.Initialize()").
        //    WriteFormat("Config file TopicMediator.config does not exist");
        //}

        #endregion Logging

        Enabled = false;
      }
    }


    /// <summary>
    /// Get the specific ILinkControl for the given Colleague.
    /// </summary>
    /// <param name="colleague_"></param>
    ILinkControl ILinkManager.GetLinkControl(IColleague colleague_)
    {
      return LinkControl.GetInstance(colleague_);
    }

    /// <summary>
    /// Indicates if two colleagues are allowed to subscribe to each other.
    /// This is where you would implement the RestrictToSameTab implementation,
    /// or some other restriction.
    /// </summary>
    /// <param name="source_"></param>
    /// <param name="reciever_"></param>
    /// <returns></returns>
    bool ILinkManager.AllowedToSubscribe(IColleague source_, IColleague reciever_)
    {
      if (RestrictLinkingToOneTab)
      {
        // if we are restricting linking to the same tab, check it.
        return m_application.WindowManager.CheckOnSameTab(LinkControl.GetInstance(source_), LinkControl.GetInstance(reciever_));
      }
      return true;
    }

    /// <summary>
    /// strip out all currently registered LinkControls, but leave the topic handlers
    /// in place (the information taken from TopicMediator.config, and the Topics added
    /// programatically).
    /// </summary>
    void ILinkManager.ClearAll()
    {
      LinkControl.ClearAll();

      /* traceyco - 6th November 2006
       * 
       * The above call to ClearAll should clear out each topics list of available subscribers.
       * However, a previous bug resulted in items being leftover in these lists, causing incorrect
       * linking information to be serialized. Consequently, some layout files failed to load. See fix
       * in Perforce changelist 1188749 for more details.
       * 
       * To make sure the available subscribers lists are empty, check each list and if a list is found
       * to contain entries, log an error and clear the list. */
      foreach (LinkedTopic topic in m_linkableTopics.Values)
      {
        if (topic.AvailableSubscribers.Count > 0)
        {
          //if (Log.IsLogLevelEnabled(LogLevel.Error))
          //{
          //  Log.Error.SetLocation(CLASS_NAME, "ClearAll").WriteFormat("{0} topic's available subscribers list is not empty.", topic.TopicHandlerName);
          //}
          //topic.AvailableSubscribers.Clear();
        }
      }
    }

    /// <summary>
    /// Rebuild the linked state for this LinkManager from the given XMLNode
    /// </summary>
    void ILinkManager.LoadState(XmlNode state_, ref bool cancel_, IColleague colleague_)
    {
      LinkControl c = LinkControl.GetInstance(colleague_);
      if (c != null)
      {
        c.LoadState(state_, ref cancel_);
      }
      else
      {
        //TODO log that colleague is null
      }
    }

    /// <summary>
    /// Use the given XMLNOde to store the current linking state for all
    /// LinkedTopics that are marked for persistance.
    /// </summary>
    void ILinkManager.SaveState(XmlNode state_, ref bool cancel_, IColleague colleague_)
    {
      LinkControl c = LinkControl.GetInstance(colleague_);
      c.SaveState(state_, ref cancel_);
    }

    /// <summary>
    /// Save application level information about linking
    /// </summary>
    /// <param name="state_"></param>
    /// <param name="cancel_"></param>
    void ILinkManager.LoadState(XmlNode state_, ref bool cancel_)
    {
      // this could load the link state from an XmlElement - however this particular
      // implementation of link manager doesn't load state in this way.
    }

    /// <summary>
    /// Load application level information about linking
    /// </summary>
    /// <param name="state_"></param>
    /// <param name="cancel_"></param>
    void ILinkManager.SaveState(XmlNode state_, ref bool cancel_)
    {
      // this could save the link state to an XmlElement - however this particular
      // implementation of link manager doesn't save state in this way.
    }

    #endregion

  }
}
