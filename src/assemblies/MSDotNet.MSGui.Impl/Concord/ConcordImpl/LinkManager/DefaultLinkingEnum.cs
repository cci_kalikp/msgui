﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Concord/ConcordImpl/LinkManager/DefaultLinkingEnum.cs#5 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

namespace MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl.LinkManager
{

  /// <summary>
  /// Used to determine how to manage the link state for any subscribers as they are
  /// registered or their linkstate changes.
  /// </summary>
  public enum DefaultLinkingEnum
  {
    /// <summary>
    /// Fully manual - set every subscriber to Unlinked. This differs from disabled
    /// which sets every subscriber to inelligable.
    /// </summary>
    None,
    /// <summary>
    /// Fully manual - Link to every subscriber as it is registered
    /// </summary>
    All,
    /// <summary>
    /// Link to the first subscriber that has not already been linked to another -
    /// this will not link if no subscriber is available.
    /// </summary>
    FirstUnlinked,
    /// <summary>
    /// Move throught list of subscribers sequentially
    /// </summary>
    RoundRobin,
    /// <summary>
    /// when subscribers are linked in this topic, only one will ever be linked, the others will always be unlinked
    /// </summary>
    RadioButton,
    /// <summary>
    /// the message will only be sent to LinkControls that share the same form
    /// </summary>
    SameForm,
    /// <summary>
    /// No linking to any subsribers
    /// </summary>
    Disabled
  }
}
