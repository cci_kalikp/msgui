/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Concord/ConcordImpl/LinkManager/LinkedTopic.cs#7 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.Collections;
using MorganStanley.IED.Concord.Application;
using MorganStanley.IED.Concord.TopicMediator;

namespace MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl.LinkManager
{  
  /// <summary>
  /// Contains the properties and behaviour at the topic level: 
  /// Each topicMediator TOpicHandler has one LinkedTopic (if the
  /// linking state was not None). Each LinkedTopic can have many
  /// linkedSets - one per colleague publishing on that topic.
  /// </summary>
  internal class LinkedTopic 
  {    
    private static readonly string CLASS = "LinkedTopic";

    private readonly string m_topicHandlerName;
    private readonly string m_topicDescription;
    private readonly Hashtable m_linkedSets;
    private readonly Hashtable m_availableSubscribers;

    /// <summary>
    /// Creates an instance of the linkedTopic
    /// </summary>
    internal LinkedTopic( string topicHandlerName_, LinkableStateEnum state_, DefaultLinkingEnum defState_, bool persist_, string topicDescription_, bool restrictLinkingToOnePane_)
    {
      m_topicHandlerName = topicHandlerName_;
      m_topicDescription = topicDescription_;
      LinkableState = state_; // such as UserLinkable, none, etc.
      DefaultLinking = defState_;
      m_linkedSets = new Hashtable();
      m_availableSubscribers = new Hashtable();
      Persist = persist_;
      RestrictLinkingToOneTab = restrictLinkingToOnePane_;
      
      TopicMediator.Current.RegisteredListener += TopicMediator_RegisteredListener;
      TopicMediator.Current.UnregisteredListener += TopicMediator_UnregisteredListener;
    }

    /// <summary>
    /// Indicates whether this LinkedTopic should persist itself
    /// </summary>
    internal bool Persist { get; set; }

    /// <summary>
    /// Gets the topic name for this LinkedTopic: note
    /// this is the same as the TopicHandler name in the 
    /// TopicMediator config
    /// </summary>
    internal string TopicHandlerName
    {
      [System.Diagnostics.DebuggerStepThrough()]
      get
      {
        return m_topicHandlerName;
      }
    }

    /// <summary>
    /// Gets the topic description for this LinkedTopic.
    /// These are the words that will appear in any
    /// context menus.
    /// </summary>
    internal string TopicHandlerDescription
    {
      [System.Diagnostics.DebuggerStepThrough()]
      get
      {
        return m_topicDescription;
      }
    }

    /// <summary>
    /// Gets or sets the LinkableState of this TopicHandler.
    /// This value is indicates whether what level of Linking
    /// this Tpic should attempt.
    /// </summary>
    internal LinkableStateEnum LinkableState { get; set; }

    /// <summary>
    /// The default type of linking for this topic - 
    /// including RoundRobin, RadioButton, None, All
    /// and Disabled.
    /// </summary>
    internal DefaultLinkingEnum DefaultLinking { get; set; }

    /// <summary>
    /// Indicates whether or not this topic is allowed
    /// to link to colleagues across different panes when
    /// using the PainWindowManager. No typo.
    /// </summary>
    internal bool RestrictLinkingToOneTab { get; set; }


    /// <summary>
    /// The full list of available subscribers for this topic
    /// and publisher
    /// </summary>
    internal Hashtable AvailableSubscribers
    {
      [System.Diagnostics.DebuggerStepThrough()]
      get
      {
        return m_availableSubscribers;
      }
    }

    /// <summary>
    /// Add a LinkedSet for this topic
    /// </summary>
    internal void AddNewLinkedSet ( IColleague publisher_ )
    {
      try
      {
        if (!this.m_linkedSets.ContainsKey( publisher_.ColleagueId ))
        {
          LinkedSet ls = new LinkedSet( LinkControl.GetInstance( publisher_ ), this );
          this.m_linkedSets.Add( publisher_.ColleagueId, ls );
        }
        else
        {
          //if (Log.IsLogLevelEnabled(LogLevel.Error))
          //{
          //  Log.Error.SetLocation(CLASS, "AddNewLinkedSet").WriteFormat("Failed to add new linked set - Publisher already registered for this topic. {0}, {1}", this._topicHandlerName, publisher_.ColleagueId);
          //}
        }
      }
      catch(Exception ex_)
      {
        //if (Log.IsLogLevelEnabled(LogLevel.Error))
        //{
        //  Log.Error.SetLocation(CLASS, "AddNewLinkedSet").WriteFormat("Error adding new linked set", ex_);
        //}
      }
    }

    /// <summary>
    /// Ensure that the sender is linked, and then prepare the linked set if
    /// necessary: this means that if the developer has not linked this 
    /// specific colleague explicitly before publishing, that the links will
    /// be created now.
    /// </summary>
    internal void  BeginPublishTopic( IColleague publisher_ )
    {
      if ( !this.m_linkedSets.ContainsKey( publisher_.ColleagueId ) ) 
      {
        this.AddNewLinkedSet( publisher_ );
      }
      LinkedSet lSet = (LinkedSet)m_linkedSets[publisher_.ColleagueId];
      lSet.BeginPublishTopic();
    }

    /// <summary>
    /// Set the linking style for an exising LinkedSet in this topic.
    /// </summary>
    internal void SetLinked( IColleague publisher_, DefaultLinkingEnum style_ )
    {
      if ( this.m_linkedSets.ContainsKey (publisher_.ColleagueId ) )
      {
        LinkedSet lSet = (LinkedSet)m_linkedSets[ publisher_.ColleagueId ];      
        lSet.DefaultLinking = style_;
      }
    }

    /// <summary>
    /// Explicitly add a Subscribed to the specific Publisher's linked set
    /// (and create the LinkedSet if necessary).
    /// </summary>
    internal void AddSubscriptionToPublisherSet( IColleague subscriber_, IColleague publisher_ )
    {
      if ( !this.m_linkedSets.ContainsKey (publisher_.ColleagueId ) )
      {
        AddNewLinkedSet ( publisher_ );
      }
      
      LinkControl subscriber = LinkControl.GetInstance( subscriber_ );
      if ( !this.m_availableSubscribers.ContainsKey( subscriber_.ColleagueId ) )
      {
        AddSubscriber( subscriber );
      }
      LinkedSet lSet = (LinkedSet)m_linkedSets[ publisher_.ColleagueId ];      
      subscriber.SetLinkState( lSet, LinkStateEnum.Linked );
    }

    /// <summary>
    /// Remove the specified colleague from this topic
    /// </summary>
    internal void RemovePublisher ( IColleague publisher_ )
    {
      LinkedSet toRemove = (LinkedSet)m_linkedSets[ publisher_.ColleagueId ];
      if (toRemove != null)
      {
        // clear out the LinkedSet and unsubscribe to TM.
        toRemove.Clear();
      }
      m_linkedSets.Remove( publisher_.ColleagueId );
    }

    /// <summary>
    /// Add a LinkControl to the available subscribers
    /// to this topic - using the defaultLinkingEnum to 
    /// decide whether or not to link it.
    /// </summary>
    internal void AddSubscriber( LinkControl subscriber_ )
    {      
      LinkControl control = m_availableSubscribers[subscriber_.Colleague.ColleagueId] as LinkControl;
      if(control == null || control.ClientForm != subscriber_.ClientForm)
      {
        if(control != null)
        {
          //if we get here, the control is not null, but its assocatied client form is not the same for the exitsting client
          //form.  We need to remove it so that when a layout with the
          //same guid is reloaded, that correct subscriber is added.
          this.m_availableSubscribers.Remove(subscriber_.Colleague.ColleagueId);

          foreach(LinkedSet ls in this.m_linkedSets.Values)
          {
            ls.RemoveSubscriber(subscriber_.Colleague);
          }
        }

        this.m_availableSubscribers.Add ( subscriber_.Colleague.ColleagueId, subscriber_ );
        foreach(LinkedSet ls in this.m_linkedSets.Values)
        {
          ls.AddSubscriber( subscriber_ );
        }

        // Add this topic to the subscriber's list of available topics.
        subscriber_.AddAvailableTopic(this);
      }
    }

    /// <summary>
    /// Remove the specified colleague from this topic and 
    /// all LinkedSets that come from this topic
    /// </summary>
    internal void RemoveSubscriber ( IColleague subscriber_ )
    {
      if ( LinkControl.InstanceExists ( subscriber_ ) )
      {
        if (m_availableSubscribers.ContainsKey( subscriber_.ColleagueId ))
        {
          foreach(LinkedSet lSet in this.m_linkedSets.Values)
          {
            lSet.RemoveSubscriber( subscriber_ );    
          }
          m_availableSubscribers.Remove( subscriber_.ColleagueId );
        }
      }
    }    

    /// <summary>
    /// Completely empty this LinkedTopic of all LinkedSets.
    /// </summary>
    internal void Clear()
    {
      TopicMediator.Current.RegisteredListener -= TopicMediator_RegisteredListener;
      TopicMediator.Current.UnregisteredListener -= TopicMediator_UnregisteredListener;
      // and clear out the hashtables
      ArrayList al = new ArrayList();
      foreach (LinkedSet ls in m_linkedSets.Values )
      {
        al.Add(ls);
      }
      for(int i=al.Count-1;i>=0;i--)
      {
        this.RemovePublisher( ((LinkedSet)al[i]).Publisher.Colleague );
      }
      al.Clear();
    }

    /// <summary>
    /// Listen to TopicMediator for any new registered subscribers.
    /// </summary>
    private void TopicMediator_RegisteredListener(object sender_, RegisteredListenerEventArgs e_)
    {
      if (e_.TopicName == this.TopicHandlerName)
      {
        this.AddSubscriber( LinkControl.GetInstance( e_.Listener ) );
      }
    }

    /// <summary>
    /// Remove the given listener from all our linked sets and available subscribers
    /// </summary>
    private void TopicMediator_UnregisteredListener(object sender, RegisteredListenerEventArgs e)
    {
      if (e.TopicName == this.TopicHandlerName)
      {
        // you can't immediately remove this subscriber in its entirity, because it may 
        // still be subscribed on another topic
        string[] topics = TopicMediator.Current.GetSubscribedTopics( e.Listener );
        bool remove = true;
        foreach(string topic in topics)
        {
          // but the topics that this listener subscribes to are not the same as the 
          // topic handler that this responds to - so now we have to check them
          string[] handlers = TopicMediator.Current.GetTopicHandlerNames( topic );
          foreach( string handler in handlers )
          {
            if ( handler.Equals( this.TopicHandlerName ) )
            {
              // finally - found another subscribed topic that subscribes to this 
              // handler.
              remove = false;
              break;
            }
          }
          if (!remove)
          {
            // no need to go on looking.
            break;
          }
        }
        if (remove)
        {
          this.RemoveSubscriber( e.Listener );
        }
      }
    }
  }
}
