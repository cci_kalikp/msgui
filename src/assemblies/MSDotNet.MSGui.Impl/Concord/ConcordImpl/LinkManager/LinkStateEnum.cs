﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Concord/ConcordImpl/LinkManager/LinkStateEnum.cs#5 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

namespace MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl.LinkManager
{
  /// <summary>
  /// The available ways in which a linkControl can be linked to
  /// a linkedSet. Each of the enums has an assocaited image
  /// </summary>
  public enum LinkStateEnum
  {
    /// <summary>
    /// the basic green square, decorated with index if necessary.
    /// Indicates that messages from the LinkedSet will get to this
    /// LinkControl
    /// </summary>
    Linked = 0,
    /// <summary>
    /// the red square. No messages will get to this LinkControl
    /// </summary>
    Unlinked = 1,
    /// <summary>
    /// the yellow square (decorated with red cross if necessary).
    /// This LinkControl publishes messages for the LinkedSet
    /// </summary>
    Source = 2,
    /// <summary>
    /// no assocaited image - this LinkControl is not related to the
    /// LinkedSet. This can happen if the LinkedSet is currently 
    /// disabled, for instance.
    /// </summary>
    Inelligible = 3
  }
}
