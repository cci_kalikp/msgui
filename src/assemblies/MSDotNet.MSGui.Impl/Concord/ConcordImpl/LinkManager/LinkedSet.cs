/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Concord/ConcordImpl/LinkManager/LinkedSet.cs#7 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System;
using System.Xml;
using System.Collections;

using MorganStanley.IED.Concord.TopicMediator;
using MorganStanley.IED.Concord.Application;

namespace MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl.LinkManager
{
  /// <summary>
  /// Class to manage which colleagues one specific publisher 
  /// issues topic mediator messages to. A linkedSet is like 
  /// an instance of a LinkedTopic, and does the work of 
  /// intercepting Pre-Publish messages from TopicMediator.
  /// </summary>
  internal class LinkedSet : ICollection
  {
    #region Declarations
    private const string CLASS_NAME = "LinkedSet";

    private readonly LinkControl m_publisher;  
    private readonly ArrayList m_subscribers; // an ordered list - needed for the round robin implementation
    private int m_roundRobinPointer; // a pointer to the current/next control 
    private LinkControl m_lastRecipient;
    private readonly LinkedTopic m_topic;
    private DefaultLinkingEnum m_defaultLinking;
    private bool m_isEnabled;
    private Guid[] m_persistedOrder;
    
    /// <summary>
    /// The DefaultLinkingCnahged event is subscribed to
    /// by the interested LinkControls, allowing them to update their
    /// LinkImages if required.
    /// </summary>
    internal event EventHandler DefaultLinkingChanged;

    /// <summary>
    /// The RoundRobinStateChanged event is subscribed to
    /// by the interested LinkControls, allowing them to update their
    /// LinkImages if required.
    /// </summary>
    internal event EventHandler RoundRobinStateChanged;

    #endregion Declarations

    #region constructors
    
    /// <summary>
    /// Create an instance of linked set
    /// </summary>
    /// <param name="publisher_"></param>
    /// <param name="topic_"></param>
    internal LinkedSet( LinkControl publisher_, LinkedTopic topic_ )
    {
      m_publisher = publisher_;
      m_topic = topic_;
      m_subscribers = new ArrayList();
      m_publisher.AddLinkedSetPublisher( this );
      m_defaultLinking = m_topic.DefaultLinking;
      m_isEnabled = ( m_defaultLinking != DefaultLinkingEnum.Disabled );
      m_persistedOrder = null;
      m_roundRobinPointer = -1;
      m_lastRecipient = null;
      
      // use the internal method to add any existing subscribers
      foreach( LinkControl c in m_topic.AvailableSubscribers.Values )
      {
        this.AddSubscriber( c );
      }
      this.OnDefaultLinkingChanged( EventArgs.Empty ); 

      // attach to the main event we need to implement linking
      TopicMediator.Current.PrePublish += new PrePublishEventHandler( this.TopicMediator_PrePublish );      
    }

    #endregion

    #region internal mehtods

    /// <summary>
    /// Get the index of the given colleague
    /// </summary>
    internal int IndexOf( IColleague colleague_ )
    {
      return IndexOf( colleague_.ColleagueId );
    }    

    /// <summary>
    /// Get the index of the given colleagueID
    /// </summary>
    internal int IndexOf( Guid colleagueID_ )
    {
      for( int i = 0; i < this.m_subscribers.Count; i++ ) 
      {
        if ( this[i].Colleague.ColleagueId == colleagueID_ )
        {
          return i;
        }
      }
      return -1;
    }    


    /// <summary>
    /// Change the order of subscribers by promoting the given one by places_.
    /// Note that the RoundRobin implies that moving the first one up 
    /// will put it at the end. Moving the last one down (up = false) 
    /// will put it at the beginning.
    /// </summary>
    internal void MoveSubscriber( IColleague subscriber_, bool up_ )
    {
      // so we want to swap the given subscriber with the subscriber
      // either the next or the previous eligable round robing subscriber
      LinkControl toMove = this[subscriber_];
      int oldIndex = this.GetRoundRobinIndex( toMove );
      if ( oldIndex == -1 )
      {
        // can't move it - it's not part of the round robin
        return;
      }
      if ( up_ ) // move the index down one - ie up the queue
      {
        oldIndex--;
      }
      else // move the index up one - ie down the queue
      {
        oldIndex++;
      }
      // note that GetSubscriber handles out of range indexes
      LinkControl replaced = GetSubscriber( oldIndex );
      if (replaced == toMove)
      {
        // if the two are same then there's only one control in the loop
        // and moving is meaningless.
        return;
      }
      // otherwise we can swap their places 
      int toMoveIndex = m_subscribers.IndexOf( toMove ); 
      int toReplaceIndex = m_subscribers.IndexOf( replaced );

      m_subscribers[toReplaceIndex] = null;
      m_subscribers[toMoveIndex] = replaced;
      m_subscribers[toReplaceIndex] = toMove;
      OnRoundRobinStateChanged( EventArgs.Empty );
    }
    
    /// <summary>
    /// Indicates that a particaular colleague can be found
    /// in the subscribers collection
    /// </summary>
    /// <param name="collegue_"></param>
    /// <returns></returns>
    internal bool ContainsColleague( IColleague collegue_ )
    {
      if ( this.IndexOf( collegue_ ) == -1)
      {
        return false;
      }
      return true;
    }

    /// <summary>
    /// Remove all the subscribers from this LinkedSet, and unhook the 
    /// event handler to topicmediator
    /// </summary>
    internal void Clear()
    {
      for(int i=this.Count - 1; i>=0; i--)
      {
        LinkControl control = this[i];
        m_subscribers.RemoveAt( i );
        control.RemoveLinkedSetSubscriber( this );
      }
      TopicMediator.Current.PrePublish -= new PrePublishEventHandler( this.TopicMediator_PrePublish );
      Publisher.RemoveLinkedSetPublisher( this );
    }

    /// <summary>
    /// Add a LinkControl to this topic based on serialized info.
    /// </summary>
    /// <param name="subscriber_"></param>
    /// <param name="state_"></param>
    internal void AddSubscriber( LinkControl subscriber_, XmlNode state_ )
    {
      if ( subscriber_ != this.Publisher )
      {
        if ( !m_subscribers.Contains( subscriber_ ) )
        {
          this.m_subscribers.Add( subscriber_ );
        }
        XmlAttribute attr = state_.Attributes["linkstate"];
        LinkStateEnum state = (LinkStateEnum)Enum.Parse( typeof (LinkStateEnum), attr.Value, true );
        // ensure that the order of _subscribers is correct
        int index = 0;
        for(int i = 0; i<m_persistedOrder.Length; i++)
        {
          int k = this.IndexOf( m_persistedOrder[i] );
          if (k > -1)
          {
            if (k != index)
            {
              object from = m_subscribers[k];
              m_subscribers.RemoveAt( k );
              if(m_subscribers.Count > index)
              {
                m_subscribers.Insert(index, from);
              }
              else
              {
                m_subscribers.Add(from);
              }
            }
            index ++;
          }
        }
        subscriber_.AddLinkedSetSubscriber( this, state );
      }
    }

    /// <summary>
    /// Add an LinkCOntrol to this topic
    /// </summary>
    /// <param name="subscriber_"></param>
    internal void AddSubscriber( LinkControl subscriber_ )
    {
      if ( subscriber_ == this.Publisher && DefaultLinking != DefaultLinkingEnum.SameForm)
      {
        // its the publisher, return unless this is Same Form
        return;
      }
      else if ( m_subscribers.Contains( subscriber_ ) && subscriber_.GetLinkState( this ) != LinkStateEnum.Inelligible )
      {
        // it's already been added and is not inelligable
        return;
      }
      else if ( !m_subscribers.Contains( subscriber_ ) )
      {
        // not added yet, so add it to internal list
        this.m_subscribers.Add ( subscriber_ );
      }

      //linking should be restricted to a single tab or screen with some window managers
      //this method returns true if linking is allowed between the two ids
      if (!LinkManager.Current.AllowedToSubscribe( this, subscriber_ ))
      {
        subscriber_.AddLinkedSetSubscriber( this, LinkStateEnum.Inelligible );
      }
      else
      {
        // decide on the initial state for this LinkCOntrol
        bool link = false;
        switch( DefaultLinking )
        {
          case DefaultLinkingEnum.Disabled:
          case DefaultLinkingEnum.None:
            link = false;
            break;
          case DefaultLinkingEnum.All:
          case DefaultLinkingEnum.RoundRobin:
            link = true;
            break;
          case DefaultLinkingEnum.FirstUnlinked:
          case DefaultLinkingEnum.RadioButton:
            // if this control is not linked to this topic, and no
            // other subscribers are either, then link it
            if ( ! subscriber_.LinkedToTopic( this.LinkedTopic ) )
            {
              link = true;
              foreach( LinkControl control in this)
              {
                if ( control.GetLinkState( this ) == LinkStateEnum.Linked )
                {
                  link = false;
                  break;
                }
              }
            }
            break;
          case DefaultLinkingEnum.SameForm:
            //only link these topics if the subscriber and the publisher are on the same form.
            link = false;
            if(this.Publisher.ClientForm != null && subscriber_.ClientForm != null
              && !this.Publisher.ClientForm.IsDisposed && !subscriber_.ClientForm.IsDisposed
              && this.Publisher.ClientForm.Handle == subscriber_.ClientForm.Handle)
            {
              link = true;
            }
            break;
          default:
            // not recognised: need to implement the behaviour
            //if (Log.IsLogLevelEnabled(LogLevel.Error))
            //{
            //  Log.Error.SetLocation(CLASS_NAME, "AddSubscriber").Write("Failed to understand the DefaultLinking case " + DefaultLinking.ToString() + ". You must implement it.");
            //}
            break;
        }
        // finally do whatever we've decided to do.
        subscriber_.AddLinkedSetSubscriber(this, link ? LinkStateEnum.Linked : LinkStateEnum.Unlinked);
      }
    }

    /// <summary>
    /// Remove the specified colleague from this topic
    /// </summary>
    /// <param name="subscriber_"></param>
    internal void RemoveSubscriber ( IColleague subscriber_ )
    {
      LinkControl control = this[ subscriber_ ];
      if (control != null)
      {
        m_subscribers.Remove( control );
        control.RemoveLinkedSetSubscriber( this );
      }     
    }

    /// <summary>
    /// Get the round robin index of the given linkCOntrol.
    /// Note that this is not its position in the subscribers 
    /// array as not all linkControls are Linked.
    /// </summary>
    internal int GetRoundRobinIndex( LinkControl linkControl_ )
    {
      if (IsRoundRobin)
      {
        if (linkControl_ == null)
        {
          return -1;
        }
        int index = 0;
        for( int i=0;i<this.m_subscribers.Count;i++)
        {
          // not all colleagues are registered to receive messages
          if ( this[i].GetLinkState( this ) == LinkStateEnum.Linked )
          {
            if ( this[i].Colleague.ColleagueId == linkControl_.Colleague.ColleagueId )
            {
              return index;
            }
            index ++;
          }
        }
      }
      return -1;
    }

    /// <summary>
    /// Indicates which LinkCOntrol was the last one to recieve the round
    /// robin message from this LinkedSet
    /// </summary>
    /// <param name="linkControl_"></param>
    /// <returns></returns>
    internal bool IsPointedByRoundRobin( LinkControl linkControl_ )
    {
      return ( m_lastRecipient == linkControl_ );       
    }

    /// <summary>
    /// This is called by LinkManager after the TopicMediator raises the 
    /// BeginPublishTopic which it sends once before sending a batch of 
    /// messages from a publisher to any of its subscribers for any TopicHandler
    /// </summary>
    internal void BeginPublishTopic()
    {
      if (IsRoundRobin)
      {
        PopNextRoundRobinControl();
      } 
    }

    #endregion

    #region private helper methods

    /// <summary>
    /// Get the linkControl at the given roundRobinIndex - this
    /// is not equivalent to the this[i] as not all entries
    /// are linked and so are discounted.
    /// Also note that a negative roundRobinIndex is equivalent to the
    /// counting from the end, and roundRobinIndex over the end is
    /// equivalent to going round again.
    /// </summary>
    private LinkControl GetSubscriber( int roundRobinIndex )
    {
      if (IsRoundRobin)
      {
        int index = -1;
        for( int i=0;i<this.m_subscribers.Count;i++)
        {
          // not all colleagues are registered to receive messages
          if ( this[i].GetLinkState( this ) == LinkStateEnum.Linked )
          {
            // this one is valid: so if it's the first then 
            // we want the index to be incremented to 0
            index ++;
            if (index == roundRobinIndex)
            {
              // this is the index we're looking for:
              return this[i];
            }
          }
        }
        // We failed to find any LinkControl at the given index,
        // but check that there was at least one elegable control
        if (index >= 0)
        {
          // roundRobinIndex must have been out of bounds
          if (roundRobinIndex < 0)
          {
            // we only know the bounds at this stage, but now we
            // do we can count from the end
            while (roundRobinIndex < 0)
            {
              roundRobinIndex += (index + 1);
            }
          }
          else 
          {
            // count from the beginning again - roundRobinIndex 
            // was over the available count
            while (roundRobinIndex > index)
            {
              roundRobinIndex -= (index + 1);
            }
          }
          return GetSubscriber( roundRobinIndex );
        }
      }
      // no entries at all, or not RoundRobin, so return nothing
      return null;
    }

    /// <summary>
    /// Move the interenal RoundRobinPointer to the next
    /// available Listener.
    /// </summary>
    private void PopNextRoundRobinControl()
    {
      if (m_subscribers.Count > 0)
      {
        int i = 0;
        while(i < 2 )
        {
          // first increment the pointer, so we've moved on from the 
          // previous subscriber (or up from -1 in the pre-start case)
          RoundRobinPointer++;
          if ( RoundRobinPointer >= m_subscribers.Count)
          {
            // bring pointer back to the start again
            RoundRobinPointer = 0;
            i++;
          }
          LinkStateEnum state = this[ RoundRobinPointer ].GetLinkState( this );
          if ( state == LinkStateEnum.Linked )
          {
            return;
          }
        }
      }
      // if we get here then there are no eligible ones: i incremented twice
      // which ensures we looped at least one complete cycle. In which case it
      // doesn't matter where the pointer is.
    }

    #endregion

    #region properties

    /// <summary>
    /// Indexer to get a linked listener from this collection.
    /// Returns null if the colleague is not found
    /// </summary>
    internal LinkControl this[ IColleague colleague_ ]
    {
      [System.Diagnostics.DebuggerStepThrough()]
      get
      {
        int index = this.IndexOf(colleague_);
        if (index != -1)
        {
          return this[index];
        }
        return null;
      }
    }    

    /// <summary>
    /// Indexer to get a linked listener from this collection.
    /// </summary>
    internal LinkControl this[ int index ]
    {
      [System.Diagnostics.DebuggerStepThrough()]
      get
      {
        return (LinkControl)this.m_subscribers[index];
      }
    }

    /// <summary>
    /// Get or set the current Default Linking for this LinkedSet.
    /// Initially this will be the same as the parent's LinkedTopic
    /// setting
    /// </summary>
    internal DefaultLinkingEnum DefaultLinking
    {
      get
      {
        if (m_isEnabled)
        {
          return m_defaultLinking;
        }
        else
        {
          return DefaultLinkingEnum.Disabled;
        }
      }
      set
      {
        if ( DefaultLinking != value )
        {
          if (value == DefaultLinkingEnum.Disabled)
          {
            // simply set the _isEnabled flag- this allows us to 
            // remember the previous value.
            m_isEnabled = false;
          }
          else
          {
            // in all other cases, we need to set the _isEnabled flag to true
            m_isEnabled = true;
            // and then to set the _defaultLinking variable
            m_defaultLinking = value;
            if (m_defaultLinking == DefaultLinkingEnum.Disabled)
            {
              m_defaultLinking = m_topic.DefaultLinking;
            }
          }

          if (DefaultLinking == DefaultLinkingEnum.RadioButton)
          {
            // need to enable only one of my subscribers
            bool oneEnabled = false;
            foreach(LinkControl lc in this)
            {
              if (lc.GetLinkState(this) == LinkStateEnum.Linked)
              {
                if (oneEnabled)
                {
                  lc.SetLinkState(this, LinkStateEnum.Unlinked);
                }
                oneEnabled = true;
              }
            }
          }
          // notify any interested parties
          OnDefaultLinkingChanged( EventArgs.Empty );
        }
      }
    }

    /// <summary>
    /// The LinkedTopic that this LinkedSet is based on.
    /// </summary>
    internal LinkedTopic LinkedTopic
    {
      [System.Diagnostics.DebuggerStepThrough]
      get
      {
        return this.m_topic;
      }
    }

    /// <summary>
    /// Convenience property to check the DefaultLinking property 
    /// of this class.
    /// </summary>
    internal bool IsRoundRobin
    {
      get
      {
        return DefaultLinking == DefaultLinkingEnum.RoundRobin;
      }
    }

    /// <summary>
    /// internal property accessor for the current
    /// pointer to the round robin
    /// </summary>
    protected int RoundRobinPointer
    {
      get
      {
        return m_roundRobinPointer;
      }
      set
      {
        m_roundRobinPointer = value;
      }
    }

    /// <summary>
    /// Get the single publisher that will be generating the messages
    /// </summary>
    internal LinkControl Publisher
    {
      get
      {
        return m_publisher;
      }
    }

    internal void Deserialize( XmlNode state_ )
    {
      XmlAttribute attr = state_.Attributes[ "subscriberorder" ];
      string[] colleagues = attr.Value.Split(';');
      // note that the last one is the empty, as the end of the persisted order is ";"
      m_persistedOrder = new Guid[ colleagues.Length - 1 ];
      for(int i=0;i<m_persistedOrder.Length; i++)
      {
        m_persistedOrder[i] = new Guid( colleagues[i] );
      }

      
      attr = state_.Attributes[ "defaultlinking" ];
      try
      {
        this.m_defaultLinking = (DefaultLinkingEnum)Enum.Parse( typeof(DefaultLinkingEnum), attr.Value, true );
      }
      catch (Exception ex)
      {
        //if (Logging.Log.IsLogLevelEnabled( Logging.LogLevel.Error ) )
        //{
        //  LogLayer.Error.SetLocation("LinkedSet", "Deserialize" ).Write( "Failed to parse the default linking properly", ex );
        //}
      }

      attr = state_.Attributes[ "enabled" ];
      try
      {
        this.m_isEnabled = Boolean.Parse( attr.Value );
      }
      catch (Exception ex)
      {
        //if (Logging.Log.IsLogLevelEnabled( Logging.LogLevel.Error ) )
        //{
        //  LogLayer.Error.SetLocation("LinkedSet", "Deserialize" ).Write( "Failed to parse the isEnabled boolean properly", ex );
        //}
      }

      attr = state_.Attributes["roundrobinpointer"];
      try
      {
        this.m_roundRobinPointer = int.Parse( attr.Value );
      }
      catch (Exception ex)
      {
        //if (Logging.Log.IsLogLevelEnabled( Logging.LogLevel.Error ) )
        //{
        //  LogLayer.Error.SetLocation("LinkedSet", "Deserialize" ).Write( "Failed to parse the default linking properly", ex );
        //}
      }

      foreach(XmlNode subscrNode in state_.ChildNodes)
      {
        // now go through the nodes. If the colleague doesn't 
        // currently exist, then LinkControl will keep a list of them 
        // and whenever a new LinkControl is added, we can link it 
        // as required.

        attr = subscrNode.Attributes["colleagueid"];
        Guid colleagueID = new Guid( attr.Value );
        LinkControl c = LinkControl.GetInstanceForGuid( colleagueID );
        
        if (c == null)
        {
          LinkControl.AddPendingColleague( colleagueID, this, subscrNode );
        }
        else
        {
          this.AddSubscriber( c, subscrNode );
        }
      }     
    }

    internal void Serialize( XmlNode state_ )
    {
      if (this.LinkedTopic.Persist)
      {
        // attempt to add this linkedSet to the state_ node
        XmlNode linkedSetNode = state_.OwnerDocument.CreateElement( "LinkedSet" );
        XmlAttribute attr = state_.OwnerDocument.CreateAttribute("name");
        attr.Value = this.LinkedTopic.TopicHandlerName;
        linkedSetNode.Attributes.Append( attr );

        attr = state_.OwnerDocument.CreateAttribute("defaultlinking");
        attr.Value = this.m_defaultLinking.ToString();
        linkedSetNode.Attributes.Append( attr );

        attr = state_.OwnerDocument.CreateAttribute("enabled");
        attr.Value = this.m_isEnabled.ToString();
        linkedSetNode.Attributes.Append( attr );

        attr = state_.OwnerDocument.CreateAttribute("roundrobinpointer");
        attr.Value = this.m_roundRobinPointer.ToString();
        linkedSetNode.Attributes.Append( attr );

        string order = "";

        // now add an element for each subscribing LinkControl
        foreach(LinkControl subscriber in this)
        {
          LinkStateEnum linkState = subscriber.GetLinkState(this);

          if (linkState == LinkStateEnum.Inelligible)
            continue; // Don't need to save state for inelligible link control.

          order += subscriber.Colleague.ColleagueId + ";";
          XmlElement subscriberElement = state_.OwnerDocument.CreateElement("LinkedSetSubscriber");
          attr = state_.OwnerDocument.CreateAttribute("colleagueid");
          attr.Value = subscriber.Colleague.ColleagueId.ToString();
          subscriberElement.Attributes.Append( attr );

          attr = state_.OwnerDocument.CreateAttribute("linkstate");
          attr.Value = linkState.ToString();
          subscriberElement.Attributes.Append( attr );

          linkedSetNode.AppendChild( subscriberElement );
        }
        
        attr = state_.OwnerDocument.CreateAttribute("subscriberorder");
        attr.Value = order;
        linkedSetNode.Attributes.Append( attr );

        state_.AppendChild( linkedSetNode );
      }
    }

    /// <summary>
    /// Method used to revert the DefaultLinking to the before-disabled
    /// state. You can explicitly set the state with the DefaultLinking 
    /// property, but if you wish to return to an unknown previous state
    /// use this.
    /// </summary>
    internal void DisableLinking( bool disable_ )
    {
      if (disable_)
      {
        // this will set the appropriate flag
        DefaultLinking = DefaultLinkingEnum.Disabled;
      }
      else if (DefaultLinking == DefaultLinkingEnum.Disabled)
      {
        // this will return the state to the pre-disabled value
        DefaultLinking = m_defaultLinking;
      }
    }

    #endregion

    #region events and event handlers

    /// <summary>
    /// This event is fired when the topic mediator has decided to link a publisher
    /// to a subscriber, and is about to send the message.
    /// This method is the engine for this class.
    /// </summary>
    private void TopicMediator_PrePublish(object sender_, PrePublishEventArgs e_)
    {
      if (LinkManager.Current.Enabled)
      {
        if (this.m_topic.TopicHandlerName == e_.CommonTopicName)
        {
          if (this.Publisher.Colleague.ColleagueId == e_.Sender.ColleagueId)
          {
            if (m_topic.LinkableState != LinkableStateEnum.None)
            {
              if (DefaultLinking == DefaultLinkingEnum.Disabled)
              {
                e_.Cancel = true;
              }
              else
              {
                LinkControl subscriber = this[e_.Subscriber];
                // check to see if the given sender is in this list
                if ( subscriber == null)
                {
                  // the given subscriber is not in the list
                  // and no other LinkedSet will have the same 
                  // publisher/topic key
                  e_.Cancel = true;
                }
                else 
                {
                  LinkStateEnum state = subscriber.GetLinkState ( this );
                  if (state == LinkStateEnum.Unlinked || state == LinkStateEnum.Inelligible)
                  {
                    e_.Cancel = true;
                  }
                  else if ( IsRoundRobin )
                  {
                    if ( this[RoundRobinPointer].Colleague.ColleagueId != e_.Subscriber.ColleagueId )
                    {
                      // the given subscriber is not the one that will be used now
                      e_.Cancel = true;
                    }
                    else
                    {
                      // store a reference to the last recipient of the message.
                      m_lastRecipient = LinkControl.GetInstance( e_.Subscriber );
  					          // notify the registered listeners that the RR state has now changed.
							        OnRoundRobinStateChanged( EventArgs.Empty );
                    }
                  }
                }
              }
            }
          }
        }
      }
      // in all other cases we're not interested - let the message pass unchanged
    }

    /// <summary>
    /// Calls the DefaultLinkingChanged event, which is subscribed to
    /// by the interested LinkControls. This allows them to update their
    /// LinkImages if required.
    /// </summary>
    protected virtual void OnDefaultLinkingChanged(EventArgs e_)
    {
      if (DefaultLinkingChanged != null)
      {
        DefaultLinkingChanged(this, e_);
      }
    }

    /// <summary>
    /// Calls the RoundRobinStateChanged event, which is subscribed to
    /// by the interested LinkControls. This allows them to update their
    /// LinkImages if required.
    /// </summary>
    protected virtual void OnRoundRobinStateChanged(EventArgs e_)
    {
      if (RoundRobinStateChanged!= null)
      {
        RoundRobinStateChanged(this, e_);
      }
    }   
    

    #endregion
  
    #region Implementation of ICollection
    public void CopyTo(System.Array array_, int index_)
    {
      m_subscribers.CopyTo(array_, index_);
    }

    public bool IsSynchronized
    {
      get
      {
        return m_subscribers.IsSynchronized;
      }
    }

    public int Count
    {
      get
      {
        return m_subscribers.Count;
      }
    }

    public object SyncRoot
    {
      get
      {
        return m_subscribers.SyncRoot;
      }
    }
    #endregion

    #region Implementation of IEnumerable

    public System.Collections.IEnumerator GetEnumerator()
    {
      return m_subscribers.GetEnumerator();
    }

    #endregion
  }
}
