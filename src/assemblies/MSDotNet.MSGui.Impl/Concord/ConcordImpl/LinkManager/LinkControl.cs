/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Concord/ConcordImpl/LinkManager/LinkControl.cs#10 $
// $Change: 883853 $
// $DateTime: 2014/06/09 03:12:20 $
// $Author: caijin $

using System;
using System.Collections;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Xml;
using MorganStanley.IED.Concord.Application;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions.MVVMDock;

namespace MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl.LinkManager
{
  
  /// <summary>
  /// The LinkControl contains the linking information regarding one
  /// particular IColleague. It contains the LinkState for each LinkedSet,
  /// and provides all the GUI implementations of images etc.
  /// 
  /// It also provides a number of static methods to get the specific
  /// instance of LinkControl that you need, and has no directly accessable 
  /// constructor.
  /// </summary>
  internal class LinkControl : IDisposable, ILinkControl
  {
    
    #region Constants

    private static readonly string CLASS_NAME = "LinkControl";
    private const string IMAGEKEY_MOUSEOVER = "_<<OVER>>";
    private const string IMAGEKEY_NOTPUBLISHING = "_<<NOTPUBLISHING>>";
    private const string IMAGEKEY_NOTACTIVE = "_<<NOTACTIVE>>";
    private const string IMAGEKEY_ROUNDROBIN_CURRENT = "<<CURRENT>>";
    private const string IMAGEKEY_DISABLED = "<<Disabled>>";
    private const string IMAGEKEY_SOURCE = "<<Source>>";
    private const string IMAGEKEY_LINKED = "<<Linked>>";
    private const string IMAGEKEY_UNLINKED = "<<Unlinked>>";
    private const string IMAGEKEY_ROUNDROBIN_INDEX_SEPARATOR = "$"; // must be only one character

    #endregion Constants
    
    #region constructors

    static LinkControl()
    {
      // the contrsucted LinkControls
      m_linkControls = new Hashtable();
      m_pendingLinkControls = new Hashtable();
      
      // the gui requirements
      m_bitmaps = new Hashtable();
      m_fonts = new Hashtable(); 
      m_brushes = new Hashtable();
      m_pens = new Hashtable();
      m_stringFormat = new StringFormat();     
      m_stringFormat.Alignment = StringAlignment.Center;
      m_stringFormat.LineAlignment = StringAlignment.Center;
      m_stringFormat.Trimming = StringTrimming.EllipsisWord;
      m_stringFormat.FormatFlags = StringFormatFlags.NoWrap;
    }

    private LinkControl( IColleague collegue_ )
    {
      IsFloating = false;
      m_colleague = collegue_;
      m_linkedState = new Hashtable();
      m_linkedSetsSubscribed = new ArrayList();
      m_linkedSetsPublished = new ArrayList();
      m_availableTopics = new ArrayList();
      // Gui requirements
      m_contextMenuSourceItems = new Hashtable();
      LinkImageKey = IMAGEKEY_DISABLED;
    }

    #endregion

    #region static methods

    #region static declarations

    private static readonly Hashtable m_linkControls;
    private static readonly Hashtable m_pendingLinkControls;

    #endregion

    /// <summary>
    /// Gets an existing LinkContol unless one does not exist,
    /// in which case we create one.
    /// </summary>
    internal static LinkControl GetInstance( IColleague colleague_ )
    {
      if (colleague_ == null)
      {
        return null;
      }
      // register the component if necessary
      lock( m_linkControls )
      {
        if ( m_linkControls [ colleague_.ColleagueId ] == null )
        {
          m_linkControls.Add( colleague_.ColleagueId, new LinkControl( colleague_ ) );
          if (m_pendingLinkControls.ContainsKey( colleague_.ColleagueId ))
          {
            // initialise the control
            LinkControl c = (LinkControl)m_linkControls[ colleague_.ColleagueId ];
            ArrayList pending = (ArrayList) m_pendingLinkControls[ colleague_.ColleagueId ];
            foreach( PendingColleagueSubscription subscr in pending )
            {
              subscr.LinkedSet.AddSubscriber( c, subscr.State );
            }
          }
          m_pendingLinkControls.Remove( colleague_.ColleagueId );
        }
      }
      return (LinkControl)m_linkControls[ colleague_.ColleagueId ];
    }

    /// <summary>
    /// Used for deserialisation
    /// </summary>
    [System.Diagnostics.DebuggerStepThrough]
    internal static LinkControl GetInstanceForGuid( Guid colleagueId_ )
    {
      if (colleagueId_ != Guid.Empty)
      {
        lock( m_linkControls )
        {
          if ( m_linkControls.ContainsKey ( colleagueId_ ) )
          {
            return (LinkControl)m_linkControls[ colleagueId_ ];
          }
        }
      }
      return null;
    }
    
    /// <summary>
    /// This one can't create the LinkControl if it doens't
    /// already exist - beware
    /// </summary>
    internal static bool InstanceExists( IColleague colleague_ )
    {
      if (colleague_ == null) 
      {
        return false;
      }
      else
      {
        return m_linkControls.ContainsKey( colleague_.ColleagueId );
      }
    }

    /// <summary>
    /// Completely strip out all references to any controls in the 
    /// LinkManager collection.
    /// </summary>
    internal static void ClearAll()
    {
      lock (m_linkControls )
      {
        if (m_linkControls.Count == 0)
        {
          return;
        }

        object[] controls = new object[m_linkControls.Count];
        m_linkControls.Values.CopyTo(controls, 0);
        /* Delete all controls, one at a time based on previously obtained keys. Cannot enumerate over
        * controls, using foreach, while deleting items from_linkControls. */
        for (int i = 0; i < controls.Length; i++)
        {
          // Delete the control - this will destroy the control and remove it from _linkControls.
          ((LinkControl)controls[i]).Delete();
        }
      }
    }

    /// <summary>
    /// The red, green etc. indicators for linking can refer to only 1 topic at a time (eg. mdSymbol)
    /// This property is set for every LinkControl at the same time, so that all know which topic to
    /// paint for. 
    /// </summary>
    private static LinkedSet LinkedSetForUI
    {
      [System.Diagnostics.DebuggerStepThrough]
      get 
      {
        return m_linkedSetForUi; 
      }
      set
      {
        if (m_linkedSetForUi != value)
        {
          m_linkedSetForUi = value;
          lock( m_linkControls )
          {
            // set the properties for all LinkControls
            foreach(LinkControl control in m_linkControls.Values)
            {
              control.SetImageKey();
            }
          }
        }
      }
    }

    /// <summary>
    /// Adds a colleague ID and state for a colleague that has not yet been added 
    /// but has deserialization information.
    /// </summary>
    internal static void AddPendingColleague(Guid colleagueID_, LinkedSet set_, XmlNode state_)
    {
      // colleague not yet registered - need to wait for it.
      PendingColleagueSubscription pendingSubscription = new PendingColleagueSubscription( set_, state_ );

      if (!m_pendingLinkControls.ContainsKey( colleagueID_ ))
      {
        m_pendingLinkControls.Add( colleagueID_, new ArrayList() );
      }
      ArrayList list = (ArrayList)m_pendingLinkControls[ colleagueID_ ];
      // store the subscription for later use
      list.Add( pendingSubscription );
    }

    #endregion

    #region state related stuff

    #region state related declarations

    private readonly IColleague m_colleague;
    private readonly Hashtable m_linkedState;
    private readonly ArrayList m_linkedSetsSubscribed;
    private readonly ArrayList m_linkedSetsPublished;

    /// <summary>
    /// The topics which this control can subscribe to.  Maintained so that when the control is
    /// deleted, the control can remove itself from all topics to which it can subscribe.
    /// </summary>
    private readonly ArrayList m_availableTopics;
    
    #endregion Declarations

    #region properties

    public bool Enabled
    {
      get
      {
        return LinkManager.Current.Enabled;
      }
      set
      {
        LinkManager.Current.Enabled = value;
      }
    }

    public IColleague Colleague
    {
      [System.Diagnostics.DebuggerStepThrough]
      get
      {
        return this.m_colleague;
      }
    }

    #endregion

    #region internal methods

    public void LoadState( XmlNode state_, ref bool cancel_ )
    {
      // so, from here, we need to rebuild the topics we were publishing,
      // we need to rebuild the topics we subscribed to and then their 
      // publishers (which we can't do).
      // We need to persist the order and LinkState of each LinkedSet


      // what we should do is remember the state of our own LinkedSets,
      // the ones we publish, including the status of all the subscribing 
      // linked sets. When we create our linked sets, we should then check 
      // the LinkControl cache for already registered controls.

      // Once we've done that, every time a new LinkControl is added to the
      // pool we should check to see if it's a listener we're interested in,
      // and set it up if it is.

      // This requires that the IColleague.ColleagueID value is persisted.

      // go through the LinkedSets that we publish, and serialize the information
      bool weHaveFocus = ( LinkedSetForUI != null && LinkedSetForUI.Publisher == this );
      
      XmlNode linkControlState = state_.SelectSingleNode("LinkControlState");

      if (linkControlState != null)
      {
        foreach( XmlNode lSetNode in linkControlState )
        {
          XmlAttribute topicName = lSetNode.Attributes["name"];
          // check we don't have this LinkedSet already - if so, clear it out.
          LinkedSet lSet = GetPublishedLinkSet( topicName.Value );
          if (lSet != null)
          {
            lSet.LinkedTopic.RemovePublisher( this.Colleague );
          }
          // now create the given linked topic and linked set if it's not already created
          LinkManager.Current.AddTopicPublisher( this.Colleague, topicName.Value, false );
          // now find the set in our linked sets again
          lSet = GetPublishedLinkSet( topicName.Value );
            if (null != lSet)
            {
                lSet.Deserialize( lSetNode );                
            }
        }
      }
    

      if (weHaveFocus)
      {
        // get the next best match
        this.SelectLinkedSetForGui();
      }
    }

    private LinkedSet GetPublishedLinkSet( string topicname_ )
    {
      foreach(LinkedSet lSet in m_linkedSetsPublished)
      {
        if (lSet.LinkedTopic.TopicHandlerName.Equals( topicname_ ))
        {
          return lSet;
        }
      }
      return null;
    }

    public void SaveState( XmlNode state_, ref bool cancel_ )
    {
      // for detailed description, see Deserialize

      // go through the LinkedSets that we publish, and serialize the information
      XmlNode linkControlState = state_.OwnerDocument.CreateElement("LinkControlState");
      foreach(LinkedSet lSet in this.m_linkedSetsPublished)
      {
        lSet.Serialize( linkControlState );
      }
      state_.AppendChild( linkControlState );
    }

    /// <summary>
    /// Sets the state of the given linked set in the _linkedState hashtable
    /// to be the given state, if possible.
    /// </summary>
    internal void SetLinkState( LinkedSet set_, LinkStateEnum state_ )
    {
      SetLinkState( set_, state_, true );
    }

    /// <summary>
    /// Sets the state of the given linked set in the _linkedState hashtable
    /// to be the given state, if possible. Update the images if necessary
    /// </summary>
    private void SetLinkState( LinkedSet set_, LinkStateEnum state_,  bool updateImages_ )
    {
      // we only want to update the images if this is the LinkedSetForUI: 
      updateImages_ &= ( set_ == LinkedSetForUI );

      switch( state_ )
      {
        case LinkStateEnum.Inelligible:
        case LinkStateEnum.Source:
          // these linked states are not dependent on the set.
          // No further action required.
          break;
          
        case LinkStateEnum.Linked:
        case LinkStateEnum.Unlinked:
          lock( m_linkedState )
          {
            if ( set_ != null && set_.Publisher != this && m_linkedState.ContainsKey( set_ ) )
            {
              LinkStateEnum oldState = GetLinkState( set_ );
              if (oldState != state_)
              {
                // first check that we are allowed to link:
                if (state_ == LinkStateEnum.Linked 
                  && !LinkManager.Current.AllowedToSubscribe( set_, this ) )
                {
                  // the two linked controls are not allowed to be linked
                  state_ = LinkStateEnum.Inelligible;
                }
                else if (set_.DefaultLinking == DefaultLinkingEnum.SameForm 
                  && set_.Publisher.ClientForm != this.ClientForm)
                {
                  // the two controls are not on the same form, and the Linking mode requires
                  // that they are.
                  state_ = LinkStateEnum.Unlinked;
                }
                else if ( set_.DefaultLinking == DefaultLinkingEnum.RadioButton )
                {
                  // if switching state need to check whether or not to
                  // unlink our other subscribers (ie - radio button linking )

                  // we are moving from unlinked to linked
                  if (oldState == LinkStateEnum.Unlinked)
                  {
                    // first, assign the state for this control so that
                    // when the SetLinkState (called below) happens, there will be a valid Linked
                    // control
                    m_linkedState[set_] = state_; 
                    // we're linking this so now unlink the others
                    foreach(LinkControl c in set_)
                    {
                      if (c != this)
                      {
                        c.SetLinkState( set_, LinkStateEnum.Unlinked, updateImages_ );
                      }
                    }
                  }
                  else
                  {
                    // we were linked and now want to be unlinked - however with radio 
                    // buttons that doesn't work: clickin on a linked one leaves it linked
                    // We do need to check that only one is selected though
                    bool oneSelected = false;
                    foreach(LinkControl c in set_)
                    {
                      if ( c != this && c.GetLinkState( set_ ) == LinkStateEnum.Linked )
                      {
                        // one LinkControl that's not this one is selected
                        oneSelected = true;
                        break;
                      }
                    }

                    if ( !oneSelected )
                    {
                      // don't change the state if there is no other linked control
                      state_ = oldState;
                    }
                  }
                }

                // finally, assign the state for this control
                m_linkedState[set_] = state_; 
              }

              // Any round robin indexes will have changed, so update them if required
              if ( updateImages_ && set_.IsRoundRobin )
              {
                foreach(LinkControl c in set_ )
                {
                  if (c != this)
                  {
                    // update the images.
                    c.SetImageKey();
                  }
                }
              }
            }
          }
          break;
        default:
          // there are no other LinkStates 
          break;
      }

      if (updateImages_)
      {
        SetImageKey();
      }
    }    

    /// <summary>
    /// Establish if this LinkCOntrol is linked to the given topic at all
    /// - regardless of the publisher
    /// </summary>
    /// <param name="topic_"></param>
    /// <returns></returns>
    internal bool LinkedToTopic( LinkedTopic topic_ )
    {
      lock( m_linkedSetsSubscribed)
      {
        foreach(LinkedSet lSet in this.m_linkedSetsSubscribed)
        {
          if (lSet.LinkedTopic == topic_)
          {
            if ( GetLinkState( lSet ) == LinkStateEnum.Linked )
            {
              return true;
            }
          }
        }
      }
      return false;
    }

    /// <summary>
    /// Adds a linked set from this LinkControl's publishing sets and hooks
    /// any event handlers
    /// </summary>
    internal void AddLinkedSetPublisher( LinkedSet set_ )
    {
      lock (m_linkedSetsPublished)
      {
        if (!m_linkedSetsPublished.Contains( set_ ))
        {
          m_linkedSetsPublished.Add( set_ );
          set_.DefaultLinkingChanged += LinkedSet_DefaultLinkingChanged;
          set_.RoundRobinStateChanged += LinkedSet_RoundRobinStateChanged;
        }
      }
      SetImageKey();
    }

    /// <summary>
    /// Removes a linked set from this LinkControl's publishing sets and unhooks
    /// any event handlers
    /// </summary>
    internal void RemoveLinkedSetPublisher( LinkedSet set_ )
    {
      lock (m_linkedSetsPublished)
      {
        if (m_linkedSetsPublished.Contains( set_ ))
        {
          m_linkedSetsPublished.Remove( set_ );
          set_.DefaultLinkingChanged -= LinkedSet_DefaultLinkingChanged;
          set_.RoundRobinStateChanged -= LinkedSet_RoundRobinStateChanged;
        }
      }
      SetImageKey();
    }

    /// <summary>
    /// Adds a linked set to this LinkControl's subscribed sets and hooks
    /// any event handlers
    /// </summary>
    internal void AddLinkedSetSubscriber( LinkedSet set_, LinkStateEnum state_ )
    {
      lock (m_linkedSetsSubscribed)
      {
        if ( !m_linkedSetsSubscribed.Contains( set_ ) )
        {
          m_linkedSetsSubscribed.Add( set_ );
          m_linkedState.Add( set_,  state_ );
          set_.DefaultLinkingChanged += LinkedSet_DefaultLinkingChanged;
          set_.RoundRobinStateChanged += LinkedSet_RoundRobinStateChanged;
        }
        else
        {
          m_linkedState[ set_ ] = state_;
        }
      }
      SetImageKey();
    }

    /// <summary>
    /// Removes a linked set from this LinkControl's subscribed sets and unhooks
    /// any event handlers
    /// </summary>
    internal void RemoveLinkedSetSubscriber( LinkedSet set_ )
    {
      lock (m_linkedSetsSubscribed)
      {
        if (m_linkedSetsSubscribed.Contains( set_ ))
        {
          m_linkedSetsSubscribed.Remove( set_ );
          m_linkedState.Remove( set_ );
          m_contextMenuSourceItems.Remove( set_ );
          set_.DefaultLinkingChanged -= LinkedSet_DefaultLinkingChanged;
          set_.RoundRobinStateChanged -= LinkedSet_RoundRobinStateChanged;
        }
      }
    }

    /// <summary>
    /// Adds a topic to this LinkControl's list of available topics
    /// </summary>
    internal void AddAvailableTopic(LinkedTopic topic_)
    {
      lock (m_availableTopics)
      {
        if (!m_availableTopics.Contains(topic_))
        {
          m_availableTopics.Add(topic_);
        }
      }
    }

    /// <summary>
    /// Remove this LinkControl from everywhere
    /// </summary>
    public void Delete()
    {
      int i = 0; // use i to prevent any possible infinte loop

      // remove all the LinkedSets that this is a publisher for
      while( m_linkedSetsPublished.Count > 0 && i < 1000)
      {
        // specifically do not lock the _linkedSetsPublished object - we
        // want it to be modified at this stage.
        LinkedSet lSet = (LinkedSet)this.m_linkedSetsPublished[0];
        lSet.LinkedTopic.RemovePublisher( this.m_colleague );
        i++;
      }
      // remove ourselves from any topics we subscribe to.
      while( m_linkedSetsSubscribed.Count > 0 && i < 1000)
      {
        // specifically do not lock the _linkedSetsSubscried object - we
        // want it to be modified at this stage.
        LinkedSet lSet = (LinkedSet)this.m_linkedSetsSubscribed[0];
        // clear ourselves out of any LinkedSets that are based on the 
        // linked topic - note that this may be multiple linkedSets
        lSet.LinkedTopic.RemoveSubscriber( this.m_colleague );
        i++;
      }

      // Make sure we are removed from all topics that we can subscribe to.
      while (m_availableTopics.Count > 0)
      {
        ((LinkedTopic) m_availableTopics[0]).AvailableSubscribers.Remove(m_colleague.ColleagueId);
        m_availableTopics.RemoveAt(0);
      }

      if (i>=1000)
      {
        //if (Log.IsLogLevelEnabled(LogLevel.Error))
        //{
        //  Log.Error.SetLocation(CLASS_NAME, "Delete").Write("Failed to delete LinkControl: fell into loop: " + _colleague.ColleagueId );
        //}
      }

      // Dispose of the link control
      Dispose(true);
    }

    /// <summary>
    /// Get the linking state for the give set_. If you want the LinkedState
    /// for the selected gui state, use GetLinkState().
    /// </summary>
    internal LinkStateEnum GetLinkState( LinkedSet set_)
    {
      if (set_ == null) 
      {
        return LinkStateEnum.Inelligible;
      }

      if ( m_linkedSetsPublished.Contains( set_ ) )
      {
        return LinkStateEnum.Source;
      }

      if (m_linkedState[set_] != null)
      {
        return (LinkStateEnum)m_linkedState[set_];
      }
      
      return LinkStateEnum.Inelligible;
    }

    #endregion

    #region IDisposable

    ~LinkControl()
    {
      Dispose( false );
    }

    void IDisposable.Dispose()
    {
      Dispose( true );
    }

    /// <summary>
    /// The main dispose method - called from IDisposabele.Dispose with 
    /// disposing = true, and from ~LinkControl with disposing = false.
    /// </summary>
    /// <param name="disposing_"></param>
    protected void Dispose(bool disposing_)
    {
      lock( m_linkControls )
      {
        if ( m_linkControls.ContainsKey( m_colleague.ColleagueId ) )
        {
          m_linkControls.Remove( this.m_colleague.ColleagueId );
        }
		m_form = null;
      }
      if (disposing_)
      {
        GC.SuppressFinalize( this );
      }
    }


    #endregion

    #endregion

    #region GUI related stuff 
    
    #region Gui related declarations

    private static readonly Hashtable m_bitmaps;
    private static readonly Hashtable m_fonts;
    private static readonly Hashtable m_brushes;
    private static readonly Hashtable m_pens;
    private static readonly StringFormat m_stringFormat;
    private static LinkedSet m_linkedSetForUi;
    private static LinkControl m_linkControlAskedLast;

    private System.Windows.Forms.Form m_form;
    private string m_imageKey = null;
    private System.Windows.Forms.ContextMenu m_contextMenuSource;
    private readonly Hashtable m_contextMenuSourceItems;
    private ContextMenu m_contextMenuRoundRobin;
    private ContextMenu m_contextMenuLink;
    private MenuItem[] m_selectedTopicSubMenu;
    private LinkedSet m_lastLinkedSetForUI;

    #endregion

    #region Gui related Events

    /// <summary>
    /// Called whenever this LinkControl's current LinkState
    /// (that is - for the currently selected LinkedSet) changes
    /// </summary>
    public event EventHandler LinkStateChanged;

    /// <summary>
    /// Call the LinkStateChanged event
    /// </summary>
    protected virtual void OnLinkStateChanged(EventArgs e)
    {
      if (LinkStateChanged != null)
      {
        LinkStateChanged(this, e);
      }
    }

    #endregion GUI related Events

    #region GUI related Properties

    /// <summary>
    /// Am I on a floating window
    /// </summary>
    public bool IsFloating { get; set; }

    /// <summary>
    /// Usually the caption of the window but not if it is floating
    /// </summary>
    public string Text { get; set; }


    /// <summary>
    /// The bitmap to display in the title bar of the form
    /// </summary>
    public System.Drawing.Image LinkImage( int width )
    {
      return GetImage( width, m_imageKey );
    }

    /// <summary>
    /// The context menu for this LinkControl
    /// </summary>
    internal System.Windows.Forms.ContextMenu LinkMenuSource
    {
      [System.Diagnostics.DebuggerStepThrough()]
      get 
      { 
        if (m_contextMenuSource == null)
        {
          m_contextMenuSource = new System.Windows.Forms.ContextMenu();
          m_contextMenuSource.Popup += new EventHandler( _contextMenuSource_PopUp );
          m_selectedTopicSubMenu = new MenuItem[7];
          m_selectedTopicSubMenu[0] = new MenuItem( "Round Robin", new EventHandler(_contextMenuSourceItems_ClickRoundRobin) );
          m_selectedTopicSubMenu[1] = new MenuItem( "Always one", new EventHandler(_contextMenuSourceItems_ClickRadioButton) );
          m_selectedTopicSubMenu[2] = new MenuItem( "Fully manual", new EventHandler(_contextMenuSourceItems_ClickManual) );
          m_selectedTopicSubMenu[3] = new MenuItem( "-" );
          m_selectedTopicSubMenu[4] = new MenuItem( "Restore default", new EventHandler(_contextMenuSourceItems_ClickDefault) );
          m_selectedTopicSubMenu[5] = new MenuItem( "-" );
          m_selectedTopicSubMenu[6] = new MenuItem( "*** publishing", new EventHandler(_contextMenuSourceItems_ClickStop) );
        }        
        return m_contextMenuSource; 
      }
    }

    internal ContextMenu LinkMenuRoundRobin
    {
      get
      {
        if (m_contextMenuRoundRobin == null)
        {
          m_contextMenuRoundRobin = new System.Windows.Forms.ContextMenu();
          m_contextMenuRoundRobin.Popup += new EventHandler( _contextMenuRoundRobin_PopUp );
          m_contextMenuRoundRobin.MenuItems.Add("Link to <source>", new EventHandler(_contextMenuToggleLink_Click));
          m_contextMenuRoundRobin.MenuItems.Add("-");
          m_contextMenuRoundRobin.MenuItems.Add("Move up the queue.", new EventHandler( _contextMenuRoundRobin_MoveUpClick ) );
          m_contextMenuRoundRobin.MenuItems.Add("Move down the queue.", new EventHandler( _contextMenuRoundRobin_MoveDownClick ));
        }        
        return m_contextMenuRoundRobin;
      }
    }

    internal ContextMenu LinkMenuLink
    {
      get
      {
        if (m_contextMenuLink == null)
		{
          m_contextMenuLink = new System.Windows.Forms.ContextMenu();
          m_contextMenuLink.Popup += new EventHandler( _contextMenuLink_PopUp );
          m_contextMenuLink.MenuItems.Add( "Link to <source>", new EventHandler( _contextMenuToggleLink_Click ) );
          // need to build up the Round Robin index
        }        
        return m_contextMenuLink;
      }
    }

    /// <summary>
    /// Indicates whether or not this LinkControl can do any publishing.
    /// </summary>
    internal bool IsPublisher
    {
      get
      {
        return m_linkedSetsPublished.Count > 0;
      }
    }

    public System.IntPtr GetActiveWindowHandle(bool isLaunched_)
    {
      return LinkManager.Current.GetActiveWindowHandle( isLaunched_ );
    }

    /// <summary>
    /// The optional windows form associated with this LinkControl
    /// </summary>
    public System.Windows.Forms.Form ClientForm
    {
      [System.Diagnostics.DebuggerStepThrough()]
      get { return m_form; }
      [System.Diagnostics.DebuggerStepThrough()]
      set 
      { 
        if (m_form != value)
        {
          // update the LinkedSets for any we were inellegible - we may be elligable now.
          m_form = value;
          foreach( LinkedSet set_ in this.m_linkedSetsSubscribed)
          {
            if ( this.GetLinkState( set_ ) == LinkStateEnum.Inelligible )
            {
              // attempt to re-add the subscriber: this will
              // change the state if possible.
              set_.AddSubscriber( this );
            }
          }
        }
      }
    }

    /// <summary>
    /// Get the last LinkedSet that was selected for this LinkControl
    /// </summary>
    private LinkedSet ThisLinkedSetForUI
    {
      get
      {
        return m_lastLinkedSetForUI;
      }
      set
      {
        m_lastLinkedSetForUI = value;
        LinkedSetForUI = m_lastLinkedSetForUI;
      }
    }

    #endregion Gui related properties

    #region gui related internal methods

    void ILinkControl.SetActive( bool activate_ )
    {
      System.Diagnostics.Debug.Assert( ClientForm != null, "Client Form is null. ", "You must have Registered this colleague with the IWindow in order to use this method.");
      LinkManager.Current.SetActive( this.Colleague, activate_ );
    }

    /// <summary>
    /// Brightens the icon - the window has focus.
    /// </summary>
    void ILinkControl.HandleActivate()
    {
      if (LinkImageKey.IndexOf(IMAGEKEY_NOTACTIVE) >= 0)
      {
        LinkImageKey = LinkImageKey.Replace( IMAGEKEY_NOTACTIVE, string.Empty );
      }
    }

    /// <summary>
    /// Fades the icon - the application has lost focus.
    /// </summary>
    void ILinkControl.HandleDeactivate()
    {
      if ( LinkImageKey.IndexOf(IMAGEKEY_NOTACTIVE) == -1 )
      {
        LinkImageKey += IMAGEKEY_NOTACTIVE;
      }
    }

    /// <summary>
    /// Called by the IWindow for current form when the mouse moves into
    /// the LinkImage region
    /// </summary>
    void ILinkControl.HandleMouseMove()
    {
      if (!LinkImageKey.EndsWith(IMAGEKEY_MOUSEOVER))
      {
        LinkImageKey += IMAGEKEY_MOUSEOVER;
      }
    }

    /// <summary>
    /// Called by the IWindow for current form when the mouse leaves
    /// the LinkImage region
    /// </summary>
    void ILinkControl.HandleMouseLeave()
    {
      LinkImageKey = LinkImageKey.Replace(IMAGEKEY_MOUSEOVER, string.Empty);
    }

    /// <summary>
    /// Called by the IWindow for current form when the mouse is 
    /// clicked in the LinkImage region
    /// </summary>
    void ILinkControl.HandleMouseClick(bool leftClick_, Point defaultClickLoc_)
    {
      if (leftClick_ == true)
      {
        HandleLeftClick( defaultClickLoc_ );
      }
      else
      {
        HandleRightClick();
      }
    }

    /// <summary>
    /// Get the linkstate for the current selected linked set
    /// </summary>
    /// <returns></returns>
    internal LinkStateEnum GetLinkState()
    {
      return this.GetLinkState( LinkedSetForUI );
    }

    /// <summary>
    /// Set the linkstate for the current selected linked set
    /// </summary>
    /// <param name="state_"></param>
    internal void SetLinkState(LinkStateEnum state_)
    {
      SetLinkState( LinkedSetForUI, state_ );
    }

    /// <summary>
    /// Clears the LinkedSetForGUI: essentially a deselect
    /// </summary>
    internal void ResetLinkedSetForGui( )
    {
      LinkedSetForUI = null;
    }

    /// <summary>
    /// Set the most appropriate LinkedSetForUI
    /// </summary>
    internal void SelectLinkedSetForGui( )
    {
      m_linkControlAskedLast = this;
      // need to chose an appropriate LinkedSetForGui for this LinkControl
      // as the publisher
      if (!this.IsPublisher)
      {
        LinkedSetForUI = null;
      }
      else if ( LinkedSetForUI == null && ThisLinkedSetForUI == null )
      {
        // remember the last set this particular gui was using
        ThisLinkedSetForUI = (LinkedSet)m_linkedSetsPublished[0];
      }
      else if ( LinkedSetForUI == null)
      {
        LinkedSetForUI = ThisLinkedSetForUI;
      }
      else if( !this.m_linkedSetsPublished.Contains( LinkedSetForUI ) )
      {
        LinkedSet ls = GetPublishedLinkSet( LinkedSetForUI.LinkedTopic.TopicHandlerName );
        if ( ls != null )
        {
          // Found a valid LinnkedSetForUI .
          ThisLinkedSetForUI = ls;
        } 
        else
        {
          // do the case where LinkedSetForUI is null
          if ( ThisLinkedSetForUI == null )
          {
            // remember the last set this particular gui was using
            ThisLinkedSetForUI = (LinkedSet)m_linkedSetsPublished[0];
          }
          else 
          {
            LinkedSetForUI = ThisLinkedSetForUI;
          }
        }
      }
      // the final case is that the LinkedSetForUI is valid so no 
      // change is necesary
    }

    #endregion

    #region gui related event handlers - mostly menu clicking

    private void LinkedSet_RoundRobinStateChanged (object sender, EventArgs e)
    {
      if (sender == LinkedSetForUI)
      {
        SetImageKey();
      }
    }

    private void LinkedSet_DefaultLinkingChanged(object sender, EventArgs e)
    {
      // handle the special case where the current form has just
      // been given a LinkedSet that it didn't have before 
      if ( ClientForm != null && m_linkControlAskedLast == this && LinkedSetForUI == null )
      {
        LinkedSetForUI = (LinkedSet)sender;
      }
      else if (sender == LinkedSetForUI)
      {
        SetImageKey();
      }
    }

    private void _contextMenuSourceItems_Click(object sender, EventArgs e)
    {
      ThisLinkedSetForUI = LinkedSetForMenuItem( sender );
    }

    private void _contextMenuSourceItems_ClickRoundRobin(object sender, EventArgs e)
    {
      if (LinkedSetForUI != null)
      {
        LinkedSetForUI.DefaultLinking = DefaultLinkingEnum.RoundRobin;
      }
    }

    private void _contextMenuSourceItems_ClickRadioButton(object sender, EventArgs e)
    {
      if (LinkedSetForUI != null)
      {
        LinkedSetForUI.DefaultLinking = DefaultLinkingEnum.RadioButton;
      }
    }

    private void _contextMenuSourceItems_ClickManual(object sender, EventArgs e)
    {
      if (LinkedSetForUI != null)
      {
        LinkedSetForUI.DefaultLinking = DefaultLinkingEnum.All;
      }
    }

    private void _contextMenuSourceItems_ClickDefault(object sender, EventArgs e)
    {
      if (LinkedSetForUI != null)
      {
        LinkedSetForUI.DefaultLinking = LinkedSetForUI.LinkedTopic.DefaultLinking;
      }
    }

    private void _contextMenuSourceItems_ClickStop(object sender, EventArgs e)
    {
      if (LinkedSetForUI != null)
      {
        // disable or reenable linking
        LinkedSetForUI.DisableLinking( LinkedSetForUI.DefaultLinking != DefaultLinkingEnum.Disabled );
      }
    }

    private void AddSubMenuItems( Menu menu )
    {
      if (m_selectedTopicSubMenu[0].Parent != null)
      {
        m_selectedTopicSubMenu[0].Parent.MenuItems.Clear();
      }
      menu.MenuItems.AddRange( m_selectedTopicSubMenu );
    }

    private void _contextMenuSource_PopUp(object sender, EventArgs e)
    { 
      m_contextMenuSource.MenuItems.Clear();
      lock ( m_linkedSetsPublished )
      {
        foreach(LinkedSet ls in this.m_linkedSetsPublished)
        {
          m_contextMenuSource.MenuItems.Add( MenuItemForLinkedSet( ls ) );        
        }
      }

      if (m_contextMenuSource.MenuItems.Count == 1)
      {
        // if there's only one subject to link on, only show the linking for that subject
        // not that the above step is necessary because it also initialises the menu.
        m_contextMenuSource.MenuItems.Clear();
        AddSubMenuItems( m_contextMenuSource );
      }
      else
      {
        AddSubMenuItems( (MenuItem)m_contextMenuSourceItems[ LinkedSetForUI ] );
      }

    }

    private void _contextMenuRoundRobin_PopUp(object sender, EventArgs e)
    {
      LinkStateEnum state = LinkStateEnum.Inelligible;
      if (LinkedSetForUI != null && LinkedSetForUI.IsRoundRobin ) 
      {
        state = GetLinkState();
      }

      switch (state)
      {
        case LinkStateEnum.Linked:
          SetMenuItemsEnabled( m_contextMenuRoundRobin, true );
          m_contextMenuRoundRobin.MenuItems[0].Text = "Unlink from " + LinkedSetForUI.LinkedTopic.TopicHandlerDescription;
          break;
        case LinkStateEnum.Unlinked:
        case LinkStateEnum.Inelligible:
        default:            
          SetMenuItemsEnabled( m_contextMenuRoundRobin, false );
          m_contextMenuRoundRobin.MenuItems[0].Text = "Cannot link to source.";
          break;
      }
    }

    private void _contextMenuRoundRobin_MoveUpClick (object sender, EventArgs e)
    {
      if (LinkedSetForUI != null)
      {
        LinkedSetForUI.MoveSubscriber( this.Colleague, true );
      }
    }
    
    private void _contextMenuRoundRobin_MoveDownClick (object sender, EventArgs e)
    {
      if (LinkedSetForUI != null)
      {
        LinkedSetForUI.MoveSubscriber( this.Colleague, false );
      }
    }

    private void _contextMenuToggleLink_Click (object sender, EventArgs e)
    {
		LinkedSetForUI = this.linkedSetBuffer;
		
		
		LinkStateEnum state = LinkStateEnum.Inelligible;
      if (LinkedSetForUI != null) 
      {
        state = GetLinkState();
      }

      switch (state)
      {
        case LinkStateEnum.Linked:
          this.SetLinkState( LinkStateEnum.Unlinked );
          break;
        case LinkStateEnum.Unlinked:
          this.SetLinkState( LinkStateEnum.Linked );
          break;          
        case LinkStateEnum.Inelligible:
        default:            
          // do nothing
          break;
      }

	  PaneFactory.FocusBounceBackNeeded = false;
	  PaneFactory.BounceBackTo = null;
	}

    private void _contextMenuLink_PopUp(object sender, EventArgs e)
    {
      LinkStateEnum state = LinkStateEnum.Inelligible;
      if (LinkedSetForUI != null) 
      {
        state = GetLinkState();
      }
      
      m_contextMenuLink.MenuItems[0].Enabled = true;
      switch (state)
      {
        case LinkStateEnum.Linked:
          m_contextMenuLink.MenuItems[0].Text = "Unlink from " + LinkedSetForUI.LinkedTopic.TopicHandlerDescription;
          break;
        case LinkStateEnum.Unlinked:
          m_contextMenuLink.MenuItems[0].Text = "Link to " + LinkedSetForUI.LinkedTopic.TopicHandlerDescription;
          break;          
        case LinkStateEnum.Inelligible:
        default:            
          m_contextMenuLink.MenuItems[0].Enabled = false;
          m_contextMenuLink.MenuItems[0].Text = "Cannot link to source.";
          break;
      }
    }

    #endregion gui related event handlers

    #region private gui-related helper methods

    private void HandleLeftClick( Point defaultClickLoc_ )
    {
      // simply toggles the link state for this LinkControl on the LinkedForUI topic
      if (GetLinkState() == LinkStateEnum.Linked)
      {        
        this.SetLinkState( LinkStateEnum.Unlinked );
      }
      else if (GetLinkState() == LinkStateEnum.Unlinked)
      {        
        this.SetLinkState( LinkStateEnum.Linked );
      }
      else if (GetLinkState() == LinkStateEnum.Source)
      {
        LinkMenuSource.Show( ClientForm, defaultClickLoc_ );
      }
      // we've clicked so we must be over the image
      LinkImageKey += IMAGEKEY_MOUSEOVER;
    }
	private LinkedSet linkedSetBuffer = null;
    private void HandleRightClick()
    {
      if (ClientForm != null)
      {
        LinkStateEnum state = GetLinkState();
		this.linkedSetBuffer = LinkedSetForUI;
		  if (state == LinkStateEnum.Source)
        {
          LinkMenuSource.Show(ClientForm,ClientForm.PointToClient(System.Windows.Forms.Control.MousePosition));        
          return;
        }
        else if (LinkedSetForUI != null && LinkedSetForUI.IsRoundRobin)
        {
          if (state == LinkStateEnum.Linked)
          {
            LinkMenuRoundRobin.Show(ClientForm,ClientForm.PointToClient(System.Windows.Forms.Control.MousePosition));
            return;
          }
        }
        // otherwise the generic link/unlink menu
        LinkMenuLink.Show(ClientForm,ClientForm.PointToClient(System.Windows.Forms.Control.MousePosition));
		PaneFactory.FocusBounceBackNeeded = true;
      }
    }

    /// <summary>
    /// Set the LinkImage for the currently selected LinkedSet
    /// </summary>
    private void SetImageKey()
    {
      if (LinkedSetForUI != null)
      {
        if (m_linkedSetsPublished.Contains(LinkedSetForUI))
        {
          string key = IMAGEKEY_SOURCE;
          if ( this.ClientForm != null && !ClientForm.ContainsFocus)
          {
            //key += IMAGEKEY_NOTACTIVE;
          }
          if ( LinkedSetForUI.DefaultLinking == DefaultLinkingEnum.Disabled )
          {
            key += IMAGEKEY_NOTPUBLISHING;
          }
          LinkImageKey = key;
          return;
        }
        else if (m_linkedSetsSubscribed.Contains( LinkedSetForUI ) )
        {
          SetSubscribedImageKey( );
          return;
        }
      }
      LinkImageKey = IMAGEKEY_DISABLED;
    }

    /// <summary>
    /// Sets the LinkImageKey property for the given set_. Note that 
    /// the currently selected LinkedSet must be valid by this stage
    /// </summary>
    private void SetSubscribedImageKey()
    {
      System.Diagnostics.Debug.Assert( m_linkedState.ContainsKey( LinkedSetForUI ), "Key not found!", "This method shouldn't be called if set_ isn't def. in the state table.");
      
      if (LinkedSetForUI.DefaultLinking == DefaultLinkingEnum.Disabled)
      {
        LinkImageKey = IMAGEKEY_DISABLED;
        return;
      }

      LinkStateEnum state = this.GetLinkState( LinkedSetForUI );
      if (state == LinkStateEnum.Linked)
      {
        string key = IMAGEKEY_LINKED;
                
        if (LinkedSetForUI.IsRoundRobin)
        {
          key += IMAGEKEY_ROUNDROBIN_INDEX_SEPARATOR + LinkedSetForUI.GetRoundRobinIndex( this );
          if (LinkedSetForUI.IsPointedByRoundRobin( this ))
          {
            key += IMAGEKEY_ROUNDROBIN_INDEX_SEPARATOR + IMAGEKEY_ROUNDROBIN_CURRENT;
          }          
        }
        LinkImageKey = key;
      }
      else if (state == LinkStateEnum.Unlinked)
      {
        LinkImageKey = IMAGEKEY_UNLINKED;
      }
    }

    /// <summary>
    /// Gets or sets the image key used to 
    /// create images
    /// </summary>
    private string LinkImageKey
    {
      [System.Diagnostics.DebuggerStepThrough()]
      get
      {
        return m_imageKey;
      }
      set
      {
        if (m_imageKey != value )
        {
          m_imageKey = value;
          OnLinkStateChanged( EventArgs.Empty );    
        }
      }
    }

    /// <summary>
    /// Create and cache the image with the specified imageKey name and the 
    /// specified width.
    /// </summary>
    protected static Image GetImage( int width, string imageKey )
    {
      if (imageKey.IndexOf(IMAGEKEY_DISABLED)>=0)
      {
        // not interested in displaying images for the disabled state
        return null;
      }

      if (m_bitmaps[ width.ToString() + IMAGEKEY_ROUNDROBIN_INDEX_SEPARATOR + imageKey ] == null)
      {
        Image bmp = new Bitmap( width, width, PixelFormat.Format32bppArgb );        
        Graphics graphics = Graphics.FromImage( bmp );
        graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

        Brush backGround = null;
        Pen innerBorderPen = null;
        Pen outerBorderPen = null;

        // first handle the highlight part - always tacked on the end
        bool highlight = false;
        string modImageKey = imageKey;
        if (imageKey.IndexOf(IMAGEKEY_MOUSEOVER) >= 0)
        {
          modImageKey = imageKey.Replace(IMAGEKEY_MOUSEOVER, string.Empty);
          highlight = true;
          outerBorderPen = CachedPens( Color.White );
        }

        bool inactive = false;
        if (modImageKey.IndexOf(IMAGEKEY_NOTACTIVE) >= 0)
        {
          modImageKey = modImageKey.Replace(IMAGEKEY_NOTACTIVE, string.Empty);
          inactive = true;
        }

        // now handle the disconnected part 
        bool paintX = false;
        if (modImageKey.IndexOf(IMAGEKEY_NOTPUBLISHING) >= 0)
        {
          modImageKey = modImageKey.Replace(IMAGEKEY_NOTPUBLISHING, string.Empty);
          paintX = true;
        }

        Rectangle bgRect = new Rectangle( 0,0,width,width);

        bool highRes = width < 10;

        string[] stem = modImageKey.Split( IMAGEKEY_ROUNDROBIN_INDEX_SEPARATOR[0] );
        switch (stem[0])
        {
          case IMAGEKEY_SOURCE: 
            if (highlight)
            {
              innerBorderPen = CachedPens( Color.Orange );
              backGround = CachedBrushes( Color.Yellow, Color.Orange, bgRect,  LinearGradientMode.ForwardDiagonal );
            }
            else if (inactive)
            {
              outerBorderPen = CachedPens( Color.LightGray );
              backGround = CachedBrushes( Color.Gray, Color.Yellow, bgRect, LinearGradientMode.BackwardDiagonal );
            }
            else
            {
              outerBorderPen = CachedPens( Color.AntiqueWhite );
              backGround = CachedBrushes( Color.Yellow, Color.Orange, bgRect, LinearGradientMode.BackwardDiagonal );
            }
            break;
          case IMAGEKEY_LINKED: 
            if (highlight)
            {
              innerBorderPen = CachedPens( Color.Green);
              backGround = CachedBrushes(Color.LightGreen, Color.Green, bgRect, LinearGradientMode.ForwardDiagonal );
            }
            else
            {
              outerBorderPen = CachedPens( Color.LightGray );
              backGround = CachedBrushes( highRes ? Color.Green : Color.Gray, Color.LightGreen, bgRect, LinearGradientMode.BackwardDiagonal );
            }
            break;
          case IMAGEKEY_UNLINKED: 
            if (highlight)
            {
              innerBorderPen = CachedPens( Color.Red );
              backGround = CachedBrushes( Color.Pink, Color.Red, bgRect, LinearGradientMode.ForwardDiagonal );
            }
            else
            {
              outerBorderPen = CachedPens( Color.LightGray );
              backGround = CachedBrushes( highRes ? Color.Red : Color.Gray, Color.Pink, bgRect, LinearGradientMode.BackwardDiagonal );
            }
            break;
        }

        if (!highRes)
        {
          // clip the image into a rounded rect
          GraphicsPath clipPath = new GraphicsPath( );
          clipPath.AddEllipse( -3.5F,-3.5F,width+6F, width+6F);
          graphics.Clip = new Region( clipPath );

          // paint the background
          graphics.FillRectangle( backGround, 0,0,width,width );

          // draw the inner border
          if (highlight)
          {
            graphics.DrawRectangle( innerBorderPen, 1, 1, width-3, width-3);
          }

          // draw the outer border - which is also slightly rounded
          GraphicsPath borderPath = new GraphicsPath( );
          float diameter = 5F;
          SizeF sizeF = new SizeF( diameter, diameter );
          RectangleF arc = new RectangleF( new PointF(0F,0F), sizeF );

          borderPath.AddArc( arc, 180, 90 );
          arc.X = width - diameter - 1;
          borderPath.AddArc( arc, 270, 90 );
          arc.Y = width - diameter - 1;
          borderPath.AddArc( arc, 0, 90 );
          arc.X = 0;
          borderPath.AddArc( arc, 90, 90 );
          borderPath.CloseFigure();

          graphics.DrawPath( outerBorderPen, borderPath );
        }
        else
        {
          graphics.FillRectangle( backGround, 0,0,width,width );
          // draw the inner border
          if (highlight)
          {
            graphics.DrawRectangle( innerBorderPen, 0, 0, width-1, width-1);
          }
        }

        // now add the red x if required
        if (paintX)
        {
          float border = (width / 5F);
          float crossSize = (border / 3F);
          GraphicsPath crossFwd = new GraphicsPath();
          crossFwd.AddLines( new PointF[] 
            {
              new PointF( border, border + crossSize ),
              new PointF( border + crossSize, border ),
              new PointF( width - border, width - border - crossSize ),
              new PointF( width - border - crossSize, width - border )
            });

          GraphicsPath crossBack = new GraphicsPath();
          crossBack.AddLines( new PointF[] 
            {
              new PointF( border, width - border - crossSize ),
              new PointF( border + crossSize, width - border ),
              new PointF( width - border, border + crossSize ),
              new PointF( width - border - crossSize, border )
            });

          graphics.FillPath( Brushes.Firebrick, crossFwd );
          graphics.FillPath( Brushes.Firebrick, crossBack );
        }

        // add any round robin text
        if (stem.Length > 1)
        {
          graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
          Brush fontBrush = Brushes.White;
          if (stem.Length > 2 && stem[2].Equals("<<CURRENT>>")) 
          {
            fontBrush = Brushes.Firebrick;
          }
          if (stem[1].Length > 1)
          {
            // can't display more than one number and for some reason no
            // type of Trimming allows trimming the first character,
            stem[1] = "�";
          }
          // bring the number down ever-so slightly as it never has an under-part so looks too high
          float topBorder = CachedFonts(width, graphics).Size / 15F;
          RectangleF stringRect = new RectangleF( 0, topBorder, width, width );
          graphics.DrawString( stem[1], CachedFonts(width, graphics), fontBrush, stringRect, m_stringFormat );
        }

        m_bitmaps.Add( width.ToString() + IMAGEKEY_ROUNDROBIN_INDEX_SEPARATOR + imageKey, bmp );
        //dispose any uncached graphics objects
        graphics.Dispose();
      }
      return (Image)m_bitmaps[ width.ToString() + IMAGEKEY_ROUNDROBIN_INDEX_SEPARATOR + imageKey ];
    }

    /// <summary>
    /// caches fonts used to put numbers in images
    /// </summary>
    protected static Font CachedFonts( int imageHeight, Graphics g )
    {
      if (m_fonts.ContainsKey( imageHeight ))
      {
        return (Font)m_fonts[ imageHeight ];
      }
      else
      {
        Font font = new Font("Microsoft Sans Serif", imageHeight, System.Drawing.FontStyle.Bold );
        bool smaller = true;
        bool firstLap = true;
        while(smaller)
        {
          if (!firstLap)
          {
            font = new Font( font.FontFamily, font.Size - 0.25F, font.Style );
          }
          firstLap = false;
          SizeF fs = g.MeasureString("0123456789", font, 100, m_stringFormat);
          smaller = (fs.Height > imageHeight + 1);
        }
        m_fonts[imageHeight] = font;
        return CachedFonts(imageHeight, g);
      }
    }

    protected static Brush CachedBrushes( Color colorStart, Color colorEnd, Rectangle rect, LinearGradientMode mode )
    {
      string key = colorStart.Name + colorEnd.Name + rect.Width + mode.ToString();
      if ( !m_brushes.ContainsKey( key ))
      {
        m_brushes.Add( key, new LinearGradientBrush(rect, colorStart, colorEnd, mode ) );
      }
      return (Brush)m_brushes[ key ];
    }

    protected static Pen CachedPens( Color color )
    {
      if ( !m_pens.ContainsKey( color ))
      {
        m_pens.Add( color, new Pen(color, 0F ) );
      }
      return (Pen)m_pens[ color ];
    }

    /// <summary>
    /// Used to establish which menu item sent a click event
    /// and which LinkedSet it refers to.
    /// </summary>
    private LinkedSet LinkedSetForMenuItem( object item )
    {
      foreach(object key in m_contextMenuSourceItems.Keys)
      {
        if ( item == m_contextMenuSourceItems[ key ] )
        {
          return (LinkedSet)key;
        }
      }
      return null;
    }

    /// <summary>
    /// Gets the right cached MenuItem for the given LinkedSet and sets all the properties
    /// for any sub menu items.
    /// </summary>
    private MenuItem MenuItemForLinkedSet( LinkedSet set_ )
    {
      // create a new menu item if necesary        
      if ( !m_contextMenuSourceItems.ContainsKey( set_ ) )
      {
        MenuItem newItem = new MenuItem( set_.LinkedTopic.TopicHandlerDescription, new EventHandler(_contextMenuSourceItems_Click) );
        m_contextMenuSourceItems.Add( set_, newItem );
      }

      MenuItem rtn = (MenuItem)m_contextMenuSourceItems[ set_ ];
      // reset the text, because it can change
      rtn.Text = set_.LinkedTopic.TopicHandlerDescription;

      // if the set is the selected set then set up the the selected set's menu items.
      if (set_ == LinkedSetForUI)
      {
        bool enabled = (set_.DefaultLinking != DefaultLinkingEnum.Disabled);

        for (int i =0;i<m_selectedTopicSubMenu.Length - 2; i++)
        {
          m_selectedTopicSubMenu[i].Enabled = enabled;
        }
        m_selectedTopicSubMenu[0].Checked = enabled && set_.DefaultLinking == DefaultLinkingEnum.RoundRobin;
        m_selectedTopicSubMenu[1].Checked = enabled && set_.DefaultLinking == DefaultLinkingEnum.RadioButton;
        m_selectedTopicSubMenu[2].Checked = enabled && set_.DefaultLinking == DefaultLinkingEnum.All;
        m_selectedTopicSubMenu[2].Checked |= enabled && set_.DefaultLinking == DefaultLinkingEnum.None;
        m_selectedTopicSubMenu[ m_selectedTopicSubMenu.Length - 1].Enabled = true;
        m_selectedTopicSubMenu[ m_selectedTopicSubMenu.Length - 1].Text = (enabled ? "Stop" : "Start") + " publishing on " + LinkedSetForUI.LinkedTopic.TopicHandlerDescription;
               
        rtn.DefaultItem = true;
      }
      else
      {
        if (set_.DefaultLinking == DefaultLinkingEnum.Disabled)
        {
          rtn.Text += " (Disabled)";
        }
        rtn.DefaultItem = false;
      }

      return rtn;
    }   

    private void SetMenuItemsEnabled(ContextMenu menu, bool enabled)
    {
      foreach(MenuItem item in menu.MenuItems)
      {
        item.Enabled = enabled;
      }
    }

    #endregion

    #endregion

    #region Pending Colleague Subscription class 

    private class PendingColleagueSubscription
    {
      private readonly LinkedSet m_linkedSet;
      private readonly XmlNode m_state;
      public PendingColleagueSubscription( LinkedSet linkedSet, XmlNode state_ )
      {
        m_linkedSet = linkedSet;
        m_state = state_;
      }
      public LinkedSet LinkedSet { get { return m_linkedSet; } }
      public XmlNode State { get { return m_state; } }
    }

    #endregion

  }
  
}
