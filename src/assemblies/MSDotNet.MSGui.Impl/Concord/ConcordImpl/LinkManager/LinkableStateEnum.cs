﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Concord/ConcordImpl/LinkManager/LinkableStateEnum.cs#5 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

namespace MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl.LinkManager
{
  /// <summary>
  /// Describes the linkable nature for a LinkedTopic
  /// </summary>
  public enum LinkableStateEnum
  {
    /// <summary>
    /// Attempt to draw the GUI link images, but this is
    /// only possible if there are forms associated with the LinkCOntrols.
    /// </summary>
    UserLinkable,
    /// <summary>
    /// Would normally have Links made or broken programatically.
    /// </summary>
    NonUserLinkable,
    /// <summary>
    /// Do not intercept Notify messages for this Topic. If this is the specified state 
    /// in the config, no LinkedTopic will be created.
    /// </summary>
    None
  }
}
