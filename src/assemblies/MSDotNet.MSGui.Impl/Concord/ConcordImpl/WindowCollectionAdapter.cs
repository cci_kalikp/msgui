/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Concord/ConcordImpl/WindowCollectionAdapter.cs#4 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using MorganStanley.IED.Concord.Application;

namespace MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl
{
  internal class WindowCollectionAdapter : WindowCollection
  {
    internal void Add(IWindow window_)
    {
      this.InnerList.Add(window_);
    }

    internal void Remove(IWindow window_)
    {
      this.InnerList.Remove(window_);
    }
  }
}