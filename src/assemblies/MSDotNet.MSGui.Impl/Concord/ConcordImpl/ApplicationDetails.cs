﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using MSDesktop.AuthenticationService.Configuration;

namespace MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl
{
    class ApplicationDetails: IApplicationDetails
    { 
        public string Name { get; set; }
         
        public string DisplayName { get; set; }

        public string Version { get; set; }

        public ImageSource Image { get; set; }
    }
}
