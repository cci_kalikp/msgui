/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Concord/ConcordImpl/BarAdapters/CommandBarControlAdapter.cs#8 $
// $Change: 901409 $
// $DateTime: 2014/10/17 05:24:50 $
// $Author: caijin $

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows;
using System.Windows.Forms;
using MorganStanley.IED.Concord.Application;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSNet;
using MorganStanley.MSDotNet.My;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;

namespace MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl.BarAdapters
{
	internal abstract class CommandBarControlAdapter : ICommandBarControl
	{
		private static readonly IMSLogger m_logger = MSLoggerFactory.CreateLogger<CommandBarControlAdapter>();

		protected string m_name;
		protected string m_caption = string.Empty;
		protected string m_hint;
		protected bool m_enabled = true;
		protected bool m_beginGroup;
		protected Shortcut m_shortcut;
		protected Bitmap m_image;
		protected CommandBarControlType m_cbControlType;
		protected CommandBarControlStyle m_style = CommandBarControlStyle.Default;
		protected IMSNetLoop m_loop;
		protected readonly string m_registryKey;
		private readonly Dictionary<ItemClickEventHandler, IMSNetLoop> m_eventHandlers = new Dictionary<ItemClickEventHandler, IMSNetLoop>();
		public string Token = "";

		protected CommandBarControlAdapter(
		  string name_,
		  string caption_,
		  Bitmap image_,
		  ItemClickEventHandler eventHandler_,
		  IMSNetLoop callbackLoop_,
		  string registryKey_)
		{
			m_logger.DebugWithFormat("Creating CommandBarControlAdapter name = '{0}', caption = '{1}', image_ = '{2}'", name_, caption_, image_ == null ? "Null" : "NotNull");
			m_name = name_;
			m_caption = caption_;
			m_image = image_;
			m_loop = callbackLoop_;
			m_registryKey = registryKey_;
			if (eventHandler_ != null)
			{
				AddItemClickEventHandler(eventHandler_, callbackLoop_);
			}
		}

		public EventHandler Command
		{
			get
			{
				return this.ControlCommand;
			}
		}

		private void ControlCommand(object sender, EventArgs e)
		{
			KeyValuePair<ItemClickEventHandler, IMSNetLoop>[] handlers;
			lock (m_eventHandlers)
			{
				handlers = m_eventHandlers.ToArray();
			}

			foreach (KeyValuePair<ItemClickEventHandler, IMSNetLoop> current in handlers)
			{
				ItemClickEventHandler handler = current.Key;
				IMSNetLoop loop = current.Value;
				if (loop != null)
				{
					loop.Invoke(() => handler.Invoke(this, new ItemClickEventArgs(this)));
				}
				else
				{
					handler(this, new ItemClickEventArgs(this));
				}
			}
		}

		public void AddItemClickEventHandler(ItemClickEventHandler eventHandler_, IMSNetLoop callbackLoop_)
		{
			lock (m_eventHandlers)
			{
				m_eventHandlers[eventHandler_] = callbackLoop_;
			}
		}

		public void RemoveItemClickEventHandler(ItemClickEventHandler eventHandler_)
		{
			lock (m_eventHandlers)
			{
				if (m_eventHandlers.ContainsKey(eventHandler_))
				{
					m_eventHandlers.Remove(eventHandler_);
				}
			}
		}

		public bool BeginGroup
		{
			get
			{
				return m_beginGroup;
			}
			set
			{
				m_logger.DebugWithFormat("Changing BeginGroup from '{0}' to '{1}' for '{2}' '{3}'", m_beginGroup, value, m_name, m_caption);
				m_beginGroup = value;
				InvokeChanged("BeginGroup");
			}
		}

		public string Caption
		{
			get
			{
				return m_caption;
			}
			set
			{
				m_logger.DebugWithFormat("Changing Caption from '{0}' to '{1}' for '{2}' ", m_caption, value, m_name);
				m_caption = value;
				InvokeChanged("Caption");
			}
		}

		public bool Enabled
		{
			get
			{
				return m_enabled;
			}
			set
			{
				m_logger.DebugWithFormat("Changing Enabled from '{0}' to '{1}' for '{2}' '{3}'", m_enabled, value, m_name, m_caption);
				m_enabled = value;
				InvokeChanged("Enabled");
			}
		}


		public string Hint
		{
			get
			{
				return m_hint;
			}
			set
			{
				m_logger.DebugWithFormat("Changing Hint from '{0}' to '{1}' for '{2}' '{3}'", m_hint, value, m_name, m_caption);
				m_hint = value;
				InvokeChanged("Hint");
			}
		}

		public Bitmap Image
		{
			get
			{
				return m_image;
			}
			set
			{
				m_logger.DebugWithFormat("Changing Image from '{0}' to '{1}' for '{2}' '{3}'", m_image == null ? "null" : "not null", value == null ? "null" : "not null", m_name, m_caption);
				m_image = value;
				InvokeChanged("Image");
			}
		}

		public string Name
		{
			get
			{
				return m_name;
			}
			set
			{
				m_logger.DebugWithFormat("Changing Name from '{0}' to '{1}' for '{2}' ", m_name, value, m_caption);
				m_name = value;
				InvokeChanged("Name");
			}
		}

		public Shortcut ShortCut
		{
			get
			{
				return m_shortcut;
			}
			set
			{
				m_logger.DebugWithFormat("Changing ShortCut from '{0}' to '{1}' for '{2}' '{3}'", m_shortcut, value, m_name, m_caption);
				m_shortcut = value;

        if (ConcordExtensionStorage.KeyBindings.ContainsKey(this))
          ConcordExtensionStorage.KeyBindings[this].Shortcut = value;
				else
          ConcordExtensionStorage.KeyBindings.Add(this, new ConcordExtensionStorage.ShortcutMetadata() { Shortcut = value });

				InvokeChanged("ShortCut");
			}
		}

		public CommandBarControlStyle Style
		{
			get
			{
				return m_style;
			}
			set
			{
				m_logger.DebugWithFormat("Changing Style from '{0}' to '{1}' for '{2}' '{3}'", m_style, value, m_name, m_caption);
				m_style = value;
				InvokeChanged("Style");
			}
		}

		public CommandBarControlType Type
		{
			get
			{
				return m_cbControlType;
			}
			set
			{
				m_logger.DebugWithFormat("Changing ControlType from '{0}' to '{1}' for '{2}' '{3}'", m_cbControlType, value, m_name, m_caption);
				m_cbControlType = value;
				InvokeChanged("Type");
			}
		}

		protected void InvokeChanged(string propertyName_)
		{
			var copy = PropertyChanged;
			if (copy != null)
			{
				copy(this, new PropertyChangedEventArgs(propertyName_));
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		internal Action RemoveControlHandler;

		public string Key
		{
			get
			{
				return m_registryKey;
			}
		}

		//TODO
		private bool m_isControlledByConcord = true;
		public bool IsControlledByConcord
		{
			get
			{
				return m_isControlledByConcord;
			}
			set
			{
				m_isControlledByConcord = value;
			}
		}

		public abstract UIElement CreateControl();
		public abstract void ReleaseControl(UIElement control);
        public IWidgetViewContainer ParentContainer { get; set; }
	}
}
