﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Concord/ConcordImpl/BarAdapters/CommandBarPopupAdapter.cs#7 $
// $Change: 902331 $
// $DateTime: 2014/10/24 05:43:11 $
// $Author: caijin $

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows;
using System.Windows.Controls;
using MorganStanley.IED.Concord.Application; 
using MorganStanley.MSDotNet.MSGui.Impl.ControlParadigm;
using MorganStanley.MSDotNet.MSNet;
using MorganStanley.MSDotNet.My;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl.BarAdapters
{
  class CommandBarPopupAdapter : CommandBarControlAdapter, ICommandBarPopupControl
  {
    private static readonly IMSLogger m_logger = MSLoggerFactory.CreateLogger<CommandBarPopupAdapter>();

    private readonly IHostRegistry m_hostRegistry;
    private readonly BarControlsCreatorBase m_controlsCreator;
    private ItemsControlButtonsHost m_mainHost;
    private IList<IButtonsHost> m_hosts;

    public CommandBarPopupAdapter(
      string name_, 
      string caption_, 
      Bitmap image_,
      ItemClickEventHandler eventHandler_, 
      IMSNetLoop callbackLoop_, 
      IHostRegistry hostRegistry_, 
      string registryKey_,
      BarControlsCreatorBase controlsCreator_) :
      base(name_, caption_, image_, eventHandler_, callbackLoop_, registryKey_)
    {
      m_logger.DebugWithFormat("Creating CommandBarPopupAdapter name = '{0}', caption = '{1}', image_ = '{2}'", name_, caption_, image_ == null ? "Null" : "NotNull");
      m_hostRegistry = hostRegistry_;
      m_controlsCreator = controlsCreator_;
      m_adapter = new CommandBarAdapter(m_hostRegistry);
      m_mainHost = new ItemsControlButtonsHost();
      m_hosts = new List<IButtonsHost> { m_mainHost };
	  string hostKey = m_hostRegistry.AddHost(m_mainHost, ChromeRegistry.CreateCommandBarKey(m_registryKey, "CommandBar"));
      m_adapter.AttachToHost(hostKey);
    }

    private readonly CommandBarAdapter m_adapter;
    public ICommandBar CommandBar
    {
      get
      {
        return m_adapter;
      }
    }

    public event EventHandler Popup;
    public override UIElement CreateControl()
    {
      m_logger.DebugWithFormat("Creating control for ComboPopupAdapter name = '{0}'", m_name);
      UIElement control = null;
      ItemsControl itemsControl = null;
      m_loop.Invoke(() => control = m_controlsCreator.CreateMenuBar(this, Caption ?? Name, out itemsControl));
      if (m_hosts.Count == 1)
      {
        m_mainHost.SetControl(itemsControl);
      }
      else
      {
        ItemsControlButtonsHost host = new ItemsControlButtonsHost(itemsControl);
        m_hosts.Add(host);
		m_hostRegistry.AddHost(host, ChromeRegistry.CreateCommandBarKey(m_registryKey, "CommandBar"));
      }
      return control;
    }

   public override void ReleaseControl(UIElement element_)
    {
      
    }
  }
}
