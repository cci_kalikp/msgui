﻿using System; 
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives; 
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;

namespace MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl.BarAdapters
{
    internal abstract class BarControlsCreatorBase
    {
        public const string DEFAULT_TAB_NAME = "Applets"; 
         
        public virtual UIElement CreateButton(CommandBarButtonAdapter cba_)
        { 
            var element = CreateButtonCore(cba_);
            if (element == null) return null;
            var shortcutMetadata = new ConcordExtensionStorage.ShortcutMetadata()
            {
                Shortcut = System.Windows.Forms.Shortcut.None
            };

            if (ConcordExtensionStorage.KeyBindings.ContainsKey(cba_))
                ConcordExtensionStorage.KeyBindings[cba_].Command = shortcutMetadata.Command;
            else
                ConcordExtensionStorage.KeyBindings.Add(cba_, shortcutMetadata);
            shortcutMetadata.Command = new DelegateCommand(
                (
                obj_ =>
                {
                    //buttonTool.IsChecked = cba_.Checked;
                    //System.Windows.Application.Current.DoEvents();
                    ConcordExtensionStorage.ConcordCommands.Push(cba_.Token);
                    cba_.Command(cba_, EventArgs.Empty);

                    if (ConcordExtensionStorage.ConcordInitialViewSettings.Count > 0)
                    {
                        ConcordExtensionStorage.ConcordCommands.Pop();
                    }
                }
                ));
            element.SetValue(ButtonBase.CommandProperty, shortcutMetadata.Command);

            return element;
        }
         
        protected abstract UIElement CreateButtonCore(CommandBarButtonAdapter cba_);

        public abstract UIElement CreateMenuBar(CommandBarControlAdapter adapter_, string name_,
                                                out ItemsControl itemsControl_);


        public abstract UIElement CreateNormalBar(string name_); 
    }
}
