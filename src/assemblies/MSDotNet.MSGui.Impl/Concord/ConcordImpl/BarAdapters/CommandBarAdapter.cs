﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Concord/ConcordImpl/BarAdapters/CommandBarAdapter.cs#9 $
// $Change: 821591 $
// $DateTime: 2013/03/25 06:29:39 $
// $Author: boqwang $

using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using MorganStanley.IED.Concord.Application;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ControlParadigm;
using MorganStanley.MSDotNet.My;
using Point = System.Drawing.Point;

namespace MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl.BarAdapters
{
  class CommandBarAdapter : ICommandBar
  {
    private static readonly IMSLogger m_logger = MSLoggerFactory.CreateLogger<CommandBarAdapter>();
    //for backwards compatibility
    //see lines 86-88 in \\ms\d*v\otclcm\tx\2010.3.6.2\src\assemblies\TX.Infrastructure\Applet\ScopedAppletBase.cs
    private ArrayList _items = new ArrayList() { null, null };
    private readonly IHostRegistry m_hostRegistry;
    private readonly IList<string > m_keys = new List<string>();
    private string m_hostId;
    public CommandBarAdapter(IHostRegistry hostRegistry_)
    {
      m_logger.Debug("Creating new CommandBarAdapter");
      m_hostRegistry = hostRegistry_;
    }

    public void AttachToHost(string hostId_)
    {
      m_logger.DebugWithFormat("Attaching CommandBarAdapter with name = '{0}' to hostid = '{1}'", Name, hostId_);
      if (hostId_ == null)
      {
        throw new ArgumentException("hostId_ can't be null");
      }
      m_hostId = hostId_;
      foreach (var key in m_keys)
      {
        m_hostRegistry.AddRecommendation(key, hostId_, null);
      }
      m_keys.Clear();
    }

    public void AddCommand(ICommandBarControl control_)
    {
      if (control_ == null)
      {
        m_logger.Warning("Adding command. command == null");
      }
      else
      {
        m_logger.DebugWithFormat("Adding command name = '{0}', caption = '{1}'", control_.Name, control_.Caption);
      }

      if (control_ is CommandBarControlAdapter && ((CommandBarControlAdapter) control_).IsControlledByConcord)
      {
        string key = ((CommandBarControlAdapter)control_).Key;
		((CommandBarControlAdapter)control_).Token = key;
		if (m_hostId != null)
        {
            InitialWidgetParameters parameters = null;
            if (BackwardCompatibilityDropDownHandling)
            {
                if (control_ is ICommandBarDropDownButton || control_ is ICommandBarPopupControl)
                    parameters = new InitialDropdownButtonParameters
                                     {
                                         Text = control_.Caption
                                     };
            }
            m_hostRegistry.AddRecommendation(key, m_hostId, parameters);
        }
        else
        {
          m_keys.Add(key);
        }
      }
    }

    public void InsertCommand(int index_, ICommandBarControl control_)
    {
      if (control_ == null)
      {
        m_logger.Warning("Inserting command. command == null");
      }
      else
      {
        m_logger.DebugWithFormat("Inserting command name = '{0}', caption = '{1}', index = '{2}'", control_.Name, control_.Caption, index_);
      }
      //Add, because Insert may not work - we might have not all indexes
      AddCommand(control_);
    }

    public void RemoveCommand(ICommandBarControl control_)
    {
      if (control_ == null)
      {
        m_logger.Warning("Removing command. command == null");
      }
      else
      {
        m_logger.DebugWithFormat("Removing command name = '{0}', caption = '{1}'", control_.Name, control_.Caption);
      }
      if (control_ is CommandBarControlAdapter && ((CommandBarControlAdapter)control_).IsControlledByConcord)
      {
        string key = ((CommandBarControlAdapter)control_).Key;
        if (m_hostId != null)
        {
          m_hostRegistry.RemoveRecommendation(key);
        }
        else
        {
          m_keys.Remove(key);
        }
      }
    }

    public void Clear()
    {
      m_logger.Debug("Clear called");
      if (m_hostId != null)
      {
        m_hostRegistry.ClearRecommendationsForHost(m_hostId);
      }
      else
      {
        m_keys.Clear();
      }      
    }

    public Color BackColor
    {
      get
      {
        return Color.Empty;
      }
      set
      {
        m_logger.Debug("Changing BackColor of a CommandBar");
        //Do nothing. Color is not supported.
      }
    }

    private string m_name;
    public string Name
    {
      get
      {
        return m_name;
      } 
      set
      {
        m_logger.DebugWithFormat("Changing Name from '{0}' to '{1}'", m_name, value);
        m_name = value;
      }
    }

    private bool m_visible;
    public bool Visible
    {
      get
      {
        return m_visible;
      }
      set
      {
        m_logger.DebugWithFormat("Changing Visibility from '{0}' to '{1}' for '{2}'", m_visible, value, m_name);
        m_visible = value;        
      }
    }

    private CommandBarType m_commandBarType;
      public static bool BackwardCompatibilityDropDownHandling = false;

      public CommandBarType Type
    {
      get
      {
        return m_commandBarType;
      }
    }

    public void Popup(Control parent_, Point point_)
    {
      m_logger.DebugWithFormat("Popup called for commandBar '{0}'", m_name);
    }
  }
}
