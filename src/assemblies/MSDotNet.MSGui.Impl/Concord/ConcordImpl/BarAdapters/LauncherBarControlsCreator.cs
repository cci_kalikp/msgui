﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls; 
using MorganStanley.IED.Concord.Application;
using MorganStanley.MSDotNet.MSGui.Controls;
using MorganStanley.MSDotNet.MSGui.Controls.BarMenu;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.ViewContainers; 

namespace MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl.BarAdapters
{
    class LauncherBarControlsCreator:BarControlsCreatorBase
    { 

        protected override UIElement CreateButtonCore(CommandBarButtonAdapter cba_)
        {
            if (!(cba_.ParentContainer is IDropdownButtonViewContainer))
            {
                var buttonTool = new BarToggleButton() { AllowToggle = false };
                buttonTool.IsChecked = cba_.Checked;
                buttonTool.Name = Display.FormatName(cba_.Name);
                buttonTool.Caption = Display.FormatMenuCaption(cba_.Caption);
                buttonTool.ToolTip = cba_.Hint ?? Display.FormatCaption(cba_.Caption);
                buttonTool.IsEnabled = cba_.Enabled;
                if (cba_.Image != null && cba_.Style != CommandBarControlStyle.Caption)
                {
                    buttonTool.Image = cba_.Image.ToImageSource();
                }

                buttonTool.SetResourceReference(Control.ForegroundProperty, MSGuiColors.TextFillBrushKey);
                // code duplicate below and above, sorry
                cba_.PropertyChanged +=
                    (sender, e) =>
                    {
                        switch (e.PropertyName)
                        {
                            case "Caption":
                                buttonTool.Caption = Display.FormatMenuCaption(cba_.Caption);
                                break;
                            case "Enabled":
                                buttonTool.IsEnabled = cba_.Enabled;
                                break;
                            case "Hint":
                                buttonTool.ToolTip = cba_.Hint ?? Display.FormatCaption(cba_.Caption);
                                break;
                            case "Image":
                                if (cba_.Image != null && cba_.Style != CommandBarControlStyle.Caption)
                                {
                                    buttonTool.Image = cba_.Image.ToImageSource();
                                }
                                break;
                            case "Name":
                                break;
                            case "Checked":
                                buttonTool.IsChecked = cba_.Checked;
                                break;
                        }
                    }; 
                return buttonTool; 
            }
            else
            {
                var menuItem = new ToolMenuItem()
                {
                    IsChecked = cba_.Checked,
                    //IsCheckable = true,
                    ToolTip = Display.FormatMenuCaption(cba_.Caption),
                    Header = Display.FormatMenuCaption(cba_.Caption),
                    IsEnabled = cba_.Enabled,
                };


                if (cba_.Image != null && cba_.Style != CommandBarControlStyle.Caption)
                {
                    menuItem.Icon = new Image() { Source = cba_.Image.ToImageSource() };
                }

                // code duplicate below and above, sorry
                cba_.PropertyChanged +=
                    (sender, e) =>
                    {
                        switch (e.PropertyName)
                        {
                            case "Caption":
                                menuItem.Header = Display.FormatMenuCaption(cba_.Caption);
                                break;
                            case "Enabled":
                                menuItem.IsEnabled = cba_.Enabled;
                                break;
                            case "Hint":
                                menuItem.ToolTip = cba_.Hint ?? Display.FormatCaption(cba_.Caption);
                                break;
                            case "Image":
                                if (cba_.Image != null && cba_.Style != CommandBarControlStyle.Caption)
                                {
                                    menuItem.Icon = new Image() { Source = cba_.Image.ToImageSource() };
                                }
                                break;
                            case "Name":
                                break;
                            case "Checked":
                                menuItem.IsChecked = cba_.Checked;
                                break;
                        }
                    };
                 
                return menuItem;
            }
        }

        public override UIElement CreateMenuBar(CommandBarControlAdapter adapter_, string name_, out ItemsControl itemsControl_)
        {
            if (adapter_.ParentContainer is IDropdownButtonViewContainer)
            {
                var menuItem = new ToolMenuItem() { Header = name_ };
                if (adapter_.Image != null)
                {
                    menuItem.Icon = new Image() { Source = adapter_.Image.ToImageSource() };
                }
                itemsControl_ = menuItem;
                return menuItem;

            }
            else
            {
                var barMenuTool = new BarMenuTool
                {
                    Caption = name_
                };

                if (adapter_.Image != null)
                {
                    barMenuTool.Image = adapter_.Image.ToImageSource();
                }
                barMenuTool.SetResourceReference(Control.ForegroundProperty, MSGuiColors.TextFillBrushKey);
                itemsControl_ = barMenuTool;
                return barMenuTool;
            }
        }

        public override UIElement CreateNormalBar(string name_)
        {
            Panel panel = new WrapPanel() { Orientation = Orientation.Vertical };
            GroupBox box = new GroupBox() { Header = name_, Content = panel };

            return box;
        }
      
    }
}
