﻿////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Concord/ConcordImpl/BarAdapters/CommandBarsAdapter.cs#11 $
// $Change: 902331 $
// $DateTime: 2014/10/24 05:43:11 $
// $Author: caijin $

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using MorganStanley.IED.Concord.Application;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ControlParadigm;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.MSNet;
using MorganStanley.MSDotNet.My;

namespace MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl.BarAdapters
{
    internal class CommandBarsAdapter : ICommandBars
    {
        private static readonly IMSLogger m_logger = MSLoggerFactory.CreateLogger<CommandBarsAdapter>();

        private readonly ConcordApplication m_concordApplication;
        private readonly IControlParadigmRegistrator m_registrator;
        private readonly IHostRegistry m_hostRegistry;
        private readonly IMSNetLoop m_loop;
        private readonly BarControlsCreatorBase m_controlsCreator;
        private readonly IList<CommandBarAdapter> m_adapters = new List<CommandBarAdapter>();

        public CommandBarsAdapter(
          ConcordApplication concordApplication_,
          IControlParadigmRegistrator registrator_,
          IHostRegistry hostRegistry_,
          IMSNetLoop loop_,
          ShellMode shellMode_)
        {
            m_logger.Debug("Creating new CommandBarsAdapter");
            m_concordApplication = concordApplication_;
            m_registrator = registrator_;
            m_hostRegistry = hostRegistry_;
            m_loop = loop_; //currently unused TODO check potential places where it could have been used. esp. situations when buttons happen to be created from a non-gui thread 
            m_controlsCreator = shellMode_ == ShellMode.LauncherBarAndFloatingWindows ||
                                shellMode_ == ShellMode.LauncherBarAndWindow
                                    ? new LauncherBarControlsCreator()
                                    : new RibbonWindowControlsCreator() as BarControlsCreatorBase;
        }



        public ICommandBarControl AddNamedCommand(
          string name_, string caption_, Bitmap image_, CommandBarControlType type_,
          ItemClickEventHandler eventHandler_, IMSNetLoop callbackLoop_)
        {
            //System.Diagnostics.Debugger.Break();
            m_logger.DebugWithFormat("Adding new NamedCommand name = '{0}', caption = '{1}', image_ = '{2}', type = '{3}'", name_, caption_, image_ == null ? "Null" : "NotNull", type_.GetType().Name);
             string key = name_;

            if ((key == "-") || (caption_ == "-"))
                return (CommandBarControlAdapter)null;
            while (m_registrator.IsRegistered(key))
            {
                key += "_";
            }

            CommandBarControlAdapter commandBarControl = null;
            switch (type_)
            {
                case CommandBarControlType.Button:
                    commandBarControl = new CommandBarButtonAdapter(name_, caption_, image_, eventHandler_, callbackLoop_, key, m_controlsCreator);
                    break;
                case CommandBarControlType.ComboBox:
                    commandBarControl = new CommandBarComboBoxAdapter(name_, caption_, image_, eventHandler_, callbackLoop_, m_hostRegistry, key, m_controlsCreator);
                    break;
                case CommandBarControlType.DropDown:
                    commandBarControl = new CommandBarDropDownAdapter(name_, caption_, image_, eventHandler_, callbackLoop_, m_hostRegistry, key, m_controlsCreator);
                    break;
                case CommandBarControlType.Popup:
                    commandBarControl = new CommandBarPopupAdapter(name_, caption_, image_, eventHandler_, callbackLoop_, m_hostRegistry, key, m_controlsCreator);
                    break;
                default:
                    throw new ArgumentOutOfRangeException("type_");
            }

            if (commandBarControl is CommandBarDropDownAdapter)
            {
                (m_registrator as ChromeRegistry).RegisterDropdown(key, delegate(IWidget widget_)
                    {
                        commandBarControl.ParentContainer = widget_.Parent;
                    widget_.MinimizedView = commandBarControl.CreateControl();
                });
            }
            else
            {
                m_registrator.Register(key, delegate(IWidget widget_)
                {
                    commandBarControl.ParentContainer = widget_.Parent;
                    widget_.MinimizedView = commandBarControl.CreateControl();
                });
            }
            return commandBarControl;
        }

        
        
        public void RemoveCommand(ICommandBarControl control_)
        {
            //TODO not implemented (and not used anywhere that I know of)
            if (control_ == null)
            {
                m_logger.Warning("Removing command. command == null");
            }
            else
            {
                m_logger.DebugWithFormat("Removing command name = '{0}', caption = '{1}'", control_.Name, control_.Caption);
            }
        }

        public ICommandBar AddCommandBar(string name_, CommandBarType type_)
        {
            m_logger.DebugWithFormat("Adding CommandBar name = '{0}', type = '{1}'", name_, type_.GetType().Name);
            foreach (CommandBarAdapter ad in m_adapters)
            {
                if (ad.Name == name_)
                {
                    return ad;
                }
            }
            if (name_ == "Tools")
            {
                type_ = CommandBarType.Menu;
            }
            CommandBarAdapter adapter = new CommandBarAdapter(m_hostRegistry) { Name = name_ };
            m_adapters.Add(adapter);
            string hostId = null;
            switch (type_)
            {
                case CommandBarType.Menu:
                    hostId = m_hostRegistry.AddMenuRootHost(name_);
                    break;
                case CommandBarType.Normal:
                    hostId = m_hostRegistry.AddRootHost(name_);
                    break;
                case CommandBarType.Popup:
                    break;
            }

            if (hostId != null)
            {
                adapter.AttachToHost(hostId);
            }
            return adapter;
        }

        public void RemoveCommandBar(ICommandBar bar_)
        {
            //TODO not implemented
            if (bar_ == null)
            {
                m_logger.Warning("Removing command bar. bar_ == null");
            }
            else
            {
                m_logger.DebugWithFormat("Removing command bar. name = '{0}', type = '{1}'", bar_.Name, bar_.Type);
            }
        }

        public int Count
        {
            get
            {
                m_logger.Debug("Requesting command bars Count");
                return 0;
            }
        }

        ICommandBar ICommandBars.this[int index_]
        {
            get
            {
                m_logger.DebugWithFormat("Requesting command bar with index '{0}'", index_);
                return m_adapters[index_];
            }
        }

        ICommandBar ICommandBars.this[string name_]
        {
            get
            {
                m_logger.DebugWithFormat("Requesting command bar with name '{0}'", name_);
                foreach (CommandBarAdapter adapter in m_adapters)
                {
                    if (adapter.Name == name_)
                    {
                        return adapter;
                    }
                }
                return AddCommandBar(name_, CommandBarType.Normal);
            }
        }
    }
}
