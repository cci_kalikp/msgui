﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Infragistics.Windows.Ribbon;
using MorganStanley.IED.Concord.Application;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;  
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.Converters;

namespace MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl.BarAdapters
{
    class RibbonWindowControlsCreator:BarControlsCreatorBase
    { 

        protected override UIElement CreateButtonCore(CommandBarButtonAdapter cba_)
        {
            var buttonTool = new UnCheckableToggleButtonTool();
            buttonTool.IsChecked = cba_.Checked;
            buttonTool.Name = Display.FormatName(cba_.Name);
            buttonTool.Caption = Display.FormatMenuCaption(cba_.Caption);
            buttonTool.ToolTip = cba_.Hint ?? Display.FormatCaption(cba_.Caption);
            buttonTool.IsEnabled = cba_.Enabled;
            if (cba_.Image != null && cba_.Style != CommandBarControlStyle.Caption)
            {
                buttonTool.SmallImage = buttonTool.LargeImage = cba_.Image.ToImageSource();
            }
            switch (cba_.Style)
            {
                case CommandBarControlStyle.Default:
                case CommandBarControlStyle.IconAndCaption:
                    buttonTool.SetValue(RibbonGroup.MaximumSizeProperty, RibbonToolSizingMode.ImageAndTextNormal);
                    break;
                case CommandBarControlStyle.Caption:
                    cba_.Image = null;
                    buttonTool.SetValue(RibbonGroup.MaximumSizeProperty, RibbonToolSizingMode.ImageAndTextNormal);
                    break;
                case CommandBarControlStyle.Icon:
                    buttonTool.SetValue(RibbonGroup.MaximumSizeProperty, RibbonToolSizingMode.ImageOnly);
                    break;
            }

            // code duplicate below and above, sorry
            cba_.PropertyChanged +=
                (sender, e) =>
                {
                    switch (e.PropertyName)
                    {
                        case "Caption":
                            buttonTool.Caption = Display.FormatMenuCaption(cba_.Caption);
                            break;
                        case "Enabled":
                            buttonTool.IsEnabled = cba_.Enabled;
                            break;
                        case "Hint":
                            buttonTool.ToolTip = cba_.Hint ?? Display.FormatCaption(cba_.Caption);
                            break;
                        case "Image":
                            if (cba_.Image != null && cba_.Style != CommandBarControlStyle.Caption)
                            {
                                buttonTool.SmallImage =
                                    buttonTool.LargeImage = cba_.Image.ToImageSource();
                            }
                            break;
                        case "Name":
                            break;
                        case "Style":
                            switch (cba_.Style)
                            {
                                case CommandBarControlStyle.Default:
                                case CommandBarControlStyle.IconAndCaption:
                                    buttonTool.SetValue(RibbonGroup.MaximumSizeProperty,
                                                        RibbonToolSizingMode.ImageAndTextNormal);
                                    break;
                                case CommandBarControlStyle.Caption:
                                    cba_.Image = null;
                                    buttonTool.SetValue(RibbonGroup.MaximumSizeProperty,
                                                        RibbonToolSizingMode.ImageAndTextNormal);
                                    break;
                                case CommandBarControlStyle.Icon:
                                    buttonTool.SetValue(RibbonGroup.MaximumSizeProperty,
                                                        RibbonToolSizingMode.ImageOnly);
                                    break;
                            }
                            break;
                        case "Checked":
                            buttonTool.IsChecked = cba_.Checked;
                            break;
                    }
                };
            return buttonTool;
        }

        public override UIElement CreateMenuBar(CommandBarControlAdapter adapter_, string name_, out ItemsControl itemsControl_)
        {
            MenuTool menuTool = new MenuTool
            {
                Caption = name_,
                ButtonType = MenuToolButtonType.DropDown,
            };
            if (adapter_.Image != null)
            {
                menuTool.SmallImage = adapter_.Image.ToImageSource();
            }
            menuTool.SetValue(RibbonGroup.MaximumSizeProperty, RibbonToolSizingMode.ImageAndTextNormal);
            itemsControl_ = menuTool;
            return menuTool;
        }

        public override UIElement CreateNormalBar(string name_)
        {
            RibbonGroup ribbonGroup = CreateRibbonGroup(name_);
            return ribbonGroup;
        }
        
        //todo: move me to the ribbon helper class. or not?
        public static RibbonGroup CreateRibbonGroup(string name_)
        {
            RibbonGroup ribbonGroup = new RibbonGroup { Caption = Display.FormatCaption(name_) };
            Binding binding = new Binding("Items.Count")
            {
                RelativeSource = RelativeSource.Self,
                Converter = new VisibilityConverter()
            };
            ribbonGroup.SetBinding(RibbonGroup.VisibilityProperty, binding);
            return ribbonGroup;
        }
    }
}
