﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Concord/ConcordImpl/BarAdapters/CommandBarButtonAdapter.cs#7 $
// $Change: 902331 $
// $DateTime: 2014/10/24 05:43:11 $
// $Author: caijin $

using System.Drawing;
using System.Windows;
using MorganStanley.IED.Concord.Application; 
using MorganStanley.MSDotNet.MSNet;

namespace MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl.BarAdapters
{
  class CommandBarButtonAdapter : CommandBarControlAdapter, ICommandBarButton
  {
      private readonly BarControlsCreatorBase m_controlsCreator;
    private bool m_checked;

    public CommandBarButtonAdapter(string name_, string caption_, Bitmap image_,
      ItemClickEventHandler eventHandler_, IMSNetLoop callbackLoop_, string key_, BarControlsCreatorBase controlsCreator_) :
      base(name_, caption_, image_, eventHandler_, callbackLoop_, key_)
    {
      m_controlsCreator = controlsCreator_;
    }

    public bool Checked
    {
      get
      {
        return m_checked;
      }
      set
      {
        m_checked = value;
        InvokeChanged("Checked");
      }
    }

    public override UIElement CreateControl()
    {
      UIElement result  = null;
      m_loop.Invoke(() => result = m_controlsCreator.CreateButton(this));
      return result;
    }

    public override void ReleaseControl(UIElement element_)
    {
      
    }
  }
}
