/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Concord/ConcordImpl/ConcordApplication.cs#44 $
// $Change: 886670 $
// $DateTime: 2014/07/01 05:24:47 $
// $Author: milosp $

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using System.Xml;
using Microsoft.Practices.Unity;
using MorganStanley.IED.Concord.Application;
using MorganStanley.IED.Concord.Configuration;
using MorganStanley.IED.Concord.Logging;
using MorganStanley.IED.Concord.Runtime;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MorganStanley.MSDotNet.MSLog;
using MorganStanley.MSDotNet.MSNet;
using MorganStanley.MSDotNet.My;
using ConfigurationManager = MorganStanley.IED.Concord.Configuration.ConfigurationManager;
using StatusLevel = MorganStanley.IED.Concord.Application.StatusLevel;
using StatusUpdatedEventArgs = MorganStanley.IED.Concord.Application.StatusUpdatedEventArgs;
using StatusUpdatedEventHandler = MorganStanley.IED.Concord.Application.StatusUpdatedEventHandler;
using System.Windows;
using ConfigurationSection = System.Configuration.ConfigurationSection;

namespace MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl
{
	internal class ConcordApplication : IConcordApplication, IEntitlementsProvider
	{
		private static readonly IMSLogger m_logger = MSLoggerFactory.CreateLogger<ConcordApplication>();

		private readonly Hashtable m_entitlements;
		private readonly ConcordAppletCollection m_applets;

		private readonly IStatusBar m_statusBar;
		private readonly IApplicationOptions m_options;
		private readonly PersistenceProfileService m_persistenceProfileService;
	    private readonly IModuleLoadInfos m_moduleLoadInfos;

	    private Arguments m_arguments;

		private IWindowManager m_windowManager;
		private IMSNetLoop m_loop;
		private ICommandBars m_commandbars;

		private Form m_fakeMainForm = Bootstrapper.ConcordFakeFormShortcut;
		internal static IApplication m_msguiApplication;

		private Configurator m_mainConfiguration;
		private bool m_initCalled = false;
		private static MSLogFileDestination m_logFile;

		private const string APPLETS_ENTITLEMENTS_ID = "Concord.Host.Applets";
		private const string ENTITLEMENTS_ROOT_NAME = "entitlements";
		private const string ENTITLEMENTS_PROVIDERS = "entitlementProviders/entitlementProvider";
		private const string ENTITLEMENTS_PROVIDER_NAMES = "names/name";
		private const string APPLETS_NODE_NAME = "applets";
		private const string CONCORD_CONFIG_NAME = "Concord";


	    /// <summary>
	    /// Initializes a new instance of the <see cref="ConcordApplication"/> class.
	    /// </summary>
	    /// <param name="container_">The container.</param>
	    /// <param name="persistenceProfileService_">The persistence profile service.</param>
	    public ConcordApplication(IUnityContainer container_, PersistenceProfileService persistenceProfileService_)
		{
			m_statusBar = container_.Resolve<IStatusBar>();
			m_options = container_.Resolve<IApplicationOptions>();
			m_msguiApplication = container_.Resolve<IApplication>();
	        m_moduleLoadInfos = container_.Resolve<IModuleLoadInfos>();
			if (!(m_msguiApplication is Application.Application))
			{
				throw new InvalidOperationException("IApplication must be Application");
			}

			m_persistenceProfileService = persistenceProfileService_;

	        m_entitlements = new Hashtable();
			m_entitlements["Default"] = new NullEntitlements();
			m_applets = new ConcordAppletCollection(this);

			m_statusBar.MainItem.StatusUpdated += OnStatusUpdated;


			//We need these for the concord applications that depend on the mainform's bounds to work.
			if (System.Windows.Application.Current.MainWindow != null)
			{
				System.Windows.Window window = System.Windows.Application.Current.MainWindow;

				window.LocationChanged += (a, b) =>
																		{
																			//m_fakeMainForm.ShowInTaskbar = false;
																			//m_fakeMainForm.Show();
																			//m_fakeMainForm.Location = new System.Drawing.Point((int) window.Left, (int) window.Top);
																			x_fieldInfo.SetValue(m_fakeMainForm, (int)window.Left);
																			y_fieldInfo.SetValue(m_fakeMainForm, (int)window.Top);
																			//m_fakeMainForm.SendToBack();
																			//m_fakeMainForm.Hide();
																			//m_fakeMainForm.SendToBack();
																		};

				window.SizeChanged += (a, b) =>
				{
					//m_fakeMainForm.ShowInTaskbar = false;
					//m_fakeMainForm.Show();

					//m_fakeMainForm.Width = (int)window.Width;
					//m_fakeMainForm.Height = (int)window.Height;
					width_fieldInfo.SetValue(m_fakeMainForm, (int)window.Width);
					height_fieldInfo.SetValue(m_fakeMainForm, (int)window.Height);
					//m_fakeMainForm.SendToBack();
					//m_fakeMainForm.Hide();
					//m_fakeMainForm.SendToBack();

				};

				window.StateChanged += (a, b) =>
				{
					//m_fakeMainForm.ShowInTaskbar = false;
					//m_fakeMainForm.Show();

					BitVector32.Section section = (BitVector32.Section)ReflHelper.StaticFieldGet(typeof(Form), "FormStateWindowState");
					BitVector32 formState = (BitVector32)formState_fieldInfo.GetValue(m_fakeMainForm);


					switch (window.WindowState)
					{
						case System.Windows.WindowState.Normal:
							formState[section] = (int)FormWindowState.Normal;
							break;
						case System.Windows.WindowState.Minimized:
							formState[section] = (int)FormWindowState.Minimized;
							break;
						case System.Windows.WindowState.Maximized:
							formState[section] = (int)FormWindowState.Maximized;
							break;
					}

					//var bv = BitVector32.CreateSection(2,
					//                                   BitVector32.CreateSection(4,
					//                                                             BitVector32.CreateSection(1,
					//                                                                                       BitVector32.CreateSection(
					//                                                                                        1,
					//                                                                                        BitVector32.CreateSection(
					//                                                                                          1,
					//                                                                                          BitVector32.CreateSection(
					//                                                                                            1,
					//                                                                                            BitVector32.CreateSection
					//                                                                                              (1,
					//                                                                                               BitVector32.
					//                                                                                                CreateSection(1,
					//                                                                                                              BitVector32
					//                                                                                                                .
					//                                                                                                                CreateSection
					//                                                                                                                (1,
					//                                                                                                                 BitVector32
					//                                                                                                                  .
					//                                                                                                                  CreateSection
					//                                                                                                                  (6,
					//                                                                                                                   BitVector32
					//                                                                                                                    .
					//                                                                                                                    CreateSection
					//                                                                                                                    (1)))))))))));



					//m_fakeMainForm.SendToBack();
					//m_fakeMainForm.Opacity = 0.00;
					//m_fakeMainForm.FormBorderStyle = FormBorderStyle.None;
					//m_fakeMainForm.ShowIcon = false;
					//m_fakeMainForm.SendToBack();
					//m_fakeMainForm.Hide();
					//m_fakeMainForm.SendToBack();
				};

				RoutedEventHandler eh = null;
				eh = (a, b) =>
				{
					//m_fakeMainForm.ShowInTaskbar = false;
					//m_fakeMainForm.Show();

					BitVector32.Section section = (BitVector32.Section)ReflHelper.StaticFieldGet(typeof(Form), "FormStateWindowState");
					BitVector32 formState = (BitVector32)formState_fieldInfo.GetValue(m_fakeMainForm);

					switch (window.WindowState)
					{
						case WindowState.Minimized:
							formState[section] = (int)FormWindowState.Minimized;
							break;
						case WindowState.Normal:
							formState[section] = (int)FormWindowState.Normal;
							break;
						case WindowState.Maximized:
							formState[section] = (int)FormWindowState.Maximized;
							break;
					};
					//m_fakeMainForm.SendToBack();
					//m_fakeMainForm.Hide();
					//m_fakeMainForm.SendToBack();
					window.Loaded -= eh;
				};
				window.Loaded += eh;
			}
		}

		/// <summary>
		/// Called when the status is updated.
		/// </summary>
		/// <param name="sender_">The sender.</param>
		/// <param name="e_">The <see cref="MorganStanley.MSDotNet.MSGui.Core.StatusUpdatedEventArgs"/> instance containing the event data.</param>
		void OnStatusUpdated(object sender_, Core.StatusUpdatedEventArgs e_)
		{
			StatusLevel status = StatusLevel.Information;
			switch (e_.Level)
			{
				case Core.StatusLevel.Information:
					status = StatusLevel.Information;
					break;
				case Core.StatusLevel.Critical:
					status = StatusLevel.Critical;
					break;
			}

			StatusUpdatedEventHandler temp = StatusUpdated;
			if (temp != null)
			{
				temp(this, new StatusUpdatedEventArgs(status, e_.Text));
			}
		}

		/// <summary>
		/// Initializes the application.
		/// </summary>
		/// <param name="windowManager_">The window manager.</param>
		/// <param name="loop_">The netloop.</param>
		/// <param name="commandbars_">The command bars.</param>
		public void Init(IWindowManager windowManager_, IMSNetLoop loop_, ICommandBars commandbars_)
		{
			m_windowManager = windowManager_;
			m_loop = loop_;
			m_commandbars = commandbars_;
		}

		public void AddOptionPage(ConcordAppletBase applet_, string title_, OptionPageBase page_)
		{
			AddOptionPage(applet_.Title, title_, page_);
		}

		public void AddOptionPage(string group_, string title_, OptionPageBase page_)
		{
            m_options.AddOptionPage(group_, title_, new OptionPageAdapter(page_)); 
		}

		/// <summary>
		/// Initializes this instance.
		/// </summary>
		public void Initialize()
		{
			if (m_windowManager == null && m_commandbars == null && m_loop == null)
			{
				throw new InvalidOperationException("This implementation requires Init to be called before Initialize");
			}
			ConcordRuntime.Current.Context["Entitlements"] = this;
			m_initCalled = true;
		}

		internal string concordDefaultProfile;

		private static List<string> m_uiOnlyList = new List<string>
			(new[] { "SurfaceMonitorApplet" });

		private readonly FieldInfo x_fieldInfo = typeof(Control).GetField("x",
																																			 BindingFlags.FlattenHierarchy |
																																			 BindingFlags.Public |
																																			 BindingFlags.NonPublic |
																																			 BindingFlags.Static |
																																			 BindingFlags.Instance);

		private readonly FieldInfo y_fieldInfo = typeof(Control).GetField("y",
																																			 BindingFlags.FlattenHierarchy |
																																			 BindingFlags.Public |
																																			 BindingFlags.NonPublic |
																																			 BindingFlags.Static |
																																			 BindingFlags.Instance);

		private readonly FieldInfo width_fieldInfo = typeof(Control).GetField("width",
																																					 BindingFlags.
																																						FlattenHierarchy |
																																					 BindingFlags.Public |
																																					 BindingFlags.NonPublic |
																																					 BindingFlags.Static |
																																					 BindingFlags.Instance);

		private readonly FieldInfo height_fieldInfo = typeof(Control).GetField("height",
																																						BindingFlags.FlattenHierarchy |
																																						BindingFlags.Public |
																																						BindingFlags.NonPublic | BindingFlags.Static |
																																						BindingFlags.Instance);

		private readonly FieldInfo formState_fieldInfo = typeof(Form).GetField("formState",
																																						BindingFlags.FlattenHierarchy |
																																						BindingFlags.Public |
																																						BindingFlags.NonPublic | BindingFlags.Static |
																																						BindingFlags.Instance);

		internal static bool DoNotReparseVersionNumber;
        internal static bool EnableSuppressLogSetup;
        internal static bool EnableForceSuppressLogSetup;

		internal static void AddToUIOnlyList(string appletName)
		{
			m_uiOnlyList.Add(appletName);
		}

		internal string ConcordDefaultProfile
		{
			get { return concordDefaultProfile; }
			set { concordDefaultProfile = value; }
		}
		internal static void StartLog(bool loggingEnabled)
		{
            if (EnableSuppressLogSetup && (MSLogConfig.WasInitialized() || EnableForceSuppressLogSetup))
                return;

			int numberOfDays = 30;
			if (ConfigurationSettings.AppSettings["AppDeleteLogFilesOlderThan"] != null)
			{
				if (!Int32.TryParse(ConfigurationSettings.AppSettings["AppDeleteLogFilesOlderThan"], out numberOfDays))
					numberOfDays = 30;
			}
			ConcordLoggingHelper.PurgeOldLogFiles(numberOfDays);

            m_logFile = ConcordLoggingHelper.SetupLogging(loggingEnabled);
		}
		/// <summary>
		/// Runs the application.
		/// </summary>
		/// <param name="args_">The arguments.</param>
		public void Run(string[] args_)
		{
			if (!m_initCalled)
			{
				throw new InvalidOperationException("This implementation requires Initialize to be called before Run");
			}

			m_arguments = new Arguments(args_);
			m_logger.Info(m_arguments.ToString());
			MorganStanley.IED.Concord.Logging.Log.Info.SetLayer("msdotnet", "msdesktop").SetLocation("MSDesktop.ConcordLoader", "StartLog()").
				Write(m_arguments.ToString());

			UpdateStatus(StatusLevel.Information, "Loading Concord Configuration...");
			LoadConfiguration(concordDefaultProfile);

			ILinkManager linkManager = CreateLinkManagerInstance();

			((ConcordWindowManager)m_windowManager).LinkManager = linkManager;

			UpdateStatus(StatusLevel.Information, "Loading Concord Applets...");
			LoadApplets();
		}

		/// <summary>
		/// Clears the status.
		/// </summary>
		public void ClearStatus()
		{
			m_statusBar.MainItem.ClearStatus();
		}

		/// <summary>
		/// Updates the status.
		/// </summary>
		/// <param name="level_">The level_.</param>
		/// <param name="statusText_">The status text_.</param>
		public void UpdateStatus(StatusLevel level_, string statusText_)
		{
			switch (level_)
			{
				case StatusLevel.Information:
					m_statusBar.MainItem.UpdateStatus(Core.StatusLevel.Information, statusText_);
					break;
				case StatusLevel.Critical:
					m_statusBar.MainItem.UpdateStatus(Core.StatusLevel.Critical, statusText_);
					break;
			}
		}

		/// <summary>
		/// Quits.
		/// </summary>
		public void Quit()
		{
			if (MSLog.MSLog.Mode == MSLog.MSLog.MSLogMode.Async)
			{
				MSLog.MSLog.Stop();
			}
			if (m_logFile != null)
			{
				m_logFile.Dispose();
			}

			m_msguiApplication.Quit();
		}

		/// <summary>
		/// Gets the configuration.
		/// </summary>
		public MorganStanley.IED.Concord.Configuration.Configurator Configuration
		{
			get
			{
				return ConfigurationManager.GetConfig(CONCORD_CONFIG_NAME) as Configurator;
			}
		}

		/// <summary>
		/// Gets the command line.
		/// </summary>
		public string[] CommandLine
		{
			get
			{
				return m_msguiApplication.CommandLine;
			}
		}

		/// <summary>
		/// Gets the command bars.
		/// </summary>
		public ICommandBars CommandBars
		{
			get
			{
				return m_commandbars;
			}
		}

		/// <summary>
		/// Gets the concord applets.
		/// </summary>
		public ConcordAppletCollection ConcordApplets
		{
			get
			{
				return m_applets;
			}
		}

		/// <summary>
		/// Gets the main loop.
		/// </summary>
		public IMSNetLoop MainLoop
		{
			get
			{
				return m_loop;
			}
		}

		/// <summary>
		/// Gets the main form.
		/// </summary>
		public Form MainForm
		{
			get
			{
				return m_fakeMainForm;
			}
		}

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>
		/// The name.
		/// </value>
		public string Name
		{
			get
			{
				return m_msguiApplication.Name;
			}
			set
			{
				((Application.Application)m_msguiApplication).Name = value;
			}
		}

		/// <summary>
		/// Gets or sets the version.
		/// </summary>
		/// <value>
		/// The version.
		/// </value>
		public string Version
		{
			get
			{
				return m_msguiApplication.Version.ToString();
			}
			set
			{
				var application = (Application.Application) m_msguiApplication;

				if (DoNotReparseVersionNumber)
				{
					application.Version = value;
				}
				else
				{

					int v = 0;
					string[] numbers = value.Split(new[] {'.'}).SkipWhile(number_ => !int.TryParse(number_, out v)).ToArray();

					try
					{
						switch (numbers.Length)
						{
							case 1:
								application.Version = new Version(int.Parse(numbers[0]), 0).ToString();
								break;
							case 2:
								application.Version = new Version(int.Parse(numbers[0]), int.Parse(numbers[1])).ToString();
								break;
							case 3:
								application.Version =
									new Version(int.Parse(numbers[0]), int.Parse(numbers[1]), int.Parse(numbers[2])).ToString();
								break;
							case 4:
								application.Version =
									new Version(int.Parse(numbers[0]), int.Parse(numbers[1]), int.Parse(numbers[2]), int.Parse(numbers[3])).
										ToString();
								break;
						}
					}
					catch (Exception)
					{
						m_logger.Info(string.Format("Version string is in the wrong format: {0}", value));
						application.Version = value;
					}
				}
			}
		}

		/// <summary>
		/// Gets the window manager.
		/// </summary>
		public IWindowManager WindowManager
		{
			get
			{
				return m_windowManager;
			}
		}

		/// <summary>
		/// Gets the active layout.
		/// </summary>
		public string ActiveLayout
		{
			get
			{
				return m_persistenceProfileService.CurrentProfile;
			}
		}

		public event StatusUpdatedEventHandler StatusUpdated;
		public event EventHandler StartupFinished;

	    public void InvokeStartupFinished(EventArgs e_)
	    {
	        EventHandler handler = StartupFinished;
	        if (handler != null) handler(this, e_);
	    }

	    public IEntitlements GetEntitlements(string id_)
		{
			lock (m_entitlements.SyncRoot)
			{
				IEntitlements entitlements = m_entitlements[id_] as IEntitlements;
				return (IEntitlements)(entitlements ?? m_entitlements["Default"]);
			}
		}

		//Applets 

		/// <summary>
		/// Loads the applets.
		/// </summary>
		void LoadApplets()
		{

			IList<KeyValuePair<string, string>> appletList = GetApplets();
			foreach (KeyValuePair<string, string> appletNameTypePair in appletList)
			{
				if (!this.IsAppletEntitled(appletNameTypePair.Key))
				{
					UpdateStatus(StatusLevel.Information, String.Format("Not entitled to add applet '{0}'", appletNameTypePair.Key));
					m_logger.WarningWithFormat("Not entitled to add applet '{0}'", appletNameTypePair.Key);
					continue;
				}

				UpdateStatus(StatusLevel.Information, String.Format("Loading applet \'{0}\'", appletNameTypePair.Key));
				m_logger.DebugWithFormat("Loading applet \'{0}\'", appletNameTypePair.Key);
				try
				{
					if (m_uiOnlyList.Contains(appletNameTypePair.Key))
					{
						KeyValuePair<string, string> pair = appletNameTypePair;
						m_loop.Invoke(() => LoadApplet(pair.Key, pair.Value, this));
					}
					else
					{
						LoadApplet(appletNameTypePair.Key, appletNameTypePair.Value, this);
					}
				}
				catch (Exception ex)
				{
					this.UpdateStatus(StatusLevel.Critical,
														String.Format("Couldn't load applet {0}. Error: {1}", appletNameTypePair.Key,
																	ex.Message));
					m_logger.Warning(String.Format("Couldn't load applet {0}", appletNameTypePair.Key), ex);
				}
				ClearStatus();
			}
			ClearStatus();
		}

		private void LoadApplet(string appletName_, string appletTypeName_, IConcordApplication application_)
		{
            Type type = Type.GetType(appletTypeName_, false);
            IModuleLoadInfo info = null;
            if (type == null)
            {
                info = m_moduleLoadInfos.AddModuleLoadInfo(appletTypeName_, ModuleStatus.TypeResolutionFailed);
                info.Type = ModuleType.ConcordApplet;
                return;
            }
            info = m_moduleLoadInfos.AddModuleLoadInfo(type.AssemblyQualifiedName);
            OnBeforeModuleInitialized(application_, new EventArgs<string>(type.AssemblyQualifiedName));
            try
            {
                if (typeof(ConcordAppletBase).IsAssignableFrom(type))
                {
                    ConcordAppletBase applet = application_.WindowManager.CreateApplet(appletName_, type);

                    if (applet != null)
                    {
                        application_.ConcordApplets.Add(applet);
                    }
                }
            }
            catch (Exception ex)
            {
                info.Status = ModuleStatus.Exceptioned;
                info.Error = ex;
                throw;
            }
            OnAfterModuleInitialized(application_, new EventArgs<string>(type.AssemblyQualifiedName));
		}

		internal static event EventHandler<EventArgs<string>> BeforeModuleInitialized;

		private static void OnBeforeModuleInitialized(object sender, EventArgs<string> e)
		{
			var copy = BeforeModuleInitialized;
			if (copy != null)
			{
				copy(sender, e);
			}
		}

		internal static event EventHandler<EventArgs<string>> AfterModuleInitialized;

		private static void OnAfterModuleInitialized(object sender, EventArgs<string> e)
		{
			var copy = AfterModuleInitialized;
			if (copy != null)
			{
				copy(sender, e);
			}
		}


		/// <summary>
		/// Gets the applets.
		/// </summary>
		/// <returns></returns>
		public static IList<KeyValuePair<string, string>> GetApplets()
		{
			Configurator concordConfig = ConfigurationManager.GetConfig(CONCORD_CONFIG_NAME) as Configurator;
			if (concordConfig != null)
			{
				List<KeyValuePair<string, string>> applets = new List<KeyValuePair<string, string>>();
				XmlNode appletsNode = concordConfig.GetNode(APPLETS_NODE_NAME, null);
				if (appletsNode != null)
				{
					NameValueSectionHandler appletHandler = new NameValueSectionHandler();
					// Clone the node before sending to Create, because Create removes the Xml from the document
					NameValueCollection appletCollection = appletHandler.Create(null, null, appletsNode.Clone()) as NameValueCollection;
					if (appletCollection != null)
					{
						foreach (string key in appletCollection.Keys)
						{
							applets.Add(new KeyValuePair<string, string>(key, appletCollection[key]));
						}
					}
				}
				return applets;
			}
			return null;
		}

		/// <summary>
		/// Determines whether applet with name <paramref name="name_"/> is entitled.
		/// </summary>
		/// <param name="name_">The name of the applet.</param>
		/// <returns>
		///   <c>true</c> if applet with name <paramref name="name_"/> is entitled; otherwise, <c>false</c>.
		/// </returns>
		private bool IsAppletEntitled(string name_)
		{
			if (name_ == null) throw new ArgumentNullException("name_");

			try
			{
				IEntitlements entitlements = this.GetEntitlements(APPLETS_ENTITLEMENTS_ID);
				if (entitlements != null)
				{
					return entitlements.IsEntitled(name_, null);
				}
			}
			catch (Exception ex_)
			{
				UpdateStatus(StatusLevel.Critical, "Error retrieving entitlements. Error: " + ex_.Message);
			}

			return true;
		}

		/// <summary>
		/// Loads the configuration.
		/// </summary>
		private void LoadConfiguration(string concordDefaultProfile)
		{
			m_mainConfiguration = LoadMainConfiguration(concordDefaultProfile);

			var configs = new ConfigurationIdentityCollection();

			// Add host level configs
			configs.Add(new ConfigurationIdentity("Layouts"));

			// Enumerate applets and get a list of
			// all configs that they need
			// TODO we are loading this list twice. Is it correct?
			XmlNode appletNode = m_mainConfiguration.GetNode(APPLETS_NODE_NAME, null);
			if (appletNode != null)
			{
				var appletHandler = new NameValueSectionHandler();
				var applets = appletHandler.Create(null, null, appletNode.Clone()) as NameValueCollection;

				if (applets != null)
				{
					foreach (string key in applets)
					{
						Type type = Type.GetType(applets[key]);

						if (type == null)
						{
							throw new Exception(String.Format("Error. Invalid Type. {0}", applets[key]));
						}

						object[] attribs = type.GetCustomAttributes(typeof(ConfigurationSectionAttribute), true);
						if (attribs != null && attribs.Length > 0)
						{
							foreach (ConfigurationSectionAttribute attrib in attribs)
							{
								configs.Add(new ConfigurationIdentity(attrib.Name, attrib.Version));
							}
						}
					}
				}
			}

			this.LoadConfiguration(configs);
		}

		/// <summary>
		/// Loads the configuration.
		/// </summary>
		/// <param name="configs_">The configs.</param>
		private void LoadConfiguration(ConfigurationIdentityCollection configs_)
		{
			ConfigurationManager.LoadConfigurations(configs_);
			try
			{
				XmlNode entitlementsConfigNode = m_mainConfiguration.GetNode(ENTITLEMENTS_ROOT_NAME, null);
				if (entitlementsConfigNode != null)
				{
					XmlNodeList providerNodes = entitlementsConfigNode.SelectNodes(ENTITLEMENTS_PROVIDERS);
					if (providerNodes != null)
					{
						foreach (XmlNode providerNode in providerNodes)
						{
							string typeName = (providerNode.Attributes["type"] != null) ? providerNode.Attributes["type"].Value : null;
							if (!string.IsNullOrEmpty(typeName))
							{
								try
								{
									Type providerType = Type.GetType(typeName, true);
									if (providerType != null)
									{
										var provider = (IEntitlements)Activator.CreateInstance(providerType);
										provider.Initialize(providerNode);

										XmlNodeList nameNodes = providerNode.SelectNodes(ENTITLEMENTS_PROVIDER_NAMES);
										if (nameNodes != null)
										{
											foreach (XmlNode nameNode in nameNodes)
											{
												string id = nameNode.InnerText;
												if (!string.IsNullOrEmpty(id))
												{
													m_entitlements[id] = provider;
												}
											}
										}

										string defaultProviderFlag = (providerNode.Attributes["default"] != null) ? providerNode.Attributes["default"].Value : null;
										if (!string.IsNullOrEmpty(defaultProviderFlag))
										{
											if (Convert.ToBoolean(defaultProviderFlag))
											{
												m_entitlements["Default"] = provider;
											}
										}
									}
								}
								catch (Exception ex_)
								{
									UpdateStatus(StatusLevel.Critical,
												 String.Format("Error initializing entitlements provider, type='{1}', {0}", ex_.Message,
																 typeName));
								}
							}
						}
					}
				}
			}
			catch (Exception ex_)
			{
				UpdateStatus(StatusLevel.Critical, "Error initializing entitlements " + ex_.Message);
			}
		}

		/// <summary>
		/// Loads the main configuration.
		/// </summary>
		/// <returns>The configurator.</returns>
		private Configurator LoadMainConfiguration(string concordDefaultProfile_)
		{
			if (!ConcordPersistenceStorage.ConcordConfigurationManagerInitialized)
			{
				ConcordConfigurationManager.ApplicationName = Name;
				ConcordConfigurationManager.Initialize(m_arguments, concordDefaultProfile_);
			}
			return (Configurator)ConfigurationManager.GetConfig(CONCORD_CONFIG_NAME);
		}

		/// <summary>
		/// Creates the link manager instance.
		/// </summary>
		/// <returns>The link manager.</returns>
		private ILinkManager CreateLinkManagerInstance()
		{
			ILinkManager linkManager = null;
			try
			{
				linkManager = LinkManager.LinkManager.Current;
				linkManager.Initialize(this);
			}
			catch (System.Reflection.TargetInvocationException ex_)
			{
				throw new Exception("Failed to create and initialize linkmanager instance", ex_);
			}
			return linkManager;
		}

	}
}
