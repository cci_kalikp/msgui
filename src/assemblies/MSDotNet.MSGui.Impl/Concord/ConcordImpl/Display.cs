/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Concord/ConcordImpl/Display.cs#5 $
// $Change: 813214 $
// $DateTime: 2013/01/18 15:24:18 $
// $Author: smulovic $

using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl
{
  public class Display
  {
    public static string FormatCaption(string text_)
    {
      return Display.Format(text_);
    }

    public static string FormatMenuCaption(string text_)
    {
      return Display.Format(text_, true);
    }

    public static string FormatName(string name_)
    {
      //pitfall: "Bad Name___111" and "Bad_Name_ _111" become indistinguishable
      if (!string.IsNullOrEmpty(name_))
      {
        var newname = new char[name_.Length];
        for (int i = 0; i < name_.Length; i++)
        {
          if (char.IsLetterOrDigit(name_[i]) || name_[i] == '_')
          {
            newname[i] = name_[i];
          }
          else
          {
            newname[i] = '_';
          }
        }
        string result = new string(newname);
        if (!char.IsLetter(newname[0]))
        {
          result = '_' + result;
        }
        return result;
      }
      return name_;
    }

    #region Private Methods
    private static string Format(string text_)
    {
      return Display.Format(text_, false);
    }

    private static string Format(string text_, bool menuText_)
    {
      if (!string.IsNullOrEmpty(text_))
      {
        StringBuilder strBuilder = new StringBuilder(text_.Length);

        for (int i = 0; i < text_.Length; i++)
        {
          char ch = text_[i];
          if (ch == '&')
          {
            if (i > 0 && text_[i - 1] == '&')
            {
              strBuilder.Append(ch);
            }
            else if (i < text_.Length && text_[i + 1] == '&')
            {
              strBuilder.Append(ch);
            }
            else if (menuText_)
            {
              strBuilder.Append('_');
            }
          }
          else
          {
            strBuilder.Append(ch);
          }
        }

        return strBuilder.ToString();
      }

      return text_;
    }
    #endregion Private Methods
  }
}