﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Concord/ConcordImpl/AppletDomain.cs#17 $
// $Change: 880888 $
// $DateTime: 2014/05/13 23:42:33 $
// $Author: caijin $

using System;
using System.Collections;
using System.Threading;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Unity;
using MorganStanley.IED.Concord.Application;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Shell;
using MorganStanley.MSDotNet.MSNet;
using MorganStanley.MSDotNet.My;
using Microsoft.Practices.Composite.Modularity;

namespace MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl
{
    /// <summary>
    /// Summary description for AppletDomain.
    /// </summary>
    internal class AppletDomain
    {
        private static readonly IMSLogger m_logger = MSLoggerFactory.CreateLogger<AppletDomain>();

        private static readonly AppletDomain m_sharedInstance = new AppletDomain(AppletActivationType.Shared);
        private static readonly AppletDomain m_hostInstance = new AppletDomain(AppletActivationType.None);

        private static readonly Hashtable m_threadToLoop = Hashtable.Synchronized(new Hashtable());
        private static readonly Hashtable m_appletToLoop = Hashtable.Synchronized(new Hashtable());
        private static readonly Hashtable m_appletToThread = Hashtable.Synchronized(new Hashtable());
        private static bool m_allowMultipleThreads = true;

        private readonly object m_lock = new object();
        private MSNetLoopDispatcherImpl m_loop;
        private readonly AppletActivationType m_activationType;
        private IConcordApplication m_application;
        private Type m_type;
        private string m_name;
        private ConcordAppletBase m_applet;
        private readonly ManualResetEvent m_appletCreatedEvent;
        private readonly MSNetVoidObjectDelegate m_appletConnectCaller;
        private static IUnityContainer m_unityContainer;

        public static event EventHandler ApplicationCreated;

        private AppletDomain()
        {
            m_logger.Debug("Creating new AppletDomain");
            m_appletCreatedEvent = new ManualResetEvent(false);
            m_appletConnectCaller = new MSNetVoidObjectDelegate(AppletOnConnect);
        }

        private AppletDomain(AppletActivationType activationType_)
            : this()
        {
            m_logger.DebugWithFormat("New AppletDomain's activation type {0}", activationType_);
            m_activationType = AllowMultipleThreads ? activationType_ : AppletActivationType.None;
        }

        public static bool AllowMultipleThreads
        {
            get { return m_allowMultipleThreads; }
            set { m_allowMultipleThreads = value; }
        }

        public static IMSNetLoop CurrentLoop
        {
            get { return (IMSNetLoop) m_threadToLoop[Thread.CurrentThread]; }
        }

        public static AppletDomain Shared
        {
            get { return m_sharedInstance; }
        }

        public static AppletDomain Host
        {
            get { return m_hostInstance; }
        }

        public static IUnityContainer CurrentContainer
        {
            set { m_unityContainer = value; }
        }

        public IMSNetLoop GetLoopFromApplet(ConcordAppletBase applet_)
        {
            return m_appletToLoop[applet_] as IMSNetLoop;
        }

        public Thread GetThreadFromApplet(ConcordAppletBase applet_)
        {
            return m_appletToThread[applet_] as Thread;
        }

        //todo: handle create applet to add module info when handling isolated module
        public static ConcordAppletBase CreateIsolatedApplet(IConcordApplication application_, Type type_, string name_)
        {
            AppletDomain domain = new AppletDomain(AppletActivationType.Isolated);
            return domain.CreateApplet(application_, type_, name_);
        }

        public ConcordAppletBase CreateApplet(IConcordApplication application_, Type type_, string name_)
        {
            m_logger.DebugWithFormat("Creating applet '{0}'", name_);
            lock (m_lock)
            {
                m_type = type_;
                m_name = name_;
                m_application = application_;
                m_applet = null;

                switch (m_activationType)
                {
                    case AppletActivationType.None:
                        m_application.MainLoop.InvokeWithState(m_appletConnectCaller, m_application.MainLoop);
                        break;

                    case AppletActivationType.Shared:
                    case AppletActivationType.Isolated:
                        if (m_activationType == AppletActivationType.Shared && m_loop != null)
                        {
                            m_loop.InvokeWithState(m_appletConnectCaller, m_loop);
                        }
                        else
                        {
                            Thread appletThread = new Thread(new ThreadStart(AppletStart));
                            appletThread.Name = (m_activationType == AppletActivationType.Shared)
                                                    ? "Shared Applet: "
                                                    : "Applet: " + name_;
                            appletThread.ApartmentState = ApartmentState.STA;
                            appletThread.IsBackground = true;
                            appletThread.Start();
                        }
                        break;
                }

                m_appletCreatedEvent.WaitOne();
                m_appletCreatedEvent.Reset();

                return m_applet;
            }
        }

        protected virtual void OnApplicationCreated()
        {
            if (ApplicationCreated != null)
            {
                ApplicationCreated(this, EventArgs.Empty);
            }
        }

        private void AppletStart()
        {
            m_logger.DebugWithFormat("Calling AppletStart '{0}'", m_name);
            m_loop = new MSNetLoopDispatcherImpl(m_unityContainer.Resolve<IShell>().MainWindow);
            m_loop.InvokeLater(new MSNetVoidVoidDelegate(this.OnApplicationCreated));
            m_loop.InvokeLaterWithState(m_appletConnectCaller, m_loop);
            m_loop.Loop();
        }

        private void AppletOnConnect(object state_)
        {
            var moduleLoadInfos = m_unityContainer.Resolve<IModuleLoadInfos>();
            m_logger.DebugWithFormat("Calling AppletOnConnect '{0}'", m_name);
            try
            {
                IMSNetLoop loop = (IMSNetLoop) state_;
                m_threadToLoop[Thread.CurrentThread] = loop;
                //m_applet = (ConcordAppletBase)Activator.CreateInstance(m_type, new object[] { loop });
                m_applet = (ConcordAppletBase) m_unityContainer.Resolve(m_type);
                if (m_applet == null)
                {
                    throw new Exception("Applet of not created");
                }
                m_appletToLoop[m_applet] = loop;
                m_appletToThread[m_applet] = Thread.CurrentThread;
                m_applet.OnConnect(m_application);
                if (m_applet is IModule)
                {
                    ((IModule) m_applet).Initialize();
                }
            }
            catch (Exception exception)
            {
                IModuleLoadInfo info = moduleLoadInfos.GetModuleLoadInfo(m_type.AssemblyQualifiedName);
                if (info != null)
                {
                    info.Status = ModuleStatus.Exceptioned;
                    info.Error = exception;
                }

                m_logger.ErrorWithFormat(
                    "Error while loading applet '{0}' Exception:'{1}', Error message:'{2}', Stack trace:'{3}'", m_name,
                    exception.ToString(), exception.Message, exception.StackTrace);
            }
            finally
            {
                m_appletCreatedEvent.Set();
            }
        }
    }
}
