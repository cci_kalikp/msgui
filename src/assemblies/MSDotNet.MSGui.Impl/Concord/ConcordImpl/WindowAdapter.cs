﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Concord/ConcordImpl/WindowAdapter.cs#22 $
// $Change: 896821 $
// $DateTime: 2014/09/15 02:20:20 $
// $Author: caijin $

using System;
using System.IO;
using System.Windows.Controls;
using System.Windows.Forms;
using MorganStanley.IED.Concord.Application;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls;
using MorganStanley.MSDotNet.MSGui.Impl.View;

namespace MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl
{
	internal class WindowAdapter : IWindow
	{
		internal static bool AutosizeConcordForms = false;

		private string m_caption;
		private readonly Form m_form;
		private readonly WindowViewContainer m_viewContainer;
		private ILinkControl m_linkControl = null;
		private readonly EventHandler m_formClosedHandler;
		private readonly AppletFormHost m_formsHost;

		public Image LinkControlImage
		{
			get;
			set;
		}

		public WindowViewContainer ViewContainer
		{
			get
			{
				return m_viewContainer;
			}
		}

		/// <summary>
		/// Initialize caption, create a m_viewContainer and host the given form in it.
		/// </summary>
		/// <param name="caption_"></param>
		/// <param name="form_"></param>
		/// <param name="guid_"></param>
		/// <param name="visible_"></param>
		public WindowAdapter(string caption_, Form form_, Guid guid_, bool visible_, string id)
		{
            //add this extra check, since when loading from workspace, the guid might not be recreated
            Guid = guid_ != Guid.Empty && !ConcordExtensionStorage.ViewContainerByWindowGuidStore.ContainsKey(guid_.ToString()) ? 
            guid_ : Guid.NewGuid(); 
			m_caption = caption_;

			m_form = form_;
			m_form.TopLevel = false;
			m_form.FormBorderStyle = FormBorderStyle.None;

			m_form.Closed += m_formClosedHandler = delegate
			{
				m_form.Closed -= m_formClosedHandler;
			};

			m_formsHost = new AppletFormHost { Window = this };

			LinkControlImage = new Image() { Tag = "LinkControl" };
			LinkControlImage.MouseLeftButtonDown += (sender_, args_) => HandleLinkClick(true);
			LinkControlImage.MouseRightButtonDown += (sender_, args_) => HandleLinkClick(false);

			DockPanel dp = new DockPanel() { LastChildFill = true };
			dp.Children.Add(m_formsHost);

			m_viewContainer = new WindowViewContainer("")
			{
				IsVisible = visible_,
				Title = caption_,
				Content = dp,
				ViewContainerType = ViewContainerType.Concord,
				ID = id
			};

			ConcordExtensionStorage.ViewContainerByWindowGuidStore.Add(Guid.ToString(), m_viewContainer.ID);

			m_formsHost.Child = form_;

			//new ViewContainer
			//                {
			//                  ViewMeta = new ViewMetadata(),
			//                  IsVisible = visible_,
			//                  Content = dp,
			//                  Title = caption_
			//                };

			if (MorganStanley.MSDotNet.MSGui.Impl.Extensions.ConcordExtensionStorage.ConcordCommands.Count > 0) //TODO this may be better w/ a stack walk to see if we initiated, but maybe the entire stack thing is unnecessary as it is... (a simple variable may suffice)
			{
				//System.Diagnostics.Debugger.Break();
				var ivsID = MorganStanley.MSDotNet.MSGui.Impl.Extensions.ConcordExtensionStorage.ConcordCommands.Peek();
				if (ivsID != null)
				{
					if (MorganStanley.MSDotNet.MSGui.Impl.Extensions.ConcordExtensionStorage.ConcordInitialViewSettings.ContainsKey(ivsID))
					{
						InitialViewSettings ivs = MorganStanley.MSDotNet.MSGui.Impl.Extensions.ConcordExtensionStorage.ConcordInitialViewSettings[ivsID];

						m_viewContainer.Parameters.EnforceSizeRestrictions = ivs.EnforceSizeRestrictions;
						m_viewContainer.Parameters.InitialLocation = ivs.InitialLocation;
						m_viewContainer.Parameters.SizingMethod = ivs.SizingMethod;
						m_viewContainer.Parameters.Width = ivs.Width;
						m_viewContainer.Parameters.Height = ivs.Height;
					}
				}
			}
			if (WindowAdapter.AutosizeConcordForms)
			{
				m_viewContainer.Parameters.SizingMethod = SizingMethod.Custom;
                m_viewContainer.Parameters.Width = form_.ClientSize.Width + this.m_viewContainer.HorizontalContentDiff;
                m_viewContainer.Parameters.Height = form_.ClientSize.Height + this.m_viewContainer.VerticalContentDiff;
			}

			m_viewContainer.HeaderItems.Add(LinkControlImage);
			m_viewContainer.PropertyChanged += OnViewContainerPropertyChanged;
			m_viewContainer.Closed += OnViewContainerClosed;
			m_viewContainer.ClosedQuietly += OnViewContainerClosed;

		}

		private void OnViewContainerClosed(object sender_, WindowEventArgs e_)
		{
			if (IsLinkingEnabled)
			{
				LinkControl.LinkStateChanged -= OnLinkStateChanged;
				LinkControl.Delete();
			}
			m_viewContainer.HeaderItems.Clear();
			m_viewContainer.PropertyChanged -= OnViewContainerPropertyChanged;
			m_viewContainer.Closed -= OnViewContainerClosed;
			m_viewContainer.ClosedQuietly -= OnViewContainerClosed;
			m_form.Visible = false;
			//m_formsHost.Child = null;
			m_formsHost.Dispose();
			//m_form.Close();
			m_form.Dispose();
		}

		private void OnViewContainerPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			WindowViewContainer viewContainer = (WindowViewContainer)sender;

			switch (e.PropertyName)
			{
				case "IsActive":
					if (viewContainer.IsActive)
					{
						if (IsLinkingEnabled)
						{
							LinkControl.SetActive(true);
						}
					}
					else
					{
						if (IsLinkingEnabled)
						{
							LinkControl.SetActive(false);
						}
					}
					break;
				case "Title":
					//! the field not Caption property, you'll get infinite recursion otherwise
					m_caption = m_form.Text = viewContainer.Title;
					break;
			}
		}

		public string Prefix
		{
			get
			{
				return "IWindow";
			}
		}

		public void Close()
		{
			m_viewContainer.Close();
		}

		public void SetFocus()
		{
			m_viewContainer.Activate();
		}

		public string Caption
		{
			get
			{
				return m_caption;
			}
			set
			{
				m_form.Text = value;
				m_viewContainer.Title = value;
			}
		}

		public Form Form
		{
			get
			{
				return m_form;
			}
		}

		public bool Visible
		{
			get
			{
				return m_viewContainer.IsVisible;
			}
			set
			{
				m_viewContainer.IsVisible = value;
			}
		}

		public Guid Guid { get; set; }

		public ILinkControl LinkControl
		{
			get
			{
				return m_linkControl;
			}
			set
			{
				if (m_linkControl == null && value != null)
				{
					m_linkControl = value;
					m_linkControl.LinkStateChanged += OnLinkStateChanged;
					m_linkControl.ClientForm = m_form;
					//m_linkControl.IsFloating = (this.Floatingform != null);
					//if (m_form.ContainsFocus)// || ((PaneControl != null) && PaneControl.Active))
					{

						LinkControl.SetActive(true);
					}
					// assign the image immediately that the LinkControl has been added
					this.OnLinkStateChanged(this, EventArgs.Empty);
				}
			}
		}

		private bool IsLinkingEnabled
		{
			get
			{
				if (LinkControl != null && LinkControl.Enabled)
				{
					if (LinkControl.ClientForm == null && m_form != null)
					{
						LinkControl.ClientForm = m_form;
					}
					return LinkControl.ClientForm != null;
				}
				return false;
			}
		}

		private void OnLinkStateChanged(object sender_, EventArgs e_)
		{
			if (IsLinkingEnabled)
			{
				SetImage(LinkControl.LinkImage(9));
			}
		}

		private void HandleLinkClick(bool leftClick_)
		{
			if (IsLinkingEnabled)
			{
				LinkControl.HandleMouseClick(leftClick_, LinkControl.ClientForm.PointToClient(System.Windows.Forms.Control.MousePosition));
			}
		}

		public void SetImage(System.Drawing.Image image_)
		{
			if (image_ == null)
			{
				LinkControlImage.Source = null;
				return;
			}
			//converting to wpf Image -- it would be better to move it to some global helper class
			MemoryStream ms = new MemoryStream();
			image_.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
			System.Windows.Media.Imaging.BitmapImage bImg = new System.Windows.Media.Imaging.BitmapImage();
			bImg.BeginInit();
			bImg.StreamSource = new MemoryStream(ms.ToArray());
			bImg.EndInit();
			//finished converting
			LinkControlImage.Source = bImg;
		}

	}
}