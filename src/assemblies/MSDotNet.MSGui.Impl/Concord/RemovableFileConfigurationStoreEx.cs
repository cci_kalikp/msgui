﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using MorganStanley.IED.Concord.Configuration;

namespace MorganStanley.MSDotNet.MSGui.Impl.Concord
{
    internal class RemovableFileConfigurationStoreEx:FileConfigurationStoreEx
    {
        private NameValueCollection settings;
        public override void Initialize(string settings_)
        {
            base.Initialize(settings_);
            settings = ParseProfileString(settings_);
        }

        public void RemoveConfiguration(string name_, string version_)
        {
            string fileName = GetUserFilePath(settings, name_, version_);
            if (File.Exists(fileName))
            {
                File.Delete(fileName); 
            }
        }

        public List<string> GetConfigurations(string prefix_, string version_, bool returnWithPrefix_)
        {
            string app = (settings[Configurator.CONFIG_SETTING_LOCAL_APP_KEY] != null) ? settings[Configurator.CONFIG_SETTING_LOCAL_APP_KEY] : settings[Configurator.CONFIG_SETTING_APP_KEY];
            string user = settings[Configurator.CONFIG_SETTING_USER_KEY];
            string reg = (settings[Configurator.CONFIG_SETTING_LOCAL_REGION_KEY] != null) ? settings[Configurator.CONFIG_SETTING_LOCAL_REGION_KEY] : settings[Configurator.CONFIG_SETTING_REGION_KEY];
            string env = (settings[Configurator.CONFIG_SETTING_LOCAL_ENV_KEY] != null) ? settings[Configurator.CONFIG_SETTING_LOCAL_ENV_KEY] : settings[Configurator.CONFIG_SETTING_ENV_KEY];
            string role = settings[Configurator.CONFIG_SETTING_ROLE_KEY];

            string dir = GetUserDirectory(user, app, reg, env, role);
            string extension = string.IsNullOrEmpty(version_)
                                 ? ".config" 
                                 : ("." + version_ + ".config");
            string pattern = prefix_ + "*" + extension;
            List<string> configurations = new List<string>();
            foreach (var fileName in Directory.GetFiles(dir, pattern))
            {
                string configuration = fileName.Substring(dir.Length);
                configuration = configuration.Remove(configuration.Length - extension.Length);
                if (!returnWithPrefix_)
                {
                    configuration = configuration.Substring(prefix_.Length);
                }
                configurations.Add(configuration);
            }
            return configurations;
        }
    }
}
