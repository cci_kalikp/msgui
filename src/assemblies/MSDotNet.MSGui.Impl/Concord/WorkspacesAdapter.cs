﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using MorganStanley.IED.Concord.Configuration;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl;
using MorganStanley.MSDotNet.MSGui.Impl.Helpers;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;

namespace MorganStanley.MSDotNet.MSGui.Impl.Concord
{
    public class WorkspacesAdapter : IConfigurationInterceptingWriter
    {
        private static RemovableFileConfigurationStoreEx storage;
        private static object syncRoot = new object();

        private void EnsureStorage()
        {
            if (storage == null)
            {
                lock (syncRoot)
                {
                    if (storage == null)
                    {
                        storage = new RemovableFileConfigurationStoreEx();
                        storage.Initialize(
                            ConcordConfigurationManager.GetConcordProfileString(
                                ConcordConfigurationManager.CurrentProfile));
                    }

                }
            }
            
        }

        public void InterceptWrite(string name, string version, System.Xml.XmlNode node,
	                               InterceptingConfigurationStore.DecoratedWriteConfiguration decoratedWriteConfiguration)
        {

            EnsureStorage();
            var layouts = storage.GetConfigurations(ChromeManagerBase.SubLayoutPrefix, version, false);
            foreach (string layout in layouts)
            {
                string subLayoutName = XmlConvert.DecodeName(layout);
                if (!string.IsNullOrEmpty(subLayoutName))
                {
                    if (node.SelectNodes("//Workspace[@Name='" + subLayoutName + "']").Count == 0)
                    {
                        storage.RemoveConfiguration(ChromeManagerBase.SubLayoutPrefix + layout, version);
                    }
                } 
            }
            decoratedWriteConfiguration(name, version, node);
        }

	    public ConfigurationSection InterceptRead(ConfigurationSection original)
	    {
	        return original;
	    }
    }
}
