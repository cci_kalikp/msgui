﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using MorganStanley.IED.Concord.Configuration;

namespace MorganStanley.MSDotNet.MSGui.Impl.Concord
{
    public static class ConfigurationManagerHelper
    {
        public static void DirtyConfiguration(string configName_)
        {
            var fieldInfo = typeof(ConfigurationManager).GetField("_configMgrImpl",
                              BindingFlags.FlattenHierarchy | BindingFlags.Static |
                              BindingFlags.NonPublic);
            if (fieldInfo != null)
            {
                var cminst = fieldInfo.GetValue(null);
                var field = cminst.GetType()
                                  .GetField("_configs",
                                            BindingFlags.FlattenHierarchy | BindingFlags.Instance |
                                            BindingFlags.NonPublic);
                if (field != null)
                {
                    var ht = field.GetValue(cminst) as Hashtable;
                    if (ht != null)
                    {
                        ht.Remove(configName_);
                    }
                }
            }
        }
    }
}
