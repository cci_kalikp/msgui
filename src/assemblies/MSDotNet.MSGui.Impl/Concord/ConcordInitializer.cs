﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Concord/ConcordInitializer.cs#29 $
// $Change: 902331 $
// $DateTime: 2014/10/24 05:43:11 $
// $Author: caijin $

using System.Linq;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Unity;
using MorganStanley.IED.Concord.Application;
using MorganStanley.IED.Concord.Runtime;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ControlParadigm;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.Concord.ConcordImpl;
using MorganStanley.MSDotNet.MSGui.Impl.ControlParadigm;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.Shell;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Extensions.MVVMDock;
using MorganStanley.MSDotNet.MSNet;
using MorganStanley.MSDotNet.My;
using System; 

namespace MorganStanley.MSDotNet.MSGui.Impl.Concord
{
	internal static class ConcordInitializer
	{
		private static readonly IMSLogger m_logger = MSLoggerFactory.CreateLogger("ConcordInitializer");
		internal static bool SuppressConcordStatusBarItems;

		public static void InitializeConcordApplication(IUnityContainer container_, PersistenceService persistenceService_, PersistenceProfileService persistenceProfileService_, string concordDefaultProfile, bool loggingEnabled)
		{
			m_logger.Debug("Initializing Concord application");

			IMSNetLoop loop = MSNetLoopWithAffinityBase.GetLoop() ?? new MSNetLoopDispatcherImpl(container_.Resolve<IShell>().MainWindow);
			ConcordApplication concordApplication = new ConcordApplication(container_, persistenceProfileService_);
			ConcordApplication.StartLog(loggingEnabled);
			ConcordApplication.AfterModuleInitialized += OnAfterModuleInitialized;
			ConcordApplication.BeforeModuleInitialized += OnBeforeModuleInitialized;


			container_.RegisterInstance<IConcordApplication>(concordApplication);
			container_.RegisterInstance<IMSNetLoop>(loop);

			//statusBar_.MainItem.UpdateStatus(StatusLevel.Information, "Initializing Concord Profiles...");
			//if (!ConcordConfigurationManager.Initialized)
			//{
			//  ConcordConfigurationManager.Initialize();
			//}

			ConcordWindowManager windowManager = new ConcordWindowManager(concordApplication, container_);
			IShell shell = container_.Resolve<IShell>();
			IControlParadigmRegistrator registrator = container_.Resolve<IControlParadigmRegistrator>();
			IControlParadigmImplementator implementator = container_.Resolve<IControlParadigmImplementator>(); //TODO maybe it's better to register/resolve IHostRegistry directly or merge that interface with this one
			InitialSettings initialSettings = container_.Resolve<InitialSettings>();
 			ConcordImpl.BarAdapters.CommandBarsAdapter commandBarsAdapter =
			  new ConcordImpl.BarAdapters.CommandBarsAdapter(
				concordApplication,
				registrator,
				(IHostRegistry)implementator,
				loop,
				initialSettings.ShellMode); //TODO casting breaks some advantages of interfaces
			//CommandBarsAdapter commandBarsAdapter = new CommandBarsAdapter(concordApplication, bar, loop);
			concordApplication.Init(windowManager, loop, commandBarsAdapter);

			shell.ConcordMSNetLoopForm = concordApplication.MainForm;
			shell.TryCloseConcordMainWindow = () =>
			{

				var form = concordApplication.MainForm as ConcordMSNetLoopForm;
				if (null != form)
				{
					var arg = new System.Windows.Forms.FormClosingEventArgs(
																									System.Windows.Forms.CloseReason.FormOwnerClosing, // The Form's owner window is closing
																									false                          // cancel
																									);
					form.RaiseOnClosing(null, arg);
					return !arg.Cancel;
				}

				return true; // default allow close form
			};
			concordApplication.Initialize();
			concordApplication.ConcordDefaultProfile = concordDefaultProfile;

			IStatusBar statusBar = container_.Resolve<IStatusBar>();

			implementator.SetOnClosingAction(new Action(() =>
			{
				foreach (var applet in concordApplication.ConcordApplets)
				{
					try
					{
						if (m_logger.IsInfoLoggable())
						{
                            m_logger.Info(string.Format("Unloading applet '{0}'", applet.Title), "FormClosed");
						}

						applet.OnDisconnect();
					}
					catch (Exception ex_)
					{
						if (m_logger.IsErrorLoggable())
						{
							m_logger.Error(string.Format("Error disconnecting applet '{0}'.", applet.Title), ex_, "FormClosed");
						}
					}
				}
			}));
			if (!SuppressConcordStatusBarItems)
			{
				string userItemKey = statusBar.AddTextStatusItem(StatusBarItemAlignment.Right);
				statusBar.AddSeparator(StatusBarItemAlignment.Right);
				string profileItemKey = statusBar.AddTextStatusItem(StatusBarItemAlignment.Right);
				statusBar.AddSeparator(StatusBarItemAlignment.Right);

				// Reorganized the order 


				// *** - Start of statements group 1
				statusBar[userItemKey].UpdateStatus(Core.StatusLevel.Information,
				                                    "User: " + ConcordRuntime.Current.UserInfo.Username);
				statusBar[profileItemKey].UpdateStatus(Core.StatusLevel.Information,
				                                       "Profile: " + ConcordConfigurationManager.CurrentProfile);
			}
			container_.RegisterInstance(ConcordRuntime.Current);

			m_logger.InfoWithFormat("Login User='{0}', Profile='{1}', ProfileString='{2}'",
															ConcordRuntime.Current.UserInfo.Username, ConcordConfigurationManager.CurrentProfile,
															ConcordConfigurationManager.GetConcordProfileString(
																ConcordConfigurationManager.CurrentProfile));
			MorganStanley.IED.Concord.Logging.Log.Info.SetLayer("msdotnet", "msdesktop").SetLocation("MSDesktop.ConcordLoader", "StartLog()").
		WriteFormat("Login User='{0}', Profile='{1}', ProfileString='{2}'",
									ConcordRuntime.Current.UserInfo.Username, ConcordConfigurationManager.CurrentProfile,
									ConcordConfigurationManager.GetConcordProfileString(
									  ConcordConfigurationManager.CurrentProfile));

			AppletDomain.CurrentContainer = container_;

            InitializeEarlyModules(container_);
            // *** - End of statements group 1


            // *** - Start of statement group 2
            concordApplication.Run(container_.Resolve<IApplication>().CommandLine);
            // *** - end of statement group 2

            PaneFactory.LinkControlFocusHackEnabled = true;

            var application = container_.Resolve<IApplication>();

            EventHandler applicationLoaded = null;
            applicationLoaded = (sender_, e_) =>
            {
                concordApplication.InvokeStartupFinished(new EventArgs());
                application.ApplicationLoaded -= applicationLoaded;
            };
            application.ApplicationLoaded += applicationLoaded;
		}

		private static void InitializeEarlyModules(IUnityContainer unityContainer_)
		{
            if (!unityContainer_.IsRegistered<IModuleManager>()) return;
		    IModuleManager manager = unityContainer_.Resolve<IModuleManager>();
			IModuleCatalog catalog = unityContainer_.Resolve<IModuleCatalog>();
			foreach (var earlyModule in catalog.Modules.Where(
				m =>
				    {
				        var type = Type.GetType(m.ModuleType);
				        if (type == null) return false;
				        return type.GetInterface(typeof (IEarlyInitializedModule).FullName) != null;
				    }))
			{ 
				manager.LoadModule(earlyModule.ModuleName);
			}
		}

		internal static event EventHandler<EventArgs<string>> BeforeModuleInitialized;

		private static void OnBeforeModuleInitialized(object sender, EventArgs<string> e)
		{
			var copy = BeforeModuleInitialized;
			if (copy != null)
			{
				copy(sender, e);
			}
		}

		internal static event EventHandler<EventArgs<string>> AfterModuleInitialized;

		private static void OnAfterModuleInitialized(object sender, EventArgs<string> e)
		{
			var copy = AfterModuleInitialized;
			if (copy != null)
			{
				copy(sender, e);
			}
		}

	}
}
