﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Framework.cs#39 $
// $Change: 902331 $
// $DateTime: 2014/10/24 05:43:11 $
// $Author: caijin $

using System;
using System.Diagnostics;
using System.Reflection;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core; 
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.Concord;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Shell; 

namespace MorganStanley.MSDotNet.MSGui.Impl
{
    public sealed class Framework : IHideObjectMembers, IFramework
    {
        internal static bool UseLegacySplash = false;
        private readonly Bootstrapper _bootstrapper;
        private readonly IUnityContainer _container;
        private readonly System.Windows.Application _app;
        private readonly bool _winAppCreatedInternally;
        private readonly InitialSettings _initialSettings;

        public Framework()
            : this(string.Empty)
        {
        }

        public Framework(string configFile)
        {
            SmartApi.Extensions.EnablePostSharpResolver();
            _container = new UnityContainer();
            if (UnityExtensions.UnitySafeBehavior)
            {
                _container.RemoveAllExtensions();
                _container.AddExtension(new UnityClearBuildPlanStrategies());
                _container.AddExtension(new UnitySafeBehaviorExtension());
#pragma warning disable 612,618
                _container.AddExtension(new InjectedMembers());
#pragma warning restore 612,618
                _container.AddExtension(new UnityDefaultStrategiesExtension());

            }
            _winAppCreatedInternally = System.Windows.Application.Current == null;
            _app = System.Windows.Application.Current ?? new System.Windows.Application();

            _app.Dispatcher.BeginInvoke((Action) SetDefaultShutdownMode);
            
            _initialSettings = new InitialSettings();
            _bootstrapper = string.IsNullOrEmpty(configFile)
                                 ? new Bootstrapper(_container, _initialSettings)
                                 : new Bootstrapper(_container, configFile, _initialSettings);
            _bootstrapper.BeforeModuleInitialized += OnBeforeModuleInitialized;
            _bootstrapper.AfterModuleInitialized += OnAfterModuleInitialized; 
        }


        private void SetDefaultShutdownMode()
        {
            _app.ShutdownMode = System.Windows.ShutdownMode.OnExplicitShutdown;
        }

        private void OnBeforeModuleInitialized(object sender, ModuleInitializeArgs e)
        {
            var copy = BeforeModuleInitialized;
            if (copy != null)
            {
                copy(sender, e);
            }
        }

        private void OnAfterModuleInitialized(object sender, ModuleInitializeArgs e)
        {
            var copy = AfterModuleInitialized;
            if (copy != null)
            {
                copy(sender, e);
            }
        }

        public event EventHandler<ModuleInitializeArgs> BeforeModuleInitialized;
        public event EventHandler<ModuleInitializeArgs> AfterModuleInitialized;

        public void Start()
        {
            //PassportExtensionPoints.InvokeBeforeStart();
            // Enable Concord by default to use Concord.Configuration
            //this.EnableConcordInternal();

            if (_winAppCreatedInternally)
            {
                _app.Startup += OnAppStartup;
                _app.Run();
            }
            else
            {
                StartAll();
            }
        }

        public void AddPreInitializer<T>() where T : IPreInitializer, new()
        {
            AddPreInitializer(new T());
        }

        public void AddPreInitializer<T>(T preInitializer) where T : IPreInitializer
        {
            _bootstrapper.AddPreInitializer(preInitializer);
        }

        public void AddModule<T>()
        {
            Debug.Assert(
                typeof (T).GetInterface("imodule", true) != null ||
                typeof (T).GetInterface("IMSDesktopModule", true) != null);
            _bootstrapper.AddModule(typeof(T));
        }

        public void AddModule(Type moduleType, string moduleName)
        {
            _bootstrapper.AddModule(moduleType, moduleName);
        }

        public void AddModule(Type moduleType)
        {
            _bootstrapper.AddModule(moduleType);
        }

        internal  void AddModule(object moduleInfo)
        {
            _bootstrapper.AddModule(moduleInfo);
        }

        private void StartAll()
        {
            if (!SkipSplashScreen)
            {
                if (UseLegacySplash)
                {
                    SplashLauncher<SplashWindow>.Show(_bootstrapper.Application.Name,
                                                      "v" + _bootstrapper.Application.Version);
                }
                else
                {
                    SplashLauncher<ModernSplashWindow>.Show(_bootstrapper.Application.Name,
                                                            "v" + _bootstrapper.Application.Version);
                }
            }

            _bootstrapper.Run();
        }

        internal static bool SkipSplashScreen { get; set; } 
       
        public bool EnableLegacyDialog
        {
            get { return _initialSettings.EnableLegacyDialog; }
            set
            {
                _initialSettings.EnableLegacyDialog = value;
            }
        }

        private void OnAppStartup(object sender, System.Windows.StartupEventArgs e)
        {
            StartAll();
        }

        internal Bootstrapper Bootstrapper
        {
            get { return _bootstrapper; }
        }

        internal InitialSettings InitialSettings
        {
            get { return _initialSettings; }
        }

        internal IUnityContainer Container
        {
            get { return _container; }
        }

        public void SuppressConcordStatusBarItems()
        {
            ConcordInitializer.SuppressConcordStatusBarItems = true;
        }
    }
}
