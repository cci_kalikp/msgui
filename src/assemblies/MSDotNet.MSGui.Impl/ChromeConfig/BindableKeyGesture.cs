﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Input;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeConfig
{
    internal class BindableKeyGesture : ViewModelBase
    {
        private Key m_key;
        private ModifierKeys m_modifierKeys;
        private bool m_gestureEnabled;

        public event PropertyChangedEventHandler PropertyChanged;

        public Key Key
        {
            get
            {
                return m_key;
            }
            set
            {
                m_key = value;
                OnPropertyChanged("Key");
            }
        }

        public ModifierKeys ModifierKey
        {
            get
            {
                return m_modifierKeys;
            }
            set
            {
                m_modifierKeys = value;
                OnPropertyChanged("ModifierKey");
            }
        }

        public bool GestureEnabled
        {
            get
            {
                return m_gestureEnabled;
            }
            set
            {
                m_gestureEnabled = value;
                OnPropertyChanged("Enabled");
            }
        }
         
    }

    [ValueConversion(typeof(KeyGesture), typeof(BindableKeyGesture))]
    internal class KeyGestureConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var bindableGesture = new BindableKeyGesture {GestureEnabled = true};
            var gesture = value as KeyGesture;
            if (gesture == null)
            {
                bindableGesture.GestureEnabled = false;
                return bindableGesture;
            }
            bindableGesture.Key = gesture.Key;
            bindableGesture.ModifierKey = gesture.Modifiers;
            return bindableGesture;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var bindableGesture = value as BindableKeyGesture;
            if (bindableGesture == null || !bindableGesture.GestureEnabled) return null;
            return new KeyGesture(bindableGesture.Key, bindableGesture.ModifierKey);
        }
    }
}
