﻿using System;
using System.Globalization;
using System.Windows.Data;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeConfig
{
    internal class WidgetTextConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values[0] is IWidgetViewContainerConfigNode)
            {
                return (values[0] as IWidgetViewContainerConfigNode).CurrentName;
            }
            if (values[0] is IWidgetViewContainer)
            {
                var widgetViewContainer = (values[0] as IWidgetViewContainer);
                if (widgetViewContainer.Parameters == null)
                    return widgetViewContainer.FactoryID;
                var text = widgetViewContainer.Parameters.Text;
                if (string.IsNullOrEmpty(text)) return widgetViewContainer.FactoryID;
                return text;
            }
            return null;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return new object[] {};
        }
    }
}
