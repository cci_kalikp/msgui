﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeConfig
{
    /// <summary>
    /// Interaction logic for GesturePicker.xaml
    /// </summary>
    public partial class GesturePicker
    {
        private bool m_settingGesture;

        public GesturePicker()
        {
            InitializeComponent();
        }

        private void ResetGesture()
        {
            if (m_settingGesture) return;
            try
            {
                m_settingGesture = true;
                var old = SelectedKeyGesture;
                SelectedKeyGesture = null;
                SelectedKeyGesture = old;
                    
            }
            catch (NotSupportedException)
            {
                TaskDialog.ShowMessage("Gesture not allowed. Try a different combination.",
                                       "Gesture not allowed", TaskDialogCommonButtons.Close,
                                       VistaTaskDialogIcon.Warning);
                SelectedKeyGesture = new BindableKeyGesture
                    {
                        GestureEnabled = true,
                        Key = Key.None,
                        ModifierKey = ModifierKeys.None
                    };
            }
            finally
            {
                m_settingGesture = false;
            }
        }

        internal BindableKeyGesture SelectedKeyGesture
        {
            get { return (BindableKeyGesture)GetValue(SelectedKeyGestureProperty); }
            set { SetValue(SelectedKeyGestureProperty, value); }
        }

        internal static readonly DependencyProperty SelectedKeyGestureProperty =
            DependencyProperty.Register("SelectedKeyGesture", typeof(BindableKeyGesture), typeof(GesturePicker), new UIPropertyMetadata(new BindableKeyGesture { GestureEnabled = false }));

        private void KeySelectionChanged(object sender_, SelectionChangedEventArgs e_)
        {
            ResetGesture();
        }

        private void ModifierKeySelectionChanged(object sender_, SelectionChangedEventArgs e_)
        {
            ResetGesture();
        }

        private void EnabledChecked(object sender_, RoutedEventArgs e_)
        {
            ResetGesture();
        }

        private void EnableUnchecked(object sender_, RoutedEventArgs e_)
        {
            ResetGesture();
        }
    }

    [ValueConversion(typeof(WidgetViewContainer), typeof(bool))]
    internal class GestureSupportedConverter : IValueConverter
    {
        public object Convert(object value_, Type targetType_, object parameter_, CultureInfo culture_)
        {
            return value_ is WidgetViewContainer &&
                   (value_ as WidgetViewContainer).Parameters is ISupportsGesture;
        }

        public object ConvertBack(object value_, Type targetType_, object parameter_, CultureInfo culture_)
        {
            return null;
        }
    }
}
