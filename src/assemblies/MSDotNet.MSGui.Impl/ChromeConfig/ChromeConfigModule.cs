﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows; 
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.LauncherBar;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Ribbon;
using MorganStanley.MSDotNet.MSGui.Impl.Shell;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeConfig
{
    internal class ChromeConfigModule : IMSDesktopModule
    {
        private readonly IChromeRegistry m_registry;
        private readonly IChromeManager m_manager;
        private readonly IShell m_shell;
        private readonly IPersistenceService m_persistenceService; 

        public ChromeConfigModule(IChromeRegistry registry_, IChromeManager manager_, IShell shell_,
            IPersistenceService persistenceService_, IApplication application_)
        {
            m_registry = registry_;
            m_manager = manager_;
            m_shell = shell_;
            m_persistenceService = persistenceService_;
        }

        public void Initialize()
        {
            var rootWidgets = new List<IWidgetViewContainer>();
            var isRibbon = m_shell is ShellWithRibbon;
            rootWidgets.Add(m_manager[ChromeArea.ApplicationMenu]);
            if (isRibbon)
            {
                var ribbon = m_manager[ChromeArea.Ribbon] as RibbonRootArea;
                rootWidgets.Add(ribbon);
                rootWidgets.AddRange(ribbon.Children.Where(w_ => w_ is RibbonContextualArea));
                rootWidgets.Add(m_manager[ChromeArea.QAT]);
            }
            else
            {
                var launcherBar = m_manager[ChromeArea.LauncherBar] as LauncherBarRootArea;
                rootWidgets.Add(launcherBar);

            }
            var configViewModel = new ChromeConfigViewModel(m_manager, rootWidgets, m_persistenceService,
               ChromeManagerBase.DefaultWidgetFactoriesGlobal == true); 

            string title = isRibbon ? "Ribbon configuration" : "Launcher bar configuration"; 
            m_registry.RegisterWindowFactory("RibbonConfigUI", (container_, state_) =>
                                                                   {
                                                                       container_.Title = title;
                                                                       container_.Content = configViewModel;
                                                                       return true;
                                                                   }); 
            m_manager[ChromeManagerBase.MENU_COMMANDAREA_NAME].AddWidget("RibbonConfigUIButton", new InitialButtonParameters()
                {
                    Text = title,
                    Click = (sender_, args_) => CreateView()
                });
            System.Windows.Application.Current.Resources.MergedDictionaries.Add(
                new ResourceDictionary
                {
                    Source = new Uri("MSDotNet.MSGui.Impl;component/ChromeConfig/ChromeConfigResources.xaml",
                        UriKind.RelativeOrAbsolute)
                });
        }

        private IWindowViewContainer CreateView()
        {
            return m_manager.CreateWindow("RibbonConfigUI",
                                          new InitialWindowParameters
                                          {
                                              //IsModal = true,
                                              Transient = true,
                                              Singleton = true,
                                              InitialLocation = InitialLocation.FloatingOnly,
                                              SizingMethod = SizingMethod.Custom,
                                              Width = 700,
                                              Height = 500
                                          });
        }
    }
}
