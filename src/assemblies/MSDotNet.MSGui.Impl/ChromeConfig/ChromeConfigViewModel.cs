﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;
using System.Linq;
using System.Windows.Threading;
using System.Xml.Linq;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager.Helpers;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.ChromeManager.ChromeArea.Ribbon;
using RelayCommand = MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.RelayCommand;

namespace MorganStanley.MSDotNet.MSGui.Impl.ChromeConfig
{
    //todo: there is bug here, if delayed layout is used, the state is not restored correctly
    internal class ChromeConfigViewModel : INotifyPropertyChanged
    {
        internal const string ChromeConfigPersistorId = "{5F19DAB9-F0B5-4C66-81A1-F604A7F6FA71}.RibbonConfigPersistorId";

        private readonly ChromeManagerBase m_chromeManager;
        private IWidgetViewContainer m_lastSelectedLockerItem;
        private IWidgetViewContainer m_selectedLockerItem;
        private readonly IEnumerable<IWidgetViewContainer> m_rootWidgets;
        private readonly IPersistenceService m_persistenceService;
        private XDocument m_resetState;
        private readonly Dictionary<IWidgetViewContainer, IWidgetViewContainerConfigNode> m_rootWidgetAffinity;
        private readonly RibbonQATArea m_qat;

        public ChromeConfigViewModel(IChromeManager chromeManager_, IEnumerable<IWidgetViewContainer> rootWidgets_,
            IPersistenceService persistenceService_, bool global_)
        {
            m_chromeManager = chromeManager_ as ChromeManagerBase;
            m_rootWidgets = rootWidgets_;
            m_persistenceService = persistenceService_;
            m_rootWidgetAffinity = new Dictionary<IWidgetViewContainer, IWidgetViewContainerConfigNode>();

            CreateHideItemCommand();
            UnhideItemCommand = new RelayCommand(DoUnhideItem, CanUnhideItem);
            MoveItemUpCommand = new RelayCommand(DoMoveItemUp, CanMoveUp);
            MoveItemDownCommand = new RelayCommand(DoMoveItemDown, CanMoveDown);
            DuplicateCommand = new RelayCommand(DoDuplicate, CanDuplicate);
            RenameCommand = new RelayCommand(DoRename, CanRename);
            ResetCommand = new RelayCommand(DoReset, CanReset);

            if (global_)
            {
                m_persistenceService.AddGlobalPersistor(ChromeConfigPersistorId, RestoreStateHandler, SaveStateHandler);
            }
            else
            {
                m_persistenceService.AddPersistor(ChromeConfigPersistorId, RestoreStateHandler, SaveStateHandler);
            }

            if (m_chromeManager.GetWidget(ChromeArea.QAT) != null)
            {
                m_qat = (RibbonQATArea) m_chromeManager[ChromeArea.QAT];
            }
            m_chromeManager.AfterCreateChrome += OnChromeCreated;

        }

        #region Properties

        public IEnumerable<IWidgetViewContainer> RootWidgets 
        { 
            get
            {
                return m_rootWidgets;
            } 
        }

        public ObservableCollection<IWidgetViewContainer> LockerWidgets
        {
            get
            {
                return (m_chromeManager[ChromeArea.Locker] as ObservableWidgetViewContainer).ObservableChildren;
            }
        }

        public IWidgetViewContainer SelectedItem { get; set; }

        public IWidgetViewContainer SelectedLockerItem
        {
            get
            {
                return m_selectedLockerItem;
            }
            set
            {
                m_selectedLockerItem = value;
                if (value != null) m_lastSelectedLockerItem = value;
            }
        }

        public IWidgetViewContainerConfigNode SelectedRootWidget { get; set; }
        
        #endregion

        #region Commands

        public ICommand HideItemCommand { get; private set; }
        public ICommand UnhideItemCommand { get; private set; }
        public ICommand MoveItemUpCommand { get; private set; }
        public ICommand MoveItemDownCommand { get; private set; }
        public ICommand DuplicateCommand { get; private set; }
        public ICommand RenameCommand { get; private set; }
        public ICommand ResetCommand { get; private set; }

        private void CreateHideItemCommand()
        {
            HideItemCommand = new RelayCommand(
                o_ =>
                {
                    if (SelectedRootWidget is RibbonQATArea && !(SelectedItem.Parent is RibbonQATArea))
                    {
                        (SelectedRootWidget as WidgetViewContainer).RemoveVisual(SelectedItem);
                        (SelectedRootWidget as WidgetViewContainer).RemoveLogical(SelectedItem);
                        return;
                    }
                    m_rootWidgetAffinity[SelectedItem] = SelectedRootWidget;
                    m_chromeManager.MoveWidget(SelectedItem, m_chromeManager[ChromeArea.Locker], null);
                },
                o_ => SelectedItem != null);
        }

        private void DoUnhideItem(object o_)
        {
            if (SelectedRootWidget.CanAddwidget(m_lastSelectedLockerItem))
            {
                m_chromeManager.MoveWidget(m_lastSelectedLockerItem, SelectedRootWidget, null);
                return;
            }
            m_chromeManager.MoveWidget(m_lastSelectedLockerItem, SelectedItem, null);
        }

        private bool CanUnhideItem(object o_)
        {
            if (SelectedLockerItem == null) return false;
            if (m_rootWidgetAffinity.ContainsKey(SelectedLockerItem) &&
                m_rootWidgetAffinity[m_lastSelectedLockerItem] != SelectedRootWidget)
            {
                return false;
            }
            if (SelectedItem is IWidgetViewContainerConfigNode &&
                (SelectedItem as IWidgetViewContainerConfigNode).CanAddwidget(SelectedLockerItem))
                return true;
            if (SelectedRootWidget.CanAddwidget(SelectedLockerItem)) return true;
            return false;
        }

        private void DoMoveItemUp(object o_)
        {
            var idx = GetFlattenedWidgets(SelectedRootWidget, SelectedItem).ToList().IndexOf(SelectedItem);
            var previous = GetFlattenedWidgets(SelectedRootWidget, SelectedItem).ElementAt(idx - 1);
            if (SelectedRootWidget is RibbonQATArea)
            {
                SelectedRootWidget.SwapChildren(previous, SelectedItem);
            }
            else if (previous.Parent == SelectedItem.Parent)
            {
                (previous.Parent as IWidgetViewContainerConfigNode).SwapChildren(previous, SelectedItem);
            }
            else
            {
                m_chromeManager.MoveWidget(SelectedItem, previous.Parent, null);
            }
        }

        private bool CanMoveDown(object o_)
        {
            return SelectedItem != null &&
                   GetFlattenedWidgets(SelectedRootWidget, SelectedItem).Last() != SelectedItem;
        }

        private void DoMoveItemDown(object o_)
        {
            var idx = GetFlattenedWidgets(SelectedRootWidget, SelectedItem).ToList().IndexOf(SelectedItem);
            var next = GetFlattenedWidgets(SelectedRootWidget, SelectedItem).ElementAt(idx + 1);
            if (SelectedRootWidget is RibbonQATArea)
            {
                SelectedRootWidget.SwapChildren(next, SelectedItem);
            }
            else if (next.Parent == SelectedItem.Parent)
            {
                (next.Parent as IWidgetViewContainerConfigNode).SwapChildren(next, SelectedItem);
            }
            else
            {
                m_chromeManager.MoveWidget(SelectedItem, next.Parent, null);
            }
        }

        private bool CanMoveUp(object o_)
        {
            return SelectedItem != null &&
                   GetFlattenedWidgets(SelectedRootWidget, SelectedItem).First() != SelectedItem;
        }

        private bool CanDuplicate(object o_)
        {
            return (SelectedItem != null 
                && SelectedItem.Parent is IWidgetViewContainerConfigNode
                && SelectedItem is IWidgetViewContainerConfigNode);
        }

        private void DoDuplicate(object o_)
        {
            var id = SelectedItem.Parameters.Text;
            var parameters = (SelectedItem.Parent as IWidgetViewContainerConfigNode).AddDefault(ref id);
            parameters.Text = SelectedItem.Parameters.Text;
            m_chromeManager.AddWidget(Guid.NewGuid().ToString("N"), parameters, SelectedItem.Parent,
                                                             false);
        }

        private bool CanRename(object o_)
        {
            return SelectedItem is IWidgetViewContainerConfigNode;
        }

        private void DoRename(object o_)
        {
            var result = TaskDialog.Show(new TaskDialogOptions
                                {
                                    UserInputEnabled = true,
                                    Content = "Please enter a new name for the item",
                                    Title = "Item rename",
                                    AllowDialogCancellation = true,
                                    CommonButtons = TaskDialogCommonButtons.OKCancel,
                                    InitialUserInput = (SelectedItem as IWidgetViewContainerConfigNode).CurrentName
                                });
            if (result.Result != TaskDialogSimpleResult.Ok) return;
            (SelectedItem as IWidgetViewContainerConfigNode).Rename(result.UserInput);
        }

        private static bool CanReset(object o_)
        {
            return true;
        }

        private void DoReset(object o_)
        {
            RestoreStateHandler(m_resetState);
            foreach (var id in m_chromeManager.GetPersistableWidgets(false))
            {
                m_chromeManager.LoadWidgetWithState(id, null);
            }
        }

        #endregion

        #region Private methods

        private void OnChromeCreated(object sender_, EventArgs eventArgs_)
        {
            if (m_qat == null || !ChromeManagerBase.WillLoadChromeFromState)
            { 
                m_resetState = SaveStateHandler();
            }
            m_chromeManager.AfterCreateChrome -= OnChromeCreated;
             
        }

        private XDocument SaveStateHandler()
        {
            var dummyRoot = new XElement("Root");
            foreach (IWidgetViewContainerConfigNode widgetViewContainer in RootWidgets)
            {
                dummyRoot.Add(widgetViewContainer.SaveState());
            }
            return new XDocument(dummyRoot);
        }

        private void RestoreStateHandler(XDocument xmlState_)
        {
            if (xmlState_ != null)
            {
                ChromeManagerBase.WillLoadChromeFromState = false;  
                if (m_qat != null && !ChromeManagerBase.StartupCompleted)
                {

                    EventHandler handler = null;
                    handler = (sender_, args_) =>
                                  {
                                      DoRestoreState(xmlState_);
                                      m_resetState = SaveStateHandler();
                                      m_qat.AllPlacementsProcessed -= handler;
                                  };
                    m_qat.AllPlacementsProcessed += handler;
                }
                else
                {
                    if (m_qat != null && xmlState_ != m_resetState)
                    {
                        m_qat.SuspendLoadLayout();
                    }
                    DoRestoreState(xmlState_); 
                    m_resetState = SaveStateHandler();
                    if (m_qat != null && xmlState_ != m_resetState)
                    {
                        m_qat.ResumeLoadLayout(); 
                    }
                }
            }
            else if (m_qat != null)
            {
                //m_qat.AllPlacementsProcessed += OnChromeCreated;
                m_resetState = SaveStateHandler();
            }
        }

        private void DoRestoreState(XDocument xmlState_)
        {
            HideAllWidgets((IWidgetViewContainerConfigNode) m_chromeManager[ChromeArea.Locker]);
            foreach (IWidgetViewContainerConfigNode widget in RootWidgets)
            {
                IWidgetViewContainerConfigNode widget1 = widget;
                var stateEl = xmlState_.Root.Elements()
                    .FirstOrDefault(el_ => el_.Attribute("name").Value == widget1.FactoryID ||
                                           (el_.Attribute("currentName") != null &&
                                            el_.Attribute("currentName").Value == widget1.CurrentName));
                if (stateEl != null)
                {
                    HideAllWidgets(widget);
                    widget.LoadState(stateEl);
                }
            }
        }

        private void HideAllWidgets(IWidgetViewContainerConfigNode rootWidget_)
        {
            var locker = m_chromeManager[ChromeArea.Locker];
            var widgetsToHide = new List<IWidgetViewContainer>();
            Action<IWidgetViewContainerConfigNode> hideAllWidgetsRec = null;
            hideAllWidgetsRec =
                delegate (IWidgetViewContainerConfigNode widget_)
                    {
                        foreach (var child in widget_.ObservableChildren)
                        {
                            if (child is IWidgetViewContainerConfigNode)
                            {
                                hideAllWidgetsRec(child as IWidgetViewContainerConfigNode);
                                if (child.Parent != null)
                                {
                                    widgetsToHide.Add(child);
                                }
                            }
                            else
                            {
                                widgetsToHide.Add(child);
                            }                            
                        }
                    };
            hideAllWidgetsRec(rootWidget_);
            foreach (var widgetViewContainer in widgetsToHide)
            {
                if (rootWidget_ is RibbonQATArea && !(widgetViewContainer.Parent is RibbonQATArea))
                {
                    (rootWidget_ as WidgetViewContainer).RemoveVisual(widgetViewContainer);
                    (rootWidget_ as WidgetViewContainer).RemoveLogical(widgetViewContainer);
                }
                else
                {
                    m_chromeManager.MoveWidget(widgetViewContainer, locker, null);
                }
            }
        }

        private static IEnumerable<IWidgetViewContainer> GetFlattenedWidgets(IWidgetViewContainer container_,
            IWidgetViewContainer selectedItem_)
        {
            if (container_ is IWidgetViewContainerConfigNode &&
                !(container_.GetType().IsInstanceOfType(selectedItem_))
                )
            {
                if ((container_ as IWidgetViewContainerConfigNode).ObservableChildren.Count == 0)
                {
                    return Yield(new WidgetViewContainer("{placeholder}") { Parent = container_ });
                }
                return (container_ as IWidgetViewContainerConfigNode)
                    .ObservableChildren
                    .Select(w_ => GetFlattenedWidgets(w_, selectedItem_))
                    .SelectMany(x_ => x_);
            }
            return Yield(container_);
        }

        private static IEnumerable<T> Yield<T>(T item_)
        {
            yield return item_;
        }

        #endregion

        public event PropertyChangedEventHandler PropertyChanged;

        public void InvokePropertyChanged(string name_)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(name_));
        }
    }
}
