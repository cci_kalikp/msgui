﻿using System.Drawing;
using System.Windows.Media;
using Color = System.Windows.Media.Color;

namespace MorganStanley.MSDotNet.MSGui.Impl.ModernTheme
{
    internal static class ModernThemeHelper
    {
        public static bool IsModernThemeEnabled { get; set; }
        public static ModernThemeBase BaseColor { get; set; }

        public static Color AccentColor { get; set; }
        public static Color SecondaryAccentColor { get; set; }
        public static Color AccentForegroundColor { get; set; }
        public static Color SecondaryAccentForegroundColor { get; set; }

        static ModernThemeHelper()
        {
            AccentColor = Colors.LightSteelBlue;
            SecondaryAccentColor = Colors.LightBlue;
            AccentForegroundColor = Colors.Black;
            SecondaryAccentForegroundColor = Colors.Black;
        }
    }
}
