﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Impl.Options
{
    public class SearchablePageBasedOnViewModel:SearchableItemsProvider, ISearchablePage
    {
        private readonly object viewModel;
        public SearchablePageBasedOnViewModel(object viewModel_)
        {
            viewModel = viewModel_;
        }
        public override PropertyDescriptorCollection GetProperties()
        {
            return TypeDescriptor.GetProperties(viewModel);
        } 

        public int GetMatchCount(string textToSearch_)
        {
            return SearchableItems.Count(item_ => item_.MatchesSearchInput(textToSearch_));
        }
    }
}
