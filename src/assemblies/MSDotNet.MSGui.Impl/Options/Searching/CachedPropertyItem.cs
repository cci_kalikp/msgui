﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using MorganStanley.MSDotNet.My;

namespace MorganStanley.MSDotNet.MSGui.Impl.Options
{
    public class CachedPropertyItem : ISearchableItem
    {
        private static readonly IMSLogger Logger = MSLoggerFactory.CreateLogger<CachedPropertyItem>();
        private readonly PropertyDescriptor property;

        private IEnumerable<string> itemValues;

        public string Group { get; set; }
        public string Display
        {
            get; set;
        }

        public string Name { get; private set; }

        public string Category { get; set; } 
        public SearchableCategoryAttribute SearchableCategoryAttribute { get; set; }
        public List<SearchTagsAttribute> SearchTags { get; set; }

        /// <summary>
        /// At the time of registration Values May not have been initialized in the provider --> Particular case : OptionsWatch: Exchanges
        /// So we don't need to read the values until later when we really need this.
        /// </summary>
        public Func<IEnumerable<string>> ItemValuesGenerator { private get; set; }

        private IEnumerable<string> ItemValues
        {
            get
            {
                if (itemValues != null)
                {
                    return itemValues;
                }
                if (ItemValuesGenerator != null)
                {
                    try
                    {
                        return itemValues = ItemValuesGenerator.Invoke();
                    }
                    catch (Exception exception)
                    {
                        if (Logger.IsErrorLoggable())
                        {
                            Logger.ErrorWithFormat("Error getting ItemValues for Property : {0}. Error {1}", property.ComponentType.FullName + "." + property.Name,
                                                   exception);
                        }
                    }
                }
                return itemValues = Enumerable.Empty<string>();
            }
        }

        public string Description
        {
            get; set;
        }
         
        public CachedPropertyItem(PropertyDescriptor property_)
        {
            SearchTags = new List<SearchTagsAttribute>();
            Display = property_.DisplayName;
            Name = property_.Name;
            Description = property_.Description; 
            property = property_;
        }

         
        public bool MatchesSearchInput(string searchText_)
        {

            CompareInfo compareInfo = CultureInfo.InvariantCulture.CompareInfo;
             
            bool matches =!string.IsNullOrWhiteSpace(searchText_) && 
                (SearchableCategoryAttribute != null &&
                    SearchableCategoryAttribute.MatchesSearchInput(searchText_)) ||
                   SearchTags.Any(tag_ => tag_.MatchesSearchInput(searchText_)) ||
                   (compareInfo.IndexOf(Description, searchText_, CompareOptions.IgnoreCase) != -1) ||
                   (compareInfo.IndexOf(Display, searchText_, CompareOptions.IgnoreCase) != -1) ||
                   (!string.IsNullOrWhiteSpace(Category) &&
                    compareInfo.IndexOf(Category, searchText_, CompareOptions.IgnoreCase) != -1) ||
                   (ItemValues != null &&
                    ItemValues.Any(val_ => compareInfo.IndexOf(val_, searchText_, CompareOptions.IgnoreCase) != -1));

            SettingItemDescriptor descriptor = this.property as SettingItemDescriptor;
            if (descriptor != null)
            {
                descriptor.SearchText = searchText_; 
                descriptor.IsSearchMatched = matches; 
            }  
            return matches;
        }
         
    }
}
