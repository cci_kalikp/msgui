﻿namespace MorganStanley.MSDotNet.MSGui.Impl.Options
{

    public interface ISearchableItem
    {
        bool MatchesSearchInput(string searchText_);
    }

}