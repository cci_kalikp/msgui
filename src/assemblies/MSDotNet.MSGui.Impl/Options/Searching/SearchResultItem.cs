﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MorganStanley.MSDotNet.MSGui.Controls.SearchBox;

namespace MorganStanley.MSDotNet.MSGui.Impl.Options
{
    public class SearchResultItem : SearchResultItemBase
    {
        public OptionPage Page { get; set; }

        public override string ItemPath
        {
            get
            {
                string path = string.Join(" → ", Page.Paths);
                if (!string.IsNullOrWhiteSpace(ItemName))
                {
                    path += " → " + ItemName;
                }
                return path;
            }
        }
    }
}  
