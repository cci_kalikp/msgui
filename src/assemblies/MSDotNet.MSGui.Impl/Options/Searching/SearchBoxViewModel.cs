﻿using System;
using System.Collections.Generic; 
using System.ComponentModel; 
using System.Linq;
using System.Threading;
using MorganStanley.MSDotNet.MSGui.Controls.SearchBox;

namespace MorganStanley.MSDotNet.MSGui.Impl.Options
{
    public enum SearchDepth
    {
        [Description("Search in property name, description etc.")] PropertiesOnly,

        [Description("Search in header and categories along with property fields")] HeadersCategoriesAndProperties
    }

    public class SearchBoxViewModel : SearchBoxViewModelBase
    {
        private readonly Action<string, IList<OptionPage>> showSearchResultCallback;  
        private IList<OptionPage> searchablePages;
        private SearchDepth searchDepth = SearchDepth.HeadersCategoriesAndProperties;  
        public SearchDepth SearchDepth
        {
            get { return searchDepth; }
            set
            {
                if (searchDepth != value)
                {
                    searchDepth = value;
                    OnPropertyChanged("SearchDepth");
                    lastResultSearchText = string.Empty;
                    UpdateSearchResult();
                }
            }
        }
         
        public IList<OptionPage> SearchablePages
        {
            get { return searchablePages; }
            set
            {
                searchablePages = value;
                OnPropertyChanged("SearchablePages");
            }
        }
         
        public IList<OptionPage> SearchResult { get; private set; } 
        public SearchBoxViewModel(
            Action<string, IList<OptionPage>> showSearchResultCallback_)
        { 
            SearchDepth = SearchDepth.HeadersCategoriesAndProperties;
            showSearchResultCallback = showSearchResultCallback_;
            this.WatermarkText = "search settings";

        }

        protected override void UpdateSearchResultCore(CancellationToken ct_)
        {
            if (string.IsNullOrWhiteSpace(SearchText) || (SearchablePages == null) || SearchablePages.Count == 0)
            {
                if (!EnableTypeaheadDropDown)
                {
                    if (showSearchResultCallback != null)
                    {
                        showSearchResultCallback(string.Empty, null);
                    }                
                }
                SearchResultItems.Clear();
                SelectedItem = null;
                ShowDropDown = false;
                return;
            }
            if (ct_.IsCancellationRequested)
            {
                return;
            }

            IList<OptionPage> searchSet = (!string.IsNullOrWhiteSpace(lastResultSearchText)) &&
                                                                SearchText.StartsWith(lastResultSearchText) && !AsyncSearch
                                                                    ? SearchResult
                                                                    : SearchablePages;
            if (!AsyncSearch)
            {
                SearchResult = searchSet.Where(
                    page_ => page_.UpdateMatchResult(SearchText, SearchDepth) > 0
                    ).ToList();

            }
            else
            {
                var result = new List<OptionPage>();
                foreach (var optionPage in searchSet)
                {
                    if (ct_.IsCancellationRequested)
                    {
                        return;
                    }
                    if (optionPage.UpdateMatchResult(SearchText, SearchDepth) > 0)
                    {
                        result.Add(optionPage);
                    }
                }

            }


            if (!EnableTypeaheadDropDown)
            {
                if (showSearchResultCallback != null)
                {
                    showSearchResultCallback(SearchText, SearchResult);
                }
                base.OnPropertyChanged("SearchResult");
            }
            else
            {
                if (ct_.IsCancellationRequested)
                {
                    return;
                } 
                List<ISearchResultItem> items = new List<ISearchResultItem>();
                if (SearchResult.Count > 0)
                {
                    foreach (var optionPage in SearchResult)
                    {
                        items.AddRange(optionPage.MatchedItems.Values);
                    }
                }
                ShowDropDown = items.Count > 0;
                this.SearchResultItems = items;
                base.OnPropertyChanged("SearchResultItems");
            }
        }
    }

}
