﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Impl.Options
{
    public interface ISearchablePage
    {
        int GetMatchCount(string textToSearch_);
    }

    public interface ISubItemAwareSeachablePage
    {
        IList<KeyValuePair<string, object>> GetMatchedItems(string textToSearch_);
        void LocateItem(object item_);
    }
}
