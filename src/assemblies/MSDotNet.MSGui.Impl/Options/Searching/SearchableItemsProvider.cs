﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;
using MorganStanley.MSDotNet.My;

namespace MorganStanley.MSDotNet.MSGui.Impl.Options
{
    public abstract class SearchableItemsProvider:ViewModelBase
    {
        private static readonly IMSLogger Logger =
    MSLoggerFactory.CreateLogger<SearchableItemsProvider>();
        private IList<ISearchableItem> searchableItems; 
        public IList<ISearchableItem> SearchableItems
        {
            get
            {
                if (searchableItems == null)
                {
                    BuildSearchableItems();
                }
                return searchableItems;
            }
        }

        public abstract PropertyDescriptorCollection GetProperties();

        protected virtual void BuildSearchableItems()
        {
            searchableItems = new List<ISearchableItem>();
            foreach (PropertyDescriptor property in GetProperties())
            {
                if (property.Attributes.OfType<BrowsableAttribute>().Any(b_ => !b_.Browsable))
                {
                    continue;
                }

                var cachedProperty = new CachedPropertyItem(property);

                foreach (Attribute attribute in property.Attributes)
                {
                    if (attribute is CategoryAttribute)
                    {
                        cachedProperty.Category = (attribute as CategoryAttribute).Category;
                        if (attribute is SearchableCategoryAttribute)
                        {
                            cachedProperty.SearchableCategoryAttribute = attribute as SearchableCategoryAttribute;
                        }
                    }
                    if (attribute is SearchTagsAttribute)
                    {
                        cachedProperty.SearchTags.Add(attribute as SearchTagsAttribute);
                    }

                    if (attribute.GetType().FullName == "Xceed.Wpf.Toolkit.PropertyGrid.Attributes.ItemsSourceAttribute")
                    {
                        ItemSourceAttributeHelper.SetItemGenerator(attribute, cachedProperty);
                    }
                }
                searchableItems.Add(cachedProperty);

                OnPropertyChanged("SearchableItems");
            }
        }

    }
}
