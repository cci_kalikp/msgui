﻿using System;

namespace MorganStanley.MSDotNet.MSGui.Impl.Options
{
    /// <summary>
    /// The class allows you to define a custom template for the property. 
    /// It Works for all properties within the standard property grid framework.
    /// For a normal property with the showInPopup=false, it acts as a custom editor.
    /// For a property to be shown in the grid, the template is applied on the  view model defined.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public sealed class CustomTemplateAttribute : Attribute
    {
        public CustomTemplateAttribute(string templateKey_, bool showInPopup_ = false)
        {
            TemplateKey = templateKey_;
            ShowInPopup = showInPopup_;
        }

        public string TemplateKey { get; private set; }
        public bool ShowInPopup { get; private set; }
    }
}