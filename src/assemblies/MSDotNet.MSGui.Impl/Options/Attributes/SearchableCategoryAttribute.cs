﻿using System;
using System.ComponentModel;
using System.Globalization;

namespace MorganStanley.MSDotNet.MSGui.Impl.Options
{
    /// <summary>
    /// For the property grid, displays the category that the properties are grouped under
    /// This goes with the SearchableCategoryAttribute  which provides any extended explanation 
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public sealed class SearchableCategoryAttribute : CategoryAttribute, ISearchableItem
    {
        public SearchableCategoryAttribute(string category_)
            : base(category_)
        {
        }

        public bool MatchesSearchInput(string searchText_)
        {
            if (string.IsNullOrEmpty(searchText_))
            {
                throw new ArgumentNullException("searchText_");
            }

            return CultureInfo.InvariantCulture.CompareInfo.IndexOf(Category, searchText_, CompareOptions.IgnoreCase) !=
                   -1;
        }
    }
}
