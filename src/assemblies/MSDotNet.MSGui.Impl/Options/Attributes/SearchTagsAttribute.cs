﻿using System;
using System.Globalization;
using System.Linq;

namespace MorganStanley.MSDotNet.MSGui.Impl.Options
{
    /// <summary>
    /// Add tags to make things easily searchable. Tags are string params
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public sealed class SearchTagsAttribute : Attribute, ISearchableItem
    {
        private readonly string[] tags;

        public SearchTagsAttribute(params string[] searchTags_)
        {
            if (searchTags_ == null)
            {
                throw new ArgumentNullException("searchTags_");
            }

            tags = searchTags_;
        }

        public bool MatchesSearchInput(string searchText_)
        {
            return
                tags.Any(
                    tag_ =>
                    CultureInfo.InvariantCulture.CompareInfo.IndexOf(tag_, searchText_, CompareOptions.IgnoreCase) != -1);
        }
    }
}