﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MorganStanley.MSDotNet.MSGui.Impl.Options
{
    public sealed class BindableVisibilityAttribute : Attribute
    { 
        public BindableVisibilityAttribute(string propertyName_, bool reverse_ = false)
        {
            DependsOn = propertyName_;
            Reverse = reverse_;
        }

        public string DependsOn { get; set; }
        public bool Reverse { get; set; }
    }
}
