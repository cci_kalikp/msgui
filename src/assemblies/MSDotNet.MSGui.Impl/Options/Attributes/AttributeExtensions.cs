﻿using System;
using System.ComponentModel;  

namespace MorganStanley.MSDotNet.MSGui.Impl.Options
{
    internal static class AttributeExtensions
    {
        internal static T GetAttribute<T>(this AttributeCollection attributeCollection_) where T : Attribute
        {
            return attributeCollection_[typeof (T)] as T;
        }

        internal static T GetAttribute<T>(this PropertyDescriptor propertyDescriptor_) where T : Attribute
        {
            AttributeCollection attributeCollection = propertyDescriptor_.Attributes;
            return attributeCollection.GetAttribute<T>();
        }
    }
}