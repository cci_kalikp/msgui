﻿using System;

namespace MorganStanley.MSDotNet.MSGui.Impl.Options
{
    /// <summary>
    /// Is responsible for defining the property name  that defines the IsEnabled property
    /// eg. a property "Time Zone" could be made non-editable by defining a dependency 
    /// using the attribute on "CanEditTimeZoneProperty" which returns a boolean value
    /// as per requirement
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public sealed class BindableEnablementAttribute : Attribute
    {

        public BindableEnablementAttribute(string propertyName_, bool reverse_=false)
        {
            DependsOn = propertyName_;
            Reverse = reverse_;
        }

        public string DependsOn { get; set; }
        public bool Reverse { get; set; }
    }
}