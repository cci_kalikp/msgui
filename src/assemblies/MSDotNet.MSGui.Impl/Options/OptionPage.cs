﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using MorganStanley.MSDotNet.MSGui.Core;
using System.Linq;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MorganStanley.MSDotNet.My;

namespace MorganStanley.MSDotNet.MSGui.Impl.Options
{
    public class OptionPage
    {
        public readonly string Path;
        public readonly string Name;
        public readonly string FullPath;
        private string[] paths; 
        private ISearchablePage searchablePage;
        private ISubItemAwareSeachablePage searchablePage2; 
        public readonly IOptionView View;

        private static readonly IMSLogger logger = MSLoggerFactory.CreateLogger<OptionPage>();
        private readonly IPersistenceService persistenceService;
        private readonly PersistenceProfileService persistenceProfileService;

        internal OptionPage(IPersistenceService persistence_, PersistenceProfileService persistenceProfileService_, IOptionView view_, string path_, string name_)
        {
            this.persistenceProfileService = persistenceProfileService_;
            this.persistenceService = persistence_;
            MatchedItems = new SortedDictionary<string, SearchResultItem>();
            View = view_;
            Path = path_;
            Name = name_;
            FullPath = name_; 
            if (!string.IsNullOrEmpty(path_))
            {
                FullPath = path_.TrimEnd(SideNavigationItem.PathSeparator.ToArray()) + SideNavigationItem.PathSeparator +
                           FullPath;
            }
            this.HelpCommand = new DelegateCommand(o_=> this.Help());
            this.SaveCommand = new DelegateCommand(o=> this.Save());
        }

        public IList<string> Paths
        {
            get
            {
                if (paths == null)
                {
                    string[] tmp = string.IsNullOrEmpty(Path) ? new string[0] :
                        Path.Split(new string[] { SideNavigationItem.PathSeparator }, StringSplitOptions.RemoveEmptyEntries); 
                    paths = new string[tmp.Length + 1];
                    Array.Copy(tmp, paths, tmp.Length);
                    paths[tmp.Length] = Name;
                }
                return paths;
            }
        }

        public int MatchCount { get; private set; }
        public IDictionary<string, SearchResultItem> MatchedItems { get; private set; }

        public ISubItemAwareSeachablePage SubItemAwarePage
        {
            get
            {
                EnsureSearchablePage();
                return searchablePage2;
            }
        }

        private void EnsureSearchablePage()
        {
            if (searchablePage == null && searchablePage2 == null)
            {
                if (View is IOptionViewWithModel && !(View is ISearchablePage) && !(View is ISubItemAwareSeachablePage))
                {
                    object viewModel = (View as IOptionViewWithModel).ViewModel;
                    if (viewModel == null) return;
                    searchablePage = viewModel as ISearchablePage;
                    searchablePage2 = viewModel as ISubItemAwareSeachablePage;
                    if (searchablePage == null && searchablePage2 == null)
                    {
                        searchablePage = new SearchablePageBasedOnViewModel(viewModel);
                    }
                }
                if (searchablePage == null && searchablePage2 == null)
                {
                    searchablePage = View as ISearchablePage;
                    searchablePage2 = View as ISubItemAwareSeachablePage;
                }
            }
        }

        public int UpdateMatchResult(string searchText_, SearchDepth searchDepth_)
        {
            EnsureSearchablePage();
            MatchCount = 0;
            MatchedItems.Clear();
            if (string.IsNullOrWhiteSpace(searchText_)) return MatchCount;
            if (searchablePage2 != null)
            {
                var matchedSubItems = searchablePage2.GetMatchedItems(searchText_);
                MatchCount = matchedSubItems.Count;
                if (MatchCount > 0)
                {
                    foreach (var matchedSubItem in matchedSubItems)
                    {
                        SearchResultItem item = new SearchResultItem()
                        {
                            Item = matchedSubItem.Value,
                            ItemName = matchedSubItem.Key,
                            Page = this,
                            SearchText = searchText_
                        };
                        string itemPath = item.ItemPath;
                        if (!MatchedItems.ContainsKey(itemPath))
                        {
                            MatchedItems.Add(itemPath, item);
                        }
                    }
                }
            }
            else if (searchablePage != null)
            {
                MatchCount = searchablePage.GetMatchCount(searchText_.Trim());
                if (MatchCount > 0)
                {
                    var item = new SearchResultItem() { Page = this, SearchText = searchText_ };
                    MatchedItems.Add(item.ItemPath, item);
                } 
            }
            if (searchDepth_ == SearchDepth.HeadersCategoriesAndProperties &&
                FullPath.ToLower().Contains(searchText_.ToLower()))
            {
                MatchCount++;
                var item = new SearchResultItem() { Page = this, SearchText = searchText_ };
                string itemPath = item.ItemPath;
                if (!MatchedItems.ContainsKey(itemPath))
                {
                    MatchedItems.Add(itemPath, item);
                }  
            } 
            return MatchCount;
        }

        public ICommand HelpCommand { get; private set; } 

        public ICommand SaveCommand { get; private set; }

        public bool UseGlobalPersistor { get; internal set; }

        public bool ProfileSaveNeeded { get; private set; }

        public bool GlobalSaveNeeded { get; private set; } 

        public UIElement RenderPage()
        { 
            IOptionView currentOptionPage = this.View;  
            try
            {
                currentOptionPage.OnDisplay();
            }
            catch (Exception exc)
            {
                logger.Info("Option display failed", exc);
                return new TextBlock(new Run("The option dialog failed to load."));
            } 
            var currentContent = currentOptionPage.Content;
            var contentPresenter = LogicalTreeHelper.GetParent(currentContent) as ContentControl;
            if (contentPresenter != null)
            {
                contentPresenter.Content = null;
            }
            return currentContent;
        }

        public void Help()
        {
            this.View.OnHelp();
        }
        public void Cancel()
        {
            try
            {
                this.View.OnCancel();
            }
            catch (Exception exc)
            {
                logger.Error("Option cancel failed", exc);

            }
        }

        public bool Save()
        {
            try
            {
                IValidatableOptionView validatable = this.View as IValidatableOptionView;
                if (validatable != null && !validatable.OnValidate())
                {  
                    validatable.OnInvalid();
                    return false;
                }
            }
            catch (Exception ex)
            {
                logger.Info("Option Validate failed", ex);
            }

            this.View.OnOK();
            if (this.UseGlobalPersistor)
            {
                persistenceService.PersistGlobalItems(); 
            }
            else if (this.View is IImplicitSaveOptionView)
            {
                persistenceProfileService.SaveCurrentProfile();
            }
            return true;
        }
          
        public void Dispose()
        {
            IDisposableOptionView optionView = this.View as IDisposableOptionView;
            if (optionView != null)
            {
                optionView.OnLeave();
            }
        }

        public void LocateItem(object resultItem_)
        {
            this.View.Content.UpdateLayout();
            if (this.SubItemAwarePage != null)
            {
                var grid = LogicalTreeHelper.GetParent(this.View.Content) as Grid;
                if (grid != null)
                {
                    grid.UpdateLayout();
                }
                this.SubItemAwarePage.LocateItem(resultItem_);
            }
        }
    }
}