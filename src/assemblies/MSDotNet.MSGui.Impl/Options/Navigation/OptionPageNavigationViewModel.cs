﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input; 
using Microsoft.Practices.Composite.Presentation.Commands;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;

namespace MorganStanley.MSDotNet.MSGui.Impl.Options
{
    public class OptionPageNavigationViewModel : ViewModelBase
    {
        #region Private properties

        private SideNavigationItem sideNavigationSelectedItem;
        private SearchBoxViewModel searchBox;
        private IList<OptionPage> searchResults;
        private readonly IList<OptionPage> searchablePages;
        private ObservableCollection<SideNavigationItem> sideNavigationItems;
        private readonly ICommand collapseAllSideNavigationCommand;
        private readonly ICommand expandAllSideNavigationCommand;
        private string searchText;
        private readonly SideNavigationItem allResultsNode;

        #endregion

        public OptionPageNavigationViewModel(IList<OptionPage> pages_)
        {
            pages_ = pages_.OrderBy(p_ => -ApplicationOptions.GetPriority(p_)).ToList();
            searchablePages = pages_;
            SearchBox = new SearchBoxViewModel(ShowSearchResultCallback) {SearchablePages = pages_, EnableTypeaheadDropDown = OptionsExtensions.EnableTypeaheadDropDownSearch};
            collapseAllSideNavigationCommand = new DelegateCommand<object>(obj_ => ToggleSidenavigationState(false));
            expandAllSideNavigationCommand = new DelegateCommand<object>(obj_ => ToggleSidenavigationState(true));
            allResultsNode = new SideNavigationItem(null, string.Empty, true) {Header = "All Results"};
        }
         
        #region Public Properties

        public string SearchText
        {
            get { return searchText; }
            set
            {
                if (searchText != value)
                {
                    searchText = value;
                    OnPropertyChanged("SearchText");
                }
            }
        }

        public IList<OptionPage> SearchResults
        {
            get { return searchResults; }
            private set
            {
                if (!Equals(searchResults, value))
                {
                    searchResults = value;
                    OnPropertyChanged("SearchResults");
                }
            }
        }

        public ICommand CollapseAllSideNavigationCommand
        {
            get { return collapseAllSideNavigationCommand; }
        }

        public ICommand ExpandAllSideNavigationCommand
        {
            get { return expandAllSideNavigationCommand; }
        }

        public SearchBoxViewModel SearchBox
        {
            get { return searchBox; }
            set
            {
                if (searchBox != value)
                {
                    searchBox = value;
                    OnPropertyChanged("SearchBox");
                }
            }
        }

        public ObservableCollection<SideNavigationItem> SideNavigationItems
        {
            get
            {
                if (sideNavigationItems == null)
                {
                    RecreateSideNavigation();
                }
                return sideNavigationItems;
            }
            private set
            {
                if (sideNavigationItems != value)
                {
                    sideNavigationItems = value;
                    OnPropertyChanged("SideNavigationItems");
                }
            }
        }

        public SideNavigationItem SideNavigationSelectedItem
        {
            get { return sideNavigationSelectedItem; }
            set
            {
                if (sideNavigationSelectedItem != value || (value != null && !value.IsSelected))
                {
                    sideNavigationSelectedItem = value;
                      
                    List<SideNavigationItem> anyOtherSelected =
                        SideNavigationItems.SelectMany(itm_ => itm_.GetSelectedNavigationItem())
                                           .Where(itm_ => itm_ != sideNavigationSelectedItem).ToList();
                    anyOtherSelected.ForEach(itm_ => itm_.IsSelected = false);
                    if (sideNavigationSelectedItem != null)
                    {
                        sideNavigationSelectedItem.IsSelected = true;
                    }
                    OnPropertyChanged("SideNavigationSelectedItem");
                }
            }
        } 

        #endregion

        private void ShowSearchResultCallback(string searchKey_, IList<OptionPage> foundPages_)
        {
            SearchText = searchKey_;
            SearchResults = foundPages_;
            foreach (SideNavigationItem sideNavigationItem in SideNavigationItems)
            {
                foreach (SideNavigationItem navigationItem in sideNavigationItem)
                {
                    navigationItem.SearchText = searchKey_;
                    navigationItem.FoundItemCount = navigationItem.FoundPageCount = 0;
                }
            } 
            if ((SearchResults == null) || SearchResults.Count == 0)
            {
                SideNavigationSelectedItem = null;
                return;
            } 
            foreach (SideNavigationItem sideNavigationItem in SideNavigationItems)
            {
                foreach (SideNavigationItem navigationItem in sideNavigationItem)
                {
                    navigationItem.SearchText = searchKey_;

                    foreach (var optionPage in foundPages_)
                    {
                        if (navigationItem.IsSearchOnlyNode || navigationItem.IsItemPathUnderCurrentNode(optionPage.FullPath))
                        {
                            navigationItem.FoundPageCount += 1;
                            navigationItem.FoundItemCount += optionPage.MatchCount;
                        }
                    } 
                }
            }
            if (SideNavigationSelectedItem == null || !SideNavigationSelectedItem.IsSearchOnlyNode)
            {
                SideNavigationSelectedItem =
                    SideNavigationItems.SelectMany(itm_ => itm_).FirstOrDefault(itm_ => itm_.FoundPageCount > 0 && itm_.Page != null);
            }
        }

        private void RecreateSideNavigation()
        {
            IEnumerable<SideNavigationItem> navItems = SideNavigationItem.CreateSideNavigation(searchablePages);
            SideNavigationItems = new ObservableCollection<SideNavigationItem>
                {
                    allResultsNode
                };
            foreach (var searchablePage in searchablePages)
            {
                allResultsNode.Items.Add(new SideNavigationItem(allResultsNode, searchablePage.FullPath, false) { Page = searchablePage, Header = searchablePage.Name });
            }
            foreach (SideNavigationItem sideNavigationItem in navItems)
            {
                SideNavigationItems.Add(sideNavigationItem);
            }
        }

        private void ToggleSidenavigationState(bool expand_)
        {
            if ((SideNavigationItems != null))
            {
                foreach (SideNavigationItem sideNavigationItem in SideNavigationItems)
                {
                    foreach (SideNavigationItem navigationItem in sideNavigationItem)
                    {
                        navigationItem.IsExpanded = expand_;
                    }
                }
            }
        } 
    }
}
  