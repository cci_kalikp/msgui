﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;

namespace MorganStanley.MSDotNet.MSGui.Impl.Options
{
    public class SideNavigationItem : ViewModelBase, IEnumerable<SideNavigationItem>
    {
        private readonly bool isSearchOnlyNode;
        internal const string PathSeparator = "/";
        private string pathInHierarchy; 
        private string header;
        private IList<SideNavigationItem> items;
        private bool isSelected;
        private bool isExpanded;
        private int foundItemCount;
        private int foundPageCount;
        private string searchText;
        private SideNavigationItem parent;

        public string PathInHierarchy
        {
            get { return pathInHierarchy; }
            set
            {
                if (pathInHierarchy != value)
                {
                    pathInHierarchy = value;
                    OnPropertyChanged("PathInHierarchy");
                }
            }
        }

        public string Header
        {
            get { return header; }
            set
            {
                if (header != value)
                {
                    header = value;
                    OnPropertyChanged("Header"); 
                }
            }
        }
         
        public IList<SideNavigationItem> Items
        {
            get { return items; }
            private set
            {
                items = value;
                OnPropertyChanged("Items");
            }
        }

        public bool IsSelected
        {
            get { return isSelected; }
            set
            {
                if (isSelected != value)
                {
                    isSelected = value;
                    if (isSelected && parent != null && !parent.IsExpanded)
                    {
                        parent.IsExpanded = true;
                    }
                    OnPropertyChanged("IsSelected");
                }
            }
        }

        public bool IsExpanded
        {
            get { return isExpanded; }
            set
            {
                if (isExpanded != value)
                {
                    isExpanded = value;
                    if (isExpanded && parent != null && !parent.IsExpanded)
                    {
                        parent.IsExpanded = true;
                    }
                    OnPropertyChanged("IsExpanded");
                }
            }
        }

        public int FoundPageCount
        {
            get { return foundPageCount; }
            set
            {
                if (foundPageCount != value)
                {
                    foundPageCount = value;
                    OnPropertyChanged("FoundPageCount");
                    OnPropertyChanged("FoundResult"); 
                    OnPropertyChanged("ToolTip");
                    OnPropertyChanged("HasResults");
                    OnPropertyChanged("ShowNode");
                }
            }
        }

        public int FoundItemCount
        {
            get { return foundItemCount; }
            set
            {
                if (foundItemCount != value)
                {
                    foundItemCount = value;
                    OnPropertyChanged("FoundItemCount");
                    OnPropertyChanged("FoundResult"); 
                    OnPropertyChanged("ToolTip");
                    OnPropertyChanged("HasResults");
                    OnPropertyChanged("ShowNode");
                }
            }
        }
        public bool HasResults
        {
            get { return FoundPageCount > 0; }
        }

        public bool ShowNode
        {
            get { return (isSearchOnlyNode  || (parent != null && parent.isSearchOnlyNode))? HasResults : string.IsNullOrWhiteSpace(SearchText) || HasResults; }
        }

        public string ToolTip
        {
            get
            {
                if (!HasResults) return string.Empty;
                if (Page == null || this.Items.Count > 0) // group node
                {
                    if (FoundPageCount > 1)
                    { 
                        return string.Format(
                            "{1} items in {0} pages match search text \"{2}\" under this section.", FoundPageCount,
                            FoundItemCount, SearchText);
                    }
                    else if (FoundPageCount == 1 && FoundItemCount > 1)
                    {
                        return string.Format(
    "{1} items in {0} page match search text \"{2}\" under this section.", FoundPageCount,
    FoundItemCount, SearchText);
                    } 
                    else //one page with one item
                    {
                        return string.Format(
    "{1} item in {0} page matches search text \"{2}\" under this section.", FoundPageCount,
    FoundItemCount, SearchText);
                    }
                }
                else if (FoundItemCount > 1)
                {
                    return string.Format("{0} items in this page match search text \"{1}\".", FoundItemCount,
                                         SearchText); 
                }
                else
                {
                    return string.Format("{0} item in this page matches search text \"{1}\".", FoundItemCount,
                     SearchText); 

                }
                
            }
        }

        public string FoundResult
        {
            get 
            { 
                if (!HasResults) return null;
                if (Page == null || this.Items.Count > 0) return string.Format("[{1} ({0})]", FoundPageCount, FoundItemCount);
                return string.Format("[{0}]", FoundItemCount);
            }
        }

        public string SearchText
        {
            get { return searchText; }
            set
            {
                if (searchText != value)
                {
                    searchText = value;
                    OnPropertyChanged("SearchText");
                    OnPropertyChanged("ToolTip");
                    OnPropertyChanged("ShowNode");
                }
            }
        }

        public bool IsSearchOnlyNode
        {
            get { return isSearchOnlyNode; }
        }

        public OptionPage Page { get; internal set; }


        public SideNavigationItem(SideNavigationItem parent_, string fullPath_, bool isSearchOnlyNode_ = false)
        {
            this.parent = parent_;
            isSearchOnlyNode = isSearchOnlyNode_;
            PathInHierarchy = fullPath_ ?? string.Empty;
            Items = new ObservableCollection<SideNavigationItem>();
            IsExpanded = true;
        } 
        /// <summary>
        /// Checks if specified ItemPath will come under current node
        /// </summary>
        /// <param name="path_"></param>
        /// <returns></returns>
        public bool IsItemPathUnderCurrentNode(string path_)
        {
            if (string.IsNullOrWhiteSpace(PathInHierarchy))
            {
                return false;
            }
            return (path_+PathSeparator).StartsWith(PathInHierarchy + PathSeparator);
        }

        public IEnumerator<SideNavigationItem> GetEnumerator()
        {
            yield return this;
            if (Items != null)
            {
                foreach (SideNavigationItem sideNavigationItem in Items)
                {
                    foreach (SideNavigationItem navigationItem in sideNavigationItem)
                    {
                        yield return navigationItem;
                    }
                }
            }
        }

        public override string ToString()
        {
            return PathInHierarchy;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Ideally this should return only 1 item, however as we are not enforcing single selectivity anywhere,
        ///  we have to be aware of the fact that we may get multiple selections
        /// </summary>
        /// <returns></returns>
        public IEnumerable<SideNavigationItem> GetSelectedNavigationItem()
        {
            return this.Where(sideNavigationItem_ => sideNavigationItem_.IsSelected);
        }

        public override bool Equals(object obj_)
        {
            if ((obj_ == null) || !(obj_ is SideNavigationItem))
            {
                return false;
            }
            var navItem = obj_ as SideNavigationItem;
            return string.IsNullOrWhiteSpace(PathInHierarchy)
                       ? string.IsNullOrWhiteSpace(navItem.PathInHierarchy)
                       : PathInHierarchy.Equals(navItem.PathInHierarchy, StringComparison.InvariantCultureIgnoreCase);
        }

        public override int GetHashCode()
        {
            return string.IsNullOrWhiteSpace(PathInHierarchy) ? 0 : PathInHierarchy.GetHashCode();
        }

        public static bool operator ==(SideNavigationItem itemA_, SideNavigationItem itemB_)
        {
            if (ReferenceEquals(itemA_, itemB_))
            {
                return true;
            }
            if ((object) itemA_ == null)
            {
                return false;
            }
            return itemA_.Equals(itemB_);
        }

        public static bool operator !=(SideNavigationItem itemA_, SideNavigationItem itemB_)
        {
            return !(itemA_ == itemB_);
        }

        /// <summary>
        /// Creates the SideNavigationTree out of the Sequence of SearchablePages 
        /// </summary>
        /// <param name="pages_"></param>
        /// <returns></returns>
        public static IEnumerable<SideNavigationItem> CreateSideNavigation(IList<OptionPage> pages_ )
        {
            var sideNav = new List<SideNavigationItem>();
            if (pages_ != null && pages_.Count >     0)
            {
                
                Dictionary<string, SideNavigationItem> currentLevel = null;
                int curDepth = 0;
                while (pages_.Count > 0)
                {
                    if (currentLevel == null)
                    {
                        currentLevel = new Dictionary<string, SideNavigationItem>();
                        foreach (var page in pages_)
                        {
                            if (currentLevel.ContainsKey(page.Paths[0])) continue;
                            var item = new SideNavigationItem(null, page.Paths[0]);
                            item.Header = item.PathInHierarchy;
                            if (page.Paths.Count == 1)
                            {
                                item.Page = page;
                            }
                            currentLevel.Add(page.Paths[0], item);
                            sideNav.Add(item);
                        } 
                    }
                    else
                    {
                        var nextLevel = new Dictionary<string, SideNavigationItem>();
                        foreach (OptionPage page in pages_)
                        {
                            string path = page.Paths[curDepth];
                            string parentPath = string.Join(PathSeparator, page.Paths.Take(curDepth));
                            var fullPath = string.Join(PathSeparator, parentPath, path);
                            if (!nextLevel.ContainsKey(fullPath))
                            {
                                var item = new SideNavigationItem(currentLevel[parentPath], fullPath) { Header = path };
                                if (curDepth == page.Paths.Count - 1)
                                {
                                    item.Page = page;
                                }
                                currentLevel[parentPath].Items.Add(item);
                                nextLevel.Add(fullPath, item);
                            }
                        }
                        currentLevel = nextLevel;
                    }
                    curDepth++;
                    pages_ = pages_.Where(page_=> page_.Paths.Count > curDepth).ToList();
                }
            }
            return sideNav;
        }
    }
}
