﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Options/ApplicationOptions.cs#41 $
// $Change: 902331 $
// $DateTime: 2014/10/24 05:43:11 $
// $Author: caijin $

using System;
using System.Collections.Generic;
using System.Windows; 
using System.Windows.Media.Imaging;
using System.Xml.Linq;
using System.Linq; 
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.Application;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.ModernTheme;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.Shell;

namespace MorganStanley.MSDotNet.MSGui.Impl.Options
{
    internal class ApplicationOptions : IApplicationOptions, IPersistable
    {
        public const string PersistorPrefix = "ViewModelBasedOptionPagePersistor.";

        private readonly object _isLoadingOrSaving = new object();
        private readonly Window _mainWindow;
        private readonly List<OptionPage> _optionPages = new List<OptionPage>();
        private readonly ShellMode _shellMode;
        private readonly PersistenceProfileService _persistenceProfileService;
        private readonly IPersistenceService _persistenceService;
        private readonly IUnityContainer _container; 
        private Rect _pos = Rect.Empty;
        private readonly List<OptionPage> _pagesUsePropertyGrid = new List<OptionPage>(); 
        private bool _resourceAdded = false;
        private bool _propertyGridResourceAdded = false; 

        public ApplicationOptions(IShell shell,
                                  InitialSettings initialSettings,
                                  PersistenceProfileService persistenceProfileService,
                                  IPersistenceService persistenceService,
                                  IApplication application,
                                  IUnityContainer container)
        {
            _mainWindow = shell.MainWindow;
            _shellMode = initialSettings.ShellMode;
            _persistenceProfileService = persistenceProfileService;
            _persistenceService = persistenceService;
            var app = (Application.Application)application;
            app.ThemeChanged += (sender_, args_) =>
                {
                    _resourceAdded = false;
                };
            _container = container;
            priorities["MSDesktop"] = int.MaxValue;
        }

       

        internal static readonly Dictionary<string, int> priorities = new Dictionary<string, int>(); 
        public static void SetPriority(string path, string title, int priority)
        {
            string fullPath = title;
            if (!string.IsNullOrEmpty(path))
            {
                fullPath = path.TrimEnd(SideNavigationItem.PathSeparator.ToArray()) + SideNavigationItem.PathSeparator +
                            fullPath;
            }
            priorities[fullPath] = priority;
        }

        
        internal static int GetPriority(OptionPage page_)
        {
            if (priorities.Count == 0) return 0;
            int priority;
            if (priorities.TryGetValue(page_.FullPath, out priority))
            {
                return priority;
            }
            if (page_.Paths.Count > 1)
            {
                var paths = page_.Paths.ToArray();
                for (int i = paths.Length - 1; i > 0; i--)
                {
                    string path = String.Join(SideNavigationItem.PathSeparator, paths, 0, i);
                    if (priorities.TryGetValue(path, out priority))
                    {
                        return priority;
                    }
                }
            }
            return 0;

        }
        #region IApplicationOptions Members

        public void AddOptionPage(string path, string title, IOptionView view)
        {
            AddOptionPageCore(path, title, view);
        }


        public void AddViewModelBasedOptionPage<TViewModel>(string path, string title)
            where TViewModel : IOptionViewViewModel
        {
            AddViewModelBasedOptionPage<TViewModel>(path, title, false);
        }

        public void AddViewModelBasedOptionPage<TViewModel>(string path, string title, bool global)
            where TViewModel : IOptionViewViewModel
        {
            if (!_container.IsRegistered<TViewModel>())
            {
                _container.RegisterType<TViewModel>(new ContainerControlledLifetimeManager());
            }
            var viewModel = _container.Resolve<TViewModel>();

            var persistorID = PersistorPrefix + typeof (TViewModel);
            if (global)
            {
                _persistenceService.AddGlobalPersistor(persistorID, viewModel.LoadState, viewModel.Persist);
            }
            else
            {
                _persistenceService.AddPersistor(persistorID, viewModel.LoadState, viewModel.Persist);
            }

            OptionPage page = AddOptionPageCore(path, title, new DefaultOptionView<TViewModel>(_container,  System.Windows.Application.Current.Resources));
            if (!viewModel.HasCustomTemplate)
            {
                _pagesUsePropertyGrid.Add(page);
            }
            if (global)
            {
                page.UseGlobalPersistor = true;
            }
        }

        private OptionPage AddOptionPageCore(string path, string title, IOptionView view)
        {
            if (string.IsNullOrEmpty(title))
                throw new ArgumentNullException("title", "The title name of option cannot be null.");
            if (view == null) throw new ArgumentNullException("view", "The view of option cannot be null.");

            OptionPage page = new OptionPage(_persistenceService, _persistenceProfileService, view, path, title);
            _optionPages.Add(page);
            return page;
        }

        #endregion

        #region IPersistable Members

        public void LoadState(XDocument state)
        {
            try
            {
                lock (_isLoadingOrSaving)
                {
                    var mainElem = state.Root;

                    if (mainElem != null && mainElem.Name.LocalName == "Dialog")
                    {
                        var valid = true;

                        double left = -1;
                        XAttribute attr = mainElem.Attribute("Left");
                        if (attr != null)
                            valid = valid && double.TryParse(attr.Value, out left);

                        double top = -1;
                        attr = mainElem.Attribute("Top");
                        if (attr != null)
                            valid = valid && double.TryParse(attr.Value, out top);

                        double width = -1;
                        attr = mainElem.Attribute("Width");
                        if (attr != null)
                            valid = valid && double.TryParse(attr.Value, out width);

                        double height = -1;
                        attr = mainElem.Attribute("Height");
                        if (attr != null)
                            valid = valid && double.TryParse(attr.Value, out height);

                        if (valid)
                            _pos = new Rect(left, top, width, height);
                    }
                }
            }
            catch
            {
                return;
            }
        }

        public XDocument SaveState()
        {
            try
            {
                lock (_isLoadingOrSaving)
                {
                    if (_pos != Rect.Empty)
                    {
                        var state = new XDocument(
                            new XElement("Dialog",
                                         new XAttribute("Left", _pos.Left),
                                         new XAttribute("Top", _pos.Top),
                                         new XAttribute("Width", _pos.Width),
                                         new XAttribute("Height", _pos.Height)
                                )
                            );

                        return state;
                    }
                    return null;
                }
            }
            catch
            {
                return null;
            }
        }

        public string PersistorId
        {
            get { return "MSGui.Internal.ApplicationOptionsDialog"; }
        }

        #endregion

        public IList<OptionPage> Pages
        {
            get { return _optionPages; }
        }
         

        internal void EnsureStyle()
        {
            if (!_resourceAdded)
            {
                if (ModernThemeHelper.IsModernThemeEnabled)
                {
                    System.Windows.Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary()
                    {
                        Source = new Uri("pack://application:,,,/MSDotNet.MSGui.Impl;component/Options/Styles/OptionsSharedStyle.xaml")
                    }); 
                }
                //todo: remove the shell mode test once the options menu in the launcherbar modes is no longer accessible
                if (_shellMode == ShellMode.LauncherBarAndFloatingWindows ||
                    _shellMode == ShellMode.LauncherBarAndWindow ||
                    !ModernThemeHelper.IsModernThemeEnabled)
                {
                    if (OptionsExtensions.EnableTheme)
                    {
                        System.Windows.Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary()
                        {
                            Source = new Uri("pack://application:,,,/MSDotNet.MSGui.Impl;component/Options/Styles/Themed/OptionsWindowThemedStyle.xaml")
                        });
                    }
                    else
                    {
                        System.Windows.Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary()
                        {
                            Source = new Uri("pack://application:,,,/MSDotNet.MSGui.Impl;component/Options/Styles/Simple/OptionsWindowSimpleStyle.xaml")
                        });
                    }
                }

                _resourceAdded = true;
            }

            if (!_propertyGridResourceAdded && _pagesUsePropertyGrid.Count > 0)
            {
                if (OptionsExtensions.EnableTheme && !ModernThemeHelper.IsModernThemeEnabled)
                {
                    System.Windows.Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary()
                    {
                        Source = new Uri("pack://application:,,,/MSDotNet.MSGui.Impl;component/Options/Styles/Themed/PropertyGridThemedStyle.xaml")
                    });
                }
                else
                {
                    System.Windows.Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary()
                    {
                        Source = new Uri("pack://application:,,,/MSDotNet.MSGui.Impl;component/Options/Styles/Simple/PropertyGridSimpleStyle.xaml")
                    });
                }
                _propertyGridResourceAdded = true;
            } 
        }
        internal void OnShowOptions(object sender_, EventArgs e_)
        {
            EnsureStyle();
            var optWindow = new OptionsWindow(_persistenceService, _persistenceProfileService, _optionPages) { Owner = _mainWindow };

            if (_pos != Rect.Empty)
            {
                optWindow.Window.Left = _pos.Left;
                optWindow.Window.Top = _pos.Top;
                optWindow.Window.Width = _pos.Width;
                optWindow.Window.Height = _pos.Height;
                optWindow.WindowStartupLocation = WindowStartupLocation.Manual;
            }
            else
            {
                optWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            }

            optWindow.ShowThisWindow();
            _pos = new Rect(optWindow.Window.Left, optWindow.Window.Top, optWindow.Window.Width, optWindow.Window.Height);
        }

        public InitialButtonParameters GetOptionButtonParameter()
        {
            return new InitialButtonParameters()
                {
                    Image = new BitmapImage(new Uri(@"/MSDotNet.MSGui.Impl;component/Images/icons/Cog.png",
                                                    UriKind.RelativeOrAbsolute)),
                    Text = "Options",
                    Click = OnShowOptions

                };
            
        }


        public void RemoveOptionPage(string path, string title)
        {
            var pageToBeRemoved = _optionPages.FirstOrDefault(p => (p.Path == path) && (p.Name == title));
            if (pageToBeRemoved != null)
            {
                _optionPages.Remove(pageToBeRemoved);
                _pagesUsePropertyGrid.Remove(pageToBeRemoved); 
            }
        }
    }
}
