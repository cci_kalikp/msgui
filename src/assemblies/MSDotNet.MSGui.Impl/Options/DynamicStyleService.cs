﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using MorganStanley.MSDotNet.MSGui.Controls.Extensions;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.ModernTheme;
using MorganStanley.MSDotNet.MSGui.Impl.Shell;

namespace MorganStanley.MSDotNet.MSGui.Impl.Options
{
    /// <summary>
    /// This service is used for Options Pages which want to have the appropriate style either under the themed option window or under the classic option wind
    /// If the option page is decided at design time that it would only be used in themed option window, this service is not needed
    /// If the option page is decided at design time that it would only be used in classic theme, this service is not needed as well, 
    /// but style/properties of the control should be adjusted to make it not affected by the current theming
    /// If the option page is used in both case, this service can be used, but remember to not use style to customize the control, 
    /// since utilizing of this service would replace the style specified if used in classic mode. 
    /// </summary>
    public static class DynamicStyleService
    {
        private readonly static Dictionary<Type, Style> ClassicStyles = new Dictionary<Type, Style>(); 
        static DynamicStyleService()
        {
            ClassicStyles.Add(typeof (CheckBox), new Style(typeof (CheckBox)));
            ClassicStyles.Add(typeof(GroupBox), new Style(typeof(GroupBox)));
            ClassicStyles.Add(typeof(Expander), new Style(typeof(Expander)));
            ClassicStyles.Add(typeof(Label), new Style(typeof(Label)));
            ClassicStyles.Add(typeof(TextBox), new Style(typeof(TextBox)));
            ClassicStyles.Add(typeof(ComboBox), new Style(typeof(ComboBox)));
        }
        public static string GetAllowClassicStyle(DependencyObject obj_)
        {
            return (string)obj_.GetValue(AllowClassicStyleProperty);
        }

        public static void SetAllowClassicStyle(DependencyObject obj_, bool value_)
        {
            obj_.SetValue(AllowClassicStyleProperty, value_);
        }

        /// <summary>
        /// This property is used for Options Pages which want to have the appropriate style either under the themed option window or under the classic option wind
        /// If the option page is decided at design time that it would only be used in themed option window, this property is not needed
        /// If the option page is decided at design time that it would only be used in classic theme, this property is not needed as well, 
        /// but style/properties of the control should be adjusted to make it not affected by the current theming
        /// If the option page is used in both case, this property can be used, but remember to not use style to customize the control, 
        /// since utilizing of this property would replace the style specified if used in classic mode. 
        /// </summary>
        public static readonly DependencyProperty AllowClassicStyleProperty =
            DependencyProperty.RegisterAttached("AllowClassicStyle", typeof(bool), typeof(DynamicStyleService), new UIPropertyMetadata(false, AllowClassicStyleChanged));

        private static void AllowClassicStyleChanged(DependencyObject d_, DependencyPropertyChangedEventArgs e_)
        {
            if (Convert.ToBoolean(e_.NewValue) && !OptionsExtensions.EnableTheme && !ModernThemeHelper.IsModernThemeEnabled)
            {
                Style classicStyle;
                if (ClassicStyles.TryGetValue(d_.GetType(), out classicStyle))
                {
                    ((FrameworkElement) d_).Style = classicStyle;
                }
                else
                { 
                    foreach (FrameworkElement element in d_.FindLogicalChildren<FrameworkElement>())
                    {
                        if (ClassicStyles.TryGetValue(element.GetType(), out classicStyle))
                        {
                            element.Style = classicStyle;
                        }
                    }
                }
            }
        }
    }
}
