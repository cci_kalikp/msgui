﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Options/OptionsWindow.xaml.cs#33 $
// $Change: 898640 $
// $DateTime: 2014/09/29 02:59:06 $
// $Author: caijin $

using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input; 
using MorganStanley.MSDotNet.MSGui.Core; 
using MorganStanley.MSDotNet.MSGui.Impl.Persistence;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.MicrosoftShell;
using MorganStanley.MSDotNet.My;

namespace MorganStanley.MSDotNet.MSGui.Impl.Options
{
    /// <summary>
    /// Interaction logic for OptionsWindow.xaml
    /// </summary>
    internal partial class OptionsWindow : Window
    {
        private readonly List<OptionPage> touchedOptionPages = new List<OptionPage>();
        private static readonly IMSLogger logger = MSLoggerFactory.CreateLogger<OptionsWindow>();
        private readonly IPersistenceService persistenceService;
        private readonly PersistenceProfileService persistenceProfileService;
        private readonly OptionPageNavigationViewModel navigation;
        public OptionsWindow(IPersistenceService persistenceService_, PersistenceProfileService persistenceProfileService_, IList<OptionPage> pages_)
        {
            this.InitializeComponent();
            persistenceService = persistenceService_;
            this.persistenceProfileService = persistenceProfileService_;
            this.DataContext = navigation = new OptionPageNavigationViewModel(pages_);
            if (navigation.SearchBox.EnableTypeaheadDropDown)
            {
                navigation.SearchBox.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(SearchBox_PropertyChanged);         
            }
            CommandBindings.Add(new CommandBinding(SystemCommands.CloseWindowCommand, OnCloseCommand));
            
        }

        void SearchBox_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "SelectedItem" && navigation.SearchBox.SelectedItem != null) 
            {
                NavigateTo(navigation.SearchBox.SelectedItem as SearchResultItem);
            }
        }
   
        private void OnCloseCommand(object sender_, ExecutedRoutedEventArgs e_)
        {
            SystemCommands.CloseWindow(this);
        }
        private void buttonHelp_Click(object sender, RoutedEventArgs e)
        {
            OptionPage control = GetCurrentOptionPage();
            if (control == null) return;
            control.Help();
        }

        private void buttonCancel_Click(object sender, RoutedEventArgs e)
        {
            foreach (OptionPage op in touchedOptionPages)
            {
                 op.Cancel(); 
            }
            Close();
        }

        protected override void OnClosed(EventArgs e)
        {
            foreach (var touchedOptionPage in touchedOptionPages)
            {
                touchedOptionPage.Dispose();
            }
            touchedOptionPages.Clear();
            base.OnClosed(e);
        }

        private void buttonOk_Click(object sender, RoutedEventArgs e)
        {
            foreach (OptionPage op in touchedOptionPages)
            {
                try
                {
                    IValidatableOptionView validatable = op.View as IValidatableOptionView;
                    if (validatable != null && !validatable.OnValidate())
                    {
                        NavigateTo(op);
                        validatable.OnInvalid();
                        return;
                    }
                }
                catch (Exception exc)
                {
                    logger.Error("Option Validate failed", exc); 
                }
            }

            bool saveNeeded = false;
            bool saveGlobalNeeded = false;
            foreach (OptionPage op in touchedOptionPages)
            {
                try
                {
                    if (op.View is IImplicitSaveOptionView)
                        saveNeeded = true;
                    if (op.UseGlobalPersistor)
                        saveGlobalNeeded = true;
                    op.View.OnOK();
                }
                catch (Exception exc)
                {
                    logger.Error("Option OK failed", exc);

                }
            } 
            if (saveNeeded)
            {
                persistenceProfileService.SaveCurrentProfile();
            }
            if (saveGlobalNeeded)
            {
                persistenceService.PersistGlobalItems();
            }
            Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            HideThisWindow();
        }

        private void OptionTreeViewSelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            OptionPage currentPage = GetCurrentOptionPage();
            if (currentPage == null)
            {
                panelControl.Children.Clear(); 
                return;
            }
            DisplayPage(currentPage);
        }

        private void DisplayPage(OptionPage page_)
        {
            if (touchedOptionPages.Count > 0)
            {
                OptionPage oldPage =
                    touchedOptionPages[touchedOptionPages.Count - 1];
                oldPage.Dispose();
            }
            else if (touchedOptionPages.Contains(page_))
            {
                touchedOptionPages.Remove(page_);
            }
            touchedOptionPages.Add(page_); 
            panelControl.Children.Clear(); 
            var currentContent = page_.RenderPage();
            FrameworkElement currentElement = currentContent as FrameworkElement;
            if (currentElement != null)
            {
                Panel p = currentElement.Parent as Panel;
                if (p != null && p != panelControl)
                {
                    p.Children.Remove(currentElement);
                }
            }
            if (currentContent != null && !panelControl.Children.Contains(currentContent))
            {
                panelControl.Children.Add(currentContent);
            } 
        }

        public void NavigateTo(OptionPage page_)
        {
            foreach (var sideNavigationItem in navigation.SideNavigationItems)
            {
                foreach (var navigationItem in sideNavigationItem)
                {
                    if (navigationItem.PathInHierarchy == page_.FullPath &&  navigationItem.ShowNode)
                    {
                        navigation.SideNavigationSelectedItem = navigationItem;
                        return; 
                    }
                } 
            }
            DisplayPage(page_);
        }

        internal void NavigateTo(SearchResultItem resultItem_)
        {
            NavigateTo(resultItem_.Page);
            if (resultItem_.Item != null)
            {
                resultItem_.Page.LocateItem(resultItem_.Item);
               
            }
        }
         
        private void HideThisWindow()
        {
            panelControl.Children.Clear();
        }

        public void ShowThisWindow()
        {
            OptionPage currenPage = GetCurrentOptionPage();
            if (currenPage != null)
            {
                touchedOptionPages.Add(currenPage);
                currenPage.View.OnDisplay();
            }
            Visibility = Visibility.Visible;
            ShowDialog();
        }

        private OptionPage GetCurrentOptionPage()
        {
            var selectedItem = sideNavigationTree.SelectedItem as SideNavigationItem;
            if (selectedItem == null) return null;
            return selectedItem.Page;
        }
    }
}