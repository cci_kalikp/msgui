﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xceed.Wpf.Toolkit.PropertyGrid.Attributes; 

namespace MorganStanley.MSDotNet.MSGui.Impl.Options
{
    internal static class ItemSourceAttributeHelper
    {
        internal static void SetItemGenerator(Attribute attribute, CachedPropertyItem cachedProperty)
        { 
            Type itemSourceType = (attribute as ItemsSourceAttribute).Type;
            var defaultConstructor = itemSourceType.GetConstructor(new Type[] { });
            if (defaultConstructor == null)
            {
                return;
            }
            IItemsSource itemSource = defaultConstructor.Invoke(new object[] { }) as IItemsSource;
            if (itemSource != null)
            {
                cachedProperty.ItemValuesGenerator = (() =>
                    {
                        ItemCollection source = itemSource.GetValues();
                        if (source != null && source.Any())
                        {
                            return source.Select(itm_ => itm_.DisplayName).ToList();
                        }
                        return Enumerable.Empty<string>();
                    });
            }
        }
    }
}
