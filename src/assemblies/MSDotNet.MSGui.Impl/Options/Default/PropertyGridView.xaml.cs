﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using Microsoft.Practices.Unity; 
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Controls.Converters;
using Xceed.Wpf.Toolkit.PropertyGrid;
using System.Collections;

namespace MorganStanley.MSDotNet.MSGui.Impl.Options
{
    /// <summary>
    /// Interaction logic for PropertyGridView.xaml
    /// </summary>
    public partial class PropertyGridView : UserControl
    {
        private readonly IUnityContainer container;
        private readonly IChromeManager chromeManager;
        private readonly IChromeRegistry chromeRegistry;
        private static bool factoryRegistered;
        private const string PopupViewId = "Options.PropertyPopup"; 
        private PropertyItem currentPropertyItem;
        private Point popupLocation;

        public PropertyGridView(CustomTypeDescriptor typeDescriptor_, IUnityContainer container_)
        {
            InitializeComponent();
            container = container_;
            chromeManager = container.Resolve<IChromeManager>();
            chromeRegistry = container.Resolve<IChromeRegistry>();
            propGrid.DataContext = propGrid.SelectedObject = typeDescriptor_;
            if (propGrid.EditorDefinitions.Count > 0)
            {
                propGrid.EditorDefinitions.ToList()
                         .ForEach(editorDefinition_ => typeDescriptor_.EditorDefinitions.Definitions.Add(editorDefinition_));
            }
            propGrid.EditorDefinitions = typeDescriptor_.EditorDefinitions.Definitions; 
            propGrid.PreparePropertyItem += propGrid_PreparePropertyItem;


        }
 
        public void HighlightProperty(string propertyName_)
        {
            string[] subProperties = propertyName_.Split('.');

            int level = 0;
            HighlightProperty(propGrid.Properties, subProperties, ref level); 
        }

        private void HighlightProperty(IList properties_, string[] propertyNames_, ref int level_)
        {
            foreach (PropertyItem propertyItem in properties_)
            {
                if (propertyItem != null && propertyItem.PropertyDescriptor.Name == propertyNames_[level_])
                {
                    propertyItem.ApplyTemplate();
                    if (level_ == propertyNames_.Length - 1)
                    {
                        HighlightPropertyItem(propertyItem);
                        return;
                    }
                    else
                    {
                        if (propertyItem.IsExpandable)
                        {
                            propertyItem.IsExpanded = true;
                        }
                        level_++;

                        SettingItemDescriptor descriptor = propertyItem.PropertyDescriptor as SettingItemDescriptor;
                        if (descriptor != null && descriptor.CustomTemplateKey != null)
                        {
                            string currentPropertyName = propertyNames_[level_];
                            if (!descriptor.CustomTemplateKey.ShowInPopup)
                            {
                                foreach (FrameworkElement element in propertyItem.Editor.GetBindingSources(currentPropertyName))
                                {
                                    if (element != null)
                                    {
                                        element.Highlight(); 
                                        return; 
                                    }
                                }
                                HighlightPropertyItem(propertyItem);
                                return;
                            }
                            else
                            {
                                
                                currentPropertyItem = propertyItem;
                                ShowPopupEditor(new Action<Window>(
                                    w_ =>
                                        {
                                            foreach (FrameworkElement element in w_.GetBindingSources(currentPropertyName))
                                            {
                                                if (element != null)
                                                {
                                                    element.Highlight();
                                                    return;
                                                }
                                            }
                                            w_.Highlight();
                                        }));

                                return; 
                            }
                        }
                        else
                        {
                            HighlightProperty(propertyItem.Properties, propertyNames_, ref level_); 
                        }
                    }

                }
            } 
        } 
         
        private static void HighlightPropertyItem(PropertyItem propertyItem_)
        {
            UIElement element = propertyItem_.Template == null ?
     propertyItem_.Editor as UIElement :
     propertyItem_.Template.FindName("FocusBorder", propertyItem_) as UIElement;
            if (element != null)
            {
                OptionsExtensions.RaiseNeedsFocusEvent(element);
            }
            propertyItem_.Focus();
        }
        void propGrid_PreparePropertyItem(object sender_, PropertyItemEventArgs e_)
        {
            var propertyItem = e_.PropertyItem as PropertyItem;
            if (propertyItem == null) return; 
            SettingItemDescriptor customProperty =
                         propertyItem.PropertyDescriptor as SettingItemDescriptor;
            if (customProperty == null) return;
            
            if (customProperty.EditableSource != null &&
                !String.IsNullOrWhiteSpace(customProperty.EditableSource.DependsOn))
            {
                var binding = new Binding(customProperty.EditableSource.DependsOn); 
                propertyItem.DataContext = customProperty.Owner;
                binding.Mode = BindingMode.OneWay;
                if (customProperty.EditableSource.Reverse)
                {
                    binding.Converter = new OppositeBoolConverter();
                }
                propertyItem.SetBinding(IsEnabledProperty, binding);
            }
            if (customProperty.VisibilitySource != null &&
                !String.IsNullOrWhiteSpace(customProperty.VisibilitySource.DependsOn))
            {
                var binding = new Binding(customProperty.VisibilitySource.DependsOn); 
                propertyItem.DataContext = customProperty.Owner;
                binding.Mode = BindingMode.OneWay;
                var converter = new ConfigurableBooleanToVisibilityConverter();
                if (customProperty.VisibilitySource.Reverse)
                {
                    converter.VisibilityWhenFalse =
                    converter.VisibilityWhenNull = Visibility.Visible;
                    converter.VisibilityWhenTrue = Visibility.Collapsed;
                }
                binding.Converter = converter;

                propertyItem.SetBinding(VisibilityProperty, binding);
            } 
            if (propertyItem.Editor != null)
            {
                FixEditorStyle(propertyItem, propertyItem.Editor);
            }
            else
            {
                propertyItem.PropertyChanged += propertyItem_PropertyChanged; 
            }
        }

        void propertyItem_PropertyChanged(object sender_, PropertyChangedEventArgs e_)
        {
            if (e_.PropertyName == "Editor")
            {
                PropertyItem item = sender_ as PropertyItem;
                if (item != null && item.Editor != null)
                {
                    FixEditorStyle(item, item.Editor);
                    item.PropertyChanged -= propertyItem_PropertyChanged;
                }
            }
        }

        private void FixEditorStyle(PropertyItem item_, FrameworkElement editor_)
        {
            if (OptionsExtensions.EnableTheme)
            {
                 if (editor_ is ComboBox)
                 {
                     ComboBox combo = editor_ as ComboBox;
                     var comboBoxStyle = System.Windows.Application.Current.FindResource(typeof(ComboBox)) as Style;
                     if (!String.IsNullOrEmpty(combo.DisplayMemberPath))
                     {
                         FixEmptyItem(item_, combo);
                         comboBoxStyle = GetComboBoxStyle(combo.DisplayMemberPath, comboBoxStyle);
                     }
                     combo.Style = comboBoxStyle; 
                  }
                 else if (editor_ is TextBox)
                 {
                     editor_.Style = System.Windows.Application.Current.FindResource(typeof(TextBox)) as Style;
                 }
                  
            }
            else
            {
                if (editor_ is ComboBox)
                {
                    ComboBox combo = editor_ as ComboBox;
                    if (!String.IsNullOrEmpty(combo.DisplayMemberPath))
                    {
                        FixEmptyItem(item_, combo);
                        combo.Style = GetComboBoxStyle(combo.DisplayMemberPath, null);
                    } 
                }
                else if (editor_ is CheckBox)
                {
                    editor_.Style = new Style(typeof(CheckBox));
                } 
            }
        }

        
        private static void FixEmptyItem(PropertyItem propertyItem_, ComboBox comboBox_)
        {
            string value = (propertyItem_.Value ?? string.Empty).ToString();
            if (string.IsNullOrEmpty(value))
            {
                comboBox_.SelectedIndex = -1;
            } 
        } 

        private static Style GetComboBoxStyle(string displayName_, Style baseStyle_)
        {
            var comboBoxStyle = new Style(typeof(ComboBox), baseStyle_);
            var textFactory = new FrameworkElementFactory(typeof(TextBlock));
            textFactory.SetBinding(TextBlock.TextProperty, new Binding(displayName_));

            var template = new DataTemplate() { VisualTree = textFactory };
            template.Seal();

            comboBoxStyle.Setters.Add(new Setter(ItemsControl.ItemTemplateProperty, template));

            comboBoxStyle.Seal();
            return comboBoxStyle;
        }

       
        private void ShowSettingsPopup(object sender_, ExecutedRoutedEventArgs e_)
        {
            if (!(e_.OriginalSource is Button))
            {
                return;
            }

            var button = e_.OriginalSource as Button;
            currentPropertyItem = button.DataContext as PropertyItem;
            if (currentPropertyItem == null) return;
            ShowPopupEditor(null);
        }

        private void ShowPopupEditor(Action<Window> dialogActivateCallback_)
        {
            popupLocation =
     currentPropertyItem.Editor.PointToScreen(new Point(0, currentPropertyItem.Editor.ActualHeight + 2));

            if (!factoryRegistered)
            {
#pragma warning disable 612,618
                chromeRegistry.RegisterDialogFactory(PopupViewId, new InitializeDialogHandler(CreateDialog));
#pragma warning restore 612,618
                factoryRegistered = true;
            }
#pragma warning disable 612,618
            IDialogWindow dialog = chromeManager.CreateDialog(PopupViewId);
            if (dialog != null)
            {
                if (dialogActivateCallback_ != null)
                {
                    Window dialogWindow = dialog as Window;
                    if (dialogWindow != null)
                    {
                        dialogWindow.Tag = dialogActivateCallback_;
                        dialogWindow.Loaded += new RoutedEventHandler(dialogWindow_Loaded);
                    }
                }

                dialog.ShowDialog();
            } 
#pragma warning restore 612,618
        }

        void dialogWindow_Loaded(object sender, RoutedEventArgs e)
        {
            Window dialogWindow = sender as Window;
            Action<Window> callback = dialogWindow.Tag as Action<Window>;
            if (callback != null) callback(dialogWindow);
            dialogWindow.Loaded -= new RoutedEventHandler(dialogWindow_Loaded);

        }
#pragma warning disable 0618
        private bool CreateDialog(IDialogWindow dialog_)
#pragma warning restore 0618
        {
            var descriptor = currentPropertyItem.PropertyDescriptor as SettingItemDescriptor; 
            var contentControl = new ContentControl();
            contentControl.ContentTemplate = descriptor.GetPopupTemplate();
            contentControl.SetBinding(ContentProperty, new Binding("Value"));
            contentControl.DataContext = currentPropertyItem;  
            dialog_.Title = currentPropertyItem.DisplayName;
            dialog_.Content = contentControl;
            dialog_.Topmost = true;

            dialog_.Left = Convert.ToDouble(popupLocation.X);
            dialog_.Top = Convert.ToDouble(popupLocation.Y);
            dialog_.WindowStartupLocation = WindowStartupLocation.Manual; 
            dialog_.SizeToContent = SizeToContent.WidthAndHeight; 
            dialog_.ResizeMode = ResizeMode.NoResize;
            dialog_.Owner = Window.GetWindow(this);
            return true;
        }
    }
}
