﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Practices.Unity;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.SmartApi;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;

namespace MorganStanley.MSDotNet.MSGui.Impl.Options
{
    class DefaultOptionView<TViewModel> : IValidatableOptionView, ISubItemAwareSeachablePage
        where TViewModel : IOptionViewViewModel
    {
        private readonly IUnityContainer container;
        private TViewModel viewModel;
        private CustomTypeDescriptor typeDescriptor;
        private bool isDirty = false;
        private readonly ResourceDictionary resource;
        string invalidControlName;
        string invalidPropertyName;

        public DefaultOptionView(IUnityContainer container_, ResourceDictionary resource_)
        {
            container = container_;
            resource = resource_;
        }

        public void OnOK()
        {
            container.Resolve<TViewModel>().AcceptChanges(viewModel);
            isDirty = false;
        } 
        public void OnCancel()
        {
            isDirty = false;
        }
         
        public void OnDisplay()
        {
            if (!isDirty)
            {
                viewModel = (TViewModel) container.Resolve<TViewModel>().Clone();
                typeDescriptor = null;
                content = null;
                isDirty = true;
            }
        }

        public void OnHelp()
        { }

        private static bool xceedAssemblyLoaded = false;
        private static readonly object SyncRoot = new object();
        private UIElement content;
        public UIElement Content
        {
            get
            {
                if (content == null)
                {
                    if (viewModel.HasCustomTemplate)
                    {
                        content = new ContentControl() {Content = viewModel, DataContext = viewModel};
                    }
                    else
                    {
                        lock (SyncRoot)
                        { 
                            if (!xceedAssemblyLoaded)
                            {
                                ExtensionsUtils.RequireAssembly(ExternalAssembly.XceedWpfToolkit);
                                xceedAssemblyLoaded = true;
                            }
                        }
                        content = new PropertyGridView(TypeDescriptor, container);
                    }
                }
                return content;
            }
        }

        public CustomTypeDescriptor TypeDescriptor
        {
            get
            {
                if (typeDescriptor == null || !isDirty)
                {
                    if (!isDirty || viewModel == null)
                    {
                        OnDisplay();
                    }
                    typeDescriptor = new CustomTypeDescriptor(viewModel, resource);
                }
                return typeDescriptor;
            }
        } 

        public bool OnValidate()
        {
            string errorString;
            if (!viewModel.Validate(out errorString, out invalidControlName, out invalidPropertyName))
            {
                if (errorString == null)
                {
                    errorString = "Some settings are not valid";
                } 
                TaskDialog.ShowMessage(errorString,
                                       "Invalid Settings", TaskDialogCommonButtons.Close, VistaTaskDialogIcon.Error);
                return false;
            }
            return true;
        }

        
        public void OnInvalid()
        {
            if (invalidControlName != null)
            {
                FrameworkElement element = this.Content.FindVisualChildByName<FrameworkElement>(invalidControlName);
                if (element != null)
                {
                    element.Highlight();
                }
                return;
            }
            if (invalidPropertyName != null)
            {
                if (viewModel.HasCustomTemplate)
                {
                    foreach (FrameworkElement element in this.Content.GetBindingSources(invalidPropertyName))
                    {
                        if (element != null)
                        {
                            element.Highlight();
                            return;
                        }
                    }
                }
                else
                {
                    PropertyGridView gridView = Content as PropertyGridView;
                    if (gridView != null) gridView.HighlightProperty(invalidPropertyName);
                }
            }
        }

        public IList<KeyValuePair<string, object>> GetMatchedItems(string textToSearch_)
        {
            var matchedItems = new List<KeyValuePair<string, object>>();
            foreach (var searchableItem in TypeDescriptor.SearchableItems)
            {
                if (searchableItem.MatchesSearchInput(textToSearch_))
                {
                    CachedPropertyItem cachedPropertyItem = searchableItem as CachedPropertyItem;
                    if (cachedPropertyItem != null)
                    {
                        string description = cachedPropertyItem.Display;
                        if (string.IsNullOrEmpty(description)) description = cachedPropertyItem.Name;
                        matchedItems.Add(new KeyValuePair<string, object>(description,cachedPropertyItem.Name));
                    }
                }
            }
            return matchedItems;
        }

        public void LocateItem(object item_)
        {
            string propertyName = item_ as string;
            if (propertyName == null)
            {
                return; 
            }
            if (viewModel.HasCustomTemplate)
            {
                foreach (FrameworkElement element in this.Content.GetBindingSources(propertyName))
                {
                    if (element != null)
                    {
                        element.Highlight();
                        return;
                    }
                }
            }
            else
            {
                PropertyGridView gridView = Content as PropertyGridView;
                if (gridView != null) gridView.HighlightProperty(propertyName);
            }
        }
    }
}
