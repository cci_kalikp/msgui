﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Xceed.Wpf.Toolkit.PropertyGrid;

namespace MorganStanley.MSDotNet.MSGui.Impl.Options
{
    internal class EditorDefinitionsHolder
    {
        private CustomTypeDescriptor descriptor;
        private EditorDefinitionCollection editorDefinitions;

        public EditorDefinitionsHolder(CustomTypeDescriptor descriptor_)
        {
            this.descriptor = descriptor_;
        }

        public EditorDefinitionCollection Definitions
        {
            get
            {
                if (editorDefinitions == null)
                {
                    editorDefinitions = new EditorDefinitionCollection();
                    foreach (SettingItemDescriptor property in descriptor.Properties)
                    {
                        GenerateEditorDefinitions(property);
                    }
                }
                return editorDefinitions;
            }
        }

        private void GenerateEditorDefinitions(SettingItemDescriptor property_)
        {
            DataTemplate dataTemplate = property_.GetTemplate();
            if (dataTemplate != null)
            {
                bool isPresent = false;
                foreach (EditorTemplateDefinition definition in editorDefinitions)
                {
                    if (definition.EditingTemplate == dataTemplate)
                    {
                        definition.TargetProperties.Add(property_.Name);
                        isPresent = true;
                        break;
                    }
                }
                if (!isPresent)
                {
                    var editorDefinition = new EditorTemplateDefinition { EditingTemplate = dataTemplate };
                    editorDefinition.TargetProperties.Add(property_.Name);
                    editorDefinitions.Add(editorDefinition);
                }
            }
        }
    }
}
