﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Xceed.Wpf.Toolkit.PropertyGrid.Attributes;

namespace MorganStanley.MSDotNet.MSGui.Impl.Options
{
    public class SettingItemDescriptor : PropertyDescriptor, IEquatable<SettingItemDescriptor>, INotifyPropertyChanged
    { 
        private readonly PropertyDescriptor propertyDescriptor; 
        public Func<string, bool> MatchesSearch { get; private set; }
        private readonly ResourceDictionary resource;
        private static readonly Dictionary<DataTemplate, DataTemplate> templates = new Dictionary<DataTemplate, DataTemplate>();

        public SettingItemDescriptor(object owner_, PropertyDescriptor propertyDescriptor_, ResourceDictionary resource_)
            : base(propertyDescriptor_.Name, propertyDescriptor_.Attributes.OfType<Attribute>().ToArray())
        {
            Owner = owner_;
            propertyDescriptor = propertyDescriptor_;
            resource = resource_;
            EditableSource = propertyDescriptor.GetAttribute<BindableEnablementAttribute>();
            VisibilitySource = propertyDescriptor.GetAttribute<BindableVisibilityAttribute>();
            CustomTemplateKey = propertyDescriptor.GetAttribute<CustomTemplateAttribute>();
            
            if (typeof (ISearchableItem).IsAssignableFrom(propertyDescriptor_.PropertyType))
            {
                MethodInfo function = propertyDescriptor.PropertyType.GetMethod("MatchesSearchInput");
                object actualInstance = Owner.GetType().GetProperty(propertyDescriptor.Name).GetValue(Owner, null);
                MatchesSearch =
                    (Func<string, bool>) Delegate.CreateDelegate(typeof (Func<string, bool>), actualInstance, function);
            } 
        }
 
        private bool isSearchMatched = false;
        public bool IsSearchMatched
        {
            get { return isSearchMatched; }
            set
            {
                if (isSearchMatched != value)
                {
                    isSearchMatched = value;
                    OnPropertyChanged("IsSearchMatched");
                }
            }
        }

        private string searchText;
        public string SearchText
        {
            get { return searchText; }
            set
            {
                if (searchText != value)
                {
                    searchText = value;
                    OnPropertyChanged("SearchText");
                }
            }
        }

 
        public DataTemplate GetTemplate()
        {
            if (CustomTemplateKey == null) return null;
            return GetTemplate(CustomTemplateKey.TemplateKey, CustomTemplateKey.ShowInPopup);
        }

        public DataTemplate GetPopupTemplate()
        {
            if (CustomTemplateKey == null || !CustomTemplateKey.ShowInPopup) return null;
            return resource[CustomTemplateKey.TemplateKey] as DataTemplate;
        }

        private DataTemplate GetTemplate(string customTemplateName_, bool isPopup_)
        {
            DataTemplate dataTemplate =
    resource[isPopup_ ? "HyperLinkButtonPopup" : customTemplateName_] as DataTemplate;
            if (dataTemplate == null || isPopup_) return dataTemplate;

            DataTemplate realTemplate;
            if (!templates.TryGetValue(dataTemplate, out realTemplate))
            {
                var contentFactory = new FrameworkElementFactory(typeof(ContentControl));
                contentFactory.SetBinding(ContentControl.ContentProperty, new Binding("Value"));
                contentFactory.SetValue(ContentControl.MarginProperty, new Thickness(0, 2, 0, 2));
                contentFactory.SetValue(ContentPresenter.ContentTemplateProperty, dataTemplate); 
                realTemplate = new DataTemplate() { VisualTree = contentFactory };
                realTemplate.Seal();
                templates.Add(dataTemplate, realTemplate);
            }
            return realTemplate;

        }

        /// <summary>
        /// Raised when a property on this object has a new value.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises this object's PropertyChanged event.
        /// </summary>
        /// <param name="propertyName_">The property that has a new value.</param>
        protected virtual void OnPropertyChanged(string propertyName_)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                var e = new PropertyChangedEventArgs(propertyName_);
                handler(this, e);
            }
        }
        public override bool CanResetValue(object component_)
        {
            return propertyDescriptor.CanResetValue(Owner);
        }

        public override object GetValue(object component_)
        {
            object value = propertyDescriptor.GetValue(Owner);
            if (value != null && propertyDescriptor.GetAttribute<ExpandableObjectAttribute>() != null)
            {
                return new CustomTypeDescriptor(propertyDescriptor.GetValue(Owner), resource);
            }
            return value;
        }

        public override void ResetValue(object component_)
        {
            propertyDescriptor.ResetValue(Owner);
        }

        public override void SetValue(object component_, object value_)
        {
            CustomTypeDescriptor descriptor = value_ as CustomTypeDescriptor;
            if (descriptor != null)
            {
                propertyDescriptor.SetValue(Owner, descriptor.SourceObject);
            }
            else
            {
                propertyDescriptor.SetValue(Owner, value_);
            }
        }
         
        public override bool ShouldSerializeValue(object component_)
        {
            return propertyDescriptor.ShouldSerializeValue(Owner);
        }
         

        public override Type ComponentType
        {
            get { return Owner.GetType(); }
        }

        public override bool IsReadOnly
        {
            get { return false; }
        }

        public override Type PropertyType
        {
            get { return propertyDescriptor.PropertyType; }
        }

        public object Owner { get; private set; }

        public BindableVisibilityAttribute VisibilitySource { get; private set; }
        public BindableEnablementAttribute EditableSource { get; private set; }
        public CustomTemplateAttribute CustomTemplateKey { get; private set; } 

        public override bool Equals(object obj_)
        {
            if (ReferenceEquals(null, obj_))
            {
                return false;
            }
            if (ReferenceEquals(this, obj_))
            {
                return true;
            }
            if (obj_.GetType() != GetType())
            {
                return false;
            }
            return Equals((SettingItemDescriptor) obj_);
        }

        #region Equality members

        public bool Equals(SettingItemDescriptor other_)
        {
            if (ReferenceEquals(null, other_))
            {
                return false;
            }
            if (ReferenceEquals(this, other_))
            {
                return true;
            }
            return base.Equals(other_) && Equals(Owner, other_.Owner);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (base.GetHashCode() * 397) ^ (Owner != null ? Owner.GetHashCode() : 0);
            }
        }

        public static bool operator ==(SettingItemDescriptor left_, SettingItemDescriptor right_)
        {
            return Equals(left_, right_);
        }

        public static bool operator !=(SettingItemDescriptor left_, SettingItemDescriptor right_)
        {
            return !Equals(left_, right_);
        }

        #endregion
    }
}