﻿  
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.My; 

namespace MorganStanley.MSDotNet.MSGui.Impl.Options
{
    /// <summary>
    /// This will also cache the property descriptor definition
    /// 
    /// </summary>
    public class CustomTypeDescriptor : SearchableItemsProvider, ICustomTypeDescriptor
    {
        #region Private fields
        private readonly object sourceObject;
        private readonly EditorDefinitionsHolder editorDefinitions;   
        private readonly ResourceDictionary resource;
        private PropertyDescriptorCollection properties;

        #endregion

        public CustomTypeDescriptor(object model_, ResourceDictionary resource_)
        {
            sourceObject = model_;
            resource = resource_;
            var notifable = model_ as INotifyPropertyChanged;
            if (notifable != null)
            {
                notifable.PropertyChanged += ModelPropertyChanged;   
            }
            editorDefinitions = new EditorDefinitionsHolder(this);

        }

        #region Properties

        internal EditorDefinitionsHolder EditorDefinitions
        {
            get { return editorDefinitions; }
        }


        public PropertyDescriptorCollection Properties
        {
            get { return properties; }
            set
            {
                if (properties != value)
                {
                    properties = value;
                    OnPropertyChanged("Properties");
                }
            }
        }

        #endregion
 
        public object SourceObject
        {
            get { return sourceObject; }
        }

        #region Implementation of ICustomtypeDescriptor

        public AttributeCollection GetAttributes()
        {
            return TypeDescriptor.GetAttributes(sourceObject);
        }

        public string GetClassName()
        {
            return TypeDescriptor.GetClassName(this);
        }

        public string GetComponentName()
        {
            return TypeDescriptor.GetComponentName(this);
        }

        public TypeConverter GetConverter()
        {
            return null;
        }

        public EventDescriptor GetDefaultEvent()
        {
            return null;
        }

        public PropertyDescriptor GetDefaultProperty()
        {
            return null;
        }

        public object GetEditor(Type editorBaseType_)
        {
            return null;
        }

        public EventDescriptorCollection GetEvents()
        {
            return GetEvents(null);
        }

        public EventDescriptorCollection GetEvents(Attribute[] attributes_)
        {
            return TypeDescriptor.GetEvents(this, attributes_);
        }

        public override PropertyDescriptorCollection GetProperties()
        {
            return GetProperties(null);
        }

        public PropertyDescriptorCollection GetProperties(Attribute[] attributes_)
        {
            if (Properties == null)
            {
                List<PropertyDescriptor> propertyDescriptors = new List<PropertyDescriptor>();
                foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(sourceObject, attributes_))
                {
                    if (property.Name == "HasCustomTemplate" && sourceObject is IOptionViewViewModel) continue;
                    
                    SettingItemDescriptor descriptor = new SettingItemDescriptor(sourceObject, property, resource);
                    propertyDescriptors.Add(descriptor); 
                }
                 Properties = new PropertyDescriptorCollection(propertyDescriptors.ToArray());
     
            }
            return Properties;
        }

        public object GetPropertyOwner(PropertyDescriptor pd_)
        {
            SettingItemDescriptor customPropertyDescriptor = pd_ as SettingItemDescriptor;
            if (customPropertyDescriptor != null)
            {
                return customPropertyDescriptor.Owner;
            }
            return null;
        }

        public override string ToString()
        {
            return sourceObject.ToString();
        }

        #endregion

  
        #region Private Methods 
        private void ModelPropertyChanged(object sender_, PropertyChangedEventArgs e_)
        {
            OnPropertyChanged(e_.PropertyName);
        }

        #endregion
 
    }
}
