﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace MorganStanley.MSDotNet.MSGui.Impl.Options
{
    public static class ShowPopupCommands
    {
        public static RoutedCommand ShowPopupCommand = new RoutedUICommand("Show Popup", "ShowThePopup", typeof(ShowPopupCommands)); 
    }
}
