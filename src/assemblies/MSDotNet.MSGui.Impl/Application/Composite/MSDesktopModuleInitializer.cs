﻿using System;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.ServiceLocation;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.My;

namespace MorganStanley.MSDotNet.MSGui.Impl.Application
{
    internal class MSDesktopModuleInitializer : MSDesktopModuleInitializerBase<ModuleInfo, IModule>,
                                                IModuleInitializer
    {
        private readonly ILoggerFacade loggerFacade;
        private static readonly IMSLogger Logger = MSLoggerFactory.CreateLogger<MSDesktopModuleInitializer>();

        /// <summary>
        /// Initializes a new instance of <see cref="ModuleInitializer"/>.
        /// </summary>
        /// <param name="serviceLocator_">The container that will be used to resolve the modules by specifying its type.</param>
        /// <param name="loggerFacade_">The logger to use.</param>
        /// <param name="moduleLoadInfos_"></param>
        public MSDesktopModuleInitializer(IServiceLocator serviceLocator_, ILoggerFacade loggerFacade_,
                                          IModuleLoadInfos moduleLoadInfos_)
            : base(serviceLocator_, moduleLoadInfos_)
        {

            if (loggerFacade_ == null)
            {
                throw new ArgumentNullException("loggerFacade_");
            }

            loggerFacade = loggerFacade_;
        }

        protected override string GetModuleTypeName(ModuleInfo moduleInfo_)
        {
            return moduleInfo_.ModuleType;
        }

        protected override string GetModuleName(ModuleInfo moduleInfo_)
        {
            return moduleInfo_.ModuleName;
        }

        protected override void InitializeModule(IModule moduleInstance_)
        {
            moduleInstance_.Initialize();
        }

        protected override IModule NomalizeModule(object module_)
        {
            var module2 = module_ as IModule;
            if (module2 != null) return module2;
            var module3 = module_ as IMSDesktopModule;
            if (module3 != null) return new CompositeModuleWrapper(module3);
            throw new ApplicationException("The module is not of type IModule");
        }

        protected override Exception NomalizeModuleError(Exception exception_, string moduleName_, string assemblyName_)
        {
            if (exception_ is ModuleInitializeException)
            {
                return exception_;
            }
            if (!string.IsNullOrEmpty(assemblyName_))
            {
                return new ModuleInitializeException(moduleName_, assemblyName_, exception_.Message, exception_);
            }
            return new ModuleInitializeException(moduleName_, exception_.Message, exception_);
        }

        protected override void LogException(Exception exception_, string message_)
        {
            loggerFacade.Log(exception_.ToString(), Category.Exception, Priority.High);
            Logger.Error(message_, exception_);
        }
    }
}
