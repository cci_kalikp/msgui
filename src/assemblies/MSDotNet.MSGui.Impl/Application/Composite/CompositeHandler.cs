﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using Microsoft.Practices.Composite;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Composite.Presentation.Regions;
using Microsoft.Practices.Composite.Presentation.Regions.Behaviors;
using Microsoft.Practices.Composite.Regions;
using Microsoft.Practices.Composite.UnityExtensions;
using Microsoft.Practices.ObjectBuilder2;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Core.Persistence.DelayedLoading;
using MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication.Composite;
using MorganStanley.MSDotNet.My;

namespace MorganStanley.MSDotNet.MSGui.Impl.Application.Composite
{
    internal sealed class CompositeHandler : IModuleFrameworkHandler
    {
        private static readonly IMSLogger _logger = MSLoggerFactory.CreateLogger<Bootstrapper>();

        private readonly string _configFile;
        private readonly bool _isUseReflection;

        private readonly IList<Tuple<Type, string>> _registeredModuleTypesWithNames = new List<Tuple<Type, string>>();
        private readonly IList<ModuleInfo> _registeredModuleInfos = new List<ModuleInfo>();
        private readonly IList<Type> _registeredModuleTypes = new List<Type>();
        private readonly IUnityContainer _container;
        private readonly IList<IPreInitializer> _preInitializers = new List<IPreInitializer>();

        private IUnityContainer Container
        {
            get { return _container; }
        }

        internal CompositeHandler(IUnityContainer unityContainer)
            : this(unityContainer, false, null)
        {
        }

        internal CompositeHandler(IUnityContainer unityContainer, bool useReflection, string configFile)
        {
            _container = unityContainer;
            _isUseReflection = useReflection;
            _configFile = configFile;
        }

        private IModuleCatalog GetModuleCatalog()
        {
            _logger.Debug("Getting module catalog");
            var moduleLoadInfos = Container.Resolve<IModuleLoadInfos>();
            return String.IsNullOrEmpty(_configFile) ? 
                new ResolverConfigurationModuleCatalog(_registeredModuleTypes, _registeredModuleTypesWithNames,
                                                             _registeredModuleInfos, moduleLoadInfos):
                new ResolverConfigurationModuleCatalog(_configFile, _registeredModuleTypes,
                            _registeredModuleTypesWithNames,
                            _registeredModuleInfos, moduleLoadInfos); 
             
        }

        public void InvokePreInitializers()
        {
            _preInitializers.ForEach(p => p.PreInitialize(_container));
        }

        public void AddPreInitializer(IPreInitializer preInitializer)
        {
            _preInitializers.Add(preInitializer);
        }

        /// <summary>
        /// Registers in the <see cref="IUnityContainer"/> the <see cref="Type"/> of the Exceptions
        /// that are not considered root exceptions by the <see cref="Microsoft.Practices.Composite.ExceptionExtensions"/>.
        /// </summary>
        public /*virtual*/ void RegisterFrameworkExceptionTypes()
        {
            ExceptionExtensions.RegisterFrameworkExceptionType(typeof (ActivationException));
            ExceptionExtensions.RegisterFrameworkExceptionType(typeof (ResolutionFailedException));
        }

        public void AddModule(Type moduleType)
        {
            _logger.DebugWithFormat("Adding module {0}", moduleType.FullName);
            _registeredModuleTypes.Add(moduleType);
        }


        public void AddModule(Type moduleType, string moduleName)
        {
            _logger.DebugWithFormat("Adding module {0} with name {1}", moduleType.FullName, moduleName);
            _registeredModuleTypesWithNames.Add(new Tuple<Type, string>(moduleType, moduleName));
        }

        public void AddModule(object moduleInfo)
        {
            AddModule((ModuleInfo)moduleInfo);
        }

        public void AddModule(ModuleInfo moduleInfo)
        {
            _logger.DebugWithFormat("Adding module {0} with name {1}", moduleInfo.ModuleType, moduleInfo.ModuleName);
            _registeredModuleInfos.Add(moduleInfo);
        }

        public bool HasModule(Type moduleType)
        {
            return _registeredModuleTypes.Contains(moduleType) ||
                   _registeredModuleTypesWithNames.Any(m => m.Item1 == moduleType)
                   || _registeredModuleInfos.Any(m => m.ModuleType == moduleType.FullName);
        }

        public Exception GetModuleInitializeException(string moduleType)
        {
            return new ModuleInitializeException(String.Format(CultureInfo.CurrentCulture,
                                                               "Module {0} is not valid when Concord is not enabled.",
                                                               moduleType));

        }

        public void InvokeAllModulesLoaded()
        {
            DelayedProfileLoadingExtensionPoints.InvokeAllModulesLoaded(new AllModulesLoadedEventArgs
                {
                    Modules = Container.Resolve<IModuleLoadInfos>()
                });
        }

        private readonly ILoggerFacade _loggerFacade = new TextLogger();

        public ILoggerFacade LoggerFacade
        {
            get { return _loggerFacade; }
        }

        /// <summary>
        /// Configures the <see cref="IUnityContainer"/>. May be overwritten in a derived class to add specific
        /// type mappings required by the application.
        /// </summary>
        public void ConfigureContainer(bool useDefaultConfiguration)
        {


            // We register the container with an ExternallyControlledLifetimeManager to avoid
            // recursive calls if Container.Dispose() is called.
            //Container.RegisterInstance<IUnityContainer>(Container);


            Container.RegisterInstance(LoggerFacade);

            var catalog = GetModuleCatalog();

            if (catalog != null)
            {
                Container.RegisterInstance(catalog);
                var interceptable = catalog as IInterceptableModuleCatalog;
                if (interceptable != null)
                {
                    Container.RegisterInstance(interceptable);
                    if (_isUseReflection)
                    {
                        interceptable.AddInterceptor(new ReflectionModuleInfoInterceptor());
                    }
                } 
            }

            if (useDefaultConfiguration)
            {

                RegisterTypeIfMissing(typeof (IServiceLocator), typeof (UnityServiceLocatorAdapter), true);
                RegisterTypeIfMissing(typeof (IModuleManager), typeof (ModuleManager), true);
                RegisterTypeIfMissing(typeof (IEventAggregator), typeof (EventAggregator), true);


                RegisterTypeIfMissing(typeof(IRegionManager), typeof(RegionManager), true);
                RegisterTypeIfMissing(typeof(RegionAdapterMappings), typeof(RegionAdapterMappings), true);

                RegisterTypeIfMissing(typeof(IRegionViewRegistry), typeof(RegionViewRegistry), true);

                RegisterTypeIfMissing(typeof(IRegionBehaviorFactory), typeof(RegionBehaviorFactory), true);
                RegisterTypeIfMissing(typeof(ContentControlRegionAdapter), typeof(ContentControlRegionAdapter), true);
                RegisterTypeIfMissing(typeof(SelectorRegionAdapter), typeof(SelectorRegionAdapter), true);
                RegisterTypeIfMissing(typeof(ItemsControlRegionAdapter), typeof(ItemsControlRegionAdapter), true);


                var behaviorFactory = _container.Resolve<IRegionBehaviorFactory>();
                behaviorFactory.AddIfMissing("RegionManagerRegistrationBehavior",
                                             typeof(RegionManagerRegistrationBehavior));
                behaviorFactory.AddIfMissing("AutoPopulateRegionBehavior",
                                            typeof(AutoPopulateRegionBehavior));

                var moduleInitializer = new MSDesktopModuleInitializer(
                    Container.Resolve<IServiceLocator>(),
                    Container.Resolve<ILoggerFacade>(), Container.Resolve<IModuleLoadInfos>());

                Container.RegisterInstance<IModuleInitializer>(moduleInitializer);

                ServiceLocator.SetLocatorProvider(() => Container.Resolve<IServiceLocator>());

                var mappings = _container.Resolve<RegionAdapterMappings>();
                mappings.RegisterMapping(typeof(ContentControl), _container.Resolve<ContentControlRegionAdapter>());
                mappings.RegisterMapping(typeof(Selector), _container.Resolve<SelectorRegionAdapter>());
                mappings.RegisterMapping(typeof(ItemsControl), _container.Resolve<ItemsControlRegionAdapter>());
            }
        }

        public event EventHandler<EventArgs<string>> BeforeModuleInitialized
        {
            add
            {
                var initializer = Container.Resolve<IModuleInitializer>();
                ((MSDesktopModuleInitializer) initializer).BeforeModuleInitialized += value;
            }
            remove
            {
                var initializer = Container.Resolve<IModuleInitializer>();
                ((MSDesktopModuleInitializer) initializer).BeforeModuleInitialized -= value;
            }
        }

        public event EventHandler<EventArgs<string>> AfterModuleInitialized
        {
            add
            {
                var initializer = Container.Resolve<IModuleInitializer>();
                ((MSDesktopModuleInitializer) initializer).AfterModuleInitialized += value;
            }
            remove
            {
                var initializer = Container.Resolve<IModuleInitializer>();
                ((MSDesktopModuleInitializer) initializer).AfterModuleInitialized -= value;
            }
        }

        public bool DisableEventAggregatorBridge { get; set; }
        public void InjectRootRegionManger(IWindowViewContainer view)
        {
            if (RegionManager.GetRegionManager(view.Content as DependencyObject) == null)
            {
                RegionManager.SetRegionManager(view.Content as DependencyObject, _container.Resolve<IRegionManager>());
            }
        }

        public void InitializeEarlyFrameworkModules()
        {
            try
            {
                var manager = Container.Resolve<IModuleManager>();
                var catalog = Container.Resolve<IModuleCatalog>(); 
                foreach (var earlyModule in catalog.Modules.Where(
                    m =>
                        {
                            var type = Type.GetType(m.ModuleType);
                            if (type == null) return false;
                            return type.GetInterface(typeof (IEarlyInitializedFrameworkModule).FullName) != null;
                        })
                    )
                {
                    manager.LoadModule(earlyModule.ModuleName);
                }
            }
            catch (ResolutionFailedException ex)
            {
                if (ex.Message.Contains("IModuleCatalog"))
                {
                    throw new InvalidOperationException("ModuleCatalog is not initialized.");
                }

                throw;
            } 
        }
 

        /// <summary>
        /// Initializes the modules. May be overwritten in a derived class to use a custom Modules Catalog
        /// </summary>
        public void InitializeModules()
        {
            IModuleManager manager; 
            try
            {
                manager = Container.Resolve<IModuleManager>();
            }
            catch (ResolutionFailedException ex)
            {
                if (ex.Message.Contains("IModuleCatalog"))
                {
                    throw new InvalidOperationException("ModuleCatalog is not initialized.");
                }

                throw;
            }

            manager.Run();
        }


        public void ThrowModuleInitializationExceptionIfneeded()
        {
            var moduleInitializer =
                Container.Resolve<IModuleInitializer>() as MSDesktopModuleInitializer;
            if (moduleInitializer != null) moduleInitializer.ThrowExceptionIfNeeded();
        }


        /// <summary>
        /// Registers a type in the container only if that type was not already registered.
        /// </summary>
        /// <param name="fromType">The interface type to register.</param>
        /// <param name="toType">The type implementing the interface.</param>
        /// <param name="registerAsSingleton">Registers the type as a singleton.</param>
        private void RegisterTypeIfMissing(Type fromType, Type toType, bool registerAsSingleton)
        {

            if (!Container.IsTypeRegistered(fromType))
            {
                if (registerAsSingleton)
                {
                    Container.RegisterType(fromType, toType, new ContainerControlledLifetimeManager());
                }
                else
                {
                    Container.RegisterType(fromType, toType);
                }
            }
        }


        public void RegisterTypes()
        {
            if (DisableEventAggregatorBridge)
            {
                Container.RegisterType<IEventAggregator, EventAggregator>();
            }
            else
            {
                var eventAggregatorWrapper = new EventAggregatorWrapper(Container.Resolve<IEventAggregator>());

                var msDesktopEventAggregator = new EventAggregatorRegistrator(eventAggregatorWrapper);
                Container.RegisterInstance<IMSDesktopEventAggregator>(msDesktopEventAggregator);
                //Container.RegisterType<IMSDesktopEventAggregator, EventAggregatorRegistrator>("InnerMSDesktopEventAggregator",
                //        new ContainerControlledLifetimeManager());
                Container.RegisterInstance<IIPCEventAggregator>(msDesktopEventAggregator);
                // Container.RegisterType<IIPCEventAggregator, EventAggregatorRegistrator>("InnerMSDesktopEventAggregator",
                //   new ContainerControlledLifetimeManager());

                Container.RegisterInstance<IEventAggregator>(
                    new MSDesktopEventAggregatorWrapper(msDesktopEventAggregator));
                //Container.RegisterType<IEventAggregator, MSDesktopEventAggregatorWrapper>(
                //    new ContainerControlledLifetimeManager());
            }

            Container.RegisterType<ICommunicator, Communicator>(new ContainerControlledLifetimeManager());
            IPCCompositePresentationEventContainerHolder.Container = Container;

            Container.RegisterType<IEventRegistrator, InterProcessMessenger>(new ContainerControlledLifetimeManager());


            Container.RegisterType<IMessageTypeRegistrator, MessageTypeRegistrator>(
                new ContainerControlledLifetimeManager());
            Container.RegisterType<IV2VConnectionManager, V2VConfigModule>(new ContainerControlledLifetimeManager());
        }

        public IModuleLoadInfo GetModuleLoadInfo(object moduleInfo)
        {
            var info = moduleInfo as ModuleInfo;
            if (info != null)
            {
                return new ModuleLoadInfo(info.ModuleType)
                    {
                        DependsOnModuleNames = info.DependsOn,
                        ModuleName = info.ModuleName
                    };
            }

            throw new ApplicationException("Not a module Info");
        }

    }
}
