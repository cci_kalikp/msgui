﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Application/BootstrapperBase.cs#35 $
// $Change: 899282 $
// $DateTime: 2014/10/02 12:30:38 $
// $Author: anbuy $

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Logging;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity;
using MorganStanley.MSDotNet.My;

namespace MorganStanley.MSDotNet.MSGui.Impl.Application
{
    /// <summary>
    /// Base class that provides a basic bootstrapping sequence that
    /// registers most of the Composite Application Library assets
    /// in a <see cref="IUnityContainer"/>.
    /// </summary>
    /// <remarks>
    /// This class must be overridden to provide application specific configuration.
    /// </remarks>
    internal abstract class BootstrapperBase
    {
        public static bool InjectCustomMsLoggerProvider = false; 

        public static bool UsePrismAsInfrasturcture = false;

        public event EventHandler<ContainerEventArgs> ConfiguringContainer;
        public event EventHandler<EventArgs> InitializationFinished;
        

        private bool _useDefaultConfiguration = true;
        private bool _isFrameworkInitialized;
        
        private readonly IUnityContainer _container;

        protected BootstrapperBase(IUnityContainer container)
        {
            _container = container;
        }

       

        protected IUnityContainer Container
        {
            get { return _container; }
        }

        protected virtual void SetUpLogging()
        {
            if (InjectCustomMsLoggerProvider)
            {
                MSLoggerFactory.Provider = null;
                MSLoggerFactory.Provider = new MethodNameResolvingMSLoggerProvider();
            }
            // else branch removed as DNParts has been fixed not to depend on a default logger.
            //else
            //{
            //    MSLoggerFactory.Provider = new MSLoggerProvider();
            //}
        }

        /// <summary>
        /// Runs the bootstrapper process.
        /// </summary>
        public void Run()
        {
            this.Run(true);
        }

        private IModuleFrameworkHandler _handler;
        internal IModuleFrameworkHandler ModuleFrameworkHandler
        {
            get
            {
                if(_handler == null)
                    InitializeModuleHandler();
                
                return _handler;
            }
            set { _handler = value; }
        }

        public bool IsFrameworkInitialized
        {
            get { return _isFrameworkInitialized; }
        }

        /// <summary>
        /// Run the bootstrapper process.
        /// </summary>
        /// <param name="runWithDefaultConfiguration">If <see langword="true"/>, registers default Composite Application Library services in the container. This is the default behavior.</param>
        public void Run(bool runWithDefaultConfiguration)
        {
#if DEBUG_BREAK
      System.Diagnostics.Debugger.Break();
#endif
            _useDefaultConfiguration = runWithDefaultConfiguration;
            SetUpLogging();
            
            ConfigureContainer();
            
            _container.RegisterInstance<IModuleFrameworkHandler>(ModuleFrameworkHandler);
            ModuleFrameworkHandler.RegisterFrameworkExceptionTypes();
            CreateShell();


            ModuleFrameworkHandler.InvokePreInitializers();
            InitializeModules();
            InvokeInitializationFinished(null);
        }

     
        protected virtual void OnAfterModuleInitialized(object sender, EventArgs<string> e)
        {

        }

        protected virtual void OnBeforeModuleInitialized(object sender, EventArgs<string> e)
        {

        }


        /// <summary>
        /// Configures the <see cref="IUnityContainer"/>. May be overwritten in a derived class to add specific
        /// type mappings required by the application.
        /// </summary>
        protected virtual void ConfigureContainer()
        {
            InvokeConfiguringContainer(new ContainerEventArgs(Container));
            ModuleFrameworkHandler.ConfigureContainer(_useDefaultConfiguration);
        }





        /// <summary>
        /// Initializes the modules. May be overwritten in a derived class to use a custom Modules Catalog
        /// </summary>
        protected virtual void InitializeModules()
        {
            ModuleFrameworkHandler.InitializeEarlyFrameworkModules();
            ModuleFrameworkHandler.InitializeModules(); 
        }

        /// <summary>
        /// Creates the <see cref="IUnityContainer"/> that will be used as the default container.
        /// </summary>
        /// <returns>A new instance of <see cref="IUnityContainer"/>.</returns>
        [CLSCompliant(false)]
        protected virtual IUnityContainer CreateContainer()
        {
            return new UnityContainer();
        }


        /// <summary>
        /// Creates the shell or main window of the application.
        /// </summary>
        /// <returns>The shell of the application.</returns>
        /// <remarks>
        /// If the returned instance is a <see cref="DependencyObject"/>, the
        /// <see cref="UnityBootstrapper"/> will attach the default <seealso cref="Microsoft.Practices.Composite.Regions.IRegionManager"/> of
        /// the application in its <see cref="RegionManager.RegionManagerProperty"/> attached property
        /// in order to be able to add regions by using the <seealso cref="RegionManager.RegionNameProperty"/>
        /// attached property from XAML.
        /// </remarks>
        protected abstract DependencyObject CreateShell();

        protected abstract void InitializeModuleHandler();
      

        private void InvokeConfiguring(EventArgs args)
        {
         ModuleFrameworkHandler.InvokeAllModulesLoaded();   
        }

        internal void InvokeConfiguringContainer(ContainerEventArgs e)
        {
            var handler = ConfiguringContainer;
            if (handler != null) handler(this, e);
        }

        internal void InvokeInitializationFinished(EventArgs e)
        {
            _isFrameworkInitialized = true;

            var handler = InitializationFinished;
            if (handler != null)
            {
                handler(this, e);
            }

            ModuleFrameworkHandler.ThrowModuleInitializationExceptionIfneeded();
        }
    }

    internal sealed class ContainerEventArgs : EventArgs
    {
        private readonly IUnityContainer _container;

        public ContainerEventArgs(IUnityContainer container)
        {
            _container = container;
        }

        public IUnityContainer Container
        {
            get { return _container; }
        }
    }


}
