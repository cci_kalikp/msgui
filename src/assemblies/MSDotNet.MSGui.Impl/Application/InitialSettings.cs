/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Application/InitialSettings.cs#20 $
// $Change: 902331 $
// $DateTime: 2014/10/24 05:43:11 $
// $Author: caijin $

using System;
using System.Windows;
using System.Windows.Input;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Extensions;

namespace MorganStanley.MSDotNet.MSGui.Impl.Application
{
	internal sealed class InitialSettings
	{
		private bool _frozen;
        private ShellMode _shellMode = ShellMode.RibbonWindow; 
        private string _appBarTitle;
        private string _concordDefaultProfile;
        private bool _concordEnabled;
        private bool _enableSoftwareRendering;
        private bool _disableEnforcedSizeRestrictions;
        private LayoutLoadStrategy _layoutLoadStrategy = LayoutLoadStrategy.Default;
        private LayoutLoadStrategyCallback _layoutLoadStrategyCallback;
        private bool _legacyControlPlacementEnabled;
        private KeyGesture _windowRenameGesture;
        private bool _enableNonOwnedWindows;
        private IAlertConsumer _alertConsumer;
        private SkipSaveProfileOnExitCondition skipSaveProfileOnExitCondition = SkipSaveProfileOnExitCondition.Never;
	    private bool preventRibbonCleaning;
	    private Func<string[], string> customToolWindowTitleGenerator;
	    private TextTrimming viewTitleTextTrimming = TextTrimming.None;
	    private bool disableQatHack;
	    private bool enableLegacyDialog;

	    public void Freeze()
		{
			_frozen = true;
		}

		public ShellMode ShellMode
		{
			get
			{
				return _shellMode;
			}
			set
			{
				if (!_frozen)
				{
					_shellMode = value;
				}
			}
		}
 
		public string AppBarTitle
		{
			get
			{
				return _appBarTitle;
			}
			set
			{
				if (!_frozen)
				{
					_appBarTitle = value;
				}
			}
		}

		public string ConcordDefaultProfile
		{
			get
			{
				return _concordDefaultProfile;
			}
			set
			{
				if (!_frozen)
				{
					_concordDefaultProfile = value;
				}
			}
		}

		public bool ConcordEnabled
		{
			get
			{
				return _concordEnabled;
			}
			set
			{
				if (!_frozen)
				{
					_concordEnabled = value;
				}
			}
		}

		public bool EnableSoftwareRendering
		{
			get
			{
				return _enableSoftwareRendering;
			}
			set
			{
				if (!_frozen)
				{
					_enableSoftwareRendering = value;
				}
			}
		}

		public bool DisableEnforcedSizeRestrictions
		{
			get
			{
				return _disableEnforcedSizeRestrictions;
			}
			set
			{
				if (!_frozen)
				{
					_disableEnforcedSizeRestrictions = value;
				}
			}
		}

		public LayoutLoadStrategy LayoutLoadStrategy
		{
			get
			{
				return _layoutLoadStrategy;
			}
			set
			{
				if (!_frozen)
				{
					_layoutLoadStrategy = value;
				}
			}
		}

		public LayoutLoadStrategyCallback LayoutLoadStrategyCallback
		{
			get
			{
				return _layoutLoadStrategyCallback;
			}
			set
			{
				if (!_frozen)
				{
					_layoutLoadStrategyCallback = value;
				}
			}
		}

		public bool LegacyControlPlacementEnabled
		{
			get
			{
				return _legacyControlPlacementEnabled;
			}
			set
			{
				if (!_frozen)
				{
					_legacyControlPlacementEnabled = value;
				}
			}
		}

		public KeyGesture WindowRenameGesture
		{
			get
			{
				return _windowRenameGesture;
			}
			set
			{
				if (!_frozen)
				{
					_windowRenameGesture = value;
				}
			}
		}

		public IAlertConsumer AlertConsumer
		{
			get
			{
				return _alertConsumer;
			}
			set
			{
				if (!_frozen)
				{
					_alertConsumer = value;
				}
			}
		}

		public bool EnableNonOwnedWindows 
		{ 
			get
			{
				return _enableNonOwnedWindows;
			}
			set
			{
				if (!_frozen)
				{
					_enableNonOwnedWindows = value;
				}
			}
		}

        public SkipSaveProfileOnExitCondition SkipSaveProfileOnExitCondition
        {
            get
            {
                return skipSaveProfileOnExitCondition;
            }
            set
            {
                if (!_frozen)
                {
                    skipSaveProfileOnExitCondition = value;
                }
            }
        }

	    public bool PreventRibbonCleaning
	    {
	        get
	        {
	            return preventRibbonCleaning;
	        }
	        set
	        {
                if (!_frozen)
                {
                    preventRibbonCleaning = value;
                }
	        }
	    }

        public Func<string[], string> CustomToolWindowTitleGenerator
        {
            get
            {
                return customToolWindowTitleGenerator;
            }
            set
            {
                if (!_frozen)
                {
                    customToolWindowTitleGenerator = value;
                }
            }
        }

	    public TextTrimming ViewTitleTextTrimming
	    {
	        get
	        {
	            return viewTitleTextTrimming;
	        }
	        set
	        {
                if (!_frozen)
                {
                    viewTitleTextTrimming = value;
                }
	        }
	    }

	    public bool DisableQATHack
	    {
	        get
	        {
	            return disableQatHack;
	        }
	        set
	        {
                if (!_frozen)
                {
                    disableQatHack = value;
                }
	        }
	    }
 
	    public bool EnableLegacyDialog
	    {
	        get { return enableLegacyDialog; }
            set
            {
                if (!_frozen)
                {
                    enableLegacyDialog = value;
                }
            }
	    }
	}

    [Flags]
    public enum SkipSaveProfileOnExitCondition
    {
        Never = 0,
        ModuleLoadExceptioned = 1,
        ModuleTypeResolutionFailed = 2,
        ModuleBlockedByEntitlement = 4
    }
}