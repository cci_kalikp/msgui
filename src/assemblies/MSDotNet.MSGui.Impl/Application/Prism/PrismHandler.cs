﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using Microsoft.Practices.ObjectBuilder2;
using Microsoft.Practices.Prism;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.Prism.Logging;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Prism.Regions.Behaviors;
using Microsoft.Practices.Prism.UnityExtensions;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Core.Messaging;
using MorganStanley.MSDotNet.MSGui.Core.Persistence.DelayedLoading;
using MorganStanley.MSDotNet.MSGui.Impl.InterProcessMessaging;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.ModuleToModuleCommunication;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.Prism;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication;
using MorganStanley.MSDotNet.MSGui.Impl.ViewToViewCommunication.Prism;
using MorganStanley.MSDotNet.My;

namespace MorganStanley.MSDotNet.MSGui.Impl.Application.Prism
{
    internal sealed class PrismHandler: IModuleFrameworkHandler
    {
        private static readonly IMSLogger _logger = MSLoggerFactory.CreateLogger<Bootstrapper>();

        private readonly IList<Tuple<Type, string>> _registeredModuleTypesWithNames = new List<Tuple<Type, string>>();
        private readonly IList<ModuleInfo> _registeredModuleInfos = new List<ModuleInfo>();
        private readonly IList<Type> _registeredModuleTypes = new List<Type>();
        private readonly IList<IPreInitializer> _preInitializers = new List<IPreInitializer>();

        private readonly string _configFile;
        private readonly bool _isUseReflection;
        private readonly IUnityContainer _container;

        internal PrismHandler(IUnityContainer unityContainer)
            : this(unityContainer, false, null)
        {
        }

        internal PrismHandler(IUnityContainer unityContainer, bool useReflection, string configFile)
        {
            _container = unityContainer;
            _isUseReflection = useReflection;
            _configFile = configFile;
        }


        private IModuleCatalog GetModuleCatalog()
        {
            _logger.Debug("Getting module catalog"); 
            var moduleLoadInfos = _container.Resolve<IModuleLoadInfos>();
            return String.IsNullOrEmpty(_configFile) ? 
                new ResolverConfigurationPrismModuleCatalog(_registeredModuleTypes, _registeredModuleTypesWithNames, _registeredModuleInfos, moduleLoadInfos)
                :new ResolverConfigurationPrismModuleCatalog(_configFile, _registeredModuleTypes, _registeredModuleTypesWithNames, _registeredModuleInfos, moduleLoadInfos); 
        }


        /// <summary>
        /// Registers in the <see cref="IUnityContainer"/> the <see cref="Type"/> of the Exceptions
        /// that are not considered root exceptions by the <see cref="Microsoft.Practices.Prism.ExceptionExtensions"/>.
        /// </summary>
        public /*virtual*/ void RegisterFrameworkExceptionTypes()
        {
            ExceptionExtensions.RegisterFrameworkExceptionType(typeof(ActivationException));
            ExceptionExtensions.RegisterFrameworkExceptionType(typeof(ResolutionFailedException));
        }

        public void AddPreInitializer(IPreInitializer preInitializer)
        {
            _preInitializers.Add(preInitializer);
        }

        public void InvokePreInitializers()
        {
            _preInitializers.ForEach(p => p.PreInitialize(_container));
        }

        public void AddModule(Type moduleType)
        {
            _logger.DebugWithFormat("Adding module {0}", moduleType.FullName);
            _registeredModuleTypes.Add(moduleType);
        }


        public void AddModule(Type moduleType, string moduleName)
        {
            _logger.DebugWithFormat("Adding module {0} with name {1}", moduleType.FullName, moduleName);
            _registeredModuleTypesWithNames.Add(new Tuple<Type, string>(moduleType, moduleName));
        }

        public void AddModule(object moduleInfo)
        {
            AddModule((ModuleInfo)moduleInfo);
        }

        public void AddModule(ModuleInfo moduleInfo)
        {
            _logger.DebugWithFormat("Adding module {0} with name {1}", moduleInfo.ModuleType, moduleInfo.ModuleName);
            _registeredModuleInfos.Add(moduleInfo);
        }

        public bool HasModule(Type moduleType)
        {
            return _registeredModuleTypes.Contains(moduleType) ||
                   _registeredModuleTypesWithNames.Any(m => m.Item1 == moduleType)
                   || _registeredModuleInfos.Any(m => m.ModuleType == moduleType.FullName);
        }

        public Exception GetModuleInitializeException(string moduleType)
        {
            return new ModuleInitializeException(String.Format(CultureInfo.CurrentCulture,
                                                                     "Module {0} is not valid when Concord is not enabled.",
                                                                     moduleType));

        }

        public void InvokeAllModulesLoaded()
        {
            
            DelayedProfileLoadingExtensionPoints.InvokeAllModulesLoaded(new AllModulesLoadedEventArgs
            {
                Modules = _container.Resolve<IModuleLoadInfos>()
            });
            
        }


        private ILoggerFacade _loggerFacade = new TextLogger();
        public ILoggerFacade LoggerFacade { get { return _loggerFacade; } }

        /// <summary>
        /// Configures the <see cref="IUnityContainer"/>. May be overwritten in a derived class to add specific
        /// type mappings required by the application.
        /// </summary>
        public void ConfigureContainer(bool useDefaultConfiguration)
        {
            // We register the container with an ExternallyControlledLifetimeManager to avoid
            // recursive calls if Container.Dispose() is called.
            //Container.RegisterInstance<IUnityContainer>(Container);

            _container.RegisterInstance(LoggerFacade);

            var catalog = GetModuleCatalog();

            if (catalog != null)
            {
                _container.RegisterInstance(catalog); 
                var interceptable = catalog as IInterceptablePrismModuleCatalog;
                if (interceptable != null)
                {
                    _container.RegisterInstance(interceptable);
                    if (_isUseReflection)
                    {
                        interceptable.AddInterceptor(new ReflectionPrismModuleInfoInterceptor());
                    }
                } 
            }

            if (useDefaultConfiguration)
            {

                RegisterTypeIfMissing(typeof(IServiceLocator), typeof(UnityServiceLocatorAdapter), true);
                RegisterTypeIfMissing(typeof(IModuleManager), typeof(ModuleManager), true);
                RegisterTypeIfMissing(typeof(IEventAggregator), typeof(EventAggregator), true);


                RegisterTypeIfMissing(typeof(IRegionManager), typeof(RegionManager), true);
                RegisterTypeIfMissing(typeof(RegionAdapterMappings), typeof(RegionAdapterMappings), true);

                RegisterTypeIfMissing(typeof(IRegionViewRegistry), typeof(RegionViewRegistry), true);

                RegisterTypeIfMissing(typeof(IRegionBehaviorFactory), typeof(RegionBehaviorFactory), true);
                RegisterTypeIfMissing(typeof(ContentControlRegionAdapter), typeof(ContentControlRegionAdapter), true);
                RegisterTypeIfMissing(typeof(SelectorRegionAdapter), typeof(SelectorRegionAdapter), true);
                RegisterTypeIfMissing(typeof(ItemsControlRegionAdapter), typeof(ItemsControlRegionAdapter), true);


                var behaviorFactory = _container.Resolve<IRegionBehaviorFactory>();
                behaviorFactory.AddIfMissing("RegionManagerRegistrationBehavior",
                                             typeof (RegionManagerRegistrationBehavior));
                behaviorFactory.AddIfMissing("AutoPopulateRegionBehavior",
                                            typeof(AutoPopulateRegionBehavior));

                var moduleInitializer = new MSDesktopPrismModuleInitializer(
                    _container.Resolve<IServiceLocator>(),
                    _container.Resolve<ILoggerFacade>(), _container.Resolve<IModuleLoadInfos>());
               
                _container.RegisterInstance<IModuleInitializer>(moduleInitializer);

                ServiceLocator.SetLocatorProvider(() => _container.Resolve<IServiceLocator>());

                var mappings = _container.Resolve<RegionAdapterMappings>();
                mappings.RegisterMapping(typeof(ContentControl), _container.Resolve<ContentControlRegionAdapter>());
                mappings.RegisterMapping(typeof(Selector), _container.Resolve<SelectorRegionAdapter>());
                mappings.RegisterMapping(typeof(ItemsControl), _container.Resolve<ItemsControlRegionAdapter>());

            }
        }

        public event EventHandler<EventArgs<string>> BeforeModuleInitialized
        {
            add{
                var initializer = _container.Resolve<IModuleInitializer>();
                ((MSDesktopPrismModuleInitializer) initializer).BeforeModuleInitialized += value;
            }
            remove
            {
                var initializer = _container.Resolve<IModuleInitializer>();
                ((MSDesktopPrismModuleInitializer)initializer).BeforeModuleInitialized -= value;
            }
        }

        public event EventHandler<EventArgs<string>> AfterModuleInitialized
        {
            add
            {
                var initializer = _container.Resolve<IModuleInitializer>();
                ((MSDesktopPrismModuleInitializer)initializer).AfterModuleInitialized += value;
            }
            remove
            {
                var initializer = _container.Resolve<IModuleInitializer>();
                ((MSDesktopPrismModuleInitializer)initializer).AfterModuleInitialized -= value;
            }   
        }

        public bool DisableEventAggregatorBridge { get; set; }
        public void InjectRootRegionManger(IWindowViewContainer view)
        {
            if (RegionManager.GetRegionManager(view.Content as DependencyObject) == null)
            {   
                RegionManager.SetRegionManager(view.Content as DependencyObject, _container.Resolve<IRegionManager>());    
                RegionManager.UpdateRegions();
            }
        }


        public void InitializeEarlyFrameworkModules()
        {
            try
            {

                var manager = _container.Resolve<IModuleManager>();
                var catalog = _container.Resolve<IModuleCatalog>();

                foreach (var earlyModule in catalog.Modules.Where(
                    m =>
                    {
                        var type = Type.GetType(m.ModuleType);
                        if (type == null) return false;
                        return type.GetInterface(typeof(IEarlyInitializedFrameworkModule).FullName) != null;
                    }))
                {
                    manager.LoadModule(earlyModule.ModuleName);
                }
            }
            catch (ResolutionFailedException ex)
            {
                if (ex.Message.Contains("IModuleCatalog"))
                {
                    throw new InvalidOperationException("ModuleCatalog is not initialized.");
                }

                throw;
            }
        }
 

        public void RegisterTypes()
        {
            //var underylingEventAggregator = Container.Resolve<IEventAggregator>();

            if (DisableEventAggregatorBridge)
            {
                _container.RegisterType<IEventAggregator, EventAggregator>();
            }
            else
            {
                var eventAggregatorWrapper = new PrismEventAggregatorWrapper(_container.Resolve<IEventAggregator>());

                //Container.RegisterType<IMSDesktopEventAggregator, EventAggregatorWrapper>("UnderylingAggregator",
                //    new ContainerControlledLifetimeManager());

                var msDesktopEventAggregator = new EventAggregatorRegistrator(eventAggregatorWrapper);
                _container.RegisterInstance<IMSDesktopEventAggregator>(msDesktopEventAggregator);
                //Container.RegisterType<IMSDesktopEventAggregator, EventAggregatorRegistrator>("InnerMSDesktopEventAggregator",
                //        new ContainerControlledLifetimeManager());
                _container.RegisterInstance<IIPCEventAggregator>(msDesktopEventAggregator);
                // Container.RegisterType<IIPCEventAggregator, EventAggregatorRegistrator>("InnerMSDesktopEventAggregator",
                //   new ContainerControlledLifetimeManager());

                _container.RegisterInstance<IEventAggregator>(new PrismMSDesktopEventAggregatorWrapper(msDesktopEventAggregator));
                //Container.RegisterType<IEventAggregator, MSDesktopEventAggregatorWrapper>(
                //    new ContainerControlledLifetimeManager());
            }

            _container.RegisterType<ICommunicator, Communicator>(new ContainerControlledLifetimeManager());
            //IPCCompositePresentationEventContainerHolder.Container = Container;

            _container.RegisterType<IEventRegistrator, InterProcessMessenger>(new ContainerControlledLifetimeManager());
            

            _container.RegisterType<IMessageTypeRegistrator, MessageTypeRegistrator>(
                new ContainerControlledLifetimeManager());
            _container.RegisterType<V2VConfigModule>(new ContainerControlledLifetimeManager());
        }

        public IModuleLoadInfo GetModuleLoadInfo(object moduleInfo)
        {
            if (moduleInfo is ModuleInfo)
            {
                return new ModuleLoadInfo(((ModuleInfo)moduleInfo).ModuleType)
                {
                    DependsOnModuleNames = ((ModuleInfo)moduleInfo).DependsOn,
                    ModuleName = ((ModuleInfo)moduleInfo).ModuleName
                };
            }
            throw new ApplicationException("Not a module Info");
        }


        /// <summary>
        /// Initializes the modules. May be overwritten in a derived class to use a custom Modules Catalog
        /// </summary>
        public void InitializeModules()
        {
            IModuleManager manager;
            var catalog = _container.Resolve<IModuleCatalog>();
            try
            {
                manager = _container.Resolve<IModuleManager>();
            }
            catch (ResolutionFailedException ex)
            {
                if (ex.Message.Contains("IModuleCatalog"))
                {
                    throw new InvalidOperationException("ModuleCatalog is not initialized.");
                }

                throw;
            }

            manager.Run();
        }

        public void ThrowModuleInitializationExceptionIfneeded()
        {
            MSDesktopPrismModuleInitializer moduleInitializer =
               _container.Resolve<IModuleInitializer>() as MSDesktopPrismModuleInitializer;
            if (moduleInitializer != null) moduleInitializer.ThrowExceptionIfNeeded();
        }

       

        /// <summary>
        /// Registers a type in the container only if that type was not already registered.
        /// </summary>
        /// <param name="fromType">The interface type to register.</param>
        /// <param name="toType">The type implementing the interface.</param>
        /// <param name="registerAsSingleton">Registers the type as a singleton.</param>
        protected void RegisterTypeIfMissing(Type fromType, Type toType, bool registerAsSingleton)
        {

            if (!_container.IsTypeRegistered(fromType))
            {
                if (registerAsSingleton)
                {
                    _container.RegisterType(fromType, toType, new ContainerControlledLifetimeManager());
                }
                else
                {
                    _container.RegisterType(fromType, toType);
                }
            }
        }
         
    }
}
