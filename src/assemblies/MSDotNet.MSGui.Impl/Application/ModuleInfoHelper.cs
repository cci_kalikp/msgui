﻿using System;
using System.Collections.Generic;
using System.Linq;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MorganStanley.MSDotNet.MSGui.Impl.Application
{
    internal static class ModuleInfoHelper
    {
        private static readonly IDictionary<Type, IModuleInfoProvider> Providers =
            new Dictionary<Type, IModuleInfoProvider>();

        public static Type GetModuleType(Type wrapperType)
        {
            IModuleInfoProvider provider;
            if (!Providers.TryGetValue(wrapperType, out provider))
            {
                var attr = wrapperType
                               .GetCustomAttributes(typeof (ModuleInfoProviderAttribute), true)
                               .FirstOrDefault()
                           as ModuleInfoProviderAttribute;

                if (attr == null || attr.Provider == null)
                {
                    return wrapperType;
                }

                provider = Activator.CreateInstance(attr.Provider) as IModuleInfoProvider;
                if (provider == null)
                {
                    return wrapperType;
                }

                Providers[wrapperType] = provider;
            }

            return provider.GetModuleType(wrapperType);
        }
    }
}
