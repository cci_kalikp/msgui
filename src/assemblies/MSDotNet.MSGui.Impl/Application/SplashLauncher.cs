﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////
// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Application/SplashLauncher.cs#26 $
// $Change: 878883 $
// $DateTime: 2014/04/29 04:57:51 $
// $Author: milosp $

using System;
using System.Threading;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;
using Point = System.Windows.Point;
using System.Diagnostics;
using MorganStanley.MSDotNet.My;

namespace MorganStanley.MSDotNet.MSGui.Impl.Application
{
  internal static class SplashLauncher<T> where T : Window, new()
  {
    #region Private static fields

    private static readonly IMSLogger m_logger = MSLoggerFactory.CreateLogger("MorganStanley.MSDotNet.MSGui.Impl.Application.SplashLauncher<" + typeof(T).FullName + ">");
    private static readonly object m_lock = new object();
    private static volatile T m_splashWindow;
    private static Thread m_splashThread;
    private static volatile bool m_closeHasBeenCalled;
    private static string m_backgroundImagePath;
    private static bool m_isApplicationNameShown = true;
    private static bool? m_isBitmapTransparent;
    private static Color m_versionColor = Colors.Black;
    private static Color m_nameColor = Color.FromArgb(0xff, 0x1f, 0x1f, 0x1f);
    private static Color m_statusColor = Colors.Black;
    private static Point m_statusPosition;
    private static Point m_namePosition;
    private static Point m_versionPosition;
    private static FontFamily m_statusFontFamily;
    private static FontFamily m_nameFontFamily;
    private static bool m_autosize;
    private static bool m_topMost;
    private static Point m_xButtonPosition;
  	private static Size m_xButtonSize;
    private static bool m_isXButtonShown = true;
    private static volatile bool m_isWindowCreationComplete;

    #endregion

    private static void Log(string str_)
    {
      m_logger.Info("Thread #" + Thread.CurrentThread.ManagedThreadId
        + " Dispatcher " + Dispatcher.CurrentDispatcher.GetHashCode()
        + " m_closeHasBeenCalled=" + m_closeHasBeenCalled
        + " " + str_);
    }

    /// <summary>
    /// Show the SplashForm
    /// </summary>
    public static void Show(string titleText_, string version_)
    {
      Log("SplashLauncher.Show(titleText_=\"" + titleText_ + "\", version_=\"" + version_ + "\")");

      if (m_splashThread != null)
        return;

      Log("SplashLauncher.Show() entering lock");
      lock (m_lock)
      {
        Log("SplashLauncher.Show() in lock, initing m_splashThread");
        m_splashThread = new Thread(delegate()
                                        {
                                          Log("SplashLauncher.Show().delegate started");
                                          if (!m_closeHasBeenCalled)
                                          {
                                            Dispatcher.CurrentDispatcher.BeginInvoke((Action)(() =>
                                              {
                                                Log("SplashLauncher.Show().delegate new T");
                                                m_splashWindow = new T { Title = titleText_ };
                                                lock (m_lock)
                                                {
                                                  Log("SplashLauncher.Show().delegate lock");
                                                  var splashWindow = m_splashWindow as SplashWindow;
                                                  if (splashWindow != null)
                                                  {
                                                    Log("SplashLauncher.Show().delegate init, VersionText=" + version_);
                                                    splashWindow.VersionText = version_;
                                                    Log("SplashLauncher.Show().delegate init, BackgroundImage=" + m_backgroundImagePath);
                                                    splashWindow.BackgroundImage = m_backgroundImagePath;
                                                    m_backgroundImagePath = null;
                                                    Log("SplashLauncher.Show().delegate init, IsApplicationNameShown=" + m_isApplicationNameShown);
                                                    splashWindow.IsApplicationNameShown = m_isApplicationNameShown;
                                                    Log("SplashLauncher.Show().delegate init, IsBitmapTransparent=" + (m_isBitmapTransparent != false));
                                                    splashWindow.IsBitmapTransparent = m_isBitmapTransparent != false;
                                                    Log("SplashLauncher.Show().delegate init, VersionColor=" + m_versionColor);
                                                    splashWindow.VersionColor = new SolidColorBrush(m_versionColor);
                                                    Log("SplashLauncher.Show().delegate init, NameColor=" + m_nameColor);
                                                    splashWindow.NameColor = new SolidColorBrush(m_nameColor);
                                                    Log("SplashLauncher.Show().delegate init, StatusColor=" + m_statusColor);
                                                    splashWindow.StatusColor = new SolidColorBrush(m_statusColor);
                                                    Log("SplashLauncher.Show().delegate init, StatusFontFamily=" + m_statusFontFamily);
                                                    splashWindow.StatusFontFamily = m_statusFontFamily;
                                                    m_statusFontFamily = null;
                                                    Log("SplashLauncher.Show().delegate init, NameFontFamily=" + m_nameFontFamily);
                                                    splashWindow.NameFontFamily = m_nameFontFamily;
                                                    m_nameFontFamily = null;
                                                    Log("SplashLauncher.Show().delegate init, StatusPosition=" + m_statusPosition);
                                                    splashWindow.StatusPosition = m_statusPosition;
                                                    Log("SplashLauncher.Show().delegate init, VersionPosition=" + m_versionPosition);
                                                    splashWindow.VersionPosition = m_versionPosition;
                                                    Log("SplashLauncher.Show().delegate init, NamePosition=" + m_namePosition);
                                                    splashWindow.NamePosition = m_namePosition;
                                                    Log("SplashLauncher.Show().delegate init, IsAutosize=" + m_autosize);
                                                    splashWindow.IsAutosize = m_autosize;
                                                    Log("SplashLauncher.Show().delegate init, IsXButtonShown=" + m_isXButtonShown);
                                                    splashWindow.IsXButtonShown = m_isXButtonShown;
                                                    Log("SplashLauncher.Show().delegate init, XButtonPosition=" + m_xButtonPosition);
                                                    splashWindow.XButtonPosition = m_xButtonPosition;
                                                    Log("SplashLauncher.Show().delegate init, XButtonSize=" + m_xButtonSize);
                                                    splashWindow.XButtonSize = m_xButtonSize;
                                                    Log("SplashLauncher.Show().delegate init, TopMost=" + m_topMost);
                                                    splashWindow.IsTopMost = m_topMost;
                                                    Log("SplashLauncher.Show().delegate init end");
                                                  }
                                                  else if (m_splashWindow is ModernSplashWindow)
                                                  {
                                                      var splash = m_splashWindow as ModernSplashWindow;
                                                      splash.MSDesktopVersionText = "MSDesktop " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
                                                      splash.VersionText = version_;
                                                      splash.IsTopMost = IsTopMost;
                                                  }
                                                  Log("SplashLauncher.Show().delegate lock end");
                                                }

                                                Log("SplashLauncher.Show() check InitialWindow=" + InitialSplashWindowFrameworkWrapper.InitialWindow);
                                                if (InitialSplashWindowFrameworkWrapper.InitialWindow != null)
                                                {
                                                  System.Drawing.Point p = InitialSplashWindowFrameworkWrapper.InitialWindow.GetLocation();
                                                  Log("SplashLauncher.Show() InitialWindow.GetLocation()=" + p);
                                                  if (p != new System.Drawing.Point())
                                                  {
                                                    Log("SplashLauncher.Show() set splashwindow pos");
                                                    m_splashWindow.Top = p.Y;
                                                    m_splashWindow.Left = p.X;
                                                  }
                                                }

                                                Log("SplashLauncher.Show() m_splashWindow.Show()");
                                                m_splashWindow.Show();

                                                Log("SplashLauncher.Show() check InitialWindow=" + InitialSplashWindowFrameworkWrapper.InitialWindow);
                                                if (InitialSplashWindowFrameworkWrapper.InitialWindow != null)
                                                {
                                                  System.Drawing.Point p = InitialSplashWindowFrameworkWrapper.InitialWindow.GetLocation();
                                                  Log("SplashLauncher.Show() InitialWindow.GetLocation()=" + p);
                                                  if (p != new System.Drawing.Point())
                                                  {
                                                    Log("SplashLauncher.Show() set splashwindow pos");
                                                    m_splashWindow.Top = p.Y;
                                                    m_splashWindow.Left = p.X;
                                                  }
                                                }
                                                if (InitialSplashWindowFrameworkWrapper.InitialWindow != null)
                                                {
                                                  Log("SplashLauncher.Show() InitialSplashWindowFrameworkWrapper.InitialWindow.Stop()");
                                                  InitialSplashWindowFrameworkWrapper.InitialWindow.Stop();
                                                }
                                                m_isWindowCreationComplete = true;
                                                Log("SplashLauncher.Show() window creation complete");
                                              }), DispatcherPriority.Send);

                                            // Cannot launch splash screen in normal app thread, as the window is now owned by a 
                                            // different thread to the Main UI.
                                            // Can though create a separate Dispatcher within this thread, and run that - which will
                                            // start a new message pump just for this window.
                                            //Application.Current.Run(_splashWindow);
                                            Log("SplashLauncher.Show() Dispatcher.Run()");
																						//try
																						//{
                                            Dispatcher.Run();
																					//}
																					//  catch (Exception exc)
																					//  {
																					//    Log(string.Format("Dispatcher run failed. {0}", exc));
																					//  }
                                          }
                                          Log("SplashLauncher.Show() end");
                                        })
        {
          IsBackground = true
        };
        Log(" m_splashThread=" + (m_splashThread != null ? m_splashThread.ManagedThreadId.ToString() : "null") + " SplashLauncher.Show()");

        if (!m_closeHasBeenCalled)
        {
          Log(" m_splashThread=" + (m_splashThread != null ? m_splashThread.ManagedThreadId.ToString() : "null") + " SplashLauncher.Show() apartment state");
          m_splashThread.SetApartmentState(ApartmentState.STA);
        }

        if (!m_closeHasBeenCalled)
        {
          Log(" m_splashThread=" + (m_splashThread != null ? m_splashThread.ManagedThreadId.ToString() : "null") + " SplashLauncher.Show() splashThread start");
          m_splashThread.Start();
        }
      }
      Log("SplashLauncher.Show() end");
    }

    #region Properties

    public static T SplashWindowInstance
    {
      get { return m_splashWindow; }
    }

    public static string BackgroundImage
    {
      get
      {
        lock (m_lock)
        {
          var window = m_splashWindow as SplashWindow;
          return window != null ? window.BackgroundImage : m_backgroundImagePath;
        }
      }

      set
      {
        lock (m_lock)
        {
          var window = m_splashWindow as SplashWindow;
          if (window != null)
          {
            window.BackgroundImage = value;
          }
          else
          {
            m_backgroundImagePath = value;
          }
        }
      }
    }

    public static bool IsApplicationNameShown
    {
      get
      {
        lock (m_lock)
        {
          var window = m_splashWindow as SplashWindow;
          return window != null ? window.IsApplicationNameShown : m_isApplicationNameShown;
        }
      }
      set
      {
        lock (m_lock)
        {
          var window = m_splashWindow as SplashWindow;
          if (window != null)
          {
            window.IsApplicationNameShown = value;
          }
          else
          {
            m_isApplicationNameShown = value;
          }
        }
      }
    }

    public static bool IsAutosize
    {
      get
      {
        lock (m_lock)
        {
          var window = m_splashWindow as SplashWindow;
          return window != null ? window.IsAutosize : m_autosize;
        }
      }
      set
      {
        lock (m_lock)
        {
          var window = m_splashWindow as SplashWindow;
          if (window != null)
          {
            window.IsAutosize = value;
          }
          else
          {
            m_autosize = value;
          }
        }
      }
    }

    public static bool IsTopMost
    {
        get
        {
            lock (m_lock)
            {
                var window = m_splashWindow as SplashWindow;
                return window != null ? window.IsTopMost : m_topMost;
            }
        }
        set
        {
            lock (m_lock)
            {
                var window = m_splashWindow as SplashWindow;
                if (window != null)
                {
                    window.IsTopMost = value;
                }
                else
                {
                    m_topMost = value;
                }
            }
        }
    }

    public static bool IsBitmapTransparent
    {
      get
      {
        lock (m_lock)
        {
          var window = m_splashWindow as SplashWindow;
            return window != null ? window.IsBitmapTransparent : (m_isBitmapTransparent == true);
        }
      }
      set
      {
        lock (m_lock)
        {
          var window = m_splashWindow as SplashWindow;
          if (window != null)
          {
            window.IsBitmapTransparent = value;
          }
          else
          {
            m_isBitmapTransparent = value;
          }
        }
      }
    }

    public static Color VersionColor
    {
      get
      {
        lock (m_lock)
        {
          var window = m_splashWindow as SplashWindow;
          return window != null ? window.VersionColor.Color : m_versionColor;
        }
      }
      set
      {
        lock (m_lock)
        {
          var window = m_splashWindow as SplashWindow;
          if (window != null)
          {
            window.VersionColor = new SolidColorBrush(value);
          }
          else
          {
            m_versionColor = value;
          }
        }
      }
    }

    public static Color NameColor
    {
      get
      {
        lock (m_lock)
        {
          var window = m_splashWindow as SplashWindow;
          return window != null ? window.NameColor.Color : m_nameColor;
        }
      }
      set
      {
        lock (m_lock)
        {
          var window = m_splashWindow as SplashWindow;
          if (window != null)
          {
            window.NameColor = new SolidColorBrush(value);
          }
          else
          {
            m_nameColor = value;
          }
        }
      }
    }

    public static Color StatusColor
    {
      get
      {
        lock (m_lock)
        {
          var window = m_splashWindow as SplashWindow;
          return window != null ? window.StatusColor.Color : m_statusColor;
        }
      }
      set
      {
        lock (m_lock)
        {
          var window = m_splashWindow as SplashWindow;
          if (window != null)
          {
            window.StatusColor = new SolidColorBrush(value);
          }
          else
          {
            m_statusColor = value;
          }
        }
      }
    }

    public static Point StatusPosition
    {
      get
      {
        lock (m_lock)
        {
          var window = m_splashWindow as SplashWindow;
          return window != null ? window.StatusPosition : m_statusPosition;
        }
      }
      set
      {
        lock (m_lock)
        {
          var window = m_splashWindow as SplashWindow;
          if (window != null)
          {
            window.StatusPosition = value;
          }
          else
          {
            m_statusPosition = value;
          }
        }
      }
    }

    public static Point NamePosition
    {
      get
      {
        lock (m_lock)
        {
          var window = m_splashWindow as SplashWindow;
          return window != null ? window.NamePosition : m_namePosition;
        }
      }
      set
      {
        lock (m_lock)
        {
          var window = m_splashWindow as SplashWindow;
          if (window != null)
          {
            window.NamePosition = value;
          }
          else
          {
            m_namePosition = value;
          }
        }
      }
    }

    public static Point VersionPosition
    {
      get
      {
        lock (m_lock)
        {
          var window = m_splashWindow as SplashWindow;
          return window != null ? window.VersionPosition : m_versionPosition;
        }
      }
      set
      {
        lock (m_lock)
        {
          var window = m_splashWindow as SplashWindow;
          if (window != null)
          {
            window.VersionPosition = value;
          }
          else
          {
            m_versionPosition = value;
          }
        }
      }
    }

    public static FontFamily StatusFontFamily
    {
      get
      {
        lock (m_lock)
        {
          var window = m_splashWindow as SplashWindow;
          return window != null ? window.StatusFontFamily : m_statusFontFamily;
        }
      }
      set
      {
        lock (m_lock)
        {
          var window = m_splashWindow as SplashWindow;
          if (window != null)
          {
            window.StatusFontFamily = value;
          }
          else
          {
            m_statusFontFamily = value;
          }
        }
      }
    }

    public static FontFamily NameFontFamily
    {
      get
      {
        lock (m_lock)
        {
          var window = m_splashWindow as SplashWindow;
          return window != null ? window.NameFontFamily : m_nameFontFamily;
        }
      }
      set
      {
        lock (m_lock)
        {
          var window = m_splashWindow as SplashWindow;
          if (window != null)
          {
            window.NameFontFamily = value;
          }
          else
          {
            m_nameFontFamily = value;
          }
        }
      }
    }

    public static bool IsXButtonShown
    {
      get
      {
        lock (m_lock)
        {
          var window = m_splashWindow as SplashWindow;
          return window != null ? window.IsXButtonShown : m_isXButtonShown;
        }
      }
      set
      {
        lock (m_lock)
        {
          var window = m_splashWindow as SplashWindow;
          if (window != null)
          {
            window.IsXButtonShown = value;
          }
          else
          {
            m_isXButtonShown = value;
          }
        }
      }
    }

    public static Point XButtonPosition
    {
      get
      {
        lock (m_lock)
        {
          var window = m_splashWindow as SplashWindow;
          return window != null ? window.XButtonPosition : m_xButtonPosition;
        }
      }
      set
      {
        lock (m_lock)
        {
          var window = m_splashWindow as SplashWindow;
          if (window != null)
          {
            window.XButtonPosition = value;
          }
          else
          {
            m_xButtonPosition = value;
          }
        }
      }
    }

		public static Size XButtonSize
		{
			get
			{
				lock (m_lock)
				{
					var window = m_splashWindow as SplashWindow;
					return window != null ? window.XButtonSize : m_xButtonSize;
				}
			}
			set
			{
				lock (m_lock)
				{
					var window = m_splashWindow as SplashWindow;
					if (window != null)
					{
						window.XButtonSize = value;
					}
					else
					{
						m_xButtonSize = value;
					}
				}
			}
		}

    #endregion

    /// <summary>
    /// Close the Splash Window.
    /// </summary>
    public static void Close()
    {
      Log("m_isWindowCreationComplete: " + m_isWindowCreationComplete);
      if (m_isWindowCreationComplete)
      {
        Log("SplashLauncher.Close()");
        m_closeHasBeenCalled = true;
        if (m_splashThread == null || m_splashWindow == null) return;

        Log("SplashLauncher.Close() m_splashWindow.Dispatcher.BeginInvokeShutdown(DispatcherPriority.Normal)");
        m_splashWindow.Dispatcher.BeginInvokeShutdown(DispatcherPriority.Normal);
        m_splashWindow = null;
        //_splashThread.Abort();
        m_splashThread = null;
        Log("SplashLauncher.Close() end");
      }
      else
      {
        m_logger.Critical("The splash window cannot be closed before it has fully been created");
      }
    }


  }
}
