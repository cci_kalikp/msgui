﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Microsoft.Practices.Unity;
using MorganStanley.IED.Concord.Configuration;
using MorganStanley.MSDotNet.MSGui.Core;

namespace MorganStanley.MSDotNet.MSGui.Impl.Application
{
    internal abstract class AbstractMSDesktopEnvironment : IMSDesktopEnvironment
    {
        protected Environ _environment;
        protected EnvironmentRegion _region;
        protected string _user;

        public Environ Environment
        {
            get { return _environment; }
            protected set { _environment = value; }
        }

        public EnvironmentRegion Region
        {
            get { return _region; }
            protected set { _region = value; }
        }

        public string User
        {
            get { return _user; }
            protected set { _user = value; }
        }

        public string Subenvironment { get; set; }

        internal AbstractMSDesktopEnvironment()
        {
            Environment = Environ.N_A;
            Region = EnvironmentRegion.N_A;
            User = string.Empty;
        }
    }

    internal class ConcordConfigBasedMSDesktopEnvironment :AbstractMSDesktopEnvironment, IMSDesktopEnvironment
    {


        internal ConcordConfigBasedMSDesktopEnvironment()
        {
            var configurator = (Configurator)ConfigurationManager.GetConfig("Environment");

            string environment = configurator.GetValue("Environment", null);
            if (string.IsNullOrEmpty(environment))
            {
                throw new InvalidOperationException("The Environment config must define an 'Environment' Node");
            }
            if (!Enum.TryParse(environment, true, out _environment))
            {
                throw new InvalidOperationException(string.Format("The Environment config must define an 'Environment' Node that is convertable to the 'Environment' enum.  Value is'{0}'.", environment));
            }

            string region = configurator.GetValue("Region", null);
            if (string.IsNullOrEmpty(region))
            {
                throw new InvalidOperationException("The Environment config must define an 'Region' Node");
            }
            if (!Enum.TryParse(region, true, out _region))
            {
                throw new InvalidOperationException(string.Format("The Environment config must define a 'Region' Node that is convertible to the 'Environment' enum.  Value is'{0}'.", region));
            }
        }
    }

    internal class ConcordProfileBasedMSDesktopEnvironment : AbstractMSDesktopEnvironment, IMSDesktopEnvironment
    {

        internal ConcordProfileBasedMSDesktopEnvironment(IUnityContainer container)
        {
        }
    }
}
