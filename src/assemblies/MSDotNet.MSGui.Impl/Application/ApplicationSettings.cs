﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Application/ApplicationSettings.cs#13 $
// $Change: 875617 $
// $DateTime: 2014/04/08 21:26:23 $
// $Author: caijin $

using System;
using System.ComponentModel;
using System.Xml.Linq;
using System.Xml.XPath;
using MorganStanley.MSDotNet.MSGui.Core;
//using MorganStanley.MSDotNet.MSGui.Impl.Options;
using MorganStanley.MSDotNet.MSGui.Core.ViewModel;
using MorganStanley.MSDotNet.My;
//using Xceed.Wpf.Toolkit.PropertyGrid.Attributes;

namespace MorganStanley.MSDotNet.MSGui.Impl.Application
{
    public class ApplicationSettings : ViewModelBase, IOptionViewViewModel
    {
        internal static IMSLogger Logger = MSLoggerFactory.CreateLogger<ApplicationSettings>();

        private bool m_saveOnExit = true;
        [DisplayName("Save on exit")]
        //[Category("Exit dialog settings")]
        //[BindableEnablement("IsShutdownTimeEnabled")]
        //[PropertyOrder(1)]
        public bool SaveOnExit
        {
            get
            {
                return m_saveOnExit;
            }
            set
            {
                m_saveOnExit = value;
                OnPropertyChanged("SaveOnExit");
            }
        }

        private bool m_showExitDialog = true;
        [DisplayName("Show exit dialog")]
        //[Category("Exit dialog settings")]
        //[PropertyOrder(0)]
        public bool ShowExitDialog
        {
            get
            {
                return m_showExitDialog;
            }
            set
            {
                m_showExitDialog = value;
                OnPropertyChanged("ShowExitDialog");
            }
        }

        private TimeSpan m_shutdownTime;
        [DisplayName("Timed shutdown")]
        //[Category("Timed shutdown")]
        //[PropertyOrder(3)]
        //[BindableEnablement("IsShutdownTimeEnabled")]
        public TimeSpan ShutdownTime
        {
            get
            {
                return m_shutdownTime;
            }
            set
            {
                m_shutdownTime = value;
                OnPropertyChanged("ShutdownTime");
            }
        }

        private bool m_isShutdownTimeEnabled;
        [DisplayName("Enable timed shutdown")]
        //[Category("Timed shutdown")]
        //[PropertyOrder(2)]
        public bool IsShutdownTimeEnabled
        {
            get
            {
                return m_isShutdownTimeEnabled;
            }
            set
            {
                m_isShutdownTimeEnabled = value;
                OnPropertyChanged("IsShutdownTimeEnabled");
            }
        }

        private bool m_isShutdownTimeUTC;
        [DisplayName("Use UTC time")]
        //[Category("Timed shutdown")]
        //[PropertyOrder(4)]
        //[BindableEnablement("IsShutdownTimeEnabled")]
        public bool IsShutdownTimeUTC
        {
            get
            {
                return m_isShutdownTimeUTC;
            }
            set
            {
                m_isShutdownTimeUTC = value;
                OnPropertyChanged("IsShutdownTimeUTC");
            }
        }

        private readonly object m_isLoadingOrSaving = new object();

        public void LoadState(XDocument state)
        {
            try
            {
                lock (m_isLoadingOrSaving)
                {

                    string saveString = (string)state.XPathSelectElement("/ApplicationSettings/SaveOnExit");
                    if (saveString != null)
                    {
                        switch (saveString.ToLower())
                        {
                            case "true":
                                SaveOnExit = true;
                                break;
                            case "false":
                                SaveOnExit = false;
                                break;
                        }
                    }
                    string showDialog = (string)state.XPathSelectElement("/ApplicationSettings/ShowExitDialog");
                    if (showDialog != null)
                    {
                        switch (showDialog.ToLower())
                        {
                            case "true":
                                ShowExitDialog = true;
                                break;
                            case "false":
                                ShowExitDialog = false;
                                break;
                        }
                    }

                    var shutdownTimeString = (string)state.XPathSelectElement("/ApplicationSettings/ShutdownTime");
                    TimeSpan shutdownTime;
                    if (TimeSpan.TryParse(shutdownTimeString, out shutdownTime))
                    {
                        ShutdownTime = shutdownTime;
                    }

                    var isShutdownTimeEnabled = (bool?) state.XPathSelectElement("/ApplicationSettings/IsShutdownTimeEnabled");
                    IsShutdownTimeEnabled = isShutdownTimeEnabled.HasValue && isShutdownTimeEnabled.Value;

                    var shutdownTimeIsUTC = (bool?)state.XPathSelectElement("/ApplicationSettings/ShutdownTimeIsUTC");
                    IsShutdownTimeUTC = shutdownTimeIsUTC.HasValue && shutdownTimeIsUTC.Value;
                }
            }
            catch
            {
                Logger.Error("Failed to load application settings");
                return;
            }
        }

        public XDocument Persist()
        {
            try
            {
                lock (m_isLoadingOrSaving)
                {
                    var srcTree = new XDocument(
                      new XElement("ApplicationSettings",
                                   new XElement("SaveOnExit", SaveOnExit.ToString()),
                                   new XElement("ShowExitDialog", ShowExitDialog.ToString()),
                                   new XElement("ShutdownTime", ShutdownTime.ToString()),
                                   new XElement("IsShutdownTimeEnabled", IsShutdownTimeEnabled.ToString()),
                                   new XElement("ShutdownTimeIsUTC", IsShutdownTimeUTC.ToString())
                        )
                      );
                    return srcTree;
                }
            }
            catch
            {
                return null;
            }
        }
        
 
        public object Clone()
        {
            var copy = new ApplicationSettings();
            copy.AcceptChanges(this);
            return copy;
        }

        public void AcceptChanges(IOptionViewViewModel newViewModel_)
        {
            var vm = newViewModel_ as ApplicationSettings;
            if (vm == null) return;
            IsShutdownTimeEnabled = vm.IsShutdownTimeEnabled;
            IsShutdownTimeUTC = vm.IsShutdownTimeUTC;
            ShutdownTime = vm.ShutdownTime;
            SaveOnExit = vm.SaveOnExit;
            ShowExitDialog = vm.ShowExitDialog;
        }

        public bool Validate(out string errorString_, out string invalidControlName_, out string invalidPropertyName_)
        {
            errorString_ = invalidControlName_ = invalidPropertyName_ = null;
            if (IsShutdownTimeEnabled && ShutdownTime.TotalSeconds <= 0d)
            {
                errorString_ = "Shut down time must be specified if timed shutdown is enabled.";
                invalidPropertyName_ = "ShutdownTime";
                return false;
            }
            return true;
        }

        public bool HasCustomTemplate
        {
            get { return true; }
        }
    }
}
