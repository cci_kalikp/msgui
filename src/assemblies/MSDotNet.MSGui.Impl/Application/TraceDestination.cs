﻿using System;
using System.Diagnostics;
using MorganStanley.MSDotNet.MSLog;

namespace MorganStanley.MSDotNet.MSGui.Impl.Application
{

  #region TraceDestination Class

  public class TraceDestination : MSLogDestination
  {
    #region Base Class Overrides

    #region Send

    public override void Send(MSLogMessage message_)
    {
      string category = String.Format("{0}.{1}", message_.Layer.Meta, message_.Layer.Proj);
      if (category.Trim() != ".")
      {
        Trace.WriteLine(
          String.Format("({0}{1}) [{2}] {3} [{4}]", message_.ClassName,
                        (message_.Method == null || message_.Method.Trim().Length == 0) ? "" : "." + message_.Method,
                        message_.Priority, message_.Message, message_.ThreadId), category);
      }
      else
      {
        Trace.WriteLine(String.Format("({0}{1}) [{2}] {3} [{4}]", message_.ClassName,
                                      (message_.Method == null || message_.Method.Trim().Length == 0)
                                        ? ""
                                        : "." + message_.Method, message_.Priority, message_.Message, message_.ThreadId));
      }
    }

    #endregion Send

    #endregion Base Class Overrides
  }

  #endregion TraceDestination Class
}