﻿using System;
using System.Drawing;
using System.Reflection;
using MorganStanley.MSDotNet.My;

namespace MorganStanley.MSDotNet.MSGui.Impl.Application
{
    public class InitialSplashWindowFrameworkWrapper
    {
        private readonly object m_splash;
        internal static InitialSplashWindowFrameworkWrapper InitialWindow;
        private readonly MethodInfo m_stop;
        private readonly MethodInfo m_location;
        private PropertyInfo m_isHiddenProp;
        private static readonly IMSLogger m_logger = MSLoggerFactory.CreateLogger<InitialSplashWindowFrameworkWrapper>();

        /// <summary>
        /// Initializes a new instance of the <see cref="InitialSplashWindowFrameworkWrapper"/> class.
        /// </summary>
        /// <param name="fw_">The framework.</param>
        /// <param name="splashLoader_">The splash loader.</param>
        public InitialSplashWindowFrameworkWrapper(Framework fw_, object splashLoader_)
        {
            m_stop = splashLoader_.GetType().GetMethod("Stop");
            if (m_stop == null || m_stop.GetParameters().Length != 0)
            {
                throw new InvalidOperationException("Initial splash window must contain a public method 'Stop' which takes no parameters.");
            }
            m_location = splashLoader_.GetType().GetMethod("GetLocation");
            if (m_location == null ||
                m_stop.GetParameters().Length != 0 ||
                m_location.ReturnType != typeof(Point))
            {
                throw new InvalidOperationException("Initial splash window must contain a public method 'GetLocation' which takes no parameters and returns a System.Drawing.Point.");
            }

            m_isHiddenProp = splashLoader_.GetType().GetProperty("IsHidden", typeof(bool));
            if (m_isHiddenProp == null ||
                !m_isHiddenProp.CanRead ||
                !m_isHiddenProp.CanWrite)
            {
                m_logger.Warning("Initial splash window must contain a public property 'IsHidden' with type bool for it to be hidden");
            }

            m_splash = splashLoader_;
            InitialWindow = this;
        }

        public bool IsHidden
        {
            get { return m_isHiddenProp == null ? false : (bool) m_isHiddenProp.GetValue(m_splash, null); }
            set { if(m_isHiddenProp != null) m_isHiddenProp.SetValue(m_splash, value, null); }
        }

        public void Stop()
        {
            m_stop.Invoke(m_splash, new object[] { });
        }

        public Point GetLocation()
        {
            var res = m_location.Invoke(m_splash, new object[] { });

            if (res is Point)
            {
                return (Point)res;
            }

            return Point.Empty;
        }
    }
}