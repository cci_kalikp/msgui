﻿using System;

using Microsoft.Practices.Unity;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Core.ChromeManager;
using MorganStanley.MSDotNet.MSGui.Impl.Application.Composite;
using MorganStanley.MSDotNet.MSGui.Impl.Application.Prism;


namespace MorganStanley.MSDotNet.MSGui.Impl.Application
{
    enum ModuleFramework
    {
        UsePrism, UseComposite, UseBoth
    }

    internal static class ModuleFrameworkFactory
    {
        public static IModuleFrameworkHandler CurrentHandler { get; private set; }
        public static IModuleFrameworkHandler CreateCompositeHandler(IUnityContainer unityContainer,  bool useReflection,  string  configFile)
        {
            CurrentHandler = new CompositeHandler(unityContainer, useReflection, configFile);
            return CurrentHandler;
        }

        public static IModuleFrameworkHandler CreatePrismHandler(IUnityContainer unityContainer, bool useReflection, string configFile)
        {
            CurrentHandler = new PrismHandler(unityContainer, useReflection, configFile);
            return CurrentHandler;
        }

    }

    internal interface IModuleFrameworkHandler
    {
        void RegisterFrameworkExceptionTypes();

        void AddModule(Type moduleType);
        void AddModule(Type moduleType, string moduleName);
        void AddPreInitializer(IPreInitializer preInitializer);

        /// <summary>
        /// </summary>
        /// <param name="moduleInfo"> of type ModuleInfo  (prism or composite)</param>
        void AddModule(object moduleInfo);  

        bool HasModule(Type moduleType);

        Exception GetModuleInitializeException(string moduleType);

        void ThrowModuleInitializationExceptionIfneeded();
        void InvokeAllModulesLoaded();
        void InvokePreInitializers();

        /// <summary>
        /// Configures the <see cref="IUnityContainer"/>. May be overwritten in a derived class to add specific
        /// type mappings required by the application.
        /// </summary>
        void ConfigureContainer(bool useDefaultConfiguration);
         
        /// <summary>
        /// Initializes the modules. May be overwritten in a derived class to use a custom Modules Catalog
        /// </summary>
        void InitializeModules();
         
        void InitializeEarlyFrameworkModules();

        void RegisterTypes();

        IModuleLoadInfo GetModuleLoadInfo(object moduleInfo);
        event EventHandler<EventArgs<string>> BeforeModuleInitialized;
        event EventHandler<EventArgs<string>> AfterModuleInitialized;

        bool DisableEventAggregatorBridge { get; set; }

        void InjectRootRegionManger(IWindowViewContainer view);
    }
}
