﻿using MorganStanley.MSDotNet.MSGui.Core;

namespace MorganStanley.MSDotNet.MSGui.Impl.Application
{
    
    abstract class ModuleWrapper
    {
        private readonly IMSDesktopModule _inner;
        protected ModuleWrapper(IMSDesktopModule desktopModule)
        {
            _inner = desktopModule;
        }

        public IMSDesktopModule ActualModule {get { return _inner; }}

        public void Initialize()
        {
            _inner.Initialize();
        }
    }

    class CompositeModuleWrapper : ModuleWrapper, Microsoft.Practices.Composite.Modularity.IModule
    {
        public CompositeModuleWrapper(IMSDesktopModule desktopModule):base(desktopModule)
        {
        }

       
    }

    class PrismModuleWrapper : ModuleWrapper, Microsoft.Practices.Prism.Modularity.IModule 
    {
        public PrismModuleWrapper(IMSDesktopModule desktopModule)
            : base(desktopModule)
        {

        }
    }
}
