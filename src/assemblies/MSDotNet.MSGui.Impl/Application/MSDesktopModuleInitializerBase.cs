﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Text;
using Microsoft.Practices.ServiceLocation;
using MorganStanley.MSDotNet.MSGui.Core;
//using MorganStanley.MSDotNet.MSGui.Impl.Modularity.Prism;
using MorganStanley.MSDotNet.MSGui.Core.Persistence.DelayedLoading; 
using MorganStanley.MSDotNet.MSGui.Impl.Modularity;
using MorganStanley.MSDotNet.MSGui.Impl.Modularity.Prism;

namespace MorganStanley.MSDotNet.MSGui.Impl.Application
{
    internal abstract class MSDesktopModuleInitializerBase<TModuleInfo, TIModule>
    {
        protected readonly IServiceLocator ServiceLocator;
        protected readonly IModuleLoadInfos ModuleLoadInfos;
        private readonly List<Exception> moduleLoadExceptions = new List<Exception>();


        protected MSDesktopModuleInitializerBase(IServiceLocator serviceLocator_, IModuleLoadInfos moduleLoadInfos_)
        {
            if (serviceLocator_ == null)
            {
                throw new ArgumentNullException("serviceLocator_");
            }

            ServiceLocator = serviceLocator_;
            ModuleLoadInfos = moduleLoadInfos_; 
        }

        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes",
     Justification =
         "Catches Exception to handle any exception thrown during the initialization process with the HandleModuleInitializationError method."
     )]
        
        public void Initialize(TModuleInfo moduleInfo_)
        {
            TIModule moduleInstance = default(TIModule);
            IModuleLoadInfo info = null;
            try
            {
                string typeName = GetModuleTypeName(moduleInfo_);
                OnBeforeModuleInitialized(typeName);
                info = ModuleLoadInfos.GetModuleLoadInfo(typeName);
                moduleInstance = CreateModule(typeName, info);
                var resolverModuleInfo = moduleInfo_ as IMSDesktopModuleInfo;
                if (resolverModuleInfo != null)
                {
                    ModuleWrapper wrapper = moduleInstance as ModuleWrapper;
                    if (wrapper != null)
                    {
                        DelayedProfileLoadingExtensionPoints.InvokeModuleInfoLoaded(new BeforeModuleInitializedEventArgs
                            {
                                ModuleName = GetModuleName(moduleInfo_),
                                Module = wrapper.ActualModule,
                                ExtraInformation = resolverModuleInfo.ExtraValues
                            });
                    }

                }
                InitializeModule(moduleInstance);
                OnAfterModuleInitialized(info != null ? info.FullTypeName : typeName);
            }
            catch (Exception ex)
            {
                 HandleModuleInitializationError(moduleInfo_, info, moduleInstance, ex);
            }
        }

        private void HandleModuleInitializationError(TModuleInfo moduleInfo_, IModuleLoadInfo info_, TIModule moduleInstance_,
                                                     Exception exception_)
        {
            string assemblyName = moduleInstance_ == null ? null : moduleInstance_.GetType().Assembly.FullName;
            string moduleName = GetModuleName(moduleInfo_);
            Exception moduleException = NomalizeModuleError(exception_, moduleName, assemblyName); 
            if (moduleException != null)
            {
                string message = null;
                string moduleType = GetModuleTypeName(moduleInfo_);
                if (info_ != null && info_.FullTypeName != moduleType)
                {
                    message = string.Format("Module init error, module {0} {1} {2} (with module {3} embedded)", assemblyName,
                                      moduleType, moduleName, info_.FullTypeName);

                }
                else
                {
                    message = string.Format("Module init error, module {0} {1} {2}", assemblyName, moduleType, moduleName);

                }

                LogException(moduleException, message);
                if (info_ != null)
                {
                    info_.Status = ModuleStatus.Exceptioned;
                    info_.Error = moduleException;
                }
                moduleLoadExceptions.Add(moduleException);
            }

        }

        internal event EventHandler<EventArgs<string>> BeforeModuleInitialized;

        internal event EventHandler<EventArgs<string>> AfterModuleInitialized;


        private void OnAfterModuleInitialized(string moduleType_)
        {
            EventHandler<EventArgs<string>> copy = AfterModuleInitialized;
            if (copy != null)
            {
                copy(this, new EventArgs<string>(moduleType_));
            }
        }

        private void OnBeforeModuleInitialized(string moduleType_)
        {
            EventHandler<EventArgs<string>> copy = BeforeModuleInitialized;
            if (copy != null)
            {
                copy(this, new EventArgs<string>(moduleType_));
            }
        }


        internal void ThrowExceptionIfNeeded()
        {
            if (moduleLoadExceptions.Count == 0) return;
            if (moduleLoadExceptions.Count == 1) throw moduleLoadExceptions[0];
            throw new AggregateException(moduleLoadExceptions);
        }



        protected virtual TIModule CreateModule(string typeName_, IModuleLoadInfo info_)
        {
            Type moduleType = Type.GetType(typeName_);
            if (moduleType == null)
            {
                throw new Exception(string.Format(CultureInfo.CurrentCulture,
                                                          "Unable to retrieve the module type {0} from the loaded assemblies.  You may need to specify a more fully-qualified type name.",
                                                          typeName_));
            }
            if (info_ != null)
            {
                var realModuleType = ModuleInfoHelper.GetModuleType(moduleType);
                if (realModuleType != moduleType)
                {
                    info_.FullTypeName = realModuleType.AssemblyQualifiedName;
                }
            }
            var module = ServiceLocator.GetInstance(moduleType);

            return NomalizeModule(module);
        }


        protected abstract string GetModuleTypeName(TModuleInfo moduleInfo_);
        protected abstract string GetModuleName(TModuleInfo moduleInfo_);

        protected abstract void InitializeModule(TIModule moduleInstance_);

        protected abstract Exception NomalizeModuleError(Exception exception_, string moduleName_, string assemblyName_);
        protected abstract void LogException(Exception exception_, string message_);
         
        protected abstract TIModule NomalizeModule(object module_);
    }
}
