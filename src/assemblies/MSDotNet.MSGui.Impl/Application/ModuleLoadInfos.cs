﻿using System;
using System.Collections.Generic;
using System.Linq;
using MorganStanley.MSDotNet.MSGui.Core;
namespace MorganStanley.MSDotNet.MSGui.Impl.Application
{
    internal class ModuleLoadInfos:IModuleLoadInfos
    {
        private List<IModuleLoadInfo> moduleInfos = new List<IModuleLoadInfo>();
        private int currentIndex = 0;
        public IModuleLoadInfo AddModuleLoadInfo(object moduleInfo_)
        {
            var info = ModuleFrameworkFactory.CurrentHandler.GetModuleLoadInfo(moduleInfo_);
            moduleInfos.Add(info); 
            return info;
        }


        public IModuleLoadInfo AddModuleLoadInfo(string moduleTypeName_, ModuleStatus status_ = ModuleStatus.NotLoaded)
        {
            ModuleLoadInfo info = new ModuleLoadInfo(moduleTypeName_); 
            info.Status = status_;
            moduleInfos.Add(info); 
            return info;      
        }


        public IModuleLoadInfo GetModuleLoadInfo(string moduleTypeName_)
        {
            return moduleInfos.FirstOrDefault(m_ => m_.FullTypeName == moduleTypeName_);
        }

        public void StartLoading(IModuleLoadInfo moduleLoadInfo_)
        {
            var moduleLoadInfo = (ModuleLoadInfo)moduleLoadInfo_;
            moduleLoadInfo.Status = ModuleStatus.NotLoaded;
            moduleLoadInfo.LoadStartTime = DateTime.UtcNow.Ticks;
            moduleLoadInfo.LoadOrder = currentIndex;
            currentIndex++;
        }

        public void EndLoading(IModuleLoadInfo moduleLoadInfo_)
        {
            var moduleLoadInfo = (ModuleLoadInfo)moduleLoadInfo_;
            if (moduleLoadInfo.Status != ModuleStatus.Exceptioned)
            {
                moduleLoadInfo.Status = ModuleStatus.Loaded;
                moduleLoadInfo.LoadingTime = DateTime.UtcNow.Ticks - moduleLoadInfo.LoadStartTime;          
            }
        }

        private bool moduleSorted = false;
        public List<IModuleLoadInfo> Infos
        {
            get
            {
                if (!moduleSorted)
                {
                    List<IModuleLoadInfo> sorted = new List<IModuleLoadInfo>();
                    List<IModuleLoadInfo> unsorted = new List<IModuleLoadInfo>();
                    foreach (var moduleLoadInfo in moduleInfos)
                    {
                        if (moduleLoadInfo.Status != ModuleStatus.Loaded &&
                            moduleLoadInfo.Status != ModuleStatus.NotLoaded && 
                            moduleLoadInfo.Status != ModuleStatus.Exceptioned)
                        {
                            sorted.Add(moduleLoadInfo);
                        }
                        else
                        {
                            unsorted.Add(moduleLoadInfo);
                        }
                    }
                    unsorted.Sort((x_, y_) => x_.LoadOrder - y_.LoadOrder);
                    sorted.AddRange(unsorted);
                    moduleInfos = sorted;
                    moduleSorted = true;
                }
                return moduleInfos;
            }
        }

        public bool HasErrorOnInit { get; internal set; }
    }
}