﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Application/Application.cs#28 $
// $Change: 898612 $
// $DateTime: 2014/09/28 23:36:02 $
// $Author: caijin $

using System;
using System.Collections.Generic;
using System.Reflection;
using System.Windows.Media;
using System.Windows.Threading;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Helpers;
using MorganStanley.MSDotNet.MSGui.Impl.ModernTheme;
using MorganStanley.MSDotNet.MSGui.Impl.Shell.Themes;

namespace MorganStanley.MSDotNet.MSGui.Impl.Application
{
    internal class Application : IMSDesktopThreadedApplication
    {
        private readonly string[] _args = Environment.GetCommandLineArgs();
        private readonly Dictionary<string, Theme> _themes = new Dictionary<string, Theme>();

        private Theme _currentTheme;

        public Application()
        {
            var assembly = EntryAssemblyHelper.GetEntryAssembly();
            var name = assembly != null
                                    ? assembly.GetName()
                                    : new AssemblyName("MSGuiApplication") {Version = new Version(1, 2, 3, 4)};
            Name = name.Name;
            Version = name.Version.ToString();
            Settings = new ApplicationSettings();
        }

        #region IApplication implementation

        public string Name { get; internal set; }
        public string Version { get; internal set; }
        public ImageSource Icon { get; internal set; }

        public string[] CommandLine
        {
            get { return _args; }
        }

        public virtual Dispatcher MainWindowDispatcher
        {
            get { return System.Windows.Application.Current.MainWindow.Dispatcher; }
        }

        public ApplicationSettings Settings { get; internal set; }

        public event EventHandler ApplicationLoaded;

        public void Quit()
        {
            System.Windows.Application.Current.Shutdown(0);
        }

        #endregion

        #region Themes

        public event EventHandler ThemeChanged;

        public Theme CurrentTheme
        {
            get { return _currentTheme; }
            private set
            {
                _currentTheme = value;
                InvokeThemeChanged();
            }
        }

        public string CurrentThemeName { get; private set; }

        public void SetTheme(string themeName, InitialSettings initialSettings)
        {
            if (!Themes.ContainsKey(themeName)) return;
            var theme = Themes[themeName];
            ModernThemeHelper.IsModernThemeEnabled = theme.IsModernTheme;
            ResourcesHelper.AddApplicationThemeResource(theme, initialSettings);
            CurrentThemeName = themeName;
            CurrentTheme = theme;
            
        }

        public Dictionary<string, Theme> Themes
        {
            get { return _themes; }
        }

        private void InvokeThemeChanged()
        {
            EventHandler handler = ThemeChanged;
            if (handler != null) handler(this, EventArgs.Empty);
        }

        #endregion

        internal void InvokeApplicationLoaded()
        {
            var copy = ApplicationLoaded;
            if (copy != null)
            {
                copy(this, EventArgs.Empty);
            }
        }

        internal void InvokeApplicationClosed()
        {
            var copy = ApplicationClosed;
            if (copy != null)
            {
                copy(this, EventArgs.Empty);
            }
        }

        public event EventHandler ApplicationClosed;

        internal void InvokeApplicationClosing()
        {
            var copy = ApplicationClosing;
            if (copy != null)
            {
                copy(this, EventArgs.Empty);
            }
        }

        internal event EventHandler ApplicationClosing;
         
        internal void InvokeApplicationClosingCancelled()
        {
            var copy = ApplicationClosingCancelled;
            if (copy != null)
            {
                copy(this, EventArgs.Empty);
            }
        }

        public event EventHandler ApplicationClosingCancelled;
    }
}
