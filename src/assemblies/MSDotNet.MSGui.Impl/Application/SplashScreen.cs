﻿/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2010 Morgan Stanley, Inc., All Rights Reserved
//
// Unpublished copyright. This material contains proprietary information
// that shall be used or copied only within Morgan Stanley,
// except with written permission of Morgan Stanley.
//
/////////////////////////////////////////////////////////////////////////////

// $Header: //eai/msdotnet/msgui/trunk/assemblies/MSDotNet.MSGui.Impl/Application/SplashScreen.cs#16 $
// $Change: 886978 $
// $DateTime: 2014/07/02 22:23:30 $
// $Author: caijin $

using System;
using System.Windows;
using MorganStanley.Desktop.GUI.Controls.TaskDialogInterop;
using MorganStanley.MSDotNet.MSGui.Core;
using MorganStanley.MSDotNet.MSGui.Impl.Persistence.LayoutLoadingScreen;

namespace MorganStanley.MSDotNet.MSGui.Impl.Application
{
  internal class SplashScreen : ISplashScreen, ILayoutLoadingScreen
  {
      static SplashScreen()
      {
          TaskDialog.FailedToDetermineOwner += 
              (sender, args) =>
                {
                    if (WindowInstance != null)
                    {
                        args.Owner = WindowInstance;
                    }
                };
      }

    public SplashScreen(IStatusBar statusBar_)
    {
      statusBar_.MainItem.StatusUpdated += MainItem_StatusUpdated;
      ModernSplashWindow.HideRequested += ModernSplashWindowOnHideRequested;
        ModernSplashWindow.CancelLayoutLoadRequested += ModernSplashWindowOnCancelLayoutLoadRequested;
    }

      private void ModernSplashWindowOnCancelLayoutLoadRequested(object sender_, EventArgs eventArgs_)
      {
          SetText("Cancelling layout load");
          OnCancelRequested();

      }

      private void ModernSplashWindowOnHideRequested(object sender, EventArgs eventArgs)
      {
          //SetText("Cancelling layout load");
          //OnCancelRequested();
      }

      /// <summary>
    /// Showing main status changes on Splash Screen
    /// </summary>
    /// <param name="sender_"></param>
    /// <param name="e_"></param>
    void MainItem_StatusUpdated(object sender_, StatusUpdatedEventArgs e_)
    {
      if (e_.Level == StatusLevel.Information)
      {
        SetText(e_.Text);
      }
    }

    #region ISplashScreen Members

    public void SetText(string text_)
    {
 
        if (SplashLauncher<SplashWindow>.SplashWindowInstance != null)
        { 
            SplashLauncher<SplashWindow>.SplashWindowInstance.Dispatcher.Invoke(new Action(() => SplashLauncher<SplashWindow>.SplashWindowInstance.SetProgressText(text_)));
        }
        else if (SplashLauncher<ModernSplashWindow>.SplashWindowInstance != null)
        {
            SplashLauncher<ModernSplashWindow>.SplashWindowInstance.Dispatcher.Invoke(new Action(() => SplashLauncher<ModernSplashWindow>.SplashWindowInstance.SetProgressText(text_)));
        }
    }
    public void BringToFront()
    {
        if (SplashLauncher<SplashWindow>.SplashWindowInstance != null)
        {
            SplashLauncher<SplashWindow>.SplashWindowInstance.Dispatcher.Invoke(new Action(() =>
                {
                    SplashLauncher<SplashWindow>.SplashWindowInstance.IsTopMost = true;
                    SplashLauncher<SplashWindow>.SplashWindowInstance.Activate();
                }));
        }
        else if (SplashLauncher<ModernSplashWindow>.SplashWindowInstance != null)
        {
            SplashLauncher<ModernSplashWindow>.SplashWindowInstance.Dispatcher.Invoke(new Action(() =>
            {
                SplashLauncher<ModernSplashWindow>.SplashWindowInstance.IsTopMost = true;
                SplashLauncher<ModernSplashWindow>.SplashWindowInstance.Activate();
            }));
        }
    }
      public void Close()
      {
          if (SplashLauncher<SplashWindow>.SplashWindowInstance != null)
          {
              SplashLauncher<SplashWindow>.SplashWindowInstance.Dispatcher.Invoke(new Action(SplashLauncher<SplashWindow>.Close));
          }
          else if (SplashLauncher<ModernSplashWindow>.SplashWindowInstance != null)
          { 
              SplashLauncher<ModernSplashWindow>.SplashWindowInstance.Dispatcher.Invoke(new Action(SplashLauncher<ModernSplashWindow>.Close));
          }
      }
      public bool IsVisible
      {
          get
          {
              return WindowInstance.Visibility == Visibility.Visible;
          }
          set
          {
              WindowInstance.Dispatcher.Invoke(
                  new Action(() =>
                        {
                            WindowInstance.Visibility = value
                                                            ? Visibility.Visible
                                                            : Visibility.Collapsed;
                        }));
          }
      }

      public void SetCurrentView(string text)
      {
          SetText(String.Format("Loading view {0}", text));
      }

      public void SetStatus(string text)
      {
          this.SetText(text);
      }
      public void ShowProgressBar(int maxSize)
      {
          if (SplashLauncher<ModernSplashWindow>.SplashWindowInstance != null)
          {
              SplashLauncher<ModernSplashWindow>.SplashWindowInstance.ShowProgressBar(maxSize);
          }
      }

      public void SetProgress(int value)
      {
          if (SplashLauncher<ModernSplashWindow>.SplashWindowInstance != null)
          {
              SplashLauncher<ModernSplashWindow>.SplashWindowInstance.SetProgress(value);
          }
      }

      public event EventHandler CancelRequested;

      protected virtual void OnCancelRequested()
      {
          var handler = CancelRequested;
          if (handler != null) handler(this, EventArgs.Empty);
      }

      #endregion

      private static Window WindowInstance
      {
          get
          {
              if (SplashLauncher<SplashWindow>.SplashWindowInstance != null)
              {
                  return SplashLauncher<SplashWindow>.SplashWindowInstance;
              }
              if (SplashLauncher<ModernSplashWindow>.SplashWindowInstance != null)
              {
                  return SplashLauncher<ModernSplashWindow>.SplashWindowInstance;
              }
              return null;
          }
      }
  }
}
